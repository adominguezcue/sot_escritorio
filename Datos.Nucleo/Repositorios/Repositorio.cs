﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Dominio.Nucleo.Repositorios;
using Dominio.Nucleo.Entidades;
using Datos.Nucleo.Contextos;

namespace Datos.Nucleo.Repositorios
{
    public class Repositorio<TClass> : IRepositorio<TClass> where TClass : class, IEntidad//, IEntidadComparable<TClass>
    {
        protected IContexto contexto;

        public Repositorio(IContexto c) 
        {
            contexto = c;
        }

        public virtual void Agregar(TClass elemento)
        {
            contexto.Set<TClass>().Add(elemento);
        }

        public virtual void Modificar(TClass elemento)
        {
            contexto.Set<TClass>().Attach(elemento);
            //contexto.Entry(elemento).CurrentValues.SetValues(elemento);
            contexto.Entry(elemento).State = System.Data.Entity.EntityState.Modified;

            var propiedades = elemento.ObtenerPropiedadesNavegacion();

            foreach (var item in propiedades.Where(m => m.EntidadEstado == EntidadEstados.Modificado))
            {
                contexto.Set(item.GetType()).Attach(item);
                //contexto.Entry(elemento).CurrentValues.SetValues(elemento);
                contexto.Entry(item).State = System.Data.Entity.EntityState.Modified;
            }

            foreach (var item in propiedades.Where(m => m.EntidadEstado == EntidadEstados.Creado))
            {
                contexto.Set(item.GetType()).Add(item);
                //contexto.Entry(elemento).CurrentValues.SetValues(elemento);
                contexto.Entry(item).State = System.Data.Entity.EntityState.Added;
            }

            //var collections = GetCollections(elemento);
            //foreach (var collection in collections)
            //{
            //    foreach (var r in collection)
            //    {
            //        if (contexto.Entry(r).State == System.Data.Entity.EntityState.Detached)
            //        {
            //            contexto.Set(r.GetType()).Attach(r);
            //            contexto.Entry(r).State = System.Data.Entity.EntityState.Modified;
            //        }
            //    }
            //}

            //Sincronizar(elemento);
        }

        public virtual void Eliminar(TClass elemento)
        {
            contexto.Set<TClass>().Remove(elemento);
        }

        public virtual TClass Obtener(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return contexto.Set<TClass>().FirstOrDefault(filtro);
        }
        public virtual TClass Obtener(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, object>> path)
        {
            return contexto.Set<TClass>().Include(path).FirstOrDefault(filtro);
        }

        public virtual TClass ObtenerOrdenado<TKey>(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, TKey>> orden)
        {
            return contexto.Set<TClass>().Where(filtro).OrderBy(orden).FirstOrDefault();
        }

        public virtual IEnumerable<TClass> ObtenerElementos(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return contexto.Set<TClass>().Where(filtro);
        }

        public IEnumerable<TClass> ObtenerElementos(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, object>> path)
        {
            return contexto.Set<TClass>().Include(path).Where(filtro);
        }

        public virtual IEnumerable<TClass> ObtenerElementos<TKey>(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, TKey>> orden, int pagina, int elementos)
        {
            return contexto.Set<TClass>().Where(filtro).OrderBy(orden).Skip(pagina * elementos).Take(elementos);
        }

        public virtual IEnumerable<TClass> ObtenerElementos<TKey>(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, object>> path, System.Linq.Expressions.Expression<Func<TClass, TKey>> orden, int pagina, int elementos)
        {
            return contexto.Set<TClass>().Include(path).Where(filtro).OrderBy(orden).Skip(pagina * elementos).Take(elementos);
        }

        public virtual IEnumerable<TClass> ObtenerTodo()
        {
            return contexto.Set<TClass>();
        }

        public virtual IEnumerable<TClass> ObtenerTodo(System.Linq.Expressions.Expression<Func<TClass, object>> path)
        {
            return contexto.Set<TClass>().Include(path);
        }

        public bool Alguno()
        {
            return contexto.Set<TClass>().Any();
        }

        public bool Alguno(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return contexto.Set<TClass>().Any(filtro);
        }

        public int Contador(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return contexto.Set<TClass>().Count(filtro);
        }

        public virtual int GuardarCambios()
        {
            return contexto.SaveChanges();
        }

        public virtual void DescartarCambios()
        {
            contexto.DescartarCambios();
        }

        private IEnumerable<IEnumerable<dynamic>> GetCollections(object o)
        {
            var result = new List<IEnumerable<dynamic>>();
            foreach (var prop in o.GetType().GetProperties())
            {
                if (typeof(IEnumerable<dynamic>).IsAssignableFrom(prop.PropertyType))
                {
                    var get = prop.GetGetMethod();
                    if (!get.IsStatic && get.GetParameters().Length == 0)
                    {
                        var enumerable = (IEnumerable<dynamic>)get.Invoke(o, null);
                        if (enumerable != null) result.Add(enumerable);
                    }
                }
            }
            return result;
        }


        //private void Sincronizar(TClass elemento)
        //{
        //    var comparador = elemento.Comparador();

        //    var elementoOriginal = contexto.Set<TClass>().FirstOrDefault(comparador);

        //    contexto.Entry(elementoOriginal).CurrentValues.SetValues(elemento);
        //    contexto.Entry(elementoOriginal).State = EntityState.Modified;

        //    elemento = elementoOriginal;
        //}
    }
}
