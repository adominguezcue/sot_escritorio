﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Datos.Nucleo.Contextos
{
    public interface IContexto
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbSet Set(Type entityType);
        DbEntityEntry Entry(object entity);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        List<TEntity> ConsultaSql<TEntity>(string sql, params object[] parametros);//where TEntity : class;//Dominio.Nucleo.Entidades.IEntidad;

        int EjecutarComandoSql(string sql, params object[] parametros);

        DataSet EjecutarComandoSqlSP(string sql, params object[] parametros);

        IEnumerable<System.Collections.ICollection> ConsultaMultipleSql(string sql, object[] parametros, params Type[] salida);

        int SaveChanges();
        void DescartarCambios();
    }
}
