﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Nucleo.Contextos
{
    public interface IConexionSOT
    {
        string OrigenSOT { get; set; }
        string DbSOT { get; set; }
        string UsuarioSOT { get; set; }
        string PassSOT { get; set; }
        bool SeguridadIntegradaSOT { get; set; }
    }
}
