﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Nucleo.Contextos
{
    public static class ConexionHelper
    {
        private static readonly string LLAVE_SOFTWARE = "SOFTWARE";
        private static readonly string LLAVE_SOT = "SOT";
        private static readonly string LLAVE_CONEXION = "CONEXION_DBSOT";

        private static SqlConnectionStringBuilder ConexionBuilder = null;//new SqlConnectionStringBuilder();

        static ConexionHelper()
        {
            CargarLlave();
        }

        public static string CadenaConexion
        {
            get { return ConexionBuilder?.ConnectionString; }
        }

        // all params are optional
        //public static void CambiarConexion(
        //    string initialCatalog = "",
        //    string dataSource = "",
        //    string userId = "",
        //    string password = "",
        //    bool integratedSecuity = true/*,
        //    string configConnectionStringName = ""*/)
        //{
        //    CambiarConexion(true, initialCatalog, dataSource, userId, password, integratedSecuity);
        //}

        public static void CambiarConexion(
            bool guardarLlave,
            string initialCatalog = "",
            string dataSource = "",
            string userId = "",
            string password = "",
            bool integratedSecuity = true)
        {
            if (ConexionBuilder == null)
                ConexionBuilder = new SqlConnectionStringBuilder();

            if (!string.IsNullOrEmpty(initialCatalog))
                ConexionBuilder.InitialCatalog = initialCatalog;
            if (!string.IsNullOrEmpty(dataSource))
                ConexionBuilder.DataSource = dataSource;

            ConexionBuilder.IntegratedSecurity = integratedSecuity;

            if (integratedSecuity)
            {
                ConexionBuilder.UserID = ConexionBuilder.Password = "";
            }
            else
            {
                ConexionBuilder.UserID = userId ?? "";
                ConexionBuilder.Password = password ?? "";
            }

            if (guardarLlave)
                GuardarLlave();
        }

        private static void GuardarLlave()
        {
            using (var llaveSoftware = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(LLAVE_SOFTWARE, true))
            {
                var llaveSOT = llaveSoftware.OpenSubKey(LLAVE_SOT, true);

                if (llaveSOT == null)
                    llaveSOT = llaveSoftware.CreateSubKey(LLAVE_SOT);

                using (llaveSOT)
                {
                    var key = llaveSOT.OpenSubKey(LLAVE_CONEXION, true);

                    if (key == null)
                        key = llaveSOT.CreateSubKey(LLAVE_CONEXION);

                    using (key)
                    {
                        /*

                         string initialCatalog = "",
                        string dataSource = "",
                        string userId = "",
                        string password = "",
                        bool integratedSecuity*/
                        key.SetValue(nameof(IConexionSOT.OrigenSOT), ConexionBuilder.DataSource);
                        key.SetValue(nameof(IConexionSOT.DbSOT), ConexionBuilder.InitialCatalog);
                        key.SetValue(nameof(IConexionSOT.UsuarioSOT), ConexionBuilder.UserID);
                        key.SetValue(nameof(IConexionSOT.PassSOT), ConexionBuilder.Password);
                        key.SetValue(nameof(IConexionSOT.SeguridadIntegradaSOT), ConexionBuilder.IntegratedSecurity);

                        key.Close();
                    }

                    llaveSOT.Close();
                }
                llaveSoftware.Close();
            }
        }

        private static void CargarLlave()
        {
            string dataSource = "", initialCatalog = "", userId = "", password = "";
            bool integratedSecuity = false;

            using (var llaveSoftware = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(LLAVE_SOFTWARE, false))
            {
                var llaveSOT = llaveSoftware.OpenSubKey(LLAVE_SOT, false);

                if (llaveSOT != null)
                {
                    using (llaveSOT)
                    {
                        var key = llaveSOT.OpenSubKey(LLAVE_CONEXION, false);

                        if (key != null)
                        {

                            using (key)
                            {
                                /*

                                 string initialCatalog = "",
                                string dataSource = "",
                                string userId = "",
                                string password = "",
                                bool integratedSecuity*/
                                dataSource = key.GetValue(nameof(IConexionSOT.OrigenSOT), "") as string;
                                initialCatalog = key.GetValue(nameof(IConexionSOT.DbSOT), "") as string;
                                userId = key.GetValue(nameof(IConexionSOT.UsuarioSOT), "") as string;
                                password = key.GetValue(nameof(IConexionSOT.PassSOT), "") as string;

                                bool.TryParse(key.GetValue(nameof(IConexionSOT.SeguridadIntegradaSOT), "") as string, out integratedSecuity);
                                //integratedSecuity = (key.GetValue(nameof(IConexionSOT.SeguridadIntegradaSOT)) as bool?) ?? false;

                                key.Close();
                            }
                        }
                        llaveSOT.Close();
                    }
                }
                llaveSoftware.Close();
            }

            CambiarConexion(false, initialCatalog, dataSource, userId, password, integratedSecuity);
        }

        public static List<T> Translate<T>(IObjectContextAdapter oca, DbDataReader reader)
        {
            return oca.ObjectContext.Translate<T>(reader).ToList();
        }
    }
}
