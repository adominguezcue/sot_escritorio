﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Permisos.Modelo;
using System.Windows.Forms;
namespace Permisos.Controlador
{
    public class Controlador
    {

        public class vistaroles
        {
            int _cod_user;
            int _cod_rol;
            string _nombre;
            public int cod_user
            {
                set { _cod_user = value; }
                get { return _cod_user; }
            }
            public int cod_rol
            {
                set { _cod_rol = value; }
                get { return _cod_rol; }
            }
            public string nombre
            {
                set { _nombre = value; }
                get { return _nombre; }
            }
        }
        public class vistapermisos
            
        {
            Boolean _habilita, _edita, _agrega, _elimina, _cerrar;
            public Boolean habilita
            {
                set { _habilita = value; }
                get { return _habilita; }
            }

            public Boolean agrega
            {
                set { _agrega = value; }
                get { return _agrega; }
            }
            public Boolean edita
            {
                set { _edita = value; }
                get { return _edita; }
            }
            public Boolean elimina
            {
                set { _elimina = value; }
                get { return _elimina; }
            }
            public Boolean cierra
            {
                set { _cerrar = value; }
                get { return _cerrar; }
            }
        }
        public class vistaautorizacion
        {
            int _cod_auto;
            public int cod_auto
            {
                set { _cod_auto = value; }
                get { return _cod_auto; }
            }
            string _descripcion;
            public string descripcion
            {
                set { _descripcion = value; }
                get { return _descripcion; }
            }
            Boolean _Autorizado;
            public Boolean  autorizado
            {
                set { _Autorizado = value; }
                get { return _Autorizado; }
            }
        }


        private DAODataContext dao;
        public Controlador(string conn)
        {
            dao = new DAODataContext(conn);
        }
        public List<ZctPermisos> get_permisosrol(int cod_rol, int cod_menu)
        {
            return (from p in dao.ZctPermisos where p.Cod_rol == cod_rol && p.Cod_menu == cod_menu select p).ToList();
        }
        public void insertaPermisosRolMenu(int cod_rol, int cod_menu)
        {
            // List<ZctMenus > menus=(from m in dao.ZctMenus).ToList();
            //foreach (ZctMenus menu in menus )
            //{
            ZctPermisos permiso = new ZctPermisos();
            permiso.Cod_menu = cod_menu;
            permiso.Cod_rol = cod_rol;
            permiso.habilitado = false;
            permiso.agregar = false;
            permiso.editar = false;
            permiso.eliminar = false;
            permiso.cerrar = false;
            dao.ZctPermisos.InsertOnSubmit(permiso);
            dao.SubmitChanges();
            //}
        }
        public List<ZctMenus> get_menus(string Nombre)
        {
            try
            {
                return (from M in dao.ZctMenus where M.nombre == Nombre select M).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error cargando menus para permisos de usuario: " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return null;
            }
        }
        public List<ZctMenus> get_menus()
        {
            return (from M in dao.ZctMenus select M).ToList();
        }
        public List<ZctRoles> get_roles()
        {
            return (from R in dao.ZctRoles select R).ToList();
        }
        public List<ZctRoles> get_roles(string Nombre)
        {
            return (from R in dao.ZctRoles where R.nombre == Nombre select R).ToList();
        }
        public int AgregarRol(string Nombre, string Descripcion)
        {
            List<ZctRoles> _Roles = get_roles(Nombre);
            if (_Roles.Count > 0)
            {
                //throw new Exception("Ya hay un rol con nombre " + Nombre);
                return _Roles[0].Cod_rol;
            }
            int Max = 0;
            if (get_roles().Count > 0)
            {
                Max = (from C in dao.ZctRoles select C.Cod_rol).Max();
            }


            var NuevoRol = new Permisos.Modelo.ZctRoles();
            NuevoRol.Cod_rol = Max + 1;
            NuevoRol.nombre = Nombre;
            NuevoRol.descripcion = Descripcion;
            NuevoRol.prioridad = 0;
            dao.ZctRoles.InsertOnSubmit(NuevoRol);
            return NuevoRol.Cod_rol;
        }
        public void ActualizaPermisos(int cod_rol, int cod_menu, Boolean Acceso, Boolean Agrega, Boolean Edita, Boolean Elimina, Boolean Cerrar)
        {
            List<ZctPermisos> permisos = (from p in dao.ZctPermisos where p.Cod_rol == cod_rol && p.Cod_menu == cod_menu select p).ToList();
            if (permisos.Count == 0)
            {
                throw new Exception("No hay roles con Cod_col= " + cod_rol);
            }
            permisos[0].habilitado = Acceso;
            permisos[0].agregar = Agrega;
            permisos[0].editar = Edita;
            permisos[0].eliminar = Elimina;
            permisos[0].cerrar = Cerrar;
        }
        public Boolean EliminaRol(int Cod_rol)
        {
            List<ZctRoles> Roles = (from R in dao.ZctRoles where R.Cod_rol == Cod_rol select R).ToList();
            if (Roles.Count == 0)
            {
                throw new Exception("No hay roles con Cod_col= " + Cod_rol);
            }
            List<ZctPermisos> Permisos = (from R in dao.ZctPermisos where R.Cod_rol == Cod_rol select R).ToList();
            List<ZctRolesUsuarios> Relacion = (from R in dao.ZctRolesUsuarios where R.Cod_Rol == Cod_rol select R).ToList();
            dao.ZctRoles.DeleteAllOnSubmit(Roles);
            dao.ZctPermisos.DeleteAllOnSubmit(Permisos);
            dao.ZctRolesUsuarios.DeleteAllOnSubmit(Relacion);

            return true;
        }
        public void CancelarCambios()
        {
            dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
        }
        public void GuardarCambios()
        {
            dao.SubmitChanges();
        }
        public Boolean ActualizaRol(int cod_rol, string nombre, string descripcion)
        {
            List<ZctRoles> Roles = (from R in dao.ZctRoles where R.Cod_rol == cod_rol select R).ToList();
            if (Roles.Count == 0)
            {
                throw new Exception("No hay roles con Cod_col= " + cod_rol);
            }
            Roles[0].nombre = nombre;
            Roles[0].descripcion = descripcion;
            return true;
        }
        public void InsertarPermisosMenuDefault(Boolean Permitido, string Rol)
        {
            List<ZctRoles> Roles = (from R in dao.ZctRoles where R.nombre == Rol select R).ToList();
            if (Roles.Count == 0)
            {
                throw new Exception("No hay roles con Nombre= " + Rol);
            }
            IList<ZctMenus> Menus = get_menus();
            foreach (ZctMenus m in Menus)
            {
                ZctPermisos Permiso = new Permisos.Modelo.ZctPermisos();
                Permiso.Cod_menu = m.Cod_menu;
                Permiso.Cod_rol = Roles[0].Cod_rol;
                Permiso.agregar = Permitido;
                Permiso.habilitado = Permitido;
                Permiso.editar = Permitido;
                Permiso.eliminar = Permitido;
                Permiso.cerrar = Permitido;
                dao.ZctPermisos.InsertOnSubmit(Permiso);
            }
        }
        public List<vistaroles> get_rolesusuario(int cod_usu)
        {
            var resultado = new List<vistaroles>();
            var v = from r in dao.ZctRolesUsuarios
                    join r2 in dao.ZctRoles
                    on r.Cod_Rol equals r2.Cod_rol
                    where r.Cod_Usu == cod_usu
                    select new { r, r2.nombre };
            foreach (var r in v)
            {
                var vista = new vistaroles();
                vista.nombre = r.nombre;
                vista.cod_rol = r.r.Cod_Rol;
                vista.cod_user = r.r.Cod_Usu;
                resultado.Add(vista);
            }
            return resultado;
        }
        public Boolean AgregaRolaUsuario(int cod_usu, int cod_rol)
        {
            List<ZctRolesUsuarios> roles = (from Ru in dao.ZctRolesUsuarios
                                            where Ru.Cod_Rol == cod_rol &&
                                            Ru.Cod_Usu == cod_usu
                                            select Ru).ToList();
            if (roles.Count > 0)
            {
                throw (new Exception("Ya tiene asignado este rol, seleccione otro"));
            }

            var RolUsu = new ZctRolesUsuarios();
            RolUsu.Cod_Usu = cod_usu;
            RolUsu.Cod_Rol = cod_rol;
            dao.ZctRolesUsuarios.InsertOnSubmit(RolUsu);
            //dao.SubmitChanges();
            return true;
        }
        public void EliminaRol(int cod_usu, int cod_rol)
        {
            List<ZctRolesUsuarios> roles = (from Ru in dao.ZctRolesUsuarios
                                            where Ru.Cod_Rol == cod_rol &&
                                            Ru.Cod_Usu == cod_usu
                                            select Ru).ToList();
            if (roles.Count < 0)
            {
                throw (new Exception("no tiene asignado este rol, seleccione otro"));
            }
            dao.ZctRolesUsuarios.DeleteAllOnSubmit(roles);
        }
        public List<VW_ZctPermisosUsuario> getpermisomenu(int cod_usu,
            string NombreObjeto)
        {
            return ((from vp in dao.VW_ZctPermisosUsuario
                     where vp.NombreMenu == (NombreObjeto)
                     select vp).ToList());

        }
        public List<VW_ZctPermisosUsuario> getpermisomenu(int cod_usu,
           string TextoMenu, Boolean habilitado)
        {
            return (from vp in dao.VW_ZctPermisosUsuario
                     where vp.NombreMenu == TextoMenu && vp.habilitado == habilitado && vp.Cod_Usu == cod_usu
                     select vp).ToList();
        }
        public List<VW_ZctPermisosUsuario> getpermisoventanas(int cod_usu,
           string Tituloventana)
        {
            List<VW_ZctPermisosUsuario> ResultadoLista = (from vp in dao.VW_ZctPermisosUsuario
                                                          where vp.titulo_formulario == Tituloventana
                                                          select vp).ToList();
            if (ResultadoLista.Count <= 0)
            {
                ResultadoLista.Add(new VW_ZctPermisosUsuario
                {
                    Cod_Rol = 0,
                    Cod_Usu = cod_usu,
                    habilitado = false,
                    agregar = false,
                    editar = false,
                    eliminar = false
                });
            }
            return ResultadoLista;
        }
        public vistapermisos getpermisoventana(int cod_usu, string Tituloventana)
        {
            var Permisos = new vistapermisos();

            Permisos.habilita = (from P in dao.VW_ZctPermisosUsuario
                             where P.titulo_formulario == Tituloventana && P.Cod_Usu == cod_usu
                                        && P.habilitado
                             select P.habilitado).Any();
            Permisos.agrega = (from P in dao.VW_ZctPermisosUsuario
                             where P.titulo_formulario == Tituloventana && P.Cod_Usu == cod_usu
                                       && P.agregar
                             select P.agregar).Any();
            Permisos.edita = (from P in dao.VW_ZctPermisosUsuario
                            where P.titulo_formulario == Tituloventana && P.Cod_Usu == cod_usu
                                        && P.editar
                            select P.editar).Any();
            Permisos.elimina = (from P in dao.VW_ZctPermisosUsuario
                               where P.titulo_formulario == Tituloventana && P.Cod_Usu == cod_usu
                                       && P.eliminar
                               select P.eliminar).Any();
            Permisos.cierra = (from P in dao.VW_ZctPermisosUsuario
                               where P.titulo_formulario == Tituloventana && P.Cod_Usu == cod_usu
                                       && P.cerrar == true
                              select P.eliminar).Any();

            //Permisos.habilita = PerAcceso > 0;
            //Permisos.agrega = PerAgrega > 0;
            //Permisos.edita = PerEdita > 0;
            //Permisos.elimina = PerEliminar > 0;
            //Permisos.cierra = PerCerrar > 0;

            return Permisos;
        }

        public void Recorrermenu(int cod_usu, MenuStrip menu)
        {
            //try
            //{
            int CodSuperAdministrador = AgregarRol("SUPER ADMINISTRADOR", "LO HACE TODO");
            dao.SubmitChanges();

            foreach (ToolStripMenuItem T in (from ToolStripMenuItem S in menu.Items
                                             where S.GetType().FullName == "System.Windows.Forms.ToolStripMenuItem"
                                             select S).ToList())
            {
                if (T.Name == "WindowsMenu" || T.Name == "SalirToolStripMenuItem")
                    continue;

                string Nombre = T.Name;
                List<Permisos.Modelo.VW_ZctPermisosUsuario> PermisoMenu = getpermisomenu(cod_usu, Nombre, true);
                if (PermisoMenu.Count > 0)
                {
                    T.Visible = T.Enabled;
                }
                else
                {
                    T.Visible = false;
                    string titulo = "";
                    if (T.Tag == null)
                        T.Tag = T.Text;

                    PermisoMenu = getpermisomenu(cod_usu, Nombre, false);
                    if (PermisoMenu.Count == 0)
                    {
                        AgregaMenu(T.Name, T.Tag.ToString(), titulo, CodSuperAdministrador);
                        PermisoMenu = getpermisomenu(cod_usu, Nombre, true);

                        T.Visible = PermisoMenu.Count > 0 && T.Enabled && PermisoMenu[0].habilitado;
                    }

                }
                RecorrerSubMenu(T, cod_usu, CodSuperAdministrador);
                Application.DoEvents();
            }
            //menu.Visible = true;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Error cargando permisos usuario " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
        }
        private void RecorrerSubMenu(ToolStripMenuItem menuItem, int cod_usu, int cod_rol)
        {

            foreach (object subMenuItem in menuItem.DropDownItems)
            {
                //MessageBox.Show(subMenuItem.GetType().FullName);
                if (subMenuItem.GetType().FullName == "System.Windows.Forms.ToolStripSeparator")
                    continue;

                var O = (ToolStripMenuItem)subMenuItem;
                string Nombre = O.Name;
                //Llamada recursiva
                RecorrerSubMenu(O, cod_usu, cod_rol);
                List<Permisos.Modelo.VW_ZctPermisosUsuario> PermisoMenu = getpermisomenu(cod_usu, Nombre, true);
                if (PermisoMenu.Count > 0)
                {
                    O.Visible = PermisoMenu[0].habilitado && O.Enabled;
                }
                else
                {
                    //O.Enabled = false ;
                    ToolStripMenuItem Sm = (ToolStripMenuItem)subMenuItem;
                    string titulo = "";
                    if (Sm.Tag != null)
                        titulo = Sm.Tag.ToString();

                    AgregaMenu(Sm.Name, Sm.Text, titulo, cod_rol);
                    PermisoMenu = getpermisomenu(cod_usu, Nombre, true);

                    O.Visible = PermisoMenu.Count > 0 && O.Enabled;
                }
                Application.DoEvents();
            }

        }
        public void AgregaMenu(string nombreobjeto, string TextoMenu, string TituloFormulario, int cod_rol)
        {
            var Existe = (from Modelo.VW_ZctPermisosUsuario m in dao.VW_ZctPermisosUsuario where m.texto == TextoMenu && m.NombreMenu == nombreobjeto select m).ToList();
            if (Existe.Count > 1)
            {
                return;
            }
            List<Modelo.ZctMenus> Menus = (from Modelo.ZctMenus M in dao.ZctMenus
                                           where M.texto == TextoMenu
                                               && M.nombre == nombreobjeto
                                           select M).ToList();
            if (Menus.Count == 0)
            {
                Modelo.ZctMenus Menu = new ZctMenus();
                Menu.texto = TextoMenu;
                Menu.titulo_formulario = TituloFormulario;
                Menu.nombre = nombreobjeto;
                dao.ZctMenus.InsertOnSubmit(Menu);
                dao.SubmitChanges();
            }
            List<int> CodMenus = (from Modelo.ZctMenus m in dao.ZctMenus
                                  where m.texto == TextoMenu && m.nombre == nombreobjeto
                                  select Convert.ToInt32(m.Cod_menu)).ToList();
            if (CodMenus.Count == 0)
            {
                CodMenus = null;
                throw new Exception("No existe ningun menu con el texto" + TextoMenu);
            }
            Modelo.ZctPermisos Permisos = new ZctPermisos();
            Permisos.Cod_rol = cod_rol;
            Permisos.agregar = true;
            Permisos.editar = true;
            Permisos.eliminar = true;
            Permisos.habilitado = true;
            Permisos.Cod_menu = CodMenus[0];
            dao.ZctPermisos.InsertOnSubmit(Permisos);
            dao.SubmitChanges();
        }

        public List<vistaautorizacion> traeautorizacionesusuario(int cod_usu)
        {
            List<vistaautorizacion> autorizado = new List<vistaautorizacion>();
            List<Modelo.ZctCatAut> catauto = (from cat in dao.ZctCatAut select cat).ToList();

            foreach (Modelo.ZctCatAut auto in catauto)
            {
                vistaautorizacion a = new vistaautorizacion();
                a.cod_auto = auto.Cod_Aut;
                a.descripcion = auto.Desc_Aut;
                List<Modelo.ZctSegAut> autoriza = (from aut in dao.ZctSegAut where aut.Cod_Aut == auto.Cod_Aut && aut.Cod_Usu == cod_usu select aut).ToList();
                if (autoriza.Count == 0)
                {
                    a.autorizado = false;
                }
                else
                {
                    a.autorizado = true;
                }
                autorizado.Add(a);
            }
            return autorizado;
        }
        public void agregaautorizacion(int cod_usu,int cod_auto,string descripcion)
        {
            Modelo.ZctSegAut autorizacion = new ZctSegAut();
            autorizacion.Cod_Aut = cod_auto;
            autorizacion.Cod_Usu  = cod_usu;
            autorizacion.Desc_SegAut = descripcion;
            dao.ZctSegAut.InsertOnSubmit(autorizacion);
        }
        public void eliminaautorizacion(int cod_usu, int cod_auto)
        {
           List < Modelo.ZctSegAut> auto =(from a in dao.ZctSegAut 
                                        where a.Cod_Usu==cod_usu && a.Cod_Aut==cod_auto select a ).ToList ();
           if (auto.Count > 0)
           {
               dao.ZctSegAut.DeleteAllOnSubmit(auto);
           }
        }
    }
}