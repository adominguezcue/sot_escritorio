﻿using System;

[Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
public partial class RolesUsuarios : System.Windows.Forms.Form
{
	//Form reemplaza a Dispose para limpiar la lista de componentes.
	[System.Diagnostics.DebuggerNonUserCode()]
	protected override void Dispose(bool disposing)
	{
		try
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
		}
		finally
		{
			base.Dispose(disposing);
		}
	}

	//Requerido por el Diseñador de Windows Forms
	private System.ComponentModel.IContainer components;

	//NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	//Se puede modificar usando el Diseñador de Windows Forms.  
	//No lo modifique con el editor de código.
	[System.Diagnostics.DebuggerStepThrough()]
	private void InitializeComponent()
	{
		this.components = new System.ComponentModel.Container();
		this.ZctRolesUsuariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
		this.zctRolesUsuario = new System.Windows.Forms.BindingSource(this.components);
		this.ZctRoles = new System.Windows.Forms.BindingSource(this.components);
		this.Belimina = new ZctSOT.ZctSOTButton();
		this.Basigna = new ZctSOT.ZctSOTButton();
		this.CboRoles = new ZctSOT.ZctControlCombo();
		this.Bmod = new ZctSOT.ZctSOTButton();
		this.Bcancelar = new ZctSOT.ZctSOTButton();
		this.Bguardar = new ZctSOT.ZctSOTButton();
		this.grdDatos = new ZctSOT.ZctSotGrid();
		this.cboUsuario = new ZctSOT.ZctControlCombo();
		((System.ComponentModel.ISupportInitialize)this.ZctRolesUsuariosBindingSource).BeginInit();
		((System.ComponentModel.ISupportInitialize)this.zctRolesUsuario).BeginInit();
		((System.ComponentModel.ISupportInitialize)this.ZctRoles).BeginInit();
		((System.ComponentModel.ISupportInitialize)this.grdDatos).BeginInit();
		this.SuspendLayout();
		//
		//ZctRolesUsuariosBindingSource
		//
		this.ZctRolesUsuariosBindingSource.DataMember = "ZctRolesUsuarios";
		this.ZctRolesUsuariosBindingSource.DataSource = this.ZctRoles;
		//
		//ZctRoles
		//
		this.ZctRoles.DataSource = typeof(Permisos.Modelo.ZctRoles);
		//
		//Belimina
		//
		this.Belimina.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		this.Belimina.Enabled = false;
		this.Belimina.Location = new System.Drawing.Point(275, 292);
		this.Belimina.Name = "Belimina";
		this.Belimina.Size = new System.Drawing.Size(75, 23);
		this.Belimina.TabIndex = 7;
		this.Belimina.Text = "Eliminar Rol";
		this.Belimina.UseVisualStyleBackColor = true;
		//
		//Basigna
		//
		this.Basigna.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		this.Basigna.Enabled = false;
		this.Basigna.Location = new System.Drawing.Point(113, 292);
		this.Basigna.Name = "Basigna";
		this.Basigna.Size = new System.Drawing.Size(75, 23);
		this.Basigna.TabIndex = 6;
		this.Basigna.Text = "Asignar Rol ";
		this.Basigna.UseVisualStyleBackColor = true;
		//
		//CboRoles
		//
		this.CboRoles.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.CboRoles.Enabled = false;
		this.CboRoles.Location = new System.Drawing.Point(58, 228);
		this.CboRoles.Name = "CboRoles";
		this.CboRoles.Nombre = "Roles Usuario";
		this.CboRoles.Size = new System.Drawing.Size(463, 28);
		this.CboRoles.TabIndex = 5;
		this.CboRoles.value = null;
		this.CboRoles.ValueItem = 0;
		this.CboRoles.ValueItemStr = null;
		//
		//Bmod
		//
		this.Bmod.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		this.Bmod.Location = new System.Drawing.Point(243, 262);
		this.Bmod.Name = "Bmod";
		this.Bmod.Size = new System.Drawing.Size(75, 23);
		this.Bmod.TabIndex = 4;
		this.Bmod.Text = "Modificar";
		this.Bmod.UseVisualStyleBackColor = true;
		//
		//Bcancelar
		//
		this.Bcancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		this.Bcancelar.Location = new System.Drawing.Point(373, 292);
		this.Bcancelar.Name = "Bcancelar";
		this.Bcancelar.Size = new System.Drawing.Size(75, 23);
		this.Bcancelar.TabIndex = 3;
		this.Bcancelar.Text = "Cancelar";
		this.Bcancelar.UseVisualStyleBackColor = true;
		//
		//Bguardar
		//
		this.Bguardar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		this.Bguardar.Enabled = false;
		this.Bguardar.Location = new System.Drawing.Point(194, 292);
		this.Bguardar.Name = "Bguardar";
		this.Bguardar.Size = new System.Drawing.Size(75, 23);
		this.Bguardar.TabIndex = 2;
		this.Bguardar.Text = "Guardar";
		this.Bguardar.UseVisualStyleBackColor = true;
		//
		//grdDatos
		//
		this.grdDatos.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.grdDatos.AutoGenerateColumns = false;
		this.grdDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
		this.grdDatos.DataSource = this.zctRolesUsuario;
		this.grdDatos.Location = new System.Drawing.Point(58, 59);
		this.grdDatos.Name = "grdDatos";
		this.grdDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
		this.grdDatos.Size = new System.Drawing.Size(463, 157);
		this.grdDatos.TabIndex = 1;
		//
		//cboUsuario
		//
		this.cboUsuario.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.cboUsuario.Location = new System.Drawing.Point(58, 12);
		this.cboUsuario.Name = "cboUsuario";
		this.cboUsuario.Nombre = "Usuario";
		this.cboUsuario.Size = new System.Drawing.Size(463, 28);
		this.cboUsuario.TabIndex = 0;
		this.cboUsuario.value = null;
		this.cboUsuario.ValueItem = 0;
		this.cboUsuario.ValueItemStr = null;
		//
		//RolesUsuarios
		//
		this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		this.ClientSize = new System.Drawing.Size(560, 327);
		this.Controls.Add(this.Belimina);
		this.Controls.Add(this.Basigna);
		this.Controls.Add(this.CboRoles);
		this.Controls.Add(this.Bmod);
		this.Controls.Add(this.Bcancelar);
		this.Controls.Add(this.Bguardar);
		this.Controls.Add(this.grdDatos);
		this.Controls.Add(this.cboUsuario);
		this.Name = "RolesUsuarios";
		this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
		this.Text = "Roles Usuarios";
		((System.ComponentModel.ISupportInitialize)this.ZctRolesUsuariosBindingSource).EndInit();
		((System.ComponentModel.ISupportInitialize)this.zctRolesUsuario).EndInit();
		((System.ComponentModel.ISupportInitialize)this.ZctRoles).EndInit();
		((System.ComponentModel.ISupportInitialize)this.grdDatos).EndInit();
		this.ResumeLayout(false);

//INSTANT C# NOTE: Converted event handler wireups:
		base.Load += new System.EventHandler(RolesUsuarios_Load);
		cboUsuario.SelectedValueChanged += new System.EventHandler(cboUsuario_SelectedValueChanged);
		Bmod.Click += new System.EventHandler(Bmod_Click);
		Basigna.Click += new System.EventHandler(Basigna_Click);
		grdDatos.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(grdDatos_RowEnter);
		Bguardar.Click += new System.EventHandler(Buardar_Click);
		Bcancelar.Click += new System.EventHandler(Bcancelar_Click);
		Belimina.Click += new System.EventHandler(Belimina_Click);
	}
	internal ZctSOT.ZctControlCombo cboUsuario;
	internal ZctSOT.ZctSotGrid grdDatos;
	internal ZctSOT.ZctSOTButton Bguardar;
	internal ZctSOT.ZctSOTButton Bcancelar;
	internal ZctSOT.ZctSOTButton Bmod;
	internal System.Windows.Forms.BindingSource ZctRoles;
	internal ZctSOT.ZctControlCombo CboRoles;
	internal ZctSOT.ZctSOTButton Basigna;
	internal System.Windows.Forms.BindingSource zctRolesUsuario;
	internal System.Windows.Forms.BindingSource ZctRolesUsuariosBindingSource;
	internal ZctSOT.ZctSOTButton Belimina;
}
