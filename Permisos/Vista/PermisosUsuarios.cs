﻿using System;
using Permisos.Modelo;

public partial class PermisosUsuarios
{
	internal PermisosUsuarios()
	{
		InitializeComponent();
	}

	public class PermisosModificados
	{
		private int _Rol;
		private int _Menu;
		private Permisos.Modelo.ZctPermisos _permisos;
		public int Rol
		{
			set
			{
				_Rol = value;
			}
			get
			{
				return _Rol;
			}
		}
		public int Menu
		{
			set
			{
				_Menu = value;

			}
			get
			{
				return _Menu;
			}
		}
		public Permisos.Modelo.ZctPermisos Permisos
		{
			set
			{
				_permisos = value;
			}
			get
			{
				return _permisos;
			}
		}
	}
	internal Permisos.Controlador.Controlador _Controlador = new Permisos.Controlador.Controlador(DAO.Coneccion.CadenaConexion);
	private string TextoNodo;
	private List<string> _Accion = new List<string>();
	private Dictionary<string, PermisosModificados> _PermisosModificados = new Dictionary<string, PermisosModificados>();
	private List<Permisos.Modelo.ZctPermisos> _permisos = new List<Permisos.Modelo.ZctPermisos>();
	private bool _Todos;
	private string Key = "";
	private TreeNode[] CreateTreeNodesForMenuItems(ToolStripMenuItem menuItem)
	{
		List<TreeNode> nodes = new List<TreeNode>();
//INSTANT C# NOTE: Commented this declaration since looping variables in 'foreach' loops are declared in the 'foreach' header in C#:
//		dynamic subMenuItem = null;
		foreach (dynamic subMenuItem in menuItem.DropDownItems)
		{
			if (subMenuItem.GetType().FullName != "System.Windows.Forms.ToolStripMenuItem")
			{
				continue;
			}
			TreeNode node = new TreeNode(subMenuItem.Text);
			string Nombre = subMenuItem.Name;
			//Llamada recursiva
			node.Nodes.AddRange(CreateTreeNodesForMenuItems((ToolStripMenuItem)subMenuItem));
			List<Permisos.Modelo.ZctMenus> IdMenu = _Controlador.get_menus(Nombre);
			if (IdMenu.Count > 0)
			{
				node.Tag = IdMenu(0);
			}
			else
			{
				//MsgBox(Nombre)
			}
			nodes.Add(node);
		}

		return nodes.ToArray();
	}
	private void AltaPerfil_Load(object sender, System.EventArgs e)
	{
		foreach (ToolStripMenuItem N in ZctSOTMain.MainMenuStrip.Items)
		{
			string Nombre = N.Name;
			TreeNode nodo = new TreeNode(N.Text);
			List<Permisos.Modelo.ZctMenus> IdMenu = _Controlador.get_menus(Nombre);
			if (IdMenu.Count > 0)
			{
				nodo.Tag = IdMenu(0);
			}
			nodo.Nodes.AddRange(CreateTreeNodesForMenuItems(N));
			ArbolMenu.Nodes.Add(nodo);
			Application.DoEvents();
		}
		CboPerfiles.GetData("Zctroles");
		CboPerfiles.Enabled = true;
	}
	private void ActualizaDatos(string key)
	{
		if (ArbolMenu.SelectedNode == null)
		{
			return;
		}
		if (_PermisosModificados.ContainsKey(key) == false)
		{

			Permisos.Modelo.ZctPermisos p = new Permisos.Modelo.ZctPermisos();
			p.habilitado = chAcceso.Checked;
			p.agregar = chAgregar.Checked;
			p.editar = ChEditar.Checked;
			p.eliminar = ChEliminar.Checked;

			PermisosModificados PM = new PermisosModificados();
			var Datos = key.Split('-');
			PM.Menu = Datos(1);
			PM.Rol = Datos(0);
			PM.Permisos = p;
			_PermisosModificados.Add(key, PM);
		}
		else
		{
			_PermisosModificados(key).Permisos.habilitado = chAcceso.Checked;
			_PermisosModificados(key).Permisos.agregar = chAgregar.Checked;
			_PermisosModificados(key).Permisos.editar = ChEditar.Checked;
			_PermisosModificados(key).Permisos.eliminar = ChEliminar.Checked;
		}
	}
	private void CargarPermisos()
	{
		if ((ArbolMenu.SelectedNode == null))
		{
			return;
		}
		BguardarP.Enabled = true;
		//BguardarPerfil.Enabled = True
		TextoNodo = ArbolMenu.SelectedNode.Text;
		Permisos.Modelo.ZctMenus Menu = ArbolMenu.SelectedNode.Tag as Permisos.Modelo.ZctMenus;
		if (SimulateIsNumeric.IsNumeric(ArbolMenu.SelectedNode.Tag.cod_menu) == false)
		{
			this.GroupBox.Enabled = false;
			return;
		}
		else
		{
			this.GroupBox.Enabled = true;
		}
		string Key = CboPerfiles.value + "-" + ArbolMenu.SelectedNode.Tag as Permisos.Modelo.ZctMenus.Cod_menu;
		if (_PermisosModificados.ContainsKey(Key))
		{
			_permisos.Add(_PermisosModificados(Key).Permisos);
			//MsgBox(_PermisosModificados(Key).Permisos.habilitado)
		}
		else
		{
			_permisos = _Controlador.get_permisosrol(CboPerfiles.value, Menu.Cod_menu);
		}
		if (_permisos.Count == 0)
		{
			Gpermisos.Enabled = false;
			return;
		}
		else
		{
			Gpermisos.Enabled = true;
		}
		chAcceso.Checked = _permisos(0).habilitado;
		chAgregar.Checked = _permisos(0).agregar;
		ChEditar.Checked = _permisos(0).editar;
		ChEliminar.Checked = _permisos(0).eliminar;
	}


	private void CboPerfiles_SelectedValueChanged(object sender, System.EventArgs e)
	{
		if (SimulateIsNumeric.IsNumeric(CboPerfiles.value) == false)
		{
			return;
		}
		List<Permisos.Modelo.ZctRoles> Roles = _Controlador.get_roles(CboPerfiles.ZctSOTCombo1.Text);
		if (Roles.Count > 0)
		{
			TxtNombre.Text = Roles(0).nombre;
			TxtDescripcionPerfil.Text = Roles(0).descripcion;
		}
		else
		{
			TxtNombre.Text = "";
			TxtDescripcionPerfil.Text = "";
		}
		CargarPermisos();
	}
	private void BperfilNuevo_Click(object sender, System.EventArgs e)
	{
		try
		{
			BcancelaE.Enabled = true;
			_Accion.Add("N");
			TxtNombre.Text = "";
			TxtDescripcionPerfil.Text = "";
			long _perfil = CboPerfiles.value;
			ArbolMenu.Enabled = false;
			BperfilNuevo.Enabled = false;
			TxtNombre.Enabled = true;
			CboPrioridad.Enabled = true;
			BeliminaPerfil.Enabled = false;
			Beditar.Enabled = false;
			BguardarPerfil.Enabled = true;
			BcancelaE.Enabled = true;
			CboPerfiles.Enabled = false;
			TxtDescripcionPerfil.Enabled = true;
			TxtNombre.Focus();
		}
		catch (Exception ex)
		{
			MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
	}
	private void Beditar_Click(object sender, System.EventArgs e)
	{
		BcancelaE.Enabled = true;
		_Accion.Add("E");
		BguardarPerfil.Enabled = true;
		Beditar.Enabled = false;
		ArbolMenu.Enabled = false;
		BguardarPerfil.Enabled = false;
		BeliminaPerfil.Enabled = false;
		CboPerfiles.Enabled = false;
		BperfilNuevo.Enabled = false;
		BcancelaE.Enabled = true;
		CboPerfiles.Enabled = false;
		TxtDescripcionPerfil.Enabled = true;
		TxtNombre.Enabled = true;
		CboPrioridad.Enabled = true;
		BguardarPerfil.Enabled = true;
	}

	private void BeliminaPerfil_Click(object sender, System.EventArgs e)
	{
		try
		{
			BguardarPerfil.Enabled = true;
			ArbolMenu.Enabled = true;
			_Accion.Add("B");

			if (_Controlador.EliminaRol(CboPerfiles.value))
			{
				CboPerfiles.ZctSOTCombo1.DataSource = null;
				CboPerfiles.GetData("Zctroles");
			}
		}
		catch (Exception ex)
		{
			MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
	}

	private void BguardarPerfil_Click(object sender, System.EventArgs e)
	{
		try
		{
			ArbolMenu.Enabled = true;
			foreach (string A in _Accion)
			{
				switch (A)
				{
					case "N":
						_Controlador.AgregarRol(TxtNombre.Text, TxtDescripcionPerfil.Text);
						_Controlador.GuardarCambios();
						_Controlador.InsertarPermisosMenuDefault(false, TxtNombre.Text);
						_Controlador.GuardarCambios();
						break;
					case ("E"):
						_Controlador.ActualizaRol(CboPerfiles.value, TxtNombre.Text, TxtDescripcionPerfil.Text);
						_Controlador.GuardarCambios();
						break;
					case "B":
						_Controlador.EliminaRol(CboPerfiles.value);
						_Controlador.GuardarCambios();
						break;
					default:
						return;
				}
				Application.DoEvents();
			}
			TxtNombre.Enabled = false;
			TxtDescripcionPerfil.Enabled = false;
			CboPrioridad.Enabled = false;
			BeliminaPerfil.Enabled = true;
			BperfilNuevo.Enabled = true;
			Beditar.Enabled = true;
			BcancelaE.Enabled = true;
			CboPerfiles.Enabled = true;
			BguardarPerfil.Enabled = false;
			BcancelaE.Enabled = false;
			CboPerfiles.GetData("Zctroles");
			CboPerfiles.Focus();
			_Accion.Clear();
			MessageBox.Show("Cambios en los roles de usuario Guardados correctamente", "Realiizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		catch (Exception ex)
		{
			MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}

	private void BcancelaE_Click(object sender, System.EventArgs e)
	{
		try
		{
			ArbolMenu.Enabled = true;
			_Controlador.CancelarCambios();
			BcancelaE.Enabled = false;
			BperfilNuevo.Enabled = true;
			TxtNombre.Enabled = false;
			TxtDescripcionPerfil.Enabled = false;
			CboPrioridad.Enabled = false;
			BeliminaPerfil.Enabled = true;
			Beditar.Enabled = true;
			BcancelaE.Enabled = true;
			CboPerfiles.Enabled = true;
			MessageBox.Show("Los cambios se han revertido", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
		catch (Exception ex)
		{
			MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
	}

	private void ZctSOTButton6_Click(object sender, System.EventArgs e)
	{
		_Todos = false;
		if (ArbolMenu.SelectedNode == null)
		{
			return;
		}
		foreach (PermisosModificados PM in _PermisosModificados.Values)
		{
			_Controlador.ActualizaPermisos(CboPerfiles.value, PM.Menu, PM.Permisos.habilitado, PM.Permisos.agregar, PM.Permisos.editar, PM.Permisos.eliminar);
			_Controlador.GuardarCambios();
			Application.DoEvents();
		}
		_PermisosModificados.Clear();
		_permisos.Clear();
		_Controlador.Recorrermenu(ZctSOTMain._UsuAct, ZctSOTMain.MenuStrip);
		MessageBox.Show("Permisos menu/ventana Guardados", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
	}

	private void ZctSOTButton5_Click(object sender, System.EventArgs e)
	{
		_PermisosModificados.Clear();
		this.Close();
	}

	private void ArbolMenu_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
	{
		Key = CboPerfiles.value + "-" + ArbolMenu.SelectedNode.Tag as Permisos.Modelo.ZctMenus.Cod_menu;
		CargarPermisos();
	}

	private void ArbolMenu_NodeMouseClick(object sender, System.Windows.Forms.TreeNodeMouseClickEventArgs e)
	{
		//CArgarpermisos()

	}

	private void chAcceso_Click(object sender, System.EventArgs e)
	{
		ActualizaDatos(Key);
	}



	private void CheckBox1_CheckedChanged(object sender, System.EventArgs e)
	{
	  _Todos = true;
		chAgregar.Checked = sender.Checked;
		foreach (TreeNode Nodo in ArbolMenu.SelectedNode.Nodes)
		{
			var Keyl = CboPerfiles.value + "-" + Nodo.Tag as Permisos.Modelo.ZctMenus.Cod_menu;
			ActualizaDatos(Keyl);
		}
		ArbolMenu.Focus();
		_Todos = false;
	}


	private void CheckBox2_CheckedChanged(object sender, System.EventArgs e)
	{
	   _Todos = true;
		ChEditar.Checked = sender.Checked;
		foreach (TreeNode Nodo in ArbolMenu.SelectedNode.Nodes)
		{
			var Keyl = CboPerfiles.value + "-" + Nodo.Tag as Permisos.Modelo.ZctMenus.Cod_menu;
			ActualizaDatos(Keyl);
		}
		ArbolMenu.Focus();
		_Todos = false;
	}

	private void CheckBox3_CheckedChanged(object sender, System.EventArgs e)
	{
	  _Todos = true;
		ChEliminar.Checked = sender.Checked;
		foreach (TreeNode Nodo in ArbolMenu.SelectedNode.Nodes)
		{
			var Keyl = CboPerfiles.value + "-" + Nodo.Tag as Permisos.Modelo.ZctMenus.Cod_menu;
			ActualizaDatos(Keyl);
		}
		ArbolMenu.Focus();
		_Todos = false;
	}

	private void Chmenus_Click(object sender, System.EventArgs e)
	{
		 _Todos = true;
		chAcceso.Checked = sender.Checked;
		foreach (TreeNode Nodo in ArbolMenu.Nodes)
		{
			var Keyl = CboPerfiles.value + "-" + Nodo.Tag as Permisos.Modelo.ZctMenus.Cod_menu;
			ActualizaDatos(Keyl);
		}
		ArbolMenu.Focus();
		_Todos = false;
	}

	private void Chsm_Click(object sender, System.EventArgs e)
	{
	}

	private void Chsm_CheckedChanged(object sender, System.EventArgs e)
	{
		_Todos = true;
		chAcceso.Checked = sender.Checked;
		foreach (TreeNode Nodo in ArbolMenu.SelectedNode.Nodes)
		{
			var Keyl = CboPerfiles.value + "-" + Nodo.Tag as Permisos.Modelo.ZctMenus.Cod_menu;
			ActualizaDatos(Keyl);
		}
		ArbolMenu.Focus();
		_Todos = false;

	}

	private void chAcceso_CheckedChanged(object sender, System.EventArgs e)
	{

	}

	private void Chmenus_CheckedChanged(object sender, System.EventArgs e)
	{

	}

	private void CboPerfiles_Load(object sender, System.EventArgs e)
	{

	}
}