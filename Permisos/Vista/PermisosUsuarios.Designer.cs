﻿using System;
using Permisos.Modelo;

[Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
public partial class PermisosUsuarios : System.Windows.Forms.Form
{
	//Form reemplaza a Dispose para limpiar la lista de componentes.
	[System.Diagnostics.DebuggerNonUserCode()]
	protected override void Dispose(bool disposing)
	{
		try
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
		}
		finally
		{
			base.Dispose(disposing);
		}
	}

	//Requerido por el Diseñador de Windows Forms
	private System.ComponentModel.IContainer components;

	//NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
	//Se puede modificar usando el Diseñador de Windows Forms.  
	//No lo modifique con el editor de código.
	[System.Diagnostics.DebuggerStepThrough()]
	private void InitializeComponent()
	{
		this.components = new System.ComponentModel.Container();
		this.ArbolMenu = new System.Windows.Forms.TreeView();
		this.GroupBox = new System.Windows.Forms.GroupBox();
		this.CboPrioridad = new ZctSOT.ZctControlCombo();
		this.BguardarPerfil = new ZctSOT.ZctSOTButton();
		this.BcancelaE = new ZctSOT.ZctSOTButton();
		this.BeliminaPerfil = new ZctSOT.ZctSOTButton();
		this.Beditar = new ZctSOT.ZctSOTButton();
		this.BperfilNuevo = new ZctSOT.ZctSOTButton();
		this.TxtDescripcionPerfil = new ZctSOT.ZctControlTexto();
		this.ZctRolesBindingSource = new System.Windows.Forms.BindingSource(this.components);
		this.TxtNombre = new ZctSOT.ZctControlTexto();
		this.Gpermisos = new System.Windows.Forms.GroupBox();
		this.CheckBox3 = new System.Windows.Forms.CheckBox();
		this.CheckBox2 = new System.Windows.Forms.CheckBox();
		this.chAgregas = new System.Windows.Forms.CheckBox();
		this.ChEliminar = new System.Windows.Forms.CheckBox();
		this.ChEditar = new System.Windows.Forms.CheckBox();
		this.chAgregar = new System.Windows.Forms.CheckBox();
		this.chAcceso = new System.Windows.Forms.CheckBox();
		this.BSroles = new System.Windows.Forms.BindingSource(this.components);
		this.Chmenus = new System.Windows.Forms.CheckBox();
		this.Chsm = new System.Windows.Forms.CheckBox();
		this.GroupBox1 = new System.Windows.Forms.GroupBox();
		this.ZctSOTButton5 = new ZctSOT.ZctSOTButton();
		this.BguardarP = new ZctSOT.ZctSOTButton();
		this.CboPerfiles = new ZctSOT.ZctControlCombo();
		this.TxtDescripcionPerfil1 = new ZctSOT.ZctControlTexto();
		this.GroupBox.SuspendLayout();
		((System.ComponentModel.ISupportInitialize)this.ZctRolesBindingSource).BeginInit();
		this.Gpermisos.SuspendLayout();
		((System.ComponentModel.ISupportInitialize)this.BSroles).BeginInit();
		this.GroupBox1.SuspendLayout();
		this.SuspendLayout();
		//
		//ArbolMenu
		//
		this.ArbolMenu.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left);
		this.ArbolMenu.FullRowSelect = true;
		this.ArbolMenu.HideSelection = false;
		this.ArbolMenu.Location = new System.Drawing.Point(15, 12);
		this.ArbolMenu.Name = "ArbolMenu";
		this.ArbolMenu.Size = new System.Drawing.Size(251, 409);
		this.ArbolMenu.TabIndex = 0;
		//
		//GroupBox
		//
		this.GroupBox.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.GroupBox.Controls.Add(this.CboPrioridad);
		this.GroupBox.Controls.Add(this.BguardarPerfil);
		this.GroupBox.Controls.Add(this.BcancelaE);
		this.GroupBox.Controls.Add(this.BeliminaPerfil);
		this.GroupBox.Controls.Add(this.Beditar);
		this.GroupBox.Controls.Add(this.BperfilNuevo);
		this.GroupBox.Controls.Add(this.TxtDescripcionPerfil);
		this.GroupBox.Controls.Add(this.TxtNombre);
		this.GroupBox.Location = new System.Drawing.Point(14, 82);
		this.GroupBox.Name = "GroupBox";
		this.GroupBox.Size = new System.Drawing.Size(436, 169);
		this.GroupBox.TabIndex = 1;
		this.GroupBox.TabStop = false;
		this.GroupBox.Text = "Nuevo/ Edicion Rol usuarios";
		//
		//CboPrioridad
		//
		this.CboPrioridad.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.CboPrioridad.Location = new System.Drawing.Point(22, 83);
		this.CboPrioridad.Name = "CboPrioridad";
		this.CboPrioridad.Nombre = "ZctSOTLabel1";
		this.CboPrioridad.Size = new System.Drawing.Size(400, 28);
		this.CboPrioridad.TabIndex = 6;
		this.CboPrioridad.value = null;
		this.CboPrioridad.ValueItem = 0;
		this.CboPrioridad.ValueItemStr = null;
		this.CboPrioridad.Visible = false;
		//
		//BguardarPerfil
		//
		this.BguardarPerfil.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.BguardarPerfil.Enabled = false;
		this.BguardarPerfil.Location = new System.Drawing.Point(266, 117);
		this.BguardarPerfil.Name = "BguardarPerfil";
		this.BguardarPerfil.Size = new System.Drawing.Size(75, 23);
		this.BguardarPerfil.TabIndex = 6;
		this.BguardarPerfil.Text = "Guardar";
		this.BguardarPerfil.UseVisualStyleBackColor = true;
		//
		//BcancelaE
		//
		this.BcancelaE.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.BcancelaE.Enabled = false;
		this.BcancelaE.Location = new System.Drawing.Point(347, 118);
		this.BcancelaE.Name = "BcancelaE";
		this.BcancelaE.Size = new System.Drawing.Size(75, 23);
		this.BcancelaE.TabIndex = 5;
		this.BcancelaE.Text = "Cancelar";
		this.BcancelaE.UseVisualStyleBackColor = true;
		//
		//BeliminaPerfil
		//
		this.BeliminaPerfil.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.BeliminaPerfil.Location = new System.Drawing.Point(184, 117);
		this.BeliminaPerfil.Name = "BeliminaPerfil";
		this.BeliminaPerfil.Size = new System.Drawing.Size(75, 23);
		this.BeliminaPerfil.TabIndex = 4;
		this.BeliminaPerfil.Text = "Eliminar";
		this.BeliminaPerfil.UseVisualStyleBackColor = true;
		//
		//Beditar
		//
		this.Beditar.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.Beditar.Location = new System.Drawing.Point(103, 117);
		this.Beditar.Name = "Beditar";
		this.Beditar.Size = new System.Drawing.Size(75, 23);
		this.Beditar.TabIndex = 3;
		this.Beditar.Text = "Editar";
		this.Beditar.UseVisualStyleBackColor = true;
		//
		//BperfilNuevo
		//
		this.BperfilNuevo.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.BperfilNuevo.Location = new System.Drawing.Point(22, 117);
		this.BperfilNuevo.Name = "BperfilNuevo";
		this.BperfilNuevo.Size = new System.Drawing.Size(75, 23);
		this.BperfilNuevo.TabIndex = 2;
		this.BperfilNuevo.Text = "Nuevo";
		this.BperfilNuevo.UseVisualStyleBackColor = true;
		//
		//TxtDescripcionPerfil
		//
		this.TxtDescripcionPerfil.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.TxtDescripcionPerfil.DataBindings.Add(new System.Windows.Forms.Binding("Tag", this.ZctRolesBindingSource, "descripcion", true));
		this.TxtDescripcionPerfil.Enabled = false;
		this.TxtDescripcionPerfil.Location = new System.Drawing.Point(17, 63);
		this.TxtDescripcionPerfil.Multiline = false;
		this.TxtDescripcionPerfil.Name = "TxtDescripcionPerfil";
		this.TxtDescripcionPerfil.Nombre = "Descripcion";
		this.TxtDescripcionPerfil.Size = new System.Drawing.Size(405, 28);
		this.TxtDescripcionPerfil.TabIndex = 1;
		this.TxtDescripcionPerfil.TipoDato = ZctSOT.ClassGen.Tipo_Dato.ALFANUMERICO;
		this.TxtDescripcionPerfil.ToolTip = "";
		//
		//ZctRolesBindingSource
		//
		this.ZctRolesBindingSource.DataSource = typeof(Permisos.Modelo.ZctRoles);
		//
		//TxtNombre
		//
		this.TxtNombre.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.TxtNombre.DataBindings.Add(new System.Windows.Forms.Binding("Tag", this.ZctRolesBindingSource, "nombre", true));
		this.TxtNombre.Enabled = false;
		this.TxtNombre.Location = new System.Drawing.Point(17, 29);
		this.TxtNombre.Multiline = false;
		this.TxtNombre.Name = "TxtNombre";
		this.TxtNombre.Nombre = "Nombre";
		this.TxtNombre.Size = new System.Drawing.Size(405, 28);
		this.TxtNombre.TabIndex = 0;
		this.TxtNombre.TipoDato = ZctSOT.ClassGen.Tipo_Dato.ALFANUMERICO;
		this.TxtNombre.ToolTip = "";
		//
		//Gpermisos
		//
		this.Gpermisos.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.Gpermisos.Controls.Add(this.CheckBox3);
		this.Gpermisos.Controls.Add(this.CheckBox2);
		this.Gpermisos.Controls.Add(this.chAgregas);
		this.Gpermisos.Controls.Add(this.ChEliminar);
		this.Gpermisos.Controls.Add(this.ChEditar);
		this.Gpermisos.Controls.Add(this.chAgregar);
		this.Gpermisos.Controls.Add(this.chAcceso);
		this.Gpermisos.Enabled = false;
		this.Gpermisos.Location = new System.Drawing.Point(59, 262);
		this.Gpermisos.Name = "Gpermisos";
		this.Gpermisos.Size = new System.Drawing.Size(347, 178);
		this.Gpermisos.TabIndex = 2;
		this.Gpermisos.TabStop = false;
		this.Gpermisos.Text = "Permisos ventanas";
		//
		//CheckBox3
		//
		this.CheckBox3.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
		this.CheckBox3.AutoSize = true;
		this.CheckBox3.Location = new System.Drawing.Point(108, 129);
		this.CheckBox3.Name = "CheckBox3";
		this.CheckBox3.Size = new System.Drawing.Size(139, 43);
		this.CheckBox3.TabIndex = 10;
		this.CheckBox3.Text = "Permitir/negar eliminar a" + "\r" + "\n" + "todos los submenus " + "\r" + "\n" + "del menu seleccionado";
		this.CheckBox3.UseVisualStyleBackColor = true;
		//
		//CheckBox2
		//
		this.CheckBox2.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
		this.CheckBox2.AutoSize = true;
		this.CheckBox2.Location = new System.Drawing.Point(175, 72);
		this.CheckBox2.Name = "CheckBox2";
		this.CheckBox2.Size = new System.Drawing.Size(135, 43);
		this.CheckBox2.TabIndex = 9;
		this.CheckBox2.Text = "Permitir/negar editar a" + "\r" + "\n" + "todos los submenus " + "\r" + "\n" + "del menu seleccionado";
		this.CheckBox2.UseVisualStyleBackColor = true;
		//
		//chAgregas
		//
		this.chAgregas.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
		this.chAgregas.AutoSize = true;
		this.chAgregas.Location = new System.Drawing.Point(6, 69);
		this.chAgregas.Name = "chAgregas";
		this.chAgregas.Size = new System.Drawing.Size(143, 43);
		this.chAgregas.TabIndex = 8;
		this.chAgregas.Text = "Permitir/negar  agregar a" + "\r" + "\n" + "todos los submenus " + "\r" + "\n" + "del menu seleccionado";
		this.chAgregas.UseVisualStyleBackColor = true;
		//
		//ChEliminar
		//
		this.ChEliminar.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.ChEliminar.AutoSize = true;
		this.ChEliminar.Location = new System.Drawing.Point(238, 30);
		this.ChEliminar.Name = "ChEliminar";
		this.ChEliminar.Size = new System.Drawing.Size(62, 17);
		this.ChEliminar.TabIndex = 3;
		this.ChEliminar.Text = "Eliminar";
		this.ChEliminar.UseVisualStyleBackColor = true;
		//
		//ChEditar
		//
		this.ChEditar.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.ChEditar.AutoSize = true;
		this.ChEditar.Location = new System.Drawing.Point(175, 30);
		this.ChEditar.Name = "ChEditar";
		this.ChEditar.Size = new System.Drawing.Size(53, 17);
		this.ChEditar.TabIndex = 2;
		this.ChEditar.Text = "Editar";
		this.ChEditar.UseVisualStyleBackColor = true;
		//
		//chAgregar
		//
		this.chAgregar.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.chAgregar.AutoSize = true;
		this.chAgregar.Location = new System.Drawing.Point(108, 30);
		this.chAgregar.Name = "chAgregar";
		this.chAgregar.Size = new System.Drawing.Size(63, 17);
		this.chAgregar.TabIndex = 1;
		this.chAgregar.Text = "Agregar";
		this.chAgregar.UseVisualStyleBackColor = true;
		//
		//chAcceso
		//
		this.chAcceso.Anchor = System.Windows.Forms.AnchorStyles.Top;
		this.chAcceso.AutoSize = true;
		this.chAcceso.Location = new System.Drawing.Point(46, 30);
		this.chAcceso.Name = "chAcceso";
		this.chAcceso.Size = new System.Drawing.Size(62, 17);
		this.chAcceso.TabIndex = 0;
		this.chAcceso.Text = "Acceso";
		this.chAcceso.UseVisualStyleBackColor = true;
		//
		//Chmenus
		//
		this.Chmenus.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
		this.Chmenus.AutoSize = true;
		this.Chmenus.Location = new System.Drawing.Point(6, 427);
		this.Chmenus.Name = "Chmenus";
		this.Chmenus.Size = new System.Drawing.Size(115, 43);
		this.Chmenus.TabIndex = 6;
		this.Chmenus.Text = "Permitir/negar " + "\r" + "\n" + "acceso a todos los" + "\r" + "\n" + "menus principales";
		this.Chmenus.UseVisualStyleBackColor = true;
		//
		//Chsm
		//
		this.Chsm.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
		this.Chsm.AutoSize = true;
		this.Chsm.Location = new System.Drawing.Point(121, 427);
		this.Chsm.Name = "Chsm";
		this.Chsm.Size = new System.Drawing.Size(142, 43);
		this.Chsm.TabIndex = 7;
		this.Chsm.Text = "Permitir/negar  acceso a" + "\r" + "\n" + "todos los submenus " + "\r" + "\n" + "del menu seleccionado";
		this.Chsm.UseVisualStyleBackColor = true;
		//
		//GroupBox1
		//
		this.GroupBox1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.GroupBox1.Controls.Add(this.ZctSOTButton5);
		this.GroupBox1.Controls.Add(this.BguardarP);
		this.GroupBox1.Controls.Add(this.CboPerfiles);
		this.GroupBox1.Controls.Add(this.Gpermisos);
		this.GroupBox1.Controls.Add(this.GroupBox);
		this.GroupBox1.Location = new System.Drawing.Point(272, 0);
		this.GroupBox1.Name = "GroupBox1";
		this.GroupBox1.Size = new System.Drawing.Size(465, 475);
		this.GroupBox1.TabIndex = 8;
		this.GroupBox1.TabStop = false;
		//
		//ZctSOTButton5
		//
		this.ZctSOTButton5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		this.ZctSOTButton5.Location = new System.Drawing.Point(237, 446);
		this.ZctSOTButton5.Name = "ZctSOTButton5";
		this.ZctSOTButton5.Size = new System.Drawing.Size(75, 23);
		this.ZctSOTButton5.TabIndex = 5;
		this.ZctSOTButton5.Text = "Cerrar";
		this.ZctSOTButton5.UseVisualStyleBackColor = true;
		//
		//BguardarP
		//
		this.BguardarP.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
		this.BguardarP.Enabled = false;
		this.BguardarP.Location = new System.Drawing.Point(156, 446);
		this.BguardarP.Name = "BguardarP";
		this.BguardarP.Size = new System.Drawing.Size(75, 23);
		this.BguardarP.TabIndex = 4;
		this.BguardarP.Text = "Guardar";
		this.BguardarP.UseVisualStyleBackColor = true;
		//
		//CboPerfiles
		//
		this.CboPerfiles.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
		this.CboPerfiles.Location = new System.Drawing.Point(23, 38);
		this.CboPerfiles.Name = "CboPerfiles";
		this.CboPerfiles.Nombre = "Perfiles";
		this.CboPerfiles.Size = new System.Drawing.Size(416, 28);
		this.CboPerfiles.TabIndex = 0;
		this.CboPerfiles.value = null;
		this.CboPerfiles.ValueItem = 0;
		this.CboPerfiles.ValueItemStr = null;
		//
		//TxtDescripcionPerfil1
		//
		this.TxtDescripcionPerfil1.Location = new System.Drawing.Point(17, 63);
		this.TxtDescripcionPerfil1.Multiline = false;
		this.TxtDescripcionPerfil1.Name = "TxtDescripcionPerfil1";
		this.TxtDescripcionPerfil1.Nombre = "ZctSOTLabel1";
		this.TxtDescripcionPerfil1.Size = new System.Drawing.Size(327, 28);
		this.TxtDescripcionPerfil1.TabIndex = 1;
		this.TxtDescripcionPerfil1.TipoDato = ZctSOT.ClassGen.Tipo_Dato.ALFANUMERICO;
		this.TxtDescripcionPerfil1.ToolTip = "";
		//
		//PermisosUsuarios
		//
		this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
		this.ClientSize = new System.Drawing.Size(743, 476);
		this.Controls.Add(this.GroupBox1);
		this.Controls.Add(this.Chsm);
		this.Controls.Add(this.Chmenus);
		this.Controls.Add(this.ArbolMenu);
		this.Name = "PermisosUsuarios";
		this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
		this.Text = "Permisos";
		this.GroupBox.ResumeLayout(false);
		((System.ComponentModel.ISupportInitialize)this.ZctRolesBindingSource).EndInit();
		this.Gpermisos.ResumeLayout(false);
		this.Gpermisos.PerformLayout();
		((System.ComponentModel.ISupportInitialize)this.BSroles).EndInit();
		this.GroupBox1.ResumeLayout(false);
		this.ResumeLayout(false);
		this.PerformLayout();

//INSTANT C# NOTE: Converted event handler wireups:
		this.Load += new System.EventHandler(AltaPerfil_Load);
		CboPerfiles.SelectedValueChanged += new System.EventHandler(CboPerfiles_SelectedValueChanged);
		BperfilNuevo.Click += new System.EventHandler(BperfilNuevo_Click);
		Beditar.Click += new System.EventHandler(Beditar_Click);
		BeliminaPerfil.Click += new System.EventHandler(BeliminaPerfil_Click);
		BguardarPerfil.Click += new System.EventHandler(BguardarPerfil_Click);
		BcancelaE.Click += new System.EventHandler(BcancelaE_Click);
		BguardarP.Click += new System.EventHandler(ZctSOTButton6_Click);
		ZctSOTButton5.Click += new System.EventHandler(ZctSOTButton5_Click);
		ArbolMenu.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(ArbolMenu_AfterSelect);
		ArbolMenu.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(ArbolMenu_NodeMouseClick);
		chAcceso.Click += new System.EventHandler(chAcceso_Click);
		chAgregar.Click += new System.EventHandler(chAcceso_Click);
		ChEditar.Click += new System.EventHandler(chAcceso_Click);
		ChEliminar.Click += new System.EventHandler(chAcceso_Click);
		chAgregas.CheckedChanged += new System.EventHandler(CheckBox1_CheckedChanged);
		CheckBox2.CheckedChanged += new System.EventHandler(CheckBox2_CheckedChanged);
		CheckBox3.CheckedChanged += new System.EventHandler(CheckBox3_CheckedChanged);
		Chmenus.Click += new System.EventHandler(Chmenus_Click);
		Chsm.Click += new System.EventHandler(Chsm_Click);
		Chsm.CheckedChanged += new System.EventHandler(Chsm_CheckedChanged);
		chAcceso.CheckedChanged += new System.EventHandler(chAcceso_CheckedChanged);
		Chmenus.CheckedChanged += new System.EventHandler(Chmenus_CheckedChanged);
		CboPerfiles.Load += new System.EventHandler(CboPerfiles_Load);
	}
	internal System.Windows.Forms.TreeView ArbolMenu;
	internal System.Windows.Forms.GroupBox GroupBox;
	internal ZctSOT.ZctSOTButton BcancelaE;
	internal ZctSOT.ZctSOTButton BeliminaPerfil;
	internal ZctSOT.ZctSOTButton Beditar;
	internal ZctSOT.ZctSOTButton BperfilNuevo;
	internal ZctSOT.ZctControlTexto TxtDescripcionPerfil;
	internal ZctSOT.ZctControlTexto TxtNombre;
	internal System.Windows.Forms.GroupBox Gpermisos;
	internal System.Windows.Forms.CheckBox ChEliminar;
	internal System.Windows.Forms.CheckBox ChEditar;
	internal System.Windows.Forms.CheckBox chAgregar;
	internal System.Windows.Forms.CheckBox chAcceso;
	internal ZctSOT.ZctControlCombo CboPerfiles;
	internal ZctSOT.ZctSOTButton ZctSOTButton5;
	internal ZctSOT.ZctSOTButton BguardarP;
	internal ZctSOT.ZctControlTexto TxtDescripcionPerfil1;
	internal ZctSOT.ZctSOTButton BguardarPerfil;
	internal ZctSOT.ZctControlCombo CboPrioridad;
	internal System.Windows.Forms.BindingSource BSroles;
	internal System.Windows.Forms.BindingSource ZctRolesBindingSource;
	internal System.Windows.Forms.CheckBox Chmenus;
	internal System.Windows.Forms.CheckBox Chsm;
	internal System.Windows.Forms.GroupBox GroupBox1;
	internal System.Windows.Forms.CheckBox CheckBox3;
	internal System.Windows.Forms.CheckBox CheckBox2;
	internal System.Windows.Forms.CheckBox chAgregas;
}
