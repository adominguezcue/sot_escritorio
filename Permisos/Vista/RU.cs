﻿using System;

public partial class RU
{
	internal RU()
	{
		InitializeComponent();
	}

private Permisos.Controlador.Controlador _Controlador = new Permisos.Controlador.Controlador(DAO.Coneccion.CadenaConexion);
	private List<Permisos.Controlador.Controlador.vistaRoles> RolesUsuario = new List<Permisos.Controlador.Controlador.vistaRoles>();
	private void RolesUsuarios_Load(object sender, System.EventArgs e)
	{
		cboUsuario.GetData("ZctSegUsu");
		CboRoles.GetData("Zctroles");
		//SetDGV()
	}
	private void SetDGV()
	{
		grdDatos.DataSource = null;
		grdDatos.Columns.Clear();
		grdDatos.Rows.Clear();
		grdDatos.AutoGenerateColumns = false;
		grdDatos.AllowUserToAddRows = true;
		grdDatos.AllowUserToDeleteRows = true;
		grdDatos.Columns.Add(zctColumnComun.GetColumn("rol", "Rol", "nombre", true,,, true));
		grdDatos.Columns.Add(zctColumnComun.GetColumn("Cod_Rol", "rol", "cod_rol", false,,, true));
		grdDatos.Columns.Add(zctColumnComun.GetColumn("Cod_Usu", "cod_user", "cod_user", false,,, true));
		RolesUsuario = _Controlador.get_rolesusuario(cboUsuario.value);
		this.grdDatos.DataSource = RolesUsuario;
	}
	private void cboUsuario_SelectedValueChanged(object sender, System.EventArgs e)
	{
		SetDGV();
	}

	private void Bmod_Click(object sender, System.EventArgs e)
	{
		Belimina.Enabled = true;
		CboRoles.Enabled = true;
		Basigna.Enabled = true;
		Bmod.Enabled = false;
		Bcancelar.Enabled = true;
	}

	private void Basigna_Click(object sender, System.EventArgs e)
	{
		try
		{
			Basigna.Enabled = false;
			Bguardar.Enabled = true;
			Bcancelar.Enabled = true;
			_Controlador.AgregaRolaUsuario(cboUsuario.value, CboRoles.value);
			//SetDGV()
		}
		catch (Exception ex)
		{
			MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}

	private void grdDatos_RowEnter(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
	{
		CboRoles.ZctSOTCombo1.SelectedItem = e.RowIndex;
	}

	private void Buardar_Click(object sender, System.EventArgs e)
	{
		Belimina.Enabled = false;
		Bguardar.Enabled = false;
		CboRoles.Enabled = false;
		Bcancelar.Enabled = false;
		_Controlador.GuardarCambios();
		_Controlador.Recorrermenu(ZctSOT.ZctSOTMain._UsuAct, ZctSOT.ZctSOTMain.MenuStrip);
		Basigna.Enabled = false;
		Bmod.Enabled = true;
		SetDGV();
	}

	private void Bcancelar_Click(object sender, System.EventArgs e)
	{
		Bguardar.Enabled = false;
		Bcancelar.Enabled = false;
		Basigna.Enabled = false;
		Belimina.Enabled = false;
		Bmod.Enabled = true;
		_Controlador.CancelarCambios();
		MessageBox.Show("Cambios cancelados", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		SetDGV();
	}

	private void Belimina_Click(object sender, System.EventArgs e)
	{
		try
		{
			Bguardar.Enabled = true;
			Belimina.Enabled = false;
			foreach (DataGridViewRow R in grdDatos.SelectedRows)
			{
				_Controlador.EliminaRol(R.Cells["Cod_Usu"].Value, R.Cells["Cod_rol"].Value);
				Application.DoEvents();
			}
			SetDGV();
		}
		catch (Exception ex)
		{
			MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
	}
}