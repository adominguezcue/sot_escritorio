﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Compartido.Sincronizacion
{
    public interface IServicioSincronizacionCompartido
    {
        bool VerificarExisteSucursal(string identificadorSucursal);
    }
}
