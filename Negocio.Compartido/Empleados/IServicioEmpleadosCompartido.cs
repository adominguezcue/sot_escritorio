﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Compartido.Empleados
{
    public interface IServicioEmpleadosCompartido
    {
        string ObtenerNombreCompletoEmpleado(int idEmpleado);
        int ObtenerIdRol(int idEmpleado);

        bool VerificarRolEnUso(int idRol);
    }
}
