﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.Compartido.CentrosCostos
{
    public interface IServicioCentrosCostosCompartido
    {
        void ValidarExistencia(int idCentroCostos);

        void RegistrarGasto(int idCentroCostos, decimal valor, int idUsuario);
    }
}
