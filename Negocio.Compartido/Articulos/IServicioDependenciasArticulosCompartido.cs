﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Compartido.Articulos
{
    public interface IServicioDependenciasArticulosCompartido
    {
        /// <summary>
        /// Verifica que no existan referencias al código de artículo proporcionado
        /// </summary>
        /// <param name="folioArticulo"></param>
        /// <returns></returns>
        bool VerificarNoExistenDependenciasArticulo(string codigoArticulo);
    }
}
