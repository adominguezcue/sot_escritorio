﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Compartido.ConfiguracionesGlobales
{
    public interface IServicioConfiguracionesGlobalesCompartido
    {
        bool VerificarEsDepartamentoMaestro(int idDepartamento);
    }
}
