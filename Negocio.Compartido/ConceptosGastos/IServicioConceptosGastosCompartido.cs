﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Compartido.ConceptosGastos
{
    public interface IServicioConceptosGastosCompartido
    {
        void RegistrarGasto(int idCuentaPago, string codigoArticulo, decimal valor, int idUsuario, ClasificacionesGastos? clasificacion, DateTime fechaFiltroTurno/*int? idTurno*/);
        void RegistrarGasto(int idCuentaPago, int idConceptoGasto, decimal valor, int idUsuario, ClasificacionesGastos? clasificacion, DateTime fechaFiltroTurno/*int? idTurno*/);
        /// <summary>
        /// Elimina los gastos vinculados a la cuenta de pago proporcionada..
        /// </summary>
        /// <param name="idCuentaPago"></param>
        /// <param name="idUsuario"></param>
        void EliminarGastos(int idCuentaPago, int idUsuario);
        void ValidarConceptoGasto(int idGasto, bool soloActivos);
    }
}
