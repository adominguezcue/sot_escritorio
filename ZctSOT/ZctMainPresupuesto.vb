﻿Imports ZctSOT.Datos
Imports SOTControladores.Controladores

Public Class ZctMainPresupuesto
    Private _rdn As New RDN.ZctRdnPresupuestos
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    'Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos 'Permisos.Controlador.Controlador.vistapermisos
    Dim _Guardar As Boolean
#Region "Metodos"
    Private Sub cmdBuscaArchivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaArchivo.Click
        'Try
        If MsgBox("Los presupuestos serán modificados cuando usted grabe por los existentes en el archivo de excel, ¿Desea continuar?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

        OpenFileDialog1.ShowDialog()
        If OpenFileDialog1.FileName <> "" Then
            _rdn.Path_Exl = OpenFileDialog1.FileName
            lblArchivo.Text = OpenFileDialog1.FileName
            CargarExcel()

        End If
        'Catch ex As Exception
        '    MsgBox("Ha ocurrido un error.")
        'End Try
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try

            Cursor = Cursors.WaitCursor
            _rdn.Grabar()
            MsgBox("Se ha grabado correctamente.")
            pHabilita(True)
            pLimpia()
            'Catch ex As ZctReglaNegocioEx
            '    MsgBox(ex.Message)

            'Catch ex As Exception
            '    MsgBox("Ha ocurrido un error, comuniquese con sistemas.")
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub txtAnno_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnno.lostfocus

        Try
            _rdn.CodAlmacen = almacen.value
            If txtAnno.Text.Trim = String.Empty Then Exit Sub
            If Not IsNumeric(txtAnno.Text) Then
                MsgBox("Debe de proporcionar el año en un valor numérico")
                Exit Sub
            End If


            'Catch ex As ZctReglaNegocioEx
            '    MsgBox(ex.Message)
            'Catch ex As Exception
            '    MsgBox("Ha ocurrido un error, comuniquese con sistemas.")
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub


    Private Sub grdDatos_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles grdDatos.CellBeginEdit
        'Try
        If e.ColumnIndex = grdDatos.Columns("Cod_Cte").Index And Not CBool(grdDatos.Rows(grdDatos.CurrentCell.RowIndex).Cells("Modificado").Value) Then
            e.Cancel = True
        End If
        'Catch ex As Exception
        '    MsgBox("Ha ocurrido un error.")
        'End Try
    End Sub

    Private Sub grdDatos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDatos.CellEndEdit
        'Try
        _rdn.CodAlmacen = almacen.value
        If e.ColumnIndex = grdDatos.Columns("Cod_Cte").Index Then
            If grdDatos.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString <> "" And IsNumeric(grdDatos.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                Dim cte As Clases.Catalogos.ZctCatCliente = _rdn.GetCliente(grdDatos.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                If cte Is Nothing OrElse cte.Nom_Cte Is Nothing Then
                    MsgBox("El cliente proporcionado no exite.")
                    grdDatos.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 0
                    Exit Sub
                Else
                    grdDatos.Rows(e.RowIndex).Cells("NombreCte").Value = cte.Nombre
                    grdDatos.Rows(e.RowIndex).Cells("Anno_Pres").Value = CInt(txtAnno.Text)
                    grdDatos.Rows(e.RowIndex).Cells("codalmacen").Value = CInt(almacen.value)
                    grdDatos.Rows(e.RowIndex).Cells("Enero").Value = 0
                    'If e.RowIndex = grdDatos.NewRowIndex Then
                    '    ZctPresupuesto_ElementoBindingSource.AllowNew = True
                    'End If
                    grdDatos.UpdateCellValue(grdDatos.Columns("Enero").Index, grdDatos.CurrentCell.RowIndex)
                    'ZctPresupuesto_ElementoBindingSource.EndEdit()

                End If
            Else
                'grdDatos.Rows.Remove(grdDatos.Rows(e.RowIndex))
            End If

        End If
        'Catch ex As ZctReglaNegocioEx
        '    MsgBox(ex.Message)
        'Catch ex As Exception
        '    MsgBox("Ha ocurrido un error.")
        'End Try

    End Sub

    Private Sub grdDatos_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grdDatos.DataError
        Try
            MsgBox(e.Exception.Message)
            e.Cancel = True
        Catch ex As Exception

        End Try

    End Sub

    Private Sub grdDatos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdDatos.KeyDown

        'Try
        If e.KeyCode = Keys.F3 And CBool(grdDatos.Rows(grdDatos.CurrentCell.RowIndex).Cells("Modificado").Value) And grdDatos.CurrentCell.ColumnIndex = grdDatos.Columns("Cod_Cte").Index Then

            Dim fBusqueda As New ZctBusqueda
            fBusqueda.sSPName = "SP_ZctCatCte"
            fBusqueda.sSpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
            fBusqueda.ShowDialog(Me)
            grdDatos.BeginEdit(True)
            grdDatos.CurrentCell.Value = fBusqueda.iValor
            grdDatos.EndEdit(True)
            grdDatos.UpdateCellValue(grdDatos.Columns("Cod_Cte").Index, grdDatos.CurrentCell.RowIndex)
            grdDatos.Rows(grdDatos.CurrentCell.RowIndex).Cells("Enero").Selected = True
        ElseIf e.KeyCode = Keys.Delete AndAlso grdDatos.Rows(grdDatos.CurrentCell.RowIndex).Cells("Cod_Cte").Value IsNot Nothing AndAlso grdDatos.Rows(grdDatos.CurrentCell.RowIndex).Cells("Cod_Cte").Value.ToString.Trim <> "" Then
            'Try
            If MsgBox("¿Esta seguro que desea eliminar el presupuesto?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
            Dim elemento As ZctPresupuesto_Elemento = _rdn.GetElemento(CInt(grdDatos.Rows(grdDatos.CurrentCell.RowIndex).Cells("Cod_Cte").Value))
            _rdn.DelData(elemento)
            grdDatos.Rows.Remove(grdDatos.Rows(grdDatos.CurrentRow.Index))

            MsgBox("El registro se ha eliminado correctamente.")
            'Catch ex As ZctReglaNegocioEx
            '    MsgBox(ex.Message)
            'Catch ex As Exception
            '    MsgBox("Ha ocurrido un error.")
            'End Try
        End If
        'Catch ex As Exception
        '    MsgBox("Ha ocurrido un error, comuniquese con sistemas.")
        'End Try
    End Sub

    'Private Sub grdDatos_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles grdDatos.CellValidating
    '    Try

    '        If e.ColumnIndex = grdDatos.Columns("Cod_Cte").Index Then
    '            If grdDatos.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString <> "" And IsNumeric(grdDatos.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
    '                Dim cte As Clases.Catalogos.ZctCatCliente = _rdn.GetCliente(grdDatos.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
    '                If cte Is Nothing OrElse cte.Nom_Cte Is Nothing Then
    '                    MsgBox("El cliente proporcionado no exite.")
    '                    'grdDatos.Rows.Remove(grdDatos.Rows(e.RowIndex)) 
    '                    e.Cancel = True

    '                Else
    '                    grdDatos.Rows(e.RowIndex).Cells("NombreCte").Value = cte.Nombre
    '                    grdDatos.Rows(e.RowIndex).Cells("Anno_Pres").Value = CInt(txtAnno.Text)
    '                End If
    '            Else
    '                e.Cancel = True
    '                ' grdDatos.Rows.Remove(grdDatos.Rows(e.RowIndex))
    '            End If
    '        Else
    '            'If grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value.ToString = "" OrElse Not IsNumeric(grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value) OrElse grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value = 0 Then
    '            '    MsgBox("Seleccione primero el cliente.")
    '            '    e.Cancel = True
    '            'End If
    '        End If
    '    Catch ex As ZctReglaNegocioEx
    '        MsgBox(ex.Message)
    '    Catch ex As Exception
    '        MsgBox("Ha ocurrido un error.")
    '    End Try
    'End Sub
    Private Sub BindingSource1_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.BindingManagerDataErrorEventArgs)
        Try
            MsgBox(e.Exception.Message)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub ZctMainPresupuesto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '_Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        
        pHabilita(True)
        almacen.GetData("ZctCatAlm")
    End Sub

    Private Sub cmdConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConsultar.Click
        Try
            If txtAnno.Text.Trim = String.Empty OrElse Not IsNumeric(txtAnno.Text) Then
                MsgBox("Debe de proporcionar el año en un valor numérico")
                Exit Sub
            End If
            Cursor = Cursors.WaitCursor
            _rdn.Anno = txtAnno.Text
            _rdn.CodAlmacen = almacen.value
            _rdn.GetData()
            Setgrid()
            ZctPresupuesto_ElementoBindingSource.DataSource = _rdn.Lista
            grdDatos.DataSource = ZctPresupuesto_ElementoBindingSource
            pHabilita(False)
            'Catch ex As ZctReglaNegocioEx
            '    MsgBox(ex.Message)
            'Catch ex As Exception
            '    MsgBox("Ha ocurrido un error, comuniquese con sistemas.")
        Finally
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If txtAnno.Enabled Then Me.Close() : Exit Sub
        pLimpia()
        pHabilita(True)
    End Sub

#End Region

    Private Sub CargarExcel()
        'Carga aqui el archivo de excel.
        Try
            Cursor = Cursors.WaitCursor
            If txtAnno.Text = "" Or Not IsNumeric(txtAnno.Text) Then
                MsgBox("Proporcione el año al que pertenece el presupuesto.")
                txtAnno.Text = ""
                Exit Sub

            End If
            If _rdn.Path_Exl <> "" Then
                'Carga los datos de los presupuestos
                _rdn.Anno = txtAnno.Text
                _rdn.CodAlmacen = almacen.value
                _rdn.GetExcel()
                Setgrid()
                grdDatos.DataSource = _rdn.Lista
                MsgBox("El archivo ha sido importado correctamente.")
            End If
            'Catch ex As ZctReglaNegocioEx
            '    MsgBox(ex.Message)

            'Catch ex As Exception
            '    MsgBox("Ha ocurrido un error, comuniquese con sistemas.")
        Finally
            Cursor = Cursors.Default

        End Try

    End Sub


    Public Sub Setgrid()
        ZctPresupuesto_ElementoBindingSource.DataSource = Nothing
        grdDatos.DataSource = Nothing
        grdDatos.Columns.Clear()
        grdDatos.Rows.Clear()
        grdDatos.AutoGenerateColumns = False
        grdDatos.AllowUserToAddRows = True
        grdDatos.AllowUserToDeleteRows = True

        grdDatos.Columns.Add(zctColumnComun.GetColumn("Cod_Cte", "Código", "Cod_Cte", True, , , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Anno_Pres", "Anno_Pres", "Anno_Pres", False, , , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("NombreCte", "Cliente", "NombreCte", True, , , True))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Enero", "Enero", "Enero", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Febrero", "Febrero", "Febrero", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Marzo", "Marzo", "Marzo", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Abril", "Abril", "Abril", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Mayo", "Mayo", "Mayo", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Junio", "Junio", "Junio", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Julio", "Julio", "Julio", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Agosto", "Agosto", "Agosto", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Septiembre", "Septiembre", "Septiembre", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Octubre", "Octubre", "Octubre", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Noviembre", "Noviembre", "Noviembre", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Diciembre", "Diciembre", "Diciembre", True, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Modificado", "Modificado", "Modificado", False, "$##,###.00", , False))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Codalmacen", "cod_alm", "CodAlmacen", False, , , False))

    End Sub


    Public Sub pHabilita(ByVal habilita As Boolean)

        Dim _Permisos = New ControladorPermisos().ObtenerPermisosActuales()
        cmdAceptar.Enabled = Not habilita AndAlso IIf(_Permisos.ModificarPresupuestos = False And _Permisos.CrearPresupuestos = False And _Permisos.EliminarPresupuestos = False, False, True)


        txtAnno.Enabled = habilita
        cmdConsultar.Enabled = habilita
        almacen.Enabled = habilita
        cmdBuscaArchivo.Enabled = Not habilita
        grdDatos.Enabled = Not habilita
        'cmdAceptar.Enabled = Not habilita

    End Sub


    Public Sub pLimpia()
        txtAnno.Text = ""
        grdDatos.DataSource = Nothing
        grdDatos.Rows.Clear()
        lblArchivo.Text = ""
        _rdn = New RDN.ZctRdnPresupuestos
        almacen.ZctSOTCombo1.SelectedIndex = 0
    End Sub

    'Private Sub grdDatos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDatos.CellContentClick

    'End Sub
    'Private Sub grdDatos_NewRowNeeded(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs)

    'End Sub
    'Private Sub grdDatos_RowsAdded(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs)

    'End Sub

    'Private Sub grdDatos_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDatos.CellLeave
    '    'MsgBox(grdDatos.NewRowIndex)
    '    If e.RowIndex = grdDatos.NewRowIndex Then
    '        If grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value IsNot Nothing AndAlso grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value <> 0 Then
    '            'grdDatos.Rows.Remove(grdDatos.Rows(e.RowIndex))
    '            'grdDatos.InvalidateRow(e.RowIndex)
    '            'IsCurrentCellDirty()
    '            'MsgBox(grdDatos.Rows(e.RowIndex))
    '            'grdDatos.Rows(e.RowIndex) = grdDatos.Rows.

    '            'ZctPresupuesto_ElementoBindingSource.CancelEdit()
    '            'MsgBox("limpio")
    '        End If
    '        'MsgBox(grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value)
    '    End If
    'End Sub

    'Private Sub grdDatos_RowDirtyStateNeeded(ByVal sender As Object, ByVal e As System.Windows.Forms.QuestionEventArgs) Handles grdDatos.RowDirtyStateNeeded
    '    MsgBox("sucio")
    'End Sub

    'Private Sub grdDatos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDatos.CellEnter

    'End Sub

    Private Sub grdDatos_RowLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDatos.RowLeave
        If e.RowIndex = grdDatos.NewRowIndex Then
            If grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value IsNot Nothing AndAlso grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value <> 0 Then
                'grdDatos.Rows.Remove(grdDatos.Rows(e.RowIndex))
                'grdDatos.InvalidateRow(e.RowIndex)
                'IsCurrentCellDirty()
                'MsgBox(grdDatos.Rows(e.RowIndex))
                'grdDatos.Rows(e.RowIndex) = grdDatos.Rows.

                ZctPresupuesto_ElementoBindingSource.CancelEdit()
                'MsgBox("limpio")
            End If
            'MsgBox(grdDatos.Rows(e.RowIndex).Cells("Cod_Cte").Value)
        End If
    End Sub
End Class