<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctfrmComunGen
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdAceptar = New System.Windows.Forms.Button
        Me.CmdCancelar = New System.Windows.Forms.Button
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox
        Me.DGBusqueda = New System.Windows.Forms.DataGridView
        Me.CmdAyuda = New ZctSOT.ZctSOTButton
        Me.ZctSOTGroupBox1.SuspendLayout()
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(457, 312)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Text = "Aceptar"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'CmdCancelar
        '
        Me.CmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdCancelar.Location = New System.Drawing.Point(538, 312)
        Me.CmdCancelar.Name = "CmdCancelar"
        Me.CmdCancelar.Size = New System.Drawing.Size(75, 23)
        Me.CmdCancelar.TabIndex = 2
        Me.CmdCancelar.Text = "Cancelar"
        Me.CmdCancelar.UseVisualStyleBackColor = True
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctSOTGroupBox1.Controls.Add(Me.DGBusqueda)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(610, 303)
        Me.ZctSOTGroupBox1.TabIndex = 4
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'DGBusqueda
        '
        Me.DGBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGBusqueda.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DGBusqueda.Location = New System.Drawing.Point(3, 16)
        Me.DGBusqueda.Name = "DGBusqueda"
        Me.DGBusqueda.Size = New System.Drawing.Size(604, 284)
        Me.DGBusqueda.TabIndex = 0
        '
        'CmdAyuda
        '
        Me.CmdAyuda.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CmdAyuda.Location = New System.Drawing.Point(12, 312)
        Me.CmdAyuda.Name = "CmdAyuda"
        Me.CmdAyuda.Size = New System.Drawing.Size(75, 23)
        Me.CmdAyuda.TabIndex = 3
        Me.CmdAyuda.Text = "Ayuda"
        Me.CmdAyuda.UseVisualStyleBackColor = True
        '
        'ZctfrmComunGen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(615, 342)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.CmdCancelar)
        Me.Controls.Add(Me.CmdAyuda)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Name = "ZctfrmComunGen"
        Me.Text = "Busqueda"
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents DGBusqueda As System.Windows.Forms.DataGridView
    Friend WithEvents CmdAyuda As ZctSOT.ZctSOTButton
    Friend WithEvents CmdCancelar As System.Windows.Forms.Button
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
End Class
