﻿Imports ZctSOT.Datos

Public Class ZctConfOCS

    Private _rdn As New Datos.RDN.Compras.ZctOCSRDN
    Private Cfg As New Datos.Clases.Compras.ZctConfOCS

    Private _Modular As Boolean = False

    Public Property Modular() As Boolean
        Get
            Return _Modular
        End Get
        Set(ByVal value As Boolean)
            _Modular = value
        End Set
    End Property



    Private Sub ZctConfOCS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtCod_Alm.SqlBusqueda = "SELECT Cod_Alm AS Código, Desc_CatAlm AS Almacén FROM ZctCatAlm "
        txtCod_Alm.SPName = "SP_ZctCatAlm"
        txtCod_Alm.SpVariables = "@Cod_Alm;INT|@Desc_CatAlm;VARCHAR"

        txtCod_Alm.Tabla = "ZctCatAlm"

    End Sub

    Private Sub txtCod_Alm_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCod_Alm.lostfocus
        Try
            If txtCod_Alm.Text.Trim <> "" Then
                Cfg = _rdn.GetConfig(txtCod_Alm.Text)
                ZctConfOCSBindingSource.DataSource = Cfg
                txtCod_Alm.Enabled = False
                cmdAceptar.Enabled = True
            End If
        Catch ex As Datos.ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error.")
        End Try
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try
            If txtCod_Alm.Text.Trim <> "" Then
                _rdn.SetConfig(Cfg)

                If Modular Then Me.Close()
                txtCod_Alm.Text = ""
                txtCod_Alm.Descripcion = ""
                Cfg = New Datos.Clases.Compras.ZctConfOCS
                ZctConfOCSBindingSource.DataSource = Cfg
                txtCod_Alm.Enabled = True
                cmdAceptar.Enabled = False
            End If
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error.")
        End Try

    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        Try
            If txtCod_Alm.Text.Trim <> "" Then
                Cfg = New Clases.Compras.ZctConfOCS
                txtCod_Alm.Text = ""
                txtCod_Alm.Descripcion = ""
                ZctConfOCSBindingSource.DataSource = Cfg
                txtCod_Alm.Enabled = True
                cmdAceptar.Enabled = False
            Else
                Me.Close()
            End If
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error.")
        End Try
    End Sub

    Private Sub MesesTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles MesesTextBox.LostFocus
        ZctConfOCSBindingSource.ResetBindings(False)
    End Sub

    Private Sub MesesTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class