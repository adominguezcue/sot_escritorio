Imports ZctSOT.ZctDataBase

Public Class ZctBusquedaGen
    Public sSQLP As String

    'Parametros para los SP
    Public sSPName As String
    Public sSpVariables As String
    'Parametro de la busqueda 
    Public iColBusq As Integer
    Public sBusq As String
    Public sColNuevo = "ColNuevo"

    Public iValor As String
    Public sServer As String
    Public iRow As Integer
    Public iCol As Integer

    Private Sub PkBusqueda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Posici�n Inicial
        Me.Top = 0
        Me.Left = 0

        Try
            GetData()
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        iValor = DGBusqueda.Item(0, DGBusqueda.CurrentCell.RowIndex).Value
        Me.Dispose()
    End Sub
    

    Private Sub GetData()
        Dim PkBusCon As New Datos.ZctDataBase
        Dim iSp As Integer
        Dim sVector() As String = Split(sSpVariables, "|")
        'PkBusCon.Open()
        PkBusCon.IniciaProcedimiento(sSPName)

        PkBusCon.AddParameterSP("@TpConsulta", 1, SqlDbType.Int)
        Dim iTot As Integer = UBound(sVector)
        For iSp = 0 To iTot
            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                Case "INT"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.Int)

                Case "VARCHAR"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.VarChar)
                Case "SMALLDATETIME"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.SmallDateTime)
                Case "DECIMAL"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.Decimal)
            End Select
        Next
        PkBusCon.InciaDataAdapter()
        DGBusqueda.DataSource = PkBusCon.GetReaderSP.Tables("DATOS")
        If DGBusqueda.Columns.Contains(sColNuevo) = True Then
            DGBusqueda.Columns(sColNuevo).Visible = False
        End If
    End Sub

    Private Sub GetData(ByVal TpConsulta As Integer, ByVal iRow As Integer)
        Dim PkBusCon As New Datos.ZctDataBase
        Dim iSp As Integer
        Dim sVector() As String = Split(sSpVariables, "|")

        If iRow > DGBusqueda.RowCount Then Exit Sub

        PkBusCon.IniciaProcedimiento(sSPName)

        PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
        Dim iTot As Integer = UBound(sVector)
        For iSp = 0 To iTot
            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                Case "INT"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, Integer), SqlDbType.Int)

                Case "VARCHAR"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, String), SqlDbType.VarChar)
                Case "SMALLDATETIME"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, Date), SqlDbType.SmallDateTime)
                Case "DECIMAL"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, Decimal), SqlDbType.Decimal)
            End Select
        Next
        PkBusCon.GetScalarSP()
    End Sub

    Private Sub DGBusqueda_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGBusqueda.CellDoubleClick
        iValor = DGBusqueda.Item(0, e.RowIndex).Value
        Me.Dispose()
    End Sub

    Private Sub DGBusqueda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGBusqueda.KeyDown
        If e.KeyCode = Keys.Enter Then
            iValor = DGBusqueda.Item(0, DGBusqueda.CurrentCell.RowIndex).Value
            Me.Dispose()
        End If
    End Sub

End Class

