<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctBusquedaGen
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdAceptar = New System.Windows.Forms.Button
        Me.DGBusqueda = New System.Windows.Forms.DataGridView
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(493, 233)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Text = "Aceptar"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'DGBusqueda
        '
        Me.DGBusqueda.AllowUserToAddRows = False
        Me.DGBusqueda.AllowUserToDeleteRows = False
        Me.DGBusqueda.AllowUserToOrderColumns = True
        Me.DGBusqueda.AllowUserToResizeRows = False
        Me.DGBusqueda.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGBusqueda.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.DGBusqueda.Location = New System.Drawing.Point(3, 2)
        Me.DGBusqueda.MultiSelect = False
        Me.DGBusqueda.Name = "DGBusqueda"
        Me.DGBusqueda.ReadOnly = True
        Me.DGBusqueda.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DGBusqueda.Size = New System.Drawing.Size(565, 225)
        Me.DGBusqueda.TabIndex = 2
        '
        'ZctBusquedaGen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(570, 263)
        Me.Controls.Add(Me.DGBusqueda)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Name = "ZctBusquedaGen"
        Me.Text = "Busqueda"
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents DGBusqueda As System.Windows.Forms.DataGridView
End Class
