Imports System.Windows.Forms

Public Class ZctFrmAyuda


    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ZctFrmAyuda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Public Property Texto() As String
        Get
            Return ZctRichBox.Text
        End Get
        Set(ByVal value As String)


            ZctRichBox.Text = value



            Dim iRow As Integer = 1
            Dim iSelLast As Integer
            While iRow < ZctRichBox.Text.Length
                If InStr(ZctRichBox.Text, "<B>") > 0 Then
                    If InStr(ZctRichBox.Text, "</B>") > 0 Then
                        iSelLast = InStr(ZctRichBox.Text, "</B>")
                    Else
                        iSelLast = ZctRichBox.Text.Length
                    End If
                    ZctRichBox.Select(InStr(ZctRichBox.Text, "<B>") - 1, Math.Abs(InStr(ZctRichBox.Text, "<B>") - iSelLast - 1))

                    ZctRichBox.Font = New Font("Tahoma", 10, FontStyle.Regular)
                    ZctRichBox.SelectionFont = New Font("Tahoma", 12, FontStyle.Bold)

                    'ZctRichBox.SelectionColor = Color.Aqua
                    'Font(ZctRichBox.SelectionFont.FontFamily, 14, FontStyle.Bold)
                    ZctRichBox.Text = Replace(ZctRichBox.Text, "<B>", "", 1, 1)
                    ZctRichBox.Text = Replace(ZctRichBox.Text, "</B>", "", 1, 1)
                    iRow = iSelLast + 1
                Else
                    iRow = ZctRichBox.Text.Length
                End If

            End While
            ZctRichBox.Text = Replace(ZctRichBox.Text, "<BR>", vbCrLf)



        End Set
    End Property

   
End Class
