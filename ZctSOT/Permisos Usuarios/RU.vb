﻿Imports SOTControladores.Controladores

Public Class RU
    Dim _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Dim RolesUsuario As New List(Of Permisos.Controlador.Controlador.vistaroles)
    Private Sub RolesUsuarios_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        cboUsuario.GetData("ZctSegUsu")
        CboRoles.GetData("Zctroles")
        'SetDGV()
    End Sub
    Private Sub SetDGV()
        With grdDatos
            grdDatos.DataSource = Nothing
            grdDatos.Columns.Clear()
            grdDatos.Rows.Clear()
            grdDatos.AutoGenerateColumns = False
            grdDatos.AllowUserToAddRows = True
            grdDatos.AllowUserToDeleteRows = True
            grdDatos.Columns.Add(zctColumnComun.GetColumn("rol", "Rol", "nombre", True, , , True))
            grdDatos.Columns.Add(zctColumnComun.GetColumn("Cod_Rol", "rol", "cod_rol", False, , , True))
            grdDatos.Columns.Add(zctColumnComun.GetColumn("Cod_Usu", "cod_user", "cod_user", False, , , True))
            RolesUsuario = _Controlador.get_rolesusuario(cboUsuario.value)
            Me.grdDatos.DataSource = RolesUsuario
        End With
    End Sub
    Private Sub cboUsuario_SelectedValueChanged(sender As Object, e As System.EventArgs) Handles cboUsuario.SelectedValueChanged
        SetDGV()
    End Sub

    Private Sub Bmod_Click(sender As System.Object, e As System.EventArgs) Handles Bmod.Click
        Belimina.Enabled = True
        CboRoles.Enabled = True
        Basigna.Enabled = True
        Bmod.Enabled = False
        Bcancelar.Enabled = True
    End Sub

    Private Sub Basigna_Click(sender As System.Object, e As System.EventArgs) Handles Basigna.Click
        Try
            Basigna.Enabled = False
            Bguardar.Enabled = True
            Bcancelar.Enabled = True
            _Controlador.AgregaRolaUsuario(cboUsuario.value, CboRoles.value)
            'SetDGV()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Atencion")
        End Try
    End Sub

    Private Sub grdDatos_RowEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdDatos.RowEnter
        CboRoles.ZctSOTCombo1.SelectedItem = e.RowIndex
    End Sub

    Private Sub Buardar_Click(sender As System.Object, e As System.EventArgs) Handles Bguardar.Click
        Belimina.Enabled = False
        Bguardar.Enabled = False
        CboRoles.Enabled = False
        Bcancelar.Enabled = False
        _Controlador.GuardarCambios()
        _Controlador.Recorrermenu(ControladorBase.UsuarioActual.Id, ZctSOT.ZctSOTMain.MenuStrip)
        Basigna.Enabled = False
        Bmod.Enabled = True
        SetDGV()
    End Sub

    Private Sub Bcancelar_Click(sender As System.Object, e As System.EventArgs) Handles Bcancelar.Click
        Bguardar.Enabled = False
        Bcancelar.Enabled = False
        Basigna.Enabled = False
        Belimina.Enabled = False
        Bmod.Enabled = True
        _Controlador.CancelarCambios()
        MsgBox("Cambios cancelados", MsgBoxStyle.Exclamation, "Atencion")
        SetDGV()
    End Sub

    Private Sub Belimina_Click(sender As System.Object, e As System.EventArgs) Handles Belimina.Click
        Try
            Bguardar.Enabled = True
            Belimina.Enabled = False
            For Each R As DataGridViewRow In grdDatos.SelectedRows
                _Controlador.EliminaRol(R.Cells("Cod_Usu").Value, _
                                        R.Cells("Cod_rol").Value)
                Application.DoEvents()
            Next
            SetDGV()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atencion")
        End Try
    End Sub
End Class