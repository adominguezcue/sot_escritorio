﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RU
Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ZctRolesUsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.zctRolesUsuario = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctRoles = New System.Windows.Forms.BindingSource(Me.components)
        Me.Belimina = New ZctSOT.ZctSOTButton()
        Me.Basigna = New ZctSOT.ZctSOTButton()
        Me.CboRoles = New ZctSOT.ZctControlCombo()
        Me.Bmod = New ZctSOT.ZctSOTButton()
        Me.Bcancelar = New ZctSOT.ZctSOTButton()
        Me.Bguardar = New ZctSOT.ZctSOTButton()
        Me.grdDatos = New ZctSOT.ZctSotGrid()
        Me.cboUsuario = New ZctSOT.ZctControlCombo()
        CType(Me.ZctRolesUsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.zctRolesUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ZctRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ZctRolesUsuariosBindingSource
        '
        Me.ZctRolesUsuariosBindingSource.DataMember = "ZctRolesUsuarios"
        Me.ZctRolesUsuariosBindingSource.DataSource = Me.ZctRoles
        '
        'ZctRoles
        '
        Me.ZctRoles.DataSource = GetType(Permisos.Modelo.ZctRoles)
        '
        'Belimina
        '
        Me.Belimina.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Belimina.Enabled = False
        Me.Belimina.Location = New System.Drawing.Point(275, 292)
        Me.Belimina.Name = "Belimina"
        Me.Belimina.Size = New System.Drawing.Size(75, 23)
        Me.Belimina.TabIndex = 7
        Me.Belimina.Text = "Eliminar Rol"
        Me.Belimina.UseVisualStyleBackColor = True
        '
        'Basigna
        '
        Me.Basigna.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Basigna.Enabled = False
        Me.Basigna.Location = New System.Drawing.Point(113, 292)
        Me.Basigna.Name = "Basigna"
        Me.Basigna.Size = New System.Drawing.Size(75, 23)
        Me.Basigna.TabIndex = 6
        Me.Basigna.Text = "Asignar Rol "
        Me.Basigna.UseVisualStyleBackColor = True
        '
        'CboRoles
        '
        Me.CboRoles.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CboRoles.Enabled = False
        Me.CboRoles.Location = New System.Drawing.Point(58, 228)
        Me.CboRoles.Name = "CboRoles"
        Me.CboRoles.Nombre = "Roles Usuario"
        Me.CboRoles.Size = New System.Drawing.Size(463, 28)
        Me.CboRoles.TabIndex = 5
        Me.CboRoles.value = Nothing
        Me.CboRoles.ValueItem = 0
        Me.CboRoles.ValueItemStr = Nothing
        '
        'Bmod
        '
        Me.Bmod.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Bmod.Location = New System.Drawing.Point(243, 262)
        Me.Bmod.Name = "Bmod"
        Me.Bmod.Size = New System.Drawing.Size(75, 23)
        Me.Bmod.TabIndex = 4
        Me.Bmod.Text = "Modificar"
        Me.Bmod.UseVisualStyleBackColor = True
        '
        'Bcancelar
        '
        Me.Bcancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Bcancelar.Location = New System.Drawing.Point(373, 292)
        Me.Bcancelar.Name = "Bcancelar"
        Me.Bcancelar.Size = New System.Drawing.Size(75, 23)
        Me.Bcancelar.TabIndex = 3
        Me.Bcancelar.Text = "Cancelar"
        Me.Bcancelar.UseVisualStyleBackColor = True
        '
        'Bguardar
        '
        Me.Bguardar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Bguardar.Enabled = False
        Me.Bguardar.Location = New System.Drawing.Point(194, 292)
        Me.Bguardar.Name = "Bguardar"
        Me.Bguardar.Size = New System.Drawing.Size(75, 23)
        Me.Bguardar.TabIndex = 2
        Me.Bguardar.Text = "Guardar"
        Me.Bguardar.UseVisualStyleBackColor = True
        '
        'grdDatos
        '
        Me.grdDatos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDatos.AutoGenerateColumns = False
        Me.grdDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDatos.DataSource = Me.zctRolesUsuario
        Me.grdDatos.Location = New System.Drawing.Point(58, 59)
        Me.grdDatos.Name = "grdDatos"
        Me.grdDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdDatos.Size = New System.Drawing.Size(463, 157)
        Me.grdDatos.TabIndex = 1
        '
        'cboUsuario
        '
        Me.cboUsuario.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboUsuario.Location = New System.Drawing.Point(58, 12)
        Me.cboUsuario.Name = "cboUsuario"
        Me.cboUsuario.Nombre = "Usuario"
        Me.cboUsuario.Size = New System.Drawing.Size(463, 28)
        Me.cboUsuario.TabIndex = 0
        Me.cboUsuario.value = Nothing
        Me.cboUsuario.ValueItem = 0
        Me.cboUsuario.ValueItemStr = Nothing
        '
        'RolesUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(560, 327)
        Me.Controls.Add(Me.Belimina)
        Me.Controls.Add(Me.Basigna)
        Me.Controls.Add(Me.CboRoles)
        Me.Controls.Add(Me.Bmod)
        Me.Controls.Add(Me.Bcancelar)
        Me.Controls.Add(Me.Bguardar)
        Me.Controls.Add(Me.grdDatos)
        Me.Controls.Add(Me.cboUsuario)
        Me.Name = "RolesUsuarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Roles Usuarios"
        CType(Me.ZctRolesUsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.zctRolesUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ZctRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cboUsuario As ZctSOT.ZctControlCombo
    Friend WithEvents grdDatos As ZctSOT.ZctSotGrid
    Friend WithEvents Bguardar As ZctSOT.ZctSOTButton
    Friend WithEvents Bcancelar As ZctSOT.ZctSOTButton
    Friend WithEvents Bmod As ZctSOT.ZctSOTButton
    Friend WithEvents ZctRoles As System.Windows.Forms.BindingSource
    Friend WithEvents CboRoles As ZctSOT.ZctControlCombo
    Friend WithEvents Basigna As ZctSOT.ZctSOTButton
    Friend WithEvents zctRolesUsuario As System.Windows.Forms.BindingSource
    Friend WithEvents ZctRolesUsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Belimina As ZctSOT.ZctSOTButton
   
End Class
