﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RolesUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ZctRolesUsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctRoles = New System.Windows.Forms.BindingSource(Me.components)
        Me.zctRolesUsuario = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctSOTLabel2 = New ZctSOT.ZctSOTLabel()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel()
        Me.dgvauto = New ZctSOT.ZctSotGrid()
        Me.grdDatos = New ZctSOT.ZctSotGrid()
        Me.Belimina = New ZctSOT.ZctSOTButton()
        Me.Basigna = New ZctSOT.ZctSOTButton()
        Me.CboRoles = New ZctSOT.ZctControlCombo()
        Me.Bmod = New ZctSOT.ZctSOTButton()
        Me.Bcancelar = New ZctSOT.ZctSOTButton()
        Me.Bguardar = New ZctSOT.ZctSOTButton()
        Me.cboUsuario = New ZctSOT.ZctControlCombo()
        CType(Me.ZctRolesUsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ZctRoles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.zctRolesUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvauto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ZctRolesUsuariosBindingSource
        '
        Me.ZctRolesUsuariosBindingSource.DataMember = "ZctRolesUsuarios"
        Me.ZctRolesUsuariosBindingSource.DataSource = Me.ZctRoles
        '
        'ZctRoles
        '
        Me.ZctRoles.DataSource = GetType(Permisos.Modelo.ZctRoles)
        '
        'ZctSOTLabel2
        '
        Me.ZctSOTLabel2.AutoSize = True
        Me.ZctSOTLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel2.Location = New System.Drawing.Point(429, 58)
        Me.ZctSOTLabel2.Name = "ZctSOTLabel2"
        Me.ZctSOTLabel2.Size = New System.Drawing.Size(110, 16)
        Me.ZctSOTLabel2.TabIndex = 10
        Me.ZctSOTLabel2.Text = "Autorizaciones"
        Me.ZctSOTLabel2.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(149, 58)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(49, 16)
        Me.ZctSOTLabel1.TabIndex = 9
        Me.ZctSOTLabel1.Text = "Roles"
        Me.ZctSOTLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dgvauto
        '
        Me.dgvauto.AllowUserToAddRows = False
        Me.dgvauto.AllowUserToDeleteRows = False
        Me.dgvauto.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvauto.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvauto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvauto.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvauto.Location = New System.Drawing.Point(293, 87)
        Me.dgvauto.Name = "dgvauto"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvauto.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvauto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvauto.Size = New System.Drawing.Size(547, 177)
        Me.dgvauto.TabIndex = 8
        '
        'grdDatos
        '
        Me.grdDatos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDatos.AutoGenerateColumns = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdDatos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.grdDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDatos.DataSource = Me.zctRolesUsuario
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdDatos.DefaultCellStyle = DataGridViewCellStyle5
        Me.grdDatos.Location = New System.Drawing.Point(58, 87)
        Me.grdDatos.Name = "grdDatos"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdDatos.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.grdDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdDatos.Size = New System.Drawing.Size(229, 177)
        Me.grdDatos.TabIndex = 1
        '
        'Belimina
        '
        Me.Belimina.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Belimina.Location = New System.Drawing.Point(443, 334)
        Me.Belimina.Name = "Belimina"
        Me.Belimina.Size = New System.Drawing.Size(75, 23)
        Me.Belimina.TabIndex = 7
        Me.Belimina.Text = "Eliminar Rol"
        Me.Belimina.UseVisualStyleBackColor = True
        '
        'Basigna
        '
        Me.Basigna.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Basigna.Location = New System.Drawing.Point(281, 334)
        Me.Basigna.Name = "Basigna"
        Me.Basigna.Size = New System.Drawing.Size(75, 23)
        Me.Basigna.TabIndex = 6
        Me.Basigna.Text = "Asignar Rol "
        Me.Basigna.UseVisualStyleBackColor = True
        '
        'CboRoles
        '
        Me.CboRoles.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CboRoles.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboRoles.Location = New System.Drawing.Point(58, 270)
        Me.CboRoles.Name = "CboRoles"
        Me.CboRoles.Nombre = "Roles Usuario"
        Me.CboRoles.Size = New System.Drawing.Size(799, 28)
        Me.CboRoles.TabIndex = 5
        Me.CboRoles.value = Nothing
        Me.CboRoles.ValueItem = 0
        Me.CboRoles.ValueItemStr = Nothing
        '
        'Bmod
        '
        Me.Bmod.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Bmod.Location = New System.Drawing.Point(411, 304)
        Me.Bmod.Name = "Bmod"
        Me.Bmod.Size = New System.Drawing.Size(75, 23)
        Me.Bmod.TabIndex = 4
        Me.Bmod.Text = "Modificar"
        Me.Bmod.UseVisualStyleBackColor = True
        Me.Bmod.Visible = False
        '
        'Bcancelar
        '
        Me.Bcancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Bcancelar.Location = New System.Drawing.Point(541, 334)
        Me.Bcancelar.Name = "Bcancelar"
        Me.Bcancelar.Size = New System.Drawing.Size(75, 23)
        Me.Bcancelar.TabIndex = 3
        Me.Bcancelar.Text = "Cancelar"
        Me.Bcancelar.UseVisualStyleBackColor = True
        '
        'Bguardar
        '
        Me.Bguardar.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Bguardar.Location = New System.Drawing.Point(362, 334)
        Me.Bguardar.Name = "Bguardar"
        Me.Bguardar.Size = New System.Drawing.Size(75, 23)
        Me.Bguardar.TabIndex = 2
        Me.Bguardar.Text = "Guardar"
        Me.Bguardar.UseVisualStyleBackColor = True
        '
        'cboUsuario
        '
        Me.cboUsuario.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUsuario.Location = New System.Drawing.Point(58, 12)
        Me.cboUsuario.Name = "cboUsuario"
        Me.cboUsuario.Nombre = "Usuario"
        Me.cboUsuario.Size = New System.Drawing.Size(799, 28)
        Me.cboUsuario.TabIndex = 0
        Me.cboUsuario.value = Nothing
        Me.cboUsuario.ValueItem = 0
        Me.cboUsuario.ValueItemStr = Nothing
        '
        'RolesUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(896, 369)
        Me.Controls.Add(Me.ZctSOTLabel2)
        Me.Controls.Add(Me.ZctSOTLabel1)
        Me.Controls.Add(Me.dgvauto)
        Me.Controls.Add(Me.grdDatos)
        Me.Controls.Add(Me.Belimina)
        Me.Controls.Add(Me.Basigna)
        Me.Controls.Add(Me.CboRoles)
        Me.Controls.Add(Me.Bmod)
        Me.Controls.Add(Me.Bcancelar)
        Me.Controls.Add(Me.Bguardar)
        Me.Controls.Add(Me.cboUsuario)
        Me.MaximizeBox = False
        Me.Name = "RolesUsuarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Roles Usuarios"
        CType(Me.ZctRolesUsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ZctRoles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.zctRolesUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvauto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboUsuario As ZctSOT.ZctControlCombo
    Friend WithEvents grdDatos As ZctSOT.ZctSotGrid
    Friend WithEvents Bguardar As ZctSOT.ZctSOTButton
    Friend WithEvents Bcancelar As ZctSOT.ZctSOTButton
    Friend WithEvents Bmod As ZctSOT.ZctSOTButton
    Friend WithEvents ZctRoles As System.Windows.Forms.BindingSource
    Friend WithEvents CboRoles As ZctSOT.ZctControlCombo
    Friend WithEvents Basigna As ZctSOT.ZctSOTButton
    Friend WithEvents zctRolesUsuario As System.Windows.Forms.BindingSource
    Friend WithEvents ZctRolesUsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Belimina As ZctSOT.ZctSOTButton
    Friend WithEvents dgvauto As ZctSOT.ZctSotGrid
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSOTLabel2 As ZctSOT.ZctSOTLabel
    Friend WithEvents CodAutDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescAutDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodUsuDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomUsuDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AutorizadoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
