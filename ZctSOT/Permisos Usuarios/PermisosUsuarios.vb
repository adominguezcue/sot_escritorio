﻿Imports SOTControladores.Controladores

Public Class PermisosUsuarios
    Class PermisosModificados
        Dim _Rol, _Menu As Integer
        Dim _permisos As Permisos.Modelo.ZctPermisos
        Public Property Rol As Integer
            Set(value As Integer)
                _Rol = value
            End Set
            Get
                Return _Rol
            End Get
        End Property
        Public Property Menu As Integer
            Set(value As Integer)
                _Menu = value

            End Set
            Get
                Return _Menu
            End Get
        End Property
        Public Property Permisos As Permisos.Modelo.ZctPermisos
            Set(value As Permisos.Modelo.ZctPermisos)
                _permisos = value
            End Set
            Get
                Return _permisos
            End Get
        End Property
    End Class
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Dim TextoNodo As String
    Dim _Accion As New List(Of String)
    Dim _PermisosModificados As New Dictionary(Of String, PermisosModificados)
    Dim _permisos As New List(Of Permisos.Modelo.ZctPermisos)
    Private _Todos As Boolean
    Dim Key As String = ""
    Private Function CreateTreeNodesForMenuItems(menuItem As ToolStripMenuItem) As TreeNode()
        Dim nodes As New List(Of TreeNode)
        Dim subMenuItem
        For Each subMenuItem In menuItem.DropDownItems
            If subMenuItem.GetType.FullName <> "System.Windows.Forms.ToolStripMenuItem" Then
                Continue For
            End If
            Dim node As New TreeNode(subMenuItem.Text)
            Dim Nombre As String = subMenuItem.Name
            'Llamada recursiva
            node.Nodes.AddRange(CreateTreeNodesForMenuItems(subMenuItem))
            Dim IdMenu As List(Of Permisos.Modelo.ZctMenus) = _Controlador.get_menus(Nombre)
            If IdMenu.Count > 0 Then
                node.Tag = IdMenu(0)
            Else
                'MsgBox(Nombre)
            End If
            nodes.Add(node)
        Next

        Return nodes.ToArray()
    End Function
    Private Sub AltaPerfil_Load(sender As System.Object, e As System.EventArgs) Handles Me.Load
        For Each N As ToolStripMenuItem In ZctSOTMain.MainMenuStrip.Items
            'For Each N As ToolStripMenuItem In RibbonMain.MainMenuStrip.Items
            Dim Nombre As String = N.Name
            Dim nodo As New TreeNode(N.Text)
            Dim IdMenu As List(Of Permisos.Modelo.ZctMenus) = _Controlador.get_menus(Nombre)
            If IdMenu Is Nothing Then
                Exit For
            End If
            If IdMenu.Count > 0 Then
                nodo.Tag = IdMenu(0)
            Else
                Continue For
            End If
            nodo.Nodes.AddRange(CreateTreeNodesForMenuItems(N))
            ArbolMenu.Nodes.Add(nodo)
            Application.DoEvents()
        Next
        CboPerfiles.GetData("Zctroles")
        CboPerfiles.Enabled = True
    End Sub
    Private Sub ActualizaDatos(key As String)
        If ArbolMenu.SelectedNode Is Nothing Then
            Return
        End If
        If _PermisosModificados.ContainsKey(key) = False Then

            Dim p As New Permisos.Modelo.ZctPermisos
            With p
                .habilitado = chAcceso.Checked
                .agregar = chAgregar.Checked
                .editar = ChEditar.Checked
                .eliminar = ChEliminar.Checked
                .cerrar = ChcierraOc.Checked
            End With

            Dim PM As New PermisosModificados
            Dim Datos = Split(key, "-")
            With PM
                .Menu = Datos(1)
                .Rol = Datos(0)
                .Permisos = p
            End With
            _PermisosModificados.Add(key, PM)
        Else
            With _PermisosModificados(key).Permisos
                .habilitado = chAcceso.Checked
                .agregar = chAgregar.Checked
                .editar = ChEditar.Checked
                .eliminar = ChEliminar.Checked
                .cerrar = ChcierraOc.Checked
            End With
        End If
    End Sub
    Private Sub CargarPermisos()
        If IsNothing(ArbolMenu.SelectedNode) Then
            Return
        End If
        BguardarP.Enabled = True
        'BguardarPerfil.Enabled = True
        TextoNodo = ArbolMenu.SelectedNode.Text
        Dim Menu As Permisos.Modelo.ZctMenus = TryCast(ArbolMenu.SelectedNode.Tag, Permisos.Modelo.ZctMenus)
        If IsNumeric(ArbolMenu.SelectedNode.Tag.cod_menu) = False Then
            Me.GroupBox.Enabled = False
            Exit Sub
        Else
            Me.GroupBox.Enabled = True
        End If
        Dim Key As String = CboPerfiles.value & "-" & TryCast(ArbolMenu.SelectedNode.Tag, Permisos.Modelo.ZctMenus).Cod_menu
        If _PermisosModificados.ContainsKey(Key) Then
            _permisos.Add(_PermisosModificados(Key).Permisos)
            'MsgBox(_PermisosModificados(Key).Permisos.habilitado)
        Else
            _permisos = _Controlador.get_permisosrol(CboPerfiles.value, Menu.Cod_menu)
        End If
        If _permisos.Count = 0 Then
            _Controlador.insertaPermisosRolMenu(CboPerfiles.value, Menu.Cod_menu)
            _permisos = _Controlador.get_permisosrol(CboPerfiles.value, Menu.Cod_menu)
        Else
            Gpermisos.Enabled = True
        End If
        chAcceso.Checked = _permisos(0).habilitado
        chAgregar.Checked = _permisos(0).agregar
        ChEditar.Checked = _permisos(0).editar
        ChEliminar.Checked = _permisos(0).eliminar
        ChcierraOc.Checked = _permisos(0).cerrar
    End Sub


    Private Sub CboPerfiles_SelectedValueChanged(sender As Object, e As System.EventArgs) Handles CboPerfiles.SelectedValueChanged
        If IsNumeric(CboPerfiles.value) = False Then
            Return
        End If
        Dim Roles As List(Of Permisos.Modelo.ZctRoles) = _Controlador.get_roles(CboPerfiles.ZctSOTCombo1.Text)
        If Roles.Count > 0 Then
            TxtNombre.Text = Roles(0).nombre
            TxtDescripcionPerfil.Text = Roles(0).descripcion
        Else
            TxtNombre.Text = ""
            TxtDescripcionPerfil.Text = ""
        End If
        CargarPermisos()
    End Sub
    Private Sub BperfilNuevo_Click(sender As System.Object, e As System.EventArgs) Handles BperfilNuevo.Click
        Try
            BcancelaE.Enabled = True
            _Accion.Add("N")
            TxtNombre.Text = ""
            TxtDescripcionPerfil.Text = ""
            Dim _perfil As Long = CboPerfiles.value
            ArbolMenu.Enabled = False
            BperfilNuevo.Enabled = False
            TxtNombre.Enabled = True
            CboPrioridad.Enabled = True
            BeliminaPerfil.Enabled = False
            Beditar.Enabled = False
            BguardarPerfil.Enabled = True
            BcancelaE.Enabled = True
            CboPerfiles.Enabled = False
            TxtDescripcionPerfil.Enabled = True
            TxtNombre.Focus()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atencion")
        End Try
    End Sub
    Private Sub Beditar_Click(sender As System.Object, e As System.EventArgs) Handles Beditar.Click
        BcancelaE.Enabled = True
        _Accion.Add("E")
        BguardarPerfil.Enabled = True
        Beditar.Enabled = False
        ArbolMenu.Enabled = False
        BguardarPerfil.Enabled = False
        BeliminaPerfil.Enabled = False
        CboPerfiles.Enabled = False
        BperfilNuevo.Enabled = False
        BcancelaE.Enabled = True
        CboPerfiles.Enabled = False
        TxtDescripcionPerfil.Enabled = True
        TxtNombre.Enabled = True
        CboPrioridad.Enabled = True
        BguardarPerfil.Enabled = True
    End Sub

    Private Sub BeliminaPerfil_Click(sender As System.Object, e As System.EventArgs) Handles BeliminaPerfil.Click
        Try
            BguardarPerfil.Enabled = True
            ArbolMenu.Enabled = True
            _Accion.Add("B")

            If _Controlador.EliminaRol(CboPerfiles.value) Then
                CboPerfiles.ZctSOTCombo1.DataSource = Nothing
                CboPerfiles.GetData("Zctroles")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atencion")
        End Try
    End Sub

    Private Sub BguardarPerfil_Click(sender As System.Object, e As System.EventArgs) Handles BguardarPerfil.Click
        Try
            ArbolMenu.Enabled = True
            For Each A As String In _Accion
                Select Case A
                    Case "N"
                        _Controlador.AgregarRol(TxtNombre.Text, TxtDescripcionPerfil.Text)
                        _Controlador.GuardarCambios()
                        _Controlador.InsertarPermisosMenuDefault(False, TxtNombre.Text)

                    Case ("E")
                        _Controlador.ActualizaRol(CboPerfiles.value, TxtNombre.Text, TxtDescripcionPerfil.Text)

                    Case "B"
                        _Controlador.EliminaRol(CboPerfiles.value)

                    Case Else
                        Exit Sub
                End Select
                Application.DoEvents()
            Next
            _Controlador.GuardarCambios()
            TxtNombre.Enabled = False
            TxtDescripcionPerfil.Enabled = False
            CboPrioridad.Enabled = False
            BeliminaPerfil.Enabled = True
            BperfilNuevo.Enabled = True
            Beditar.Enabled = True
            BcancelaE.Enabled = True
            CboPerfiles.Enabled = True
            BguardarPerfil.Enabled = False
            BcancelaE.Enabled = False
            CboPerfiles.GetData("Zctroles")
            CboPerfiles.Focus()
            _Accion.Clear()
            MsgBox("Cambios en los roles de usuario Guardados correctamente", MsgBoxStyle.Information, "Realiizado")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Private Sub BcancelaE_Click(sender As System.Object, e As System.EventArgs) Handles BcancelaE.Click
        Try
            ArbolMenu.Enabled = True
            _Controlador.CancelarCambios()
            BcancelaE.Enabled = False
            BperfilNuevo.Enabled = True
            TxtNombre.Enabled = False
            TxtDescripcionPerfil.Enabled = False
            CboPrioridad.Enabled = False
            BeliminaPerfil.Enabled = True
            Beditar.Enabled = True
            BcancelaE.Enabled = True
            CboPerfiles.Enabled = True
            MsgBox("Los cambios se han revertido", MsgBoxStyle.Exclamation, "Atencion")
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atencion")
        End Try
    End Sub

    Private Sub ZctSOTButton6_Click(sender As System.Object, e As System.EventArgs) Handles BguardarP.Click
        _Todos = False
        If ArbolMenu.SelectedNode Is Nothing Then
            Return
        End If
        For Each PM As PermisosModificados In _PermisosModificados.Values
            _Controlador.ActualizaPermisos(CboPerfiles.value, PM.Menu, PM.Permisos.habilitado, _
                                            PM.Permisos.agregar, PM.Permisos.editar, PM.Permisos.eliminar, ChcierraOc.Checked)
            _Controlador.GuardarCambios()
            Application.DoEvents()
        Next
        _PermisosModificados.Clear()
        _permisos.Clear()
        '_Controlador.Recorrermenu(ControladorBase.UsuarioActual.Id, ZctSOTMain.MenuStrip)
        _Controlador.Recorrermenu(ControladorBase.UsuarioActual.Id, RibbonMain.MainMenuStrip)
        MsgBox("Permisos menu/ventana Guardados", MsgBoxStyle.Information, "Hecho")
    End Sub

    Private Sub ZctSOTButton5_Click(sender As System.Object, e As System.EventArgs) Handles ZctSOTButton5.Click
        _PermisosModificados.Clear()
        Me.Close()
    End Sub

    Private Sub ArbolMenu_AfterSelect(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles ArbolMenu.AfterSelect
        Key = CboPerfiles.value & "-" & TryCast(ArbolMenu.SelectedNode.Tag, Permisos.Modelo.ZctMenus).Cod_menu
        If e.Node.Level = 0 Then
            'Dim Pa = (From m As Permisos.Modelo.ZctMenus In TryCast(e.Node.Tag, Permisos.Modelo.ZctMenus) Where m.ZctPermisos)

        End If
        CargarPermisos()

    End Sub


    Private Sub chAcceso_Click(sender As Object, e As System.EventArgs) Handles chAcceso.Click, chAgregar.Click, _
        ChEditar.Click, ChEliminar.Click, ChcierraOc.Click
        ActualizaDatos(Key)
    End Sub



    Private Sub CheckBox1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chAgregas.CheckedChanged
        _Todos = True
        chAgregar.Checked = sender.Checked
        For Each Nodo As TreeNode In ArbolMenu.SelectedNode.Nodes
            Dim Keyl = CboPerfiles.value & "-" & TryCast(Nodo.Tag, Permisos.Modelo.ZctMenus).Cod_menu
            ActualizaDatos(Keyl)
        Next
        ArbolMenu.Focus()
        _Todos = False
    End Sub


    Private Sub CheckBox2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox2.CheckedChanged
        _Todos = True
        ChEditar.Checked = sender.Checked
        For Each Nodo As TreeNode In ArbolMenu.SelectedNode.Nodes
            Dim Keyl = CboPerfiles.value & "-" & TryCast(Nodo.Tag, Permisos.Modelo.ZctMenus).Cod_menu
            ActualizaDatos(Keyl)
        Next
        ArbolMenu.Focus()
        _Todos = False
    End Sub

    Private Sub CheckBox3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckBox3.CheckedChanged
        _Todos = True
        ChEliminar.Checked = sender.Checked
        For Each Nodo As TreeNode In ArbolMenu.SelectedNode.Nodes
            Dim Keyl = CboPerfiles.value & "-" & TryCast(Nodo.Tag, Permisos.Modelo.ZctMenus).Cod_menu
            ActualizaDatos(Keyl)
        Next
        ArbolMenu.Focus()
        _Todos = False
    End Sub

    Private Sub Chmenus_Click(sender As Object, e As System.EventArgs) Handles Chmenus.Click
        _Todos = True
        chAcceso.Checked = sender.Checked
        For Each Nodo As TreeNode In ArbolMenu.Nodes
            Dim Keyl = CboPerfiles.value & "-" & TryCast(Nodo.Tag, Permisos.Modelo.ZctMenus).Cod_menu
            ActualizaDatos(Keyl)
        Next
        ArbolMenu.Focus()
        _Todos = False
    End Sub

    Private Sub Chsm_Click(sender As Object, e As System.EventArgs) Handles Chsm.Click
    End Sub

    Private Sub Chsm_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles Chsm.CheckedChanged
        _Todos = True
        chAcceso.Checked = sender.Checked
        For Each Nodo As TreeNode In ArbolMenu.SelectedNode.Nodes
            Dim Keyl = CboPerfiles.value & "-" & TryCast(Nodo.Tag, Permisos.Modelo.ZctMenus).Cod_menu
            ActualizaDatos(Keyl)
        Next
        ArbolMenu.Focus()
        _Todos = False

    End Sub
End Class