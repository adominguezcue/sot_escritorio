﻿Imports Permisos.Modelo

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PermisosUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ArbolMenu = New System.Windows.Forms.TreeView()
        Me.GroupBox = New System.Windows.Forms.GroupBox()
        Me.CboPrioridad = New ZctSOT.ZctControlCombo()
        Me.BguardarPerfil = New ZctSOT.ZctSOTButton()
        Me.BcancelaE = New ZctSOT.ZctSOTButton()
        Me.BeliminaPerfil = New ZctSOT.ZctSOTButton()
        Me.Beditar = New ZctSOT.ZctSOTButton()
        Me.BperfilNuevo = New ZctSOT.ZctSOTButton()
        Me.TxtDescripcionPerfil = New ZctSOT.ZctControlTexto()
        Me.ZctRolesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TxtNombre = New ZctSOT.ZctControlTexto()
        Me.Gpermisos = New System.Windows.Forms.GroupBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.chAgregas = New System.Windows.Forms.CheckBox()
        Me.ChEliminar = New System.Windows.Forms.CheckBox()
        Me.ChEditar = New System.Windows.Forms.CheckBox()
        Me.chAgregar = New System.Windows.Forms.CheckBox()
        Me.chAcceso = New System.Windows.Forms.CheckBox()
        Me.BSroles = New System.Windows.Forms.BindingSource(Me.components)
        Me.Chmenus = New System.Windows.Forms.CheckBox()
        Me.Chsm = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ZctSOTButton5 = New ZctSOT.ZctSOTButton()
        Me.BguardarP = New ZctSOT.ZctSOTButton()
        Me.CboPerfiles = New ZctSOT.ZctControlCombo()
        Me.TxtDescripcionPerfil1 = New ZctSOT.ZctControlTexto()
        Me.ChcierraOc = New System.Windows.Forms.CheckBox()
        Me.GroupBox.SuspendLayout()
        CType(Me.ZctRolesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Gpermisos.SuspendLayout()
        CType(Me.BSroles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ArbolMenu
        '
        Me.ArbolMenu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ArbolMenu.FullRowSelect = True
        Me.ArbolMenu.HideSelection = False
        Me.ArbolMenu.Location = New System.Drawing.Point(15, 12)
        Me.ArbolMenu.Name = "ArbolMenu"
        Me.ArbolMenu.Size = New System.Drawing.Size(251, 409)
        Me.ArbolMenu.TabIndex = 0
        '
        'GroupBox
        '
        Me.GroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox.Controls.Add(Me.CboPrioridad)
        Me.GroupBox.Controls.Add(Me.BguardarPerfil)
        Me.GroupBox.Controls.Add(Me.BcancelaE)
        Me.GroupBox.Controls.Add(Me.BeliminaPerfil)
        Me.GroupBox.Controls.Add(Me.Beditar)
        Me.GroupBox.Controls.Add(Me.BperfilNuevo)
        Me.GroupBox.Controls.Add(Me.TxtDescripcionPerfil)
        Me.GroupBox.Controls.Add(Me.TxtNombre)
        Me.GroupBox.Location = New System.Drawing.Point(14, 82)
        Me.GroupBox.Name = "GroupBox"
        Me.GroupBox.Size = New System.Drawing.Size(436, 169)
        Me.GroupBox.TabIndex = 1
        Me.GroupBox.TabStop = False
        Me.GroupBox.Text = "Nuevo/ Edicion Rol usuarios"
        '
        'CboPrioridad
        '
        Me.CboPrioridad.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CboPrioridad.Location = New System.Drawing.Point(22, 83)
        Me.CboPrioridad.Name = "CboPrioridad"
        Me.CboPrioridad.Nombre = "ZctSOTLabel1"
        Me.CboPrioridad.Size = New System.Drawing.Size(400, 28)
        Me.CboPrioridad.TabIndex = 6
        Me.CboPrioridad.value = Nothing
        Me.CboPrioridad.ValueItem = 0
        Me.CboPrioridad.ValueItemStr = Nothing
        Me.CboPrioridad.Visible = False
        '
        'BguardarPerfil
        '
        Me.BguardarPerfil.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BguardarPerfil.Enabled = False
        Me.BguardarPerfil.Location = New System.Drawing.Point(266, 117)
        Me.BguardarPerfil.Name = "BguardarPerfil"
        Me.BguardarPerfil.Size = New System.Drawing.Size(75, 23)
        Me.BguardarPerfil.TabIndex = 6
        Me.BguardarPerfil.Text = "Guardar"
        Me.BguardarPerfil.UseVisualStyleBackColor = True
        '
        'BcancelaE
        '
        Me.BcancelaE.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BcancelaE.Enabled = False
        Me.BcancelaE.Location = New System.Drawing.Point(347, 118)
        Me.BcancelaE.Name = "BcancelaE"
        Me.BcancelaE.Size = New System.Drawing.Size(75, 23)
        Me.BcancelaE.TabIndex = 5
        Me.BcancelaE.Text = "Cancelar"
        Me.BcancelaE.UseVisualStyleBackColor = True
        '
        'BeliminaPerfil
        '
        Me.BeliminaPerfil.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BeliminaPerfil.Location = New System.Drawing.Point(184, 117)
        Me.BeliminaPerfil.Name = "BeliminaPerfil"
        Me.BeliminaPerfil.Size = New System.Drawing.Size(75, 23)
        Me.BeliminaPerfil.TabIndex = 4
        Me.BeliminaPerfil.Text = "Eliminar"
        Me.BeliminaPerfil.UseVisualStyleBackColor = True
        '
        'Beditar
        '
        Me.Beditar.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Beditar.Location = New System.Drawing.Point(103, 117)
        Me.Beditar.Name = "Beditar"
        Me.Beditar.Size = New System.Drawing.Size(75, 23)
        Me.Beditar.TabIndex = 3
        Me.Beditar.Text = "Editar"
        Me.Beditar.UseVisualStyleBackColor = True
        '
        'BperfilNuevo
        '
        Me.BperfilNuevo.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.BperfilNuevo.Location = New System.Drawing.Point(22, 117)
        Me.BperfilNuevo.Name = "BperfilNuevo"
        Me.BperfilNuevo.Size = New System.Drawing.Size(75, 23)
        Me.BperfilNuevo.TabIndex = 2
        Me.BperfilNuevo.Text = "Nuevo"
        Me.BperfilNuevo.UseVisualStyleBackColor = True
        '
        'TxtDescripcionPerfil
        '
        Me.TxtDescripcionPerfil.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtDescripcionPerfil.DataBindings.Add(New System.Windows.Forms.Binding("Tag", Me.ZctRolesBindingSource, "descripcion", True))
        Me.TxtDescripcionPerfil.Enabled = False
        Me.TxtDescripcionPerfil.Location = New System.Drawing.Point(17, 63)
        Me.TxtDescripcionPerfil.Multiline = False
        Me.TxtDescripcionPerfil.Name = "TxtDescripcionPerfil"
        Me.TxtDescripcionPerfil.Nombre = "Descripcion"
        Me.TxtDescripcionPerfil.Size = New System.Drawing.Size(405, 28)
        Me.TxtDescripcionPerfil.TabIndex = 1
        Me.TxtDescripcionPerfil.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtDescripcionPerfil.ToolTip = ""
        '
        'ZctRolesBindingSource
        '
        Me.ZctRolesBindingSource.DataSource = GetType(Permisos.Modelo.ZctRoles)
        '
        'TxtNombre
        '
        Me.TxtNombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtNombre.DataBindings.Add(New System.Windows.Forms.Binding("Tag", Me.ZctRolesBindingSource, "nombre", True))
        Me.TxtNombre.Enabled = False
        Me.TxtNombre.Location = New System.Drawing.Point(17, 29)
        Me.TxtNombre.Multiline = False
        Me.TxtNombre.Name = "TxtNombre"
        Me.TxtNombre.Nombre = "Nombre"
        Me.TxtNombre.Size = New System.Drawing.Size(405, 28)
        Me.TxtNombre.TabIndex = 0
        Me.TxtNombre.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtNombre.ToolTip = ""
        '
        'Gpermisos
        '
        Me.Gpermisos.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Gpermisos.Controls.Add(Me.ChcierraOc)
        Me.Gpermisos.Controls.Add(Me.CheckBox3)
        Me.Gpermisos.Controls.Add(Me.CheckBox2)
        Me.Gpermisos.Controls.Add(Me.chAgregas)
        Me.Gpermisos.Controls.Add(Me.ChEliminar)
        Me.Gpermisos.Controls.Add(Me.ChEditar)
        Me.Gpermisos.Controls.Add(Me.chAgregar)
        Me.Gpermisos.Controls.Add(Me.chAcceso)
        Me.Gpermisos.Enabled = False
        Me.Gpermisos.Location = New System.Drawing.Point(59, 262)
        Me.Gpermisos.Name = "Gpermisos"
        Me.Gpermisos.Size = New System.Drawing.Size(347, 178)
        Me.Gpermisos.TabIndex = 2
        Me.Gpermisos.TabStop = False
        Me.Gpermisos.Text = "Permisos ventanas"
        '
        'CheckBox3
        '
        Me.CheckBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(108, 129)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(139, 43)
        Me.CheckBox3.TabIndex = 10
        Me.CheckBox3.Text = "Permitir/negar eliminar a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "todos los submenus " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "del menu seleccionado"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(175, 72)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(135, 43)
        Me.CheckBox2.TabIndex = 9
        Me.CheckBox2.Text = "Permitir/negar editar a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "todos los submenus " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "del menu seleccionado"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'chAgregas
        '
        Me.chAgregas.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chAgregas.AutoSize = True
        Me.chAgregas.Location = New System.Drawing.Point(6, 69)
        Me.chAgregas.Name = "chAgregas"
        Me.chAgregas.Size = New System.Drawing.Size(143, 43)
        Me.chAgregas.TabIndex = 8
        Me.chAgregas.Text = "Permitir/negar  agregar a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "todos los submenus " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "del menu seleccionado"
        Me.chAgregas.UseVisualStyleBackColor = True
        '
        'ChEliminar
        '
        Me.ChEliminar.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.ChEliminar.AutoSize = True
        Me.ChEliminar.Location = New System.Drawing.Point(206, 30)
        Me.ChEliminar.Name = "ChEliminar"
        Me.ChEliminar.Size = New System.Drawing.Size(62, 17)
        Me.ChEliminar.TabIndex = 3
        Me.ChEliminar.Text = "Eliminar"
        Me.ChEliminar.UseVisualStyleBackColor = True
        '
        'ChEditar
        '
        Me.ChEditar.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.ChEditar.AutoSize = True
        Me.ChEditar.Location = New System.Drawing.Point(143, 30)
        Me.ChEditar.Name = "ChEditar"
        Me.ChEditar.Size = New System.Drawing.Size(53, 17)
        Me.ChEditar.TabIndex = 2
        Me.ChEditar.Text = "Editar"
        Me.ChEditar.UseVisualStyleBackColor = True
        '
        'chAgregar
        '
        Me.chAgregar.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chAgregar.AutoSize = True
        Me.chAgregar.Location = New System.Drawing.Point(76, 30)
        Me.chAgregar.Name = "chAgregar"
        Me.chAgregar.Size = New System.Drawing.Size(63, 17)
        Me.chAgregar.TabIndex = 1
        Me.chAgregar.Text = "Agregar"
        Me.chAgregar.UseVisualStyleBackColor = True
        '
        'chAcceso
        '
        Me.chAcceso.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chAcceso.AutoSize = True
        Me.chAcceso.Location = New System.Drawing.Point(14, 30)
        Me.chAcceso.Name = "chAcceso"
        Me.chAcceso.Size = New System.Drawing.Size(62, 17)
        Me.chAcceso.TabIndex = 0
        Me.chAcceso.Text = "Acceso"
        Me.chAcceso.UseVisualStyleBackColor = True
        '
        'Chmenus
        '
        Me.Chmenus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Chmenus.AutoSize = True
        Me.Chmenus.Location = New System.Drawing.Point(6, 427)
        Me.Chmenus.Name = "Chmenus"
        Me.Chmenus.Size = New System.Drawing.Size(115, 43)
        Me.Chmenus.TabIndex = 6
        Me.Chmenus.Text = "Permitir/negar " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "acceso a todos los" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "menus principales"
        Me.Chmenus.UseVisualStyleBackColor = True
        '
        'Chsm
        '
        Me.Chsm.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Chsm.AutoSize = True
        Me.Chsm.Location = New System.Drawing.Point(121, 427)
        Me.Chsm.Name = "Chsm"
        Me.Chsm.Size = New System.Drawing.Size(142, 43)
        Me.Chsm.TabIndex = 7
        Me.Chsm.Text = "Permitir/negar  acceso a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "todos los submenus " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "del menu seleccionado"
        Me.Chsm.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.ZctSOTButton5)
        Me.GroupBox1.Controls.Add(Me.BguardarP)
        Me.GroupBox1.Controls.Add(Me.CboPerfiles)
        Me.GroupBox1.Controls.Add(Me.Gpermisos)
        Me.GroupBox1.Controls.Add(Me.GroupBox)
        Me.GroupBox1.Location = New System.Drawing.Point(272, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(465, 475)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'ZctSOTButton5
        '
        Me.ZctSOTButton5.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.ZctSOTButton5.Location = New System.Drawing.Point(237, 446)
        Me.ZctSOTButton5.Name = "ZctSOTButton5"
        Me.ZctSOTButton5.Size = New System.Drawing.Size(75, 23)
        Me.ZctSOTButton5.TabIndex = 5
        Me.ZctSOTButton5.Text = "Cerrar"
        Me.ZctSOTButton5.UseVisualStyleBackColor = True
        '
        'BguardarP
        '
        Me.BguardarP.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.BguardarP.Enabled = False
        Me.BguardarP.Location = New System.Drawing.Point(156, 446)
        Me.BguardarP.Name = "BguardarP"
        Me.BguardarP.Size = New System.Drawing.Size(75, 23)
        Me.BguardarP.TabIndex = 4
        Me.BguardarP.Text = "Guardar"
        Me.BguardarP.UseVisualStyleBackColor = True
        '
        'CboPerfiles
        '
        Me.CboPerfiles.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CboPerfiles.Location = New System.Drawing.Point(23, 38)
        Me.CboPerfiles.Name = "CboPerfiles"
        Me.CboPerfiles.Nombre = "Perfiles"
        Me.CboPerfiles.Size = New System.Drawing.Size(416, 28)
        Me.CboPerfiles.TabIndex = 0
        Me.CboPerfiles.value = Nothing
        Me.CboPerfiles.ValueItem = 0
        Me.CboPerfiles.ValueItemStr = Nothing
        '
        'TxtDescripcionPerfil1
        '
        Me.TxtDescripcionPerfil1.Location = New System.Drawing.Point(17, 63)
        Me.TxtDescripcionPerfil1.Multiline = False
        Me.TxtDescripcionPerfil1.Name = "TxtDescripcionPerfil1"
        Me.TxtDescripcionPerfil1.Nombre = "ZctSOTLabel1"
        Me.TxtDescripcionPerfil1.Size = New System.Drawing.Size(327, 28)
        Me.TxtDescripcionPerfil1.TabIndex = 1
        Me.TxtDescripcionPerfil1.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtDescripcionPerfil1.ToolTip = ""
        '
        'ChcierraOc
        '
        Me.ChcierraOc.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.ChcierraOc.AutoSize = True
        Me.ChcierraOc.Location = New System.Drawing.Point(267, 30)
        Me.ChcierraOc.Name = "ChcierraOc"
        Me.ChcierraOc.Size = New System.Drawing.Size(74, 17)
        Me.ChcierraOc.TabIndex = 11
        Me.ChcierraOc.Text = "Cierra  OC"
        Me.ChcierraOc.UseVisualStyleBackColor = True
        '
        'PermisosUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(743, 476)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Chsm)
        Me.Controls.Add(Me.Chmenus)
        Me.Controls.Add(Me.ArbolMenu)
        Me.Name = "PermisosUsuarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Permisos"
        Me.GroupBox.ResumeLayout(False)
        CType(Me.ZctRolesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Gpermisos.ResumeLayout(False)
        Me.Gpermisos.PerformLayout()
        CType(Me.BSroles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ArbolMenu As System.Windows.Forms.TreeView
    Friend WithEvents GroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents BcancelaE As ZctSOT.ZctSOTButton
    Friend WithEvents BeliminaPerfil As ZctSOT.ZctSOTButton
    Friend WithEvents Beditar As ZctSOT.ZctSOTButton
    Friend WithEvents BperfilNuevo As ZctSOT.ZctSOTButton
    Friend WithEvents TxtDescripcionPerfil As ZctSOT.ZctControlTexto
    Friend WithEvents TxtNombre As ZctSOT.ZctControlTexto
    Friend WithEvents Gpermisos As System.Windows.Forms.GroupBox
    Friend WithEvents ChEliminar As System.Windows.Forms.CheckBox
    Friend WithEvents ChEditar As System.Windows.Forms.CheckBox
    Friend WithEvents chAgregar As System.Windows.Forms.CheckBox
    Friend WithEvents chAcceso As System.Windows.Forms.CheckBox
    Friend WithEvents CboPerfiles As ZctSOT.ZctControlCombo
    Friend WithEvents ZctSOTButton5 As ZctSOT.ZctSOTButton
    Friend WithEvents BguardarP As ZctSOT.ZctSOTButton
    Friend WithEvents TxtDescripcionPerfil1 As ZctSOT.ZctControlTexto
    Friend WithEvents BguardarPerfil As ZctSOT.ZctSOTButton
    Friend WithEvents CboPrioridad As ZctSOT.ZctControlCombo
    Friend WithEvents BSroles As System.Windows.Forms.BindingSource
    Friend WithEvents ZctRolesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Chmenus As System.Windows.Forms.CheckBox
    Friend WithEvents Chsm As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents chAgregas As System.Windows.Forms.CheckBox
    Friend WithEvents ChcierraOc As System.Windows.Forms.CheckBox
End Class
