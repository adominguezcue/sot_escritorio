﻿Public Class frmSendMail
    Private _folio As String

    Public Property folio() As String
        Get
            Return _folio
        End Get
        Set(ByVal value As String)
            _folio = value
        End Set
    End Property

    Public Property path_rpt As String

private _Path As String
    Public Property Path() As String
        Get
            return _Path
        End Get
        Set(ByVal value As String)
            _Path = value
        End Set
    End Property

private _Nombre As String
    Public Property Nombre() As String
        Get
            return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Private _Correo As String
    Public Property Correo() As String
        Get
            return _Correo
        End Get
        Set(ByVal value As String)
            _Correo = value
        End Set
    End Property

    Private _Cancelacion As Boolean = False
    Public Property Cancelacion() As Boolean
        Get
            Return _Cancelacion
        End Get
        Set(ByVal value As Boolean)
            _Cancelacion = value
        End Set
    End Property

    Property cod_folio As String

    Private Sub frmSendMail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtFolio.Text = _folio
        txtCorreo.Text = _Correo
        txtCliente.Text = _Nombre
        WebBrowser1.Navigate("about:blank")
        If _Cancelacion Then
            WebBrowser1.Document.Write(String.Format(_MailCancelacion, _Nombre, _folio))
        Else
            WebBrowser1.Document.Write(String.Format(_Mail, _Nombre, _folio))
        End If
        WebBrowser1.Refresh()
    End Sub

    Private Sub cmdEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEnviar.Click
        Try
            If txtCliente.Text = String.Empty Then

                MessageBox.Show("El correo debe de ser proporcionado")
                Exit Sub
            End If

            Dim mail As New DSEnviaCorreo.EnviaMail.SendMail
            mail.bcc = "contralor@restotal.com"
            mail.User = "facturacion@restotal.com"
            mail.Password = "Restotal01"

            If _Cancelacion Then
                mail.EnviaCorreoPar("Restotal <facturacion@restotal.com>", txtCorreo.Text, String.Format(_MailCancelacion, _Nombre, _folio), "Cancelación de factura electronica No. " & _folio, "smtpout.secureserver.net")
            Else

                mail.EnviaCorreoPar("Restotal <facturacion@restotal.com>", txtCorreo.Text, String.Format(_Mail, _Nombre, _folio), "Factura electronica No. " & _folio, "smtpout.secureserver.net", New String() {_Path & _folio & ".pdf", _Path & _folio & ".xml"}, "facturacion@restotal.com")
            End If

            MsgBox("Se ha enviado la factura correctamente")
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

 
    Private Sub cmdVista_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdVista.Click
        If txtFolio.Text = "0" Or txtFolio.Text = "" Then Exit Sub
        Dim myCr As New PkVisorRpt
        myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        myCr.sSQLV = "{VW_FacturacionEImpresa.Cod_Folio} = '" & cod_folio & "' AND {VW_FacturacionEImpresa.folio_factura} = " & folio.ToString() & ""

        myCr.sRpt = path_rpt
        myCr.Show()
    End Sub

    'Esto no se debe de hacer :)
    Private _Mail As String = "<div id='rootDiv' align='center'> <table style='background-color: #FFFFFF;' bgcolor='#FFFFFF' border='0' width='100%'cellspacing='0' cellpadding='0'> <tr> <td rowspan='1' colspan='1' align='center'> <table border='0' cellspacing='0' cellpadding='1'> <tr> <td style='padding: 0px;' rowspan='1' colspan='1'> </td> </tr> <tr> <td style='background-color: #6e6a56; padding: 1px;' bgcolor='#6e6a56' rowspan='1'colspan='1'> <table style='background-color: #FFFFFF;' bgcolor='#FFFFFF' border='0' width='100%'cellspacing='0' cellpadding='0'> <tr> <td style='padding: 0px;' width='100%' rowspan='1' colspan='1' align='center'> <table border='0' width='100%' cellpadding='5' cellspacing='0' id='content_LETTER.BLOCK2'> <tr> <td style='color: #6e6a56; font-family: Trebuchet MS,Verdana,Helvetica,sans-serif;font-size: 24pt;' styleclass='style_MainTitle' rowspan='1' align='center' colspan='1'> <font color='#6e6a56' size='2' face='Trebuchet MS,Verdana,Helvetica,sans-serif' style='color: #6e6a56;font-family: Trebuchet MS,Verdana,Helvetica,sans-serif; font-size: 10pt;'><font face='verdana,arial' size='1' color='#000000' style='font-family: verdana,arial;font-size: 8pt; color: #000000;'><br /><table bgcolor='#ffffff' id='VWPLINK'> <tr> <td style='font-size: 8pt; font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000;' width='100%' rowspan='1' colspan='1' class='style2'><div>Estimado(a) {0}</div><p>Queremos confirmarle que su factura {1} ha sido procesada correctamente y podrá encontrarla en un archivo adjunto en este mensaje.</p>Saludos cordiales<br/>RESTOTAL.</td> </tr> </table> </font> </font> </td> </tr> </table><br> </div>"

    Private _MailCancelacion As String = "<div id='rootDiv' align='center'> <table style='background-color: #FFFFFF;' bgcolor='#FFFFFF' border='0' width='100%'cellspacing='0' cellpadding='0'> <tr> <td rowspan='1' colspan='1' align='center'> <table border='0' cellspacing='0' cellpadding='1'> <tr> <td style='padding: 0px;' rowspan='1' colspan='1'> </td> </tr> <tr> <td style='background-color: #6e6a56; padding: 1px;' bgcolor='#6e6a56' rowspan='1'colspan='1'> <table style='background-color: #FFFFFF;' bgcolor='#FFFFFF' border='0' width='100%'cellspacing='0' cellpadding='0'> <tr> <td style='padding: 0px;' width='100%' rowspan='1' colspan='1' align='center'> <table border='0' width='100%' cellpadding='5' cellspacing='0' id='content_LETTER.BLOCK2'> <tr> <td style='color: #6e6a56; font-family: Trebuchet MS,Verdana,Helvetica,sans-serif;font-size: 24pt;' styleclass='style_MainTitle' rowspan='1' align='center' colspan='1'> <font color='#6e6a56' size='2' face='Trebuchet MS,Verdana,Helvetica,sans-serif' style='color: #6e6a56;font-family: Trebuchet MS,Verdana,Helvetica,sans-serif; font-size: 10pt;'><font face='verdana,arial' size='1' color='#000000' style='font-family: verdana,arial;font-size: 8pt; color: #000000;'><br /><table bgcolor='#ffffff' id='VWPLINK'> <tr> <td style='font-size: 8pt; font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000;' width='100%' rowspan='1' colspan='1' class='style2'><div>Estimado(a) {0}</div><p>Queremos comunicarle que su factura {1} ha sido cancelada, cualquier aclaración al respecto estamos a sus ordenes.</p>Saludos cordiales<br/>RESTOTAL.</td> </tr> </table> </font> </font> </td> </tr> </table><br> </div>"

    Private Sub cmdSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSalir.Click
        Me.Close()
    End Sub
End Class