Imports System.Data.SqlDbType
Imports ZctSOT.ZctFunciones
Imports ZctSOT.Datos
Imports ZctSOT.Datos.ZctFunciones
Imports SOTControladores.Controladores
Imports ZctSOT.Constantes
Imports System.Linq
Imports Modelo.Almacen.Constantes

Public Class ZctOrdTrabajo
    Friend _Controlador As New SOTControladores.Controladores.ControladorPermisos() 'Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos 'Permisos.Controlador.Controlador.vistapermisos
    Dim _Guardar As Boolean

    Dim ALMACEN_DEFAULT = 0

    Dim sTemp As Integer = 0

    Dim iColArt As Integer = 0                                         'Columna del art�culo
    Dim iColDesc As Integer = 1                                        'Columna de la Descripci�n
    Dim iColAlm As Integer = 2                                         'Columna de la existencia
    Dim iColExist As Integer = 3                                       'Columna de la existencia
    Dim iColSol As Integer = 4                                         'Columna solicitada
    Dim iColSurt As Integer = 5                                        'Columna del surtido
    Dim iColPrecio As Integer = 6                                      'Columna del Precio
    Dim iColCosto As Integer = 7
    Dim iColSubTotal As Integer = 8                                    'Columna del subtotal
    Dim iColCod As Integer = 9                                         'Columna del c�digo
    Dim iColTipo As Integer = 10
    Dim iColMot As Integer = 11                                        'Columna de la motocicleta
    Dim iColUsu As Integer = 12

    Dim sStatus As String                                              'Estatus del documento
    Dim sPathRpt As String = "c:\Reportes\ZctRptOTDet.rpt"

    'Cat�logos de almacenes
    Dim CatAlm As New List(Of Clases.Catalogos.CatAlmacen)
    'Autorizaciones
    Dim sAutorizacion As String = "N"
    Dim sObservaciones As String = ""
    Public salir_en_automatico As Boolean = False
    Dim OcFolio As New zctFolios("OT")
    Private load As Boolean = False

    Dim parametrosSistema = ControladorParametros.ObtenerParametros()

    Private Sub ZctOrdComp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        inicia_controles()

    End Sub

    Public Sub inicia_controles()

        If load Then Exit Sub
        load = True
        _Permisos = _Controlador.ObtenerPermisosActuales() '.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        With _Permisos
            DGridArticulos.AllowUserToAddRows = .CrearOrdenTrabajo
            DGridArticulos.AllowUserToDeleteRows = .ModificarOrdenTrabajo
            DGridArticulos.ReadOnly = Not (.ModificarOrdenTrabajo Or .CrearOrdenTrabajo)
            _Guardar = IIf(.ModificarOrdenTrabajo = False And .CrearOrdenTrabajo = False And .EliminarOrdenTrabajo = False, False, True)
            cmdAceptar.Enabled = _Guardar
            cmdPagar.Enabled = _Guardar
            cmdCancelaOrden.Enabled = _Guardar
            'cmdCancelar.Enabled = _Guardar
        End With
        'Posici�n Inicial
        Me.Top = 0
        Me.Left = 0
        'Cliente
        'Limpia la descripci�n
        txtCliente.ZctSOTLabelDesc1.Text = ""
        'Asigna los datos de la busqueda
        txtCliente.SqlBusqueda = "SELECT [Cod_Cte] as C�digo ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
        'Asigna el nombre del procedimiento almacenado para la busqueda
        txtCliente.SPName = "SP_ZctCatCte"
        'Envia las variables del procedimiento almacenado
        txtCliente.SpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
        'Envia la tabla de referencia al procedimiento almacenado
        txtCliente.Tabla = "ZctCatCte_Busqueda"

        txtFolio.DataBindings.Clear()
        txtFolio.Text = OcFolio.Consecutivo 'GetData("Procedimientos_MAX", "ZctEncOT")

        'GetAlm()
        Dim ColAlmacen As DataGridViewComboBoxColumn = CType(DGridArticulos.Columns("ColAlmacen"), DataGridViewComboBoxColumn)
        ColAlmacen.DataSource = New ListAlmacenes("SP_ZctCatAlm_salida", "@Cod_Alm;int|@Desc_CatAlm;varchar|@ColNuevo;int", "0|0|0")
        ColAlmacen.ValueMember = "CodAlm"
        ColAlmacen.DisplayMember = "DescAlm"
        sStatus = "A"


        ALMACEN_DEFAULT = parametrosSistema.cod_alm_salida

    End Sub
    Public Sub CargaOT(Cod_ot As Integer)
        salir_en_automatico = True
        inicia_controles()
        txtFolio.Text = (Cod_ot).ToString
        Inicia_carga_orden()
    End Sub

    Private Shared Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpParametros As Dictionary(Of String, Object)) As DataTable
        Try
            Dim PkBusCon As New Datos.ZctDataBase                         'Objeto de la base de datos
            Dim iSp As Integer                                      'Indice del procedimiento almacenado
            Dim sVector() As String = sSpParametros.Keys.ToArray() 'Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
            Dim sValores() As Object = sSpParametros.Values.ToArray() 'Split(sSpValores, "|")
            'Inicia el procedimieto almacenado
            PkBusCon.IniciaProcedimiento(sSpNAME)
            'Agrega el parametro que define que se va a realizar
            PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
            'Obtiene el total de los procedimientos
            Dim iTot As Integer = UBound(sVector)
            'Recorre las variables del procedimiento almacenado
            For iSp = 0 To iTot
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                    Case "INT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Int)
                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_String), SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Date), SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Decimal)
                    Case "TEXT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_String), SqlDbType.Text)
                End Select
            Next

            Select Case TpConsulta
                'SELECT
                Case 1
                    PkBusCon.InciaDataAdapter()
                    Return PkBusCon.GetReaderSP.Tables("DATOS")
                Case 2
                    'Ejecuta el escalar
                    PkBusCon.GetScalarSP()
                    Return Nothing

                Case 3
                    'Delete
                    PkBusCon.GetScalarSP()
                    Return Nothing
                Case Else
                    Return Nothing
            End Select

        Catch ex As Exception
            'Enviar una exepci�n
            MsgBox(ex.Message.ToString)
            Return Nothing
        End Try
    End Function


    Private Shared Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpParametros As Dictionary(Of String, Object), ByRef PkCon As ZctDataBase) As DataTable


        Dim iSp As Integer                                      'Indice del procedimiento almacenado
        Dim sVector() As String = sSpParametros.Keys.ToArray() 'Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
        Dim sValores() As Object = sSpParametros.Values.ToArray() 'Split(sSpValores, "|")
        'Inicia el procedimieto almacenado
        PkCon.IniciaProcedimiento(sSpNAME)
        'Agrega el parametro que define que se va a realizar
        PkCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
        'Obtiene el total de los procedimientos
        Dim iTot As Integer = UBound(sVector)
        'Recorre las variables del procedimiento almacenado
        For iSp = 0 To iTot
            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                Case "INT"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Int)
                Case "VARCHAR"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_String), SqlDbType.VarChar)
                Case "SMALLDATETIME"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Date), SqlDbType.SmallDateTime)
                Case "DECIMAL"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Decimal)
                Case "TEXT"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_String), SqlDbType.Text)
            End Select
        Next

        Select Case TpConsulta
            'SELECT
            Case 2
                'Ejecuta el escalar
                PkCon.GetScalarSPTran()
                Return Nothing


            Case Else
                Return Nothing
        End Select
    End Function
    'Busca el valor del indice de la tabla en cuesti�n
    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String)
        Try
            Dim PkBusCon As New Datos.ZctDataBase                             'Objeto de la base de datos
            'Inicializa el procedimietno almacenado
            PkBusCon.IniciaProcedimiento(Procedimiento)
            'Asigna el nombre de la tabla 
            PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
            'Asigna el resultado de la consulta a el objeto
            Dim a As Object = PkBusCon.GetScalarSP()
            'En caso de que no este vac�o asigna su valor a la columna de indice
            If Not a Is System.DBNull.Value Then
                Return CType(a, Integer)
            Else
                Return 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return 1
        End Try
    End Function

    'Busca el valor del indice de la tabla en cuesti�n
    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String, ByVal PkCon As ZctDataBase)

        'Objeto de la base de datos
        'Inicializa el procedimietno almacenado
        PkCon.IniciaProcedimiento(Procedimiento)
        'Asigna el nombre de la tabla 
        PkCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
        'Asigna el resultado de la consulta a el objeto
        Dim a As Object = PkCon.GetScalarSPTran()
        'En caso de que no este vac�o asigna su valor a la columna de indice
        If Not a Is System.DBNull.Value Then
            Return CType(a, Integer)
        Else
            Return 1
        End If
    End Function

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If sStatus = "A" Then
            Me.Dispose()
        Else
            plimpia(True)
            sStatus = "A"
            cmdImprimir.Visible = False
        End If
        If salir_en_automatico Then
            Me.Close()
        End If
    End Sub
    Private Sub plimpia(ByVal LimpiaFolio As Boolean)
        'AHL 12/03/2008
        'Valida que la limpieza renueve o no el folio
        If LimpiaFolio Then
            txtFolio.DataBindings.Clear()
            txtFolio.Text = ""
            txtFolio.Text = OcFolio.Consecutivo 'GetData("Procedimientos_MAX", "ZctEncOT")
            txtFolio.Focus()
            'AHL 12/03/2008
            'Vuelve a habilitar el folio
            'ZCTFolio.Enabled = True
            txtFolio.Enabled = True
        End If
        'txtVin.DataBindings.Clear()
        'txtVin.Text = ""
        'txtVin.ZctSOTLabelDesc1.Text = ""

        sAutorizacion = "N"
        sObservaciones = ""
        'ZctHistAprov.Text = "Pendiente de aprobar"
        'ZctHistAprov.BackColor = lblPorSurtir.BackColor
        'chkAprobada.Checked = False
        DGridArticulos.Enabled = True
        cmdAceptar.Enabled = _Guardar
        cmdPagar.Enabled = _Guardar

        'chkAprobada.Enabled = True
        cmdCancelaOrden.Enabled = True
        cmdImprimir.Enabled = True

        txtCliente.DataBindings.Clear()
        txtCliente.Text = ""
        txtCliente.ZctSOTLabelDesc1.Text = ""

        'txtTecnico.Text = 0
        'txtTecnico.ZctSOTLabelDesc1.Text = ""


        'DTEntrada.DataBindings.Clear()
        'DTSalida.DataBindings.Clear()
        'DTRmpEntrada.DataBindings.Clear()
        'DTRmpSalida.DataBindings.Clear()
        txtTrabajo.DataBindings.Clear()

        lblSubTotal.Text = FormatCurrency(0)
        lblIva.Text = FormatCurrency(0)
        lblTotal.Text = FormatCurrency(0)

        'txtKm.DataBindings.Clear()
        'txtKm.Text = ""

        txtTrabajo.Text = ""
        DGridArticulos.Rows.Clear()
        txtFolio.Focus()

    End Sub
    Dim surtidoTMP As Integer
    Private Sub DGridArticulos_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles DGridArticulos.CellBeginEdit
        If sAutorizacion = "C" Then e.Cancel = True : Exit Sub
        If e.ColumnIndex <> iColArt Then
            'Valida que el art�culo no este vac�o
            If DGridArticulos.CurrentRow.Cells(iColArt).Value Is Nothing OrElse DGridArticulos.CurrentRow.Cells(iColArt).Value Is DBNull.Value OrElse DGridArticulos.CurrentRow.Cells(iColArt).Value.ToString.Trim = "" Then e.Cancel = True : Exit Sub
        End If
        If e.ColumnIndex = iColSurt Then

            If DGridArticulos.Item(iColSurt, e.RowIndex).Value Is DBNull.Value Or DGridArticulos.Item(iColSurt, e.RowIndex).Value Is Nothing Then Exit Sub
            sTemp = CType(IIf(DGridArticulos.Item(iColSurt, e.RowIndex).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, e.RowIndex).Value), Integer)
        ElseIf e.ColumnIndex = iColArt And (DGridArticulos.CurrentRow.Cells(iColCod).Value IsNot Nothing AndAlso IsNumeric(DGridArticulos.CurrentRow.Cells(iColCod).Value) AndAlso CInt(DGridArticulos.CurrentRow.Cells(iColCod).Value) > 0) Then
            e.Cancel = True
        ElseIf e.ColumnIndex = iColAlm And DGridArticulos.Item(iColTipo, e.RowIndex).Value IsNot DBNull.Value AndAlso DGridArticulos.Item(iColTipo, e.RowIndex).Value = "MO" Then
            e.Cancel = True
        ElseIf (e.ColumnIndex = iColCosto Or e.ColumnIndex = iColPrecio) And (DGridArticulos.CurrentRow.Cells(iColCod).Value IsNot Nothing AndAlso IsNumeric(DGridArticulos.CurrentRow.Cells(iColCod).Value) AndAlso CInt(DGridArticulos.CurrentRow.Cells(iColCod).Value) > 0) Then
            e.Cancel = True
        End If
    End Sub

    Private Sub DGridArticulos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellEndEdit
        Try

            If e.ColumnIndex = iColArt Then
                If DGridArticulos.CurrentCell.Value <> "" Then
                    Dim PkBusCon As New Datos.ZctDataBase
                    'Inicia el procedimiento almacenado
                    PkBusCon.IniciaProcedimiento("SP_ZctCatArt_Busqueda")
                    'Busca los datos del c�digo ingresado
                    PkBusCon.AddParameterSP("@Codigo", DGridArticulos.CurrentCell.Value, SqlDbType.VarChar)
                    'Inicia el Adaptador de datos
                    PkBusCon.InciaDataAdapter()
                    'Obtiene los datos
                    Dim dtDatos As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")
                    'Obtiene los datos del grid
                    If dtDatos.Rows.Count <= 0 Then
                        MsgBox("El c�digo de art�culo no existe, verifique por favor.", MsgBoxStyle.Information, "SOT")
                        'DGridArticulos.Rows.Remove(DGridArticulos.Rows(e.RowIndex))
                        'AHL 12/03/2008
                        DGridArticulos.CurrentRow.Cells(iColArt).Value = ""
                        Exit Sub
                    End If
                    If DGridArticulos.CurrentRow.Cells(iColUsu).Value = "" Then
                        DGridArticulos.CurrentRow.Cells(iColUsu).Value = ControladorBase.UsuarioActual.Id
                    End If
                    DGridArticulos.CurrentRow.Cells(iColArt).Value = dtDatos.Rows.Item(0).Item("Cod_Art").ToString
                    DGridArticulos.CurrentRow.Cells(iColDesc).Value = dtDatos.Rows.Item(0).Item("Desc_Art").ToString
                    DGridArticulos.CurrentRow.Cells(iColSurt).Value = dtDatos.Rows.Item(0).Item("ColSurt").ToString
                    DGridArticulos.CurrentRow.Cells(iColPrecio).Value = dtDatos.Rows.Item(0).Item("Prec_Art").ToString
                    DGridArticulos.CurrentRow.Cells(iColTipo).Value = dtDatos.Rows.Item(0).Item("Cod_TpArt").ToString
                    DGridArticulos.CurrentRow.Cells(iColCosto).Value = "0"
                    If (ALMACEN_DEFAULT > 0) Then
                        DGridArticulos.CurrentRow.Cells(iColAlm).Value = ALMACEN_DEFAULT
                    End If
                    CalculaExistencia(e.RowIndex)
                    DGridArticulos.CurrentRow.Cells(iColCosto).Value = FormatCurrency(DGridArticulos.CurrentRow.Cells(iColCosto).Value)

                    If DGridArticulos.CurrentRow.Cells(iColSurt).Value <= 0 Then
                        DGridArticulos.CurrentRow.Cells(iColSurt).Value = "1"
                    End If

                    DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String))
                    dtDatos.Dispose()
                    sGetTotal()

                End If

            ElseIf iColSurt = e.ColumnIndex Or iColSol = e.ColumnIndex Then
                DGridArticulos.CurrentRow.Cells(iColSol).Value = DGridArticulos.CurrentRow.Cells(iColSurt).Value
                pSetColor(e.RowIndex)

                'Si es mano de obra el surtido iguala a lo solicitado
                If DGridArticulos.Item(iColTipo, e.RowIndex).Value IsNot DBNull.Value AndAlso DGridArticulos.Item(iColTipo, e.RowIndex).Value = "MO" Then
                    If iColSurt = e.ColumnIndex Then
                        DGridArticulos.Item(iColSol, e.RowIndex).Value = DGridArticulos.Item(iColSurt, e.RowIndex).Value
                    Else
                        DGridArticulos.Item(iColSurt, e.RowIndex).Value = DGridArticulos.Item(iColSol, e.RowIndex).Value
                    End If
                End If


                If iColSurt = e.ColumnIndex Then
                    'If CType(IIf(DGridArticulos.Item(iColSurt, e.RowIndex).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, e.RowIndex).Value), Integer) <= CType(IIf(DGridArticulos.Item(iColExist, e.RowIndex).Value = "", 0, DGridArticulos.Item(iColExist, e.RowIndex).Value), Integer) OrElse (DGridArticulos.Item(iColTipo, e.RowIndex).Value IsNot DBNull.Value AndAlso DGridArticulos.Item(iColTipo, e.RowIndex).Value = "MO") Then
                    DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String))
                    sGetTotal()
                    'Else
                    'MsgBox("La cantidad surtida debe ser menor o igual a las existencias, se necesita una autorizaci�n para continuar.", MsgBoxStyle.Exclamation)
                    'Dim fLogin As New ZctLogin
                    ''Autorizaci�n para surtir sin existencia
                    'fLogin.Cod_Aut = 1
                    'fLogin.ShowDialog(Me)
                    'If fLogin.Valida = True Then
                    '    DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String))
                    '    sGetTotal()
                    'Else
                    '    DGridArticulos.Item(iColSurt, e.RowIndex).Value = sTemp
                    'End If
                    'End If
                End If
            ElseIf iColAlm = e.ColumnIndex Then
                CalculaExistencia(e.RowIndex)
            ElseIf iColPrecio = e.ColumnIndex Then
                If DGridArticulos.CurrentRow.Cells(iColPrecio).Value = "" OrElse Not IsNumeric(DGridArticulos.CurrentRow.Cells(iColPrecio).Value) OrElse CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal) < 0 Then
                    DGridArticulos.CurrentRow.Cells(iColPrecio).Value = 0
                End If
                DGridArticulos.CurrentRow.Cells(iColPrecio).Value = FormatCurrency(DGridArticulos.CurrentRow.Cells(iColPrecio).Value)
                DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String))
                sGetTotal()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ZctSotGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGridArticulos.KeyDown
        Try
            If sAutorizacion = "C" Then Exit Sub
            If e.KeyCode = Keys.F3 And DGridArticulos.CurrentCell.ColumnIndex = iColArt And Not (DGridArticulos.CurrentRow.Cells(iColCod).Value IsNot Nothing AndAlso IsNumeric(DGridArticulos.CurrentRow.Cells(iColCod).Value) AndAlso CInt(DGridArticulos.CurrentRow.Cells(iColCod).Value) > 0) Then

                Dim fBusqueda As New ZctBusqueda
                fBusqueda.sSPName = "SP_ZctCatArt"
                fBusqueda.sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT"
                fBusqueda.ShowDialog(Me)
                DGridArticulos.BeginEdit(True)
                DGridArticulos.CurrentRow.Cells(DGridArticulos.CurrentCell.ColumnIndex).Value = fBusqueda.iValor
                If (ALMACEN_DEFAULT > 0) Then
                    DGridArticulos.CurrentRow.Cells(iColAlm).Value = ALMACEN_DEFAULT
                End If
                'DGridArticulos.CurrentCell.Value = fBusqueda.iValor

                DGridArticulos.EndEdit(True)
                DGridArticulos.UpdateCellValue(iColArt, DGridArticulos.CurrentCell.RowIndex)
                DGridArticulos.UpdateCellValue(iColAlm, DGridArticulos.CurrentCell.RowIndex)
                ColSurt.Selected = True
            ElseIf e.KeyCode = Keys.Delete And Not (DGridArticulos.CurrentRow.Cells(iColCod).Value IsNot Nothing AndAlso IsNumeric(DGridArticulos.CurrentRow.Cells(iColCod).Value) AndAlso CInt(DGridArticulos.CurrentRow.Cells(iColCod).Value) > 0) Then
                If DGridArticulos.CurrentRow.IsNewRow Then Exit Sub
                If MsgBox("�Desea eliminar este art�culo?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                DGridArticulos.Rows.Remove(DGridArticulos.CurrentRow)
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub sGetTotal()
        Dim iRow As Integer
        Dim iSubTotal As Decimal = 0
        For iRow = 0 To DGridArticulos.Rows.Count - 1
            'If DGridArticulos.Item(iColSubTotal, iRow).ToString <> "" Then
            iSubTotal = iSubTotal + CType(DGridArticulos.Item(iColSubTotal, iRow).Value, Decimal)

            ' End If
        Next

        'lblSubTotal.Text = FormatCurrency(iSubTotal.ToString)
        'lblIva.Text = FormatCurrency(CType(iSubTotal * ZctSOTMain.ParametrosSistema.Iva_CatPar, String))
        lblTotal.Text = FormatCurrency(iSubTotal.ToString) '= FormatCurrency(CType(iSubTotal + CDbl(lblIva.Text), String))
    End Sub

    Public Function valida_datos() As Boolean
        'Valida los datos
        If txtCliente.Text = "" Or txtCliente.Text = "0" Then
            MsgBox("La orden no puede ser grabada si no ha ingresado el c�digo del cliente, verifique por favor.", MsgBoxStyle.Information, "SOT")
            If txtCliente.Enabled Then txtCliente.Focus()
            Return False
        End If

        If DGridArticulos.RowCount <= 0 Then
            MsgBox("La orden no puede ser grabada sin art�culos, verifique por favor.", MsgBoxStyle.Information, "SOT")
            Return False
        Else
            If DGridArticulos.Rows(0).Cells(iColArt).Value Is DBNull.Value Or DGridArticulos.Rows(0).Cells(iColArt).Value Is Nothing Then
                MsgBox("La orden no puede ser grabada sin art�culos, verifique por favor.", MsgBoxStyle.Information, "SOT")
                Return False
            End If
        End If

        'Valida que no se hagan salidas en cero.
        For i As Integer = 0 To DGridArticulos.Rows.Count - 1
            If DGridArticulos.Item(iColArt, i).Value <> "" AndAlso DGridArticulos.Item(iColSurt, i).Value > 0 AndAlso DGridArticulos.Item(iColPrecio, i).Value = 0 Then
                MsgBox("El art�culo " & DGridArticulos.Item(iColArt, i).Value & " tiene precio cero, favor de verificar.")
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub graba_datos()

        ' Dim drEncOT As DataTable
        'Dim sVariables As String
        Dim sParametros As New Dictionary(Of String, Object)
        Dim PkConAlone As New Datos.ZctDataBase                         'Objeto de la base de datos
        Try
            'sObservaciones = ControladorBase.UsuarioActual.NombreCompleto & " el d�a " & Now.ToString
            PkConAlone.OpenConTran()
            PkConAlone.Inicia_Transaccion()

            Dim fechaActual = Now.Date

            If sStatus = "A" Then

                txtFolio.DataBindings.Clear()
                txtFolio.Text = OcFolio.SetConsecutivo(PkConAlone)

                sParametros.Add("@CodEnc_OT;INT", txtFolio.Text)
                'sParametros.Add("@Folio;INT", txtFolio.Text)
                sParametros.Add("@FchDoc_OT;SMALLDATETIME", fechaActual)
                sParametros.Add("@Cod_Cte;VARCHAR", CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer))
                sParametros.Add("@Cod_Mot;INT", 0)
                sParametros.Add("@Cod_Tec;INT", 0)
                sParametros.Add("@FchEnt;SMALLDATETIME", fechaActual)
                sParametros.Add("@FchSal;SMALLDATETIME", fechaActual)
                sParametros.Add("@FchEntRmp;SMALLDATETIME", fechaActual)
                sParametros.Add("@FchSalRmp;SMALLDATETIME", fechaActual)
                sParametros.Add("@ObsOT;TEXT", txtTrabajo.Text)
                sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
                sParametros.Add("@UltKm_OT;INT", 0)
                sParametros.Add("@Cod_Estatus;VARCHAR", "A")
                sParametros.Add("@HistEstatus_OT;VARCHAR", "")
                sParametros.Add("@Cod_folio;VARCHAR", ClavesFolios.OT)
                sParametros.Add("@Cod_Contacto;INT", Nothing)
                'sParametros.Add("@Cod_TpOT;INT", )

                'sParametros = txtFolio.Text & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|0|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text & "|" & sAutorizacion & "|" & sObservaciones & "|" & CboTpOrden.value
                'sParametros = "|0|A||"
                'Debug.Print(sParametros)

                GetData(2, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sParametros, PkConAlone)
                'txtFolio.Text = GetData("Procedimientos_MAX", "ZctEncOT2", PkConAlone)
            Else
                sParametros.Add("@CodEnc_OT;INT", CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
                'sParametros.Add("@Folio;INT", txtFolio.Text)
                sParametros.Add("@FchDoc_OT;SMALLDATETIME", fechaActual)
                sParametros.Add("@Cod_Cte;VARCHAR", CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer))
                sParametros.Add("@Cod_Mot;INT", 0)
                sParametros.Add("@Cod_Tec;INT", 0)
                sParametros.Add("@FchEnt;SMALLDATETIME", fechaActual)
                sParametros.Add("@FchSal;SMALLDATETIME", fechaActual)
                sParametros.Add("@FchEntRmp;SMALLDATETIME", fechaActual)
                sParametros.Add("@FchSalRmp;SMALLDATETIME", fechaActual)
                sParametros.Add("@ObsOT;TEXT", txtTrabajo.Text)
                sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
                sParametros.Add("@UltKm_OT;INT", 0)
                sParametros.Add("@Cod_Estatus;VARCHAR", sAutorizacion)
                sParametros.Add("@HistEstatus_OT;VARCHAR", sObservaciones)
                sParametros.Add("@Cod_folio;VARCHAR", ClavesFolios.OT)
                sParametros.Add("@Cod_Contacto;INT", Nothing)

                GetData(2, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sParametros, PkConAlone)
            End If

            Dim iRow As Integer

            For iRow = 0 To DGridArticulos.Rows.Count - 1
                If DGridArticulos.Item(iColCod, iRow).Value Is Nothing Or DGridArticulos.Item(iColCod, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColCod, iRow).Value = 0
                End If

                If DGridArticulos.Item(iColSol, iRow).Value Is Nothing Or DGridArticulos.Item(iColSol, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColSol, iRow).Value = 0
                End If
                If DGridArticulos.Item(iColSurt, iRow).Value Is Nothing Or DGridArticulos.Item(iColSurt, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColSurt, iRow).Value = 0
                End If

                If DGridArticulos.Item(iColTipo, iRow).Value Is Nothing OrElse DGridArticulos.Item(iColTipo, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColTipo, iRow).Value = "ART"
                End If

                'AHL 12/03/2008
                'Valida que el c�digo de art�culo no este vac�o
                If Not (DGridArticulos.Item(iColArt, iRow).Value Is Nothing) AndAlso DGridArticulos.Item(iColArt, iRow).Value <> "" Then
                    sParametros.Clear()

                    If (DGridArticulos.Item(iColTipo, iRow).Value IsNot Nothing AndAlso DGridArticulos.Item(iColTipo, iRow).Value.ToString = "MO") Then
                        DGridArticulos.Item(iColAlm, iRow).Value = "NULL"
                    Else
                        If (DGridArticulos.Item(iColAlm, iRow).Value Is Nothing OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "" OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "0") Then
                            DGridArticulos.Item(iColAlm, iRow).ErrorText = "El almac�n en la orden de trabajo no puede estar vac�o"
                            Throw New ZctReglaNegocioEx("El almac�n en la orden de compra no puede estar vac�o")
                        Else
                            DGridArticulos.Item(iColAlm, iRow).ErrorText = ""
                        End If
                    End If

                    sParametros.Add("@Cod_EncOT;INT", CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer))
                    sParametros.Add("@Cod_DetOT;INT", CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer))
                    sParametros.Add("@Cod_Art;VARCHAR", DGridArticulos.Item(iColArt, iRow).Value)
                    sParametros.Add("@Ctd_Art;INT", CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer))
                    sParametros.Add("@CosArt_DetOT;DECIMAL", CDbl(DGridArticulos.Item(iColCosto, iRow).Value))
                    sParametros.Add("@PreArt_DetOT;DECIMAL", CDbl(DGridArticulos.Item(iColPrecio, iRow).Value))
                    sParametros.Add("@CtdStd_DetOT;INT", CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer))
                    sParametros.Add("@Fch_DetOT;SMALLDATETIME", fechaActual)
                    sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
                    sParametros.Add("@Cat_Alm;INT", DGridArticulos.Item(iColAlm, iRow).Value)
                    sParametros.Add("@Cod_Mot;INT", IIf(DGridArticulos.Item(iColMot, iRow).Value Is Nothing, "", DGridArticulos.Item(iColMot, iRow).Value))
                    sParametros.Add("@Cod_TpArt;VARCHAR", DGridArticulos.Item(iColTipo, iRow).Value)
                    sParametros.Add("@cod_usu;INT", DGridArticulos.Item(iColUsu, iRow).Value)

                    'sParametros = CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer) & "|" & CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColArt, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer) & "|" & Now.Date

                    GetData(2, ProcedimientosAlmacenados.SP_DETALLE_ORDEN_TRABAJO, sParametros, PkConAlone)
                End If
            Next
            PkConAlone.Termina_Transaccion()
            PkConAlone.CloseConTran()
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdTrabajo", "A", txtFolio.Text)


        Catch ex As Exception
            PkConAlone.Cancela_Transaccion()
            PkConAlone.CloseConTran()
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click

        If valida_datos() Then
            Try
                graba_datos()
                'Status 
                If salir_en_automatico Then
                    Me.Close()
                End If
                If sStatus = "A" Then
                    If MsgBox("�Desea imprimir la orden de trabajo?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                        pImprime()
                    End If
                End If

                If MsgBox("�Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    sStatus = "A"
                    plimpia(True)
                Else
                    plimpia(False)
                    'Carga el folio nuevamente
                    CargaOrden()
                End If

            Catch ex As Exception
                MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
            End Try
        End If
    End Sub

    Private Sub txtFolio_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolio.lostfocus
        Try
            Inicia_carga_orden()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Inicia_carga_orden()
        If txtFolio.Text <> "" And CType(txtFolio.Text, Integer) > 0 Then

            'ZCTFolio.Enabled = False
            txtFolio.Enabled = False
            CargaOrden()
        End If
        'Status 
        If sStatus = "M" Then
            cmdImprimir.Visible = True
        Else
            cmdImprimir.Visible = False
        End If
    End Sub

    Private Sub pSetColor(ByVal Renglon As Integer)

        DGridArticulos.Item(iColSurt, Renglon).Value = IIf(DGridArticulos.Item(iColSurt, Renglon).Value Is Nothing, 0, DGridArticulos.Item(iColSurt, Renglon).Value)
        DGridArticulos.Item(iColSol, Renglon).Value = IIf(DGridArticulos.Item(iColSol, Renglon).Value Is Nothing, 0, DGridArticulos.Item(iColSol, Renglon).Value)
        DGridArticulos.Item(iColSol, Renglon).Value = IIf(DGridArticulos.Item(iColSol, Renglon).Value.ToString = "", 0, DGridArticulos.Item(iColSol, Renglon).Value)
        DGridArticulos.Item(iColSurt, Renglon).Value = IIf(DGridArticulos.Item(iColSurt, Renglon).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, Renglon).Value)

        If CType(DGridArticulos.Item(iColSol, Renglon).Value, Integer) = surtidoTMP OrElse (DGridArticulos.Item(iColTipo, Renglon).Value IsNot Nothing AndAlso DGridArticulos.Item(iColTipo, Renglon).Value IsNot DBNull.Value AndAlso DGridArticulos.Item(iColTipo, Renglon).Value = "MO") Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblSurtido.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblSurtido.BackColor
        ElseIf CType(surtidoTMP, Integer) < CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblPorSurtir.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblPorSurtir.BackColor
        ElseIf CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) < CType(surtidoTMP, Integer) Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblDevolucion.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblDevolucion.BackColor

        End If
    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        pImprime()
    End Sub

    Private Sub pImprime()
        If txtFolio.Text = "0" Or txtFolio.Text = "" Then Exit Sub
        Dim myCr As New PkVisorRpt
        myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        myCr.sSQLV = "{ZctEncOT.CodEnc_OT} = " & CType(txtFolio.Text, Integer)

        myCr.sRpt = sPathRpt
        myCr.Show()
    End Sub


    'Private Sub txtVin_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If txtVin.Text = "" Or txtVin.Text = "0" Then Exit Sub
    '    Dim tDataTemp As New DataTable

    '    tDataTemp = GetData(1, "SP_ZctCatMotXVin", "@Vin_Mot;VARCHAR", txtVin.Text)
    '    If tDataTemp Is Nothing Then Exit Sub
    '    If tDataTemp.Rows.Count > 0 Then
    '        txtVin.Text = tDataTemp.Rows(0).Item(0)
    '    End If
    '    txtVin.Validar = True
    '    txtVin.pCargaDescripcion()
    '    txtVin.Validar = False

    '    tDataTemp.Dispose()
    'End Sub

    Private Sub CargaOrden()

        Dim drEncOT As DataTable
        'Dim sVariables As String
        Dim sParametros As New Dictionary(Of String, Object)

        Dim fechaActual = Now.Date

        sParametros.Add("@CodEnc_OT;INT", CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
        'sParametros.Add("@Folio;INT", txtFolio.Text)
        sParametros.Add("@Cod_folio;VARCHAR", ClavesFolios.OT)
        sParametros.Add("@FchDoc_OT;SMALLDATETIME", fechaActual)
        sParametros.Add("@Cod_Cte;VARCHAR", CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer))
        sParametros.Add("@Cod_Mot;INT", 0)
        sParametros.Add("@Cod_Tec;INT", 0)
        sParametros.Add("@FchEnt;SMALLDATETIME", fechaActual)
        sParametros.Add("@FchSal;SMALLDATETIME", fechaActual)
        sParametros.Add("@FchEntRmp;SMALLDATETIME", fechaActual)
        sParametros.Add("@FchSalRmp;SMALLDATETIME", fechaActual)
        sParametros.Add("@ObsOT;TEXT", txtTrabajo.Text)
        sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
        sParametros.Add("@UltKm_OT;INT", 0)
        sParametros.Add("@Cod_Estatus;VARCHAR", sAutorizacion)
        sParametros.Add("@HistEstatus_OT;VARCHAR", sObservaciones)
        sParametros.Add("@Cod_Contacto;INT", Nothing)
        'sParametros.Add("@Cod_TpOT;INT", )

        drEncOT = GetData(1, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sParametros)
        If Not (drEncOT.Rows.Count = 0) Then
            txtFolio.DataBindings.Clear()
            txtFolio.DataBindings.Add("Text", drEncOT, "CodEnc_OT")

            'Obtiene la autorizaci�n
            sAutorizacion = drEncOT.Rows(0).Item("Cod_Estatus").ToString
            sObservaciones = drEncOT.Rows(0).Item("HistEstatus_OT").ToString
            'chkAprobada.Checked = False
            Select Case sAutorizacion
                Case "A"
                    'ZctHistAprov.Text = "Aprobada"
                    'ZctHistAprov.BackColor = lblSurtido.BackColor
                    'chkAprobada.Checked = True

                    DGridArticulos.Enabled = True
                    cmdAceptar.Enabled = _Guardar
                    cmdPagar.Enabled = _Guardar
                    'chkAprobada.Enabled = False
                    cmdCancelaOrden.Enabled = True
                    cmdImprimir.Enabled = True

                Case "C"
                    'ZctHistAprov.Text = "Cancelada"
                    'ZctHistAprov.BackColor = lblDevolucion.BackColor

                    DGridArticulos.Enabled = False
                    cmdAceptar.Enabled = _Guardar
                    cmdPagar.Enabled = False
                    'chkAprobada.Enabled = False
                    cmdCancelaOrden.Enabled = False
                    cmdImprimir.Enabled = False
                Case "N"
                    'ZctHistAprov.Text = "Pendiente de aprobar"
                    'ZctHistAprov.BackColor = lblPorSurtir.BackColor

                    DGridArticulos.Enabled = True
                    cmdAceptar.Enabled = _Guardar
                    cmdPagar.Enabled = _Guardar
                    'chkAprobada.Enabled = True
                    cmdCancelaOrden.Enabled = True
                    cmdImprimir.Enabled = True
            End Select

            Establece_Status(txtFolio.Text)
            'If sObservaciones <> "" Then ZctHistAprov.Text &= " por : " & sObservaciones

            txtCliente.DataBindings.Clear()
            txtCliente.DataBindings.Add("Text", drEncOT, "Cod_Cte")
            txtCliente.pCargaDescripcion()

            'txtTecnico.DataBindings.Clear()
            'txtTecnico.DataBindings.Add("Text", drEncOT, "Cod_Tec")
            'txtTecnico.pCargaDescripcion()

            'DTEntrada.DataBindings.Clear()
            'DTEntrada.DataBindings.Add("Value", drEncOT, "FchEnt")

            'DTSalida.DataBindings.Clear()
            'DTSalida.DataBindings.Add("Value", drEncOT, "FchSal")

            'CboTpOrden.DataBindings.Clear()
            'CboTpOrden.DataBindings.Add("ValueItem", drEncOT, "Cod_TpOT", True, DataSourceUpdateMode.OnPropertyChanged, 0)

            'txtKm.DataBindings.Clear()
            'txtKm.DataBindings.Add("Text", drEncOT, "UltKm_OT")


            'DTRmpEntrada.DataBindings.Clear()
            'DTRmpEntrada.DataBindings.Add("Value", drEncOT, "FchEntRmp")

            'DTRmpSalida.DataBindings.Clear()
            'DTRmpSalida.DataBindings.Add("Value", drEncOT, "FchSalRmp")

            txtTrabajo.DataBindings.Clear()
            txtTrabajo.DataBindings.Add("Text", drEncOT, "ObsOT")

            'DtAplicacion.DataBindings.Clear()
            'DtAplicacion.DataBindings.Add("Value", drEncOT, "FchAplMov")

            'Datos del Grid
            sParametros.Clear()
            
            sParametros.Add("@Cod_EncOT;INT", CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
            sParametros.Add("@Cod_DetOT;INT", 0)
            sParametros.Add("@Cod_Art;VARCHAR", "")
            sParametros.Add("@Ctd_Art;INT", 0)
            sParametros.Add("@CosArt_DetOT;DECIMAL", 0)
            sParametros.Add("@PreArt_DetOT;DECIMAL", 0)
            sParametros.Add("@CtdStd_DetOT;INT", 0)
            sParametros.Add("@Fch_DetOT;SMALLDATETIME", fechaActual)
            sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
            sParametros.Add("@Cat_Alm;INT", 0)
            sParametros.Add("@Cod_Mot;INT", 0)
            sParametros.Add("@Cod_TpArt;VARCHAR", 0)
            sParametros.Add("@cod_usu;INT", 0)

            Dim drDetOT As DataTable
            Dim iRowD As Integer

            drDetOT = GetData(1, ProcedimientosAlmacenados.SP_DETALLE_ORDEN_TRABAJO, sParametros)

            For iRowD = 0 To drDetOT.Rows.Count - 1
                Dim inRow As New DataGridViewRow

                inRow.CreateCells(DGridArticulos)
                inRow.Cells(iColArt).Value = drDetOT.Rows(iRowD).Item("Cod_Art").ToString
                inRow.Cells(iColDesc).Value = drDetOT.Rows(iRowD).Item("Desc_Art").ToString
                inRow.Cells(iColSol).Value = drDetOT.Rows(iRowD).Item("Ctd_Art").ToString
                inRow.Cells(iColAlm).Value = drDetOT.Rows(iRowD).Item("Cat_Alm")
                inRow.Cells(iColTipo).Value = drDetOT.Rows(iRowD).Item("Cod_TpArt")
                'inRow.Cells(iColExist).Value = drDetOT.Rows(iRowD).Item("Exist_Art").ToString
                inRow.Cells(iColCod).Value = drDetOT.Rows(iRowD).Item("Cod_DetOT").ToString
                inRow.Cells(iColSurt).Value = drDetOT.Rows(iRowD).Item("CtdStd_DetOT").ToString
                inRow.Cells(iColPrecio).Value = drDetOT.Rows(iRowD).Item("PreArt_DetOT").ToString
                inRow.Cells(iColCosto).Value = FormatCurrency(drDetOT.Rows(iRowD).Item("CosArt_DetOT").ToString)
                CalculaExistencia(inRow, False)
                Dim surtido As Integer = 0
                If (Not IsNothing(inRow.Cells(iColSurt).Value) AndAlso Not String.IsNullOrEmpty(inRow.Cells(iColSurt).Value)) Then surtido = Integer.Parse(inRow.Cells(iColSurt).Value)
                inRow.Cells(iColSubTotal).Value = FormatCurrency(surtido * inRow.Cells(iColPrecio).Value)
                inRow.Cells(iColMot).Value = drDetOT.Rows(iRowD).Item("Cod_Mot").ToString
                inRow.Cells(iColUsu).Value = drDetOT.Rows(iRowD).Item("cod_usu").ToString


                'txtVin.DataBindings.Clear()
                'txtVin.DataBindings.Add("Text", drDetOT, "Cod_Mot")
                'txtVin.pCargaDescripcion()
                DGridArticulos.Rows.Add(inRow)
            Next
            DGridArticulos.Columns(iColCod).Visible = False

            For iRowD = 0 To drDetOT.Rows.Count - 1
                pSetColor(iRowD)
            Next

            drDetOT.Dispose()
            drEncOT.Dispose()
            sGetTotal()
            'Estatus de modificaci�n
            sStatus = "M"

        End If

    End Sub


    Private Sub DGridArticulos_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles DGridArticulos.UserDeletingRow
        If sStatus = "M" Then
            MsgBox("Esta intentando eliminar un movimiento ya almacenado, por favor ponga en ceros la cantidad surtida si este movimiento esta erroneo.", MsgBoxStyle.Information)
            e.Cancel = True
        End If
    End Sub

    Private Sub DGridArticulos_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles DGridArticulos.DataError
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).ErrorText = e.Exception.Message
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).Selected = False
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).Value = DGridArticulos.Item(e.ColumnIndex, e.RowIndex).DefaultNewRowValue
    End Sub

    Private Sub ValidaExistencia(ByRef Renglon As DataGridViewRow)
        If ZctSOTMain.Inventario_en_marcha(Renglon.Cells(iColAlm).Value) Then
            Renglon.ErrorText = "El almac�n seleccionado esta corriendo inventario y no puede ser utilizado en este momento."
            Renglon.ReadOnly = True
            'cmdAceptar.Enabled = False
            cmdCancelaOrden.Enabled = False
        End If
    End Sub


    Private Sub CalculaExistencia(ByVal Renglon As Integer, Optional ByVal GetCosto As Boolean = True)
        If DGridArticulos.Item(iColArt, Renglon).Value = "" OrElse (DGridArticulos.Item(iColAlm, Renglon).Value Is Nothing OrElse DGridArticulos.Item(iColAlm, Renglon).Value.ToString = "") Then DGridArticulos.Item(iColExist, Renglon).Value = "0" : Exit Sub
        ValidaExistencia(DGridArticulos.Rows(Renglon))
        Dim dt As DataTable = Nothing
        Try

            dt = ClassGen.GenGetData("SP_ZctArtXAlm", "@TpConsulta;int|Cod_Art;VarChar|Cod_Alm;Int|Exist_Art;Int|CostoProm_Art;Money", "1|" & DGridArticulos.Item(iColArt, Renglon).Value & "|" & DGridArticulos.Item(iColAlm, Renglon).Value & "|0|0")
            If dt.Rows.Count = 0 Then
                DGridArticulos.Item(iColExist, Renglon).Value = "0"
            Else
                DGridArticulos.Item(iColExist, Renglon).Value = ZctFunciones.GetGeneric(dt.Rows(0)("Exist_Art"))
            End If
            If GetCosto Then
                If DGridArticulos.Item(iColExist, Renglon).Value <> "0" Then
                    DGridArticulos.Item(iColCosto, Renglon).Value = FormatCurrency((ZctFunciones.GetGeneric(Of Decimal)(dt.Rows(0)("CostoProm_Art")) / DGridArticulos.Item(iColExist, Renglon).Value).ToString)
                Else
                    DGridArticulos.Item(iColCosto, Renglon).Value = FormatCurrency("0")
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            dt.Dispose()
        End Try


    End Sub

    Private Sub CalculaExistencia(ByRef Renglon As DataGridViewRow, Optional ByVal GetCosto As Boolean = True)
        If Renglon.Cells(iColArt).Value = "" OrElse (Renglon.Cells(iColAlm).Value Is Nothing OrElse Renglon.Cells(iColAlm).Value.ToString = "") Then Renglon.Cells(iColExist).Value = "0" : Exit Sub
        ValidaExistencia(Renglon)
        Dim dt As DataTable = Nothing
        Try

            dt = ClassGen.GenGetData("SP_ZctArtXAlm", "@TpConsulta;int|Cod_Art;VarChar|Cod_Alm;Int|Exist_Art;Int|CostoProm_Art;Money", "1|" & Renglon.Cells(iColArt).Value & "|" & Renglon.Cells(iColAlm).Value & "|0|0")
            If dt.Rows.Count = 0 Then
                Renglon.Cells(iColExist).Value = "0"
            Else
                Renglon.Cells(iColExist).Value = ZctFunciones.GetGeneric(dt.Rows(0)("Exist_Art"))
            End If
            If GetCosto Then
                If Renglon.Cells(iColExist).Value <> "0" Then
                    Renglon.Cells(iColCosto).Value = FormatCurrency((ZctFunciones.GetGeneric(Of Decimal)(dt.Rows(0)("CostoProm_Art")) / Renglon.Cells(iColExist).Value).ToString)
                Else
                    Renglon.Cells(iColCosto).Value = FormatCurrency("0")
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            dt.Dispose()
        End Try


    End Sub


    Private Sub txtCliente_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.lostfocus
        'txtVin.SPParametros = "0||" & txtCliente.Text & "|0|0|||0"
        'txtVin.Text = ""

    End Sub

    Private Sub txtCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.Load

    End Sub

    Private Sub cmdIzq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIzq.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) - 1 > 0 Then
            txtFolio.Text = (CInt(txtFolio.Text) - 1).ToString
            plimpia(False)
            CargaOrden()
            'Status 
            If sStatus = "M" And CInt(txtFolio.Text) < OcFolio.Consecutivo Then ' GetData("Procedimientos_MAX", "ZctEncOT") Then
                cmdImprimir.Visible = True
            Else
                'chkAprobada.Checked = False
                sStatus = "A"
                cmdImprimir.Visible = False
                'DtAplicacion.ClearValue()
                'DTEntrada.ClearValue()

                ' DTSalida.ClearValue()
            End If
        End If

    End Sub

    Private Sub cmdDer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDer.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) + 1 <= OcFolio.Consecutivo Then ' GetData("Procedimientos_MAX", "ZctEncOT") Then
            txtFolio.Text = (CInt(txtFolio.Text) + 1).ToString
            plimpia(False)
            CargaOrden()
            'Status 
            If sStatus = "M" And CInt(txtFolio.Text) < OcFolio.Consecutivo Then 'GetData("Procedimientos_MAX", "ZctEncOT") Then
                cmdImprimir.Visible = True
            Else
                'chkAprobada.Checked = False
                sStatus = "A"
                cmdImprimir.Visible = False
                'DtAplicacion.ClearValue()
                'DTEntrada.ClearValue()
                'DTSalida.ClearValue()
            End If
        End If
    End Sub

    Private Sub ZctSOTGroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdCancelaOrden_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelaOrden.Click
        'Dim sVariables As String
        Dim sParametros As New Dictionary(Of String, Object)
        Dim PkConAlone As New Datos.ZctDataBase                         'Objeto de la base de datos

        Dim fechaActual = Now.Date

        If sStatus = "A" Then
            MsgBox("Para poder cancelar la orden primero tiene que ser grabada.")
            Exit Sub
        End If
        Try
            If MsgBox("�Desea cancelar esta orden de trabajo?, se har�n devoluciones de los art�culos y ya no se podr� utilizar este folio.", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
            'Dim fLogin As New ZctLogin
            ''Autorizaci�n para surtir sin existencia
            'fLogin.Cod_Aut = 5
            'fLogin.ShowDialog(Me)
            Dim controlador = New SOTControladores.Controladores.ControladorPermisos()
            If controlador.VerificarPermisos(New Modelo.Seguridad.Dtos.DtoPermisos With {.EliminarOrdenTrabajo = True}, True) = False Then
                MsgBox("Usted no tiene permisos para cancelar las ordenes de trabajo.")
                Exit Sub
            Else
                sAutorizacion = "C"
                sObservaciones = ControladorBase.UsuarioActual.Id & " el d�a " & Now.ToString
            End If
            PkConAlone.OpenConTran()
            PkConAlone.Inicia_Transaccion()
            
            sParametros.Add("@CodEnc_OT;INT", CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
            'sParametros.Add("@Folio;INT", txtFolio.Text)
            sParametros.Add("@FchDoc_OT;SMALLDATETIME", fechaActual)
            sParametros.Add("@Cod_Cte;VARCHAR", CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer))
            sParametros.Add("@Cod_Mot;INT", 0)
            sParametros.Add("@Cod_Tec;INT", 0)
            sParametros.Add("@FchEnt;SMALLDATETIME", fechaActual)
            sParametros.Add("@FchSal;SMALLDATETIME", fechaActual)
            sParametros.Add("@FchEntRmp;SMALLDATETIME", fechaActual)
            sParametros.Add("@FchSalRmp;SMALLDATETIME", fechaActual)
            sParametros.Add("@ObsOT;TEXT", txtTrabajo.Text)
            sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
            sParametros.Add("@UltKm_OT;INT", 0)
            sParametros.Add("@Cod_Estatus;VARCHAR", sAutorizacion)
            sParametros.Add("@HistEstatus_OT;VARCHAR", sObservaciones)
            sParametros.Add("@Cod_folio;VARCHAR", ClavesFolios.OT)
            sParametros.Add("@Cod_Contacto;INT", Nothing)

            GetData(2, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sParametros, PkConAlone)

            Dim iRow As Integer

            For iRow = 0 To DGridArticulos.Rows.Count - 1
                If DGridArticulos.Item(iColCod, iRow).Value Is Nothing Or DGridArticulos.Item(iColCod, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColCod, iRow).Value = 0
                End If

                If DGridArticulos.Item(iColSol, iRow).Value Is Nothing Or DGridArticulos.Item(iColSol, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColSol, iRow).Value = 0
                End If
                DGridArticulos.Item(iColSurt, iRow).Value = 0

                If DGridArticulos.Item(iColTipo, iRow).Value Is Nothing Or DGridArticulos.Item(iColTipo, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColTipo, iRow).Value = 0
                End If

                'AHL 12/03/2008
                'Valida que el c�digo de art�culo no este vac�o
                If Not (DGridArticulos.Item(iColArt, iRow).Value Is Nothing) AndAlso DGridArticulos.Item(iColArt, iRow).Value <> "" Then
                    sParametros.Clear() '= ""

                    If DGridArticulos.Item(iColAlm, iRow).Value Is Nothing OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "" OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "0" Then
                        DGridArticulos.Item(iColAlm, iRow).ErrorText = "El almac�n en la orden de trabajo no puede estar vacia"
                        Throw New ZctReglaNegocioEx("El almac�n en la orden de compra no puede estar vacia")
                    Else
                        DGridArticulos.Item(iColAlm, iRow).ErrorText = ""
                    End If
                    
                    sParametros.Add("@Cod_EncOT;INT", CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer))
                    sParametros.Add("@Cod_DetOT;INT", CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer))
                    sParametros.Add("@Cod_Art;VARCHAR", DGridArticulos.Item(iColArt, iRow).Value)
                    sParametros.Add("@Ctd_Art;INT", CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer))
                    sParametros.Add("@CosArt_DetOT;DECIMAL", CDbl(DGridArticulos.Item(iColCosto, iRow).Value))
                    sParametros.Add("@PreArt_DetOT;DECIMAL", DGridArticulos.Item(iColPrecio, iRow).Value)
                    sParametros.Add("@CtdStd_DetOT;INT", CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer))
                    sParametros.Add("@Fch_DetOT;SMALLDATETIME", fechaActual)
                    sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
                    sParametros.Add("@Cat_Alm;INT", DGridArticulos.Item(iColAlm, iRow).Value)
                    sParametros.Add("@Cod_Mot;INT", DGridArticulos.Item(iColMot, iRow).Value)
                    sParametros.Add("@Cod_TpArt;VARCHAR", DGridArticulos.Item(iColTipo, iRow).Value)
                    sParametros.Add("@cod_usu;INT", DGridArticulos.Item(iColUsu, iRow).Value)

                    'sParametros = CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer) & "|" & CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColArt, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer) & "|" & Now.Date
    
                    GetData(2, ProcedimientosAlmacenados.SP_DETALLE_ORDEN_TRABAJO, sParametros, PkConAlone)
                End If
            Next
            PkConAlone.Termina_Transaccion()
            PkConAlone.CloseConTran()
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdTrabajo", "C", txtFolio.Text)


            If MsgBox("�Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                sStatus = "A"
                plimpia(True)
            Else
                plimpia(False)
                'Carga el folio nuevamente
                CargaOrden()
            End If

        Catch ex As Exception
            PkConAlone.Cancela_Transaccion()
            PkConAlone.CloseConTran()
            MsgBox("Ha ocurrido un error al momento de estar cancelando la orden de trabajo , comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub DtAplicacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ZctGroupControls7_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ZctSOTGroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZctSOTGroupBox1.Enter

    End Sub

    'Private Function ZctHistAprov() As Object
    '    Throw New NotImplementedException
    'End Function

    Private Sub Estatus(pagado As Boolean, facturado As Boolean, status As String)
        If (status = "A") Then
            If pagado And Not facturado Then
                lblEstatus.Text = "Pagado sin facturar"
                lblEstatus.BackColor = Color.FromArgb(128, 255, 128)
            ElseIf facturado Then
                lblEstatus.Text = "Pagado, Facturado"
                lblEstatus.BackColor = Color.FromArgb(255, 255, 128)
            Else
                lblEstatus.Text = "Sin Pagar"
                lblEstatus.BackColor = Color.FromArgb(255, 128, 128)
            End If
        Else
            lblEstatus.Text = "Sin Guardar"
            lblEstatus.BackColor = Color.Silver
        End If
    End Sub

    Private Sub Establece_Status(folio As String)
        Dim controlador As New SOTControladores.Controladores.ControladorOrdenesTrabajo 'ResTotal.Controlador.ControladorOT(Datos.DAO.Conexion.CadenaConexion)

        Dim statusOT = controlador.ObtenerStatusOrdenTrabajo(folio)

        lblEstatus.Text = getStatusOT(statusOT)
        lblEstatus.BackColor = getColorOT(statusOT)
        DGridArticulos.Enabled = controlador.PermiteEditarDetalles(folio)
        cmdAceptar.Enabled = controlador.PermiteGrabar(folio) And _Permisos.CrearOrdenTrabajo
        cmdPagar.Enabled = controlador.PermiteGrabar(folio) And _Permisos.CrearOrdenTrabajo
        cmdCancelaOrden.Enabled = controlador.PermiteGrabar(folio) And _Permisos.ModificarOrdenTrabajo
    End Sub

    Public Function getStatusOT(ot As Modelo.Almacen.Entidades.VW_ZctStatusOT) As String
        If (IsNothing(ot)) Then
            Return "Por grabar"
        ElseIf (ot.factura = 1 And ot.caja = 1) Then
            Return "Facturado y pagado"
        ElseIf (ot.factura = 1 And ot.caja = 0) Then
            Return "Facturado sin pagar"
        ElseIf (ot.factura = 0 And ot.caja = 0) Then
            Return "Sin facturar, sin pagar"
        ElseIf (ot.factura = 0 And ot.caja = 1) Then
            Return "Sin facturar, pagado"
        Else
            Return "No definido"
        End If
    End Function

    Public Function getColorOT(ot As Modelo.Almacen.Entidades.VW_ZctStatusOT) As System.Drawing.Color
        If (IsNothing(ot)) Then
            Return Color.Silver
        ElseIf (ot.factura = 1 And ot.caja = 1) Then
            Return Color.FromArgb(128, 255, 128)
        ElseIf (ot.factura = 1 And ot.caja = 0) Then
            Return Color.FromArgb(255, 255, 128)
        ElseIf (ot.factura = 0 And ot.caja = 0) Then
            Return Color.FromArgb(255, 128, 128)
        ElseIf (ot.factura = 0 And ot.caja = 1) Then
            Return Color.FromArgb(255, 255, 128)
        Else
            Return Color.IndianRed
        End If
    End Function

    Private Sub cmdPagar_Click(sender As System.Object, e As System.EventArgs) Handles cmdPagar.Click
        Try
            graba_datos()
            ZctSOTMain.CreaCaja(txtFolio.Text)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DGridArticulos_SelectionChanged(sender As Object, e As System.EventArgs) Handles DGridArticulos.SelectionChanged

        If (IsNothing(DGridArticulos.CurrentRow.Cells(iColSurt).Value) OrElse Not Integer.TryParse(DGridArticulos.CurrentRow.Cells(iColSurt).Value.ToString(), surtidoTMP)) Then
            surtidoTMP = 0
            DGridArticulos.CurrentRow.Cells(iColSurt).Value = 0
        End If
    End Sub
End Class


