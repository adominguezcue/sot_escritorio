'Importa la clase de la base de datos
Imports ZctSOT.Datos.ZctDataBase
Imports ZctSOT
Imports SOTControladores.Controladores

Public Class ZctLogin
    Dim iCont As Integer = 0
    Public Valida As Boolean = False
    'Public Cod_Aut As Integer
    Dim cClas As New ZctSOT.Datos.ClassGen

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        'EjecutarViejoLogin()
        Try
            Dim credenciales = New Modelo.Seguridad.Dtos.DtoCredencial
            credenciales.IdentificadorUsuario = UsernameTextBox.Text
            credenciales.TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena
            credenciales.ValorVerificarStr = PasswordTextBox.Text
            credenciales.IdentificadorUsuario = UsernameTextBox.Text

            Dim controladorSesiones = New ControladorSesiones()
            controladorSesiones.IniciarSesion(credenciales)

            Valida = True

        Catch ex As Exception
            Valida = False
            iCont += 1
            UsernameTextBox.Text = ""
            PasswordTextBox.Text = ""
            UsernameTextBox.Focus()

            Throw ex
        Finally
            If Valida Or iCont >= 3 Then
                Me.Close()
            End If
        End Try
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByRef parametros As Dictionary(Of String, Object)) As DataTable
        Dim PkBusCon As New ZctSOT.Datos.ZctDataBase                         'Objeto de la base de datos
        Try
            'Inicia el procedimieto almacenado
            PkBusCon.IniciaProcedimiento(sSpNAME)
            'Agrega el parametro que define que se va a realizar
            PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
            'Obtiene el total de los procedimientos
            'Recorre las variables del procedimiento almacenado
            For Each iSp As String In parametros.Keys
                Select Case parametros(iSp).GetType() 'Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                    Case GetType(Integer)
                        PkBusCon.AddParameterSP(iSp, parametros(iSp), SqlDbType.Int)
                    Case GetType(String)
                        PkBusCon.AddParameterSP(iSp, parametros(iSp), SqlDbType.VarChar)
                    Case GetType(DateTime)
                        PkBusCon.AddParameterSP(iSp, parametros(iSp), SqlDbType.SmallDateTime)
                    Case GetType(Decimal)
                        PkBusCon.AddParameterSP(iSp, parametros(iSp), SqlDbType.Decimal)
                        'Case GetType(Integer)
                        '    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)
                End Select
            Next

            Select Case TpConsulta
                'SELECT
                Case 1
                    PkBusCon.InciaDataAdapter()
                    Return PkBusCon.GetReaderSP.Tables("DATOS")
                Case 2
                    'Ejecuta el escalar
                    PkBusCon.GetScalarSP()
                    Return Nothing

                Case 3
                    'Delete
                    PkBusCon.GetScalarSP()
                    Return Nothing
                Case Else
                    Return Nothing
            End Select

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return Nothing
        Finally
            PkBusCon.CloseConTran()

        End Try
    End Function

    Private Sub ZctLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'txtServidor.Text =
        ZctSOT.Datos.DAO.Conexion.GetActualServer()
        'TxtDB.Text = ZctSOT.Datos.DAO.Conexion.Database
        'Dim PkBusCon As New ZctDataBase                         'Objeto de la base de datos
        'If PkBusCon.VerificaArch = False Then MsgBox("La instalación de su sistema no ha sido autorizada, comuniquese con sistemas") : Me.Dispose()
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    'Private Sub EjecutarViejoLogin()
    '    'Dim sVariables As String

    '    'Dim sParametros As String
    '    Dim DtUsu As DataTable
    '    'ZctSOT.Datos.DAO.Conexion.SetActualServer(txtServidor.Text)
    '    'ZctSOT.Datos.DAO.Conexion.SetActualBD(TxtDB.Text)
    '    'verifica que exista el usuario
    '    'sVariables = ";INT|;VARCHAR|;VARCHAR|;INT"
    '    PasswordTextBox.Text = PasswordTextBox.Text.Replace("|", "")
    '    UsernameTextBox.Text = UsernameTextBox.Text.Replace("|", "")

    '    'sParametros = "|" &  & "|" & & "|" & 

    '    Dim parametros As New Dictionary(Of String, Object)

    '    parametros.Add("@Cod_Usu", 0)
    '    parametros.Add("@Nom_Usu", UsernameTextBox.Text)
    '    parametros.Add("@Pass_Usu", PasswordTextBox.Text)
    '    parametros.Add("@Cod_Aut", Cod_Aut)

    '    DtUsu = GetData(1, "SP_ZctSegUsu", parametros)
    '    If DtUsu IsNot Nothing AndAlso DtUsu.Rows.Count > 0 Then
    '        'si existe graba el movimiento
    '        RibbonMain._NomUsuAct = UsernameTextBox.Text

    '        If Cod_Aut = 0 Then
    '            cClas.GrabaUsuario(CType(DtUsu.Rows(0).Item("Cod_Usu").ToString, Integer), "ZctLogin", "E", "")
    '            RibbonMain._UsuAct = DtUsu.Rows(0).Item("Cod_Usu").ToString
    '            RibbonMain._EsVendedor = DtUsu.Rows(0).Item("Vendedor").ToString
    '        Else
    '            RibbonMain._UsuAut = UsernameTextBox.Text
    '            cClas.GrabaUsuario(CType(DtUsu.Rows(0).Item("Cod_Usu").ToString, Integer), "ZctLogin", "A", Cod_Aut.ToString)
    '        End If
    '        ZctSOTMain._NomUsuAct = RibbonMain._NomUsuAct
    '        ZctSOTMain._UsuAct = RibbonMain._UsuAct
    '        ZctSOTMain._EsVendedor = RibbonMain._EsVendedor
    '        ZctSOTMain._UsuAut = RibbonMain._UsuAut

    '        Valida = True

    '    Else
    '        MsgBox("El usuario es incorrecto, verifique por favor")
    '        Valida = False
    '        iCont += 1
    '        UsernameTextBox.Text = ""
    '        PasswordTextBox.Text = ""
    '        UsernameTextBox.Focus()
    '        'caso contrario envia un false
    '    End If
    '    If Valida = True Or iCont >= 3 Then
    '        Me.Dispose()
    '    End If
    'End Sub

End Class
