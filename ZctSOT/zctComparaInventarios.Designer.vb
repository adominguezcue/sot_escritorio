<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctComparaInventarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelPrinc = New System.Windows.Forms.Panel()
        Me.estatus = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.NumConteoCaptura = New System.Windows.Forms.NumericUpDown()
        Me.txtCapturaArticulos = New System.Windows.Forms.TextBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.OFD = New System.Windows.Forms.OpenFileDialog()
        Me.Ajustat = New System.Windows.Forms.CheckBox()
        Me.Artajus = New ZctSOT.ZctSOTLabelDesc()
        Me.cmdEliminar = New ZctSOT.ZctSOTButton()
        Me.cmdImprimir = New ZctSOT.ZctSOTButton()
        Me.lblFaltan = New ZctSOT.ZctSOTLabelDesc()
        Me.lblCorrecto = New ZctSOT.ZctSOTLabelDesc()
        Me.lblSobran = New ZctSOT.ZctSOTLabelDesc()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.GridInventario = New ZctSOT.ZctSotGrid()
        Me.ZctSOTLabel3 = New ZctSOT.ZctSOTLabel()
        Me.CmdGeneraInventario = New ZctSOT.ZctSOTButton()
        Me.cmdTerminar = New ZctSOT.ZctSOTButton()
        Me.cmdAjustes = New ZctSOT.ZctSOTButton()
        Me.grpConteo = New ZctSOT.ZctSOTGroupBox()
        Me.ZctSOTLabel2 = New ZctSOT.ZctSOTLabel()
        Me.NumConteo = New System.Windows.Forms.NumericUpDown()
        Me.ZctSOTComboBox1 = New ZctSOT.ZctSOTComboBox()
        Me.cmdGetExcel = New ZctSOT.ZctSOTButton()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel()
        Me.txtArchivoExcel = New ZctSOT.ZctSOTTextBox()
        Me.GrpCaracteristicas = New ZctSOT.ZctGroupControls()
        Me.CboLinea = New ZctSOT.ZctControlCombo()
        Me.chkInvAle = New System.Windows.Forms.CheckBox()
        Me.cboDpto = New ZctSOT.ZctControlCombo()
        Me.chkInvGeneral = New System.Windows.Forms.CheckBox()
        Me.cboAlmacen = New ZctSOT.ZctControlCombo()
        Me.txtFolio = New ZctSOT.ZctControlTexto()
        Me.PanelPrinc.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumConteoCaptura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridInventario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpConteo.SuspendLayout()
        CType(Me.NumConteo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpCaracteristicas.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelPrinc
        '
        Me.PanelPrinc.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PanelPrinc.Controls.Add(Me.estatus)
        Me.PanelPrinc.Controls.Add(Me.Button2)
        Me.PanelPrinc.Controls.Add(Me.Button1)
        Me.PanelPrinc.Controls.Add(Me.GroupBox1)
        Me.PanelPrinc.Controls.Add(Me.CmdGeneraInventario)
        Me.PanelPrinc.Controls.Add(Me.cmdTerminar)
        Me.PanelPrinc.Controls.Add(Me.cmdAjustes)
        Me.PanelPrinc.Controls.Add(Me.grpConteo)
        Me.PanelPrinc.Controls.Add(Me.GrpCaracteristicas)
        Me.PanelPrinc.Controls.Add(Me.txtFolio)
        Me.PanelPrinc.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelPrinc.Location = New System.Drawing.Point(0, 0)
        Me.PanelPrinc.Name = "PanelPrinc"
        Me.PanelPrinc.Size = New System.Drawing.Size(1068, 229)
        Me.PanelPrinc.TabIndex = 0
        '
        'estatus
        '
        Me.estatus.BackColor = System.Drawing.Color.Gold
        Me.estatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(254, Byte))
        Me.estatus.Location = New System.Drawing.Point(766, 8)
        Me.estatus.Name = "estatus"
        Me.estatus.Size = New System.Drawing.Size(134, 23)
        Me.estatus.TabIndex = 17
        Me.estatus.Text = "Alta de Inventario"
        Me.estatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(152, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(22, 24)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = ">"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(128, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(22, 24)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "<"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ZctSOTLabel3)
        Me.GroupBox1.Controls.Add(Me.NumConteoCaptura)
        Me.GroupBox1.Controls.Add(Me.txtCapturaArticulos)
        Me.GroupBox1.Controls.Add(Me.label3)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 121)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(903, 44)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Captura con lectora láser"
        '
        'NumConteoCaptura
        '
        Me.NumConteoCaptura.Location = New System.Drawing.Point(616, 18)
        Me.NumConteoCaptura.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumConteoCaptura.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumConteoCaptura.Name = "NumConteoCaptura"
        Me.NumConteoCaptura.Size = New System.Drawing.Size(54, 20)
        Me.NumConteoCaptura.TabIndex = 18
        Me.NumConteoCaptura.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtCapturaArticulos
        '
        Me.txtCapturaArticulos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCapturaArticulos.Location = New System.Drawing.Point(62, 18)
        Me.txtCapturaArticulos.Name = "txtCapturaArticulos"
        Me.txtCapturaArticulos.Size = New System.Drawing.Size(490, 20)
        Me.txtCapturaArticulos.TabIndex = 16
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(12, 21)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(47, 13)
        Me.label3.TabIndex = 17
        Me.label3.Text = "Artículo:"
        '
        'OFD
        '
        Me.OFD.Filter = "Archivos Excel (*.xls)|*.xls"
        '
        'Ajustat
        '
        Me.Ajustat.AutoSize = True
        Me.Ajustat.Location = New System.Drawing.Point(848, 231)
        Me.Ajustat.Name = "Ajustat"
        Me.Ajustat.Size = New System.Drawing.Size(83, 17)
        Me.Ajustat.TabIndex = 14
        Me.Ajustat.Text = "Ajusta Todo"
        Me.Ajustat.UseVisualStyleBackColor = True
        '
        'Artajus
        '
        Me.Artajus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Artajus.BackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Artajus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Artajus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Artajus.Location = New System.Drawing.Point(322, 512)
        Me.Artajus.Name = "Artajus"
        Me.Artajus.Size = New System.Drawing.Size(124, 15)
        Me.Artajus.TabIndex = 15
        Me.Artajus.Text = "Articulos Ajustados"
        Me.Artajus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmdEliminar
        '
        Me.cmdEliminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdEliminar.Location = New System.Drawing.Point(714, 505)
        Me.cmdEliminar.Name = "cmdEliminar"
        Me.cmdEliminar.Size = New System.Drawing.Size(112, 28)
        Me.cmdEliminar.TabIndex = 4
        Me.cmdEliminar.Text = "Eliminar"
        Me.cmdEliminar.UseVisualStyleBackColor = True
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdImprimir.Location = New System.Drawing.Point(596, 505)
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Size = New System.Drawing.Size(112, 28)
        Me.cmdImprimir.TabIndex = 3
        Me.cmdImprimir.Text = "Imprimir"
        Me.cmdImprimir.UseVisualStyleBackColor = True
        Me.cmdImprimir.Visible = False
        '
        'lblFaltan
        '
        Me.lblFaltan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblFaltan.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblFaltan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFaltan.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFaltan.Location = New System.Drawing.Point(215, 512)
        Me.lblFaltan.Name = "lblFaltan"
        Me.lblFaltan.Size = New System.Drawing.Size(108, 15)
        Me.lblFaltan.TabIndex = 12
        Me.lblFaltan.Text = "Piezas de menos"
        Me.lblFaltan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCorrecto
        '
        Me.lblCorrecto.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblCorrecto.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblCorrecto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCorrecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCorrecto.Location = New System.Drawing.Point(110, 512)
        Me.lblCorrecto.Name = "lblCorrecto"
        Me.lblCorrecto.Size = New System.Drawing.Size(108, 15)
        Me.lblCorrecto.TabIndex = 11
        Me.lblCorrecto.Text = "Correcto"
        Me.lblCorrecto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSobran
        '
        Me.lblSobran.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSobran.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblSobran.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSobran.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSobran.Location = New System.Drawing.Point(8, 512)
        Me.lblSobran.Name = "lblSobran"
        Me.lblSobran.Size = New System.Drawing.Size(108, 15)
        Me.lblSobran.TabIndex = 10
        Me.lblSobran.Text = "Piezas de más"
        Me.lblSobran.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.Location = New System.Drawing.Point(953, 505)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(112, 28)
        Me.cmdCancelar.TabIndex = 6
        Me.cmdCancelar.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" &
    "alla"
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(832, 505)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(112, 28)
        Me.cmdAceptar.TabIndex = 5
        Me.cmdAceptar.Text = "Grabar"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'GridInventario
        '
        Me.GridInventario.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridInventario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridInventario.Location = New System.Drawing.Point(6, 254)
        Me.GridInventario.Name = "GridInventario"
        Me.GridInventario.Size = New System.Drawing.Size(1063, 245)
        Me.GridInventario.TabIndex = 1
        '
        'ZctSOTLabel3
        '
        Me.ZctSOTLabel3.AutoSize = True
        Me.ZctSOTLabel3.Location = New System.Drawing.Point(566, 21)
        Me.ZctSOTLabel3.Name = "ZctSOTLabel3"
        Me.ZctSOTLabel3.Size = New System.Drawing.Size(44, 13)
        Me.ZctSOTLabel3.TabIndex = 19
        Me.ZctSOTLabel3.Text = "Conteo:"
        '
        'CmdGeneraInventario
        '
        Me.CmdGeneraInventario.Location = New System.Drawing.Point(385, 8)
        Me.CmdGeneraInventario.Name = "CmdGeneraInventario"
        Me.CmdGeneraInventario.Size = New System.Drawing.Size(118, 23)
        Me.CmdGeneraInventario.TabIndex = 12
        Me.CmdGeneraInventario.Text = "Genera Inventario"
        Me.CmdGeneraInventario.UseVisualStyleBackColor = True
        '
        'cmdTerminar
        '
        Me.cmdTerminar.Enabled = False
        Me.cmdTerminar.Location = New System.Drawing.Point(518, 8)
        Me.cmdTerminar.Name = "cmdTerminar"
        Me.cmdTerminar.Size = New System.Drawing.Size(118, 23)
        Me.cmdTerminar.TabIndex = 13
        Me.cmdTerminar.Text = "Termina Conteo"
        Me.cmdTerminar.UseVisualStyleBackColor = True
        '
        'cmdAjustes
        '
        Me.cmdAjustes.Enabled = False
        Me.cmdAjustes.Location = New System.Drawing.Point(642, 8)
        Me.cmdAjustes.Name = "cmdAjustes"
        Me.cmdAjustes.Size = New System.Drawing.Size(118, 23)
        Me.cmdAjustes.TabIndex = 13
        Me.cmdAjustes.Text = "Realizar Ajustes"
        Me.cmdAjustes.UseVisualStyleBackColor = True
        '
        'grpConteo
        '
        Me.grpConteo.Controls.Add(Me.ZctSOTLabel2)
        Me.grpConteo.Controls.Add(Me.NumConteo)
        Me.grpConteo.Controls.Add(Me.ZctSOTComboBox1)
        Me.grpConteo.Controls.Add(Me.cmdGetExcel)
        Me.grpConteo.Controls.Add(Me.ZctSOTLabel1)
        Me.grpConteo.Controls.Add(Me.txtArchivoExcel)
        Me.grpConteo.Location = New System.Drawing.Point(3, 166)
        Me.grpConteo.Name = "grpConteo"
        Me.grpConteo.Size = New System.Drawing.Size(906, 58)
        Me.grpConteo.TabIndex = 11
        Me.grpConteo.TabStop = False
        Me.grpConteo.Text = "Número de conteo"
        '
        'ZctSOTLabel2
        '
        Me.ZctSOTLabel2.AutoSize = True
        Me.ZctSOTLabel2.Location = New System.Drawing.Point(12, 25)
        Me.ZctSOTLabel2.Name = "ZctSOTLabel2"
        Me.ZctSOTLabel2.Size = New System.Drawing.Size(44, 13)
        Me.ZctSOTLabel2.TabIndex = 9
        Me.ZctSOTLabel2.Text = "Conteo:"
        '
        'NumConteo
        '
        Me.NumConteo.Location = New System.Drawing.Point(62, 23)
        Me.NumConteo.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.NumConteo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumConteo.Name = "NumConteo"
        Me.NumConteo.Size = New System.Drawing.Size(54, 20)
        Me.NumConteo.TabIndex = 8
        Me.NumConteo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'ZctSOTComboBox1
        '
        Me.ZctSOTComboBox1.FormattingEnabled = True
        Me.ZctSOTComboBox1.Items.AddRange(New Object() {"Excel", "Inventarios SOT"})
        Me.ZctSOTComboBox1.Location = New System.Drawing.Point(174, 22)
        Me.ZctSOTComboBox1.Name = "ZctSOTComboBox1"
        Me.ZctSOTComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ZctSOTComboBox1.TabIndex = 6
        '
        'cmdGetExcel
        '
        Me.cmdGetExcel.Location = New System.Drawing.Point(597, 20)
        Me.cmdGetExcel.Name = "cmdGetExcel"
        Me.cmdGetExcel.Size = New System.Drawing.Size(75, 23)
        Me.cmdGetExcel.TabIndex = 2
        Me.cmdGetExcel.Text = "Obtener"
        Me.cmdGetExcel.UseVisualStyleBackColor = True
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(122, 25)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(46, 13)
        Me.ZctSOTLabel1.TabIndex = 1
        Me.ZctSOTLabel1.Text = "Archivo:"
        '
        'txtArchivoExcel
        '
        Me.txtArchivoExcel.Location = New System.Drawing.Point(301, 23)
        Me.txtArchivoExcel.Name = "txtArchivoExcel"
        Me.txtArchivoExcel.Size = New System.Drawing.Size(290, 20)
        Me.txtArchivoExcel.TabIndex = 0
        Me.txtArchivoExcel.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        '
        'GrpCaracteristicas
        '
        Me.GrpCaracteristicas.Controls.Add(Me.CboLinea)
        Me.GrpCaracteristicas.Controls.Add(Me.chkInvAle)
        Me.GrpCaracteristicas.Controls.Add(Me.cboDpto)
        Me.GrpCaracteristicas.Controls.Add(Me.chkInvGeneral)
        Me.GrpCaracteristicas.Controls.Add(Me.cboAlmacen)
        Me.GrpCaracteristicas.Location = New System.Drawing.Point(3, 37)
        Me.GrpCaracteristicas.Name = "GrpCaracteristicas"
        Me.GrpCaracteristicas.Size = New System.Drawing.Size(903, 79)
        Me.GrpCaracteristicas.TabIndex = 10
        Me.GrpCaracteristicas.TabStop = False
        Me.GrpCaracteristicas.Text = "Características del inventario"
        '
        'CboLinea
        '
        Me.CboLinea.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CboLinea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboLinea.Location = New System.Drawing.Point(544, 47)
        Me.CboLinea.Name = "CboLinea"
        Me.CboLinea.Nombre = "Sub categoría"
        Me.CboLinea.Size = New System.Drawing.Size(353, 28)
        Me.CboLinea.TabIndex = 15
        Me.CboLinea.Tag = "Los diferentes Tipos de lineas de producto que existen"
        Me.CboLinea.value = Nothing
        Me.CboLinea.ValueItem = 0
        Me.CboLinea.ValueItemStr = Nothing
        '
        'chkInvAle
        '
        Me.chkInvAle.AutoSize = True
        Me.chkInvAle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkInvAle.Location = New System.Drawing.Point(540, 19)
        Me.chkInvAle.Name = "chkInvAle"
        Me.chkInvAle.Size = New System.Drawing.Size(117, 17)
        Me.chkInvAle.TabIndex = 6
        Me.chkInvAle.Text = "Inventario Aleatorio"
        Me.chkInvAle.UseVisualStyleBackColor = True
        '
        'cboDpto
        '
        Me.cboDpto.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboDpto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDpto.Location = New System.Drawing.Point(3, 47)
        Me.cboDpto.Name = "cboDpto"
        Me.cboDpto.Nombre = "Categoría:"
        Me.cboDpto.Size = New System.Drawing.Size(459, 28)
        Me.cboDpto.TabIndex = 14
        Me.cboDpto.Tag = "Los diferentes Departamentos de producto que existen"
        Me.cboDpto.value = Nothing
        Me.cboDpto.ValueItem = 0
        Me.cboDpto.ValueItemStr = Nothing
        '
        'chkInvGeneral
        '
        Me.chkInvGeneral.AutoSize = True
        Me.chkInvGeneral.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkInvGeneral.Location = New System.Drawing.Point(663, 19)
        Me.chkInvGeneral.Name = "chkInvGeneral"
        Me.chkInvGeneral.Size = New System.Drawing.Size(113, 17)
        Me.chkInvGeneral.TabIndex = 7
        Me.chkInvGeneral.Text = "Inventario General"
        Me.chkInvGeneral.UseVisualStyleBackColor = True
        '
        'cboAlmacen
        '
        Me.cboAlmacen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAlmacen.Location = New System.Drawing.Point(12, 19)
        Me.cboAlmacen.Name = "cboAlmacen"
        Me.cboAlmacen.Nombre = "Almacén"
        Me.cboAlmacen.Size = New System.Drawing.Size(236, 28)
        Me.cboAlmacen.TabIndex = 5
        Me.cboAlmacen.value = Nothing
        Me.cboAlmacen.ValueItem = 0
        Me.cboAlmacen.ValueItemStr = Nothing
        '
        'txtFolio
        '
        Me.txtFolio.Location = New System.Drawing.Point(15, 3)
        Me.txtFolio.Multiline = False
        Me.txtFolio.Name = "txtFolio"
        Me.txtFolio.Nombre = "Folio:"
        Me.txtFolio.Size = New System.Drawing.Size(115, 28)
        Me.txtFolio.TabIndex = 3
        Me.txtFolio.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtFolio.ToolTip = ""
        '
        'ZctComparaInventarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1068, 540)
        Me.Controls.Add(Me.Artajus)
        Me.Controls.Add(Me.Ajustat)
        Me.Controls.Add(Me.cmdEliminar)
        Me.Controls.Add(Me.cmdImprimir)
        Me.Controls.Add(Me.lblFaltan)
        Me.Controls.Add(Me.lblCorrecto)
        Me.Controls.Add(Me.lblSobran)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Controls.Add(Me.GridInventario)
        Me.Controls.Add(Me.PanelPrinc)
        Me.Name = "ZctComparaInventarios"
        Me.Text = "Inventarios Físicos"
        Me.PanelPrinc.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumConteoCaptura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridInventario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpConteo.ResumeLayout(False)
        Me.grpConteo.PerformLayout()
        CType(Me.NumConteo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpCaracteristicas.ResumeLayout(False)
        Me.GrpCaracteristicas.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelPrinc As System.Windows.Forms.Panel
    Friend WithEvents cmdGetExcel As ZctSOT.ZctSOTButton
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents txtArchivoExcel As ZctSOT.ZctSOTTextBox
    Friend WithEvents OFD As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GridInventario As ZctSOT.ZctSotGrid
    Friend WithEvents txtFolio As ZctSOT.ZctControlTexto
    Friend WithEvents NumConteo As System.Windows.Forms.NumericUpDown
    Friend WithEvents ZctSOTComboBox1 As ZctSOT.ZctSOTComboBox
    Friend WithEvents ZctSOTLabel2 As ZctSOT.ZctSOTLabel
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents chkInvAle As System.Windows.Forms.CheckBox
    Friend WithEvents cboAlmacen As ZctSOT.ZctControlCombo
    Friend WithEvents chkInvGeneral As System.Windows.Forms.CheckBox
    Friend WithEvents GrpCaracteristicas As ZctSOT.ZctGroupControls
    Friend WithEvents grpConteo As ZctSOT.ZctSOTGroupBox
    Friend WithEvents CmdGeneraInventario As ZctSOT.ZctSOTButton
    Friend WithEvents lblFaltan As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblCorrecto As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblSobran As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents cmdAjustes As ZctSOT.ZctSOTButton
    Friend WithEvents cmdTerminar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdImprimir As ZctSOT.ZctSOTButton
    Friend WithEvents cmdEliminar As ZctSOT.ZctSOTButton
    Friend WithEvents CboLinea As ZctSOT.ZctControlCombo
    Friend WithEvents cboDpto As ZctSOT.ZctControlCombo
    Private WithEvents txtCapturaArticulos As System.Windows.Forms.TextBox
    Private WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ZctSOTLabel3 As ZctSOT.ZctSOTLabel
    Friend WithEvents NumConteoCaptura As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents estatus As Label
    Friend WithEvents Ajustat As CheckBox
    Friend WithEvents Artajus As ZctSOTLabelDesc
End Class
