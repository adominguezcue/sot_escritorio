Imports ZctSOT
Imports ZctInventarios
Imports ZctSQL
Imports System.Linq
Imports ZctSOT.Datos
Imports SOTControladores.Controladores
Imports System.IO
Imports Transversal.Excepciones

Public Class ZctComparaInventarios
    Friend _Controlador As New SOTControladores.Controladores.ControladorPermisos 'Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _ControladorOC As New SOTControladores.Controladores.ControladorArticulos 'ResTotal.Controlador.ControladorOC(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos 'Permisos.Controlador.Controlador.vistapermisos
    Private _Inventario As ZctInventario
    Private WithEvents _Corte As New ZctListInvCorte

    Private _Conn As String = Datos.DAO.Conexion.CadenaConexion 'String.Format(System.Configuration.ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4")
    Dim InvFolios As New zctFolios("IF")
    Private _bs As New BindingSource


    Private Sub cmdGetExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGetExcel.Click
        Dim BD As New ZctDB
        Try



            BD.GetCadenaConexion = _Conn
            BD.Conectar()

            OFD.ShowDialog(Me)
            If OFD.FileName <> "" Then

                If Not File.Exists(OFD.FileName) Then
                    Throw New SOTException("El archivo ""{0}"" no existe", OFD.FileName)
                End If

                txtArchivoExcel.Text = OFD.FileName
                Dim Datos As New ZctObjDatos(Of Captura)
                Dim Lista As List(Of Captura) = Datos.GetDatosExcel(OFD.FileName)

                _Corte.SetConteo(NumConteo.Value, Lista, BD)
                _bs.DataSource = Nothing
                _bs.DataSource = _Corte.Values
                GridInventario.DataSource = _bs
            End If
        Catch ex As SOTException
            MsgBox(ex.Message)
            Trace.WriteLine(ex.Message)
        Catch ex As ZctSQL.BaseDatosException
            MsgBox(ex.Message)
            Trace.WriteLine(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error, comuniquese con sistemas.")
            Trace.WriteLine(ex.Message)
        Finally
            BD.Desconectar()
            BD.Dispose()
        End Try


    End Sub

    Public Sub ErroresAdd(ByVal sender As Object, ByVal e As Exception) Handles _Corte.ErrorAdd

        If e.GetType Is GetType(ZctSQL.BaseDatosException) Then
            MsgBox(e.Message)
            Trace.WriteLine(e.Message)
        Else
            MsgBox("Ha ocurrido un error, comuniquese con sistemas.")
            Trace.WriteLine(e.Message)
        End If



    End Sub

    Dim sStatus As String

    Private Sub zctGeneraInventarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _Permisos = _Controlador.ObtenerPermisosActuales() 'getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        With _Permisos
            GridInventario.AllowUserToAddRows = .CrearInventario
            GridInventario.AllowUserToDeleteRows = .EliminarInventario
            GridInventario.ReadOnly = Not (.ModificarInventario Or .CrearInventario)
            If .ModificarInventario = False And .CrearInventario = False And .EliminarInventario = False Then
                cmdAceptar.Enabled = False
            Else
                cmdAceptar.Enabled = True
            End If
            cmdEliminar.Enabled = IIf(_Permisos.EliminarInventario, True, False)
        End With
        txtFolio.DataBindings.Clear()
        txtFolio.Text = InvFolios.Consecutivo 'GetData("Procedimientos_MAX", "ZctEncOC")


        cboAlmacen.ZctSOTCombo1.DataSource = New ListAlmacenes("SP_ZctCatAlm", "@Cod_Alm;int|@Desc_CatAlm;varchar|@ColNuevo;int", "0|0|0")
        cboAlmacen.ZctSOTCombo1.ValueMember = "CodAlm"
        cboAlmacen.ZctSOTCombo1.DisplayMember = "DescAlm"

        cboDpto.GetData("ZctCatDpto") ', True)
        cboDpto.ZctSOTCombo1.SelectedIndex = -1
        'El estatus es una alta
        sStatus = "A"
    End Sub



    'Busca el valor del indice de la tabla en cuestión
    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String)
        Try
            Dim PkBusCon As New Datos.ZctDataBase                             'Objeto de la base de datos
            'Inicializa el procedimietno almacenado
            PkBusCon.IniciaProcedimiento(Procedimiento)
            'Asigna el nombre de la tabla 
            PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
            'Asigna el resultado de la consulta a el objeto
            Dim a As Object = PkBusCon.GetScalarSP()
            'En caso de que no este vacío asigna su valor a la columna de indice
            If Not a Is System.DBNull.Value Then
                Return CType(a, Integer)
            Else
                Return 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return 1
        End Try
    End Function

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If sStatus = "A" Then
            Me.Dispose()
        Else
            plimpia(True)
            sStatus = "A"
        End If
    End Sub

    Private Sub plimpia(ByVal LimpiaFolio As Boolean)
        If LimpiaFolio Then
            txtFolio.DataBindings.Clear()
            txtFolio.Text = ""
            txtFolio.Text = InvFolios.Consecutivo  'GetData("Procedimientos_MAX", "ZctEncOC")

            'Deshabilita el folio
            txtFolio.Enabled = True
            txtFolio.Focus()

            GrpCaracteristicas.Enabled = True
            chkInvAle.Checked = False
            chkInvGeneral.Checked = False
            GridInventario.Columns.Clear()
            GridInventario.DataSource = Nothing
            _bs.DataSource = Nothing
            ' GetGrid()
            _Inventario = Nothing
            _Corte = Nothing
            cboDpto.ZctSOTCombo1.SelectedIndex = -1
            cboDpto.ZctSOTCombo1.SelectedIndex = -1
            CboLinea.ZctSOTCombo1.DataBindings.Clear()
            CboLinea.ZctSOTCombo1.DataSource = Nothing
            'cboDpto.DataBindings.Clear()


        End If
    End Sub

    Private Sub cmdGenera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdGeneraInventario.Click
        'Dim fLogin As New ZctLogin
        'fLogin.Cod_Aut = 15
        'fLogin.ShowDialog(Me)
        Dim controlador = New ControladorPermisos()
        If controlador.VerificarPermisos(New Modelo.Seguridad.Dtos.DtoPermisos With {.CrearInventario = True}, True) = False Then
            MsgBox("Usted no tiene permisos para generar el inventario.")
            Exit Sub
        End If
        Dim BD As New ZctDB
        Try

            If Not chkInvAle.Checked And Not chkInvGeneral.Checked Then
                MsgBox("Debe seleccionar el tipo de inventario.")
                Exit Sub
            End If

            If IsNothing(cboDpto.value) Then
                MsgBox("Seleccione una categoría para el inventario")
                Exit Sub
            End If

            Dim controladorArticulos = New ControladorArticulos()

            If chkInvAle.Checked Then
                controladorArticulos.ValidarExistenArticulosInventariables(Nothing, Nothing)
            Else
                controladorArticulos.ValidarExistenArticulosInventariables(cboDpto.value, If(CboLinea.value <> 0, CboLinea.value, Nothing))
            End If

            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.ComenzarTransaccion()
            If sStatus = "A" Then
                txtFolio.DataBindings.Clear()
                txtFolio.Text = InvFolios.SetConsecutivo(BD)  'GetData("Procedimientos_MAX", "ZctEncOC", PkConAlone)
            End If
            'Graba el encabezado

            If _Inventario Is Nothing Then _Inventario = New ZctInventario(txtFolio.Text)
            _Inventario.Folio = CInt(txtFolio.Text)
            _Inventario.Almacen = cboAlmacen.ZctSOTCombo1.SelectedValue
            _Inventario.Aleatorio = chkInvAle.Checked
            _Inventario.General = chkInvGeneral.Checked
            _Inventario.Termino = False
            _Inventario.AplicaAjuste = False
            _Inventario.CodigoDepartamento = cboDpto.value
            _Inventario.CodigoLinea = If(CboLinea.value = 0, Nothing, CboLinea.value)
            Implementacion.GenGraba(Of ZctInventario)(BD, _Inventario)



            'Generar Corte
            If _Corte Is Nothing Then _Corte = New ZctListInvCorte
            _Corte.Folio = _Inventario.Folio
            _Corte.CodAlm = _Inventario.Almacen
            If _Inventario.Aleatorio Then
                _Corte.TpInventario = 2
            ElseIf _Inventario.General Then
                _Corte.TpInventario = 1
            End If
            _Corte.CodDpto = cboDpto.value
            _Corte.CodLinea = CboLinea.value
            Implementacion.GenGraba(Of ZctListInvCorte)(BD, _Corte)

            BD.ConfirmarTransaccion()

            CargaInventario()
            GrpCaracteristicas.Enabled = False
        Catch ex As SOTException
            BD.CancelarTransaccion()
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Catch ex As Exception
            BD.CancelarTransaccion()
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        Finally

            BD.Desconectar()
            BD.Dispose()
        End Try


        'Status 
        If sStatus = "A" Then
            'If MsgBox("¿Desea imprimir la orden de compra?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            '    pImprime()
            'End If
        End If

        'If MsgBox("Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
        '    plimpia(True)
        '    sStatus = "A"
        'Else
        '    plimpia(False)

        '    sStatus = "M"
        '    'CargaCompra()
        '    CargaInventario()
        'End If

        'Cargar los datos
        EstatusInventario()
    End Sub

    Private Sub txtFolio_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolio.lostfocus
        Try
            If txtFolio.Text <> "" And CType(txtFolio.Text, Integer) > 0 Then
                'CargaCompra()
                CargaInventario()
            End If
            'Status 
            If sStatus = "M" Then
                GrpCaracteristicas.Enabled = False
            Else
                GrpCaracteristicas.Enabled = True
            End If
            'EstatusInventario()



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub EstatusInventario()
        If _Inventario IsNot Nothing Then
            If _Inventario.Termino AndAlso _Inventario.AplicaAjuste Then
                cmdTerminar.Text = "Reabre Conteo"
                cmdImprimir.Visible = True
                estatus.Text = "Cerrado"
                cmdTerminar.Enabled = False
                CmdGeneraInventario.Enabled = False
                cmdAjustes.Enabled = False
                estatus.BackColor = Color.FromArgb(243, 159, 24)
            ElseIf _Inventario.Termino And Not _Inventario.AplicaAjuste Then
                cmdTerminar.Text = "Reabre Conteo"
                CmdGeneraInventario.Enabled = False
                cmdTerminar.Enabled = True
                cmdAjustes.Enabled = True
                estatus.Text = "Conteo Terminado"
                estatus.BackColor = Color.FromArgb(204, 255, 255)
            Else

                cmdTerminar.Text = "Termina Conteo"
                CmdGeneraInventario.Enabled = False
                cmdTerminar.Enabled = True
                estatus.Text = "Abierto"
                estatus.BackColor = Color.FromArgb(204, 255, 255)
            End If
            If (_Inventario IsNot Nothing) Then
                If (_Inventario.Almacen Is Nothing) Then
                    estatus.Text = "No Existe Folio"
                    estatus.BackColor = Color.FromArgb(204, 255, 255)
                    GridInventario.Columns.Clear()
                End If
            End If
            txtCapturaArticulos.Enabled = Not _Inventario.Termino
            'cmdAjustes.Enabled = _Inventario.Termino
            grpConteo.Enabled = Not _Inventario.Termino
                grpConteo.Enabled = _Inventario.General
            End If
    End Sub
    Public Sub CargaInventario()
        If txtFolio.Text <> "" Then
            'Deshabilita el folio
            txtFolio.Enabled = False
            _Inventario = New ZctInventario(txtFolio.Text)
            'Carga los datos
            GenDatos(Of ZctInventario)(_Conn, _Inventario)
            If _Inventario IsNot Nothing AndAlso _Inventario.Almacen IsNot Nothing Then
                cboAlmacen.ValueItem = _Inventario.Almacen
                cboDpto.value = If(_Inventario.CodigoDepartamento, 0)
                CargarLineas()
                CboLinea.value = If(_Inventario.CodigoLinea, 0)
                chkInvAle.Checked = _Inventario.Aleatorio
                chkInvGeneral.Checked = _Inventario.General
                _Corte = New ZctListInvCorte
                _Corte.Folio = txtFolio.Text
                _Corte.CodAlm = _Inventario.Almacen
                Implementacion.GenDatos(Of ZctListInvCorte)(_Conn, _Corte)
                GetGrid()

                _bs.DataSource = _Corte.Values
                GridInventario.DataSource = _bs
                sStatus = "M"

            End If

        End If
    End Sub

    Private Sub chkInvAle_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInvAle.CheckedChanged
        chkInvGeneral.Checked = Not chkInvAle.Checked
        grpConteo.Enabled = chkInvGeneral.Checked
    End Sub

    Private Sub chkInvGeneral_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInvGeneral.CheckedChanged
        chkInvAle.Checked = Not chkInvGeneral.Checked
        grpConteo.Enabled = chkInvGeneral.Checked
    End Sub

    Public Sub GetGrid()
        GridInventario.Columns.Clear()
        GridInventario.AutoGenerateColumns = False
        Dim Habilita As Boolean = False
        If _Inventario IsNot Nothing Then
            Habilita = _Inventario.Termino
        End If

        Dim Aleatorio As Boolean = False
        If _Inventario IsNot Nothing AndAlso _Inventario.Aleatorio Then
            Aleatorio = True
        End If


        GridInventario.Columns.Add(zctColumnComun.GetColumn("Cod_Art", "Código", "Cod_Art", True, , , True))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Desc_Art", "Artículo", "Desc_Art", True, , , True))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Costo", "Costo", "Costo", Habilita, "$##,###.00", , True))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Existencia", "Existencia", "Existencia", Habilita, , GetType(Integer), True))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Conteo1", "Conteo 1", "Conteo1", Not Aleatorio, , GetType(Integer), , 0))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Conteo2", "Conteo 2 ", "Conteo2", Not Aleatorio, , GetType(Integer), , 0))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Conteo3", "Conteo Final ", "Conteo3", True, , GetType(Integer), , 0))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Diferencia", "Diferencia ", "Diferencia", Habilita, , GetType(Integer), True, 0))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Ajuste", "Ajuste ", "Ajuste", Habilita, , GetType(Boolean)))
        GridInventario.Columns.Add(zctColumnComun.GetColumn("Realizado", "Realizado ", "Realizado", Habilita, , GetType(Boolean)))

    End Sub

    Private Sub SetColor(ByVal RowIndex As Integer, ByVal color As Color)
        GridInventario("Diferencia", RowIndex).Style.BackColor = color
        GridInventario("Conteo1", RowIndex).Style.BackColor = color
        GridInventario("Conteo2", RowIndex).Style.BackColor = color
        GridInventario("Conteo3", RowIndex).Style.BackColor = color
        GridInventario("Existencia", RowIndex).Style.BackColor = color
        'GridInventario("Ajuste", RowIndex).Style.BackColor = color
        'GridInventario("Realizado", RowIndex).Style.BackColor = color

    End Sub

    Private Sub SetColor1(ByVal RowIndex As Integer, ByVal color As Color)
        GridInventario("Ajuste", RowIndex).Style.BackColor = color
        GridInventario("Realizado", RowIndex).Style.BackColor = color
    End Sub

    Private Sub GridInventario_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles GridInventario.CellBeginEdit
        If _Inventario.Termino AndAlso e.ColumnIndex <> GridInventario.Columns("Ajuste").Index Then
            e.Cancel = True
        End If
    End Sub





    Private Sub GridInventario_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles GridInventario.CellPainting

        If e.RowIndex < 0 Then Exit Sub
        If GridInventario.DataSource Is Nothing Then Exit Sub
        If _Inventario Is Nothing OrElse Not _Inventario.Termino Then Exit Sub
        If GridInventario("Conteo3", e.RowIndex).Value Is Nothing Then GridInventario("Conteo3", e.RowIndex).Value = 0
        If GridInventario("Diferencia", e.RowIndex).Value Is Nothing Then GridInventario("Diferencia", e.RowIndex).Value = 0

        If GridInventario("Conteo3", e.RowIndex).Value > 0 Then
            If GridInventario("Diferencia", e.RowIndex).Value = 0 Then
                SetColor(e.RowIndex, lblCorrecto.BackColor)
            ElseIf GridInventario("Diferencia", e.RowIndex).Value < 0 Then
                SetColor(e.RowIndex, lblFaltan.BackColor)
            ElseIf GridInventario("Diferencia", e.RowIndex).Value > 0 Then
                SetColor(e.RowIndex, lblSobran.BackColor)
            End If
        Else
            SetColor(e.RowIndex, Color.WhiteSmoke)
        End If
        If GridInventario("Ajuste", e.RowIndex).Value Is Nothing Then GridInventario("Ajuste", e.RowIndex).Value = False
        If GridInventario("Realizado", e.RowIndex).Value Is Nothing Then GridInventario("Realizado", e.RowIndex).Value = False

        If GridInventario("Ajuste", e.RowIndex).Value = True And GridInventario("Realizado", e.RowIndex).Value = True Then
            SetColor1(e.RowIndex, Artajus.BackColor)
        End If


    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click

        Dim BD As New ZctDB
        Try
            If _Inventario Is Nothing Then
                MsgBox("Debe de generar primero el inventario.")
                Exit Sub
            End If

            If _Corte Is Nothing Then
                MsgBox("Debe de generar primero el corte del inventario.")
                Exit Sub
            End If
            Dim PGrabar As Boolean = False
            'Valida que no existan datos sin aplicar
            For Each inv As ZctInventarios.ZctInvUnidad In _Corte.Values
                If inv.Ajuste And Not inv.Realizado Then
                    PGrabar = True
                    Exit For
                End If
            Next

            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.ComenzarTransaccion()
            If PGrabar Then
                If MsgBox("Existen artículos que tienen ajustes que no han sido aplicados, ¿Desea realizar esos ajustes ahora?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    _Corte.Ajuste(BD)
                    _Corte.Grabar(BD)
                Else
                    _Corte.GrabarSinAjustes(BD)
                End If
            Else
                _Corte.Grabar(BD)
            End If

            BD.ConfirmarTransaccion()
            If MsgBox("¿Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                plimpia(True)
                sStatus = "A"
            Else
                'sStatus = "M"
                CargaInventario()
            End If

        Catch ex As Exception
            BD.CancelarTransaccion()
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        Finally
            BD.Desconectar()
            BD.Dispose()
        End Try

    End Sub


    Private Sub GridInventario_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles GridInventario.DataError
        MsgBox(e.Exception.Message)
    End Sub


    Private Sub cmdTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTerminar.Click
        'Dim fLogin As New ZctLogin
        'fLogin.Cod_Aut = 16
        'fLogin.ShowDialog(Me)
        'If Not fLogin.Valida Then
        '    MsgBox("Usted no tiene permisos para generar el inventario.")
        '    Exit Sub
        'End If
        If _Inventario Is Nothing Then
            MsgBox("Debe de iniciar el conteo antes de poder terminarlo o reabrirlo.")
            Exit Sub
        End If
        Dim CodAut = New Modelo.Seguridad.Dtos.DtoPermisos()
        If _Inventario.Termino Then
            If MsgBox("¿Desea volver a abrir la captura de conteos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
            'Reabrir el inventario
            CodAut.ReabirInventario = True
        Else
            If MsgBox("¿Desea cerrar la captura de conteos?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
            'Reabrir el inventario
            CodAut.CerrarInventario = True
        End If


        'Dim controlador = New SOTControladores.Controladores.ControladorPermisos()
        'If controlador.VerificarPermisos(CodAut, True) = True Then
        'If fLogin.Valida = True Then
        Dim BD As New ZctDB
            Try

                BD.GetCadenaConexion = _Conn
                BD.Conectar()
                BD.ComenzarTransaccion()
                _Corte.Grabar(BD)
            _Inventario.Termino = Not _Inventario.Termino
            _Inventario.AplicaAjuste = False
            Implementacion.GenGraba(Of ZctInventario)(BD, _Inventario)
                BD.ConfirmarTransaccion()
                EstatusInventario()
                GetGrid()
                _bs.DataSource = _Corte.Values
                GridInventario.DataSource = _bs
                MsgBox("La operación se realizo correctamente.", MsgBoxStyle.Information)

            Catch ex As Exception
                BD.CancelarTransaccion()
                MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
            Finally

                BD.Desconectar()
                BD.Dispose()
            End Try


        ' Else
        'Throw New SOTException("No tiene permisos para realizar esta operación")
        'End If

    End Sub

    Private Sub txtFolio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolio.Load

    End Sub

    Private Sub cmdAjustes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAjustes.Click

        Dim BD As New ZctDB
        Try
            If _Inventario Is Nothing Then
                MsgBox("Debe de generar primero el inventario.")
                Exit Sub
            End If

            If _Corte Is Nothing Then
                MsgBox("Debe de generar primero el corte del inventario.")
                Exit Sub
            End If

            If Not _Inventario.Termino Then
                MsgBox("Debe de terminar primero el Invenatario")
                Exit Sub
            End If
            'Todo: Agregar una autorización aquí
            'graba en ajustes
            Dim CodAut = New Modelo.Seguridad.Dtos.DtoPermisos()
            Dim controlador = New SOTControladores.Controladores.ControladorPermisos()
            If controlador.VerificarPermisos(CodAut, True) = True Then
                Dim BD1 As New ZctDB
                Try
                    _Inventario.AplicaAjuste = True
                    BD1.GetCadenaConexion = _Conn
                    BD1.Conectar()
                    BD1.ComenzarTransaccion()
                    Implementacion.GenGraba(Of ZctInventario)(BD1, _Inventario)
                    BD1.ConfirmarTransaccion()
                Catch ex As Exception
                    BD1.CancelarTransaccion()
                    MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
                Finally
                    BD1.Desconectar()
                    BD1.Dispose()
                End Try
            End If


            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.ComenzarTransaccion()
            _Corte.Ajuste(BD)
            _Corte.Grabar(BD)
            BD.ConfirmarTransaccion()
            AjusteRealizado()
            MsgBox("Los ajustes se realizaron correctamente.")
        Catch ex As Exception
            BD.CancelarTransaccion()
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        Finally
            BD.Desconectar()
            BD.Dispose()
        End Try
        EstatusInventario()
        CargaInventario()
    End Sub
    Public Sub AjusteRealizado()
        For Each unidad As ZctInvUnidad In _Corte.Values
            If unidad.Ajuste Then unidad.Realizado = True
        Next
    End Sub

    Private Sub GridInventario_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GridInventario.CellContentClick

    End Sub

    Private Sub PanelPrinc_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PanelPrinc.Paint

    End Sub

    Dim sPathRpt As String = "c:\Reportes\ZctInvCierreMes.rpt"

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        If txtFolio.Text = "0" Or txtFolio.Text = "" Then Exit Sub
        'Dim myCr As New PkVisorRpt
        'myCr.MdiParent = Me.MdiParent
        ''myCr.sDataBase = cboServer.SelectedValue
        'myCr.sSQLV = "{VW_ZctInventarioFinal.folio} = '" & txtFolio.Text & "'" 'CType(txtFolio.Text, Integer)

        'myCr.sRpt = sPathRpt
        'myCr.Show()

        Dim reporte = New ReportesSOT.Inventarios.InventarioCierreMes()
        reporte.Load()

        Dim controladorInventarios = New ControladorInventarios()
        Dim detalles = controladorInventarios.DetallesInventario(txtFolio.Text).Select(Function(m)
                                                                                           Return New With {m.Exist_ArtNN, m.CostoProm_ArtNN, m.Conteo3_ArtNN, m.Diferencia_ArtNN, m.FchyHrRegNN, m.Cod_Art, m.Desc_Art, m.Folio, m.Desc_CatAlm, m.taller, m.TableName}
                                                                                       End Function).ToList()

        reporte.SetDataSource(detalles)

        Dim visorR = New VisorReportes()

        visorR.crCrystalReportViewer.ReportSource = reporte

        visorR.Update()

        visorR.ShowDialog(Me)
    End Sub

    Private Sub cmdEliminar_Click(sender As System.Object, e As System.EventArgs) Handles cmdEliminar.Click

        Dim BD As New ZctDB
        Try
            If _Inventario Is Nothing Then
                MsgBox("Debe de generar primero el inventario.")
                Exit Sub
            End If

            If _Corte Is Nothing Then
                MsgBox("Debe de generar primero el corte del inventario.")
                Exit Sub
            End If

            If _Inventario.Termino Then
                MsgBox("No puede eliminar un inventario ya terminado")
                Exit Sub
            End If
            'Todo: Agregar una autorización aquí
            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.ComenzarTransaccion()

            _Corte.Eliminar(BD)
            BD.ConfirmarTransaccion()

            MsgBox("Se ha eliminado correctamente el inventario.")
            plimpia(True)
        Catch ex As Exception
            BD.CancelarTransaccion()
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        Finally
            BD.Desconectar()
            BD.Dispose()
        End Try

    End Sub

    Private Sub GrpCaracteristicas_Enter(sender As System.Object, e As System.EventArgs) Handles GrpCaracteristicas.Enter

    End Sub

    Private Sub cboDpto_lostfocus(sender As System.Object, e As System.EventArgs) Handles cboDpto.lostfocus
        CargarLineas()
    End Sub

    Private Sub CargarLineas()
        CboLinea.GetData("ZctCatLineaXDpto", If(cboDpto.value, 0), True)
    End Sub

    Private Sub label3_Click(sender As System.Object, e As System.EventArgs) Handles label3.Click

    End Sub

    Private Sub txtCapturaArticulos_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCapturaArticulos.KeyDown
        If (e.KeyCode = Keys.Enter) Then

            System.Windows.Forms.SendKeys.Send("{TAB}")
            Exit Sub
        End If
    End Sub

    Private Sub txtCapturaArticulos_Leave(sender As Object, e As System.EventArgs) Handles txtCapturaArticulos.Leave
        CapturaArticulo()
    End Sub

    Private Sub txtCapturaArticulos_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtCapturaArticulos.TextChanged

    End Sub

    Private Function BuscarEnGrid(Columna As Integer, Valor As String)
        Dim obj As IEnumerable(Of DataGridViewRow) = From row As DataGridViewRow In GridInventario.Rows
                                            Where row.Cells(Columna).Value = Valor
                                            Select row
        If obj.Any() Then
            GridInventario.ClearSelection()
            GridInventario.Rows(obj.FirstOrDefault().Index).Selected = True
        End If
        If obj.FirstOrDefault() Is Nothing Then
            Return -1
        End If
        Return obj.FirstOrDefault().Index
    End Function

    Private Sub CapturaArticulo()
        With GridInventario
            Dim RowIndex As Integer
            Try

                If .RowCount = 0 Or txtCapturaArticulos.Text = "" Then
                    Exit Sub
                End If
                Console.Beep()
                txtCapturaArticulos.Text = txtCapturaArticulos.Text.Replace("'", "-")
                Dim DatosArticulo = _ControladorOC.ObtenerArticuloNoNull(txtCapturaArticulos.Text)

                RowIndex = BuscarEnGrid(0, DatosArticulo.Cod_Art)
                If RowIndex <> -1 Then
                    Dim inRow As New DataGridViewRow
                    inRow.CreateCells(GridInventario)
                    With inRow

                        If NumConteoCaptura.Value = 1 Then
                            GridInventario("Conteo1", RowIndex).Value += 1
                            '.Cells("Conteo1").Value += 1
                        Else
                            If NumConteoCaptura.Value = 2 Then
                                '.Cells("Conteo2").Value += 1
                                GridInventario("Conteo2", RowIndex).Value += 1
                            Else
                                GridInventario("Conteo3", RowIndex).Value += 1
                                '.Cells("Conteo3").Value += 1
                            End If
                        End If
                    End With
                    'Me.DGridArticulos.Rows.Add(DatosArticulo.Cod_Art, DatosArticulo.Desc_Art, 0, almacen_por_default, 1, 1, DatosArticulo.Cos_Art, DatosArticulo.Cos_Art, 0)

                    'RowIndex = .Rows.Count - 1
                    .ClearSelection()
                    .Rows(RowIndex).Selected = True
                    .FirstDisplayedScrollingRowIndex = RowIndex
                Else
                    MessageBox.Show("El artículo no ha sido encontrado.")
                End If




                txtCapturaArticulos.Text = ""
                txtCapturaArticulos.Focus()




            Catch ex As Exception
                txtCapturaArticulos.Text = ""
                txtCapturaArticulos.Focus()
                MsgBox("Error al buscar el artículo: " & ex.Message, MsgBoxStyle.Exclamation, "Atención")
            End Try
        End With
    End Sub

    Private Sub cboDpto_Load(sender As System.Object, e As System.EventArgs) Handles cboDpto.Load

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            If txtFolio.Text <> "" And CType(txtFolio.Text, Integer) > 0 Then
                'CargaCompra()
                Dim Folio As Integer
                Folio = CInt(Int(txtFolio.Text))
                Folio = Folio + 1
                Dim FolioActual As Integer
                FolioActual = InvFolios.Consecutivo
                If (FolioActual = Folio) Then
                    txtFolio.Text = Folio
                    Ajustat.Checked = False
                    CmdGeneraInventario.Enabled = True
                    cmdTerminar.Enabled = False
                    plimpia(True)
                    sStatus = "A"
                    estatus.Text = "Alta de Inventario"
                    estatus.BackColor = Color.Gold
                    EstatusInventario()
                End If
                If (Folio < FolioActual) Then
                    txtFolio.Text = Folio
                    Ajustat.Checked = False
                    CargaInventario()
                    EstatusInventario()
                End If
                If (Folio > FolioActual) Then
                    txtFolio.Text = Folio
                    Ajustat.Checked = False
                    plimpia(True)
                    estatus.Text = "Alta de Inventario"
                    estatus.BackColor = Color.Gold
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If txtFolio.Text <> "" And CType(txtFolio.Text, Integer) > 0 Then
                'CargaCompra()
                Dim Folio As Integer
                Folio = CInt(Int(txtFolio.Text))
                Folio = Folio - 1
                Dim FolioActual As Integer
                FolioActual = InvFolios.Consecutivo
                If (FolioActual = Folio) Then
                    txtFolio.Text = Folio
                    plimpia(True)
                    Ajustat.Checked = False
                    CmdGeneraInventario.Enabled = True
                    cmdTerminar.Enabled = False
                    sStatus = "A"
                    estatus.Text = "Alta de Inventario"
                    estatus.BackColor = Color.Gold
                    EstatusInventario()
                End If
                If (Folio < FolioActual) Then
                    txtFolio.Text = Folio
                    Ajustat.Checked = False
                    CargaInventario()
                    EstatusInventario()
                End If
                If (Folio > FolioActual) Then
                    Ajustat.Checked = False
                    txtFolio.Text = Folio
                    plimpia(True)
                    estatus.Text = "Alta de Inventario"
                    estatus.BackColor = Color.Gold
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles estatus.Click

    End Sub

    Private Sub Ajustartodo_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles Ajustat.CheckedChanged
        Dim valor1 As Boolean
        valor1 = Ajustat.CheckState
        If GridInventario.DataSource Is Nothing Then Exit Sub
        If valor1 = True Then
            For i As Integer = 0 To GridInventario.Rows.Count - 1
                GridInventario.Rows(i).Cells(8).Value = valor1  '8 es la columna de ajustar
            Next
        End If
        If valor1 = False Then
            For i As Integer = 0 To GridInventario.Rows.Count - 1
                GridInventario.Rows(i).Cells(8).Value = valor1 '8 es la columna de ajustar
            Next
        End If

    End Sub
End Class