﻿Imports ZctSOT.Datos

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctMainPresupuesto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ZctMainPresupuesto))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.grdDatos = New ZctSOT.ZctSotGrid()
        Me.almacen = New ZctSOT.ZctControlCombo()
        Me.cmdConsultar = New ZctSOT.ZctSOTButton()
        Me.cmdBuscaArchivo = New ZctSOT.ZctSOTButton()
        Me.lblArchivo = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel()
        Me.txtAnno = New ZctSOT.ZctControlTexto()
        Me.ZctPresupuesto_ElementoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ZctPresupuesto_ElementoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.almacen)
        Me.Panel1.Controls.Add(Me.cmdConsultar)
        Me.Panel1.Controls.Add(Me.cmdBuscaArchivo)
        Me.Panel1.Controls.Add(Me.lblArchivo)
        Me.Panel1.Controls.Add(Me.ZctSOTLabel1)
        Me.Panel1.Controls.Add(Me.txtAnno)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(781, 73)
        Me.Panel1.TabIndex = 0
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "Excel (*.xls)|*.xls"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(698, 447)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancelar.TabIndex = 3
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(616, 447)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 2
        Me.cmdAceptar.Text = "Grabar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'grdDatos
        '
        Me.grdDatos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDatos.Location = New System.Drawing.Point(6, 81)
        Me.grdDatos.Name = "grdDatos"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdDatos.RowHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdDatos.Size = New System.Drawing.Size(771, 360)
        Me.grdDatos.TabIndex = 1
        '
        'almacen
        '
        Me.almacen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.almacen.Location = New System.Drawing.Point(140, 10)
        Me.almacen.Name = "almacen"
        Me.almacen.Nombre = "Almacen"
        Me.almacen.Size = New System.Drawing.Size(414, 28)
        Me.almacen.TabIndex = 6
        Me.almacen.value = Nothing
        Me.almacen.ValueItem = 0
        Me.almacen.ValueItemStr = Nothing
        '
        'cmdConsultar
        '
        Me.cmdConsultar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdConsultar.Location = New System.Drawing.Point(561, 12)
        Me.cmdConsultar.Name = "cmdConsultar"
        Me.cmdConsultar.Size = New System.Drawing.Size(98, 23)
        Me.cmdConsultar.TabIndex = 1
        Me.cmdConsultar.Text = "Consultar"
        Me.cmdConsultar.UseVisualStyleBackColor = True
        '
        'cmdBuscaArchivo
        '
        Me.cmdBuscaArchivo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdBuscaArchivo.Image = CType(resources.GetObject("cmdBuscaArchivo.Image"), System.Drawing.Image)
        Me.cmdBuscaArchivo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBuscaArchivo.Location = New System.Drawing.Point(665, 36)
        Me.cmdBuscaArchivo.Name = "cmdBuscaArchivo"
        Me.cmdBuscaArchivo.Size = New System.Drawing.Size(112, 24)
        Me.cmdBuscaArchivo.TabIndex = 4
        Me.cmdBuscaArchivo.Text = "Importar Archivo"
        Me.cmdBuscaArchivo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdBuscaArchivo.UseVisualStyleBackColor = True
        '
        'lblArchivo
        '
        Me.lblArchivo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblArchivo.BackColor = System.Drawing.Color.LightBlue
        Me.lblArchivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblArchivo.Location = New System.Drawing.Point(57, 41)
        Me.lblArchivo.Name = "lblArchivo"
        Me.lblArchivo.Size = New System.Drawing.Size(602, 19)
        Me.lblArchivo.TabIndex = 3
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(5, 42)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(46, 13)
        Me.ZctSOTLabel1.TabIndex = 2
        Me.ZctSOTLabel1.Text = "Archivo:"
        '
        'txtAnno
        '
        Me.txtAnno.Location = New System.Drawing.Point(20, 10)
        Me.txtAnno.Multiline = False
        Me.txtAnno.Name = "txtAnno"
        Me.txtAnno.Nombre = "Año:"
        Me.txtAnno.Size = New System.Drawing.Size(115, 28)
        Me.txtAnno.TabIndex = 0
        Me.txtAnno.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtAnno.ToolTip = ""
        '
        'ZctPresupuesto_ElementoBindingSource
        '
        Me.ZctPresupuesto_ElementoBindingSource.DataSource = GetType(ZctPresupuesto_Elemento)
        '
        'ZctMainPresupuesto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(781, 482)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Controls.Add(Me.grdDatos)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ZctMainPresupuesto"
        Me.Text = "Presupuesto"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ZctPresupuesto_ElementoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtAnno As ZctSOT.ZctControlTexto
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents grdDatos As ZctSOT.ZctSotGrid
    Friend WithEvents cmdBuscaArchivo As ZctSOT.ZctSOTButton
    Friend WithEvents lblArchivo As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmdConsultar As ZctSOT.ZctSOTButton
    Friend WithEvents ZctPresupuesto_ElementoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents almacen As ZctSOT.ZctControlCombo
End Class
