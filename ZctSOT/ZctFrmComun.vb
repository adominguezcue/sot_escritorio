'Importa la clase de la base de datos
Imports ZctSOT.ZctDataBase
Imports ZctSOT.ZctFunciones
Imports ZctSOT.Datos.ZctFunciones
Imports SOTControladores.Controladores

Public Class ZctfrmComun
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
#Region "Declaraci�n de constantes"
    Const ColIndex = 0                                  'Contiene el valor de la columna Indice
    Const sColNuevo = "ColNuevo"                        'Cadena que servira para buscar la columna de nuevo
#End Region

#Region "Parametros de constantes"
    'Parametros para los SP
    Public sSPName As String                            'Nombre del Procedimiento Almacenado Pricipal
    Public sSpVariables As String                       'Varibles del Procedimiento Almacenado
    Public sTablaName As String                         'Nombre de la Tabla Pricipal

    'Parametro de Busqueda
    Public sSPName_Busq As String                       'Nombre del Procedimiento almacenado de busqueda
    Public sSpVariables_Busq As String                  'Variables del procedimiento almacenado de busqueda
    Public iColBusq As Integer                          'Columna en donde se encuentran la columna de busqueda
    Public sTabla_Busq As String                        'Nombre de la tabla de busqueda
#End Region

#Region "Variables Globales"
    Dim iIndice As Integer = 0                          'Indice de escritura
    Dim bCambios As Boolean = False                     'Marca si se han realizado cambios
#End Region

    Private Sub PkBusqueda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
            With _Permisos
                DGBusqueda.AllowUserToAddRows = .agrega
                DGBusqueda.AllowUserToDeleteRows = .elimina
                DGBusqueda.ReadOnly = Not (.edita Or .agrega)
                If .edita = False And .agrega = False And .elimina = False Then
                    cmdAceptar.Enabled = False
                Else
                    cmdAceptar.Enabled = True
                End If
            End With
            'Posici�n Inicial
            Me.Top = 0
            Me.Left = 0

            'Obtiene los datos del cat�logo
            GetData()

            'Asigna el tool tip para la columna de busqueda
            If sSPName_Busq <> "" Then
                DGBusqueda.Columns(iColBusq).ToolTipText = "Presione F3 para mostrar el cat�logo"
                DGBusqueda.Columns(iColBusq).DefaultCellStyle.BackColor = System.Drawing.SystemColors.InactiveCaptionText
            End If

            'Muestra los tool tips
            DGBusqueda.ShowCellToolTips = True

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try
            Dim iRow As Integer                             'Indice temporal
            With DGBusqueda
                For iRow = 0 To DGBusqueda.RowCount - 2
                    'Almacena los datos en donde el �ndice no este vac�o
                    If Not (DGBusqueda.Item(ColIndex, iRow).Value.ToString = "") Then
                        GetData(2, iRow)
                    End If
                Next
            End With
            'Refresca los datos despues de grabar
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, sSPName, "A", "")
            GetData()
            bCambios = False
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub GetData()
        Try
            Dim PkBusCon As New Datos.ZctDataBase                                     'Objeto de la base de datos
            Dim iSp As Integer                                                  'Indice temporal
            Dim sVector() As String = Split(sSpVariables, "|")                  'Obtiene las diferentes variables del Procedimiento Almacenado
            'Inicia el Procedimiento almacenado
            PkBusCon.IniciaProcedimiento(sSPName)
            'Determina el tipo de consulta
            PkBusCon.AddParameterSP("@TpConsulta", 1, SqlDbType.Int)
            'Obtiene el n�mero de variables totales
            Dim iTot As Integer = UBound(sVector)
            'Recorre las variables del procedimiento almacenado
            For iSp = 0 To iTot
                'Busca por tipo de dato la sentencia correcta
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                    Case "INT"

                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.Int)
                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.Decimal)
                End Select
            Next
            'Incia el data Adapter para la extracci�n de datos
            PkBusCon.InciaDataAdapter()
            'Inserta los datos obtenidos al Grid
            DGBusqueda.DataSource = PkBusCon.GetReaderSP.Tables("DATOS")
            DGBusqueda.Columns(sColNuevo).Visible = False
            'Obtiene el �ltimo dato
            iIndice = GetData("Procedimientos_MAX", sTablaName)

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    'Busca el valor del indice de la tabla en cuesti�n
    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String)
        Try
            Dim PkBusCon As New Datos.ZctDataBase                             'Objeto de la base de datos
            'Inicializa el procedimietno almacenado
            PkBusCon.IniciaProcedimiento(Procedimiento)
            'Asigna el nombre de la tabla 
            PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
            'Asigna el resultado de la consulta a el objeto
            Dim a As Object = PkBusCon.GetScalarSP()
            'En caso de que no este vac�o asigna su valor a la columna de indice
            If Not a Is System.DBNull.Value Then
                Return CType(a, Integer)
            Else
                Return 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return 1
        End Try
    End Function
    'Busca el valor del indice de la tabla en cuesti�n
    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String, ByVal Busqueda As Integer)
        Try
            Dim PkBusCon As New Datos.ZctDataBase                             'Objeto de la base de datos
            'Inicia el procedimiento almacenado
            PkBusCon.IniciaProcedimiento(Procedimiento)
            'Asigna el nombre de la tabla 
            PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
            'Asigna el dato a buscar
            PkBusCon.AddParameterSP("@Busqueda", Busqueda, SqlDbType.Int)
            'Asigna el resultado de la consulta a el objeto
            Dim a As Object = PkBusCon.GetScalarSP()
            'En caso de que no este vac�o asigna su valor a la columna de indice
            If Not a Is Nothing Then
                Return CType(a, Integer)
            Else
                Return 0
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return 0
        End Try
    End Function


    Private Sub GetData(ByVal TpConsulta As Integer, ByVal iRow As Integer)
        Try
            Dim PkBusCon As New Datos.ZctDataBase                         'Objeto de la base de datos
            Dim iSp As Integer                                      'Indice del procedimiento almacenado
            Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
            'En caso de que sea una columna no valida, sale del procedimiento 
            If iRow > DGBusqueda.RowCount Then Exit Sub
            'Inicia el procedimieto almacenado
            PkBusCon.IniciaProcedimiento(sSPName)
            'Agrega el parametro que define que se va a realizar
            PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
            'Obtiene el total de los procedimientos
            Dim iTot As Integer = UBound(sVector)
            'Recorre las variables del procedimiento almacenado
            For iSp = 0 To iTot
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1).ToUpper
                    Case "INT"
                        'Valida los valores por default
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(DGBusqueda.Item(iSp, iRow).Value, ZctTipos.Zct_Int), SqlDbType.Int)
                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(DGBusqueda.Item(iSp, iRow).Value, ZctTipos.Zct_String), SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(DGBusqueda.Item(iSp, iRow).Value, ZctTipos.Zct_Date), SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(DGBusqueda.Item(iSp, iRow).Value, ZctTipos.Zct_Int), SqlDbType.Decimal)
                End Select
            Next
            'Ejecuta el escalar
            PkBusCon.GetScalarSP()
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub DGBusqueda_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles DGBusqueda.CellBeginEdit
        Try
            bCambios = True
            'En caso de que la columna de indice este vacia , lo ignora
            If DGBusqueda.Item(ColIndex, e.RowIndex).Value Is System.DBNull.Value Then Exit Sub
            If DGBusqueda.Item(ColIndex, e.RowIndex).Value.ToString = "" Then
                'Caso contrario le asigna el ultimo dato grabar
                DGBusqueda.Item(ColIndex, e.RowIndex).Value = CType(iIndice, String)
                DGBusqueda.Item(sColNuevo, e.RowIndex).Value = 0
                'Aumenta el indice
                iIndice = iIndice + 1

            End If
            'En caso de que el indice sea cero, cancela la acci�n
            If e.ColumnIndex = 0 Then
                e.Cancel = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub DGBusqueda_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles DGBusqueda.DataError
        'Obtiene el error que a causado el grid
        MsgBox(e.Exception.Message)
        e.Cancel = True
    End Sub

    Private Sub DGBusqueda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGBusqueda.KeyDown
        Try
            'Elimina el elemento seleccionado
            If e.KeyCode = Keys.Delete Then
                If _Permisos.elimina = False Then
                    DGBusqueda.AllowUserToDeleteRows = False
                    MsgBox("no  tiene�permiso para eliminar en esta ventana", MsgBoxStyle.Critical, "Atencion")
                    Return
                Else
                    DGBusqueda.AllowUserToDeleteRows = True
                End If
                If Not (DGBusqueda.Item(0, DGBusqueda.CurrentCell.RowIndex).Value Is Nothing) Then
                    If MsgBox("�Desea eliminar este registro?", MsgBoxStyle.YesNo, "Eliminar") = MsgBoxResult.Yes Then
                        'Elimina el dato 
                        GetData(3, DGBusqueda.CurrentCell.RowIndex)
                        Dim cClas As New Datos.ClassGen
                        cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, sSPName, "D", DGBusqueda.CurrentCell.RowIndex.ToString)
                        'Elimina el registro
                        DGBusqueda.Rows.Remove(DGBusqueda.Rows(DGBusqueda.CurrentCell.RowIndex))
                        'Refresca la pantalla
                        'GetData()
                    End If
                End If

                'En caso de que la columna sea marcada como raiz de un cat�logo, env�a un F3
            ElseIf e.KeyCode = Keys.F3 Then
                If sSPName_Busq <> "" And DGBusqueda.CurrentCell.ColumnIndex = iColBusq Then
                    Dim fBusqueda As New ZctBusqueda
                    fBusqueda.sSPName = sSPName_Busq
                    fBusqueda.sSpVariables = sSpVariables_Busq
                    fBusqueda.ShowDialog(Me)
                    If Not (fBusqueda.iValor Is Nothing) Then
                        DGBusqueda.CurrentCell.Value = fBusqueda.iValor
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub


    Private Sub ZctSOTButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAyuda.Click
        Dim fAyuda As New ZctFrmAyuda
        fAyuda.Texto = "1 Para un nuevo registro, escribe en el rengl�n en blanco y presiona ""Aceptar"" " & vbCrLf & _
                       "2 Para modificar un registro, sobreescribe el texto y presiona ""Aceptar"" " & vbCrLf & _
                       "3 Para eliminar un registro, da click en el mismo y presiona la tecla [Supr] " & vbCrLf & _
                       "4 Para Realizar una b�squeda en las celdas marcadas con un color distinto, presione la tecla [F3] "
        fAyuda.ShowDialog(Me)
    End Sub

    Private Sub CmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancelar.Click

        If bCambios Then
            Try

                'Obtiene los datos del cat�logo
                GetData()
                bCambios = False

                'Asigna el tool tip para la columna de busqueda
                If sSPName_Busq <> "" Then
                    DGBusqueda.Columns(iColBusq).ToolTipText = "Presione F3 para mostrar el cat�logo"
                    DGBusqueda.Columns(iColBusq).DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.InactiveCaptionText
                End If

                'Muestra los tool tips
                DGBusqueda.ShowCellToolTips = True

            Catch ex As Exception
                MsgBox(ex.Message.ToString)
            End Try
        Else
            Me.Dispose()
        End If
    End Sub

    Private Sub DGBusqueda_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGBusqueda.CellContentClick

    End Sub
End Class