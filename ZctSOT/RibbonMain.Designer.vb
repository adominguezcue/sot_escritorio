﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RibbonMain
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RibbonMain))
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.Clientes = New DevExpress.XtraBars.BarButtonItem()
        Me.GalleryDropDown1 = New DevExpress.XtraBars.Ribbon.GalleryDropDown(Me.components)
        Me.BBArticulos = New DevExpress.XtraBars.BarButtonItem()
        Me.BBCategorias = New DevExpress.XtraBars.BarButtonItem()
        Me.BBSubCategorias = New DevExpress.XtraBars.BarButtonItem()
        Me.BBAlmacenes = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.SkinRibbonGalleryBarItem1 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.BBClientes = New DevExpress.XtraBars.BarButtonItem()
        Me.BBEstados = New DevExpress.XtraBars.BarButtonItem()
        Me.BBCiudades = New DevExpress.XtraBars.BarButtonItem()
        Me.BBProveedores = New DevExpress.XtraBars.BarButtonItem()
        Me.BBMarcas = New DevExpress.XtraBars.BarButtonItem()
        Me.BBCaja = New DevExpress.XtraBars.BarButtonItem()
        Me.BBFacturacion = New DevExpress.XtraBars.BarButtonItem()
        Me.BBCuentasXCobrar = New DevExpress.XtraBars.BarButtonItem()
        Me.BBControldeValores = New DevExpress.XtraBars.BarButtonItem()
        Me.BBPedidos = New DevExpress.XtraBars.BarButtonItem()
        Me.BBCompras = New DevExpress.XtraBars.BarButtonItem()
        Me.BBCompraSugerida = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem14 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem15 = New DevExpress.XtraBars.BarButtonItem()
        Me.BBInventario = New DevExpress.XtraBars.BarButtonItem()
        Me.BBIntercambios = New DevExpress.XtraBars.BarButtonItem()
        Me.BBReportesInventarios = New DevExpress.XtraBars.BarButtonItem()
        Me.BBReportesAdministracion = New DevExpress.XtraBars.BarButtonItem()
        Me.BBReportesCompras = New DevExpress.XtraBars.BarButtonItem()
        Me.BBActualizacion = New DevExpress.XtraBars.BarButtonItem()
        Me.BBParametros = New DevExpress.XtraBars.BarButtonItem()
        Me.BBCalendarioFiscal = New DevExpress.XtraBars.BarButtonItem()
        Me.BBUsuarios = New DevExpress.XtraBars.BarButtonItem()
        Me.BBPermisosPorUsuario = New DevExpress.XtraBars.BarButtonItem()
        Me.BBRoles = New DevExpress.XtraBars.BarButtonItem()
        Me.BBNotaDeCredito = New DevExpress.XtraBars.BarButtonItem()
        Me.cmdOrdenTrabajo = New DevExpress.XtraBars.BarButtonItem()
        Me.btnVehiculos = New DevExpress.XtraBars.BarButtonItem()
        Me.cmdMarcas = New DevExpress.XtraBars.BarButtonItem()
        Me.cmdModelos = New DevExpress.XtraBars.BarButtonItem()
        Me.btnRequisiciones = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage4 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup8 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage5 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup10 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GalleryDropDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.BarButtonItem1, Me.Clientes, Me.BBArticulos, Me.BBCategorias, Me.BBSubCategorias, Me.BBAlmacenes, Me.BarButtonItem6, Me.BarButtonItem7, Me.SkinRibbonGalleryBarItem1, Me.BBClientes, Me.BBEstados, Me.BBCiudades, Me.BBProveedores, Me.BBMarcas, Me.BBCaja, Me.BBFacturacion, Me.BBCuentasXCobrar, Me.BBControldeValores, Me.BBPedidos, Me.BBCompras, Me.BBCompraSugerida, Me.BarButtonItem14, Me.BarButtonItem15, Me.BBInventario, Me.BBIntercambios, Me.BBReportesInventarios, Me.BBReportesAdministracion, Me.BBReportesCompras, Me.BBActualizacion, Me.BBParametros, Me.BBCalendarioFiscal, Me.BBUsuarios, Me.BBPermisosPorUsuario, Me.BBRoles, Me.BBNotaDeCredito, Me.cmdOrdenTrabajo, Me.btnVehiculos, Me.cmdMarcas, Me.cmdModelos, Me.btnRequisiciones, Me.BarButtonItem2, Me.BarButtonItem3})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 40
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage2, Me.RibbonPage1, Me.RibbonPage4, Me.RibbonPage5})
        Me.RibbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013
        Me.RibbonControl.Size = New System.Drawing.Size(753, 143)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.ActAsDropDown = True
        Me.BarButtonItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.BarButtonItem1.Caption = "Clientes"
        Me.BarButtonItem1.Glyph = CType(resources.GetObject("BarButtonItem1.Glyph"), System.Drawing.Image)
        Me.BarButtonItem1.Id = 1
        Me.BarButtonItem1.LargeGlyph = CType(resources.GetObject("BarButtonItem1.LargeGlyph"), System.Drawing.Image)
        Me.BarButtonItem1.Name = "BarButtonItem1"
        Me.BarButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'Clientes
        '
        Me.Clientes.ActAsDropDown = True
        Me.Clientes.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.Clientes.Caption = "Clientes"
        Me.Clientes.DropDownControl = Me.GalleryDropDown1
        Me.Clientes.Glyph = Global.ZctSOT.My.Resources.Resources.customer_16x16
        Me.Clientes.Id = 2
        Me.Clientes.LargeGlyph = Global.ZctSOT.My.Resources.Resources.customer_32x32
        Me.Clientes.Name = "Clientes"
        Me.Clientes.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'GalleryDropDown1
        '
        Me.GalleryDropDown1.Name = "GalleryDropDown1"
        Me.GalleryDropDown1.Ribbon = Me.RibbonControl
        '
        'BBArticulos
        '
        Me.BBArticulos.Caption = "Artículos"
        Me.BBArticulos.Glyph = Global.ZctSOT.My.Resources.Resources.packageproduct_16x16
        Me.BBArticulos.Id = 3
        Me.BBArticulos.LargeGlyph = Global.ZctSOT.My.Resources.Resources.packageproduct_32x32
        Me.BBArticulos.Name = "BBArticulos"
        Me.BBArticulos.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'BBCategorias
        '
        Me.BBCategorias.Caption = "Categorias"
        Me.BBCategorias.Id = 4
        Me.BBCategorias.Name = "BBCategorias"
        '
        'BBSubCategorias
        '
        Me.BBSubCategorias.Caption = "SubCategoría"
        Me.BBSubCategorias.Id = 5
        Me.BBSubCategorias.Name = "BBSubCategorias"
        '
        'BBAlmacenes
        '
        Me.BBAlmacenes.Caption = "Almacenes"
        Me.BBAlmacenes.Glyph = Global.ZctSOT.My.Resources.Resources.contentautoarrange_16x16
        Me.BBAlmacenes.Id = 6
        Me.BBAlmacenes.LargeGlyph = Global.ZctSOT.My.Resources.Resources.contentautoarrange_32x32
        Me.BBAlmacenes.Name = "BBAlmacenes"
        Me.BBAlmacenes.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Proveedores"
        Me.BarButtonItem6.Id = 7
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "Grabar"
        Me.BarButtonItem7.Glyph = Global.ZctSOT.My.Resources.Resources.save_16x16
        Me.BarButtonItem7.Id = 8
        Me.BarButtonItem7.LargeGlyph = Global.ZctSOT.My.Resources.Resources.save_32x32
        Me.BarButtonItem7.Name = "BarButtonItem7"
        Me.BarButtonItem7.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large
        '
        'SkinRibbonGalleryBarItem1
        '
        Me.SkinRibbonGalleryBarItem1.Caption = "Cambiar diseño de pantallas"
        Me.SkinRibbonGalleryBarItem1.Id = 1
        Me.SkinRibbonGalleryBarItem1.Name = "SkinRibbonGalleryBarItem1"
        '
        'BBClientes
        '
        Me.BBClientes.Caption = "Clientes"
        Me.BBClientes.Glyph = CType(resources.GetObject("BBClientes.Glyph"), System.Drawing.Image)
        Me.BBClientes.Id = 2
        Me.BBClientes.LargeGlyph = CType(resources.GetObject("BBClientes.LargeGlyph"), System.Drawing.Image)
        Me.BBClientes.Name = "BBClientes"
        '
        'BBEstados
        '
        Me.BBEstados.Caption = "Estados"
        Me.BBEstados.Id = 3
        Me.BBEstados.Name = "BBEstados"
        '
        'BBCiudades
        '
        Me.BBCiudades.Caption = "Ciudades"
        Me.BBCiudades.Id = 4
        Me.BBCiudades.Name = "BBCiudades"
        '
        'BBProveedores
        '
        Me.BBProveedores.Caption = "Proveedores"
        Me.BBProveedores.Glyph = CType(resources.GetObject("BBProveedores.Glyph"), System.Drawing.Image)
        Me.BBProveedores.Id = 5
        Me.BBProveedores.LargeGlyph = CType(resources.GetObject("BBProveedores.LargeGlyph"), System.Drawing.Image)
        Me.BBProveedores.Name = "BBProveedores"
        '
        'BBMarcas
        '
        Me.BBMarcas.Caption = "Marcas"
        Me.BBMarcas.Id = 6
        Me.BBMarcas.Name = "BBMarcas"
        '
        'BBCaja
        '
        Me.BBCaja.Caption = "Caja"
        Me.BBCaja.Glyph = CType(resources.GetObject("BBCaja.Glyph"), System.Drawing.Image)
        Me.BBCaja.Id = 7
        Me.BBCaja.LargeGlyph = CType(resources.GetObject("BBCaja.LargeGlyph"), System.Drawing.Image)
        Me.BBCaja.Name = "BBCaja"
        '
        'BBFacturacion
        '
        Me.BBFacturacion.AccessibleDescription = ""
        Me.BBFacturacion.Caption = "Facturación"
        Me.BBFacturacion.Glyph = CType(resources.GetObject("BBFacturacion.Glyph"), System.Drawing.Image)
        Me.BBFacturacion.Id = 8
        Me.BBFacturacion.LargeGlyph = CType(resources.GetObject("BBFacturacion.LargeGlyph"), System.Drawing.Image)
        Me.BBFacturacion.Name = "BBFacturacion"
        '
        'BBCuentasXCobrar
        '
        Me.BBCuentasXCobrar.Caption = "Cuentas por Cobrar"
        Me.BBCuentasXCobrar.Glyph = CType(resources.GetObject("BBCuentasXCobrar.Glyph"), System.Drawing.Image)
        Me.BBCuentasXCobrar.Id = 9
        Me.BBCuentasXCobrar.LargeGlyph = CType(resources.GetObject("BBCuentasXCobrar.LargeGlyph"), System.Drawing.Image)
        Me.BBCuentasXCobrar.Name = "BBCuentasXCobrar"
        '
        'BBControldeValores
        '
        Me.BBControldeValores.Caption = "Control de valores"
        Me.BBControldeValores.Glyph = CType(resources.GetObject("BBControldeValores.Glyph"), System.Drawing.Image)
        Me.BBControldeValores.Id = 10
        Me.BBControldeValores.LargeGlyph = CType(resources.GetObject("BBControldeValores.LargeGlyph"), System.Drawing.Image)
        Me.BBControldeValores.Name = "BBControldeValores"
        '
        'BBPedidos
        '
        Me.BBPedidos.Caption = "Pedidos"
        Me.BBPedidos.Glyph = CType(resources.GetObject("BBPedidos.Glyph"), System.Drawing.Image)
        Me.BBPedidos.Id = 11
        Me.BBPedidos.LargeGlyph = CType(resources.GetObject("BBPedidos.LargeGlyph"), System.Drawing.Image)
        Me.BBPedidos.Name = "BBPedidos"
        '
        'BBCompras
        '
        Me.BBCompras.Caption = "Compras"
        Me.BBCompras.Glyph = CType(resources.GetObject("BBCompras.Glyph"), System.Drawing.Image)
        Me.BBCompras.Id = 12
        Me.BBCompras.LargeGlyph = CType(resources.GetObject("BBCompras.LargeGlyph"), System.Drawing.Image)
        Me.BBCompras.Name = "BBCompras"
        '
        'BBCompraSugerida
        '
        Me.BBCompraSugerida.Caption = "Compra Sugerida"
        Me.BBCompraSugerida.Glyph = CType(resources.GetObject("BBCompraSugerida.Glyph"), System.Drawing.Image)
        Me.BBCompraSugerida.Id = 13
        Me.BBCompraSugerida.LargeGlyph = CType(resources.GetObject("BBCompraSugerida.LargeGlyph"), System.Drawing.Image)
        Me.BBCompraSugerida.Name = "BBCompraSugerida"
        '
        'BarButtonItem14
        '
        Me.BarButtonItem14.Caption = "Inventario"
        Me.BarButtonItem14.Glyph = CType(resources.GetObject("BarButtonItem14.Glyph"), System.Drawing.Image)
        Me.BarButtonItem14.Id = 14
        Me.BarButtonItem14.LargeGlyph = CType(resources.GetObject("BarButtonItem14.LargeGlyph"), System.Drawing.Image)
        Me.BarButtonItem14.Name = "BarButtonItem14"
        '
        'BarButtonItem15
        '
        Me.BarButtonItem15.Caption = "Intercambio entre almacenes"
        Me.BarButtonItem15.Glyph = CType(resources.GetObject("BarButtonItem15.Glyph"), System.Drawing.Image)
        Me.BarButtonItem15.Id = 15
        Me.BarButtonItem15.LargeGlyph = CType(resources.GetObject("BarButtonItem15.LargeGlyph"), System.Drawing.Image)
        Me.BarButtonItem15.Name = "BarButtonItem15"
        '
        'BBInventario
        '
        Me.BBInventario.Caption = "Inventario"
        Me.BBInventario.Glyph = CType(resources.GetObject("BBInventario.Glyph"), System.Drawing.Image)
        Me.BBInventario.Id = 16
        Me.BBInventario.LargeGlyph = CType(resources.GetObject("BBInventario.LargeGlyph"), System.Drawing.Image)
        Me.BBInventario.Name = "BBInventario"
        '
        'BBIntercambios
        '
        Me.BBIntercambios.Caption = "Intercambio entre almacenes"
        Me.BBIntercambios.Glyph = CType(resources.GetObject("BBIntercambios.Glyph"), System.Drawing.Image)
        Me.BBIntercambios.Id = 17
        Me.BBIntercambios.LargeGlyph = CType(resources.GetObject("BBIntercambios.LargeGlyph"), System.Drawing.Image)
        Me.BBIntercambios.Name = "BBIntercambios"
        '
        'BBReportesInventarios
        '
        Me.BBReportesInventarios.Caption = "Reportes"
        Me.BBReportesInventarios.Glyph = CType(resources.GetObject("BBReportesInventarios.Glyph"), System.Drawing.Image)
        Me.BBReportesInventarios.Id = 18
        Me.BBReportesInventarios.LargeGlyph = CType(resources.GetObject("BBReportesInventarios.LargeGlyph"), System.Drawing.Image)
        Me.BBReportesInventarios.Name = "BBReportesInventarios"
        '
        'BBReportesAdministracion
        '
        Me.BBReportesAdministracion.Caption = "Reportes"
        Me.BBReportesAdministracion.Glyph = CType(resources.GetObject("BBReportesAdministracion.Glyph"), System.Drawing.Image)
        Me.BBReportesAdministracion.Id = 19
        Me.BBReportesAdministracion.LargeGlyph = CType(resources.GetObject("BBReportesAdministracion.LargeGlyph"), System.Drawing.Image)
        Me.BBReportesAdministracion.Name = "BBReportesAdministracion"
        '
        'BBReportesCompras
        '
        Me.BBReportesCompras.Caption = "Reportes"
        Me.BBReportesCompras.Glyph = CType(resources.GetObject("BBReportesCompras.Glyph"), System.Drawing.Image)
        Me.BBReportesCompras.Id = 20
        Me.BBReportesCompras.LargeGlyph = CType(resources.GetObject("BBReportesCompras.LargeGlyph"), System.Drawing.Image)
        Me.BBReportesCompras.Name = "BBReportesCompras"
        '
        'BBActualizacion
        '
        Me.BBActualizacion.Caption = "Actualización"
        Me.BBActualizacion.Glyph = CType(resources.GetObject("BBActualizacion.Glyph"), System.Drawing.Image)
        Me.BBActualizacion.Id = 21
        Me.BBActualizacion.LargeGlyph = CType(resources.GetObject("BBActualizacion.LargeGlyph"), System.Drawing.Image)
        Me.BBActualizacion.Name = "BBActualizacion"
        '
        'BBParametros
        '
        Me.BBParametros.Caption = "Parametros"
        Me.BBParametros.Glyph = CType(resources.GetObject("BBParametros.Glyph"), System.Drawing.Image)
        Me.BBParametros.Id = 22
        Me.BBParametros.LargeGlyph = CType(resources.GetObject("BBParametros.LargeGlyph"), System.Drawing.Image)
        Me.BBParametros.Name = "BBParametros"
        '
        'BBCalendarioFiscal
        '
        Me.BBCalendarioFiscal.Caption = "Calendario Fiscal"
        Me.BBCalendarioFiscal.Glyph = CType(resources.GetObject("BBCalendarioFiscal.Glyph"), System.Drawing.Image)
        Me.BBCalendarioFiscal.Id = 23
        Me.BBCalendarioFiscal.LargeGlyph = CType(resources.GetObject("BBCalendarioFiscal.LargeGlyph"), System.Drawing.Image)
        Me.BBCalendarioFiscal.Name = "BBCalendarioFiscal"
        '
        'BBUsuarios
        '
        Me.BBUsuarios.Caption = "Usuarios"
        Me.BBUsuarios.Glyph = CType(resources.GetObject("BBUsuarios.Glyph"), System.Drawing.Image)
        Me.BBUsuarios.Id = 24
        Me.BBUsuarios.LargeGlyph = CType(resources.GetObject("BBUsuarios.LargeGlyph"), System.Drawing.Image)
        Me.BBUsuarios.Name = "BBUsuarios"
        '
        'BBPermisosPorUsuario
        '
        Me.BBPermisosPorUsuario.Caption = "Permisos por usuario"
        Me.BBPermisosPorUsuario.Glyph = CType(resources.GetObject("BBPermisosPorUsuario.Glyph"), System.Drawing.Image)
        Me.BBPermisosPorUsuario.Id = 25
        Me.BBPermisosPorUsuario.LargeGlyph = CType(resources.GetObject("BBPermisosPorUsuario.LargeGlyph"), System.Drawing.Image)
        Me.BBPermisosPorUsuario.Name = "BBPermisosPorUsuario"
        '
        'BBRoles
        '
        Me.BBRoles.Caption = "Roles"
        Me.BBRoles.Glyph = CType(resources.GetObject("BBRoles.Glyph"), System.Drawing.Image)
        Me.BBRoles.Id = 26
        Me.BBRoles.LargeGlyph = CType(resources.GetObject("BBRoles.LargeGlyph"), System.Drawing.Image)
        Me.BBRoles.Name = "BBRoles"
        '
        'BBNotaDeCredito
        '
        Me.BBNotaDeCredito.Caption = "Notas de Credito"
        Me.BBNotaDeCredito.Glyph = CType(resources.GetObject("BBNotaDeCredito.Glyph"), System.Drawing.Image)
        Me.BBNotaDeCredito.Id = 28
        Me.BBNotaDeCredito.LargeGlyph = CType(resources.GetObject("BBNotaDeCredito.LargeGlyph"), System.Drawing.Image)
        Me.BBNotaDeCredito.Name = "BBNotaDeCredito"
        '
        'cmdOrdenTrabajo
        '
        Me.cmdOrdenTrabajo.Caption = "Orden de trabajo"
        Me.cmdOrdenTrabajo.Glyph = CType(resources.GetObject("cmdOrdenTrabajo.Glyph"), System.Drawing.Image)
        Me.cmdOrdenTrabajo.Id = 29
        Me.cmdOrdenTrabajo.LargeGlyph = CType(resources.GetObject("cmdOrdenTrabajo.LargeGlyph"), System.Drawing.Image)
        Me.cmdOrdenTrabajo.Name = "cmdOrdenTrabajo"
        '
        'btnVehiculos
        '
        Me.btnVehiculos.Caption = "Vehículos"
        Me.btnVehiculos.Glyph = CType(resources.GetObject("btnVehiculos.Glyph"), System.Drawing.Image)
        Me.btnVehiculos.Id = 30
        Me.btnVehiculos.LargeGlyph = CType(resources.GetObject("btnVehiculos.LargeGlyph"), System.Drawing.Image)
        Me.btnVehiculos.Name = "btnVehiculos"
        '
        'cmdMarcas
        '
        Me.cmdMarcas.Caption = "Marcas"
        Me.cmdMarcas.Id = 31
        Me.cmdMarcas.Name = "cmdMarcas"
        '
        'cmdModelos
        '
        Me.cmdModelos.Caption = "Modelos"
        Me.cmdModelos.Id = 32
        Me.cmdModelos.Name = "cmdModelos"
        '
        'btnRequisiciones
        '
        Me.btnRequisiciones.Caption = "Requisiciones"
        Me.btnRequisiciones.Id = 35
        Me.btnRequisiciones.Name = "btnRequisiciones"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Entradas y salidas"
        Me.BarButtonItem2.Id = 37
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Gestión de recetas"
        Me.BarButtonItem3.Id = 39
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2, Me.RibbonPageGroup3})
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "Inventarios"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BBArticulos)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BBAlmacenes)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BBCategorias)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BBSubCategorias)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BBMarcas)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem3)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem2)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Articulos"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BBInventario)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BBIntercambios)
        Me.RibbonPageGroup3.ItemLinks.Add(Me.BBReportesInventarios)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "Inventarios"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1, Me.RibbonPageGroup7})
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Ventas"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BBClientes)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BBEstados)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BBCiudades)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BBCuentasXCobrar)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.btnRequisiciones)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Datos Clientes"
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.ItemLinks.Add(Me.BBCaja)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.BBFacturacion)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.BBControldeValores)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.BBPedidos)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.BBNotaDeCredito)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.BBReportesAdministracion)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.cmdOrdenTrabajo)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.btnVehiculos)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.cmdMarcas)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.cmdModelos)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.Text = "Administración"
        '
        'RibbonPage4
        '
        Me.RibbonPage4.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup6, Me.RibbonPageGroup8})
        Me.RibbonPage4.Name = "RibbonPage4"
        Me.RibbonPage4.Text = "Compras"
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.BBProveedores)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.Text = "Proveedores"
        '
        'RibbonPageGroup8
        '
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BBCompras)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BBCompraSugerida)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.BBReportesCompras)
        Me.RibbonPageGroup8.Name = "RibbonPageGroup8"
        Me.RibbonPageGroup8.Text = "Compras"
        '
        'RibbonPage5
        '
        Me.RibbonPage5.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup5, Me.RibbonPageGroup10})
        Me.RibbonPage5.Name = "RibbonPage5"
        Me.RibbonPage5.Text = "Sistema"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BBActualizacion)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BBParametros)
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BBCalendarioFiscal)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "Parámetros"
        '
        'RibbonPageGroup10
        '
        Me.RibbonPageGroup10.ItemLinks.Add(Me.BBUsuarios)
        Me.RibbonPageGroup10.ItemLinks.Add(Me.BBPermisosPorUsuario)
        Me.RibbonPageGroup10.ItemLinks.Add(Me.BBRoles)
        Me.RibbonPageGroup10.Name = "RibbonPageGroup10"
        Me.RibbonPageGroup10.Text = "Seguridad"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.ItemLinks.Add(Me.SkinRibbonGalleryBarItem1)
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 418)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(753, 31)
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "Otros"
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.BarButtonItem14)
        Me.RibbonPageGroup9.ItemLinks.Add(Me.BarButtonItem15)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.Text = "Inventarios"
        '
        'RibbonMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(753, 449)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.IsMdiContainer = True
        Me.Name = "RibbonMain"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "SOT"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GalleryDropDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents Clientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GalleryDropDown1 As DevExpress.XtraBars.Ribbon.GalleryDropDown
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BBArticulos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBCategorias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBSubCategorias As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBAlmacenes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage4 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents SkinRibbonGalleryBarItem1 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
    Friend WithEvents BBClientes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBEstados As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBCiudades As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBProveedores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBMarcas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPage5 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents BBCaja As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBFacturacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBCuentasXCobrar As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBControldeValores As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BBPedidos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBCompras As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup8 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BBCompraSugerida As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem14 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem15 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BBInventario As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBIntercambios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBReportesInventarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBReportesAdministracion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBReportesCompras As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBActualizacion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBParametros As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBCalendarioFiscal As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBUsuarios As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup10 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BBPermisosPorUsuario As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBRoles As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BBNotaDeCredito As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents cmdOrdenTrabajo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnVehiculos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents cmdMarcas As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents cmdModelos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnRequisiciones As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem


End Class
