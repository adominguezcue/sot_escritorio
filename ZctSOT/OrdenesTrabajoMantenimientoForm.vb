Imports System.Data.SqlDbType
Imports ZctSOT.Datos.ZctFunciones
Imports ZctSOT.Datos
Imports SOTControladores.Controladores
Imports ZctSOT.Constantes
Imports System.Linq
Imports Modelo.Almacen.Constantes

Public Class OrdenesTrabajoMantenimientoForm
    Friend _Controlador As New SOTControladores.Controladores.ControladorPermisos 'Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos 'Permisos.Controlador.Controlador.vistapermisos
    Dim _Guardar As Boolean
    Dim ALMACEN_DEFAULT As Integer = 2
    Dim sTemp As Integer = 0
    'Dim Cod_OT As Integer = 1
    Dim iColArt As Integer = 0                                         'Columna del art�culo
    Dim iColDesc As Integer = 1                                        'Columna de la Descripci�n
    Dim iColAlm As Integer = 2                                         'Columna de la existencia
    Dim iColMot As Integer = 3                                         'Columna de la existencia
    Dim iColExist As Integer = 4                                       'Columna de la existencia
    Dim iColSol As Integer = 5                                         'Columna solicitada
    Dim iColSurt As Integer = 6                                        'Columna del surtido
    Dim iColPrecio As Integer = 7                                      'Columna del Precio
    Dim iColCosto As Integer = 8
    Dim iColSubTotal As Integer = 9                                    'Columna del subtotal
    Dim iColCod As Integer = 10                                         'Columna del c�digo
    Dim iColTipo As Integer = 11

    Dim sStatus As String                                              'Estatus del documento
    Dim sPathRpt As String = "c:\Reportes\ZctRptOT.rpt" '"c:\Reportes\ZctRptOTDet.rpt"
    'Cat�logos de almacenes
    Dim CatAlm As New List(Of Clases.Catalogos.CatAlmacen)
    'Autorizaciones
    Dim sAutorizacion As String = "N"
    Dim sObservaciones As String = ""
    Dim OcFolio As New zctFolios("OT")

    Dim primeraCarga As Boolean = True

    Private Sub ZctOrdComp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not primeraCarga Then
                Exit Sub
            End If
            _Permisos = _Controlador.ObtenerPermisosActuales '(ControladorBase.UsuarioActual.Id, Me.Text)
            With _Permisos
                DGridArticulos.AllowUserToAddRows = .CrearOrdenTrabajo
                DGridArticulos.AllowUserToDeleteRows = .ModificarOrdenTrabajo
                DGridArticulos.ReadOnly = Not (.ModificarOrdenTrabajo Or .CrearOrdenTrabajo)
                _Guardar = IIf(.ModificarOrdenTrabajo = False And .CrearOrdenTrabajo = False And .EliminarOrdenTrabajo = False, False, True)
                cmdAceptar.Enabled = _Guardar
                cmdCancelaOrden.Enabled = _Guardar
            End With

            cmdCancelar.Enabled = True
            'Posici�n Inicial
            Me.Top = 0
            Me.Left = 0
            'Cliente
            'Limpia la descripci�n
            txtCliente.ZctSOTLabelDesc1.Text = ""
            'Asigna los datos de la busqueda
            txtCliente.SqlBusqueda = "SELECT [Cod_Cte] as C�digo ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
            'Asigna el nombre del procedimiento almacenado para la busqueda
            txtCliente.SPName = "SP_ZctCatCte"
            'Envia las variables del procedimiento almacenado
            txtCliente.SpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
            'Envia la tabla de referencia al procedimiento almacenado
            txtCliente.Tabla = "ZctCatCte_Busqueda"
            'Limpia la descripci�n
            txtContacto.ZctSOTLabelDesc1.Text = ""
            'Asigna los datos de la busqueda
            txtContacto.SqlBusqueda = "SELECT [Cod_Cte] as C�digo ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
            'Asigna el nombre del procedimiento almacenado para la busqueda
            txtContacto.SPName = "SP_ZctCatCte"
            'Envia las variables del procedimiento almacenado
            txtContacto.SpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
            'Envia la tabla de referencia al procedimiento almacenado
            txtContacto.Tabla = "ZctCatCte_Busqueda"


            'Oculta el costo 
            'DGridArticulos.Columns(iColCosto).Visible = False
            'DGridArticulos.Columns(iColCod).Visible = False
            CboTpOrden.GetData("ZctCatTpOT")
            'Tecnicos
            'Limpia la descripci�n
            txtTecnico.ZctSOTLabelDesc1.Text = ""
            'Asigna los datos de la busqueda
            txtTecnico.SqlBusqueda = "SELECT [Cod_Tec] as C�digo  , isnull([Nom_Tec], space(0)) + space(1) + isnull([ApPat_Tec], space(0)) + space(1) +  isnull([ApMat_Tec], space(0)) as T�cnico   FROM [ZctSOT].[dbo].[ZctCatTec]"
            'Asigna el nombre del procedimiento almacenado para la busqueda
            txtTecnico.SPName = "SP_ZctCatTecF3"
            'Envia las variables del procedimiento almacenado
            txtTecnico.SpVariables = "@Cod_Tec;INT|@Nom_Tec;VARCHAR|@ApPat_Tec;VARCHAR|@ApMat_Tec;VARCHAR|@ColNuevo;INT"
            'Envia la tabla de referencia al procedimiento almacenado
            txtTecnico.Tabla = "ZctCatTec"

            'GetAlm()
            'cboTipoOrden.GetDataAlfa("ZctCatFolioOT")

            obtiene_folio()

            txtVin.ZctSOTLabelDesc1.Text = ""
            'Asigna los datos de la busqueda
            txtVin.SqlBusqueda = "SELECT Cod_Mot as C�digo, Placas_Mot as Placas, Right(Vin_Mot,6) as Vin, Motor_Mot as Motor  FROM [ZctSOT].[dbo].[ZctCatMot]"
            'Asigna el nombre del procedimiento almacenado para la busqueda
            txtVin.SPName = "SP_ZctCatMotXCte"
            'Envia las variables del procedimiento almacenado
            txtVin.SpVariables = "@Cod_Mot;INT|@Vin_Mot;VARCHAR|@Cod_Cte;INT|@Cod_Mar;INT;@CodMod_Mot;INT|@Placas_Mot;VARCHAR|@Motor_Mot;VARCHAR|@Anno_Mot;INT"
            txtVin.SpVariables = "@Cod_Mot;int|@Vin_Mot;varchar|@Cod_Cte;int|@Cod_Mar;int|@CodMod_Mot;int|@Placas_Mot;varchar|@Motor_Mot;varchar|@Anno_Mot;INT"
            txtVin.SPParametros = "0||0|0|0|||0"
            'Envia la tabla de referencia al procedimiento almacenado
            txtVin.Tabla = "ZctCatMot"
            txtVin.Validar = False


            Dim ColAlmacen As DataGridViewComboBoxColumn = CType(DGridArticulos.Columns("ColAlmacen"), DataGridViewComboBoxColumn)
            ColAlmacen.DataSource = New ListAlmacenes("SP_ZctCatAlm", "@Cod_Alm;int|@Desc_CatAlm;varchar|@ColNuevo;int", "0|0|0")
            ColAlmacen.ValueMember = "CodAlm"
            ColAlmacen.DisplayMember = "DescAlm"

            'El estatus es una alta
            sStatus = "A"
        Finally
            primeraCarga = False
        End Try
    End Sub

    Public Sub obtiene_folio()
        txtFolio.DataBindings.Clear()
        OcFolio = New zctFolios(ClavesFolios.OT)
        txtFolio.Text = OcFolio.Consecutivo 'GetData("Procedimientos_MAX", "ZctEncOT")

    End Sub
    Public Sub CargaOT(_Cod_ot As Integer)
        txtFolio.Text = (_Cod_ot).ToString
        CargaOrden()
    End Sub
    Public Sub CargaOrden(ByVal folio As Integer)
        Try
            If folio <> 0 AndAlso folio <= OcFolio.Consecutivo Then 'GetData("Procedimientos_MAX", "ZctEncOT") Then

                txtFolio.Text = (folio).ToString

                'CargaOrden()
                sStatus = "M"
                If sStatus = "M" Then
                    cmdImprimir.Visible = True
                Else
                    cmdImprimir.Visible = False
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpVariables As String, ByVal sSpValores As String) As DataTable
        Try
            Dim PkBusCon As New Datos.ZctDataBase                         'Objeto de la base de datos
            Dim iSp As Integer                                      'Indice del procedimiento almacenado
            Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
            Dim sValores() As String = Split(sSpValores, "|")
            'Inicia el procedimieto almacenado
            PkBusCon.IniciaProcedimiento(sSpNAME)
            'Agrega el parametro que define que se va a realizar
            PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
            'Obtiene el total de los procedimientos
            Dim iTot As Integer = UBound(sVector)
            'Recorre las variables del procedimiento almacenado
            For iSp = 0 To iTot
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                    Case "INT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Int)
                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_String), SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_Date), SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Decimal)
                    Case "TEXT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_String), SqlDbType.Text)
                End Select
            Next

            Select Case TpConsulta
                'SELECT
                Case 1
                    PkBusCon.InciaDataAdapter()
                    Return PkBusCon.GetReaderSP.Tables("DATOS")
                Case 2
                    'Ejecuta el escalar
                    PkBusCon.GetScalarSP()
                    Return Nothing

                Case 3
                    'Delete
                    PkBusCon.GetScalarSP()
                    Return Nothing
                Case Else
                    Return Nothing
            End Select

        Catch ex As Exception
            'Enviar una exepci�n
            MsgBox(ex.Message.ToString)
            Return Nothing
        End Try
    End Function


    Private Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpVariables As Dictionary(Of String, Object), ByRef PkCon As ZctDataBase) As DataTable


        Dim iSp As Integer                                      'Indice del procedimiento almacenado
        Dim sVector() As String = sSpVariables.Keys.ToArray() 'Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
        Dim sValores() As Object = sSpVariables.Values.ToArray() 'Split(sSpValores, "|")
        'Inicia el procedimieto almacenado
        PkCon.IniciaProcedimiento(sSpNAME)
        'Agrega el parametro que define que se va a realizar
        PkCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
        'Obtiene el total de los procedimientos
        Dim iTot As Integer = UBound(sVector)
        'Recorre las variables del procedimiento almacenado
        For iSp = 0 To iTot
            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                Case "INT"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Int)
                Case "VARCHAR"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_String), SqlDbType.VarChar)
                Case "SMALLDATETIME"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Date), SqlDbType.SmallDateTime)
                Case "DECIMAL"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Decimal)
                Case "TEXT"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefaultV2(sValores(iSp), ZctTipos.Zct_String), SqlDbType.Text)
            End Select
        Next

        Select Case TpConsulta
            'SELECT
            Case 2
                'Ejecuta el escalar
                PkCon.GetScalarSPTran()
                Return Nothing


            Case Else
                Return Nothing
        End Select
    End Function
    'Busca el valor del indice de la tabla en cuesti�n
    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String)
        Try
            Dim PkBusCon As New Datos.ZctDataBase                             'Objeto de la base de datos
            'Inicializa el procedimietno almacenado
            PkBusCon.IniciaProcedimiento(Procedimiento)
            'Asigna el nombre de la tabla 
            PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
            'Asigna el resultado de la consulta a el objeto
            Dim a As Object = PkBusCon.GetScalarSP()
            'En caso de que no este vac�o asigna su valor a la columna de indice
            If Not a Is System.DBNull.Value Then
                Return CType(a, Integer)
            Else
                Return 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return 1
        End Try
    End Function

    'Busca el valor del indice de la tabla en cuesti�n
    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String, ByVal PkCon As ZctDataBase)

        'Objeto de la base de datos
        'Inicializa el procedimietno almacenado
        PkCon.IniciaProcedimiento(Procedimiento)
        'Asigna el nombre de la tabla 
        PkCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
        'Asigna el resultado de la consulta a el objeto
        Dim a As Object = PkCon.GetScalarSPTran()
        'En caso de que no este vac�o asigna su valor a la columna de indice
        If Not a Is System.DBNull.Value Then
            Return CType(a, Integer)
        Else
            Return 1
        End If
    End Function

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If sStatus = "A" Then
            Me.Dispose()
        Else
            plimpia(True)
            sStatus = "A"
            cmdImprimir.Visible = False
        End If
    End Sub
    Private Sub plimpia(ByVal LimpiaFolio As Boolean)
        'AHL 12/03/2008
        'Valida que la limpieza renueve o no el folio
        If LimpiaFolio Then
            txtFolio.DataBindings.Clear()
            txtFolio.Text = ""
            txtFolio.Text = OcFolio.Consecutivo 'GetData("Procedimientos_MAX", "ZctEncOT")
            txtFolio.Focus()
            'AHL 12/03/2008
            'Vuelve a habilitar el folio
            'ZCTFolio.Enabled = True
            txtFolio.Enabled = True
        End If

        'txtVin.DataBindings.Clear()
        'txtVin.Text = ""
        'txtVin.ZctSOTLabelDesc1.Text = ""
        sAutorizacion = "N"
        sObservaciones = ""
        ZctHistAprov.Text = "Pendiente de aprobar"
        ZctHistAprov.BackColor = lblPorSurtir.BackColor
        chkAprobada.Checked = False

        DGridArticulos.Enabled = True
        cmdAceptar.Enabled = True
        chkAprobada.Enabled = True
        cmdCancelaOrden.Enabled = True
        cmdImprimir.Enabled = True

        txtCliente.DataBindings.Clear()
        txtCliente.Text = ""
        txtCliente.ZctSOTLabelDesc1.Text = ""

        txtTecnico.Text = 0
        txtTecnico.ZctSOTLabelDesc1.Text = ""


        DTEntrada.DataBindings.Clear()
        DTSalida.DataBindings.Clear()
        'DTRmpEntrada.DataBindings.Clear()
        'DTRmpSalida.DataBindings.Clear()
        txtTrabajo.DataBindings.Clear()

        'lblSubTotal.Text = FormatCurrency(0)
        'lblIva.Text = FormatCurrency(0)
        lblTotal.Text = FormatCurrency(0)

        txtKm.DataBindings.Clear()
        txtKm.Text = ""

        txtTrabajo.Text = ""
        DGridArticulos.Rows.Clear()
        txtFolio.Focus()

    End Sub

    Private Sub DGridArticulos_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles DGridArticulos.CellBeginEdit
        'If sAutorizacion = "A" OrElse sAutorizacion = "C" Then e.Cancel = True : Exit Sub
        If sAutorizacion = "C" Then e.Cancel = True : Exit Sub
        If e.ColumnIndex <> iColArt Then
            'Valida que el art�culo no este vac�o
            If DGridArticulos.CurrentRow.Cells(iColArt).Value Is Nothing OrElse DGridArticulos.CurrentRow.Cells(iColArt).Value Is DBNull.Value OrElse DGridArticulos.CurrentRow.Cells(iColArt).Value.ToString.Trim = "" Then e.Cancel = True : Exit Sub
        End If
        If e.ColumnIndex = iColSurt Then
            If DGridArticulos.Item(iColSurt, e.RowIndex).Value Is DBNull.Value Or DGridArticulos.Item(iColSurt, e.RowIndex).Value Is Nothing Then Exit Sub
            sTemp = CType(IIf(DGridArticulos.Item(iColSurt, e.RowIndex).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, e.RowIndex).Value), Integer)
        ElseIf e.ColumnIndex = iColAlm And DGridArticulos.Item(iColTipo, e.RowIndex).Value IsNot DBNull.Value AndAlso DGridArticulos.Item(iColTipo, e.RowIndex).Value = "MO" Then
            e.Cancel = True
        ElseIf e.ColumnIndex = iColArt And (DGridArticulos.CurrentRow.Cells(iColCod).Value IsNot Nothing AndAlso IsNumeric(DGridArticulos.CurrentRow.Cells(iColCod).Value) AndAlso CInt(DGridArticulos.CurrentRow.Cells(iColCod).Value) > 0) Then
            e.Cancel = True
        End If
    End Sub

    Private Sub DGridArticulos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellEndEdit
        Try
            If e.ColumnIndex = iColArt Then
                If DGridArticulos.CurrentCell.Value <> "" Then
                    Dim PkBusCon As New Datos.ZctDataBase
                    'Inicia el procedimiento almacenado
                    PkBusCon.IniciaProcedimiento("SP_ZctCatArt_Busqueda")
                    'Busca los datos del c�digo ingresado
                    PkBusCon.AddParameterSP("@Codigo", DGridArticulos.CurrentCell.Value, SqlDbType.VarChar)
                    'Inicia el Adaptador de datos
                    PkBusCon.InciaDataAdapter()
                    'Obtiene los datos
                    Dim dtDatos As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")
                    'Obtiene los datos del grid
                    If dtDatos.Rows.Count <= 0 Then
                        MsgBox("El c�digo de art�culo no existe, verifique por favor.", MsgBoxStyle.Information, "SOT")
                        'DGridArticulos.Rows.Remove(DGridArticulos.Rows(e.RowIndex))
                        'AHL 12/03/2008
                        DGridArticulos.CurrentRow.Cells(iColArt).Value = ""
                        Exit Sub
                    End If
                    DGridArticulos.CurrentRow.Cells(iColArt).Value = dtDatos.Rows.Item(0).Item("Cod_Art").ToString
                    DGridArticulos.CurrentRow.Cells(iColDesc).Value = dtDatos.Rows.Item(0).Item("Desc_Art").ToString
                    DGridArticulos.CurrentRow.Cells(iColSurt).Value = dtDatos.Rows.Item(0).Item("ColSurt").ToString
                    DGridArticulos.CurrentRow.Cells(iColPrecio).Value = dtDatos.Rows.Item(0).Item("Prec_Art").ToString
                    DGridArticulos.CurrentRow.Cells(iColTipo).Value = dtDatos.Rows.Item(0).Item("Cod_TpArt").ToString
                    DGridArticulos.CurrentRow.Cells(iColCosto).Value = "0"
                    CalculaExistencia(e.RowIndex)
                    DGridArticulos.CurrentRow.Cells(iColCosto).Value = FormatCurrency(DGridArticulos.CurrentRow.Cells(iColCosto).Value)
                    DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColCosto).Value, Decimal)), String))

                    'DGridArticulos.Columns(iColCosto).DefaultCellStyle.Format = "C2"
                    'DGridArticulos.Columns(iColSubTotal).DefaultCellStyle.Format = "C2"

                    dtDatos.Dispose()
                    sGetTotal()

                End If
            ElseIf iColMot = e.ColumnIndex Then
                Dim PkBusCon As New Datos.ZctDataBase
                'Inicia el procedimiento almacenado
                PkBusCon.IniciaProcedimiento("SP_ZctCatMotXCte_Busqueda")
                'Busca los datos del c�digo ingresado
                If txtCliente.Text = "" Then txtCliente.Text = "0"
                If DGridArticulos.CurrentCell.Value = "" Then Exit Sub

                PkBusCon.AddParameterSP("@Cod_Cte", CInt(txtCliente.Text), SqlDbType.Int)
                PkBusCon.AddParameterSP("@Cod_Mot", CInt(DGridArticulos.CurrentCell.Value), SqlDbType.Int)

                'Inicia el Adaptador de datos
                PkBusCon.InciaDataAdapter()
                'Obtiene los datos
                Dim dtDatos As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")
                'Obtiene los datos del grid
                If dtDatos.Rows.Count <= 0 Then
                    MsgBox("La motocicleta no existe para el cliente dado, verifique por favor.", MsgBoxStyle.Information, "SOT")
                    'DGridArticulos.Rows.Remove(DGridArticulos.Rows(e.RowIndex))
                    'AHL 12/03/2008
                    DGridArticulos.CurrentRow.Cells(iColMot).Value = ""
                    Exit Sub
                End If

            ElseIf iColSurt = e.ColumnIndex Or iColSol = e.ColumnIndex Then
                pSetColor(e.RowIndex)

                'Si es mano de obra el surtido iguala a lo solicitado
                If DGridArticulos.Item(iColTipo, e.RowIndex).Value IsNot DBNull.Value AndAlso DGridArticulos.Item(iColTipo, e.RowIndex).Value = "MO" Then
                    If iColSurt = e.ColumnIndex Then
                        DGridArticulos.Item(iColSol, e.RowIndex).Value = DGridArticulos.Item(iColSurt, e.RowIndex).Value
                    Else
                        DGridArticulos.Item(iColSurt, e.RowIndex).Value = DGridArticulos.Item(iColSol, e.RowIndex).Value
                    End If
                End If

                If iColSurt = e.ColumnIndex Then
                    If CType(IIf(DGridArticulos.Item(iColSurt, e.RowIndex).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, e.RowIndex).Value), Integer) <= CType(IIf(DGridArticulos.Item(iColExist, e.RowIndex).Value = "", 0, DGridArticulos.Item(iColExist, e.RowIndex).Value), Integer) OrElse (DGridArticulos.Item(iColTipo, e.RowIndex).Value IsNot DBNull.Value AndAlso DGridArticulos.Item(iColTipo, e.RowIndex).Value = "MO") Then
                        DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColCosto).Value, Decimal)), String))
                        sGetTotal()
                    Else
                        MsgBox("La cantidad surtida debe ser menor o igual a las existencias, se necesita una autorizaci�n para continuar.", MsgBoxStyle.Exclamation)
                        'Dim fLogin As New ZctLogin
                        ''Autorizaci�n para surtir sin existencia
                        'fLogin.Cod_Aut = 1
                        'fLogin.ShowDialog(Me)

                        Dim controlador = New SOTControladores.Controladores.ControladorPermisos()
                        If controlador.VerificarPermisos(New Modelo.Seguridad.Dtos.DtoPermisos With {.SalidaSinInventario = True}, True) = True Then
                            DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColCosto).Value, Decimal)), String))
                            sGetTotal()
                        Else
                            DGridArticulos.Item(iColSurt, e.RowIndex).Value = sTemp
                        End If


                    End If

                End If
            ElseIf iColAlm = e.ColumnIndex Then
                CalculaExistencia(e.RowIndex)
            ElseIf iColCosto = e.ColumnIndex Then
                If DGridArticulos.CurrentRow.Cells(iColCosto).Value = "" OrElse Not IsNumeric(DGridArticulos.CurrentRow.Cells(iColCosto).Value) OrElse CType(DGridArticulos.CurrentRow.Cells(iColCosto).Value, Decimal) < 0 Then
                    DGridArticulos.CurrentRow.Cells(iColCosto).Value = "0"
                End If
                DGridArticulos.CurrentRow.Cells(iColCosto).Value = FormatCurrency(DGridArticulos.CurrentRow.Cells(iColCosto).Value)
                DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = FormatCurrency(CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColCosto).Value, Decimal)), String))
                sGetTotal()

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ZctSotGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGridArticulos.KeyDown
        Try
            ' If sAutorizacion = "A" OrElse sAutorizacion = "C" Then Exit Sub
            If sAutorizacion = "C" Then Exit Sub
            If e.KeyCode = Keys.F3 And DGridArticulos.CurrentCell.ColumnIndex = iColArt And Not (DGridArticulos.CurrentRow.Cells(iColCod).Value IsNot Nothing AndAlso IsNumeric(DGridArticulos.CurrentRow.Cells(iColCod).Value) AndAlso CInt(DGridArticulos.CurrentRow.Cells(iColCod).Value) > 0) Then

                Dim fBusqueda As New ZctBusqueda
                fBusqueda.sSPName = "SP_ZctCatArt"
                fBusqueda.sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT"
                fBusqueda.ShowDialog(Me)
                DGridArticulos.BeginEdit(True)
                'DGridArticulos.CurrentCell.Value = fBusqueda.iValor
                DGridArticulos.CurrentRow.Cells(DGridArticulos.CurrentCell.ColumnIndex).Value = fBusqueda.iValor
                DGridArticulos.CurrentRow.Cells(iColAlm).Value = ALMACEN_DEFAULT
                DGridArticulos.EndEdit(True)
                DGridArticulos.UpdateCellValue(iColArt, DGridArticulos.CurrentCell.RowIndex)
                DGridArticulos.UpdateCellValue(iColAlm, DGridArticulos.CurrentCell.RowIndex)
                ColAlmacen.Selected = True
            ElseIf e.KeyCode = Keys.F3 And DGridArticulos.CurrentCell.ColumnIndex = iColMot Then

                Dim fBusqueda As New ZctBusqueda
                fBusqueda.sSPName = "SP_ZctCatMotXCte"
                fBusqueda.sSpVariables = "@Cod_Mot;int|@Vin_Mot;varchar|@Cod_Cte;int|@Cod_Mar;int|@CodMod_Mot;int|@Placas_Mot;varchar|@Motor_Mot;varchar|@Anno_Mot;INT"
                fBusqueda.sSPParametros = "0||" & txtCliente.Text & "|0|0|||0"

                fBusqueda.ShowDialog(Me)

                DGridArticulos.BeginEdit(True)
                DGridArticulos.CurrentCell.Value = fBusqueda.iValor
                DGridArticulos.EndEdit(True)
                DGridArticulos.UpdateCellValue(iColMot, DGridArticulos.CurrentCell.RowIndex)
                ColSurt.Selected = True
            ElseIf e.KeyCode = Keys.Delete And Not (DGridArticulos.CurrentRow.Cells(iColCod).Value IsNot Nothing AndAlso IsNumeric(DGridArticulos.CurrentRow.Cells(iColCod).Value) AndAlso CInt(DGridArticulos.CurrentRow.Cells(iColCod).Value) > 0) Then
                If DGridArticulos.CurrentRow.IsNewRow Then Exit Sub
                If MsgBox("�Desea eliminar este art�culo?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                DGridArticulos.Rows.Remove(DGridArticulos.CurrentRow)
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub sGetTotal()
        Dim iRow As Integer
        Dim iSubTotal As Decimal = 0
        For iRow = 0 To DGridArticulos.Rows.Count - 1
            'If DGridArticulos.Item(iColSubTotal, iRow).ToString <> "" Then
            iSubTotal = iSubTotal + CType(DGridArticulos.Item(iColSubTotal, iRow).Value, Decimal)

            ' End If
        Next

        'lblSubTotal.Text = FormatCurrency(iSubTotal.ToString)
        'lblIva.Text = FormatCurrency(CType(iSubTotal * ZctSOTMain.ParametrosSistema.Iva_CatPar, String))
        lblTotal.Text = FormatCurrency(iSubTotal.ToString) ' FormatCurrency(CType(iSubTotal + CDbl(lblIva.Text), String))
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        ' Dim drEncOT As DataTable
        'Dim sVariables As String
        Dim sParametros As New Dictionary(Of String, Object)
        Dim PkConAlone As New Datos.ZctDataBase                         'Objeto de la base de datos

        'Valida los datos
        If txtCliente.Text = "" Or txtCliente.Text = 0 Then
            MsgBox("La orden no puede ser grabada si no ha ingresado el c�digo del cliente, verifique por favor.", MsgBoxStyle.Information, "SOT")
            If txtCliente.Enabled Then txtCliente.Focus()
            Exit Sub
        End If

        If DGridArticulos.RowCount <= 0 Then
            MsgBox("La orden no puede ser grabada sin art�culos, verifique por favor.", MsgBoxStyle.Information, "SOT")
            Exit Sub
        Else
            If DGridArticulos.Rows(0).Cells(iColArt).Value Is DBNull.Value Or DGridArticulos.Rows(0).Cells(iColArt).Value Is Nothing Then
                MsgBox("La orden no puede ser grabada sin art�culos, verifique por favor.", MsgBoxStyle.Information, "SOT")
                Exit Sub
            End If
        End If

        'Valida que no se hagan salidas en cero.
        For i As Integer = 0 To DGridArticulos.Rows.Count - 1
            If DGridArticulos.Item(iColArt, i).Value <> "" AndAlso DGridArticulos.Item(iColSurt, i).Value > 0 AndAlso DGridArticulos.Item(iColCosto, i).Value = 0 Then
                MsgBox("El art�culo " & DGridArticulos.Item(iColArt, i).Value & " tiene costo cero, favor de verificar.")
                Exit Sub
            End If
        Next

        Try

            If chkAprobada.Checked And sAutorizacion <> "A" Then
                If MsgBox("�Desea autorizar esta orden de trabajo?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
                'Dim fLogin As New ZctLogin
                ''Autorizaci�n para surtir sin existencia
                'fLogin.Cod_Aut = 4
                'fLogin.ShowDialog(Me)
                Dim controlador = New SOTControladores.Controladores.ControladorPermisos()
                If controlador.VerificarPermisos(New Modelo.Seguridad.Dtos.DtoPermisos With {.AutorizarOrdenTrabajo = True}, True) = False Then
                    MsgBox("Usted no tiene permisos para autorizar las ordenes de trabajo.")
                    Exit Sub
                Else
                    sAutorizacion = "A"
                    sObservaciones = ControladorBase.UsuarioActual.Id & " el d�a " & Now.ToString
                End If
            End If
            'sObservaciones = ControladorBase.UsuarioActual.NombreCompleto & " el d�a " & Now.ToString


            PkConAlone.OpenConTran()
            PkConAlone.Inicia_Transaccion()

            Dim fechaActual = Now.Date

            'TODO: Agregar campo en el detalle con el �c�digo de la moto
            If sStatus = "A" Then
                txtFolio.DataBindings.Clear()
                txtFolio.Text = OcFolio.SetConsecutivo(PkConAlone) 'GetData("Procedimientos_MAX", "ZctEncOT2", PkConAlone)

                sParametros.Add("@CodEnc_OT;INT", txtFolio.Text)
                'sParametros.Add("@Folio;INT", txtFolio.Text)
                sParametros.Add("@FchDoc_OT;SMALLDATETIME", fechaActual)
                sParametros.Add("@Cod_Cte;VARCHAR", CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer))
                sParametros.Add("@Cod_Mot;INT", 0)
                sParametros.Add("@Cod_Tec;INT", 0)
                sParametros.Add("@FchEnt;SMALLDATETIME", DTEntrada.Value)
                sParametros.Add("@FchSal;SMALLDATETIME", DTSalida.Value)
                sParametros.Add("@FchEntRmp;SMALLDATETIME", DTEntrada.Value)
                sParametros.Add("@FchSalRmp;SMALLDATETIME", DTSalida.Value)
                sParametros.Add("@ObsOT;TEXT", txtTrabajo.Text)
                sParametros.Add("@FchAplMov;SMALLDATETIME", DtAplicacion.Value)
                sParametros.Add("@UltKm_OT;INT", 0)
                sParametros.Add("@Cod_Estatus;VARCHAR", sAutorizacion)
                sParametros.Add("@HistEstatus_OT;VARCHAR", "")
                sParametros.Add("@Cod_folio;VARCHAR", ClavesFolios.OT)
                sParametros.Add("@Cod_Contacto;INT", Nothing)



                GetData(2, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sParametros, PkConAlone)
            Else
                'sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & ClavesFolios.OT & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|" & _
                '"0" & "|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text & "|" & sAutorizacion & "|" & sObservaciones & "|" & CboTpOrden.value

                sParametros.Add("@CodEnc_OT;INT", txtFolio.Text)
                'sParametros.Add("@Folio;INT", txtFolio.Text)
                sParametros.Add("@FchDoc_OT;SMALLDATETIME", fechaActual)
                sParametros.Add("@Cod_Cte;VARCHAR", CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer))
                sParametros.Add("@Cod_Mot;INT", 0)
                sParametros.Add("@Cod_Tec;INT", 0)
                sParametros.Add("@FchEnt;SMALLDATETIME", DTEntrada.Value)
                sParametros.Add("@FchSal;SMALLDATETIME", DTSalida.Value)
                sParametros.Add("@FchEntRmp;SMALLDATETIME", DTEntrada.Value)
                sParametros.Add("@FchSalRmp;SMALLDATETIME", DTSalida.Value)
                sParametros.Add("@ObsOT;TEXT", txtTrabajo.Text)
                sParametros.Add("@FchAplMov;SMALLDATETIME", DtAplicacion.Value)
                sParametros.Add("@UltKm_OT;INT", 0)
                sParametros.Add("@Cod_Estatus;VARCHAR", sAutorizacion)
                sParametros.Add("@HistEstatus_OT;VARCHAR", "")
                sParametros.Add("@Cod_folio;VARCHAR", ClavesFolios.OT)
                sParametros.Add("@Cod_Contacto;INT", Nothing)

                GetData(2, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sParametros, PkConAlone)

            End If
            'sVariables = "@Cod_EncOT;INT|@Cod_DetOT;INT|@Cod_Art;VARCHAR|@Ctd_Art;INT|@CosArt_DetOT;DECIMAL|@PreArt_DetOT;DECIMAL|@CtdStd_DetOT;INT|@Fch_DetOT;SMALLDATETIME|@FchAplMov;SMALLDATETIME|@Cat_Alm;INT|@Cod_Mot;INT|Cod_TpArt;VARCHAR"
            Dim iRow As Integer

            For iRow = 0 To DGridArticulos.Rows.Count - 1
                If DGridArticulos.Item(iColCod, iRow).Value Is Nothing Or DGridArticulos.Item(iColCod, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColCod, iRow).Value = 0
                End If

                If DGridArticulos.Item(iColSol, iRow).Value Is Nothing Or DGridArticulos.Item(iColSol, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColSol, iRow).Value = 0
                End If
                If DGridArticulos.Item(iColSurt, iRow).Value Is Nothing Or DGridArticulos.Item(iColSurt, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColSurt, iRow).Value = 0
                End If

                If DGridArticulos.Item(iColTipo, iRow).Value Is Nothing OrElse DGridArticulos.Item(iColTipo, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColTipo, iRow).Value = "ART"
                End If

                'AHL 12/03/2008
                'Valida que el c�digo de art�culo no este vac�o
                If Not (DGridArticulos.Item(iColArt, iRow).Value Is Nothing) AndAlso DGridArticulos.Item(iColArt, iRow).Value <> "" Then
                    sParametros.Clear() ' = ""
                    If (DGridArticulos.Item(iColTipo, iRow).Value IsNot Nothing AndAlso DGridArticulos.Item(iColTipo, iRow).Value.ToString = "MO") Then
                        DGridArticulos.Item(iColAlm, iRow).Value = "NULL"
                    Else
                        If DGridArticulos.Item(iColAlm, iRow).Value Is Nothing OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "" OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "0" Then
                            DGridArticulos.Item(iColAlm, iRow).ErrorText = "El almac�n en la orden de compra no puede estar vacia"
                            Throw New ZctReglaNegocioEx("El almac�n en la orden de compra no puede estar vacia")
                        Else
                            DGridArticulos.Item(iColAlm, iRow).ErrorText = ""
                        End If
                    End If
                    'sParametros = CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer) & "|" & CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColArt, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer) & "|" & Now.Date
                    'sParametros = sParametros & CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer) & "|"
                    'sParametros = sParametros & CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer) & "|"
                    'sParametros = sParametros & DGridArticulos.Item(iColArt, iRow).Value & "|"
                    'sParametros = sParametros & CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer) & "|"
                    'sParametros = sParametros & CDbl(DGridArticulos.Item(iColCosto, iRow).Value) & "|"
                    'sParametros = sParametros & DGridArticulos.Item(iColPrecio, iRow).Value & "|"
                    'sParametros = sParametros & CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer) & "|"
                    'sParametros = sParametros & Now.Date & "|"
                    'sParametros = sParametros & DtAplicacion.Value & "|" & DGridArticulos.Item(iColAlm, iRow).Value.ToString
                    'sParametros = sParametros & "|" & DGridArticulos.Item(iColMot, iRow).Value.ToString
                    'sParametros = sParametros & "|" & DGridArticulos.Item(iColTipo, iRow).Value

                    sParametros.Add("@Cod_EncOT;INT", CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer))
                    sParametros.Add("@Cod_DetOT;INT", CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer))
                    sParametros.Add("@Cod_Art;VARCHAR", DGridArticulos.Item(iColArt, iRow).Value)
                    sParametros.Add("@Ctd_Art;INT", CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer))
                    sParametros.Add("@CosArt_DetOT;DECIMAL", CDbl(DGridArticulos.Item(iColCosto, iRow).Value))
                    sParametros.Add("@PreArt_DetOT;DECIMAL", CDbl(DGridArticulos.Item(iColPrecio, iRow).Value))
                    sParametros.Add("@CtdStd_DetOT;INT", CType(IIf(String.IsNullOrWhiteSpace(DGridArticulos.Item(iColSurt, iRow).Value.ToString), 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer))
                    sParametros.Add("@Fch_DetOT;SMALLDATETIME", fechaActual)
                    sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
                    sParametros.Add("@Cat_Alm;INT", DGridArticulos.Item(iColAlm, iRow).Value)
                    sParametros.Add("@Cod_Mot;INT", IIf(DGridArticulos.Item(iColMot, iRow).Value Is Nothing, "", DGridArticulos.Item(iColMot, iRow).Value))
                    sParametros.Add("@Cod_TpArt;VARCHAR", DGridArticulos.Item(iColTipo, iRow).Value)
                    'sParametros.Add("@cod_usu;INT", DGridArticulos.Item(iColUsu, iRow).Value)

                    GetData(2, ProcedimientosAlmacenados.SP_DETALLE_ORDEN_TRABAJO, sParametros, PkConAlone)
                End If

            Next
            PkConAlone.Termina_Transaccion()
            PkConAlone.CloseConTran()
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdTrabajo", "A", txtFolio.Text)

            'Status 
            If sStatus = "A" Then
                If MsgBox("�Desea imprimir la orden de trabajo?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                    pImprime()
                End If
            End If

            If MsgBox("�Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                sStatus = "A"
                plimpia(True)
            Else
                plimpia(False)
                'Carga el folio nuevamente
                CargaOrden()
            End If
        Catch ex As Exception
            PkConAlone.Cancela_Transaccion()
            PkConAlone.CloseConTran()
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub txtFolio_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolio.lostfocus
        Try
            If txtFolio.Text <> "" And CType(txtFolio.Text, Integer) > 0 Then

                'ZCTFolio.Enabled = False
                txtFolio.Enabled = False
                CargaOrden()

            End If
            'Status 
            If sStatus = "M" Then
                cmdImprimir.Visible = True
            Else
                cmdImprimir.Visible = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub pSetColor(ByVal Renglon As Integer)

        DGridArticulos.Item(iColSurt, Renglon).Value = IIf(DGridArticulos.Item(iColSurt, Renglon).Value Is Nothing, 0, DGridArticulos.Item(iColSurt, Renglon).Value)
        DGridArticulos.Item(iColSol, Renglon).Value = IIf(DGridArticulos.Item(iColSol, Renglon).Value Is Nothing, 0, DGridArticulos.Item(iColSol, Renglon).Value)
        DGridArticulos.Item(iColSol, Renglon).Value = IIf(DGridArticulos.Item(iColSol, Renglon).Value.ToString = "", 0, DGridArticulos.Item(iColSol, Renglon).Value)
        DGridArticulos.Item(iColSurt, Renglon).Value = IIf(DGridArticulos.Item(iColSurt, Renglon).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, Renglon).Value)
        If CType(DGridArticulos.Item(iColSol, Renglon).Value, Integer) = CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) OrElse (DGridArticulos.Item(iColTipo, Renglon).Value IsNot Nothing AndAlso DGridArticulos.Item(iColTipo, Renglon).Value = "MO") Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblSurtido.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblSurtido.BackColor
        ElseIf CType(DGridArticulos.Item(iColSol, Renglon).Value, Integer) > CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblPorSurtir.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblPorSurtir.BackColor
        ElseIf CType(DGridArticulos.Item(iColSol, Renglon).Value, Integer) < CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblDevolucion.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblDevolucion.BackColor

        End If

    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        pImprime()
    End Sub

    Private Sub pImprime()
        If txtFolio.Text = "0" Or txtFolio.Text = "" Then Exit Sub
        Dim myCr As New PkVisorRpt
        myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        myCr.sSQLV = "{ZctEncOT.CodEnc_OT} = " & CType(txtFolio.Text, Integer)

        myCr.sRpt = sPathRpt
        myCr.Show()
    End Sub

    Private Sub DGridArticulos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellContentClick

    End Sub

    Private Sub txtFolio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolio.Load

    End Sub

    Private Sub CargaOrden()

        Dim drEncOT As DataTable
        Dim sVariables As String
        Dim sParametros As String

        'sVariables = "@CodEnc_OT;INT|@Cod_folio;VARCHAR|@Folio;INT|@FchDoc_OT;SMALLDATETIME|@Cod_Cte;VARCHAR|@Cod_Contacto;INT|@Cod_Mot;INT|@Cod_Tec;INT|@FchEnt;SMALLDATETIME|@FchSal;SMALLDATETIME|@FchEntRmp;SMALLDATETIME|@FchSalRmp;SMALLDATETIME|@ObsOT;TEXT|@FchAplMov;SMALLDATETIME|@UltKm_OT;INT|@Cod_Estatus;VARCHAR|@HistEstatus_OT;VARCHAR|@Cod_TpOT;INT"
        sVariables = "@CodEnc_OT;INT|@Cod_folio;VARCHAR|@FchDoc_OT;SMALLDATETIME|@Cod_Cte;VARCHAR|@Cod_Contacto;INT|@Cod_Mot;INT|@Cod_Tec;INT|@FchEnt;SMALLDATETIME|@FchSal;SMALLDATETIME|@FchEntRmp;SMALLDATETIME|@FchSalRmp;SMALLDATETIME|@ObsOT;TEXT|@FchAplMov;SMALLDATETIME|@UltKm_OT;INT|@Cod_Estatus;VARCHAR|@HistEstatus_OT;VARCHAR|@Cod_TpOT;INT"
        '        sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|" & CType(IIf(txtVin.Text = "", 0, txtVin.Text), Integer) & "|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text
        'sParametros = Cod_OT.ToString & "|" & cboTipoOrden.value & "|" & CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|" & txtContacto.Text & "|" & CType(IIf(txtVin.Text = "", 0, txtVin.Text), Integer) & "|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text & "|" & sAutorizacion & "|" & sObservaciones & "|" & CboTpOrden.value
        sParametros = txtFolio.Text & "|" & ClavesFolios.OT & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|" & txtContacto.Text & "|" & CType(IIf(txtVin.Text = "", 0, txtVin.Text), Integer) & "|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text & "|" & sAutorizacion & "|" & sObservaciones & "|" & CboTpOrden.value


        drEncOT = GetData(1, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sVariables, sParametros)
        If Not (drEncOT.Rows.Count = 0) Then
            txtFolio.DataBindings.Clear()
            txtFolio.DataBindings.Add("Text", drEncOT, "Folio")
            'Cod_OT = drEncOT.Rows(0).Item("CodEnc_OT")
            'Obtiene la autorizaci�n
            sAutorizacion = drEncOT.Rows(0).Item("Cod_Estatus").ToString
            sObservaciones = drEncOT.Rows(0).Item("HistEstatus_OT").ToString
            chkAprobada.Checked = False
            Select Case sAutorizacion
                Case "A"
                    ZctHistAprov.Text = "Aprobada"
                    ZctHistAprov.BackColor = lblSurtido.BackColor
                    chkAprobada.Checked = True

                    DGridArticulos.Enabled = True
                    cmdAceptar.Enabled = False
                    chkAprobada.Enabled = False
                    cmdCancelaOrden.Enabled = True
                    cmdImprimir.Enabled = True
                Case "C"
                    ZctHistAprov.Text = "Cancelada"
                    ZctHistAprov.BackColor = lblDevolucion.BackColor

                    DGridArticulos.Enabled = False
                    cmdAceptar.Enabled = False
                    chkAprobada.Enabled = False
                    cmdCancelaOrden.Enabled = False
                    cmdImprimir.Enabled = False
                Case "N"
                    ZctHistAprov.Text = "Pendiente de aprobar"
                    ZctHistAprov.BackColor = lblPorSurtir.BackColor

                    DGridArticulos.Enabled = True
                    cmdAceptar.Enabled = _Guardar
                    chkAprobada.Enabled = True
                    cmdCancelaOrden.Enabled = True
                    cmdImprimir.Enabled = True

            End Select
            If sObservaciones <> "" Then ZctHistAprov.Text &= " por : " & sObservaciones

            txtCliente.DataBindings.Clear()
            txtCliente.DataBindings.Add("Text", drEncOT, "Cod_Cte")
            txtCliente.pCargaDescripcion()


            'txtVin.DataBindings.Clear()
            'txtVin.DataBindings.Add("Text", drEncOT, "Cod_Mot")
            'txtVin.pCargaDescripcion()

            txtTecnico.DataBindings.Clear()
            txtTecnico.DataBindings.Add("Text", drEncOT, "Cod_Tec")
            txtTecnico.pCargaDescripcion()

            DTEntrada.DataBindings.Clear()
            DTEntrada.DataBindings.Add("Value", drEncOT, "FchEnt")

            DTSalida.DataBindings.Clear()
            DTSalida.DataBindings.Add("Value", drEncOT, "FchSal")


            txtKm.DataBindings.Clear()
            txtKm.DataBindings.Add("Text", drEncOT, "UltKm_OT")


            'DTRmpEntrada.DataBindings.Clear()
            'DTRmpEntrada.DataBindings.Add("Value", drEncOT, "FchEntRmp")

            'DTRmpSalida.DataBindings.Clear()
            'DTRmpSalida.DataBindings.Add("Value", drEncOT, "FchSalRmp")

            txtTrabajo.DataBindings.Clear()
            txtTrabajo.DataBindings.Add("Text", drEncOT, "ObsOT")

            DtAplicacion.DataBindings.Clear()
            DtAplicacion.DataBindings.Add("Value", drEncOT, "FchAplMov")

            CboTpOrden.DataBindings.Clear()
            CboTpOrden.DataBindings.Add("ValueItem", drEncOT, "Cod_TpOT", True, DataSourceUpdateMode.OnPropertyChanged, 0)


            'Datos del Grid
            sVariables = "@Cod_EncOT;INT|@Cod_DetOT;INT|@Cod_Art;VARCHAR|@Ctd_Art;INT|@CosArt_DetOT;DECIMAL|@PreArt_DetOT;DECIMAL|@CtdStd_DetOT;INT|@Fch_DetOT;SMALLDATETIME|@FchAplMov;SMALLDATETIME|@Cat_Alm;INT|@Cod_Mot;INT"
            sParametros = txtFolio.Text & "|0||0|0|0|0|" & Now.Date & "|" & DtAplicacion.Value & "|0|0"
            Dim drDetOT As DataTable
            Dim iRowD As Integer

            drDetOT = GetData(1, ProcedimientosAlmacenados.SP_DETALLE_ORDEN_TRABAJO, sVariables, sParametros)

            For iRowD = 0 To drDetOT.Rows.Count - 1
                Dim inRow As New DataGridViewRow

                inRow.CreateCells(DGridArticulos)
                inRow.Cells(iColArt).Value = drDetOT.Rows(iRowD).Item("Cod_Art").ToString
                inRow.Cells(iColDesc).Value = drDetOT.Rows(iRowD).Item("Desc_Art").ToString
                inRow.Cells(iColSol).Value = drDetOT.Rows(iRowD).Item("Ctd_Art").ToString
                inRow.Cells(iColAlm).Value = drDetOT.Rows(iRowD).Item("Cat_Alm")
                inRow.Cells(iColTipo).Value = drDetOT.Rows(iRowD).Item("Cod_TpArt")

                'inRow.Cells(iColExist).Value = drDetOT.Rows(iRowD).Item("Exist_Art").ToString
                inRow.Cells(iColCod).Value = drDetOT.Rows(iRowD).Item("Cod_DetOT").ToString
                inRow.Cells(iColSurt).Value = If(String.IsNullOrEmpty(drDetOT.Rows(iRowD).Item("CtdStd_DetOT").ToString), 0, drDetOT.Rows(iRowD).Item("CtdStd_DetOT").ToString)
                inRow.Cells(iColPrecio).Value = drDetOT.Rows(iRowD).Item("PreArt_DetOT").ToString
                inRow.Cells(iColCosto).Value = FormatCurrency(drDetOT.Rows(iRowD).Item("CosArt_DetOT").ToString)

                inRow.Cells(iColSubTotal).Value = FormatCurrency(inRow.Cells(iColSurt).Value * inRow.Cells(iColCosto).Value)
                inRow.Cells(iColMot).Value = drDetOT.Rows(iRowD).Item("Cod_Mot").ToString
                CalculaExistencia(inRow, False)

                DGridArticulos.Rows.Add(inRow)
            Next
            DGridArticulos.Columns(iColCod).Visible = False

            For iRowD = 0 To drDetOT.Rows.Count - 1
                pSetColor(iRowD)
            Next

            drDetOT.Dispose()
            drEncOT.Dispose()
            sGetTotal()
            'Estatus de modificaci�n
            sStatus = "M"

        End If

    End Sub


    Private Sub DGridArticulos_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles DGridArticulos.UserDeletingRow
        If sStatus = "M" Then
            MsgBox("Esta intentando eliminar un movimiento ya almacenado, por favor ponga en ceros la cantidad surtida si este movimiento esta erroneo.", MsgBoxStyle.Information)
            e.Cancel = True
        End If
    End Sub

    Private Sub DGridArticulos_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles DGridArticulos.DataError
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).ErrorText = e.Exception.Message
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).Selected = False
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).Value = DGridArticulos.Item(e.ColumnIndex, e.RowIndex).DefaultNewRowValue
    End Sub

    Private Sub ValidaExistencia(ByRef Renglon As DataGridViewRow)
        If ZctSOTMain.Inventario_en_marcha(Renglon.Cells(iColAlm).Value) Then
            Renglon.ErrorText = "El almac�n seleccionado esta corriendo inventario y no puede ser utilizado en este momento."
            Renglon.ReadOnly = True
            'cmdAceptar.Enabled = False
            cmdCancelaOrden.Enabled = False
        End If
    End Sub

    Private Sub CalculaExistencia(ByVal Renglon As Integer, Optional ByVal GetCosto As Boolean = True)
        If DGridArticulos.Item(iColArt, Renglon).Value = "" OrElse (DGridArticulos.Item(iColAlm, Renglon).Value Is Nothing OrElse DGridArticulos.Item(iColAlm, Renglon).Value.ToString = "") Then Exit Sub
        ValidaExistencia(DGridArticulos.Rows(Renglon))

        Dim dt As DataTable = Nothing
        Try

            dt = ClassGen.GenGetData("SP_ZctArtXAlm", "@TpConsulta;int|Cod_Art;VarChar|Cod_Alm;Int|Exist_Art;Int|CostoProm_Art;Money", "1|" & DGridArticulos.Item(iColArt, Renglon).Value & "|" & DGridArticulos.Item(iColAlm, Renglon).Value & "|0|0")
            If dt.Rows.Count = 0 Then
                DGridArticulos.Item(iColExist, Renglon).Value = "0"
            Else
                DGridArticulos.Item(iColExist, Renglon).Value = ZctFunciones.GetGeneric(dt.Rows(0)("Exist_Art"))
            End If
            If GetCosto Then
                If DGridArticulos.Item(iColExist, Renglon).Value <> "0" Then
                    DGridArticulos.Item(iColCosto, Renglon).Value = FormatCurrency((ZctFunciones.GetGeneric(Of Decimal)(dt.Rows(0)("CostoProm_Art")) / DGridArticulos.Item(iColExist, Renglon).Value).ToString)
                Else
                    DGridArticulos.Item(iColCosto, Renglon).Value = FormatCurrency("0")
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            dt.Dispose()
        End Try


    End Sub

    Private Sub CalculaExistencia(ByRef Renglon As DataGridViewRow, Optional ByVal GetCosto As Boolean = True)
        If Renglon.Cells(iColArt).Value = "" OrElse (Renglon.Cells(iColAlm).Value Is Nothing OrElse Renglon.Cells(iColAlm).Value.ToString = "") Then Exit Sub
        ValidaExistencia(Renglon)

        Dim dt As DataTable = Nothing
        Try

            dt = ClassGen.GenGetData("SP_ZctArtXAlm", "@TpConsulta;int|Cod_Art;VarChar|Cod_Alm;Int|Exist_Art;Int|CostoProm_Art;Money", "1|" & Renglon.Cells(iColArt).Value & "|" & Renglon.Cells(iColAlm).Value & "|0|0")
            If dt.Rows.Count = 0 Then
                Renglon.Cells(iColExist).Value = "0"
            Else
                Renglon.Cells(iColExist).Value = ZctFunciones.GetGeneric(If(dt.Rows(0)("Exist_Art") Is DBNull.Value, 0, dt.Rows(0)("Exist_Art")))
            End If
            If GetCosto Then
                If Renglon.Cells(iColExist).Value <> "0" Then
                    Renglon.Cells(iColCosto).Value = FormatCurrency((ZctFunciones.GetGeneric(Of Decimal)(dt.Rows(0)("CostoProm_Art")) / Renglon.Cells(iColExist).Value).ToString)
                Else
                    Renglon.Cells(iColCosto).Value = FormatCurrency("0")
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            dt.Dispose()
        End Try


    End Sub

    Private Sub txtCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.Load

    End Sub

    Private Sub txtCliente_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCliente.lostfocus
        'If txtCliente.Text <> "0" AndAlso txtCliente.Text <> "" Then
        '    'ListMotos = New ListaGen(Of zctGenMotos)("SP_ZctCatMotXCte", "@Cod_Mot;INT|@Vin_Mot;VARCHAR|@Cod_Cte;INT|@Cod_Mar;INT|@CodMod_Mot;INT|@Placas_Mot;VARCHAR|@Motor_Mot;VARCHAR|@Anno_Mot;INT", "0|0|" & txtCliente.Text & "|0|0|||0")

        '    Dim ColMotos As DataGridViewComboBoxColumn = CType(DGridArticulos.Columns("ColMoto"), DataGridViewComboBoxColumn)
        '    ColMotos.DataSource = ListMotos.
        '    ColMotos.ValueMember = "Cod_Mot"
        '    ColMotos.DisplayMember = "Vin_Mot"
        'End If
    End Sub

    Private Sub DGridArticulos_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellEnter

    End Sub

    Private Sub cmdIzq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIzq.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) - 1 > 0 Then
            txtFolio.Text = (CInt(txtFolio.Text) - 1).ToString
            plimpia(False)
            CargaOrden()
            'Status 
            If sStatus = "M" And CInt(txtFolio.Text) < OcFolio.Consecutivo Then 'GetData("Procedimientos_MAX", "ZctEncOT") Then
                cmdImprimir.Visible = True
            Else
                sStatus = "A"
                chkAprobada.Checked = False
                cmdImprimir.Visible = False
                DtAplicacion.ClearValue()
                DTEntrada.ClearValue()
                DTSalida.ClearValue()
            End If
        End If

    End Sub

    Private Sub cmdDer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDer.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) + 1 <= OcFolio.Consecutivo Then 'GetData("Procedimientos_MAX", "ZctEncOT") Then
            txtFolio.Text = (CInt(txtFolio.Text) + 1).ToString
            plimpia(False)
            CargaOrden()
            'Status 
            If sStatus = "M" And CInt(txtFolio.Text) < OcFolio.Consecutivo Then 'GetData("Procedimientos_MAX", "ZctEncOT") Then
                cmdImprimir.Visible = True
            Else
                chkAprobada.Checked = False
                sStatus = "A"
                cmdImprimir.Visible = False
                DtAplicacion.ClearValue()
                DTEntrada.ClearValue()
                DTSalida.ClearValue()
            End If
        End If
    End Sub

    Private Sub cmdCancelaOrden_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelaOrden.Click
        'Dim sVariables As String
        Dim sParametros As New Dictionary(Of String, Object)
        Dim PkConAlone As New Datos.ZctDataBase                         'Objeto de la base de datos

        If sStatus = "A" Then
            MsgBox("Para poder cancelar la orden primero tiene que ser grabada.")
            Exit Sub
        End If
        Try
            If MsgBox("�Desea cancelar esta orden de trabajo?, se har�n devoluciones de los art�culos y ya no se podr� utilizar este folio.", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub
            'Dim fLogin As New ZctLogin
            ''Autorizaci�n para surtir sin existencia
            'fLogin.Cod_Aut = 5
            'fLogin.ShowDialog(Me)
            Dim controlador = New SOTControladores.Controladores.ControladorPermisos()
            If controlador.VerificarPermisos(New Modelo.Seguridad.Dtos.DtoPermisos With {.EliminarOrdenTrabajo = True}, True) = False Then
                MsgBox("Usted no tiene permisos para cancelar las ordenes de trabajo.")
                Exit Sub
            Else
                sAutorizacion = "C"
                sObservaciones = ControladorBase.UsuarioActual.Id & " el d�a " & Now.ToString
            End If
            PkConAlone.OpenConTran()
            PkConAlone.Inicia_Transaccion()

            'sVariables = "@CodEnc_OT;INT|@FchDoc_OT;SMALLDATETIME|@Cod_Cte;VARCHAR|@Cod_Mot;INT|@Cod_Tec;INT|@FchEnt;SMALLDATETIME|@FchSal;SMALLDATETIME|@FchEntRmp;SMALLDATETIME|@FchSalRmp;SMALLDATETIME|@ObsOT;TEXT|@FchAplMov;SMALLDATETIME|@UltKm_OT;INT|@Cod_Estatus;VARCHAR|@HistEstatus_OT;VARCHAR"

            'sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|" & _
            '   "0" & "|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text & "|" & sAutorizacion & "|" & sObservaciones
            'sVariables = "@CodEnc_OT;INT|@Cod_folio;VARCHAR|@Folio;INT|@FchDoc_OT;SMALLDATETIME|@Cod_Cte;VARCHAR|@Cod_Contacto;INT|@Cod_Mot;INT|@Cod_Tec;INT|@FchEnt;SMALLDATETIME|@FchSal;SMALLDATETIME|@FchEntRmp;SMALLDATETIME|@FchSalRmp;SMALLDATETIME|@ObsOT;TEXT|@FchAplMov;SMALLDATETIME|@UltKm_OT;INT|@Cod_Estatus;VARCHAR|@HistEstatus_OT;VARCHAR|@Cod_TpOT;INT"
            '        sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|" & CType(IIf(txtVin.Text = "", 0, txtVin.Text), Integer) & "|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text

            Dim fechaActual = Now.Date

            sParametros.Add("@CodEnc_OT;INT", CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
            'sParametros.Add("@Folio;INT", txtFolio.Text)
            sParametros.Add("@FchDoc_OT;SMALLDATETIME", fechaActual)
            sParametros.Add("@Cod_Cte;VARCHAR", CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer))
            sParametros.Add("@Cod_Mot;INT", 0)
            sParametros.Add("@Cod_Tec;INT", 0)
            sParametros.Add("@FchEnt;SMALLDATETIME", fechaActual)
            sParametros.Add("@FchSal;SMALLDATETIME", fechaActual)
            sParametros.Add("@FchEntRmp;SMALLDATETIME", fechaActual)
            sParametros.Add("@FchSalRmp;SMALLDATETIME", fechaActual)
            sParametros.Add("@ObsOT;TEXT", txtTrabajo.Text)
            sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
            sParametros.Add("@UltKm_OT;INT", 0)
            sParametros.Add("@Cod_Estatus;VARCHAR", sAutorizacion)
            sParametros.Add("@HistEstatus_OT;VARCHAR", sObservaciones)
            sParametros.Add("@Cod_folio;VARCHAR", ClavesFolios.OT)
            sParametros.Add("@Cod_Contacto;INT", Nothing)


            'sParametros = Cod_OT.ToString & "|" & ClavesFolios.OT & "|" & CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & Now.Date & "|" & CType(IIf(txtCliente.Text = "", 0, txtCliente.Text), Integer) & "|" & txtContacto.Text & "|" & CType(IIf(txtVin.Text = "", 0, txtVin.Text), Integer) & "|" & CType(IIf(txtTecnico.Text = "", 0, txtTecnico.Text), Integer) & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & DTEntrada.Value & "|" & DTSalida.Value & "|" & txtTrabajo.Text & "|" & DtAplicacion.Value & "|" & txtKm.Text & "|" & sAutorizacion & "|" & sObservaciones & "|" & CboTpOrden.value

            GetData(2, ProcedimientosAlmacenados.SP_ENCABEZADO_ORDEN_TRABAJO, sParametros, PkConAlone)

            'sVariables = "@Cod_EncOT;INT|@Cod_DetOT;INT|@Cod_Art;VARCHAR|@Ctd_Art;INT|@CosArt_DetOT;DECIMAL|@PreArt_DetOT;DECIMAL|@CtdStd_DetOT;INT|@Fch_DetOT;SMALLDATETIME|@FchAplMov;SMALLDATETIME|@Cat_Alm;INT|@Cod_Mot;INT"
            Dim iRow As Integer

            For iRow = 0 To DGridArticulos.Rows.Count - 1
                If DGridArticulos.Item(iColCod, iRow).Value Is Nothing Or DGridArticulos.Item(iColCod, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColCod, iRow).Value = 0
                End If

                If DGridArticulos.Item(iColSol, iRow).Value Is Nothing Or DGridArticulos.Item(iColSol, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColSol, iRow).Value = 0
                End If
                DGridArticulos.Item(iColSurt, iRow).Value = 0

                If DGridArticulos.Item(iColTipo, iRow).Value Is Nothing Or DGridArticulos.Item(iColTipo, iRow).Value Is DBNull.Value Then
                    DGridArticulos.Item(iColTipo, iRow).Value = 0
                End If


                'AHL 12/03/2008
                'Valida que el c�digo de art�culo no este vac�o
                If Not (DGridArticulos.Item(iColArt, iRow).Value Is Nothing) AndAlso DGridArticulos.Item(iColArt, iRow).Value <> "" Then
                    sParametros.Clear() ' = ""

                    If DGridArticulos.Item(iColAlm, iRow).Value Is Nothing OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "" OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "0" Then
                        DGridArticulos.Item(iColAlm, iRow).ErrorText = "El almac�n en la orden de trabajo no puede estar vacia"
                        Throw New ZctReglaNegocioEx("El almac�n en la orden de compra no puede estar vacia")
                    Else
                        DGridArticulos.Item(iColAlm, iRow).ErrorText = ""
                    End If

                    'sParametros = CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer) & "|" & CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColArt, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer) & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & DGridArticulos.Item(iColPrecio, iRow).Value & "|" & CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer) & "|" & Now.Date
                    'sParametros = sParametros & CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer) & "|"
                    'sParametros = sParametros & CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer) & "|"
                    'sParametros = sParametros & DGridArticulos.Item(iColArt, iRow).Value & "|"
                    'sParametros = sParametros & CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer) & "|"
                    'sParametros = sParametros & CDbl(DGridArticulos.Item(iColCosto, iRow).Value) & "|"
                    'sParametros = sParametros & DGridArticulos.Item(iColPrecio, iRow).Value & "|"
                    'sParametros = sParametros & CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer) & "|"
                    'sParametros = sParametros & Now.Date & "|"
                    'sParametros = sParametros & DtAplicacion.Value & "|" & DGridArticulos.Item(iColAlm, iRow).Value.ToString
                    'sParametros = sParametros & "|" & DGridArticulos.Item(iColMot, iRow).Value.ToString & "|" & DGridArticulos.Item(iColTipo, iRow).Value.ToString


                    sParametros.Add("@Cod_EncOT;INT", CType(IIf(CType(txtFolio.Text, String) = "", 0, txtFolio.Text), Integer))
                    sParametros.Add("@Cod_DetOT;INT", CType(IIf(DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer))
                    sParametros.Add("@Cod_Art;VARCHAR", DGridArticulos.Item(iColArt, iRow).Value)
                    sParametros.Add("@Ctd_Art;INT", CType(IIf(DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer))
                    sParametros.Add("@CosArt_DetOT;DECIMAL", CDbl(DGridArticulos.Item(iColCosto, iRow).Value))
                    sParametros.Add("@PreArt_DetOT;DECIMAL", DGridArticulos.Item(iColPrecio, iRow).Value)
                    sParametros.Add("@CtdStd_DetOT;INT", CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, iRow).Value), Integer))
                    sParametros.Add("@Fch_DetOT;SMALLDATETIME", fechaActual)
                    sParametros.Add("@FchAplMov;SMALLDATETIME", fechaActual)
                    sParametros.Add("@Cat_Alm;INT", DGridArticulos.Item(iColAlm, iRow).Value)
                    sParametros.Add("@Cod_Mot;INT", DGridArticulos.Item(iColMot, iRow).Value)
                    sParametros.Add("@Cod_TpArt;VARCHAR", DGridArticulos.Item(iColTipo, iRow).Value)
                    'sParametros.Add("@cod_usu;INT", DGridArticulos.Item(iColUsu, iRow).Value)

                    GetData(2, ProcedimientosAlmacenados.SP_DETALLE_ORDEN_TRABAJO, sParametros, PkConAlone)
                End If
            Next
            PkConAlone.Termina_Transaccion()
            PkConAlone.CloseConTran()
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdTrabajo", "C", txtFolio.Text)


            If MsgBox("�Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                sStatus = "A"
                plimpia(True)
            Else
                plimpia(False)
                'Carga el folio nuevamente
                CargaOrden()
            End If

        Catch ex As Exception
            PkConAlone.Cancela_Transaccion()
            PkConAlone.CloseConTran()
            MsgBox("Ha ocurrido un error al momento de estar cancelando la orden de trabajo , comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub ZctSOTGroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZctSOTGroupBox1.Enter

    End Sub

    Private Sub CboTpOrden_Load(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub txtTrabajo_Load(sender As Object, e As EventArgs) Handles txtTrabajo.Load

    End Sub

    Private Sub cboTipoOrden_lostfocus(sender As Object, e As EventArgs)
        obtiene_folio()
    End Sub
End Class


