﻿Public Class ZctDiccRegiones
    Inherits ZctDiccGen(Of String)

    Public Sub New()
        MyBase.New()
        Me.CargaDatos("SELECT Cod_Region, Desc_Region FROM ZctCatRegion")
    End Sub

    Public Sub New(ByVal codigo As String, ByVal Descripcion As String)
        MyBase.New(codigo, Descripcion)
        Me.CargaDatos("SELECT Cod_Region, Desc_Region FROM ZctCatRegion")
    End Sub
End Class

Public Class ZctDiccUbicaciones
    Inherits ZctDiccGen(Of String)

    Public Sub New()
        MyBase.New()
        Me.CargaDatos("SELECT Cod_Ubicacion, Desc_Ubicacion FROM ZctCatUbicacion")
    End Sub

    Public Sub New(ByVal codigo As String, ByVal Descripcion As String)
        MyBase.New(codigo, Descripcion)
        Me.CargaDatos("SELECT Cod_Ubicacion, Desc_Ubicacion FROM ZctCatUbicacion")
    End Sub
End Class
