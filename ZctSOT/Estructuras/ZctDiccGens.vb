﻿Public Class ZctDiccGen(Of T)
    Inherits Dictionary(Of T, String)
    Private _Conn As String = Datos.DAO.Conexion.CadenaConexion 'String.Format(ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4")

    Public Sub New()

    End Sub

    Public Sub New(ByVal codigo As T, ByVal Descripcion As String)
        Me.Add(codigo, Descripcion)
    End Sub

    Public Sub CargaDatos(ByVal _SQL As String)
        Dim BD As New ZctDB
        Dim rs As SqlClient.SqlDataReader = Nothing
        If _SQL = "" Then Throw New ArgumentException("Debe de porporcionar la sentencia SQL")
        Try
            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.CrearComando(_SQL)
            rs = BD.EjecutarConsulta()
            While rs.Read
                Me.Add(rs(0), rs(1).ToString)
            End While
        Catch ex As BaseDatosException
            Throw ex
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
        Finally
            If rs IsNot Nothing Then rs.Close()
            BD.Desconectar()
            BD.Dispose()
        End Try

    End Sub

End Class
