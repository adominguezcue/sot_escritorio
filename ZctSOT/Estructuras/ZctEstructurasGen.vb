﻿Imports ZctSOTRDN
Imports System.Configuration
''' <summary>
''' Estructuras
''' </summary>
''' <remarks></remarks>
Public Class ZctElementoEstructuras
    Private _Codigo As Object
    ''' <summary>
    ''' Código de la estructura
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Codigo() As Object
        Get
            Return _Codigo
        End Get
        Set(ByVal value As Object)
            _Codigo = value
        End Set
    End Property
    Private _Descripcion As String
    ''' <summary>
    ''' Descripción de la estructura
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal Codigo As Object, ByVal Descripcion As String)
        _Codigo = Codigo
        _Descripcion = Descripcion
    End Sub
End Class

''' <summary>
''' Clase Abstracta de las estructuras
''' </summary>
''' <remarks></remarks>
Public MustInherit Class ZctEstructurasGen
    Inherits List(Of ZctElementoEstructuras)
    Implements IComparer(Of ZctElementoEstructuras)
    
    Private _Conn As String = Datos.DAO.Conexion.CadenaConexion 'String.Format(ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4")

    Public Sub New()

    End Sub

    Public Sub New(ByVal inicial As ZctElementoEstructuras)
        Me.Add(inicial)
    End Sub

    Public Sub CargaDatos(ByVal _SQL As String)
        Dim BD As New ZctDB
        Dim rs As SqlClient.SqlDataReader = Nothing
        If _SQL = "" Then Throw New ArgumentException("Debe de porporcionar la sentencia SQL")
        Try
            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.CrearComando(_SQL)
            rs = BD.EjecutarConsulta()
            While rs.Read
                Me.Add(New ZctElementoEstructuras(rs(0), rs(1).ToString))
            End While
        Catch ex As BaseDatosException
            Throw ex
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
        Finally
            If rs IsNot Nothing Then rs.Close()
            BD.Desconectar()
            BD.Dispose()
        End Try

    End Sub



    Public Function Compare(ByVal x As ZctElementoEstructuras, ByVal y As ZctElementoEstructuras) As Integer Implements System.Collections.Generic.IComparer(Of ZctElementoEstructuras).Compare
        Return x.Descripcion.CompareTo(y.Descripcion)
    End Function
End Class


