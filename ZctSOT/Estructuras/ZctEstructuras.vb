﻿''' <summary>
''' Carga el tipo de artículos
''' </summary>
''' <remarks></remarks>
Public Class ZctEstructuraTpArticulo
    Inherits ZctEstructurasGen

    Private _sql As String = "SELECT Cod_TpArt, Desc_TpArt FROM ZctCatTpArt"
    Public Sub New()
        MyBase.New()
        Me.CargaDatos(_sql)
    End Sub

    Public Sub New(ByVal inicial As ZctElementoEstructuras)
        MyBase.new(inicial)
        Me.CargaDatos(_sql)
    End Sub

End Class
