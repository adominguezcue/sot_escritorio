﻿Public Class ZctFrmComunGenZona
    Private ListRegion As New Datos.Clases.Catalogos.ListaRegion
    Private ProcesaDpto As New Datos.DAO.ClProcesaCatalogos
    Private Sub ZctFrmComunGenZona_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Protected Overrides Sub CargaDatos()
        MyBase.CargaDatos()

        If DGBusqueda.Columns.Contains("Cod_Region") Then
            ProcesaDpto.CargaDatos(ListRegion)
            Dim cCol As New DataGridViewComboBoxColumn
            cCol.Name = "Region"
            cCol.DataSource = ListRegion
            cCol.HeaderText = "Region"
            cCol.DataPropertyName = "Cod_Region"
            cCol.DisplayMember = "Region"
            cCol.ValueMember = "Codigo"

            DGBusqueda.Columns.Add(cCol)
            DGBusqueda.Columns("Cod_Region").Visible = False
            CalculaWidth()
            DGBusqueda.AutoResizeColumn(cCol.Index)

        End If
    End Sub
End Class
