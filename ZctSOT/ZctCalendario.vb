Public Class ZctCalendario
    Public sFecha As String
    Public sValue As String

    

    Private Sub ZctCalendario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Width = 175
        Me.Height = 175

        If sValue = "" Or sValue = "  /  /" Then
            MiCalendario.SetDate(Date.Today)
        Else
            MiCalendario.SetDate(CType(sValue, Date))
        End If
    End Sub


    Private Sub MiCalendario_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MiCalendario.DateChanged
        sFecha = CType(MiCalendario.SelectionStart, String)
    End Sub

    Private Sub MiCalendario_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MiCalendario.DateSelected
        sFecha = CType(MiCalendario.SelectionStart, String)
        Me.Close()
    End Sub


    Private Sub ZctCalendario_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Me.Width = 175
        Me.Height = 175
    End Sub
End Class