﻿Imports System.Linq
Imports Modelo.Sistemas.Constantes
Imports Modelo.Sistemas.Entidades.Dtos
Imports ZctSOT.Datos

Public Class ControladorSincronizacionExterna

    Private Shared lock As Object = New Object()
    Private Shared sincronizando As Boolean = False

    ''Public ParametrosSistema As New Clases.Sistema.ZctCatPar

    Private Shared Sub GetPendientesPorEnviar()


        Try
            Dim _dao_conf As New Global.DAO.DAOConfig(Datos.DAO.Conexion.CadenaConexion)

            Dim total_registros As Integer = _dao_conf.ObtieneRenglonesPorEnviar()


            'If total_registros > 0 Then
            '    ToolTipPendientes.Text = total_registros.ToString + " Registros pendientes por enviar a la página web."
            '    ToolTipPendientes.ForeColor = Color.DarkRed
            'Else
            '    ToolTipPendientes.Text = "0 Registros pendientes por enviar a la página web"
            '    ToolTipPendientes.ForeColor = Color.DarkGreen
            'End If
        Catch ex As Exception

        End Try

    End Sub

    Private Shared Sub actualizacion_generica(Of T, C)(ParametrosSistema As Clases.Sistema.ZctCatPar, ByVal _dao As Controlador.IDAOModelo(Of T), ByVal actualiza As Controlador.Controlador(Of T, C), ByVal modelo As String)
        Try

            'Dim _dao As Ne w Global.DAO.DAOBaseMoto(Datos.DAO.Coneccion.CadenaConexion)

            'Dim actualiza As New Controlador.Controlador(Of T, C)(_dao)

            Dim elementos As List(Of T)
            elementos = _dao.ObtieneModelos()

            'Transversal.Log.Logger.Info("[REQUISICIONES]: Obtenidos " & elementos.Count & " elementos de " & modelo)

            For Each elemento As T In elementos
                actualiza.GrabaModeloWeb(ParametrosSistema.Reporteador_web, modelo, "arhernandez@engranedigital.com", "12345678", elemento)
            Next
        Catch ex As Exception
            error_management(ex)
        End Try
    End Sub

    Private Shared Sub error_management(ByVal ex As Exception)

        'Dim sSource As String
        'Dim sLog As String
        'Dim sEvent As String
        'Dim sMachine As String

        'sSource = "ZctSOT"
        'sLog = "Application"
        'sEvent = ex.Message & " "
        'If ex.InnerException IsNot Nothing Then
        '    sEvent &= ex.InnerException.Message
        'End If
        'sMachine = "."
        ''MsgBox(sEvent)
        'Exit Sub
        'If Not EventLog.SourceExists(sSource, sMachine) Then
        '    Dim esc As New EventSourceCreationData(sSource, sLog)
        '    esc.MachineName = sMachine
        '    EventLog.CreateEventSource(esc) '(sSource, sLog, sMachine)
        'End If

        'Dim ELog As New EventLog(sLog, sMachine, sSource)
        'ELog.WriteEntry(sEvent)
        'ELog.WriteEntry(sEvent, EventLogEntryType.Warning, 234, CType(3, Short))

        Transversal.Log.Logger.Error("[REQUISICIONES]: Error - " & ex.Message)

    End Sub

    Private Shared Sub avance_managment(etapa As String, Optional fin As Boolean = False)

        'Dim sSource As String
        'Dim sLog As String
        'Dim sEvent As String
        'Dim sMachine As String

        'sSource = "ZctSOT"
        'sLog = "Application"
        'sEvent = ex.Message & " "
        'If ex.InnerException IsNot Nothing Then
        '    sEvent &= ex.InnerException.Message
        'End If
        'sMachine = "."
        ''MsgBox(sEvent)
        'Exit Sub
        'If Not EventLog.SourceExists(sSource, sMachine) Then
        '    Dim esc As New EventSourceCreationData(sSource, sLog)
        '    esc.MachineName = sMachine
        '    EventLog.CreateEventSource(esc) '(sSource, sLog, sMachine)
        'End If

        'Dim ELog As New EventLog(sLog, sMachine, sSource)
        'ELog.WriteEntry(sEvent)
        'ELog.WriteEntry(sEvent, EventLogEntryType.Warning, 234, CType(3, Short))

        Transversal.Log.Logger.Info("[REQUISICIONES]: " & IIf(fin, "Fin ", "Inicio ") & etapa)

    End Sub

    Public Shared Sub RealizarActualizacionInformacionExterna(nombreSucursal As String)
        SyncLock lock
            If sincronizando Then
                Return
            End If

            sincronizando = True
        End SyncLock


        Try
            Dim controlador = New SOTControladores.Controladores.ControladorConfiguracionSistemas()

            Dim parametrosConfig = controlador.ObtenerParametros(IdentificadoresSistemas.CONFIGURADOR_SOT)

            Dim configuracion = New ParametrosConfigurador
            For Each propiedad In GetType(ParametrosConfigurador).GetProperties()

                Dim parametro = parametrosConfig.FirstOrDefault(Function(m) m.Nombre = propiedad.Name)

                If Not IsNothing(parametro) Then

                    propiedad.SetValue(configuracion, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType))
                End If
            Next

            If Not configuracion.ConfiguracionFinalizada Then Throw New Transversal.Excepciones.SOTException("La configuración de la sucursal no ha finalizado")

            GetPendientesPorEnviar()

            Dim ParametrosSistema As New Clases.Sistema.ZctCatPar
            Dim DAOSistema As New Datos.DAO.ClProcesaCatalogos()

            DAOSistema.CargaDatos(ParametrosSistema)

            If ParametrosSistema.Reporteador_web = "" Or ParametrosSistema.Taller = "" Then
                Exit Sub
            End If

            ''Dim _dao As New Global.DAO.DAOBaseMoto(Datos.DAO.Coneccion.CadenaConexion)
            ''Dim actualiza As New Controlador.ControladorBaseMotos(_dao)
            ''actualizacion_generica(Of Modelo.base_moto, Modelo.BaseMotosCollection)(_dao, actualiza, "base_motos")

            avance_managment("Regiones")
            Dim _dao_reportes As New Global.DAO.DAORegion(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_reportes As New Controlador.ControladorRegiones(_dao_reportes)
            actualizacion_generica(Of Modelo.region, Modelo.RegionesCollection)(ParametrosSistema, _dao_reportes, actualiza_reportes, "regions")
            avance_managment("Regiones", True)

            avance_managment("Corte mensual")
            Dim _dao_corte As New Global.DAO.DAOCorteMensual(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_corte As New Controlador.ControladorCorteMensuales(_dao_corte)
            actualizacion_generica(Of Modelo.corte_mensual, Modelo.CorteMensualesCollection)(ParametrosSistema, _dao_corte, actualiza_corte, "corte_mensuals")
            avance_managment("Corte mensual", True)

            avance_managment("Compras")
            Dim _dao_compras As New Global.DAO.DAOCompra(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_compra As New Controlador.ControladorCompras(_dao_compras)
            actualizacion_generica(Of Modelo.compra, Modelo.ComprasCollection)(ParametrosSistema, _dao_compras, actualiza_compra, "compras")
            avance_managment("Compras", True)

            avance_managment("Inventarios finales")
            Dim _dao_inventarios As New Global.DAO.DAOInventarioFinal(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_inventario As New Controlador.ControladorInventarioFinales(_dao_inventarios)
            actualizacion_generica(Of Modelo.inventario, Modelo.InventarioFinalesCollection)(ParametrosSistema, _dao_inventarios, actualiza_inventario, "inventarios")
            avance_managment("Inventarios finales", True)

            'Dim _dao_reporte_general As New Global.DAO.DAOReporteGeneral(Datos.DAO.Coneccion.CadenaConexion)
            'Dim actualiza_reporte_general As New Controlador.ControladorReporteGeneral(_dao_reporte_general)
            'actualizacion_generica(Of Modelo.ReporteGeneral, Modelo.ReporteGeneralCollection)(_dao_reporte_general, actualiza_reporte_general, "reporte_generals")

            avance_managment("Productos")
            Dim _dao_productos As New Global.DAO.DAOProducto(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_productos As New Controlador.ControladorProducto(_dao_productos)
            actualizacion_generica(Of Modelo.Product, Modelo.ProductCollection)(ParametrosSistema, _dao_productos, actualiza_productos, "products")
            avance_managment("Productos", True)

            avance_managment("Clientes")
            Dim _dao_cliente As New Global.DAO.DAOCliente(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_clientes As New Controlador.ControladorCliente(_dao_cliente)
            actualizacion_generica(Of Modelo.customer, Modelo.CustomerCollection)(ParametrosSistema, _dao_cliente, actualiza_clientes, "customers")
            avance_managment("Clientes", True)

            avance_managment("Stock")
            Dim _dao_stock As New Global.DAO.DAOStock(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_stock As New Controlador.ControladorStock(_dao_stock)
            actualizacion_generica(Of Modelo.stock, Modelo.stockCollection)(ParametrosSistema, _dao_stock, actualiza_stock, "stocks")
            avance_managment("Stock", True)

            avance_managment("Productos por stock")
            Dim _dao_productoxstock As New Global.DAO.DAOproductxstock(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_productoxstock As New Controlador.ControladorProductoXStock(_dao_productoxstock)
            actualizacion_generica(Of Modelo.productxstock, Modelo.productxstockCollection)(ParametrosSistema, _dao_productoxstock, actualiza_productoxstock, "productxstocks")
            avance_managment("Productos por stock", True)

            avance_managment("Requisiciones")
            Dim _dao_requisition As New Global.DAO.DAORequisition(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_requisition As New Controlador.ControladorRequisitions(_dao_requisition)
            Dim requisiciones As List(Of Modelo.Requisition) = actualiza_requisition.ObtieneModelosWeb(ParametrosSistema.Reporteador_web, "arhernandez@engranedigital.com", "12345678", "requisitions", nombreSucursal)
            For Each rq As Modelo.Requisition In requisiciones
                Try
                    actualiza_requisition.GrabaModelo(rq)
                Catch r_ex As Exception
                    error_management(r_ex)
                End Try
            Next
            actualizacion_generica(Of Modelo.Requisition, Modelo.RequisitionsCollection)(ParametrosSistema, _dao_requisition, actualiza_requisition, "requisitions")
            avance_managment("Requisiciones", True)

            avance_managment("Presupuestos")
            Dim _dao_budget As New Global.DAO.DAOBudget(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_budget As New Controlador.ControladorBudgets(_dao_budget)
            actualizacion_generica(Of Modelo.budget, Modelo.BudgetsCollection)(ParametrosSistema, _dao_budget, actualiza_budget, "budgets")
            avance_managment("Presupuestos", True)

            ''Temporalmente desactivada la sincronización de las cuentas por pagar

            'avance_managment("Cuentas por pagar")
            'Dim _dao_cxp As New Global.DAO.DAOCXP(Datos.DAO.Conexion.CadenaConexion)
            'Dim actualiza_cxp As New Controlador.ControladorCXP(_dao_cxp)
            'actualizacion_generica(Of Modelo.CXP, Modelo.cxpCollection)(ParametrosSistema, _dao_cxp, actualiza_cxp, "cxps")
            'avance_managment("Cuentas por pagar", True)
        Catch ex As Exception
            error_management(ex)
        Finally
            SyncLock lock
                sincronizando = False
            End SyncLock
        End Try

    End Sub
End Class
