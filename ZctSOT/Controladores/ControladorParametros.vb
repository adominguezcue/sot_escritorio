﻿Imports ZctSOT.Datos

Public Class ControladorParametros
    Public Shared Function ObtenerParametros() As Clases.Sistema.ZctCatPar
        Dim ParametrosSistema = New Clases.Sistema.ZctCatPar
        Dim DAOSistema As New Datos.DAO.ClProcesaCatalogos()
        DAOSistema.CargaDatos(ParametrosSistema)

        Return ParametrosSistema
    End Function
End Class
