﻿Imports System.Windows.Forms
Imports Microsoft.Win32
Imports System.Configuration
Imports System.Timers
Imports System.Diagnostics
Imports ConviertePDF
Imports ResTotal.Vista.CxP
Imports System.Threading
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms
Imports ZctSOT.Datos
Imports SOTControladores.Controladores
Imports MixItems.View

Public Class ZctSOTMainV2
    Dim _Controlador As Permisos.Controlador.Controlador
    'Public _UsuAct As String = ""
    'Public _NomUsuAct As String = ""
    'Public _UsuAut As String = ""
    'Public _EsVendedor As Boolean
    Public ParametrosSistema As New Clases.Sistema.ZctCatPar
    Private DAOSistema As New Datos.DAO.ClProcesaCatalogos()


    Public Sub RealizarActualizacionInformacionExterna()
        Try
            GetPendientesPorEnviar()
            If ParametrosSistema.Reporteador_web = "" Or ParametrosSistema.Taller = "" Then
                Exit Sub
            End If

            ''Dim _dao As New Global.DAO.DAOBaseMoto(Datos.DAO.Coneccion.CadenaConexion)
            ''Dim actualiza As New Controlador.ControladorBaseMotos(_dao)
            ''actualizacion_generica(Of Modelo.base_moto, Modelo.BaseMotosCollection)(_dao, actualiza, "base_motos")

            Dim _dao_reportes As New Global.DAO.DAORegion(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_reportes As New Controlador.ControladorRegiones(_dao_reportes)
            actualizacion_generica(Of Modelo.region, Modelo.RegionesCollection)(_dao_reportes, actualiza_reportes, "regions")

            Dim _dao_corte As New Global.DAO.DAOCorteMensual(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_corte As New Controlador.ControladorCorteMensuales(_dao_corte)
            actualizacion_generica(Of Modelo.corte_mensual, Modelo.CorteMensualesCollection)(_dao_corte, actualiza_corte, "corte_mensuals")


            Dim _dao_compras As New Global.DAO.DAOCompra(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_compra As New Controlador.ControladorCompras(_dao_compras)
            actualizacion_generica(Of Modelo.compra, Modelo.ComprasCollection)(_dao_compras, actualiza_compra, "compras")


            Dim _dao_inventarios As New Global.DAO.DAOInventarioFinal(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_inventario As New Controlador.ControladorInventarioFinales(_dao_inventarios)
            actualizacion_generica(Of Modelo.inventario, Modelo.InventarioFinalesCollection)(_dao_inventarios, actualiza_inventario, "inventarios")

            'Dim _dao_reporte_general As New Global.DAO.DAOReporteGeneral(Datos.DAO.Coneccion.CadenaConexion)
            'Dim actualiza_reporte_general As New Controlador.ControladorReporteGeneral(_dao_reporte_general)
            'actualizacion_generica(Of Modelo.ReporteGeneral, Modelo.ReporteGeneralCollection)(_dao_reporte_general, actualiza_reporte_general, "reporte_generals")

            Dim _dao_productos As New Global.DAO.DAOProducto(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_productos As New Controlador.ControladorProducto(_dao_productos)
            actualizacion_generica(Of Modelo.Product, Modelo.ProductCollection)(_dao_productos, actualiza_productos, "products")


            Dim _dao_cliente As New Global.DAO.DAOCliente(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_clientes As New Controlador.ControladorCliente(_dao_cliente)
            actualizacion_generica(Of Modelo.customer, Modelo.CustomerCollection)(_dao_cliente, actualiza_clientes, "customers")


            Dim _dao_stock As New Global.DAO.DAOStock(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_stock As New Controlador.ControladorStock(_dao_stock)
            actualizacion_generica(Of Modelo.stock, Modelo.stockCollection)(_dao_stock, actualiza_stock, "stocks")

            Dim _dao_productoxstock As New Global.DAO.DAOproductxstock(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_productoxstock As New Controlador.ControladorProductoXStock(_dao_productoxstock)
            actualizacion_generica(Of Modelo.productxstock, Modelo.productxstockCollection)(_dao_productoxstock, actualiza_productoxstock, "productxstocks")

            Dim _dao_requisition As New Global.DAO.DAORequisition(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_requisition As New Controlador.ControladorRequisitions(_dao_requisition)
            Dim requisiciones As List(Of Modelo.Requisition) = actualiza_requisition.ObtieneModelosWeb(ParametrosSistema.Reporteador_web, "arhernandez@engranedigital.com", "12345678", "requisitions", "RESTOTAL")
            For Each rq As Modelo.Requisition In requisiciones
                actualiza_requisition.GrabaModelo(rq)
            Next
            actualizacion_generica(Of Modelo.Requisition, Modelo.RequisitionsCollection)(_dao_requisition, actualiza_requisition, "requisitions")

            'Dim _dao_budget As New Global.DAO.DAOBudget(Datos.DAO.Coneccion.CadenaConexion)
            'Dim actualiza_budget As New Controlador.ControladorBudgets(_dao_budget)
            'actualizacion_generica(Of Modelo.budget, Modelo.BudgetsCollection)(_dao_budget, actualiza_budget, "budgets")
            Dim _dao_cxp As New Global.DAO.DAOCXP(Datos.DAO.Conexion.CadenaConexion)
            Dim actualiza_cxp As New Controlador.ControladorCXP(_dao_cxp)
            actualizacion_generica(Of Modelo.CXP, Modelo.cxpCollection)(_dao_cxp, actualiza_cxp, "cxps")

        Catch ex As Exception
            error_managment(ex)
        End Try

    End Sub

    Public Sub error_managment(ByVal ex As Exception)

        Dim sSource As String
        Dim sLog As String
        Dim sEvent As String
        Dim sMachine As String

        sSource = "ZctSOT"
        sLog = "Application"
        sEvent = ex.Message & " "
        If ex.InnerException IsNot Nothing Then
            sEvent &= ex.InnerException.Message
        End If
        sMachine = "."
        'MsgBox(sEvent)
        Exit Sub
        If Not EventLog.SourceExists(sSource, sMachine) Then
            EventLog.CreateEventSource(sSource, sLog, sMachine)
        End If

        Dim ELog As New EventLog(sLog, sMachine, sSource)
        ELog.WriteEntry(sEvent)
        ELog.WriteEntry(sEvent, EventLogEntryType.Warning, 234, CType(3, Short))

    End Sub

    Public Sub actualizacion_generica(Of T, C)(ByVal _dao As Controlador.IDAOModelo(Of T), ByVal actualiza As Controlador.Controlador(Of T, C), ByVal modelo As String)
        Try

            'Dim _dao As Ne w Global.DAO.DAOBaseMoto(Datos.DAO.Coneccion.CadenaConexion)

            'Dim actualiza As New Controlador.Controlador(Of T, C)(_dao)

            Dim elementos As List(Of T)
            elementos = _dao.ObtieneModelos()
            For Each elemento As T In elementos
                actualiza.GrabaModeloWeb(ParametrosSistema.Reporteador_web, modelo, "arhernandez@engranedigital.com", "12345678", elemento)
            Next
        Catch ex As Exception
            error_managment(ex)
        End Try
    End Sub

    Public Sub GetPendientesPorEnviar()


        Try
            Dim _dao_conf As New Global.DAO.DAOConfig(Datos.DAO.Conexion.CadenaConexion)

            Dim total_registros As Integer = _dao_conf.ObtieneRenglonesPorEnviar()


            If total_registros > 0 Then
                ToolTipPendientes.Text = total_registros.ToString + " Registros pendientes por enviar a la página web."
                ToolTipPendientes.ForeColor = Color.DarkRed
            Else
                ToolTipPendientes.Text = "0 Registros pendientes por enviar a la página web"
                ToolTipPendientes.ForeColor = Color.DarkGreen
            End If
        Catch ex As Exception

        End Try

    End Sub


    Public Function Inventario_en_marcha(ByVal cod_alm As Integer) As Boolean
        Dim PkConAlone As New Datos.ZctDataBase
        'Objeto de la base de datos
        PkConAlone.IniciaProcedimiento("SP_Inventario_en_marcha")
        PkConAlone.InciaDataAdapter()
        PkConAlone.AddParameterSP("Cod_Alm", cod_alm, SqlDbType.Int)
        Dim dt As DataTable = PkConAlone.GetReaderSP.Tables("DATOS")
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub ActualizaParametros()
        If ParametrosSistema Is Nothing Then ParametrosSistema = New Clases.Sistema.ZctCatPar
        DAOSistema.CargaDatos(ParametrosSistema)
    End Sub

    Private Sub ClienteForm(ByVal sender As Object, ByVal e As EventArgs) Handles ClienteStripMenuItem.Click
        Dim fCatcte As New ZctClientes
        fCatcte.StartPosition = FormStartPosition.CenterScreen
        fCatcte.MdiParent = Me
        fCatcte.Show()
    End Sub

    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Global.System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CascadeToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticleToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileVerticalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileHorizontalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ArrangeIconsToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAllToolStripMenuItem.Click
        ' Cierre todos los formularios secundarios del primario.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer = 0

    Private Sub MarcasMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MarcasMenuItem.Click
        Dim fMarcaBusqueda As New ZctfrmComunGen
        'fMarcaBusqueda.sTablaName = "ZctCatMar"
        'fMarcaBusqueda.sSPName = "SP_ZctCatMar"
        'fMarcaBusqueda.sSpVariables = "@Codigo;INT|@Descripcion;VARCHAR|@ColNuevo;INT"
        'fMarcaBusqueda.sSPName_Busq = ""
        'fMarcaBusqueda.iColBusq = 0
        'fMarcaBusqueda.sSpVariables_Busq = ""
        'fMarcaBusqueda.sTabla_Busq = "ZctCatMar"
        'fMarcaBusqueda.Height = 400
        'fMarcaBusqueda.Width = 400
        fMarcaBusqueda.EnumGenerico = New Clases.Catalogos.ListaMarcas
        fMarcaBusqueda.Text = "Catálogo de Marcas"
        fMarcaBusqueda.MdiParent = Me
        fMarcaBusqueda.Show()
    End Sub

    Private Sub EstadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadosToolStripMenuItem.Click
        Dim fEdoBusqueda As New ZctfrmComunGen
        'fEdoBusqueda.sTablaName = "ZctCatEdo"
        'fEdoBusqueda.sSPName = "SP_ZctCatEdo"
        'fEdoBusqueda.sSpVariables = "@Codigo;INT|@Descripcion;VARCHAR|@ColNuevo;INT"
        'fEdoBusqueda.iColBusq = 0
        'fEdoBusqueda.sSPName_Busq = ""
        'fEdoBusqueda.sSpVariables_Busq = ""
        'fEdoBusqueda.sTabla_Busq = ""
        'fEdoBusqueda.Height = 400
        'fEdoBusqueda.Width = 400
        fEdoBusqueda.EnumGenerico = New Clases.Catalogos.ListaEstados
        fEdoBusqueda.Text = "Catálogo de Estados"
        fEdoBusqueda.MdiParent = Me
        fEdoBusqueda.Show()
    End Sub

    Private Sub CiudadesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CiudadesToolStripMenuItem.Click
        Dim fCiuBusqueda As New ZctFrmComunGenCiudad
        fCiuBusqueda.EnumGenerico = New Clases.Catalogos.listaCiudades
        fCiuBusqueda.Text = "Catálogo de Ciudades"
        fCiuBusqueda.MdiParent = Me
        fCiuBusqueda.Show()
    End Sub

    Private Sub ArtículosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArtículosToolStripMenuItem.Click
        Dim fArticulosBusqueda As New ZctCatArt
        AddHandler fArticulosBusqueda.ImprimeEtiqueta, AddressOf Imprime_etiqueta_anaquel
        AddHandler fArticulosBusqueda.ImprimeEtiquetaArticulo, AddressOf Imprime_etiqueta_articulo
        fArticulosBusqueda.MdiParent = Me
        fArticulosBusqueda.Show()
    End Sub

    Private Sub Proveedores_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProveedoresToolStripMenuItem.Click
        'Dim fProveedores As New ZctfrmComunGen
        'fProveedores.MdiParent = Me
        'fProveedores.EnumGenerico = New ZctSOT.Clases.Catalogos.ListaProveedores
        'fProveedores.Text = "Catálogo de Proveedores"
        'fProveedores.Show()
        Dim fProveedores As New ResTotal.Vista.ZctCatProv(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id)
        AddHandler fProveedores.BuscaProveedor, AddressOf BuscaProveedor
        fProveedores.MdiParent = Me
        fProveedores.Show()

    End Sub

    Private Function BuscaProveedor() As Integer
        Dim fBusqueda As New ZctBusqueda
        fBusqueda.sSPName = "SP_ZctCatProv"
        fBusqueda.sSpVariables = "@Cod_Prov;INT|@Nom_Prov;VARCHAR|@RFC_Prov;VARCHAR|@Dir_Prov;VARCHAR"
        fBusqueda.ShowDialog(Me)
        Return fBusqueda.iValor
    End Function

    Private Sub TecnicosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fTecnicos As New ZctfrmComunGen
        fTecnicos.MdiParent = Me
        fTecnicos.EnumGenerico = New Clases.Catalogos.ListaTecnicos
        fTecnicos.Text = "Catálogo de Técnicos"
        fTecnicos.Show()
    End Sub

    Private Sub MotosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fMotos As New ZctMotos
        fMotos.MdiParent = Me
        fMotos.Show()
    End Sub

    Private Sub ModelosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fModelos As New ZctFrmComunGenMod
        'fModelos.sTablaName = "ZctCatMod"
        'fModelos.sSPName = "SP_ZctCatMod"
        'fModelos.sSpVariables = "@CodMod_Mot;INT|@Desc_Mod;VARCHAR|@Cod_Mar;INT|@ColNuevo;INT"

        'fModelos.sSPName_Busq = "SP_ZctCatMarF3"
        'fModelos.iColBusq = 2
        'fModelos.sSpVariables_Busq = "@Codigo;INT|@Descripcion;VARCHAR"
        'fModelos.sTabla_Busq = "ZctCatMar"
        'fModelos.Height = 400
        'fModelos.Width = 400
        fModelos.EnumGenerico = New Clases.Catalogos.listaModelos
        fModelos.Text = "Catálogo de Modelos"
        fModelos.MdiParent = Me
        fModelos.Show()
    End Sub


    Private Sub OrdenesTrabToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesTrabToolStripMenuItem.Click
        Dim fOrdTrab As New ZctOrdTrabajo
        fOrdTrab.MdiParent = Me
        fOrdTrab.Show()

    End Sub

    Private Sub ZctSOTMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try


            Dim splash As New ZctIntro
            'CreaLlaves()
            'Actualizaciones

            splash.ShowDialog()

            Dim fLogin As New ZctLogin
            'fLogin.Cod_Aut = 0
            fLogin.ShowDialog(Me)
            If fLogin.Valida = False Then
                Me.Dispose()
            End If
            _Controlador = New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)

            'If Not IsNothing(ControladorBase.UsuarioActual) Then
            '    _Controlador.Recorrermenu(ControladorBase.UsuarioActual.Id, Me.MenuStrip)
            'End If

            'If (IsNumeric(_UsuAct)) Then
            '    _Controlador.Recorrermenu(_UsuAct, Me.MenuStrip)
            'End If
            MenuStrip.Visible = True
            ActualizacionesIniciales()
            ActualizaParametros()
            GetPendientesPorEnviar()
            'If _EsVendedor Then
            '    CreaCaja(0)
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InventariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles InventariosToolStripMenuItem.Click
        Dim fShowRpt As New ZctShowRpt
        fShowRpt.MdiParent = Me
        '1 para inventarios
        fShowRpt.iCodCatego = 1
        fShowRpt.Show()

    End Sub

    Private Sub ServicioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles ServicioToolStripMenuItem.Click
        Dim fShowRpt As New ZctShowRpt
        fShowRpt.MdiParent = Me
        '1 para inventarios
        fShowRpt.iCodCatego = 2
        fShowRpt.Show()
    End Sub


    Private Sub LíneasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LíneasToolStripMenuItem.Click
        Dim fLineaBusqueda As New ZctFrmComunGenLinea
        fLineaBusqueda.Text = "Catálogo de Subcategorias"
        fLineaBusqueda.MdiParent = Me
        fLineaBusqueda.EnumGenerico = New Clases.Catalogos.listaLineas
        fLineaBusqueda.Show()
    End Sub

    Private Sub DepartamentosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepartamentosToolStripMenuItem.Click
        Dim fDptoBusqueda As New ZctfrmComunGen
        fDptoBusqueda.Height = 400
        fDptoBusqueda.Width = 400
        fDptoBusqueda.Text = "Catálogo de Categorias"
        fDptoBusqueda.EnumGenerico = New Clases.Catalogos.listaDepartamentos
        fDptoBusqueda.MdiParent = Me
        fDptoBusqueda.Show()
    End Sub

    Private Sub RespaldoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RespaldoToolStripMenuItem.Click
        Dim frmResp As New ZctRespaldo
        frmResp.MdiParent = Me
        frmResp.Show()

    End Sub

    Private Sub GerentesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fCatGte As New ZctfrmComunGen
        fCatGte.Text = "Catálogo de Gerentes"
        fCatGte.EnumGenerico = New Clases.Catalogos.ListaGerentes
        fCatGte.MdiParent = Me
        fCatGte.Show()
    End Sub

    Private Sub AlmacenesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AlmacenesToolStripMenuItem.Click

        Dim fCatAlm As New ZctFrmComunGenLinea ' ZctfrmComun
        fCatAlm.EnumGenerico = New Clases.Catalogos.ListaAlmacenes
        fCatAlm.Text = "Catálogo de Almacenes"
        fCatAlm.MdiParent = Me
        fCatAlm.Show()
    End Sub

    Private Sub OrdenesDeTrabajoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fOrdTrabmot As New ZctOrdTrabajoMot
        fOrdTrabmot.MdiParent = Me
        fOrdTrabmot.Show()

    End Sub

    Private Sub ActualizaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizaciónToolStripMenuItem.Click
        Try
            Dim Actualiza As New ZctActiva.Activa(Datos.DAO.Conexion.CadenaConexion) 'String.Format(System.Configuration.ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4"))
            Actualiza.BuscaActualizaciones()
            If Actualiza.Errores = "" Then
                MsgBox("Las actualizaciones se han ejecutado de manera correcta.")
            Else
                MsgBox(Actualiza.Errores)
            End If


        Catch ex As Exception
            MsgBox("Ha ocurrido un error al ejecutar las actualizaciones")
            Debug.Print(ex.Message)
        End Try
    End Sub

    Private Sub ComparativoDeInventariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComparativoDeInventariosToolStripMenuItem.Click
        Dim frmInv As New ZctComparaInventarios
        frmInv.MdiParent = Me
        frmInv.Show()
    End Sub


    Private Sub InventarioDeMotocicletasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim zctinv As New ZctInvMotos
        zctinv.MdiParent = Me
        zctinv.Show()

    End Sub

    Private Sub RegionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fDptoBusqueda As New ZctFrmComunGenString
        fDptoBusqueda.EnumGenerico = New Clases.Catalogos.ListaRegion
        fDptoBusqueda.Text = "Catálogo de Región"
        fDptoBusqueda.MdiParent = Me
        fDptoBusqueda.Show()
    End Sub

    Private Sub UbicacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fDptoUbic As New ZctFrmComunGenString
        fDptoUbic.EnumGenerico = New Clases.Catalogos.ListaUbicaciones
        fDptoUbic.MdiParent = Me
        fDptoUbic.Text = "Catálogo de Ubicaciones de motocicletas"
        fDptoUbic.Show()
    End Sub

    Private Sub CompraSugeridaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompraSugeridaToolStripMenuItem.Click
        Dim frmOCS As New ZctOrdenCompraSug
        frmOCS.MdiParent = (Me)
        frmOCS.Show()

    End Sub

    Private Sub OrdenesCompToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdenesCompToolStripMenuItem.Click

        Dim fOrdComp As New ZctOrdCompra
        fOrdComp.MdiParent = Me
        fOrdComp.Show()
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsuariosToolStripMenuItem.Click
        Dim zctUsuarios As New ZctSegUsu
        zctUsuarios.MdiParent = Me
        zctUsuarios.Show()
    End Sub

    Private Sub ParametrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ParametrosToolStripMenuItem.Click
        Dim zctPar As New ZctCatPar
        zctPar.MdiParent = Me
        zctPar.Show()
    End Sub

    Sub ActualizacionesIniciales()
        BackgroundWorker.RunWorkerAsync()
    End Sub

    Private Sub PresupuestosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            Dim pres As New ZctMainPresupuesto
            pres.MdiParent = Me
            pres.Show()

        Catch ex As Exception
            MsgBox("Ha ocurrido un error")
        End Try

    End Sub

    Private Sub TipoDeOrdenesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fCatAlm As New ZctFrmComunGenLinea ' ZctfrmComun
        fCatAlm.EnumGenerico = New Clases.Catalogos.ListaTipo_Orden
        fCatAlm.Text = "Catálogo de tipos de trabajos"
        fCatAlm.MdiParent = Me
        fCatAlm.Show()

    End Sub

    Private Sub ClasificaciónDeArticulosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim artViejos As New ZctArtViejos
        artViejos.MdiParent = Me
        artViejos.Show()
    End Sub

    Private Sub IntercambioEntreAlmacenesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IntercambioEntreAlmacenesToolStripMenuItem.Click
        Dim invart As New ZctIncAlmacenes
        invart.MdiParent = Me
        invart.Show()
    End Sub

    Private Sub ReporteDeZonasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReporteDeZonasToolStripMenuItem.Click
        Dim zona As New ZctArtViejos
        zona.MdiParent = Me
        zona.Show()

    End Sub

    Private Sub MotocicletasPorMesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmMotosPorMes
        frm.MdiParent = Me
        frm.Show()

    End Sub

    Private Sub GerencialesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles GerencialesToolStripMenuItem.Click
        Dim fShowRpt As New ZctShowRpt
        fShowRpt.MdiParent = Me
        '1 para inventarios
        fShowRpt.iCodCatego = 3
        fShowRpt.Show()
    End Sub

    Private Sub NotasDeCréditoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NotasDeCréditoToolStripMenuItem.Click
        Dim frmCdt As New ZctFormNotaCredito()
        frmCdt.MdiParent = Me
        frmCdt.Show()
    End Sub

    Private Sub TipoDeRefacciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fTpref As New ZctfrmComun()
        fTpref.Text = "Catálogo de tipos de refacciones"
        fTpref.MdiParent = Me
        fTpref.sTablaName = "ZctCatTpRef"
        fTpref.sSPName = "SP_ZctCatTpRef"
        fTpref.sSpVariables = "@Codigo;INT|@Descripcion;VARCHAR|@ColNuevo;INT"
        fTpref.Show()
    End Sub

    Private Sub ResponsablesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fCatRep As New ZctfrmComunGen
        fCatRep.Text = "Catálogo de Responsables"
        fCatRep.EnumGenerico = New Clases.Catalogos.ListaResponsables
        fCatRep.MdiParent = Me
        fCatRep.Show()
    End Sub

    Private Sub MarcaPorTiendaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fMarcaTiendaBusqueda As New ZctfrmComunGen
        fMarcaTiendaBusqueda.EnumGenerico = New Clases.Catalogos.ListaMarcaTiendas
        fMarcaTiendaBusqueda.Text = "Catálogo de Marcas por tienda"
        fMarcaTiendaBusqueda.MdiParent = Me
        fMarcaTiendaBusqueda.Show()
    End Sub

    Private Sub ZonasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim fZonaBusqueda As New ZctFrmComunGenZona
        fZonaBusqueda.EnumGenerico = New Clases.Catalogos.listaZonas
        fZonaBusqueda.Text = "Catálogo de Zonas"
        fZonaBusqueda.MdiParent = Me
        fZonaBusqueda.Show()
    End Sub

    Private Sub ActualizaciónWebToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ActualizaciónWebToolStripMenuItem.Click
        RealizarActualizacionInformacionExterna()
    End Sub

    Private Sub WebTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WebTimer.Tick
        Dim t As New Thread(AddressOf RealizarActualizacionInformacionExterna)
        t.Start()
    End Sub

    Private Sub CalendarioFiscalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CalendarioFiscalToolStripMenuItem.Click

        Dim frm As New CalendarioFiscal.Vista.FrmCalendarioFiscal(Datos.DAO.Conexion.CadenaConexion)
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub StatusStrip_ItemClicked(sender As System.Object, e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles StatusStrip.ItemClicked

    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        If MsgBox("¿Realmente desea salir del sistema?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Me.Close()
        End If
    End Sub

    Private Sub RequisicionesWebToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RequisicionesWebToolStripMenuItem.Click
        Dim frm_requisiciones As New RequisicionesWeb.Vista.Requisicion(Datos.DAO.Conexion.CadenaConexion)
        frm_requisiciones.MdiParent = Me
        AddHandler frm_requisiciones.lanza_salida, AddressOf Crea_salida
        AddHandler frm_requisiciones.lanza_compra, AddressOf Crea_compra

        frm_requisiciones.Show()
    End Sub

    Private Function Crea_salida(ByVal Cod_EncOT As Integer) As Integer
        Try
            Dim OTS As New ZctOrdTrabajoMot()
            OTS.Show()
            OTS.CargaOrden(Cod_EncOT)

            OTS.Focus()
            OTS.MdiParent = Me

        Catch ex As Exception
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try

        Return 0
    End Function

    Private Function Crea_compra(ByVal Cod_OC As Integer) As Integer

        Try
            Dim Ocs As New ZctOrdCompra
            'Abre la orden de compra normal y carga la orden de compra en cuestion
            Ocs.Show()
            Ocs.CargaCompra(Cod_OC)
            Ocs.MdiParent = Me
            Ocs.Focus()


        Catch ex As Exception
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try

        Return 0
    End Function

    Private Sub PermisosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Dim Permisos As New PermisosUsuarios
        Permisos.MdiParent = Me
        Permisos.Show()
    End Sub

    Private Sub RelacionPermisosUsuariosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Dim Permisos As New RolesUsuarios
        Permisos.MdiParent = Me
        Permisos.Show()
    End Sub

    Private Sub CajaToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs)
        CreaCaja(0)
    End Sub
    Friend Sub CreaCaja(folio_pedido As Integer)

        If IsNothing(ControladorBase.UsuarioActual) Then
            Exit Sub
        End If

        Dim Caja As New ResTotal.Vista.frmCaja(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id, "CJ", ParametrosSistema.cod_cte_default)
        AddHandler Caja.LanzaCliente, AddressOf ModificaCliente
        AddHandler Caja.LanzaOT, AddressOf ModificaOT
        AddHandler Caja.Lanzaimpresion, AddressOf imprime_caja
        AddHandler Caja.Autoriza, AddressOf AutorizaCaja
        AddHandler Caja.CreaOT, AddressOf CreatOT
        AddHandler Caja.LanzaBuscaDeCliente, AddressOf BuscaCliente
        AddHandler Caja.ImprimeRetiro, AddressOf imprime_retiro
        AddHandler Caja.ImprimeCorte, AddressOf imprime_corte
        AddHandler Caja.LanzaBusquedaArticulos, AddressOf BusquedaArticulos
        Caja.MdiParent = Me
        Caja.Show()
        If folio_pedido > 0 Then
            Caja.CargaFolio(folio_pedido)
        End If
    End Sub
    Private Function BusquedaArticulos()
        Dim fBusqueda As New ZctBusqueda
        fBusqueda.sSPName = "SP_ZctCatArt"
        fBusqueda.sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT"
        fBusqueda.ShowDialog(Me)
        Return fBusqueda.iValor
    End Function
    Private Sub AutorizaCaja(cod_auto As Integer, Padre As ResTotal.Vista.frmCaja)
        Dim Login As New ZctLogin
        Login.Cod_Aut = cod_auto
        Login.ShowDialog()
        Padre._PermisoCancelar = Login.Valida = True
    End Sub

    Private Sub Imprime_directo(pathRpt_ruta As String, sql As String, copias As Integer)
        Dim crTableLogonInfo As New TableLogOnInfo
        Dim crTableLogonInfos As New TableLogOnInfos
        Dim loginfo As CrystalDecisions.Shared.ConnectionInfo
        loginfo = New CrystalDecisions.Shared.ConnectionInfo
        loginfo.ServerName = Datos.DAO.Conexion.Server
        loginfo.DatabaseName = Datos.DAO.Conexion.Database
        loginfo.UserID = Datos.DAO.Conexion.User
        loginfo.Password = "L0k0m0t0r4"
        crTableLogonInfo.ConnectionInfo = loginfo
        crTableLogonInfos.Add(crTableLogonInfo)


        Dim reporte As New ReportDocument

        reporte.Load(pathRpt_ruta, OpenReportMethod.OpenReportByDefault)
        reporte.RecordSelectionFormula = sql
        Dim CrTables As Tables
        Dim CrTable As Table
        reporte.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        ' crConnectionInfo = loginfo
        CrTables = reporte.Database.Tables
        For Each subRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument In reporte.Subreports
            subRpt.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        Next

        For Each CrTable In CrTables
            CrTable.ApplyLogOnInfo(crTableLogonInfo)
        Next

        reporte.PrintToPrinter(copias, True, 0, 0)

    End Sub

    Private Sub imprime_corte(FolioRetiro As Integer)
        Imprime_directo("C:\Reportes\rptCortecaja.rpt", "{ZctRetirosCajas.cod_retiro} = " + FolioRetiro, 2)
    End Sub

    Private Sub Imprime_etiqueta_articulo(Cod_Art As String)
        Imprime_directo("C:\Reportes\rptEtiquetaArt_pq.rpt", "{zctcatart.Cod_Art} = '" & Cod_Art & "'", 1)
    End Sub
    Private Sub Imprime_etiqueta_anaquel(Cod_Art As String)
        Imprime_directo("C:\Reportes\rptEtiquetaArt.rpt", "{zctcatart.Cod_Art} = '" & Cod_Art & "'", 1)
    End Sub
    Private Sub imprime_caja(Cod_folio As String, folio As Integer)
        Imprime_directo("C:\Reportes\ZctRptCJ2.rpt", "{VW_ZctTicked_Caja.Cod_Folio} = '" & Cod_folio & "' AND {VW_ZctTicked_Caja.CodEnc_Caja} = " & folio.ToString() & "", 2)

    End Sub

    Private Function BuscaCliente() As Integer
        Dim fBusqueda As New ZctBusqueda
        fBusqueda.sSPName = "SP_ZctCatCte"
        fBusqueda.sSpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
        fBusqueda.ShowDialog(Me)
        Return fBusqueda.iValor
    End Function

    Private Function ModificaCliente(cod_cte As Integer) As Integer
        Dim frm As New ZctClientes()
        frm.CargaCliente(cod_cte)
        frm.StartPosition = FormStartPosition.CenterScreen

        'frm.MdiParent = Me
        frm.ShowDialog()
        'frm.ShowDialog()

        Return 0
    End Function

    Private Function CreatOT() As Integer
        Dim frm As New ZctOrdTrabajo()
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()

        Return 0
    End Function

    Private Function ModificaOT(cod_OT As Long) As Integer
        Dim frm As New ZctOrdTrabajo()
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CargaOT(cod_OT)
        frm.ShowDialog()

        Return 0
    End Function
    Private Sub ModificaProveedor(cod_prov As Integer)
        Dim frm As New ResTotal.Vista.ZctCatProv(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id)
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CargaProveedor(cod_prov)
        frm.ShowDialog()
    End Sub
    Private Sub CuentasPorPagarToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Dim CxP As New ResTotal.Vista.CxP_principal(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id, "CJ")
        AddHandler CxP.cambiaproveedor, AddressOf ModificaProveedor
        CxP.MdiParent = Me
        CxP.StartPosition = FormStartPosition.CenterParent
        CxP.Show()
    End Sub

    Private Sub BackgroundWorker_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker.DoWork
        Try
            Dim Actualiza As New ZctActiva.Activa(Datos.DAO.Conexion.CadenaConexion) 'String.Format(System.Configuration.ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4"))
            Actualiza.BuscaActualizaciones()
            If Actualiza.Errores <> "" Then
                'MsgBox("Las actualizaciones se han ejecutado de manera correcta.")
                ' Else
                MsgBox(Actualiza.Errores)
            End If
            GetPendientesPorEnviar()
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al ejecutar las actualizaciones " + ex.Message)
            Debug.Print(ex.Message)
        End Try
    End Sub

    Private Sub ControlDeValoresToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)
        Dim frmValores As New ResTotal.Vista.ZctCatUbicacionesValores(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id)
        With frmValores
            .MdiParent = Me
            .Show()
        End With

    End Sub
    Private Sub imprime_retiro(FolioRetiro As Integer)
        Dim crTableLogonInfo As New TableLogOnInfo
        Dim crTableLogonInfos As New TableLogOnInfos
        Dim loginfo As CrystalDecisions.Shared.ConnectionInfo
        loginfo = New CrystalDecisions.Shared.ConnectionInfo
        loginfo.ServerName = Datos.DAO.Conexion.Server ' ConfigurationManager.AppSettings.Get("Server")
        loginfo.DatabaseName = Datos.DAO.Conexion.Database  'ConfigurationManager.AppSettings.Get("Database")
        loginfo.UserID = Datos.DAO.Conexion.User 'ConfigurationManager.AppSettings.Get("User")
        loginfo.Password = "L0k0m0t0r4" 'ConfigurationManager.AppSettings.Get("Password")
        crTableLogonInfo.ConnectionInfo = loginfo
        crTableLogonInfos.Add(crTableLogonInfo)


        Dim reporte As New ReportDocument

        Dim pathRpt_ruta As String = "C:\Reportes\rptRetirosCajas.rpt"

        reporte.Load(pathRpt_ruta, OpenReportMethod.OpenReportByDefault)


        'logonrpt(rpt)
        'If sSQLF <> "" Then
        reporte.RecordSelectionFormula = "{ZctRetirosCajas.cod_retiro} = " & Convert.ToInt32(FolioRetiro)
        'End If


        ' Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As Table
        reporte.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        ' crConnectionInfo = loginfo
        CrTables = reporte.Database.Tables
        For Each subRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument In reporte.Subreports
            subRpt.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        Next

        For Each CrTable In CrTables
            CrTable.ApplyLogOnInfo(crTableLogonInfo)
        Next

        reporte.PrintToPrinter(1, True, 0, 0)

    End Sub
    Private Sub imprime_corte(fondocaja As Decimal, cod_folio As String, cod_usu As Integer)
        Dim crTableLogonInfo As New TableLogOnInfo
        Dim crTableLogonInfos As New TableLogOnInfos
        Dim loginfo As CrystalDecisions.Shared.ConnectionInfo
        loginfo = New CrystalDecisions.Shared.ConnectionInfo
        loginfo.ServerName = Datos.DAO.Conexion.Server ' ConfigurationManager.AppSettings.Get("Server")
        loginfo.DatabaseName = Datos.DAO.Conexion.Database  'ConfigurationManager.AppSettings.Get("Database")
        loginfo.UserID = Datos.DAO.Conexion.User 'ConfigurationManager.AppSettings.Get("User")
        loginfo.Password = "L0k0m0t0r4" 'ConfigurationManager.AppSettings.Get("Password")
        crTableLogonInfo.ConnectionInfo = loginfo
        crTableLogonInfos.Add(crTableLogonInfo)


        Dim reporte As New ReportDocument

        Dim pathRpt_ruta As String = "C:\Reportes\rptCortecaja.rpt"


        reporte.Load(pathRpt_ruta, OpenReportMethod.OpenReportByDefault)

        reporte.SetParameterValue("fondocaja", fondocaja)
        reporte.SetParameterValue("@cod_folio", cod_folio)
        reporte.SetParameterValue("cod_usu", cod_usu)
        reporte.RecordSelectionFormula = "{WV_ZCTtickedCaja.cod_usu} = " & cod_usu


        Dim CrTables As Tables
        Dim CrTable As Table
        reporte.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)

        CrTables = reporte.Database.Tables
        For Each subRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument In reporte.Subreports
            subRpt.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        Next

        For Each CrTable In CrTables
            CrTable.ApplyLogOnInfo(crTableLogonInfo)
        Next

        reporte.PrintToPrinter(1, True, 0, 0)

    End Sub

    Private Sub INVENTARIOACTUALToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles INVENTARIOACTUALToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.MdiParent = Me
            '1 para inventarios
            fShowRpt.iCodCatego = 1
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.MdiParent = Me
            fShowRpt.Show()
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 0
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub

    Private Sub REPORTEDECOMPRASToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles REPORTEDECOMPRASToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.iCodCatego = 1
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.MdiParent = Me
            fShowRpt.Show()
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 1
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub

    Private Sub INVENTARIOACTUALNEGATIVOSToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles INVENTARIOACTUALNEGATIVOSToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.iCodCatego = 1
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.MdiParent = Me
            fShowRpt.Show()
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 2
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub

    Private Sub CATÁLOGODEARTÍCULOSToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CATÁLOGODEARTÍCULOSToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.iCodCatego = 3
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.MdiParent = Me
            fShowRpt.Show()
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 0
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub

    Private Sub REPORTEDEVENTASToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles REPORTEDEVENTASToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.iCodCatego = 3
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.MdiParent = Me
            fShowRpt.Show()
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 1
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub

    Private Sub REPORTEDEESTATUSDEVENTASToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles REPORTEDEESTATUSDEVENTASToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.iCodCatego = 3
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.MdiParent = Me
            fShowRpt.Show()
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 2
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub

    Private Sub REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.MdiParent = Me
            '1 para inventarios
            fShowRpt.iCodCatego = 2
            fShowRpt.Show()
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 0
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub


    Private Sub REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem.Click
        Try
            Dim fShowRpt As New ZctShowRpt
            fShowRpt.iCodCatego = 1
            fShowRpt.CboReportes.Enabled = False
            fShowRpt.MdiParent = Me
            fShowRpt.Show()
            fShowRpt.CboReportes.ZctSOTCombo1.SelectedIndex = 3
            fShowRpt.CargarParametros()
        Catch ex As Exception
            MsgBox("Error cargando reporte " & ex.Message)
        End Try
    End Sub

    'Private Sub RecetasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RecetasToolStripMenuItem.Click
    '    Dim frm As New frmMixItems()
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub

    'Private Sub EntradasYSalidasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EntradasYSalidasToolStripMenuItem.Click
    '    Dim frm As New frmMixMovInv()
    '    frm.MdiParent = Me
    '    frm.Show()
    'End Sub
End Class