﻿Imports ZctSOT.Datos

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctConfOCS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim SSLabel As System.Windows.Forms.Label
        Dim OCLabel As System.Windows.Forms.Label
        Dim MesesLabel As System.Windows.Forms.Label
        Dim LTLabel As System.Windows.Forms.Label
        Dim DiasHabilesLabel As System.Windows.Forms.Label
        Me.cmdAceptar = New ZctSOT.ZctSOTButton
        Me.cmdCancelar = New ZctSOT.ZctSOTButton
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox
        Me.SSTextBox = New System.Windows.Forms.TextBox
        Me.ZctConfOCSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OCTextBox = New System.Windows.Forms.TextBox
        Me.MesesTextBox = New System.Windows.Forms.TextBox
        Me.LTTextBox = New System.Windows.Forms.TextBox
        Me.DiasHabilesTextBox = New System.Windows.Forms.TextBox
        Me.txtCod_Alm = New ZctSOT.ZctControlBusqueda
        SSLabel = New System.Windows.Forms.Label
        OCLabel = New System.Windows.Forms.Label
        MesesLabel = New System.Windows.Forms.Label
        LTLabel = New System.Windows.Forms.Label
        DiasHabilesLabel = New System.Windows.Forms.Label
        Me.ZctSOTGroupBox1.SuspendLayout()
        CType(Me.ZctConfOCSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SSLabel
        '
        SSLabel.AutoSize = True
        SSLabel.Location = New System.Drawing.Point(23, 101)
        SSLabel.Name = "SSLabel"
        SSLabel.Size = New System.Drawing.Size(104, 13)
        SSLabel.TabIndex = 5
        SSLabel.Text = "Stock de Seguridad:"
        '
        'OCLabel
        '
        OCLabel.AutoSize = True
        OCLabel.Location = New System.Drawing.Point(41, 75)
        OCLabel.Name = "OCLabel"
        OCLabel.Size = New System.Drawing.Size(86, 13)
        OCLabel.TabIndex = 3
        OCLabel.Text = "Ciclo de compra:"
        '
        'MesesLabel
        '
        MesesLabel.AutoSize = True
        MesesLabel.Location = New System.Drawing.Point(86, 127)
        MesesLabel.Name = "MesesLabel"
        MesesLabel.Size = New System.Drawing.Size(41, 13)
        MesesLabel.TabIndex = 7
        MesesLabel.Text = "Meses:"
        '
        'LTLabel
        '
        LTLabel.AutoSize = True
        LTLabel.Location = New System.Drawing.Point(28, 49)
        LTLabel.Name = "LTLabel"
        LTLabel.Size = New System.Drawing.Size(99, 13)
        LTLabel.TabIndex = 1
        LTLabel.Text = "Tiempo de entrega:"
        '
        'DiasHabilesLabel
        '
        DiasHabilesLabel.AutoSize = True
        DiasHabilesLabel.Location = New System.Drawing.Point(58, 23)
        DiasHabilesLabel.Name = "DiasHabilesLabel"
        DiasHabilesLabel.Size = New System.Drawing.Size(69, 13)
        DiasHabilesLabel.TabIndex = 0
        DiasHabilesLabel.Text = "Dias Habiles:"
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Location = New System.Drawing.Point(214, 208)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 3
        Me.cmdAceptar.Text = "Aceptar"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(295, 208)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancelar.TabIndex = 0
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Controls.Add(SSLabel)
        Me.ZctSOTGroupBox1.Controls.Add(Me.SSTextBox)
        Me.ZctSOTGroupBox1.Controls.Add(OCLabel)
        Me.ZctSOTGroupBox1.Controls.Add(Me.OCTextBox)
        Me.ZctSOTGroupBox1.Controls.Add(MesesLabel)
        Me.ZctSOTGroupBox1.Controls.Add(Me.MesesTextBox)
        Me.ZctSOTGroupBox1.Controls.Add(LTLabel)
        Me.ZctSOTGroupBox1.Controls.Add(Me.LTTextBox)
        Me.ZctSOTGroupBox1.Controls.Add(DiasHabilesLabel)
        Me.ZctSOTGroupBox1.Controls.Add(Me.DiasHabilesTextBox)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(9, 44)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(360, 158)
        Me.ZctSOTGroupBox1.TabIndex = 2
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'SSTextBox
        '
        Me.SSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctConfOCSBindingSource, "SS", True))
        Me.SSTextBox.Location = New System.Drawing.Point(133, 98)
        Me.SSTextBox.Name = "SSTextBox"
        Me.SSTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SSTextBox.TabIndex = 6
        '
        'ZctConfOCSBindingSource
        '
        Me.ZctConfOCSBindingSource.DataSource = GetType(Clases.Compras.ZctConfOCS)
        '
        'OCTextBox
        '
        Me.OCTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctConfOCSBindingSource, "OC", True))
        Me.OCTextBox.Location = New System.Drawing.Point(133, 72)
        Me.OCTextBox.Name = "OCTextBox"
        Me.OCTextBox.Size = New System.Drawing.Size(100, 20)
        Me.OCTextBox.TabIndex = 4
        '
        'MesesTextBox
        '
        Me.MesesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctConfOCSBindingSource, "Meses", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.MesesTextBox.Location = New System.Drawing.Point(133, 124)
        Me.MesesTextBox.Name = "MesesTextBox"
        Me.MesesTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MesesTextBox.TabIndex = 8
        '
        'LTTextBox
        '
        Me.LTTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctConfOCSBindingSource, "LT", True))
        Me.LTTextBox.Location = New System.Drawing.Point(133, 46)
        Me.LTTextBox.Name = "LTTextBox"
        Me.LTTextBox.Size = New System.Drawing.Size(100, 20)
        Me.LTTextBox.TabIndex = 2
        '
        'DiasHabilesTextBox
        '
        Me.DiasHabilesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctConfOCSBindingSource, "DiasHabiles", True))
        Me.DiasHabilesTextBox.Location = New System.Drawing.Point(133, 20)
        Me.DiasHabilesTextBox.Name = "DiasHabilesTextBox"
        Me.DiasHabilesTextBox.Size = New System.Drawing.Size(100, 20)
        Me.DiasHabilesTextBox.TabIndex = 1
        '
        'txtCod_Alm
        '
        Me.txtCod_Alm.Descripcion = " "
        Me.txtCod_Alm.Location = New System.Drawing.Point(12, 12)
        Me.txtCod_Alm.Name = "txtCod_Alm"
        Me.txtCod_Alm.Nombre = "Almacén:"
        Me.txtCod_Alm.Size = New System.Drawing.Size(358, 27)
        Me.txtCod_Alm.SPName = Nothing
        Me.txtCod_Alm.SPParametros = Nothing
        Me.txtCod_Alm.SpVariables = Nothing
        Me.txtCod_Alm.SqlBusqueda = Nothing
        Me.txtCod_Alm.TabIndex = 0
        Me.txtCod_Alm.Tabla = Nothing
        Me.txtCod_Alm.TextWith = 111
        Me.txtCod_Alm.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCod_Alm.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtCod_Alm.Validar = True
        '
        'ZctConfOCS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(376, 240)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.txtCod_Alm)
        Me.Name = "ZctConfOCS"
        Me.Text = "Configuración Compra Sugerida"
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        Me.ZctSOTGroupBox1.PerformLayout()
        CType(Me.ZctConfOCSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCod_Alm As ZctSOT.ZctControlBusqueda
    Friend WithEvents ZctConfOCSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents SSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents OCTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MesesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents LTTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DiasHabilesTextBox As System.Windows.Forms.TextBox
End Class
