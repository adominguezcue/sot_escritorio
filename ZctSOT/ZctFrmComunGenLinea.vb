﻿Public Class ZctFrmComunGenCiudad
    Private ListEstado As New Datos.Clases.Catalogos.ListaEstados
    Private ProcesaDpto As New Datos.DAO.ClProcesaCatalogos
    Private Sub ZctFrmComunGenLinea_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Protected Overrides Sub CargaDatos()
        MyBase.CargaDatos()

        If DGBusqueda.Columns.Contains("Cod_Edo") Then
            ProcesaDpto.CargaDatos(ListEstado)
            Dim cCol As New DataGridViewComboBoxColumn
            cCol.Name = "Departamento"
            cCol.DataSource = ListEstado
            cCol.HeaderText = "Estado"
            cCol.DataPropertyName = "Cod_Edo"
            cCol.DisplayMember = "Estado"
            cCol.ValueMember = "Codigo"

            DGBusqueda.Columns.Add(cCol)
            DGBusqueda.Columns("Cod_Edo").Visible = False
            CalculaWidth()
            DGBusqueda.AutoResizeColumn(cCol.Index)

        End If
    End Sub
End Class
