<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctClientes
    Inherits Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox()
        Me.cmdEliminar = New ZctSOT.ZctSOTButton()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.ZctGroupControls2 = New ZctSOT.ZctGroupControls()
        Me.cbTiposPersonas = New System.Windows.Forms.ComboBox()
        Me.lblTipoP = New System.Windows.Forms.Label()
        Me.txtpais = New ZctSOT.ZctControlTexto()
        Me.TxtEmail = New ZctSOT.ZctControlTexto()
        Me.TxtCP = New ZctSOT.ZctControlTexto()
        Me.TxtNumeroInterior = New ZctSOT.ZctControlTexto()
        Me.TxtNumExt = New ZctSOT.ZctControlTexto()
        Me.TxtColonia = New ZctSOT.ZctControlTexto()
        Me.TxtCalle = New ZctSOT.ZctControlTexto()
        Me.txtTelefono = New ZctSOT.ZctControlTexto()
        Me.CboCiudad = New ZctSOT.ZctControlCombo()
        Me.CboEstado = New ZctSOT.ZctControlCombo()
        Me.txtRFC = New ZctSOT.ZctControlTexto()
        Me.txtApMaterno = New ZctSOT.ZctControlTexto()
        Me.txtApPaterno = New ZctSOT.ZctControlTexto()
        Me.txtNombre = New ZctSOT.ZctControlTexto()
        Me.ZctGroupControls1 = New ZctSOT.ZctGroupControls()
        Me.txtBusqCodCte = New ZctSOT.ZctControlBusqueda()
        Me.ZctSOTToolTip1 = New ZctSOT.ZctSOTToolTip()
        Me.ZctSOTGroupBox1.SuspendLayout
        Me.ZctGroupControls2.SuspendLayout
        Me.ZctGroupControls1.SuspendLayout
        Me.SuspendLayout
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdEliminar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdAceptar)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(3, 346)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(757, 51)
        Me.ZctSOTGroupBox1.TabIndex = 3
        Me.ZctSOTGroupBox1.TabStop = false
        '
        'cmdEliminar
        '
        Me.cmdEliminar.Location = New System.Drawing.Point(468, 17)
        Me.cmdEliminar.Name = "cmdEliminar"
        Me.cmdEliminar.Size = New System.Drawing.Size(75, 28)
        Me.cmdEliminar.TabIndex = 28
        Me.cmdEliminar.Tag = "Elimina el cliente seleccionado"
        Me.cmdEliminar.Text = "Eliminar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdEliminar, "Graba las modificaciones realizadas, si el código es nuevo da de alta el cliente")
        Me.cmdEliminar.UseVisualStyleBackColor = true
        Me.cmdEliminar.Visible = False
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(674, 17)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 28)
        Me.cmdCancelar.TabIndex = 27
        Me.cmdCancelar.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant"& _ 
    "alla"
        Me.cmdCancelar.Text = "Cancelar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelar, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant"& _ 
        "alla")
        Me.cmdCancelar.UseVisualStyleBackColor = true
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Location = New System.Drawing.Point(569, 17)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 28)
        Me.cmdAceptar.TabIndex = 26
        Me.cmdAceptar.Tag = "Graba las modificaciones realizadas, si el código es nuevo da de alta el cliente"
        Me.cmdAceptar.Text = "Grabar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdAceptar, "Graba las modificaciones realizadas, si el código es nuevo da de alta el cliente")
        Me.cmdAceptar.UseVisualStyleBackColor = true
        '
        'ZctGroupControls2
        '
        Me.ZctGroupControls2.Controls.Add(Me.cbTiposPersonas)
        Me.ZctGroupControls2.Controls.Add(Me.lblTipoP)
        Me.ZctGroupControls2.Controls.Add(Me.txtpais)
        Me.ZctGroupControls2.Controls.Add(Me.TxtEmail)
        Me.ZctGroupControls2.Controls.Add(Me.TxtCP)
        Me.ZctGroupControls2.Controls.Add(Me.TxtNumeroInterior)
        Me.ZctGroupControls2.Controls.Add(Me.TxtNumExt)
        Me.ZctGroupControls2.Controls.Add(Me.TxtColonia)
        Me.ZctGroupControls2.Controls.Add(Me.TxtCalle)
        Me.ZctGroupControls2.Controls.Add(Me.txtTelefono)
        Me.ZctGroupControls2.Controls.Add(Me.CboCiudad)
        Me.ZctGroupControls2.Controls.Add(Me.CboEstado)
        Me.ZctGroupControls2.Controls.Add(Me.txtRFC)
        Me.ZctGroupControls2.Controls.Add(Me.txtApMaterno)
        Me.ZctGroupControls2.Controls.Add(Me.txtApPaterno)
        Me.ZctGroupControls2.Controls.Add(Me.txtNombre)
        Me.ZctGroupControls2.Location = New System.Drawing.Point(3, 63)
        Me.ZctGroupControls2.Name = "ZctGroupControls2"
        Me.ZctGroupControls2.Size = New System.Drawing.Size(757, 277)
        Me.ZctGroupControls2.TabIndex = 2
        Me.ZctGroupControls2.TabStop = false
        '
        'cbTiposPersonas
        '
        Me.cbTiposPersonas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTiposPersonas.FormattingEnabled = true
        Me.cbTiposPersonas.Location = New System.Drawing.Point(107, 13)
        Me.cbTiposPersonas.Name = "cbTiposPersonas"
        Me.cbTiposPersonas.Size = New System.Drawing.Size(258, 21)
        Me.cbTiposPersonas.TabIndex = 24
        '
        'lblTipoP
        '
        Me.lblTipoP.AutoSize = true
        Me.lblTipoP.Location = New System.Drawing.Point(14, 16)
        Me.lblTipoP.Name = "lblTipoP"
        Me.lblTipoP.Size = New System.Drawing.Size(87, 13)
        Me.lblTipoP.TabIndex = 23
        Me.lblTipoP.Text = "Tipo de persona:"
        '
        'txtpais
        '
        Me.txtpais.Location = New System.Drawing.Point(67, 243)
        Me.txtpais.Multiline = false
        Me.txtpais.Name = "txtpais"
        Me.txtpais.Nombre = "País:"
        Me.txtpais.Size = New System.Drawing.Size(299, 28)
        Me.txtpais.TabIndex = 19
        Me.txtpais.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtpais.ToolTip = ""
        '
        'TxtEmail
        '
        Me.TxtEmail.Location = New System.Drawing.Point(428, 175)
        Me.TxtEmail.Multiline = false
        Me.TxtEmail.Name = "TxtEmail"
        Me.TxtEmail.Nombre = "Email:"
        Me.TxtEmail.Size = New System.Drawing.Size(321, 28)
        Me.TxtEmail.TabIndex = 14
        Me.TxtEmail.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtEmail.ToolTip = ""
        '
        'TxtCP
        '
        Me.TxtCP.Location = New System.Drawing.Point(75, 175)
        Me.TxtCP.Multiline = false
        Me.TxtCP.Name = "TxtCP"
        Me.TxtCP.Nombre = "CP:"
        Me.TxtCP.Size = New System.Drawing.Size(290, 28)
        Me.TxtCP.TabIndex = 13
        Me.TxtCP.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtCP.ToolTip = ""
        '
        'TxtNumeroInterior
        '
        Me.TxtNumeroInterior.Location = New System.Drawing.Point(17, 145)
        Me.TxtNumeroInterior.Multiline = false
        Me.TxtNumeroInterior.Name = "TxtNumeroInterior"
        Me.TxtNumeroInterior.Nombre = "Numero Interior:"
        Me.TxtNumeroInterior.Size = New System.Drawing.Size(349, 28)
        Me.TxtNumeroInterior.TabIndex = 11
        Me.TxtNumeroInterior.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtNumeroInterior.ToolTip = ""
        '
        'TxtNumExt
        '
        Me.TxtNumExt.Location = New System.Drawing.Point(378, 110)
        Me.TxtNumExt.Multiline = false
        Me.TxtNumExt.Name = "TxtNumExt"
        Me.TxtNumExt.Nombre = "Numero Exterior:"
        Me.TxtNumExt.Size = New System.Drawing.Size(371, 28)
        Me.TxtNumExt.TabIndex = 10
        Me.TxtNumExt.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtNumExt.ToolTip = ""
        '
        'TxtColonia
        '
        Me.TxtColonia.Location = New System.Drawing.Point(418, 145)
        Me.TxtColonia.Multiline = false
        Me.TxtColonia.Name = "TxtColonia"
        Me.TxtColonia.Nombre = "Colonia:"
        Me.TxtColonia.Size = New System.Drawing.Size(331, 28)
        Me.TxtColonia.TabIndex = 12
        Me.TxtColonia.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtColonia.ToolTip = ""
        '
        'TxtCalle
        '
        Me.TxtCalle.Location = New System.Drawing.Point(67, 110)
        Me.TxtCalle.Multiline = false
        Me.TxtCalle.Name = "TxtCalle"
        Me.TxtCalle.Nombre = "Calle:"
        Me.TxtCalle.Size = New System.Drawing.Size(298, 28)
        Me.TxtCalle.TabIndex = 9
        Me.TxtCalle.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtCalle.ToolTip = ""
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(408, 243)
        Me.txtTelefono.Multiline = false
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Nombre = "Teléfono:"
        Me.txtTelefono.Size = New System.Drawing.Size(341, 28)
        Me.txtTelefono.TabIndex = 22
        Me.txtTelefono.Tag = "Teléfono del cliente"
        Me.txtTelefono.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtTelefono.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtTelefono, "Teléfono del cliente")
        '
        'CboCiudad
        '
        Me.CboCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboCiudad.Location = New System.Drawing.Point(418, 209)
        Me.CboCiudad.Name = "CboCiudad"
        Me.CboCiudad.Nombre = "Ciudad:"
        Me.CboCiudad.Size = New System.Drawing.Size(331, 28)
        Me.CboCiudad.TabIndex = 18
        Me.CboCiudad.Tag = "Catálogo de Ciudades, para dar de alta una Ciudad nueva, ingrésela en la pantalla"& _ 
    " de catálogo de Ciudades"
        Me.ZctSOTToolTip1.SetToolTip(Me.CboCiudad, "Catálogo de Ciudades, para dar de alta una Ciudad nueva, ingrésela en la pantalla"& _ 
        " de catálogo de Ciudades")
        Me.CboCiudad.value = Nothing
        Me.CboCiudad.ValueItem = 0
        Me.CboCiudad.ValueItemStr = Nothing
        '
        'CboEstado
        '
        Me.CboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboEstado.Location = New System.Drawing.Point(53, 209)
        Me.CboEstado.Name = "CboEstado"
        Me.CboEstado.Nombre = "Estado:"
        Me.CboEstado.Size = New System.Drawing.Size(313, 28)
        Me.CboEstado.TabIndex = 16
        Me.CboEstado.Tag = "Catálogo de Estados, para dar de alta un Estado nuevo, ingréselo en la pantalla d"& _ 
    "e catálogo de Estados"
        Me.ZctSOTToolTip1.SetToolTip(Me.CboEstado, "Catálogo de Estados, para dar de alta un Estado nuevo, ingréselo en la pantalla d"& _ 
        "e catálogo de Estados")
        Me.CboEstado.value = Nothing
        Me.CboEstado.ValueItem = 0
        Me.CboEstado.ValueItemStr = Nothing
        '
        'txtRFC
        '
        Me.txtRFC.Location = New System.Drawing.Point(428, 16)
        Me.txtRFC.Multiline = false
        Me.txtRFC.Name = "txtRFC"
        Me.txtRFC.Nombre = "RFC:"
        Me.txtRFC.Size = New System.Drawing.Size(321, 28)
        Me.txtRFC.TabIndex = 3
        Me.txtRFC.Tag = "RFC del Cliente"
        Me.txtRFC.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtRFC.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtRFC, "RFC del Cliente")
        '
        'txtApMaterno
        '
        Me.txtApMaterno.Location = New System.Drawing.Point(391, 81)
        Me.txtApMaterno.Multiline = false
        Me.txtApMaterno.Name = "txtApMaterno"
        Me.txtApMaterno.Nombre = " Ap. Materno:"
        Me.txtApMaterno.Size = New System.Drawing.Size(358, 28)
        Me.txtApMaterno.TabIndex = 8
        Me.txtApMaterno.Tag = "Apellido Materno del cliente"
        Me.txtApMaterno.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtApMaterno.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtApMaterno, "Apellido Materno del cliente")
        '
        'txtApPaterno
        '
        Me.txtApPaterno.Location = New System.Drawing.Point(31, 81)
        Me.txtApPaterno.Multiline = false
        Me.txtApPaterno.Name = "txtApPaterno"
        Me.txtApPaterno.Nombre = " Ap. Paterno:"
        Me.txtApPaterno.Size = New System.Drawing.Size(335, 28)
        Me.txtApPaterno.TabIndex = 5
        Me.txtApPaterno.Tag = "Apellido Paterno del cliente"
        Me.txtApPaterno.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtApPaterno.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtApPaterno, "Apellido Paterno del cliente")
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(53, 49)
        Me.txtNombre.Multiline = false
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Nombre = "Nombre:"
        Me.txtNombre.Size = New System.Drawing.Size(697, 28)
        Me.txtNombre.TabIndex = 4
        Me.txtNombre.Tag = "Nombre del Cliente"
        Me.txtNombre.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtNombre.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtNombre, "Nombre del Cliente")
        '
        'ZctGroupControls1
        '
        Me.ZctGroupControls1.Controls.Add(Me.txtBusqCodCte)
        Me.ZctGroupControls1.Location = New System.Drawing.Point(3, 3)
        Me.ZctGroupControls1.Name = "ZctGroupControls1"
        Me.ZctGroupControls1.Size = New System.Drawing.Size(757, 53)
        Me.ZctGroupControls1.TabIndex = 0
        Me.ZctGroupControls1.TabStop = false
        '
        'txtBusqCodCte
        '
        Me.txtBusqCodCte.Descripcion = "ZctSOTLabelDesc1"
        Me.txtBusqCodCte.Location = New System.Drawing.Point(6, 19)
        Me.txtBusqCodCte.Name = "txtBusqCodCte"
        Me.txtBusqCodCte.Nombre = "Código del Cliente:"
        Me.txtBusqCodCte.Size = New System.Drawing.Size(743, 28)
        Me.txtBusqCodCte.SPName = Nothing
        Me.txtBusqCodCte.SPParametros = Nothing
        Me.txtBusqCodCte.SpVariables = Nothing
        Me.txtBusqCodCte.SqlBusqueda = Nothing
        Me.txtBusqCodCte.TabIndex = 0
        Me.txtBusqCodCte.Tabla = Nothing
        Me.txtBusqCodCte.Tag = "Para Dar de Alta un Cliente nuevo, deje el valor en cero <BR>Para Modificar un cl"& _ 
    "iente existente, teclee el código o búsquelo presionando [F3]"
        Me.txtBusqCodCte.TextWith = 111
        Me.txtBusqCodCte.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtBusqCodCte.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtBusqCodCte, "Para Dar de Alta un Cliente nuevo, deje el valor en cero; Para Modificar un clien"& _ 
        "te existente, teclee el código o búsquelo presionando [F3]")
        Me.txtBusqCodCte.Validar = false
        '
        'ZctClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(776, 406)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.ZctGroupControls2)
        Me.Controls.Add(Me.ZctGroupControls1)
        Me.Name = "ZctClientes"
        Me.Text = "Datos fiscales del hotel"
        Me.ZctSOTGroupBox1.ResumeLayout(false)
        Me.ZctGroupControls2.ResumeLayout(false)
        Me.ZctGroupControls2.PerformLayout
        Me.ZctGroupControls1.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents txtBusqCodCte As ZctSOT.ZctControlBusqueda
    Friend WithEvents ZctGroupControls1 As ZctSOT.ZctGroupControls
    Friend WithEvents ZctGroupControls2 As ZctSOT.ZctGroupControls
    Friend WithEvents txtRFC As ZctSOT.ZctControlTexto
    Friend WithEvents txtApMaterno As ZctSOT.ZctControlTexto
    Friend WithEvents txtApPaterno As ZctSOT.ZctControlTexto
    Friend WithEvents txtNombre As ZctSOT.ZctControlTexto
    Friend WithEvents CboEstado As ZctSOT.ZctControlCombo
    Friend WithEvents CboCiudad As ZctSOT.ZctControlCombo
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents ZctSOTToolTip1 As ZctSOT.ZctSOTToolTip
    Friend WithEvents cmdEliminar As ZctSOT.ZctSOTButton
    Friend WithEvents txtTelefono As ZctSOT.ZctControlTexto
    Friend WithEvents TxtCP As ZctSOT.ZctControlTexto
    Friend WithEvents TxtNumeroInterior As ZctSOT.ZctControlTexto
    Friend WithEvents TxtNumExt As ZctSOT.ZctControlTexto
    Friend WithEvents TxtColonia As ZctSOT.ZctControlTexto
    Friend WithEvents TxtCalle As ZctSOT.ZctControlTexto
    Friend WithEvents TxtEmail As ZctSOT.ZctControlTexto
    Friend WithEvents txtpais As ZctSOT.ZctControlTexto
    Friend WithEvents cbTiposPersonas As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoP As System.Windows.Forms.Label
End Class
