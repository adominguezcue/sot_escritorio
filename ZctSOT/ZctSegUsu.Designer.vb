﻿Imports ZctSOT.Datos

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctSegUsu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim lblUsuario As System.Windows.Forms.Label
        Dim PassLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Me.txtCodUsu = New ZctSOT.ZctControlBusqueda()
        Me.cmdEliminar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox()
        Me.ChVendedor = New System.Windows.Forms.CheckBox()
        Me.ZctSegUsuBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Password2TextBox = New System.Windows.Forms.TextBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        lblUsuario = New System.Windows.Forms.Label()
        PassLabel = New System.Windows.Forms.Label()
        NombreLabel = New System.Windows.Forms.Label()
        Me.ZctSOTGroupBox1.SuspendLayout()
        CType(Me.ZctSegUsuBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCodUsu
        '
        Me.txtCodUsu.Descripcion = " "
        Me.txtCodUsu.Location = New System.Drawing.Point(3, 6)
        Me.txtCodUsu.Name = "txtCodUsu"
        Me.txtCodUsu.Nombre = "Código:"
        Me.txtCodUsu.Size = New System.Drawing.Size(358, 27)
        Me.txtCodUsu.SPName = Nothing
        Me.txtCodUsu.SPParametros = Nothing
        Me.txtCodUsu.SpVariables = Nothing
        Me.txtCodUsu.SqlBusqueda = Nothing
        Me.txtCodUsu.TabIndex = 0
        Me.txtCodUsu.Tabla = Nothing
        Me.txtCodUsu.TextWith = 111
        Me.txtCodUsu.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCodUsu.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtCodUsu.Validar = True
        '
        'cmdEliminar
        '
        Me.cmdEliminar.Location = New System.Drawing.Point(119, 164)
        Me.cmdEliminar.Name = "cmdEliminar"
        Me.cmdEliminar.Size = New System.Drawing.Size(75, 23)
        Me.cmdEliminar.TabIndex = 2
        Me.cmdEliminar.Text = "Eliminar"
        Me.cmdEliminar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Location = New System.Drawing.Point(200, 164)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 3
        Me.cmdAceptar.Text = "Aceptar"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(282, 164)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancelar.TabIndex = 4
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Controls.Add(Me.ChVendedor)
        Me.ZctSOTGroupBox1.Controls.Add(lblUsuario)
        Me.ZctSOTGroupBox1.Controls.Add(PassLabel)
        Me.ZctSOTGroupBox1.Controls.Add(Me.Password2TextBox)
        Me.ZctSOTGroupBox1.Controls.Add(Me.txtUsuario)
        Me.ZctSOTGroupBox1.Controls.Add(NombreLabel)
        Me.ZctSOTGroupBox1.Controls.Add(Me.PasswordTextBox)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(1, 35)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(356, 123)
        Me.ZctSOTGroupBox1.TabIndex = 1
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'ChVendedor
        '
        Me.ChVendedor.AutoSize = True
        Me.ChVendedor.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.ZctSegUsuBindingSource, "Vendedor", True))
        Me.ChVendedor.Location = New System.Drawing.Point(110, 97)
        Me.ChVendedor.Name = "ChVendedor"
        Me.ChVendedor.Size = New System.Drawing.Size(72, 17)
        Me.ChVendedor.TabIndex = 6
        Me.ChVendedor.Text = "Vendedor"
        Me.ChVendedor.UseVisualStyleBackColor = True
        '
        'ZctSegUsuBindingSource
        '
        Me.ZctSegUsuBindingSource.DataSource = GetType(Clases.Seguridad.ZctSegUsu)
        '
        'lblUsuario
        '
        lblUsuario.AutoSize = True
        lblUsuario.Location = New System.Drawing.Point(59, 22)
        lblUsuario.Name = "lblUsuario"
        lblUsuario.Size = New System.Drawing.Size(46, 13)
        lblUsuario.TabIndex = 0
        lblUsuario.Text = "Usuario:"
        '
        'PassLabel
        '
        PassLabel.AutoSize = True
        PassLabel.Location = New System.Drawing.Point(5, 74)
        PassLabel.Name = "PassLabel"
        PassLabel.Size = New System.Drawing.Size(103, 13)
        PassLabel.TabIndex = 4
        PassLabel.Text = "Repita su password:"
        '
        'Password2TextBox
        '
        Me.Password2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctSegUsuBindingSource, "Pass_Usu2", True))
        Me.Password2TextBox.Location = New System.Drawing.Point(110, 71)
        Me.Password2TextBox.MaxLength = 20
        Me.Password2TextBox.Name = "Password2TextBox"
        Me.Password2TextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Password2TextBox.Size = New System.Drawing.Size(166, 20)
        Me.Password2TextBox.TabIndex = 5
        Me.Password2TextBox.UseSystemPasswordChar = True
        '
        'txtUsuario
        '
        Me.txtUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctSegUsuBindingSource, "Nom_Usu", True))
        Me.txtUsuario.Location = New System.Drawing.Point(110, 19)
        Me.txtUsuario.MaxLength = 10
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(166, 20)
        Me.txtUsuario.TabIndex = 1
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Location = New System.Drawing.Point(48, 48)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(56, 13)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Password:"
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctSegUsuBindingSource, "Pass_Usu", True))
        Me.PasswordTextBox.Location = New System.Drawing.Point(110, 45)
        Me.PasswordTextBox.MaxLength = 20
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasswordTextBox.Size = New System.Drawing.Size(166, 20)
        Me.PasswordTextBox.TabIndex = 3
        Me.PasswordTextBox.UseSystemPasswordChar = True
        '
        'ZctSegUsu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 196)
        Me.Controls.Add(Me.txtCodUsu)
        Me.Controls.Add(Me.cmdEliminar)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Name = "ZctSegUsu"
        Me.Text = "Alta de usuarios"
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        Me.ZctSOTGroupBox1.PerformLayout()
        CType(Me.ZctSegUsuBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Password2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdEliminar As ZctSOT.ZctSOTButton
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents ZctSegUsuBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents txtCodUsu As ZctSOT.ZctControlBusqueda
    Friend WithEvents ChVendedor As System.Windows.Forms.CheckBox
End Class
