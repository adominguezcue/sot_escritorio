'Importa la clase de la base de datos
Imports ZctSOT.Datos.ZctDataBase
Imports ZctSOT.Datos.ZctFunciones
Imports SOTControladores.Controladores

Public Class ZctfrmComunGen
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
#Region "Declaraci�n de constantes"
    Const ColIndex = 0                                  'Contiene el valor de la columna Indice
    Const sColNuevo = "ColNuevo"                        'Cadena que servira para buscar la columna de nuevo
#End Region

#Region "Parametros de constantes"
    Public EnumGenerico As Datos.Interfaces.IEnumGenericoEnum 'IEnumGenerico
    Private cClas As New Datos.ClassGen
#End Region

#Region "Variables Globales"
    Dim iIndice As Integer = 0                          'Indice de escritura
    Dim bCambios As Boolean = False                     'Marca si se han realizado cambios
    Private ProcesaCatalogos As New Datos.DAO.ClProcesaCatalogos
    Private BS As New BindingSource
#End Region

    Private Sub PkBusqueda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
            DGBusqueda.AllowUserToAddRows = _Permisos.agrega
            DGBusqueda.AllowUserToDeleteRows = _Permisos.elimina
            DGBusqueda.ReadOnly = Not (_Permisos.edita Or _Permisos.agrega)
            CargaDatos()
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try
            If _Permisos.agrega = False And _Permisos.edita = False And _Permisos.elimina = False Then
                MsgBox("No tiene permiso para guardar informacion en esta ventana", MsgBoxStyle.Exclamation, "Atencion")
                Return
            End If
            ProcesaCatalogos.ActualizarDatos(EnumGenerico)
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, Me.Text, "A", "")
            If MsgBox("�Desea salir del cat�logo?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then Me.Close() : Exit Sub
            CargaDatos()
            bCambios = False
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub



    Private Sub DGBusqueda_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles DGBusqueda.CellBeginEdit
        If _Permisos.edita = False And _Permisos.agrega = False Then
            DGBusqueda.CancelEdit()
            DGBusqueda.ReadOnly = True
            MsgBox("No tiene permiso para Editar la informacion en esta ventana", MsgBoxStyle.Critical, "Atencion")
            Return
        Else
            DGBusqueda.ReadOnly = False
        End If
        'Try
        '    bCambios = True
        '    'En caso de que la columna de indice este vacia , lo ignora
        '    If DGBusqueda.Item(ColIndex, e.RowIndex).Value Is System.DBNull.Value Then Exit Sub
        '    If DGBusqueda.Item(ColIndex, e.RowIndex).Value.ToString = "" Then
        '        'Caso contrario le asigna el ultimo dato grabar
        '        DGBusqueda.Item(ColIndex, e.RowIndex).Value = CType(iIndice, String)
        '        DGBusqueda.Item(sColNuevo, e.RowIndex).Value = 0
        '        'Aumenta el indice
        '        iIndice = iIndice + 1

        '    End If
        '    'En caso de que el indice sea cero, cancela la acci�n
        '    If e.ColumnIndex = 0 Then
        '        e.Cancel = True
        '    End If
        'Catch ex As Exception
        '    MsgBox(ex.Message.ToString)
        'End Try
    End Sub

    Private Sub DGBusqueda_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles DGBusqueda.DataError
        'Obtiene el error que a causado el grid
        'MsgBox(e.Exception.Message)
        e.Cancel = True
    End Sub

    Private Sub DGBusqueda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGBusqueda.KeyDown
        Try
            If _Permisos.edita = False And _Permisos.elimina = False Then
                DGBusqueda.CancelEdit()
                DGBusqueda.AllowUserToDeleteRows = False
                MsgBox("No tiene permiso para eliminar la informacion en esta ventana", MsgBoxStyle.Critical, "Atencion")
                Return
            Else
                DGBusqueda.AllowUserToDeleteRows = True
            End If
            'Elimina el elemento seleccionado
            If e.KeyCode = Keys.Delete Then
                If Not (DGBusqueda.Item(0, DGBusqueda.CurrentCell.RowIndex).Value Is Nothing) Then
                    If MsgBox("�Desea eliminar este registro?", MsgBoxStyle.YesNo, "Eliminar") = MsgBoxResult.Yes Then

                        ProcesaCatalogos.EliminaDatos(DGBusqueda.Rows(DGBusqueda.CurrentCell.RowIndex).DataBoundItem)
                        CargaDatos()
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub


    Private Sub ZctSOTButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdAyuda.Click
        Dim fAyuda As New ZctFrmAyuda
        fAyuda.Texto = "1 Para un nuevo registro, escribe en el rengl�n en blanco y presiona ""Aceptar"" " & vbCrLf & _
                       "2 Para modificar un registro, sobreescribe el texto y presiona ""Aceptar"" " & vbCrLf & _
                       "3 Para eliminar un registro, da click en el mismo y presiona la tecla [Supr] " & vbCrLf
        fAyuda.ShowDialog(Me)
    End Sub

    Private Sub CmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancelar.Click

        If bCambios Then
            Try
                CargaDatos()
                'Muestra los tool tips
                DGBusqueda.ShowCellToolTips = True

            Catch ex As Exception
                MsgBox(ex.Message.ToString)
            End Try
        Else
            Me.Dispose()
        End If
    End Sub

    Protected Overridable Sub CargaDatos()
        DGBusqueda.DataSource = Nothing
        DGBusqueda.Rows.Clear()
        ProcesaCatalogos.CargaDatos(EnumGenerico)
        BS.DataSource = EnumGenerico
        DGBusqueda.DataSource = BS
        'Deshabilida el c�digo para que el sistema lo asigne autom�ticamente.
        DGBusqueda.Columns(0).ReadOnly = True

        If DGBusqueda.Columns.Contains("SP_Name") Then
            DGBusqueda.Columns("SP_Name").Visible = False
        End If

        If DGBusqueda.Columns.Contains("SP_Parametros") Then
            DGBusqueda.Columns("SP_Parametros").Visible = False
        End If

        If DGBusqueda.Columns.Contains("SentenciaElimina") Then
            DGBusqueda.Columns("SentenciaElimina").Visible = False
        End If

        If DGBusqueda.Columns.Contains("SentenciaGraba") Then
            DGBusqueda.Columns("SentenciaGraba").Visible = False
        End If

        If DGBusqueda.Columns.Contains("Actualiza") Then
            DGBusqueda.Columns("Actualiza").Visible = False
        End If


        'Muestra los tool tips
        DGBusqueda.ShowCellToolTips = True
        CalculaWidth()
    End Sub

    Public Sub CalculaWidth()
        DGBusqueda.AutoSize = True
        DGBusqueda.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
        DGBusqueda.ScrollBars = ScrollBars.Vertical

        Dim width As Integer = 200
        For i As Integer = 0 To DGBusqueda.Columns.Count - 1
            If DGBusqueda.Columns(i).Visible Then width = width + DGBusqueda.Columns(i).Width
        Next
        Me.Size = New System.Drawing.Size(width, Me.Height)

    End Sub
End Class