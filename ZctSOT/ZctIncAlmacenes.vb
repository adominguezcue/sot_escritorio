Option Strict On

Imports System.Data
Imports System.Data.SqlDbType
Imports System.Text
Imports ZctSOT.Datos
Imports SOTControladores.Controladores
Imports SOTControladores.Fabricas
Imports Transversal.Excepciones
Imports System.IO

Public Class ZctIncAlmacenes
    Friend _Controlador As New SOTControladores.Controladores.ControladorPermisos 'Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos 'Permisos.Controlador.Controlador.vistapermisos
    Private folio As New zctFolios("IA")
    Private iFolio As Integer = 0
    Private folios As New List(Of Integer)

#Region "Datos"

    Private Function cargarTabla(ByRef sentencia As String) As DataTable
        Dim db As New Datos.ZctDataBase
        Dim tabla As DataTable
        db.SQL = sentencia
        Try
            tabla = db.GetDataTable
        Catch ex As Exception
            Throw ex
        Finally
            'tabla.Dispose()
            db.Dispose()
        End Try
        Return tabla
    End Function

    Private Sub cargarAlmacenes()

        Me.ZctAlmEntrada.GetData("ZctCatAlm", False)
        Me.ZctAlmSalida.GetData("ZctCatAlm", False)

    End Sub

    Private Sub cargarArticulos()
        If Me.DGridArticulos.CurrentCell.Value IsNot Nothing Then
            Try
                Dim datos As DataTable = cargarTabla( _
                    "Select a.Desc_Art,aa.Exist_Art, a.Cos_Art " & _
                    "from ZctArtXAlm aa, ZctCatArt a " & _
                    "where a.Cod_Art = aa.Cod_Art " & _
                    "and aa.Exist_Art > 0 " & _
                    "and aa.Cod_Alm = " & _
                    Me.ZctAlmSalida.ValueItemStr & _
                    " and a.Cod_Art = '" & CStr(Me.DGridArticulos.CurrentCell.Value) & "'")

                If datos.Rows.Item(0).Item(0) IsNot DBNull.Value Then


                    Me.DGridArticulos.CurrentRow.Cells(1).Value = datos.Rows.Item(0).Item(0)
                    Me.DGridArticulos.CurrentRow.Cells(2).Value = datos.Rows.Item(0).Item(1)
                    Me.DGridArticulos.CurrentRow.Cells(4).Value = datos.Rows.Item(0).Item(2)
                Else
                    MsgBox("Artículo sin existencias en este almacén", MsgBoxStyle.Exclamation, "Atención")
                    Me.DGridArticulos.CurrentRow.Cells(0).Value = Nothing

                End If

            Catch ex As Exception
                MsgBox("Artículo sin existencias en este almacén", MsgBoxStyle.Exclamation, "Atención")
                Me.DGridArticulos.CurrentRow.Cells(0).Value = Nothing
            End Try

        End If

    End Sub
    Private Function ValidaAlmacenes() As Boolean
        If ZctSOTMain.Inventario_en_marcha(Convert.ToInt32(Me.ZctAlmSalida.ValueItemStr)) Then
            MsgBox("El almacén de salida esta corriendo inventario y no puede ser utilizado en este momento.")
            Return False
        End If

        If ZctSOTMain.Inventario_en_marcha(Convert.ToInt32(Me.ZctAlmEntrada.ValueItemStr)) Then
            MsgBox("El almacén de entrada esta corriendo inventario y no puede ser utilizado en este momento.")
            Return False
        End If
        Return True
    End Function

    Private Sub grabarMovimiento(ByVal renglon As Integer, ByVal folio As Integer, ByRef dbCon As ZctDataBase)
        Try


            dbCon.IniciaProcedimiento("SP_traspasarArticulos")

            'Agregamos los parametros
            dbCon.AddParameterSP("@Codigo", Me.DGridArticulos.Rows.Item(renglon).Cells.Item(0).Value, SqlDbType.Char)
            dbCon.AddParameterSP("@Cantidad", Me.DGridArticulos.Rows.Item(renglon).Cells.Item(3).Value, SqlDbType.Money)
            dbCon.AddParameterSP("@AlmacenEnvia", Me.ZctAlmSalida.ValueItemStr, SqlDbType.Int)
            dbCon.AddParameterSP("@AlmacenDest", Me.ZctAlmEntrada.ValueItemStr, SqlDbType.Int)
            dbCon.AddParameterSP("@Folio", folio, SqlDbType.VarChar)
            dbCon.AddParameterSP("@Costo", Me.DGridArticulos.Rows.Item(renglon).Cells.Item(5).Value, SqlDbType.Money)
            dbCon.AddParameterSP("@Fecha", Me.DtAplicacion.Value, SqlDbType.SmallDateTime)

            dbCon.GetScalarSPTran()

        Catch ex As SqlClient.SqlException
            Throw ex
        End Try

    End Sub

    Private Sub cancelarMovimientos()

        If String.IsNullOrWhiteSpace(Me.ZctAlmEntrada.ValueItemStr) Then
            Throw New SOTException("Seleccione un almacén de entrada")
        End If

        If String.IsNullOrWhiteSpace(Me.ZctAlmSalida.ValueItemStr) Then
            Throw New SOTException("Seleccione un almacén de salida")
        End If

        Dim almIndex As Object = Me.ZctAlmEntrada.value

        Me.ZctAlmEntrada.value = Me.ZctAlmSalida.value
        Me.ZctAlmSalida.value = almIndex


        grabarMovimientos(True)

    End Sub

    Private Sub limpiar()

        Me.DGridArticulos.Rows.Clear()
        Me.lblTotal.Text = "0"
        Me.lblSubTotal.Text = "0"
        Me.lblIva.Text = "0"
        Me.ZctAlmEntrada.value = 1
        Me.ZctAlmSalida.value = 1
        Me.DtAplicacion.Value = Now()
        Me.iFolio = 0
        Me.txtFolio.Text = CStr(Me.folios.Item((Me.iFolio)))


    End Sub

    Private Sub grabarMovimientos()
        grabarMovimientos(False)
    End Sub

    Private Sub grabarMovimientos(ByVal eliminar As Boolean)

        'Validamos los movimientos
        If Not movimientosValidos(eliminar) Then
            MsgBox("Verifique los movimientos", MsgBoxStyle.Exclamation, "Atención")
            Exit Sub
        End If

        Dim db As New Datos.ZctDataBase()

        Try

            'Iniciamos la conexión, y una transacción
            db.OpenConTran()
            db.Inicia_Transaccion()

            'Obtenemos un folio nuevo para los movimientos
            Dim folio As Integer = obtenerNuevoFolio(db)

            'Grabamos cada uno de los movimientos
            If eliminar Then
                For i As Integer = 0 To Me.DGridArticulos.Rows.Count - 1
                    grabarMovimiento(i, folio, db)
                Next
            Else
                For i As Integer = 0 To Me.DGridArticulos.Rows.Count - 2
                    grabarMovimiento(i, folio, db)
                Next

            End If


            db.Termina_Transaccion()
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctIncAlmacenes", "A", txtFolio.Text)


            cargarFolios()
            limpiar()

            'Ponemos el nuevo folio en el campo de texto
            Me.txtFolio.Text = CStr(Me.folios(0))
        Catch ex As Exception

            'Si hubo un error, cancelamos la transacción
            db.Cancela_Transaccion()

        Finally
            db.CloseConTran()
            db.Dispose()
        End Try


    End Sub

#End Region

#Region "Cálculos"

    Private Sub calculaSubTotalArticulo()
        DGridArticulos.CurrentRow.Cells.Item(5).Value = CDbl(DGridArticulos.CurrentRow.Cells.Item(3).Value) *
                              CDbl(DGridArticulos.CurrentRow.Cells.Item(4).Value)

    End Sub

    Private Sub calculaSubTotal()
        Dim cuenta As Double = 0
        Dim nRenglones As Integer = Me.DGridArticulos.Rows.Count - 1

        For i As Integer = 0 To nRenglones
            cuenta += CDbl(Me.DGridArticulos.Rows.Item(i).Cells.Item(5).Value)
        Next

        Me.lblSubTotal.Text = CStr(cuenta)
    End Sub

    Private Sub calculaIva()
        Dim iva As Double = CDbl(cargarTabla("select * from ZctCatPar").Rows.Item(0).Item(0))
        Me.lblIva.Text = CStr(iva * CDbl(Me.lblSubTotal.Text))
    End Sub

    Private Sub calculaTotal()
        Me.lblTotal.Text = CStr(CDbl(Me.lblIva.Text) + CDbl(Me.lblSubTotal.Text))
    End Sub

#End Region

#Region "Folios"

    Private Function obtenerNuevoFolio(ByRef dbCon As ZctDataBase) As Integer

        'Me.txtFolio.Text = CStr(CInt(Me.txtFolio.Text) + 1)
        Return Me.folio.SetConsecutivo(dbCon)
    End Function

    Private Sub mostrarFolio(ByVal nFolio As Integer)

        If iFolio < 1 Then
            limpiar()
        Else

            Dim datos As DataTable = cargarTabla(
                "Select distinct a.Cod_Art, a.Desc_Art, e.ExistAct_Inv, abs(e.CtdMov_Inv), " &
                "a.Cos_Art,abs(a.Cos_Art*e.CtdMov_Inv), e.Cod_Alm ,e.FchMov_Inv , e.CodMov_Inv " &
                "from ZctEncMovInv e, ZctCatArt a, ZctArtXAlm aa  " &
                "where(a.Cod_Art = e.Cod_Art) " &
                "and aa.Cod_Art = e.Cod_Art " &
                "and CtdMov_Inv < 0" &
                "and e.TpOs_Inv = 'IA' and e.FolOS_Inv = " &
                nFolio)

            If datos.Rows(0).Item(0) IsNot DBNull.Value Then

                'Quitamos los datos anteriores
                DGridArticulos.Rows().Clear()

                'Agregamos los renglones que nos hagan falta
                If datos.Rows.Count - 1 > 0 Then
                    Me.DGridArticulos.Rows.Add(datos.Rows.Count - 1)
                End If

                'Renglones
                For i As Integer = 0 To datos.Rows.Count - 1

                    'Columnas
                    For j As Integer = 0 To 5
                        'Datos que van en el datagrind
                        Me.DGridArticulos(j, i).Value = datos.Rows.Item(i).Item(j)
                    Next

                Next

                'Datos que van en los otros campos

                'Almacén de entrada
                'COLCHO QUE HORROR!!!!!
                Me.ZctAlmSalida.value = datos.Rows(0).Item(6)

                'Almacén de salida
                Me.ZctAlmEntrada.value = cargarTabla("Select Cod_Alm from ZctEncMovInv where TpOS_Inv = 'IA' and TpMov_Inv = 'EN' and FolOS_Inv = " &
                                                    nFolio).Rows(0).Item(0)

                'Fecha del intercambio
                Me.DtAplicacion.Value = CDate(datos.Rows(0).Item(7))

                calculaSubTotal()
                calculaIva()
                calculaTotal()
            End If
        End If
    End Sub

    Private Sub cargarFolios()

        'Establecemos el indice de folios en 0
        Me.iFolio = 0

        'Si la lista tiene elementos, los quitamos
        Me.folios.Clear()

        'Cargamos los folios de la base de datos
        Dim datos As DataTable = cargarTabla("Select distinct convert(int,FolOS_Inv) from ZctEncMovInv " &
                                             "where TpOs_Inv = 'IA' order by convert(int,FolOS_Inv) desc")

        'Los pasamos a la lista
        'datos.Select.ToList().ForEach(AddressOf guardaFolio)
        'datos.Select.FindAll(AddressOf guardaFolio)
        For Each renglon As DataRow In datos.Rows
            folios.Add(CInt(renglon.Item(0)))
        Next

        'AHL
        If folios.Count > 0 Then
            'Insertamos el número del folio nuevo
            Me.folios.Insert(0, Me.folios.Item(0) + 1)
        Else
            Me.folios.Insert(0, 1)
        End If

        ' Me.folio.SetConsecutivo( 
        'Ponemos el nuevo folio en el campo de texto
        Me.txtFolio.Text = CStr(Me.folios.Item(0))
        'Liberamos los recursos de la cache de la tabla
        datos.Dispose()

    End Sub

    Private Sub actualizaIndex()
        Me.iFolio = Me.folios.FindIndex(AddressOf ecuentraFolio)
    End Sub

    'Función delegada para encontrar un folio que concuerde con el número dado
    Private Function ecuentraFolio(ByVal f As Integer) As Boolean
        Return f = CInt(Me.txtFolio.Text)
    End Function

    Private Sub buscaFolio()
        Dim busqueda As New ZctBusqueda

        busqueda.sSPName = "SP_ZctCatFolios"
        busqueda.sSpVariables = "@Cod_Folio;VARCHAR"
        busqueda.ShowDialog(Me)
        Me.txtFolio.Text = busqueda.iValor

    End Sub

#End Region

#Region "Eventos"

    Private Sub DGridArticulos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGridArticulos.KeyDown
        If e.KeyCode = Keys.F3 Then
            'Dim busqueda As New ZctBusqueda
            'busqueda.sSPName = "SP_ZctCatArt"
            'busqueda.sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT"
            'busqueda.ShowDialog(Me)
            'DGridArticulos.BeginEdit(True)
            'DGridArticulos.CurrentRow.Cells.Item(0).Value = busqueda.iValor
            'DGridArticulos.EndEdit()
            'DGridArticulos.UpdateCellValue(0, DGridArticulos.CurrentCell.RowIndex)
            'ColSurt.Selected = True

            Try
                Dim fBusqueda = FabricaBuscadores.ObtenerBuscadorArticulos()
                fBusqueda.SoloInventariables = True
                fBusqueda.Mostrar()
                If Not IsNothing(fBusqueda.ArticuloSeleccionado) Then
                    DGridArticulos.BeginEdit(True)
                    DGridArticulos.CurrentRow.Cells.Item(0).Value = fBusqueda.ArticuloSeleccionado.Codigo ' .iValor
                    DGridArticulos.EndEdit()
                    DGridArticulos.UpdateCellValue(0, DGridArticulos.CurrentRow.Index)
                    ColSurt.Selected = True
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
                MsgBox(ex.Message.ToString)
            End Try
        End If
    End Sub


    Private Sub ZctIncAlmacenes_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _Permisos = _Controlador.ObtenerPermisosActuales() '_Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        With _Permisos

            DGridArticulos.AllowUserToAddRows = .CrearIntercambiosAlmacenes
            DGridArticulos.AllowUserToDeleteRows = .EliminarIntercambiosAlmacenes
            DGridArticulos.ReadOnly = Not (.ModificarIntercambiosAlmacenes Or .CrearIntercambiosAlmacenes)
            If .ModificarIntercambiosAlmacenes = False And .CrearIntercambiosAlmacenes = False And .EliminarIntercambiosAlmacenes = False Then
                cmdAceptar.Enabled = False
            Else
                cmdAceptar.Enabled = True
            End If
        End With
        cargarAlmacenes()

        cargarFolios()

        Me.DGridArticulos.Focus()
    End Sub

    Private Sub DGridArticulos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellEndEdit

        'Si se cambia un codigo de articulo
        If DGridArticulos.CurrentCell.ColumnIndex = 0 Then
            cargarArticulos()

            'Si se cambia el surtido
        ElseIf DGridArticulos.CurrentCell.ColumnIndex = 3 Then
            calculaSubTotalArticulo()
            calculaSubTotal()
            calculaIva()
            calculaTotal()

        End If
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        If ValidaAlmacenes() Then
            grabarMovimientos()
        End If

    End Sub

    Private Sub cmdIzq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIzq.Click

        If Not Me.iFolio + 1 >= Me.folios.Count Then
            Me.iFolio = Me.iFolio + 1
            Me.txtFolio.Text = CStr(Me.folios.Item(Me.iFolio))
            mostrarFolio(Me.folios.Item(Me.iFolio))
        End If
    End Sub

    Private Sub cmdDer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDer.Click

        If Not Me.iFolio - 1 < 0 Then
            Me.iFolio = Me.iFolio - 1
            Me.txtFolio.Text = CStr(Me.folios.Item(Me.iFolio))
            mostrarFolio(Me.folios.Item(Me.iFolio))
        End If
    End Sub

    Private Sub txtFolio_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolio.lostfocus
        Try
            mostrarFolio(CInt(Me.txtFolio.Text))
            actualizaIndex()
        Catch ex As Exception
            MsgBox("Folio incorrecto", MsgBoxStyle.Exclamation, "Atención")
        End Try
    End Sub

    Private Sub txtFolio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFolio.KeyDown
        If e.KeyCode = Keys.F3 Then
            buscaFolio()
        End If
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If MsgBox("¿Realmente desea cancelar el intercambio?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            cancelarMovimientos()
        End If
    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        imprimir()
    End Sub

    Private Sub ZctSOTButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZctSOTButton1.Click
        ActualicacionCritica()
    End Sub

    Private Sub ZctSOTButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZctSOTButton2.Click
        actualizarMovimiento()
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        limpiar()
    End Sub

#End Region

#Region "Reportes"

    'If txtFolio.Text = "0" Or txtFolio.Text = "" Then Exit Sub
    'Dim myCr As New PkVisorRpt
    '    myCr.MdiParent = Me.MdiParent
    ''myCr.sDataBase = cboServer.SelectedValue
    '    myCr.sSQLV = "{ZctEncOC.Cod_OC} = " & CType(txtFolio.Text, Integer)

    '    myCr.sRpt = sPathRpt
    '    myCr.Show()

    Private Sub imprimir()

        Dim controlador = New SOTControladores.Controladores.ControladorAlmacenes()

        Dim folio As Integer

        If Integer.TryParse(txtFolio.Text, folio) Then

            Dim controladorGlobal = New ControladorConfiguracionGlobal()
            Dim controladorClientes = New ControladorClientes()

            Dim config = controladorGlobal.ObtenerConfiguracionGlobal()
            Dim datosFiscales = controladorClientes.ObtenerUltimoCliente()


            Dim rutaL = ReportesSOT.Parametros.Logos.RutaLogoGeneral

            If File.Exists(ReportesSOT.Parametros.Logos.RutaLogoGeneral) Then

                rutaL = Path.GetTempFileName() + Guid.NewGuid().ToString()

                While File.Exists(rutaL)
                    rutaL = Path.GetTempFileName() + Guid.NewGuid().ToString()
                End While

                File.Copy(ReportesSOT.Parametros.Logos.RutaLogoGeneral, rutaL, True)

            End If

            Dim encabezadosReporte = New List(Of ReportesSOT.Dtos.DtoCabeceraReporte) From {
                New ReportesSOT.Dtos.DtoCabeceraReporte With
                {
                    .Direccion = config.Direccion,
                    .FechaInicio = Date.Now,
                    .FechaFin = Date.Now,
                    .Hotel = config.Nombre,
                    .RazonSocial = datosFiscales.RazonSocial,
                    .Usuario = ControladorBase.UsuarioActual.NombreCompleto,
                    .RutaLogo = rutaL
                }
            }


            Dim encabezado = controlador.SP_ObtenerResumenIntercambio(folio)
            Dim encabezados = New List(Of Modelo.Almacen.Entidades.Dtos.DtoEncabezadoIntercambio) From {
                encabezado
            }

            Dim documentoReporte = New ReportesSOT.Inventarios.IntercambioAlmacenes()
            documentoReporte.Load()
            documentoReporte.Database.Tables("EncabezadoReporte").SetDataSource(encabezadosReporte)
            documentoReporte.Database.Tables("EncabezadoIntercambio").SetDataSource(encabezados)
            documentoReporte.Database.Tables("DetallesIntercambio").SetDataSource(encabezado.Detalles)

            Dim visorR = New VisorReportes()
            visorR.crCrystalReportViewer.ReportSource = documentoReporte
            visorR.Update()

            visorR.ShowDialog(Me)

            'Dim visor As New PkVisorRpt
            'visor.MdiParent = Me.MdiParent
            'visor.sParam = "Folio;" & Me.txtFolio.Text
            ''visor.sLeyenda = Me.txtFolio.Text

            'visor.sRpt = "C:\REPORTES\IA.rpt"
            'visor.Show()
        End If

    End Sub

#End Region

#Region "Validaciones"
    Private Function operacionPermitida(ByVal renglon As Integer) As Boolean

        If Me.DGridArticulos.Rows.Item(renglon).Cells.Item(3).Value IsNot Nothing Then

            'Numero de existencias
            Dim res As Object = cargarTabla("select Exist_Art from ZctArtXAlm where Cod_Art = '" &
                                           CStr(Me.DGridArticulos.Rows(renglon).Cells(0).Value) &
                                           "' and Cod_Alm = " &
                                           Me.ZctAlmSalida.ValueItemStr).Rows.Item(0).Item(0)

            'Si el resultado es null ó 0, regresa falso, en caso contrario regresa verdadero
            If res IsNot DBNull.Value Then
                If CInt(res) >= CInt(Me.DGridArticulos.Rows.Item(renglon).Cells.Item(3).Value) Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function

    Private Function movimientosValidos(ByVal elimina As Boolean) As Boolean

        If String.IsNullOrWhiteSpace(Me.ZctAlmEntrada.ValueItemStr) Then
            MsgBox("Seleccione un almacén de entrada", MsgBoxStyle.Exclamation)
            Return False
        End If

        If String.IsNullOrWhiteSpace(Me.ZctAlmSalida.ValueItemStr) Then
            MsgBox("Seleccione un almacén de salida", MsgBoxStyle.Exclamation)
            Return False
        End If

        'Primero verificamos si es correcta la información de los alamacenes
        If Me.ZctAlmEntrada.ValueItemStr.Equals(Me.ZctAlmSalida.ValueItemStr) Then
            MsgBox("Los almacenes deben ser distintos", MsgBoxStyle.Exclamation)
            Return False
        End If

        'Renglones que se van a omitir
        Dim renglones As Integer

        If elimina Then
            renglones = 1
        Else
            renglones = 2
        End If

        'Verificamos que se tengan los renglones necesarios
        If Not Me.DGridArticulos.Rows.Count > renglones - 1 Then Return False

        For i As Integer = 0 To Me.DGridArticulos.Rows.Count - renglones
            'Verificamos que los movimientos que se van a realizar sean validos
            If Not operacionPermitida(i) Then
                MsgBox("Verifique las existencias", MsgBoxStyle.Exclamation, "Atención")
                Return False
            End If

            'Verificamos que cada renglon tenga un articulo valido
            If Me.DGridArticulos.Rows.Item(i).Cells.Item(0).Value Is Nothing Then
                MsgBox("Verifique las existencias", MsgBoxStyle.Exclamation, "Atención")
                Return False
            End If

        Next

        'Si llegamos hasta aquí, quiere decir que todo es valido
        Return True

    End Function
#End Region

#Region "Actualizaciones"

    Private Sub ActualicacionCritica()

        'Primero obtenemos la lista de movimientos que tienen los datos incorrectos

        Dim datos As DataTable = cargarTabla( _
            "Select CodMov_Inv from ZctEncMovInv where TpOs_Inv = 'IA'")

        'Si los datos se no cargaron correctamente, salimos
        If datos Is Nothing Then
            Throw New Exception("No hay movimientos del tipo IA que cargar")
            Exit Sub

        ElseIf datos Is DBNull.Value Then
            Throw New Exception("No hay movimientos del tipo IA que cargar")
            Exit Sub
        End If

        For Each renglon As DataRow In datos.Rows
            corregirMovimiento(CStr(renglon.Item(0)))
        Next
        MsgBox("Movimientos actualizados")
    End Sub

    Private Sub corregirMovimiento(ByRef codigo As String)
        Dim db As New Datos.ZctDataBase

        db.SQL = "update ZctEncMovInv set CosMov_Inv =  " & _
            "(select a.Cos_Art*e.CtdMov_Inv from ZctCatArt a, " & _
            "ZctEncMovInv e where a.Cod_Art =  e.Cod_Art and e.CodMov_Inv =" & codigo & _
            " ) where CodMov_Inv = " & codigo
        db.InciaDataAdapter()
        Try
            db.GetDataTable()
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error")
        Finally
            db.Dispose()
        End Try
    End Sub

    Private Sub actualizarMovimiento()
        Dim datos As DataTable

        Try
            'Cargamos los código de los movimientos correspondientes al folio actual
            datos = cargarTabla( _
            "select CodMov_Inv from ZctEncMovInv where TpOs_Inv = 'IA' and FolOS_Inv = " & _
            Me.txtFolio.Text)

        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Error")
        End Try

        'Si hay datos
        If datos IsNot Nothing AndAlso datos IsNot DBNull.Value Then
            For Each renglon As DataRow In datos.Rows
                'Corregimos cada uno de ellos
                corregirMovimiento(CStr(renglon.Item(0)))
            Next
        Else
            'Si no, lo avisamos
            MsgBox("No puedo encotrar ese movimiento, revise el folio", MsgBoxStyle.Critical, "Error")
        End If
        MsgBox("Movimientos actualizados")
    End Sub
#End Region

    Private Sub DGridArticulos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellContentClick

    End Sub
End Class
