﻿Imports ZctSOT.Clases.Catalogos
Imports ZctSOT.Clases.Servicio
Imports ZctSOT.DAO.Catalogos
Imports ZctSOT.DAO.Servicio
Imports ZctSOT.Datos.Clases.Catalogos
Imports ZctSOT.Datos.Clases.Servicio
Imports ZctSOT.Datos
Imports ZctSOT.Datos.DAO.Servicio
Imports SOTControladores.Controladores

Public Class frmMotosPorMes
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
    Private MotosPorMes As New List(Of ZctMotosPorAnno)
    Private Sub frmMotosPorMes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        With _Permisos
            gridMotos.AllowUserToAddRows = .agrega
            gridMotos.AllowUserToDeleteRows = .elimina
            gridMotos.ReadOnly = Not (.edita Or .agrega)

            If .edita = False And .agrega = False And .elimina = False Then
                cmdGrabar.Enabled = False
            Else
                cmdGrabar.Enabled = True
            End If

        End With
        CargaMeses()
    End Sub

    Public Sub CargaMeses()
        Dim meses As New List(Of Clases.Catalogos.ZctMeses)
        meses = Datos.DAO.Catalogos.ZctDAOMeses.GetMeses()
        cboMes.DataSource = meses
        cboMes.ValueMember = "Mes"
        cboMes.DisplayMember = "NombreMes"
    End Sub

    Private Sub cmdCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCargar.Click
        Try
            CargaMotosPorMes(ctrlAnno.Text, cboMes.SelectedValue)
            CargaMotosAGrid()
        Catch ex As ArgumentException
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CargaMotosPorMes(ByVal Anno As Integer, ByVal Mes As Integer)
        MotosPorMes = New List(Of ZctMotosPorAnno)
        Dim regiones As List(Of ZctRegion) = CargaRegiones()
        For Each Region As ZctRegion In regiones
            Dim motos As ZctMotosPorAnno = ObtieneMotosPorMes(Anno, Mes, Region)
            MotosPorMes.Add(motos)
        Next
    End Sub

    Private Function CargaRegiones() As List(Of ZctRegion)
        Dim regiones As New List(Of ZctRegion)
        regiones = Datos.DAO.Catalogos.ZctDAORegion.GetRegiones
        Return regiones
    End Function

    Private Function ObtieneMotosPorMes(ByVal Anno As Integer, ByVal Mes As Integer, ByVal Region As ZctRegion) As ZctMotosPorAnno
        Dim motos As New ZctMotosPorAnno
        motos.Anno = Anno
        motos.Mes = Mes
        motos.Cod_Region = Region.Codigo
        motos.Desc_Region = Region.Descripcion
        motos.NoMotos = ZctDAOMotosPorAnno.GetMotosPorMes(Anno, Mes, motos.Cod_Region)
        Return motos
    End Function

    Private Sub CargaMotosAGrid()
        ZctMotosPorAnnoBindingSource.Clear()
        ZctMotosPorAnnoBindingSource.DataSource = MotosPorMes
        gridMotos.Refresh()
    End Sub

    Private Sub cmdGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGrabar.Click
        Try
            ZctDAOMotosPorAnno.GrabaMotocicletas(MotosPorMes)
            MsgBox("Las motos han sido cargadas correctamente")
            LimpiaForma()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub LimpiaForma()
        MotosPorMes = New List(Of ZctMotosPorAnno)
        CargaMotosAGrid()
    End Sub

End Class