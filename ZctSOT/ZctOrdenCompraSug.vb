﻿Imports ZctSOT.Datos
Imports SOTControladores.Controladores

Public Class ZctOrdenCompraSug
    Private _Rdn As New RDN.Compras.ZctOCSRDN
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
    Private Sub frmOrdenCompraSug_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        With _Permisos
            If .edita = False And .agrega = False And .elimina = False Then
                cmdAceptar.Enabled = False
            Else
                cmdAceptar.Enabled = True
            End If
        End With
        txtCod_Alm.SqlBusqueda = "SELECT Cod_Alm AS Código, Desc_CatAlm AS Almacén FROM ZctCatAlm where ZctCatArt.desactivar is null OR ZctCatArt.desactivar = 0"
        txtCod_Alm.SPName = "SP_ZctCatAlm"
        txtCod_Alm.SpVariables = "@Cod_Alm;INT|@Desc_CatAlm;VARCHAR"
        txtCod_Alm.Tabla = "ZctCatAlm"
        chkEn_Equilibrio.Checked = True
        chkFaltante.Checked = True
        chkSobrante.Checked = True
        cboDpto.GetData("ZctCatDpto", True)
        cboLinea.Clear()
    End Sub

    Private Sub cmdLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoad.Click
        'Try
        load_ocs()

        'Catch ex As ZctReglaNegocioEx
        '    MsgBox(ex.Message)
        'Catch ex As Exception
        '    MsgBox("Ha ocurrido un error.")
        'End Try
    End Sub

    Private Sub ZctSotDate1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateAplica.ValueChanged

    End Sub

    Private Sub HDCampos(ByVal Habilita As Boolean)
        'Deshabilita
        txtCod_Alm.Enabled = Not Habilita
        cboDpto.Enabled = Not Habilita
        DateAplica.Enabled = Not Habilita
        cboLinea.Enabled = Not Habilita
        cmdLoad.Enabled = Not Habilita
        'Habilita
        cmdAceptar.Enabled = Habilita
        chkEn_Equilibrio.Enabled = Habilita
        chkFaltante.Enabled = Habilita
        chkSobrante.Enabled = Habilita

    End Sub

    Private Sub SetGrid()
        With GridMov
            .AutoGenerateColumns = False
            .DataSource = Nothing
            .Columns.Clear()
            .SetColumn("Cod_Art", "Cod_Art", "Artículo", , , , False)
            .SetColumn("Desc_Art", "Desc_Art", "Descripción", True, "", 150, False)
            For i As Integer = 1 To _Rdn.Cfg.Meses
                .SetColumn("Mes" & i.ToString, "Mes" & i.ToString, GetMes(DateAdd(DateInterval.Month, -1 * (i - 1), _Rdn.FechaIni).Month), True, "", 65, False)
                .Columns("Mes" & i.ToString).DefaultCellStyle.BackColor = Color.LightBlue
            Next

            .SetColumn("MovProm", "MovProm", "Promedio", True, "###,##0", 65, False)

            .SetColumn("Exist_Art", "Exist_Art", "Existencia", True, "###,##0", 65, False)

            .SetColumn("SOQ", "SOQ", "Sugerencia", True, "###,##0", 65, False)
            .SetColumn("Estatus", "Estatus", "Estatus", True, , , False)
            .SetColumn("AComprar", "AComprar", "A Comprar", True, "###,##0", 65)

        End With

    End Sub

    Public Function GetMes(ByVal Mes As Integer) As String
        Select Case Mes
            Case 1
                Return "Enero"
            Case 2
                Return "Febrero"
            Case 3
                Return "Marzo"
            Case 4
                Return "Abril"
            Case 5
                Return "Mayo"
            Case 6
                Return "Junio"
            Case 7
                Return "Julio"
            Case 8
                Return "Agosto"
            Case 9
                Return "Septiembre"
            Case 10
                Return "Octubre"
            Case 11
                Return "Noviembre"
            Case 12
                Return "Diciembre"
            Case Else
                Return ""
        End Select

    End Function
    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub cmdCfgAvanzada_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCfgAvanzada.Click
        Try
            Dim cfgAv As New ZctConfOCS
            cfgAv.Modular = True
            cfgAv.ShowDialog(Me)
            If txtCod_Alm.Text <> "" Then load_ocs()
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error.")
        End Try
    End Sub

    Private Sub load_ocs()
        If txtCod_Alm.Text = "" OrElse txtCod_Alm.Text = "0" Then
            MsgBox("Debe seleccionar un almacén.")
            txtCod_Alm.Focus()
            Exit Sub
        End If

        _Rdn.FechaIni = DateAplica.Value
        _Rdn.Cod_alm = txtCod_Alm.Text
        _Rdn.Cod_Dpto = cboDpto.value
        _Rdn.Cod_Linea = cboLinea.value
        _Rdn.GetLista()
        chkEn_Equilibrio.Checked = True
        chkFaltante.Checked = True
        chkSobrante.Checked = True
        SetGrid()
        GridMov.DataSource = _Rdn.Lista
        Pinta()
        HDCampos(True)
    End Sub

    Private Sub cboDpto_lostfocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDpto.lostfocus
        cboLinea.GetData("ZctCatLineaXDpto", cboDpto.value.ToString, True)
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If txtCod_Alm.Enabled Then
            Me.Close()
        Else
            HDCampos(False)
            txtCod_Alm.Text = ""
            txtCod_Alm.Descripcion = ""
            cboDpto.value = 0
            DateAplica.Value = Now
            cboLinea.Limpia()
            _Rdn.FechaIni = Now
            _Rdn.Cod_alm = 0
            _Rdn.Cod_Dpto = 0
            _Rdn.Cod_Linea = 0
            SetGrid()
            _Rdn.Lista.Clear()
            GridMov.DataSource = Nothing
            txtCod_Alm.Focus()
            chkEn_Equilibrio.Checked = True
            chkFaltante.Checked = True
            chkSobrante.Checked = True
        End If
    End Sub

    Public Sub Filtra()
        With GridMov
            .CurrentCell = Nothing
            For i As Integer = 0 To .Rows.Count - 1
                Select Case DirectCast(.Rows(i).Cells("Estatus").Value, Clases.Compras.Estatus_OCS)
                    Case Clases.Compras.Estatus_OCS.En_Equilibrio
                        .Rows(i).Visible = chkEn_Equilibrio.Checked
                    Case Clases.Compras.Estatus_OCS.Faltante
                        .Rows(i).Visible = chkFaltante.Checked
                    Case Clases.Compras.Estatus_OCS.Sobrante
                        .Rows(i).Visible = chkSobrante.Checked
                End Select

            Next
        End With
    End Sub

    Public Sub Pinta()
        With GridMov
            For i As Integer = 0 To .Rows.Count - 1
                Select Case DirectCast(.Rows(i).Cells("Estatus").Value, Clases.Compras.Estatus_OCS)
                    Case Clases.Compras.Estatus_OCS.En_Equilibrio
                        .Rows(i).Cells("Estatus").Style.BackColor = lblEn_Equilibrio.BackColor

                    Case Clases.Compras.Estatus_OCS.Faltante
                        .Rows(i).Cells("Estatus").Style.BackColor = lblFaltante.BackColor

                    Case Clases.Compras.Estatus_OCS.Sobrante
                        .Rows(i).Cells("Estatus").Style.BackColor = lblSobrante.BackColor
                End Select

            Next
            '   .Rows.Item(0).Cells(0).Style.BackColor
        End With
    End Sub

    Private Sub chkEn_Equilibrio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEn_Equilibrio.Click, chkFaltante.Click, chkSobrante.Click
        Filtra()
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Dim Ocs As New ZctOrdCompra
        Dim PkConAlone As New Datos.ZctDataBase                         'Objeto de la base de datos
        Dim sVariables As String
        Dim sParametros As String
        Dim OcFolio As New zctFolios("OC")
        Try
            PkConAlone.OpenConTran()
            PkConAlone.Inicia_Transaccion()
            Dim iFolio As Integer
            iFolio = OcFolio.SetConsecutivo(PkConAlone)  'GetData("Procedimientos_MAX", "ZctEncOC", PkConAlone)

            sVariables = "@Cod_OC;INT|@Cod_Prov;INT|@Fch_OC;SMALLDATETIME|@FchAplMov;SMALLDATETIME|@Fac_OC;VARCHAR|@FchFac_OC;SMALLDATETIME"

            sParametros = iFolio.ToString & "|0|" & Now.Date & "|" & Now.Date & "||" & Now.Date
            GetData(2, "SP_ZctEncOC", sVariables, sParametros, PkConAlone)

            sVariables = "@Cod_OC;INT|@CodDet_Oc;INT|@Cod_Art;VARCHAR|@Ctd_Art;INT|@Cos_Art;DECIMAL|@CtdStdDet_OC;INT|@FchAplMov;SMALLDATETIME|@Cat_Alm;INT"
            For Each Art As Clases.Compras.ZctArtDet In _Rdn.Lista
                If Art.Cod_Art <> "" AndAlso Art.AComprar > 0 Then

                    sParametros = iFolio.ToString & "|0|" & Art.Cod_Art & "|" & Art.AComprar & "|" & Art.Cos_Art & "|0|" & Now.Date & "|" & _Rdn.Cod_alm

                    GetData(2, "SP_ZctDetOC", sVariables, sParametros, PkConAlone)
                End If

            Next

            PkConAlone.Termina_Transaccion()
            PkConAlone.CloseConTran()
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdCompraSugerida", "A", iFolio.ToString)

            'Abre la orden de compra normal y carga la orden de compra en cuestion
            ZctOrdCompra.Show()
            ZctOrdCompra.CargaCompra(iFolio.ToString)
            ZctOrdCompra.Focus()
            ZctOrdCompra.MdiParent = Me.MdiParent
            Me.Close()
        Catch ex As Exception
            PkConAlone.Cancela_Transaccion()
            PkConAlone.CloseConTran()
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpVariables As String, ByVal sSpValores As String, ByVal PkCon As ZctDataBase) As DataTable

        'Objeto de la base de datos
        Dim iSp As Integer                                      'Indice del procedimiento almacenado
        Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
        Dim sValores() As String = Split(sSpValores, "|")
        'Inicia el procedimieto almacenado
        PkCon.IniciaProcedimiento(sSpNAME)
        'Agrega el parametro que define que se va a realizar
        PkCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
        'Obtiene el total de los procedimientos
        Dim iTot As Integer = UBound(sVector)
        'Recorre las variables del procedimiento almacenado
        For iSp = 0 To iTot
            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1).ToUpper
                Case "INT"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Integer), SqlDbType.Int)
                Case "VARCHAR"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.VarChar)
                Case "SMALLDATETIME"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Date), SqlDbType.SmallDateTime)
                Case "DECIMAL"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Decimal), SqlDbType.Decimal)
                Case "TEXT"
                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)
            End Select
        Next

        Select Case TpConsulta

            Case 2
                'Ejecuta el escalar
                PkCon.GetScalarSPTran()
                Return Nothing
            Case Else
                Return Nothing
        End Select

    End Function

End Class