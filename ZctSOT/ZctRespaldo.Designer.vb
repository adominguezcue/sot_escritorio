<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctRespaldo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ZctTxtPath = New ZctSOT.ZctControlTexto
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.ZctGroupControls1 = New ZctSOT.ZctGroupControls
        Me.ZctGroupControls1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ZctTxtPath
        '
        Me.ZctTxtPath.Location = New System.Drawing.Point(15, 19)
        Me.ZctTxtPath.Multiline = False
        Me.ZctTxtPath.Name = "ZctTxtPath"
        Me.ZctTxtPath.Nombre = "Archivo:"
        Me.ZctTxtPath.Size = New System.Drawing.Size(332, 28)
        Me.ZctTxtPath.TabIndex = 0
        Me.ZctTxtPath.Tag = "De click en examinar para indicar en donde se va a guardar el respaldo"
        Me.ZctTxtPath.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.ZctTxtPath.ToolTip = ""
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(353, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Examinar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(357, 81)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Respaldar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ZctGroupControls1
        '
        Me.ZctGroupControls1.Controls.Add(Me.ZctTxtPath)
        Me.ZctGroupControls1.Controls.Add(Me.Button1)
        Me.ZctGroupControls1.Location = New System.Drawing.Point(4, 6)
        Me.ZctGroupControls1.Name = "ZctGroupControls1"
        Me.ZctGroupControls1.Size = New System.Drawing.Size(434, 59)
        Me.ZctGroupControls1.TabIndex = 3
        Me.ZctGroupControls1.TabStop = False
        '
        'ZctRespaldo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 117)
        Me.Controls.Add(Me.ZctGroupControls1)
        Me.Controls.Add(Me.Button2)
        Me.Name = "ZctRespaldo"
        Me.Text = "Respaldo de la base de datos"
        Me.ZctGroupControls1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ZctTxtPath As ZctSOT.ZctControlTexto
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ZctGroupControls1 As ZctSOT.ZctGroupControls

End Class
