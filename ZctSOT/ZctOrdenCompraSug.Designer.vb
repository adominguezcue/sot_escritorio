﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctOrdenCompraSug
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ZctGroupControls2 = New ZctSOT.ZctGroupControls
        Me.chkFaltante = New System.Windows.Forms.CheckBox
        Me.chkSobrante = New System.Windows.Forms.CheckBox
        Me.chkEn_Equilibrio = New System.Windows.Forms.CheckBox
        Me.lblEn_Equilibrio = New ZctSOT.ZctSOTLabel
        Me.lblSobrante = New ZctSOT.ZctSOTLabel
        Me.lblFaltante = New ZctSOT.ZctSOTLabel
        Me.ZctGroupControls1 = New ZctSOT.ZctGroupControls
        Me.cboDpto = New ZctSOT.ZctControlCombo
        Me.cboLinea = New ZctSOT.ZctControlCombo
        Me.txtCod_Alm = New ZctSOT.ZctControlBusqueda
        Me.cmdLoad = New ZctSOT.ZctSOTButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.DateAplica = New ZctSOT.ZctSotDate
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox
        Me.cmdCfgAvanzada = New ZctSOT.ZctSOTButton
        Me.cmdCancelar = New ZctSOT.ZctSOTButton
        Me.cmdAceptar = New ZctSOT.ZctSOTButton
        Me.GridMov = New ZctSOT.ZctSotGrid
        Me.GroupBox1.SuspendLayout()
        Me.ZctGroupControls2.SuspendLayout()
        Me.ZctGroupControls1.SuspendLayout()
        Me.ZctSOTGroupBox1.SuspendLayout()
        CType(Me.GridMov, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.GridMov)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 128)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(981, 301)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Movimientos"
        '
        'ZctGroupControls2
        '
        Me.ZctGroupControls2.Controls.Add(Me.chkFaltante)
        Me.ZctGroupControls2.Controls.Add(Me.chkSobrante)
        Me.ZctGroupControls2.Controls.Add(Me.chkEn_Equilibrio)
        Me.ZctGroupControls2.Controls.Add(Me.lblEn_Equilibrio)
        Me.ZctGroupControls2.Controls.Add(Me.lblSobrante)
        Me.ZctGroupControls2.Controls.Add(Me.lblFaltante)
        Me.ZctGroupControls2.Location = New System.Drawing.Point(3, 92)
        Me.ZctGroupControls2.Name = "ZctGroupControls2"
        Me.ZctGroupControls2.Size = New System.Drawing.Size(981, 35)
        Me.ZctGroupControls2.TabIndex = 9
        Me.ZctGroupControls2.TabStop = False
        Me.ZctGroupControls2.Text = "Ver"
        '
        'chkFaltante
        '
        Me.chkFaltante.AutoSize = True
        Me.chkFaltante.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.chkFaltante.Location = New System.Drawing.Point(288, 16)
        Me.chkFaltante.Name = "chkFaltante"
        Me.chkFaltante.Size = New System.Drawing.Size(15, 14)
        Me.chkFaltante.TabIndex = 15
        Me.chkFaltante.UseVisualStyleBackColor = False
        '
        'chkSobrante
        '
        Me.chkSobrante.AutoSize = True
        Me.chkSobrante.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.chkSobrante.Location = New System.Drawing.Point(191, 16)
        Me.chkSobrante.Name = "chkSobrante"
        Me.chkSobrante.Size = New System.Drawing.Size(15, 14)
        Me.chkSobrante.TabIndex = 14
        Me.chkSobrante.UseVisualStyleBackColor = False
        '
        'chkEn_Equilibrio
        '
        Me.chkEn_Equilibrio.AutoSize = True
        Me.chkEn_Equilibrio.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.chkEn_Equilibrio.Location = New System.Drawing.Point(93, 16)
        Me.chkEn_Equilibrio.Name = "chkEn_Equilibrio"
        Me.chkEn_Equilibrio.Size = New System.Drawing.Size(15, 14)
        Me.chkEn_Equilibrio.TabIndex = 13
        Me.chkEn_Equilibrio.UseVisualStyleBackColor = False
        '
        'lblEn_Equilibrio
        '
        Me.lblEn_Equilibrio.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblEn_Equilibrio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEn_Equilibrio.Location = New System.Drawing.Point(17, 14)
        Me.lblEn_Equilibrio.Name = "lblEn_Equilibrio"
        Me.lblEn_Equilibrio.Size = New System.Drawing.Size(92, 18)
        Me.lblEn_Equilibrio.TabIndex = 10
        Me.lblEn_Equilibrio.Text = "En Equilibrio"
        '
        'lblSobrante
        '
        Me.lblSobrante.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblSobrante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSobrante.Location = New System.Drawing.Point(115, 14)
        Me.lblSobrante.Name = "lblSobrante"
        Me.lblSobrante.Size = New System.Drawing.Size(92, 18)
        Me.lblSobrante.TabIndex = 11
        Me.lblSobrante.Text = "Sobrante"
        '
        'lblFaltante
        '
        Me.lblFaltante.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lblFaltante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFaltante.Location = New System.Drawing.Point(213, 14)
        Me.lblFaltante.Name = "lblFaltante"
        Me.lblFaltante.Size = New System.Drawing.Size(92, 18)
        Me.lblFaltante.TabIndex = 12
        Me.lblFaltante.Text = "Faltante"
        '
        'ZctGroupControls1
        '
        Me.ZctGroupControls1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls1.Controls.Add(Me.cboDpto)
        Me.ZctGroupControls1.Controls.Add(Me.cboLinea)
        Me.ZctGroupControls1.Controls.Add(Me.txtCod_Alm)
        Me.ZctGroupControls1.Controls.Add(Me.cmdLoad)
        Me.ZctGroupControls1.Controls.Add(Me.Label1)
        Me.ZctGroupControls1.Controls.Add(Me.DateAplica)
        Me.ZctGroupControls1.Location = New System.Drawing.Point(3, 1)
        Me.ZctGroupControls1.Name = "ZctGroupControls1"
        Me.ZctGroupControls1.Size = New System.Drawing.Size(981, 92)
        Me.ZctGroupControls1.TabIndex = 0
        Me.ZctGroupControls1.TabStop = False
        '
        'cboDpto
        '
        Me.cboDpto.Location = New System.Drawing.Point(416, 17)
        Me.cboDpto.Name = "cboDpto"
        Me.cboDpto.Nombre = "Departamento:"
        Me.cboDpto.Size = New System.Drawing.Size(305, 28)
        Me.cboDpto.TabIndex = 3
        Me.cboDpto.value = Nothing
        Me.cboDpto.ValueItem = 0
        Me.cboDpto.ValueItemStr = Nothing
        '
        'cboLinea
        '
        Me.cboLinea.Location = New System.Drawing.Point(456, 51)
        Me.cboLinea.Name = "cboLinea"
        Me.cboLinea.Nombre = "Linea:"
        Me.cboLinea.Size = New System.Drawing.Size(265, 28)
        Me.cboLinea.TabIndex = 4
        Me.cboLinea.value = Nothing
        Me.cboLinea.ValueItem = 0
        Me.cboLinea.ValueItemStr = Nothing
        '
        'txtCod_Alm
        '
        Me.txtCod_Alm.Descripcion = ""
        Me.txtCod_Alm.Location = New System.Drawing.Point(6, 17)
        Me.txtCod_Alm.Name = "txtCod_Alm"
        Me.txtCod_Alm.Nombre = "Almacén"
        Me.txtCod_Alm.Size = New System.Drawing.Size(398, 27)
        Me.txtCod_Alm.SPName = Nothing
        Me.txtCod_Alm.SPParametros = Nothing
        Me.txtCod_Alm.SpVariables = Nothing
        Me.txtCod_Alm.SqlBusqueda = Nothing
        Me.txtCod_Alm.TabIndex = 0
        Me.txtCod_Alm.Tabla = Nothing
        Me.txtCod_Alm.TextWith = 111
        Me.txtCod_Alm.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCod_Alm.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtCod_Alm.Validar = True
        '
        'cmdLoad
        '
        Me.cmdLoad.Location = New System.Drawing.Point(860, 21)
        Me.cmdLoad.Name = "cmdLoad"
        Me.cmdLoad.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoad.TabIndex = 5
        Me.cmdLoad.Text = "Cargar"
        Me.cmdLoad.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Aplica:"
        '
        'DateAplica
        '
        Me.DateAplica.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateAplica.Location = New System.Drawing.Point(64, 50)
        Me.DateAplica.Name = "DateAplica"
        Me.DateAplica.Size = New System.Drawing.Size(97, 20)
        Me.DateAplica.TabIndex = 2
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCfgAvanzada)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdAceptar)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(3, 435)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(981, 51)
        Me.ZctSOTGroupBox1.TabIndex = 2
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'cmdCfgAvanzada
        '
        Me.cmdCfgAvanzada.Location = New System.Drawing.Point(9, 14)
        Me.cmdCfgAvanzada.Name = "cmdCfgAvanzada"
        Me.cmdCfgAvanzada.Size = New System.Drawing.Size(144, 28)
        Me.cmdCfgAvanzada.TabIndex = 0
        Me.cmdCfgAvanzada.Text = "Configuración Avanzada"
        Me.cmdCfgAvanzada.UseVisualStyleBackColor = True
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.Location = New System.Drawing.Point(860, 13)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(112, 28)
        Me.cmdCancelar.TabIndex = 2
        Me.cmdCancelar.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
            "alla"
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(742, 13)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(112, 28)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Tag = "Genera la orden de compra"
        Me.cmdAceptar.Text = "Generar Compra"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'GridMov
        '
        Me.GridMov.AllowUserToAddRows = False
        Me.GridMov.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridMov.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GridMov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridMov.DefaultCellStyle = DataGridViewCellStyle2
        Me.GridMov.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridMov.Location = New System.Drawing.Point(3, 16)
        Me.GridMov.Name = "GridMov"
        Me.GridMov.Size = New System.Drawing.Size(975, 282)
        Me.GridMov.TabIndex = 0
        '
        'ZctOrdenCompraSug
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(987, 489)
        Me.Controls.Add(Me.ZctGroupControls2)
        Me.Controls.Add(Me.ZctGroupControls1)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ZctOrdenCompraSug"
        Me.Text = "Compra sugerida"
        Me.GroupBox1.ResumeLayout(False)
        Me.ZctGroupControls2.ResumeLayout(False)
        Me.ZctGroupControls2.PerformLayout()
        Me.ZctGroupControls1.ResumeLayout(False)
        Me.ZctGroupControls1.PerformLayout()
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        CType(Me.GridMov, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GridMov As ZctSOT.ZctSotGrid
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdLoad As ZctSOT.ZctSOTButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DateAplica As ZctSOT.ZctSotDate
    Friend WithEvents ZctGroupControls1 As ZctSOT.ZctGroupControls
    Friend WithEvents txtCod_Alm As ZctSOT.ZctControlBusqueda
    Friend WithEvents cmdCfgAvanzada As ZctSOT.ZctSOTButton
    Friend WithEvents cboDpto As ZctSOT.ZctControlCombo
    Friend WithEvents cboLinea As ZctSOT.ZctControlCombo
    Friend WithEvents ZctGroupControls2 As ZctSOT.ZctGroupControls
    Friend WithEvents chkFaltante As System.Windows.Forms.CheckBox
    Friend WithEvents chkSobrante As System.Windows.Forms.CheckBox
    Friend WithEvents chkEn_Equilibrio As System.Windows.Forms.CheckBox
    Friend WithEvents lblEn_Equilibrio As ZctSOT.ZctSOTLabel
    Friend WithEvents lblSobrante As ZctSOT.ZctSOTLabel
    Friend WithEvents lblFaltante As ZctSOT.ZctSOTLabel
End Class
