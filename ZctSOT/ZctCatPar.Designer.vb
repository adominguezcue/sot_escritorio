﻿Imports ZctSOT.Datos

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctCatPar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Iva_CatParLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Me.Iva_CatParTextBox = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TxtRIA = New System.Windows.Forms.TextBox()
        Me.TxtMaximoCaja = New System.Windows.Forms.TextBox()
        Me.TxtMinimoCaja = New System.Windows.Forms.TextBox()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdGrabar = New System.Windows.Forms.Button()
        Me.ZctSOTLabel4 = New ZctSOT.ZctSOTLabel()
        Me.ZctCatParBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctSOTLabel3 = New ZctSOT.ZctSOTLabel()
        Me.ZctSOTLabel2 = New ZctSOT.ZctSOTLabel()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel()
        Iva_CatParLabel = New System.Windows.Forms.Label()
        Label1 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ZctCatParBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Iva_CatParLabel
        '
        Iva_CatParLabel.Anchor = System.Windows.Forms.AnchorStyles.None
        Iva_CatParLabel.AutoSize = True
        Iva_CatParLabel.Location = New System.Drawing.Point(9, 17)
        Iva_CatParLabel.Name = "Iva_CatParLabel"
        Iva_CatParLabel.Size = New System.Drawing.Size(27, 13)
        Iva_CatParLabel.TabIndex = 1
        Iva_CatParLabel.Text = "IVA:"
        '
        'Label1
        '
        Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(9, 58)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(66, 13)
        Label1.TabIndex = 4
        Label1.Text = "Minimo caja:"
        '
        'Label2
        '
        Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(9, 94)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(69, 13)
        Label2.TabIndex = 7
        Label2.Text = "Maximo caja:"
        '
        'Label3
        '
        Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(9, 131)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(173, 13)
        Label3.TabIndex = 10
        Label3.Text = "Ruta guardado iimagenes articulos:"
        '
        'Iva_CatParTextBox
        '
        Me.Iva_CatParTextBox.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Iva_CatParTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctCatParBindingSource, "Iva_CatPar", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, "0"))
        Me.Iva_CatParTextBox.Location = New System.Drawing.Point(107, 17)
        Me.Iva_CatParTextBox.Name = "Iva_CatParTextBox"
        Me.Iva_CatParTextBox.Size = New System.Drawing.Size(69, 20)
        Me.Iva_CatParTextBox.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.ZctSOTLabel4)
        Me.GroupBox1.Controls.Add(Label3)
        Me.GroupBox1.Controls.Add(Me.TxtRIA)
        Me.GroupBox1.Controls.Add(Me.ZctSOTLabel3)
        Me.GroupBox1.Controls.Add(Label2)
        Me.GroupBox1.Controls.Add(Me.TxtMaximoCaja)
        Me.GroupBox1.Controls.Add(Me.ZctSOTLabel2)
        Me.GroupBox1.Controls.Add(Label1)
        Me.GroupBox1.Controls.Add(Me.TxtMinimoCaja)
        Me.GroupBox1.Controls.Add(Me.ZctSOTLabel1)
        Me.GroupBox1.Controls.Add(Iva_CatParLabel)
        Me.GroupBox1.Controls.Add(Me.Iva_CatParTextBox)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(566, 168)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button1.Location = New System.Drawing.Point(413, 122)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(41, 26)
        Me.Button1.TabIndex = 13
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TxtRIA
        '
        Me.TxtRIA.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TxtRIA.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctCatParBindingSource, "RutaImagenesArticulos", True))
        Me.TxtRIA.Location = New System.Drawing.Point(186, 125)
        Me.TxtRIA.Name = "TxtRIA"
        Me.TxtRIA.Size = New System.Drawing.Size(218, 20)
        Me.TxtRIA.TabIndex = 11
        '
        'TxtMaximoCaja
        '
        Me.TxtMaximoCaja.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TxtMaximoCaja.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctCatParBindingSource, "MaximoCaja", True))
        Me.TxtMaximoCaja.Location = New System.Drawing.Point(107, 87)
        Me.TxtMaximoCaja.Name = "TxtMaximoCaja"
        Me.TxtMaximoCaja.Size = New System.Drawing.Size(69, 20)
        Me.TxtMaximoCaja.TabIndex = 8
        '
        'TxtMinimoCaja
        '
        Me.TxtMinimoCaja.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TxtMinimoCaja.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ZctCatParBindingSource, "MinimoCaja", True))
        Me.TxtMinimoCaja.Location = New System.Drawing.Point(107, 51)
        Me.TxtMinimoCaja.Name = "TxtMinimoCaja"
        Me.TxtMinimoCaja.Size = New System.Drawing.Size(69, 20)
        Me.TxtMinimoCaja.TabIndex = 5
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancelar.Location = New System.Drawing.Point(500, 177)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancelar.TabIndex = 4
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdGrabar
        '
        Me.cmdGrabar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdGrabar.Location = New System.Drawing.Point(419, 177)
        Me.cmdGrabar.Name = "cmdGrabar"
        Me.cmdGrabar.Size = New System.Drawing.Size(75, 23)
        Me.cmdGrabar.TabIndex = 5
        Me.cmdGrabar.Text = "Grabar"
        Me.cmdGrabar.UseVisualStyleBackColor = True
        '
        'ZctSOTLabel4
        '
        Me.ZctSOTLabel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ZctSOTLabel4.AutoSize = True
        Me.ZctSOTLabel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ZctSOTLabel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZctSOTLabel4.Location = New System.Drawing.Point(460, 124)
        Me.ZctSOTLabel4.Name = "ZctSOTLabel4"
        Me.ZctSOTLabel4.Size = New System.Drawing.Size(94, 28)
        Me.ZctSOTLabel4.TabIndex = 12
        Me.ZctSOTLabel4.Text = "Ruta guardado" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " imagenes articulo"
        '
        'ZctCatParBindingSource
        '
        Me.ZctCatParBindingSource.DataSource = GetType(Clases.Sistema.ZctCatPar)
        '
        'ZctSOTLabel3
        '
        Me.ZctSOTLabel3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ZctSOTLabel3.AutoSize = True
        Me.ZctSOTLabel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ZctSOTLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZctSOTLabel3.Location = New System.Drawing.Point(460, 87)
        Me.ZctSOTLabel3.Name = "ZctSOTLabel3"
        Me.ZctSOTLabel3.Size = New System.Drawing.Size(89, 28)
        Me.ZctSOTLabel3.TabIndex = 9
        Me.ZctSOTLabel3.Text = "Monto maximo  " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "existente en caja"
        '
        'ZctSOTLabel2
        '
        Me.ZctSOTLabel2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ZctSOTLabel2.AutoSize = True
        Me.ZctSOTLabel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ZctSOTLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZctSOTLabel2.Location = New System.Drawing.Point(460, 51)
        Me.ZctSOTLabel2.Name = "ZctSOTLabel2"
        Me.ZctSOTLabel2.Size = New System.Drawing.Size(89, 28)
        Me.ZctSOTLabel2.TabIndex = 6
        Me.ZctSOTLabel2.Text = "Monto minimo  " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "existente en caja"
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ZctSOTLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(460, 15)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(56, 28)
        Me.ZctSOTLabel1.TabIndex = 3
        Me.ZctSOTLabel1.Text = "Valor en " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "decimales"
        '
        'ZctCatPar
        '
        Me.AcceptButton = Me.cmdGrabar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancelar
        Me.ClientSize = New System.Drawing.Size(584, 208)
        Me.Controls.Add(Me.cmdGrabar)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "ZctCatPar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Parametros Generales"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ZctCatParBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ZctCatParBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Iva_CatParTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdCancelar As System.Windows.Forms.Button
    Friend WithEvents cmdGrabar As System.Windows.Forms.Button
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSOTLabel3 As ZctSOT.ZctSOTLabel
    Friend WithEvents TxtMaximoCaja As System.Windows.Forms.TextBox
    Friend WithEvents ZctSOTLabel2 As ZctSOT.ZctSOTLabel
    Friend WithEvents TxtMinimoCaja As System.Windows.Forms.TextBox
    Friend WithEvents ZctSOTLabel4 As ZctSOT.ZctSOTLabel
    Friend WithEvents TxtRIA As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
