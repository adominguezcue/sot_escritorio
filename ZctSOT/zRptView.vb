Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms
Imports System.Configuration
Imports System.Data.SqlClient



Public Class zRptView
    Private loginfo As CrystalDecisions.Shared.ConnectionInfo
    Public rutaRpt As String
    Private lista As New ParameterFields
    Private crTableLogonInfo As New TableLogOnInfo
    Public crTableLogonInfos As New TableLogOnInfos
    Public Sub conectar()

        Dim ConexionBuilder As New SqlConnectionStringBuilder(Datos.DAO.Conexion.CadenaConexion())

        loginfo = New CrystalDecisions.Shared.ConnectionInfo
        loginfo.ServerName = ConexionBuilder.DataSource
        loginfo.DatabaseName = ConexionBuilder.InitialCatalog  'ConfigurationManager.AppSettings.Get("Database")
        If ConexionBuilder.IntegratedSecurity Then
            loginfo.IntegratedSecurity = True
        Else
            loginfo.UserID = ConexionBuilder.UserID 'ConfigurationManager.AppSettings.Get("User")
            loginfo.Password = ConexionBuilder.Password 'ConfigurationManager.AppSettings.Get("Password")
        End If
        
        crTableLogonInfo.ConnectionInfo = loginfo

        'loginfo = New CrystalDecisions.Shared.ConnectionInfo
        'loginfo.ServerName = Datos.DAO.Conexion.Server ' ConfigurationManager.AppSettings.Get("Server")
        'loginfo.DatabaseName = Datos.DAO.Conexion.Database  'ConfigurationManager.AppSettings.Get("Database")
        'loginfo.UserID = Datos.DAO.Conexion.User 'ConfigurationManager.AppSettings.Get("User")
        'loginfo.Password = "L0k0m0t0r4" 'ConfigurationManager.AppSettings.Get("Password")
        'crTableLogonInfo.ConnectionInfo = loginfo
        crTableLogonInfos.Add(crTableLogonInfo)

    End Sub

    Public Function fGetPar(ByVal ParamArray sParMatriz() As String) As ParameterFields
        Dim iRowC As Long
        Dim sPar1, sPar2 As String
        Dim iLim As Integer
        Dim parametros As New ParameterFields

        For iRowC = 0 To sParMatriz.Length - 1

            iLim = InStr(sParMatriz(iRowC), ";")

            If iLim > 0 Then

                sPar1 = Mid(sParMatriz(iRowC), 1, iLim - 1)

                sPar2 = Mid(sParMatriz(iRowC), iLim + 1, Len(sParMatriz(iRowC)) - iLim)

                Dim parametro As New ParameterField

                Dim dVal As New ParameterDiscreteValue

                parametro.ParameterFieldName = sPar1

                dVal.Value = sPar2

                parametro.CurrentValues.Add(dVal)
                'Debug.Print(parametro.EditMask)


                parametros.Add(parametro)

            End If

        Next

        Return (parametros)
    End Function


    Private Sub logonrpt(ByRef reporte As ReportDocument)
        Try




            ' Dim crConnectionInfo As New ConnectionInfo
            Dim CrTables As Tables
            Dim CrTable As Table
            reporte.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
            ' crConnectionInfo = loginfo
            CrTables = reporte.Database.Tables
            For Each subRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument In reporte.Subreports
                subRpt.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
            Next


            For Each CrTable In CrTables
                'crTableLogonInfo = CrTable.LogOnInfo
                'crTableLogonInfo.ConnectionInfo = crConnectionInfo

                CrTable.ApplyLogOnInfo(crTableLogonInfo)
            Next

            reporte.VerifyDatabase()
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try
    End Sub


    Public Overloads Function printrpt(ByVal nombrereporte As String, ByVal sSQLF As String) As ReportDocument
        Try


            Dim rpt As New ReportDocument


            If rutaRpt.Trim.Length = 0 Then
                rpt.Load(nombrereporte, OpenReportMethod.OpenReportByDefault)
            ElseIf Mid(rutaRpt.Trim, rutaRpt.Trim.Length, 1) = "\" Then
                rpt.Load(rutaRpt & nombrereporte, OpenReportMethod.OpenReportByDefault)
            Else
                rpt.Load(rutaRpt & "\" & nombrereporte, OpenReportMethod.OpenReportByDefault)
            End If

            logonrpt(rpt)
            If sSQLF <> "" Then
                rpt.RecordSelectionFormula = sSQLF
            End If

            Return rpt
        Catch ex As Exception
            Debug.Print(ex.Message.ToString)
            Return Nothing

        End Try

    End Function

    Private Sub ingresa_valores(ByVal valor As String, ByVal nombre_parametro As String)
        Dim Valores As New ParameterDiscreteValue
        Dim DatosSP As New ParameterField
        Valores.Value = valor
        DatosSP.Name = nombre_parametro
        DatosSP.CurrentValues.Add(Valores)
        lista.Add(DatosSP)
    End Sub

End Class



