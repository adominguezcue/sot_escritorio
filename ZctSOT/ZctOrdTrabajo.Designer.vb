<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctOrdTrabajo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ZctGroupControls1 = New ZctSOT.ZctGroupControls()
        Me.lblEstatus = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctGroupControls3 = New ZctSOT.ZctGroupControls()
        Me.lblDevolucion = New ZctSOT.ZctSOTLabelDesc()
        Me.lblSurtido = New ZctSOT.ZctSOTLabelDesc()
        Me.lblPorSurtir = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel3 = New ZctSOT.ZctSOTLabel()
        Me.lblSubTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel2 = New ZctSOT.ZctSOTLabel()
        Me.lblIva = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel()
        Me.lblTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.DGridArticulos = New ZctSOT.ZctSotGrid()
        Me.ColArt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDesc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAlmacen = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ColExist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSurt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Motocicleta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Usuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox()
        Me.cmdPagar = New ZctSOT.ZctSOTButton()
        Me.cmdCancelaOrden = New ZctSOT.ZctSOTButton()
        Me.cmdImprimir = New ZctSOT.ZctSOTButton()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.ZctGroupControls2 = New ZctSOT.ZctGroupControls()
        Me.txtTrabajo = New ZctSOT.ZctControlTexto()
        Me.txtCliente = New ZctSOT.ZctControlBusqueda()
        Me.ZCTFolio = New ZctSOT.ZctGroupControls()
        Me.txtFolio = New ZctSOT.ZctControlTexto()
        Me.cmdDer = New ZctSOT.ZctSOTButton()
        Me.cmdIzq = New ZctSOT.ZctSOTButton()
        Me.ZctSOTToolTip1 = New ZctSOT.ZctSOTToolTip()
        Me.ZctGroupControls1.SuspendLayout
        Me.ZctGroupControls3.SuspendLayout
        CType(Me.DGridArticulos,System.ComponentModel.ISupportInitialize).BeginInit
        Me.ZctSOTGroupBox1.SuspendLayout
        Me.ZctGroupControls2.SuspendLayout
        Me.ZCTFolio.SuspendLayout
        Me.SuspendLayout
        '
        'ZctGroupControls1
        '
        Me.ZctGroupControls1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls1.Controls.Add(Me.lblEstatus)
        Me.ZctGroupControls1.Location = New System.Drawing.Point(695, 3)
        Me.ZctGroupControls1.Name = "ZctGroupControls1"
        Me.ZctGroupControls1.Size = New System.Drawing.Size(207, 53)
        Me.ZctGroupControls1.TabIndex = 7
        Me.ZctGroupControls1.TabStop = false
        '
        'lblEstatus
        '
        Me.lblEstatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lblEstatus.BackColor = System.Drawing.Color.LightGray
        Me.lblEstatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblEstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblEstatus.Location = New System.Drawing.Point(16, 16)
        Me.lblEstatus.Name = "lblEstatus"
        Me.lblEstatus.Size = New System.Drawing.Size(182, 26)
        Me.lblEstatus.TabIndex = 10
        Me.lblEstatus.Text = "Sin Guardar"
        Me.lblEstatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ZctGroupControls3
        '
        Me.ZctGroupControls3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls3.Controls.Add(Me.lblDevolucion)
        Me.ZctGroupControls3.Controls.Add(Me.lblSurtido)
        Me.ZctGroupControls3.Controls.Add(Me.lblPorSurtir)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel3)
        Me.ZctGroupControls3.Controls.Add(Me.lblSubTotal)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel2)
        Me.ZctGroupControls3.Controls.Add(Me.lblIva)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel1)
        Me.ZctGroupControls3.Controls.Add(Me.lblTotal)
        Me.ZctGroupControls3.Controls.Add(Me.DGridArticulos)
        Me.ZctGroupControls3.Location = New System.Drawing.Point(0, 166)
        Me.ZctGroupControls3.Name = "ZctGroupControls3"
        Me.ZctGroupControls3.Size = New System.Drawing.Size(902, 308)
        Me.ZctGroupControls3.TabIndex = 4
        Me.ZctGroupControls3.TabStop = false
        '
        'lblDevolucion
        '
        Me.lblDevolucion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lblDevolucion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255,Byte),Integer), CType(CType(128,Byte),Integer), CType(CType(128,Byte),Integer))
        Me.lblDevolucion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDevolucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblDevolucion.Location = New System.Drawing.Point(183, 285)
        Me.lblDevolucion.Name = "lblDevolucion"
        Me.lblDevolucion.Size = New System.Drawing.Size(81, 15)
        Me.lblDevolucion.TabIndex = 9
        Me.lblDevolucion.Text = "Devolución"
        Me.lblDevolucion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSurtido
        '
        Me.lblSurtido.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lblSurtido.BackColor = System.Drawing.Color.FromArgb(CType(CType(128,Byte),Integer), CType(CType(255,Byte),Integer), CType(CType(128,Byte),Integer))
        Me.lblSurtido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSurtido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblSurtido.Location = New System.Drawing.Point(96, 285)
        Me.lblSurtido.Name = "lblSurtido"
        Me.lblSurtido.Size = New System.Drawing.Size(81, 15)
        Me.lblSurtido.TabIndex = 8
        Me.lblSurtido.Text = "Surtido"
        Me.lblSurtido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPorSurtir
        '
        Me.lblPorSurtir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lblPorSurtir.BackColor = System.Drawing.Color.FromArgb(CType(CType(255,Byte),Integer), CType(CType(255,Byte),Integer), CType(CType(128,Byte),Integer))
        Me.lblPorSurtir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPorSurtir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblPorSurtir.Location = New System.Drawing.Point(9, 285)
        Me.lblPorSurtir.Name = "lblPorSurtir"
        Me.lblPorSurtir.Size = New System.Drawing.Size(81, 15)
        Me.lblPorSurtir.TabIndex = 7
        Me.lblPorSurtir.Text = "Por Surtir"
        Me.lblPorSurtir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ZctSOTLabel3
        '
        Me.ZctSOTLabel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ZctSOTLabel3.AutoSize = true
        Me.ZctSOTLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ZctSOTLabel3.Location = New System.Drawing.Point(403, 285)
        Me.ZctSOTLabel3.Name = "ZctSOTLabel3"
        Me.ZctSOTLabel3.Size = New System.Drawing.Size(62, 13)
        Me.ZctSOTLabel3.TabIndex = 1
        Me.ZctSOTLabel3.Text = "SubTotal:"
        '
        'lblSubTotal
        '
        Me.lblSubTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lblSubTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblSubTotal.Location = New System.Drawing.Point(471, 285)
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblSubTotal.TabIndex = 2
        Me.lblSubTotal.Text = "$0"
        Me.lblSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ZctSOTLabel2
        '
        Me.ZctSOTLabel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ZctSOTLabel2.AutoSize = true
        Me.ZctSOTLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ZctSOTLabel2.Location = New System.Drawing.Point(581, 285)
        Me.ZctSOTLabel2.Name = "ZctSOTLabel2"
        Me.ZctSOTLabel2.Size = New System.Drawing.Size(31, 13)
        Me.ZctSOTLabel2.TabIndex = 3
        Me.ZctSOTLabel2.Text = "IVA:"
        '
        'lblIva
        '
        Me.lblIva.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lblIva.BackColor = System.Drawing.Color.LightBlue
        Me.lblIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblIva.Location = New System.Drawing.Point(618, 285)
        Me.lblIva.Name = "lblIva"
        Me.lblIva.Size = New System.Drawing.Size(104, 15)
        Me.lblIva.TabIndex = 4
        Me.lblIva.Text = "$0"
        Me.lblIva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ZctSOTLabel1.AutoSize = true
        Me.ZctSOTLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(726, 285)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(40, 13)
        Me.ZctSOTLabel1.TabIndex = 5
        Me.ZctSOTLabel1.Text = "Total:"
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lblTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblTotal.Location = New System.Drawing.Point(772, 285)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "$0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DGridArticulos
        '
        Me.DGridArticulos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.DGridArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGridArticulos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColArt, Me.ColDesc, Me.ColAlmacen, Me.ColExist, Me.ColSol, Me.ColSurt, Me.ColPrecio, Me.ColCosto, Me.SubTotal, Me.ColCod, Me.Tipo, Me.Motocicleta, Me.Usuario})
        Me.DGridArticulos.Location = New System.Drawing.Point(9, 16)
        Me.DGridArticulos.Name = "DGridArticulos"
        Me.DGridArticulos.Size = New System.Drawing.Size(884, 254)
        Me.DGridArticulos.TabIndex = 0
        Me.DGridArticulos.Tag = "En esta cuadricula se dan de alta los artículos a surtir, es indispensable que in"& _ 
    "serta la candidad a surtir para agregar más artículos"
        Me.ZctSOTToolTip1.SetToolTip(Me.DGridArticulos, "En esta cuadricula se dan de alta los artículos a surtir, es indispensable que in"& _ 
        "serta la candidad a surtir para agregar más artículos")
        '
        'ColArt
        '
        Me.ColArt.HeaderText = "Artículo"
        Me.ColArt.Name = "ColArt"
        Me.ColArt.Width = 120
        '
        'ColDesc
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue
        Me.ColDesc.DefaultCellStyle = DataGridViewCellStyle1
        Me.ColDesc.HeaderText = "Descripción"
        Me.ColDesc.Name = "ColDesc"
        Me.ColDesc.ReadOnly = true
        Me.ColDesc.Width = 200
        '
        'ColAlmacen
        '
        Me.ColAlmacen.HeaderText = "Almacén"
        Me.ColAlmacen.Name = "ColAlmacen"
        '
        'ColExist
        '
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.ColExist.DefaultCellStyle = DataGridViewCellStyle2
        Me.ColExist.HeaderText = "Existencias"
        Me.ColExist.Name = "ColExist"
        Me.ColExist.ReadOnly = true
        Me.ColExist.Width = 80
        '
        'ColSol
        '
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = "0"
        Me.ColSol.DefaultCellStyle = DataGridViewCellStyle3
        Me.ColSol.HeaderText = "Solicitada"
        Me.ColSol.Name = "ColSol"
        Me.ColSol.Visible = false
        Me.ColSol.Width = 80
        '
        'ColSurt
        '
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = "0"
        Me.ColSurt.DefaultCellStyle = DataGridViewCellStyle4
        Me.ColSurt.HeaderText = "Surtido"
        Me.ColSurt.Name = "ColSurt"
        Me.ColSurt.Width = 80
        '
        'ColPrecio
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = "0"
        Me.ColPrecio.DefaultCellStyle = DataGridViewCellStyle5
        Me.ColPrecio.HeaderText = "Precio"
        Me.ColPrecio.Name = "ColPrecio"
        Me.ColPrecio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ColPrecio.Width = 80
        '
        'ColCosto
        '
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle6.Format = "C2"
        DataGridViewCellStyle6.NullValue = "0"
        Me.ColCosto.DefaultCellStyle = DataGridViewCellStyle6
        Me.ColCosto.HeaderText = "Costo"
        Me.ColCosto.Name = "ColCosto"
        Me.ColCosto.ReadOnly = true
        Me.ColCosto.Visible = false
        Me.ColCosto.Width = 70
        '
        'SubTotal
        '
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle7.Format = "C2"
        DataGridViewCellStyle7.NullValue = "0"
        Me.SubTotal.DefaultCellStyle = DataGridViewCellStyle7
        Me.SubTotal.HeaderText = "Sub Total"
        Me.SubTotal.Name = "SubTotal"
        Me.SubTotal.ReadOnly = true
        Me.SubTotal.Width = 80
        '
        'ColCod
        '
        Me.ColCod.HeaderText = "Codigo"
        Me.ColCod.Name = "ColCod"
        Me.ColCod.Visible = false
        '
        'Tipo
        '
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.Visible = false
        '
        'Motocicleta
        '
        Me.Motocicleta.HeaderText = "Motocicleta"
        Me.Motocicleta.Name = "Motocicleta"
        Me.Motocicleta.Visible = false
        '
        'Usuario
        '
        Me.Usuario.HeaderText = "Usuario"
        Me.Usuario.Name = "Usuario"
        Me.Usuario.Visible = false
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdPagar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelaOrden)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdImprimir)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdAceptar)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(3, 480)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(899, 77)
        Me.ZctSOTGroupBox1.TabIndex = 8
        Me.ZctSOTGroupBox1.TabStop = false
        '
        'cmdPagar
        '
        Me.cmdPagar.Location = New System.Drawing.Point(403, 17)
        Me.cmdPagar.Name = "cmdPagar"
        Me.cmdPagar.Size = New System.Drawing.Size(112, 54)
        Me.cmdPagar.TabIndex = 4
        Me.cmdPagar.Text = "Pagar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdPagar, "Imprime la orden de trabajo")
        Me.cmdPagar.UseVisualStyleBackColor = true
        '
        'cmdCancelaOrden
        '
        Me.cmdCancelaOrden.Location = New System.Drawing.Point(640, 17)
        Me.cmdCancelaOrden.Name = "cmdCancelaOrden"
        Me.cmdCancelaOrden.Size = New System.Drawing.Size(112, 54)
        Me.cmdCancelaOrden.TabIndex = 3
        Me.cmdCancelaOrden.Tag = "Cancela la Orden y regresa los artículos que fuerón surtidos"
        Me.cmdCancelaOrden.Text = "Cancelar orden"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelaOrden, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant"& _ 
        "alla")
        Me.cmdCancelaOrden.UseVisualStyleBackColor = true
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Location = New System.Drawing.Point(9, 17)
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Size = New System.Drawing.Size(112, 54)
        Me.cmdImprimir.TabIndex = 0
        Me.cmdImprimir.Text = "Imprimir"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdImprimir, "Imprime la orden de trabajo")
        Me.cmdImprimir.UseVisualStyleBackColor = true
        Me.cmdImprimir.Visible = false
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(758, 17)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(112, 54)
        Me.cmdCancelar.TabIndex = 2
        Me.cmdCancelar.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant"& _ 
    "alla"
        Me.cmdCancelar.Text = "Cancelar edición"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelar, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant"& _ 
        "alla")
        Me.cmdCancelar.UseVisualStyleBackColor = true
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Location = New System.Drawing.Point(524, 17)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(112, 54)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Text = "Aceptar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdAceptar, "Graba las modificaciones realizadas, si el código es nuevo da de alta la orden de"& _ 
        " trabajo")
        Me.cmdAceptar.UseVisualStyleBackColor = true
        '
        'ZctGroupControls2
        '
        Me.ZctGroupControls2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls2.Controls.Add(Me.txtTrabajo)
        Me.ZctGroupControls2.Controls.Add(Me.txtCliente)
        Me.ZctGroupControls2.Location = New System.Drawing.Point(3, 62)
        Me.ZctGroupControls2.Name = "ZctGroupControls2"
        Me.ZctGroupControls2.Size = New System.Drawing.Size(899, 98)
        Me.ZctGroupControls2.TabIndex = 3
        Me.ZctGroupControls2.TabStop = false
        '
        'txtTrabajo
        '
        Me.txtTrabajo.Location = New System.Drawing.Point(6, 16)
        Me.txtTrabajo.Multiline = true
        Me.txtTrabajo.Name = "txtTrabajo"
        Me.txtTrabajo.Nombre = "Trabajo a realizar:"
        Me.txtTrabajo.Size = New System.Drawing.Size(405, 77)
        Me.txtTrabajo.TabIndex = 9
        Me.txtTrabajo.Tag = "Descripción del trabajo a realizar"
        Me.txtTrabajo.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtTrabajo.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtTrabajo, "Descripción del trabajo a realizar")
        '
        'txtCliente
        '
        Me.txtCliente.Descripcion = "ZctSOTLabelDesc1"
        Me.txtCliente.Location = New System.Drawing.Point(417, 19)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Nombre = "Cliente:"
        Me.txtCliente.Size = New System.Drawing.Size(476, 28)
        Me.txtCliente.SPName = Nothing
        Me.txtCliente.SPParametros = Nothing
        Me.txtCliente.SpVariables = Nothing
        Me.txtCliente.SqlBusqueda = Nothing
        Me.txtCliente.TabIndex = 0
        Me.txtCliente.Tabla = Nothing
        Me.txtCliente.Tag = "Cliente al cual pertenece la Motocicleta, para buscar presione [F3]"
        Me.txtCliente.TextWith = 111
        Me.txtCliente.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCliente.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtCliente, "Cliente al cual pertenece la Motocicleta, para buscar presione [F3]")
        Me.txtCliente.Validar = true
        '
        'ZCTFolio
        '
        Me.ZCTFolio.Controls.Add(Me.txtFolio)
        Me.ZCTFolio.Controls.Add(Me.cmdDer)
        Me.ZCTFolio.Controls.Add(Me.cmdIzq)
        Me.ZCTFolio.Location = New System.Drawing.Point(3, 3)
        Me.ZCTFolio.Name = "ZCTFolio"
        Me.ZCTFolio.Size = New System.Drawing.Size(221, 53)
        Me.ZCTFolio.TabIndex = 0
        Me.ZCTFolio.TabStop = false
        '
        'txtFolio
        '
        Me.txtFolio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.txtFolio.Location = New System.Drawing.Point(9, 18)
        Me.txtFolio.Multiline = false
        Me.txtFolio.Name = "txtFolio"
        Me.txtFolio.Nombre = "Folio:"
        Me.txtFolio.Size = New System.Drawing.Size(165, 28)
        Me.txtFolio.TabIndex = 0
        Me.txtFolio.Tag = "Folio de la orden de trabajo, si la orden es nueva, deje el código por default"
        Me.txtFolio.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtFolio.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtFolio, "Folio de la orden de trabajo, si la orden es nueva, deje el código por default")
        '
        'cmdDer
        '
        Me.cmdDer.Location = New System.Drawing.Point(195, 18)
        Me.cmdDer.Name = "cmdDer"
        Me.cmdDer.Size = New System.Drawing.Size(19, 23)
        Me.cmdDer.TabIndex = 6
        Me.cmdDer.Text = ">"
        Me.cmdDer.UseVisualStyleBackColor = true
        '
        'cmdIzq
        '
        Me.cmdIzq.Location = New System.Drawing.Point(174, 18)
        Me.cmdIzq.Name = "cmdIzq"
        Me.cmdIzq.Size = New System.Drawing.Size(19, 23)
        Me.cmdIzq.TabIndex = 5
        Me.cmdIzq.Text = "<"
        Me.cmdIzq.UseVisualStyleBackColor = true
        '
        'ZctOrdTrabajo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(906, 567)
        Me.Controls.Add(Me.ZctGroupControls1)
        Me.Controls.Add(Me.ZctGroupControls3)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.ZctGroupControls2)
        Me.Controls.Add(Me.ZCTFolio)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "ZctOrdTrabajo"
        Me.Tag = "Pedidos"
        Me.Text = "Órdenes de trabajo"
        Me.ZctGroupControls1.ResumeLayout(false)
        Me.ZctGroupControls3.ResumeLayout(false)
        Me.ZctGroupControls3.PerformLayout
        CType(Me.DGridArticulos,System.ComponentModel.ISupportInitialize).EndInit
        Me.ZctSOTGroupBox1.ResumeLayout(false)
        Me.ZctGroupControls2.ResumeLayout(false)
        Me.ZCTFolio.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub
    Friend WithEvents ZCTFolio As ZctSOT.ZctGroupControls
    Friend WithEvents ZctGroupControls2 As ZctSOT.ZctGroupControls
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents txtCliente As ZctSOT.ZctControlBusqueda
    Friend WithEvents txtFolio As ZctSOT.ZctControlTexto
    Friend WithEvents ZctGroupControls3 As ZctSOT.ZctGroupControls
    Friend WithEvents DGridArticulos As ZctSOT.ZctSotGrid
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTLabel3 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblSubTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTLabel2 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblIva As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblDevolucion As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblSurtido As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblPorSurtir As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents cmdImprimir As ZctSOT.ZctSOTButton
    Friend WithEvents ZctSOTToolTip1 As ZctSOT.ZctSOTToolTip
    Friend WithEvents cmdDer As ZctSOT.ZctSOTButton
    Friend WithEvents cmdIzq As ZctSOT.ZctSOTButton
    Friend WithEvents cmdCancelaOrden As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents ZctGroupControls1 As ZctSOT.ZctGroupControls
    Friend WithEvents lblEstatus As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents cmdPagar As ZctSOT.ZctSOTButton
    Friend WithEvents txtTrabajo As ZctSOT.ZctControlTexto
    Friend WithEvents ColArt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAlmacen As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ColExist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSurt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Motocicleta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Usuario As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
