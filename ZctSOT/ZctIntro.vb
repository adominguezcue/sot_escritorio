Imports ZctSOT.Datos

Public NotInheritable Class ZctIntro
    Dim iCont As Integer = 0

    '  del Diseñador de proyectos ("Propiedades" bajo el menú "Proyecto").


    Private Sub ZctIntro_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Configure el texto del cuadro de diálogo en tiempo de ejecución según la información del ensamblado de la aplicación.  



        'Título de la aplicación
        If My.Application.Info.Title <> "" Then
            ApplicationTitle.Text = My.Application.Info.Title
        Else
            'Si falta el título de la aplicación, utilice el nombre de la aplicación sin la extensión
            ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        End If

        'Dé formato a la información de versión usando el texto establecido en el control de versión en tiempo de diseño como
        '  cadena de formato. Esto le permite una localización efectiva si lo desea.
        '  Se pudo incluir la información de generación y revisión usando el siguiente código y cambiando el 
        '  texto en tiempo de diseño del control de versión a "Versión {0}.{1:00}.{2}.{3}" o algo parecido. Consulte
        '  String.Format() en la Ayuda para obtener más información.
        '
        Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Revision)

        'Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        'Información de Copyright
        Copyright.Text = My.Application.Info.Copyright
        'Seguridad()

    End Sub

    Private Sub MainLayoutPanel_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles MainLayoutPanel.Paint

    End Sub

    Private Sub Seguridad()
        'verifica que exista el usuario
        Dim sVariables As String = "@Dato1;VARCHAR|@Dato2;VARCHAR"
        'Dim sDatos As New Datos.ClassGen
        Dim gMac As New ZctDatos
        Dim DtUsu As DataTable
        Try
            Dim sParametros As String = gMac.GetMac() & "|0"

            Application.DoEvents()
            DtUsu = Datos.ClassGen.GenGetData(1, "SP_ZctDato", sVariables, sParametros)
            Application.DoEvents()
            If DtUsu.Rows.Count <= 0 Then
                'MsgBox("No se encuentra el servidor, verifique su configuración.", MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            MsgBox("No se encuentra el servidor, verifique su configuración.", MsgBoxStyle.Critical)
        End Try
        Exit Sub
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        iCont = iCont + 1
        If iCont > 10 Then Me.Dispose() : Exit Sub
        ProgressBar1.Value = iCont
    End Sub
End Class
