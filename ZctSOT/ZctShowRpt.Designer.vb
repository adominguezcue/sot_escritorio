<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctShowRpt
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.CmdEnviar = New System.Windows.Forms.Button()
        Me.txtDescRpt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtIdRpt = New System.Windows.Forms.TextBox()
        Me.PkCatServBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblCategoria = New System.Windows.Forms.Label()
        Me.lblDescCategoria = New System.Windows.Forms.TextBox()
        Me.DGrdFiltros = New System.Windows.Forms.DataGridView()
        Me.DGColIdRptFil = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGDescFil = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DgColValor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGColidCatTp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGColSQL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGColShow = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSpName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Alfa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OmitirParametros = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.gboxFiltros = New System.Windows.Forms.GroupBox()
        Me.CboReportes = New ZctSOT.ZctControlCombo()
        Me.ZctSOTToolTip1 = New ZctSOT.ZctSOTToolTip()
        CType(Me.PkCatServBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGrdFiltros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gboxFiltros.SuspendLayout()
        Me.SuspendLayout()
        '
        'CmdEnviar
        '
        Me.CmdEnviar.Location = New System.Drawing.Point(624, 442)
        Me.CmdEnviar.Name = "CmdEnviar"
        Me.CmdEnviar.Size = New System.Drawing.Size(75, 23)
        Me.CmdEnviar.TabIndex = 7
        Me.CmdEnviar.Tag = "Muestra el reporte"
        Me.CmdEnviar.Text = "Enviar"
        Me.ZctSOTToolTip1.SetToolTip(Me.CmdEnviar, "Muestra el reporte")
        Me.CmdEnviar.UseVisualStyleBackColor = True
        '
        'txtDescRpt
        '
        Me.txtDescRpt.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtDescRpt.Enabled = False
        Me.txtDescRpt.Location = New System.Drawing.Point(122, 70)
        Me.txtDescRpt.Multiline = True
        Me.txtDescRpt.Name = "txtDescRpt"
        Me.txtDescRpt.Size = New System.Drawing.Size(577, 44)
        Me.txtDescRpt.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(50, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Descripción:"
        '
        'txtIdRpt
        '
        Me.txtIdRpt.Enabled = False
        Me.txtIdRpt.Location = New System.Drawing.Point(281, 80)
        Me.txtIdRpt.Name = "txtIdRpt"
        Me.txtIdRpt.Size = New System.Drawing.Size(100, 20)
        Me.txtIdRpt.TabIndex = 5
        Me.txtIdRpt.TabStop = False
        '
        'PkCatServBindingSource
        '
        Me.PkCatServBindingSource.DataMember = "PkCatServ"
        '
        'lblCategoria
        '
        Me.lblCategoria.AutoSize = True
        Me.lblCategoria.Location = New System.Drawing.Point(61, 12)
        Me.lblCategoria.Name = "lblCategoria"
        Me.lblCategoria.Size = New System.Drawing.Size(55, 13)
        Me.lblCategoria.TabIndex = 0
        Me.lblCategoria.Text = "Categoria:"
        '
        'lblDescCategoria
        '
        Me.lblDescCategoria.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblDescCategoria.Enabled = False
        Me.lblDescCategoria.Location = New System.Drawing.Point(122, 9)
        Me.lblDescCategoria.Name = "lblDescCategoria"
        Me.lblDescCategoria.Size = New System.Drawing.Size(577, 20)
        Me.lblDescCategoria.TabIndex = 1
        '
        'DGrdFiltros
        '
        Me.DGrdFiltros.AllowUserToAddRows = False
        Me.DGrdFiltros.AllowUserToDeleteRows = False
        Me.DGrdFiltros.AllowUserToResizeRows = False
        Me.DGrdFiltros.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.DGrdFiltros.CausesValidation = False
        Me.DGrdFiltros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGrdFiltros.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DGColIdRptFil, Me.DGDescFil, Me.DgColValor, Me.DGColidCatTp, Me.DGColSQL, Me.DGColShow, Me.ColSpName, Me.Alfa, Me.OmitirParametros})
        Me.DGrdFiltros.Location = New System.Drawing.Point(6, 19)
        Me.DGrdFiltros.Name = "DGrdFiltros"
        Me.DGrdFiltros.Size = New System.Drawing.Size(675, 187)
        Me.DGrdFiltros.TabIndex = 0
        Me.DGrdFiltros.Tag = "Filtros para el reporte"
        Me.ZctSOTToolTip1.SetToolTip(Me.DGrdFiltros, "Filtros para el reporte")
        '
        'DGColIdRptFil
        '
        Me.DGColIdRptFil.HeaderText = "Indice"
        Me.DGColIdRptFil.Name = "DGColIdRptFil"
        Me.DGColIdRptFil.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DGColIdRptFil.Visible = False
        Me.DGColIdRptFil.Width = 42
        '
        'DGDescFil
        '
        Me.DGDescFil.HeaderText = "Filtro"
        Me.DGDescFil.Name = "DGDescFil"
        Me.DGDescFil.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DGDescFil.Width = 35
        '
        'DgColValor
        '
        Me.DgColValor.HeaderText = "Valor"
        Me.DgColValor.Name = "DgColValor"
        Me.DgColValor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DgColValor.Width = 37
        '
        'DGColidCatTp
        '
        Me.DGColidCatTp.HeaderText = "DGColidCatTp"
        Me.DGColidCatTp.Name = "DGColidCatTp"
        Me.DGColidCatTp.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DGColidCatTp.Visible = False
        Me.DGColidCatTp.Width = 81
        '
        'DGColSQL
        '
        Me.DGColSQL.HeaderText = "Sql"
        Me.DGColSQL.Name = "DGColSQL"
        Me.DGColSQL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DGColSQL.Visible = False
        Me.DGColSQL.Width = 28
        '
        'DGColShow
        '
        Me.DGColShow.HeaderText = "DGColShow"
        Me.DGColShow.Name = "DGColShow"
        Me.DGColShow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DGColShow.Visible = False
        Me.DGColShow.Width = 71
        '
        'ColSpName
        '
        Me.ColSpName.HeaderText = "SpName"
        Me.ColSpName.Name = "ColSpName"
        Me.ColSpName.Visible = False
        Me.ColSpName.Width = 73
        '
        'Alfa
        '
        Me.Alfa.HeaderText = "Alfa"
        Me.Alfa.Name = "Alfa"
        Me.Alfa.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Alfa.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Alfa.Visible = False
        Me.Alfa.Width = 31
        '
        'OmitirParametros
        '
        Me.OmitirParametros.HeaderText = "OmitirParametros"
        Me.OmitirParametros.Name = "OmitirParametros"
        Me.OmitirParametros.ReadOnly = True
        Me.OmitirParametros.Visible = False
        Me.OmitirParametros.Width = 92
        '
        'gboxFiltros
        '
        Me.gboxFiltros.Controls.Add(Me.DGrdFiltros)
        Me.gboxFiltros.Location = New System.Drawing.Point(17, 119)
        Me.gboxFiltros.Name = "gboxFiltros"
        Me.gboxFiltros.Size = New System.Drawing.Size(687, 213)
        Me.gboxFiltros.TabIndex = 6
        Me.gboxFiltros.TabStop = False
        Me.gboxFiltros.Text = "Filtros"
        '
        'CboReportes
        '
        Me.CboReportes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboReportes.Location = New System.Drawing.Point(66, 35)
        Me.CboReportes.Name = "CboReportes"
        Me.CboReportes.Nombre = "Reporte:"
        Me.CboReportes.Size = New System.Drawing.Size(638, 28)
        Me.CboReportes.TabIndex = 2
        Me.ZctSOTToolTip1.SetToolTip(Me.CboReportes, "Seleccione el reporte que desea consultar")
        Me.CboReportes.value = Nothing
        Me.CboReportes.ValueItem = 0
        Me.CboReportes.ValueItemStr = Nothing
        '
        'ZctShowRpt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(707, 480)
        Me.Controls.Add(Me.gboxFiltros)
        Me.Controls.Add(Me.CboReportes)
        Me.Controls.Add(Me.lblDescCategoria)
        Me.Controls.Add(Me.lblCategoria)
        Me.Controls.Add(Me.CmdEnviar)
        Me.Controls.Add(Me.txtDescRpt)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtIdRpt)
        Me.Name = "ZctShowRpt"
        Me.Text = "Visor de Reportes"
        CType(Me.PkCatServBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGrdFiltros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gboxFiltros.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CmdEnviar As System.Windows.Forms.Button
    Friend WithEvents txtDescRpt As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtIdRpt As System.Windows.Forms.TextBox
    'Friend WithEvents Reportes As SisPinkyProd.Reportes
    Friend WithEvents PkCatServBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents lblCategoria As System.Windows.Forms.Label
    Friend WithEvents lblDescCategoria As System.Windows.Forms.TextBox
    Friend WithEvents CboReportes As ZctSOT.ZctControlCombo
    Friend WithEvents DGrdFiltros As System.Windows.Forms.DataGridView
    Friend WithEvents gboxFiltros As System.Windows.Forms.GroupBox
    Friend WithEvents ZctSOTToolTip1 As ZctSOT.ZctSOTToolTip
    Friend WithEvents DGColIdRptFil As DataGridViewTextBoxColumn
    Friend WithEvents DGDescFil As DataGridViewTextBoxColumn
    Friend WithEvents DgColValor As DataGridViewTextBoxColumn
    Friend WithEvents DGColidCatTp As DataGridViewTextBoxColumn
    Friend WithEvents DGColSQL As DataGridViewTextBoxColumn
    Friend WithEvents DGColShow As DataGridViewTextBoxColumn
    Friend WithEvents ColSpName As DataGridViewTextBoxColumn
    Friend WithEvents Alfa As DataGridViewTextBoxColumn
    Friend WithEvents OmitirParametros As DataGridViewCheckBoxColumn
    'Friend WithEvents PkCatServTableAdapter As SisPinkyProd.ReportesTableAdapters.PkCatServTableAdapter
End Class
