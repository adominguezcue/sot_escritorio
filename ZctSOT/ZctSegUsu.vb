﻿Imports ZctSOT.Datos

Public Class ZctSegUsu
    Private _Usu As New Clases.Seguridad.ZctSegUsu
    Private ProcesaCatalogos As New Datos.DAO.ClProcesaCatalogos

    Private sSpVariables As String = "@Cod_Usu;INT|@Nom_Usu;VARCHAR"
    Private sSPName As String = "SP_ZctSegUsu_Busqueda"
    Private sTabla As String = "ZctSegUsu"
    Private sSqlBusqueda As String = "SELECT Cod_Usu as Codigo, Nom_Usu as Usuario FROM ZctSegUsu"


    Private Sub ZctSegUsu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Habilita(False)

        txtCodUsu.ZctSOTLabelDesc1.Text = ""
        txtCodUsu.SqlBusqueda = sSqlBusqueda
        txtCodUsu.SPName = sSPName
        txtCodUsu.SpVariables = sSpVariables
        txtCodUsu.Tabla = sTabla

    End Sub

    Private Sub txtCodUsu_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodUsu.lostfocus
        Try
            'Usuario nuevo
            If txtCodUsu.Text.Trim = "" OrElse Not IsNumeric(txtCodUsu.Text) OrElse txtCodUsu.Text = "0" Then
                txtCodUsu.Text = 0
                _Usu = New Clases.Seguridad.ZctSegUsu
            Else
                'Usuario existente
                ProcesaCatalogos.CargaDatos(_Usu, New Object() {txtCodUsu.Text})
                If _Usu Is Nothing Then _Usu = New Clases.Seguridad.ZctSegUsu

            End If

            ZctSegUsuBindingSource.DataSource = _Usu
            ZctSegUsuBindingSource.ResetBindings(False)
            txtCodUsu.Text = _Usu.Cod_Usu.ToString

            If _Usu.Cod_Usu > 0 Then
                Habilita(True)
                PasswordTextBox.Focus()
            Else
                HabilitaNuevo()
                txtUsuario.Focus()
            End If

        Catch ex As Exception
            MsgBox("Ha ocurrido un error, favor de verificar con sistemas.")
        End Try

    End Sub


#Region "Funciones"
    Public Sub Habilita(ByVal bHabilita As Boolean)

        txtCodUsu.Enabled = Not bHabilita

        txtUsuario.Enabled = False
        PasswordTextBox.Enabled = bHabilita
        Password2TextBox.Enabled = bHabilita
        cmdEliminar.Enabled = bHabilita
        cmdAceptar.Enabled = bHabilita
        ChVendedor.Enabled = bHabilita
    End Sub

    Public Sub HabilitaNuevo()

        txtCodUsu.Enabled = True
        txtUsuario.Enabled = True
        PasswordTextBox.Enabled = True
        Password2TextBox.Enabled = True
        cmdEliminar.Enabled = False
        cmdAceptar.Enabled = True
        ChVendedor.Enabled = True
    End Sub

#End Region

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If txtCodUsu.Enabled Then
            Me.Close()
        Else
            Habilita(False)
            limpia()
            txtCodUsu.Focus()
        End If
    End Sub

    Public Sub limpia()
        _Usu = New Clases.Seguridad.ZctSegUsu
        txtCodUsu.Text = 0
        ZctSegUsuBindingSource.DataSource = _Usu
        ZctSegUsuBindingSource.ResetBindings(False)
        txtCodUsu.Descripcion = ""
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try
            ProcesaCatalogos.ActualizarDatos(_Usu)
            MsgBox("El usuario ha sido guardado correctamente.", MsgBoxStyle.Information, "Hecho")
            Habilita(False)
            limpia()
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al grabar, favor de comunicarse con sistemas.", MsgBoxStyle.Exclamation, "Atencion")
        End Try
    End Sub

    Private Sub cmdEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEliminar.Click
        Try
            If (MsgBox("¿Esta seguro que desea eliminar a este usuario?", MsgBoxStyle.YesNo) = MsgBoxResult.No) Then Exit Sub
            Dim aut As Clases.Seguridad.ListaAutorizacionesByUser = ProcesaCatalogos.DevuelveDatos(New Clases.Seguridad.ListaAutorizacionesByUser, New Object() {_Usu.Cod_Usu})
            If aut IsNot Nothing AndAlso aut.Count > 0 Then
                MsgBox("El usuario no puede ser eliminado ya que tiene autorizaciones asignadas.")
                Exit Sub
            End If
            ProcesaCatalogos.EliminaDatos(_Usu)
            MsgBox("El usuario ha sido eliminado correctamente.")
            Habilita(False)
            limpia()
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al eliminar, favor de comunicarse con sistemas.")
        End Try
    End Sub

    Private Sub txtCodUsu_Load(sender As System.Object, e As System.EventArgs) Handles txtCodUsu.Load

    End Sub
End Class