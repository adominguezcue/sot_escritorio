Imports System.Data.SqlDbType
Imports SOTControladores.Controladores

Public Class ZctMotos
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
    Private sSpVariables As String = "@Cod_Mot;INT|@Vin_Mot;VARCHAR|@Cod_Cte;INT|@Cod_Mar;INT;@CodMod_Mot;INT|@Placas_Mot;VARCHAR|@Motor_Mot;VARCHAR|@Anno_Mot;INT|@Cod_Ubicacion;VARCHAR|@disable_CatMot;BIT|@Cod_Rep;INT|@fecha_alta;SMALLDATETIME|@fecha_baja;SMALLDATETIME|@asegurada;BIT|@ECO;VARCHAR|@Cod_Usu;INT"

    Private sSPName As String = "SP_ZctCatMot"
    Private sTabla As String = "ZctCatMot"
    Private sSqlBusqueda As String = "SELECT Cod_Mot as C�digo, Placas_Mot as Placas, Right(Vin_Mot,6) as Vin, Motor_Mot as Motor  FROM [ZctSOT].[dbo].[ZctCatMot]"
    Private Sub ZctClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        'With _Permisos
        '    If .edita = False And .agrega = False And .elimina = False Then
        '        cmdAceptar.Enabled = False
        '    Else
        '        cmdAceptar.Enabled = True
        '    End If
        '    cmdEliminar.Enabled = .elimina
        'End With
        'Posici�n Inicial
        Me.Top = 0
        Me.Left = 0

        habilita(True)
        'Limpia la descripci�n
        txtCodMoto.ZctSOTLabelDesc1.Text = ""
        'Asigna los datos de la busqueda
        txtCodMoto.SqlBusqueda = sSqlBusqueda
        'Asigna el nombre del procedimiento almacenado para la busqueda
        txtCodMoto.SPName = sSPName
        'Envia las variables del procedimiento almacenado
        txtCodMoto.SpVariables = sSpVariables
        'Envia la tabla de referencia al procedimiento almacenado
        txtCodMoto.Tabla = sTabla

        'Ubicaciones
        cboUbicacion.GetDataAlfa("ZctCatUbicacion")

        'Limpia la descripci�n
        TxtCliente.ZctSOTLabelDesc1.Text = ""
        'Asigna los datos de la busqueda
        TxtCliente.SqlBusqueda = "SELECT [Cod_Cte] as C�digo ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
        'Asigna el nombre del procedimiento almacenado para la busqueda
        TxtCliente.SPName = "SP_ZctCatCte"
        'Envia las variables del procedimiento almacenado
        TxtCliente.SpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
        'Envia la tabla de referencia al procedimiento almacenado
        TxtCliente.Tabla = "ZctCatCte_Busqueda"

        CboMarca.GetData("ZctCatMar")
        cboModelo.Clear()


        'Responsables
        'Limpia la descripci�n
        ZctCodRep.Text = ""
        ZctCodRep.ZctSOTLabelDesc1.Text = ""
        'Asigna el nombre del procedimiento almacenado para la busqueda
        ZctCodRep.SPName = "SP_ZctCatRepF3"
        'Envia las variables del procedimiento almacenado
        ZctCodRep.SpVariables = "@Cod_Rep;INT|@Nom_Rep;VARCHAR"
        'Envia la tabla de referencia al procedimiento almacenado
        ZctCodRep.Tabla = "ZctCatRep"

        Dtfecha_alta.ClearValue()
        Dtfecha_baja.ClearValue()
        txtECO.Text = ""
        chkasegurada.Checked = False

    End Sub

    Private Sub GetData(ByVal sTpConsulta As Integer)
        Try

            Dim PkBusCon As New Datos.ZctDataBase

            PkBusCon.IniciaProcedimiento(sSPName)

            PkBusCon.AddParameterSP("@TpConsulta", sTpConsulta, SqlDbType.Int)
            PkBusCon.AddParameterSP("@Cod_Mot", CType(IIf(txtCodMoto.Text = "", 0, txtCodMoto.Text), Integer), Int)
            PkBusCon.AddParameterSP("@Vin_Mot", txtVin.Text.ToString.ToUpper, VarChar)
            PkBusCon.AddParameterSP("@Cod_Cte", CType(IIf(TxtCliente.Text = "", 0, TxtCliente.Text), Integer), Int)
            PkBusCon.AddParameterSP("@Cod_Mar", CType(CboMarca.value, Integer), SqlDbType.Int)
            PkBusCon.AddParameterSP("@CodMod_Mot", CType(cboModelo.value, Integer), SqlDbType.Int)
            PkBusCon.AddParameterSP("@Placas_Mot", txtPlacas.Text.ToString.ToUpper, VarChar)
            PkBusCon.AddParameterSP("@Motor_Mot", txtNumMotor.Text.ToString.ToUpper, VarChar)
            PkBusCon.AddParameterSP("@Anno_Mot", CType(IIf(txtAnno.Text = "", 0, txtAnno.Text), Integer), Int)
            PkBusCon.AddParameterSP("@Cod_Ubicacion", IIf(cboUbicacion.value = "", DBNull.Value, cboUbicacion.value), SqlDbType.VarChar)
            'PkBusCon.AddParameterSP("@disable_CatMot", IIf(chkdisable.Checked, "1", "0"), SqlDbType.Bit)
            PkBusCon.AddParameterSP("@disable_CatMot", chkdisable.Checked, SqlDbType.Bit)

            PkBusCon.AddParameterSP("@Cod_Rep", CType(IIf(ZctCodRep.Text = "", 0, ZctCodRep.Text), Integer), SqlDbType.Int)
            PkBusCon.AddParameterSP("@fecha_alta", Dtfecha_alta.Value, SqlDbType.SmallDateTime)
            PkBusCon.AddParameterSP("@fecha_baja", Dtfecha_baja.Value, SqlDbType.SmallDateTime)
            PkBusCon.AddParameterSP("@asegurada", chkasegurada.Checked, SqlDbType.Bit)
            PkBusCon.AddParameterSP("@ECO", txtECO.Text.ToUpper, VarChar)
            PkBusCon.AddParameterSP("@Cod_Usu", ControladorBase.UsuarioActual.Id, Int)

            Select Case sTpConsulta
                'SELECT
                Case 1
                    PkBusCon.InciaDataAdapter()
                    Dim dtDatos As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")
                    txtCodMoto.DataBindings.Clear()
                    txtCodMoto.DataBindings.Add("Text", dtDatos, "Cod_Mot")

                    txtVin.DataBindings.Clear()
                    txtVin.DataBindings.Add("Text", dtDatos, "Vin_Mot")

                    TxtCliente.DataBindings.Clear()
                    TxtCliente.DataBindings.Add("Text", dtDatos, "Cod_Cte")
                    TxtCliente.pCargaDescripcion()

                    ZctCodRep.DataBindings.Clear()
                    ZctCodRep.DataBindings.Add("Text", dtDatos, "Cod_Rep", True, DataSourceUpdateMode.OnPropertyChanged, "")
                    ZctCodRep.pCargaDescripcion()

                    chkdisable.DataBindings.Clear()
                    chkdisable.DataBindings.Add("Checked", dtDatos, "disable_CatMot")


                    CboMarca.DataBindings.Clear()
                    CboMarca.DataBindings.Add("ValueItem", dtDatos, "Cod_Mar")

                    cboModelo.GetData("ZctCatModXMar", CboMarca.value)

                    cboModelo.DataBindings.Clear()
                    cboModelo.DataBindings.Add("ValueItem", dtDatos, "CodMod_Mot")

                    txtPlacas.DataBindings.Clear()
                    txtPlacas.DataBindings.Add("Text", dtDatos, "Placas_Mot")

                    txtNumMotor.DataBindings.Clear()
                    txtNumMotor.DataBindings.Add("Text", dtDatos, "Motor_Mot")

                    txtAnno.DataBindings.Clear()
                    txtAnno.DataBindings.Add("Text", dtDatos, "Anno_Mot")

                    cboUbicacion.DataBindings.Clear()
                    cboUbicacion.DataBindings.Add("ValueItemStr", dtDatos, "Cod_Ubicacion", False, DataSourceUpdateMode.OnPropertyChanged, "NTE")

                    Dtfecha_alta.DataBindings.Clear()
                    Dtfecha_alta.DataBindings.Add("Value", dtDatos, "fecha_alta")
                    Dtfecha_baja.DataBindings.Clear()
                    Dtfecha_baja.DataBindings.Add("Value", dtDatos, "fecha_alta")
                    txtECO.DataBindings.Clear()
                    txtECO.DataBindings.Add("Text", dtDatos, "ECO")
                    chkasegurada.DataBindings.Clear()
                    chkasegurada.DataBindings.Add("Checked", dtDatos, "asegurada")

                    dtDatos.Dispose()
                Case 2
                    'Update,Insert  
                    PkBusCon.GetScalarSP()
                Case 3
                    'Delete
                    PkBusCon.GetScalarSP()
            End Select

            'PkBusCon.InciaDataAdapter()
            'DGBusqueda.DataSource = PkBusCon.GetReaderSP.Tables("DATOS")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub txtBusqCodCte_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodMoto.lostfocus
        If CType(IIf(txtCodMoto.Text = "", 0, txtCodMoto.Text), Integer) > 0 Then
            habilita(False)
            GetData(1)
        End If
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        If txtVin.Text = "" Then MsgBox("Debe proporcinar n�mero de vin.") : Exit Sub
        If TxtCliente.Text = "0" Or TxtCliente.Text = "" Then MsgBox("Debe proporcinar el c�digo del cliente.") : Exit Sub
        If CboMarca.Text = "" Or cboModelo.Text = "" Then MsgBox("Debe de porporcionar la marca y el modelo.") : CboMarca.Focus() : Exit Sub

        If txtCodMoto.Text = "0" Then
            Dim PkBusCon As New Datos.ZctDataBase

            PkBusCon.IniciaProcedimiento("SP_ZctCatMotXVin")

            PkBusCon.AddParameterSP("@TpConsulta", 1, SqlDbType.Int)
            PkBusCon.AddParameterSP("@Vin_Mot", txtVin.Text.ToString.ToUpper, VarChar)
            Dim cod_moto As Integer = PkBusCon.GetScalarSP()
            If cod_moto > 0 Then
                MsgBox("El vin proporcionado ya existe, favor de verificar.")
                Exit Sub
            End If
        End If

        'VALIDACI�N PARA LOS DATOS
        GetData(2)
        Dim cClas As New Datos.ClassGen
        cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctMotos", "A", txtCodMoto.Text)
        plimpia()
    End Sub


    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If txtCodMoto.Text = 0 Then
            Me.Dispose()
        Else
            plimpia()
        End If
    End Sub
    Private Sub plimpia()
        habilita(True)
        txtCodMoto.DataBindings.Clear()
        txtCodMoto.Text = ""
        txtVin.DataBindings.Clear()
        txtVin.Text = ""
        TxtCliente.DataBindings.Clear()
        TxtCliente.Text = ""
        TxtCliente.ZctSOTLabelDesc1.Text = ""

        txtAnno.DataBindings.Clear()
        txtAnno.Text = ""
        txtPlacas.DataBindings.Clear()
        txtPlacas.Text = ""
        txtNumMotor.DataBindings.Clear()
        txtNumMotor.Text = ""

        chkdisable.DataBindings.Clear()
        chkdisable.Checked = False
        Dtfecha_alta.DataBindings.Clear()
        Dtfecha_alta.ClearValue()
        Dtfecha_baja.DataBindings.Clear()
        Dtfecha_baja.ClearValue()
        txtECO.DataBindings.Clear()
        txtECO.Text = ""
        chkasegurada.DataBindings.Clear()
        chkasegurada.Checked = False
        ZctCodRep.Text = ""
        ZctCodRep.ZctSOTLabelDesc1.Text = ""


        txtCodMoto.Text = 0
        txtCodMoto.ZctSOTLabelDesc1.Text = ""
        txtCodMoto.Focus()


    End Sub

    Private Sub CboMarca_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboMarca.lostfocus
        cboModelo.GetData("ZctCatModXMar", CboMarca.value)
    End Sub


    Private Sub habilita(ByVal Valor As Boolean)
        txtCodMoto.Enabled = Valor
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkdisable.CheckedChanged

    End Sub

    Private Sub txtECO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtECO.Load

    End Sub

    Private Sub cmdEliminar_Click(sender As System.Object, e As System.EventArgs) Handles cmdEliminar.Click
        Try
            If MsgBox("�Desea eliminar esta motocicleta?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

            'VALIDACI�N PARA LOS DATOS
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctCatMot", "D", txtCodMoto.Text)
            GetData(3)
            plimpia()
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al eliminar el cliente, verfique que dicha motocicleta no tenga ordenes de trabajo asignadas.")
        End Try
    End Sub

    Private Sub ZctSOTGroupBox1_Enter(sender As Object, e As EventArgs) Handles ZctSOTGroupBox1.Enter

    End Sub
End Class