Imports System.Diagnostics
Imports System.Windows.Forms
Imports SOTControladores.Controladores
Imports System.IO
Imports Transversal.Excepciones

Public Class ZctRespaldo

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Archivos de respaldo (*.bak)|*.bak"
        SaveFileDialog.ShowDialog(Me)
        Dim FileName As String = SaveFileDialog.FileName
        ZctTxtPath.Text = FileName
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If ZctTxtPath.Text = "" Then Exit Sub

            If Not Directory.Exists(Path.GetDirectoryName(ZctTxtPath.Text)) Then
                Throw New SOTException("El directorio {0} no existe", Path.GetDirectoryName(ZctTxtPath.Text))
            End If

            Cursor = Cursors.WaitCursor
            Dim pkConR As New Datos.ZctDataBase
            'Inicia el procedimieto almacenado
            pkConR.IniciaProcedimiento("SP_ZctRespaldo")
            'Agrega el parametro que define que se va a realizar

            pkConR.AddParameterSP("@Ruta", ZctTxtPath.Text, SqlDbType.VarChar)
            Dim res = pkConR.GetScalarSP()
            Cursor = Cursors.Default

            If (Not IsNothing(res)) Then
                MsgBox("Se ha respaldado la base de datos de forma correcta.", MsgBoxStyle.Information)
                Dim cClas As New Datos.ClassGen
                cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdCompra", "A", ZctTxtPath.Text)
            End If

            Me.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
            Cursor = Cursors.Default

        End Try
    End Sub



End Class
