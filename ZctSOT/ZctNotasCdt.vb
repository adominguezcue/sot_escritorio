﻿Imports System.Data
Imports System.Data.SqlClient.SqlException
Imports System.Collections.Generic
Imports System.Text
Imports ZctSOT.Datos

Namespace NotasCredito
    Public Class NotaCredito

        Private _folio As Integer
        Private _ordenOrigen As zctFolios
        Private _cliente As Integer
        Private _fecha As Date
        Private _articulos As articulo_notaCredito


        Public Property folio() As Integer

            Get
                Return _folio
            End Get
            Set(ByVal value As Integer)
                Me._folio = value
            End Set
        End Property

        Public Property OrdenOrigen() As zctFolios
            Get
                Return Me._ordenOrigen
            End Get
            Set(ByVal value As zctFolios)
                Me._ordenOrigen = value
            End Set
        End Property

        Public Property Cliente() As Integer
            Get
                Return Me._cliente
            End Get
            Set(ByVal value As Integer)
                Me._cliente = value
            End Set
        End Property

        Public Property Fecha() As Date
            Get
                Return Me._fecha
            End Get
            Set(ByVal value As Date)
                Me._fecha = value
            End Set
        End Property

        Public Property Articulos() As articulo_notaCredito
            Get
                Return Me._articulos
            End Get
            Set(ByVal value As articulo_notaCredito)
                Me._articulos = value
            End Set
        End Property

    End Class


    Public Class articulo_notaCredito

        Private _codigo As String
        Private _descripcion As String
        Private _cantidad_surtido_orden As Integer
        Private _cantidad_aplicada_anteriormente As Integer
        Private _cantidad_por_aplicar As Integer

        Public Property Codigo() As String
            Get
                Return Me._codigo
            End Get
            Set(ByVal value As String)
                Me._codigo = value
            End Set
        End Property

        Public Property Descripcion() As String
            Get
                Return Me._descripcion
            End Get
            Set(ByVal value As String)
                Me._descripcion = value
            End Set
        End Property

        Public Property CantidadSurtidoOrden() As Integer
            Get
                Return Me._cantidad_surtido_orden
            End Get
            Set(ByVal value As Integer)
                Me._cantidad_surtido_orden = value
            End Set
        End Property

        Public Property CantidadAplicadaAnteriormente() As Integer
            Get
                Return Me._cantidad_aplicada_anteriormente
            End Get
            Set(ByVal value As Integer)
                Me._cantidad_aplicada_anteriormente = value
            End Set
        End Property

        Public Property CantidadPorAplicar() As Integer
            Get
                Return Me._cantidad_por_aplicar
            End Get
            Set(ByVal value As Integer)
                Me._cantidad_por_aplicar = value

            End Set
        End Property
    End Class

    Public Class Controler

        Public Function ObtenerNomCliente(ByVal codigo As Integer) As String
            Dim db As New Datos.ZctDataBase()

            'NO es posible una inyeccion SQL porque se recive un entero
            db.SQL = "Select c.Nom_Cte from ZctCatCte c, ZctEncOT ot where c.Cod_Cte = ot.Cod_Cte and ot.CodEnc_OT = " & CStr(codigo)

            Try

                Dim data As DataTable = db.GetDataTable()
                If data.Rows.Count < 0 Then
                    Throw New Exception("Error, no existe ningúna orden de trabajo con ese código")
                Else
                    Return data.Rows(0).Item("Nom_Cte")
                End If
            Catch ex As SqlClient.SqlException
                Throw ex
            Finally
                db.Dispose()
            End Try
        End Function

        Public Function obtenerArticulos(ByVal codigo As Integer, _
                                         ByVal codCdt As Integer, _
                                               Optional ByVal nuevo As Boolean = False) As DataTable
            Try
                Dim data As DataTable = ejecutarProcedimiento(0, cod_encOt:=codigo).Tables(0)
                Dim anteriores As DataTable = ejecutarProcedimientoDet(0, cod_encOt:=codigo).Tables(0)
                Dim aplicar As New DataTable

                If Not nuevo Then
                    aplicar = ejecutarProcedimientoDet(0, codCdt, cod_encOt:=codigo).Tables(0)
                End If

                data.Columns.Add("Anterior")
                data.Columns.Add("PorAplicar")

                Dim Cod_DetOT As String

                For Each rowData As DataRow In data.Rows

                    Cod_DetOT = rowData("Cod_DetOT")
                    For Each rowAnterior As DataRow In anteriores.Rows
                        If rowAnterior("Cod_DetOT") = Cod_DetOT Then
                            rowData("Anterior") = rowAnterior("Cantidad")
                            Exit For
                        End If
                    Next
                    If Not nuevo Then
                        For Each rowAplicar As DataRow In aplicar.Rows
                            If rowAplicar("Cod_DetOT") = Cod_DetOT Then
                                rowData("PorAplicar") = rowAplicar("Cantidad")
                                Exit For
                            End If
                        Next
                    End If
                Next
                Return data
            Catch ex As SqlClient.SqlException
                Throw ex
            End Try

        End Function


        Private Function ejecutarProcedimiento( _
            ByVal tpConsulta As Integer, _
                  Optional ByVal cod_cdt As Integer = 0, _
                  Optional ByVal Fecha As DateTime = Nothing, _
                  Optional ByVal cod_encOt As Integer = 0, _
                  Optional ByVal Observaciones As String = "") As DataSet

            Dim db As New Datos.ZctDataBase()

            db.IniciaProcedimiento("SP_ZctNotasCdt")

            db.AddParameterSP("@TpConsulta", tpConsulta, SqlDbType.Int)
            db.AddParameterSP("@Cod_Cdt", cod_cdt, SqlDbType.Int)
            If Fecha = Nothing Then
                db.AddParameterSP("@Fecha", DateTime.Now, SqlDbType.DateTime)
            Else
                db.AddParameterSP("@Fecha", Fecha, SqlDbType.DateTime)
            End If

            db.AddParameterSP("@Cod_EncOT", cod_encOt, SqlDbType.Int)
            db.AddParameterSP("@Observaciones", Observaciones, SqlDbType.Text)

            Try
                Return db.GetReaderSP()
            Catch ex As SqlClient.SqlException
                Throw ex
            Finally
                db.Dispose()
            End Try

        End Function

        Private Function ejecutarProcedimientoDet( _
    ByVal tpConsulta As Integer, _
          Optional ByVal cod_cdt As Integer = 0, _
          Optional ByVal Cod_DetCdt As Integer = 0, _
          Optional ByVal codigoArt As String = "", _
          Optional ByVal cantidad As Integer = 0, _
          Optional ByVal cod_encOt As Integer = 0, _
          Optional ByVal cod_detOt As Integer = 0) As DataSet

            Dim db As New Datos.ZctDataBase()

            db.IniciaProcedimiento("SP_ZctDetNotasCdt")

            db.AddParameterSP("@TpConsulta", tpConsulta, SqlDbType.Int)
            db.AddParameterSP("@Cod_Cdt", cod_cdt, SqlDbType.Int)
            db.AddParameterSP("@Cod_DetCdt", 0, SqlDbType.Int)
            db.AddParameterSP("@Cod_Art", codigoArt, SqlDbType.VarChar)
            db.AddParameterSP("@Cantidad", cantidad, SqlDbType.Int)
            db.AddParameterSP("@Cod_EncOT", cod_encOt, SqlDbType.Int)
            db.AddParameterSP("@Cod_DetOT", cod_detOt, SqlDbType.Int)


            Try
                Return db.GetReaderSP()
            Catch ex As SqlClient.SqlException
                Throw ex
            Finally
                db.Dispose()
            End Try

        End Function

        Public Sub grabar(ByRef tabla As DataTable, ByVal codigoOTS As String, ByVal folio As String, ByVal observaciones As String)

            Dim codigoOT, codigoCdt As Integer

            Try
                codigoOT = CInt(codigoOTS)
                codigoCdt = CInt(folio)
            Catch ex As Exception
                Throw New Exception("Error, el código de la orden, y el folio necesitan ser números")
            End Try

            Try
                Me.ejecutarProcedimiento(1, codigoCdt, DateTime.Now, codigoOT, observaciones)
            Catch ex As Exception
                Throw ex
            End Try

            For Each row As DataRow In tabla.Rows

                If Not IsDBNull(row("PorAplicar")) Then
                    Try
                        grabarCambio(codigoOT, row("Cod_Art"), CInt(row("PorAplicar")), codigoCdt, CInt(row("Cod_DetOT")))
                    Catch ex As Exception
                        Throw ex
                    End Try

                End If
            Next

        End Sub

        Private Sub grabarCambio(ByVal codigoOT As Integer, ByVal codigoArt As String, ByVal cantidad As Integer, ByVal CodCdt As Integer, ByVal CodDetOt As Integer)
            ejecutarProcedimientoDet(1, CodCdt, 0, codigoArt, cantidad, cod_detOt:=CodDetOt)
        End Sub

        Public Function obtenerNota(ByVal codigo As Integer) As DataTable
            Try
                Return ejecutarProcedimiento(0, cod_cdt:=codigo).Tables(0)
            Catch ex As SqlClient.SqlException
                Throw ex
            End Try
        End Function



    End Class
End Namespace
