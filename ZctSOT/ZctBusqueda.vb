Imports ZctSOT.ZctDataBase
Imports ZctSOT.ZctFunciones
Imports ZctSOT.Datos.ZctFunciones

Public Class ZctBusqueda
    Dim dDataView As DataView

    Public sSQLP As String

    'Parametros para los SP
    Public sSPName As String
    Public sSpVariables As String
    Public sSPParametros As String
    Public OmitirParametros As Boolean



    'Parametro de la busqueda 
    Public iColBusq As Integer
    Public sBusq As String
    Public sColNuevo = "ColNuevo"

    Public iValor As String
    Public sServer As String
    Public iRow As Integer
    Public iCol As Integer

    Private Sub ZctBusqueda_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If sSPParametros Is Nothing OrElse sSPParametros = "" Then
                GetData()
            Else
                GetData(sSPParametros)
            End If
            DGBusqueda.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            DGBusqueda.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells)

            If DGBusqueda.ColumnCount >= 2 Then
                TxtColBus.Nombre = DGBusqueda.Columns(1).HeaderText.ToString
                TxtColBus.Text = ""
                TxtColBus.Enabled = True
                TxtColBus.Focus()
            End If
            If DGBusqueda.Columns.Contains("ColNuevo") Then
                DGBusqueda.Columns.Item("ColNuevo").Visible = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        iValor = DGBusqueda.Item(0, DGBusqueda.CurrentCell.RowIndex).Value
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub


    Private Sub GetData()
        Dim PkBusCon As New Datos.ZctDataBase
        Dim iSp As Integer

        Dim sVector() As String = Split(sSpVariables, "|")


        'PkBusCon.Open()
        PkBusCon.IniciaProcedimiento(sSPName)
        If Not OmitirParametros Then
            PkBusCon.AddParameterSP("@TpConsulta", 1, SqlDbType.Int)
            Dim iTot As Integer = UBound(sVector)
            For iSp = 0 To iTot
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                    Case "INT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.Int)

                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), Now, SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), 0, SqlDbType.Decimal)
                End Select
            Next
        End If
        PkBusCon.InciaDataAdapter()
        If DGBusqueda.Columns.Contains(sColNuevo) = True Then
            DGBusqueda.Columns(sColNuevo).Visible = False
            DGBusqueda.DataSource = PkBusCon.GetReaderSP.Tables("DATOS")


        Else
            dDataView = New DataView(PkBusCon.GetReaderSP.Tables("DATOS"))
            DGBusqueda.DataSource = dDataView
        End If
    End Sub


    Private Sub GetData(ByVal TpConsulta As Integer, ByVal iRow As Integer)
        Dim PkBusCon As New Datos.ZctDataBase
        Dim iSp As Integer
        Dim sVector() As String = Split(sSpVariables, "|")

        If iRow > DGBusqueda.RowCount Then Exit Sub

        PkBusCon.IniciaProcedimiento(sSPName)

        PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
        Dim iTot As Integer = UBound(sVector)
        For iSp = 0 To iTot
            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
                Case "INT"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, Integer), SqlDbType.Int)
                Case "VARCHAR"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, String), SqlDbType.VarChar)
                Case "SMALLDATETIME"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, Date), SqlDbType.SmallDateTime)
                Case "DECIMAL"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(DGBusqueda.Item(iSp, iRow).Value, Decimal), SqlDbType.Decimal)
            End Select
        Next
        PkBusCon.GetScalarSP()
    End Sub

    Private Sub GetData(ByVal sSPParametros As String)
        Dim PkBusCon As New Datos.ZctDataBase
        Dim iSp As Integer

        Dim sVector() As String = Split(sSpVariables, "|")
        Dim sValores() As String = Split(sSPParametros, "|")


        'PkBusCon.Open()
        PkBusCon.IniciaProcedimiento(sSPName)

        PkBusCon.AddParameterSP("@TpConsulta", 1, SqlDbType.Int)
        Dim iTot As Integer = UBound(sVector)
        For iSp = 0 To iTot
            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1).ToUpper
                Case "INT"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Int)

                Case "VARCHAR"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_String), SqlDbType.VarChar)
                Case "SMALLDATETIME"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_Date), SqlDbType.SmallDateTime)
                Case "DECIMAL"
                    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), GetDefault(sValores(iSp), ZctTipos.Zct_Int), SqlDbType.Decimal)
            End Select
        Next
        PkBusCon.InciaDataAdapter()
        If DGBusqueda.Columns.Contains(sColNuevo) = True Then
            DGBusqueda.Columns(sColNuevo).Visible = False
            DGBusqueda.DataSource = PkBusCon.GetReaderSP.Tables("DATOS")


        Else
            dDataView = New DataView(PkBusCon.GetReaderSP.Tables("DATOS"))
            DGBusqueda.DataSource = dDataView
        End If
    End Sub




    Private Sub DGBusqueda_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGBusqueda.CellDoubleClick
        If e.RowIndex >= 0 Then
            iValor = DGBusqueda.Item(0, e.RowIndex).Value


            Me.Dispose()
            Me.Close()
            Application.DoEvents()

        End If
    End Sub

    Private Sub DGBusqueda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGBusqueda.KeyDown
        If e.KeyCode = Keys.Enter Then
            iValor = DGBusqueda.Item(0, DGBusqueda.CurrentCell.RowIndex).Value

            Me.Dispose()
            Me.Close()
            Application.DoEvents()
        End If
    End Sub

    Private Sub DGBusqueda_ColumnHeaderMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles DGBusqueda.ColumnHeaderMouseDoubleClick
        If e.ColumnIndex >= 0 And DGBusqueda.Columns(e.ColumnIndex).ValueType.Name = "String" Then

            TxtColBus.Nombre = DGBusqueda.Columns(e.ColumnIndex).HeaderText.ToString
            TxtColBus.Text = ""
            TxtColBus.Enabled = True
            TxtColBus.Focus()
        End If
    End Sub

    Private Sub TxtColBus_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtColBus.TextChanged
        If TxtColBus.Nombre <> "Columna:" Then
            dDataView.RowFilter = TxtColBus.Nombre & " LIKE '%" & TxtColBus.Text & "%' "
        End If
    End Sub
End Class

