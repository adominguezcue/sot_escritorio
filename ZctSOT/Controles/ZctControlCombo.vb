Imports System.Math
Public Class ZctControlCombo
    Public Shadows Event lostfocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event SelectedValueChanged(sender As Object, e As System.EventArgs)

    Public Property Nombre() As String
        Get
            Return ZctSOTLabel1.Text
        End Get
        Set(ByVal value As String)
            ZctSOTLabel1.Text = value
        End Set
    End Property

    Public Sub Clear()
        Me.ZctSOTCombo1.Items.Clear()
    End Sub

    Public Property value() As Object
        Get
            Return ZctSOTCombo1.SelectedValue
        End Get
        Set(ByVal value As Object)
            ZctSOTCombo1.SelectedValue = value
        End Set
    End Property

    Public Overrides Property Text() As String
        Get
            Return ZctSOTCombo1.Text
        End Get
        Set(ByVal value As String)
            ZctSOTCombo1.Text = value
        End Set
    End Property

    Public Property ValueItem() As Integer
        Get
            Return ZctSOTCombo1.SelectedValue
        End Get
        Set(ByVal value As Integer)
            ZctSOTCombo1.SelectedValue = value
        End Set
    End Property

    Public Property ValueItemStr() As String
        Get
            Return ZctSOTCombo1.SelectedValue
        End Get
        Set(ByVal value As String)
            ZctSOTCombo1.SelectedValue = value
        End Set
    End Property

    Public Property DropDownStyle() As System.Windows.Forms.ComboBoxStyle
        Get
            Return ZctSOTCombo1.DropDownStyle
        End Get
        Set(ByVal value As System.Windows.Forms.ComboBoxStyle)
            ZctSOTCombo1.DropDownStyle = value
        End Set
    End Property

    Private Sub ZctSOTLabel1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTLabel1.Resize
        ZctSOTCombo1.Left = ZctSOTLabel1.Width + ZctSOTLabel1.Left + 5
        ZctSOTCombo1.Width = Abs(Me.Width - (ZctSOTCombo1.Left + 5))
    End Sub

    Private Sub ZctControlBusqueda_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        ZctSOTCombo1.Width = Abs(Me.Width - (ZctSOTCombo1.Left + 5))
    End Sub

    Public Sub GetData(ByVal Tabla As String, ByVal Parametro As Integer, Optional ByVal AddAll As Boolean = False)
        GetData(Tabla, Parametro.ToString, AddAll)
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Tabla"></param>
    ''' <param name="AddAll">
    ''' Indica si se agrega un item llamado todos
    ''' </param>
    ''' <remarks></remarks>
    Public Sub GetData(ByVal Tabla As String, Optional ByVal AddAll As Boolean = False)
        Dim PkBusCon As New Datos.ZctDataBase

        PkBusCon.IniciaProcedimiento("SP_ZctBusqueda")
        PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)

        PkBusCon.InciaDataAdapter()
        'ZctSOTCombo1.Items.Clear()
        Dim dt As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")
        If AddAll Then
            dt.Rows.Add(0, "TODOS")
        End If
        Dim dw As New DataView(dt)
        dw.Sort = dt.Columns(0).ColumnName

        ZctSOTCombo1.DataSource = dw
        ZctSOTCombo1.DisplayMember = "Descripcion"
        ZctSOTCombo1.ValueMember = "Codigo"
    End Sub

    Public Sub Limpia()
        If ZctSOTCombo1.DataSource IsNot Nothing Then ZctSOTCombo1.DataSource = Nothing
        ZctSOTCombo1.Items.Clear()
    End Sub

    Public Sub GetData(ByVal Tabla As String, ByVal Parametro As String, Optional ByVal AddAll As Boolean = False)
        Dim PkBusCon As New Datos.ZctDataBase

        PkBusCon.IniciaProcedimiento("SP_ZctBusqueda")
        PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@Busqueda", Parametro, SqlDbType.Int)
        PkBusCon.InciaDataAdapter()
        ZctSOTCombo1.DataSource = Nothing
        'ZctSOTCombo1.Items.Clear()
        Dim dt As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")
        If AddAll Then
            dt.Rows.Add(0, "TODOS")
        End If

        Dim dw As New DataView(dt)
        dw.Sort = dt.Columns(0).ColumnName

        ZctSOTCombo1.DataSource = dw
        ZctSOTCombo1.DisplayMember = "Descripcion"
        ZctSOTCombo1.ValueMember = "Codigo"

    End Sub

    Public Sub GetDataAlfa(ByVal Tabla As String)
        Dim PkBusCon As New Datos.ZctDataBase

        PkBusCon.IniciaProcedimiento("SP_ZctBusqueda_Alfa")
        PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)

        PkBusCon.InciaDataAdapter()
        'ZctSOTCombo1.Items.Clear()
        ZctSOTCombo1.DataSource = PkBusCon.GetReaderSP.Tables("DATOS")
        ZctSOTCombo1.DisplayMember = "Descripcion"
        ZctSOTCombo1.ValueMember = "Codigo"
    End Sub

    Public Sub GetDataAlfa(ByVal Tabla As String, ByVal Parametro As String)
        Dim PkBusCon As New Datos.ZctDataBase

        PkBusCon.IniciaProcedimiento("SP_ZctBusqueda_Alfa")
        PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@Busqueda", Parametro, SqlDbType.VarChar)
        PkBusCon.InciaDataAdapter()
        ZctSOTCombo1.DataSource = Nothing
        ZctSOTCombo1.Items.Clear()
        Dim dt As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")
        Dim dw As New DataView(dt)
        ZctSOTCombo1.DataSource = dw 'PkBusCon.GetReaderSP.Tables("DATOS")
        ZctSOTCombo1.DisplayMember = "Descripcion"
        ZctSOTCombo1.ValueMember = "Codigo"

    End Sub


    Private Sub ZctSOTCombo1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTCombo1.LostFocus
        RaiseEvent lostfocus(sender, e)
    End Sub

    Public Overloads Property Tag() As Object
        Get
            Return ZctSOTCombo1.Tag
        End Get
        Set(ByVal value)
            ZctSOTCombo1.Tag = value
        End Set
    End Property

    Private Sub ZctSOTCombo1_SelectedValueChanged(sender As Object, e As System.EventArgs) Handles ZctSOTCombo1.SelectedValueChanged
        RaiseEvent SelectedValueChanged(sender, e)
    End Sub
End Class
