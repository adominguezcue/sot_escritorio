Imports System.Math
Imports ZctSOT.Datos.ClassGen
<DebuggerStepThrough()> Public Class ZctControlTexto
    Public Shadows Event lostfocus(ByVal sender As Object, ByVal e As System.EventArgs)
    'Public Shadows Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event TextChanged As EventHandler



    Public Property Nombre() As String
        Get
            Return ZctSOTLabel1.Text
        End Get
        Set(ByVal value As String)
            ZctSOTLabel1.Text = value
        End Set
    End Property

    Public Property TipoDato() As Tipo_Dato
        Get
            Return ZctSOTTextBox1.TipoDato
        End Get
        Set(ByVal value As Tipo_Dato)
            ZctSOTTextBox1.TipoDato = value
        End Set
    End Property


    Public Overrides Property Text() As String
        Get
            Return ZctSOTTextBox1.Text.ToUpper

        End Get
        Set(ByVal value As String)
            ZctSOTTextBox1.Text = value.ToUpper

        End Set
    End Property

    Private Sub ZctSOTLabel1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTLabel1.Resize
        ZctSOTTextBox1.Left = ZctSOTLabel1.Width + ZctSOTLabel1.Left + 5
        ZctSOTTextBox1.Width = Abs(Me.Width - (ZctSOTTextBox1.Left + 5))
    End Sub

    Private Sub ZctControlTexto_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        ZctSOTTextBox1.Width = Abs(Me.Width - (ZctSOTTextBox1.Left + 5))
        ZctSOTTextBox1.Height = Abs(Me.Height - 10)
    End Sub

    Public Property Multiline() As Boolean
        Get
            Return ZctSOTTextBox1.Multiline
        End Get

        Set(ByVal value As Boolean)
            ZctSOTTextBox1.Multiline = value
            If value = True Then
                ZctSOTTextBox1.ScrollBars = ScrollBars.Vertical
            Else
                ZctSOTTextBox1.ScrollBars = ScrollBars.None
            End If

        End Set
    End Property

    Private Sub ZctSOTTextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTTextBox1.LostFocus
        RaiseEvent lostfocus(sender, e)
    End Sub


    Private Sub ZctSOTTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZctSOTTextBox1.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

    Public Overloads Property ToolTip()
        Get
            Return Me.ZctSOTToolTipInt.GetToolTip(Me.ZctSOTTextBox1)
        End Get
        Set(ByVal value)
            Me.ZctSOTToolTipInt.SetToolTip(Me.ZctSOTTextBox1, value)
        End Set
    End Property

    Public Overloads Property Tag() As Object
        Get
            Return ZctSOTTextBox1.Tag
        End Get
        Set(ByVal value)
            ZctSOTTextBox1.Tag = value
        End Set
    End Property

End Class
