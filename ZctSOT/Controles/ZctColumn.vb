Public Class zctColumnText
    Inherits DataGridViewColumn

    Sub New(ByVal Nombre As String, ByVal Header As String, ByVal Propiedad As String, ByVal Visible As Boolean, Optional ByVal Format As String = "")
        MyBase.New()
        Me.Name = Nombre
        Me.HeaderText = Header
        Me.DataPropertyName = Propiedad
        Me.Visible = Visible
        Me.CellTemplate = New DataGridViewTextBoxCell


        Me.DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8)
        Me.DefaultCellStyle.Format = Format
    End Sub

    Sub New(ByVal Nombre As String, ByVal Header As String, ByVal Propiedad As String, ByVal Visible As Boolean, ByVal MeForeColor As Drawing.Color, ByVal MeBackColor As Drawing.Color, Optional ByVal Bold As Boolean = False, Optional ByVal Format As String = "")
        MyBase.New()
        Me.Name = Nombre
        Me.HeaderText = Header
        Me.DataPropertyName = Propiedad
        Me.Visible = Visible
        Me.CellTemplate = New DataGridViewTextBoxCell
        Me.DefaultCellStyle.Format = Format
        Me.DefaultCellStyle.ForeColor = MeForeColor
        Me.DefaultCellStyle.BackColor = MeBackColor
        Me.DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8)
        If Me.DefaultCellStyle.Font IsNot Nothing AndAlso Bold = True Then Me.DefaultCellStyle.Font = New System.Drawing.Font(Me.DefaultCellStyle.Font, FontStyle.Bold)



    End Sub

    Public Sub New()

    End Sub



End Class

Public Class zctColumnBoton
    Inherits DataGridViewButtonColumn

    Sub New(ByVal Nombre As String, ByVal Header As String, ByVal Propiedad As String, ByVal Visible As Boolean, ByVal Texto As String, Optional ByVal Format As String = "")
        MyBase.New()
        Me.Name = Nombre
        Me.HeaderText = Header
        Me.DataPropertyName = Propiedad
        Me.Visible = Visible
        'Me.CellTemplate = New DataGridViewTextBox
        Me.DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8)
        Me.DefaultCellStyle.Format = Format
        Me.Text = Texto
    End Sub

    Sub New(ByVal Nombre As String, ByVal Header As String, ByVal Propiedad As String, ByVal Visible As Boolean, ByVal MeForeColor As Drawing.Color, ByVal MeBackColor As Drawing.Color, ByVal Texto As String, Optional ByVal Bold As Boolean = False, Optional ByVal Format As String = "")
        MyBase.New()
        Me.Name = Nombre
        Me.HeaderText = Header
        Me.DataPropertyName = Propiedad
        Me.Visible = Visible
        'Me.CellTemplate = New DataGridViewTextBoxCell
        Me.DefaultCellStyle.Format = Format
        Me.DefaultCellStyle.ForeColor = MeForeColor
        Me.DefaultCellStyle.BackColor = MeBackColor
        Me.DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8)
        Me.Text = Texto
        If Me.DefaultCellStyle.Font IsNot Nothing AndAlso Bold = True Then Me.DefaultCellStyle.Font = New System.Drawing.Font(Me.DefaultCellStyle.Font, FontStyle.Bold)



    End Sub

    Public Sub New()

    End Sub



End Class

Public Class zctColumnComun


    Public Shared Function GetColumn(ByVal Nombre As String, ByVal Header As String, ByVal Propiedad As String, ByVal Visible As Boolean, Optional ByVal Format As String = "", Optional ByVal Tipo As Type = Nothing, Optional ByVal SoloLectura As Boolean = False, Optional ByVal ValorDefault As Object = Nothing) As DataGridViewColumn
        Dim miCol As New DataGridViewColumn
        miCol.Name = Nombre
        miCol.HeaderText = Header
        miCol.DataPropertyName = Propiedad
        miCol.Visible = Visible
        miCol.ReadOnly = SoloLectura
        'miCol.CellTemplate = New DataGridViewTextBoxCell
        If Tipo IsNot Nothing AndAlso Tipo Is GetType(Boolean) Then
            miCol.CellTemplate = New DataGridViewCheckBoxCell
            miCol.ValueType = Tipo
        Else
            miCol.CellTemplate = New DataGridViewTextBoxCell
            If Tipo IsNot Nothing Then miCol.ValueType = Tipo
        End If

        If ValorDefault IsNot Nothing Then
            miCol.DefaultCellStyle.NullValue = ValorDefault
        End If

        miCol.DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8)
        miCol.DefaultCellStyle.Format = Format
        Return miCol
    End Function

    Public Shared Function GetColumn(ByVal Nombre As String, ByVal Header As String, ByVal Propiedad As String, ByVal Visible As Boolean, ByVal MeForeColor As Drawing.Color, ByVal MeBackColor As Drawing.Color, Optional ByVal Bold As Boolean = False, Optional ByVal Format As String = "", Optional ByVal Tipo As Type = Nothing) As DataGridViewColumn
        Dim miCol As New DataGridViewColumn
        miCol.Name = Nombre
        miCol.HeaderText = Header
        miCol.DataPropertyName = Propiedad
        miCol.Visible = Visible
        miCol.DefaultCellStyle.Format = Format
        miCol.DefaultCellStyle.ForeColor = MeForeColor
        miCol.DefaultCellStyle.BackColor = MeBackColor
        If Tipo IsNot Nothing AndAlso Tipo Is GetType(Boolean) Then
            miCol.CellTemplate = New DataGridViewCheckBoxCell
            miCol.ValueType = Tipo
        Else
            miCol.CellTemplate = New DataGridViewTextBoxCell
            If Tipo IsNot Nothing Then miCol.ValueType = Tipo
        End If
        miCol.DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8)
        If miCol.DefaultCellStyle.Font IsNot Nothing AndAlso Bold = True Then miCol.DefaultCellStyle.Font = New System.Drawing.Font(miCol.DefaultCellStyle.Font, FontStyle.Bold)
        Return miCol


    End Function
End Class

Public Class zctColumnCombo
    Public Shared Function GetColumn(ByVal Nombre As String, ByVal Header As String, ByVal Propiedad As String, ByVal Visible As Boolean, ByVal Dic As IDictionary) As DataGridViewComboBoxColumn
        Dim miCol As New DataGridViewComboBoxColumn
        miCol.Name = Nombre
        miCol.HeaderText = Header
        miCol.DataPropertyName = Propiedad
        miCol.Visible = Visible
        miCol.DefaultCellStyle.Font = New Drawing.Font("Microsoft Sans Serif", 8)
        Dim ds As New BindingSource
        ds.DataSource = Dic
        miCol.DataSource = ds
        miCol.DisplayMember = "value"
        miCol.ValueMember = "key"
        Return miCol


    End Function
End Class