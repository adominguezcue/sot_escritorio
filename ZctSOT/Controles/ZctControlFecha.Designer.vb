<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctControlFecha
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel
        Me.ZctSotDate1 = New ZctSOT.ZctSotDate
        Me.ZctSotTime1 = New ZctSOT.ZctSotTime
        Me.SuspendLayout()
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(3, 6)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(77, 13)
        Me.ZctSOTLabel1.TabIndex = 0
        Me.ZctSOTLabel1.Text = "ZctSOTLabel1"
        '
        'ZctSotDate1
        '
        Me.ZctSotDate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ZctSotDate1.Location = New System.Drawing.Point(86, 2)
        Me.ZctSotDate1.Name = "ZctSotDate1"
        Me.ZctSotDate1.Size = New System.Drawing.Size(91, 20)
        Me.ZctSotDate1.TabIndex = 1
        '
        'ZctSotTime1
        '
        Me.ZctSotTime1.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.ZctSotTime1.Location = New System.Drawing.Point(183, 2)
        Me.ZctSotTime1.Name = "ZctSotTime1"
        Me.ZctSotTime1.ShowUpDown = True
        Me.ZctSotTime1.Size = New System.Drawing.Size(98, 20)
        Me.ZctSotTime1.TabIndex = 2
        '
        'ZctControlFecha
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ZctSotTime1)
        Me.Controls.Add(Me.ZctSotDate1)
        Me.Controls.Add(Me.ZctSOTLabel1)
        Me.Name = "ZctControlFecha"
        Me.Size = New System.Drawing.Size(289, 27)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSotDate1 As ZctSOT.ZctSotDate
    Friend WithEvents ZctSotTime1 As ZctSOT.ZctSotTime

End Class
