Imports System.Math
Imports ZctSOT.Datos.ClassGen
Imports ZctSOT.Datos

Public Class ZctControlBusqueda
    Private _SPName As String
    Private _SpVariables As String
    Private _Tabla As String
    Private _TipoDato As Tipo_Dato = Tipo_Dato.NUMERICO
    Private _Validar As Boolean = True
    Private _SqlBusqueda As String


    Public Shadows Event lostfocus(ByVal sender As Object, ByVal e As System.EventArgs)

    Public Property Validar() As Boolean
        Get
            Return _Validar
        End Get
        Set(ByVal value As Boolean)
            _Validar = value
        End Set
    End Property

    Public Property Tabla() As String
        Get
            Return _Tabla
        End Get
        Set(ByVal value As String)
            _Tabla = value
        End Set
    End Property

    Public Property Tipo_Dato() As Tipo_Dato
        Get
            Return _TipoDato
        End Get
        Set(ByVal value As Tipo_Dato)
            _TipoDato = value
        End Set
    End Property

    Public Property TipoDato() As Tipo_Dato
        Get
            Return ZctSOTTextBox1.TipoDato
        End Get
        Set(ByVal value As Tipo_Dato)
            ZctSOTTextBox1.TipoDato = value
        End Set
    End Property

    Public Property SPName() As String
        Get
            Return _SPName
        End Get
        Set(ByVal value As String)
            _SPName = value
        End Set
    End Property

    Public Property SpVariables() As String
        Get
            Return _SpVariables
        End Get
        Set(ByVal value As String)
            _SpVariables = value
        End Set
    End Property


    Private _SPParametros As String

    Public Property SPParametros() As String
        Get
            Return _SPParametros
        End Get
        Set(ByVal value As String)
            _SPParametros = value
        End Set
    End Property

    Public Property Nombre() As String
        Get
            Return ZctSOTLabel1.Text
        End Get
        Set(ByVal value As String)
            ZctSOTLabel1.Text = value
        End Set
    End Property

    Public Overrides Property Text() As String
        Get
            Return ZctSOTTextBox1.Text
        End Get
        Set(ByVal value As String)
            ZctSOTTextBox1.Text = value
        End Set
    End Property

    Public Property Descripcion() As String
        Get
            Return ZctSOTLabelDesc1.Text
        End Get
        Set(ByVal value As String)
            ZctSOTLabelDesc1.Text = value
        End Set
    End Property

    Public Property TextWith() As Integer
        Get
            Return ZctSOTTextBox1.Width
        End Get
        Set(ByVal value As Integer)
            ZctSOTTextBox1.Width = value
        End Set
    End Property

    Public Property SqlBusqueda() As String
        Get
            Return _SqlBusqueda
        End Get
        Set(ByVal value As String)
            _SqlBusqueda = value
        End Set
    End Property

    Private Sub ZctSOTTextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ZctSOTTextBox1.KeyDown
        Try
            If e.KeyCode = Keys.F3 Then
                Dim fBusqueda As New ZctBusqueda
                fBusqueda.sSPName = _SPName
                fBusqueda.sSpVariables = _SpVariables
                If _SPParametros <> "" Then
                    fBusqueda.sSPParametros = _SPParametros
                End If
                fBusqueda.ShowDialog(Me)
                Me.ZctSOTTextBox1.Text = fBusqueda.iValor
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try
    End Sub

    Private Sub ZctSOTTextBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTTextBox1.LostFocus

        pCargaDescripcion()
        RaiseEvent lostfocus(sender, e)
    End Sub

    Private Sub ZctSOTTextBox1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTTextBox1.Resize

        ZctSOTLabelDesc1.Left = ZctSOTTextBox1.Width + ZctSOTTextBox1.Left + 5
        ZctSOTLabelDesc1.Width = Abs(Me.Width - (ZctSOTLabelDesc1.Left + 5))
    End Sub

    Private Sub ZctSOTLabel1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTLabel1.Resize

        ZctSOTTextBox1.Left = ZctSOTLabel1.Width + ZctSOTLabel1.Left + 5
        ZctSOTLabelDesc1.Left = ZctSOTTextBox1.Width + ZctSOTTextBox1.Left + 5
        ZctSOTLabelDesc1.Width = Abs(Me.Width - (ZctSOTLabelDesc1.Left + 5))
    End Sub

    Private Sub ZctControlBusqueda_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        ZctSOTLabelDesc1.Width = Abs(Me.Width - (ZctSOTLabelDesc1.Left + 5))
    End Sub

    Public Sub pCargaDescripcion()
        Try
            Dim ZctBusCon As New Datos.ZctDataBase
            ZctSOTTextBox1.Text = ZctSOTTextBox1.Text.Trim.Replace("'", "-")
            If _Tabla <> "" And ZctSOTTextBox1.Text <> "" And ZctSOTTextBox1.Text <> "0" Then
                'Procedimietno de busqueda

                Select Case _TipoDato
                    Case Tipo_Dato.ALFANUMERICO
                        ZctBusCon.IniciaProcedimiento("SP_ZctBusqueda_Alfa")
                        ZctBusCon.AddParameterSP("@Busqueda", ZctSOTTextBox1.Text, SqlDbType.VarChar)
                    Case Tipo_Dato.NUMERICO
                        If Not IsNumeric(ZctSOTTextBox1.Text) Then Throw New ZctReglaNegocioEx("Se esperaba un n�mero.")
                        ZctBusCon.IniciaProcedimiento("SP_ZctBusqueda")
                        ZctBusCon.AddParameterSP("@Busqueda", ZctSOTTextBox1.Text, SqlDbType.Int)
                End Select

                ZctBusCon.AddParameterSP("@Tabla", _Tabla, SqlDbType.VarChar)

                'Incia el adaptador
                ZctBusCon.InciaDataAdapter()
                'Obtiene los datos de la consulta generica
                Dim DtTemp As DataTable = ZctBusCon.GetReaderSP.Tables("DATOS")
                If Not (DtTemp Is Nothing) Then
                    If DtTemp.Rows.Count > 0 Then
                        ZctSOTLabelDesc1.DataBindings.Clear()
                        ZctSOTLabelDesc1.DataBindings.Add("Text", DtTemp, "Descripcion")
                        DtTemp.Dispose()
                    Else
                        If _Validar = True Then
                            MsgBox("El dato no es correcto, verifique por favor.", MsgBoxStyle.Information)
                            ZctSOTLabelDesc1.DataBindings.Clear()
                            ZctSOTLabelDesc1.Text = ""
                            ZctSOTTextBox1.Text = ""
                        End If
                    End If

                End If
            Else
                ZctSOTLabelDesc1.DataBindings.Clear()
                ZctSOTLabelDesc1.Text = ""
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            ZctSOTLabelDesc1.DataBindings.Clear()
            ZctSOTLabelDesc1.Text = ""
            ZctSOTTextBox1.Text = ""

        End Try
    End Sub


    Public Overloads Property Tag() As Object
        Get
            Return ZctSOTTextBox1.Tag
        End Get
        Set(ByVal value As Object)
            ZctSOTTextBox1.Tag = value
        End Set
    End Property


    Private Sub ZctSOTTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZctSOTTextBox1.TextChanged

    End Sub
End Class
