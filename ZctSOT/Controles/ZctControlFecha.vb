Imports System.Math
Imports ZctSOT.Datos.ClassGen

Public Class ZctControlFecha
    Private _SqlBusqueda As String
    Private _SPName As String
    Private _SpVariables As String
    Private _Tabla As String

    Public Shadows Event lostfocus(ByVal sender As Object, ByVal e As System.EventArgs)

    Public Property Nombre() As String
        Get
            Return ZctSOTLabel1.Text
        End Get
        Set(ByVal value As String)
            ZctSOTLabel1.Text = value
        End Set
    End Property

    Public Property Value() As DateTime
        Get
            Return CType(Format(ZctSotDate1.Value, "d") & " " & Format(ZctSotTime1.Value, "T"), DateTime)

        End Get
        Set(ByVal value As DateTime)
            ZctSotDate1.Value = value
            ZctSotTime1.Value = value
        End Set
    End Property

    Public Property DateWith() As Integer
        Get
            Return ZctSotDate1.Width
        End Get
        Set(ByVal value As Integer)
            ZctSotDate1.Width = value
        End Set
    End Property

    Private Sub ZctSOTLabel1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSOTLabel1.Resize

        ZctSotDate1.Left = ZctSOTLabel1.Width + ZctSOTLabel1.Left + 5
        ZctSotTime1.Left = ZctSotDate1.Width + ZctSotDate1.Left + 5
        Me.Width = ZctSOTLabel1.Width + ZctSotDate1.Width + ZctSotTime1.Width + 15
    End Sub

    Private Sub ZctControlBusqueda_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        'ZctSOTLabel1.Width = Abs(Me.Width) '- (ZctSotTime1.Left + 5) - (ZctSotDate1.Left + 5))
        ZctSotTime1.Left = Abs(Me.Width - 5 - ZctSotTime1.Width)
        ZctSotDate1.Left = Abs(ZctSotTime1.Left - 5 - ZctSotDate1.Width)
        ZctSOTLabel1.Left = Abs(ZctSotDate1.Left - 5 - ZctSOTLabel1.Width)

    End Sub

    Private Sub ZctSotDate1_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles ZctSotDate1.Resize
        ZctSotTime1.Left = ZctSotDate1.Width + ZctSotDate1.Left + 5
        ZctSotTime1.Width = Abs(Me.Width - (ZctSotTime1.Left + 5))
    End Sub

    Private Sub ZctControlFecha_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ClearValue()
    End Sub

    Public Sub ClearValue()
        ZctSotDate1.Value = Now
        ZctSotTime1.Value = Now
    End Sub

End Class

