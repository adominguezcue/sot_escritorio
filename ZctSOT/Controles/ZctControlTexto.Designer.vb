<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctControlTexto
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ZctSOTTextBox1 = New ZctSOT.ZctSOTTextBox
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel
        Me.ZctSOTToolTipInt = New ZctSOT.ZctSOTToolTip
        Me.SuspendLayout()
        '
        'ZctSOTTextBox1
        '
        Me.ZctSOTTextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.ZctSOTTextBox1.Location = New System.Drawing.Point(86, 3)
        Me.ZctSOTTextBox1.Name = "ZctSOTTextBox1"
        Me.ZctSOTTextBox1.Size = New System.Drawing.Size(111, 20)
        Me.ZctSOTTextBox1.TabIndex = 3
        Me.ZctSOTTextBox1.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(3, 6)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(77, 13)
        Me.ZctSOTLabel1.TabIndex = 2
        Me.ZctSOTLabel1.Text = "ZctSOTLabel1"
        Me.ZctSOTLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ZctSOTToolTipInt
        '
        Me.ZctSOTToolTipInt.AutoPopDelay = 5000
        Me.ZctSOTToolTipInt.InitialDelay = 50
        Me.ZctSOTToolTipInt.ReshowDelay = 100
        Me.ZctSOTToolTipInt.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ZctSOTToolTipInt.ToolTipTitle = "Ayuda"
        '
        'ZctControlTexto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.ZctSOTTextBox1)
        Me.Controls.Add(Me.ZctSOTLabel1)
        Me.Name = "ZctControlTexto"
        Me.Size = New System.Drawing.Size(200, 28)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ZctSOTTextBox1 As ZctSOT.ZctSOTTextBox
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSOTToolTipInt As ZctSOT.ZctSOTToolTip

End Class
