Public Class ZctGroupControls
    Inherits ZctSOT.ZctSOTGroupBox

    Public Sub AddItem(ByVal cControl As Control)

        Me.SuspendLayout()
        Me.Controls.Add(cControl)
        'cControl.Location = New System.Drawing.Point(10, _Item + 10)
        cControl.Top = Me.Controls.Count * 40
        cControl.Left = 0
        cControl.TabIndex = Me.Controls.Count
        cControl.Visible = True
        Me.ResumeLayout(False)

    End Sub

    Public Function Valor(ByVal Nombre As String) As String
        Return Me.Controls.Item(Nombre).Text
    End Function

 
End Class
