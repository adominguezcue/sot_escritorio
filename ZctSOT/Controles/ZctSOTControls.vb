Imports System.Windows.Forms
Imports ZctSOT.Datos.ClassGen
Imports DevExpress.XtraBars.Ribbon

Public Class ZctSOTToolTip
    Inherits ToolTip
End Class

Public Class ZctSOTLabel
    Inherits Label

End Class

Public Class ZctSOTTextBox
    Inherits TextBox
   
    Dim _TipoDato As Tipo_Dato = Tipo_Dato.ALFANUMERICO

    Public Property TipoDato() As Tipo_Dato
        Get
            Return _TipoDato

        End Get
        Set(ByVal value As Tipo_Dato)
            _TipoDato = value
            If value = Tipo_Dato.ALFANUMERICO Then
                Me.Text = ""
            Else
                Me.Text = 0
            End If
        End Set
    End Property


    Protected Overrides Sub OnLostFocus(ByVal e As System.EventArgs)
        If _TipoDato = Tipo_Dato.NUMERICO Then
            If Not IsNumeric(MyBase.Text) Then
                MyBase.Text = 0
            End If
        ElseIf _TipoDato = Tipo_Dato.ALFANUMERICO Then
            If IsNumeric(MyBase.Text) Then
                '     MyBase.Text = ""
            End If
        End If
        MyBase.OnLostFocus(e)
    End Sub

    Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set(ByVal value As String)
            MyBase.Text = value
        End Set
    End Property

    Protected Overrides Sub OnKeyDown(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.OnKeyDown(e)
        If e.KeyCode = Keys.F1 Then
            If Me.Tag <> "" Then
                Dim fAyuda As New ZctFrmAyuda
                fAyuda.Texto = Me.Tag
                fAyuda.ShowDialog(Me)
            End If
        End If

    End Sub

    Protected Overrides Sub OnKeyPress(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Me.Multiline = False Then
            If e.KeyChar = ChrW(Keys.Enter) Then
                e.Handled = True
                SendKeys.Send("{TAB}")
            Else
                MyBase.OnKeyPress(e)
            End If
        Else
            MyBase.OnKeyPress(e)
        End If

    End Sub

End Class


Public Class ZctSOTLabelDesc
    Inherits Label

    Public Sub New()
        Me.AutoSize = False
        Me.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
        Me.BackColor = Color.LightBlue
    End Sub

End Class

Public Class ZctSOTButton
    Inherits Button

End Class

Public Class ZctSOTGroupBox
    Inherits GroupBox

End Class

Public Class ZctSOTForm
    Inherits RibbonForm
    Sub New()
        Me.WindowState = FormWindowState.Maximized
    End Sub

End Class

Public Class ZctSOTModalForm
    Inherits RibbonForm
    Sub New()

    End Sub

End Class

Public Class ZctSOTComboBox
    Inherits ComboBox
    Protected Overrides Sub OnKeyPress(ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            SendKeys.Send("{TAB}")
        Else
            MyBase.OnKeyPress(e)
        End If
    End Sub

    Protected Overrides Sub OnKeyDown(ByVal e As System.Windows.Forms.KeyEventArgs)
        MyBase.OnKeyDown(e)
        If e.KeyCode = Keys.F1 Then
            If Me.Tag <> "" Then
                Dim fAyuda As New ZctFrmAyuda
                fAyuda.Texto = Me.Tag
                fAyuda.ShowDialog(Me)
            End If
        End If

    End Sub
End Class

Public Class ZctSotGrid
    Inherits DataGridView

    Public Sub SetColumn(ByVal Nombre As String, ByVal Propiedad As String, ByVal Header As String, Optional ByVal visible As Boolean = True, Optional ByVal format As String = "", Optional ByVal size As Integer = 0, Optional ByVal Editar As Boolean = True)
        Dim col As New DataGridViewColumn
        col.Name = Nombre
        col.DataPropertyName = Propiedad
        col.HeaderText = Header
        col.Visible = visible
        col.DefaultCellStyle.Format = format
        col.CellTemplate = New DataGridViewTextBoxCell
        col.ReadOnly = Not Editar
        If size > 0 Then col.Width = size
        Me.Columns.Add(col)

    End Sub

End Class

Public Class ZctSotTime
    Inherits DateTimePicker

    Public Sub New()
        Me.Format = DateTimePickerFormat.Time
        Me.ShowUpDown = True
    End Sub

End Class

Public Class ZctSotDate
    Inherits DateTimePicker

    Public Sub New()
        Me.Format = DateTimePickerFormat.Short
    End Sub
End Class