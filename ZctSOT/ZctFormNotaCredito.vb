﻿Imports ZctSOT.Datos
Imports SOTControladores.Controladores

Public Class ZctFormNotaCredito
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
    Private folio As New zctFolios("NT")
    Private esNuevo As Boolean = False

    Private Sub imprimir()
        If TextBox1.Text = "0" Or TextBox1.Text = "" Then Exit Sub
        Dim report As New PkVisorRpt
        report.MdiParent = Me.MdiParent
        Try
            report.sSQLV = "{ZctNotasCdt.Cod_Cdt} = " & CType(TextBox1.Text, Integer)

            report.sRpt = "C:\Reportes\ZctNtsCdt.rpt"
            report.Show()
        Catch ex As Exception
            MsgBox(ex.ToString, MsgBoxStyle.Critical, "Ha ocurrido un error")
        End Try

    End Sub

    Private Function getNext(ByRef actual As String, ByVal aumento As Integer) As Integer
        Dim num As Integer = 1
        Try
            num = CInt(actual)
            If num + aumento <= 1 Then
                Me.Button1.Enabled = False
                Me.Button2.Enabled = True
                num = 1
                Me.esNuevo = False

            ElseIf (num + aumento >= folio.Consecutivo) Then
                Me.Button2.Enabled = False
                Me.Button1.Enabled = True
                num = folio.Consecutivo
                Me.esNuevo = True

            Else
                Me.Button1.Enabled = True
                Me.Button2.Enabled = True
                num = num + aumento
                Me.esNuevo = False
            End If

            Me.Button5.Enabled = esNuevo
            Me.ZctSotGrid1.Columns("PorAplicar").ReadOnly = Not esNuevo
            Me.TextBox4.ReadOnly = Not esNuevo

            Return num
        Catch ex As Exception
            MsgBox("Error necesito un número")
            Return num
        Finally
            cargarFolio(num)
        End Try
    End Function

    Private Sub cargarDatos(Optional ByVal EncOt As Integer = 0, Optional ByVal cod_cdt As Integer = 0)

        Dim ctrl As New NotasCredito.Controler()
        Dim codigo As Integer
        Dim codCdt As Integer
        Try
            If EncOt = 0 Then
                codigo = CInt(TextBox2.Text)
            Else
                codigo = EncOt
            End If

            If cod_cdt = 0 Then
                codCdt = CInt(Me.TextBox1.Text)
            Else
                codCdt = cod_cdt
            End If

            TextBox3.Text = ctrl.ObtenerNomCliente(codigo)
            Me.ZctSotGrid1.AutoGenerateColumns = False
            Me.ZctSotGrid1.DataSource = ctrl.obtenerArticulos(codigo, codCdt, Me.esNuevo)
        Catch fex As FormatException
            MsgBox("Error eso no es un número")
        Catch ice As InvalidCastException
            MsgBox("Error eso no es un número")
        Catch sqlEx As SqlClient.SqlException
            MsgBox("Error en la base de datos: " & sqlEx.Message)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub validar(ByVal index As Integer)
        Dim anterior As Integer = 0
        Try
            Dim total As Integer = CInt(Me.ZctSotGrid1.Rows(index).Cells("Ctd_Art").Value)
            If Not IsDBNull(Me.ZctSotGrid1.Rows(index).Cells("Anterior").Value) Then
                anterior = CInt(Me.ZctSotGrid1.Rows(index).Cells("Anterior").Value)
            End If
            Dim porAplicar As Integer = CInt(Me.ZctSotGrid1.Rows(index).Cells("PorAplicar").Value)

            'If porAplicar < 0 Then
            'MsgBox("Error, no puede aplicar una cantidad negativa", MsgBoxStyle.Exclamation)
            'Me.ZctSotGrid1.Rows(index).Cells("PorAplicar").Value = 0
            If porAplicar + anterior > total Then
                MsgBox("Error, no se puede exceder la cantidad todal de artículos", MsgBoxStyle.Exclamation)
                Me.ZctSotGrid1.Rows(index).Cells("PorAplicar").Value = 0
            End If
        Catch ex As Exception
            MsgBox("Error, la cantidad por aplicar necesita ser un número", MsgBoxStyle.Exclamation)
            Me.ZctSotGrid1.Rows(index).Cells("PorAplicar").Value = 0
        End Try
    End Sub

    Private Sub guardarDatos()
        Dim ctrl As New NotasCredito.Controler()
        Try
            If _Permisos.edita And _Permisos.agrega = False And _Permisos.elimina = False Then
                MsgBox("No tiene permiso para guardar en esta ventana", MsgBoxStyle.Critical, "Atencion")
                Return
            End If
            ctrl.grabar(Me.ZctSotGrid1.DataSource, Me.TextBox2.Text, Me.TextBox1.Text, TextBox4.Text)
            MsgBox("Datos guardados correctamente", MsgBoxStyle.Information)
            Me.TextBox1.Text = getNext(Me.folio.Consecutivo.ToString, 1)
            cargarFolio()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub cargarFolio()

        Try
            cargarFolio(CInt(Me.TextBox1.Text))
        Catch ex As FormatException
            MsgBox("Error, el folio necesita ser un número")
            Me.TextBox1.Text = 0
        Catch ex As InvalidCastException
            MsgBox("Error, el folio necesita ser un número")
            Me.TextBox1.Text = 0
        End Try

    End Sub

    Private Sub cargarFolio(ByRef nfolio As Integer)
        limpiar()
        Dim ctrl As New NotasCredito.Controler()
        Dim datos As DataTable

        datos = ctrl.obtenerNota(nfolio)
        If datos.Rows.Count > 0 Then
            Me.TextBox4.Text = datos.Rows(0).Item("Observaciones")
            Me.TextBox2.Text = datos.Rows(0).Item("Cod_EncOT")
            Me.cargarDatos(CInt(datos.Rows(0).Item("Cod_EncOT")), nfolio)
            Me.Button3.Enabled = False
        End If


    End Sub

    Public Sub limpiar()
        Me.TextBox2.Text = ""
        Me.TextBox3.Text = ""
        Me.TextBox4.Text = ""
        Me.Button3.Enabled = True
        Me.ZctSotGrid1.DataSource = Nothing
    End Sub

#Region "Eventos"
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.TextBox1.Text = Me.getNext(Me.TextBox1.Text, -1)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.TextBox1.Text = Me.getNext(Me.TextBox1.Text, 1)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.cargarDatos()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        guardarDatos()
    End Sub

    Private Sub ZctSotGrid1_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ZctSotGrid1.CellEndEdit
        If e.ColumnIndex = 4 Then
            validar(e.RowIndex)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.TextBox1.Text = getNext(Me.folio.Consecutivo.ToString, 1)
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyCode = Keys.Enter Then
            cargarFolio()
        End If
    End Sub

    Private Sub ZctFormNotaCredito_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        With _Permisos
            ZctSotGrid1.AllowUserToAddRows = .agrega
            ZctSotGrid1.AllowUserToDeleteRows = .elimina
            ZctSotGrid1.ReadOnly = Not (.edita Or .agrega)
            If .edita = False And .agrega = False And .elimina = False Then
                Button5.Enabled = False
            Else
                Button5.Enabled = True
            End If
        End With
        Me.TextBox1.Text = getNext(Me.folio.Consecutivo.ToString, 1)
    End Sub
#End Region

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        imprimir()
    End Sub
End Class