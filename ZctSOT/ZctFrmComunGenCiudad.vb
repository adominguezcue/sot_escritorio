﻿Public Class ZctFrmComunGenLinea
    Private ListDpto As New Datos.Clases.Catalogos.listaDepartamentos
    Private ProcesaDpto As New Datos.DAO.ClProcesaCatalogos
    Private Sub ZctFrmComunGenLinea_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Protected Overrides Sub CargaDatos()
        MyBase.CargaDatos()

        If DGBusqueda.Columns.Contains("Cod_Dpto") Then
            ProcesaDpto.CargaDatos(ListDpto)
            Dim cCol As New DataGridViewComboBoxColumn
            cCol.Name = "Departamento"
            cCol.DataSource = ListDpto
            cCol.HeaderText = "Departamento"
            cCol.DataPropertyName = "Cod_Dpto"
            cCol.DisplayMember = "Departamento"
            cCol.ValueMember = "Codigo"

            DGBusqueda.Columns.Add(cCol)
            DGBusqueda.Columns("Cod_Dpto").Visible = False
            CalculaWidth()
            DGBusqueda.AutoResizeColumn(cCol.Index)

        End If
    End Sub
End Class
