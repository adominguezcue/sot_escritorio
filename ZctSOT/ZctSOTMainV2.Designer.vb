﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctSOTMainV2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.FileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArtículosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecetasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntradasYSalidasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.DepartamentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LíneasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AlmacenesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MarcasMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ClienteStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.EstadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CiudadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesTrabToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotasDeCréditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RequisicionesWebToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrdenesCompToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompraSugeridaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.INVENTARIOACTUALToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTEDECOMPRASToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.INVENTARIOACTUALNEGATIVOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServicioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GerencialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CATÁLOGODEARTÍCULOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTEDEVENTASToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTEDEESTATUSDEVENTASToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RespaldoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ParametrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ActualizaciónWebToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalendarioFiscalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventariosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComparativoDeInventariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntercambioEntreAlmacenesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeZonasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WindowsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.CascadeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileVerticalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileHorizontalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArrangeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTipPendientes = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.WebTimer = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker = New System.ComponentModel.BackgroundWorker()
        Me.MenuStrip.SuspendLayout
        Me.StatusStrip.SuspendLayout
        Me.SuspendLayout
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileMenu, Me.EditMenu, Me.ComprasToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.SistemaToolStripMenuItem, Me.InventariosToolStripMenuItem1, Me.WindowsMenu, Me.SalirToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.MdiWindowListItem = Me.WindowsMenu
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(798, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'FileMenu
        '
        Me.FileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArtículosToolStripMenuItem, Me.RecetasToolStripMenuItem, Me.EntradasYSalidasToolStripMenuItem, Me.ToolStripSeparator4, Me.DepartamentosToolStripMenuItem, Me.LíneasToolStripMenuItem, Me.AlmacenesToolStripMenuItem, Me.MarcasMenuItem, Me.ToolStripSeparator1, Me.ClienteStripMenuItem, Me.ProveedoresToolStripMenuItem, Me.ToolStripSeparator3, Me.EstadosToolStripMenuItem, Me.CiudadesToolStripMenuItem})
        Me.FileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.FileMenu.Name = "FileMenu"
        Me.FileMenu.Size = New System.Drawing.Size(72, 20)
        Me.FileMenu.Tag = "cat"
        Me.FileMenu.Text = "&Catálogos"
        '
        'ArtículosToolStripMenuItem
        '
        Me.ArtículosToolStripMenuItem.Name = "ArtículosToolStripMenuItem"
        Me.ArtículosToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ArtículosToolStripMenuItem.Tag = "Catálogo de Artículos"
        Me.ArtículosToolStripMenuItem.Text = "Artículos"
        '
        'RecetasToolStripMenuItem
        '
        Me.RecetasToolStripMenuItem.Name = "RecetasToolStripMenuItem"
        Me.RecetasToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.RecetasToolStripMenuItem.Text = "Recetas"
        '
        'EntradasYSalidasToolStripMenuItem
        '
        Me.EntradasYSalidasToolStripMenuItem.Name = "EntradasYSalidasToolStripMenuItem"
        Me.EntradasYSalidasToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.EntradasYSalidasToolStripMenuItem.Text = "Entradas y salidas"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(163, 6)
        '
        'DepartamentosToolStripMenuItem
        '
        Me.DepartamentosToolStripMenuItem.Name = "DepartamentosToolStripMenuItem"
        Me.DepartamentosToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.DepartamentosToolStripMenuItem.Tag = "Catálogo de Categorias"
        Me.DepartamentosToolStripMenuItem.Text = "Categorias"
        '
        'LíneasToolStripMenuItem
        '
        Me.LíneasToolStripMenuItem.Name = "LíneasToolStripMenuItem"
        Me.LíneasToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.LíneasToolStripMenuItem.Tag = "Catálogo de Subcategorias"
        Me.LíneasToolStripMenuItem.Text = "Sub categorias"
        '
        'AlmacenesToolStripMenuItem
        '
        Me.AlmacenesToolStripMenuItem.Name = "AlmacenesToolStripMenuItem"
        Me.AlmacenesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.AlmacenesToolStripMenuItem.Tag = "Catálogo de Almacenes"
        Me.AlmacenesToolStripMenuItem.Text = "Almacenes"
        '
        'MarcasMenuItem
        '
        Me.MarcasMenuItem.Name = "MarcasMenuItem"
        Me.MarcasMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.MarcasMenuItem.Tag = "Catálogo de Marcas"
        Me.MarcasMenuItem.Text = "Marcas"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(163, 6)
        '
        'ClienteStripMenuItem
        '
        Me.ClienteStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.ClienteStripMenuItem.Name = "ClienteStripMenuItem"
        Me.ClienteStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N),System.Windows.Forms.Keys)
        Me.ClienteStripMenuItem.ShowShortcutKeys = false
        Me.ClienteStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ClienteStripMenuItem.Tag = "Catálogo de Clientes"
        Me.ClienteStripMenuItem.Text = "Clientes"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O),System.Windows.Forms.Keys)
        Me.ProveedoresToolStripMenuItem.ShowShortcutKeys = false
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ProveedoresToolStripMenuItem.Tag = "Catálogo de Proveedores"
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(163, 6)
        '
        'EstadosToolStripMenuItem
        '
        Me.EstadosToolStripMenuItem.Name = "EstadosToolStripMenuItem"
        Me.EstadosToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.EstadosToolStripMenuItem.Tag = "Catálogo de Estados"
        Me.EstadosToolStripMenuItem.Text = "Estados"
        '
        'CiudadesToolStripMenuItem
        '
        Me.CiudadesToolStripMenuItem.Name = "CiudadesToolStripMenuItem"
        Me.CiudadesToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.CiudadesToolStripMenuItem.Tag = "Catálogo de Ciudades"
        Me.CiudadesToolStripMenuItem.Text = "Ciudades"
        '
        'EditMenu
        '
        Me.EditMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OrdenesTrabToolStripMenuItem, Me.NotasDeCréditoToolStripMenuItem, Me.RequisicionesWebToolStripMenuItem})
        Me.EditMenu.Name = "EditMenu"
        Me.EditMenu.Size = New System.Drawing.Size(53, 20)
        Me.EditMenu.Text = "&Ventas"
        '
        'OrdenesTrabToolStripMenuItem
        '
        Me.OrdenesTrabToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.OrdenesTrabToolStripMenuItem.Name = "OrdenesTrabToolStripMenuItem"
        Me.OrdenesTrabToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z),System.Windows.Forms.Keys)
        Me.OrdenesTrabToolStripMenuItem.ShowShortcutKeys = false
        Me.OrdenesTrabToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.OrdenesTrabToolStripMenuItem.Tag = "Ordenes de Trabajo"
        Me.OrdenesTrabToolStripMenuItem.Text = "&Pedidos"
        '
        'NotasDeCréditoToolStripMenuItem
        '
        Me.NotasDeCréditoToolStripMenuItem.Name = "NotasDeCréditoToolStripMenuItem"
        Me.NotasDeCréditoToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.NotasDeCréditoToolStripMenuItem.Tag = "Nota de crédito"
        Me.NotasDeCréditoToolStripMenuItem.Text = "Notas de crédito"
        '
        'RequisicionesWebToolStripMenuItem
        '
        Me.RequisicionesWebToolStripMenuItem.Name = "RequisicionesWebToolStripMenuItem"
        Me.RequisicionesWebToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.RequisicionesWebToolStripMenuItem.Text = "Requisiciones Web"
        '
        'ComprasToolStripMenuItem
        '
        Me.ComprasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OrdenesCompToolStripMenuItem, Me.CompraSugeridaToolStripMenuItem})
        Me.ComprasToolStripMenuItem.Name = "ComprasToolStripMenuItem"
        Me.ComprasToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.ComprasToolStripMenuItem.Text = "Compras"
        '
        'OrdenesCompToolStripMenuItem
        '
        Me.OrdenesCompToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.OrdenesCompToolStripMenuItem.Name = "OrdenesCompToolStripMenuItem"
        Me.OrdenesCompToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y),System.Windows.Forms.Keys)
        Me.OrdenesCompToolStripMenuItem.ShowShortcutKeys = false
        Me.OrdenesCompToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.OrdenesCompToolStripMenuItem.Tag = "Ordenes de Compra"
        Me.OrdenesCompToolStripMenuItem.Text = "Ordenes de Compra"
        '
        'CompraSugeridaToolStripMenuItem
        '
        Me.CompraSugeridaToolStripMenuItem.Name = "CompraSugeridaToolStripMenuItem"
        Me.CompraSugeridaToolStripMenuItem.Size = New System.Drawing.Size(215, 22)
        Me.CompraSugeridaToolStripMenuItem.Tag = "Compra sugerida"
        Me.CompraSugeridaToolStripMenuItem.Text = "Orden de compra sugerida"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InventariosToolStripMenuItem, Me.ServicioToolStripMenuItem, Me.GerencialesToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(65, 20)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'InventariosToolStripMenuItem
        '
        Me.InventariosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.INVENTARIOACTUALToolStripMenuItem, Me.REPORTEDECOMPRASToolStripMenuItem, Me.INVENTARIOACTUALNEGATIVOSToolStripMenuItem, Me.REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem})
        Me.InventariosToolStripMenuItem.Name = "InventariosToolStripMenuItem"
        Me.InventariosToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.InventariosToolStripMenuItem.Tag = "Compra sugerida"
        Me.InventariosToolStripMenuItem.Text = "Inventarios"
        '
        'INVENTARIOACTUALToolStripMenuItem
        '
        Me.INVENTARIOACTUALToolStripMenuItem.Name = "INVENTARIOACTUALToolStripMenuItem"
        Me.INVENTARIOACTUALToolStripMenuItem.Size = New System.Drawing.Size(349, 22)
        Me.INVENTARIOACTUALToolStripMenuItem.Text = "INVENTARIO ACTUAL"
        '
        'REPORTEDECOMPRASToolStripMenuItem
        '
        Me.REPORTEDECOMPRASToolStripMenuItem.Name = "REPORTEDECOMPRASToolStripMenuItem"
        Me.REPORTEDECOMPRASToolStripMenuItem.Size = New System.Drawing.Size(349, 22)
        Me.REPORTEDECOMPRASToolStripMenuItem.Text = "REPORTE DE COMPRAS"
        '
        'INVENTARIOACTUALNEGATIVOSToolStripMenuItem
        '
        Me.INVENTARIOACTUALNEGATIVOSToolStripMenuItem.Name = "INVENTARIOACTUALNEGATIVOSToolStripMenuItem"
        Me.INVENTARIOACTUALNEGATIVOSToolStripMenuItem.Size = New System.Drawing.Size(349, 22)
        Me.INVENTARIOACTUALNEGATIVOSToolStripMenuItem.Text = "INVENTARIO ACTUAL NEGATIVOS"
        '
        'REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem
        '
        Me.REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem.Name = "REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENES"& _ 
    "ToolStripMenuItem"
        Me.REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem.Size = New System.Drawing.Size(349, 22)
        Me.REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem.Text = "REPORTE DE EXISTENCIAS TOTALES EN ALMACENES"
        '
        'ServicioToolStripMenuItem
        '
        Me.ServicioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem})
        Me.ServicioToolStripMenuItem.Enabled = false
        Me.ServicioToolStripMenuItem.Name = "ServicioToolStripMenuItem"
        Me.ServicioToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ServicioToolStripMenuItem.Tag = "Compra sugerida"
        Me.ServicioToolStripMenuItem.Text = "Servicio"
        Me.ServicioToolStripMenuItem.Visible = false
        '
        'REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem
        '
        Me.REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem.Name = "REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem"
        Me.REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem.Size = New System.Drawing.Size(359, 22)
        Me.REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem.Text = "REPORTE DE VENTAS POR ARTICULO CONCENTRADO"
        '
        'GerencialesToolStripMenuItem
        '
        Me.GerencialesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CATÁLOGODEARTÍCULOSToolStripMenuItem, Me.REPORTEDEVENTASToolStripMenuItem, Me.REPORTEDEESTATUSDEVENTASToolStripMenuItem})
        Me.GerencialesToolStripMenuItem.Name = "GerencialesToolStripMenuItem"
        Me.GerencialesToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.GerencialesToolStripMenuItem.Tag = "Compra sugerida"
        Me.GerencialesToolStripMenuItem.Text = "Gerenciales"
        '
        'CATÁLOGODEARTÍCULOSToolStripMenuItem
        '
        Me.CATÁLOGODEARTÍCULOSToolStripMenuItem.Name = "CATÁLOGODEARTÍCULOSToolStripMenuItem"
        Me.CATÁLOGODEARTÍCULOSToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.CATÁLOGODEARTÍCULOSToolStripMenuItem.Text = "CATÁLOGO DE ARTÍCULOS"
        '
        'REPORTEDEVENTASToolStripMenuItem
        '
        Me.REPORTEDEVENTASToolStripMenuItem.Name = "REPORTEDEVENTASToolStripMenuItem"
        Me.REPORTEDEVENTASToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.REPORTEDEVENTASToolStripMenuItem.Text = "REPORTE DE VENTAS"
        '
        'REPORTEDEESTATUSDEVENTASToolStripMenuItem
        '
        Me.REPORTEDEESTATUSDEVENTASToolStripMenuItem.Name = "REPORTEDEESTATUSDEVENTASToolStripMenuItem"
        Me.REPORTEDEESTATUSDEVENTASToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.REPORTEDEESTATUSDEVENTASToolStripMenuItem.Text = "REPORTE DE ESTATUS DE VENTAS"
        '
        'SistemaToolStripMenuItem
        '
        Me.SistemaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RespaldoToolStripMenuItem, Me.ActualizaciónToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.ParametrosToolStripMenuItem, Me.ActualizaciónWebToolStripMenuItem, Me.CalendarioFiscalToolStripMenuItem})
        Me.SistemaToolStripMenuItem.Name = "SistemaToolStripMenuItem"
        Me.SistemaToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.SistemaToolStripMenuItem.Text = "Sistema"
        '
        'RespaldoToolStripMenuItem
        '
        Me.RespaldoToolStripMenuItem.Name = "RespaldoToolStripMenuItem"
        Me.RespaldoToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.RespaldoToolStripMenuItem.Tag = "Respaldo de la base de datos"
        Me.RespaldoToolStripMenuItem.Text = "Respaldo"
        '
        'ActualizaciónToolStripMenuItem
        '
        Me.ActualizaciónToolStripMenuItem.Name = "ActualizaciónToolStripMenuItem"
        Me.ActualizaciónToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.ActualizaciónToolStripMenuItem.Text = "Actualización"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.UsuariosToolStripMenuItem.Tag = "Alta de usuarios"
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'ParametrosToolStripMenuItem
        '
        Me.ParametrosToolStripMenuItem.Name = "ParametrosToolStripMenuItem"
        Me.ParametrosToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.ParametrosToolStripMenuItem.Tag = "Parametros Generales"
        Me.ParametrosToolStripMenuItem.Text = "Parametros"
        '
        'ActualizaciónWebToolStripMenuItem
        '
        Me.ActualizaciónWebToolStripMenuItem.Name = "ActualizaciónWebToolStripMenuItem"
        Me.ActualizaciónWebToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.ActualizaciónWebToolStripMenuItem.Text = "Actualización web"
        '
        'CalendarioFiscalToolStripMenuItem
        '
        Me.CalendarioFiscalToolStripMenuItem.Name = "CalendarioFiscalToolStripMenuItem"
        Me.CalendarioFiscalToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.CalendarioFiscalToolStripMenuItem.Text = "Calendario Fiscal"
        '
        'InventariosToolStripMenuItem1
        '
        Me.InventariosToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ComparativoDeInventariosToolStripMenuItem, Me.IntercambioEntreAlmacenesToolStripMenuItem, Me.ReporteDeZonasToolStripMenuItem})
        Me.InventariosToolStripMenuItem1.Name = "InventariosToolStripMenuItem1"
        Me.InventariosToolStripMenuItem1.Size = New System.Drawing.Size(77, 20)
        Me.InventariosToolStripMenuItem1.Text = "Inventarios"
        '
        'ComparativoDeInventariosToolStripMenuItem
        '
        Me.ComparativoDeInventariosToolStripMenuItem.Name = "ComparativoDeInventariosToolStripMenuItem"
        Me.ComparativoDeInventariosToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.ComparativoDeInventariosToolStripMenuItem.Tag = "Inventarios Físicos"
        Me.ComparativoDeInventariosToolStripMenuItem.Text = "Comparativo de Inventarios"
        '
        'IntercambioEntreAlmacenesToolStripMenuItem
        '
        Me.IntercambioEntreAlmacenesToolStripMenuItem.Name = "IntercambioEntreAlmacenesToolStripMenuItem"
        Me.IntercambioEntreAlmacenesToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.IntercambioEntreAlmacenesToolStripMenuItem.Tag = "Traspaso entre almacenes"
        Me.IntercambioEntreAlmacenesToolStripMenuItem.Text = "Intercambio entre almacenes"
        '
        'ReporteDeZonasToolStripMenuItem
        '
        Me.ReporteDeZonasToolStripMenuItem.Name = "ReporteDeZonasToolStripMenuItem"
        Me.ReporteDeZonasToolStripMenuItem.Size = New System.Drawing.Size(227, 22)
        Me.ReporteDeZonasToolStripMenuItem.Tag = "ZctArtViejos"
        Me.ReporteDeZonasToolStripMenuItem.Text = "Reporte de Zonas"
        '
        'WindowsMenu
        '
        Me.WindowsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CascadeToolStripMenuItem, Me.TileVerticalToolStripMenuItem, Me.TileHorizontalToolStripMenuItem, Me.CloseAllToolStripMenuItem, Me.ArrangeIconsToolStripMenuItem})
        Me.WindowsMenu.Name = "WindowsMenu"
        Me.WindowsMenu.Size = New System.Drawing.Size(61, 20)
        Me.WindowsMenu.Text = "V&entana"
        '
        'CascadeToolStripMenuItem
        '
        Me.CascadeToolStripMenuItem.Name = "CascadeToolStripMenuItem"
        Me.CascadeToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.CascadeToolStripMenuItem.Text = "&Cascada"
        '
        'TileVerticalToolStripMenuItem
        '
        Me.TileVerticalToolStripMenuItem.Name = "TileVerticalToolStripMenuItem"
        Me.TileVerticalToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.TileVerticalToolStripMenuItem.Text = "Mosaico &vertical"
        '
        'TileHorizontalToolStripMenuItem
        '
        Me.TileHorizontalToolStripMenuItem.Name = "TileHorizontalToolStripMenuItem"
        Me.TileHorizontalToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.TileHorizontalToolStripMenuItem.Text = "Mosaico &horizontal"
        '
        'CloseAllToolStripMenuItem
        '
        Me.CloseAllToolStripMenuItem.Name = "CloseAllToolStripMenuItem"
        Me.CloseAllToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.CloseAllToolStripMenuItem.Text = "C&errar todo"
        Me.CloseAllToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ArrangeIconsToolStripMenuItem
        '
        Me.ArrangeIconsToolStripMenuItem.Name = "ArrangeIconsToolStripMenuItem"
        Me.ArrangeIconsToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.ArrangeIconsToolStripMenuItem.Text = "&Organizar iconos"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel, Me.ToolStripStatusLabel3, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel4, Me.ToolTipPendientes})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 431)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(798, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(42, 17)
        Me.ToolStripStatusLabel.Text = "Estado"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel3.Text = "|"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(56, 17)
        Me.ToolStripStatusLabel2.Text = "F1 Ayuda"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(74, 17)
        Me.ToolStripStatusLabel1.Text = "F3 Búsqueda"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(10, 17)
        Me.ToolStripStatusLabel4.Text = "|"
        '
        'ToolTipPendientes
        '
        Me.ToolTipPendientes.ForeColor = System.Drawing.Color.DarkGreen
        Me.ToolTipPendientes.Name = "ToolTipPendientes"
        Me.ToolTipPendientes.Size = New System.Drawing.Size(10, 17)
        Me.ToolTipPendientes.Text = " "
        '
        'WebTimer
        '
        Me.WebTimer.Interval = 60000
        '
        'BackgroundWorker
        '
        Me.BackgroundWorker.WorkerReportsProgress = true
        Me.BackgroundWorker.WorkerSupportsCancellation = true
        '
        'ZctSOTMainV2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(798, 453)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.IsMdiContainer = true
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "ZctSOTMainV2"
        Me.ShowIcon = false
        Me.Text = "Sistema de Ordenes de Trabajo"
        Me.ToolTip.SetToolTip(Me, "Registros pendientes por sincrinizar a la web")
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(false)
        Me.MenuStrip.PerformLayout
        Me.StatusStrip.ResumeLayout(false)
        Me.StatusStrip.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents ArrangeIconsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WindowsMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CascadeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileVerticalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TileHorizontalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents MarcasMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClienteStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents EditMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArtículosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesTrabToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CiudadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InventariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents LíneasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepartamentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RespaldoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AlmacenesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InventariosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComparativoDeInventariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompraSugeridaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OrdenesCompToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ParametrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IntercambioEntreAlmacenesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GerencialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotasDeCréditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActualizaciónWebToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WebTimer As System.Windows.Forms.Timer
    Friend WithEvents CalendarioFiscalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTipPendientes As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RequisicionesWebToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackgroundWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents ReporteDeZonasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents INVENTARIOACTUALToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTEDECOMPRASToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents INVENTARIOACTUALNEGATIVOSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ServicioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTEDEVENTASPORARTICULOCONCENTRADOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CATÁLOGODEARTÍCULOSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTEDEVENTASToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTEDEESTATUSDEVENTASToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTEDEEXISTENCIASTOTALESENALMACENESTOTALDECOSTOSDEARTICULOSENTODOSLOSALMACENESToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecetasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntradasYSalidasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator

End Class
