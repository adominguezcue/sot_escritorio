﻿Imports System.Linq
Imports ZctSOT.Datos

Public Class ZctCatPar
    Private _par As New Clases.Sistema.ZctCatPar
    Private ProcesaCatalogos As New Datos.DAO.ClProcesaCatalogos
    Private bCambios As Boolean = False

    Private Sub ZctCatPar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            CargaDatos()

        Catch ex As Exception
            MsgBox("Ha ocurrido un error, favor de verificar con sistemas.")
        End Try
    End Sub

    Private Sub cmdGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGrabar.Click
        Try
            If TxtMinimoCaja.Text > TxtMaximoCaja.Text Then
                TxtMinimoCaja.Focus()
                Throw New Exception("El minimo de caja no puede ser mayor al maximo de caja")
            End If
            If _par.Iva_CatPar > 1 Then
                If MsgBox("¿Esta seguro que el valor del IVA es del " & _par.Iva_CatPar * 100 & "?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                    Exit Sub
                End If
            End If
            ProcesaCatalogos.ActualizarDatos(_par)
            ZctSOTMain.ActualizaParametros()
            MsgBox("Los parametros han sido guardado correctamente.", MsgBoxStyle.Information)
            If MsgBox("¿Desea salir del catálogo?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then Me.Close() : Exit Sub

        Catch ex As Exception
            MsgBox("Ha ocurrido un error, favor de verificar con sistemas " & ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Public Sub CargaDatos()
        If _par Is Nothing Then _par = New Clases.Sistema.ZctCatPar
        ProcesaCatalogos.CargaDatos(_par)
        ZctCatParBindingSource.DataSource = _par
        ZctCatParBindingSource.ResumeBinding()
        bCambios = False
    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If bCambios Then
            CargaDatos()
        Else
            Me.Close()
        End If
    End Sub

    Private Sub Modificaciones(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Iva_CatParTextBox.TextChanged
        bCambios = True
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Iva_CatParTextBox_Validating(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Iva_CatParTextBox.Validating
        If Not IsNumeric(Iva_CatParTextBox.Text) Then e.Cancel = True : Iva_CatParTextBox.Text = 0
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim FBD As New FolderBrowserDialog
        With FBD
            .Description = "Ruta de guardado imagenes articulos"
        End With
        If FBD.ShowDialog = Windows.Forms.DialogResult.OK Then
            TxtRIA.Text = FBD.SelectedPath
            _par.RutaImagenesArticulos = FBD.SelectedPath
        End If
    End Sub
End Class