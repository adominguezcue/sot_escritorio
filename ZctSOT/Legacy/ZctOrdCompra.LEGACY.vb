﻿'Partial Public Class ZctOrdCompra
'    Public Sub CargaCompra()
'        'Deshabilita el folio
'        'zctFolio.Enabled = False
'        txtFolio.Enabled = False

'        Dim drEncOT As DataTable
'        Dim sVariables As String
'        Dim sParametros As String
'        sVariables = "@Cod_OC;INT|@Cod_Prov;INT|@Fch_OC;SMALLDATETIME|@FchAplMov;SMALLDATETIME|@Fac_OC;VARCHAR|@FchFac_OC;SMALLDATETIME"
'        sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer) & "|" & Now.Date & "|" & DtAplicacion.Value & "|" & 0 & "|" & Date.Now
'        drEncOT = GetData(1, "SP_ZctEncOC", sVariables, sParametros)
'        If Not (drEncOT.Rows.Count = 0) Then
'            txtFolio.DataBindings.Clear()
'            txtFolio.DataBindings.Add("Text", drEncOT, "Cod_OC")

'            'txtFactura.DataBindings.Clear()
'            'txtFactura.DataBindings.Add("Text", drEncOT, "Fac_OC")

'            DtAplicacion.DataBindings.Clear()
'            DtAplicacion.DataBindings.Add("Value", drEncOT, "FchAplMov")

'            'DtFechaFact.DataBindings.Clear()
'            'DtFechaFact.DataBindings.Add("Value", drEncOT, "FchFac_OC")


'            txtProveedor.DataBindings.Clear()
'            txtProveedor.DataBindings.Add("Text", drEncOT, "Cod_Prov")
'            txtProveedor.pCargaDescripcion()

'            Lstatus.DataBindings.Clear()
'            Lstatus.DataBindings.Add("Text", drEncOT, "status")
'            'Datos del Grid
'            sVariables = "@Cod_OC;INT|@CodDet_Oc;INT|@Cod_Art;VARCHAR|@Ctd_Art;INT|@Cos_Art;DECIMAL|@CtdStdDet_OC;INT|@FchAplMov;SMALLDATETIME|@Cat_Alm;INT"
'            sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|0|0|0|0|0|" & DtAplicacion.Value & "|0"


'            Dim drDetOT As DataTable
'            Dim iRowD As Integer

'            drDetOT = GetData(1, "SP_ZctDetOC", sVariables, sParametros)

'            For iRowD = 0 To drDetOT.Rows.Count - 1
'                Dim inRow As New DataGridViewRow

'                inRow.CreateCells(DGridArticulos)
'                inRow.Cells(iColArt).Value = drDetOT.Rows(iRowD).Item("Cod_Art").ToString
'                inRow.Cells(iColDesc).Value = drDetOT.Rows(iRowD).Item("Desc_Art").ToString
'                inRow.Cells(iColSol).Value = drDetOT.Rows(iRowD).Item("Ctd_Art").ToString

'                'inRow.Cells(iColExist).Value = drDetOT.Rows(iRowD).Item("Exist_Art").ToString
'                inRow.Cells(iColCod).Value = drDetOT.Rows(iRowD).Item("CodDet_OC").ToString
'                inRow.Cells(iColSurt).Value = drDetOT.Rows(iRowD).Item("CtdStdDet_OC").ToString
'                inRow.Cells(iColPrecio).Value = drDetOT.Rows(iRowD).Item("Cos_Art").ToString

'                inRow.Cells(iColSubTotal).Value = inRow.Cells(iColSurt).Value * inRow.Cells(iColPrecio).Value
'                inRow.Cells(iColAlm).Value = drDetOT.Rows(iRowD).Item("Cat_Alm")

'                inRow.Cells(iColMarc).Value = drDetOT.Rows(iRowD).Item("marca").ToString

'                inRow.Cells(iColArt).ReadOnly = True
'                inRow.Cells(iColAlm).ReadOnly = True
'                inRow.Cells(iColPrecio).ReadOnly = True

'                CalculaExistencia(inRow)

'                DGridArticulos.Rows.Add(inRow)
'            Next

'            For iRowD = 0 To drDetOT.Rows.Count - 1
'                pSetColor(iRowD)
'            Next

'            drDetOT.Dispose()
'            drEncOT.Dispose()
'            sGetTotal()
'            'Estatus de modificación
'            sStatus = "M"

'        End If

'        Select Case Lstatus.Text.ToUpper
'            Case "CERRADA"
'                CmdCerrar.Enabled = False
'                cmdCancelar.Enabled = True
'                cmdAceptar.Enabled = False
'                Lstatus.BackColor = Color.FromArgb(204, 255, 255)
'                txtCapturaArticulos.Enabled = False
'            Case "CANCELADO"
'                Lstatus.BackColor = Color.FromArgb(243, 159, 24)
'                CmdCerrar.Enabled = False
'                cmdCancelar.Enabled = False
'                cmdAceptar.Enabled = False
'                txtCapturaArticulos.Enabled = False
'            Case "APLICADO"
'                Lstatus.BackColor = Color.PaleGreen
'                CmdCerrar.Enabled = Cerrar
'                cmdCancelar.Enabled = True
'                cmdAceptar.Enabled = Editar
'                txtCapturaArticulos.Enabled = True
'            Case "OC NUEVA"
'                Lstatus.BackColor = Color.Gold
'                CmdCerrar.Enabled = False
'                cmdCancelar.Enabled = False
'                cmdAceptar.Enabled = Editar
'                txtCapturaArticulos.Enabled = True
'        End Select

'    End Sub

'    Private Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpVariables As String, ByVal sSpValores As String) As DataTable
'        Try
'            Dim PkBusCon As New Datos.ZctDataBase                         'Objeto de la base de datos
'            Dim iSp As Integer                                      'Indice del procedimiento almacenado
'            Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
'            Dim sValores() As String = Split(sSpValores, "|")
'            'Inicia el procedimieto almacenado
'            PkBusCon.IniciaProcedimiento(sSpNAME)
'            'Agrega el parametro que define que se va a realizar
'            PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
'            'Obtiene el total de los procedimientos
'            Dim iTot As Integer = UBound(sVector)
'            'Recorre las variables del procedimiento almacenado
'            For iSp = 0 To iTot
'                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
'                    Case "INT"
'                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Integer), SqlDbType.Int)
'                    Case "VARCHAR"
'                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.VarChar)
'                    Case "SMALLDATETIME"
'                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Date), SqlDbType.SmallDateTime)
'                    Case "DECIMAL"
'                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Decimal), SqlDbType.Decimal)
'                    Case "TEXT"
'                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)
'                End Select
'            Next

'            Select Case TpConsulta
'                'SELECT
'                Case 1
'                    PkBusCon.InciaDataAdapter()
'                    Return PkBusCon.GetReaderSP.Tables("DATOS")
'                Case 2
'                    'Ejecuta el escalar
'                    PkBusCon.GetScalarSP()
'                    Return Nothing

'                Case 3
'                    'Delete
'                    PkBusCon.GetScalarSP()
'                    Return Nothing
'                Case Else
'                    Return Nothing
'            End Select

'        Catch ex As Exception
'            MsgBox(ex.Message.ToString)
'            Return Nothing
'        End Try
'    End Function

'    'Busca el valor del indice de la tabla en cuestión
'    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String)
'        Try
'            Dim PkBusCon As New Datos.ZctDataBase                             'Objeto de la base de datos
'            'Inicializa el procedimietno almacenado
'            PkBusCon.IniciaProcedimiento(Procedimiento)
'            'Asigna el nombre de la tabla 
'            PkBusCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
'            'Asigna el resultado de la consulta a el objeto
'            Dim a As Object = PkBusCon.GetScalarSP()
'            'En caso de que no este vacío asigna su valor a la columna de indice
'            If Not a Is System.DBNull.Value Then
'                Return CType(a, Integer)
'            Else
'                Return 1
'            End If
'        Catch ex As Exception
'            MsgBox(ex.Message.ToString)
'            Return 1
'        End Try
'    End Function

'    'Busca el valor del indice de la tabla en cuestión
'    Private Function GetData(ByVal Procedimiento As String, ByVal Tabla As String, ByRef PkCon As ZctDataBase)
'        'Objeto de la base de datos
'        'Inicializa el procedimietno almacenado
'        PkCon.IniciaProcedimiento(Procedimiento)
'        'Asigna el nombre de la tabla 
'        PkCon.AddParameterSP("@Tabla", Tabla, SqlDbType.VarChar)
'        'Asigna el resultado de la consulta a el objeto
'        Dim a As Object = PkCon.GetScalarSPTran()
'        'En caso de que no este vacío asigna su valor a la columna de indice
'        If Not a Is System.DBNull.Value Then
'            Return CType(a, Integer)
'        Else
'            Return 1
'        End If
'    End Function

'    Public Sub guarda()
'        If Lstatus.Text = "CERRADA" Or Lstatus.Text = "CANCELADO" Then
'            MsgBox("No puede modificar órdenes de trabajo ya gereradas o canceladas, si desea corregir cancele/cree una OC nueva", MsgBoxStyle.Exclamation, "ATENCIÓN")
'            Exit Sub
'        ElseIf Lstatus.Text = "CERRADA*" Then
'            Lstatus.Text = "CERRADA"
'        ElseIf Lstatus.Text = "CANCELADO*" Then
'            Lstatus.Text = "CANCELADO"
'        End If
'        ' Dim drEncOT As DataTable
'        Dim PkConAlone As New Datos.ZctDataBase                         'Objeto de la base de datos
'        Dim sVariables As String
'        Dim sParametros As String

'        'Valida la existencia de un proveedor
'        If txtProveedor.Text = "" Or txtProveedor.Text = "0" Then
'            MsgBox("El código del proveedor no puede estar vacío, verfique por favor.")
'            If txtProveedor.Enabled Then txtProveedor.Focus()
'            Exit Sub
'        End If

'        'Valida la existencia de artículos
'        If DGridArticulos.RowCount <= 0 Then
'            MsgBox("La orden no puede ser grabada sin artículos, verifique por favor.", MsgBoxStyle.Information, "SOT")
'            Exit Sub
'        Else
'            If DGridArticulos.Rows(0).Cells(iColArt).Value Is DBNull.Value Or DGridArticulos.Rows(0).Cells(iColArt).Value Is Nothing Then
'                MsgBox("La orden no puede ser grabada sin artículos, verifique por favor.", MsgBoxStyle.Information, "SOT")
'                Exit Sub
'            End If
'        End If


'        'Try
'        PkConAlone.OpenConTran()
'        PkConAlone.Inicia_Transaccion()

'        If sStatus = "A" Then
'            txtFolio.DataBindings.Clear()
'            txtFolio.Text = OcFolio.SetConsecutivo(PkConAlone)  'GetData("Procedimientos_MAX", "ZctEncOC", PkConAlone)
'        End If

'        sVariables = "@Cod_OC;INT|@Cod_Prov;INT|@Fch_OC;SMALLDATETIME|@FchAplMov;SMALLDATETIME|@Fac_OC;VARCHAR|@FchFac_OC;SMALLDATETIME|@estado;VARCHAR"
'        sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer) & "|" & Now.Date & "|" & DtAplicacion.Value & "|" & 0 & "|" & Date.Now & "|" & Lstatus.Text
'        GetData(2, "SP_ZctEncOC", sVariables, sParametros, PkConAlone)

'        sVariables = "@Cod_OC;INT|@CodDet_Oc;INT|@Cod_Art;VARCHAR|@Ctd_Art;INT|@Cos_Art;DECIMAL|@CtdStdDet_OC;INT|@FchAplMov;SMALLDATETIME|@Cat_Alm;INT"
'        Dim iRow As Integer
'        For iRow = 0 To DGridArticulos.Rows.Count - 1

'            If Not (DGridArticulos.Item(iColArt, iRow).Value Is Nothing) AndAlso DGridArticulos.Item(iColArt, iRow).Value <> "" Then

'                If DGridArticulos.Item(iColAlm, iRow).Value.ToString = "" OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "0" Then
'                    DGridArticulos.Item(iColAlm, iRow).ErrorText = "El almacén en la orden de compra no puede estar vacía"
'                    Throw New ZctReglaNegocioEx("El almacén en la orden de compra no puede estar vacía")
'                Else
'                    DGridArticulos.Item(iColAlm, iRow).ErrorText = ""
'                End If




'                sParametros = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer) & "|" & _
'                 CType(IIf(DGridArticulos.Item(iColCod, iRow).Value = Nothing OrElse DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer) & "|" & _
'                 DGridArticulos.Item(iColArt, iRow).Value & "|" & _
'                 CType(IIf(DGridArticulos.Item(iColSol, iRow).Value = Nothing OrElse DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Integer) & "|" & _
'                 CType(IIf(DGridArticulos.Item(iColPrecio, iRow).Value.ToString = "", 1, DGridArticulos.Item(iColPrecio, iRow).Value), Decimal) & "|" & _
'                 CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 1, DGridArticulos.Item(iColSurt, iRow).Value), Integer) & "|" & _
'                 DtAplicacion.Value & "|" & DGridArticulos.Item(iColAlm, iRow).Value.ToString

'                'sParametros = txtFolio.Text & "|" & _
'                ' DGridArticulos.Item(iColCod, iRow).Value & "|" & _
'                ' DGridArticulos.Item(iColArt, iRow).Value & "|" & _
'                ' DGridArticulos.Item(iColSol, iRow).Value & "|" & _
'                ' DGridArticulos.Item(iColPrecio, iRow).Value & "|" & _
'                ' DGridArticulos.Item(iColSurt, iRow).Value & "|" & _
'                ' DtAplicacion.Value & "|" & DGridArticulos.Item(iColAlm, iRow).Value.ToString



'                GetData(2, "SP_ZctDetOC", sVariables, sParametros, PkConAlone)
'            End If

'        Next
'        PkConAlone.Termina_Transaccion()
'        PkConAlone.CloseConTran()
'        Dim cClas As New Datos.ClassGen
'        cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdCompra", "A", txtFolio.Text)
'        'Catch ex As Exception
'        '    PkConAlone.Cancela_Transaccion()
'        '    PkConAlone.CloseConTran()
'        '    MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
'        'End Try
'        If Lstatus.Text = "CANCELADO" Then
'            _ControladorCxP.CancelarCxP(txtFolio.Text)
'        End If
'        If Lstatus.Text = "CERRADA" Then
'            _ControladorCxP.CreaCuentaPagar(txtFolio.Text)
'        End If
'        'Status 
'        If sStatus = "A" Then
'            If MsgBox("¿Desea imprimir la orden de compra?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
'                pImprime()
'            End If
'        End If

'        If MsgBox("¿Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
'            plimpia(True)
'            sStatus = "A"
'        Else
'            plimpia(False)

'            sStatus = "M"
'            CargaCompraV2()
'        End If
'    End Sub

'    Private Function GetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpVariables As String, ByVal sSpValores As String, ByVal PkCon As ZctDataBase) As DataTable

'        'Objeto de la base de datos
'        Dim iSp As Integer                                      'Indice del procedimiento almacenado
'        Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
'        Dim sValores() As String = Split(sSpValores, "|")
'        'Inicia el procedimieto almacenado
'        PkCon.IniciaProcedimiento(sSpNAME)
'        'Agrega el parametro que define que se va a realizar
'        PkCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
'        'Obtiene el total de los procedimientos
'        Dim iTot As Integer = UBound(sVector)
'        'Recorre las variables del procedimiento almacenado
'        For iSp = 0 To iTot
'            Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1).ToUpper
'                Case "INT"
'                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Integer), SqlDbType.Int)
'                Case "VARCHAR"
'                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.VarChar)
'                Case "SMALLDATETIME"
'                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Date), SqlDbType.SmallDateTime)
'                Case "DECIMAL"
'                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Decimal), SqlDbType.Decimal)
'                Case "TEXT"
'                    PkCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)
'            End Select
'        Next

'        Select Case TpConsulta

'            Case 2
'                'Ejecuta el escalar
'                PkCon.GetScalarSPTran()
'                Return Nothing
'            Case Else
'                Return Nothing
'        End Select

'    End Function

'End Class
