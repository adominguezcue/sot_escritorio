Imports SOTControladores.Controladores
Imports Dominio.Nucleo.Entidades
Imports Transversal.Excepciones

Public Class ZctClientes
    ''
    Friend _Controlador As New SOTControladores.Controladores.ControladorPermisos 'Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos 'Permisos.Controlador.Controlador.vistapermisos
    Private sSpVariables As String = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
    Private sSpVariblesGen As String = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR|@Dir_Cte;VARCHAR|@RFC_Cte;VARCHAR|@Cod_Ciu;INT|@Cod_Edo;INT|@Ref_Cte;VARCHAR|@Cod_Gte;Int|@Cod_Region;VARCHAR|@Cod_MarcaTienda;INT|@Cod_Zona;INT|@Telefono;VARCHAR|@Telular;VARCHAR|@lugar_envio;VARCHAR|@cod_cte_base;INT|@email;varchar(50)"
    Private sSPName As String = "SP_ZctCatCte"
    Private sTabla As String = "ZctCatCte_Busqueda"
    Private sSqlBusqueda As String = "SELECT [Cod_Cte] as C�digo ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
    Private load As Boolean = False
    Private salir_en_automatico As Boolean = False

    Private Sub ZctClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        inicia_formulario()
    End Sub

    Private Sub inicia_formulario()
        'evia que recarge 2 veces el inicio del formulario para el dialogo de modificar cliente
        'If load Then Exit Sub
        'load = True

        _Permisos = _Controlador.ObtenerPermisosActuales() '.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        Me.cmdEliminar.Enabled = _Permisos.EliminarDatosFiscales
        'Posici�n Inicial
        Me.Top = 0
        Me.Left = 0

        txtBusqCodCte.ZctSOTLabelDesc1.Text = ""
        txtBusqCodCte.SqlBusqueda = sSqlBusqueda
        txtBusqCodCte.SPName = sSPName
        txtBusqCodCte.SpVariables = sSpVariables
        txtBusqCodCte.Tabla = sTabla

        'txtCodCteBase.ZctSOTLabelDesc1.Text = ""
        'txtCodCteBase.SqlBusqueda = sSqlBusqueda
        'txtCodCteBase.SPName = sSPName
        'txtCodCteBase.SpVariables = sSpVariables
        'txtCodCteBase.Tabla = sTabla
        TxtCP.ZctSOTTextBox1.MaxLength = 6

        'cboEntrega.DataSource = entrega

        'cboEntrega.GetDataAlfa("ZctCatLugarEnvio")

        CboEstado.GetData("ZctCatEdo")

        'CboMarcaTienda.GetData("ZctCatMarcaTienda")

        CboCiudad.Clear()
        'cboZona.Clear()

        'cboRegion.GetDataAlfa("ZctCatRegion")
        'cboRegion.Clear()

        'Gerentes
        'Limpia la descripci�n
        'txtGte.ZctSOTLabelDesc1.Text = ""
        ''Asigna el nombre del procedimiento almacenado para la busqueda
        'txtGte.SPName = "SP_ZctCatGteF3"
        ''Envia las variables del procedimiento almacenado
        'txtGte.SpVariables = "@Cod_Gte;INT|@Nom_Gte;VARCHAR"
        ''Envia la tabla de referencia al procedimiento almacenado
        'txtGte.Tabla = "ZctCatGte"

        cbTiposPersonas.DataSource = Transversal.Extensiones.EnumExtensions.ComoListaDto(Of TiposPersonas)()
        cbTiposPersonas.DisplayMember = "Nombre"
        cbTiposPersonas.ValueMember = "Valor"

        Dim controladorCliente As New SOTControladores.Controladores.ControladorClientes()
        cliente = controladorCliente.ObtenerUltimoIdCliente()


        'CboTipoPersona.GetData("tipospersona", 0)
        txtBusqCodCte.Text = cliente.ToString
        If cliente > 0 Then
            GetData(1)
        End If
    End Sub

    Private cliente As Integer = 0
    Public Sub CargaCliente(ByRef cod_cte As Integer)
        Try
            salir_en_automatico = True
            cliente = cod_cte
            '_Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
            'inicia_formulario()
            'txtBusqCodCte.Text = cod_cte.ToString
            'cmdAceptar.Enabled = _Permisos.edita
            'CboEstado.GetData("ZctCatEdo")
            'GetData(1)
            cmdEliminar.Enabled = False
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub GetData(ByVal sTpConsulta As Integer)
        Dim PkBusCon As New Datos.ZctDataBase

        PkBusCon.IniciaProcedimiento(sSPName)

        PkBusCon.AddParameterSP("@TpConsulta", sTpConsulta, SqlDbType.Int)
        PkBusCon.AddParameterSP("@Cod_Cte", CType(IIf(txtBusqCodCte.Text = "", 0, txtBusqCodCte.Text), Integer), SqlDbType.Int)
        PkBusCon.AddParameterSP("@Nom_Cte", txtNombre.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@ApPat_Cte", txtApPaterno.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@ApMat_Cte", txtApMaterno.Text.ToString, SqlDbType.VarChar)
        'PkBusCon.AddParameterSP("@Dir_Cte", TxtDireccion.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@RFC_Cte", txtRFC.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@Cod_Ciu", CType(CboCiudad.value, Integer), SqlDbType.Int)
        PkBusCon.AddParameterSP("@Cod_Edo", CType(CboEstado.value, Integer), SqlDbType.Int)
        'PkBusCon.AddParameterSP("@Ref_Cte", txtReferencia.Text.ToString, SqlDbType.VarChar)
        'PkBusCon.AddParameterSP("@Cod_Gte", CType(IIf(txtGte.Text = "", 0, txtGte.Text), Integer), SqlDbType.Int)
        'PkBusCon.AddParameterSP("@Cod_Region", IIf(cboRegion.value = "", DBNull.Value, cboRegion.value), SqlDbType.VarChar)
        'PkBusCon.AddParameterSP("@Cod_MarcaTienda", CType(CboMarcaTienda.value, Integer), SqlDbType.Int)
        'PkBusCon.AddParameterSP("@Cod_Zona", CType(cboZona.value, Integer), SqlDbType.Int)
        PkBusCon.AddParameterSP("@Telefono", txtTelefono.Text.ToString, SqlDbType.VarChar)
        'PkBusCon.AddParameterSP("@Telular", txtTelular.Text.ToString, SqlDbType.VarChar)
        'PkBusCon.AddParameterSP("@lugar_envio", cboEntrega.value.ToString, SqlDbType.VarChar)
        'PkBusCon.AddParameterSP("@cod_cte_base", txtCodCteBase.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@tipo_persona", If(cbTiposPersonas.SelectedValue, 0), SqlDbType.Int)
        PkBusCon.AddParameterSP("@calle", TxtCalle.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@colonia", TxtColonia.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@cp", TxtCP.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@numint", TxtNumeroInterior.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@numext", TxtNumExt.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@email", TxtEmail.Text.ToString, SqlDbType.VarChar)
        PkBusCon.AddParameterSP("@pais", txtpais.Text.ToString, SqlDbType.VarChar)

        Select Case sTpConsulta
            'SELECT
            Case 1
                PkBusCon.InciaDataAdapter()
                Dim dtDatos As DataTable = PkBusCon.GetReaderSP.Tables("DATOS")

                txtBusqCodCte.DataBindings.Clear()
                txtBusqCodCte.DataBindings.Add("Text", dtDatos, "Cod_Cte", True, DataSourceUpdateMode.OnPropertyChanged, 0)

                txtNombre.DataBindings.Clear()
                txtNombre.DataBindings.Add("Text", dtDatos, "Nom_Cte", True, DataSourceUpdateMode.OnPropertyChanged, "")

                txtApPaterno.DataBindings.Clear()
                txtApPaterno.DataBindings.Add("Text", dtDatos, "ApPat_Cte", True, DataSourceUpdateMode.OnPropertyChanged, "")

                txtApMaterno.DataBindings.Clear()
                txtApMaterno.DataBindings.Add("Text", dtDatos, "ApMat_Cte", True, DataSourceUpdateMode.OnPropertyChanged, "")

                'TxtDireccion.DataBindings.Clear()
                'TxtDireccion.DataBindings.Add("Text", dtDatos, "Dir_Cte", True, DataSourceUpdateMode.OnPropertyChanged, "")

                txtRFC.DataBindings.Clear()
                txtRFC.DataBindings.Add("Text", dtDatos, "RFC_Cte", True, DataSourceUpdateMode.OnPropertyChanged, "XAXX010101000")

                CboEstado.DataBindings.Clear()
                CboEstado.DataBindings.Add("ValueItem", dtDatos, "Cod_Edo", True, DataSourceUpdateMode.OnPropertyChanged, 0)


                'CboMarcaTienda.DataBindings.Clear()
                'CboMarcaTienda.DataBindings.Add("ValueItem", dtDatos, "Cod_MarcaTienda", True, DataSourceUpdateMode.OnPropertyChanged, 0)

                'cboEntrega.

                CboCiudad.GetData("ZctCatCiuXEdo", CboEstado.value)

                CboCiudad.DataBindings.Clear()
                CboCiudad.DataBindings.Add("ValueItem", dtDatos, "Cod_Ciu", True, DataSourceUpdateMode.OnPropertyChanged, "")

                TxtCalle.DataBindings.Clear()
                TxtCalle.DataBindings.Add("Text", dtDatos, "calle", True, DataSourceUpdateMode.OnPropertyChanged, "")

                'cboRegion.DataBindings.Clear()
                'cboRegion.DataBindings.Add("ValueItemStr", dtDatos, "Cod_Region", False, DataSourceUpdateMode.OnPropertyChanged, "NTE")

                'cboZona.GetDataAlfa("ZctCatZonaXRegion", cboRegion.value)

                'cboZona.DataBindings.Clear()
                'cboZona.DataBindings.Add("ValueItem", dtDatos, "Cod_Zona", True, DataSourceUpdateMode.OnPropertyChanged, "")

                'txtReferencia.DataBindings.Clear()
                'txtReferencia.DataBindings.Add("Text", dtDatos, "Ref_Cte", True, DataSourceUpdateMode.OnPropertyChanged, "")

                'txtGte.DataBindings.Clear()
                'txtGte.DataBindings.Add("Text", dtDatos, "Cod_Gte", True, DataSourceUpdateMode.OnPropertyChanged, "")
                'txtGte.pCargaDescripcion()


                txtTelefono.DataBindings.Clear()
                txtTelefono.DataBindings.Add("Text", dtDatos, "Telefono", True, DataSourceUpdateMode.OnPropertyChanged, "")

                'txtTelular.DataBindings.Clear()
                'txtTelular.DataBindings.Add("Text", dtDatos, "Telular", True, DataSourceUpdateMode.OnPropertyChanged, "")

                'cboEntrega.DataBindings.Clear()
                'cboEntrega.DataBindings.Add("ValueItemStr", dtDatos, "lugar_envio", False, DataSourceUpdateMode.OnPropertyChanged, "L")

                'txtCodCteBase.DataBindings.Clear()
                'txtCodCteBase.DataBindings.Add("Text", dtDatos, "cod_cte_base", True, DataSourceUpdateMode.OnPropertyChanged, "")
                'cboTipoPersona
                'CboTipoPersona.GetData("ZCTcatTipoPersona", CboTipoPersona.value)
                'cbTiposPersonas.DataBindings.Clear()
                'cbTiposPersonas.DataBindings.Add("SelectedValue", dtDatos, "tipo_persona", False, DataSourceUpdateMode.OnPropertyChanged, "1")
                If (dtDatos.Rows.Count > 0) Then
                    cbTiposPersonas.SelectedValue = CType(dtDatos.Rows(0)("tipo_persona"), TiposPersonas)
                End If
                'cbTiposPersonas.DataBindings.Clear()
                'cbTiposPersonas.DataBindings.Add("ValueItemStr", dtDatos, "tipo_persona", False, DataSourceUpdateMode.OnPropertyChanged, "1")

                TxtNumeroInterior.DataBindings.Clear()
                TxtNumeroInterior.DataBindings.Add("Text", dtDatos, "numint", True, DataSourceUpdateMode.OnPropertyChanged, "")

                TxtNumExt.DataBindings.Clear()
                TxtNumExt.DataBindings.Add("Text", dtDatos, "numext", True, DataSourceUpdateMode.OnPropertyChanged, "")

                TxtCP.DataBindings.Clear()
                TxtCP.DataBindings.Add("Text", dtDatos, "cp", True, DataSourceUpdateMode.OnPropertyChanged, "")

                TxtColonia.DataBindings.Clear()
                TxtColonia.DataBindings.Add("Text", dtDatos, "colonia", True, DataSourceUpdateMode.OnPropertyChanged, "")

                TxtEmail.DataBindings.Clear()
                TxtEmail.DataBindings.Add("Text", dtDatos, "email", True, DataSourceUpdateMode.OnPropertyChanged, "")

                txtpais.DataBindings.Clear()
                txtpais.DataBindings.Add("Text", dtDatos, "pais", True, DataSourceUpdateMode.OnPropertyChanged, "")
                'cboTipoArt.DataBindings.Add("ValueItemStr", dtDatos, "Cod_TpArt", False, DataSourceUpdateMode.OnPropertyChanged, "ART")
                pHabilita(False)
                dtDatos.Dispose()
            Case 2
                'Update,Insert  
                PkBusCon.GetScalarSP()
                pHabilita(True)
            Case 3
                'Delete
                PkBusCon.GetScalarSP()
                pHabilita(True)
        End Select

        'PkBusCon.InciaDataAdapter()
        'DGBusqueda.DataSource = PkBusCon.GetReaderSP.Tables("DATOS")


    End Sub


    Private Sub txtBusqCodCte_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBusqCodCte.lostfocus
        Try

            If CType(IIf(txtBusqCodCte.Text = "", 0, txtBusqCodCte.Text), Integer) > 0 Then
                cmdAceptar.Enabled = _Permisos.ModificarDatosFiscales
                GetData(1)
            Else
                cmdAceptar.Enabled = _Permisos.CrearDatosFiscales
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CboEstado_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles CboEstado.lostfocus

        CboCiudad.GetData("ZctCatCiuXEdo", CboEstado.value)
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try



            If Len(txtRFC.Text) = 0 Then
                txtRFC.Text = "XAXX010101000"
            End If
            Dim ErrMsj As String = ""

            Dim tipoSeleccionado As Nullable(Of TiposPersonas) = CType(cbTiposPersonas.SelectedValue, Nullable(Of TiposPersonas))

            If Not tipoSeleccionado.HasValue Then
                ErrMsj = ErrMsj & "Seleccione un tipo de persona" & vbCrLf
            ElseIf tipoSeleccionado.Value = TiposPersonas.Moral Then
                If Len(txtRFC.Text) <> 12 Then
                    ErrMsj = ErrMsj & "El RFC para las personas morales debe ser de 12 caracteres" & vbCrLf
                    txtRFC.Focus()

                End If
            Else
                If Len(txtRFC.Text) <> 13 Then
                    ErrMsj = ErrMsj & "El RFC para las personas fisicas debe ser de 13 caracteres" & vbCrLf
                    txtRFC.Focus()

                End If
            End If

            If txtNombre.Text = "" Then ErrMsj = ErrMsj & "Debe de proprocionar el nombre del hotel." & vbCrLf
            If CboCiudad.Text = "" Then ErrMsj = ErrMsj & "Debe seleccionar una ciudad y un estado." & vbCrLf : CboEstado.Focus()
            'If txtGte.Text = "" Or txtGte.Text = "0" Then ErrMsj = ErrMsj & "Debe de proprocionar el gerente a cargo." & vbCrLf
            If TxtColonia.Text = "" Then ErrMsj = ErrMsj & "Debe debe proporcionar la colonia." & vbCrLf : TxtColonia.Focus()
            'If TxtNumeroInterior.Text = "" Then ErrMsj = ErrMsj & "Debe proporcionar el numero interior." & vbCrLf : TxtNumeroInterior.Focus()
            If TxtCalle.Text = "" Then ErrMsj = ErrMsj & "Debe proporcionar la calle." & vbCrLf : TxtCalle.Focus()
            If TxtCP.Text = "" Then ErrMsj = ErrMsj & "Debe proporcionar el codigo postal." & vbCrLf : TxtCP.Focus()
            'If TxtEmail.Text = "" Then ErrMsj = ErrMsj & "Debe proporcionar un correo electronico." & vbCrLf : TxtEmail.Focus()
            If txtpais.Text = "" Then ErrMsj = ErrMsj & "Debe proporcionar el Pa�s." & vbCrLf : txtpais.Focus()
            If Len(ErrMsj) > 0 Then
                Throw New Exception("Debe corregir los siguientes errores para poder guardar los datos fiscales del hotel: " & vbCrLf & ErrMsj)
            End If
            'VALIDACI�N PARA LOS DATOS

            Dim controlador As New SOTControladores.Controladores.ControladorClientes()

            Dim idClienteExistente = controlador.ObtenerUltimoIdCliente()
            Dim idClienteForm = CType(IIf(txtBusqCodCte.Text = "", 0, txtBusqCodCte.Text), Integer)

            If Not controlador.VerificarNoExisteCliente() AndAlso idClienteExistente <> idClienteForm Then
                Throw New SOTException("Ya existen datos fiscales para este hotel")
            End If

            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctCatCte", "A", txtBusqCodCte.Text)
            GetData(2)
            pLimpia()
            If salir_en_automatico Then
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atencion")
        End Try


    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        If txtBusqCodCte.Enabled = True Then
            Me.Dispose()
        Else
            pLimpia()
        End If
        If salir_en_automatico Then
            Me.Close()
        End If
    End Sub

    Private Sub pLimpia()
        txtBusqCodCte.DataBindings.Clear()
        txtBusqCodCte.Text = ""
        txtNombre.DataBindings.Clear()
        txtNombre.Text = ""
        txtApPaterno.DataBindings.Clear()
        txtApPaterno.Text = ""
        txtApMaterno.DataBindings.Clear()
        txtApMaterno.Text = ""
        TxtEmail.DataBindings.Clear()
        TxtEmail.Text = ""
        'TxtDireccion.DataBindings.Clear()
        'TxtDireccion.Text = ""
        txtRFC.DataBindings.Clear()
        txtRFC.Text = ""
        'txtReferencia.DataBindings.Clear()
        'txtReferencia.Text = ""
        txtBusqCodCte.Text = 0
        txtBusqCodCte.ZctSOTLabelDesc1.Text = ""
        'Limpia los datos de los gerentes
        'txtGte.DataBindings.Clear()
        'txtGte.Text = ""
        'txtGte.ZctSOTLabelDesc1.Text = ""
        txtTelefono.Text = ""
        txtTelefono.DataBindings.Clear()
        'txtTelular.Text = ""
        'txtTelular.DataBindings.Clear()

        'txtCodCteBase.Text = ""
        'txtCodCteBase.DataBindings.Clear()

        TxtColonia.Text = ""
        TxtColonia.DataBindings.Clear()

        TxtNumeroInterior.Text = ""
        TxtNumeroInterior.DataBindings.Clear()

        TxtCalle.Text = ""
        TxtCalle.DataBindings.Clear()

        TxtNumExt.Text = ""
        TxtNumExt.DataBindings.Clear()

        TxtCP.Text = ""
        TxtCP.DataBindings.Clear()

        txtpais.DataBindings.Clear()
        txtpais.Text = ""
        pHabilita(True)
        txtBusqCodCte.Focus()


        Dim controladorCliente As New SOTControladores.Controladores.ControladorClientes()
        cliente = controladorCliente.ObtenerUltimoIdCliente()


        'CboTipoPersona.GetData("tipospersona", 0)
        txtBusqCodCte.Text = cliente.ToString
        If cliente > 0 Then
            GetData(1)
        End If

    End Sub

    Private Sub pHabilita(ByVal habilitar As Boolean)
        txtBusqCodCte.Enabled = habilitar
    End Sub

    Private Sub cmdEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEliminar.Click
        Try
            If MsgBox("�Desea eliminar este cliente?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then Exit Sub

            'VALIDACI�N PARA LOS DATOS
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctCatCte", "A", txtBusqCodCte.Text)
            GetData(3)
            pLimpia()
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al eliminar el cliente, verfique que dicho cliente no tenga ordenes de trabajo activas.")
        End Try
    End Sub

    Private Sub cboRegion_lostfocus(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'cboZona.GetDataAlfa("ZctCatZonaXRegion", cboRegion.value)
    End Sub

    Private Sub CboTipoPersona_lostfocus(sender As Object, e As System.EventArgs)
        Dim tipoSeleccionado As Nullable(Of TiposPersonas) = CType(cbTiposPersonas.SelectedValue, Nullable(Of TiposPersonas))

        Dim Visibles As Boolean = IIf(tipoSeleccionado.HasValue AndAlso tipoSeleccionado.Value = TiposPersonas.Moral, False, True)

        txtApMaterno.Enabled = Visibles
        txtApPaterno.Enabled = Visibles
    End Sub


End Class