<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ZctOrdServicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.groupBox3 = New System.Windows.Forms.GroupBox()
        Me.Lstatus = New System.Windows.Forms.Label()
        Me.DtoServicioOCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctGroupControls7 = New ZctSOT.ZctGroupControls()
        Me.DtAplicacion = New ZctSOT.ZctControlFecha()
        Me.ZctGroupControls3 = New ZctSOT.ZctGroupControls()
        Me.DGridServicios = New ZctSOT.ZctSotGrid()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoOrdenCompraDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Eliminar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ZctSOTLabel4 = New ZctSOT.ZctSOTLabel()
        Me.ZctSOTLabel5 = New ZctSOT.ZctSOTLabel()
        Me.lblIva = New ZctSOT.ZctSOTLabelDesc()
        Me.lblTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.lblDevolucion = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel6 = New ZctSOT.ZctSOTLabel()
        Me.lblPorSurtir = New ZctSOT.ZctSOTLabelDesc()
        Me.lblSurtido = New ZctSOT.ZctSOTLabelDesc()
        Me.lblSubTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctGroupControls2 = New ZctSOT.ZctGroupControls()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbGastos = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbCentroCostos = New System.Windows.Forms.ComboBox()
        Me.DtFechaFact = New ZctSOT.ZctControlFecha()
        Me.txtFactura = New ZctSOT.ZctControlTexto()
        Me.botonera = New ZctSOT.ZctSOTGroupBox()
        Me.CmdCerrar = New ZctSOT.ZctSOTButton()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdImprimir = New ZctSOT.ZctSOTButton()
        Me.cmdCancelarEdicion = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.zctFolio = New ZctSOT.ZctGroupControls()
        Me.cmdDer = New ZctSOT.ZctSOTButton()
        Me.txtProveedor = New ZctSOT.ZctControlBusqueda()
        Me.cmdIzq = New ZctSOT.ZctSOTButton()
        Me.txtFolio = New ZctSOT.ZctControlTexto()
        Me.ZctSOTToolTip1 = New ZctSOT.ZctSOTToolTip()
        Me.groupBox3.SuspendLayout()
        CType(Me.DtoServicioOCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ZctGroupControls7.SuspendLayout()
        Me.ZctGroupControls3.SuspendLayout()
        CType(Me.DGridServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ZctGroupControls2.SuspendLayout()
        Me.botonera.SuspendLayout()
        Me.zctFolio.SuspendLayout()
        Me.SuspendLayout()
        '
        'groupBox3
        '
        Me.groupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.groupBox3.Controls.Add(Me.Lstatus)
        Me.groupBox3.Location = New System.Drawing.Point(771, 7)
        Me.groupBox3.Name = "groupBox3"
        Me.groupBox3.Size = New System.Drawing.Size(172, 49)
        Me.groupBox3.TabIndex = 7
        Me.groupBox3.TabStop = False
        '
        'Lstatus
        '
        Me.Lstatus.BackColor = System.Drawing.Color.Gold
        Me.Lstatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Lstatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Lstatus.Location = New System.Drawing.Point(10, 16)
        Me.Lstatus.Name = "Lstatus"
        Me.Lstatus.Size = New System.Drawing.Size(153, 20)
        Me.Lstatus.TabIndex = 0
        Me.Lstatus.Text = "OC Nueva"
        Me.Lstatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DtoServicioOCBindingSource
        '
        Me.DtoServicioOCBindingSource.DataSource = GetType(Modelo.Almacen.Entidades.Dtos.DtoServicioOC)
        '
        'ZctGroupControls7
        '
        Me.ZctGroupControls7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls7.Controls.Add(Me.DtAplicacion)
        Me.ZctGroupControls7.Location = New System.Drawing.Point(5, 465)
        Me.ZctGroupControls7.Name = "ZctGroupControls7"
        Me.ZctGroupControls7.Size = New System.Drawing.Size(320, 51)
        Me.ZctGroupControls7.TabIndex = 4
        Me.ZctGroupControls7.TabStop = False
        '
        'DtAplicacion
        '
        Me.DtAplicacion.DateWith = 91
        Me.DtAplicacion.Enabled = False
        Me.DtAplicacion.Location = New System.Drawing.Point(6, 14)
        Me.DtAplicacion.Name = "DtAplicacion"
        Me.DtAplicacion.Nombre = "Fecha de aplicación:"
        Me.DtAplicacion.Size = New System.Drawing.Size(310, 27)
        Me.DtAplicacion.TabIndex = 0
        Me.DtAplicacion.Tag = "Fecha y hora en la que se va aplicar el movimiento"
        Me.ZctSOTToolTip1.SetToolTip(Me.DtAplicacion, "Fecha y hora en la que se va aplicar el movimiento")
        Me.DtAplicacion.Value = New Date(2018, 11, 9, 12, 28, 54, 0)
        '
        'ZctGroupControls3
        '
        Me.ZctGroupControls3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls3.Controls.Add(Me.DGridServicios)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel4)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel5)
        Me.ZctGroupControls3.Controls.Add(Me.lblIva)
        Me.ZctGroupControls3.Controls.Add(Me.lblTotal)
        Me.ZctGroupControls3.Controls.Add(Me.lblDevolucion)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel6)
        Me.ZctGroupControls3.Controls.Add(Me.lblPorSurtir)
        Me.ZctGroupControls3.Controls.Add(Me.lblSurtido)
        Me.ZctGroupControls3.Controls.Add(Me.lblSubTotal)
        Me.ZctGroupControls3.Location = New System.Drawing.Point(3, 112)
        Me.ZctGroupControls3.Name = "ZctGroupControls3"
        Me.ZctGroupControls3.Size = New System.Drawing.Size(940, 347)
        Me.ZctGroupControls3.TabIndex = 3
        Me.ZctGroupControls3.TabStop = False
        '
        'DGridServicios
        '
        Me.DGridServicios.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGridServicios.AutoGenerateColumns = False
        Me.DGridServicios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGridServicios.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.CodigoOrdenCompraDataGridViewTextBoxColumn, Me.DescripcionDataGridViewTextBoxColumn, Me.CostoDataGridViewTextBoxColumn, Me.Eliminar})
        Me.DGridServicios.DataSource = Me.DtoServicioOCBindingSource
        Me.DGridServicios.Location = New System.Drawing.Point(9, 19)
        Me.DGridServicios.Name = "DGridServicios"
        Me.DGridServicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGridServicios.Size = New System.Drawing.Size(925, 291)
        Me.DGridServicios.TabIndex = 5
        Me.DGridServicios.Tag = "En esta cuadricula se dan de alta los artículos que fueron comprados"
        Me.ZctSOTToolTip1.SetToolTip(Me.DGridServicios, "En esta cuadricula se dan de alta los artículos que fueron comprados")
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "Id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "Id"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.Visible = False
        '
        'CodigoOrdenCompraDataGridViewTextBoxColumn
        '
        Me.CodigoOrdenCompraDataGridViewTextBoxColumn.DataPropertyName = "CodigoOrdenCompra"
        Me.CodigoOrdenCompraDataGridViewTextBoxColumn.HeaderText = "CodigoOrdenCompra"
        Me.CodigoOrdenCompraDataGridViewTextBoxColumn.Name = "CodigoOrdenCompraDataGridViewTextBoxColumn"
        Me.CodigoOrdenCompraDataGridViewTextBoxColumn.Visible = False
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Servicio"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        '
        'CostoDataGridViewTextBoxColumn
        '
        Me.CostoDataGridViewTextBoxColumn.DataPropertyName = "Costo"
        Me.CostoDataGridViewTextBoxColumn.HeaderText = "Costo"
        Me.CostoDataGridViewTextBoxColumn.Name = "CostoDataGridViewTextBoxColumn"
        '
        'Eliminar
        '
        Me.Eliminar.DataPropertyName = "Eliminar"
        Me.Eliminar.HeaderText = "Eliminar"
        Me.Eliminar.Name = "Eliminar"
        '
        'ZctSOTLabel4
        '
        Me.ZctSOTLabel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctSOTLabel4.AutoSize = True
        Me.ZctSOTLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel4.Location = New System.Drawing.Point(429, 327)
        Me.ZctSOTLabel4.Name = "ZctSOTLabel4"
        Me.ZctSOTLabel4.Size = New System.Drawing.Size(62, 13)
        Me.ZctSOTLabel4.TabIndex = 10
        Me.ZctSOTLabel4.Text = "SubTotal:"
        '
        'ZctSOTLabel5
        '
        Me.ZctSOTLabel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctSOTLabel5.AutoSize = True
        Me.ZctSOTLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel5.Location = New System.Drawing.Point(605, 327)
        Me.ZctSOTLabel5.Name = "ZctSOTLabel5"
        Me.ZctSOTLabel5.Size = New System.Drawing.Size(31, 13)
        Me.ZctSOTLabel5.TabIndex = 12
        Me.ZctSOTLabel5.Text = "IVA:"
        '
        'lblIva
        '
        Me.lblIva.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblIva.BackColor = System.Drawing.Color.LightBlue
        Me.lblIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIva.Location = New System.Drawing.Point(642, 327)
        Me.lblIva.Name = "lblIva"
        Me.lblIva.Size = New System.Drawing.Size(104, 15)
        Me.lblIva.TabIndex = 4
        Me.lblIva.Text = "$0"
        Me.lblIva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(795, 327)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "$0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDevolucion
        '
        Me.lblDevolucion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDevolucion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblDevolucion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDevolucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDevolucion.Location = New System.Drawing.Point(183, 327)
        Me.lblDevolucion.Name = "lblDevolucion"
        Me.lblDevolucion.Size = New System.Drawing.Size(81, 15)
        Me.lblDevolucion.TabIndex = 9
        Me.lblDevolucion.Text = "Devolución"
        Me.lblDevolucion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblDevolucion.Visible = False
        '
        'ZctSOTLabel6
        '
        Me.ZctSOTLabel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctSOTLabel6.AutoSize = True
        Me.ZctSOTLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel6.Location = New System.Drawing.Point(752, 327)
        Me.ZctSOTLabel6.Name = "ZctSOTLabel6"
        Me.ZctSOTLabel6.Size = New System.Drawing.Size(40, 13)
        Me.ZctSOTLabel6.TabIndex = 14
        Me.ZctSOTLabel6.Text = "Total:"
        '
        'lblPorSurtir
        '
        Me.lblPorSurtir.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblPorSurtir.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblPorSurtir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPorSurtir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPorSurtir.Location = New System.Drawing.Point(12, 327)
        Me.lblPorSurtir.Name = "lblPorSurtir"
        Me.lblPorSurtir.Size = New System.Drawing.Size(81, 15)
        Me.lblPorSurtir.TabIndex = 7
        Me.lblPorSurtir.Text = "Por Surtir"
        Me.lblPorSurtir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblPorSurtir.Visible = False
        '
        'lblSurtido
        '
        Me.lblSurtido.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSurtido.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblSurtido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSurtido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurtido.Location = New System.Drawing.Point(96, 327)
        Me.lblSurtido.Name = "lblSurtido"
        Me.lblSurtido.Size = New System.Drawing.Size(81, 15)
        Me.lblSurtido.TabIndex = 8
        Me.lblSurtido.Text = "Surtido"
        Me.lblSurtido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblSurtido.Visible = False
        '
        'lblSubTotal
        '
        Me.lblSubTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSubTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTotal.Location = New System.Drawing.Point(497, 327)
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblSubTotal.TabIndex = 2
        Me.lblSubTotal.Text = "$0"
        Me.lblSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ZctGroupControls2
        '
        Me.ZctGroupControls2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls2.Controls.Add(Me.Label2)
        Me.ZctGroupControls2.Controls.Add(Me.cbGastos)
        Me.ZctGroupControls2.Controls.Add(Me.Label1)
        Me.ZctGroupControls2.Controls.Add(Me.cbCentroCostos)
        Me.ZctGroupControls2.Controls.Add(Me.DtFechaFact)
        Me.ZctGroupControls2.Controls.Add(Me.txtFactura)
        Me.ZctGroupControls2.Location = New System.Drawing.Point(5, 60)
        Me.ZctGroupControls2.Name = "ZctGroupControls2"
        Me.ZctGroupControls2.Size = New System.Drawing.Size(938, 51)
        Me.ZctGroupControls2.TabIndex = 1
        Me.ZctGroupControls2.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(731, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Gasto:"
        '
        'cbGastos
        '
        Me.cbGastos.FormattingEnabled = True
        Me.cbGastos.Location = New System.Drawing.Point(775, 12)
        Me.cbGastos.Name = "cbGastos"
        Me.cbGastos.Size = New System.Drawing.Size(156, 21)
        Me.cbGastos.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(474, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Centro de costos:"
        '
        'cbCentroCostos
        '
        Me.cbCentroCostos.FormattingEnabled = True
        Me.cbCentroCostos.Location = New System.Drawing.Point(570, 12)
        Me.cbCentroCostos.Name = "cbCentroCostos"
        Me.cbCentroCostos.Size = New System.Drawing.Size(156, 21)
        Me.cbCentroCostos.TabIndex = 6
        '
        'DtFechaFact
        '
        Me.DtFechaFact.DateWith = 91
        Me.DtFechaFact.Location = New System.Drawing.Point(189, 13)
        Me.DtFechaFact.Name = "DtFechaFact"
        Me.DtFechaFact.Nombre = "Fecha Factura:"
        Me.DtFechaFact.Size = New System.Drawing.Size(283, 27)
        Me.DtFechaFact.TabIndex = 5
        Me.DtFechaFact.Value = New Date(2018, 11, 9, 12, 28, 54, 0)
        '
        'txtFactura
        '
        Me.txtFactura.Location = New System.Drawing.Point(7, 12)
        Me.txtFactura.Multiline = False
        Me.txtFactura.Name = "txtFactura"
        Me.txtFactura.Nombre = "Factura:"
        Me.txtFactura.Size = New System.Drawing.Size(176, 28)
        Me.txtFactura.TabIndex = 4
        Me.txtFactura.Tag = "Factura de la Orden de compra"
        Me.txtFactura.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtFactura.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtFactura, "Factura de la Orden de compra")
        '
        'botonera
        '
        Me.botonera.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.botonera.Controls.Add(Me.CmdCerrar)
        Me.botonera.Controls.Add(Me.cmdCancelar)
        Me.botonera.Controls.Add(Me.cmdImprimir)
        Me.botonera.Controls.Add(Me.cmdCancelarEdicion)
        Me.botonera.Controls.Add(Me.cmdAceptar)
        Me.botonera.Location = New System.Drawing.Point(331, 465)
        Me.botonera.Name = "botonera"
        Me.botonera.Size = New System.Drawing.Size(612, 51)
        Me.botonera.TabIndex = 5
        Me.botonera.TabStop = False
        '
        'CmdCerrar
        '
        Me.CmdCerrar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdCerrar.Location = New System.Drawing.Point(137, 14)
        Me.CmdCerrar.Name = "CmdCerrar"
        Me.CmdCerrar.Size = New System.Drawing.Size(112, 28)
        Me.CmdCerrar.TabIndex = 7
        Me.CmdCerrar.Tag = "Cierra OC y no  permite modificacones"
        Me.CmdCerrar.Text = "Guardar y cerrar"
        Me.ZctSOTToolTip1.SetToolTip(Me.CmdCerrar, "Graba las modificaciones realizadas, si el código es nuevo da de alta la orden de" &
        " compra")
        Me.CmdCerrar.UseVisualStyleBackColor = True
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.Location = New System.Drawing.Point(369, 14)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(112, 28)
        Me.cmdCancelar.TabIndex = 10
        Me.cmdCancelar.Tag = "Cancela la Orden"
        Me.cmdCancelar.Text = "Cancelar Orden"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelar, "Cancela la OC")
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Location = New System.Drawing.Point(11, 14)
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Size = New System.Drawing.Size(112, 28)
        Me.cmdImprimir.TabIndex = 8
        Me.cmdImprimir.Text = "Imprimir"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdImprimir, "Imprime la orden de trabajo")
        Me.cmdImprimir.UseVisualStyleBackColor = True
        Me.cmdImprimir.Visible = False
        '
        'cmdCancelarEdicion
        '
        Me.cmdCancelarEdicion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelarEdicion.Location = New System.Drawing.Point(491, 14)
        Me.cmdCancelarEdicion.Name = "cmdCancelarEdicion"
        Me.cmdCancelarEdicion.Size = New System.Drawing.Size(112, 28)
        Me.cmdCancelarEdicion.TabIndex = 9
        Me.cmdCancelarEdicion.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" &
    "alla"
        Me.cmdCancelarEdicion.Text = "Cancelar edición"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelarEdicion, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" &
        "alla")
        Me.cmdCancelarEdicion.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(255, 14)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(112, 28)
        Me.cmdAceptar.TabIndex = 6
        Me.cmdAceptar.Tag = "Graba las modificaciones realizadas, si el código es nuevo da de alta la orden de" &
    " compra"
        Me.cmdAceptar.Text = "Guardar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdAceptar, "Graba las modificaciones realizadas, si el código es nuevo da de alta la orden de" &
        " compra")
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'zctFolio
        '
        Me.zctFolio.Controls.Add(Me.cmdDer)
        Me.zctFolio.Controls.Add(Me.txtProveedor)
        Me.zctFolio.Controls.Add(Me.cmdIzq)
        Me.zctFolio.Controls.Add(Me.txtFolio)
        Me.zctFolio.Location = New System.Drawing.Point(3, 5)
        Me.zctFolio.Name = "zctFolio"
        Me.zctFolio.Size = New System.Drawing.Size(762, 51)
        Me.zctFolio.TabIndex = 0
        Me.zctFolio.TabStop = False
        '
        'cmdDer
        '
        Me.cmdDer.Location = New System.Drawing.Point(204, 19)
        Me.cmdDer.Name = "cmdDer"
        Me.cmdDer.Size = New System.Drawing.Size(19, 23)
        Me.cmdDer.TabIndex = 2
        Me.cmdDer.Text = ">"
        Me.cmdDer.UseVisualStyleBackColor = True
        '
        'txtProveedor
        '
        Me.txtProveedor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProveedor.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtProveedor.Descripcion = "ZctSOTLabelDesc1"
        Me.txtProveedor.Location = New System.Drawing.Point(244, 14)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Nombre = "Proveedor:"
        Me.txtProveedor.Size = New System.Drawing.Size(512, 31)
        Me.txtProveedor.SPName = Nothing
        Me.txtProveedor.SPParametros = Nothing
        Me.txtProveedor.SpVariables = Nothing
        Me.txtProveedor.SqlBusqueda = Nothing
        Me.txtProveedor.TabIndex = 3
        Me.txtProveedor.Tabla = Nothing
        Me.txtProveedor.Tag = "Código de proveedor<BR>Para buscar presione [F3]"
        Me.txtProveedor.TextWith = 111
        Me.txtProveedor.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtProveedor.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtProveedor, "Código de proveedor;Para buscar presione [F3]")
        Me.txtProveedor.Validar = True
        '
        'cmdIzq
        '
        Me.cmdIzq.Location = New System.Drawing.Point(184, 19)
        Me.cmdIzq.Name = "cmdIzq"
        Me.cmdIzq.Size = New System.Drawing.Size(19, 23)
        Me.cmdIzq.TabIndex = 1
        Me.cmdIzq.Text = "<"
        Me.cmdIzq.UseVisualStyleBackColor = True
        '
        'txtFolio
        '
        Me.txtFolio.Location = New System.Drawing.Point(9, 17)
        Me.txtFolio.Multiline = False
        Me.txtFolio.Name = "txtFolio"
        Me.txtFolio.Nombre = "Folio:"
        Me.txtFolio.Size = New System.Drawing.Size(176, 28)
        Me.txtFolio.TabIndex = 0
        Me.txtFolio.Tag = "Folio de la Orden de compra"
        Me.txtFolio.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtFolio.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtFolio, "Folio de la Orden de compra")
        '
        'ZctOrdServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(952, 519)
        Me.Controls.Add(Me.groupBox3)
        Me.Controls.Add(Me.ZctGroupControls7)
        Me.Controls.Add(Me.ZctGroupControls3)
        Me.Controls.Add(Me.ZctGroupControls2)
        Me.Controls.Add(Me.botonera)
        Me.Controls.Add(Me.zctFolio)
        Me.Name = "ZctOrdServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Órdenes de Pago"
        Me.groupBox3.ResumeLayout(False)
        CType(Me.DtoServicioOCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ZctGroupControls7.ResumeLayout(False)
        Me.ZctGroupControls3.ResumeLayout(False)
        Me.ZctGroupControls3.PerformLayout()
        CType(Me.DGridServicios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ZctGroupControls2.ResumeLayout(False)
        Me.ZctGroupControls2.PerformLayout()
        Me.botonera.ResumeLayout(False)
        Me.zctFolio.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents zctFolio As ZctSOT.ZctGroupControls
    Friend WithEvents ZctGroupControls2 As ZctSOT.ZctGroupControls
    Friend WithEvents botonera As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelarEdicion As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents txtProveedor As ZctSOT.ZctControlBusqueda
    Friend WithEvents txtFolio As ZctSOT.ZctControlTexto
    Friend WithEvents ZctGroupControls3 As ZctSOT.ZctGroupControls
    Friend WithEvents DGridServicios As ZctSOT.ZctSotGrid
    Friend WithEvents lblTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblSubTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblIva As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblDevolucion As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblSurtido As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblPorSurtir As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTToolTip1 As ZctSOT.ZctSOTToolTip
    Friend WithEvents ZctGroupControls7 As ZctSOT.ZctGroupControls
    Friend WithEvents DtAplicacion As ZctSOT.ZctControlFecha
    Friend WithEvents ZctSOTLabel5 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSOTLabel6 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSOTLabel4 As ZctSOT.ZctSOTLabel
    Friend WithEvents cmdImprimir As ZctSOT.ZctSOTButton
    Friend WithEvents cmdDer As ZctSOT.ZctSOTButton
    Friend WithEvents cmdIzq As ZctSOT.ZctSOTButton
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Private WithEvents groupBox3 As System.Windows.Forms.GroupBox
    Private WithEvents Lstatus As System.Windows.Forms.Label
    Friend WithEvents CmdCerrar As ZctSOT.ZctSOTButton
    Friend WithEvents DtFechaFact As ZctSOT.ZctControlFecha
    Friend WithEvents txtFactura As ZctSOT.ZctControlTexto
    Friend WithEvents DtoServicioOCBindingSource As BindingSource
    Friend WithEvents IdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CodigoOrdenCompraDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CostoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Eliminar As DataGridViewCheckBoxColumn
    Friend WithEvents Label2 As Label
    Friend WithEvents cbGastos As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cbCentroCostos As ComboBox
End Class
