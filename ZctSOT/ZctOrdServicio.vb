Imports System.Data.SqlDbType
Imports ResTotal
Imports System.Linq
Imports ZctSOT.Datos
Imports SOTControladores.Controladores
Imports SOTControladores.Fabricas
Imports Transversal.Excepciones
Imports Modelo.Entidades
Imports System.IO

Public Class ZctOrdServicio
    'Friend _Controlador As New SOTControladores.Controladores.ControladorPermisos
    'Friend _ControladorCxP As SOTControladores.Controladores.ControladorCuentasPorPagar
    'Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos

    Dim sStatus As String                                              'Estatus del documento


    Dim OcFolio As New zctFolios("OC")
    'Dim fBusqueda As ZctBusqueda
    Dim Editar, Cerrar As Boolean

    Dim parametrosSistema = ControladorParametros.ObtenerParametros()
    Private centrosCostos As List(Of IGrouping(Of String, ConceptoGasto)) = New List(Of IGrouping(Of String, ConceptoGasto))

    Private Sub ZctOrdCompra_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        Dim _ControladorCxP As SOTControladores.Controladores.ControladorCuentasPorPagar
        _ControladorCxP = Nothing
        _ControladorCxP = New SOTControladores.Controladores.ControladorCuentasPorPagar
    End Sub

    Private Sub ZctOrdCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim _ControladorCxP As SOTControladores.Controladores.ControladorCuentasPorPagar
            Dim _Permisos As Modelo.Seguridad.Dtos.DtoPermisos
            Dim _Controlador As New SOTControladores.Controladores.ControladorPermisos

            _Permisos = _Controlador.ObtenerPermisosActuales()
            _ControladorCxP = New SOTControladores.Controladores.ControladorCuentasPorPagar

            With _Permisos
                Editar = (.CrearOrdenCompra Or .ModificarOrdenCompra Or .EliminarOrdenCompra)
                DGridServicios.AllowUserToAddRows = .CrearOrdenCompra
                DGridServicios.AllowUserToDeleteRows = .EliminarOrdenCompra
                DGridServicios.ReadOnly = Not (.ModificarOrdenCompra Or .CrearOrdenCompra)
                cmdAceptar.Enabled = Editar
                Cerrar = .CerrarOrdenCompra
            End With
            'Posición Inicial
            Me.Top = 0
            Me.Left = 0

            Dim controladorConceptosGastos = New ControladorConceptosGastos()
            Dim conceptos = controladorConceptosGastos.ObtenerConceptosActivos()

            conceptos.Add(New ConceptoGasto With {.Activo = True, .NombreCentroCostosTmp = ""})

            centrosCostos = conceptos.GroupBy(Function(m) m.NombreCentroCostosTmp).ToList()

            'cbCentroCostos.Items.AddRange(centrosCostos.ToArray())
            cbCentroCostos.DataSource = centrosCostos.ToArray()
            cbCentroCostos.DisplayMember = "Key"
            cbCentroCostos.ValueMember = "Key"

            cbCentroCostos.SelectedValue = ""
            cbGastos.SelectedValue = 0
            'Cliente
            'Limpia la descripción
            txtProveedor.ZctSOTLabelDesc1.Text = ""
            'Asigna los datos de la busqueda
            txtProveedor.SqlBusqueda = "SELECT [Cod_Prov] as Código ,[Nom_Prov] as Descripción FROM [ZctSOT].[dbo].[ZctCatProv]"

            'Asigna el nombre del procedimiento almacenado para la busqueda
            txtProveedor.SPName = "SP_ZctCatProvV2F3"
            'Envia las variables del procedimiento almacenado
            txtProveedor.SpVariables = "@Cod_Prov;INT|@Nom_Prov;VARCHAR|@RFC_Prov;VARCHAR|@Dir_Prov;VARCHAR"
            'Envia la tabla de referencia al procedimiento almacenado
            txtProveedor.Tabla = "ZctCatProv"

            txtFolio.DataBindings.Clear()
            txtFolio.Text = OcFolio.Consecutivo

            'El estatus es una alta
            sStatus = "A"

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelarEdicion.Click
        If sStatus = "A" Then
            Me.Dispose()
        Else
            Plimpia(True)
            sStatus = "A"
            cmdImprimir.Visible = False
        End If
    End Sub
    Private Sub Plimpia(ByVal LimpiaFolio As Boolean)
        If LimpiaFolio Then
            txtFolio.DataBindings.Clear()
            txtFolio.Text = ""
            txtFolio.Text = OcFolio.Consecutivo
            txtFolio.Enabled = True
            txtFolio.Focus()
        End If
        Lstatus.Text = "OC Nueva"
        Lstatus.BackColor = Color.Gold
        txtFactura.DataBindings.Clear()
        txtFactura.Text = ""

        txtProveedor.DataBindings.Clear()
        txtProveedor.Text = ""
        txtProveedor.ZctSOTLabelDesc1.Text = ""

        lblSubTotal.Text = FormatCurrency(0)
        lblIva.Text = FormatCurrency(0)
        lblTotal.Text = FormatCurrency(0)

        cbCentroCostos.SelectedValue = ""
        cbGastos.SelectedValue = 0

        'DGridServicios.Rows.Clear()
        DtoServicioOCBindingSource.Clear()

    End Sub

    Private Sub DGridServicios_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridServicios.CellEndEdit
        Try
            If CostoDataGridViewTextBoxColumn.Index = e.ColumnIndex OrElse Eliminar.Index = e.ColumnIndex Then
                sGetTotal()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub sGetTotal()
        Dim iRow As Integer
        Dim iTotal As Decimal = 0
        Dim iIVA As Decimal = 0
        Dim columnaCosto As Integer = CostoDataGridViewTextBoxColumn.Index

        For iRow = 0 To DGridServicios.Rows.Count - 1

            If DGridServicios.Item(Eliminar.Index, iRow).Value = True Then Continue For

            Dim costoActual As Decimal = CType(DGridServicios.Item(columnaCosto, iRow).Value, Decimal)

            iTotal = iTotal + costoActual

            'If CType(DGridServicios.Item(iColOmitirIVA, iRow).Value, Boolean) = False Then
            iIVA = iIVA + ((costoActual / (1 + parametrosSistema.Iva_CatPar)) * parametrosSistema.Iva_CatPar)
            'End If
        Next

        lblSubTotal.Text = FormatCurrency(CType(iTotal - iIVA, String)) ' FormatCurrency(iTotal.ToString)
        lblIva.Text = FormatCurrency(iIVA.ToString)
        lblTotal.Text = FormatCurrency(iTotal.ToString) ' FormatCurrency(CType(iTotal + iIVA, String))
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try
            guardaV2()
        Catch ex As Exception
            MessageBox.Show("Ha ocurrido un error al grabar, favor de verificar los datos ingresados: " & System.Environment.NewLine & System.Environment.NewLine & ex.Message)
        End Try

    End Sub



    Public Sub guardaV2()
        If Lstatus.Text = "CERRADA" Or Lstatus.Text = "CANCELADO" Then
            MsgBox("No puede modificar órdenes de trabajo ya gereradas o canceladas, si desea corregir cancele/cree una OC nueva", MsgBoxStyle.Exclamation, "ATENCIÓN")
            Exit Sub
        ElseIf Lstatus.Text = "CERRADA*" Then
            Lstatus.Text = "CERRADA"
        ElseIf Lstatus.Text = "CANCELADO*" Then
            Lstatus.Text = "CANCELADO"
        End If


        'Valida la existencia de un proveedor
        If String.IsNullOrWhiteSpace(txtProveedor.Text) OrElse txtProveedor.Text = "0" Then
            MsgBox("El código del proveedor no puede estar vacío, verfique por favor.")
            If txtProveedor.Enabled Then txtProveedor.Focus()
            Exit Sub
        End If

        'Valida la existencia de artículos
        If DGridServicios.RowCount <= 0 Then
            MsgBox("La orden no puede ser grabada sin servicios, verifique por favor.", MsgBoxStyle.Information, "SOT")
            Exit Sub
        Else
            If DGridServicios.Rows(0).Cells(DescripcionDataGridViewTextBoxColumn.Index).Value Is DBNull.Value OrElse DGridServicios.Rows(0).Cells(DescripcionDataGridViewTextBoxColumn.Index).Value Is Nothing OrElse DGridServicios.Rows(0).Cells(DescripcionDataGridViewTextBoxColumn.Index).Value = "" Then
                MsgBox("La orden no puede ser grabada sin servicios, verifique por favor.", MsgBoxStyle.Information, "SOT")
                Exit Sub
            End If
        End If

        Dim encabezado As New Modelo.Almacen.Entidades.Dtos.DtoEncabezadoOC
        encabezado.Cod_OC = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer)
        encabezado.Cod_Prov = CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer)
        encabezado.Fch_OC = Now
        encabezado.FchAplMov = DtAplicacion.Value
        encabezado.Fac_OC = txtFactura.Text
        encabezado.FchFac_OC = DtFechaFact.Value
        encabezado.status = Lstatus.Text
        encabezado.Servicios = True
        encabezado.IdConceptoGasto = CType(IIf(IsNothing(cbGastos.SelectedValue) OrElse cbGastos.SelectedValue.ToString = "", 0, cbGastos.SelectedValue), Integer)

        Dim iRow As Integer
        For iRow = 0 To DGridServicios.Rows.Count - 1

            If Not (DGridServicios.Item(DescripcionDataGridViewTextBoxColumn.Index, iRow).Value Is Nothing) AndAlso DGridServicios.Item(DescripcionDataGridViewTextBoxColumn.Index, iRow).Value <> "" Then

                Dim detalle As New Modelo.Almacen.Entidades.Dtos.DtoServicioOC
                detalle.CodigoOrdenCompra = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer)
                detalle.Id = CType(IIf(DGridServicios.Item(IdDataGridViewTextBoxColumn.Index, iRow).Value = Nothing OrElse DGridServicios.Item(IdDataGridViewTextBoxColumn.Index, iRow).Value.ToString = "", 0, DGridServicios.Item(IdDataGridViewTextBoxColumn.Index, iRow).Value), Integer)
                detalle.Costo = CType(IIf(DGridServicios.Item(CostoDataGridViewTextBoxColumn.Index, iRow).Value.ToString = "", 0, DGridServicios.Item(CostoDataGridViewTextBoxColumn.Index, iRow).Value), Decimal)
                detalle.Descripcion = DGridServicios.Item(DescripcionDataGridViewTextBoxColumn.Index, iRow).Value.ToString
                detalle.Eliminar = CType(IIf(DGridServicios.Item(Eliminar.Index, iRow).Value.ToString = "", False, DGridServicios.Item(Eliminar.Index, iRow).Value), Boolean)

                encabezado.ServiciosOC.Add(detalle)
            End If

        Next

        Dim controladorOC = New SOTControladores.Controladores.ControladorOrdenesCompra()
        controladorOC.EditarOrdenServicio(encabezado, sStatus)

        Dim cClas As New Datos.ClassGen
        cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdCompra", "A", txtFolio.Text)

        'Status 
        If sStatus = "A" Then
            'Antonio Domínguez Cuevas 18/12/2019
            'If MsgBox("¿Desea imprimir la orden de compra?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            'pImprime()
            'End If
        End If

        ' If MsgBox("¿Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
        'Plimpia(True)
        'sStatus = "A"
        'Else
        Plimpia(False)

            sStatus = "M"
            CargaCompraV2()

            'Status 
            If sStatus = "M" Then
                cmdImprimir.Visible = True
            Else
                cmdImprimir.Visible = False
            End If
        'End If
    End Sub

    Public Sub CargaCompra(ByVal Folio As String)


        Try
            sStatus = "A"
            If IsNumeric(Folio) AndAlso Folio <> "" AndAlso CType(Folio, Integer) > 0 Then
                Plimpia(False)
                txtFolio.Enabled = False
                txtFolio.Text = Folio
                CargaCompraV2()
            End If

            'Status 
            If sStatus = "M" Then
                cmdImprimir.Visible = True
            Else
                cmdImprimir.Visible = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub txtFolio_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolio.lostfocus

        CargaCompra(txtFolio.Text)
    End Sub

    Public Sub CargaCompraV2()
        'Deshabilita el folio
        'zctFolio.Enabled = False
        txtFolio.Enabled = False

        Dim controladorOrdenesCompra As New SOTControladores.Controladores.ControladorOrdenesCompra()

        Dim drEncOT = controladorOrdenesCompra.ObtenerResumenOrdenCompra(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
        'controladorOrdenesCompra.SP_ZctEncOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer),
        '                                          Now.Date, DtAplicacion.Value, 0, Date.Now)


        If Not IsNothing(drEncOT) AndAlso drEncOT.Servicios.HasValue AndAlso drEncOT.Servicios.Value = True Then ' (drEncOT.Count > 0) Then
            txtFolio.DataBindings.Clear()
            txtFolio.DataBindings.Add("Text", drEncOT, "Cod_OC")

            txtFactura.DataBindings.Clear()
            txtFactura.DataBindings.Add("Text", drEncOT, "Fac_OC")

            DtAplicacion.DataBindings.Clear()
            DtAplicacion.DataBindings.Add("Value", drEncOT, "FchAplMov")

            DtFechaFact.DataBindings.Clear()
            DtFechaFact.DataBindings.Add("Value", drEncOT, "FchFac_OC")


            txtProveedor.DataBindings.Clear()
            txtProveedor.DataBindings.Add("Text", drEncOT, "Cod_Prov")
            txtProveedor.pCargaDescripcion()

            Lstatus.DataBindings.Clear()
            Lstatus.DataBindings.Add("Text", drEncOT, "status")

            For Each grupo In centrosCostos
                If grupo.Any(Function(m) m.Id = drEncOT.IdConceptoGasto.Value) Then
                    cbCentroCostos.SelectedValue = grupo.Key
                    cbGastos.SelectedValue = drEncOT.IdConceptoGasto.Value
                End If
            Next
            'Datos del Grid

            'Dim drDetOT = controladorOrdenesCompra.SP_ZctDetOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), 0, "0", 0, 0, 0, DtAplicacion.Value, 0, False)

            DtoServicioOCBindingSource.DataSource = drEncOT.ServiciosOC

            'For Each item In drEncOT.Detalles 'drDetOT
            '    Dim inRow As New DataGridViewRow

            '    inRow.CreateCells(DGridServicios)
            '    inRow.Cells(iColArt).Value = If(item.Cod_Art, "")
            '    inRow.Cells(iColDesc).Value = If(item.Desc_Art, "")
            '    inRow.Cells(iColSol).Value = If(item.Ctd_Art, 0)

            '    'inRow.Cells(iColExist).Value = drDetOT.Rows(iRowD).Item("Exist_Art").ToString
            '    inRow.Cells(iColCod).Value = item.CodDet_Oc
            '    inRow.Cells(iColSurt).Value = If(item.CtdStdDet_OC, 0)
            '    inRow.Cells(iColPrecio).Value = If(item.Cos_Art, 0)

            '    'inRow.Cells(iColSubTotal).Value = inRow.Cells(iColSurt).Value * inRow.Cells(iColPrecio).Value
            '    inRow.Cells(iColSubTotal).Value = inRow.Cells(iColSol).Value * inRow.Cells(iColPrecio).Value
            '    inRow.Cells(iColAlm).Value = If(item.Cat_Alm, 0)

            '    inRow.Cells(iColMarc).Value = If(item.marca, "")

            '    inRow.Cells(iColOmitirIVA).Value = item.OmitirIVA

            '    ' Revisar si no hay implicaciones negativas por dejar editable la columna de almacenes
            '    inRow.Cells(iColArt).ReadOnly = Not Editar
            '    inRow.Cells(iColAlm).ReadOnly = Not Editar
            '    inRow.Cells(iColPrecio).ReadOnly = Not Editar
            '    inRow.Cells(iColOmitirIVA).ReadOnly = Not Editar

            '    CalculaExistencia(inRow)

            '    DGridServicios.Rows.Add(inRow)
            'Next

            'For iRowD = 0 To drEncOT.Detalles.Count - 1 ' drDetOT.Count - 1
            '    pSetColor(iRowD)
            'Next

            sGetTotal()
            'Estatus de modificación
            sStatus = "M"

            botonera.Visible = True
        ElseIf IsNothing(drEncOT) Then
            botonera.Visible = True
        Else
            botonera.Visible = False
        End If

        Select Case Lstatus.Text.ToUpper
            Case "CERRADA"
                CmdCerrar.Enabled = False
                cmdCancelar.Enabled = True
                cmdAceptar.Enabled = False
                Lstatus.BackColor = Color.FromArgb(204, 255, 255)
                'txtCapturaArticulos.Enabled = False
            Case "CANCELADO"
                Lstatus.BackColor = Color.FromArgb(243, 159, 24)
                CmdCerrar.Enabled = False
                cmdCancelar.Enabled = False
                cmdAceptar.Enabled = False
                'txtCapturaArticulos.Enabled = False
            Case "APLICADO"
                Lstatus.BackColor = Color.PaleGreen
                CmdCerrar.Enabled = Cerrar
                cmdCancelar.Enabled = True
                cmdAceptar.Enabled = Editar
                'txtCapturaArticulos.Enabled = True
            Case "OC NUEVA"
                Lstatus.BackColor = Color.Gold
                CmdCerrar.Enabled = False
                cmdCancelar.Enabled = False
                cmdAceptar.Enabled = Editar
                'txtCapturaArticulos.Enabled = True
        End Select

    End Sub



    Private Sub DGridServicios_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles DGridServicios.UserDeletingRow
        If sStatus = "M" AndAlso Not (e.Row.Cells(IdDataGridViewTextBoxColumn.Index).Value = Nothing OrElse e.Row.Cells(IdDataGridViewTextBoxColumn.Index).Value.ToString = "" OrElse e.Row.Cells(IdDataGridViewTextBoxColumn.Index).Value = "0") Then
            MsgBox("Está intentando eliminar un servicio ya almacenado, por favor marque la casilla de eliminar y el sistema se encargará de eliminarlo.", MsgBoxStyle.Information)
            e.Cancel = True
        End If
    End Sub

    Private Sub DGridServicios_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles DGridServicios.DataError
        DGridServicios.Item(e.ColumnIndex, e.RowIndex).ErrorText = e.Exception.Message
        DGridServicios.Item(e.ColumnIndex, e.RowIndex).Selected = False
        DGridServicios.Item(e.ColumnIndex, e.RowIndex).Value = DGridServicios.Item(e.ColumnIndex, e.RowIndex).DefaultNewRowValue
    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        pImprime()
    End Sub

    Private Sub pImprime()

        Dim controladorGlobal = New ControladorConfiguracionGlobal()
        Dim controladorVPoints = New ControladorVPoints()
        Dim controladorClientes = New ControladorClientes()

        Dim config = controladorGlobal.ObtenerConfiguracionGlobal()
        Dim configPunto = ControladorVPoints.ObtenerConfiguracion()
        Dim datosFiscales = controladorClientes.ObtenerUltimoCliente()

        Dim documentoReporte = New ReportesSOT.OrdenesCompra.OrdenesServicio()
        documentoReporte.Load()


        Dim controladorOrdenesCompra As New SOTControladores.Controladores.ControladorOrdenesCompra()

        'Dim drEncOT = controladorOrdenesCompra.SP_ZctEncOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer),
        '                                              Now.Date, DtAplicacion.Value, 0, Date.Now)

        'Dim drDetOT = controladorOrdenesCompra.SP_ZctDetOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), 0, "0", 0, 0, 0, DtAplicacion.Value, 0, False)

        Dim drEncOT = controladorOrdenesCompra.ObtenerResumenOrdenCompra(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
        'Dim drDetOT = drEncOT.ServiciosOC

        Select Case drEncOT.status
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA
                drEncOT.Fch_OC = drEncOT.Fch_OC
                Exit Select
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.APLICADO
                drEncOT.Fch_OC = drEncOT.FchAplMov
                Exit Select
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO
                drEncOT.Fch_OC = If(drEncOT.FchAplMov, drEncOT.Fch_OC)
                Exit Select
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA
                Dim controladorCXP = New ControladorCuentasPorPagar()
                drEncOT.Fch_OC = controladorCXP.ObtenerFechaUltimaCuenta(drEncOT.Cod_OC)
                Exit Select
        End Select

        Dim params = New List(Of Modelo.Almacen.Entidades.ZctCatPar) From {
            config
        }

        Dim prms = params.Select(Function(m) New With {m.Direccion, m.id, m.Iva_CatPar, m.maximo_caja, m.MaximoComandasSimultaneas, m.MaximoTaxisSimultaneos, m.minimo_caja, m.Nombre, m.NombreSucursal, m.Operadora, m.PrecioPorDefectoTaxi, .PrecioTarjetasPuntos = If(configPunto?.PrecioTarjetas, 0), m.Propietaria, datosFiscales.RazonSocial, m.Reporteador_web, .RFC = datosFiscales.RFC_Cte, m.ruta_imagenes_articulos, m.RutaActualizaciones, m.RutaActualizacionesJSON, m.RutaReportes, m.SubNombre, m.taller}).ToList()

        Dim encabezados = New List(Of Object) From {
            New With {drEncOT.Cod_OC, .Cod_Prov = If(drEncOT.Cod_Prov, 0), drEncOT.Fac_OC, drEncOT.status, drEncOT.NN_Fch_OC, drEncOT.NN_FchAplMov, drEncOT.NN_FchFac_OC}
        }

        'Dim encabezados = drEncOT.Select(Function(m) New With {m.Cod_OC, .Cod_Prov = If(m.Cod_Prov, 0), m.Fac_OC, m.status, m.NN_Fch_OC, m.NN_FchAplMov, m.NN_FchFac_OC}).ToList()
        'Dim detalles = drDetOT.Select(Function(m) New With {m.Cod_Art, m.Cod_OC, m.CodDet_Oc, m.Desc_Art, m.marca, m.NN_Cat_Alm, m.NN_Cos_Art, m.NN_Ctd_Art, m.NN_CtdStdDet_OC, m.OmitirIVA}).ToList()

        Dim controladorProveedores = New ControladorProveedores()
        'Dim proveedor = controladorProveedores.ObtenerProveedor(drEncOT.Cod_Prov) '.First().Cod_Prov)

        Dim proveedores = New List(Of Modelo.Almacen.Entidades.Proveedor) From {
            controladorProveedores.ObtenerProveedor(drEncOT.Cod_Prov) '.First().Cod_Prov)
        }

        Dim controladorcentrodecostos = New ControladorConceptosGastos()

        Dim Conceptogasto = controladorcentrodecostos.ObtenerConceptosActivos()
        Dim CentrodecostoSeleccionado = New List(Of ReportesSOT.Dtos.DtoCentroDeCostos)
        Dim centrocostoreporte = New ReportesSOT.Dtos.DtoCentroDeCostos()
        Dim Conceptogastoseleciionado
        For Each current In Conceptogasto
            If current.Id = drEncOT.IdConceptoGasto Then
                Conceptogastoseleciionado = current
                centrocostoreporte.Gastos = current.Concepto.ToString()
            End If

        Next

        Dim ControladorCentroCostos = New ControladorCentrosCostos()
        Dim centrodecosto = ControladorCentroCostos.ObtenerCentrosCostosFiltrados()

        For Each current1 In centrodecosto
            If current1.Id = DirectCast(Conceptogastoseleciionado, Modelo.Entidades.ConceptoGasto).IdCentroCostos Then
                centrocostoreporte.NombreCentroDeCostos = current1.Nombre
            End If
        Next
        CentrodecostoSeleccionado.Add(centrocostoreporte)
		
        Dim provs = proveedores.Select(Function(m) New With {m.NN_dias_credito, m.DireccionAutomatica, m.calle, m.ciudad, m.Cod_Prov, m.colonia, m.Contacto, m.cp, m.Dir_Prov, m.email, m.estado, m.Nom_Prov, m.NombreComercial, m.numext, m.numint, m.RFC_Prov, m.telefono}).ToList()

        Dim rutaL = ReportesSOT.Parametros.Logos.RutaLogoGeneral

        If File.Exists(ReportesSOT.Parametros.Logos.RutaLogoGeneral) Then

            rutaL = Path.GetTempFileName() + Guid.NewGuid().ToString()

            While File.Exists(rutaL)
                rutaL = Path.GetTempFileName() + Guid.NewGuid().ToString()
            End While

            File.Copy(ReportesSOT.Parametros.Logos.RutaLogoGeneral, rutaL, True)

        End If

        Dim encabezadosReporte = New List(Of ReportesSOT.Dtos.DtoCabeceraReporte) From {
                New ReportesSOT.Dtos.DtoCabeceraReporte With
                {
                    .Direccion = config.Direccion,
                    .FechaInicio = Date.Now,
                    .FechaFin = Date.Now,
                    .Hotel = config.Nombre,
                    .RazonSocial = datosFiscales.RazonSocial,
                    .Usuario = ControladorBase.UsuarioActual.NombreCompleto,
                    .RutaLogo = rutaL
                }
            }
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesServicio_Constantes.TABLA_CENTRODECOSTOS).SetDataSource(CentrodecostoSeleccionado)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesServicio_Constantes.TABLA_CENTRODECOSTOS).SetDataSource(CentrodecostoSeleccionado)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesServicio_Constantes.TABLA_ENCABEZADO_ORDEN_COMPRA).SetDataSource(encabezados)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesServicio_Constantes.TABLA_SERVICIOS_ORDEN_COMPRA).SetDataSource(drEncOT.ServiciosOC)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesServicio_Constantes.TABLA_PARAMETROS).SetDataSource(prms)
        'documentoReporte.Database.Tables("ZctCatAlm").SetDataSource(ColAlmacen.DataSource)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesServicio_Constantes.TABLA_PROVEEDOR).SetDataSource(provs)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesServicio_Constantes.TABLA_ENCABEZADO_REPORTE).SetDataSource(encabezadosReporte)

        Dim visorR = New VisorReportes()
        visorR.crCrystalReportViewer.ReportSource = documentoReporte
        visorR.Update()

        visorR.ShowDialog(Me)
    End Sub

    Private Sub cmdIzq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIzq.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) - 1 > 0 Then

            Dim cod = New ControladorOrdenesCompra().ObtenerUltimoFolioMenorQue(CInt(txtFolio.Text), True)
            If Not cod.HasValue Then
                Exit Sub
            End If

            txtFolio.Text = cod.Value '(CInt(txtFolio.Text) - 1).ToString
            Plimpia(False)
            CargaCompraV2()
            'Status 
            If sStatus = "M" AndAlso CInt(txtFolio.Text) < OcFolio.Consecutivo Then
                cmdImprimir.Visible = True
            Else
                sStatus = "A"
                cmdImprimir.Visible = False
                DtFechaFact.ClearValue()
                DtAplicacion.ClearValue()
            End If
        End If

    End Sub

    Private Sub cmdDer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDer.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) + 1 <= OcFolio.Consecutivo Then

            Dim cod = New ControladorOrdenesCompra().ObtenerPrimerFolioMayorQue(CInt(txtFolio.Text), True)
            If Not cod.HasValue Then
                Exit Sub
            End If

            txtFolio.Text = cod.Value '(CInt(txtFolio.Text) + 1).ToString
            Plimpia(False)
            Lstatus.Text = "OC Nueva"
            Lstatus.BackColor = Color.Gold

            CargaCompraV2()
            'Status 
            If sStatus = "M" AndAlso CInt(txtFolio.Text) < OcFolio.Consecutivo Then
                cmdImprimir.Visible = True
            Else
                sStatus = "A"
                cmdImprimir.Visible = False
                DtFechaFact.ClearValue()
                DtAplicacion.ClearValue()
            End If
        End If
    End Sub

    'Private Sub cmdSurteAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    With DGridServicios
    '        'Dim iSubTotal As Decimal = 0
    '        For i As Integer = 0 To .Rows.Count - 1
    '            .Rows(i).Cells(iColSurt).Value = .Rows(i).Cells(iColSol).Value

    '            '.Rows(i).Cells(iColSubTotal).Value = CType((CType(.Rows(i).Cells(iColSurt).Value, Decimal)) * (CType(.Rows(i).Cells(iColPrecio).Value, Decimal)), String)
    '            .Rows(i).Cells(iColSubTotal).Value = CType((CType(.Rows(i).Cells(iColSol).Value, Decimal)) * (CType(.Rows(i).Cells(iColPrecio).Value, Decimal)), String)


    '            DGridServicios.Item(iColSol, i).Style.BackColor = lblSurtido.BackColor
    '            DGridServicios.Item(iColSurt, i).Style.BackColor = lblSurtido.BackColor
    '        Next
    '    End With

    '    sGetTotal()
    'End Sub

    Private Sub cmdCancelar_Click_1(sender As System.Object, e As System.EventArgs) Handles cmdCancelar.Click


        If MessageBox.Show("Esta operación es irreversible ¿desea continuar?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
            Return
        End If

        Try
            Lstatus.Text = "CANCELADO*"
            Lstatus.BackColor = Color.FromArgb(243, 159, 24)

            guardaV2()
        Catch
            Plimpia(False)

            sStatus = "M"
            CargaCompraV2()

            'Status 
            If sStatus = "M" Then
                cmdImprimir.Visible = True
            Else
                cmdImprimir.Visible = False
            End If

            Throw
        End Try

    End Sub

    Private Sub cbCentroCostos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbCentroCostos.SelectedIndexChanged
        Dim seleccionado = TryCast(cbCentroCostos.SelectedItem, IGrouping(Of String, ConceptoGasto))

        If IsNothing(seleccionado) Then Exit Sub

        'cbGastos.Items.Clear()
        'cbGastos.Items.AddRange(seleccionado.ToArray())
        cbGastos.DataSource = seleccionado.ToArray()

        cbGastos.DisplayMember = Transversal.Extensiones.ObjectExtensions.NombrePropiedad(Of ConceptoGasto, String)(Function(m) m.Concepto)
        cbGastos.ValueMember = Transversal.Extensiones.ObjectExtensions.NombrePropiedad(Of ConceptoGasto, Integer)(Function(m) m.Id)
    End Sub

    Private Sub CmdCerrar_Click(sender As System.Object, e As System.EventArgs) Handles CmdCerrar.Click

        Dim res = MsgBox("Si cierra la OC no podrá hacerle modificaciones ¿desea continuar?", MsgBoxStyle.YesNo, "Pregunta")
        If res = vbNo Then
            Exit Sub
        End If
        Lstatus.Text = "CERRADA*"
        CmdCerrar.Enabled = False
        Lstatus.BackColor = Color.FromArgb(204, 255, 255)
        guardaV2()
    End Sub

    Private Sub DGridServicios_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridServicios.CellContentClick
        If e.ColumnIndex = Eliminar.Index Then
            DGridServicios.BeginEdit(True)
            DGridServicios.EndEdit()
        End If
    End Sub

End Class
