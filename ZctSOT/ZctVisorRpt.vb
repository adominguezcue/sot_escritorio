Imports CrystalDecisions.Shared

Public Class PkVisorRpt
    Public sParam As String
    Public sRpt As String
    Public sSQLV As String
    Public sLeyenda As String

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Posición Inicial
        Me.Top = 0
        Me.Left = 0

        With My.Settings
            Try


                Cursor = Cursors.WaitCursor

                Dim nRptView As New zRptView
                nRptView.conectar()
                CRView.LogOnInfo = nRptView.crTableLogonInfos
                nRptView.rutaRpt = ""

               
                CRView.ReportSource = nRptView.printrpt(sRpt, sSQLV)



                If sParam <> "" Then

                    '                    CRView.ParameterFieldInfo = nRptView.fGetPar(Split(sParam, "|"))

                    Dim sPar1, sPar2 As String
                    Dim iLim As Integer
                    For Each sParametro As String In Split(sParam, "|")
                        iLim = InStr(sParametro, ";")

                        If iLim > 0 Then

                            sPar1 = Mid(sParametro, 1, iLim - 1)

                            sPar2 = Mid(sParametro, iLim + 1, Len(sParametro) - iLim)

                            Dim parametrofield As ParameterField = CRView.ParameterFieldInfo.Find(sPar1, "")

                            Dim dValor As New ParameterDiscreteValue

                            parametrofield.ParameterValueType = ParameterValueKind.StringParameter

                            dValor.Value = sPar2

                            parametrofield.CurrentValues.Add(dValor)
                            'Debug.Print(parametro.EditMask)


                            CRView.ParameterFieldInfo.Add(parametrofield)

                        End If
                    Next
                End If

                



                Dim pf As ParameterField = CRView.ParameterFieldInfo.Find("leyenda", "")
                If pf IsNot Nothing Then

                    Dim v As New ParameterDiscreteValue
                    v.Value = sLeyenda
                    pf.ParameterValueType = ParameterValueKind.StringParameter
                    pf.CurrentValues.Add(v)
                    CRView.ParameterFieldInfo.Add(pf)
                End If

                'CRView.
                CRView.Refresh()

                Cursor = Cursors.Default
            Catch ex As Exception
                MsgBox("Ha ocurrido un error, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
                Cursor = Cursors.Default
            End Try
        End With

    End Sub

    Private Sub CRView_Error(ByVal source As Object, ByVal e As CrystalDecisions.Windows.Forms.ExceptionEventArgs) Handles CRView.Error
        Debug.Print(e.Exception.Message)
    End Sub
End Class