<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OrdenesTrabajoMantenimientoForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ZctGroupControls6 = New ZctSOT.ZctGroupControls()
        Me.CboTpOrden = New ZctSOT.ZctControlCombo()
        Me.txtKm = New ZctSOT.ZctControlTexto()
        Me.txtTecnico = New ZctSOT.ZctControlBusqueda()
        Me.ZctSOTGroupBox3 = New ZctSOT.ZctSOTGroupBox()
        Me.ZctHistAprov = New ZctSOT.ZctSOTLabel()
        Me.chkAprobada = New System.Windows.Forms.CheckBox()
        Me.ZctGroupControls7 = New ZctSOT.ZctGroupControls()
        Me.DtAplicacion = New ZctSOT.ZctControlFecha()
        Me.ZctGroupControls4 = New ZctSOT.ZctGroupControls()
        Me.txtTrabajo = New ZctSOT.ZctControlTexto()
        Me.ZctGroupControls3 = New ZctSOT.ZctGroupControls()
        Me.lblDevolucion = New ZctSOT.ZctSOTLabelDesc()
        Me.lblSurtido = New ZctSOT.ZctSOTLabelDesc()
        Me.lblPorSurtir = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel3 = New ZctSOT.ZctSOTLabel()
        Me.lblSubTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel2 = New ZctSOT.ZctSOTLabel()
        Me.lblIva = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel()
        Me.lblTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.DGridArticulos = New ZctSOT.ZctSotGrid()
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox()
        Me.cmdCancelaOrden = New ZctSOT.ZctSOTButton()
        Me.cmdImprimir = New ZctSOT.ZctSOTButton()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.ZctGroupControls2 = New ZctSOT.ZctGroupControls()
        Me.txtVin = New ZctSOT.ZctControlBusqueda()
        Me.txtContacto = New ZctSOT.ZctControlBusqueda()
        Me.txtCliente = New ZctSOT.ZctControlBusqueda()
        Me.ZCTFolio = New ZctSOT.ZctGroupControls()
        Me.DTEntrada = New ZctSOT.ZctControlFecha()
        Me.cmdDer = New ZctSOT.ZctSOTButton()
        Me.cmdIzq = New ZctSOT.ZctSOTButton()
        Me.txtFolio = New ZctSOT.ZctControlTexto()
        Me.ZctGroupCtrFchSal = New ZctSOT.ZctGroupControls()
        Me.DTSalida = New ZctSOT.ZctControlFecha()
        Me.ZctSOTToolTip1 = New ZctSOT.ZctSOTToolTip()
        Me.ColArt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDesc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAlmacen = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ColMot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColExist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSurt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColPrecio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZctGroupControls6.SuspendLayout()
        Me.ZctSOTGroupBox3.SuspendLayout()
        Me.ZctGroupControls7.SuspendLayout()
        Me.ZctGroupControls4.SuspendLayout()
        Me.ZctGroupControls3.SuspendLayout()
        CType(Me.DGridArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ZctSOTGroupBox1.SuspendLayout()
        Me.ZctGroupControls2.SuspendLayout()
        Me.ZCTFolio.SuspendLayout()
        Me.ZctGroupCtrFchSal.SuspendLayout()
        Me.SuspendLayout()
        '
        'ZctGroupControls6
        '
        Me.ZctGroupControls6.Controls.Add(Me.CboTpOrden)
        Me.ZctGroupControls6.Controls.Add(Me.txtKm)
        Me.ZctGroupControls6.Controls.Add(Me.txtTecnico)
        Me.ZctGroupControls6.Location = New System.Drawing.Point(427, 493)
        Me.ZctGroupControls6.Name = "ZctGroupControls6"
        Me.ZctGroupControls6.Size = New System.Drawing.Size(496, 94)
        Me.ZctGroupControls6.TabIndex = 6
        Me.ZctGroupControls6.TabStop = False
        Me.ZctGroupControls6.Visible = False
        '
        'CboTpOrden
        '
        Me.CboTpOrden.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboTpOrden.Location = New System.Drawing.Point(138, 50)
        Me.CboTpOrden.Name = "CboTpOrden"
        Me.CboTpOrden.Nombre = "Calsificación de trabajo:"
        Me.CboTpOrden.Size = New System.Drawing.Size(349, 28)
        Me.CboTpOrden.TabIndex = 9
        Me.CboTpOrden.Tag = "Tipos de trabajo"
        Me.CboTpOrden.value = Nothing
        Me.CboTpOrden.ValueItem = 0
        Me.CboTpOrden.ValueItemStr = Nothing
        '
        'txtKm
        '
        Me.txtKm.Location = New System.Drawing.Point(2, 50)
        Me.txtKm.Multiline = False
        Me.txtKm.Name = "txtKm"
        Me.txtKm.Nombre = "Kilometraje:"
        Me.txtKm.Size = New System.Drawing.Size(130, 28)
        Me.txtKm.TabIndex = 8
        Me.txtKm.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtKm.ToolTip = ""
        '
        'txtTecnico
        '
        Me.txtTecnico.Descripcion = "ZctSOTLabelDesc1"
        Me.txtTecnico.Location = New System.Drawing.Point(12, 19)
        Me.txtTecnico.Name = "txtTecnico"
        Me.txtTecnico.Nombre = "Técnico:"
        Me.txtTecnico.Size = New System.Drawing.Size(475, 30)
        Me.txtTecnico.SPName = Nothing
        Me.txtTecnico.SPParametros = Nothing
        Me.txtTecnico.SpVariables = Nothing
        Me.txtTecnico.SqlBusqueda = Nothing
        Me.txtTecnico.TabIndex = 0
        Me.txtTecnico.Tabla = Nothing
        Me.txtTecnico.Tag = "Técnico que realizo el trabajo, presione [F3] para buscar"
        Me.txtTecnico.TextWith = 60
        Me.txtTecnico.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtTecnico.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtTecnico, "Técnico que realizo el trabajo, presione [F3] para buscar")
        Me.txtTecnico.Validar = True
        '
        'ZctSOTGroupBox3
        '
        Me.ZctSOTGroupBox3.Controls.Add(Me.ZctHistAprov)
        Me.ZctSOTGroupBox3.Controls.Add(Me.chkAprobada)
        Me.ZctSOTGroupBox3.Location = New System.Drawing.Point(427, 348)
        Me.ZctSOTGroupBox3.Name = "ZctSOTGroupBox3"
        Me.ZctSOTGroupBox3.Size = New System.Drawing.Size(496, 47)
        Me.ZctSOTGroupBox3.TabIndex = 11
        Me.ZctSOTGroupBox3.TabStop = False
        '
        'ZctHistAprov
        '
        Me.ZctHistAprov.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ZctHistAprov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZctHistAprov.Location = New System.Drawing.Point(83, 14)
        Me.ZctHistAprov.Name = "ZctHistAprov"
        Me.ZctHistAprov.Size = New System.Drawing.Size(402, 19)
        Me.ZctHistAprov.TabIndex = 2
        Me.ZctHistAprov.Text = "Pendiente de aprobar"
        Me.ZctHistAprov.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkAprobada
        '
        Me.chkAprobada.AutoSize = True
        Me.chkAprobada.Location = New System.Drawing.Point(7, 16)
        Me.chkAprobada.Name = "chkAprobada"
        Me.chkAprobada.Size = New System.Drawing.Size(72, 17)
        Me.chkAprobada.TabIndex = 0
        Me.chkAprobada.Text = "Aprobada"
        Me.chkAprobada.UseVisualStyleBackColor = True
        '
        'ZctGroupControls7
        '
        Me.ZctGroupControls7.Controls.Add(Me.DtAplicacion)
        Me.ZctGroupControls7.Location = New System.Drawing.Point(3, 493)
        Me.ZctGroupControls7.Name = "ZctGroupControls7"
        Me.ZctGroupControls7.Size = New System.Drawing.Size(405, 47)
        Me.ZctGroupControls7.TabIndex = 9
        Me.ZctGroupControls7.TabStop = False
        Me.ZctGroupControls7.Visible = False
        '
        'DtAplicacion
        '
        Me.DtAplicacion.DateWith = 91
        Me.DtAplicacion.Location = New System.Drawing.Point(54, 14)
        Me.DtAplicacion.Name = "DtAplicacion"
        Me.DtAplicacion.Nombre = "Fecha Aplicación:"
        Me.DtAplicacion.Size = New System.Drawing.Size(296, 27)
        Me.DtAplicacion.TabIndex = 0
        Me.DtAplicacion.Tag = "Fecha y hora en la que se va aplicar el movimiento"
        Me.ZctSOTToolTip1.SetToolTip(Me.DtAplicacion, "Fecha y hora en la que se va aplicar el movimiento")
        Me.DtAplicacion.Value = New Date(2018, 2, 25, 19, 40, 48, 0)
        '
        'ZctGroupControls4
        '
        Me.ZctGroupControls4.Controls.Add(Me.txtTrabajo)
        Me.ZctGroupControls4.Location = New System.Drawing.Point(2, 339)
        Me.ZctGroupControls4.Name = "ZctGroupControls4"
        Me.ZctGroupControls4.Size = New System.Drawing.Size(405, 94)
        Me.ZctGroupControls4.TabIndex = 5
        Me.ZctGroupControls4.TabStop = False
        '
        'txtTrabajo
        '
        Me.txtTrabajo.Location = New System.Drawing.Point(6, 9)
        Me.txtTrabajo.Multiline = True
        Me.txtTrabajo.Name = "txtTrabajo"
        Me.txtTrabajo.Nombre = "Trabajo a realizar:"
        Me.txtTrabajo.Size = New System.Drawing.Size(393, 77)
        Me.txtTrabajo.TabIndex = 0
        Me.txtTrabajo.Tag = "Descripción del trabajo a realizar"
        Me.txtTrabajo.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtTrabajo.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtTrabajo, "Descripción del trabajo a realizar")
        '
        'ZctGroupControls3
        '
        Me.ZctGroupControls3.Controls.Add(Me.lblDevolucion)
        Me.ZctGroupControls3.Controls.Add(Me.lblSurtido)
        Me.ZctGroupControls3.Controls.Add(Me.lblPorSurtir)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel3)
        Me.ZctGroupControls3.Controls.Add(Me.lblSubTotal)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel2)
        Me.ZctGroupControls3.Controls.Add(Me.lblIva)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel1)
        Me.ZctGroupControls3.Controls.Add(Me.lblTotal)
        Me.ZctGroupControls3.Controls.Add(Me.DGridArticulos)
        Me.ZctGroupControls3.Location = New System.Drawing.Point(2, 115)
        Me.ZctGroupControls3.Name = "ZctGroupControls3"
        Me.ZctGroupControls3.Size = New System.Drawing.Size(920, 218)
        Me.ZctGroupControls3.TabIndex = 4
        Me.ZctGroupControls3.TabStop = False
        '
        'lblDevolucion
        '
        Me.lblDevolucion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblDevolucion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDevolucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDevolucion.Location = New System.Drawing.Point(183, 200)
        Me.lblDevolucion.Name = "lblDevolucion"
        Me.lblDevolucion.Size = New System.Drawing.Size(81, 15)
        Me.lblDevolucion.TabIndex = 9
        Me.lblDevolucion.Text = "Devolución"
        Me.lblDevolucion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSurtido
        '
        Me.lblSurtido.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblSurtido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSurtido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSurtido.Location = New System.Drawing.Point(96, 200)
        Me.lblSurtido.Name = "lblSurtido"
        Me.lblSurtido.Size = New System.Drawing.Size(81, 15)
        Me.lblSurtido.TabIndex = 8
        Me.lblSurtido.Text = "Surtido"
        Me.lblSurtido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPorSurtir
        '
        Me.lblPorSurtir.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblPorSurtir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPorSurtir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPorSurtir.Location = New System.Drawing.Point(9, 200)
        Me.lblPorSurtir.Name = "lblPorSurtir"
        Me.lblPorSurtir.Size = New System.Drawing.Size(81, 15)
        Me.lblPorSurtir.TabIndex = 7
        Me.lblPorSurtir.Text = "Por Surtir"
        Me.lblPorSurtir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ZctSOTLabel3
        '
        Me.ZctSOTLabel3.AutoSize = True
        Me.ZctSOTLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel3.Location = New System.Drawing.Point(439, 200)
        Me.ZctSOTLabel3.Name = "ZctSOTLabel3"
        Me.ZctSOTLabel3.Size = New System.Drawing.Size(62, 13)
        Me.ZctSOTLabel3.TabIndex = 1
        Me.ZctSOTLabel3.Text = "SubTotal:"
        Me.ZctSOTLabel3.Visible = False
        '
        'lblSubTotal
        '
        Me.lblSubTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTotal.Location = New System.Drawing.Point(507, 200)
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblSubTotal.TabIndex = 2
        Me.lblSubTotal.Text = "$0"
        Me.lblSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSubTotal.Visible = False
        '
        'ZctSOTLabel2
        '
        Me.ZctSOTLabel2.AutoSize = True
        Me.ZctSOTLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel2.Location = New System.Drawing.Point(617, 200)
        Me.ZctSOTLabel2.Name = "ZctSOTLabel2"
        Me.ZctSOTLabel2.Size = New System.Drawing.Size(31, 13)
        Me.ZctSOTLabel2.TabIndex = 3
        Me.ZctSOTLabel2.Text = "IVA:"
        Me.ZctSOTLabel2.Visible = False
        '
        'lblIva
        '
        Me.lblIva.BackColor = System.Drawing.Color.LightBlue
        Me.lblIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIva.Location = New System.Drawing.Point(654, 200)
        Me.lblIva.Name = "lblIva"
        Me.lblIva.Size = New System.Drawing.Size(104, 15)
        Me.lblIva.TabIndex = 4
        Me.lblIva.Text = "$0"
        Me.lblIva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblIva.Visible = False
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(764, 201)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(40, 13)
        Me.ZctSOTLabel1.TabIndex = 5
        Me.ZctSOTLabel1.Text = "Total:"
        '
        'lblTotal
        '
        Me.lblTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(810, 199)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "$0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DGridArticulos
        '
        Me.DGridArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGridArticulos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColArt, Me.ColDesc, Me.ColAlmacen, Me.ColMot, Me.ColExist, Me.ColSol, Me.ColSurt, Me.ColPrecio, Me.ColCosto, Me.SubTotal, Me.ColCod, Me.Tipo})
        Me.DGridArticulos.Location = New System.Drawing.Point(9, 16)
        Me.DGridArticulos.Name = "DGridArticulos"
        Me.DGridArticulos.Size = New System.Drawing.Size(905, 180)
        Me.DGridArticulos.TabIndex = 0
        Me.DGridArticulos.Tag = "En esta cuadricula se dan de alta los artículos a surtir, es indispensable que in" & _
    "serta la candidad a surtir para agregar más artículos"
        Me.ZctSOTToolTip1.SetToolTip(Me.DGridArticulos, "En esta cuadricula se dan de alta los artículos a surtir, es indispensable que in" & _
        "serta la candidad a surtir para agregar más artículos")
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelaOrden)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdImprimir)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdAceptar)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(3, 436)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(920, 51)
        Me.ZctSOTGroupBox1.TabIndex = 8
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'cmdCancelaOrden
        '
        Me.cmdCancelaOrden.Location = New System.Drawing.Point(676, 17)
        Me.cmdCancelaOrden.Name = "cmdCancelaOrden"
        Me.cmdCancelaOrden.Size = New System.Drawing.Size(112, 28)
        Me.cmdCancelaOrden.TabIndex = 4
        Me.cmdCancelaOrden.Tag = "Cancela la Orden y regresa los artículos que fuerón surtidos"
        Me.cmdCancelaOrden.Text = "Cancelar Orden"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelaOrden, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
        "alla")
        Me.cmdCancelaOrden.UseVisualStyleBackColor = True
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Location = New System.Drawing.Point(440, 17)
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Size = New System.Drawing.Size(112, 28)
        Me.cmdImprimir.TabIndex = 0
        Me.cmdImprimir.Text = "Imprimir"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdImprimir, "Imprime la orden de trabajo")
        Me.cmdImprimir.UseVisualStyleBackColor = True
        Me.cmdImprimir.Visible = False
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(802, 17)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(112, 28)
        Me.cmdCancelar.TabIndex = 2
        Me.cmdCancelar.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
    "alla"
        Me.cmdCancelar.Text = "Cancelar Edición"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelar, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
        "alla")
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Location = New System.Drawing.Point(558, 17)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(112, 28)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Text = "Aceptar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdAceptar, "Graba las modificaciones realizadas, si el código es nuevo da de alta la orden de" & _
        " trabajo")
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'ZctGroupControls2
        '
        Me.ZctGroupControls2.Controls.Add(Me.txtVin)
        Me.ZctGroupControls2.Controls.Add(Me.txtContacto)
        Me.ZctGroupControls2.Controls.Add(Me.txtCliente)
        Me.ZctGroupControls2.Location = New System.Drawing.Point(3, 54)
        Me.ZctGroupControls2.Name = "ZctGroupControls2"
        Me.ZctGroupControls2.Size = New System.Drawing.Size(922, 56)
        Me.ZctGroupControls2.TabIndex = 3
        Me.ZctGroupControls2.TabStop = False
        Me.ZctGroupControls2.Text = "Clientes"
        '
        'txtVin
        '
        Me.txtVin.Descripcion = "ZctSOTLabelDesc1"
        Me.txtVin.Location = New System.Drawing.Point(18, 84)
        Me.txtVin.Name = "txtVin"
        Me.txtVin.Nombre = "Vehículo:"
        Me.txtVin.Size = New System.Drawing.Size(895, 28)
        Me.txtVin.SPName = Nothing
        Me.txtVin.SPParametros = Nothing
        Me.txtVin.SpVariables = Nothing
        Me.txtVin.SqlBusqueda = Nothing
        Me.txtVin.TabIndex = 13
        Me.txtVin.Tabla = Nothing
        Me.txtVin.Tag = "Vehículo a trabajar, para buscar presione [F3]"
        Me.txtVin.TextWith = 111
        Me.txtVin.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtVin.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtVin, "Vehículo a trabajar, para buscar presione [F3]")
        Me.txtVin.Validar = True
        Me.txtVin.Visible = False
        '
        'txtContacto
        '
        Me.txtContacto.Descripcion = "ZctSOTLabelDesc1"
        Me.txtContacto.Location = New System.Drawing.Point(18, 53)
        Me.txtContacto.Name = "txtContacto"
        Me.txtContacto.Nombre = "Contacto:"
        Me.txtContacto.Size = New System.Drawing.Size(895, 28)
        Me.txtContacto.SPName = Nothing
        Me.txtContacto.SPParametros = Nothing
        Me.txtContacto.SpVariables = Nothing
        Me.txtContacto.SqlBusqueda = Nothing
        Me.txtContacto.TabIndex = 12
        Me.txtContacto.Tabla = Nothing
        Me.txtContacto.Tag = "Cliente al cual pertenece la Motocicleta, para buscar presione [F3]"
        Me.txtContacto.TextWith = 111
        Me.txtContacto.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtContacto.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtContacto, "Cliente al cual pertenece la Motocicleta, para buscar presione [F3]")
        Me.txtContacto.Validar = True
        Me.txtContacto.Visible = False
        '
        'txtCliente
        '
        Me.txtCliente.Descripcion = "ZctSOTLabelDesc1"
        Me.txtCliente.Location = New System.Drawing.Point(29, 20)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Nombre = "Cliente:"
        Me.txtCliente.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCliente.Size = New System.Drawing.Size(884, 28)
        Me.txtCliente.SPName = Nothing
        Me.txtCliente.SPParametros = Nothing
        Me.txtCliente.SpVariables = Nothing
        Me.txtCliente.SqlBusqueda = Nothing
        Me.txtCliente.TabIndex = 0
        Me.txtCliente.Tabla = Nothing
        Me.txtCliente.Tag = "Cliente al cual pertenece la Motocicleta, para buscar presione [F3]"
        Me.txtCliente.TextWith = 111
        Me.txtCliente.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCliente.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtCliente, "Cliente al cual pertenece la Motocicleta, para buscar presione [F3]")
        Me.txtCliente.Validar = True
        '
        'ZCTFolio
        '
        Me.ZCTFolio.Controls.Add(Me.DTEntrada)
        Me.ZCTFolio.Controls.Add(Me.cmdDer)
        Me.ZCTFolio.Controls.Add(Me.cmdIzq)
        Me.ZCTFolio.Controls.Add(Me.txtFolio)
        Me.ZCTFolio.Location = New System.Drawing.Point(3, -4)
        Me.ZCTFolio.Name = "ZCTFolio"
        Me.ZCTFolio.Size = New System.Drawing.Size(922, 52)
        Me.ZCTFolio.TabIndex = 0
        Me.ZCTFolio.TabStop = False
        '
        'DTEntrada
        '
        Me.DTEntrada.DateWith = 91
        Me.DTEntrada.Location = New System.Drawing.Point(630, 16)
        Me.DTEntrada.Name = "DTEntrada"
        Me.DTEntrada.Nombre = "Fecha Entrada:"
        Me.DTEntrada.Size = New System.Drawing.Size(286, 27)
        Me.DTEntrada.TabIndex = 1
        Me.DTEntrada.Tag = "Fecha de entrada al taller"
        Me.ZctSOTToolTip1.SetToolTip(Me.DTEntrada, "Fecha de entrada al taller")
        Me.DTEntrada.Value = New Date(2018, 2, 25, 19, 40, 48, 0)
        Me.DTEntrada.Visible = False
        '
        'cmdDer
        '
        Me.cmdDer.Location = New System.Drawing.Point(207, 20)
        Me.cmdDer.Name = "cmdDer"
        Me.cmdDer.Size = New System.Drawing.Size(19, 23)
        Me.cmdDer.TabIndex = 4
        Me.cmdDer.Text = ">"
        Me.cmdDer.UseVisualStyleBackColor = True
        '
        'cmdIzq
        '
        Me.cmdIzq.Location = New System.Drawing.Point(186, 20)
        Me.cmdIzq.Name = "cmdIzq"
        Me.cmdIzq.Size = New System.Drawing.Size(19, 23)
        Me.cmdIzq.TabIndex = 3
        Me.cmdIzq.Text = "<"
        Me.cmdIzq.UseVisualStyleBackColor = True
        '
        'txtFolio
        '
        Me.txtFolio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.txtFolio.Location = New System.Drawing.Point(4, 19)
        Me.txtFolio.Multiline = False
        Me.txtFolio.Name = "txtFolio"
        Me.txtFolio.Nombre = "Folio:"
        Me.txtFolio.Size = New System.Drawing.Size(176, 28)
        Me.txtFolio.TabIndex = 0
        Me.txtFolio.Tag = "Folio de la orden de trabajo, si la orden es nueva, deje el código por default"
        Me.txtFolio.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtFolio.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtFolio, "Folio de la orden de trabajo, si la orden es nueva, deje el código por default")
        '
        'ZctGroupCtrFchSal
        '
        Me.ZctGroupCtrFchSal.Controls.Add(Me.DTSalida)
        Me.ZctGroupCtrFchSal.Location = New System.Drawing.Point(636, 96)
        Me.ZctGroupCtrFchSal.Name = "ZctGroupCtrFchSal"
        Me.ZctGroupCtrFchSal.Size = New System.Drawing.Size(287, 83)
        Me.ZctGroupCtrFchSal.TabIndex = 2
        Me.ZctGroupCtrFchSal.TabStop = False
        Me.ZctGroupCtrFchSal.Visible = False
        '
        'DTSalida
        '
        Me.DTSalida.DateWith = 91
        Me.DTSalida.Location = New System.Drawing.Point(3, 50)
        Me.DTSalida.Name = "DTSalida"
        Me.DTSalida.Nombre = "Fecha Salida:"
        Me.DTSalida.Size = New System.Drawing.Size(286, 27)
        Me.DTSalida.TabIndex = 0
        Me.DTSalida.Tag = "Fecha de salida al taller"
        Me.ZctSOTToolTip1.SetToolTip(Me.DTSalida, "Fecha de salida al taller")
        Me.DTSalida.Value = New Date(2018, 2, 25, 19, 40, 48, 0)
        '
        'ColArt
        '
        Me.ColArt.HeaderText = "Articulo"
        Me.ColArt.Name = "ColArt"
        Me.ColArt.Width = 120
        '
        'ColDesc
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue
        Me.ColDesc.DefaultCellStyle = DataGridViewCellStyle1
        Me.ColDesc.HeaderText = "Descripción"
        Me.ColDesc.Name = "ColDesc"
        Me.ColDesc.ReadOnly = True
        Me.ColDesc.Width = 200
        '
        'ColAlmacen
        '
        Me.ColAlmacen.HeaderText = "Almacen"
        Me.ColAlmacen.Name = "ColAlmacen"
        '
        'ColMot
        '
        Me.ColMot.HeaderText = "Vehículo"
        Me.ColMot.Name = "ColMot"
        Me.ColMot.Visible = False
        Me.ColMot.Width = 80
        '
        'ColExist
        '
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.ColExist.DefaultCellStyle = DataGridViewCellStyle2
        Me.ColExist.HeaderText = "Existencias"
        Me.ColExist.Name = "ColExist"
        Me.ColExist.ReadOnly = True
        Me.ColExist.Width = 70
        '
        'ColSol
        '
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = "0"
        Me.ColSol.DefaultCellStyle = DataGridViewCellStyle3
        Me.ColSol.HeaderText = "Solicitada"
        Me.ColSol.Name = "ColSol"
        Me.ColSol.Width = 70
        '
        'ColSurt
        '
        DataGridViewCellStyle4.Format = "N0"
        DataGridViewCellStyle4.NullValue = "0"
        Me.ColSurt.DefaultCellStyle = DataGridViewCellStyle4
        Me.ColSurt.HeaderText = "Surtido"
        Me.ColSurt.Name = "ColSurt"
        Me.ColSurt.Width = 70
        '
        'ColPrecio
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = "0"
        Me.ColPrecio.DefaultCellStyle = DataGridViewCellStyle5
        Me.ColPrecio.HeaderText = "Precio"
        Me.ColPrecio.Name = "ColPrecio"
        Me.ColPrecio.ReadOnly = True
        Me.ColPrecio.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ColPrecio.Visible = False
        Me.ColPrecio.Width = 70
        '
        'ColCosto
        '
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle6.Format = "C2"
        DataGridViewCellStyle6.NullValue = "0"
        Me.ColCosto.DefaultCellStyle = DataGridViewCellStyle6
        Me.ColCosto.HeaderText = "Costo"
        Me.ColCosto.Name = "ColCosto"
        Me.ColCosto.ReadOnly = True
        Me.ColCosto.Width = 70
        '
        'SubTotal
        '
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle7.Format = "C2"
        DataGridViewCellStyle7.NullValue = "0"
        Me.SubTotal.DefaultCellStyle = DataGridViewCellStyle7
        Me.SubTotal.HeaderText = "Sub Total"
        Me.SubTotal.Name = "SubTotal"
        Me.SubTotal.ReadOnly = True
        Me.SubTotal.Width = 80
        '
        'ColCod
        '
        Me.ColCod.HeaderText = "Codigo"
        Me.ColCod.Name = "ColCod"
        Me.ColCod.Visible = False
        '
        'Tipo
        '
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.Visible = False
        '
        'OrdenesTrabajoMantenimientoForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(929, 490)
        Me.Controls.Add(Me.ZctGroupControls6)
        Me.Controls.Add(Me.ZctSOTGroupBox3)
        Me.Controls.Add(Me.ZctGroupControls7)
        Me.Controls.Add(Me.ZctGroupControls4)
        Me.Controls.Add(Me.ZctGroupControls3)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.ZctGroupControls2)
        Me.Controls.Add(Me.ZCTFolio)
        Me.Controls.Add(Me.ZctGroupCtrFchSal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "OrdenesTrabajoMantenimientoForm"
        Me.Text = "Ordenes de Trabajo"
        Me.ZctGroupControls6.ResumeLayout(False)
        Me.ZctSOTGroupBox3.ResumeLayout(False)
        Me.ZctSOTGroupBox3.PerformLayout()
        Me.ZctGroupControls7.ResumeLayout(False)
        Me.ZctGroupControls4.ResumeLayout(False)
        Me.ZctGroupControls3.ResumeLayout(False)
        Me.ZctGroupControls3.PerformLayout()
        CType(Me.DGridArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        Me.ZctGroupControls2.ResumeLayout(False)
        Me.ZCTFolio.ResumeLayout(False)
        Me.ZctGroupCtrFchSal.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ZCTFolio As ZctSOT.ZctGroupControls
    Friend WithEvents ZctGroupControls2 As ZctSOT.ZctGroupControls
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents txtCliente As ZctSOT.ZctControlBusqueda
    Friend WithEvents txtFolio As ZctSOT.ZctControlTexto
    Friend WithEvents ZctGroupControls3 As ZctSOT.ZctGroupControls
    Friend WithEvents DGridArticulos As ZctSOT.ZctSotGrid
    Friend WithEvents ZctGroupControls4 As ZctSOT.ZctGroupControls
    Friend WithEvents txtTrabajo As ZctSOT.ZctControlTexto
    Friend WithEvents ZctGroupCtrFchSal As ZctSOT.ZctGroupControls
    Friend WithEvents DTSalida As ZctSOT.ZctControlFecha
    Friend WithEvents ZctGroupControls6 As ZctSOT.ZctGroupControls
    Friend WithEvents txtTecnico As ZctSOT.ZctControlBusqueda
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblDevolucion As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblSurtido As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents lblPorSurtir As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents cmdImprimir As ZctSOT.ZctSOTButton
    Friend WithEvents ZctSOTToolTip1 As ZctSOT.ZctSOTToolTip
    Friend WithEvents ZctGroupControls7 As ZctSOT.ZctGroupControls
    Friend WithEvents DtAplicacion As ZctSOT.ZctControlFecha
    Friend WithEvents cmdDer As ZctSOT.ZctSOTButton
    Friend WithEvents cmdIzq As ZctSOT.ZctSOTButton
    Friend WithEvents ZctSOTGroupBox3 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents ZctHistAprov As ZctSOT.ZctSOTLabel
    Friend WithEvents chkAprobada As System.Windows.Forms.CheckBox
    Friend WithEvents cmdCancelaOrden As ZctSOT.ZctSOTButton
    Friend WithEvents ZctSOTLabel3 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblSubTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTLabel2 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblIva As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents txtContacto As ZctSOT.ZctControlBusqueda
    Friend WithEvents CboTpOrden As ZctSOT.ZctControlCombo
    Friend WithEvents txtKm As ZctSOT.ZctControlTexto
    Friend WithEvents DTEntrada As ZctSOT.ZctControlFecha
    Friend WithEvents txtVin As ZctSOT.ZctControlBusqueda
    Friend WithEvents ColArt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAlmacen As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ColMot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColExist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSurt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColPrecio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
