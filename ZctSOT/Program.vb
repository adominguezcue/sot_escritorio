﻿Imports System.Security.Permissions
Imports System.Data.Entity.Validation
Imports System.Drawing.Text

Module Program

    <STAThread>
    <SecurityPermission(SecurityAction.Demand, Flags:=SecurityPermissionFlag.ControlAppDomain)>
    Public Sub Main(args As String())

        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException)
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf MyApplication_UnhandledException
        AddHandler Application.ThreadException, AddressOf ThreadExceptionEvent
        Application.Run(New ZctSOTMain)
    End Sub

    Private Sub MyApplication_UnhandledException(sender As Object, e As UnhandledExceptionEventArgs)
#If DEBUG Then
        If e.ExceptionObject.GetType() = GetType(DbEntityValidationException) Then

        End If
#End If

        MessageBox.Show(CType(e.ExceptionObject, Exception).Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

    Private Sub ThreadExceptionEvent(sender As Object, e As Threading.ThreadExceptionEventArgs)
#If DEBUG Then
        If e.Exception.GetType() = GetType(DbEntityValidationException) Then

        End If
#End If
        MessageBox.Show(e.Exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Sub

End Module
