﻿Imports ZctSOT.Datos
Imports SOTControladores.Controladores

Public Class ZctInvMotos
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
    Dim _Guardar As Boolean
    Private _Rdn As New RDN.ZctRDNInvMotos
    Private _BS As New BindingSource

    Private Sub ZctInvMotos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
            With _Permisos
                grdDatos.AllowUserToAddRows = .agrega
                grdDatos.AllowUserToDeleteRows = .elimina
                grdDatos.ReadOnly = Not (.edita Or .agrega)
                If .edita = False And .agrega = False And .elimina = False Then
                    _Guardar = False
                Else
                    _Guardar = True
                End If
                cmdBuscaArchivo.Enabled = .agrega
            End With
            'txtFolio.DataBindings.Clear()
            txtFolio.Text = _Rdn.Folio

            txtCliente.SqlBusqueda = "SELECT [Cod_Cte] as Código ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
            'Asigna el nombre del procedimiento almacenado para la busqueda
            txtCliente.SPName = "SP_ZctCatCte"
            'Envia las variables del procedimiento almacenado
            txtCliente.SpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
            'Envia la tabla de referencia al procedimiento almacenado
            txtCliente.Tabla = "ZctCatCte_Busqueda"
            pHabilita(1)
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al iniciar la aplicación")
        End Try
    End Sub

    Private Sub plimpia()
        txtFolio.Text = _Rdn.Folio
        _Rdn.Datos.Clear()
        _Rdn.Cod_Cte = 0
        lblArchivo.Text = ""
        txtCliente.Text = ""
        txtCliente.Descripcion = ""
        GetGrid()
    End Sub

    Private Sub cmdBuscaArchivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuscaArchivo.Click
        Try
            ' esto va a ir dentro del Datos.DAO
            OpDialog.ShowDialog()
            If OpDialog.FileName <> "" Then
                lblArchivo.Text = OpDialog.FileName
                _Rdn.CargaExcel(OpDialog.FileName, txtFolio.Text)
                _BS.DataSource = Nothing
                _BS.DataSource = _Rdn.Datos.Values
                GetGrid()
                grdDatos.DataSource = _BS
                pHabilita(3)
                grdDatos.AutoResizeColumns()
            End If
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al cargar los datos")
        End Try

    End Sub



    Private Sub cmdCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCargar.Click
        If txtCliente.Text = "" Then
            MsgBox("Debe proporcionar el cliente.")
            txtCliente.Focus()
            Exit Sub
        End If
        Try
            _Rdn.CargaDatos(txtFolio.Text, CType(txtCliente.Text, Integer))
            _BS.DataSource = _Rdn.Datos.Values
            GetGrid()
            grdDatos.DataSource = _BS
            grdDatos.AutoResizeColumns()

            pHabilita(2)
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al cargar los datos")
        End Try
    End Sub


    Public Sub pHabilita(ByVal Estado As Integer)
        Dim bInicial As Boolean = False
        Dim bExcel As Boolean = False
        Dim bGraba As Boolean = False
        Dim bSoloLectura As Boolean = False
        Select Case Estado
            Case 1
                bInicial = True
            Case 2
                bExcel = True
            Case 3
                bGraba = True
            Case 4
                bExcel = False
                bGraba = False
                bSoloLectura = True
        End Select
        txtFolio.Enabled = bInicial
        cmdIzq.Enabled = bInicial
        cmdDer.Enabled = bInicial
        txtCliente.Enabled = bInicial
        cmdCargar.Enabled = bInicial
        lblArchivo.Enabled = bExcel
        cmdBuscaArchivo.Enabled = bExcel
        cmdAceptar.Enabled = bGraba
        grdDatos.Enabled = bGraba Or bSoloLectura
        cmdCancelar.Enabled = True
    End Sub

    Public Sub GetGrid()


        If grdDatos.DataSource IsNot Nothing Then grdDatos.DataSource = Nothing
        grdDatos.Columns.Clear()
        grdDatos.AutoGenerateColumns = False
        Dim Habilita As Boolean = False
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Cod_Mot", "Cod_Mot", "Cod_Mot", True, , , True))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Vin_Mot", "Vin", "Vin_Mot", True, , , True))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Desc_Mod", "Modelo", "Desc_Mod", True, , , True))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Anno_Mot", "Año", "Anno_Mot", True, , , True))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Desc_Mar", "Marca", "Desc_Mar", True, , , True))
        grdDatos.Columns.Add(zctColumnComun.GetColumn("Estatus_Text", "Estatus", "Estatus_Text", True, , , True))
        grdDatos.Columns.Add(zctColumnCombo.GetColumn("Accion", "Accion", "Accion", True, _Rdn.DiccAcciones))
        grdDatos.Columns.Add(zctColumnCombo.GetColumn("Cod_Ubicacion", "Ubicacion", "Cod_Ubicacion", True, New ZctDiccUbicaciones()))


    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        pHabilita(1)
        plimpia()
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try
            If MsgBox("¿Esta seguro que desea guardar los cambios?", MsgBoxStyle.YesNo, "SOT") = MsgBoxResult.No Then Exit Sub
            _Rdn.Guardar(txtFolio.Text, CType(txtCliente.Text, Integer))
            pHabilita(1)
            plimpia()
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al guardar los datos")
        End Try
    End Sub


    Private Sub grdDatos_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grdDatos.DataError
        MsgBox(e.Exception.Message)
        e.Cancel = True
    End Sub

    Public Sub CargaFolio()
        Try
            If Not IsNumeric(txtFolio.Text) Then txtFolio.Text = _Rdn.Folio : Exit Sub
            If CInt(txtFolio.Text) >= _Rdn.Folio Then txtFolio.Text = _Rdn.Folio : Exit Sub
            _Rdn.CargaDatos(txtFolio.Text)
            If _Rdn.Datos.Count = 0 Then Exit Sub
            txtCliente.Text = _Rdn.Cod_Cte.ToString
            txtCliente.pCargaDescripcion()
            _BS.DataSource = _Rdn.Datos.Values
            GetGrid()
            grdDatos.DataSource = _BS
            grdDatos.AutoResizeColumns()

            pHabilita(4)
        Catch ex As ZctReglaNegocioEx
            MsgBox(ex.Message)
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al cargar los datos")
        End Try
    End Sub

    Private Sub txtFolio_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolio.lostfocus

        CargaFolio()
    End Sub
End Class