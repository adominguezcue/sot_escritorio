﻿'Imports DevExpress.rXtraBars
'Imports DevExpess.XtraTabbedMdi
Imports Microsoft.Win32
Imports System.Configuration
Imports System.Timers
Imports System.Diagnostics
Imports ConviertePDF
Imports ResTotal.Vista.CxP
Imports System.Threading
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows.Forms
Imports ZctSOT.Datos
Imports SOTControladores.Controladores
Imports MixItems.View

Public Class RibbonMain
    Dim _Controlador As Permisos.Controlador.Controlador
    'Public _UsuAct As String = ""
    'Public _NomUsuAct As String = ""
    'Public _UsuAut As String = ""
    'Public _EsVendedor As Boolean
    Public ParametrosSistema As New Datos.Clases.Sistema.ZctCatPar
    Private DAOSistema As New Datos.DAO.ClProcesaCatalogos()

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Dim frm As New XtraForm1()

        frm.MdiParent = Me
        frm.Show()
    End Sub


    'Private Sub CreaLlaves()
    '    Try
    '        'Dim ruta As String = "Software\ODBC\ODBC.INI\ODBC File DSN"
    '        'HKEY_LOCAL_MACHINE\Software 
    '        Dim key As RegistryKey = Registry.LocalMachine.OpenSubKey("Software", True)
    '        If key.OpenSubKey("ZctSOT") Is Nothing Then
    '            Dim newkey As RegistryKey = key.CreateSubKey("ZctSOT")
    '            newkey.SetValue("Server", ConfigurationManager.AppSettings.Get("Server"))
    '            newkey.SetValue("Database", ConfigurationManager.AppSettings.Get("Database"))
    '            newkey.SetValue("User", ConfigurationManager.AppSettings.Get("User"))
    '        End If
    '    Catch ex As Exception
    '        Trace.Write(ex.Message)
    '        Trace.Flush()
    '    End Try

    'End Sub

    Private Sub RibbonMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            'Dim splash As New ZctIntro
            'CreaLlaves()
            'Actualizaciones

            'splash.ShowDialog()

            Dim fLogin As New ZctLogin
            'fLogin.Cod_Aut = 0
            fLogin.ShowDialog(Me)
            If fLogin.Valida = False Then
                Me.Dispose()
            End If
            '_Controlador = New Permisos.Controlador.Controlador(Datos.DAO.Coneccion.CadenaConexion)
            'If (IsNumeric(_UsuAct)) Then
            '    _Controlador.Recorrermenu(_UsuAct, Me.MenuStrip)
            'End If
            'MenuStrip.Visible = True
            'ActualizacionesIniciales()
            ActualizaParametros()
            'GetPendientesPorEnviar()
            'If _EsVendedor Then
            '    CreaCaja(0)
            'End If


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Public Sub ActualizaParametros()
        If ParametrosSistema Is Nothing Then ParametrosSistema = New Datos.Clases.Sistema.ZctCatPar
        DAOSistema.CargaDatos(ParametrosSistema)
    End Sub


    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBArticulos.ItemClick
        Dim fArticulosBusqueda As New ZctCatArt
        AddHandler fArticulosBusqueda.ImprimeEtiqueta, AddressOf Imprime_etiqueta_anaquel
        AddHandler fArticulosBusqueda.ImprimeEtiquetaArticulo, AddressOf Imprime_etiqueta_articulo
        fArticulosBusqueda.MdiParent = Me
        fArticulosBusqueda.Show()
    End Sub


    Private Sub AlmacenesToolStripMenuItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBAlmacenes.ItemClick

        Dim fCatAlm As New ZctFrmComunGenLinea ' ZctfrmComun
        fCatAlm.EnumGenerico = New Clases.Catalogos.ListaAlmacenes
        fCatAlm.Text = "Catálogo de Almacenes"
        fCatAlm.MdiParent = Me
        fCatAlm.Show()
    End Sub

    Private Sub DepartamentosToolStripMenuItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBCategorias.ItemClick
        Dim fDptoBusqueda As New ZctfrmComunGen
        fDptoBusqueda.Height = 400
        fDptoBusqueda.Width = 400
        fDptoBusqueda.Text = "Catálogo de Categorias"
        fDptoBusqueda.EnumGenerico = New Clases.Catalogos.listaDepartamentos
        fDptoBusqueda.MdiParent = Me
        fDptoBusqueda.Show()
    End Sub

    Private Sub LíneasToolStripMenuItem_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBSubCategorias.ItemClick
        Dim fLineaBusqueda As New ZctFrmComunGenLinea
        'fLineaBusqueda.sTablaName = "ZctCatLinea"
        'fLineaBusqueda.sSPName = "SP_ZctCatLinea"
        'fLineaBusqueda.sSpVariables = "@Codigo;INT|@Descripcion;VARCHAR|@Cod_Dpto;INT|@ColNuevo;INT"
        'fLineaBusqueda.iColBusq = 2
        'fLineaBusqueda.sSPName_Busq = "SP_ZctCatDpto_BusquedaGen"
        'fLineaBusqueda.sSpVariables_Busq = "@Codigo;INT" '|@Descripcion;VARCHAR"
        'fLineaBusqueda.sTabla_Busq = "ZctCatDpto"
        'fLineaBusqueda.Height = 400
        'fLineaBusqueda.Width = 400
        fLineaBusqueda.Text = "Catálogo de Subcategorias"
        fLineaBusqueda.MdiParent = Me
        fLineaBusqueda.EnumGenerico = New Clases.Catalogos.listaLineas
        fLineaBusqueda.Show()
    End Sub

    Private Sub BBMarcas_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBMarcas.ItemClick
        Dim fMarcaBusqueda As New ZctfrmComunGen
        fMarcaBusqueda.EnumGenerico = New Clases.Catalogos.ListaMarcas
        fMarcaBusqueda.Text = "Catálogo de Marcas"
        fMarcaBusqueda.MdiParent = Me
        fMarcaBusqueda.Show()
    End Sub

    Private Sub BBInventario_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBInventario.ItemClick
        Dim frmInv As New ZctComparaInventarios
        frmInv.MdiParent = Me
        frmInv.Show()
    End Sub

    Private Sub BBIntercambios_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        Dim invart As New ZctIncAlmacenes
        invart.MdiParent = Me
        invart.Show()
    End Sub

    Private Sub BBReportesInventarios_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBReportesInventarios.ItemClick
        Dim fShowRpt As New ZctShowRpt
        fShowRpt.MdiParent = Me
        '1 para inventarios
        fShowRpt.iCodCatego = 1
        fShowRpt.Show()
    End Sub

    Private Sub BBClientes_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBClientes.ItemClick
        Dim fCatcte As New ZctClientes
        fCatcte.StartPosition = FormStartPosition.CenterScreen
        fCatcte.MdiParent = Me
        fCatcte.Show()
    End Sub

    Private Sub BBEstados_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBEstados.ItemClick
        Dim fEdoBusqueda As New ZctfrmComunGen
        'fEdoBusqueda.sTablaName = "ZctCatEdo"
        'fEdoBusqueda.sSPName = "SP_ZctCatEdo"
        'fEdoBusqueda.sSpVariables = "@Codigo;INT|@Descripcion;VARCHAR|@ColNuevo;INT"
        'fEdoBusqueda.iColBusq = 0
        'fEdoBusqueda.sSPName_Busq = ""
        'fEdoBusqueda.sSpVariables_Busq = ""
        'fEdoBusqueda.sTabla_Busq = ""
        'fEdoBusqueda.Height = 400
        'fEdoBusqueda.Width = 400
        fEdoBusqueda.EnumGenerico = New Clases.Catalogos.ListaEstados
        fEdoBusqueda.Text = "Catálogo de Estados"
        fEdoBusqueda.MdiParent = Me
        fEdoBusqueda.Show()
    End Sub

    Private Sub BBCiudades_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBCiudades.ItemClick
        Dim fCiuBusqueda As New ZctFrmComunGenCiudad
        fCiuBusqueda.EnumGenerico = New Clases.Catalogos.listaCiudades
        fCiuBusqueda.Text = "Catálogo de Ciudades"
        fCiuBusqueda.MdiParent = Me
        fCiuBusqueda.Show()
    End Sub

    Private Sub BBCuentasXCobrar_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBCuentasXCobrar.ItemClick
        Dim CxP As New ResTotal.Vista.CxP_principal(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id, "CJ")
        AddHandler CxP.cambiaproveedor, AddressOf ModificaProveedor
        CxP.MdiParent = Me
        CxP.StartPosition = FormStartPosition.CenterParent
        CxP.Show()
    End Sub

    Private Sub ModificaProveedor(cod_prov As Integer)
        Dim frm As New ResTotal.Vista.ZctCatProv(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id)
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CargaProveedor(cod_prov)
        frm.ShowDialog()
    End Sub

    Private Sub BBCaja_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBCaja.ItemClick
        CreaCaja(0)
    End Sub
    Friend Sub CreaCaja(folio_pedido As Integer)
        'If IsNumeric(_UsuAct) = False Then
        '    Exit Sub
        'End If

        If IsNothing(ControladorBase.UsuarioActual) Then
            Exit Sub
        End If

        Dim Caja As New ResTotal.Vista.frmCaja(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id, "CJ", ParametrosSistema.cod_cte_default)
        AddHandler Caja.LanzaCliente, AddressOf ModificaCliente
        AddHandler Caja.LanzaOT, AddressOf ModificaOT
        AddHandler Caja.Lanzaimpresion, AddressOf imprime_caja
        AddHandler Caja.Autoriza, AddressOf AutorizaCaja
        AddHandler Caja.CreaOT, AddressOf CreatOT
        AddHandler Caja.LanzaBuscaDeCliente, AddressOf BuscaCliente
        AddHandler Caja.ImprimeRetiro, AddressOf imprime_retiro
        AddHandler Caja.ImprimeCorte, AddressOf imprime_corte
        AddHandler Caja.LanzaBusquedaArticulos, AddressOf BusquedaArticulos
        Caja.MdiParent = Me
        Caja.Show()
        If folio_pedido > 0 Then
            Caja.CargaFolio(folio_pedido)
        End If
    End Sub

    Private Function BuscaCliente() As Integer

        ''Asigna los datos de la busqueda
        'txtCliente.SqlBusqueda = "SELECT [Cod_Cte] as Código ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
        ''Asigna el nombre del procedimiento almacenado para la busqueda
        'txtCliente.SPName = 
        ''Envia las variables del procedimiento almacenado
        'txtCliente.SpVariables =
        ''Envia la tabla de referencia al procedimiento almacenado
        'txtCliente.Tabla = "ZctCatCte_Busqueda"
        Dim fBusqueda As New ZctBusqueda
        fBusqueda.sSPName = "SP_ZctCatCte"
        fBusqueda.sSpVariables = "@Cod_Cte;INT|@Nom_Cte;VARCHAR|@ApPat_Cte;VARCHAR|@ApMat_Cte;VARCHAR"
        fBusqueda.ShowDialog(Me)
        Return fBusqueda.iValor
    End Function

    Private Function CreatOT() As Integer
        Dim frm As New ZctOrdTrabajo()
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()

        Return 0
    End Function

    Private Function ModificaOT(cod_OT As Long) As Integer
        Dim frm As New ZctOrdTrabajo()
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.CargaOT(cod_OT)
        frm.ShowDialog()

        Return 0
    End Function
    Private Function ModificaCliente(cod_cte As Integer) As Integer
        Dim frm As New ZctClientes()
        frm.CargaCliente(cod_cte)
        frm.StartPosition = FormStartPosition.CenterScreen

        'frm.MdiParent = Me
        frm.ShowDialog()
        'frm.ShowDialog()

        Return 0
    End Function
    Private Function BusquedaArticulos()
        Dim fBusqueda As New ZctBusqueda
        fBusqueda.sSPName = "SP_ZctCatArt"
        fBusqueda.sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT"
        fBusqueda.ShowDialog(Me)
        Return fBusqueda.iValor
    End Function
    Private Sub AutorizaCaja(cod_auto As Integer, Padre As ResTotal.Vista.frmCaja)
        Dim Login As New ZctLogin
        Login.Cod_Aut = cod_auto
        Login.ShowDialog()
        Padre._PermisoCancelar = Login.Valida = True
    End Sub

    Private Sub Imprime_directo(pathRpt_ruta As String, sql As String, copias As Integer)
        Dim crTableLogonInfo As New TableLogOnInfo
        Dim crTableLogonInfos As New TableLogOnInfos
        Dim loginfo As CrystalDecisions.Shared.ConnectionInfo
        loginfo = New CrystalDecisions.Shared.ConnectionInfo
        loginfo.ServerName = Datos.DAO.Conexion.Server
        loginfo.DatabaseName = Datos.DAO.Conexion.Database
        loginfo.UserID = Datos.DAO.Conexion.User
        loginfo.Password = "L0k0m0t0r4"
        crTableLogonInfo.ConnectionInfo = loginfo
        crTableLogonInfos.Add(crTableLogonInfo)


        Dim reporte As New ReportDocument

        'Dim pathRpt_ruta As String = "C:\Reportes\ZctRptCJ2.rpt"

        reporte.Load(pathRpt_ruta, OpenReportMethod.OpenReportByDefault)
        reporte.RecordSelectionFormula = sql
        Dim CrTables As Tables
        Dim CrTable As Table
        reporte.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        ' crConnectionInfo = loginfo
        CrTables = reporte.Database.Tables
        For Each subRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument In reporte.Subreports
            subRpt.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        Next

        For Each CrTable In CrTables
            CrTable.ApplyLogOnInfo(crTableLogonInfo)
        Next

        reporte.PrintToPrinter(copias, True, 0, 0)

    End Sub

    Private Sub imprime_corte(FolioRetiro As Integer)
        Imprime_directo("C:\Reportes\rptCortecaja.rpt", "{ZctRetirosCajas.cod_retiro} = " + FolioRetiro, 2)
    End Sub


    'Private Sub imprime_retiro(FolioRetiro As Integer)
    '    Imprime_directo("c:\\Reportes\\rptRetirosCajas.rpt", "{ZctRetirosCajas.cod_retiro} = " + FolioRetiro)
    'End Sub

    Private Sub Imprime_etiqueta_articulo(Cod_Art As String)
        Imprime_directo("C:\Reportes\rptEtiquetaArt_pq.rpt", "{zctcatart.Cod_Art} = '" & Cod_Art & "'", 1)
    End Sub
    Private Sub Imprime_etiqueta_anaquel(Cod_Art As String)
        Imprime_directo("C:\Reportes\rptEtiquetaArt.rpt", "{zctcatart.Cod_Art} = '" & Cod_Art & "'", 1)
    End Sub
    Private Sub imprime_caja(Cod_folio As String, folio As Integer)
        Imprime_directo("C:\Reportes\ZctRptCJ2.rpt", "{VW_ZctTicked_Caja.Cod_Folio} = '" & Cod_folio & "' AND {VW_ZctTicked_Caja.CodEnc_Caja} = " & folio.ToString() & "", 2)

    End Sub

    Private Sub imprime_retiro(FolioRetiro As Integer)
        Dim crTableLogonInfo As New TableLogOnInfo
        Dim crTableLogonInfos As New TableLogOnInfos
        Dim loginfo As CrystalDecisions.Shared.ConnectionInfo
        loginfo = New CrystalDecisions.Shared.ConnectionInfo
        loginfo.ServerName = Datos.DAO.Conexion.Server ' ConfigurationManager.AppSettings.Get("Server")
        loginfo.DatabaseName = Datos.DAO.Conexion.Database  'ConfigurationManager.AppSettings.Get("Database")
        loginfo.UserID = Datos.DAO.Conexion.User 'ConfigurationManager.AppSettings.Get("User")
        loginfo.Password = "L0k0m0t0r4" 'ConfigurationManager.AppSettings.Get("Password")
        crTableLogonInfo.ConnectionInfo = loginfo
        crTableLogonInfos.Add(crTableLogonInfo)


        Dim reporte As New ReportDocument

        Dim pathRpt_ruta As String = "C:\Reportes\rptRetirosCajas.rpt"

        '//Dim myCr As New PkVisorRpt
        '//myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        'myCr.sSQLV = "{VW_ZctTicked_Caja.Cod_Folio} = '" & Cod_folio & "' AND {VW_ZctTicked_Caja.CodEnc_Caja} = " & folio.ToString() & ""



        reporte.Load(pathRpt_ruta, OpenReportMethod.OpenReportByDefault)


        'logonrpt(rpt)
        'If sSQLF <> "" Then
        reporte.RecordSelectionFormula = "{ZctRetirosCajas.cod_retiro} = " & Convert.ToInt32(FolioRetiro)
        'End If


        ' Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As Table
        reporte.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        ' crConnectionInfo = loginfo
        CrTables = reporte.Database.Tables
        For Each subRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument In reporte.Subreports
            subRpt.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        Next

        For Each CrTable In CrTables
            CrTable.ApplyLogOnInfo(crTableLogonInfo)
        Next

        reporte.PrintToPrinter(1, True, 0, 0)

    End Sub
    Private Sub imprime_corte(fondocaja As Decimal, cod_folio As String, cod_usu As Integer)
        Dim crTableLogonInfo As New TableLogOnInfo
        Dim crTableLogonInfos As New TableLogOnInfos
        Dim loginfo As CrystalDecisions.Shared.ConnectionInfo
        loginfo = New CrystalDecisions.Shared.ConnectionInfo
        loginfo.ServerName = Datos.DAO.Conexion.Server ' ConfigurationManager.AppSettings.Get("Server")
        loginfo.DatabaseName = Datos.DAO.Conexion.Database  'ConfigurationManager.AppSettings.Get("Database")
        loginfo.UserID = Datos.DAO.Conexion.User 'ConfigurationManager.AppSettings.Get("User")
        loginfo.Password = "L0k0m0t0r4" 'ConfigurationManager.AppSettings.Get("Password")
        crTableLogonInfo.ConnectionInfo = loginfo
        crTableLogonInfos.Add(crTableLogonInfo)


        Dim reporte As New ReportDocument

        Dim pathRpt_ruta As String = "C:\Reportes\rptCortecaja.rpt"


        '//Dim myCr As New PkVisorRpt
        '//myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        'myCr.sSQLV = "{VW_ZctTicked_Caja.Cod_Folio} = '" & Cod_folio & "' AND {VW_ZctTicked_Caja.CodEnc_Caja} = " & folio.ToString() & ""



        reporte.Load(pathRpt_ruta, OpenReportMethod.OpenReportByDefault)

        reporte.SetParameterValue("fondocaja", fondocaja)
        reporte.SetParameterValue("@cod_folio", cod_folio)
        reporte.SetParameterValue("cod_usu", cod_usu)
        reporte.RecordSelectionFormula = "{WV_ZCTtickedCaja.cod_usu} = " & cod_usu


        Dim CrTables As Tables
        Dim CrTable As Table
        reporte.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)

        CrTables = reporte.Database.Tables
        For Each subRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument In reporte.Subreports
            subRpt.SetDatabaseLogon(loginfo.UserID, loginfo.Password, loginfo.ServerName, loginfo.DatabaseName)
        Next

        For Each CrTable In CrTables
            CrTable.ApplyLogOnInfo(crTableLogonInfo)
        Next

        reporte.PrintToPrinter(1, True, 0, 0)

    End Sub

    Private Sub BBFacturacion_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBFacturacion.ItemClick
        Try


            Dim timbra As New FacturacionElectronica.Procesa.TimbrarCFDPrueba(AddressOf TimbrarCFD)
            Dim cancela As New FacturacionElectronica.Procesa.CancelarCFDI(AddressOf CancelarCFDI)
            Dim frm_facturacion As New ResTotal.Vista.frmFacturacion(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id, timbra, cancela)
            AddHandler frm_facturacion.lanza_cliente, AddressOf ModificaCliente
            AddHandler frm_facturacion.lanza_rpt, AddressOf CreaPdf
            AddHandler frm_facturacion.lanza_print, AddressOf imprime_factura

            frm_facturacion.MdiParent = Me
            frm_facturacion.Show()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub CreaPdf(folio As Integer, cod_folio As String, pathRpt As String, pathRpt_ruta As String, nombre As String, email As String)
        imprime_factura_dontshow(folio, cod_folio, pathRpt_ruta)
        Dim pdf As New toPDF
        pdf.CrystalReportFileNameFullPath = pathRpt_ruta  ' My.Settings.ReportPath & "rptFactura.rpt"
        'pdf.CrystalReportFileNameFullPath = "C:\Users\arhernandez\Documents\Visual Studio 2008\Projects\Desktop\DesKTOP\Reportes\" & "rptFactura.rpt"
        pdf.pdfFileNameFullPath = pathRpt + folio.ToString() + ".pdf"
        pdf.recSelFormula = "{VW_FacturacionEImpresa.Cod_Folio} = '" & cod_folio & "' AND {VW_FacturacionEImpresa.folio_factura} = " & folio.ToString() & ""
        pdf.Transfer(Datos.DAO.Conexion.Server, Datos.DAO.Conexion.Database)
        If MsgBox(" ¿Desea enviar la factura por correo?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Dim ms As New frmSendMail
            ms.Path = pathRpt
            ms.Nombre = nombre
            ms.Correo = email
            ms.folio = folio
            ms.path_rpt = pathRpt_ruta
            ms.cod_folio = cod_folio

            ms.ShowDialog()
        End If
    End Sub

    Private Sub imprime_factura_dontshow(folio As Integer, cod_folio As String, pathRpt_ruta As String)
        Dim ctr As New ResTotal.Controlador.ControladorFacturacion(Datos.DAO.Conexion.CadenaConexion)
        ctr.genera_imagen_qr(cod_folio, folio)
        Dim myCr As New PkVisorRpt
        myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        myCr.sSQLV = "{VW_FacturacionEImpresa.Cod_Folio} = '" & cod_folio & "' AND {VW_FacturacionEImpresa.folio_factura} = " & folio.ToString() & ""
        myCr.sRpt = pathRpt_ruta
        myCr.Text = "Impresion Caja"
        myCr.Show()
        myCr.Close()
    End Sub

    Private Sub imprime_factura(folio As Integer, cod_folio As String, pathRpt_ruta As String)
        Dim ctr As New ResTotal.Controlador.ControladorFacturacion(Datos.DAO.Conexion.CadenaConexion)
        ctr.genera_imagen_qr(cod_folio, folio)
        Dim myCr As New PkVisorRpt
        myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        myCr.sSQLV = "{VW_FacturacionEImpresa.Cod_Folio} = '" & cod_folio & "' AND {VW_FacturacionEImpresa.folio_factura} = " & folio.ToString() & ""
        myCr.sRpt = pathRpt_ruta
        myCr.Text = "Impresion Caja"
        myCr.MdiParent = Me
        myCr.Show()
    End Sub

    Private Function TimbrarCFD(usuario As String, password As String, xml As String) As String()
        Dim ServicioFY As New WSFY.WS_TFD_FYSoapClient
        Dim Respuesta As New WSFY.ArrayOfString

        Respuesta = ServicioFY.TimbrarCFD(usuario, password, xml, "")
        'Respuesta = ServicioFY.TimbrarCFDPrueba(usuario, password, xml)
        Return Respuesta.ToArray()
    End Function

    Private Function CancelarCFDI(ByVal usuario As String, ByVal password As String, ByVal RFC As String, ByVal listaCFDI As String(), cadena As String, password_pfx As String) As String()
        Dim ServicioFY As New WSFY.WS_TFD_FYSoapClient
        Dim Respuesta As New WSFY.ArrayOfString 'Se recibe la respuesta
        Dim lista As New WSFY.ArrayOfString
        lista.AddRange(listaCFDI)
        Respuesta = ServicioFY.CancelarCFDI(usuario, password, RFC, lista, cadena, password_pfx)
        Return listaCFDI
    End Function

    Private Sub BBControldeValores_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBControldeValores.ItemClick
        Dim frmValores As New ResTotal.Vista.ZctCatUbicacionesValores(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id)
        With frmValores
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub BBPedidos_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBPedidos.ItemClick
        Dim fOrdTrab As New ZctOrdTrabajo
        fOrdTrab.MdiParent = Me
        fOrdTrab.Show()
    End Sub

    Private Sub BBNotaDeCredito_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBNotaDeCredito.ItemClick
        Dim frmCdt As New ZctFormNotaCredito()
        frmCdt.MdiParent = Me
        frmCdt.Show()
    End Sub

    Private Sub BBCompras_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBCompras.ItemClick

        Dim fOrdComp As New ZctOrdCompra
        fOrdComp.MdiParent = Me
        fOrdComp.Show()
    End Sub

    Private Sub BBReportesAdministracion_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBReportesAdministracion.ItemClick
        Dim fShowRpt As New ZctShowRpt
        fShowRpt.MdiParent = Me
        '1 para inventarios
        fShowRpt.iCodCatego = 2
        fShowRpt.Show()
    End Sub

    Private Sub BBCompraSugerida_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBCompraSugerida.ItemClick
        Dim frmOCS As New ZctOrdenCompraSug
        frmOCS.MdiParent = (Me)
        frmOCS.Show()
    End Sub

    Private Sub BBProveedores_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBProveedores.ItemClick
        Dim fProveedores As New ResTotal.Vista.ZctCatProv(Datos.DAO.Conexion.CadenaConexion, ControladorBase.UsuarioActual.Id)
        AddHandler fProveedores.BuscaProveedor, AddressOf BuscaProveedor
        fProveedores.MdiParent = Me
        fProveedores.Show()

    End Sub

    Private Function BuscaProveedor() As Integer
        Dim fBusqueda As New ZctBusqueda
        fBusqueda.sSPName = "SP_ZctCatProv"
        fBusqueda.sSpVariables = "@Cod_Prov;INT|@Nom_Prov;VARCHAR|@RFC_Prov;VARCHAR|@Dir_Prov;VARCHAR"
        fBusqueda.ShowDialog(Me)
        Return fBusqueda.iValor
    End Function

    Private Sub BBReportesCompras_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBReportesCompras.ItemClick
        Dim fShowRpt As New ZctShowRpt
        fShowRpt.MdiParent = Me
        '1 para inventarios
        fShowRpt.iCodCatego = 3
        fShowRpt.Show()
    End Sub

    Private Sub BBActualizacion_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBActualizacion.ItemClick
        Try
            Dim Actualiza As New ZctActiva.Activa(Datos.DAO.Conexion.CadenaConexion) 'String.Format(System.Configuration.ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4"))
            Actualiza.BuscaActualizaciones()
            If Actualiza.Errores = "" Then
                MsgBox("Las actualizaciones se han ejecutado de manera correcta.")
            Else
                MsgBox(Actualiza.Errores)
            End If


        Catch ex As Exception
            MsgBox("Ha ocurrido un error al ejecutar las actualizaciones")
            Debug.Print(ex.Message)
        End Try
    End Sub

    Private Sub BBUsuarios_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBUsuarios.ItemClick
        Dim zctUsuarios As New ZctSegUsu
        zctUsuarios.MdiParent = Me
        zctUsuarios.Show()
    End Sub

    Private Sub BBParametros_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBParametros.ItemClick
        Dim zctPar As New ZctCatPar
        zctPar.MdiParent = Me
        zctPar.Show()
    End Sub

    Private Sub BBCalendarioFiscal_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBCalendarioFiscal.ItemClick

        Dim frm As New CalendarioFiscal.Vista.FrmCalendarioFiscal(Datos.DAO.Conexion.CadenaConexion)
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub BBPermisosPorUsuario_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBPermisosPorUsuario.ItemClick
        Dim Permisos As New PermisosUsuarios
        Permisos.MdiParent = Me
        Permisos.Show()
    End Sub

    Private Sub BBRoles_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BBRoles.ItemClick
        Dim Permisos As New RolesUsuarios
        Permisos.MdiParent = Me
        Permisos.Show()
    End Sub

    Private Sub BarButtonItem2_ItemClick_1(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles cmdOrdenTrabajo.ItemClick
        Dim ordenDeTrabajo As New ZctOrdTrabajo 'Mot
        ordenDeTrabajo.MdiParent = Me
        ordenDeTrabajo.Show()
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles cmdMarcas.ItemClick
        Dim fMarcaBusqueda As New ZctfrmComunGen
        fMarcaBusqueda.EnumGenerico = New Clases.Catalogos.ListaMarcas
        fMarcaBusqueda.Text = "Catálogo de Marcas"
        fMarcaBusqueda.MdiParent = Me
        fMarcaBusqueda.Show()
    End Sub

    Private Sub cmdModelos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles cmdModelos.ItemClick
        Dim fModelos As New ZctFrmComunGenMod
        'fModelos.sTablaName = "ZctCatMod"
        'fModelos.sSPName = "SP_ZctCatMod"
        'fModelos.sSpVariables = "@CodMod_Mot;INT|@Desc_Mod;VARCHAR|@Cod_Mar;INT|@ColNuevo;INT"

        'fModelos.sSPName_Busq = "SP_ZctCatMarF3"
        'fModelos.iColBusq = 2
        'fModelos.sSpVariables_Busq = "@Codigo;INT|@Descripcion;VARCHAR"
        'fModelos.sTabla_Busq = "ZctCatMar"
        'fModelos.Height = 400
        'fModelos.Width = 400
        fModelos.EnumGenerico = New Clases.Catalogos.listaModelos
        fModelos.Text = "Catálogo de Modelos"
        fModelos.MdiParent = Me
        fModelos.Show()
    End Sub

    Private Sub btnVehiculos_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnVehiculos.ItemClick
        Dim fMotos As New ZctMotos
        fMotos.MdiParent = Me
        fMotos.Show()
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Public Sub MostrarComoDialogoExt()
        Me.ShowDialog()
    End Sub

    Private Sub btnRequisiciones_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnRequisiciones.ItemClick
        Dim frm_requisiciones As New RequisicionesWeb.Vista.Requisicion(Datos.DAO.Conexion.CadenaConexion)
        frm_requisiciones.MdiParent = Me
        'AddHandler frm_requisiciones.lanza_salida, AddressOf Crea_salida
        AddHandler frm_requisiciones.lanza_compra, AddressOf Crea_compra

        frm_requisiciones.Show()
    End Sub

    Private Function Crea_compra(ByVal Cod_OC As Integer) As Integer

        Try
            Dim Ocs As New ZctOrdCompra
            'Abre la orden de compra normal y carga la orden de compra en cuestion
            Ocs.Show()
            Ocs.CargaCompra(Cod_OC)
            Ocs.MdiParent = Me
            Ocs.Focus()


        Catch ex As Exception
            MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        End Try

        Return 0
    End Function

    Private Sub BarButtonItem2_ItemClick_2(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        Dim frm As New frmMixMovInv(Datos.DAO.Conexion.CadenaConexion)
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub BarButtonItem3_ItemClick_1(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        Dim frm As New frmMixItems(Datos.DAO.Conexion.CadenaConexion)
        frm.MdiParent = Me
        frm.Show()
    End Sub
End Class

