﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctInvMotos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ZctInvMotos))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.cmdDer = New ZctSOT.ZctSOTButton
        Me.cmdIzq = New ZctSOT.ZctSOTButton
        Me.cmdCargar = New ZctSOT.ZctSOTButton
        Me.cmdBuscaArchivo = New ZctSOT.ZctSOTButton
        Me.txtCliente = New ZctSOT.ZctControlBusqueda
        Me.lblArchivo = New ZctSOT.ZctSOTLabelDesc
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel
        Me.txtFolio = New ZctSOT.ZctControlTexto
        Me.OpDialog = New System.Windows.Forms.OpenFileDialog
        Me.cmdCancelar = New ZctSOT.ZctSOTButton
        Me.cmdAceptar = New ZctSOT.ZctSOTButton
        Me.grdDatos = New ZctSOT.ZctSotGrid
        Me.Panel1.SuspendLayout()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.cmdDer)
        Me.Panel1.Controls.Add(Me.cmdIzq)
        Me.Panel1.Controls.Add(Me.cmdCargar)
        Me.Panel1.Controls.Add(Me.cmdBuscaArchivo)
        Me.Panel1.Controls.Add(Me.txtCliente)
        Me.Panel1.Controls.Add(Me.lblArchivo)
        Me.Panel1.Controls.Add(Me.ZctSOTLabel1)
        Me.Panel1.Controls.Add(Me.txtFolio)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(700, 71)
        Me.Panel1.TabIndex = 0
        '
        'cmdDer
        '
        Me.cmdDer.Location = New System.Drawing.Point(147, 3)
        Me.cmdDer.Name = "cmdDer"
        Me.cmdDer.Size = New System.Drawing.Size(19, 23)
        Me.cmdDer.TabIndex = 8
        Me.cmdDer.Text = ">"
        Me.cmdDer.UseVisualStyleBackColor = True
        '
        'cmdIzq
        '
        Me.cmdIzq.Location = New System.Drawing.Point(127, 3)
        Me.cmdIzq.Name = "cmdIzq"
        Me.cmdIzq.Size = New System.Drawing.Size(19, 23)
        Me.cmdIzq.TabIndex = 7
        Me.cmdIzq.Text = "<"
        Me.cmdIzq.UseVisualStyleBackColor = True
        '
        'cmdCargar
        '
        Me.cmdCargar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCargar.Location = New System.Drawing.Point(594, 5)
        Me.cmdCargar.Name = "cmdCargar"
        Me.cmdCargar.Size = New System.Drawing.Size(98, 23)
        Me.cmdCargar.TabIndex = 6
        Me.cmdCargar.Text = "Cargar Inventario"
        Me.cmdCargar.UseVisualStyleBackColor = True
        '
        'cmdBuscaArchivo
        '
        Me.cmdBuscaArchivo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdBuscaArchivo.Image = CType(resources.GetObject("cmdBuscaArchivo.Image"), System.Drawing.Image)
        Me.cmdBuscaArchivo.Location = New System.Drawing.Point(668, 37)
        Me.cmdBuscaArchivo.Name = "cmdBuscaArchivo"
        Me.cmdBuscaArchivo.Size = New System.Drawing.Size(24, 24)
        Me.cmdBuscaArchivo.TabIndex = 5
        Me.cmdBuscaArchivo.UseVisualStyleBackColor = True
        '
        'txtCliente
        '
        Me.txtCliente.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCliente.Descripcion = ""
        Me.txtCliente.Location = New System.Drawing.Point(172, 3)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Nombre = "Cliente:"
        Me.txtCliente.Size = New System.Drawing.Size(416, 27)
        Me.txtCliente.SPName = Nothing
        Me.txtCliente.SPParametros = Nothing
        Me.txtCliente.SpVariables = Nothing
        Me.txtCliente.SqlBusqueda = Nothing
        Me.txtCliente.TabIndex = 2
        Me.txtCliente.Tabla = Nothing
        Me.txtCliente.TextWith = 111
        Me.txtCliente.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCliente.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtCliente.Validar = True
        '
        'lblArchivo
        '
        Me.lblArchivo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblArchivo.BackColor = System.Drawing.Color.LightBlue
        Me.lblArchivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblArchivo.Location = New System.Drawing.Point(55, 37)
        Me.lblArchivo.Name = "lblArchivo"
        Me.lblArchivo.Size = New System.Drawing.Size(607, 19)
        Me.lblArchivo.TabIndex = 4
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(3, 43)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(46, 13)
        Me.ZctSOTLabel1.TabIndex = 3
        Me.ZctSOTLabel1.Text = "Archivo:"
        '
        'txtFolio
        '
        Me.txtFolio.Location = New System.Drawing.Point(15, 3)
        Me.txtFolio.Multiline = False
        Me.txtFolio.Name = "txtFolio"
        Me.txtFolio.Nombre = "Folio:"
        Me.txtFolio.Size = New System.Drawing.Size(115, 28)
        Me.txtFolio.TabIndex = 0
        Me.txtFolio.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtFolio.ToolTip = ""
        '
        'OpDialog
        '
        Me.OpDialog.DefaultExt = "*.xls"
        Me.OpDialog.Filter = "Archivos de Excel(*.xls)|*.xls"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.Image = CType(resources.GetObject("cmdCancelar.Image"), System.Drawing.Image)
        Me.cmdCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancelar.Location = New System.Drawing.Point(613, 502)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancelar.TabIndex = 3
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Image = CType(resources.GetObject("cmdAceptar.Image"), System.Drawing.Image)
        Me.cmdAceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAceptar.Location = New System.Drawing.Point(531, 502)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 2
        Me.cmdAceptar.Text = "Aceptar"
        Me.cmdAceptar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'grdDatos
        '
        Me.grdDatos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDatos.Location = New System.Drawing.Point(0, 73)
        Me.grdDatos.Name = "grdDatos"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdDatos.RowHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdDatos.Size = New System.Drawing.Size(700, 410)
        Me.grdDatos.TabIndex = 1
        '
        'ZctInvMotos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 537)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Controls.Add(Me.grdDatos)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ZctInvMotos"
        Me.Text = "Inventario de motos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdDatos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtFolio As ZctSOT.ZctControlTexto
    Friend WithEvents cmdBuscaArchivo As ZctSOT.ZctSOTButton
    Friend WithEvents txtCliente As ZctSOT.ZctControlBusqueda
    Friend WithEvents lblArchivo As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents grdDatos As ZctSOT.ZctSotGrid
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents OpDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmdCargar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdDer As ZctSOT.ZctSOTButton
    Friend WithEvents cmdIzq As ZctSOT.ZctSOTButton
End Class
