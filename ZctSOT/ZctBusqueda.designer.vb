<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctBusqueda
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdAceptar = New System.Windows.Forms.Button
        Me.DGBusqueda = New System.Windows.Forms.DataGridView
        Me.ZctGroupControls1 = New ZctSOT.ZctGroupControls
        Me.ZctSOTLabelDesc1 = New ZctSOT.ZctSOTLabelDesc
        Me.TxtColBus = New ZctSOT.ZctControlTexto
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ZctGroupControls1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(671, 476)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Text = "Aceptar"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'DGBusqueda
        '
        Me.DGBusqueda.AllowUserToAddRows = False
        Me.DGBusqueda.AllowUserToDeleteRows = False
        Me.DGBusqueda.AllowUserToOrderColumns = True
        Me.DGBusqueda.AllowUserToResizeRows = False
        Me.DGBusqueda.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGBusqueda.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.DGBusqueda.Location = New System.Drawing.Point(3, 57)
        Me.DGBusqueda.MultiSelect = False
        Me.DGBusqueda.Name = "DGBusqueda"
        Me.DGBusqueda.ReadOnly = True
        Me.DGBusqueda.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DGBusqueda.Size = New System.Drawing.Size(753, 413)
        Me.DGBusqueda.TabIndex = 2
        '
        'ZctGroupControls1
        '
        Me.ZctGroupControls1.Controls.Add(Me.ZctSOTLabelDesc1)
        Me.ZctGroupControls1.Controls.Add(Me.TxtColBus)
        Me.ZctGroupControls1.Location = New System.Drawing.Point(3, -1)
        Me.ZctGroupControls1.Name = "ZctGroupControls1"
        Me.ZctGroupControls1.Size = New System.Drawing.Size(565, 52)
        Me.ZctGroupControls1.TabIndex = 4
        Me.ZctGroupControls1.TabStop = False
        Me.ZctGroupControls1.Text = "Busqueda por:"
        '
        'ZctSOTLabelDesc1
        '
        Me.ZctSOTLabelDesc1.BackColor = System.Drawing.Color.LightBlue
        Me.ZctSOTLabelDesc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ZctSOTLabelDesc1.Location = New System.Drawing.Point(320, 13)
        Me.ZctSOTLabelDesc1.Name = "ZctSOTLabelDesc1"
        Me.ZctSOTLabelDesc1.Size = New System.Drawing.Size(239, 33)
        Me.ZctSOTLabelDesc1.TabIndex = 4
        Me.ZctSOTLabelDesc1.Text = "Doble Click en el encabezado de columna para seleccionar la busqueda (No datos nu" & _
            "mericos)"
        '
        'TxtColBus
        '
        Me.TxtColBus.Enabled = False
        Me.TxtColBus.Location = New System.Drawing.Point(6, 13)
        Me.TxtColBus.Multiline = False
        Me.TxtColBus.Name = "TxtColBus"
        Me.TxtColBus.Nombre = "Columna:"
        Me.TxtColBus.Size = New System.Drawing.Size(315, 28)
        Me.TxtColBus.TabIndex = 3
        Me.TxtColBus.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtColBus.ToolTip = ""
        '
        'ZctBusqueda
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(758, 506)
        Me.Controls.Add(Me.ZctGroupControls1)
        Me.Controls.Add(Me.DGBusqueda)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Name = "ZctBusqueda"
        Me.Text = "Busqueda"
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ZctGroupControls1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents DGBusqueda As System.Windows.Forms.DataGridView
    Friend WithEvents TxtColBus As ZctSOT.ZctControlTexto
    Friend WithEvents ZctGroupControls1 As ZctSOT.ZctGroupControls
    Friend WithEvents ZctSOTLabelDesc1 As ZctSOT.ZctSOTLabelDesc
End Class
