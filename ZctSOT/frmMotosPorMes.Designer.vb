﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMotosPorMes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.grpControles = New System.Windows.Forms.GroupBox
        Me.cmdGrabar = New ZctSOT.ZctSOTButton
        Me.gridMotos = New ZctSOT.ZctSotGrid
        Me.AnnoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CodRegionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DescRegionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NoMotosDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ZctMotosPorAnnoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel
        Me.cboMes = New ZctSOT.ZctSOTComboBox
        Me.cmdCargar = New ZctSOT.ZctSOTButton
        Me.ctrlAnno = New ZctSOT.ZctControlTexto
        Me.grpControles.SuspendLayout()
        CType(Me.gridMotos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ZctMotosPorAnnoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpControles
        '
        Me.grpControles.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpControles.Controls.Add(Me.ZctSOTLabel1)
        Me.grpControles.Controls.Add(Me.cboMes)
        Me.grpControles.Controls.Add(Me.cmdCargar)
        Me.grpControles.Controls.Add(Me.ctrlAnno)
        Me.grpControles.Location = New System.Drawing.Point(10, 3)
        Me.grpControles.Name = "grpControles"
        Me.grpControles.Size = New System.Drawing.Size(525, 49)
        Me.grpControles.TabIndex = 2
        Me.grpControles.TabStop = False
        '
        'cmdGrabar
        '
        Me.cmdGrabar.Location = New System.Drawing.Point(460, 283)
        Me.cmdGrabar.Name = "cmdGrabar"
        Me.cmdGrabar.Size = New System.Drawing.Size(75, 23)
        Me.cmdGrabar.TabIndex = 5
        Me.cmdGrabar.Text = "Grabar"
        Me.cmdGrabar.UseVisualStyleBackColor = True
        '
        'gridMotos
        '
        Me.gridMotos.AllowUserToAddRows = False
        Me.gridMotos.AllowUserToDeleteRows = False
        Me.gridMotos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridMotos.AutoGenerateColumns = False
        Me.gridMotos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMotos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AnnoDataGridViewTextBoxColumn, Me.MesDataGridViewTextBoxColumn, Me.CodRegionDataGridViewTextBoxColumn, Me.DescRegionDataGridViewTextBoxColumn, Me.NoMotosDataGridViewTextBoxColumn})
        Me.gridMotos.DataSource = Me.ZctMotosPorAnnoBindingSource
        Me.gridMotos.Location = New System.Drawing.Point(10, 58)
        Me.gridMotos.Name = "gridMotos"
        Me.gridMotos.Size = New System.Drawing.Size(523, 216)
        Me.gridMotos.TabIndex = 3
        '
        'AnnoDataGridViewTextBoxColumn
        '
        Me.AnnoDataGridViewTextBoxColumn.DataPropertyName = "Anno"
        Me.AnnoDataGridViewTextBoxColumn.HeaderText = "Anno"
        Me.AnnoDataGridViewTextBoxColumn.Name = "AnnoDataGridViewTextBoxColumn"
        Me.AnnoDataGridViewTextBoxColumn.Visible = False
        '
        'MesDataGridViewTextBoxColumn
        '
        Me.MesDataGridViewTextBoxColumn.DataPropertyName = "Mes"
        Me.MesDataGridViewTextBoxColumn.HeaderText = "Mes"
        Me.MesDataGridViewTextBoxColumn.Name = "MesDataGridViewTextBoxColumn"
        Me.MesDataGridViewTextBoxColumn.Visible = False
        '
        'CodRegionDataGridViewTextBoxColumn
        '
        Me.CodRegionDataGridViewTextBoxColumn.DataPropertyName = "Cod_Region"
        Me.CodRegionDataGridViewTextBoxColumn.HeaderText = "Cod_Region"
        Me.CodRegionDataGridViewTextBoxColumn.Name = "CodRegionDataGridViewTextBoxColumn"
        Me.CodRegionDataGridViewTextBoxColumn.Visible = False
        '
        'DescRegionDataGridViewTextBoxColumn
        '
        Me.DescRegionDataGridViewTextBoxColumn.DataPropertyName = "Desc_Region"
        Me.DescRegionDataGridViewTextBoxColumn.HeaderText = "Region"
        Me.DescRegionDataGridViewTextBoxColumn.Name = "DescRegionDataGridViewTextBoxColumn"
        Me.DescRegionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NoMotosDataGridViewTextBoxColumn
        '
        Me.NoMotosDataGridViewTextBoxColumn.DataPropertyName = "NoMotos"
        Me.NoMotosDataGridViewTextBoxColumn.HeaderText = "Motos"
        Me.NoMotosDataGridViewTextBoxColumn.Name = "NoMotosDataGridViewTextBoxColumn"
        '
        'ZctMotosPorAnnoBindingSource
        '
        Me.ZctMotosPorAnnoBindingSource.DataSource = GetType(ZctSOT.Datos.Clases.Servicio.ZctMotosPorAnno)
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(168, 19)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(30, 13)
        Me.ZctSOTLabel1.TabIndex = 4
        Me.ZctSOTLabel1.Text = "Mes:"
        '
        'cboMes
        '
        Me.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Location = New System.Drawing.Point(204, 16)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(121, 21)
        Me.cboMes.TabIndex = 3
        '
        'cmdCargar
        '
        Me.cmdCargar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCargar.Location = New System.Drawing.Point(443, 14)
        Me.cmdCargar.Name = "cmdCargar"
        Me.cmdCargar.Size = New System.Drawing.Size(75, 23)
        Me.cmdCargar.TabIndex = 2
        Me.cmdCargar.Text = "Cargar"
        Me.cmdCargar.UseVisualStyleBackColor = True
        '
        'ctrlAnno
        '
        Me.ctrlAnno.Location = New System.Drawing.Point(2, 9)
        Me.ctrlAnno.Multiline = False
        Me.ctrlAnno.Name = "ctrlAnno"
        Me.ctrlAnno.Nombre = "Año:"
        Me.ctrlAnno.Size = New System.Drawing.Size(139, 28)
        Me.ctrlAnno.TabIndex = 0
        Me.ctrlAnno.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.ctrlAnno.ToolTip = ""
        '
        'frmMotosPorMes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(541, 318)
        Me.Controls.Add(Me.cmdGrabar)
        Me.Controls.Add(Me.gridMotos)
        Me.Controls.Add(Me.grpControles)
        Me.Name = "frmMotosPorMes"
        Me.Text = "Motos por mes y año"
        Me.grpControles.ResumeLayout(False)
        Me.grpControles.PerformLayout()
        CType(Me.gridMotos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ZctMotosPorAnnoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ctrlAnno As ZctSOT.ZctControlTexto
    Friend WithEvents grpControles As System.Windows.Forms.GroupBox
    Friend WithEvents cmdCargar As ZctSOT.ZctSOTButton
    Friend WithEvents gridMotos As ZctSOT.ZctSotGrid
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents cboMes As ZctSOT.ZctSOTComboBox
    Friend WithEvents ZctMotosPorAnnoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AnnoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodRegionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescRegionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoMotosDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdGrabar As ZctSOT.ZctSOTButton
End Class
