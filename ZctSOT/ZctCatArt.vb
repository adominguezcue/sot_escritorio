Imports System.Data.SqlDbType
Imports ZctSOT.Datos.BaseDatosException
Imports ZctSOTRDN
Imports ZctSOT.Datos.DAO
Imports ResTotal
Imports System.Linq
Imports ZctSOT.Datos
Imports SOTControladores.Controladores

Public Class ZctCatArt
    Public Event ImprimeEtiqueta(Cod_Art As String)
    Public Event ImprimeEtiquetaArticulo(Cod_Art As String)

    Private bloqueador() As Boolean = {False}

    Dim esNuevo As Boolean = False

    Class ImagenArticulo
        Public RutaOriginal As String
        Public RutaImg As String
    End Class
    Dim ListaImagenes As New List(Of ImagenArticulo)
    Dim PathImg As String = ""
    Dim ArticuloActual As New Modelo.Almacen.Entidades.Articulo
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
    Private Lista_Existencias As New List(Of Existencias)
    Private sSPName As String = "SP_ZctCatArt"
    Private sTabla As String = "ZctCatArt"
    Private sSpVariables As String = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;INT|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT|@Cod_TpArt;VARCHAR|@Cod_TpRef;INT|@desactivar;BIT"
    Private sSqlBusqueda As String = "SELECT [Cod_Cte] as C�digo ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]"
    Dim Pedita As Boolean
    Public ParametrosSistema As New Clases.Sistema.ZctCatPar

    Private Sub Enlazar()
        Try

            ArticulodBindingSource.DataSource = ArticuloActual
            ImagenesArticuloBindingSource.DataSource = ArticuloActual.ZctImagenesArticulos

            txtArticulo.DataBindings.Clear()
            txtArticulo.DataBindings.Add("TEXT", ArticulodBindingSource, "Cod_Art", True, DataSourceUpdateMode.OnValidation, 0)
            'txtArticulo.Enabled = False

            lblPrecioIVA.DataBindings.Clear()
            lblPrecioIVA.DataBindings.Add("Text", ArticulodBindingSource, "Prec_Art", True, DataSourceUpdateMode.OnValidation, 0)
            txtDescArt.DataBindings.Clear()
            txtDescArt.DataBindings.Add("Text", ArticulodBindingSource, "Desc_Art", True, DataSourceUpdateMode.OnValidation, "")

            txtCosto.DataBindings.Clear()
            txtCosto.DataBindings.Add("Text", ArticulodBindingSource, "Cos_Art", True, DataSourceUpdateMode.OnValidation, 0)

            CboMarca.DataBindings.Clear()
            CboMarca.DataBindings.Add("ValueItem", ArticulodBindingSource, "Cod_Mar", True, DataSourceUpdateMode.OnValidation, 0)

            cboTipoArt.DataBindings.Clear()
            cboTipoArt.DataBindings.Add("ValueItemStr", ArticulodBindingSource, "Cod_TpArt", False, DataSourceUpdateMode.OnValidation, "ART")

            cboDpto.DataBindings.Clear()
            cboDpto.DataBindings.Add("ValueItem", ArticulodBindingSource, "Cod_Dpto", True, DataSourceUpdateMode.OnValidation, 0)

            CboLinea.GetData("ZctCatLineaXDpto")
            CboLinea.DataBindings.Clear()
            CboLinea.DataBindings.Add("ValueItem", ArticulodBindingSource, "Cod_Linea", True, DataSourceUpdateMode.OnValidation, 0)

            'txtExistencias.DataBindings.Clear()
            'txtExistencias.DataBindings.Add("Text", ArticulodBindingSource, "Exist_Art", True, DataSourceUpdateMode.OnValidation, 0)

            'CtrlTpRef.DataBindings.Clear()
            'CtrlTpRef.DataBindings.Add("ValueItem", ArticulodBindingSource, "Cod_TpRef", True, DataSourceUpdateMode.OnValidation, 0)

            chkDeshabilita.DataBindings.Clear()
            chkDeshabilita.DataBindings.Add("Checked", ArticulodBindingSource, "desactivar", True, DataSourceUpdateMode.OnValidation, "false")

            'TxtUpc.DataBindings.Clear()
            'TxtUpc.DataBindings.Add("text", ArticulodBindingSource, "upc", True, DataSourceUpdateMode.OnValidation, "")

            'TxtUbicacion.DataBindings.Clear()
            'TxtUbicacion.DataBindings.Add("text", ArticulodBindingSource, "ubicacion", True, DataSourceUpdateMode.OnValidation, "")
            'dtDatos.Dispose()

            CboMarca.DataBindings.Clear()
            CboMarca.DataBindings.Add("ValueItem", ArticulodBindingSource, "Cod_Mar", True, DataSourceUpdateMode.OnValidation, 0)

            'txtNoArticulos.DataBindings.Clear()
            'txtNoArticulos.DataBindings.Add("text", ArticulodBindingSource, "noArticulo", True, DataSourceUpdateMode.OnValidation, "0")

            txtPais.DataBindings.Clear()
            txtPais.DataBindings.Add("text", ArticulodBindingSource, "pais", True, DataSourceUpdateMode.OnValidation, "")

            'cboMoneda.DataBindings.Clear()
            'cboMoneda.DataBindings.Add("ValueItem", ArticulodBindingSource, "id_cat_moneda", True, DataSourceUpdateMode.OnValidation, 0)

            cboPresentacion.DataBindings.Clear()
            cboPresentacion.DataBindings.Add("ValueItem", ArticulodBindingSource, "id_presentacion", True, DataSourceUpdateMode.OnValidation, 0)

            txtCaracteristicas.DataBindings.Clear()
            txtCaracteristicas.DataBindings.Add("text", ArticulodBindingSource, "caracteristicas", True, DataSourceUpdateMode.OnValidation, "")

            'txtPerUtilidad.DataBindings.Clear()
            'txtPerUtilidad.DataBindings.Add("text", ArticulodBindingSource, "porcentaje", True, DataSourceUpdateMode.OnValidation, "")

            TxtPasillo.DataBindings.Clear()
            TxtPasillo.DataBindings.Add("text", ArticulodBindingSource, "pasillo", True, DataSourceUpdateMode.OnValidation, "")

            TxtSeccion.DataBindings.Clear()
            TxtSeccion.DataBindings.Add("text", ArticulodBindingSource, "seccion", True, DataSourceUpdateMode.OnValidation, "")

            TxtRepisa.DataBindings.Clear()
            TxtRepisa.DataBindings.Add("text", ArticulodBindingSource, "repisa", True, DataSourceUpdateMode.OnValidation, "")

            'Descuento articulo

            TxtDescuento.Text = CDbl(Convert.ToDouble(If(ArticuloActual.descuento, 0)) * 100)

            'Txtrefill.DataBindings.Clear()
            'Txtrefill.DataBindings.Add("text", ArticulodBindingSource, "refill", True, DataSourceUpdateMode.OnValidation, "")


            If cboDpto.value IsNot Nothing Then
                CboLinea.GetData("ZctCatLineaXDpto")
                CboLinea.DataBindings.Clear()
                CboLinea.DataBindings.Add("ValueItem", ArticulodBindingSource, "Cod_Linea", True, DataSourceUpdateMode.OnValidation, 0)
            End If
            calcula_precio_con_iva()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atenci�n")
        End Try
    End Sub
    Public Sub CargarDatos(ByVal codigo As String)
        Me.txtArticulo.Text = codigo
        Me.Show()
        Me.GetData()
    End Sub
    Private Sub GetData()
        Try
            Dim _ControladorArt As New ControladorArticulos

            ArticuloActual = _ControladorArt.TraerOGenerarArticuloPorCodigoOUpc(txtArticulo.Text)

            esNuevo = String.IsNullOrEmpty(ArticuloActual.Cod_Art)

            txtArticulo.Enabled = esNuevo
            'Porque :'(
            'ArticuloActual.Cod_TpArt = "ART"
            If esNuevo Then

                Dim tmp = txtArticulo.Text
                Limpiar()
                ArticuloActual.Cod_Art = tmp
                txtArticulo.Text = tmp
                txtDescArt.Focus()
                btnEliminar.Enabled = False
            Else
                btnEliminar.Enabled = _Permisos.elimina
            End If
            Enlazar()
            Lista_Existencias = New ExistenciasDAO().ListaExistencia(txtArticulo.Text.ToString.ToUpper)
            If Lista_Existencias.Count > 0 Then
                GridExistencias.DataSource = Lista_Existencias
                cboTipoArt.Enabled = False
            Else
                cboTipoArt.Enabled = True
            End If

            If DGVimagenes.RowCount > 0 Then
                Bedita.Enabled = Pedita
                Belimina.Enabled = Pedita
            Else
                particulo.Image = My.Resources.no_foto
                Bedita.Enabled = Pedita
                Belimina.Enabled = Pedita
            End If
            BimprimeEtiqueta.Enabled = Not esNuevo
            cmdPreview.Enabled = Not esNuevo
            cmdEtArticulo.Enabled = Not esNuevo
            cmdPrevArt.Enabled = Not esNuevo
            'If Txtrefill.Text = "" Then
            'Txtrefill.Text = 0
            'End If
        Catch ex As Exception
            MsgBox("Error cargando informaci�n del art�culo: " & ex.Message, MsgBoxStyle.Exclamation, "Atenci�n")
        End Try
    End Sub
    'Private Sub cboDpto_lostfocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDpto.lostfocus

    '    Dim valtmp As Integer

    '    If IsNothing(cboDpto.value) OrElse Not Integer.TryParse(cboDpto.value.ToString(), valtmp) Then
    '        valtmp = 0
    '    End If

    '    CboLinea.GetData("ZctCatLineaXDpto", valtmp)
    'End Sub
    Private Function valida_datos() As Boolean
        Dim ErrorMsj As String = ""

        If (txtArticulo.Text = "") Then
            ErrorMsj += "Debe de asignar un c�digo de art�culo." & vbCrLf
            'Return False
        End If

        If (txtDescArt.Text = "") Then
            ErrorMsj += "Debe de asignar una descripci�n de art�culo." & vbCrLf
            'Return False
        End If
        If (txtCosto.Text = "0") Then
            ErrorMsj += "Debe de asignar un costo." & vbCrLf
            'Return False
        End If
        'If (TxtUpc.Text = "") Then
        '    ErrorMsj += "Debe especificar el campo UPC." & vbCrLf
        '    'Return False
        'End If
        If (txtPrecio.Text = "0") Then
            ErrorMsj += "Debe de asignar un precio" & vbCrLf
            'Return False
        End If
        'If (CboMarca.Text = "") Then
        '    ErrorMsj += "Debe de asignar una marca." & vbCrLf
        '    'Return False
        'End If
        If (cboDpto.Text = "") Then
            ErrorMsj += "Debe de asignar una categor�a." & vbCrLf
            'Return False
        End If
        If (CboLinea.Text = "") Then
            ErrorMsj += "Debe de asignar una sub categor�a." & vbCrLf
            'Return False
        End If
        If ErrorMsj.Length > 0 Then
            ErrorMsj = "Debe corregir los siguientes errores antes de guardar el art�culo:" & vbCrLf & ErrorMsj
            Throw New Exception(ErrorMsj)
        End If
        Return True
    End Function
    Private Sub ZctCatArt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        Pedita = (_Permisos.agrega Or _Permisos.edita)
        btnEliminar.Enabled = _Permisos.elimina
        Bnueva.Enabled = Pedita
        Bedita.Enabled = Pedita
        Belimina.Enabled = Pedita
        cmdAceptar.Enabled = Pedita

        txtPrecio.Visible = Pedita
        txtCosto.Visible = Pedita

        If Pedita = False Then
            sSPName = "SP_ZctCatArtSinCostoPrecio"
        End If
        'WTF!!!?????
        'LprecioIva.Left = IIf(Pedita = True, cboDpto.Left, cboPresentacion.Left)
        'lblPrecioIVA.Left = LprecioIva.Left + (10 + LprecioIva.Width)

        'TxtUpc.ZctSOTTextBox1.MaxLength = 50

        Dim _ControladorArt As New ControladorArticulos
        Dim _dao As New ResTotal.Modelo.DAODataContext(Datos.DAO.Conexion.CadenaConexion)
        _ControladorArt = New SOTControladores.Controladores.ControladorArticulos 'ResTotal.Controlador.ControladorZctCatArt(Datos.DAO.Conexion.CadenaConexion)

        Dim DAOSistema As New Datos.DAO.ClProcesaCatalogos()
        DAOSistema.CargaDatos(ParametrosSistema)
        PathImg = ParametrosSistema.RutaImagenesArticulos
        txtArticulo.SqlBusqueda = sSqlBusqueda
        txtArticulo.SPName = sSPName
        txtArticulo.SpVariables = sSpVariables
        txtArticulo.Tabla = sTabla
        txtArticulo.Tipo_Dato = ClassGen.Tipo_Dato.ALFANUMERICO
        CboLinea.Clear()
        CboMarca.GetData("ZctCatMar")
        cboTipoArt.GetDataAlfa("ZctCatTpArt")
        cboDpto.GetData("ZctCatDpto")
        ' cboMoneda.GetData("ZctCatMoneda")
        cboPresentacion.GetData("ZctCatPresentacion")

        Limpiar()
    End Sub
    Private Sub Limpiar()
        Try
            txtArticulo.Enabled = True
            txtArticulo.DataBindings.Clear()
            cboTipoArt.DataBindings.Clear()
            cboDpto.DataBindings.Clear()
            'txtExistencias.DataBindings.Clear()
            chkDeshabilita.DataBindings.Clear()
            'TxtUpc.DataBindings.Clear()
            'TxtUbicacion.DataBindings.Clear()
            txtDescArt.DataBindings.Clear()
            chkDeshabilita.Checked = False

            Bedita.Enabled = Pedita
            Belimina.Enabled = Pedita

            txtArticulo.Text = ""
            txtArticulo.ZctSOTLabelDesc1.Text = ""
            txtDescArt.Text = ""
            txtCosto.DataBindings.Clear()
            txtCosto.Text = 0
            lblPrecioIVA.DataBindings.Clear()
            txtPrecio.Text = 0
            'TxtUpc.Text = ""
            'TxtUbicacion.Text = ""
            TxtPasillo.Text = ""
            TxtSeccion.Text = ""
            TxtRepisa.Text = ""
            ImagenesArticuloBindingSource.DataSource = ""
            particulo.Image = My.Resources.no_foto
            GridExistencias.DataSource = ""
            cboDpto.DataBindings.Clear()
            cboDpto.ZctSOTCombo1.SelectedIndex = -1 'IIf(cboDpto.ZctSOTCombo1.Items.Count > 0, 0, -1)
            CboLinea.DataBindings.Clear()
            CboLinea.ZctSOTCombo1.SelectedIndex = -1 ' IIf(CboLinea.ZctSOTCombo1.Items.Count > 0, 0, -1)
            CboMarca.DataBindings.Clear()
            CboMarca.ZctSOTCombo1.SelectedIndex = -1
            DGVimagenes.Rows.Clear()
            'txtExistencias.DataBindings.Clear()
            'txtExistencias.Text = 0

            ' txtNoArticulos.DataBindings.Clear()
            ' txtNoArticulos.Text = "0"

            txtPais.DataBindings.Clear()
            txtPais.Text = ""

            'cboMoneda.DataBindings.Clear()
            'cboMoneda.ZctSOTCombo1.SelectedIndex = -1

            cboPresentacion.DataBindings.Clear()
            cboPresentacion.ZctSOTCombo1.SelectedIndex = -1

            txtCaracteristicas.DataBindings.Clear()
            txtCaracteristicas.Text = ""

            ' txtPerUtilidad.DataBindings.Clear()
            'txtPerUtilidad.Text = ""

            TxtPasillo.DataBindings.Clear()
            TxtSeccion.DataBindings.Clear()
            TxtRepisa.DataBindings.Clear()
            TxtDescuento.Text = ""

            BimprimeEtiqueta.Enabled = Not esNuevo
            cmdPreview.Enabled = Not esNuevo
            cmdEtArticulo.Enabled = Not esNuevo
            cmdPrevArt.Enabled = Not esNuevo
            'Txtrefill.Text = ""
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atenci�n")
        End Try
    End Sub
    Private Sub cmdAceptar_Click(sender As System.Object, e As System.EventArgs) Handles cmdAceptar.Click
        SyncLock bloqueador

            If bloqueador(0) Then
                Return
            End If

            bloqueador(0) = True
        End SyncLock

        Try
            If (esNuevo And _Permisos.agrega = False) Then
                Throw New Exception("No tiene permiso para agregar art�culos nuevos")
            End If

            If (String.IsNullOrWhiteSpace(cboTipoArt.ValueItemStr)) Then
                Throw New Exception(ZctSOT.Recursos.Articulos.tipo_no_seleccionado_exception)
            End If

            Dim descuento As Decimal

            If Not Decimal.TryParse(TxtDescuento.Text, descuento) Then
                Throw New Exception(ZctSOT.Recursos.Articulos.descuento_invalido_exception)
            End If

            If CDbl(TxtDescuento.Text) > 1 Then
                ArticuloActual.descuento = CDbl(TxtDescuento.Text) / 100
            End If
            ArticulodBindingSource.EndEdit()
            ImagenesArticuloBindingSource.EndEdit()
            valida_datos()

            Dim _ControladorArt As New ControladorArticulos

            If esNuevo Then
                _ControladorArt.AgregaArticulo(ArticulodBindingSource.DataSource, Nothing)
            Else
                _ControladorArt.ModificarArticulo(ArticulodBindingSource.DataSource, Nothing)
            End If

            '_ControladorArt.Graba()

            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctCatArt", "A", txtArticulo.Text)
            '_ControladorArt.Graba()

            For Each I As ImagenArticulo In ListaImagenes
                IO.File.Copy(I.RutaOriginal, I.RutaImg, True)
                Application.DoEvents()
            Next
            MsgBox("Guardado", MsgBoxStyle.Information, "Hecho")
            'Dim Respuesta As MsgBoxResult = MsgBox("�Desea imprimir la etiqueta del art�culo?", MsgBoxStyle.YesNo, "Atenci�n")
            'If Respuesta = MsgBoxResult.Yes Then
            '    RaiseEvent ImprimeEtiqueta(txtArticulo.Text)
            'End If
            ListaImagenes.Clear()
            Limpiar()
            txtArticulo.Focus()
            Limpiar()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Atenci�n")
        Finally
            SyncLock bloqueador
                bloqueador(0) = False
            End SyncLock
        End Try
    End Sub

    Private Sub txtArticulo_lostfocus(sender As Object, e As System.EventArgs) Handles txtArticulo.lostfocus
        If txtArticulo.Text = "" Or txtArticulo.Text = "0" Then
            Bnueva.Enabled = False
            BimprimeEtiqueta.Enabled = False
            cmdPreview.Enabled = False
            cmdEtArticulo.Enabled = False
            cmdPrevArt.Enabled = False
            Return
        End If
        Bnueva.Enabled = Pedita
        GetData()
    End Sub
    Public Function EscalarIMagen(ByVal OldImage As Image, ByVal TargetHeight As Integer, _
                               ByVal TargetWidth As Integer) As System.Drawing.Image

        Dim NewHeight As Integer = TargetHeight
        Dim NewWidth As Integer = NewHeight / OldImage.Height * OldImage.Width

        If NewWidth > TargetWidth Then
            NewWidth = TargetWidth
            NewHeight = NewWidth / OldImage.Width * OldImage.Height
        End If

        Return New Bitmap(OldImage, NewWidth, NewHeight)

    End Function
    Private Sub cmdCancelar_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancelar.Click
        SyncLock bloqueador

            If bloqueador(0) Then
                Return
            End If

            bloqueador(0) = True
        End SyncLock

        Try
            Limpiar()
            txtArticulo.Focus()
            MsgBox("Edici�n cancelada", MsgBoxStyle.Information, "Hecho")

        Finally
            SyncLock bloqueador
                bloqueador(0) = False
            End SyncLock
        End Try
    End Sub
    Private Sub txtDescArt_lostfocus(sender As Object, e As System.EventArgs) Handles txtDescArt.lostfocus
        txtArticulo.ZctSOTLabelDesc1.Text = txtDescArt.Text
    End Sub

    Private Sub Bnueva_Click(sender As System.Object, e As System.EventArgs) Handles Bnueva.Click
        Try

            If String.IsNullOrWhiteSpace(txtArticulo.Text) Then
                Throw New Exception(ZctSOT.Recursos.Articulos.codigo_articulo_invalido_exception)
            End If

            If IO.Directory.Exists(PathImg) = False Then
                IO.Directory.CreateDirectory(PathImg)
            End If



            If OFD.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim _ControladorArt As New ControladorArticulos

                Dim IO As New IO.FileInfo(OFD.FileName)
                Dim Max = _ControladorArt.MaxImg(txtArticulo.Text)
                Dim maxG = 0
                If DGVimagenes.Rows.Count > 0 Then
                    maxG = DGVimagenes.Rows(DGVimagenes.Rows.Count - 1).Cells(0).Value
                End If


                Dim _numero = IIf(maxG > 0, CInt(maxG) + 1, _ControladorArt.MaxImg(txtArticulo.Text))

                Dim _Rutaimg As String = PathImg & "\" & txtArticulo.Text & "_" & _numero & IO.Extension
                Dim IO2 As New IO.FileInfo(_Rutaimg)
                'If IO.Exists = True Then
                '    IO2.Delete()
                'End If
                'IO.CopyTo(_Rutaimg)
                Dim imgarticulo As New ImagenArticulo
                With imgarticulo
                    .RutaImg = _Rutaimg
                    .RutaOriginal = OFD.FileName
                End With
                ListaImagenes.Add(imgarticulo)
                Dim imagen As New Modelo.Almacen.Entidades.ZctImagenesArticulos
                With imagen
                    .imagen = _Rutaimg
                    .cod_art = txtArticulo.Text
                    .numimg = _numero
                End With
                IO = Nothing
                IO2 = Nothing
                ImagenesArticuloBindingSource.Add(imagen)

                'Dim IMG As Image = System.Drawing.Image.FromFile(OFD.FileName)
                'particulo.Image = EscalarIMagen(IMG, particulo.Height, particulo.Width)

            End If
        Catch ex As Exception
            MsgBox("Error al guardar imagen(es) de art�culo: " & ex.Message, MsgBoxStyle.Exclamation, "Atenci�n")
        End Try
    End Sub

    Private Sub DGVimagenes_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles DGVimagenes.DataError
        Try

        Catch ex As Exception

        End Try
    End Sub

    Private Sub DGVimagenes_DataSourceChanged(sender As Object, e As System.EventArgs) Handles DGVimagenes.DataSourceChanged
        DGVimagenes.ClearSelection()
        DGVimagenes.CurrentCell = Nothing
    End Sub
    Private Sub CargarFotos(R As DataGridViewRow)
        Try
            Dim Ruta As String = ""
            If (R.Cells("tmp").Value Is Nothing) Then
                Ruta = R.Cells("ImagenDataGridViewTextBoxColumn").Value
            Else
                Ruta = R.Cells("tmp").Value
            End If

            If Ruta Is Nothing Then
                Return
            End If
            Dim IO As New IO.FileInfo(Ruta)
            If IO.Exists Then
                Dim IMG As Image = System.Drawing.Image.FromFile(Ruta)
                particulo.Image = EscalarIMagen(IMG, particulo.Height, particulo.Width)
                IMG.Dispose()
            Else
                Ruta = R.Cells("tmp").Value

                If Ruta Is Nothing Then
                    Throw New Exception(ZctSOT.Recursos.Articulos.imagen_no_encontrada_exception)
                End If

                IO = New IO.FileInfo(Ruta)
                If IO.Exists = False Then
                    'MsgBox("El archivo de imagen no se encuentra en la ruta especificada, verifique", MsgBoxStyle.Exclamation, "Atenci�n")
                    particulo.Image = My.Resources.no_foto
                    Throw New Exception(ZctSOT.Recursos.Articulos.imagen_no_encontrada_exception)
                End If
                Dim IMG As Image = System.Drawing.Image.FromFile(Ruta)
                particulo.Image = EscalarIMagen(IMG, particulo.Height, particulo.Width)
                IMG.Dispose()
            End If
            Bedita.Enabled = Pedita
            Belimina.Enabled = Pedita
        Catch ex As Exception
            MsgBox("Error cargando imagenes de art�culo: " & ex.Message, MsgBoxStyle.Critical, "Atenci�n")

        End Try

    End Sub
    Private Sub DGVimagenes_RowEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGVimagenes.RowEnter
        CargarFotos(DGVimagenes.Rows(e.RowIndex))
    End Sub

    Private Sub Bedita_Click(sender As System.Object, e As System.EventArgs) Handles Bedita.Click
        Try


            If Not IsNothing(DGVimagenes.CurrentRow) AndAlso OFD.ShowDialog = Windows.Forms.DialogResult.OK Then
                particulo.Image = My.Resources.no_foto
                Dim Id As Integer = DGVimagenes.CurrentRow.Cells("CodimagenDataGridViewTextBoxColumn").Value
                Dim io As New IO.FileInfo(OFD.FileName)
                Dim _Rutaimg As String = PathImg & "\" & txtArticulo.Text & "_" & Id & io.Extension
                Dim IO2 As New IO.FileInfo(_Rutaimg)
                Dim IA As New ImagenArticulo
                With IA
                    .RutaOriginal = OFD.FileName
                    .RutaImg = _Rutaimg
                End With
                ListaImagenes.Add(IA)
                DGVimagenes.CurrentRow.Cells("ImagenDataGridViewTextBoxColumn").Value = _Rutaimg
                DGVimagenes.CurrentRow.Cells("tmp").Value = OFD.FileName
                Dim IMG As Image = System.Drawing.Image.FromFile(OFD.FileName)
                particulo.Image = EscalarIMagen(IMG, particulo.Height, particulo.Width)
                IMG.Dispose()
                IO2 = Nothing
                io = Nothing
                CargarFotos(DGVimagenes.CurrentRow)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub particulo_Click(sender As System.Object, e As System.EventArgs) Handles particulo.Click
        Try
            If DGVimagenes.CurrentRow Is Nothing Then
                Return
            End If
            Dim _Ruta As String = ""
            _Ruta = IIf(Not DGVimagenes.CurrentRow.Cells("tmp").Value Is Nothing, DGVimagenes.CurrentRow.Cells("tmp").Value, DGVimagenes.CurrentRow.Cells("ImagenDataGridViewTextBoxColumn").Value)
            Dim IO As New IO.FileInfo(_Ruta)
            If IO.Exists Then
                Process.Start(_Ruta)
            Else
                _Ruta = DGVimagenes.CurrentRow.Cells("tmp").Value
                IO = New IO.FileInfo(_Ruta)
                If IO.Exists = False Then
                    MsgBox(ZctSOT.Recursos.Articulos.imagen_no_encontrada_exception, MsgBoxStyle.Exclamation, "Atenci�n")
                    particulo.Image = My.Resources.no_foto
                End If
                Dim IMG As Image = System.Drawing.Image.FromFile(_Ruta)
                particulo.Image = EscalarIMagen(IMG, particulo.Height, particulo.Width)
                IMG.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Belimina_Click(sender As System.Object, e As System.EventArgs) Handles Belimina.Click
        Try
            If DGVimagenes.CurrentRow Is Nothing Then
                Return
            End If
            Dim R = MsgBox("�Desea eliminar la fotograf�a del art�culo?, esta acci�n no se podr� dehacer", MsgBoxStyle.YesNo, "Atecion")
            If R = vbNo Then
                Return
            End If
            Dim _idfoto = DGVimagenes.CurrentRow.Cells("CodimagenDataGridViewTextBoxColumn").Value.ToString()
            particulo.Image = My.Resources.no_foto
            System.IO.File.Delete(DGVimagenes.CurrentRow.Cells("ImagenDataGridViewTextBoxColumn").Value.ToString())

            Dim _ControladorArt As New ControladorArticulos

            _ControladorArt.EliminarFoto(txtArticulo.Text, _idfoto)
            DGVimagenes.Rows.Remove(DGVimagenes.CurrentRow)
            '_ControladorArt.Graba()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DGVimagenes_RowsAdded(sender As Object, e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles DGVimagenes.RowsAdded
        Try
            If ListaImagenes.Count = 0 Then
                Return
            End If
            DGVimagenes.Rows(e.RowIndex).Cells("tmp").Value = ListaImagenes(ListaImagenes.Count - 1).RutaOriginal
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ZctSOTButton1_Click(sender As System.Object, e As System.EventArgs) Handles btnEliminar.Click

        SyncLock bloqueador

            If bloqueador(0) Then
                Return
            End If

            bloqueador(0) = True
        End SyncLock

        Try
            If IsNothing(ArticuloActual) OrElse String.IsNullOrWhiteSpace(ArticuloActual.Cod_Art) Then
                Throw New Exception(Recursos.Articulos.articulo_no_seleccionado_exception)
            End If

            Dim R = MsgBox("�Desea eliminar el art�culo " & txtArticulo.ZctSOTLabelDesc1.Text & "? esta acci�n no se podr� deshacer", MsgBoxStyle.YesNo, "Atenci�n")
            If R = vbNo Then
                Return
            End If
            For Each Row As DataGridViewRow In DGVimagenes.Rows
                IO.File.Delete(Row.Cells("ImagenDataGridViewTextBoxColumn").Value)
                Application.DoEvents()
            Next

            Dim _ControladorArt As New ControladorArticulos

            _ControladorArt.EliminaArticulo(txtArticulo.Text)
            Dim cClas As New Datos.ClassGen
            cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctCatArt", "D", txtArticulo.Text)
            Limpiar()
            txtArticulo.Focus()

            MsgBox("Art�culo eliminado", MsgBoxStyle.Information, "Heho")
        Catch ex As Exception
            MsgBox("Error eliminado art�culo: " & ex.Message, MsgBoxStyle.Information, "Atenci�n")
        Finally
            SyncLock bloqueador
                bloqueador(0) = False
            End SyncLock
        End Try
    End Sub

    Private Sub BimprimeEtiqueta_Click(sender As System.Object, e As System.EventArgs) Handles BimprimeEtiqueta.Click
        'Dim pathRpt_ruta As String = "C:\Reportes\rptEtiquetaArt.rpt"
        'Dim myCr As New PkVisorRpt
        'myCr.Text = "Etiqueta Articulo"
        'myCr.MdiParent = Me.MdiParent
        ''myCr.sDataBase = cboServer.SelectedValue
        'myCr.sSQLV = "{zctcatart.Cod_Art} = '" & txtArticulo.Text & "'"

        'myCr.sRpt = pathRpt_ruta
        'myCr.Show()
        RaiseEvent ImprimeEtiqueta(txtArticulo.Text)
    End Sub


    Private Sub cmdPreview_Click(sender As System.Object, e As System.EventArgs) Handles cmdPreview.Click
        Dim pathRpt_ruta As String = "C:\Reportes\rptEtiquetaArt.rpt"
        Dim myCr As New PkVisorRpt
        myCr.Text = "Etiqueta Art�culo"
        myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        myCr.sSQLV = "{zctcatart.Cod_Art} = '" & txtArticulo.Text & "'"

        myCr.sRpt = pathRpt_ruta
        myCr.Show()
    End Sub

    Private Sub ZctSOTButton3_Click(sender As System.Object, e As System.EventArgs) Handles cmdEtArticulo.Click
        RaiseEvent ImprimeEtiquetaArticulo(txtArticulo.Text)
    End Sub

    Private Sub cmdPrevArt_Click(sender As System.Object, e As System.EventArgs) Handles cmdPrevArt.Click
        Dim pathRpt_ruta As String = "C:\Reportes\rptEtiquetaArt_pq.rpt"
        Dim myCr As New PkVisorRpt
        myCr.Text = "Etiqueta Art�culo"
        myCr.MdiParent = Me.MdiParent
        'myCr.sDataBase = cboServer.SelectedValue
        myCr.sSQLV = "{zctcatart.Cod_Art} = '" & txtArticulo.Text & "'"

        myCr.sRpt = pathRpt_ruta
        myCr.Show()
    End Sub

    Private Sub txtPrecio_lostfocus(sender As System.Object, e As System.EventArgs) Handles txtPrecio.TextChanged
        calcula_precio_con_iva()
    End Sub

    Private Sub calcula_precio_con_iva()
        If IsNumeric(txtPrecio.Text) Then
            lblPrecioIVA.Text = String.Format("{0:C}", Math.Round(Convert.ToDouble(txtPrecio.Text) / (1 + ParametrosSistema.Iva_CatPar), 2))
        End If
    End Sub

    Private Sub cboDpto_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDpto.SelectedValueChanged
        Dim valtmp As Integer

        If IsNothing(cboDpto.value) OrElse Not Integer.TryParse(cboDpto.value.ToString(), valtmp) Then
            valtmp = 0
        End If

        CboLinea.GetData("ZctCatLineaXDpto", valtmp)
    End Sub
End Class