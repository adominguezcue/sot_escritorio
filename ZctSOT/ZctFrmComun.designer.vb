<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctfrmComun
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdAceptar = New System.Windows.Forms.Button
        Me.DGBusqueda = New System.Windows.Forms.DataGridView
        Me.CmdAyuda = New ZctSOT.ZctSOTButton
        Me.CmdCancelar = New System.Windows.Forms.Button
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(169, 231)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 23)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Text = "Aceptar"
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'DGBusqueda
        '
        Me.DGBusqueda.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGBusqueda.Location = New System.Drawing.Point(3, 2)
        Me.DGBusqueda.Name = "DGBusqueda"
        Me.DGBusqueda.Size = New System.Drawing.Size(322, 223)
        Me.DGBusqueda.TabIndex = 0
        '
        'CmdAyuda
        '
        Me.CmdAyuda.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CmdAyuda.Location = New System.Drawing.Point(12, 231)
        Me.CmdAyuda.Name = "CmdAyuda"
        Me.CmdAyuda.Size = New System.Drawing.Size(75, 23)
        Me.CmdAyuda.TabIndex = 3
        Me.CmdAyuda.Text = "Ayuda"
        Me.CmdAyuda.UseVisualStyleBackColor = True
        '
        'CmdCancelar
        '
        Me.CmdCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdCancelar.Location = New System.Drawing.Point(250, 231)
        Me.CmdCancelar.Name = "CmdCancelar"
        Me.CmdCancelar.Size = New System.Drawing.Size(75, 23)
        Me.CmdCancelar.TabIndex = 2
        Me.CmdCancelar.Text = "Cancelar"
        Me.CmdCancelar.UseVisualStyleBackColor = True
        '
        'ZctfrmComun
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(327, 261)
        Me.Controls.Add(Me.CmdCancelar)
        Me.Controls.Add(Me.CmdAyuda)
        Me.Controls.Add(Me.DGBusqueda)
        Me.Controls.Add(Me.cmdAceptar)
        Me.Name = "ZctfrmComun"
        Me.Text = "Busqueda"
        CType(Me.DGBusqueda, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdAceptar As System.Windows.Forms.Button
    Friend WithEvents DGBusqueda As System.Windows.Forms.DataGridView
    Friend WithEvents CmdAyuda As ZctSOT.ZctSOTButton
    Friend WithEvents CmdCancelar As System.Windows.Forms.Button
End Class
