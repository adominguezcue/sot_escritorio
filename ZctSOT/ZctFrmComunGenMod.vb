﻿Public Class ZctFrmComunGenMod
    Private List As New Datos.Clases.Catalogos.ListaMarcas
    Private ProcesaDpto As New Datos.DAO.ClProcesaCatalogos
    Private Sub ZctFrmComunGenLinea_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Protected Overrides Sub CargaDatos()
        MyBase.CargaDatos()

        If DGBusqueda.Columns.Contains("Cod_Mar") Then
            ProcesaDpto.CargaDatos(List)
            Dim cCol As New DataGridViewComboBoxColumn
            cCol.Name = "Marca"
            cCol.DataSource = List
            cCol.HeaderText = "Marca"
            cCol.DataPropertyName = "Cod_Mar"
            cCol.DisplayMember = "Marca"
            cCol.ValueMember = "Codigo"

            DGBusqueda.Columns.Add(cCol)
            DGBusqueda.Columns("Cod_Mar").Visible = False
            CalculaWidth()
            DGBusqueda.AutoResizeColumn(cCol.Index)

        End If
    End Sub
End Class
