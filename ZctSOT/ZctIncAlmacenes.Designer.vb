<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctIncAlmacenes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ZctSOTButton1 = New ZctSOT.ZctSOTButton()
        Me.ZctAlmEntrada = New ZctSOT.ZctControlCombo()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.ZctAlmSalida = New ZctSOT.ZctControlCombo()
        Me.ZctGroupControls7 = New ZctSOT.ZctGroupControls()
        Me.DtAplicacion = New ZctSOT.ZctControlFecha()
        Me.ZctGroupControls3 = New ZctSOT.ZctGroupControls()
        Me.ZctSOTLabel4 = New ZctSOT.ZctSOTLabel()
        Me.ZctSOTLabel5 = New ZctSOT.ZctSOTLabel()
        Me.lblIva = New ZctSOT.ZctSOTLabelDesc()
        Me.lblTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel6 = New ZctSOT.ZctSOTLabel()
        Me.lblSubTotal = New ZctSOT.ZctSOTLabelDesc()
        Me.ZctSOTLabel3 = New ZctSOT.ZctSOTLabel()
        Me.ZctSOTLabel2 = New ZctSOT.ZctSOTLabel()
        Me.DGridArticulos = New ZctSOT.ZctSotGrid()
        Me.ZctSOTLabel1 = New ZctSOT.ZctSOTLabel()
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox()
        Me.ZctSOTButton2 = New ZctSOT.ZctSOTButton()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cmdImprimir = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.zctFolio = New ZctSOT.ZctGroupControls()
        Me.cmdDer = New ZctSOT.ZctSOTButton()
        Me.cmdIzq = New ZctSOT.ZctSOTButton()
        Me.txtFolio = New ZctSOT.ZctControlTexto()
        Me.ZctSOTToolTip1 = New ZctSOT.ZctSOTToolTip()
        Me.ColArt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColDesc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColExist = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColSurt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColCod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ZctGroupControls7.SuspendLayout()
        Me.ZctGroupControls3.SuspendLayout()
        CType(Me.DGridArticulos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ZctSOTGroupBox1.SuspendLayout()
        Me.zctFolio.SuspendLayout()
        Me.SuspendLayout()
        '
        'ZctSOTButton1
        '
        Me.ZctSOTButton1.Location = New System.Drawing.Point(130, 447)
        Me.ZctSOTButton1.Name = "ZctSOTButton1"
        Me.ZctSOTButton1.Size = New System.Drawing.Size(121, 40)
        Me.ZctSOTButton1.TabIndex = 8
        Me.ZctSOTButton1.Text = "Actualizar todos los movimientos"
        Me.ZctSOTButton1.UseVisualStyleBackColor = True
        Me.ZctSOTButton1.Visible = False
        '
        'ZctAlmEntrada
        '
        Me.ZctAlmEntrada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ZctAlmEntrada.Location = New System.Drawing.Point(228, 62)
        Me.ZctAlmEntrada.Name = "ZctAlmEntrada"
        Me.ZctAlmEntrada.Nombre = "Almacén de entrada:"
        Me.ZctAlmEntrada.Size = New System.Drawing.Size(200, 28)
        Me.ZctAlmEntrada.TabIndex = 7
        Me.ZctAlmEntrada.value = Nothing
        Me.ZctAlmEntrada.ValueItem = 0
        Me.ZctAlmEntrada.ValueItemStr = Nothing
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancelar.Location = New System.Drawing.Point(12, 445)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(112, 42)
        Me.cmdCancelar.TabIndex = 2
        Me.cmdCancelar.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
    "alla"
        Me.cmdCancelar.Text = "Cancelar movimiento"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelar, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
        "alla")
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'ZctAlmSalida
        '
        Me.ZctAlmSalida.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ZctAlmSalida.Location = New System.Drawing.Point(7, 62)
        Me.ZctAlmSalida.Name = "ZctAlmSalida"
        Me.ZctAlmSalida.Nombre = "Almacén de salida:"
        Me.ZctAlmSalida.Size = New System.Drawing.Size(200, 28)
        Me.ZctAlmSalida.TabIndex = 6
        Me.ZctAlmSalida.value = Nothing
        Me.ZctAlmSalida.ValueItem = 0
        Me.ZctAlmSalida.ValueItemStr = Nothing
        '
        'ZctGroupControls7
        '
        Me.ZctGroupControls7.Controls.Add(Me.DtAplicacion)
        Me.ZctGroupControls7.Location = New System.Drawing.Point(244, 5)
        Me.ZctGroupControls7.Name = "ZctGroupControls7"
        Me.ZctGroupControls7.Size = New System.Drawing.Size(319, 51)
        Me.ZctGroupControls7.TabIndex = 4
        Me.ZctGroupControls7.TabStop = False
        '
        'DtAplicacion
        '
        Me.DtAplicacion.DateWith = 91
        Me.DtAplicacion.Location = New System.Drawing.Point(6, 14)
        Me.DtAplicacion.Name = "DtAplicacion"
        Me.DtAplicacion.Nombre = "Fecha de aplicación:"
        Me.DtAplicacion.Size = New System.Drawing.Size(310, 27)
        Me.DtAplicacion.TabIndex = 0
        Me.DtAplicacion.Tag = "Fecha y hora en la que se va aplicar el movimiento"
        Me.ZctSOTToolTip1.SetToolTip(Me.DtAplicacion, "Fecha y hora en la que se va aplicar el movimiento")
        Me.DtAplicacion.Value = New Date(2017, 11, 23, 18, 39, 23, 0)
        '
        'ZctGroupControls3
        '
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel4)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel5)
        Me.ZctGroupControls3.Controls.Add(Me.lblIva)
        Me.ZctGroupControls3.Controls.Add(Me.lblTotal)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel6)
        Me.ZctGroupControls3.Controls.Add(Me.lblSubTotal)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel3)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel2)
        Me.ZctGroupControls3.Controls.Add(Me.DGridArticulos)
        Me.ZctGroupControls3.Controls.Add(Me.ZctSOTLabel1)
        Me.ZctGroupControls3.Location = New System.Drawing.Point(7, 96)
        Me.ZctGroupControls3.Name = "ZctGroupControls3"
        Me.ZctGroupControls3.Size = New System.Drawing.Size(886, 341)
        Me.ZctGroupControls3.TabIndex = 3
        Me.ZctGroupControls3.TabStop = False
        '
        'ZctSOTLabel4
        '
        Me.ZctSOTLabel4.AutoSize = True
        Me.ZctSOTLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel4.Location = New System.Drawing.Point(410, 321)
        Me.ZctSOTLabel4.Name = "ZctSOTLabel4"
        Me.ZctSOTLabel4.Size = New System.Drawing.Size(62, 13)
        Me.ZctSOTLabel4.TabIndex = 10
        Me.ZctSOTLabel4.Text = "SubTotal:"
        Me.ZctSOTLabel4.Visible = False
        '
        'ZctSOTLabel5
        '
        Me.ZctSOTLabel5.AutoSize = True
        Me.ZctSOTLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel5.Location = New System.Drawing.Point(588, 321)
        Me.ZctSOTLabel5.Name = "ZctSOTLabel5"
        Me.ZctSOTLabel5.Size = New System.Drawing.Size(31, 13)
        Me.ZctSOTLabel5.TabIndex = 12
        Me.ZctSOTLabel5.Text = "IVA:"
        Me.ZctSOTLabel5.Visible = False
        '
        'lblIva
        '
        Me.lblIva.BackColor = System.Drawing.Color.LightBlue
        Me.lblIva.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblIva.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIva.Location = New System.Drawing.Point(623, 321)
        Me.lblIva.Name = "lblIva"
        Me.lblIva.Size = New System.Drawing.Size(104, 15)
        Me.lblIva.TabIndex = 4
        Me.lblIva.Text = "$0"
        Me.lblIva.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblIva.Visible = False
        '
        'lblTotal
        '
        Me.lblTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(776, 321)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "$0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTotal.Visible = False
        '
        'ZctSOTLabel6
        '
        Me.ZctSOTLabel6.AutoSize = True
        Me.ZctSOTLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel6.Location = New System.Drawing.Point(733, 321)
        Me.ZctSOTLabel6.Name = "ZctSOTLabel6"
        Me.ZctSOTLabel6.Size = New System.Drawing.Size(40, 13)
        Me.ZctSOTLabel6.TabIndex = 14
        Me.ZctSOTLabel6.Text = "Total:"
        Me.ZctSOTLabel6.Visible = False
        '
        'lblSubTotal
        '
        Me.lblSubTotal.BackColor = System.Drawing.Color.LightBlue
        Me.lblSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTotal.Location = New System.Drawing.Point(478, 321)
        Me.lblSubTotal.Name = "lblSubTotal"
        Me.lblSubTotal.Size = New System.Drawing.Size(104, 15)
        Me.lblSubTotal.TabIndex = 2
        Me.lblSubTotal.Text = "$0"
        Me.lblSubTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSubTotal.Visible = False
        '
        'ZctSOTLabel3
        '
        Me.ZctSOTLabel3.AutoSize = True
        Me.ZctSOTLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel3.Location = New System.Drawing.Point(317, 364)
        Me.ZctSOTLabel3.Name = "ZctSOTLabel3"
        Me.ZctSOTLabel3.Size = New System.Drawing.Size(62, 13)
        Me.ZctSOTLabel3.TabIndex = 4
        Me.ZctSOTLabel3.Text = "SubTotal:"
        '
        'ZctSOTLabel2
        '
        Me.ZctSOTLabel2.AutoSize = True
        Me.ZctSOTLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel2.Location = New System.Drawing.Point(495, 364)
        Me.ZctSOTLabel2.Name = "ZctSOTLabel2"
        Me.ZctSOTLabel2.Size = New System.Drawing.Size(31, 13)
        Me.ZctSOTLabel2.TabIndex = 0
        Me.ZctSOTLabel2.Text = "IVA:"
        '
        'DGridArticulos
        '
        Me.DGridArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGridArticulos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColArt, Me.ColDesc, Me.ColExist, Me.ColSurt, Me.ColCosto, Me.SubTotal, Me.ColCod})
        Me.DGridArticulos.Location = New System.Drawing.Point(9, 24)
        Me.DGridArticulos.Name = "DGridArticulos"
        Me.DGridArticulos.Size = New System.Drawing.Size(871, 294)
        Me.DGridArticulos.TabIndex = 0
        Me.DGridArticulos.Tag = "En esta cuadricula se dan de alta los artículos que fueron comprados"
        Me.ZctSOTToolTip1.SetToolTip(Me.DGridArticulos, "En esta cuadricula se dan de alta los artículos que fueron comprados")
        '
        'ZctSOTLabel1
        '
        Me.ZctSOTLabel1.AutoSize = True
        Me.ZctSOTLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZctSOTLabel1.Location = New System.Drawing.Point(640, 364)
        Me.ZctSOTLabel1.Name = "ZctSOTLabel1"
        Me.ZctSOTLabel1.Size = New System.Drawing.Size(40, 13)
        Me.ZctSOTLabel1.TabIndex = 1
        Me.ZctSOTLabel1.Text = "Total:"
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Controls.Add(Me.ZctSOTButton2)
        Me.ZctSOTGroupBox1.Controls.Add(Me.Button1)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdImprimir)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdAceptar)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(323, 443)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(566, 51)
        Me.ZctSOTGroupBox1.TabIndex = 5
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'ZctSOTButton2
        '
        Me.ZctSOTButton2.Location = New System.Drawing.Point(428, 14)
        Me.ZctSOTButton2.Name = "ZctSOTButton2"
        Me.ZctSOTButton2.Size = New System.Drawing.Size(112, 28)
        Me.ZctSOTButton2.TabIndex = 9
        Me.ZctSOTButton2.Text = "Actualizar"
        Me.ZctSOTButton2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(310, 14)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 28)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Cancelar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cmdImprimir
        '
        Me.cmdImprimir.Location = New System.Drawing.Point(71, 14)
        Me.cmdImprimir.Name = "cmdImprimir"
        Me.cmdImprimir.Size = New System.Drawing.Size(112, 28)
        Me.cmdImprimir.TabIndex = 0
        Me.cmdImprimir.Text = "Imprimir"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdImprimir, "Imprime la orden de trabajo")
        Me.cmdImprimir.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAceptar.Location = New System.Drawing.Point(192, 14)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(112, 28)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Tag = "Graba las modificaciones realizadas, si el código es nuevo da de alta la orden de" & _
    " compra"
        Me.cmdAceptar.Text = "Grabar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdAceptar, "Graba las modificaciones realizadas, si el código es nuevo da de alta la orden de" & _
        " compra")
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'zctFolio
        '
        Me.zctFolio.Controls.Add(Me.cmdDer)
        Me.zctFolio.Controls.Add(Me.cmdIzq)
        Me.zctFolio.Controls.Add(Me.txtFolio)
        Me.zctFolio.Location = New System.Drawing.Point(3, 5)
        Me.zctFolio.Name = "zctFolio"
        Me.zctFolio.Size = New System.Drawing.Size(235, 51)
        Me.zctFolio.TabIndex = 0
        Me.zctFolio.TabStop = False
        '
        'cmdDer
        '
        Me.cmdDer.Location = New System.Drawing.Point(204, 19)
        Me.cmdDer.Name = "cmdDer"
        Me.cmdDer.Size = New System.Drawing.Size(19, 23)
        Me.cmdDer.TabIndex = 8
        Me.cmdDer.Text = ">"
        Me.cmdDer.UseVisualStyleBackColor = True
        '
        'cmdIzq
        '
        Me.cmdIzq.Location = New System.Drawing.Point(184, 19)
        Me.cmdIzq.Name = "cmdIzq"
        Me.cmdIzq.Size = New System.Drawing.Size(19, 23)
        Me.cmdIzq.TabIndex = 7
        Me.cmdIzq.Text = "<"
        Me.cmdIzq.UseVisualStyleBackColor = True
        '
        'txtFolio
        '
        Me.txtFolio.Location = New System.Drawing.Point(9, 17)
        Me.txtFolio.Multiline = False
        Me.txtFolio.Name = "txtFolio"
        Me.txtFolio.Nombre = "Folio:"
        Me.txtFolio.Size = New System.Drawing.Size(176, 28)
        Me.txtFolio.TabIndex = 0
        Me.txtFolio.Tag = "Folio de la Orden de compra"
        Me.txtFolio.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtFolio.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtFolio, "Folio de la Orden de compra")
        '
        'ColArt
        '
        Me.ColArt.HeaderText = "Artículo"
        Me.ColArt.Name = "ColArt"
        Me.ColArt.Width = 120
        '
        'ColDesc
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue
        Me.ColDesc.DefaultCellStyle = DataGridViewCellStyle1
        Me.ColDesc.HeaderText = "Descripción"
        Me.ColDesc.Name = "ColDesc"
        Me.ColDesc.ReadOnly = True
        Me.ColDesc.Width = 200
        '
        'ColExist
        '
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = "0"
        Me.ColExist.DefaultCellStyle = DataGridViewCellStyle2
        Me.ColExist.HeaderText = "Existencias"
        Me.ColExist.Name = "ColExist"
        Me.ColExist.ReadOnly = True
        Me.ColExist.Width = 80
        '
        'ColSurt
        '
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = "0"
        Me.ColSurt.DefaultCellStyle = DataGridViewCellStyle3
        Me.ColSurt.HeaderText = "Surtido"
        Me.ColSurt.Name = "ColSurt"
        Me.ColSurt.Width = 80
        '
        'ColCosto
        '
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.ColCosto.DefaultCellStyle = DataGridViewCellStyle4
        Me.ColCosto.HeaderText = "Costo"
        Me.ColCosto.Name = "ColCosto"
        Me.ColCosto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ColCosto.Visible = False
        Me.ColCosto.Width = 80
        '
        'SubTotal
        '
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = "0"
        Me.SubTotal.DefaultCellStyle = DataGridViewCellStyle5
        Me.SubTotal.HeaderText = "Sub Total"
        Me.SubTotal.Name = "SubTotal"
        Me.SubTotal.ReadOnly = True
        Me.SubTotal.Visible = False
        Me.SubTotal.Width = 80
        '
        'ColCod
        '
        Me.ColCod.HeaderText = "Código"
        Me.ColCod.Name = "ColCod"
        Me.ColCod.Visible = False
        '
        'ZctIncAlmacenes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(892, 497)
        Me.Controls.Add(Me.ZctSOTButton1)
        Me.Controls.Add(Me.ZctAlmEntrada)
        Me.Controls.Add(Me.cmdCancelar)
        Me.Controls.Add(Me.ZctAlmSalida)
        Me.Controls.Add(Me.ZctGroupControls7)
        Me.Controls.Add(Me.ZctGroupControls3)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.zctFolio)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ZctIncAlmacenes"
        Me.Text = "Traspaso entre almacenes"
        Me.ZctGroupControls7.ResumeLayout(False)
        Me.ZctGroupControls3.ResumeLayout(False)
        Me.ZctGroupControls3.PerformLayout()
        CType(Me.DGridArticulos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        Me.zctFolio.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents zctFolio As ZctSOT.ZctGroupControls
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents txtFolio As ZctSOT.ZctControlTexto
    Friend WithEvents ZctGroupControls3 As ZctSOT.ZctGroupControls
    Friend WithEvents DGridArticulos As ZctSOT.ZctSotGrid
    Friend WithEvents ZctSOTLabel1 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTLabel3 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblSubTotal As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTLabel2 As ZctSOT.ZctSOTLabel
    Friend WithEvents lblIva As ZctSOT.ZctSOTLabelDesc
    Friend WithEvents ZctSOTToolTip1 As ZctSOT.ZctSOTToolTip
    Friend WithEvents ZctGroupControls7 As ZctSOT.ZctGroupControls
    Friend WithEvents DtAplicacion As ZctSOT.ZctControlFecha
    Friend WithEvents ZctSOTLabel5 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSOTLabel6 As ZctSOT.ZctSOTLabel
    Friend WithEvents ZctSOTLabel4 As ZctSOT.ZctSOTLabel
    Friend WithEvents cmdImprimir As ZctSOT.ZctSOTButton
    Friend WithEvents cmdDer As ZctSOT.ZctSOTButton
    Friend WithEvents cmdIzq As ZctSOT.ZctSOTButton
    Friend WithEvents ZctAlmSalida As ZctSOT.ZctControlCombo
    Friend WithEvents ZctAlmEntrada As ZctSOT.ZctControlCombo
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ZctSOTButton1 As ZctSOT.ZctSOTButton
    Friend WithEvents ZctSOTButton2 As ZctSOT.ZctSOTButton
    Friend WithEvents ColArt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColExist As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSurt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColCod As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
