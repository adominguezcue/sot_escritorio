﻿Option Strict On

Imports System.Data.SqlClient
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core
Imports System.IO
Imports System.Text
Imports SOTControladores.Controladores

Public Class ZctArtViejos
    Friend _Controlador As New Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _Permisos As Permisos.Controlador.Controlador.vistapermisos
    'Private data As DataTable
    Private _Meses As Integer = 6

#Region "Exportar"

    Private Sub exportarAExcelHtml()

        If Me.DGridArticulos.DataSource Is Nothing Then
            MsgBox("No hay datos a exportar", MsgBoxStyle.Exclamation, "Atención")
            Exit Sub
        Else
            Dim dialog As New SaveFileDialog

            If dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then

                Dim grind As New System.Web.UI.WebControls.GridView
                grind.HeaderStyle.Font.Bold = True
                grind.DataSource = Me.DGridArticulos.DataSource
                grind.DataMember = Me.DGridArticulos.DataMember
                grind.DataBind()

                Using sw As New StreamWriter(dialog.FileName & ".xls")
                    Using hw As New System.Web.UI.HtmlTextWriter(sw)
                        grind.RenderControl(hw)
                    End Using
                End Using
            End If
        End If
    End Sub

    Private Sub crystalReport()
        If Me.DGridArticulos.DataSource Is Nothing Then
            MsgBox("No hay datos para imprimir", MsgBoxStyle.Exclamation, "Atención")
            Exit Sub
        Else
            Dim visor As New PkVisorRpt
            visor.MdiParent = Me.MdiParent
            visor.sParam = "Almacen;" & Me.ZctControlCombo1.ValueItemStr
            visor.sRpt = "C:\REPORTES\VelVentas.rpt"
            visor.Show()
        End If
    End Sub

#End Region

#Region "Datos"

    Private Function obtenerSentencia(Optional ByVal velocidad As String = "") As String
        Dim sql As StringBuilder = New StringBuilder("select t.Código as Artículo, t.Velocidad, a.Desc_Art as Descripción, " & _
                                   "al.Desc_CatAlm as Almacén,aa.CostoProm_Art as Costo, aa.Exist_Art as Existencias " & _
                                   "from tbl3 t, ZctCatArt a, ZctArtXAlm aa, ZctCatAlm al " & _
                                   "Where t.Código = a.Cod_Art and aa.Cod_Art = a.Cod_Art and al.Cod_Alm = aa.Cod_Alm " & _
                                   "and aa.Cod_Alm = ")
        sql.Append(Me.ZctControlCombo1.ValueItemStr)

        If Not velocidad.Equals(String.Empty) Then
            sql.Append(" and t.Velocidad = '")
            sql.Append(velocidad)
            sql.Append("'")
        End If

        Return sql.ToString


    End Function


    Private Sub cargarDatos()
        cargarProcedimiento()
        Dim db As New Datos.ZctDataBase
        db.SQL = obtenerSentencia()

        Try
            db.InciaDataAdapter()
            Me.DGridArticulos.DataSource = New DataView(db.GetDataTable)
        Catch ex As Exception
            MsgBox("Error: " & vbCr & ex.ToString _
                              , MsgBoxStyle.Exclamation, "Error")
        Finally
            db.Dispose()
        End Try
    End Sub

    Private Sub cargarProcedimiento()
        Dim db As New Datos.ZctDataBase()

        If IsNumeric(Me.TxtMeses.Text) Then
            Try
                Me._Meses = CInt(Me.TxtMeses.Text)
            Catch ex As Exception
                Me._Meses = 6
                Me.TxtMeses.Text = "6"
            End Try

        End If

        db.IniciaProcedimiento("SP_ArtViejos")
        db.AddParameterSP("@Meses", Me._Meses, SqlDbType.Int)
        db.AddParameterSP("@Almacen", Me.ZctControlCombo1.ValueItem, SqlDbType.Int)
        'db.sqlConn.CommandTimeout = 180
        Try
            db.GetReaderSP()
            darFormato()
        Catch ex As Exception
            MsgBox("Error: " & vbCr & ex.ToString _
                   , MsgBoxStyle.Exclamation, "Error")
        Finally
            db.Dispose()
        End Try
    End Sub

    Private Sub cargarVentasMeses()
        Dim db As New Datos.ZctDataBase()
        If Me.DGridArticulos.RowCount > 0 Then
            If IsNumeric(Me.TxtMeses.Text) Then
                Try
                    Me._Meses = CInt(Me.TxtMeses.Text)
                Catch ex As Exception
                    Me._Meses = 6
                    Me.TxtMeses.Text = "6"
                End Try

            End If

            db.IniciaProcedimiento("SP_VentasMes")
            db.AddParameterSP("@Código", Me.DGridArticulos.CurrentRow.Cells.Item(0).Value.ToString, SqlDbType.Char)
            db.AddParameterSP("@Meses", Me._Meses, SqlDbType.Int)

            Try
                Me.DGridArticulos.DataSource = New DataView(db.GetReaderSP().Tables(0))
                darFormato()
            Catch ex As Exception
                MsgBox("Error: " & vbCr & ex.ToString _
                       , MsgBoxStyle.Exclamation, "Error")
            Finally
                db.Dispose()
            End Try
        End If
    End Sub

    Private Sub cargarVelocidades()

        Me.ComboVelocidades.Items.Add("Todas")

        Dim db As New Datos.ZctDataBase
        db.SQL = "SELECT Nombre FROM VelocidadMov"

        Try
            'Me.ComboBox1.DataSource = db.GetDataTable
            For Each row As DataRow In db.GetDataTable.Rows
                Me.ComboVelocidades.Items.Add(row(0))
            Next
        Catch ex As Exception
            MsgBox("Error: " & vbCr & ex.ToString _
                   , MsgBoxStyle.Exclamation, "Error")

        Finally
            db.Dispose()
            Me.ComboVelocidades.SelectedIndex = 0
        End Try

    End Sub

    Private Sub buscarCódigo()
        If (Not Me.txtCodigo.Text.Equals(String.Empty) AndAlso (Me.DGridArticulos.DataSource IsNot Nothing)) Then
            CType(Me.DGridArticulos.DataSource, DataView).RowFilter = "Artículo = '" & Me.txtCodigo.Text & "'"
        End If
    End Sub

    Private Sub filtrarVelocidades()

        ' Si se selecciono una velocidad para filtrar, y si ya hay una cache de datos
        If Me.DGridArticulos.DataSource IsNot Nothing Then
            If CStr(Me.ComboVelocidades.SelectedItem) = "Todas" Then
                cargarDatos()
                darFormato()
            Else

                Dim db As New Datos.ZctDataBase
                db.SQL = obtenerSentencia(CStr(Me.ComboVelocidades.SelectedItem))

                Try
                    db.InciaDataAdapter()
                    Me.DGridArticulos.DataSource = New DataView(db.GetDataTable)
                    darFormato()
                Catch ex As Exception
                    MsgBox("Error: " & vbCr & ex.ToString _
                                      , MsgBoxStyle.Exclamation, "Error")
                Finally
                    db.Dispose()
                End Try
            End If
        End If
    End Sub

    Private Sub contarArticulos()
        If Me.DGridArticulos.DataSource IsNot Nothing Then
            Dim db As New Datos.ZctDataBase

            db.SQL = _
                "Select Velocidad, count(Velocidad) as 'Número de artículos', sum (CostoProm_Art) as 'Costo'" & _
                "from tbl3 t, ZctArtXAlm aa, ZctCatAlm al " & _
                "where t.Código = aa.Cod_Art And aa.Cod_Alm = al.Cod_Alm " & _
                "and al.Cod_Alm = " & Me.ZctControlCombo1.ValueItemStr & _
                " group by Velocidad"

            Try
                db.InciaDataAdapter()
                Me.DGridArticulos.DataSource = New DataView(db.GetDataTable)
                darFormato()
            Catch ex As Exception
                MsgBox("Error: " & vbCr & ex.ToString _
                                  , MsgBoxStyle.Exclamation, "Error")
            Finally
                db.Dispose()
            End Try
        End If
    End Sub

#End Region

#Region "Eventos"
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnExcel.Click
        exportarAExcelHtml()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnDetalles.Click
        verDetalles()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAceptar.Click
        MostrarDatos()
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnFiltrar.Click
        filtrarVelocidades()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBuscar.Click
        buscarCódigo()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnVentas.Click
        mostrarVentas()
    End Sub

    Private Sub BtnAnterior_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAnterior.Click
        Me._Meses = Me._Meses - 1
        Me.TxtMeses.Text = CStr(Me._Meses)
    End Sub

    Private Sub BtnSiguiente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSiguiente.Click
        Me._Meses = Me._Meses + 1
        Me.TxtMeses.Text = CStr(Me._Meses)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        limpiar()
    End Sub

    Private Sub ZctArtViejos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F1 Then
            MostrarAyuda()
        End If
    End Sub
    Private Sub BtnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnImprimir.Click
        crystalReport()
    End Sub

    Private Sub ZctArtViejos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _Permisos = _Controlador.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
        With _Permisos
            DGridArticulos.AllowUserToAddRows = .agrega
            DGridArticulos.AllowUserToDeleteRows = .elimina
            DGridArticulos.ReadOnly = Not (.edita Or .agrega)
            'If .edita = False And .agrega = False And .elimina = False Then
            '    cmdAceptar.Enabled = False
            'Else
            '    cmdAceptar.Enabled = True
            'End If
            'ZctSOTButton1.Enabled = .edita
            'ZctSOTButton2.Enabled = .edita
            'Button1.Enabled = .edita
            'cmdCancelar.Enabled = .edita
        End With
        Me.ZctControlCombo1.GetData("ZctCatAlm", False)
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        mostrarCuentaArticulos()

    End Sub


#End Region


#Region "Formulario"
    ''' <summary>
    ''' Da el formato requerido a las columnas
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub darFormato()

        For Each columna As DataGridViewColumn In Me.DGridArticulos.Columns
            columna.DefaultCellStyle.BackColor = Color.LightBlue
            columna.ReadOnly = True
            columna.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next

    End Sub

    Private Sub limpiar()

        Me.DGridArticulos.DataSource = Nothing
        Me._Meses = 6
        Me.TxtMeses.Text = CStr(Me._Meses)
        Me.txtCodigo.Text = String.Empty
        If Me.ComboVelocidades.Items.Count > 0 Then Me.ComboVelocidades.SelectedIndex = 0
    End Sub

    Private Sub MostrarAyuda()
        Dim ayuda As New ZctFrmAyuda
        ayuda.Texto = _
        "Esta pantalla sirve para revisar la velocidad de venta de los articulos," & vbCr & _
        "para establecer la velocidad de los articulos, introdusca el número de meses" & vbCr & _
        "que desea revisar. " & vbCr & _
        "Puede exportar los resultado a excel mediante el boton ""Exportar a excel""." & vbCr & _
        "Puede imprimir los datos obtenidos mediate un Crystal report dando click al boton ""Imprimir""." & vbCr & _
        "Para ver lo detalles de un artículo dado, seleccione el renglón que desea y de click en" & vbCr & _
        " ""Ver detalles del artículo"". " & vbCr & _
        "Para ver los meses que un artículo tuvo ventas, seleccione un renglón y de click en ""Ventas del artículo""."

        ayuda.ShowDialog(Me)

    End Sub

    Private Sub verDetalles()
        If Me.DGridArticulos.RowCount > 0 Then
            Dim CatArticulos As New ZctCatArt
            CatArticulos.MdiParent = ZctSOTMain
            CatArticulos.CargarDatos(Me.DGridArticulos.CurrentRow.Cells(0).Value.ToString)
        End If
    End Sub

    ''' <summary>
    ''' Muestra los datos de los articulos, en el almacen dado
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub MostrarDatos()
        cargarDatos()
        darFormato()
        'cargarDatos2(obtenerDatos())
        If Me.ComboVelocidades.Items.Count < 2 Then
            Me.cargarVelocidades()
        End If
        Me.BtnAceptar.Text = "Actualizar"
    End Sub

    ''' <summary>
    ''' Nos muestra el total de artículos de cada tipo de velocidad
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub mostrarCuentaArticulos()

        If Me.Button1.Text.Equals("Contar artículos") Then
            contarArticulos()
            Me.Button1.Text = "Regresar"

        ElseIf Me.Button1.Text.Equals("Regresar") Then
            MostrarDatos()
            Me.Button1.Text = "Contar artículos"
        End If
    End Sub

    Private Sub mostrarVentas()
        If Me.BtnVentas.Text.Equals("Ventas del articulo") Then
            cargarVentasMeses()
            Me.BtnVentas.Text = "Regresar"

        ElseIf Me.BtnVentas.Text.Equals("Regresar") Then
            MostrarDatos()
            Me.BtnVentas.Text = "Ventas del articulo"
        End If

    End Sub

#End Region


End Class
