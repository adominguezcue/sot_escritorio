<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctMotos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox()
        Me.cmdEliminar = New ZctSOT.ZctSOTButton()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.ZctGroupControls3 = New ZctSOT.ZctGroupControls()
        Me.txtVin = New ZctSOT.ZctControlTexto()
        Me.ZctGroupControls2 = New ZctSOT.ZctGroupControls()
        Me.txtECO = New ZctSOT.ZctControlTexto()
        Me.ZctCodRep = New ZctSOT.ZctControlBusqueda()
        Me.Dtfecha_baja = New ZctSOT.ZctControlFecha()
        Me.Dtfecha_alta = New ZctSOT.ZctControlFecha()
        Me.chkasegurada = New System.Windows.Forms.CheckBox()
        Me.chkdisable = New System.Windows.Forms.CheckBox()
        Me.cboUbicacion = New ZctSOT.ZctControlCombo()
        Me.cboModelo = New ZctSOT.ZctControlCombo()
        Me.CboMarca = New ZctSOT.ZctControlCombo()
        Me.txtAnno = New ZctSOT.ZctControlTexto()
        Me.txtPlacas = New ZctSOT.ZctControlTexto()
        Me.txtNumMotor = New ZctSOT.ZctControlTexto()
        Me.TxtCliente = New ZctSOT.ZctControlBusqueda()
        Me.ZctGroupControls1 = New ZctSOT.ZctGroupControls()
        Me.txtCodMoto = New ZctSOT.ZctControlBusqueda()
        Me.ZctSOTToolTip1 = New ZctSOT.ZctSOTToolTip()
        Me.ZctSOTGroupBox1.SuspendLayout()
        Me.ZctGroupControls3.SuspendLayout()
        Me.ZctGroupControls2.SuspendLayout()
        Me.ZctGroupControls1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdEliminar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdAceptar)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(10, 359)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(757, 51)
        Me.ZctSOTGroupBox1.TabIndex = 3
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'cmdEliminar
        '
        Me.cmdEliminar.Location = New System.Drawing.Point(403, 17)
        Me.cmdEliminar.Name = "cmdEliminar"
        Me.cmdEliminar.Size = New System.Drawing.Size(112, 28)
        Me.cmdEliminar.TabIndex = 0
        Me.cmdEliminar.Tag = "Elimina los cambios realizados a la motocicleta;si es un código nuevo lo da de al" & _
    "ta"
        Me.cmdEliminar.Text = "Eliminar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdEliminar, "Elimina los cambios realizados a la motocicleta;si es un código nuevo lo da de al" & _
        "ta")
        Me.cmdEliminar.UseVisualStyleBackColor = True
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(639, 17)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(112, 28)
        Me.cmdCancelar.TabIndex = 2
        Me.cmdCancelar.Tag = "Cancela las modificaciones<BR>Si no se ha seleccionado algún código, cierra la pa" & _
    "ntalla"
        Me.cmdCancelar.Text = "Cancelar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdCancelar, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
        "alla")
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Location = New System.Drawing.Point(521, 17)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(112, 28)
        Me.cmdAceptar.TabIndex = 1
        Me.cmdAceptar.Tag = "Guarda los cambios realizados a la motocicleta;si es un código nuevo lo da de alt" & _
    "a"
        Me.cmdAceptar.Text = "Guardar"
        Me.ZctSOTToolTip1.SetToolTip(Me.cmdAceptar, "Guarda los cambios realizados a la motocicleta;si es un código nuevo lo da de alt" & _
        "a")
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'ZctGroupControls3
        '
        Me.ZctGroupControls3.Controls.Add(Me.txtVin)
        Me.ZctGroupControls3.Location = New System.Drawing.Point(465, 3)
        Me.ZctGroupControls3.Name = "ZctGroupControls3"
        Me.ZctGroupControls3.Size = New System.Drawing.Size(295, 53)
        Me.ZctGroupControls3.TabIndex = 1
        Me.ZctGroupControls3.TabStop = False
        '
        'txtVin
        '
        Me.txtVin.Location = New System.Drawing.Point(6, 19)
        Me.txtVin.Multiline = False
        Me.txtVin.Name = "txtVin"
        Me.txtVin.Nombre = "Vin:"
        Me.txtVin.Size = New System.Drawing.Size(283, 28)
        Me.txtVin.TabIndex = 0
        Me.txtVin.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtVin.ToolTip = ""
        '
        'ZctGroupControls2
        '
        Me.ZctGroupControls2.Controls.Add(Me.txtECO)
        Me.ZctGroupControls2.Controls.Add(Me.ZctCodRep)
        Me.ZctGroupControls2.Controls.Add(Me.Dtfecha_baja)
        Me.ZctGroupControls2.Controls.Add(Me.Dtfecha_alta)
        Me.ZctGroupControls2.Controls.Add(Me.chkasegurada)
        Me.ZctGroupControls2.Controls.Add(Me.chkdisable)
        Me.ZctGroupControls2.Controls.Add(Me.cboUbicacion)
        Me.ZctGroupControls2.Controls.Add(Me.cboModelo)
        Me.ZctGroupControls2.Controls.Add(Me.CboMarca)
        Me.ZctGroupControls2.Controls.Add(Me.txtAnno)
        Me.ZctGroupControls2.Controls.Add(Me.txtPlacas)
        Me.ZctGroupControls2.Controls.Add(Me.txtNumMotor)
        Me.ZctGroupControls2.Controls.Add(Me.TxtCliente)
        Me.ZctGroupControls2.Location = New System.Drawing.Point(3, 60)
        Me.ZctGroupControls2.Name = "ZctGroupControls2"
        Me.ZctGroupControls2.Size = New System.Drawing.Size(757, 293)
        Me.ZctGroupControls2.TabIndex = 2
        Me.ZctGroupControls2.TabStop = False
        '
        'txtECO
        '
        Me.txtECO.Location = New System.Drawing.Point(24, 332)
        Me.txtECO.Multiline = False
        Me.txtECO.Name = "txtECO"
        Me.txtECO.Nombre = "ECO:"
        Me.txtECO.Size = New System.Drawing.Size(321, 28)
        Me.txtECO.TabIndex = 14
        Me.txtECO.Tag = "Número economico de la motocicleta"
        Me.txtECO.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtECO.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtECO, "Número economico de la motocicleta")
        Me.txtECO.Visible = False
        '
        'ZctCodRep
        '
        Me.ZctCodRep.Descripcion = "ZctSOTLabelDesc1"
        Me.ZctCodRep.Location = New System.Drawing.Point(4, 280)
        Me.ZctCodRep.Name = "ZctCodRep"
        Me.ZctCodRep.Nombre = "Repartidor:"
        Me.ZctCodRep.Size = New System.Drawing.Size(727, 28)
        Me.ZctCodRep.SPName = Nothing
        Me.ZctCodRep.SPParametros = Nothing
        Me.ZctCodRep.SpVariables = Nothing
        Me.ZctCodRep.SqlBusqueda = Nothing
        Me.ZctCodRep.TabIndex = 13
        Me.ZctCodRep.Tabla = Nothing
        Me.ZctCodRep.Tag = "Teclee el código del responsable o bien presione [F3] para buscarlo"
        Me.ZctCodRep.TextWith = 111
        Me.ZctCodRep.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctCodRep.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.ZctCodRep, "Teclee el código del responsable o bien presione [F3] para buscarlo")
        Me.ZctCodRep.Validar = True
        Me.ZctCodRep.Visible = False
        '
        'Dtfecha_baja
        '
        Me.Dtfecha_baja.DateWith = 91
        Me.Dtfecha_baja.Location = New System.Drawing.Point(4, 247)
        Me.Dtfecha_baja.Name = "Dtfecha_baja"
        Me.Dtfecha_baja.Nombre = "Fecha Baja:"
        Me.Dtfecha_baja.Size = New System.Drawing.Size(268, 27)
        Me.Dtfecha_baja.TabIndex = 12
        Me.Dtfecha_baja.Value = New Date(2015, 10, 17, 21, 10, 50, 0)
        '
        'Dtfecha_alta
        '
        Me.Dtfecha_alta.DateWith = 91
        Me.Dtfecha_alta.Location = New System.Drawing.Point(7, 217)
        Me.Dtfecha_alta.Name = "Dtfecha_alta"
        Me.Dtfecha_alta.Nombre = "Fecha Alta:"
        Me.Dtfecha_alta.Size = New System.Drawing.Size(265, 27)
        Me.Dtfecha_alta.TabIndex = 11
        Me.Dtfecha_alta.Value = New Date(2015, 10, 17, 21, 10, 50, 0)
        '
        'chkasegurada
        '
        Me.chkasegurada.AutoSize = True
        Me.chkasegurada.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkasegurada.Location = New System.Drawing.Point(409, 247)
        Me.chkasegurada.Name = "chkasegurada"
        Me.chkasegurada.Size = New System.Drawing.Size(80, 17)
        Me.chkasegurada.TabIndex = 10
        Me.chkasegurada.Text = "Asegurado:"
        Me.chkasegurada.UseVisualStyleBackColor = True
        '
        'chkdisable
        '
        Me.chkdisable.AutoSize = True
        Me.chkdisable.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkdisable.Location = New System.Drawing.Point(405, 217)
        Me.chkdisable.Name = "chkdisable"
        Me.chkdisable.Size = New System.Drawing.Size(84, 17)
        Me.chkdisable.TabIndex = 9
        Me.chkdisable.Text = "Deshabilitar:"
        Me.chkdisable.UseVisualStyleBackColor = True
        '
        'cboUbicacion
        '
        Me.cboUbicacion.Location = New System.Drawing.Point(405, 174)
        Me.cboUbicacion.Name = "cboUbicacion"
        Me.cboUbicacion.Nombre = "Ubicación:"
        Me.cboUbicacion.Size = New System.Drawing.Size(344, 28)
        Me.cboUbicacion.TabIndex = 8
        Me.cboUbicacion.Tag = "Catálogo de ubicaciones"
        Me.ZctSOTToolTip1.SetToolTip(Me.cboUbicacion, "Catálogo de ubicaciones")
        Me.cboUbicacion.value = Nothing
        Me.cboUbicacion.ValueItem = 0
        Me.cboUbicacion.ValueItemStr = Nothing
        '
        'cboModelo
        '
        Me.cboModelo.Location = New System.Drawing.Point(417, 128)
        Me.cboModelo.Name = "cboModelo"
        Me.cboModelo.Nombre = "Modelo:"
        Me.cboModelo.Size = New System.Drawing.Size(332, 28)
        Me.cboModelo.TabIndex = 7
        Me.cboModelo.Tag = "Modelo de la motocicleta"
        Me.ZctSOTToolTip1.SetToolTip(Me.cboModelo, "Modelo de la motocicleta")
        Me.cboModelo.value = Nothing
        Me.cboModelo.ValueItem = 0
        Me.cboModelo.ValueItemStr = Nothing
        '
        'CboMarca
        '
        Me.CboMarca.Location = New System.Drawing.Point(422, 83)
        Me.CboMarca.Name = "CboMarca"
        Me.CboMarca.Nombre = "Marca:"
        Me.CboMarca.Size = New System.Drawing.Size(327, 28)
        Me.CboMarca.TabIndex = 6
        Me.CboMarca.Tag = "Marca de la motocicleta"
        Me.ZctSOTToolTip1.SetToolTip(Me.CboMarca, "Marca de la motocicleta")
        Me.CboMarca.value = Nothing
        Me.CboMarca.ValueItem = 0
        Me.CboMarca.ValueItemStr = Nothing
        '
        'txtAnno
        '
        Me.txtAnno.Location = New System.Drawing.Point(35, 128)
        Me.txtAnno.Multiline = False
        Me.txtAnno.Name = "txtAnno"
        Me.txtAnno.Nombre = "Año:"
        Me.txtAnno.Size = New System.Drawing.Size(308, 28)
        Me.txtAnno.TabIndex = 2
        Me.txtAnno.Tag = "Año de la motocicleta"
        Me.txtAnno.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtAnno.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtAnno, "Año de la motocicleta")
        '
        'txtPlacas
        '
        Me.txtPlacas.Location = New System.Drawing.Point(22, 174)
        Me.txtPlacas.Multiline = False
        Me.txtPlacas.Name = "txtPlacas"
        Me.txtPlacas.Nombre = "Placas:"
        Me.txtPlacas.Size = New System.Drawing.Size(321, 28)
        Me.txtPlacas.TabIndex = 3
        Me.txtPlacas.Tag = "Placas de la motocicleta"
        Me.txtPlacas.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtPlacas.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtPlacas, "Placas de la motocicleta")
        '
        'txtNumMotor
        '
        Me.txtNumMotor.Location = New System.Drawing.Point(7, 83)
        Me.txtNumMotor.Multiline = False
        Me.txtNumMotor.Name = "txtNumMotor"
        Me.txtNumMotor.Nombre = "No. Motor:"
        Me.txtNumMotor.Size = New System.Drawing.Size(335, 28)
        Me.txtNumMotor.TabIndex = 1
        Me.txtNumMotor.Tag = "Número de motor de la motocicleta"
        Me.txtNumMotor.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtNumMotor.ToolTip = ""
        Me.ZctSOTToolTip1.SetToolTip(Me.txtNumMotor, "Número de motor de la motocicleta")
        '
        'TxtCliente
        '
        Me.TxtCliente.Descripcion = "ZctSOTLabelDesc1"
        Me.TxtCliente.Location = New System.Drawing.Point(22, 35)
        Me.TxtCliente.Name = "TxtCliente"
        Me.TxtCliente.Nombre = "Cliente:"
        Me.TxtCliente.Size = New System.Drawing.Size(727, 28)
        Me.TxtCliente.SPName = Nothing
        Me.TxtCliente.SPParametros = Nothing
        Me.TxtCliente.SpVariables = Nothing
        Me.TxtCliente.SqlBusqueda = Nothing
        Me.TxtCliente.TabIndex = 0
        Me.TxtCliente.Tabla = Nothing
        Me.TxtCliente.Tag = "Teclee el código del cliente o bien presione [F3] para buscarlo"
        Me.TxtCliente.TextWith = 111
        Me.TxtCliente.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.TxtCliente.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.TxtCliente, "Teclee el código del cliente o bien presione [F3] para buscarlo")
        Me.TxtCliente.Validar = True
        '
        'ZctGroupControls1
        '
        Me.ZctGroupControls1.Controls.Add(Me.txtCodMoto)
        Me.ZctGroupControls1.Location = New System.Drawing.Point(3, 3)
        Me.ZctGroupControls1.Name = "ZctGroupControls1"
        Me.ZctGroupControls1.Size = New System.Drawing.Size(448, 53)
        Me.ZctGroupControls1.TabIndex = 0
        Me.ZctGroupControls1.TabStop = False
        '
        'txtCodMoto
        '
        Me.txtCodMoto.Descripcion = "ZctSOTLabelDesc1"
        Me.txtCodMoto.Location = New System.Drawing.Point(6, 19)
        Me.txtCodMoto.Name = "txtCodMoto"
        Me.txtCodMoto.Nombre = "Código:"
        Me.txtCodMoto.Size = New System.Drawing.Size(431, 28)
        Me.txtCodMoto.SPName = Nothing
        Me.txtCodMoto.SPParametros = Nothing
        Me.txtCodMoto.SpVariables = Nothing
        Me.txtCodMoto.SqlBusqueda = Nothing
        Me.txtCodMoto.TabIndex = 0
        Me.txtCodMoto.Tabla = Nothing
        Me.txtCodMoto.Tag = "Para dar de alta una nueva motocicleta, deje el código en 0.<BR>Para modificar un" & _
    "a motocicleta existente, teclee el código o búsquelo presionando [F3]."
        Me.txtCodMoto.TextWith = 111
        Me.txtCodMoto.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCodMoto.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.ZctSOTToolTip1.SetToolTip(Me.txtCodMoto, "Para dar de alta una nueva motocicleta, deje el código en 0. Para modificar una m" & _
        "otocicleta existente, teclee el código o búsquelo presionando [F3].")
        Me.txtCodMoto.Validar = True
        '
        'ZctMotos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(764, 416)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.ZctGroupControls3)
        Me.Controls.Add(Me.ZctGroupControls2)
        Me.Controls.Add(Me.ZctGroupControls1)
        Me.Name = "ZctMotos"
        Me.Text = "Catálogo de vehículo"
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        Me.ZctGroupControls3.ResumeLayout(False)
        Me.ZctGroupControls2.ResumeLayout(False)
        Me.ZctGroupControls2.PerformLayout()
        Me.ZctGroupControls1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCodMoto As ZctSOT.ZctControlBusqueda
    Friend WithEvents ZctGroupControls1 As ZctSOT.ZctGroupControls
    Friend WithEvents ZctGroupControls2 As ZctSOT.ZctGroupControls
    Friend WithEvents txtVin As ZctSOT.ZctControlTexto
    Friend WithEvents ZctGroupControls3 As ZctSOT.ZctGroupControls
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents TxtCliente As ZctSOT.ZctControlBusqueda
    Friend WithEvents txtAnno As ZctSOT.ZctControlTexto
    Friend WithEvents txtPlacas As ZctSOT.ZctControlTexto
    Friend WithEvents txtNumMotor As ZctSOT.ZctControlTexto
    Friend WithEvents CboMarca As ZctSOT.ZctControlCombo
    Friend WithEvents cboModelo As ZctSOT.ZctControlCombo
    Friend WithEvents ZctSOTToolTip1 As ZctSOT.ZctSOTToolTip
    Friend WithEvents cboUbicacion As ZctSOT.ZctControlCombo
    Friend WithEvents chkdisable As System.Windows.Forms.CheckBox
    Friend WithEvents chkasegurada As System.Windows.Forms.CheckBox
    Friend WithEvents ZctCodRep As ZctSOT.ZctControlBusqueda
    Friend WithEvents Dtfecha_baja As ZctSOT.ZctControlFecha
    Friend WithEvents Dtfecha_alta As ZctSOT.ZctControlFecha
    Friend WithEvents txtECO As ZctSOT.ZctControlTexto
    Friend WithEvents cmdEliminar As ZctSOT.ZctSOTButton
End Class
