<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctCatArt
    Inherits Form 'ZctSOTForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ZctCatArt))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.OFD = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.particulo = New System.Windows.Forms.PictureBox()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGVimagenes = New ZctSOT.ZctSotGrid()
        Me.CodimagenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImagenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tmp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImagenesArticuloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Belimina = New ZctSOT.ZctSOTButton()
        Me.Bedita = New ZctSOT.ZctSOTButton()
        Me.Bnueva = New ZctSOT.ZctSOTButton()
        Me.ZctSOTGroupBox2 = New ZctSOT.ZctSOTGroupBox()
        Me.GridExistencias = New ZctSOT.ZctSotGrid()
        Me.CodigoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AlmacenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExistenciasDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoPromedioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodigoAlmacenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ExistenciasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctSOTGroupBox1 = New ZctSOT.ZctSOTGroupBox()
        Me.cmdPrevArt = New ZctSOT.ZctSOTButton()
        Me.cmdEtArticulo = New ZctSOT.ZctSOTButton()
        Me.cmdPreview = New ZctSOT.ZctSOTButton()
        Me.BimprimeEtiqueta = New ZctSOT.ZctSOTButton()
        Me.btnEliminar = New ZctSOT.ZctSOTButton()
        Me.cmdCancelar = New ZctSOT.ZctSOTButton()
        Me.cmdAceptar = New ZctSOT.ZctSOTButton()
        Me.ZctGroupControls2 = New ZctSOT.ZctGroupControls()
        Me.lblPrecioIVA = New System.Windows.Forms.Label()
        Me.LprecioIva = New System.Windows.Forms.Label()
        Me.TxtDescuento = New ZctSOT.ZctControlTexto()
        Me.TxtRepisa = New ZctSOT.ZctControlTexto()
        Me.TxtSeccion = New ZctSOT.ZctControlTexto()
        Me.TxtPasillo = New ZctSOT.ZctControlTexto()
        Me.txtCaracteristicas = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPais = New ZctSOT.ZctControlTexto()
        Me.cboPresentacion = New ZctSOT.ZctControlCombo()
        Me.CboLinea = New ZctSOT.ZctControlCombo()
        Me.cboDpto = New ZctSOT.ZctControlCombo()
        Me.chkDeshabilita = New System.Windows.Forms.CheckBox()
        Me.txtPrecio = New ZctSOT.ZctControlTexto()
        Me.txtCosto = New ZctSOT.ZctControlTexto()
        Me.CboMarca = New ZctSOT.ZctControlCombo()
        Me.txtDescArt = New ZctSOT.ZctControlTexto()
        Me.ZctGroupControls1 = New ZctSOT.ZctGroupControls()
        Me.txtArticulo = New ZctSOT.ZctControlBusqueda()
        Me.cboTipoArt = New ZctSOT.ZctControlCombo()
        Me.ArticulodBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ZctSOTToolTip = New ZctSOT.ZctSOTToolTip()
        Me.GroupBox1.SuspendLayout()
        CType(Me.particulo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DGVimagenes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImagenesArticuloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ZctSOTGroupBox2.SuspendLayout()
        CType(Me.GridExistencias, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ExistenciasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ZctSOTGroupBox1.SuspendLayout()
        Me.ZctGroupControls2.SuspendLayout()
        Me.ZctGroupControls1.SuspendLayout()
        CType(Me.ArticulodBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OFD
        '
        Me.OFD.Filter = "JPEG(*.jpg)|*.jpg|GIF  (*.gif)|*.gif"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.DGVimagenes)
        Me.GroupBox1.Controls.Add(Me.Belimina)
        Me.GroupBox1.Controls.Add(Me.Bedita)
        Me.GroupBox1.Controls.Add(Me.Bnueva)
        Me.GroupBox1.Controls.Add(Me.particulo)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 228)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(997, 130)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Imágenes de Artículos"
        '
        'particulo
        '
        Me.particulo.Image = CType(resources.GetObject("particulo.Image"), System.Drawing.Image)
        Me.particulo.Location = New System.Drawing.Point(502, 11)
        Me.particulo.Name = "particulo"
        Me.particulo.Size = New System.Drawing.Size(180, 113)
        Me.particulo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.particulo.TabIndex = 18
        Me.particulo.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Codigo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Código"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Almacen"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Almacén"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Existencias"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Existencias"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Costo_Promedio"
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = "0"
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn4.HeaderText = "Costo Promedio"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Codigo_Almacen"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Codigo_Almacen"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.DataGridViewTextBoxColumn6.HeaderText = "tmp"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DGVimagenes
        '
        Me.DGVimagenes.AllowUserToAddRows = False
        Me.DGVimagenes.AllowUserToDeleteRows = False
        Me.DGVimagenes.AutoGenerateColumns = False
        Me.DGVimagenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVimagenes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodimagenDataGridViewTextBoxColumn, Me.ImagenDataGridViewTextBoxColumn, Me.tmp})
        Me.DGVimagenes.DataSource = Me.ImagenesArticuloBindingSource
        Me.DGVimagenes.Location = New System.Drawing.Point(9, 22)
        Me.DGVimagenes.MultiSelect = False
        Me.DGVimagenes.Name = "DGVimagenes"
        Me.DGVimagenes.ReadOnly = True
        Me.DGVimagenes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVimagenes.Size = New System.Drawing.Size(380, 93)
        Me.DGVimagenes.TabIndex = 19
        '
        'CodimagenDataGridViewTextBoxColumn
        '
        Me.CodimagenDataGridViewTextBoxColumn.DataPropertyName = "numimg"
        Me.CodimagenDataGridViewTextBoxColumn.HeaderText = "Num imagen"
        Me.CodimagenDataGridViewTextBoxColumn.Name = "CodimagenDataGridViewTextBoxColumn"
        Me.CodimagenDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ImagenDataGridViewTextBoxColumn
        '
        Me.ImagenDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.ImagenDataGridViewTextBoxColumn.DataPropertyName = "imagen"
        Me.ImagenDataGridViewTextBoxColumn.HeaderText = "imagen"
        Me.ImagenDataGridViewTextBoxColumn.Name = "ImagenDataGridViewTextBoxColumn"
        Me.ImagenDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImagenDataGridViewTextBoxColumn.Visible = False
        '
        'tmp
        '
        Me.tmp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.tmp.HeaderText = "tmp"
        Me.tmp.Name = "tmp"
        Me.tmp.ReadOnly = True
        Me.tmp.Visible = False
        '
        'ImagenesArticuloBindingSource
        '
        Me.ImagenesArticuloBindingSource.DataSource = GetType(ResTotal.Modelo.ZctImagenesArticulos)
        '
        'Belimina
        '
        Me.Belimina.Enabled = False
        Me.Belimina.Location = New System.Drawing.Point(421, 76)
        Me.Belimina.Name = "Belimina"
        Me.Belimina.Size = New System.Drawing.Size(75, 23)
        Me.Belimina.TabIndex = 22
        Me.Belimina.Text = "Eliminar"
        Me.Belimina.UseVisualStyleBackColor = True
        '
        'Bedita
        '
        Me.Bedita.Enabled = False
        Me.Bedita.Location = New System.Drawing.Point(421, 48)
        Me.Bedita.Name = "Bedita"
        Me.Bedita.Size = New System.Drawing.Size(75, 23)
        Me.Bedita.TabIndex = 21
        Me.Bedita.Text = "Cambiar"
        Me.Bedita.UseVisualStyleBackColor = True
        '
        'Bnueva
        '
        Me.Bnueva.Enabled = False
        Me.Bnueva.Location = New System.Drawing.Point(421, 23)
        Me.Bnueva.Name = "Bnueva"
        Me.Bnueva.Size = New System.Drawing.Size(75, 23)
        Me.Bnueva.TabIndex = 20
        Me.Bnueva.Text = "Nueva"
        Me.Bnueva.UseVisualStyleBackColor = True
        '
        'ZctSOTGroupBox2
        '
        Me.ZctSOTGroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctSOTGroupBox2.Controls.Add(Me.GridExistencias)
        Me.ZctSOTGroupBox2.Location = New System.Drawing.Point(5, 398)
        Me.ZctSOTGroupBox2.Name = "ZctSOTGroupBox2"
        Me.ZctSOTGroupBox2.Size = New System.Drawing.Size(997, 118)
        Me.ZctSOTGroupBox2.TabIndex = 3
        Me.ZctSOTGroupBox2.TabStop = False
        Me.ZctSOTGroupBox2.Text = "Existencias"
        '
        'GridExistencias
        '
        Me.GridExistencias.AllowUserToAddRows = False
        Me.GridExistencias.AllowUserToDeleteRows = False
        Me.GridExistencias.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridExistencias.AutoGenerateColumns = False
        Me.GridExistencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridExistencias.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodigoDataGridViewTextBoxColumn, Me.AlmacenDataGridViewTextBoxColumn, Me.ExistenciasDataGridViewTextBoxColumn, Me.CostoPromedioDataGridViewTextBoxColumn, Me.CodigoAlmacenDataGridViewTextBoxColumn})
        Me.GridExistencias.DataSource = Me.ExistenciasBindingSource
        Me.GridExistencias.Location = New System.Drawing.Point(9, 30)
        Me.GridExistencias.Name = "GridExistencias"
        Me.GridExistencias.Size = New System.Drawing.Size(879, 86)
        Me.GridExistencias.TabIndex = 0
        '
        'CodigoDataGridViewTextBoxColumn
        '
        Me.CodigoDataGridViewTextBoxColumn.DataPropertyName = "Codigo"
        Me.CodigoDataGridViewTextBoxColumn.HeaderText = "Código"
        Me.CodigoDataGridViewTextBoxColumn.Name = "CodigoDataGridViewTextBoxColumn"
        '
        'AlmacenDataGridViewTextBoxColumn
        '
        Me.AlmacenDataGridViewTextBoxColumn.DataPropertyName = "Almacen"
        Me.AlmacenDataGridViewTextBoxColumn.HeaderText = "Almacén"
        Me.AlmacenDataGridViewTextBoxColumn.Name = "AlmacenDataGridViewTextBoxColumn"
        '
        'ExistenciasDataGridViewTextBoxColumn
        '
        Me.ExistenciasDataGridViewTextBoxColumn.DataPropertyName = "Existencias"
        Me.ExistenciasDataGridViewTextBoxColumn.HeaderText = "Existencias"
        Me.ExistenciasDataGridViewTextBoxColumn.Name = "ExistenciasDataGridViewTextBoxColumn"
        '
        'CostoPromedioDataGridViewTextBoxColumn
        '
        Me.CostoPromedioDataGridViewTextBoxColumn.DataPropertyName = "Costo_Promedio"
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = "0"
        Me.CostoPromedioDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.CostoPromedioDataGridViewTextBoxColumn.HeaderText = "Costo Promedio"
        Me.CostoPromedioDataGridViewTextBoxColumn.Name = "CostoPromedioDataGridViewTextBoxColumn"
        '
        'CodigoAlmacenDataGridViewTextBoxColumn
        '
        Me.CodigoAlmacenDataGridViewTextBoxColumn.DataPropertyName = "Codigo_Almacen"
        Me.CodigoAlmacenDataGridViewTextBoxColumn.HeaderText = "Codigo_Almacen"
        Me.CodigoAlmacenDataGridViewTextBoxColumn.Name = "CodigoAlmacenDataGridViewTextBoxColumn"
        Me.CodigoAlmacenDataGridViewTextBoxColumn.Visible = False
        '
        'ExistenciasBindingSource
        '
        Me.ExistenciasBindingSource.DataSource = GetType(ZctSOTRDN.Existencias)
        '
        'ZctSOTGroupBox1
        '
        Me.ZctSOTGroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdPrevArt)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdEtArticulo)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdPreview)
        Me.ZctSOTGroupBox1.Controls.Add(Me.BimprimeEtiqueta)
        Me.ZctSOTGroupBox1.Controls.Add(Me.btnEliminar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdCancelar)
        Me.ZctSOTGroupBox1.Controls.Add(Me.cmdAceptar)
        Me.ZctSOTGroupBox1.Location = New System.Drawing.Point(5, 520)
        Me.ZctSOTGroupBox1.Name = "ZctSOTGroupBox1"
        Me.ZctSOTGroupBox1.Size = New System.Drawing.Size(997, 69)
        Me.ZctSOTGroupBox1.TabIndex = 4
        Me.ZctSOTGroupBox1.TabStop = False
        '
        'cmdPrevArt
        '
        Me.cmdPrevArt.Enabled = False
        Me.cmdPrevArt.Location = New System.Drawing.Point(80, 20)
        Me.cmdPrevArt.Name = "cmdPrevArt"
        Me.cmdPrevArt.Size = New System.Drawing.Size(131, 43)
        Me.cmdPrevArt.TabIndex = 0
        Me.cmdPrevArt.Text = "Ver etiqueta"
        Me.cmdPrevArt.UseVisualStyleBackColor = True
        Me.cmdPrevArt.Visible = False
        '
        'cmdEtArticulo
        '
        Me.cmdEtArticulo.Enabled = False
        Me.cmdEtArticulo.Location = New System.Drawing.Point(231, 20)
        Me.cmdEtArticulo.Name = "cmdEtArticulo"
        Me.cmdEtArticulo.Size = New System.Drawing.Size(131, 43)
        Me.cmdEtArticulo.TabIndex = 1
        Me.cmdEtArticulo.Text = "Imprimir etiqueta"
        Me.cmdEtArticulo.UseVisualStyleBackColor = True
        Me.cmdEtArticulo.Visible = False
        '
        'cmdPreview
        '
        Me.cmdPreview.Enabled = False
        Me.cmdPreview.Location = New System.Drawing.Point(382, 20)
        Me.cmdPreview.Name = "cmdPreview"
        Me.cmdPreview.Size = New System.Drawing.Size(158, 43)
        Me.cmdPreview.TabIndex = 2
        Me.cmdPreview.Text = "Ver etiqueta de anaquel"
        Me.cmdPreview.UseVisualStyleBackColor = True
        Me.cmdPreview.Visible = False
        '
        'BimprimeEtiqueta
        '
        Me.BimprimeEtiqueta.Enabled = False
        Me.BimprimeEtiqueta.Location = New System.Drawing.Point(560, 20)
        Me.BimprimeEtiqueta.Name = "BimprimeEtiqueta"
        Me.BimprimeEtiqueta.Size = New System.Drawing.Size(145, 43)
        Me.BimprimeEtiqueta.TabIndex = 3
        Me.BimprimeEtiqueta.Text = "Imprimir etiqueta de anaquel"
        Me.BimprimeEtiqueta.UseVisualStyleBackColor = True
        Me.BimprimeEtiqueta.Visible = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(725, 20)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(75, 43)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.TabStop = False
        Me.btnEliminar.Tag = "Elimina los datos del artículo"
        Me.btnEliminar.Text = "Eliminar"
        Me.ZctSOTToolTip.SetToolTip(Me.btnEliminar, "Elimina los datos del artículo")
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(913, 20)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 43)
        Me.cmdCancelar.TabIndex = 6
        Me.cmdCancelar.Tag = "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
    "alla"
        Me.cmdCancelar.Text = "Cancelar"
        Me.ZctSOTToolTip.SetToolTip(Me.cmdCancelar, "Cancela las modificaciones; Si no se ha seleccionado algún código, cierra la pant" & _
        "alla")
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdAceptar
        '
        Me.cmdAceptar.Location = New System.Drawing.Point(820, 20)
        Me.cmdAceptar.Name = "cmdAceptar"
        Me.cmdAceptar.Size = New System.Drawing.Size(75, 43)
        Me.cmdAceptar.TabIndex = 5
        Me.cmdAceptar.Tag = "Guarda los datos del artículo"
        Me.cmdAceptar.Text = "Guardar"
        Me.ZctSOTToolTip.SetToolTip(Me.cmdAceptar, "Guarda los datos del artículo")
        Me.cmdAceptar.UseVisualStyleBackColor = True
        '
        'ZctGroupControls2
        '
        Me.ZctGroupControls2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls2.Controls.Add(Me.lblPrecioIVA)
        Me.ZctGroupControls2.Controls.Add(Me.LprecioIva)
        Me.ZctGroupControls2.Controls.Add(Me.TxtDescuento)
        Me.ZctGroupControls2.Controls.Add(Me.TxtRepisa)
        Me.ZctGroupControls2.Controls.Add(Me.TxtSeccion)
        Me.ZctGroupControls2.Controls.Add(Me.TxtPasillo)
        Me.ZctGroupControls2.Controls.Add(Me.txtCaracteristicas)
        Me.ZctGroupControls2.Controls.Add(Me.Label1)
        Me.ZctGroupControls2.Controls.Add(Me.txtPais)
        Me.ZctGroupControls2.Controls.Add(Me.cboPresentacion)
        Me.ZctGroupControls2.Controls.Add(Me.CboLinea)
        Me.ZctGroupControls2.Controls.Add(Me.cboDpto)
        Me.ZctGroupControls2.Controls.Add(Me.chkDeshabilita)
        Me.ZctGroupControls2.Controls.Add(Me.txtPrecio)
        Me.ZctGroupControls2.Controls.Add(Me.txtCosto)
        Me.ZctGroupControls2.Controls.Add(Me.CboMarca)
        Me.ZctGroupControls2.Controls.Add(Me.txtDescArt)
        Me.ZctGroupControls2.Location = New System.Drawing.Point(5, 52)
        Me.ZctGroupControls2.Name = "ZctGroupControls2"
        Me.ZctGroupControls2.Size = New System.Drawing.Size(997, 170)
        Me.ZctGroupControls2.TabIndex = 0
        Me.ZctGroupControls2.TabStop = False
        '
        'lblPrecioIVA
        '
        Me.lblPrecioIVA.AutoSize = True
        Me.lblPrecioIVA.Location = New System.Drawing.Point(802, 150)
        Me.lblPrecioIVA.Name = "lblPrecioIVA"
        Me.lblPrecioIVA.Size = New System.Drawing.Size(13, 13)
        Me.lblPrecioIVA.TabIndex = 14
        Me.lblPrecioIVA.Text = "0"
        '
        'LprecioIva
        '
        Me.LprecioIva.AutoSize = True
        Me.LprecioIva.Location = New System.Drawing.Point(688, 150)
        Me.LprecioIva.Name = "LprecioIva"
        Me.LprecioIva.Size = New System.Drawing.Size(76, 13)
        Me.LprecioIva.TabIndex = 13
        Me.LprecioIva.Text = "Precio sin IVA:"
        '
        'TxtDescuento
        '
        Me.TxtDescuento.Location = New System.Drawing.Point(1001, 107)
        Me.TxtDescuento.Multiline = False
        Me.TxtDescuento.Name = "TxtDescuento"
        Me.TxtDescuento.Nombre = "Descuento: %"
        Me.TxtDescuento.Size = New System.Drawing.Size(135, 28)
        Me.TxtDescuento.TabIndex = 15
        Me.TxtDescuento.Tag = "Descuento del Artículo"
        Me.TxtDescuento.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.TxtDescuento.ToolTip = ""
        Me.ZctSOTToolTip.SetToolTip(Me.TxtDescuento, "Descuento del Artículo")
        Me.TxtDescuento.Visible = False
        '
        'TxtRepisa
        '
        Me.TxtRepisa.Location = New System.Drawing.Point(691, 45)
        Me.TxtRepisa.Multiline = False
        Me.TxtRepisa.Name = "TxtRepisa"
        Me.TxtRepisa.Nombre = "Repisa"
        Me.TxtRepisa.Size = New System.Drawing.Size(117, 28)
        Me.TxtRepisa.TabIndex = 3
        Me.TxtRepisa.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtRepisa.ToolTip = ""
        '
        'TxtSeccion
        '
        Me.TxtSeccion.Location = New System.Drawing.Point(809, 17)
        Me.TxtSeccion.Multiline = False
        Me.TxtSeccion.Name = "TxtSeccion"
        Me.TxtSeccion.Nombre = "Sección"
        Me.TxtSeccion.Size = New System.Drawing.Size(110, 28)
        Me.TxtSeccion.TabIndex = 2
        Me.TxtSeccion.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtSeccion.ToolTip = ""
        '
        'TxtPasillo
        '
        Me.TxtPasillo.Location = New System.Drawing.Point(691, 17)
        Me.TxtPasillo.Multiline = False
        Me.TxtPasillo.Name = "TxtPasillo"
        Me.TxtPasillo.Nombre = "Pasillo"
        Me.TxtPasillo.Size = New System.Drawing.Size(112, 28)
        Me.TxtPasillo.TabIndex = 1
        Me.TxtPasillo.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.TxtPasillo.ToolTip = ""
        '
        'txtCaracteristicas
        '
        Me.txtCaracteristicas.Location = New System.Drawing.Point(98, 113)
        Me.txtCaracteristicas.Multiline = True
        Me.txtCaracteristicas.Name = "txtCaracteristicas"
        Me.txtCaracteristicas.Size = New System.Drawing.Size(584, 50)
        Me.txtCaracteristicas.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 113)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Características:"
        '
        'txtPais
        '
        Me.txtPais.Location = New System.Drawing.Point(691, 79)
        Me.txtPais.Multiline = False
        Me.txtPais.Name = "txtPais"
        Me.txtPais.Nombre = "País de procedencia:"
        Me.txtPais.Size = New System.Drawing.Size(294, 28)
        Me.txtPais.TabIndex = 6
        Me.txtPais.Tag = ""
        Me.txtPais.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtPais.ToolTip = ""
        '
        'cboPresentacion
        '
        Me.cboPresentacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPresentacion.Location = New System.Drawing.Point(381, 45)
        Me.cboPresentacion.Name = "cboPresentacion"
        Me.cboPresentacion.Nombre = "Presentación:"
        Me.cboPresentacion.Size = New System.Drawing.Size(301, 28)
        Me.cboPresentacion.TabIndex = 5
        Me.cboPresentacion.Tag = ""
        Me.cboPresentacion.value = Nothing
        Me.cboPresentacion.ValueItem = 0
        Me.cboPresentacion.ValueItemStr = Nothing
        '
        'CboLinea
        '
        Me.CboLinea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboLinea.Location = New System.Drawing.Point(381, 79)
        Me.CboLinea.Name = "CboLinea"
        Me.CboLinea.Nombre = "Sub categoría(*):"
        Me.CboLinea.Size = New System.Drawing.Size(301, 28)
        Me.CboLinea.TabIndex = 8
        Me.CboLinea.Tag = "Los diferentes Tipos de lineas de producto que existen"
        Me.ZctSOTToolTip.SetToolTip(Me.CboLinea, "Los diferentes Tipos de lineas de producto que existen")
        Me.CboLinea.value = Nothing
        Me.CboLinea.ValueItem = 0
        Me.CboLinea.ValueItemStr = Nothing
        '
        'cboDpto
        '
        Me.cboDpto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDpto.Location = New System.Drawing.Point(31, 79)
        Me.cboDpto.Name = "cboDpto"
        Me.cboDpto.Nombre = "Categoría(*):"
        Me.cboDpto.Size = New System.Drawing.Size(344, 28)
        Me.cboDpto.TabIndex = 7
        Me.cboDpto.Tag = "Los diferentes Departamentos de producto que existen"
        Me.ZctSOTToolTip.SetToolTip(Me.cboDpto, "Los diferentes departamentos de producto que existen")
        Me.cboDpto.value = Nothing
        Me.cboDpto.ValueItem = 0
        Me.cboDpto.ValueItemStr = Nothing
        '
        'chkDeshabilita
        '
        Me.chkDeshabilita.AutoSize = True
        Me.chkDeshabilita.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkDeshabilita.Location = New System.Drawing.Point(903, 150)
        Me.chkDeshabilita.Name = "chkDeshabilita"
        Me.chkDeshabilita.Size = New System.Drawing.Size(81, 17)
        Me.chkDeshabilita.TabIndex = 16
        Me.chkDeshabilita.Text = "Deshabilitar"
        Me.chkDeshabilita.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkDeshabilita.UseVisualStyleBackColor = True
        '
        'txtPrecio
        '
        Me.txtPrecio.Location = New System.Drawing.Point(867, 113)
        Me.txtPrecio.Multiline = False
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Nombre = "Precio(*): $"
        Me.txtPrecio.Size = New System.Drawing.Size(118, 28)
        Me.txtPrecio.TabIndex = 10
        Me.txtPrecio.Tag = "Precio del Artículo"
        Me.txtPrecio.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtPrecio.ToolTip = ""
        Me.ZctSOTToolTip.SetToolTip(Me.txtPrecio, "Precio del Artículo")
        '
        'txtCosto
        '
        Me.txtCosto.Location = New System.Drawing.Point(688, 113)
        Me.txtCosto.Multiline = False
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Nombre = "Costo(*): $"
        Me.txtCosto.Size = New System.Drawing.Size(135, 28)
        Me.txtCosto.TabIndex = 9
        Me.txtCosto.Tag = "Costo del Artículo"
        Me.txtCosto.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtCosto.ToolTip = ""
        Me.ZctSOTToolTip.SetToolTip(Me.txtCosto, "Costo del Artículo")
        '
        'CboMarca
        '
        Me.CboMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown
        Me.CboMarca.Location = New System.Drawing.Point(48, 45)
        Me.CboMarca.Name = "CboMarca"
        Me.CboMarca.Nombre = "Marca:"
        Me.CboMarca.Size = New System.Drawing.Size(327, 28)
        Me.CboMarca.TabIndex = 4
        Me.CboMarca.Tag = "Marcas definidas en el cátalogo de Marcas"
        Me.ZctSOTToolTip.SetToolTip(Me.CboMarca, "Marcas definidas en el cátalogo de Marcas")
        Me.CboMarca.value = Nothing
        Me.CboMarca.ValueItem = 0
        Me.CboMarca.ValueItemStr = Nothing
        '
        'txtDescArt
        '
        Me.txtDescArt.Location = New System.Drawing.Point(22, 17)
        Me.txtDescArt.Multiline = False
        Me.txtDescArt.Name = "txtDescArt"
        Me.txtDescArt.Nombre = "Descripción(*):"
        Me.txtDescArt.Size = New System.Drawing.Size(660, 28)
        Me.txtDescArt.TabIndex = 0
        Me.txtDescArt.Tag = "Descripción del artículo"
        Me.txtDescArt.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.txtDescArt.ToolTip = ""
        Me.ZctSOTToolTip.SetToolTip(Me.txtDescArt, "Descripción del artículo")
        '
        'ZctGroupControls1
        '
        Me.ZctGroupControls1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZctGroupControls1.Controls.Add(Me.txtArticulo)
        Me.ZctGroupControls1.Controls.Add(Me.cboTipoArt)
        Me.ZctGroupControls1.Location = New System.Drawing.Point(5, -1)
        Me.ZctGroupControls1.Name = "ZctGroupControls1"
        Me.ZctGroupControls1.Size = New System.Drawing.Size(997, 53)
        Me.ZctGroupControls1.TabIndex = 0
        Me.ZctGroupControls1.TabStop = False
        '
        'txtArticulo
        '
        Me.txtArticulo.Descripcion = ""
        Me.txtArticulo.Location = New System.Drawing.Point(17, 15)
        Me.txtArticulo.Name = "txtArticulo"
        Me.txtArticulo.Nombre = "SKU(*):"
        Me.txtArticulo.Size = New System.Drawing.Size(732, 27)
        Me.txtArticulo.SPName = Nothing
        Me.txtArticulo.SPParametros = Nothing
        Me.txtArticulo.SpVariables = Nothing
        Me.txtArticulo.SqlBusqueda = Nothing
        Me.txtArticulo.TabIndex = 0
        Me.txtArticulo.Tabla = Nothing
        Me.txtArticulo.Tag = "<B>Buscar un Artículo:</B><BR>En caso de que se desee buscar un  artículo Presion" & _
    "e [F3].<BR><B>Dar de alta un  Artículo:</B><BR>En Caso de que se desee dar de al" & _
    "ta un artículo Teclee el  nombre."
        Me.txtArticulo.TextWith = 111
        Me.txtArticulo.Tipo_Dato = ZctSOT.Datos.ClassGen.Tipo_Dato.NUMERICO
        Me.txtArticulo.TipoDato = ZctSOT.Datos.ClassGen.Tipo_Dato.ALFANUMERICO
        Me.ZctSOTToolTip.SetToolTip(Me.txtArticulo, "En caso de que se desee buscar un artículo Presione [F3], para dar de alta un  Ar" & _
        "tículo teclee el  nombre.")
        Me.txtArticulo.Validar = False
        '
        'cboTipoArt
        '
        Me.cboTipoArt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoArt.Location = New System.Drawing.Point(755, 15)
        Me.cboTipoArt.Name = "cboTipoArt"
        Me.cboTipoArt.Nombre = "Tipo(*):"
        Me.cboTipoArt.Size = New System.Drawing.Size(219, 28)
        Me.cboTipoArt.TabIndex = 1
        Me.cboTipoArt.Tag = "Tipo de artículo"
        Me.ZctSOTToolTip.SetToolTip(Me.cboTipoArt, "Tipo de artículo")
        Me.cboTipoArt.value = Nothing
        Me.cboTipoArt.ValueItem = 0
        Me.cboTipoArt.ValueItemStr = Nothing
        '
        'ArticulodBindingSource
        '
        Me.ArticulodBindingSource.DataSource = GetType(ResTotal.Modelo.ZctCatArt)
        '
        'ZctSOTToolTip
        '
        Me.ZctSOTToolTip.AutoPopDelay = 5000
        Me.ZctSOTToolTip.InitialDelay = 50
        Me.ZctSOTToolTip.ReshowDelay = 100
        Me.ZctSOTToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        '
        'ZctCatArt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1014, 595)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ZctSOTGroupBox2)
        Me.Controls.Add(Me.ZctSOTGroupBox1)
        Me.Controls.Add(Me.ZctGroupControls2)
        Me.Controls.Add(Me.ZctGroupControls1)
        Me.Name = "ZctCatArt"
        Me.Text = "Catálogo de Artículos"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.particulo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DGVimagenes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImagenesArticuloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ZctSOTGroupBox2.ResumeLayout(False)
        CType(Me.GridExistencias, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ExistenciasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ZctSOTGroupBox1.ResumeLayout(False)
        Me.ZctGroupControls2.ResumeLayout(False)
        Me.ZctGroupControls2.PerformLayout()
        Me.ZctGroupControls1.ResumeLayout(False)
        CType(Me.ArticulodBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ZctGroupControls1 As ZctSOT.ZctGroupControls
    Friend WithEvents ZctGroupControls2 As ZctSOT.ZctGroupControls
    Friend WithEvents txtDescArt As ZctSOT.ZctControlTexto
    Friend WithEvents CboMarca As ZctSOT.ZctControlCombo
    Friend WithEvents ZctSOTGroupBox1 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents cmdCancelar As ZctSOT.ZctSOTButton
    Friend WithEvents cmdAceptar As ZctSOT.ZctSOTButton
    Friend WithEvents txtCosto As ZctSOT.ZctControlTexto
    Friend WithEvents txtPrecio As ZctSOT.ZctControlTexto
    Friend WithEvents txtArticulo As ZctSOT.ZctControlBusqueda
    Friend WithEvents ZctSOTToolTip As ZctSOT.ZctSOTToolTip
    Friend WithEvents CboLinea As ZctSOT.ZctControlCombo
    Friend WithEvents btnEliminar As ZctSOT.ZctSOTButton
    Friend WithEvents cboDpto As ZctSOT.ZctControlCombo
    Friend WithEvents ZctSOTGroupBox2 As ZctSOT.ZctSOTGroupBox
    Friend WithEvents GridExistencias As ZctSOT.ZctSotGrid
    Friend WithEvents ExistenciasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CodigoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AlmacenDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ExistenciasDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoPromedioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodigoAlmacenDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboTipoArt As ZctSOT.ZctControlCombo
    Friend WithEvents chkDeshabilita As System.Windows.Forms.CheckBox
    Friend WithEvents OFD As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ImagenesArticuloBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ArticulodBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents txtPais As ZctSOT.ZctControlTexto
    Friend WithEvents cboPresentacion As ZctSOT.ZctControlCombo
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DGVimagenes As ZctSOT.ZctSotGrid
    Friend WithEvents CodimagenDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tmp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImagenDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Belimina As ZctSOT.ZctSOTButton
    Friend WithEvents Bedita As ZctSOT.ZctSOTButton
    Friend WithEvents Bnueva As ZctSOT.ZctSOTButton
    Friend WithEvents particulo As System.Windows.Forms.PictureBox
    Friend WithEvents txtCaracteristicas As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtRepisa As ZctSOT.ZctControlTexto
    Friend WithEvents TxtSeccion As ZctSOT.ZctControlTexto
    Friend WithEvents TxtPasillo As ZctSOT.ZctControlTexto
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BimprimeEtiqueta As ZctSOT.ZctSOTButton
    Friend WithEvents cmdPreview As ZctSOT.ZctSOTButton
    Friend WithEvents cmdPrevArt As ZctSOT.ZctSOTButton
    Friend WithEvents cmdEtArticulo As ZctSOT.ZctSOTButton
    Friend WithEvents TxtDescuento As ZctSOT.ZctControlTexto
    Friend WithEvents lblPrecioIVA As System.Windows.Forms.Label
    Friend WithEvents LprecioIva As System.Windows.Forms.Label
End Class
