Imports System.Data.SqlDbType
Imports ResTotal
Imports System.Linq
Imports ZctSOT.Datos
Imports SOTControladores.Controladores
Imports SOTControladores.Fabricas
Imports Transversal.Excepciones
Imports System.IO

Public Class ZctOrdCompra
    Friend _Controlador As New SOTControladores.Controladores.ControladorPermisos 'Permisos.Controlador.Controlador(Datos.DAO.Conexion.CadenaConexion)
    Friend _ControladorCxP As SOTControladores.Controladores.ControladorCuentasPorPagar 'ResTotal.Controlador.ControladorCxP
    Friend _Permisos As Modelo.Seguridad.Dtos.DtoPermisos 'Permisos.Controlador.Controlador.vistapermisos
    Friend _ControladorOC As New SOTControladores.Controladores.ControladorArticulos 'ResTotal.Controlador.ControladorOC(Datos.DAO.Conexion.CadenaConexion)

    Dim iColArt As Integer = 0                                         'Columna del artículo
    Dim iColDesc As Integer = 1                                        'Columna de la Descripción
    Dim iColMarc As Integer = 2
    Dim iColExist As Integer = 3                                       'Columna de la existencia
    Dim iColAlm As Integer = 4                                      'Columna de la existencia
    Dim iColSol As Integer = 5                                         'Columna solicitada
    Dim iColSurt As Integer = 6                                        'Columna del surtido
    Dim iColPrecio As Integer = 7                                      'Columna del Precio
    Dim iColSubTotal As Integer = 8                                    'Columna del subtotal
    Dim iColCod As Integer = 9                                         'Columna del código
    Dim iColOmitirIVA As Integer = 10                                  'Columna de omisión de iva
    Dim sStatus As String                                              'Estatus del documento
    'Dim almacen_por_default As Integer
    Dim sPathRpt As String = "c:\Reportes\ZctRptOC.rpt"

    Dim OcFolio As New zctFolios("OC")
    'Dim fBusqueda As ZctBusqueda
    Dim Editar, Cerrar As Boolean

    Dim parametrosSistema = ControladorParametros.ObtenerParametros()

    Private Sub ZctOrdCompra_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        _ControladorCxP = Nothing
        _ControladorCxP = New SOTControladores.Controladores.ControladorCuentasPorPagar 'ResTotal.Controlador.ControladorCxP(Datos.DAO.Conexion.CadenaConexion)
    End Sub

    Private Sub ZctOrdCompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            _Permisos = _Controlador.ObtenerPermisosActuales() '.getpermisoventana(ControladorBase.UsuarioActual.Id, Me.Text)
            _ControladorCxP = New SOTControladores.Controladores.ControladorCuentasPorPagar 'ResTotal.Controlador.ControladorCxP(Datos.DAO.Conexion.CadenaConexion)

            With _Permisos
                Editar = (.CrearOrdenCompra Or .ModificarOrdenCompra Or .EliminarOrdenCompra)
                DGridArticulos.AllowUserToAddRows = .CrearOrdenCompra
                DGridArticulos.AllowUserToDeleteRows = .EliminarOrdenCompra
                DGridArticulos.ReadOnly = Not (.ModificarOrdenCompra Or .CrearOrdenCompra)
                cmdAceptar.Enabled = Editar
                Cerrar = .CerrarOrdenCompra
            End With
            'Posición Inicial
            Me.Top = 0
            Me.Left = 0

            'Cliente
            'Limpia la descripción
            txtProveedor.ZctSOTLabelDesc1.Text = ""
            'Asigna los datos de la busqueda
            txtProveedor.SqlBusqueda = "SELECT [Cod_Prov] as Código ,[Nom_Prov] as Descripción FROM [ZctSOT].[dbo].[ZctCatProv]"

            'Asigna el nombre del procedimiento almacenado para la busqueda
            txtProveedor.SPName = "SP_ZctCatProvV2F3"
            'Envia las variables del procedimiento almacenado
            txtProveedor.SpVariables = "@Cod_Prov;INT|@Nom_Prov;VARCHAR|@RFC_Prov;VARCHAR|@Dir_Prov;VARCHAR"
            'Envia la tabla de referencia al procedimiento almacenado
            txtProveedor.Tabla = "ZctCatProv"

            txtFolio.DataBindings.Clear()
            txtFolio.Text = OcFolio.Consecutivo 'GetData("Procedimientos_MAX", "ZctEncOC")

            'El estatus es una alta
            sStatus = "A"

            Dim ColAlmacen As DataGridViewComboBoxColumn = CType(DGridArticulos.Columns("ColAlmacen"), DataGridViewComboBoxColumn)
            ColAlmacen.DataSource = New ListAlmacenes("SP_ZctCatAlm_entrada", "@Cod_Alm;int|@Desc_CatAlm;varchar|@ColNuevo;int", "0|0|0")
            ColAlmacen.ValueMember = "CodAlm"
            ColAlmacen.DisplayMember = "DescAlm"

            'almacen_por_default = parametrosSistema.cod_alm_entrada

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub cmdCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancelarEdicion.Click
        If sStatus = "A" Then
            Me.Dispose()
        Else
            plimpia(True)
            sStatus = "A"
            cmdImprimir.Visible = False
        End If
    End Sub
    Private Sub plimpia(ByVal LimpiaFolio As Boolean)
        If LimpiaFolio Then
            txtFolio.DataBindings.Clear()
            txtFolio.Text = ""
            txtFolio.Text = OcFolio.Consecutivo
            txtFolio.Enabled = True
            txtFolio.Focus()
        End If
        Lstatus.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA
        Lstatus.BackColor = Color.Gold
        txtFactura.DataBindings.Clear()
        txtFactura.Text = ""

        txtProveedor.DataBindings.Clear()
        txtProveedor.Text = ""
        txtProveedor.ZctSOTLabelDesc1.Text = ""

        lblSubTotal.Text = FormatCurrency(0)
        lblIva.Text = FormatCurrency(0)
        lblTotal.Text = FormatCurrency(0)

        DGridArticulos.Rows.Clear()

    End Sub

    Private Sub DGridArticulos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellEndEdit
        Try
            If e.ColumnIndex = iColArt Then
                RecargarArticulo(e.RowIndex)
            ElseIf iColSurt = e.ColumnIndex Or iColSol = e.ColumnIndex Then
                pSetColor(e.RowIndex)
                'If iColSurt = e.ColumnIndex Then
                If iColSol = e.ColumnIndex Then
                    'DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String)
                    DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = CType((CType(DGridArticulos.CurrentRow.Cells(iColSol).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String)
                    sGetTotal()
                End If
            ElseIf iColPrecio = e.ColumnIndex Then
                'DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String)
                DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = CType((CType(DGridArticulos.CurrentRow.Cells(iColSol).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String)
                sGetTotal()
            ElseIf Eliminar.Index = e.ColumnIndex Then
                sGetTotal()
            ElseIf iColAlm = e.ColumnIndex Then
                CalculaExistencia(e.RowIndex)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub RecargarArticulo(fila As Integer)
        If DGridArticulos.CurrentRow.Cells(iColArt).Value <> "" Then

            Dim controladorArt As New ControladorArticulos()
            Dim dtDatos = controladorArt.SP_ZctCatArt_Busqueda(DGridArticulos.CurrentRow.Cells(iColArt).Value.ToString(), True)

            'Obtiene los datos del grid
            If IsNothing(dtDatos) Then '.Rows.Count <= 0 Then
                MsgBox("No existe un artículo con el código proporcionado o el artículo no es válido para una orden de compra.", MsgBoxStyle.Information, "SOT")
                '                        DGridArticulos.Rows(e.RowIndex)
                DGridArticulos.CurrentRow.Cells(iColArt).Value = ""


                Exit Sub
            End If
            'Obtiene los datos del grid
            DGridArticulos.CurrentRow.Cells(iColArt).Value = dtDatos.Cod_Art 'dtDatos.Rows.Item(0).Item("Cod_Art").ToString
            DGridArticulos.CurrentRow.Cells(iColDesc).Value = dtDatos.Desc_Art 'dtDatos.Rows.Item(0).Item("Desc_Art").ToString
            DGridArticulos.CurrentRow.Cells(iColSurt).Value = dtDatos.ColSurt 'dtDatos.Rows.Item(0).Item("ColSurt").ToString
            DGridArticulos.CurrentRow.Cells(iColPrecio).Value = dtDatos.Cos_Art 'dtDatos.Rows.Item(0).Item("Cos_Art").ToString
            DGridArticulos.CurrentRow.Cells(iColMarc).Value = dtDatos.marca 'dtDatos.Rows.Item(0).Item("marca").ToString
            'DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = CType((CType(DGridArticulos.CurrentRow.Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String)
            DGridArticulos.CurrentRow.Cells(iColSubTotal).Value = CType((CType(DGridArticulos.CurrentRow.Cells(iColSol).Value, Decimal)) * (CType(DGridArticulos.CurrentRow.Cells(iColPrecio).Value, Decimal)), String)
            DGridArticulos.CurrentRow.Cells(iColAlm).Value = If(dtDatos.cod_alm, 0)  ' almacen_por_default
            DGridArticulos.CurrentRow.Cells(iColOmitirIVA).Value = dtDatos.OmitirIVA

            CalculaExistencia(fila)


            'dtDatos.Dispose()
            sGetTotal()

        End If
    End Sub

    Private Sub ZctSotGrid1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGridArticulos.KeyDown
        Try
            If e.KeyCode = Keys.F3 Then 'And DGridArticulos.CurrentCell.ColumnIndex = iColArt Then

                If Not (_Permisos.CrearOrdenCompra Or _Permisos.ModificarOrdenCompra) Then
                    Return
                End If

                If (DGridArticulos.CurrentRow.Cells(iColCod).Value = Nothing OrElse DGridArticulos.CurrentRow.Cells(iColCod).Value.ToString = "" OrElse DGridArticulos.CurrentRow.Cells(iColCod).Value = "0") Then


                    'Dim fBusqueda As New ZctBusqueda
                    'fBusqueda.sSPName = "SP_ZctCatArt"
                    'fBusqueda.sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT"
                    'fBusqueda.ShowDialog(Me)

                    Dim fBusqueda = FabricaBuscadores.ObtenerBuscadorArticulos()
                    fBusqueda.SoloInventariables = True
                    fBusqueda.Mostrar()

                    If Not IsNothing(fBusqueda.ArticuloSeleccionado) Then

                        'DGridArticulos.BeginEdit(True)
                        'DGridArticulos.CurrentCell.Value = fBusqueda.ArticuloSeleccionado.Codigo ' .iValor
                        'DGridArticulos.EndEdit(True)
                        'DGridArticulos.UpdateCellValue(iColArt, DGridArticulos.CurrentCell.RowIndex)
                        'ColSurt.Selected = True


                        'ColArt.Selected = True
                        DGridArticulos.BeginEdit(True)
                        DGridArticulos.CurrentRow.Cells(iColArt).Value = fBusqueda.ArticuloSeleccionado.Codigo ' .iValor

                        If (DGridArticulos.CurrentCell.ColumnIndex <> iColArt) Then
                            RecargarArticulo(DGridArticulos.CurrentRow.Index)
                        End If

                        DGridArticulos.EndEdit(True)
                        DGridArticulos.UpdateCellValue(iColArt, DGridArticulos.CurrentRow.Index)
                        ColSurt.Selected = True

                    End If
                Else
                    Throw New SOTException("El elemento ya está almacenado, no puede cambiar el artículo relacionado.")
                End If
            End If
        Catch ex As Exception
            Debug.Print(ex.Message)
            MsgBox(ex.Message.ToString)
        End Try
    End Sub
    'Private Sub LanzarBusquedaArticulos(e As System.Windows.Forms.KeyEventArgs)
    '    Try
    '        If e.KeyCode = Keys.F3 Then
    '            If Not (_Permisos.CrearOrdenCompra Or _Permisos.ModificarOrdenCompra) Then
    '                Return
    '            End If
    '            Dim fBusqueda As New ZctBusqueda
    '            fBusqueda.sSPName = "SP_ZctCatArt"
    '            fBusqueda.sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT"
    '            fBusqueda.ShowDialog(Me)
    '        End If
    '    Catch ex As Exception
    '        Debug.Print(ex.Message)
    '        MsgBox(ex.Message.ToString)
    '    End Try
    'End Sub


    Private Sub CalculaExistencia(ByVal Renglon As Integer)
        If DGridArticulos.Item(iColArt, Renglon).Value = "" OrElse (DGridArticulos.Item(iColAlm, Renglon).Value Is Nothing OrElse DGridArticulos.Item(iColAlm, Renglon).Value.ToString = "") Then DGridArticulos.Item(iColExist, Renglon).Value = "0" : Exit Sub

        ValidaExistencia(DGridArticulos.Rows(Renglon))

        Dim dt As DataTable = Nothing
        Try

            dt = ClassGen.GenGetData("SP_ZctArtXAlm", "@TpConsulta;int|Cod_Art;VarChar|Cod_Alm;Int|Exist_Art;Int|CostoProm_Art;Money", "1|" & DGridArticulos.Item(iColArt, Renglon).Value & "|" & DGridArticulos.Item(iColAlm, Renglon).Value & "|0|0")
            If dt.Rows.Count = 0 Then
                DGridArticulos.Item(iColExist, Renglon).Value = "0"
            Else
                DGridArticulos.Item(iColExist, Renglon).Value = ZctFunciones.GetGeneric(dt.Rows(0)("Exist_Art"))
            End If


        Catch ex As Exception
            Throw ex
        Finally
            dt.Dispose()
        End Try


    End Sub

    Private Sub ValidaExistencia(ByRef Renglon As DataGridViewRow)
        If ZctSOTMain.Inventario_en_marcha(Renglon.Cells(iColAlm).Value) Then
            'Renglon.ErrorText = "El almacén seleccionado esta corriendo inventario y no puede ser utilizado en este momento."
            'Renglon.ReadOnly = True
        End If
    End Sub


    Private Sub CalculaExistencia(ByRef Renglon As DataGridViewRow)
        If Renglon.Cells(iColArt).Value = "" OrElse (Renglon.Cells(iColAlm).Value Is Nothing OrElse Renglon.Cells(iColAlm).Value.ToString = "") Then Renglon.Cells(iColExist).Value = "0" : Exit Sub

        ValidaExistencia(Renglon)
        Dim dt As DataTable = Nothing
        Try

            dt = ClassGen.GenGetData("SP_ZctArtXAlm", "@TpConsulta;int|Cod_Art;VarChar|Cod_Alm;Int|Exist_Art;Int|CostoProm_Art;Money", "1|" & Renglon.Cells(iColArt).Value & "|" & Renglon.Cells(iColAlm).Value & "|0|0")
            If dt.Rows.Count = 0 Then
                Renglon.Cells(iColExist).Value = "0"
            Else
                Renglon.Cells(iColExist).Value = ZctFunciones.GetGeneric(dt.Rows(0)("Exist_Art"))
            End If

        Catch ex As Exception
            Throw ex
        Finally
            dt.Dispose()
        End Try


    End Sub


    Private Sub sGetTotal()
        Dim iRow As Integer
        Dim iSubTotal As Decimal = 0
        Dim iIVA As Decimal = 0
        For iRow = 0 To DGridArticulos.Rows.Count - 1

            If DGridArticulos.Item(Eliminar.Index, iRow).Value = True Then Continue For

            'DGridArticulos.Item(iColSubTotal, iRow).Value = CType((CType(DGridArticulos.Item(iColSurt, iRow).Value, Decimal)) * (CType(DGridArticulos.Item(iColPrecio, iRow).Value, Decimal)), String)
            DGridArticulos.Item(iColSubTotal, iRow).Value = CType((CType(DGridArticulos.Item(iColSol, iRow).Value, Decimal)) * (CType(DGridArticulos.Item(iColPrecio, iRow).Value, Decimal)), String)

            'If DGridArticulos.Item(iColSubTotal, iRow).ToString <> "" Then
            Dim subtotalActual As Decimal = CType(DGridArticulos.Item(iColSubTotal, iRow).Value, Decimal)

            iSubTotal = iSubTotal + subtotalActual

            If CType(DGridArticulos.Item(iColOmitirIVA, iRow).Value, Boolean) = False Then
                iIVA = iIVA + (subtotalActual * parametrosSistema.Iva_CatPar)
            End If
        Next

        lblSubTotal.Text = FormatCurrency(iSubTotal.ToString)
        lblIva.Text = FormatCurrency(iIVA.ToString)
        lblTotal.Text = FormatCurrency(CType(iSubTotal + iIVA, String))
    End Sub

    Private Sub cmdAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAceptar.Click
        Try
            guardaV2()
        Catch ex As Exception
            MessageBox.Show("Ha ocurrido un error al grabar, favor de verificar los datos ingresados: " & System.Environment.NewLine & System.Environment.NewLine & ex.Message)
        End Try

    End Sub



    Public Sub guardaV2()
        If Lstatus.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA Or Lstatus.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO Then
            MsgBox("No puede modificar órdenes de trabajo ya gereradas o canceladas, si desea corregir cancele/cree una OC nueva", MsgBoxStyle.Exclamation, "ATENCIÓN")
            Exit Sub
        ElseIf Lstatus.Text = "CERRADA*" Then
            Lstatus.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA
        ElseIf Lstatus.Text = "CANCELADO*" Then
            Lstatus.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO
        End If


        'Valida la existencia de un proveedor
        If String.IsNullOrWhiteSpace(txtProveedor.Text) OrElse txtProveedor.Text = "0" Then
            MsgBox("El código del proveedor no puede estar vacío, verfique por favor.")
            If txtProveedor.Enabled Then txtProveedor.Focus()
            Exit Sub
        End If

        'Valida la existencia de artículos
        If DGridArticulos.RowCount <= 0 Then
            MsgBox("La orden no puede ser grabada sin artículos, verifique por favor.", MsgBoxStyle.Information, "SOT")
            Exit Sub
        Else
            If DGridArticulos.Rows(0).Cells(iColArt).Value Is DBNull.Value Or DGridArticulos.Rows(0).Cells(iColArt).Value Is Nothing Then
                MsgBox("La orden no puede ser grabada sin artículos, verifique por favor.", MsgBoxStyle.Information, "SOT")
                Exit Sub
            End If
        End If

        Dim encabezado As New Modelo.Almacen.Entidades.Dtos.DtoEncabezadoOC
        encabezado.Cod_OC = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer)
        encabezado.Cod_Prov = CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer)
        encabezado.Fch_OC = Now
        encabezado.FchAplMov = DtAplicacion.Value
        encabezado.Fac_OC = txtFactura.Text
        encabezado.FchFac_OC = DtFechaFact.Value
        encabezado.status = Lstatus.Text

        Dim iRow As Integer
        For iRow = 0 To DGridArticulos.Rows.Count - 1

            If Not (DGridArticulos.Item(iColArt, iRow).Value Is Nothing) AndAlso DGridArticulos.Item(iColArt, iRow).Value <> "" Then

                If IsNothing(DGridArticulos.Item(iColAlm, iRow).Value) OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "" OrElse DGridArticulos.Item(iColAlm, iRow).Value.ToString = "0" Then
                    DGridArticulos.Item(iColAlm, iRow).ErrorText = "El almacén en la orden de compra no puede estar vacío"
                    Throw New ZctReglaNegocioEx("El almacén en la orden de compra no puede estar vacío")
                Else
                    DGridArticulos.Item(iColAlm, iRow).ErrorText = ""
                End If

                Dim detalle As New Modelo.Almacen.Entidades.Dtos.DtoDetalleOC
                detalle.Cod_OC = CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer)
                detalle.CodDet_Oc = CType(IIf(DGridArticulos.Item(iColCod, iRow).Value = Nothing OrElse DGridArticulos.Item(iColCod, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColCod, iRow).Value), Integer)
                detalle.Cod_Art = DGridArticulos.Item(iColArt, iRow).Value
                detalle.Ctd_Art = CType(IIf(DGridArticulos.Item(iColSol, iRow).Value = Nothing OrElse DGridArticulos.Item(iColSol, iRow).Value.ToString = "", 0, DGridArticulos.Item(iColSol, iRow).Value), Decimal)
                detalle.Cos_Art = CType(IIf(DGridArticulos.Item(iColPrecio, iRow).Value.ToString = "", 1, DGridArticulos.Item(iColPrecio, iRow).Value), Decimal)
                detalle.CtdStdDet_OC = CType(IIf(DGridArticulos.Item(iColSurt, iRow).Value.ToString = "", 1, DGridArticulos.Item(iColSurt, iRow).Value), Decimal)
                detalle.FchAplMov = DtAplicacion.Value
                detalle.Cat_Alm = DGridArticulos.Item(iColAlm, iRow).Value.ToString
                detalle.OmitirIVA = DGridArticulos.Item(iColOmitirIVA, iRow).Value
                detalle.Eliminar = CType(IIf(IsNothing(DGridArticulos.Item(Eliminar.Index, iRow).Value) OrElse DGridArticulos.Item(Eliminar.Index, iRow).Value.ToString = "", False, DGridArticulos.Item(Eliminar.Index, iRow).Value), Boolean)

                encabezado.Detalles.Add(detalle)
            End If

        Next

        Dim controladorOC = New SOTControladores.Controladores.ControladorOrdenesCompra()
        controladorOC.EditarOrdenCompra(encabezado, sStatus)

        Dim cClas As New Datos.ClassGen
        cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctOrdCompra", "A", txtFolio.Text)
        'Catch ex As Exception
        '    PkConAlone.Cancela_Transaccion()
        '    PkConAlone.CloseConTran()
        '    MsgBox("Ha ocurrido un error al momento de estar grabando los datos, comuniquese con sistemas." & ex.Message, MsgBoxStyle.Information)
        'End Try

        'Status 
        If sStatus = "A" Then
            ' 2019-12-12 Antonio De Jesús Do´mínguez Cuevas
            ' If MsgBox("¿Desea imprimir la orden de compra?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            'pImprime()
            'End If
        End If

        ' If MsgBox("¿Desea seguir trabajando con este folio?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
        'plimpia(True)
        'sStatus = "A"
        ' Else
        plimpia(False)

            sStatus = "M"
            CargaCompraV2()

            'Status 
            If sStatus = "M" Then
                cmdImprimir.Visible = True
            Else
                cmdImprimir.Visible = False
            End If
        ' End If
    End Sub




    Private Sub txtFolio_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolio.lostfocus

        CargaCompra(txtFolio.Text)
        'Try
        '    If txtFolio.Text <> "" And CType(txtFolio.Text, Integer) > 0 Then
        '        CargaCompraV2()
        '    End If

        '    'Status 
        '    If sStatus = "M" Then
        '        cmdImprimir.Visible = True
        '    Else
        '        cmdImprimir.Visible = False
        '    End If

        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Sub

    Private Sub pSetColor(ByVal Renglon As Integer)

        Dim resultadoConversion As Decimal

        If Not Decimal.TryParse(DGridArticulos.Item(iColSol, Renglon).Value, resultadoConversion) Then
            DGridArticulos.Item(iColSol, Renglon).Value = 0
        End If

        If Not Decimal.TryParse(DGridArticulos.Item(iColSurt, Renglon).Value, resultadoConversion) Then
            DGridArticulos.Item(iColSurt, Renglon).Value = 0
        End If

        If DGridArticulos.Item(iColSol, Renglon).Value Is Nothing Or DGridArticulos.Item(iColSol, Renglon).Value Is DBNull.Value Then DGridArticulos.Item(iColSol, Renglon).Value = 0
        If DGridArticulos.Item(iColSurt, Renglon).Value Is Nothing Or DGridArticulos.Item(iColSurt, Renglon).Value Is DBNull.Value Then DGridArticulos.Item(iColSurt, Renglon).Value = 0
        IIf(DGridArticulos.Item(iColSol, Renglon).Value.ToString = "", 0, DGridArticulos.Item(iColSol, Renglon).Value)
        IIf(DGridArticulos.Item(iColSurt, Renglon).Value.ToString = "", 0, DGridArticulos.Item(iColSurt, Renglon).Value)

        If CType(DGridArticulos.Item(iColSol, Renglon).Value, Integer) > CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblPorSurtir.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblPorSurtir.BackColor
        ElseIf CType(DGridArticulos.Item(iColSol, Renglon).Value, Integer) < CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblDevolucion.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblDevolucion.BackColor
        ElseIf CType(DGridArticulos.Item(iColSol, Renglon).Value, Integer) = CType(DGridArticulos.Item(iColSurt, Renglon).Value, Integer) Then
            DGridArticulos.Item(iColSol, Renglon).Style.BackColor = lblSurtido.BackColor
            DGridArticulos.Item(iColSurt, Renglon).Style.BackColor = lblSurtido.BackColor
        End If
    End Sub

    Public Sub CargaCompraV2()
        'Deshabilita el folio
        'zctFolio.Enabled = False
        txtFolio.Enabled = False

        Dim controladorOrdenesCompra As New SOTControladores.Controladores.ControladorOrdenesCompra()

        Dim drEncOT = controladorOrdenesCompra.ObtenerResumenOrdenCompra(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))
        'controladorOrdenesCompra.SP_ZctEncOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer),
        '                                          Now.Date, DtAplicacion.Value, 0, Date.Now)


        If Not IsNothing(drEncOT) AndAlso (Not drEncOT.Servicios.HasValue OrElse drEncOT.Servicios.Value = False) Then ' (drEncOT.Count > 0) Then
            txtFolio.DataBindings.Clear()
            txtFolio.DataBindings.Add("Text", drEncOT, "Cod_OC")

            txtFactura.DataBindings.Clear()
            txtFactura.DataBindings.Add("Text", drEncOT, "Fac_OC")

            DtAplicacion.DataBindings.Clear()
            DtAplicacion.DataBindings.Add("Value", drEncOT, "FchAplMov")

            DtFechaFact.DataBindings.Clear()
            DtFechaFact.DataBindings.Add("Value", drEncOT, "FchFac_OC")


            txtProveedor.DataBindings.Clear()
            txtProveedor.DataBindings.Add("Text", drEncOT, "Cod_Prov")
            txtProveedor.pCargaDescripcion()

            Lstatus.DataBindings.Clear()
            Lstatus.DataBindings.Add("Text", drEncOT, "status")
            'Datos del Grid

            'Dim drDetOT = controladorOrdenesCompra.SP_ZctDetOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), 0, "0", 0, 0, 0, DtAplicacion.Value, 0, False)

            For Each item In drEncOT.Detalles 'drDetOT
                Dim inRow As New DataGridViewRow

                inRow.CreateCells(DGridArticulos)
                inRow.Cells(iColArt).Value = If(item.Cod_Art, "")
                inRow.Cells(iColDesc).Value = If(item.Desc_Art, "")
                inRow.Cells(iColSol).Value = If(item.Ctd_Art, 0)

                'inRow.Cells(iColExist).Value = drDetOT.Rows(iRowD).Item("Exist_Art").ToString
                inRow.Cells(iColCod).Value = item.CodDet_Oc
                inRow.Cells(iColSurt).Value = If(item.CtdStdDet_OC, 0)
                inRow.Cells(iColPrecio).Value = If(item.Cos_Art, 0)

                'inRow.Cells(iColSubTotal).Value = inRow.Cells(iColSurt).Value * inRow.Cells(iColPrecio).Value
                inRow.Cells(iColSubTotal).Value = inRow.Cells(iColSol).Value * inRow.Cells(iColPrecio).Value
                inRow.Cells(iColAlm).Value = If(item.Cat_Alm, 0)

                inRow.Cells(iColMarc).Value = If(item.marca, "")

                inRow.Cells(iColOmitirIVA).Value = item.OmitirIVA
                inRow.Cells(Eliminar.Index).Value = item.Eliminar

                ' Revisar si no hay implicaciones negativas por dejar editable la columna de almacenes
                inRow.Cells(iColArt).ReadOnly = Not Editar
                inRow.Cells(iColAlm).ReadOnly = Not Editar
                inRow.Cells(iColPrecio).ReadOnly = Not Editar
                inRow.Cells(iColOmitirIVA).ReadOnly = Not Editar
                CalculaExistencia(inRow)

                DGridArticulos.Rows.Add(inRow)
            Next

            For iRowD = 0 To drEncOT.Detalles.Count - 1 ' drDetOT.Count - 1
                pSetColor(iRowD)
            Next

            sGetTotal()
            'Estatus de modificación
            sStatus = "M"

            botonera.Visible = True
        ElseIf IsNothing(drEncOT) Then
            botonera.Visible = True
        Else
            botonera.Visible = False
        End If

        Select Case Lstatus.Text.ToUpper
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA
                CmdCerrar.Enabled = False
                cmdCancelar.Enabled = True
                cmdAceptar.Enabled = False
                Lstatus.BackColor = Color.FromArgb(204, 255, 255)
                txtCapturaArticulos.Enabled = False
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO
                Lstatus.BackColor = Color.FromArgb(243, 159, 24)
                CmdCerrar.Enabled = False
                cmdCancelar.Enabled = False
                cmdAceptar.Enabled = False
                txtCapturaArticulos.Enabled = False
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.APLICADO
                Lstatus.BackColor = Color.PaleGreen
                CmdCerrar.Enabled = Cerrar
                cmdCancelar.Enabled = True
                cmdAceptar.Enabled = Editar
                txtCapturaArticulos.Enabled = True
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA
                Lstatus.BackColor = Color.Gold
                CmdCerrar.Enabled = False
                cmdCancelar.Enabled = False
                cmdAceptar.Enabled = Editar
                txtCapturaArticulos.Enabled = True
        End Select

    End Sub



    Private Sub DGridArticulos_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles DGridArticulos.UserDeletingRow
        If sStatus = "M" AndAlso Not (e.Row.Cells(iColCod).Value = Nothing OrElse e.Row.Cells(iColCod).Value.ToString = "" OrElse e.Row.Cells(iColCod).Value = "0") Then
            MsgBox("Está intentando eliminar un movimiento ya almacenado, por favor marque la casilla de eliminar y el sistema se encargará de eliminarlo.", MsgBoxStyle.Information)
            e.Cancel = True
        End If
    End Sub

    Private Sub DGridArticulos_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles DGridArticulos.DataError
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).ErrorText = e.Exception.Message
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).Selected = False
        DGridArticulos.Item(e.ColumnIndex, e.RowIndex).Value = DGridArticulos.Item(e.ColumnIndex, e.RowIndex).DefaultNewRowValue
    End Sub

    Private Sub cmdImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImprimir.Click
        pImprime()
    End Sub

    Private Sub pImprime()
        'If txtFolio.Text = "0" Or txtFolio.Text = "" Then Exit Sub
        'Dim myCr As New PkVisorRpt
        'myCr.MdiParent = Me.MdiParent
        ''myCr.sDataBase = cboServer.SelectedValue
        'myCr.sSQLV = "{ZctEncOC.Cod_OC} = " & CType(txtFolio.Text, Integer)

        'myCr.sRpt = sPathRpt
        'myCr.Show()

        Dim controladorGlobal = New ControladorConfiguracionGlobal()
        Dim controladorVPoints = New ControladorVPoints()
        Dim controladorClientes = New ControladorClientes()

        Dim config = controladorGlobal.ObtenerConfiguracionGlobal()
        Dim configPunto = controladorVPoints.ObtenerConfiguracion()
        Dim datosFiscales = controladorClientes.ObtenerUltimoCliente()
        Dim documentoReporte = New ReportesSOT.OrdenesCompra.OrdenesCompra()
        documentoReporte.Load()
        'documentoReporte.SetDataSource(New List(Of DtoCabeceraReporte) From {
        '    New DtoCabeceraReporte With {
        '        .Direccion = config.Direccion,
        '        .FechaInicio = dpFechaInicial.SelectedDate.Value,
        '        .FechaFin = dpFechaFinal.SelectedDate.Value,
        '        .Hotel = config.Nombre,
        '        .Usuario = ControladorBase.UsuarioActual.NombreCompleto
        '    }
        '})

        Dim controladorOrdenesCompra As New ControladorOrdenesCompra()

        'Dim drEncOT = controladorOrdenesCompra.SP_ZctEncOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), CType(IIf(txtProveedor.Text = "", 0, txtProveedor.Text), Integer),
        '                                              Now.Date, DtAplicacion.Value, 0, Date.Now)

        'Dim drDetOT = controladorOrdenesCompra.SP_ZctDetOC_CONSULTA(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer), 0, "0", 0, 0, 0, DtAplicacion.Value, 0, False)

        Dim drEncOT = controladorOrdenesCompra.ObtenerResumenOrdenCompra(CType(IIf(txtFolio.Text = "", 0, txtFolio.Text), Integer))

        Select Case drEncOT.status
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA
                drEncOT.Fch_OC = drEncOT.Fch_OC
                Exit Select
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.APLICADO
                drEncOT.Fch_OC = drEncOT.FchAplMov
                Exit Select
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO
                drEncOT.Fch_OC = If(drEncOT.FchAplMov, drEncOT.Fch_OC)
                Exit Select
            Case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA
                Dim controladorCXP = New ControladorCuentasPorPagar()
                drEncOT.Fch_OC = controladorCXP.ObtenerFechaUltimaCuenta(drEncOT.Cod_OC)
                Exit Select
        End Select


        Dim drDetOT = drEncOT.Detalles

        Dim params = New List(Of Modelo.Almacen.Entidades.ZctCatPar) From {
            config
        }

        Dim prms = params.Select(Function(m) New With {m.Direccion, m.id, m.Iva_CatPar, m.maximo_caja, m.MaximoComandasSimultaneas, m.MaximoTaxisSimultaneos, m.minimo_caja, m.Nombre, m.NombreSucursal, m.Operadora, m.PrecioPorDefectoTaxi, .PrecioTarjetasPuntos = If(configPunto?.PrecioTarjetas, 0), m.Propietaria, datosFiscales.RazonSocial, m.Reporteador_web, .RFC = datosFiscales.RFC_Cte, m.ruta_imagenes_articulos, m.RutaActualizaciones, m.RutaActualizacionesJSON, m.RutaReportes, m.SubNombre, m.taller}).ToList()

        Dim encabezados = New List(Of Object) From
            {
            New With
            {
                drEncOT.Cod_OC,
                .Cod_Prov = If(drEncOT.Cod_Prov, 0),
                drEncOT.Fac_OC,
                drEncOT.status,
                drEncOT.NN_Fch_OC,
                drEncOT.NN_FchAplMov,
                drEncOT.NN_FchFac_OC
            }
        }


        'Dim encabezados = drEncOT.Select(Function(m) New With {m.Cod_OC, .Cod_Prov = If(m.Cod_Prov, 0), m.Fac_OC, m.status, m.NN_Fch_OC, m.NN_FchAplMov, m.NN_FchFac_OC}).ToList()
        Dim detalles = drDetOT.Select(Function(m) New With {m.Cod_Art, m.Cod_OC, m.CodDet_Oc, m.Desc_Art, m.marca, m.NN_Cat_Alm, m.NN_Cos_Art, m.NN_Ctd_Art, m.NN_CtdStdDet_OC, m.OmitirIVA}).ToList()

        Dim controladorProveedores = New ControladorProveedores()
        'Dim proveedor = controladorProveedores.ObtenerProveedor(drEncOT.Cod_Prov) '.First().Cod_Prov)

        Dim proveedores = New List(Of Modelo.Almacen.Entidades.Proveedor) From {
            controladorProveedores.ObtenerProveedor(drEncOT.Cod_Prov) '.First().Cod_Prov)
        }

        Dim provs = proveedores.Select(Function(m) New With {m.NN_dias_credito, m.DireccionAutomatica, m.calle, m.ciudad, m.Cod_Prov, m.colonia, m.Contacto, m.cp, m.Dir_Prov, m.email, m.estado, m.Nom_Prov, m.NombreComercial, m.numext, m.numint, m.RFC_Prov, m.telefono}).ToList()

        Dim rutaL = ReportesSOT.Parametros.Logos.RutaLogoGeneral

        If File.Exists(ReportesSOT.Parametros.Logos.RutaLogoGeneral) Then

            rutaL = Path.GetTempFileName() + Guid.NewGuid().ToString()

            While File.Exists(rutaL)
                rutaL = Path.GetTempFileName() + Guid.NewGuid().ToString()
            End While

            File.Copy(ReportesSOT.Parametros.Logos.RutaLogoGeneral, rutaL, True)

        End If

        Dim encabezadosReporte = New List(Of ReportesSOT.Dtos.DtoCabeceraReporte) From {
                New ReportesSOT.Dtos.DtoCabeceraReporte With
                {
                    .Direccion = config.Direccion,
                    .FechaInicio = Date.Now,
                    .FechaFin = Date.Now,
                    .Hotel = config.Nombre,
                    .RazonSocial = datosFiscales.RazonSocial,
                    .Usuario = ControladorBase.UsuarioActual.NombreCompleto,
                    .RutaLogo = rutaL
                }
            }

        'Dim rutas = New List(Of ReportesSOT.Dtos.DtoRutas) From {
        '        New ReportesSOT.Dtos.DtoRutas With
        '        {
        '            .RutaLogo = ReportesSOT.Parametros.Logos.RutaLogoGeneral
        '        }
        '    }

        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_ENCABEZADO_ORDEN_COMPRA).SetDataSource(encabezados)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_DETALLES_ORDEN_COMPRA).SetDataSource(detalles)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_PARAMETROS).SetDataSource(prms)
        'documentoReporte.Database.Tables("ZctCatAlm").SetDataSource(ColAlmacen.DataSource)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_PROVEEDOR).SetDataSource(provs)
        documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_ENCABEZADO_REPORTE).SetDataSource(encabezadosReporte)
        'documentoReporte.Database.Tables(ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_RUTAS).SetDataSource(rutas)

        'documentoReporte.SetParameterValue("rutaLogo", ReportesSOT.Parametros.Logos.RutaLogoGeneral)

        'documentoReporte.Refresh()

        Dim visorR = New VisorReportes()
        visorR.crCrystalReportViewer.ReportSource = documentoReporte
        visorR.Update()

        visorR.ShowDialog(Me)
    End Sub

    Public Sub CargaCompra(ByVal Folio As String)
        Try
            sStatus = "A"
            If IsNumeric(Folio) AndAlso Folio <> "" AndAlso CType(Folio, Integer) > 0 Then
                plimpia(False)
                txtFolio.Enabled = False
                txtFolio.Text = Folio
                CargaCompraV2()
            End If

            'Status 
            If sStatus = "M" Then
                cmdImprimir.Visible = True
            Else
                cmdImprimir.Visible = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cmdIzq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIzq.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) - 1 > 0 Then

            Dim cod = New ControladorOrdenesCompra().ObtenerUltimoFolioMenorQue(CInt(txtFolio.Text), False)
            If Not cod.HasValue Then
                Exit Sub
            End If

            txtFolio.Text = cod.Value '(CInt(txtFolio.Text) - 1).ToString
            plimpia(False)
            CargaCompraV2()
            'Status 
            If sStatus = "M" AndAlso CInt(txtFolio.Text) < OcFolio.Consecutivo Then
                cmdImprimir.Visible = True
            Else
                sStatus = "A"
                cmdImprimir.Visible = False
                DtFechaFact.ClearValue()
                DtAplicacion.ClearValue()
            End If
        End If

    End Sub

    Private Sub cmdDer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDer.Click
        If txtFolio.Text <> "" AndAlso CInt(txtFolio.Text) + 1 <= OcFolio.Consecutivo Then

            Dim cod = New ControladorOrdenesCompra().ObtenerPrimerFolioMayorQue(CInt(txtFolio.Text), False)
            If Not cod.HasValue Then
                Exit Sub
            End If

            txtFolio.Text = cod.Value ' (CInt(txtFolio.Text) + 1).ToString
            plimpia(False)
            Lstatus.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA
            Lstatus.BackColor = Color.Gold

            CargaCompraV2()
            'Status 
            If sStatus = "M" AndAlso CInt(txtFolio.Text) < OcFolio.Consecutivo Then
                cmdImprimir.Visible = True
            Else
                sStatus = "A"
                cmdImprimir.Visible = False
                DtFechaFact.ClearValue()
                DtAplicacion.ClearValue()
            End If
        End If
    End Sub

    Private Sub cmdSurteAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSurteAll.Click
        With DGridArticulos
            'Dim iSubTotal As Decimal = 0
            For i As Integer = 0 To .Rows.Count - 1
                .Rows(i).Cells(iColSurt).Value = .Rows(i).Cells(iColSol).Value

                '.Rows(i).Cells(iColSubTotal).Value = CType((CType(.Rows(i).Cells(iColSurt).Value, Decimal)) * (CType(.Rows(i).Cells(iColPrecio).Value, Decimal)), String)
                .Rows(i).Cells(iColSubTotal).Value = CType((CType(.Rows(i).Cells(iColSol).Value, Decimal)) * (CType(.Rows(i).Cells(iColPrecio).Value, Decimal)), String)
                'iSubTotal = iSubTotal + CType(.Item(iColSubTotal, i).Value, Decimal)

                DGridArticulos.Item(iColSol, i).Style.BackColor = lblSurtido.BackColor
                DGridArticulos.Item(iColSurt, i).Style.BackColor = lblSurtido.BackColor
            Next

            'lblSubTotal.Text = FormatCurrency(iSubTotal.ToString)
            'lblIva.Text = FormatCurrency(CType(iSubTotal * parametrosSistema.Iva_CatPar, String))
            'lblTotal.Text = FormatCurrency(CType(iSubTotal + CDbl(lblIva.Text), String))

        End With

        sGetTotal()
    End Sub

    Private Sub cmdCancelar_Click_1(sender As System.Object, e As System.EventArgs) Handles cmdCancelar.Click
        'Dim login As New ZctLogin
        'login.Cod_Aut = 11
        'login.ShowDialog()
        'If login.Valida Then

        If MessageBox.Show("Esta operación es irreversible ¿desea continuar?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
            Return
        End If

        Try
            Lstatus.Text = "CANCELADO*"
            Lstatus.BackColor = Color.FromArgb(243, 159, 24)

            guardaV2()
        Catch
            plimpia(False)

            sStatus = "M"
            CargaCompraV2()

            'Status 
            If sStatus = "M" Then
                cmdImprimir.Visible = True
            Else
                cmdImprimir.Visible = False
            End If

            Throw
        End Try
        'Else
        '    MsgBox("No está autorizado para realizar esta acción", MsgBoxStyle.Exclamation, "Atención")
        'End If
    End Sub

    Private Sub CmdCerrar_Click(sender As System.Object, e As System.EventArgs) Handles CmdCerrar.Click
        'Dim fLogin As New ZctLogin
        'fLogin.Cod_Aut = 17
        'fLogin.ShowDialog(Me)
        'If Not fLogin.Valida Then
        '    MsgBox("Usted no tiene permisos para cerrar OC.")
        '    Exit Sub
        'End If
        Dim res = MsgBox("Si cierra la OC no podrá hacerle modificaciones ¿desea continuar?", MsgBoxStyle.YesNo, "Pregunta")
        If res = vbNo Then
            Exit Sub
        End If
        Lstatus.Text = "CERRADA*"
        CmdCerrar.Enabled = False
        Lstatus.BackColor = Color.FromArgb(204, 255, 255)
        guardaV2()
    End Sub
    Private Function BuscarEnGrid(Columna As Integer, Valor As String)
        Dim obj As IEnumerable(Of DataGridViewRow) = From row As DataGridViewRow In DGridArticulos.Rows
                                                     Where row.Cells(Columna).Value = Valor
                                                     Select row
        If obj.Any() Then
            DGridArticulos.ClearSelection()
            DGridArticulos.Rows(obj.FirstOrDefault().Index).Selected = True
        End If
        If obj.FirstOrDefault() Is Nothing Then
            Return -1
        End If
        Return obj.FirstOrDefault().Index
    End Function

    Private Sub txtCapturaArticulos_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtCapturaArticulos.KeyDown
        'If e.KeyCode <> Keys.F3 Then
        '    Exit Sub
        'End If

        'fBusqueda = New ZctBusqueda
        'LanzarBusquedaArticulos(e)
        'txtCapturaArticulos.Text = fBusqueda.iValor
        'CapturaArticulo()
        'fBusqueda.Dispose()
        If (e.KeyCode = Keys.Enter) Then

            System.Windows.Forms.SendKeys.Send("{TAB}")
            Exit Sub
        End If
    End Sub
    Private Sub CapturaArticulo()
        Dim RowIndex As Integer
        Try
            If DGridArticulos.RowCount = 0 Or txtCapturaArticulos.Text = "" Then
                Exit Sub
            End If
            txtCapturaArticulos.Text = txtCapturaArticulos.Text.Replace("'", "-")
            Dim DatosArticulo = _ControladorOC.ObtenerArticuloNoNull(txtCapturaArticulos.Text)

            'If _ControladorOC.ValidaArticuloOC(txtFolio.Text, txtCapturaArticulos.Text) = False Then
            RowIndex = BuscarEnGrid(0, DatosArticulo.Cod_Art)
            If RowIndex = -1 Then
                Dim inRow As New DataGridViewRow
                inRow.CreateCells(DGridArticulos)
                With inRow
                    .Cells(iColArt).Value = DatosArticulo.Cod_Art
                    .Cells(iColDesc).Value = DatosArticulo.Desc_Art
                    .Cells(iColExist).Value = 0
                    '.Cells(iColAlm).Value = almacen_por_default
                    .Cells(iColSol).Value = 1
                    .Cells(iColSurt).Value = 1
                    .Cells(iColPrecio).Value = DatosArticulo.Cos_Art
                    .Cells(iColSubTotal).Value = DatosArticulo.Cos_Art
                    .Cells(iColCod).Value = 0
                    .Cells(iColMarc).Value = If(DatosArticulo.ZctCatMar Is Nothing, "", DatosArticulo.ZctCatMar.Desc_Mar)
                End With
                'Me.DGridArticulos.Rows.Add(DatosArticulo.Cod_Art, DatosArticulo.Desc_Art, 0, almacen_por_default, 1, 1, DatosArticulo.Cos_Art, DatosArticulo.Cos_Art, 0)
                Me.DGridArticulos.Rows.Add(inRow)
                CalculaExistencia(inRow)
                RowIndex = DGridArticulos.Rows.Count - 1
                DGridArticulos.ClearSelection()
                DGridArticulos.Rows(RowIndex - 1).Selected = True
            Else

                If (Me.DGridArticulos.Rows(RowIndex).Cells("ColSol").Value <= Me.DGridArticulos.Rows(RowIndex).Cells("ColSurt").Value) Then
                    Me.DGridArticulos.Rows(RowIndex).Cells("ColSol").Value += 1
                End If
                Me.DGridArticulos.Rows(RowIndex).Cells("ColSurt").Value += 1
                'DGridArticulos.Rows(RowIndex).Cells(iColSubTotal).Value = CType((CType(DGridArticulos.Rows(RowIndex).Cells(iColSurt).Value, Decimal)) * (CType(DGridArticulos.Rows(RowIndex).Cells(iColPrecio).Value, Decimal)), String)
                DGridArticulos.Rows(RowIndex).Cells(iColSubTotal).Value = CType((CType(DGridArticulos.Rows(RowIndex).Cells(iColSol).Value, Decimal)) * (CType(DGridArticulos.Rows(RowIndex).Cells(iColPrecio).Value, Decimal)), String)
            End If


            DGridArticulos.FirstDisplayedScrollingRowIndex = RowIndex
            sGetTotal()
            txtCapturaArticulos.Text = ""
            txtCapturaArticulos.Focus()
            Console.Beep()


        Catch ex As Exception
            txtCapturaArticulos.Text = ""
            txtCapturaArticulos.Focus()
            MsgBox("Error buscando artículo " & ex.Message, MsgBoxStyle.Exclamation, "Atención")
        End Try
    End Sub
    Private Sub txtCapturaArticulos_Leave(sender As Object, e As System.EventArgs) Handles txtCapturaArticulos.Leave
        CapturaArticulo()
    End Sub
    Private Sub DGridArticulos_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGridArticulos.CellContentClick
        If e.ColumnIndex = Eliminar.Index Then
            DGridArticulos.BeginEdit(True)
            DGridArticulos.EndEdit()
        End If
    End Sub

    Private Sub txtFolio_Load(sender As System.Object, e As System.EventArgs) Handles txtFolio.Load

    End Sub

    Private Sub txtCapturaArticulos_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtCapturaArticulos.TextChanged

    End Sub

    Private Sub ZctGroupControls3_Enter(sender As Object, e As EventArgs) Handles ZctGroupControls3.Enter

    End Sub

    Private Sub DGridArticulos_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles DGridArticulos.CellBeginEdit
        If iColArt = e.ColumnIndex AndAlso Not (DGridArticulos.Rows(e.RowIndex).Cells(iColCod).Value = Nothing OrElse DGridArticulos.Rows(e.RowIndex).Cells(iColCod).Value.ToString = "" OrElse DGridArticulos.Rows(e.RowIndex).Cells(iColCod).Value = "0") Then
            e.Cancel = True
            Throw New SOTException("El elemento ya está almacenado, no puede cambiar el artículo relacionado.")
        End If
    End Sub
End Class
