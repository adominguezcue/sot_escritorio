
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 10/18/2019 11:12:20
-- Generated from EDMX file: E:\Source\Trabajo\Git\EngraneDigital\sot_global\Datos\Modelo\ModeloSOT.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SOT];
GO
IF SCHEMA_ID(N'SotSchema') IS NULL EXECUTE(N'CREATE SCHEMA [SotSchema]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[SotSchema].[FK_AreaIncidenciaTurno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[IncidenciasTurno] DROP CONSTRAINT [FK_AreaIncidenciaTurno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_AreaPuesto]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Puestos] DROP CONSTRAINT [FK_AreaPuesto];
GO
IF OBJECT_ID(N'[SotSchema].[FK_AreaSolicitudCambioDescanso]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesCambioDescanso] DROP CONSTRAINT [FK_AreaSolicitudCambioDescanso];
GO
IF OBJECT_ID(N'[SotSchema].[FK_AreaSolicitudFalta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesFaltas] DROP CONSTRAINT [FK_AreaSolicitudFalta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_AreaSolicitudPermiso]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesPermisos] DROP CONSTRAINT [FK_AreaSolicitudPermiso];
GO
IF OBJECT_ID(N'[SotSchema].[FK_AreaSolicitudVacaciones]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesVacaciones] DROP CONSTRAINT [FK_AreaSolicitudVacaciones];
GO
IF OBJECT_ID(N'[SotSchema].[FK_BloqueoHabitacionHabitacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[BloqueosHabitaciones] DROP CONSTRAINT [FK_BloqueoHabitacionHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CategoriaCentroCostosCentroCostos]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[CentrosCostos] DROP CONSTRAINT [FK_CategoriaCentroCostosCentroCostos];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CategoriaEmpleadoEmpleado]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Empleados] DROP CONSTRAINT [FK_CategoriaEmpleadoEmpleado];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CentroCostosConceptoGasto]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ConceptosGastos] DROP CONSTRAINT [FK_CentroCostosConceptoGasto];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ComandaArticuloComanda]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ArticulosComandas] DROP CONSTRAINT [FK_ComandaArticuloComanda];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ComandaHistorialComanda]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[HistorialesComanda] DROP CONSTRAINT [FK_ComandaHistorialComanda];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ComandaPagoComanda]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PagosComandas] DROP CONSTRAINT [FK_ComandaPagoComanda];
GO
IF OBJECT_ID(N'[SotSchema].[FK_Comandas_Preventas]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Comandas] DROP CONSTRAINT [FK_Comandas_Preventas];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConceptoGastoArticuloConceptoGasto]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ArticulosConceptosGasto] DROP CONSTRAINT [FK_ConceptoGastoArticuloConceptoGasto];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConceptoGastoGasto]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Gastos] DROP CONSTRAINT [FK_ConceptoGastoGasto];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConceptoMantenimientoMantenimiento]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Mantenimientos] DROP CONSTRAINT [FK_ConceptoMantenimientoMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConceptoSistemaTareaLimpieza]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TareasLimpieza] DROP CONSTRAINT [FK_ConceptoSistemaTareaLimpieza];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionFajillaFajilla]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Fajillas] DROP CONSTRAINT [FK_ConfiguracionFajillaFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionFajillaMonedaExtranjera]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[MonedasExtranjeras] DROP CONSTRAINT [FK_ConfiguracionFajillaMonedaExtranjera];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionFajillaMontoConfiguracionFajilla]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[MontosConfiguracionFajilla] DROP CONSTRAINT [FK_ConfiguracionFajillaMontoConfiguracionFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionPropinasFondoDia]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[FondosDia] DROP CONSTRAINT [FK_ConfiguracionPropinasFondoDia];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionPropinasPorcentajePropinaTipo]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PorcentajesPropinaTipo] DROP CONSTRAINT [FK_ConfiguracionPropinasPorcentajePropinaTipo];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionPropinasPorcentajeTarjeta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PorcentajesTarjeta] DROP CONSTRAINT [FK_ConfiguracionPropinasPorcentajeTarjeta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTipoConfiguracionNegocio]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ConfiguracionesTipos] DROP CONSTRAINT [FK_ConfiguracionTipoConfiguracionNegocio];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTipoRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Rentas] DROP CONSTRAINT [FK_ConfiguracionTipoRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTipoReservacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Reservaciones] DROP CONSTRAINT [FK_ConfiguracionTipoReservacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTipoTiempoHospedaje]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TiemposHospedajes] DROP CONSTRAINT [FK_ConfiguracionTipoTiempoHospedaje];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTipoTipoHabitacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ConfiguracionesTipos] DROP CONSTRAINT [FK_ConfiguracionTipoTipoHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTurnoAsistencia]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Asistencias] DROP CONSTRAINT [FK_ConfiguracionTurnoAsistencia];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTurnoCorteTurno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[CortesTurno] DROP CONSTRAINT [FK_ConfiguracionTurnoCorteTurno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConfiguracionTurnoEmpleado]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Empleados] DROP CONSTRAINT [FK_ConfiguracionTurnoEmpleado];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConsumoInternoArticuloConsumoInterno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ArticulosConsumoInterno] DROP CONSTRAINT [FK_ConsumoInternoArticuloConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConsumoInternoHistorialConsumoInterno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[HistorialesConsumoInterno] DROP CONSTRAINT [FK_ConsumoInternoHistorialConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConsumoInternoPagoConsumoInterno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PagosConsumoInterno] DROP CONSTRAINT [FK_ConsumoInternoPagoConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ConsumosInternos_Preventas]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ConsumosInternos] DROP CONSTRAINT [FK_ConsumosInternos_Preventas];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CorteTurnoComanda]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Comandas] DROP CONSTRAINT [FK_CorteTurnoComanda];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CorteTurnoConsumoInterno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ConsumosInternos] DROP CONSTRAINT [FK_CorteTurnoConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CorteTurnoFajilla]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Fajillas] DROP CONSTRAINT [FK_CorteTurnoFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CorteTurnoGasto]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Gastos] DROP CONSTRAINT [FK_CorteTurnoGasto];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CorteTurnoIncidenciaTurno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[IncidenciasTurno] DROP CONSTRAINT [FK_CorteTurnoIncidenciaTurno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CorteTurnoTareaLimpieza]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TareasLimpieza] DROP CONSTRAINT [FK_CorteTurnoTareaLimpieza];
GO
IF OBJECT_ID(N'[SotSchema].[FK_CorteTurnoVenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Ventas] DROP CONSTRAINT [FK_CorteTurnoVenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_DatosFiscalesOcupacionMesa]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OcupacionesMesa] DROP CONSTRAINT [FK_DatosFiscalesOcupacionMesa];
GO
IF OBJECT_ID(N'[SotSchema].[FK_DatosFiscalesRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Rentas] DROP CONSTRAINT [FK_DatosFiscalesRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_DatosFiscalesReservacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Reservaciones] DROP CONSTRAINT [FK_DatosFiscalesReservacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoAsistencia]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Asistencias] DROP CONSTRAINT [FK_EmpleadoAsistencia];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoComanda]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Comandas] DROP CONSTRAINT [FK_EmpleadoComanda];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoConsumoInterno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ConsumosInternos] DROP CONSTRAINT [FK_EmpleadoConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoConsumoInterno1]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ConsumosInternos] DROP CONSTRAINT [FK_EmpleadoConsumoInterno1];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoIncidencia]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[IncidenciasHabitacion] DROP CONSTRAINT [FK_EmpleadoIncidencia];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoLimpiezaEmpleado]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[LimpiezaEmpleados] DROP CONSTRAINT [FK_EmpleadoLimpiezaEmpleado];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoNomina]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Nominas] DROP CONSTRAINT [FK_EmpleadoNomina];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoObjetoOlvidado]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ObjetosOlvidados] DROP CONSTRAINT [FK_EmpleadoObjetoOlvidado];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoOcupacionMesa]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OcupacionesMesa] DROP CONSTRAINT [FK_EmpleadoOcupacionMesa];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoOrdenTaxi]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OrdenesTaxis] DROP CONSTRAINT [FK_EmpleadoOrdenTaxi];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoPropina]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Propinas] DROP CONSTRAINT [FK_EmpleadoPropina];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoSolicitudCambioDescanso]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesCambioDescanso] DROP CONSTRAINT [FK_EmpleadoSolicitudCambioDescanso];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoSolicitudCambioDescanso1]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesCambioDescanso] DROP CONSTRAINT [FK_EmpleadoSolicitudCambioDescanso1];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoSolicitudFalta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesFaltas] DROP CONSTRAINT [FK_EmpleadoSolicitudFalta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoSolicitudFalta1]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesFaltas] DROP CONSTRAINT [FK_EmpleadoSolicitudFalta1];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoSolicitudPermiso]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesPermisos] DROP CONSTRAINT [FK_EmpleadoSolicitudPermiso];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoSolicitudVacaciones]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[SolicitudesVacaciones] DROP CONSTRAINT [FK_EmpleadoSolicitudVacaciones];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoTareaLimpieza]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TareasLimpieza] DROP CONSTRAINT [FK_EmpleadoTareaLimpieza];
GO
IF OBJECT_ID(N'[SotSchema].[FK_EmpleadoVentaRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[VentasRenta] DROP CONSTRAINT [FK_EmpleadoVentaRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_FajillaMontoFajilla]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[MontosFajilla] DROP CONSTRAINT [FK_FajillaMontoFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[FK_HabitacionHistorialHabitacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[HistorialesHabitacion] DROP CONSTRAINT [FK_HabitacionHistorialHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_HabitacionIncidencia]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[IncidenciasHabitacion] DROP CONSTRAINT [FK_HabitacionIncidencia];
GO
IF OBJECT_ID(N'[SotSchema].[FK_HabitacionObjetoOlvidado]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ObjetosOlvidados] DROP CONSTRAINT [FK_HabitacionObjetoOlvidado];
GO
IF OBJECT_ID(N'[SotSchema].[FK_HabitacionRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Rentas] DROP CONSTRAINT [FK_HabitacionRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_HabitacionReservacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Reservaciones] DROP CONSTRAINT [FK_HabitacionReservacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_HabitacionTareaLimpieza]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TareasLimpieza] DROP CONSTRAINT [FK_HabitacionTareaLimpieza];
GO
IF OBJECT_ID(N'[SotSchema].[FK_HabitacionTareaMantenimiento]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TareasMantenimiento] DROP CONSTRAINT [FK_HabitacionTareaMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[FK_MantenimientoOrdenTrabajoMantenimiento]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OrdenesTrabajoMantenimiento] DROP CONSTRAINT [FK_MantenimientoOrdenTrabajoMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[FK_MesaOcupacionMesa]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OcupacionesMesa] DROP CONSTRAINT [FK_MesaOcupacionMesa];
GO
IF OBJECT_ID(N'[SotSchema].[FK_MonedaExtranjeraMontoConfiguracionFajilla]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[MontosConfiguracionFajilla] DROP CONSTRAINT [FK_MonedaExtranjeraMontoConfiguracionFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[FK_MontoConfiguracionFajillaMontoFajilla]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[MontosFajilla] DROP CONSTRAINT [FK_MontoConfiguracionFajillaMontoFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[FK_OcupacionesMesa_CortesTurno]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OcupacionesMesa] DROP CONSTRAINT [FK_OcupacionesMesa_CortesTurno];
GO
IF OBJECT_ID(N'[SotSchema].[FK_OcupacionesMesa_Preventas]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OcupacionesMesa] DROP CONSTRAINT [FK_OcupacionesMesa_Preventas];
GO
IF OBJECT_ID(N'[SotSchema].[FK_OcupacionMesaOrdenRestaurante]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OrdenesRestaurante] DROP CONSTRAINT [FK_OcupacionMesaOrdenRestaurante];
GO
IF OBJECT_ID(N'[SotSchema].[FK_OcupacionMesaPagoOcupacionMesa]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PagosOcupacionMesa] DROP CONSTRAINT [FK_OcupacionMesaPagoOcupacionMesa];
GO
IF OBJECT_ID(N'[SotSchema].[FK_OrdenesTaxis_Preventas]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[OrdenesTaxis] DROP CONSTRAINT [FK_OrdenesTaxis_Preventas];
GO
IF OBJECT_ID(N'[SotSchema].[FK_OrdenRestauranteArticuloOrdenRestaurante]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ArticulosOrdenesRestaurante] DROP CONSTRAINT [FK_OrdenRestauranteArticuloOrdenRestaurante];
GO
IF OBJECT_ID(N'[SotSchema].[FK_OrdenRestauranteHistorialOrdenRestaurante]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[HistorialesOrdenRestaurante] DROP CONSTRAINT [FK_OrdenRestauranteHistorialOrdenRestaurante];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PaquetePaqueteRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PaquetesRenta] DROP CONSTRAINT [FK_PaquetePaqueteRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PaquetePaqueteReservacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PaquetesReservacion] DROP CONSTRAINT [FK_PaquetePaqueteReservacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PaqueteProgramaPaquete]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ProgramasPaquete] DROP CONSTRAINT [FK_PaqueteProgramaPaquete];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PremioArticuloPremio]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[ArticulosPremio] DROP CONSTRAINT [FK_PremioArticuloPremio];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PuestoEmpleado]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Empleados] DROP CONSTRAINT [FK_PuestoEmpleado];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PuestoPuestosMaestro]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PuestosMaestros] DROP CONSTRAINT [FK_PuestoPuestosMaestro];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PuestoPuestosMaestroS]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PuestosMaestros] DROP CONSTRAINT [FK_PuestoPuestosMaestroS];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PuestoPuestosMaestrosM]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PuestosMaestros] DROP CONSTRAINT [FK_PuestoPuestosMaestrosM];
GO
IF OBJECT_ID(N'[SotSchema].[FK_PuestoPuestosMaestrosV]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PuestosMaestros] DROP CONSTRAINT [FK_PuestoPuestosMaestrosV];
GO
IF OBJECT_ID(N'[SotSchema].[FK_RentaAbonoPuntos]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[AbonosPuntos] DROP CONSTRAINT [FK_RentaAbonoPuntos];
GO
IF OBJECT_ID(N'[SotSchema].[FK_RentaComanda]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Comandas] DROP CONSTRAINT [FK_RentaComanda];
GO
IF OBJECT_ID(N'[SotSchema].[FK_RentaRentaAutomovil]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[AutomovilesRenta] DROP CONSTRAINT [FK_RentaRentaAutomovil];
GO
IF OBJECT_ID(N'[SotSchema].[FK_RentaVentaRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[VentasRenta] DROP CONSTRAINT [FK_RentaVentaRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ReservacionMontoNoReembolsable]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[MontosNoReembolsables] DROP CONSTRAINT [FK_ReservacionMontoNoReembolsable];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ReservacionPagoReservacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PagosReservaciones] DROP CONSTRAINT [FK_ReservacionPagoReservacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_ReservacionPaqueteReservacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PaquetesReservacion] DROP CONSTRAINT [FK_ReservacionPaqueteReservacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TareaLimpiezaLimpiezaEmpleado]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[LimpiezaEmpleados] DROP CONSTRAINT [FK_TareaLimpiezaLimpiezaEmpleado];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TareaMantenimientoConceptoMantenimiento]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TareasMantenimiento] DROP CONSTRAINT [FK_TareaMantenimientoConceptoMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TareaMantenimientoMantenimiento]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Mantenimientos] DROP CONSTRAINT [FK_TareaMantenimientoMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TarjetaPuntosPagoTarjetaPuntos]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PagosTarjetaPuntos] DROP CONSTRAINT [FK_TarjetaPuntosPagoTarjetaPuntos];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TipoHabitacionHabitacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Habitaciones] DROP CONSTRAINT [FK_TipoHabitacionHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TipoHabitacionPaquete]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Paquetes] DROP CONSTRAINT [FK_TipoHabitacionPaquete];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion] DROP CONSTRAINT [FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_TipoHabitacionTiempoLimpieza]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[TiemposLimpieza] DROP CONSTRAINT [FK_TipoHabitacionTiempoLimpieza];
GO
IF OBJECT_ID(N'[SotSchema].[FK_VentaRentaDetallePago]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[DetallesPago] DROP CONSTRAINT [FK_VentaRentaDetallePago];
GO
IF OBJECT_ID(N'[SotSchema].[FK_VentaRentaMontoNoReembolsable]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[MontosNoReembolsablesRenta] DROP CONSTRAINT [FK_VentaRentaMontoNoReembolsable];
GO
IF OBJECT_ID(N'[SotSchema].[FK_VentaRentaPagoRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PagosRenta] DROP CONSTRAINT [FK_VentaRentaPagoRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_VentaRentaPaqueteRenta]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PaquetesRenta] DROP CONSTRAINT [FK_VentaRentaPaqueteRenta];
GO
IF OBJECT_ID(N'[SotSchema].[FK_VentaRentaPersonaExtra]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[PersonasExtra] DROP CONSTRAINT [FK_VentaRentaPersonaExtra];
GO
IF OBJECT_ID(N'[SotSchema].[FK_VentaRentaRenovacion]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[Extensiones] DROP CONSTRAINT [FK_VentaRentaRenovacion];
GO
IF OBJECT_ID(N'[SotSchema].[FK_VentasRenta_Preventas]', 'F') IS NOT NULL
    ALTER TABLE [SotSchema].[VentasRenta] DROP CONSTRAINT [FK_VentasRenta_Preventas];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[SotSchema].[AbonosPuntos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[AbonosPuntos];
GO
IF OBJECT_ID(N'[SotSchema].[Areas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Areas];
GO
IF OBJECT_ID(N'[SotSchema].[ArticulosComandas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ArticulosComandas];
GO
IF OBJECT_ID(N'[SotSchema].[ArticulosConceptosGasto]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ArticulosConceptosGasto];
GO
IF OBJECT_ID(N'[SotSchema].[ArticulosConsumoInterno]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ArticulosConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[ArticulosOrdenesRestaurante]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ArticulosOrdenesRestaurante];
GO
IF OBJECT_ID(N'[SotSchema].[ArticulosPremio]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ArticulosPremio];
GO
IF OBJECT_ID(N'[SotSchema].[Asistencias]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Asistencias];
GO
IF OBJECT_ID(N'[SotSchema].[AutomovilesRenta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[AutomovilesRenta];
GO
IF OBJECT_ID(N'[SotSchema].[BloqueadoresFolios]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[BloqueadoresFolios];
GO
IF OBJECT_ID(N'[SotSchema].[BloqueosHabitaciones]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[BloqueosHabitaciones];
GO
IF OBJECT_ID(N'[SotSchema].[CategoriasCentroCostos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[CategoriasCentroCostos];
GO
IF OBJECT_ID(N'[SotSchema].[CategoriasEmpleados]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[CategoriasEmpleados];
GO
IF OBJECT_ID(N'[SotSchema].[CentrosCostos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[CentrosCostos];
GO
IF OBJECT_ID(N'[SotSchema].[Comandas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Comandas];
GO
IF OBJECT_ID(N'[SotSchema].[ConceptosGastos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConceptosGastos];
GO
IF OBJECT_ID(N'[SotSchema].[ConceptosMantenimiento]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConceptosMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[ConceptosSistema]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConceptosSistema];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesAsistencias]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesAsistencias];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesFajilla]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesImpresoras]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesImpresoras];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesPropinas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesPropinas];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesPuntosLealtad]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesPuntosLealtad];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesTarifas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesTarifas];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesTipos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesTipos];
GO
IF OBJECT_ID(N'[SotSchema].[ConfiguracionesTurno]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConfiguracionesTurno];
GO
IF OBJECT_ID(N'[SotSchema].[ConsumosInternos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConsumosInternos];
GO
IF OBJECT_ID(N'[SotSchema].[ConsumosInternosHabitacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ConsumosInternosHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[CortesTurno]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[CortesTurno];
GO
IF OBJECT_ID(N'[SotSchema].[DatosFiscales]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[DatosFiscales];
GO
IF OBJECT_ID(N'[SotSchema].[DepartamentosMaestros]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[DepartamentosMaestros];
GO
IF OBJECT_ID(N'[SotSchema].[DetallesPago]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[DetallesPago];
GO
IF OBJECT_ID(N'[SotSchema].[Empleados]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Empleados];
GO
IF OBJECT_ID(N'[SotSchema].[ErroresCupon]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ErroresCupon];
GO
IF OBJECT_ID(N'[SotSchema].[Extensiones]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Extensiones];
GO
IF OBJECT_ID(N'[SotSchema].[Fajillas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Fajillas];
GO
IF OBJECT_ID(N'[SotSchema].[FoliosClasificacionVenta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[FoliosClasificacionVenta];
GO
IF OBJECT_ID(N'[SotSchema].[FondosDia]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[FondosDia];
GO
IF OBJECT_ID(N'[SotSchema].[Gastos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Gastos];
GO
IF OBJECT_ID(N'[SotSchema].[Habitaciones]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Habitaciones];
GO
IF OBJECT_ID(N'[SotSchema].[HistorialesComanda]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[HistorialesComanda];
GO
IF OBJECT_ID(N'[SotSchema].[HistorialesConsumoInterno]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[HistorialesConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[HistorialesHabitacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[HistorialesHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[HistorialesOrdenRestaurante]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[HistorialesOrdenRestaurante];
GO
IF OBJECT_ID(N'[SotSchema].[IncidenciasHabitacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[IncidenciasHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[IncidenciasTurno]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[IncidenciasTurno];
GO
IF OBJECT_ID(N'[SotSchema].[LimpiezaEmpleados]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[LimpiezaEmpleados];
GO
IF OBJECT_ID(N'[SotSchema].[Mantenimientos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Mantenimientos];
GO
IF OBJECT_ID(N'[SotSchema].[Mesas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Mesas];
GO
IF OBJECT_ID(N'[SotSchema].[MonedasExtranjeras]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[MonedasExtranjeras];
GO
IF OBJECT_ID(N'[SotSchema].[MontosConfiguracionFajilla]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[MontosConfiguracionFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[MontosFajilla]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[MontosFajilla];
GO
IF OBJECT_ID(N'[SotSchema].[MontosNoReembolsables]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[MontosNoReembolsables];
GO
IF OBJECT_ID(N'[SotSchema].[MontosNoReembolsablesRenta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[MontosNoReembolsablesRenta];
GO
IF OBJECT_ID(N'[SotSchema].[Nominas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Nominas];
GO
IF OBJECT_ID(N'[SotSchema].[ObjetosOlvidados]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ObjetosOlvidados];
GO
IF OBJECT_ID(N'[SotSchema].[OcupacionesMesa]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[OcupacionesMesa];
GO
IF OBJECT_ID(N'[SotSchema].[OrdenesRestaurante]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[OrdenesRestaurante];
GO
IF OBJECT_ID(N'[SotSchema].[OrdenesTaxis]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[OrdenesTaxis];
GO
IF OBJECT_ID(N'[SotSchema].[OrdenesTrabajoMantenimiento]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[OrdenesTrabajoMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[PagosComandas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PagosComandas];
GO
IF OBJECT_ID(N'[SotSchema].[PagosConsumoInterno]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PagosConsumoInterno];
GO
IF OBJECT_ID(N'[SotSchema].[PagosOcupacionMesa]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PagosOcupacionMesa];
GO
IF OBJECT_ID(N'[SotSchema].[PagosRenta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PagosRenta];
GO
IF OBJECT_ID(N'[SotSchema].[PagosReservaciones]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PagosReservaciones];
GO
IF OBJECT_ID(N'[SotSchema].[PagosSeguros]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PagosSeguros];
GO
IF OBJECT_ID(N'[SotSchema].[PagosTarjetaPuntos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PagosTarjetaPuntos];
GO
IF OBJECT_ID(N'[SotSchema].[Paquetes]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Paquetes];
GO
IF OBJECT_ID(N'[SotSchema].[PaquetesRenta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PaquetesRenta];
GO
IF OBJECT_ID(N'[SotSchema].[PaquetesReservacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PaquetesReservacion];
GO
IF OBJECT_ID(N'[SotSchema].[PersonasExtra]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PersonasExtra];
GO
IF OBJECT_ID(N'[SotSchema].[PorcentajesPropinaTipo]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PorcentajesPropinaTipo];
GO
IF OBJECT_ID(N'[SotSchema].[PorcentajesTarjeta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PorcentajesTarjeta];
GO
IF OBJECT_ID(N'[SotSchema].[PreciosTipoHabitacionSincronizacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion];
GO
IF OBJECT_ID(N'[SotSchema].[Premios]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Premios];
GO
IF OBJECT_ID(N'[SotSchema].[Preventas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Preventas];
GO
IF OBJECT_ID(N'[SotSchema].[ProgramasPaquete]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ProgramasPaquete];
GO
IF OBJECT_ID(N'[SotSchema].[Propinas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Propinas];
GO
IF OBJECT_ID(N'[SotSchema].[Puestos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Puestos];
GO
IF OBJECT_ID(N'[SotSchema].[PuestosMaestros]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PuestosMaestros];
GO
IF OBJECT_ID(N'[SotSchema].[ReembolsosPuntos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ReembolsosPuntos];
GO
IF OBJECT_ID(N'[SotSchema].[Rentas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Rentas];
GO
IF OBJECT_ID(N'[SotSchema].[ReportesMatriculas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[ReportesMatriculas];
GO
IF OBJECT_ID(N'[SotSchema].[Reservaciones]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Reservaciones];
GO
IF OBJECT_ID(N'[SotSchema].[SolicitudesCambioDescanso]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[SolicitudesCambioDescanso];
GO
IF OBJECT_ID(N'[SotSchema].[SolicitudesFaltas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[SolicitudesFaltas];
GO
IF OBJECT_ID(N'[SotSchema].[SolicitudesPermisos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[SolicitudesPermisos];
GO
IF OBJECT_ID(N'[SotSchema].[SolicitudesVacaciones]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[SolicitudesVacaciones];
GO
IF OBJECT_ID(N'[SotSchema].[TareasLimpieza]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TareasLimpieza];
GO
IF OBJECT_ID(N'[SotSchema].[TareasMantenimiento]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TareasMantenimiento];
GO
IF OBJECT_ID(N'[SotSchema].[TarjetasPuntos]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TarjetasPuntos];
GO
IF OBJECT_ID(N'[SotSchema].[TiemposHospedajes]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TiemposHospedajes];
GO
IF OBJECT_ID(N'[SotSchema].[TiemposLimpieza]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TiemposLimpieza];
GO
IF OBJECT_ID(N'[SotSchema].[TiposHabitacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TiposHabitacion];
GO
IF OBJECT_ID(N'[SotSchema].[TiposHabitacionSincronizacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TiposHabitacionSincronizacion];
GO
IF OBJECT_ID(N'[SotSchema].[Ventas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Ventas];
GO
IF OBJECT_ID(N'[SotSchema].[VentasRenta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[VentasRenta];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Habitaciones'
CREATE TABLE [SotSchema].[Habitaciones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdTipoHabitacion] int  NOT NULL,
    [IdEstadoHabitacion] int  NOT NULL,
    [NumeroHabitacion] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [Piso] int  NOT NULL,
    [Posicion] int  NOT NULL
);
GO

-- Creating table 'Rentas'
CREATE TABLE [SotSchema].[Rentas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaRegistro] datetime  NOT NULL,
    [FechaSalida] datetime  NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaFin] datetime  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activa] bit  NOT NULL,
    [NumeroServicio] nvarchar(max)  NOT NULL,
    [IdHabitacion] int  NOT NULL,
    [PrecioHabitacion] decimal(19,4)  NOT NULL,
    [IdConfiguracionTipo] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NULL,
    [IdDatosFiscales] int  NULL,
    [MotivoCancelacion] nvarchar(max)  NULL
);
GO

-- Creating table 'AutomovilesRenta'
CREATE TABLE [SotSchema].[AutomovilesRenta] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Matricula] nvarchar(max)  NOT NULL,
    [Modelo] nvarchar(max)  NOT NULL,
    [Marca] nvarchar(max)  NOT NULL,
    [Color] nvarchar(max)  NOT NULL,
    [IdRenta] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL
);
GO

-- Creating table 'ConfiguracionesTarifas'
CREATE TABLE [SotSchema].[ConfiguracionesTarifas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [RangoFijo] bit  NOT NULL,
    [CantidadAutosMaxima] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdTarifa] int  NOT NULL,
    [AdmiteReservaciones] bit  NOT NULL,
    [ExtensionesMaximas] int  NOT NULL
);
GO

-- Creating table 'TiposHabitacion'
CREATE TABLE [SotSchema].[TiposHabitacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Clave] nvarchar(max)  NOT NULL,
    [ClaveSAT] nvarchar(max)  NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [DescripcionExtendida] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [MinutosEntrada] int  NOT NULL,
    [MinutosSucia] int  NOT NULL,
    [MaximoReservaciones] int  NOT NULL,
    [MaximoPersonasExtra] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL
);
GO

-- Creating table 'Reservaciones'
CREATE TABLE [SotSchema].[Reservaciones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CodigoReserva] nvarchar(max)  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [IdCliente] nvarchar(max)  NOT NULL,
    [FechaEntrada] datetime  NOT NULL,
    [FechaSalida] datetime  NOT NULL,
    [Noches] int  NOT NULL,
    [PersonasExtra] int  NOT NULL,
    [IdConfiguracionTipoHabitacion] int  NOT NULL,
    [IdHabitacion] int  NULL,
    [IdDatosFiscales] int  NULL,
    [Paquetes] decimal(19,4)  NOT NULL,
    [Descuento] decimal(19,4)  NOT NULL,
    [LeyendaDescuento] nvarchar(max)  NOT NULL,
    [Observaciones] nvarchar(max)  NOT NULL,
    [IdEstado] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [TotalHabitacion] decimal(19,4)  NOT NULL,
    [TotalPersonasExtra] decimal(19,4)  NOT NULL,
    [TotalHospedajeExtra] decimal(19,4)  NOT NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'Paquetes'
CREATE TABLE [SotSchema].[Paquetes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Clave] nvarchar(max)  NOT NULL,
    [ClaveSAT] nvarchar(max)  NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL,
    [IdTipoHabitacion] int  NOT NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [Descuento] decimal(19,4)  NOT NULL,
    [LeyendaDescuento] nvarchar(max)  NOT NULL,
    [Programado] bit  NOT NULL,
    [CobroUnico] bit  NULL
);
GO

-- Creating table 'PagosRenta'
CREATE TABLE [SotSchema].[PagosRenta] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [IdVentaRenta] int  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [IdTipoPago] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdTipoTarjeta] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'ArticulosComandas'
CREATE TABLE [SotSchema].[ArticulosComandas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL,
    [IdComanda] int  NOT NULL,
    [IdArticulo] nvarchar(max)  NOT NULL,
    [IdLinea] int  NOT NULL,
    [IdDepartamento] int  NOT NULL,
    [PrecioUnidad] decimal(19,4)  NOT NULL,
    [PrecioUnidadFinal] decimal(19,4)  NOT NULL,
    [Cantidad] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [Observaciones] nvarchar(max)  NOT NULL,
    [EsCortesia] bit  NOT NULL
);
GO

-- Creating table 'PaquetesReservacion'
CREATE TABLE [SotSchema].[PaquetesReservacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [Descuento] decimal(19,4)  NOT NULL,
    [IdPaquete] int  NOT NULL,
    [IdReservacion] int  NOT NULL,
    [Cantidad] int  NOT NULL
);
GO

-- Creating table 'TareasMantenimiento'
CREATE TABLE [SotSchema].[TareasMantenimiento] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaSiguiente] datetime  NOT NULL,
    [FechaFin] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activa] bit  NOT NULL,
    [IdHabitacion] int  NOT NULL,
    [IdConceptoMantenimiento] int  NOT NULL,
    [IdTipoMantenimiento] int  NOT NULL,
    [IdPeriodicidad] int  NULL,
    [DiasAnticipacion] int  NOT NULL,
    [Lapso] int  NOT NULL
);
GO

-- Creating table 'Puestos'
CREATE TABLE [SotSchema].[Puestos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL,
    [Asignable] bit  NOT NULL,
    [IdArea] int  NOT NULL,
    [IdRol] int  NOT NULL
);
GO

-- Creating table 'Empleados'
CREATE TABLE [SotSchema].[Empleados] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NumeroEmpleado] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [Habilitado] bit  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [ApellidoPaterno] nvarchar(max)  NOT NULL,
    [ApellidoMaterno] nvarchar(max)  NOT NULL,
    [IdPuesto] int  NOT NULL,
    [FechaIngreso] datetime  NOT NULL,
    [Salario] decimal(19,4)  NOT NULL,
    [NSS] nvarchar(max)  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Celular] nvarchar(max)  NOT NULL,
    [Domicilio] nvarchar(max)  NOT NULL,
    [TieneActa] bit  NOT NULL,
    [TieneCURP] bit  NOT NULL,
    [TieneINE] bit  NOT NULL,
    [TieneComprobanteDomicilio] bit  NOT NULL,
    [TieneComprobanteEstudios] bit  NOT NULL,
    [TieneCartaRecomendacion] bit  NOT NULL,
    [TieneFotografias] bit  NOT NULL,
    [TieneAvisoRetencion] bit  NOT NULL,
    [TieneCartaAntecedentes] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdTurno] int  NOT NULL,
    [IdCategoria] int  NOT NULL
);
GO

-- Creating table 'TareasLimpieza'
CREATE TABLE [SotSchema].[TareasLimpieza] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaInicioLimpieza] datetime  NULL,
    [FechaFinLimpieza] datetime  NULL,
    [FechaInicioSupervision] datetime  NULL,
    [FechaFinSupervision] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activa] bit  NOT NULL,
    [IdHabitacion] int  NOT NULL,
    [IdTipoLimpieza] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [IdEmpleadoSupervisa] int  NULL,
    [IdMotivo] int  NULL,
    [IdCorteTurno] int  NULL,
    [Observaciones] nvarchar(max)  NULL
);
GO

-- Creating table 'PaquetesRenta'
CREATE TABLE [SotSchema].[PaquetesRenta] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdPaquete] int  NOT NULL,
    [IdVentaRenta] int  NOT NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [Descuento] decimal(19,4)  NOT NULL,
    [Cantidad] int  NOT NULL
);
GO

-- Creating table 'LimpiezaEmpleados'
CREATE TABLE [SotSchema].[LimpiezaEmpleados] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [IdTareaLimpieza] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL,
    [TerminoLimpieza] bit  NOT NULL
);
GO

-- Creating table 'BloqueosHabitaciones'
CREATE TABLE [SotSchema].[BloqueosHabitaciones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Motivo] nvarchar(max)  NOT NULL,
    [IdHabitacion] int  NOT NULL
);
GO

-- Creating table 'ConfiguracionesTipos'
CREATE TABLE [SotSchema].[ConfiguracionesTipos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdConfiguracionNegocio] int  NOT NULL,
    [IdTipoHabitacion] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [PrecioHospedajeExtra] decimal(19,4)  NOT NULL,
    [PrecioPersonaExtra] decimal(19,4)  NOT NULL,
    [DuracionOEntrada] int  NOT NULL,
    [HoraSalida] int  NULL
);
GO

-- Creating table 'TiemposHospedajes'
CREATE TABLE [SotSchema].[TiemposHospedajes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Orden] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [Minutos] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdConfiguracionTipo] int  NOT NULL
);
GO

-- Creating table 'Comandas'
CREATE TABLE [SotSchema].[Comandas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Orden] int  NOT NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL,
    [IdCorte] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdRenta] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [IdEmpleadoCobro] int  NULL,
    [MotivoCancelacion] nvarchar(max)  NOT NULL,
    [Observaciones] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaCobro] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Cupon] nvarchar(max)  NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdPreventa] int  NOT NULL
);
GO

-- Creating table 'PagosComandas'
CREATE TABLE [SotSchema].[PagosComandas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [IdComanda] int  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [IdTipoPago] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdTipoTarjeta] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'OrdenesTaxis'
CREATE TABLE [SotSchema].[OrdenesTaxis] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdEstado] int  NOT NULL,
    [IdEmpleadoCobro] int  NULL,
    [Activo] bit  NOT NULL,
    [MotivoCancelacion] nvarchar(max)  NOT NULL,
    [IdPreventa] int  NOT NULL
);
GO

-- Creating table 'TiemposLimpieza'
CREATE TABLE [SotSchema].[TiemposLimpieza] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activa] bit  NOT NULL,
    [IdTipoLimpieza] int  NOT NULL,
    [Minutos] int  NOT NULL,
    [IdTipoHabitacion] int  NOT NULL
);
GO

-- Creating table 'PersonasExtra'
CREATE TABLE [SotSchema].[PersonasExtra] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdVentaRenta] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [EsFueraDeRango] bit  NOT NULL,
    [CobroPorSeparado] bit  NOT NULL,
    [FolioRenovacion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'OrdenesRestaurante'
CREATE TABLE [SotSchema].[OrdenesRestaurante] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL,
    [IdEstado] int  NOT NULL,
    [MotivoCancelacion] nvarchar(max)  NOT NULL,
    [Orden] int  NOT NULL,
    [IdOcupacionMesa] int  NOT NULL,
    [Observaciones] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'ArticulosOrdenesRestaurante'
CREATE TABLE [SotSchema].[ArticulosOrdenesRestaurante] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL,
    [IdOrdenRestaurante] int  NOT NULL,
    [IdArticulo] nvarchar(max)  NOT NULL,
    [IdDepartamento] int  NOT NULL,
    [IdLinea] int  NOT NULL,
    [Cantidad] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [Observaciones] nvarchar(max)  NOT NULL,
    [PrecioUnidad] decimal(19,4)  NOT NULL,
    [PrecioUnidadFinal] decimal(19,4)  NOT NULL,
    [EsCortesia] bit  NOT NULL
);
GO

-- Creating table 'PagosOcupacionMesa'
CREATE TABLE [SotSchema].[PagosOcupacionMesa] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [IdOcupacionMesa] int  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [IdTipoPago] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdTipoTarjeta] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Transaccion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ReportesMatriculas'
CREATE TABLE [SotSchema].[ReportesMatriculas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Matricula] nvarchar(max)  NOT NULL,
    [Detalles] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL
);
GO

-- Creating table 'Mesas'
CREATE TABLE [SotSchema].[Mesas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Clave] nvarchar(max)  NOT NULL,
    [IdEstado] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Fila] int  NOT NULL,
    [Columna] int  NOT NULL
);
GO

-- Creating table 'Propinas'
CREATE TABLE [SotSchema].[Propinas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdTipoTarjeta] int  NOT NULL,
    [IdTipoPropina] int  NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'PorcentajesPropinaTipo'
CREATE TABLE [SotSchema].[PorcentajesPropinaTipo] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdConfiguracionPropina] int  NOT NULL,
    [IdLineaArticulo] int  NOT NULL,
    [Porcentaje] decimal(19,4)  NOT NULL,
    [Activo] bit  NOT NULL
);
GO

-- Creating table 'ConfiguracionesPropinas'
CREATE TABLE [SotSchema].[ConfiguracionesPropinas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'PorcentajesTarjeta'
CREATE TABLE [SotSchema].[PorcentajesTarjeta] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdConfiguracionPropina] int  NOT NULL,
    [IdTipoTarjeta] int  NOT NULL,
    [Porcentaje] decimal(19,4)  NOT NULL,
    [Activo] bit  NOT NULL
);
GO

-- Creating table 'ConceptosSistema'
CREATE TABLE [SotSchema].[ConceptosSistema] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Concepto] nvarchar(max)  NOT NULL,
    [IdTipoConcepto] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'Fajillas'
CREATE TABLE [SotSchema].[Fajillas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Numero] int  NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdConfiguracionFajilla] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaAutorizacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdUsuarioAutorizo] int  NULL,
    [Autorizada] bit  NOT NULL,
    [IdCorteTurno] int  NOT NULL,
    [EsSobrante] bit  NOT NULL,
    [Sincronizada] bit  NOT NULL,
    [ErrorUltimoIntento] nvarchar(max)  NULL,
    [EsErrorSubida] bit  NOT NULL
);
GO

-- Creating table 'MontosFajilla'
CREATE TABLE [SotSchema].[MontosFajilla] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Cantidad] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdFajilla] int  NOT NULL,
    [IdMontoConfiguracionFajilla] int  NOT NULL
);
GO

-- Creating table 'ConfiguracionesFajilla'
CREATE TABLE [SotSchema].[ConfiguracionesFajilla] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'MontosConfiguracionFajilla'
CREATE TABLE [SotSchema].[MontosConfiguracionFajilla] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Monto] decimal(19,4)  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdConfiguracionFajilla] int  NOT NULL,
    [IdMonedaExtranjera] int  NULL
);
GO

-- Creating table 'MonedasExtranjeras'
CREATE TABLE [SotSchema].[MonedasExtranjeras] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Abreviatura] nvarchar(max)  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdConfiguracionFajilla] int  NOT NULL,
    [ValorCambio] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'CortesTurno'
CREATE TABLE [SotSchema].[CortesTurno] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [NumeroCorte] int  NOT NULL,
    [Habitaciones] decimal(19,4)  NOT NULL,
    [PersonasExtra] decimal(19,4)  NOT NULL,
    [HospedajeExtra] decimal(19,4)  NOT NULL,
    [PropinasHabitacion] decimal(19,4)  NOT NULL,
    [Paquetes] decimal(19,4)  NOT NULL,
    [CortesiasHabitacion] decimal(19,4)  NOT NULL,
    [ConsumoHabitacion] decimal(19,4)  NOT NULL,
    [DescuentosHabitacion] decimal(19,4)  NOT NULL,
    [TotalHabitacion] decimal(19,4)  NOT NULL,
    [RoomService] decimal(19,4)  NOT NULL,
    [PropinasRoomService] decimal(19,4)  NOT NULL,
    [CortesiasRoomService] decimal(19,4)  NOT NULL,
    [DescuentosRoomService] decimal(19,4)  NOT NULL,
    [TotalRoomService] decimal(19,4)  NOT NULL,
    [ServicioRestaurante] decimal(19,4)  NOT NULL,
    [PropinasRestaurante] decimal(19,4)  NOT NULL,
    [CortesiasRestautante] decimal(19,4)  NOT NULL,
    [ConsumoRestaurante] decimal(19,4)  NOT NULL,
    [DescuentosRestaurante] decimal(19,4)  NOT NULL,
    [TotalRestaurante] decimal(19,4)  NOT NULL,
    [Taxis] decimal(19,4)  NOT NULL,
    [TarjetasV] decimal(19,4)  NOT NULL,
    [Gastos] decimal(19,4)  NOT NULL,
    [Total] decimal(19,4)  NOT NULL,
    [PagosEfectivo] decimal(19,4)  NOT NULL,
    [PagosTarjeta] decimal(19,4)  NOT NULL,
    [ReservasValidas] decimal(19,4)  NOT NULL,
    [AnticiposReservas] decimal(19,4)  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaCorte] datetime  NULL,
    [FechaOperacion] datetime  NULL,
    [IdUsuarioCerro] int  NULL,
    [IdConfiguracionTurno] int  NOT NULL,
    [Sincronizado] bit  NOT NULL,
    [ErrorUltimoIntento] nvarchar(max)  NULL,
    [EsErrorSubida] bit  NOT NULL,
    [IdEstado] int  NOT NULL,
    [PagoNetoPropinas] decimal(19,4)  NULL,
    [PropinasPagadas] decimal(19,4)  NULL,
    [MontosNoReembolsables] decimal(19,4)  NULL
);
GO

-- Creating table 'DetallesPago'
CREATE TABLE [SotSchema].[DetallesPago] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdVentaRenta] int  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdConceptoPago] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Cantidad] int  NOT NULL
);
GO

-- Creating table 'OcupacionesMesa'
CREATE TABLE [SotSchema].[OcupacionesMesa] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdMesa] int  NOT NULL,
    [IdMesero] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaCobro] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdEstado] int  NOT NULL,
    [IdDatosFiscales] int  NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdPreventa] int  NOT NULL,
    [IdCorte] int  NOT NULL,
    [MotivoCancelacion] nvarchar(max)  NULL
);
GO

-- Creating table 'FondosDia'
CREATE TABLE [SotSchema].[FondosDia] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [Dia] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdConfiguracionPropina] int  NOT NULL
);
GO

-- Creating table 'ConsumosInternos'
CREATE TABLE [SotSchema].[ConsumosInternos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL,
    [IdEmpleadoConsume] int  NOT NULL,
    [IdMesero] int  NULL,
    [IdCorteTurno] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [Motivo] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaEntrega] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Orden] int  NOT NULL,
    [IdPreventa] int  NOT NULL,
    [MotivoCancelacion] nvarchar(max)  NULL
);
GO

-- Creating table 'Gastos'
CREATE TABLE [SotSchema].[Gastos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdConceptoGasto] int  NOT NULL,
    [ConceptoInstantaneo] nvarchar(max)  NOT NULL,
    [IdCorteTurno] int  NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [Activo] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [EsErrorSubida] bit  NOT NULL,
    [ErrorUltimoIntento] nvarchar(max)  NULL,
    [Sincronizado] bit  NOT NULL,
    [IdClasificacion] int  NULL,
    [IdCuentaPago] int  NULL
);
GO

-- Creating table 'DatosFiscales'
CREATE TABLE [SotSchema].[DatosFiscales] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [RFC] nvarchar(max)  NOT NULL,
    [NombreRazonSocial] nvarchar(max)  NOT NULL,
    [Calle] nvarchar(max)  NOT NULL,
    [NumeroExterior] nvarchar(max)  NOT NULL,
    [NumeroInterior] nvarchar(max)  NOT NULL,
    [Colonia] nvarchar(max)  NOT NULL,
    [Ciudad] nvarchar(max)  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL,
    [CP] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [Activo] bit  NOT NULL,
    [IdTipoPersona] int  NOT NULL
);
GO

-- Creating table 'Extensiones'
CREATE TABLE [SotSchema].[Extensiones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdVentaRenta] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [Precio] decimal(19,4)  NOT NULL,
    [CobroPorSeparado] bit  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaFin] datetime  NOT NULL,
    [Folio] nvarchar(max)  NOT NULL,
    [EsRenovacion] bit  NOT NULL,
    [Orden] int  NOT NULL
);
GO

-- Creating table 'TarjetasPuntos'
CREATE TABLE [SotSchema].[TarjetasPuntos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdEstado] int  NOT NULL,
    [NumeroDeTarjeta] nvarchar(max)  NOT NULL,
    [IdCliente] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PagosTarjetaPuntos'
CREATE TABLE [SotSchema].[PagosTarjetaPuntos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [IdTarjetaPuntos] int  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [IdTipoPago] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdTipoTarjeta] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'IncidenciasHabitacion'
CREATE TABLE [SotSchema].[IncidenciasHabitacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [Cancelada] bit  NOT NULL,
    [IdHabitacion] int  NOT NULL,
    [IdEmpleadoReporta] int  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [Zonas] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaCancelacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdUsuarioCancelo] int  NULL
);
GO

-- Creating table 'Ventas'
CREATE TABLE [SotSchema].[Ventas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [Correcta] bit  NOT NULL,
    [Cancelada] bit  NOT NULL,
    [Subida] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaCancelacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdUsuarioCancelo] int  NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL,
    [SumaPagos] decimal(19,4)  NOT NULL,
    [MontoIgnorable] decimal(19,4)  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [FolioTicket] int  NOT NULL,
    [SerieTicket] nvarchar(max)  NOT NULL,
    [IdCorteTurno] int  NOT NULL,
    [IdClasificacionVenta] int  NOT NULL,
    [ErrorUltimoIntento] nvarchar(max)  NULL,
    [EsErrorSubida] bit  NOT NULL,
    [IVAEnCurso] decimal(19,4)  NOT NULL,
    [MotivoCancelacion] nvarchar(max)  NULL
);
GO

-- Creating table 'IncidenciasTurno'
CREATE TABLE [SotSchema].[IncidenciasTurno] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [Cancelada] bit  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [IdArea] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaCancelacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdUsuarioCancelo] int  NULL,
    [IdCorteTurno] int  NOT NULL
);
GO

-- Creating table 'ConfiguracionesTurno'
CREATE TABLE [SotSchema].[ConfiguracionesTurno] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Orden] int  NOT NULL,
    [MinutosInicioAsistencia] int  NOT NULL
);
GO

-- Creating table 'VentasRenta'
CREATE TABLE [SotSchema].[VentasRenta] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdRenta] int  NOT NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL,
    [Activo] bit  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [EsInicial] bit  NOT NULL,
    [IdValet] int  NULL,
    [IdPreventa] int  NULL,
    [Cobrada] bit  NOT NULL,
    [MotivoCancelacion] nvarchar(max)  NULL
);
GO

-- Creating table 'CentrosCostos'
CREATE TABLE [SotSchema].[CentrosCostos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdCategoria] int  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'CategoriasCentroCostos'
CREATE TABLE [SotSchema].[CategoriasCentroCostos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'ConceptosGastos'
CREATE TABLE [SotSchema].[ConceptosGastos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Concepto] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [PagableEnCaja] bit  NOT NULL,
    [IdCentroCostos] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [EnmascaraCompras] bit  NOT NULL
);
GO

-- Creating table 'ConfiguracionesAsistencias'
CREATE TABLE [SotSchema].[ConfiguracionesAsistencias] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Retardo] float  NOT NULL,
    [Falta] float  NOT NULL,
    [DiasDescuento] decimal(19,4)  NOT NULL,
    [DiasDescuentoJustificado] decimal(19,4)  NOT NULL,
    [DiasDobleTurno] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'Asistencias'
CREATE TABLE [SotSchema].[Asistencias] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [FechaEntrada] datetime  NOT NULL,
    [FechaEntradaIdeal] datetime  NOT NULL,
    [FechaSalida] datetime  NULL,
    [FechaSalidaTentativa] datetime  NOT NULL,
    [TieneRetardo] bit  NOT NULL,
    [TieneFalta] bit  NOT NULL,
    [Justificado] bit  NOT NULL,
    [MotivoRetardoFalta] nvarchar(max)  NOT NULL,
    [EsDobleTurno] bit  NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [InicioComida] datetime  NULL,
    [FinComida] datetime  NULL,
    [FechaJustificacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdUsuarioJustifico] int  NULL,
    [IdTurno] int  NOT NULL
);
GO

-- Creating table 'SolicitudesFaltas'
CREATE TABLE [SotSchema].[SolicitudesFaltas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [IdEmpleadoSuplente] int  NULL,
    [IdFormaPago] int  NOT NULL,
    [IdArea] int  NOT NULL,
    [FechaAplicacion] datetime  NOT NULL,
    [Motivo] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'SolicitudesPermisos'
CREATE TABLE [SotSchema].[SolicitudesPermisos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [IdArea] int  NOT NULL,
    [FechaAplicacion] datetime  NOT NULL,
    [Motivo] nvarchar(max)  NOT NULL,
    [EsSalida] bit  NOT NULL,
    [SolicitudAnticipada] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'SolicitudesCambioDescanso'
CREATE TABLE [SotSchema].[SolicitudesCambioDescanso] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [EsDescanso] bit  NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [IdEmpleadoSuplente] int  NULL,
    [IdArea] int  NOT NULL,
    [FechaAplicacion] datetime  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'SolicitudesVacaciones'
CREATE TABLE [SotSchema].[SolicitudesVacaciones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [IdArea] int  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaFin] datetime  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'Premios'
CREATE TABLE [SotSchema].[Premios] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Numero] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'ArticulosPremio'
CREATE TABLE [SotSchema].[ArticulosPremio] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdPremio] int  NOT NULL,
    [CodigoArticulo] nvarchar(max)  NOT NULL,
    [Cantidad] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'ErroresCupon'
CREATE TABLE [SotSchema].[ErroresCupon] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Cupon] nvarchar(max)  NOT NULL,
    [Error] nvarchar(max)  NOT NULL,
    [EsCancelado] bit  NOT NULL
);
GO

-- Creating table 'AbonosPuntos'
CREATE TABLE [SotSchema].[AbonosPuntos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdRenta] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [Abierto] bit  NOT NULL,
    [Abonado] bit  NOT NULL,
    [ErrorControlado] bit  NOT NULL,
    [MotivoError] nvarchar(max)  NULL,
    [Tarjeta] nvarchar(max)  NOT NULL,
    [TicketInicial] nvarchar(max)  NOT NULL,
    [TicketFinal] nvarchar(max)  NOT NULL,
    [ConsumoTotal] decimal(19,4)  NOT NULL,
    [ConsumoHabitacion] decimal(19,4)  NOT NULL,
    [ConsumoPersonasExtra] decimal(19,4)  NOT NULL,
    [ConsumoAlimentos] decimal(19,4)  NOT NULL,
    [ConsumoBebidas] decimal(19,4)  NOT NULL,
    [ConsumoSexAndSpa] decimal(19,4)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'Nominas'
CREATE TABLE [SotSchema].[Nominas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [Salario] decimal(19,4)  NOT NULL,
    [Bonos] decimal(19,4)  NOT NULL,
    [EnVacaciones] bit  NOT NULL,
    [Permisos] int  NOT NULL,
    [Retardos] int  NOT NULL,
    [Faltas] int  NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaFin] datetime  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'DepartamentosMaestros'
CREATE TABLE [SotSchema].[DepartamentosMaestros] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Cocina] int  NOT NULL,
    [Bar] int  NOT NULL,
    [Lavanderia] int  NOT NULL
);
GO

-- Creating table 'CategoriasEmpleados'
CREATE TABLE [SotSchema].[CategoriasEmpleados] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [PorcentajeBonificacion] decimal(19,4)  NOT NULL,
    [Activa] bit  NOT NULL
);
GO

-- Creating table 'Areas'
CREATE TABLE [SotSchema].[Areas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activa] bit  NOT NULL
);
GO

-- Creating table 'ProgramasPaquete'
CREATE TABLE [SotSchema].[ProgramasPaquete] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Dia] int  NOT NULL,
    [Mes] int  NULL,
    [IdPaquete] int  NOT NULL,
    [Activo] bit  NOT NULL
);
GO

-- Creating table 'ObjetosOlvidados'
CREATE TABLE [SotSchema].[ObjetosOlvidados] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [Cancelado] bit  NOT NULL,
    [IdHabitacion] int  NOT NULL,
    [IdEmpleadoReporta] int  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [FechaCancelacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdUsuarioCancelo] int  NULL
);
GO

-- Creating table 'ConfiguracionesImpresoras'
CREATE TABLE [SotSchema].[ConfiguracionesImpresoras] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [ImpresoraTickets] nvarchar(max)  NOT NULL,
    [ImpresoraCocina] nvarchar(max)  NOT NULL,
    [ImpresoraBar] nvarchar(max)  NOT NULL,
    [ImpresoraCortes] nvarchar(max)  NOT NULL,
    [ImpresoraVendedores] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ConceptosMantenimiento'
CREATE TABLE [SotSchema].[ConceptosMantenimiento] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Codigo] nvarchar(max)  NOT NULL,
    [Concepto] nvarchar(max)  NOT NULL,
    [IdClasificacion] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL
);
GO

-- Creating table 'Mantenimientos'
CREATE TABLE [SotSchema].[Mantenimientos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activa] bit  NOT NULL,
    [IdHabitacion] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [IdConceptoMantenimiento] int  NOT NULL,
    [IdTipoMantenimiento] int  NOT NULL,
    [IdEncabezadoOT] int  NULL,
    [IdTareaMantenimiento] int  NOT NULL
);
GO

-- Creating table 'OrdenesTrabajoMantenimiento'
CREATE TABLE [SotSchema].[OrdenesTrabajoMantenimiento] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [IdOrdenTrabajo] int  NOT NULL,
    [IdMantenimiento] int  NOT NULL
);
GO

-- Creating table 'ArticulosConsumoInterno'
CREATE TABLE [SotSchema].[ArticulosConsumoInterno] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Activo] bit  NOT NULL,
    [IdConsumoInterno] int  NOT NULL,
    [IdArticulo] nvarchar(max)  NOT NULL,
    [IdLinea] int  NOT NULL,
    [IdDepartamento] int  NOT NULL,
    [PrecioUnidad] decimal(19,4)  NOT NULL,
    [PrecioUnidadFinal] decimal(19,4)  NOT NULL,
    [Cantidad] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [Observaciones] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PagosConsumoInterno'
CREATE TABLE [SotSchema].[PagosConsumoInterno] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [IdConsumoInterno] int  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [IdTipoPago] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdTipoTarjeta] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Transaccion] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ConsumosInternosHabitacion'
CREATE TABLE [SotSchema].[ConsumosInternosHabitacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdEmpleadoConsume] int  NOT NULL,
    [IdCorteTurno] int  NOT NULL,
    [Motivo] nvarchar(max)  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'VW_ArticulosVendidos'
CREATE TABLE [SotSchema].[VW_ArticulosVendidos] (
    [Id] varchar(61)  NOT NULL,
    [Turno] int  NOT NULL,
    [Fecha] datetime  NOT NULL,
    [Codigo] nvarchar(max)  NOT NULL,
    [Categoria] varchar(50)  NULL,
    [Subcategoria] varchar(50)  NULL,
    [Descripcion] varchar(100)  NULL,
    [Cantidad] int  NOT NULL,
    [PrecioUnidadConIVA] decimal(19,4)  NOT NULL,
    [Total] decimal(30,4)  NOT NULL
);
GO

-- Creating table 'HistorialesHabitacion'
CREATE TABLE [SotSchema].[HistorialesHabitacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdHabitacion] int  NOT NULL,
    [IdTipoHabitacion] int  NOT NULL,
    [IdEstadoHabitacion] int  NOT NULL,
    [NumeroHabitacion] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [FechaInicio] datetime  NOT NULL,
    [FechaFin] datetime  NOT NULL,
    [Piso] int  NOT NULL,
    [Posicion] int  NOT NULL
);
GO

-- Creating table 'HistorialesComanda'
CREATE TABLE [SotSchema].[HistorialesComanda] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdComanda] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [FechaInicio] datetime  NOT NULL
);
GO

-- Creating table 'HistorialesOrdenRestaurante'
CREATE TABLE [SotSchema].[HistorialesOrdenRestaurante] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdOrden] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [FechaInicio] datetime  NOT NULL
);
GO

-- Creating table 'HistorialesConsumoInterno'
CREATE TABLE [SotSchema].[HistorialesConsumoInterno] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdConsumoInterno] int  NOT NULL,
    [IdEstado] int  NOT NULL,
    [FechaInicio] datetime  NOT NULL
);
GO

-- Creating table 'PuestosMaestros'
CREATE TABLE [SotSchema].[PuestosMaestros] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Recamarera] int  NULL,
    [Supervisor] int  NULL,
    [Mesero] int  NULL,
    [Valet] int  NULL,
    [Vendedor] int  NULL
);
GO

-- Creating table 'TiposHabitacionSincronizacion'
CREATE TABLE [SotSchema].[TiposHabitacionSincronizacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Clave] nvarchar(max)  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Fecha] datetime  NOT NULL,
    [PorcentajeIVA] decimal(19,4)  NOT NULL,
    [IdMovimiento] int  NOT NULL,
    [EsErrorSubida] bit  NOT NULL,
    [ErrorUltimoIntento] nvarchar(max)  NULL,
    [Sincronizado] bit  NOT NULL
);
GO

-- Creating table 'PreciosTipoHabitacionSincronizacion'
CREATE TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdTipoSincronizacion] int  NOT NULL,
    [IdTarifa] int  NOT NULL,
    [Precio] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'ArticulosConceptosGasto'
CREATE TABLE [SotSchema].[ArticulosConceptosGasto] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdConceptoGasto] int  NOT NULL,
    [CodigoArticulo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Preventas'
CREATE TABLE [SotSchema].[Preventas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [FolioTicket] int  NOT NULL,
    [IdClasificacionVenta] int  NOT NULL,
    [SerieTicket] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'FoliosClasificacionVenta'
CREATE TABLE [SotSchema].[FoliosClasificacionVenta] (
    [IdClasificacionVenta] int  NOT NULL,
    [Serie] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [FolioActual] int  NOT NULL
);
GO

-- Creating table 'BloqueadoresFolios'
CREATE TABLE [SotSchema].[BloqueadoresFolios] (
    [IdClasificacionVenta] int  NOT NULL,
    [UUID] uniqueidentifier  NOT NULL,
    [Fecha] datetime  NOT NULL
);
GO

-- Creating table 'ReembolsosPuntos'
CREATE TABLE [SotSchema].[ReembolsosPuntos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tarjeta] nvarchar(max)  NOT NULL,
    [MontoReembolsar] decimal(19,4)  NOT NULL,
    [Activo] bit  NOT NULL,
    [Abonado] bit  NOT NULL,
    [ErrorControlado] bit  NOT NULL,
    [MotivoError] nvarchar(max)  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [NumeroServicio] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PagosReservaciones'
CREATE TABLE [SotSchema].[PagosReservaciones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [IdReservacion] int  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [IdTipoPago] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdTipoTarjeta] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'PagosSeguros'
CREATE TABLE [SotSchema].[PagosSeguros] (
    [IdTipoPago] int  NOT NULL
);
GO

-- Creating table 'VW_VentasCorrectas'
CREATE TABLE [SotSchema].[VW_VentasCorrectas] (
    [Id] int  NULL,
    [IdVentaRenta] int  NULL,
    [IdComanda] int  NULL,
    [IdConsumoInterno] int  NULL,
    [IdOcupacionMesa] int  NULL,
    [IdOrdenTaxi] int  NULL,
    [NumeroCorte] int  NOT NULL,
    [FolioTicket] int  NOT NULL,
    [SerieTicket] nvarchar(max)  NOT NULL,
    [Cancelada] bit  NULL,
    [Cobrada] bit  NULL,
    [IdClasificacionVenta] int  NOT NULL,
    [ValorConIVA] decimal(38,4)  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [Pagos] varchar(max)  NULL,
    [MotivoCancelacion] nvarchar(max)  NULL
);
GO

-- Creating table 'MontosNoReembolsables'
CREATE TABLE [SotSchema].[MontosNoReembolsables] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdReservacion] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'MontosNoReembolsablesRenta'
CREATE TABLE [SotSchema].[MontosNoReembolsablesRenta] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdVentaRenta] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'ConfiguracionesPuntosLealtad'
CREATE TABLE [SotSchema].[ConfiguracionesPuntosLealtad] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UrlServicio] nvarchar(max)  NOT NULL,
    [Usuario] nvarchar(max)  NOT NULL,
    [Contrasena] nvarchar(max)  NOT NULL,
    [PrecioTarjetas] decimal(19,4)  NOT NULL
);
GO

-- Creating table 'VW_ConsumoInternoEmpleado'
CREATE TABLE [SotSchema].[VW_ConsumoInternoEmpleado] (
    [NumeroEmpleado] nvarchar(max)  NOT NULL,
    [NombreCompleto] nvarchar(max)  NOT NULL,
    [CantidadConsumos] int  NULL,
    [Total] decimal(38,4)  NULL
);
GO

-- Creating table 'VW_ConsCancelacionesYCortesias'
CREATE TABLE [SotSchema].[VW_ConsCancelacionesYCortesias] (
    [Ticket] nvarchar(max)  NULL,
    [FechaModificacion] datetime  NOT NULL,
    [Habitacion] int  NOT NULL,
    [Concepto] varchar(29)  NOT NULL,
    [MotivoCancelacion] nvarchar(max)  NULL,
    [UsuarioCancelo] nvarchar(max)  NOT NULL,
    [Precio] decimal(38,4)  NOT NULL,
    [idVentaCorrecta] int  NOT NULL,
    [Turno] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'VW_ConceptosCancelacionesYCortesias'
CREATE TABLE [SotSchema].[VW_ConceptosCancelacionesYCortesias] (
    [Concepto] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Habitaciones'
ALTER TABLE [SotSchema].[Habitaciones]
ADD CONSTRAINT [PK_Habitaciones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Rentas'
ALTER TABLE [SotSchema].[Rentas]
ADD CONSTRAINT [PK_Rentas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AutomovilesRenta'
ALTER TABLE [SotSchema].[AutomovilesRenta]
ADD CONSTRAINT [PK_AutomovilesRenta]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesTarifas'
ALTER TABLE [SotSchema].[ConfiguracionesTarifas]
ADD CONSTRAINT [PK_ConfiguracionesTarifas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TiposHabitacion'
ALTER TABLE [SotSchema].[TiposHabitacion]
ADD CONSTRAINT [PK_TiposHabitacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Reservaciones'
ALTER TABLE [SotSchema].[Reservaciones]
ADD CONSTRAINT [PK_Reservaciones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Paquetes'
ALTER TABLE [SotSchema].[Paquetes]
ADD CONSTRAINT [PK_Paquetes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PagosRenta'
ALTER TABLE [SotSchema].[PagosRenta]
ADD CONSTRAINT [PK_PagosRenta]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ArticulosComandas'
ALTER TABLE [SotSchema].[ArticulosComandas]
ADD CONSTRAINT [PK_ArticulosComandas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaquetesReservacion'
ALTER TABLE [SotSchema].[PaquetesReservacion]
ADD CONSTRAINT [PK_PaquetesReservacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TareasMantenimiento'
ALTER TABLE [SotSchema].[TareasMantenimiento]
ADD CONSTRAINT [PK_TareasMantenimiento]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Puestos'
ALTER TABLE [SotSchema].[Puestos]
ADD CONSTRAINT [PK_Puestos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Empleados'
ALTER TABLE [SotSchema].[Empleados]
ADD CONSTRAINT [PK_Empleados]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TareasLimpieza'
ALTER TABLE [SotSchema].[TareasLimpieza]
ADD CONSTRAINT [PK_TareasLimpieza]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PaquetesRenta'
ALTER TABLE [SotSchema].[PaquetesRenta]
ADD CONSTRAINT [PK_PaquetesRenta]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LimpiezaEmpleados'
ALTER TABLE [SotSchema].[LimpiezaEmpleados]
ADD CONSTRAINT [PK_LimpiezaEmpleados]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BloqueosHabitaciones'
ALTER TABLE [SotSchema].[BloqueosHabitaciones]
ADD CONSTRAINT [PK_BloqueosHabitaciones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesTipos'
ALTER TABLE [SotSchema].[ConfiguracionesTipos]
ADD CONSTRAINT [PK_ConfiguracionesTipos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TiemposHospedajes'
ALTER TABLE [SotSchema].[TiemposHospedajes]
ADD CONSTRAINT [PK_TiemposHospedajes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Comandas'
ALTER TABLE [SotSchema].[Comandas]
ADD CONSTRAINT [PK_Comandas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PagosComandas'
ALTER TABLE [SotSchema].[PagosComandas]
ADD CONSTRAINT [PK_PagosComandas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrdenesTaxis'
ALTER TABLE [SotSchema].[OrdenesTaxis]
ADD CONSTRAINT [PK_OrdenesTaxis]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TiemposLimpieza'
ALTER TABLE [SotSchema].[TiemposLimpieza]
ADD CONSTRAINT [PK_TiemposLimpieza]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PersonasExtra'
ALTER TABLE [SotSchema].[PersonasExtra]
ADD CONSTRAINT [PK_PersonasExtra]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrdenesRestaurante'
ALTER TABLE [SotSchema].[OrdenesRestaurante]
ADD CONSTRAINT [PK_OrdenesRestaurante]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ArticulosOrdenesRestaurante'
ALTER TABLE [SotSchema].[ArticulosOrdenesRestaurante]
ADD CONSTRAINT [PK_ArticulosOrdenesRestaurante]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PagosOcupacionMesa'
ALTER TABLE [SotSchema].[PagosOcupacionMesa]
ADD CONSTRAINT [PK_PagosOcupacionMesa]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ReportesMatriculas'
ALTER TABLE [SotSchema].[ReportesMatriculas]
ADD CONSTRAINT [PK_ReportesMatriculas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Mesas'
ALTER TABLE [SotSchema].[Mesas]
ADD CONSTRAINT [PK_Mesas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Propinas'
ALTER TABLE [SotSchema].[Propinas]
ADD CONSTRAINT [PK_Propinas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PorcentajesPropinaTipo'
ALTER TABLE [SotSchema].[PorcentajesPropinaTipo]
ADD CONSTRAINT [PK_PorcentajesPropinaTipo]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesPropinas'
ALTER TABLE [SotSchema].[ConfiguracionesPropinas]
ADD CONSTRAINT [PK_ConfiguracionesPropinas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PorcentajesTarjeta'
ALTER TABLE [SotSchema].[PorcentajesTarjeta]
ADD CONSTRAINT [PK_PorcentajesTarjeta]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConceptosSistema'
ALTER TABLE [SotSchema].[ConceptosSistema]
ADD CONSTRAINT [PK_ConceptosSistema]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Fajillas'
ALTER TABLE [SotSchema].[Fajillas]
ADD CONSTRAINT [PK_Fajillas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MontosFajilla'
ALTER TABLE [SotSchema].[MontosFajilla]
ADD CONSTRAINT [PK_MontosFajilla]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesFajilla'
ALTER TABLE [SotSchema].[ConfiguracionesFajilla]
ADD CONSTRAINT [PK_ConfiguracionesFajilla]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MontosConfiguracionFajilla'
ALTER TABLE [SotSchema].[MontosConfiguracionFajilla]
ADD CONSTRAINT [PK_MontosConfiguracionFajilla]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MonedasExtranjeras'
ALTER TABLE [SotSchema].[MonedasExtranjeras]
ADD CONSTRAINT [PK_MonedasExtranjeras]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CortesTurno'
ALTER TABLE [SotSchema].[CortesTurno]
ADD CONSTRAINT [PK_CortesTurno]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DetallesPago'
ALTER TABLE [SotSchema].[DetallesPago]
ADD CONSTRAINT [PK_DetallesPago]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OcupacionesMesa'
ALTER TABLE [SotSchema].[OcupacionesMesa]
ADD CONSTRAINT [PK_OcupacionesMesa]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FondosDia'
ALTER TABLE [SotSchema].[FondosDia]
ADD CONSTRAINT [PK_FondosDia]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConsumosInternos'
ALTER TABLE [SotSchema].[ConsumosInternos]
ADD CONSTRAINT [PK_ConsumosInternos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Gastos'
ALTER TABLE [SotSchema].[Gastos]
ADD CONSTRAINT [PK_Gastos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DatosFiscales'
ALTER TABLE [SotSchema].[DatosFiscales]
ADD CONSTRAINT [PK_DatosFiscales]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Extensiones'
ALTER TABLE [SotSchema].[Extensiones]
ADD CONSTRAINT [PK_Extensiones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TarjetasPuntos'
ALTER TABLE [SotSchema].[TarjetasPuntos]
ADD CONSTRAINT [PK_TarjetasPuntos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PagosTarjetaPuntos'
ALTER TABLE [SotSchema].[PagosTarjetaPuntos]
ADD CONSTRAINT [PK_PagosTarjetaPuntos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'IncidenciasHabitacion'
ALTER TABLE [SotSchema].[IncidenciasHabitacion]
ADD CONSTRAINT [PK_IncidenciasHabitacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Ventas'
ALTER TABLE [SotSchema].[Ventas]
ADD CONSTRAINT [PK_Ventas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'IncidenciasTurno'
ALTER TABLE [SotSchema].[IncidenciasTurno]
ADD CONSTRAINT [PK_IncidenciasTurno]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesTurno'
ALTER TABLE [SotSchema].[ConfiguracionesTurno]
ADD CONSTRAINT [PK_ConfiguracionesTurno]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VentasRenta'
ALTER TABLE [SotSchema].[VentasRenta]
ADD CONSTRAINT [PK_VentasRenta]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CentrosCostos'
ALTER TABLE [SotSchema].[CentrosCostos]
ADD CONSTRAINT [PK_CentrosCostos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategoriasCentroCostos'
ALTER TABLE [SotSchema].[CategoriasCentroCostos]
ADD CONSTRAINT [PK_CategoriasCentroCostos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConceptosGastos'
ALTER TABLE [SotSchema].[ConceptosGastos]
ADD CONSTRAINT [PK_ConceptosGastos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesAsistencias'
ALTER TABLE [SotSchema].[ConfiguracionesAsistencias]
ADD CONSTRAINT [PK_ConfiguracionesAsistencias]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Asistencias'
ALTER TABLE [SotSchema].[Asistencias]
ADD CONSTRAINT [PK_Asistencias]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SolicitudesFaltas'
ALTER TABLE [SotSchema].[SolicitudesFaltas]
ADD CONSTRAINT [PK_SolicitudesFaltas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SolicitudesPermisos'
ALTER TABLE [SotSchema].[SolicitudesPermisos]
ADD CONSTRAINT [PK_SolicitudesPermisos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SolicitudesCambioDescanso'
ALTER TABLE [SotSchema].[SolicitudesCambioDescanso]
ADD CONSTRAINT [PK_SolicitudesCambioDescanso]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SolicitudesVacaciones'
ALTER TABLE [SotSchema].[SolicitudesVacaciones]
ADD CONSTRAINT [PK_SolicitudesVacaciones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Premios'
ALTER TABLE [SotSchema].[Premios]
ADD CONSTRAINT [PK_Premios]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ArticulosPremio'
ALTER TABLE [SotSchema].[ArticulosPremio]
ADD CONSTRAINT [PK_ArticulosPremio]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ErroresCupon'
ALTER TABLE [SotSchema].[ErroresCupon]
ADD CONSTRAINT [PK_ErroresCupon]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AbonosPuntos'
ALTER TABLE [SotSchema].[AbonosPuntos]
ADD CONSTRAINT [PK_AbonosPuntos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Nominas'
ALTER TABLE [SotSchema].[Nominas]
ADD CONSTRAINT [PK_Nominas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DepartamentosMaestros'
ALTER TABLE [SotSchema].[DepartamentosMaestros]
ADD CONSTRAINT [PK_DepartamentosMaestros]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategoriasEmpleados'
ALTER TABLE [SotSchema].[CategoriasEmpleados]
ADD CONSTRAINT [PK_CategoriasEmpleados]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Areas'
ALTER TABLE [SotSchema].[Areas]
ADD CONSTRAINT [PK_Areas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProgramasPaquete'
ALTER TABLE [SotSchema].[ProgramasPaquete]
ADD CONSTRAINT [PK_ProgramasPaquete]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ObjetosOlvidados'
ALTER TABLE [SotSchema].[ObjetosOlvidados]
ADD CONSTRAINT [PK_ObjetosOlvidados]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesImpresoras'
ALTER TABLE [SotSchema].[ConfiguracionesImpresoras]
ADD CONSTRAINT [PK_ConfiguracionesImpresoras]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConceptosMantenimiento'
ALTER TABLE [SotSchema].[ConceptosMantenimiento]
ADD CONSTRAINT [PK_ConceptosMantenimiento]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Mantenimientos'
ALTER TABLE [SotSchema].[Mantenimientos]
ADD CONSTRAINT [PK_Mantenimientos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrdenesTrabajoMantenimiento'
ALTER TABLE [SotSchema].[OrdenesTrabajoMantenimiento]
ADD CONSTRAINT [PK_OrdenesTrabajoMantenimiento]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ArticulosConsumoInterno'
ALTER TABLE [SotSchema].[ArticulosConsumoInterno]
ADD CONSTRAINT [PK_ArticulosConsumoInterno]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PagosConsumoInterno'
ALTER TABLE [SotSchema].[PagosConsumoInterno]
ADD CONSTRAINT [PK_PagosConsumoInterno]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConsumosInternosHabitacion'
ALTER TABLE [SotSchema].[ConsumosInternosHabitacion]
ADD CONSTRAINT [PK_ConsumosInternosHabitacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'VW_ArticulosVendidos'
ALTER TABLE [SotSchema].[VW_ArticulosVendidos]
ADD CONSTRAINT [PK_VW_ArticulosVendidos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistorialesHabitacion'
ALTER TABLE [SotSchema].[HistorialesHabitacion]
ADD CONSTRAINT [PK_HistorialesHabitacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistorialesComanda'
ALTER TABLE [SotSchema].[HistorialesComanda]
ADD CONSTRAINT [PK_HistorialesComanda]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistorialesOrdenRestaurante'
ALTER TABLE [SotSchema].[HistorialesOrdenRestaurante]
ADD CONSTRAINT [PK_HistorialesOrdenRestaurante]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistorialesConsumoInterno'
ALTER TABLE [SotSchema].[HistorialesConsumoInterno]
ADD CONSTRAINT [PK_HistorialesConsumoInterno]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PuestosMaestros'
ALTER TABLE [SotSchema].[PuestosMaestros]
ADD CONSTRAINT [PK_PuestosMaestros]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TiposHabitacionSincronizacion'
ALTER TABLE [SotSchema].[TiposHabitacionSincronizacion]
ADD CONSTRAINT [PK_TiposHabitacionSincronizacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PreciosTipoHabitacionSincronizacion'
ALTER TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion]
ADD CONSTRAINT [PK_PreciosTipoHabitacionSincronizacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ArticulosConceptosGasto'
ALTER TABLE [SotSchema].[ArticulosConceptosGasto]
ADD CONSTRAINT [PK_ArticulosConceptosGasto]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Preventas'
ALTER TABLE [SotSchema].[Preventas]
ADD CONSTRAINT [PK_Preventas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [IdClasificacionVenta] in table 'FoliosClasificacionVenta'
ALTER TABLE [SotSchema].[FoliosClasificacionVenta]
ADD CONSTRAINT [PK_FoliosClasificacionVenta]
    PRIMARY KEY CLUSTERED ([IdClasificacionVenta] ASC);
GO

-- Creating primary key on [IdClasificacionVenta] in table 'BloqueadoresFolios'
ALTER TABLE [SotSchema].[BloqueadoresFolios]
ADD CONSTRAINT [PK_BloqueadoresFolios]
    PRIMARY KEY CLUSTERED ([IdClasificacionVenta] ASC);
GO

-- Creating primary key on [Id] in table 'ReembolsosPuntos'
ALTER TABLE [SotSchema].[ReembolsosPuntos]
ADD CONSTRAINT [PK_ReembolsosPuntos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PagosReservaciones'
ALTER TABLE [SotSchema].[PagosReservaciones]
ADD CONSTRAINT [PK_PagosReservaciones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [IdTipoPago] in table 'PagosSeguros'
ALTER TABLE [SotSchema].[PagosSeguros]
ADD CONSTRAINT [PK_PagosSeguros]
    PRIMARY KEY CLUSTERED ([IdTipoPago] ASC);
GO

-- Creating primary key on [FolioTicket], [SerieTicket] in table 'VW_VentasCorrectas'
ALTER TABLE [SotSchema].[VW_VentasCorrectas]
ADD CONSTRAINT [PK_VW_VentasCorrectas]
    PRIMARY KEY CLUSTERED ([FolioTicket], [SerieTicket] ASC);
GO

-- Creating primary key on [Id] in table 'MontosNoReembolsables'
ALTER TABLE [SotSchema].[MontosNoReembolsables]
ADD CONSTRAINT [PK_MontosNoReembolsables]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MontosNoReembolsablesRenta'
ALTER TABLE [SotSchema].[MontosNoReembolsablesRenta]
ADD CONSTRAINT [PK_MontosNoReembolsablesRenta]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesPuntosLealtad'
ALTER TABLE [SotSchema].[ConfiguracionesPuntosLealtad]
ADD CONSTRAINT [PK_ConfiguracionesPuntosLealtad]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [NumeroEmpleado], [NombreCompleto] in table 'VW_ConsumoInternoEmpleado'
ALTER TABLE [SotSchema].[VW_ConsumoInternoEmpleado]
ADD CONSTRAINT [PK_VW_ConsumoInternoEmpleado]
    PRIMARY KEY CLUSTERED ([NumeroEmpleado], [NombreCompleto] ASC);
GO

-- Creating primary key on [FechaModificacion], [Habitacion], [Concepto], [UsuarioCancelo], [Precio], [idVentaCorrecta], [Turno] in table 'VW_ConsCancelacionesYCortesias'
ALTER TABLE [SotSchema].[VW_ConsCancelacionesYCortesias]
ADD CONSTRAINT [PK_VW_ConsCancelacionesYCortesias]
    PRIMARY KEY CLUSTERED ([FechaModificacion], [Habitacion], [Concepto], [UsuarioCancelo], [Precio], [idVentaCorrecta], [Turno] ASC);
GO

-- Creating primary key on [Concepto] in table 'VW_ConceptosCancelacionesYCortesias'
ALTER TABLE [SotSchema].[VW_ConceptosCancelacionesYCortesias]
ADD CONSTRAINT [PK_VW_ConceptosCancelacionesYCortesias]
    PRIMARY KEY CLUSTERED ([Concepto] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [IdRenta] in table 'AutomovilesRenta'
ALTER TABLE [SotSchema].[AutomovilesRenta]
ADD CONSTRAINT [FK_RentaRentaAutomovil]
    FOREIGN KEY ([IdRenta])
    REFERENCES [SotSchema].[Rentas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RentaRentaAutomovil'
CREATE INDEX [IX_FK_RentaRentaAutomovil]
ON [SotSchema].[AutomovilesRenta]
    ([IdRenta]);
GO

-- Creating foreign key on [IdTipoHabitacion] in table 'Habitaciones'
ALTER TABLE [SotSchema].[Habitaciones]
ADD CONSTRAINT [FK_TipoHabitacionHabitacion]
    FOREIGN KEY ([IdTipoHabitacion])
    REFERENCES [SotSchema].[TiposHabitacion]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoHabitacionHabitacion'
CREATE INDEX [IX_FK_TipoHabitacionHabitacion]
ON [SotSchema].[Habitaciones]
    ([IdTipoHabitacion]);
GO

-- Creating foreign key on [IdTipoHabitacion] in table 'Paquetes'
ALTER TABLE [SotSchema].[Paquetes]
ADD CONSTRAINT [FK_TipoHabitacionPaquete]
    FOREIGN KEY ([IdTipoHabitacion])
    REFERENCES [SotSchema].[TiposHabitacion]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoHabitacionPaquete'
CREATE INDEX [IX_FK_TipoHabitacionPaquete]
ON [SotSchema].[Paquetes]
    ([IdTipoHabitacion]);
GO

-- Creating foreign key on [IdPaquete] in table 'PaquetesReservacion'
ALTER TABLE [SotSchema].[PaquetesReservacion]
ADD CONSTRAINT [FK_PaquetePaqueteReservacion]
    FOREIGN KEY ([IdPaquete])
    REFERENCES [SotSchema].[Paquetes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaquetePaqueteReservacion'
CREATE INDEX [IX_FK_PaquetePaqueteReservacion]
ON [SotSchema].[PaquetesReservacion]
    ([IdPaquete]);
GO

-- Creating foreign key on [IdReservacion] in table 'PaquetesReservacion'
ALTER TABLE [SotSchema].[PaquetesReservacion]
ADD CONSTRAINT [FK_ReservacionPaqueteReservacion]
    FOREIGN KEY ([IdReservacion])
    REFERENCES [SotSchema].[Reservaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservacionPaqueteReservacion'
CREATE INDEX [IX_FK_ReservacionPaqueteReservacion]
ON [SotSchema].[PaquetesReservacion]
    ([IdReservacion]);
GO

-- Creating foreign key on [IdHabitacion] in table 'TareasMantenimiento'
ALTER TABLE [SotSchema].[TareasMantenimiento]
ADD CONSTRAINT [FK_HabitacionTareaMantenimiento]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HabitacionTareaMantenimiento'
CREATE INDEX [IX_FK_HabitacionTareaMantenimiento]
ON [SotSchema].[TareasMantenimiento]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdPuesto] in table 'Empleados'
ALTER TABLE [SotSchema].[Empleados]
ADD CONSTRAINT [FK_PuestoEmpleado]
    FOREIGN KEY ([IdPuesto])
    REFERENCES [SotSchema].[Puestos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PuestoEmpleado'
CREATE INDEX [IX_FK_PuestoEmpleado]
ON [SotSchema].[Empleados]
    ([IdPuesto]);
GO

-- Creating foreign key on [IdHabitacion] in table 'TareasLimpieza'
ALTER TABLE [SotSchema].[TareasLimpieza]
ADD CONSTRAINT [FK_HabitacionTareaLimpieza]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HabitacionTareaLimpieza'
CREATE INDEX [IX_FK_HabitacionTareaLimpieza]
ON [SotSchema].[TareasLimpieza]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdPaquete] in table 'PaquetesRenta'
ALTER TABLE [SotSchema].[PaquetesRenta]
ADD CONSTRAINT [FK_PaquetePaqueteRenta]
    FOREIGN KEY ([IdPaquete])
    REFERENCES [SotSchema].[Paquetes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaquetePaqueteRenta'
CREATE INDEX [IX_FK_PaquetePaqueteRenta]
ON [SotSchema].[PaquetesRenta]
    ([IdPaquete]);
GO

-- Creating foreign key on [IdTareaLimpieza] in table 'LimpiezaEmpleados'
ALTER TABLE [SotSchema].[LimpiezaEmpleados]
ADD CONSTRAINT [FK_TareaLimpiezaLimpiezaEmpleado]
    FOREIGN KEY ([IdTareaLimpieza])
    REFERENCES [SotSchema].[TareasLimpieza]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TareaLimpiezaLimpiezaEmpleado'
CREATE INDEX [IX_FK_TareaLimpiezaLimpiezaEmpleado]
ON [SotSchema].[LimpiezaEmpleados]
    ([IdTareaLimpieza]);
GO

-- Creating foreign key on [IdEmpleado] in table 'LimpiezaEmpleados'
ALTER TABLE [SotSchema].[LimpiezaEmpleados]
ADD CONSTRAINT [FK_EmpleadoLimpiezaEmpleado]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoLimpiezaEmpleado'
CREATE INDEX [IX_FK_EmpleadoLimpiezaEmpleado]
ON [SotSchema].[LimpiezaEmpleados]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdEmpleadoSupervisa] in table 'TareasLimpieza'
ALTER TABLE [SotSchema].[TareasLimpieza]
ADD CONSTRAINT [FK_EmpleadoTareaLimpieza]
    FOREIGN KEY ([IdEmpleadoSupervisa])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoTareaLimpieza'
CREATE INDEX [IX_FK_EmpleadoTareaLimpieza]
ON [SotSchema].[TareasLimpieza]
    ([IdEmpleadoSupervisa]);
GO

-- Creating foreign key on [IdHabitacion] in table 'BloqueosHabitaciones'
ALTER TABLE [SotSchema].[BloqueosHabitaciones]
ADD CONSTRAINT [FK_BloqueoHabitacionHabitacion]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BloqueoHabitacionHabitacion'
CREATE INDEX [IX_FK_BloqueoHabitacionHabitacion]
ON [SotSchema].[BloqueosHabitaciones]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdTipoHabitacion] in table 'ConfiguracionesTipos'
ALTER TABLE [SotSchema].[ConfiguracionesTipos]
ADD CONSTRAINT [FK_ConfiguracionTipoTipoHabitacion]
    FOREIGN KEY ([IdTipoHabitacion])
    REFERENCES [SotSchema].[TiposHabitacion]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTipoTipoHabitacion'
CREATE INDEX [IX_FK_ConfiguracionTipoTipoHabitacion]
ON [SotSchema].[ConfiguracionesTipos]
    ([IdTipoHabitacion]);
GO

-- Creating foreign key on [IdConfiguracionNegocio] in table 'ConfiguracionesTipos'
ALTER TABLE [SotSchema].[ConfiguracionesTipos]
ADD CONSTRAINT [FK_ConfiguracionTipoConfiguracionNegocio]
    FOREIGN KEY ([IdConfiguracionNegocio])
    REFERENCES [SotSchema].[ConfiguracionesTarifas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTipoConfiguracionNegocio'
CREATE INDEX [IX_FK_ConfiguracionTipoConfiguracionNegocio]
ON [SotSchema].[ConfiguracionesTipos]
    ([IdConfiguracionNegocio]);
GO

-- Creating foreign key on [IdHabitacion] in table 'Rentas'
ALTER TABLE [SotSchema].[Rentas]
ADD CONSTRAINT [FK_HabitacionRenta]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HabitacionRenta'
CREATE INDEX [IX_FK_HabitacionRenta]
ON [SotSchema].[Rentas]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdConfiguracionTipo] in table 'TiemposHospedajes'
ALTER TABLE [SotSchema].[TiemposHospedajes]
ADD CONSTRAINT [FK_ConfiguracionTipoTiempoHospedaje]
    FOREIGN KEY ([IdConfiguracionTipo])
    REFERENCES [SotSchema].[ConfiguracionesTipos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTipoTiempoHospedaje'
CREATE INDEX [IX_FK_ConfiguracionTipoTiempoHospedaje]
ON [SotSchema].[TiemposHospedajes]
    ([IdConfiguracionTipo]);
GO

-- Creating foreign key on [IdComanda] in table 'ArticulosComandas'
ALTER TABLE [SotSchema].[ArticulosComandas]
ADD CONSTRAINT [FK_ComandaArticuloComanda]
    FOREIGN KEY ([IdComanda])
    REFERENCES [SotSchema].[Comandas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ComandaArticuloComanda'
CREATE INDEX [IX_FK_ComandaArticuloComanda]
ON [SotSchema].[ArticulosComandas]
    ([IdComanda]);
GO

-- Creating foreign key on [IdRenta] in table 'Comandas'
ALTER TABLE [SotSchema].[Comandas]
ADD CONSTRAINT [FK_RentaComanda]
    FOREIGN KEY ([IdRenta])
    REFERENCES [SotSchema].[Rentas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RentaComanda'
CREATE INDEX [IX_FK_RentaComanda]
ON [SotSchema].[Comandas]
    ([IdRenta]);
GO

-- Creating foreign key on [IdComanda] in table 'PagosComandas'
ALTER TABLE [SotSchema].[PagosComandas]
ADD CONSTRAINT [FK_ComandaPagoComanda]
    FOREIGN KEY ([IdComanda])
    REFERENCES [SotSchema].[Comandas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ComandaPagoComanda'
CREATE INDEX [IX_FK_ComandaPagoComanda]
ON [SotSchema].[PagosComandas]
    ([IdComanda]);
GO

-- Creating foreign key on [IdEmpleadoCobro] in table 'Comandas'
ALTER TABLE [SotSchema].[Comandas]
ADD CONSTRAINT [FK_EmpleadoComanda]
    FOREIGN KEY ([IdEmpleadoCobro])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoComanda'
CREATE INDEX [IX_FK_EmpleadoComanda]
ON [SotSchema].[Comandas]
    ([IdEmpleadoCobro]);
GO

-- Creating foreign key on [IdEmpleadoCobro] in table 'OrdenesTaxis'
ALTER TABLE [SotSchema].[OrdenesTaxis]
ADD CONSTRAINT [FK_EmpleadoOrdenTaxi]
    FOREIGN KEY ([IdEmpleadoCobro])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoOrdenTaxi'
CREATE INDEX [IX_FK_EmpleadoOrdenTaxi]
ON [SotSchema].[OrdenesTaxis]
    ([IdEmpleadoCobro]);
GO

-- Creating foreign key on [IdTipoHabitacion] in table 'TiemposLimpieza'
ALTER TABLE [SotSchema].[TiemposLimpieza]
ADD CONSTRAINT [FK_TipoHabitacionTiempoLimpieza]
    FOREIGN KEY ([IdTipoHabitacion])
    REFERENCES [SotSchema].[TiposHabitacion]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoHabitacionTiempoLimpieza'
CREATE INDEX [IX_FK_TipoHabitacionTiempoLimpieza]
ON [SotSchema].[TiemposLimpieza]
    ([IdTipoHabitacion]);
GO

-- Creating foreign key on [IdConfiguracionTipo] in table 'Rentas'
ALTER TABLE [SotSchema].[Rentas]
ADD CONSTRAINT [FK_ConfiguracionTipoRenta]
    FOREIGN KEY ([IdConfiguracionTipo])
    REFERENCES [SotSchema].[ConfiguracionesTipos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTipoRenta'
CREATE INDEX [IX_FK_ConfiguracionTipoRenta]
ON [SotSchema].[Rentas]
    ([IdConfiguracionTipo]);
GO

-- Creating foreign key on [IdOrdenRestaurante] in table 'ArticulosOrdenesRestaurante'
ALTER TABLE [SotSchema].[ArticulosOrdenesRestaurante]
ADD CONSTRAINT [FK_OrdenRestauranteArticuloOrdenRestaurante]
    FOREIGN KEY ([IdOrdenRestaurante])
    REFERENCES [SotSchema].[OrdenesRestaurante]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrdenRestauranteArticuloOrdenRestaurante'
CREATE INDEX [IX_FK_OrdenRestauranteArticuloOrdenRestaurante]
ON [SotSchema].[ArticulosOrdenesRestaurante]
    ([IdOrdenRestaurante]);
GO

-- Creating foreign key on [IdEmpleado] in table 'Propinas'
ALTER TABLE [SotSchema].[Propinas]
ADD CONSTRAINT [FK_EmpleadoPropina]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoPropina'
CREATE INDEX [IX_FK_EmpleadoPropina]
ON [SotSchema].[Propinas]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdConfiguracionPropina] in table 'PorcentajesPropinaTipo'
ALTER TABLE [SotSchema].[PorcentajesPropinaTipo]
ADD CONSTRAINT [FK_ConfiguracionPropinasPorcentajePropinaTipo]
    FOREIGN KEY ([IdConfiguracionPropina])
    REFERENCES [SotSchema].[ConfiguracionesPropinas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionPropinasPorcentajePropinaTipo'
CREATE INDEX [IX_FK_ConfiguracionPropinasPorcentajePropinaTipo]
ON [SotSchema].[PorcentajesPropinaTipo]
    ([IdConfiguracionPropina]);
GO

-- Creating foreign key on [IdConfiguracionPropina] in table 'PorcentajesTarjeta'
ALTER TABLE [SotSchema].[PorcentajesTarjeta]
ADD CONSTRAINT [FK_ConfiguracionPropinasPorcentajeTarjeta]
    FOREIGN KEY ([IdConfiguracionPropina])
    REFERENCES [SotSchema].[ConfiguracionesPropinas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionPropinasPorcentajeTarjeta'
CREATE INDEX [IX_FK_ConfiguracionPropinasPorcentajeTarjeta]
ON [SotSchema].[PorcentajesTarjeta]
    ([IdConfiguracionPropina]);
GO

-- Creating foreign key on [IdFajilla] in table 'MontosFajilla'
ALTER TABLE [SotSchema].[MontosFajilla]
ADD CONSTRAINT [FK_FajillaMontoFajilla]
    FOREIGN KEY ([IdFajilla])
    REFERENCES [SotSchema].[Fajillas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FajillaMontoFajilla'
CREATE INDEX [IX_FK_FajillaMontoFajilla]
ON [SotSchema].[MontosFajilla]
    ([IdFajilla]);
GO

-- Creating foreign key on [IdConfiguracionFajilla] in table 'Fajillas'
ALTER TABLE [SotSchema].[Fajillas]
ADD CONSTRAINT [FK_ConfiguracionFajillaFajilla]
    FOREIGN KEY ([IdConfiguracionFajilla])
    REFERENCES [SotSchema].[ConfiguracionesFajilla]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionFajillaFajilla'
CREATE INDEX [IX_FK_ConfiguracionFajillaFajilla]
ON [SotSchema].[Fajillas]
    ([IdConfiguracionFajilla]);
GO

-- Creating foreign key on [IdConfiguracionFajilla] in table 'MontosConfiguracionFajilla'
ALTER TABLE [SotSchema].[MontosConfiguracionFajilla]
ADD CONSTRAINT [FK_ConfiguracionFajillaMontoConfiguracionFajilla]
    FOREIGN KEY ([IdConfiguracionFajilla])
    REFERENCES [SotSchema].[ConfiguracionesFajilla]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionFajillaMontoConfiguracionFajilla'
CREATE INDEX [IX_FK_ConfiguracionFajillaMontoConfiguracionFajilla]
ON [SotSchema].[MontosConfiguracionFajilla]
    ([IdConfiguracionFajilla]);
GO

-- Creating foreign key on [IdMontoConfiguracionFajilla] in table 'MontosFajilla'
ALTER TABLE [SotSchema].[MontosFajilla]
ADD CONSTRAINT [FK_MontoConfiguracionFajillaMontoFajilla]
    FOREIGN KEY ([IdMontoConfiguracionFajilla])
    REFERENCES [SotSchema].[MontosConfiguracionFajilla]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MontoConfiguracionFajillaMontoFajilla'
CREATE INDEX [IX_FK_MontoConfiguracionFajillaMontoFajilla]
ON [SotSchema].[MontosFajilla]
    ([IdMontoConfiguracionFajilla]);
GO

-- Creating foreign key on [IdMonedaExtranjera] in table 'MontosConfiguracionFajilla'
ALTER TABLE [SotSchema].[MontosConfiguracionFajilla]
ADD CONSTRAINT [FK_MonedaExtranjeraMontoConfiguracionFajilla]
    FOREIGN KEY ([IdMonedaExtranjera])
    REFERENCES [SotSchema].[MonedasExtranjeras]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MonedaExtranjeraMontoConfiguracionFajilla'
CREATE INDEX [IX_FK_MonedaExtranjeraMontoConfiguracionFajilla]
ON [SotSchema].[MontosConfiguracionFajilla]
    ([IdMonedaExtranjera]);
GO

-- Creating foreign key on [IdConfiguracionFajilla] in table 'MonedasExtranjeras'
ALTER TABLE [SotSchema].[MonedasExtranjeras]
ADD CONSTRAINT [FK_ConfiguracionFajillaMonedaExtranjera]
    FOREIGN KEY ([IdConfiguracionFajilla])
    REFERENCES [SotSchema].[ConfiguracionesFajilla]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionFajillaMonedaExtranjera'
CREATE INDEX [IX_FK_ConfiguracionFajillaMonedaExtranjera]
ON [SotSchema].[MonedasExtranjeras]
    ([IdConfiguracionFajilla]);
GO

-- Creating foreign key on [IdMesa] in table 'OcupacionesMesa'
ALTER TABLE [SotSchema].[OcupacionesMesa]
ADD CONSTRAINT [FK_MesaOcupacionMesa]
    FOREIGN KEY ([IdMesa])
    REFERENCES [SotSchema].[Mesas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MesaOcupacionMesa'
CREATE INDEX [IX_FK_MesaOcupacionMesa]
ON [SotSchema].[OcupacionesMesa]
    ([IdMesa]);
GO

-- Creating foreign key on [IdOcupacionMesa] in table 'OrdenesRestaurante'
ALTER TABLE [SotSchema].[OrdenesRestaurante]
ADD CONSTRAINT [FK_OcupacionMesaOrdenRestaurante]
    FOREIGN KEY ([IdOcupacionMesa])
    REFERENCES [SotSchema].[OcupacionesMesa]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OcupacionMesaOrdenRestaurante'
CREATE INDEX [IX_FK_OcupacionMesaOrdenRestaurante]
ON [SotSchema].[OrdenesRestaurante]
    ([IdOcupacionMesa]);
GO

-- Creating foreign key on [IdOcupacionMesa] in table 'PagosOcupacionMesa'
ALTER TABLE [SotSchema].[PagosOcupacionMesa]
ADD CONSTRAINT [FK_OcupacionMesaPagoOcupacionMesa]
    FOREIGN KEY ([IdOcupacionMesa])
    REFERENCES [SotSchema].[OcupacionesMesa]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OcupacionMesaPagoOcupacionMesa'
CREATE INDEX [IX_FK_OcupacionMesaPagoOcupacionMesa]
ON [SotSchema].[PagosOcupacionMesa]
    ([IdOcupacionMesa]);
GO

-- Creating foreign key on [IdMesero] in table 'OcupacionesMesa'
ALTER TABLE [SotSchema].[OcupacionesMesa]
ADD CONSTRAINT [FK_EmpleadoOcupacionMesa]
    FOREIGN KEY ([IdMesero])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoOcupacionMesa'
CREATE INDEX [IX_FK_EmpleadoOcupacionMesa]
ON [SotSchema].[OcupacionesMesa]
    ([IdMesero]);
GO

-- Creating foreign key on [IdConfiguracionPropina] in table 'FondosDia'
ALTER TABLE [SotSchema].[FondosDia]
ADD CONSTRAINT [FK_ConfiguracionPropinasFondoDia]
    FOREIGN KEY ([IdConfiguracionPropina])
    REFERENCES [SotSchema].[ConfiguracionesPropinas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionPropinasFondoDia'
CREATE INDEX [IX_FK_ConfiguracionPropinasFondoDia]
ON [SotSchema].[FondosDia]
    ([IdConfiguracionPropina]);
GO

-- Creating foreign key on [IdCorteTurno] in table 'Fajillas'
ALTER TABLE [SotSchema].[Fajillas]
ADD CONSTRAINT [FK_CorteTurnoFajilla]
    FOREIGN KEY ([IdCorteTurno])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CorteTurnoFajilla'
CREATE INDEX [IX_FK_CorteTurnoFajilla]
ON [SotSchema].[Fajillas]
    ([IdCorteTurno]);
GO

-- Creating foreign key on [IdCorteTurno] in table 'Gastos'
ALTER TABLE [SotSchema].[Gastos]
ADD CONSTRAINT [FK_CorteTurnoGasto]
    FOREIGN KEY ([IdCorteTurno])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CorteTurnoGasto'
CREATE INDEX [IX_FK_CorteTurnoGasto]
ON [SotSchema].[Gastos]
    ([IdCorteTurno]);
GO

-- Creating foreign key on [IdConfiguracionTipoHabitacion] in table 'Reservaciones'
ALTER TABLE [SotSchema].[Reservaciones]
ADD CONSTRAINT [FK_ConfiguracionTipoReservacion]
    FOREIGN KEY ([IdConfiguracionTipoHabitacion])
    REFERENCES [SotSchema].[ConfiguracionesTipos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTipoReservacion'
CREATE INDEX [IX_FK_ConfiguracionTipoReservacion]
ON [SotSchema].[Reservaciones]
    ([IdConfiguracionTipoHabitacion]);
GO

-- Creating foreign key on [IdHabitacion] in table 'Reservaciones'
ALTER TABLE [SotSchema].[Reservaciones]
ADD CONSTRAINT [FK_HabitacionReservacion]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HabitacionReservacion'
CREATE INDEX [IX_FK_HabitacionReservacion]
ON [SotSchema].[Reservaciones]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdTarjetaPuntos] in table 'PagosTarjetaPuntos'
ALTER TABLE [SotSchema].[PagosTarjetaPuntos]
ADD CONSTRAINT [FK_TarjetaPuntosPagoTarjetaPuntos]
    FOREIGN KEY ([IdTarjetaPuntos])
    REFERENCES [SotSchema].[TarjetasPuntos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TarjetaPuntosPagoTarjetaPuntos'
CREATE INDEX [IX_FK_TarjetaPuntosPagoTarjetaPuntos]
ON [SotSchema].[PagosTarjetaPuntos]
    ([IdTarjetaPuntos]);
GO

-- Creating foreign key on [IdEmpleadoReporta] in table 'IncidenciasHabitacion'
ALTER TABLE [SotSchema].[IncidenciasHabitacion]
ADD CONSTRAINT [FK_EmpleadoIncidencia]
    FOREIGN KEY ([IdEmpleadoReporta])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoIncidencia'
CREATE INDEX [IX_FK_EmpleadoIncidencia]
ON [SotSchema].[IncidenciasHabitacion]
    ([IdEmpleadoReporta]);
GO

-- Creating foreign key on [IdHabitacion] in table 'IncidenciasHabitacion'
ALTER TABLE [SotSchema].[IncidenciasHabitacion]
ADD CONSTRAINT [FK_HabitacionIncidencia]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HabitacionIncidencia'
CREATE INDEX [IX_FK_HabitacionIncidencia]
ON [SotSchema].[IncidenciasHabitacion]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdCorteTurno] in table 'IncidenciasTurno'
ALTER TABLE [SotSchema].[IncidenciasTurno]
ADD CONSTRAINT [FK_CorteTurnoIncidenciaTurno]
    FOREIGN KEY ([IdCorteTurno])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CorteTurnoIncidenciaTurno'
CREATE INDEX [IX_FK_CorteTurnoIncidenciaTurno]
ON [SotSchema].[IncidenciasTurno]
    ([IdCorteTurno]);
GO

-- Creating foreign key on [IdConfiguracionTurno] in table 'CortesTurno'
ALTER TABLE [SotSchema].[CortesTurno]
ADD CONSTRAINT [FK_ConfiguracionTurnoCorteTurno]
    FOREIGN KEY ([IdConfiguracionTurno])
    REFERENCES [SotSchema].[ConfiguracionesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTurnoCorteTurno'
CREATE INDEX [IX_FK_ConfiguracionTurnoCorteTurno]
ON [SotSchema].[CortesTurno]
    ([IdConfiguracionTurno]);
GO

-- Creating foreign key on [IdCorteTurno] in table 'Ventas'
ALTER TABLE [SotSchema].[Ventas]
ADD CONSTRAINT [FK_CorteTurnoVenta]
    FOREIGN KEY ([IdCorteTurno])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CorteTurnoVenta'
CREATE INDEX [IX_FK_CorteTurnoVenta]
ON [SotSchema].[Ventas]
    ([IdCorteTurno]);
GO

-- Creating foreign key on [IdVentaRenta] in table 'Extensiones'
ALTER TABLE [SotSchema].[Extensiones]
ADD CONSTRAINT [FK_VentaRentaRenovacion]
    FOREIGN KEY ([IdVentaRenta])
    REFERENCES [SotSchema].[VentasRenta]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VentaRentaRenovacion'
CREATE INDEX [IX_FK_VentaRentaRenovacion]
ON [SotSchema].[Extensiones]
    ([IdVentaRenta]);
GO

-- Creating foreign key on [IdVentaRenta] in table 'PersonasExtra'
ALTER TABLE [SotSchema].[PersonasExtra]
ADD CONSTRAINT [FK_VentaRentaPersonaExtra]
    FOREIGN KEY ([IdVentaRenta])
    REFERENCES [SotSchema].[VentasRenta]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VentaRentaPersonaExtra'
CREATE INDEX [IX_FK_VentaRentaPersonaExtra]
ON [SotSchema].[PersonasExtra]
    ([IdVentaRenta]);
GO

-- Creating foreign key on [IdVentaRenta] in table 'PagosRenta'
ALTER TABLE [SotSchema].[PagosRenta]
ADD CONSTRAINT [FK_VentaRentaPagoRenta]
    FOREIGN KEY ([IdVentaRenta])
    REFERENCES [SotSchema].[VentasRenta]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VentaRentaPagoRenta'
CREATE INDEX [IX_FK_VentaRentaPagoRenta]
ON [SotSchema].[PagosRenta]
    ([IdVentaRenta]);
GO

-- Creating foreign key on [IdVentaRenta] in table 'PaquetesRenta'
ALTER TABLE [SotSchema].[PaquetesRenta]
ADD CONSTRAINT [FK_VentaRentaPaqueteRenta]
    FOREIGN KEY ([IdVentaRenta])
    REFERENCES [SotSchema].[VentasRenta]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VentaRentaPaqueteRenta'
CREATE INDEX [IX_FK_VentaRentaPaqueteRenta]
ON [SotSchema].[PaquetesRenta]
    ([IdVentaRenta]);
GO

-- Creating foreign key on [IdVentaRenta] in table 'DetallesPago'
ALTER TABLE [SotSchema].[DetallesPago]
ADD CONSTRAINT [FK_VentaRentaDetallePago]
    FOREIGN KEY ([IdVentaRenta])
    REFERENCES [SotSchema].[VentasRenta]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VentaRentaDetallePago'
CREATE INDEX [IX_FK_VentaRentaDetallePago]
ON [SotSchema].[DetallesPago]
    ([IdVentaRenta]);
GO

-- Creating foreign key on [IdRenta] in table 'VentasRenta'
ALTER TABLE [SotSchema].[VentasRenta]
ADD CONSTRAINT [FK_RentaVentaRenta]
    FOREIGN KEY ([IdRenta])
    REFERENCES [SotSchema].[Rentas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RentaVentaRenta'
CREATE INDEX [IX_FK_RentaVentaRenta]
ON [SotSchema].[VentasRenta]
    ([IdRenta]);
GO

-- Creating foreign key on [IdValet] in table 'VentasRenta'
ALTER TABLE [SotSchema].[VentasRenta]
ADD CONSTRAINT [FK_EmpleadoVentaRenta]
    FOREIGN KEY ([IdValet])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoVentaRenta'
CREATE INDEX [IX_FK_EmpleadoVentaRenta]
ON [SotSchema].[VentasRenta]
    ([IdValet]);
GO

-- Creating foreign key on [IdCategoria] in table 'CentrosCostos'
ALTER TABLE [SotSchema].[CentrosCostos]
ADD CONSTRAINT [FK_CategoriaCentroCostosCentroCostos]
    FOREIGN KEY ([IdCategoria])
    REFERENCES [SotSchema].[CategoriasCentroCostos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoriaCentroCostosCentroCostos'
CREATE INDEX [IX_FK_CategoriaCentroCostosCentroCostos]
ON [SotSchema].[CentrosCostos]
    ([IdCategoria]);
GO

-- Creating foreign key on [IdConceptoGasto] in table 'Gastos'
ALTER TABLE [SotSchema].[Gastos]
ADD CONSTRAINT [FK_ConceptoGastoGasto]
    FOREIGN KEY ([IdConceptoGasto])
    REFERENCES [SotSchema].[ConceptosGastos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConceptoGastoGasto'
CREATE INDEX [IX_FK_ConceptoGastoGasto]
ON [SotSchema].[Gastos]
    ([IdConceptoGasto]);
GO

-- Creating foreign key on [IdCentroCostos] in table 'ConceptosGastos'
ALTER TABLE [SotSchema].[ConceptosGastos]
ADD CONSTRAINT [FK_CentroCostosConceptoGasto]
    FOREIGN KEY ([IdCentroCostos])
    REFERENCES [SotSchema].[CentrosCostos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CentroCostosConceptoGasto'
CREATE INDEX [IX_FK_CentroCostosConceptoGasto]
ON [SotSchema].[ConceptosGastos]
    ([IdCentroCostos]);
GO

-- Creating foreign key on [IdEmpleado] in table 'Asistencias'
ALTER TABLE [SotSchema].[Asistencias]
ADD CONSTRAINT [FK_EmpleadoAsistencia]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoAsistencia'
CREATE INDEX [IX_FK_EmpleadoAsistencia]
ON [SotSchema].[Asistencias]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdTurno] in table 'Empleados'
ALTER TABLE [SotSchema].[Empleados]
ADD CONSTRAINT [FK_ConfiguracionTurnoEmpleado]
    FOREIGN KEY ([IdTurno])
    REFERENCES [SotSchema].[ConfiguracionesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTurnoEmpleado'
CREATE INDEX [IX_FK_ConfiguracionTurnoEmpleado]
ON [SotSchema].[Empleados]
    ([IdTurno]);
GO

-- Creating foreign key on [IdTurno] in table 'Asistencias'
ALTER TABLE [SotSchema].[Asistencias]
ADD CONSTRAINT [FK_ConfiguracionTurnoAsistencia]
    FOREIGN KEY ([IdTurno])
    REFERENCES [SotSchema].[ConfiguracionesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConfiguracionTurnoAsistencia'
CREATE INDEX [IX_FK_ConfiguracionTurnoAsistencia]
ON [SotSchema].[Asistencias]
    ([IdTurno]);
GO

-- Creating foreign key on [IdEmpleado] in table 'SolicitudesFaltas'
ALTER TABLE [SotSchema].[SolicitudesFaltas]
ADD CONSTRAINT [FK_EmpleadoSolicitudFalta]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoSolicitudFalta'
CREATE INDEX [IX_FK_EmpleadoSolicitudFalta]
ON [SotSchema].[SolicitudesFaltas]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdEmpleadoSuplente] in table 'SolicitudesFaltas'
ALTER TABLE [SotSchema].[SolicitudesFaltas]
ADD CONSTRAINT [FK_EmpleadoSolicitudFalta1]
    FOREIGN KEY ([IdEmpleadoSuplente])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoSolicitudFalta1'
CREATE INDEX [IX_FK_EmpleadoSolicitudFalta1]
ON [SotSchema].[SolicitudesFaltas]
    ([IdEmpleadoSuplente]);
GO

-- Creating foreign key on [IdEmpleado] in table 'SolicitudesPermisos'
ALTER TABLE [SotSchema].[SolicitudesPermisos]
ADD CONSTRAINT [FK_EmpleadoSolicitudPermiso]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoSolicitudPermiso'
CREATE INDEX [IX_FK_EmpleadoSolicitudPermiso]
ON [SotSchema].[SolicitudesPermisos]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdEmpleado] in table 'SolicitudesCambioDescanso'
ALTER TABLE [SotSchema].[SolicitudesCambioDescanso]
ADD CONSTRAINT [FK_EmpleadoSolicitudCambioDescanso]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoSolicitudCambioDescanso'
CREATE INDEX [IX_FK_EmpleadoSolicitudCambioDescanso]
ON [SotSchema].[SolicitudesCambioDescanso]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdEmpleadoSuplente] in table 'SolicitudesCambioDescanso'
ALTER TABLE [SotSchema].[SolicitudesCambioDescanso]
ADD CONSTRAINT [FK_EmpleadoSolicitudCambioDescanso1]
    FOREIGN KEY ([IdEmpleadoSuplente])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoSolicitudCambioDescanso1'
CREATE INDEX [IX_FK_EmpleadoSolicitudCambioDescanso1]
ON [SotSchema].[SolicitudesCambioDescanso]
    ([IdEmpleadoSuplente]);
GO

-- Creating foreign key on [IdEmpleado] in table 'SolicitudesVacaciones'
ALTER TABLE [SotSchema].[SolicitudesVacaciones]
ADD CONSTRAINT [FK_EmpleadoSolicitudVacaciones]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoSolicitudVacaciones'
CREATE INDEX [IX_FK_EmpleadoSolicitudVacaciones]
ON [SotSchema].[SolicitudesVacaciones]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdCorte] in table 'Comandas'
ALTER TABLE [SotSchema].[Comandas]
ADD CONSTRAINT [FK_CorteTurnoComanda]
    FOREIGN KEY ([IdCorte])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CorteTurnoComanda'
CREATE INDEX [IX_FK_CorteTurnoComanda]
ON [SotSchema].[Comandas]
    ([IdCorte]);
GO

-- Creating foreign key on [IdPremio] in table 'ArticulosPremio'
ALTER TABLE [SotSchema].[ArticulosPremio]
ADD CONSTRAINT [FK_PremioArticuloPremio]
    FOREIGN KEY ([IdPremio])
    REFERENCES [SotSchema].[Premios]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PremioArticuloPremio'
CREATE INDEX [IX_FK_PremioArticuloPremio]
ON [SotSchema].[ArticulosPremio]
    ([IdPremio]);
GO

-- Creating foreign key on [IdRenta] in table 'AbonosPuntos'
ALTER TABLE [SotSchema].[AbonosPuntos]
ADD CONSTRAINT [FK_RentaAbonoPuntos]
    FOREIGN KEY ([IdRenta])
    REFERENCES [SotSchema].[Rentas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RentaAbonoPuntos'
CREATE INDEX [IX_FK_RentaAbonoPuntos]
ON [SotSchema].[AbonosPuntos]
    ([IdRenta]);
GO

-- Creating foreign key on [IdDatosFiscales] in table 'Reservaciones'
ALTER TABLE [SotSchema].[Reservaciones]
ADD CONSTRAINT [FK_DatosFiscalesReservacion]
    FOREIGN KEY ([IdDatosFiscales])
    REFERENCES [SotSchema].[DatosFiscales]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DatosFiscalesReservacion'
CREATE INDEX [IX_FK_DatosFiscalesReservacion]
ON [SotSchema].[Reservaciones]
    ([IdDatosFiscales]);
GO

-- Creating foreign key on [IdDatosFiscales] in table 'Rentas'
ALTER TABLE [SotSchema].[Rentas]
ADD CONSTRAINT [FK_DatosFiscalesRenta]
    FOREIGN KEY ([IdDatosFiscales])
    REFERENCES [SotSchema].[DatosFiscales]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DatosFiscalesRenta'
CREATE INDEX [IX_FK_DatosFiscalesRenta]
ON [SotSchema].[Rentas]
    ([IdDatosFiscales]);
GO

-- Creating foreign key on [IdEmpleado] in table 'Nominas'
ALTER TABLE [SotSchema].[Nominas]
ADD CONSTRAINT [FK_EmpleadoNomina]
    FOREIGN KEY ([IdEmpleado])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoNomina'
CREATE INDEX [IX_FK_EmpleadoNomina]
ON [SotSchema].[Nominas]
    ([IdEmpleado]);
GO

-- Creating foreign key on [IdCategoria] in table 'Empleados'
ALTER TABLE [SotSchema].[Empleados]
ADD CONSTRAINT [FK_CategoriaEmpleadoEmpleado]
    FOREIGN KEY ([IdCategoria])
    REFERENCES [SotSchema].[CategoriasEmpleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoriaEmpleadoEmpleado'
CREATE INDEX [IX_FK_CategoriaEmpleadoEmpleado]
ON [SotSchema].[Empleados]
    ([IdCategoria]);
GO

-- Creating foreign key on [IdDatosFiscales] in table 'OcupacionesMesa'
ALTER TABLE [SotSchema].[OcupacionesMesa]
ADD CONSTRAINT [FK_DatosFiscalesOcupacionMesa]
    FOREIGN KEY ([IdDatosFiscales])
    REFERENCES [SotSchema].[DatosFiscales]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DatosFiscalesOcupacionMesa'
CREATE INDEX [IX_FK_DatosFiscalesOcupacionMesa]
ON [SotSchema].[OcupacionesMesa]
    ([IdDatosFiscales]);
GO

-- Creating foreign key on [IdArea] in table 'Puestos'
ALTER TABLE [SotSchema].[Puestos]
ADD CONSTRAINT [FK_AreaPuesto]
    FOREIGN KEY ([IdArea])
    REFERENCES [SotSchema].[Areas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AreaPuesto'
CREATE INDEX [IX_FK_AreaPuesto]
ON [SotSchema].[Puestos]
    ([IdArea]);
GO

-- Creating foreign key on [IdArea] in table 'SolicitudesCambioDescanso'
ALTER TABLE [SotSchema].[SolicitudesCambioDescanso]
ADD CONSTRAINT [FK_AreaSolicitudCambioDescanso]
    FOREIGN KEY ([IdArea])
    REFERENCES [SotSchema].[Areas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AreaSolicitudCambioDescanso'
CREATE INDEX [IX_FK_AreaSolicitudCambioDescanso]
ON [SotSchema].[SolicitudesCambioDescanso]
    ([IdArea]);
GO

-- Creating foreign key on [IdArea] in table 'SolicitudesFaltas'
ALTER TABLE [SotSchema].[SolicitudesFaltas]
ADD CONSTRAINT [FK_AreaSolicitudFalta]
    FOREIGN KEY ([IdArea])
    REFERENCES [SotSchema].[Areas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AreaSolicitudFalta'
CREATE INDEX [IX_FK_AreaSolicitudFalta]
ON [SotSchema].[SolicitudesFaltas]
    ([IdArea]);
GO

-- Creating foreign key on [IdArea] in table 'SolicitudesPermisos'
ALTER TABLE [SotSchema].[SolicitudesPermisos]
ADD CONSTRAINT [FK_AreaSolicitudPermiso]
    FOREIGN KEY ([IdArea])
    REFERENCES [SotSchema].[Areas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AreaSolicitudPermiso'
CREATE INDEX [IX_FK_AreaSolicitudPermiso]
ON [SotSchema].[SolicitudesPermisos]
    ([IdArea]);
GO

-- Creating foreign key on [IdArea] in table 'SolicitudesVacaciones'
ALTER TABLE [SotSchema].[SolicitudesVacaciones]
ADD CONSTRAINT [FK_AreaSolicitudVacaciones]
    FOREIGN KEY ([IdArea])
    REFERENCES [SotSchema].[Areas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AreaSolicitudVacaciones'
CREATE INDEX [IX_FK_AreaSolicitudVacaciones]
ON [SotSchema].[SolicitudesVacaciones]
    ([IdArea]);
GO

-- Creating foreign key on [IdPaquete] in table 'ProgramasPaquete'
ALTER TABLE [SotSchema].[ProgramasPaquete]
ADD CONSTRAINT [FK_PaqueteProgramaPaquete]
    FOREIGN KEY ([IdPaquete])
    REFERENCES [SotSchema].[Paquetes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PaqueteProgramaPaquete'
CREATE INDEX [IX_FK_PaqueteProgramaPaquete]
ON [SotSchema].[ProgramasPaquete]
    ([IdPaquete]);
GO

-- Creating foreign key on [IdHabitacion] in table 'ObjetosOlvidados'
ALTER TABLE [SotSchema].[ObjetosOlvidados]
ADD CONSTRAINT [FK_HabitacionObjetoOlvidado]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HabitacionObjetoOlvidado'
CREATE INDEX [IX_FK_HabitacionObjetoOlvidado]
ON [SotSchema].[ObjetosOlvidados]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdEmpleadoReporta] in table 'ObjetosOlvidados'
ALTER TABLE [SotSchema].[ObjetosOlvidados]
ADD CONSTRAINT [FK_EmpleadoObjetoOlvidado]
    FOREIGN KEY ([IdEmpleadoReporta])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoObjetoOlvidado'
CREATE INDEX [IX_FK_EmpleadoObjetoOlvidado]
ON [SotSchema].[ObjetosOlvidados]
    ([IdEmpleadoReporta]);
GO

-- Creating foreign key on [IdMotivo] in table 'TareasLimpieza'
ALTER TABLE [SotSchema].[TareasLimpieza]
ADD CONSTRAINT [FK_ConceptoSistemaTareaLimpieza]
    FOREIGN KEY ([IdMotivo])
    REFERENCES [SotSchema].[ConceptosSistema]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConceptoSistemaTareaLimpieza'
CREATE INDEX [IX_FK_ConceptoSistemaTareaLimpieza]
ON [SotSchema].[TareasLimpieza]
    ([IdMotivo]);
GO

-- Creating foreign key on [IdConceptoMantenimiento] in table 'TareasMantenimiento'
ALTER TABLE [SotSchema].[TareasMantenimiento]
ADD CONSTRAINT [FK_TareaMantenimientoConceptoMantenimiento]
    FOREIGN KEY ([IdConceptoMantenimiento])
    REFERENCES [SotSchema].[ConceptosMantenimiento]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TareaMantenimientoConceptoMantenimiento'
CREATE INDEX [IX_FK_TareaMantenimientoConceptoMantenimiento]
ON [SotSchema].[TareasMantenimiento]
    ([IdConceptoMantenimiento]);
GO

-- Creating foreign key on [IdConceptoMantenimiento] in table 'Mantenimientos'
ALTER TABLE [SotSchema].[Mantenimientos]
ADD CONSTRAINT [FK_ConceptoMantenimientoMantenimiento]
    FOREIGN KEY ([IdConceptoMantenimiento])
    REFERENCES [SotSchema].[ConceptosMantenimiento]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConceptoMantenimientoMantenimiento'
CREATE INDEX [IX_FK_ConceptoMantenimientoMantenimiento]
ON [SotSchema].[Mantenimientos]
    ([IdConceptoMantenimiento]);
GO

-- Creating foreign key on [IdTareaMantenimiento] in table 'Mantenimientos'
ALTER TABLE [SotSchema].[Mantenimientos]
ADD CONSTRAINT [FK_TareaMantenimientoMantenimiento]
    FOREIGN KEY ([IdTareaMantenimiento])
    REFERENCES [SotSchema].[TareasMantenimiento]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TareaMantenimientoMantenimiento'
CREATE INDEX [IX_FK_TareaMantenimientoMantenimiento]
ON [SotSchema].[Mantenimientos]
    ([IdTareaMantenimiento]);
GO

-- Creating foreign key on [IdMantenimiento] in table 'OrdenesTrabajoMantenimiento'
ALTER TABLE [SotSchema].[OrdenesTrabajoMantenimiento]
ADD CONSTRAINT [FK_MantenimientoOrdenTrabajoMantenimiento]
    FOREIGN KEY ([IdMantenimiento])
    REFERENCES [SotSchema].[Mantenimientos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MantenimientoOrdenTrabajoMantenimiento'
CREATE INDEX [IX_FK_MantenimientoOrdenTrabajoMantenimiento]
ON [SotSchema].[OrdenesTrabajoMantenimiento]
    ([IdMantenimiento]);
GO

-- Creating foreign key on [IdArea] in table 'IncidenciasTurno'
ALTER TABLE [SotSchema].[IncidenciasTurno]
ADD CONSTRAINT [FK_AreaIncidenciaTurno]
    FOREIGN KEY ([IdArea])
    REFERENCES [SotSchema].[Areas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AreaIncidenciaTurno'
CREATE INDEX [IX_FK_AreaIncidenciaTurno]
ON [SotSchema].[IncidenciasTurno]
    ([IdArea]);
GO

-- Creating foreign key on [IdConsumoInterno] in table 'ArticulosConsumoInterno'
ALTER TABLE [SotSchema].[ArticulosConsumoInterno]
ADD CONSTRAINT [FK_ConsumoInternoArticuloConsumoInterno]
    FOREIGN KEY ([IdConsumoInterno])
    REFERENCES [SotSchema].[ConsumosInternos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConsumoInternoArticuloConsumoInterno'
CREATE INDEX [IX_FK_ConsumoInternoArticuloConsumoInterno]
ON [SotSchema].[ArticulosConsumoInterno]
    ([IdConsumoInterno]);
GO

-- Creating foreign key on [IdCorteTurno] in table 'ConsumosInternos'
ALTER TABLE [SotSchema].[ConsumosInternos]
ADD CONSTRAINT [FK_CorteTurnoConsumoInterno]
    FOREIGN KEY ([IdCorteTurno])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CorteTurnoConsumoInterno'
CREATE INDEX [IX_FK_CorteTurnoConsumoInterno]
ON [SotSchema].[ConsumosInternos]
    ([IdCorteTurno]);
GO

-- Creating foreign key on [IdConsumoInterno] in table 'PagosConsumoInterno'
ALTER TABLE [SotSchema].[PagosConsumoInterno]
ADD CONSTRAINT [FK_ConsumoInternoPagoConsumoInterno]
    FOREIGN KEY ([IdConsumoInterno])
    REFERENCES [SotSchema].[ConsumosInternos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConsumoInternoPagoConsumoInterno'
CREATE INDEX [IX_FK_ConsumoInternoPagoConsumoInterno]
ON [SotSchema].[PagosConsumoInterno]
    ([IdConsumoInterno]);
GO

-- Creating foreign key on [IdMesero] in table 'ConsumosInternos'
ALTER TABLE [SotSchema].[ConsumosInternos]
ADD CONSTRAINT [FK_EmpleadoConsumoInterno]
    FOREIGN KEY ([IdMesero])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoConsumoInterno'
CREATE INDEX [IX_FK_EmpleadoConsumoInterno]
ON [SotSchema].[ConsumosInternos]
    ([IdMesero]);
GO

-- Creating foreign key on [IdEmpleadoConsume] in table 'ConsumosInternos'
ALTER TABLE [SotSchema].[ConsumosInternos]
ADD CONSTRAINT [FK_EmpleadoConsumoInterno1]
    FOREIGN KEY ([IdEmpleadoConsume])
    REFERENCES [SotSchema].[Empleados]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoConsumoInterno1'
CREATE INDEX [IX_FK_EmpleadoConsumoInterno1]
ON [SotSchema].[ConsumosInternos]
    ([IdEmpleadoConsume]);
GO

-- Creating foreign key on [IdCorteTurno] in table 'TareasLimpieza'
ALTER TABLE [SotSchema].[TareasLimpieza]
ADD CONSTRAINT [FK_CorteTurnoTareaLimpieza]
    FOREIGN KEY ([IdCorteTurno])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CorteTurnoTareaLimpieza'
CREATE INDEX [IX_FK_CorteTurnoTareaLimpieza]
ON [SotSchema].[TareasLimpieza]
    ([IdCorteTurno]);
GO

-- Creating foreign key on [IdHabitacion] in table 'HistorialesHabitacion'
ALTER TABLE [SotSchema].[HistorialesHabitacion]
ADD CONSTRAINT [FK_HabitacionHistorialHabitacion]
    FOREIGN KEY ([IdHabitacion])
    REFERENCES [SotSchema].[Habitaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HabitacionHistorialHabitacion'
CREATE INDEX [IX_FK_HabitacionHistorialHabitacion]
ON [SotSchema].[HistorialesHabitacion]
    ([IdHabitacion]);
GO

-- Creating foreign key on [IdConsumoInterno] in table 'HistorialesConsumoInterno'
ALTER TABLE [SotSchema].[HistorialesConsumoInterno]
ADD CONSTRAINT [FK_ConsumoInternoHistorialConsumoInterno]
    FOREIGN KEY ([IdConsumoInterno])
    REFERENCES [SotSchema].[ConsumosInternos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConsumoInternoHistorialConsumoInterno'
CREATE INDEX [IX_FK_ConsumoInternoHistorialConsumoInterno]
ON [SotSchema].[HistorialesConsumoInterno]
    ([IdConsumoInterno]);
GO

-- Creating foreign key on [IdComanda] in table 'HistorialesComanda'
ALTER TABLE [SotSchema].[HistorialesComanda]
ADD CONSTRAINT [FK_ComandaHistorialComanda]
    FOREIGN KEY ([IdComanda])
    REFERENCES [SotSchema].[Comandas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ComandaHistorialComanda'
CREATE INDEX [IX_FK_ComandaHistorialComanda]
ON [SotSchema].[HistorialesComanda]
    ([IdComanda]);
GO

-- Creating foreign key on [IdOrden] in table 'HistorialesOrdenRestaurante'
ALTER TABLE [SotSchema].[HistorialesOrdenRestaurante]
ADD CONSTRAINT [FK_OrdenRestauranteHistorialOrdenRestaurante]
    FOREIGN KEY ([IdOrden])
    REFERENCES [SotSchema].[OrdenesRestaurante]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrdenRestauranteHistorialOrdenRestaurante'
CREATE INDEX [IX_FK_OrdenRestauranteHistorialOrdenRestaurante]
ON [SotSchema].[HistorialesOrdenRestaurante]
    ([IdOrden]);
GO

-- Creating foreign key on [Recamarera] in table 'PuestosMaestros'
ALTER TABLE [SotSchema].[PuestosMaestros]
ADD CONSTRAINT [FK_PuestoPuestosMaestro]
    FOREIGN KEY ([Recamarera])
    REFERENCES [SotSchema].[Puestos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PuestoPuestosMaestro'
CREATE INDEX [IX_FK_PuestoPuestosMaestro]
ON [SotSchema].[PuestosMaestros]
    ([Recamarera]);
GO

-- Creating foreign key on [Supervisor] in table 'PuestosMaestros'
ALTER TABLE [SotSchema].[PuestosMaestros]
ADD CONSTRAINT [FK_PuestoPuestosMaestroS]
    FOREIGN KEY ([Supervisor])
    REFERENCES [SotSchema].[Puestos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PuestoPuestosMaestroS'
CREATE INDEX [IX_FK_PuestoPuestosMaestroS]
ON [SotSchema].[PuestosMaestros]
    ([Supervisor]);
GO

-- Creating foreign key on [Mesero] in table 'PuestosMaestros'
ALTER TABLE [SotSchema].[PuestosMaestros]
ADD CONSTRAINT [FK_PuestoPuestosMaestrosM]
    FOREIGN KEY ([Mesero])
    REFERENCES [SotSchema].[Puestos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PuestoPuestosMaestrosM'
CREATE INDEX [IX_FK_PuestoPuestosMaestrosM]
ON [SotSchema].[PuestosMaestros]
    ([Mesero]);
GO

-- Creating foreign key on [Valet] in table 'PuestosMaestros'
ALTER TABLE [SotSchema].[PuestosMaestros]
ADD CONSTRAINT [FK_PuestoPuestosMaestrosV]
    FOREIGN KEY ([Valet])
    REFERENCES [SotSchema].[Puestos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PuestoPuestosMaestrosV'
CREATE INDEX [IX_FK_PuestoPuestosMaestrosV]
ON [SotSchema].[PuestosMaestros]
    ([Valet]);
GO

-- Creating foreign key on [Vendedor] in table 'PuestosMaestros'
ALTER TABLE [SotSchema].[PuestosMaestros]
ADD CONSTRAINT [FK_PuestoPuestosMaestros1]
    FOREIGN KEY ([Vendedor])
    REFERENCES [SotSchema].[Puestos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PuestoPuestosMaestros1'
CREATE INDEX [IX_FK_PuestoPuestosMaestros1]
ON [SotSchema].[PuestosMaestros]
    ([Vendedor]);
GO

-- Creating foreign key on [IdTipoSincronizacion] in table 'PreciosTipoHabitacionSincronizacion'
ALTER TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion]
ADD CONSTRAINT [FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion]
    FOREIGN KEY ([IdTipoSincronizacion])
    REFERENCES [SotSchema].[TiposHabitacionSincronizacion]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion'
CREATE INDEX [IX_FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion]
ON [SotSchema].[PreciosTipoHabitacionSincronizacion]
    ([IdTipoSincronizacion]);
GO

-- Creating foreign key on [IdConceptoGasto] in table 'ArticulosConceptosGasto'
ALTER TABLE [SotSchema].[ArticulosConceptosGasto]
ADD CONSTRAINT [FK_ConceptoGastoArticuloConceptoGasto]
    FOREIGN KEY ([IdConceptoGasto])
    REFERENCES [SotSchema].[ConceptosGastos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConceptoGastoArticuloConceptoGasto'
CREATE INDEX [IX_FK_ConceptoGastoArticuloConceptoGasto]
ON [SotSchema].[ArticulosConceptosGasto]
    ([IdConceptoGasto]);
GO

-- Creating foreign key on [IdPreventa] in table 'VentasRenta'
ALTER TABLE [SotSchema].[VentasRenta]
ADD CONSTRAINT [FK_PreventaVentaRenta]
    FOREIGN KEY ([IdPreventa])
    REFERENCES [SotSchema].[Preventas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PreventaVentaRenta'
CREATE INDEX [IX_FK_PreventaVentaRenta]
ON [SotSchema].[VentasRenta]
    ([IdPreventa]);
GO

-- Creating foreign key on [IdPreventa] in table 'Comandas'
ALTER TABLE [SotSchema].[Comandas]
ADD CONSTRAINT [FK_PreventaComanda]
    FOREIGN KEY ([IdPreventa])
    REFERENCES [SotSchema].[Preventas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PreventaComanda'
CREATE INDEX [IX_FK_PreventaComanda]
ON [SotSchema].[Comandas]
    ([IdPreventa]);
GO

-- Creating foreign key on [IdPreventa] in table 'ConsumosInternos'
ALTER TABLE [SotSchema].[ConsumosInternos]
ADD CONSTRAINT [FK_PreventaConsumoInterno]
    FOREIGN KEY ([IdPreventa])
    REFERENCES [SotSchema].[Preventas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PreventaConsumoInterno'
CREATE INDEX [IX_FK_PreventaConsumoInterno]
ON [SotSchema].[ConsumosInternos]
    ([IdPreventa]);
GO

-- Creating foreign key on [IdPreventa] in table 'OrdenesTaxis'
ALTER TABLE [SotSchema].[OrdenesTaxis]
ADD CONSTRAINT [FK_PreventaOrdenTaxi]
    FOREIGN KEY ([IdPreventa])
    REFERENCES [SotSchema].[Preventas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PreventaOrdenTaxi'
CREATE INDEX [IX_FK_PreventaOrdenTaxi]
ON [SotSchema].[OrdenesTaxis]
    ([IdPreventa]);
GO

-- Creating foreign key on [IdPreventa] in table 'OcupacionesMesa'
ALTER TABLE [SotSchema].[OcupacionesMesa]
ADD CONSTRAINT [FK_PreventaOcupacionMesa]
    FOREIGN KEY ([IdPreventa])
    REFERENCES [SotSchema].[Preventas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PreventaOcupacionMesa'
CREATE INDEX [IX_FK_PreventaOcupacionMesa]
ON [SotSchema].[OcupacionesMesa]
    ([IdPreventa]);
GO

-- Creating foreign key on [IdCorte] in table 'OcupacionesMesa'
ALTER TABLE [SotSchema].[OcupacionesMesa]
ADD CONSTRAINT [FK_OcupacionMesaCorteTurno]
    FOREIGN KEY ([IdCorte])
    REFERENCES [SotSchema].[CortesTurno]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OcupacionMesaCorteTurno'
CREATE INDEX [IX_FK_OcupacionMesaCorteTurno]
ON [SotSchema].[OcupacionesMesa]
    ([IdCorte]);
GO

-- Creating foreign key on [IdReservacion] in table 'PagosReservaciones'
ALTER TABLE [SotSchema].[PagosReservaciones]
ADD CONSTRAINT [FK_ReservacionPagoReservacion]
    FOREIGN KEY ([IdReservacion])
    REFERENCES [SotSchema].[Reservaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservacionPagoReservacion'
CREATE INDEX [IX_FK_ReservacionPagoReservacion]
ON [SotSchema].[PagosReservaciones]
    ([IdReservacion]);
GO

-- Creating foreign key on [IdReservacion] in table 'MontosNoReembolsables'
ALTER TABLE [SotSchema].[MontosNoReembolsables]
ADD CONSTRAINT [FK_ReservacionMontoNoReembolsable]
    FOREIGN KEY ([IdReservacion])
    REFERENCES [SotSchema].[Reservaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservacionMontoNoReembolsable'
CREATE INDEX [IX_FK_ReservacionMontoNoReembolsable]
ON [SotSchema].[MontosNoReembolsables]
    ([IdReservacion]);
GO

-- Creating foreign key on [IdVentaRenta] in table 'MontosNoReembolsablesRenta'
ALTER TABLE [SotSchema].[MontosNoReembolsablesRenta]
ADD CONSTRAINT [FK_VentaRentaMontoNoReembolsableRenta]
    FOREIGN KEY ([IdVentaRenta])
    REFERENCES [SotSchema].[VentasRenta]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VentaRentaMontoNoReembolsableRenta'
CREATE INDEX [IX_FK_VentaRentaMontoNoReembolsableRenta]
ON [SotSchema].[MontosNoReembolsablesRenta]
    ([IdVentaRenta]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------