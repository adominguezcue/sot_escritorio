//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class Renta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Renta()
        {
            this.RentaAutomoviles = new HashSet<AutomovilRenta>();
            this.Comandas = new HashSet<Comanda>();
            this.VentasRenta = new HashSet<VentaRenta>();
            this.AbonosPuntos = new HashSet<AbonoPuntos>();
        }
    
        public int Id { get; set; }
        public System.DateTime FechaRegistro { get; set; }
        public Nullable<System.DateTime> FechaSalida { get; set; }
        public System.DateTime FechaInicio { get; set; }
        public System.DateTime FechaFin { get; set; }
        public int IdUsuarioCreo { get; set; }
        public int IdUsuarioModifico { get; set; }
        public Nullable<int> IdUsuarioElimino { get; set; }
        public bool Activa { get; set; }
        public string NumeroServicio { get; set; }
        public int IdHabitacion { get; set; }
        public decimal PrecioHabitacion { get; set; }
        public int IdConfiguracionTipo { get; set; }
        public int IdEstado { get; set; }
        public System.DateTime FechaModificacion { get; set; }
        public string NumeroTarjeta { get; set; }
        public Nullable<int> IdDatosFiscales { get; set; }
        public string MotivoCancelacion { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AutomovilRenta> RentaAutomoviles { get; set; }
        public virtual Habitacion Habitacion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comanda> Comandas { get; set; }
        public virtual ConfiguracionTipo ConfiguracionTipo { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VentaRenta> VentasRenta { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AbonoPuntos> AbonosPuntos { get; set; }
        public virtual DatosFiscales DatosFiscales { get; set; }
    }
}
