//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos.Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class PorcentajePropinaTipo
    {
        public int Id { get; set; }
        public int IdConfiguracionPropina { get; set; }
        public int IdLineaArticulo { get; set; }
        public decimal Porcentaje { get; set; }
        public bool Activo { get; set; }
    
        public virtual ConfiguracionPropinas ConfiguracionPropinas { get; set; }
    }
}
