﻿USE [SOT_PROD]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCantidadCruzadas]    Script Date: 02/04/2018 07:24:45 p. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerCantidadCruzadas]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCantidadCruzadas]    Script Date: 02/04/2018 07:24:46 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date: 02/04/2018
-- Description:	Retorna la cantidad de reservaciones no canceladas que se cruzan con las fechas proporcionadas y que son del mismo tipo de habitación
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerCantidadCruzadas] 
	@fechaEntrada datetime,
	@fechaSalida datetime,
	@idReservacion int,
	@idConfiguracionTipoHabitacion int
AS
BEGIN
	

declare @idtipoHabitacion int
declare @idEstadoConfirmada int = 2
declare @idEstadoPendiente int = 1
declare @idEstadoConsumida int = 3
--declare @entrada int
--declare @salida int

select 
@idtipoHabitacion = isnull(IdTipoHabitacion, 0)
--@entrada = isnull(DuracionOEntrada, 0),
--@salida = isnull(HoraSalida, 0)
 from SotSchema.ConfiguracionesTipos
where Id = @idConfiguracionTipoHabitacion

select COUNT(*) from SotSchema.Reservaciones r
inner join SotSchema.ConfiguracionesTipos ct
	on r.IdConfiguracionTipoHabitacion = ct.Id
where r.Id <> @idReservacion
and ct.IdTipoHabitacion = @idtipoHabitacion
and r.Activo = 1
and r.IdEstado in (@idEstadoConfirmada, @idEstadoConsumida, @idEstadoPendiente)
and ((dateadd(HOUR, ct.DuracionOEntrada, r.FechaEntrada) <= @fechaEntrada and dateadd(HOUR, ct.HoraSalida, r.FechaSalida) >= @fechaEntrada and dateadd(HOUR, ct.HoraSalida, r.FechaSalida) <= @fechaSalida)
or (dateadd(HOUR, ct.DuracionOEntrada, r.FechaEntrada) >= @fechaEntrada and dateadd(HOUR, ct.DuracionOEntrada, r.FechaEntrada) <= @fechaSalida)
or (dateadd(HOUR, ct.DuracionOEntrada, r.FechaEntrada) <= @fechaEntrada and dateadd(HOUR, ct.HoraSalida, r.FechaSalida) >= @fechaSalida)
or (dateadd(HOUR, ct.DuracionOEntrada, r.FechaEntrada) >= @fechaEntrada and dateadd(HOUR, ct.HoraSalida, r.FechaSalida) <= @fechaSalida))
END

GO


