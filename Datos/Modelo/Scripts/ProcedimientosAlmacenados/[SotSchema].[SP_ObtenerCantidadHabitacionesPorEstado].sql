﻿USE [SOT]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCantidadHabitacionesPorEstado]    Script Date: 18/01/2018 11:02:11 a. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerCantidadHabitacionesPorEstado]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCantidadHabitacionesPorEstado]    Script Date: 18/01/2018 11:02:11 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date:	18 de enero de 2018
-- Description:	Retorna la cantidad de habitaciones que están (o estuvieron) en los diferentes estados posibles en la fecha proporcionada agrupadas por tipo
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerCantidadHabitacionesPorEstado] 
	-- Add the parameters for the stored procedure here
	@fecha datetime
AS
BEGIN
    ;with resumenes as
	(
		select
			IdTipoHabitacion,
			IdEstadoHabitacion,
			COUNT(Id) as Cantidad
		from SotSchema.Habitaciones
		where FechaModificacion <= @fecha
		and (FechaEliminacion is NULL or FechaEliminacion >= @fecha)
		and Activo = 1
		group by 
			IdTipoHabitacion,
			IdEstadoHabitacion
		union
		select
			his.IdTipoHabitacion,
			his.IdEstadoHabitacion,
			COUNT(h.Id) as Cantidad
		from SotSchema.HistorialesHabitacion his
		inner join SotSchema.Habitaciones h
			on his.IdHabitacion = h.Id
		where FechaInicio <= @fecha
		and FechaFin >= @fecha
		and his.Activo = 1
		group by 
			his.IdTipoHabitacion,
			his.IdEstadoHabitacion
	),
	combinaciones as
	(
		select IdTipoHabitacion, IdEstadoHabitacion, SUM(r.Cantidad) as Cantidad from resumenes r
		group by IdTipoHabitacion, IdEstadoHabitacion
	)
	select th.Descripcion as TipoHabitacion, c.* from combinaciones c
	left join SotSchema.TiposHabitacion th
		on c.IdTipoHabitacion = th.Id

END

GO


