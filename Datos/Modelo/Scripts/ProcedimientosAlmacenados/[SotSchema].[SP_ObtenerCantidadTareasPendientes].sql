﻿USE [SOT]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCantidadTareasPendientes]    Script Date: 27/11/2017 01:17:37 a. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerCantidadTareasPendientes]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCantidadTareasPendientes]    Script Date: 27/11/2017 01:17:37 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date:	01 de noviembre de 2017
-- Description:	Retorna la cantidad de mantenimientos pendientes hasta la fecha actual
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerCantidadTareasPendientes] 
	-- Add the parameters for the stored procedure here
	@idHabitacion int = 0
AS
BEGIN

    if(@idHabitacion = 0)

	   select COUNT(Id) from SotSchema.TareasMantenimiento
	   where dateadd(day, -DiasAnticipacion, FechaSiguiente) <= GETDATE()
	   and Activa = 1
    else
    	   select COUNT(Id) from SotSchema.TareasMantenimiento
	   where dateadd(day, -DiasAnticipacion, FechaSiguiente) <= GETDATE()
	   and Activa = 1
	   and IdHabitacion = @idHabitacion

END


GO


