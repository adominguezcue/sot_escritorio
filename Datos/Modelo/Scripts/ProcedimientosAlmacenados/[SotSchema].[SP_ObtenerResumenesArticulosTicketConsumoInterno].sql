USE [SOT]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketConsumoInterno]    Script Date: 22/12/2017 10:39:31 a. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketConsumoInterno]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketConsumoInterno]    Script Date: 22/12/2017 10:39:31 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Eduardo Abraham V�zquez Rosado
-- Create date:     29 de noviembre de 2017
-- Description:	Retorna un resumen con la cantidad total (con iva) y concepto por art�culo
--				perteneciente al consumo interno con el id porporcionado
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketConsumoInterno]
	@idConsumoInterno int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @IVA decimal(19,3)

    select @IVA = Iva_CatPar from dbo.ZctCatPar

    select Cantidad, Desc_Art as Nombre, ROUND(PrecioUnidad * (1 + @IVA), 2) as PrecioUnidad, lin.IdLineaBase, convert(bit, 0) as EsCortesia, art.Cod_Art as Codigo
	from SotSchema.ArticulosConsumoInterno aCom
    left join dbo.ZctCatArt art
	   on aCom.IdArticulo = art.Cod_Art
	left join dbo.ZctCatLinea lin
		on art.Cod_Linea = lin.Cod_Linea
    where Activo = 1
    and IdConsumoInterno = @idConsumoInterno
END

GO


