USE [SOT]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketOrdenRestaurante]    Script Date: 22/12/2017 10:39:37 a. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketOrdenRestaurante]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketOrdenRestaurante]    Script Date: 22/12/2017 10:39:37 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Eduardo Abraham V�zquez Rosado
-- Create date:     17 de octubre de 2017
-- Description:	Retorna un resumen con la cantidad total (con iva) y concepto por art�culo
--				perteneciente a la orden de restaurante con el id porporcionado
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketOrdenRestaurante]
	@idOrdenRestaurante int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @IVA decimal(19,3)

    select @IVA = Iva_CatPar from dbo.ZctCatPar

    select Cantidad, Desc_Art as Nombre, ROUND(PrecioUnidad * (1 + @IVA), 2) as PrecioUnidad, lin.IdLineaBase, EsCortesia, art.Cod_Art as Codigo
	from SotSchema.ArticulosOrdenesRestaurante aCom
    left join dbo.ZctCatArt art
	   on aCom.IdArticulo = art.Cod_Art
	left join dbo.ZctCatLinea lin
		on art.Cod_Linea = lin.Cod_Linea
    where Activo = 1
    and IdOrdenRestaurante = @idOrdenRestaurante
END


GO


