﻿USE [SOT]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenHabitacion]    Script Date: 12/01/2018 12:34:27 p. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerResumenHabitacion]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenHabitacion]    Script Date: 12/01/2018 12:34:27 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date: 12 de enero de 2018
-- Description:	Retorna un resumen del estatus de la habitación con el id proporcionado (estado, recamareras, tiempo, etc)
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerResumenHabitacion]
	@idHabitacion int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



declare @estado_Libre int = 1,
		@estado_PendienteCobro int = 2,
		@estado_Preparada int = 3,
		@estado_Reservada int = 4,
		@estado_PreparadaReservada int = 5,
		@estado_Bloqueada int = 6,
		@estado_Ocupada int = 7,
		@estado_Sucia int = 8,
		@estado_Limpieza int = 9,
		@estado_Supervision int = 10,
		@estado_Mantenimiento int = 11

declare @txtNumero varchar(max),
	    @txtTipo varchar(max),
	    @txtDetalle varchar(max)
declare @estado int,
		@estadoComanda int
declare @esVencida bit,
		@tieneTarjetaV bit = 0


declare @fechaModificacion datetime
declare @defaultAuto varchar(5) = 'A pie'
declare @fechaActual datetime = GETDATE()
declare @idRenta int



declare @fechaFin datetime
declare @auto varchar(max)

select
	@txtNumero = h.NumeroHabitacion,
	@txtTipo = t.Descripcion,
	@estado = h.IdEstadoHabitacion,
	@fechaModificacion = h.FechaModificacion
from SotSchema.Habitaciones h
inner join SotSchema.TiposHabitacion t
	on h.IdTipoHabitacion = t.Id
where h.Id = @idHabitacion


declare @idEstadoTarea int = 1

	declare @idTarea int,
			@idTipoLimpieza int,
			@minutosLimpieza int
	declare	@fechaTarea datetime

if(@estado = @estado_Libre)
begin
	--set @txtDetalle = FORMAT(@fechaActual - @fechaModificacion, 'd\.hh\:mm')

	select  
		@txtDetalle = Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + Cast(((sum(TotalMinutes) % 1440) / 60) as varchar) + ':' + Cast((sum(TotalMinutes) % 60) as varchar)
	from (select Abs(datediff(MINUTE, @fechaActual , @fechaModificacion)) as TotalMinutes) x

	set @esVencida = 0
end
else if(@estado = @estado_PendienteCobro)
begin

	select 
		@auto = ISNULL(automovil.Matricula, @defaultAuto),
		@fechaFin = MAX(ISNULL(extension.FechaFin, renta.FechaFin))
	from SotSchema.Rentas as renta
	left join SotSchema.AutomovilesRenta automovil
		on renta.Id = automovil.IdRenta and automovil.Activa = 1
	left join SotSchema.VentasRenta venta
		on renta.Id = venta.IdRenta and venta.Activo = 1
	left join SotSchema.Extensiones extension
		on venta.Id = extension.IdVentaRenta and extension.Activa = 1
	where renta.Activa = 1
	and renta.IdHabitacion = @idHabitacion
	group by automovil.Matricula

	if(@fechaFin is NULL)
	begin
		set @txtTipo = @auto
		set @esVencida = 0
		set @txtDetalle = ''
	end
	else
	begin
		--set @txtDetalle = FORMAT(@fechaFin - @fechaActual, 'd\.hh\:mm')

		select  
			@txtDetalle = Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + Cast(((sum(TotalMinutes) % 1440) / 60) as varchar) + ':' + Cast((sum(TotalMinutes) % 60) as varchar)
		from (select Abs(datediff(MINUTE, @fechaFin , @fechaActual)) as TotalMinutes) x

		if(@fechaFin <= @fechaActual)
		begin
			set @txtTipo = 'Vencida'
			set @esVencida = 1
		end
		else
		begin
			set @txtTipo = @auto
			set @esVencida = 0
		end
	end
end
else if(@estado = @estado_Preparada)
begin
	set @txtDetalle = 'Preparada'
	set @EsVencida = 0
end
else if(@estado = @estado_Reservada)
begin
	set @txtDetalle = 'Reservada'
	set @EsVencida = 0
end
else if(@estado = @estado_PreparadaReservada)
begin
	set @txtTipo = 'Reservación'
	set @esVencida = 0

	select @txtDetalle = CodigoReserva from SotSchema.Reservaciones
	where IdEstado = 2--Confirmada
	and IdHabitacion = @idHabitacion
end
else if(@estado = @estado_Bloqueada)
begin
	select @txtDetalle = m.Motivo from SotSchema.BloqueosHabitaciones m
	where m.Activo = 1
	and m.IdHabitacion = @idHabitacion

	set @esVencida = 0
end
else if(@estado = @estado_Ocupada)
begin
	select 
		@idRenta = renta.Id,
		@auto = ISNULL(automovil.Matricula, @defaultAuto),
		@fechaFin = MAX(ISNULL(extension.FechaFin, renta.FechaFin)),
		@tieneTarjetaV = case when renta.NumeroTarjeta is NULL then 0 else 1 end
	from SotSchema.Rentas as renta
	left join SotSchema.AutomovilesRenta automovil
		on renta.Id = automovil.IdRenta and automovil.Activa = 1
	left join SotSchema.VentasRenta venta
		on renta.Id = venta.IdRenta and venta.Activo = 1
	left join SotSchema.Extensiones extension
		on venta.Id = extension.IdVentaRenta and extension.Activa = 1
	where renta.Activa = 1
	and renta.IdHabitacion = @idHabitacion
	group by renta.Id, automovil.Matricula, renta.NumeroTarjeta

	if(@fechaFin is NULL)
	begin
		set @txtTipo = @auto
		set @esVencida = 0
		set @txtDetalle = ''
	end
	else
	begin
		--set @txtDetalle = FORMAT(@fechaFin - @fechaActual, 'd\.hh\:mm')

		select  
			@txtDetalle = Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + Cast(((sum(TotalMinutes) % 1440) / 60) as varchar) + ':' + Cast((sum(TotalMinutes) % 60) as varchar)
		from (select Abs(datediff(MINUTE, @fechaFin , @fechaActual)) as TotalMinutes) x

		if(@fechaFin <= @fechaActual)
		begin
			set @txtTipo = 'Vencida'
			set @esVencida = 1
		end
		else
		begin
			set @txtTipo = @auto


			select @estadoComanda = IdEstado from SotSchema.Comandas
			where IdRenta = @idRenta
			and Activo = 1
			and IdEstado in (1, 2, 3, 4)


			set @esVencida = 0
		end
	end
end
else if(@estado = @estado_Sucia)
begin

	select  
		@idTipoLimpieza = IdTipoLimpieza,
		@fechaTarea = FechaCreacion
	from SotSchema.TareasLimpieza
	where Activa = 1
	and IdHabitacion = @idHabitacion
	and IdEstado = @idEstadoTarea

	select 
		@minutosLimpieza = th.MinutosSucia ,
		@txtTipo = th.Descripcion
	from SotSchema.Habitaciones h
	left join SotSchema.TiposHabitacion th
		on h.IdTipoHabitacion = th.Id
	where h.Id = @idHabitacion

	--set @txtDetalle = FORMAT(@fechaActual - @fechaTarea, 'd\.hh\:mm')

	select  
		@txtDetalle = Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + Cast(((sum(TotalMinutes) % 1440) / 60) as varchar) + ':' + Cast((sum(TotalMinutes) % 60) as varchar)
	from (select Abs(datediff(MINUTE, @fechaActual, @fechaTarea)) as TotalMinutes) x

	set @esVencida = case when @fechaActual > dateadd(MINUTE, @minutosLimpieza, @fechaTarea) then 1 else 0 end
end
else if(@estado = @estado_Limpieza)
begin
	set @idEstadoTarea = 2

	select 
		@idTarea = Id, 
		@idTipoLimpieza = IdTipoLimpieza,
		@fechaTarea = FechaInicioLimpieza
		from SotSchema.TareasLimpieza
	where Activa = 1
	and IdHabitacion = @idHabitacion
	and IdEstado = @idEstadoTarea

	;with empleados as
	(
		select le.IdTareaLimpieza, le.Id, (e.Nombre + ' ' + e.ApellidoPaterno + ' ' + e.ApellidoMaterno) as NombreCompleto from SotSchema.LimpiezaEmpleados le
		inner join SotSchema.Empleados e
			on le.IdEmpleado = e.Id
		where le.Activo = 1
		and le.IdTareaLimpieza = @idTarea

	)
	select @txtTipo = (case 
							when COUNT(*) > 1 then
								Cast(COUNT(*) as varchar) + ' Recamareras'
							else
								NC.NombreCompleto
						  end)
	 from empleados e
	 cross apply (select top 1 e2.NombreCompleto from empleados e2 where e2.IdTareaLimpieza = e.IdTareaLimpieza order by e2.NombreCompleto) as NC
	 group by IdTareaLimpieza, nc.NombreCompleto

	select @minutosLimpieza = tl.Minutos from SotSchema.Habitaciones h
	left join SotSchema.TiemposLimpieza tl
		on h.IdTipoHabitacion = tl.IdTipoHabitacion
	where tl.Activa = 1
	and tl.IdTipoLimpieza = @idTipoLimpieza
	and h.Id = @idHabitacion

	--set @txtDetalle = FORMAT(@fechaActual - @fechaTarea, 'd\.hh\:mm')

	select  
		@txtDetalle = Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + Cast(((sum(TotalMinutes) % 1440) / 60) as varchar) + ':' + Cast((sum(TotalMinutes) % 60) as varchar)
	from (select Abs(datediff(MINUTE, @fechaActual, @fechaTarea)) as TotalMinutes) x

	set @esVencida = case when @fechaActual > dateadd(MINUTE, @minutosLimpieza, @fechaTarea) then 1 else 0 end
end
else if(@estado = @estado_Supervision)
begin
	set @idEstadoTarea = 3

	select  
		@idTipoLimpieza = IdTipoLimpieza,
		@fechaTarea = FechaInicioSupervision,
		@txtTipo = ISNULL(e.Nombre + ' ' + e.ApellidoPaterno + ' ' + e.ApellidoMaterno, '')
	from SotSchema.TareasLimpieza tl
	left join SotSchema.Empleados e
		on tl.IdEmpleadoSupervisa = e.Id
	where tl.Activa = 1
	and tl.IdHabitacion = @idHabitacion
	and tl.IdEstado = @idEstadoTarea

	--set @txtDetalle = FORMAT(@fechaActual - @fechaTarea, 'd\.hh\:mm')

	select  
		@txtDetalle = Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + Cast(((sum(TotalMinutes) % 1440) / 60) as varchar) + ':' + Cast((sum(TotalMinutes) % 60) as varchar)
	from (select Abs(datediff(MINUTE, @fechaActual, @fechaTarea)) as TotalMinutes) x

	set @esVencida = 0
end
else if(@estado = @estado_Mantenimiento)
begin
	select @txtDetalle = c.Concepto from SotSchema.Mantenimientos m
	inner join SotSchema.ConceptosMantenimiento c
		on m.IdConceptoMantenimiento = c.Id
	where m.Activa = 1
	and m.IdHabitacion = @idHabitacion

	set @esVencida = 0
end

select 
	ISNULL(@txtNumero, '') as NumeroHabitacion, 
	ISNULL(@txtTipo, '') as DatoBase, 
	ISNULL(@txtDetalle, '') as Detalle,
	ISNULL(@estado, 0) as IdEstado,
	ISNULL(@esVencida, 0) as EsVencida,
	@estadoComanda as IdEstadoComanda,
	ISNULL(@tieneTarjetaV, 0) as TieneTarjetaV
END
GO


