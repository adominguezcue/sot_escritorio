﻿USE [SOT]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerTareasMes]    Script Date: 01/11/2017 10:39:27 a. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerTareasMes]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerTareasMes]    Script Date: 01/11/2017 10:39:27 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date:	01 de noviembre de 2017
-- Description:	Retorna el Id y el Concepto de las tareas de mantenimiento
--				con fecha de inicio o de repetición dentro del mes solicitado
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerTareasMes] 
	-- Add the parameters for the stored procedure here
	@mes int, 
	@anio int,
	@idHabitacion int = 0
AS
BEGIN
    declare @mesCompleto datetime = CAST(
		CAST(@anio AS VARCHAR(4)) +
		RIGHT('0' + CAST(@mes AS VARCHAR(2)), 2) +
		'01'
	  AS DATETIME)

    /*
	   Periodicidades
		  Diariamente = 1,
		  Semanalmente = 2,
		  Mensualmente = 3,
		  Anualmente = 4
    */

    if(@idHabitacion = 0)

	   select tm.Id, tm.FechaSiguiente, cm.Concepto from SotSchema.TareasMantenimiento tm
	   inner join SotSchema.ConceptosMantenimiento cm
		  on tm.IdConceptoMantenimiento = cm.Id
	   where DATEPART(month, FechaSiguiente) = DATEPART(month, @mesCompleto)
	   and DATEPART(YEAR, FechaSiguiente) = DATEPART(YEAR, @mesCompleto)
	   and Activa = 1
	   --union
	   --select tm.Id, rtm.FechaInicio, cm.Concepto  from SotSchema.RepeticionesTareasMantenimiento rtm
	   --inner join SotSchema.TareasMantenimiento tm
		  --on rtm.IdTareaMantenimiento = tm.Id
	   --inner join SotSchema.ConceptosMantenimiento cm
		  --on tm.IdConceptoMantenimiento = cm.Id
	   --where DATEPART(month, rtm.FechaInicio) = DATEPART(month, @mesCompleto)
	   --and DATEPART(YEAR, rtm.FechaInicio) = DATEPART(YEAR, @mesCompleto)
	   --and rtm.Activa = 1
	   --and tm.Activa = 1
    else
    	   select tm.Id, tm.FechaSiguiente, cm.Concepto from SotSchema.TareasMantenimiento tm
	   inner join SotSchema.ConceptosMantenimiento cm
		  on tm.IdConceptoMantenimiento = cm.Id
	   where DATEPART(month, FechaSiguiente) = DATEPART(month, @mesCompleto)
	   and DATEPART(YEAR, FechaSiguiente) = DATEPART(YEAR, @mesCompleto)
	   and tm.IdHabitacion = @idHabitacion
	   and tm.Activa = 1
	   --union
	   --select tm.Id, rtm.FechaInicio, cm.Concepto  from SotSchema.RepeticionesTareasMantenimiento rtm
	   --inner join SotSchema.TareasMantenimiento tm
		  --on rtm.IdTareaMantenimiento = tm.Id
	   --inner join SotSchema.ConceptosMantenimiento cm
		  --on tm.IdConceptoMantenimiento = cm.Id
	   --where DATEPART(month, rtm.FechaInicio) = DATEPART(month, @mesCompleto)
	   --and DATEPART(YEAR, rtm.FechaInicio) = DATEPART(YEAR, @mesCompleto)
	   --and tm.IdHabitacion = @idHabitacion
	   --and rtm.Activa = 1
	   --and tm.Activa = 1

END

GO


