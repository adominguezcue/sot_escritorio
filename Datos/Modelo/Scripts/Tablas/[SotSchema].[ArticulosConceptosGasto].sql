﻿-- Creating table 'ArticulosConceptosGasto'
CREATE TABLE [SotSchema].[ArticulosConceptosGasto] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdConceptoGasto] int  NOT NULL,
    [CodigoArticulo] nvarchar(max)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'ArticulosConceptosGasto'
ALTER TABLE [SotSchema].[ArticulosConceptosGasto]
ADD CONSTRAINT [PK_ArticulosConceptosGasto]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [IdConceptoGasto] in table 'ArticulosConceptosGasto'
ALTER TABLE [SotSchema].[ArticulosConceptosGasto]
ADD CONSTRAINT [FK_ConceptoGastoArticuloConceptoGasto]
    FOREIGN KEY ([IdConceptoGasto])
    REFERENCES [SotSchema].[ConceptosGastos]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConceptoGastoArticuloConceptoGasto'
CREATE INDEX [IX_FK_ConceptoGastoArticuloConceptoGasto]
ON [SotSchema].[ArticulosConceptosGasto]
    ([IdConceptoGasto]);
GO