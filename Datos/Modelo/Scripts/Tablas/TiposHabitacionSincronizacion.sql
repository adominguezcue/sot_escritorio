﻿IF OBJECT_ID(N'[SotSchema].[TiposHabitacionSincronizacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[TiposHabitacionSincronizacion];
GO
IF OBJECT_ID(N'[SotSchema].[PreciosTipoHabitacionSincronizacion]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion];
GO

-- Creating table 'TiposHabitacionSincronizacion'
CREATE TABLE [SotSchema].[TiposHabitacionSincronizacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Clave] nvarchar(max)  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Fecha] datetime  NOT NULL,
    [PorcentajeIVA] decimal(19,4)  NOT NULL,
    [IdMovimiento] int  NOT NULL,
    [EsErrorSubida] bit  NOT NULL,
    [ErrorUltimoIntento] nvarchar(max)  NULL,
    [Sincronizado] bit  NOT NULL
);
GO

-- Creating table 'PreciosTipoHabitacionSincronizacion'
CREATE TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdTipoSincronizacion] int  NOT NULL,
    [IdTarifa] int  NOT NULL,
    [Precio] decimal(19,4)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'TiposHabitacionSincronizacion'
ALTER TABLE [SotSchema].[TiposHabitacionSincronizacion]
ADD CONSTRAINT [PK_TiposHabitacionSincronizacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PreciosTipoHabitacionSincronizacion'
ALTER TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion]
ADD CONSTRAINT [PK_PreciosTipoHabitacionSincronizacion]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [IdTipoSincronizacion] in table 'PreciosTipoHabitacionSincronizacion'
ALTER TABLE [SotSchema].[PreciosTipoHabitacionSincronizacion]
ADD CONSTRAINT [FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion]
    FOREIGN KEY ([IdTipoSincronizacion])
    REFERENCES [SotSchema].[TiposHabitacionSincronizacion]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion'
CREATE INDEX [IX_FK_TipoHabitacionSincronizacionPrecioTipoHabitacionSincronizacion]
ON [SotSchema].[PreciosTipoHabitacionSincronizacion]
    ([IdTipoSincronizacion]);
GO

