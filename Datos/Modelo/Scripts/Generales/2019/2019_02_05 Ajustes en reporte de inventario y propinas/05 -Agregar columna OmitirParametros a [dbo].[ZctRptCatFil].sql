﻿alter table [dbo].[ZctRptCatFil]
add OmitirParametros BIT
GO

update [dbo].[ZctRptCatFil]
set OmitirParametros = 0
where OmitirParametros is NULL
GO
