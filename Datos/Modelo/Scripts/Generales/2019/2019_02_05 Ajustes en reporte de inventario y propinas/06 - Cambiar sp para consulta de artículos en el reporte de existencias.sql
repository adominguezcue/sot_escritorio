﻿update campo
set campo.sSPName_Fil = 'SP_ZctCatArt_Existencias_Agrupadas',
campo.sVariables_Fil = '',
campo.OmitirParametros = 1
FROM [dbo].[ZctRptCatFil] campo
inner join dbo.ZctRptCatRpt reporte
	on campo.IdCat_Rpt = reporte.IdCat_Rpt
where reporte.Nom_Rpt = 'INVENTARIO ACTUAL'
and campo.sSPName_Fil = 'SP_ZctCatArt_Existencias_Agrupadas'