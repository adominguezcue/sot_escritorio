﻿DROP PROCEDURE [dbo].[SP_ZctCatArt_Existencias_Agrupadas]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctCatArt_Existencias_Agrupadas]    Script Date: 05/02/2019 12:33:50 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SP_ZctCatArt_Existencias_Agrupadas]
AS
BEGIN
	SELECT 
	art.Cod_Art AS Codigo, 
	art.Desc_Art AS Descripción, 
	art.Prec_Art AS Precio, 
	art.Cos_Art AS Costo, 
	tipo.Desc_TpArt AS Tipo, 
	isnull(SUM(axa.Exist_Art), 0) AS 'Existencia general',
	cat.Desc_Dpto AS Categoría, 
	lin.Desc_Linea AS Subcategoría
	FROM ZctCatArt art LEFT OUTER JOIN
			ZctCatLinea lin ON art.Cod_Linea = lin.Cod_Linea LEFT OUTER JOIN
			ZctCatDpto cat ON lin.Cod_Dpto = cat.Cod_Dpto LEFT OUTER JOIN
			ZctArtXAlm axa ON art.Cod_Art = axa.Cod_Art LEFT OUTER JOIN
			ZctCatTpArt tipo ON art.Cod_TpArt = tipo.Cod_TpArt 
	WHERE (art.desactivar IS NULL) OR
			(art.desactivar = 0)
	GROUP BY art.Cod_Art, art.Desc_Art, art.Prec_Art, art.Cos_Art, tipo.Desc_TpArt, cat.Desc_Dpto, lin.Desc_Linea
END				
GO



