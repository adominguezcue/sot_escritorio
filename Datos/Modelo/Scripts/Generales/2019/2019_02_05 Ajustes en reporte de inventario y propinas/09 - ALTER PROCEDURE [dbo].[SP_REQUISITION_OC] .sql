﻿
/****** Object:  StoredProcedure [dbo].[SP_REQUISITION_OC]    Script Date: 05/02/2019 06:48:54 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_REQUISITION_OC] 
	@Folio int,
	@Cod_OC int OUTPUT
AS
BEGIN
	DECLARE 		
	@TpConsulta Int,
	@Cod_Prov int,
	@Fch_OC smalldatetime,
	@FchAplMov smalldatetime,
	@Fac_OC varchar(20),
	@FchFac_OC smalldatetime,	
	@CodDet_Oc int,
	@Cod_Art varchar(20),
	@Ctd_Art int,
	@Cos_Art numeric(18, 2),
	@CtdStdDet_OC INT,
	@Cat_Alm int,
	@omitir_iva bit

	--OBTIENE EL FOLIO
	UPDATE ZctCatFolios SET  Consc_Folio = Consc_Folio + 1 where  Cod_Folio = 'OC'	
	Select  @Cod_OC= Consc_Folio FROM ZctCatFolios where  Cod_Folio = 'OC'

	SELECT @TpConsulta ='2' ,@Cod_Prov = NULL ,@Fch_OC=GETDATE() ,@FchAplMov = GETDATE() ,@Fac_OC = NULL ,@FchFac_OC  = GETDATE() , @Cat_Alm = cod_alm
	FROM  ZctRequisition
	WHERE folio = @Folio
	--GRABA EL ENCABEZADO
	EXECUTE [SP_ZctEncOC] @TpConsulta ,@Cod_OC ,@Cod_Prov ,@Fch_OC ,@FchAplMov ,@Fac_OC ,@FchFac_OC 

   Declare detalles cursor for
    SELECT art.Cod_Art,  amount, price, 0, GETDATE(), isnull(art.OmitirIVA, 0)
	FROM  ZctRequisitionDetail det
	left join ZctCatArt art
		on det.cod_art = art.Cod_Art
	WHERE folio_requisition = @Folio
	OPEN detalles 
	fetch next from detalles into 
	 @Cod_Art , @Ctd_Art , @Cos_Art , @CtdStdDet_OC , @FchAplMov, @omitir_iva


	set @CodDet_Oc = 0
	WHILE @@FETCH_STATUS = 0
	BEGIN

		--SET @CodDet_Oc = @CodDet_Oc +1
	    EXECUTE [SP_ZctDetOC]  @TpConsulta , @Cod_OC	, @CodDet_Oc , @Cod_Art , @Ctd_Art , @Cos_Art , @CtdStdDet_OC , @FchAplMov , @Cat_Alm, @omitir_iva


		fetch next from detalles into 
		@Cod_Art , @Ctd_Art , @Cos_Art , @CtdStdDet_OC , @FchAplMov, @omitir_iva 
	END

	CLOSE detalles
	DEALLOCATE detalles

	UPDATE ZctRequisition SET update_web = null , estatus = 'ORDEN DE COMPRA GENERADA', Cod_OC = @Cod_OC where folio = @Folio
	return @Cod_OC
END

