﻿
/****** Object:  StoredProcedure [dbo].[SP_ZctRptCatRpt]    Script Date: 31/01/2019 01:08:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Arturo Hernandez
-- Create date: 12-02-2006
-- Description:	Catálogo de Marcas
-- =============================================
ALTER PROCEDURE [dbo].[SP_ZctRptCatRpt]
		@IdCat_Rpt int

AS

		

BEGIN
SELECT
reporte.IdCat_Rpt, 
reporte.Nom_Rpt, 
reporte.Desc_Rpt, 
reporte.Path_Rpt, 
reporte.idCatCatego_Rpt, 
campo.Desc_Fil, 
campo.IdCat_TpRpt, 
campo.Sql_Fil, 
campo.Show_Fil, 
campo.sSPName_Fil, 
campo.sVariables_Fil,
campo.OmitirParametros
FROM ZctRptCatRpt reporte 
LEFT OUTER JOIN ZctRptCatFil campo 
	ON reporte.IdCat_Rpt = campo.IdCat_Rpt
 WHERE reporte.IdCat_Rpt = @IdCat_Rpt

END












