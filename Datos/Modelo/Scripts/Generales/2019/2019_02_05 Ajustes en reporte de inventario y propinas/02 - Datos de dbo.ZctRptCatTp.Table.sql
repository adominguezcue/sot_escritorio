
SET IDENTITY_INSERT [dbo].[ZctRptCatTp] ON 

INSERT [dbo].[ZctRptCatTp] ([IdCat_TpRpt], [Nom_TpRpt], [Mask_TpRpt]) VALUES (1, N'TEXTO', N'######')
INSERT [dbo].[ZctRptCatTp] ([IdCat_TpRpt], [Nom_TpRpt], [Mask_TpRpt]) VALUES (2, N'NUMERO', N'0000000')
INSERT [dbo].[ZctRptCatTp] ([IdCat_TpRpt], [Nom_TpRpt], [Mask_TpRpt]) VALUES (3, N'MONEDA', N'$00000.00')
INSERT [dbo].[ZctRptCatTp] ([IdCat_TpRpt], [Nom_TpRpt], [Mask_TpRpt]) VALUES (4, N'FECHA', N'##/##/####')
INSERT [dbo].[ZctRptCatTp] ([IdCat_TpRpt], [Nom_TpRpt], [Mask_TpRpt]) VALUES (5, N'PARAMETRO', N'#######')
INSERT [dbo].[ZctRptCatTp] ([IdCat_TpRpt], [Nom_TpRpt], [Mask_TpRpt]) VALUES (6, N'PARAMETROSQL', N'')
SET IDENTITY_INSERT [dbo].[ZctRptCatTp] OFF
