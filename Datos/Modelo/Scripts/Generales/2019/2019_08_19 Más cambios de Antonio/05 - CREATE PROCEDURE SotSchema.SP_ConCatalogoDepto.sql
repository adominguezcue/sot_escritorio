IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConCatalogoDepto') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConCatalogoDepto
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene los datos de catalogo de almacen 
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 22/07/2019
--Descripcion: Obtiene el catalogo de Departamento
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConCatalogoDepto
@vc_TipoArticulos nvarchar (3)
,@vc_Deptos		  nvarchar (200)
AS
DECLARE
@vcMensajeerror			nvarchar (150)
,@vicontadordepto		int
,@vicontadordeptoaux	int 
DECLARE @vt_tabladepto	table (id int not null identity(1,1), depto int)
DECLARE @vt_tablarespuesta table (id int not null identity(1,1), codigo int, descripcion nvarchar(50))
SET NOCOUNT ON
IF EXISTS (SELECT TOP 1 Cod_Dpto FROM dbo.ZctCatDpto WITH (NOLOCK)) 
	BEGIN
	IF (LEN (@vc_Deptos )>0) 
	BEGIN
	INSERT INTO @vt_tabladepto (depto) (SELECT value FROM SotSchema.DividirCadena (@vc_Deptos, ',') WHERE RTRIM(value) <> '' )
	IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tabladepto' GOTO ERROR END 
		SET @vicontadordepto= (SELECT COUNT(*) FROM @vt_tabladepto)
		SET @vicontadordeptoaux=1
		WHILE @vicontadordeptoaux<=@vicontadordepto
		BEGIN
			IF EXISTS(SELECT depto.Cod_Dpto
						from dbo.ZctCatDpto depto   WITH (NOLOCK)
						INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON depto.Cod_Dpto=articulo.Cod_Dpto
						WHERE articulo.Cod_TpArt=@vc_TipoArticulos AND Editable=1
						AND depto.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
			BEGIN
			INSERT INTO @vt_tablarespuesta
						(codigo,descripcion)
						(SELECT DISTINCT depto.Cod_Dpto,depto.Desc_Dpto
						from dbo.ZctCatDpto depto   WITH (NOLOCK)
						INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON depto.Cod_Dpto=articulo.Cod_Dpto
						WHERE articulo.Cod_TpArt=@vc_TipoArticulos AND depto.Editable=1
						AND depto.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
			IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tablarespuesta' GOTO ERROR END 
			END
			SET @vicontadordeptoaux=@vicontadordeptoaux+1
		END
	END
	ELSE 
	BEGIN
		INSERT INTO @vt_tablarespuesta
		(codigo,descripcion)
		(SELECT DISTINCT depto.Cod_Dpto,depto.Desc_Dpto
		from dbo.ZctCatDpto depto   WITH (NOLOCK)
		INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON depto.Cod_Dpto=articulo.Cod_Dpto
		WHERE articulo.Cod_TpArt=@vc_TipoArticulos AND Editable=1)
		IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tablarespuesta' GOTO ERROR END 
	END
	END
SELECT codigo,descripcion FROM  @vt_tablarespuesta order by descripcion
SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN

GO