IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConCatalogoLinea') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConCatalogoLinea
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene los datos de catalogo de almacen 
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 22/07/2019
--Descripcion: Obtiene el catalogo de almacen
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConCatalogoLinea
@vc_TipoArticulos nvarchar (3)
,@vc_Deptos		  nvarchar (200)
AS
DECLARE
@vcMensajeerror			nvarchar (150)
,@vicontadordepto       int
,@vicontadordeptoaux	int
DECLARE @vt_tabladepto	table (id int not null identity(1,1), depto int)
DECLARE @vt_tablarespuesta table (id int not null identity(1,1), codigo int, descripcion nvarchar(50), depto int)
SET NOCOUNT ON
IF EXISTS (SELECT TOP 1 Cod_Linea FROM dbo.ZctCatLinea WITH (NOLOCK)) 
	BEGIN
	IF (LEN ( @vc_Deptos )>0) 
	BEGIN
		INSERT INTO @vt_tabladepto (depto) (SELECT value FROM SotSchema.DividirCadena (@vc_Deptos, ',') WHERE RTRIM(value) <> '' )
		IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tabladepto' GOTO ERROR END 
		SET @vicontadordepto= (SELECT COUNT(*) FROM @vt_tabladepto)
		SET @vicontadordeptoaux=1
		WHILE @vicontadordeptoaux<=@vicontadordepto
		BEGIN
			IF EXISTS(SELECT linea.Cod_Dpto FROM dbo.ZctCatLinea linea WITH (NOLOCK) 
						inner join dbo.ZctCatArt articulo WITH(NOLOCK) ON linea.Cod_Linea=articulo.Cod_Linea
						WHERE articulo.Cod_TpArt=@vc_TipoArticulos
						AND linea.Cod_Dpto=(SELECT depto FROM @vt_tabladepto where id= @vicontadordeptoaux))
			BEGIN	
				INSERT INTO @vt_tablarespuesta
				(codigo,descripcion,depto)
				(SELECT DISTINCT linea.Cod_Linea,linea.Desc_Linea, linea.Cod_Dpto FROM dbo.ZctCatLinea linea WITH (NOLOCK) 
						inner join dbo.ZctCatArt articulo WITH(NOLOCK) ON linea.Cod_Linea=articulo.Cod_Linea
						WHERE articulo.Cod_TpArt=@vc_TipoArticulos
						AND linea.Cod_Dpto=(SELECT depto FROM @vt_tabladepto where id= @vicontadordeptoaux))
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tablarespuesta' GOTO ERROR END 
			END
		 SET @vicontadordeptoaux=@vicontadordeptoaux+1
		END
	END
	ELSE
	BEGIN
		INSERT INTO @vt_tablarespuesta 
		(codigo,descripcion, depto)
		(SELECT DISTINCT linea.Cod_Linea, linea.Desc_Linea, linea.Cod_Dpto 
		FROM dbo.ZctCatLinea linea WITH (NOLOCK) 
		inner join dbo.ZctCatArt articulo WITH(NOLOCK) ON linea.Cod_Linea=articulo.Cod_Linea
		WHERE articulo.Cod_TpArt=@vc_TipoArticulos)
		IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tablarespuesta' GOTO ERROR END 
	END
	END

SELECT codigo,descripcion,depto FROM @vt_tablarespuesta order by descripcion
SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN

GO