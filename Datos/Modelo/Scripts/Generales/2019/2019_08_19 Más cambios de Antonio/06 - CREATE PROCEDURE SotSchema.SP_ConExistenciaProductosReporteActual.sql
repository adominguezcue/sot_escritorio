IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConExistenciaProductosReporteActual') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConExistenciaProductosReporteActual
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene los datos de catalogo de almacen 
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 22/07/2019
--Descripcion: Obtine los articulos con la existencia para el reporte actual
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConExistenciaProductosReporteActual
@vc_codigosalmacen		nvarchar (200)
,@vc_Codigosarticulos	nvarchar (200)
,@vc_codigosLineas		nvarchar (200) 
,@vc_codigodeptos		nvarchar (200)
AS
DECLARE
@vcMensajeerror			nvarchar (150)
,@vicontadoralmacen		int
,@vicontadoralmacenaux  int
,@vicontadorarticulo	int
,@vicontadorarticuloaux	int
,@vicontadorlinea		int
,@vicontadorlineaaux	int
,@vicontadordepto		int
,@vicontadordeptoaux	int
,@vctiporarticulo		nvarchar(3)
DECLARE @Vt_TablaRespuesta TABLE(Id int NOT NULL identity(1,1)
,CodigoAlmacen int
,Desc_Almacen nvarchar(100)
,Codigolinea int
,Desc_Linea nvarchar(100)
,CodigoDepto int
,Desc_Depto nvarchar(100)
,CodigoProd nvarchar(20)
,Desc_prod	nvarchar(100)
,existencias dec
,costopromedio money)
DECLARE @vt_TablaAlmacen Table (Id int NOT NULL identity(1,1),almacen int)
DECLARE @vt_tablaArticulo table (Id int NOT NULL identity(1,1),articulo nvarchar(20))
DECLARE @vt_tablaLinea	 table (id int not null identity(1,1), linea  int)
DECLARE @vt_tabladepto	table (id int not null identity(1,1), depto int)
SET NOCOUNT ON
SET @vctiporarticulo='I'
IF (LEN ( @vc_codigosalmacen )>0) 
	BEGIN
	INSERT INTO @vt_TablaAlmacen (almacen) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosAlmacen, ',') WHERE RTRIM(value) <> '' )
	IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vt_TablaAlmacen' GOTO ERROR END 
	SET @vicontadoralmacen= (SELECT COUNT(*) FROM @vt_TablaAlmacen)
	SET @vicontadoralmacenaux =1
	WHILE @vicontadoralmacenaux<=@vicontadoralmacen
	BEGIN
		IF(LEN (@vc_Codigosarticulos)>0)
		BEGIN
		INSERT INTO @vt_tablaArticulo (articulo) (SELECT value FROM SotSchema.DividirCadena (@vc_Codigosarticulos, ',') WHERE RTRIM(value) <> '' )
		IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vt_tablaArticulo' GOTO ERROR END 
		SET @vicontadorarticulo= (SELECT COUNT(*) FROM @vt_tablaArticulo)
		SET @vicontadorarticuloaux= 1
		WHILE @vicontadorarticuloaux<=@vicontadorarticulo
		BEGIN
			IF (LEN (@vc_codigodeptos)>0)
			BEGIN
			INSERT INTO @vt_tabladepto (depto) (SELECT value FROM SotSchema.DividirCadena (@vc_codigodeptos, ',') WHERE RTRIM(value) <> '' )
			IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vt_tabladepto' GOTO ERROR END 
			SET @vicontadordepto= (SELECT COUNT(*) FROM @vt_tabladepto)
			SET @vicontadordeptoaux=1
			WHILE @vicontadordeptoaux<=@vicontadordepto
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
						IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
									AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
									AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
									BEGIN
										INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
													AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
													AND articulo.Cod_Dpto= (SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
													IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
									AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
									BEGIN
										INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
													AND articulo.Cod_Dpto= (SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
													IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
												END
				END
				SET @vicontadordeptoaux=@vicontadordeptoaux+1
				END
			END
			ELSE
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
									IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
									AND articulo.Cod_Linea=(SELECT linea FROM  @vt_tablaLinea WHERE id=@vicontadorlineaaux))
									BEGIN
										INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
													AND articulo.Cod_Linea=(SELECT linea FROM  @vt_tablaLinea WHERE id=@vicontadorlineaaux))
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
										END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux))
									BEGIN
										INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux))
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
										END
				END
			END
			SET @vicontadorarticuloaux=@vicontadorarticuloaux+1
			END
		END
		ELSE
		BEGIN
			IF(LEN (@vc_codigodeptos)>0)
			BEGIN
			INSERT INTO @vt_tabladepto (depto) (SELECT value FROM SotSchema.DividirCadena (@vc_codigodeptos, ',') WHERE RTRIM(value) <> '' )
			IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tabladepto' GOTO ERROR END 
			SET @vicontadordepto= (SELECT COUNT(*) FROM @vt_tabladepto)
			SET @vicontadordeptoaux=1
			WHILE @vicontadordeptoaux<=@vicontadordepto
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
								INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
								INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
								INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
								INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
								WHERE articulo.Cod_TpArt=@vctiporarticulo
								AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
								AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
								AND articulo.Cod_Dpto= (SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
						BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
													AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
													AND articulo.Cod_Dpto= (SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
					 SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END 
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
								INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
								INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
								INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
								INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
								WHERE articulo.Cod_TpArt=@vctiporarticulo
								AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
								AND articulo.Cod_Dpto= (SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
						BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
													AND articulo.Cod_Dpto= (SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
				END
				SET @vicontadordeptoaux=@vicontadordeptoaux+1
			END
			END
			ELSE
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
					IF EXISTS(SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
								INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
								INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
								INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
								INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
								WHERE articulo.Cod_TpArt=@vctiporarticulo
								AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
								AND articulo.Cod_Linea=(SELECT linea FROM  @vt_tablaLinea WHERE id=@vicontadorlineaaux))
						BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux)
													AND articulo.Cod_Linea=(SELECT linea FROM  @vt_tablaLinea WHERE id=@vicontadorlineaaux))
									IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
				END
				ELSE
				BEGIN
					IF EXISTS(SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
								INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
								INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
								INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
								INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
								WHERE articulo.Cod_TpArt=@vctiporarticulo
								AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux))
						BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Alm=(SELECT almacen FROM @vt_TablaAlmacen where Id=@vicontadoralmacenaux))
									IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
							END
				END
			END
		END
		SET @vicontadoralmacenaux=@vicontadoralmacenaux+1
		END
	END
ELSE
	BEGIN
		IF(LEN (@vc_Codigosarticulos)>0)
		BEGIN
		INSERT INTO @vt_tablaArticulo (articulo) (SELECT value FROM SotSchema.DividirCadena (@vc_Codigosarticulos, ',') WHERE RTRIM(value) <> '' )
		IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tablaArticulo' GOTO ERROR END 
		SET @vicontadorarticulo= (SELECT COUNT(*) FROM @vt_tablaArticulo)
		SET @vicontadorarticuloaux= 1
		WHILE @vicontadorarticuloaux<=@vicontadorarticulo
		BEGIN
			IF (LEN (@vc_codigodeptos)>0)
			BEGIN
			INSERT INTO @vt_tabladepto (depto) (SELECT value FROM SotSchema.DividirCadena (@vc_codigodeptos, ',') WHERE RTRIM(value) <> '' )
			IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tabladepto' GOTO ERROR END 
			SET @vicontadordepto= (SELECT COUNT(*) FROM @vt_tabladepto)
			SET @vicontadordeptoaux=1
			WHILE @vicontadordeptoaux<=@vicontadordepto
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
									AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
									AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
									BEGIN
									INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
													AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
													AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
												END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
									AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
									BEGIN
									INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
													AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
										IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
										END

				END
				SET @vicontadordeptoaux=@vicontadordeptoaux+1
			END
			END
			ELSE
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
									AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux))
						BEGIN
						INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux)
													AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux))
							IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
				END
				ELSE
				BEGIN
					IF EXISTS(SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux))
						BEGIN
						INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND artixalm.Cod_Art=(SELECT articulo FROM @vt_tablaArticulo WHERE Id=@vicontadorarticuloaux))
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
				END
			END
			SET @vicontadorarticuloaux=@vicontadorarticuloaux+1
		END
		END
		ELSE
		BEGIN
			IF (LEN (@vc_codigodeptos)>0)
			BEGIN
			INSERT INTO @vt_tabladepto (depto) (SELECT value FROM SotSchema.DividirCadena (@vc_codigodeptos, ',') WHERE RTRIM(value) <> '' )
			IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tabladepto' GOTO ERROR END 
			SET @vicontadordepto= (SELECT COUNT(*) FROM @vt_tabladepto)
			SET @vicontadordeptoaux=1
			WHILE @vicontadordeptoaux<=@vicontadordepto
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
									AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
						BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux)
													AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
						BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND articulo.Cod_Dpto=(SELECT depto FROM @vt_tabladepto WHERE id=@vicontadordeptoaux))
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
						END
				END
				SET @vicontadordeptoaux=@vicontadordeptoaux+1
			END
			END
			ELSE
			BEGIN
				IF(LEN (@vc_codigosLineas)>0)
				BEGIN
				INSERT INTO @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_codigosLineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla @vt_tablaLinea' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux=1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
				IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo
									AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea WHERE id=@vicontadorlineaaux))
							BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo
													AND articulo.Cod_Linea=(SELECT linea FROM @vt_tablaLinea where id=@vicontadorlineaaux))
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
							END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT artixalm.Cod_Art FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
									INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
									INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
									INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
									WHERE articulo.Cod_TpArt=@vctiporarticulo)
							BEGIN
							INSERT INTO @Vt_TablaRespuesta 
													(CodigoAlmacen
													,Desc_Almacen
													,Codigolinea
													,Desc_Linea
													,CodigoDepto
													,Desc_Depto
													,CodigoProd
													,Desc_prod
													,existencias
													,costopromedio)
													(SELECT 
													artixalm.Cod_Alm
													,almacen.Desc_CatAlm
													,linea.Cod_Linea
													,linea.Desc_Linea
													,depto.Cod_Dpto
													,depto.Desc_Dpto
													,artixalm.Cod_Art
													,articulo.Desc_Art
													,artixalm.Exist_Art
													,artixalm.CostoProm_Art
													FROM dbo.ZctArtXAlm artixalm WITH(NOLOCK)
													INNER JOIN dbo.ZctCatArt articulo WITH(NOLOCK) ON artixalm.Cod_Art=articulo.Cod_Art
													INNER JOIN dbo.ZctCatAlm almacen WITH(NOLOCK)  ON artixalm.Cod_Alm=almacen.Cod_Alm
													INNER JOIN dbo.ZctCatLinea linea WITH(NOLOCK)  ON articulo.Cod_Linea=linea.Cod_Linea
													INNER JOIN dbo.ZctCatDpto  depto WITH(NOLOCk)  ON articulo.Cod_Dpto=depto.Cod_Dpto
													WHERE articulo.Cod_TpArt=@vctiporarticulo)
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
							END
				END
			END 
		END
	END
SELECT DISTINCT CodigoProd,
CodigoAlmacen
,Desc_Almacen
,Codigolinea
,Desc_Linea
,CodigoDepto
,Desc_Depto
,Desc_prod
,existencias
,costopromedio
FROM @Vt_TablaRespuesta ORDER BY CodigoAlmacen
SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN
