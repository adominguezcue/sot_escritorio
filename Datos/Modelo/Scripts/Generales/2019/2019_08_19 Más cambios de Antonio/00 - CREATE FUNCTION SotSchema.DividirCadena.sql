﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Inline Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date: 2019-08-21
-- Description:	Función para separa una cadena de texto, se usará cuando no sea posible tener un nivel de compatibilidad igual o mayor a 130 para usar string_split
-- =============================================
CREATE FUNCTION SotSchema.DividirCadena
(	
	@texto varchar(max),
	@separador varchar(1)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Split.a.value('.', 'NVARCHAR(MAX)') value
	FROM
	(
		SELECT CAST('<X>'+REPLACE(@texto, @separador, '</X><X>')+'</X>' AS XML) AS String
	) AS A
	CROSS APPLY String.nodes('/X') AS Split(a)
)
GO
