IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_CoComandasPorRenta') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_CoComandasPorRenta
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene los datos de las habitaciones y costos por matricula
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 11/07/2019
--Descripcion: Comandas Por Renta
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_CoComandasPorRenta
	@viRentaId int 
AS
DECLARE
	 @vcMensajeerror			nvarchar (150)
SET NOCOUNT ON

	IF EXISTS (SELECT id FROM SotSchema.Rentas WITH(NOLOCK) WHERE Id=@viRentaId)
	BEGIN
		IF EXISTS (SELECT comand.id FROM SotSchema.Comandas comand WITH (NOLOCK) 
					INNER JOIN SotSchema.Rentas rentas WITH (NOLOCK) ON comand.IdRenta=rentas.Id
					WHERE rentas.Id=@viRentaId)
		BEGIN
			SELECT  COUNT(comand.id) as comandas ,SUM(comand.ValorConIVA) as total ,rentas.Id as idrenta  FROM SotSchema.Comandas comand WITH (NOLOCK) 
					INNER JOIN SotSchema.Rentas rentas WITH (NOLOCK) ON comand.IdRenta=rentas.Id
					WHERE rentas.Id=@viRentaId GROUP BY rentas.Id
		END
	END
SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN

GO