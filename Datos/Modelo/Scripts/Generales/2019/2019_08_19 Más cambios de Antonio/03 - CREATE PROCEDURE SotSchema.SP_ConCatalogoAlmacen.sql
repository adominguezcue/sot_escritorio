IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConCatalogoAlmacen') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConCatalogoAlmacen
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene los datos de catalogo de almacen 
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 22/07/2019
--Descripcion: Obtiene el catalogo de almacen
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConCatalogoAlmacen
@vc_TipoArticulos nvarchar(3)
,@vc_Deptos		  nvarchar(200)
,@vc_lineas		  nvarchar(200)
AS
DECLARE
@vcMensajeerror			nvarchar (150)
,@vicontadordepto       int
,@vicontadordeptoaux	int
,@vicontadorlinea		int
,@vicontadorlineaaux	int
DECLARE @vt_tablarespuesta table (id int not null identity(1,1), codigo int,descripcion nvarchar(50))
DECLARE @vt_tabladepto	table (id int not null identity(1,1), depto int)
DECLARE @vt_tablaLinea	 table (id int not null identity(1,1), linea  int)
SET NOCOUNT ON
IF EXISTS (SELECT TOP 1 Cod_Alm FROM dbo.ZctCatAlm WITH (NOLOCK)) 
	BEGIN
		IF (LEN ( @vc_Deptos )>0)
		BEGIN
			INSERT INTO @vt_tabladepto (depto) (SELECT value FROM SotSchema.DividirCadena (@vc_Deptos, ',') WHERE RTRIM(value) <> '' )
			IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tabladepto' GOTO ERROR END 
			SET @vicontadordepto= (SELECT COUNT(*) FROM @vt_tabladepto)
			SET @vicontadordeptoaux=1
			WHILE @vicontadordeptoaux<=@vicontadordepto
			BEGIN
				IF(LEN (@vc_lineas)>0)
				BEGIN	
					INSERT @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_lineas, ',') WHERE RTRIM(value) <> '' )
					IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tablaLinea' GOTO ERROR END 
					SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
					SET @vicontadorlineaaux =1
					WHILE @vicontadorlineaaux<=@vicontadorlinea
					BEGIN
						IF EXISTS(select  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
								  FROM dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
									INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
									INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
									INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
									WHERE articulos.Cod_TpArt=@vc_TipoArticulos
									AND articulos.Cod_Linea= (SELECT linea FROM @vt_tablaLinea where id=@vicontadorlineaaux)
									AND  articulos.Cod_Dpto = (SELECT depto FROM @vt_tabladepto where id=@vicontadordeptoaux))
							BEGIN
								INSERT INTO @vt_tablarespuesta
								(codigo,descripcion)
								SELECT  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
								FROM dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
								INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
								INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
								INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
								INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
								WHERE articulos.Cod_TpArt=@vc_TipoArticulos
								AND articulos.Cod_Linea= (SELECT linea FROM @vt_tablaLinea where id=@vicontadorlineaaux
								AND articulos.Cod_Dpto = (SELECT depto FROM @vt_tabladepto where id=@vicontadordeptoaux))
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tablarespuesta' GOTO ERROR END 
							END
						SET @vicontadorlineaaux=@vicontadorlineaaux+1
					END
				END
				ELSE
				BEGIN
					IF EXISTS(select  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
							FROM dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
							INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
							INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
							INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
							INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
							WHERE articulos.Cod_TpArt=@vc_TipoArticulos
							AND  articulos.Cod_Dpto = (SELECT depto FROM @vt_tabladepto where id=@vicontadordeptoaux))
					BEGIN
					INSERT INTO @vt_tablarespuesta
								(codigo,descripcion)
								SELECT  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
								FROM dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
								INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
								INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
								INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
								INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
								WHERE articulos.Cod_TpArt=@vc_TipoArticulos
								AND articulos.Cod_Dpto = (SELECT depto FROM @vt_tabladepto where id=@vicontadordeptoaux)
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar  @vt_tablarespuesta' GOTO ERROR END 
					END
				END
				SET @vicontadordeptoaux=@vicontadordeptoaux+1
			END
		END 
		ELSE
		BEGIN
			IF(LEN (@vc_lineas)>0)
			BEGIN
				INSERT @vt_tablaLinea (linea) (SELECT value FROM SotSchema.DividirCadena (@vc_lineas, ',') WHERE RTRIM(value) <> '' )
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vc_lineas' GOTO ERROR END 
				SET @vicontadorlinea= (SELECT COUNT(*) FROM @vt_tablaLinea)
				SET @vicontadorlineaaux =1
				WHILE @vicontadorlineaaux<=@vicontadorlinea
				BEGIN
					IF EXISTS(select  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
								  FROM dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
									INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
									INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
									INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
									WHERE articulos.Cod_TpArt=@vc_TipoArticulos
									AND articulos.Cod_Linea= (SELECT linea FROM @vt_tablaLinea where id=@vicontadorlineaaux))
							BEGIN
								INSERT INTO @vt_tablarespuesta
								(codigo,descripcion)
								SELECT  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
								FROM dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
								INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
								INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
								INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
								INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
								WHERE articulos.Cod_TpArt=@vc_TipoArticulos
								AND articulos.Cod_Linea= (SELECT linea FROM @vt_tablaLinea where id=@vicontadorlineaaux)
								IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vt_tablarespuesta' GOTO ERROR END 
							END
					SET @vicontadorlineaaux=@vicontadorlineaaux+1
				END
			END
			ELSE
			BEGIN
				IF EXISTS(select  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
									from dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
									INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
									INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
									INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
									INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
									WHERE 
									articulos.Cod_TpArt=@vc_TipoArticulos)
					BEGIN
						INSERT INTO @vt_tablarespuesta
						(codigo,descripcion)
						SELECT  DISTINCT alamcen.Cod_Alm,alamcen.Desc_CatAlm 
						from dbo.ZctArtXAlm articuloalmacen WITH (NOLOCK)
						INNER JOIN dbo.ZctCatArt articulos WITH (NOLOCK)	ON  articuloalmacen.Cod_Art=articulos.Cod_Art
						INNER JOIN dbo.ZctCatDpto depto WITH (NOLOCK)		ON articulos.Cod_Dpto=depto.Cod_Dpto
						INNER JOIN dbo.ZctCatAlm alamcen WITH (NOLOCK)		ON articuloalmacen.Cod_Alm=alamcen.Cod_Alm
						INNER JOIN dbo.ZctCatLinea linea WITH (NOLOCK)		ON linea.Cod_Dpto=depto.Cod_Dpto
						WHERE 
						articulos.Cod_TpArt=@vc_TipoArticulos
						IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Insertar @vt_tablarespuesta' GOTO ERROR END 
					END
			END
		END
	END
SELECT codigo, descripcion FROM @vt_tablarespuesta order by descripcion
SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN

GO