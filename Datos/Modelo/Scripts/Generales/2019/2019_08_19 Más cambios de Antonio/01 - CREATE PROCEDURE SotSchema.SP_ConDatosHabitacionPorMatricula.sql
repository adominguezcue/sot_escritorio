IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConDatosHabitacionPorMatricula') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConDatosHabitacionPorMatricula
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene los datos de las habitaciones y costos por matricula
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 11/07/2019
--Descripcion: Borrado de venta para restarante a partir del ticket
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConDatosHabitacionPorMatricula
	@vcNumMatricula nvarchar(10)
AS
DECLARE
	 @vcMensajeerror			nvarchar (150)
	,@viContIdRenta				int
	,@viContIdRentaAux			int
	,@viContIdVentaRenta		int
	,@viContIdVentaRentaAux		int
	,@vdec_Aux					decimal
	,@vi_PrecioAuxiliar			decimal
	,@vbrenovacion				bit
DECLARE @VT_tablaRespuesta TABLE (IdRenta int
,Habitacion int
,FechaRegistro Datetime
,PrecioHabi decimal
,PrecioPersonaExtra  decimal
,PrecioNoches decimal
,PrecioHoras decimal
,PrecioPaquetes decimal
,PrecioCortesias decimal
,PrecioDescuentos decimal
,Esreservacion		bit					
)
DECLARE @VT_TablaRentas TABLE(Id int NOT NULL identity(1,1),idRenta int)
SET NOCOUNT ON
	SET @vi_PrecioAuxiliar=0
	IF EXISTS(select  id from SotSchema.AutomovilesRenta with (NOLOCK) where Matricula=@vcNumMatricula)
		BEGIN
			IF EXISTS (select rentas.Id from SotSchema.Rentas rentas WITH (NOLOCK)
						INNER JOIN SotSchema.AutomovilesRenta autorenta WITH(NOLOCK) ON rentas.Id=autorenta.IdRenta
						WHERE autorenta.Matricula=@vcNumMatricula)
			BEGIN
				INSERT INTO @VT_tablaRespuesta (IdRenta,Habitacion,FechaRegistro) (select Id,IdHabitacion,FechaRegistro from SotSchema.Rentas with (NOLOCK) where id in (select  IdRenta from SotSchema.AutomovilesRenta with (NOLOCK) where Matricula=@vcNumMatricula))
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar Tabla respuesta' GOTO ERROR END 
				INSERT INTO @VT_TablaRentas (idRenta) (SELECT IdRenta FROM @VT_tablaRespuesta)
				IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al insertar @VT_TablaRentas' GOTO ERROR END 
				IF EXISTS (	select ventasrenta.Id from SotSchema.VentasRenta ventasrenta WITH (NOLOCK)
							INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON ventasrenta.IdRenta=rentas.Id
							INNER JOIN SotSchema.AutomovilesRenta autorenta WITH (NOLOCK) ON  rentas.Id = autorenta.IdRenta
							WHERE autorenta.Matricula=@vcNumMatricula)
				BEGIN
						 SET @viContIdRenta= (SELECT COUNT(*) FROM @VT_TablaRentas)
						 SET @viContIdRentaAux =1
							WHILE @viContIdRentaAux<=@viContIdRenta
							BEGIN
									IF EXISTS (SELECT DetallePago.Id FROM SotSchema.DetallesPago DetallePago WITH(NOLOCK) ---- Valida si es habitacion
									INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id  
									INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
									WHERE DetallePago.IdConceptoPago=1 AND rentas.Id=(SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))---- Valida si es habitacion
										BEGIN
											SET @vi_PrecioAuxiliar =(SELECT SUM(DetallePago.ValorConIVA) FROM SotSchema.DetallesPago DetallePago  WITH(NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id 
												INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
												WHERE DetallePago.IdConceptoPago=1 AND rentas.Id= (SELECT IdRenta From @VT_TablaRentas where Id=@viContIdRentaAux))
												UPDATE @VT_tablaRespuesta set PrecioHabi=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
										END
										ELSE
										BEGIN
											UPDATE @VT_tablaRespuesta set PrecioHabi=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
										END
										-------------------------Validamos Persona Extra 
									SET @vi_PrecioAuxiliar=0
									IF EXISTS (SELECT DetallePago.Id FROM SotSchema.DetallesPago DetallePago WITH(NOLOCK)-------------------------Validamos Persona Extra 
									INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id  
									INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
									WHERE DetallePago.IdConceptoPago=4 AND rentas.Id=(SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))-------------------------Validamos Persona Extra 
									BEGIN
												SET @vi_PrecioAuxiliar =(SELECT SUM(DetallePago.ValorConIVA) FROM SotSchema.DetallesPago DetallePago  WITH(NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventasrenta WITH (NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id 
												INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
												WHERE DetallePago.IdConceptoPago=4 AND rentas.Id= (SELECT IdRenta From @VT_TablaRentas where Id=@viContIdRentaAux))
												UPDATE @VT_tablaRespuesta set PrecioPersonaExtra=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 								
									END
									ELSE
									BEGIN
											UPDATE @VT_tablaRespuesta set PrecioPersonaExtra=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
									END
									-----------------Validamos Noches----------------------------------------
									SET @vi_PrecioAuxiliar=0
									IF EXISTS (SELECT DetallePago.Id FROM SotSchema.DetallesPago DetallePago WITH(NOLOCK)-----------------Validamos Noches----------------------------------------
									INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id  
									INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
									WHERE DetallePago.IdConceptoPago=2 AND rentas.Id=(SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))-----------------Validamos Noches----------------------------------------
									BEGIN
												SET @vi_PrecioAuxiliar =(SELECT SUM(DetallePago.ValorConIVA) FROM SotSchema.DetallesPago DetallePago  WITH(NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventasrenta WITH (NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id 
												INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
												WHERE DetallePago.IdConceptoPago=2 AND rentas.Id= (SELECT IdRenta From @VT_TablaRentas where Id=@viContIdRentaAux))
												UPDATE @VT_tablaRespuesta set PrecioNoches=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)	
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 							
									END
									ELSE
									BEGIN
											UPDATE @VT_tablaRespuesta set PrecioNoches=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
									END
									-----------------Validamos Horas----------------------------------------
									SET @vi_PrecioAuxiliar=0
									IF EXISTS (SELECT DetallePago.Id FROM SotSchema.DetallesPago DetallePago WITH(NOLOCK)-----------------Validamos Horas----------------------------------------
									INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id  
									INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
									WHERE DetallePago.IdConceptoPago=3 AND rentas.Id=(SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))-----------------Validamos Horas----------------------------------------
									BEGIN
												SET @vi_PrecioAuxiliar =(SELECT SUM(DetallePago.ValorConIVA) FROM SotSchema.DetallesPago DetallePago  WITH(NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventasrenta WITH (NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id 
												INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
												WHERE DetallePago.IdConceptoPago=3 AND rentas.Id= (SELECT IdRenta From @VT_TablaRentas where Id=@viContIdRentaAux))
												UPDATE @VT_tablaRespuesta set PrecioHoras=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 								
									END
									ELSE
									BEGIN
											UPDATE @VT_tablaRespuesta set PrecioHoras=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
									END
												-----------------Validamos PAQUETES----------------------------------------
									SET @vi_PrecioAuxiliar=0
									IF EXISTS (SELECT DetallePago.Id FROM SotSchema.DetallesPago DetallePago WITH(NOLOCK)-----------------Validamos PAQUETES----------------------------------------
									INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id  
									INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
									WHERE DetallePago.IdConceptoPago=5 AND rentas.Id=(SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))-----------------Validamos PAQUETES----------------------------------------
									BEGIN
												SET @vi_PrecioAuxiliar =(SELECT SUM(DetallePago.ValorConIVA) FROM SotSchema.DetallesPago DetallePago  WITH(NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventasrenta WITH (NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id 
												INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
												WHERE DetallePago.IdConceptoPago=5 AND rentas.Id= (SELECT IdRenta From @VT_TablaRentas where Id=@viContIdRentaAux))
												UPDATE @VT_tablaRespuesta set PrecioPaquetes=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)		
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 						
									END
									ELSE
									BEGIN
											UPDATE @VT_tablaRespuesta set PrecioPaquetes=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
									END
									-----------------Validamos DESCUENTOS----------------------------------------
									SET @vi_PrecioAuxiliar=0
									IF EXISTS (SELECT DetallePago.Id FROM SotSchema.DetallesPago DetallePago WITH(NOLOCK)-----------------Validamos DESCUENTOS----------------------------------------
									INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id  
									INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
									WHERE DetallePago.IdConceptoPago=6 AND rentas.Id=(SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))-----------------Validamos DESCUENTOS----------------------------------------
									BEGIN
												SET @vi_PrecioAuxiliar =(SELECT SUM(DetallePago.ValorConIVA) FROM SotSchema.DetallesPago DetallePago  WITH(NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventasrenta WITH (NOLOCK) on DetallePago.IdVentaRenta=ventasrenta.Id 
												INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
												WHERE DetallePago.IdConceptoPago=6 AND rentas.Id= (SELECT IdRenta From @VT_TablaRentas where Id=@viContIdRentaAux))
												UPDATE @VT_tablaRespuesta set PrecioDescuentos=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)	
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 							
									END
									ELSE
									BEGIN
											UPDATE @VT_tablaRespuesta set PrecioDescuentos=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
									END
									-----------------------------VALDIAMOS CORTESIAS ---------------------------------------------------------------------------------------------------------
									SET @vi_PrecioAuxiliar=0
									IF EXISTS (SELECT pagosrenta.Id FROM SotSchema.PagosRenta pagosrenta  WITH(NOLOCK)
									INNER JOIN SotSchema.VentasRenta ventasrenta WITH(NOLOCK) on pagosrenta.IdVentaRenta=ventasrenta.Id  
									INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
									WHERE pagosrenta.IdTipoPago =6 AND rentas.Id=(SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))
									BEGIN 
										SET @vi_PrecioAuxiliar =(SELECT SUM(PagosRenta.Valor) FROM SotSchema.PagosRenta pagosrenta  WITH(NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventasrenta WITH (NOLOCK) on pagosrenta.IdVentaRenta=ventasrenta.Id 
												INNER JOIN SotSchema.Rentas rentas WITH(NOLOCK) ON VentasRenta.IdRenta=rentas.Id
												WHERE pagosrenta.IdTipoPago=6 AND rentas.Id= (SELECT IdRenta From @VT_TablaRentas where Id=@viContIdRentaAux))
												UPDATE @VT_tablaRespuesta set PrecioCortesias=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)	
												IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
									END
									ELSE
									BEGIN
										UPDATE @VT_tablaRespuesta set PrecioCortesias=@vi_PrecioAuxiliar WHERE IdRenta = (SELECT IdRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
										IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
									END
								----Validamos si es reservacion
								IF EXISTS(select autosrenta.Id from SotSchema.PagosRenta pgosrenta WITH (NOLOCK)
												INNER JOIN SotSchema.VentasRenta ventarenta WITH (NOLOCK)ON pgosrenta.IdVentaRenta=ventarenta.Id
												INNER JOIN SotSchema.Rentas rentas WITH (NOLOCK) ON ventarenta.IdRenta=rentas.Id
												INNER JOIN SotSchema.AutomovilesRenta autosrenta WITH (NOLOCK) ON rentas.Id=autosrenta.IdRenta
												WHERE pgosrenta.IdTipoPago=4 AND autosrenta.Matricula=@vcNumMatricula
												AND rentas.Id=(SELECT idRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux))
										BEGIN
											UPDATE @VT_tablaRespuesta SET Esreservacion=1 where  IdRenta = (SELECT idRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
										END
										ELSE
										BEGIN
											UPDATE @VT_tablaRespuesta SET Esreservacion=0 where  IdRenta = (SELECT idRenta FROM @VT_TablaRentas where Id=@viContIdRentaAux)
											IF (@@ERROR<>0) BEGIN SET @vcMensajeerror='Error al Actualizar  @VT_tablaRespuesta' GOTO ERROR END 
										END
								SET @viContIdRentaAux=@viContIdRentaAux+1
							END
				END
			END
		END
SELECT IdRenta,Habitacion,FechaRegistro,PrecioHabi,PrecioPersonaExtra,PrecioNoches,PrecioHoras,PrecioPaquetes,PrecioCortesias,PrecioDescuentos,Esreservacion FROM  @VT_tablaRespuesta

SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN

GO
