﻿-- Creating table 'ConfiguracionesPuntosLealtad'
CREATE TABLE [SotSchema].[ConfiguracionesPuntosLealtad] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UrlServicio] nvarchar(max)  NOT NULL,
    [Usuario] nvarchar(max)  NOT NULL,
    [Contrasena] nvarchar(max)  NOT NULL,
    [PrecioTarjetas] decimal(19,4)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'ConfiguracionesPuntosLealtad'
ALTER TABLE [SotSchema].[ConfiguracionesPuntosLealtad]
ADD CONSTRAINT [PK_ConfiguracionesPuntosLealtad]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO