/****** Object:  Table [Sistemas].[ParametrosSistemas]    Script Date: 17/07/2019 03:44:34 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Sistemas].[ParametrosSistemas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GuidSistema] [uniqueidentifier] NOT NULL,
	[Nombre] [nvarchar](max) NOT NULL,
	[Valor] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_ParametrosSistemas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


