begin tran t1

--declare @sucursal varchar(max) = 'V SUR TEST'
declare @requiereVPoints bit = 0
declare @esPrueba bit = 1
declare @esVSur bit = 0

declare @idSOT uniqueidentifier = 'B6013E73-AA7A-45AD-A4E0-30A497EEA67C'
declare @idSincronizado uniqueidentifier = '97E45618-9D03-4500-A7B4-481D15829F29'
declare @idGeneradorFolios uniqueidentifier = 'D2DEC478-BFA3-492F-95E9-2173F696E369'
declare @idConfigurador uniqueidentifier = '68AE5DD4-8EF3-44CC-8B7D-89300B980894'

declare @parametroImagotipo varchar(max) = 'Imagotipo'
declare @parametroImagenReportes varchar(max) = 'ImagenReportes'
declare @parametroConfiguracionFinalizada varchar(max) = 'ConfiguracionFinalizada'

declare @parametroUrlRastrilleo varchar(max) = 'UrlWebServiceRastrilleo'
declare @parametroUrlSincronizacion varchar(max) = 'UrlWebServiceCorte'
declare @parametroServidor varchar(max) = 'Servidor'
declare @parametroPuerto varchar(max) = 'Puerto'

declare @imagotipo varchar(max),
		@imagenReportes varchar(max)



--Parámetros del sot---------------------------------------------
		
if(exists(select * from Sistemas.ConfiguracionesSistemas))
begin
	select 
	@imagotipo = isnull(Imagotipo, ''), 
	@imagenReportes = isnull(ImagenReportes, '') 
	from Sistemas.ConfiguracionesSistemas
end
else
begin
	set @imagotipo = ''
	set @imagenReportes = ''
end

if(@imagenReportes is not null)
	set @imagenReportes = '"' + @imagenReportes + '"'

if(@imagotipo is not null)
	set @imagotipo = '"' + @imagotipo + '"'

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idSOT and Nombre = @parametroImagotipo))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idSOT, @parametroImagotipo, @imagotipo)

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idSOT and Nombre = @parametroImagenReportes))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idSOT, @parametroImagenReportes, @imagenReportes)

--Parámetros sincronización-----------------------------------------------

declare @urlRastrilleo varchar(max)
declare @urlSincronizacion varchar(max)
declare @Servidor varchar(max)
declare @Puerto varchar(max)

set @Puerto = '8001'

if(@esPrueba = 1)
begin
	set @urlRastrilleo = '"http://admin.gmsweb.com.mx/RastrilleoPruebasInternas/"'
	set @urlSincronizacion = '"http://admin.gmsweb.com.mx/CaratulaPruebasInternas/"'
	set @Servidor = '"localhost"'
end
else
begin
	set @urlRastrilleo = '"http://admin.gmsweb.com.mx/Rastrilleo/"'
	set @urlSincronizacion = '"http://admin.gmsweb.com.mx/Caratula/"'

	if(@esVSur = 1)
	begin
		set @Servidor = '"SERVIDOR"'
	end
	else
	begin
		set @Servidor = '"SERVIDORLEREVE"'
	end
end

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idSincronizado and Nombre = @parametroUrlRastrilleo))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idSincronizado, @parametroUrlRastrilleo, @urlRastrilleo)

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idSincronizado and Nombre = @parametroUrlSincronizacion))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idSincronizado, @parametroUrlSincronizacion, @urlSincronizacion)

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idSincronizado and Nombre = @parametroServidor))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idSincronizado, @parametroServidor, @Servidor)

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idSincronizado and Nombre = @parametroPuerto))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idSincronizado, @parametroPuerto, @Puerto)

--Parámetros generador folios---------------------------------------------

set @Puerto = '8002'

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idGeneradorFolios and Nombre = @parametroServidor))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idGeneradorFolios, @parametroServidor, @Servidor)

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idGeneradorFolios and Nombre = @parametroPuerto))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idGeneradorFolios, @parametroPuerto, @Puerto)

--Parámetros del configurador---------------------------------------------

if(not exists(select * from Sistemas.ParametrosSistemas where GuidSistema = @idConfigurador and Nombre = @parametroConfiguracionFinalizada))
insert into Sistemas.ParametrosSistemas
(GuidSistema, Nombre, Valor)
values
(@idConfigurador, @parametroConfiguracionFinalizada, 'true')

--Parámetros de vpoints

if(@requiereVPoints = 1 and not exists(select * from SotSchema.ConfiguracionesPuntosLealtad))
begin

declare @precioTarjetasV decimal(18, 4)

select @precioTarjetasV = PrecioTarjetasPuntos from ZctCatPar

	if(@esPrueba = 1)
	begin
		insert into SotSchema.ConfiguracionesPuntosLealtad
		(UrlServicio, Usuario, Contrasena, PrecioTarjetas)
		values
		('http://www.vmotelboutique-rewards.com/webServiceTest/server.php', 'webservice@vmotelboutique-rewards.com', 'XqRwhnet5gnZBuQe', @precioTarjetasV)
	end
	else
	begin
		insert into SotSchema.ConfiguracionesPuntosLealtad
		(UrlServicio, Usuario, Contrasena, PrecioTarjetas)
		values
		('http://www.vmotelboutique-rewards.com/webService/server.php', 'webservice@vmotelboutique-rewards.com', '2E0UhCjC', @precioTarjetasV)
	end
end

select * from ZctCatPar
select * from SotSchema.ConfiguracionesPuntosLealtad
select * from Sistemas.ParametrosSistemas

rollback tran t1