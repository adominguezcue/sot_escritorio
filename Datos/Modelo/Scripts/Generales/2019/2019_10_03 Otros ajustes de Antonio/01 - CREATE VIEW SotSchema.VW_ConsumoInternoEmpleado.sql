IF EXISTS (select top 1 name FROM sys.views where name = 'VW_ConsumoInternoEmpleado')
BEGIN
	DROP VIEW SotSchema.VW_ConsumoInternoEmpleado
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--Vista donde obtiene los empleados para consumo interno 
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 17/09/2019
--Descripcion: Obtiene los empleados de consumo interno.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE VIEW SotSchema.VW_ConsumoInternoEmpleado
AS
SELECT Empleado.NumeroEmpleado as NumeroEmpleado
,Empleado.Nombre+' '+Empleado.ApellidoPaterno+' '+Empleado.ApellidoMaterno as NombreCompleto
,count(ConsumoInterno.Id) as CantidadConsumos
,SUM(ConsumoInterno.ValorConIVA) as Total
 FROM SotSchema.Empleados   Empleado	  WITH(NOLOCK)
 INNER JOIN SotSchema.ConsumosInternos ConsumoInterno  WITH(NOLOCK) ON	Empleado.Id= ConsumoInterno.IdEmpleadoConsume
 where Empleado.Activo=1 and Empleado.Habilitado=1	GROUP BY Empleado.NumeroEmpleado,Empleado.Nombre,Empleado.ApellidoPaterno,Empleado.ApellidoMaterno

GO