IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConConsumoInternoEmpleado') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConConsumoInternoEmpleado
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 19/09/2019
--Descripcion: Consulta el Consumo Interno Por Empleado
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConConsumoInternoEmpleado
@vi_NumeroEmpleado	 nvarchar(10)
,@vd_fechaInicial	 datetime  =NULL
,@vd_fechaFinal		 datetime =NULL
AS
SET NOCOUNT ON
IF(@vd_fechaInicial IS NOT NULL AND @vd_fechaFinal IS NOT NULL)
BEGIN
SELECT 
ArticulosInsumos.IdArticulo	 as CodigoArticulo
,articulos.Desc_Art as ArticuloDescripcion
,SUM(	ArticulosInsumos.Cantidad) as Cantidad
,SUM (consumosinternos.ValorConIVA) as Total
FROM  SotSchema.ArticulosConsumoInterno ArticulosInsumos WITH(NOLOCK)
INNER JOIN SotSchema.ConsumosInternos consumosinternos WITH(NOLOCK) ON    ArticulosInsumos.IdConsumoInterno= consumosinternos.Id
INNER JOIN SotSchema.Empleados empleados WITH(NOLOCK) ON  consumosinternos.IdEmpleadoConsume= empleados.id
INNER JOIN dbo.ZctCatArt  articulos WITH(NOLOCK) ON  ArticulosInsumos.IdArticulo= articulos.Cod_Art
WHERE  empleados.NumeroEmpleado= @vi_NumeroEmpleado  
AND  consumosinternos.FechaCreacion>=@vd_fechaInicial
AND consumosinternos.FechaCreacion<=@vd_fechaFinal
GROUP BY ArticulosInsumos.IdArticulo,articulos.Desc_Art	 
END
ELSE
BEGIN
 SELECT 
ArticulosInsumos.IdArticulo	 as CodigoArticulo
,articulos.Desc_Art as ArticuloDescripcion
,SUM(	ArticulosInsumos.Cantidad) as Cantidad
,SUM ( consumosinternos.ValorConIVA) as Total
FROM  SotSchema.ArticulosConsumoInterno ArticulosInsumos WITH(NOLOCK)
INNER JOIN SotSchema.ConsumosInternos consumosinternos WITH(NOLOCK) ON    ArticulosInsumos.IdConsumoInterno= consumosinternos.Id
INNER JOIN SotSchema.Empleados empleados WITH(NOLOCK) ON  consumosinternos.IdEmpleadoConsume= empleados.id
INNER JOIN dbo.ZctCatArt  articulos WITH(NOLOCK) ON  ArticulosInsumos.IdArticulo= articulos.Cod_Art
WHERE  empleados.NumeroEmpleado= @vi_NumeroEmpleado  
GROUP BY ArticulosInsumos.IdArticulo,articulos.Desc_Art	 
END 
SET NOCOUNT OFF
RETURN 
GO