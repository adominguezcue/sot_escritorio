IF EXISTS (select top 1 name FROM sys.views where name = 'VW_ConsCancelacionesYCortesias')
BEGIN
	DROP VIEW SotSchema.VW_ConsCancelacionesYCortesias
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--Vista donde obtiene los empleados para consumo interno 
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación 17/09/2019
--Descripcion: Obtiene los empleados de consumo interno.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE VIEW SotSchema.VW_ConsCancelacionesYCortesias
AS
---cancelacion de habitacion 
select 
Ventas.Id as idVentaCorrecta
,Ventas.SerieTicket+ '-'+convert(nvarchar(10), Ventas.FolioTicket)  as Ticket
,Ventas.FechaModificacion as FechaModificacion 
,habitacion.NumeroHabitacion as Habitacion 
,SUM( Ventas.ValorConIVA)  as Precio
,'Renta de Habitación Cancelada' as Concepto
, Ventas.MotivoCancelacion as MotivoCancelacion
,empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoMaterno	as UsuarioCancelo
,confiturno.Nombre   as Turno
from SotSchema.Ventas Ventas WITH(NOLOCK)
INNER JOIN 	SotSchema.VentasRenta ventarenta  WITH(NOLOCK) ON  ventas.Transaccion= ventarenta.Transaccion
INNER JOIN SotSchema.Rentas renta  WITH(NOLOCK) ON 	  renta.Id=	  ventarenta.IdRenta
INNER JOIN SotSchema.Habitaciones habitacion  WITH(NOLOCK)  ON 	renta.IdHabitacion=	habitacion.Id
INNER JOIN SotSchema.Empleados empleado  WITH(NOLOCK) ON  empleado.Id=  ventas.IdUsuarioCancelo
INNER JOIN SotSchema.CortesTurno CorteTurno WITH(NOLOCK) ON   Ventas.IdCorteTurno= CorteTurno.Id
INNER JOIN SotSchema.ConfiguracionesTurno confiturno WITH(NOLOCK) ON   CorteTurno.IdConfiguracionTurno=	confiturno.Id
where ventas.Cancelada=1   AND Ventas.ValorConIVA>0
group by Ventas.SerieTicket, Ventas.FolioTicket, Ventas.FechaModificacion,Ventas.MotivoCancelacion,empleado.Nombre,	empleado.ApellidoPaterno,empleado.ApellidoMaterno,habitacion.NumeroHabitacion,Ventas.Id,confiturno.Nombre
UNION ALL
--cancelacion de reservaciones
SELECT 
ventas.Id  as  idVentaCorrecta
,Ventas.SerieTicket+ '-'+convert(nvarchar(10), Ventas.FolioTicket)  as Ticket
,ventas.FechaModificacion  as FechaModificacion 
,0 as Habitacion
, SUM(Ventas.ValorConIVA)  as Precio
,'Reservación Cancelada' as Concepto 
,Ventas.MotivoCancelacion as MotivoCancelacion
,empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoMaterno	as UsuarioCancelo
,configturno.Nombre as Turno 
FROM  SotSchema.Ventas	ventas  WITH(NOLOCK)
INNER JOIN SotSchema.Empleados empleado  WITH(NOLOCK) ON  empleado.Id=  ventas.IdUsuarioCancelo
INNER JOIN SotSchema.CortesTurno  WITH(NOLOCK) ON 	ventas.IdCorteTurno= cortesturno.Id
INNER JOIN SotSchema.ConfiguracionesTurno configturno WITH(NOLOCK) ON 	 cortesturno.IdConfiguracionTurno=configturno.Id
where ventas.Cancelada=1 and ventas.SerieTicket='RE'  AND 	Ventas.ValorConIVA>0
GROUP BY Ventas.SerieTicket,Ventas.FolioTicket,	ventas.FechaModificacion,Ventas.MotivoCancelacion,empleado.Nombre, empleado.ApellidoPaterno	,empleado.ApellidoMaterno,ventas.Id,configturno.Nombre
UNION ALL
--cancelacion de consumo interno id consmo INterno
SELECT 
 consumointerno.Id as idVentaCorrecta
,preventas.SerieTicket+'-'+ 	convert(nvarchar(10), preventas.FolioTicket)  as Ticket 
, consumointerno.FechaModificacion as  FechaModificacion 
,0 as 	Habitacion
,SUM(consumointerno.ValorConIVA) as Precio
,'Consumo Interno Cancelado' as  Concepto
,consumointerno.MotivoCancelacion as MotivoCancelacion
,empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoMaterno	as UsuarioCancelo
,configturno.Nombre	as Turno
FROM 	SotSchema.ConsumosInternos consumointerno  WITH(NOLOCK)
INNER JOIN SotSchema.Preventas preventas WITH(NOLOCK) ON  preventas.Id= consumointerno.IdPreventa
INNER JOIN SotSchema.Empleados empleado  WITH(NOLOCK) ON  empleado.Id= consumointerno.IdUsuarioModifico
INNER JOIN SotSchema.CortesTurno cortesturno WITH(NOLOCK) ON  consumointerno.IdCorteTurno=cortesturno.Id
INNER JOIN SotSchema.ConfiguracionesTurno configturno WITH(NOLOCK) ON cortesturno.IdConfiguracionTurno=	configturno.Id
WHERE 	consumointerno.IdEstado=6	AND consumointerno.ValorConIVA>0
GROUp BY preventas.SerieTicket,preventas.FolioTicket, consumointerno.FechaModificacion,consumointerno.MotivoCancelacion,empleado.Nombre,empleado.ApellidoPaterno,empleado.ApellidoMaterno, consumointerno.Id,configturno.Nombre
UNION ALL
--cancelacion de restaurante --
SELECT 	ocupacionmesa.Id as idVentaCorrecta
,preventas.SerieTicket+'-'+ 	convert(nvarchar(10), preventas.FolioTicket)  as Ticket 
,ordenrestaurant.FechaModificacion as  FechaModificacion
,0 as Habitacion
,ordenrestaurant.ValorConIVA	as Precio
,'Orden Restaurante Cancelado' as  Concepto
, ocupacionmesa.MotivoCancelacion  as MotivoCancelacion
,empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoMaterno	as UsuarioCancelo
,configturno.Nombre as Turno 
FROM  SotSchema.OrdenesRestaurante ordenrestaurant  WITH(NOLOCK)
INNER JOIN  SotSchema.OcupacionesMesa ocupacionmesa  WITH(NOLOCK) ON   ocupacionmesa.Id=ordenrestaurant.IdOcupacionMesa
INNER JOIN SotSchema.Preventas preventas WITH(NOLOCK) ON   preventas.Id= ocupacionmesa.IdPreventa
INNER JOIN SotSchema.Empleados	 empleado  WITH(NOLOCK) ON  empleado.Id= ocupacionmesa.IdUsuarioElimino
INNER JOIN SotSchema.CortesTurno cortesturno WITH(NOLOCK) ON   ocupacionmesa.IdCorte= cortesturno.Id
INNER JOIN SotSchema.ConfiguracionesTurno configturno WITH(NOLOCK) 	ON cortesturno.IdConfiguracionTurno=configturno.Id
where ordenrestaurant.IdEstado=6 	 and ocupacionmesa.IdEstado=3
UNION ALL
--comanda cancelada	 id comanda
SELECT
comandas.Id as  idVentaCorrecta
,preventas.SerieTicket+'-'+ 	convert(nvarchar(10), preventas.FolioTicket)  as Ticket
,comandas.FechaCreacion as  FechaModificacion
,habitacion.NumeroHabitacion   as Habitacion
,Comandas.ValorConIVA as Precio
,'Comanda Cancelada' as 	 Concepto
, comandas.MotivoCancelacion 	as MotivoCancelacion
, empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoPaterno	as UsuarioCancelo
,configturno.Nombre as Turno
FROM   SotSchema.Comandas comandas  WITH(NOLOCK)
INNER JOIN SotSchema.Rentas rentas  WITH(NOLOCK) ON 	comandas.IdRenta=rentas.Id
INNER JOIN SotSchema.VentasRenta ventasrenta  WITH(NOLOCK) ON rentas.id=ventasrenta.IdRenta  AND ventasrenta.EsInicial=1
INNER JOIN 	SotSchema.Preventas preventas  WITH(NOLOCK) ON comandas.IdPreventa=preventas.Id 
INNER JOIN SotSchema.Habitaciones habitacion  WITH(NOLOCK) ON  rentas.IdHabitacion= habitacion.Id
INNER JOIN SotSchema.Empleados	 empleado   WITH(NOLOCK) ON  empleado.Id= comandas.IdUsuarioElimino
INNER JOIN SotSchema.CortesTurno cortesturno WITH(NOLOCK) ON   comandas.IdCorte=cortesturno.Id
INNER JOIN SotSchema.ConfiguracionesTurno configturno WITH(NOLOCK) 	ON cortesturno.IdConfiguracionTurno=configturno.Id
where  comandas.IdEstado=6	   AND 	Comandas.ValorConIVA>0
UNION ALL
--orden de taxi cancelada --id orden taxi
SELECT 
ordenestaxi.Id as 	idVentaCorrecta
,preventas.SerieTicket+'-'+ 	convert(nvarchar(10), preventas.FolioTicket)  as Ticket
, ordenestaxi.FechaCreacion  as  FechaModificacion
,0  as Habitacion
, ordenestaxi.Precio as  Precio
,'Orden Taxi cancelada' as 	 Concepto
, ordenestaxi.MotivoCancelacion as MotivoCancelacion
, empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoPaterno	as UsuarioCancelo
,'' as Turno
FROM SotSchema.OrdenesTaxis ordenestaxi  WITH(NOLOCK)
INNER JOIN 	SotSchema.Preventas preventas  WITH(NOLOCK) ON ordenestaxi.IdPreventa=preventas.Id 
INNER JOIN SotSchema.Empleados	 empleado  WITH(NOLOCK) ON  empleado.Id= ordenestaxi.IdUsuarioElimino
--INNER JOIN SotSchema.Ventas ventas WITH(NOLOCK) ON 	   ventas.SerieTicket=preventas.SerieTicket --AND ventas.FolioTicket=preventas.FolioTicket
WHERE ordenestaxi.IdEstado=3 AND ordenestaxi.Precio>0
UNION ALL
SELECT 
 ventas.Id as idVentaCorrecta
,preventas.SerieTicket+'-'+ 	convert(nvarchar(10), preventas.FolioTicket)  as Ticket
,comandas.FechaModificacion as  FechaModificacion
,habitacion.NumeroHabitacion   as Habitacion
,comandas.ValorConIVA 	 as  Precio
,'Cortesía Comandas' as 	 Concepto
,'Cortesía Comandas' 	as MotivoCancelacion
, empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoPaterno	as UsuarioCancelo
,configturno.Nombre as Turno 
FROM   SotSchema.Comandas comandas  WITH(NOLOCK) 
INNER JOIN SotSchema.PagosComandas pagoscomandas  WITH(NOLOCK) ON 	  pagoscomandas.IdComanda= comandas.Id
INNER JOIN SotSchema.Rentas rentas  WITH(NOLOCK)  ON 	comandas.IdRenta=rentas.Id
INNER JOIN SotSchema.VentasRenta ventasrenta  WITH(NOLOCK)  ON rentas.id=ventasrenta.IdRenta  AND ventasrenta.EsInicial=1
INNER JOIN SotSchema.Habitaciones habitacion  WITH(NOLOCK) ON  rentas.IdHabitacion= habitacion.Id
INNER JOIN SotSchema.Empleados	 empleado  WITH(NOLOCK) ON  empleado.Id= comandas.IdUsuarioModifico
INNER JOIN SotSchema.Ventas ventas WITH(NOLOCK) ON 	 ventas.Transaccion=comandas.Transaccion
INNER JOIN 	SotSchema.Preventas preventas  WITH(NOLOCK) ON comandas.IdPreventa=preventas.Id 
INNER JOIN SotSchema.CortesTurno cortesturno WITH(NOLOCK) ON comandas.IdCorte= cortesturno.Id
INNER JOIN SotSchema.ConfiguracionesTurno configturno WITH(NOLOCK) ON cortesturno.IdConfiguracionTurno=	configturno.Id
where  pagoscomandas.IdTipoPago=6	AND pagoscomandas.Valor>0
UNION ALL
---id ventas
SELECT 
ventas.Id as idVentaCorrecta
,preventas.SerieTicket+'-'+ 	convert(nvarchar(10), preventas.FolioTicket)  as Ticket
,ocupacionmesa.FechaModificacion as  FechaModificacion
,0 as Habitacion
,pagosocupacion.Valor as  Precio
,'Cortesía Restaurante' as  Concepto
,'Cortesía Restaurante'  as MotivoCancelacion
,empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoPaterno	as UsuarioCancelo
,configturno.Nombre as Turno 
FROM  SotSchema.OcupacionesMesa  ocupacionmesa  WITH(NOLOCK)
--INNER JOIN  SotSchema.OrdenesRestaurante ordenrestaurant WITH(NOLOCK) ON   ocupacionmesa.Id=ordenrestaurant.IdOcupacionMesa
INNER JOIN SotSchema.PagosOcupacionMesa pagosocupacion  WITH(NOLOCK) ON 	  ocupacionmesa.Id=pagosocupacion.IdOcupacionMesa	
INNER JOIN SotSchema.Preventas preventas WITH(NOLOCK) ON 	preventas.Id=ocupacionmesa.IdPreventa
INNER JOIN SotSchema.ventas ventas WITH(NOLOCK) ON 		ventas.Transaccion=ocupacionmesa.Transaccion
INNER JOIN SotSchema.Empleados	 empleado WITH(NOLOCK) ON  empleado.Id= ocupacionmesa.IdUsuarioModifico
INNER JOIN SotSchema.CortesTurno cortestuno WITH(NOLOCK) ON  ocupacionmesa.IdCorte=cortestuno.Id
INNER JOIN SotSchema.ConfiguracionesTurno configturno WITH(NOLOCK) ON  cortestuno.IdConfiguracionTurno=configturno.Id
where pagosocupacion.IdTipoPago=6  AND 	pagosocupacion.Valor>0 
group by  preventas.SerieTicket,preventas.FolioTicket,ocupacionmesa.FechaModificacion,empleado.Nombre, empleado.ApellidoPaterno,empleado.ApellidoPaterno,pagosocupacion.Valor,ventas.Id,configturno.Nombre
UNION ALL
---id ventas
 select
 Ventas.Id as idVentaCorrecta 
 ,Ventas.SerieTicket+ '-'+convert(nvarchar(10), Ventas.FolioTicket)  as Ticket
,Ventas.FechaCreacion as FechaModificacion 
,habitacion.NumeroHabitacion as Habitacion 
, Ventas.ValorConIVA as  Precio
,'Cortesía Habitación' as Concepto
,'Cortesía Habitación' as MotivoCancelacion
,empleado.Nombre+' '+empleado.ApellidoPaterno+' '+empleado.ApellidoPaterno	as UsuarioCancelo
,config.Nombre as Turno 
from SotSchema.Ventas Ventas   WITH(NOLOCK)
INNER JOIN 	SotSchema.VentasRenta ventarenta  WITH(NOLOCK) ON  ventas.Transaccion= ventarenta.Transaccion
INNER JOIN SotSchema.Rentas renta  WITH(NOLOCK) ON 	  renta.Id=	  ventarenta.IdRenta
INNER JOIN SotSchema.PagosRenta pagosrenta  WITH(NOLOCK) ON 	  ventarenta.Id=pagosrenta.IdVentaRenta
INNER JOIN SotSchema.Habitaciones habitacion WITH(NOLOCK)  ON 	renta.IdHabitacion=	habitacion.Id
INNER JOIN SotSchema.Empleados empleado  WITH(NOLOCK) ON  empleado.Id=  Ventas.IdUsuarioModifico
INNER JOIN SotSchema.CortesTurno cortesttuno WITH(NOLOCK) ON  Ventas.IdCorteTurno= cortesttuno.Id
INNER JOIN SotSchema.ConfiguracionesTurno config WITH(NOLOCK) ON  cortesttuno.IdConfiguracionTurno=config.Id
where pagosrenta.IdTipoPago=6

GO