﻿IF EXISTS (select top 1 name FROM sys.views where name = 'VW_ConceptosCancelacionesYCortesias')
BEGIN
	DROP VIEW SotSchema.VW_ConceptosCancelacionesYCortesias
END
GO

create view SotSchema.VW_ConceptosCancelacionesYCortesias
as 
select 'Renta de Habitación Cancelada' as Concepto
UNION ALL
--cancelacion de reservaciones
SELECT 'Reservación Cancelada' as Concepto 
UNION ALL
--cancelacion de consumo interno id consmo INterno
SELECT 'Consumo Interno Cancelado' as  Concepto
UNION ALL
--cancelacion de restaurante --
SELECT 	'Orden Restaurante Cancelado' as  Concepto
UNION ALL
--comanda cancelada	 id comanda
SELECT 'Comanda Cancelada' as Concepto
UNION ALL
--orden de taxi cancelada --id orden taxi
SELECT 'Orden Taxi cancelada' as 	 Concepto
UNION ALL
SELECT 'Cortesía Comandas' as 	 Concepto
UNION ALL
---id ventas
SELECT 'Cortesía Restaurante' as  Concepto
UNION ALL
---id ventas
 select 'Cortesía Habitación' as Concepto

