IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConTipoPagoReservacion') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConTipoPagoReservacion
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene El turno del cual se capturo una Reserva
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creación: 18-06-2019
--Descripcion: Borrado de venta para restarante a partir del ticket
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConTipoPagoReservacion
	@viReservacaion int
AS
DECLARE
	 @vcMensajeerror			nvarchar (150)
	 ,@vcRespuestaTipoPago		nvarchar (150)
	 ,@viTipoPago				int
SET NOCOUNT ON
	IF EXISTS (SELECT Id FROM SotSchema.Reservaciones WITH (NOLOCK) WHERE Id=@viReservacaion)
		BEGIN
			IF EXISTS (SELECT id FROM SotSchema.PagosReservaciones WITH (NOLOCK)  WHERE IdReservacion=@viReservacaion) 
				BEGIN
					SELECT TOP 1 @viTipoPago=IdTipoPago FROM SotSchema.PagosReservaciones WITH (NOLOCK)  WHERE IdReservacion=@viReservacaion
					IF(@viTipoPago =2)
					BEGIN
						SET @vcRespuestaTipoPago='Tarjeta de crédito'
					END
					ELSE IF(@viTipoPago =3)
					BEGIN
						SET @vcRespuestaTipoPago='V Points'
					END
					ELSE IF(@viTipoPago =4)
					BEGIN
						SET @vcRespuestaTipoPago='Reservación'
					END
					ELSE IF(@viTipoPago =5)
					BEGIN
						SET @vcRespuestaTipoPago='Cupón'
					END
					ELSE IF(@viTipoPago =6)
					BEGIN
						SET @vcRespuestaTipoPago='Cortesía'
					END
					ELSE IF(@viTipoPago =7)
					BEGIN
						SET @vcRespuestaTipoPago='Consumo interno'
					END
					ELSE IF(@viTipoPago =8)
					BEGIN
						SET @vcRespuestaTipoPago='Transferencia'
					END
					ELSE
					BEGIN
					SET @vcRespuestaTipoPago='Efectivo'
					END

				END
		END
SELECT @vcRespuestaTipoPago as TipoPago
SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN

GO

