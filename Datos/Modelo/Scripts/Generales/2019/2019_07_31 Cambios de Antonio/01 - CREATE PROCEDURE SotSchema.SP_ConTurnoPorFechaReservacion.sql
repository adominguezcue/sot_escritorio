IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SotSchema.SP_ConTurnoPorFechaReservacion') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE SotSchema.SP_ConTurnoPorFechaReservacion
END
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------
--SP_Obtiene El turno del cual se capturo una Reserva
-- Nombre: Antonio de jesus dominguez cuevas
-- Fecha de creaci�n: 18-06-2019
--Descripcion: Borrado de venta para restarante a partir del ticket
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE SotSchema.SP_ConTurnoPorFechaReservacion
	@vdFechaCreacion DateTime
AS
DECLARE
	 @vcMensajeerror			nvarchar (150)
	,@vcRespuestaTurno			nvarchar (150)
	,@viTurnoConfiguracion		int
SET NOCOUNT ON
	IF EXISTS (SELECT Id FROM SotSchema.Reservaciones WITH (NOLOCK) WHERE FechaCreacion=@vdFechaCreacion)
		BEGIN
			IF EXISTS (SELECT TOP 1 id FROM SotSchema.CortesTurno WITH (NOLOCK)  WHERE FechaInicio<@vdFechaCreacion ORDER BY FechaInicio DESC) 
				BEGIN
					SELECT TOP 1 @viTurnoConfiguracion=IdConfiguracionTurno FROM SotSchema.CortesTurno WITH (NOLOCK)  WHERE FechaInicio<@vdFechaCreacion ORDER BY FechaInicio DESC
					SELECT @vcRespuestaTurno=Nombre FROM SotSchema.ConfiguracionesTurno WITH (NOLOCK) where Id=@viTurnoConfiguracion
				END
		END
SELECT @vcRespuestaTurno as TurnoCaptura
SET NOCOUNT OFF
RETURN 
ERROR:	
	RAISERROR (@vcMensajeerror,16,1)
	SET NOCOUNT ON
	RETURN

GO


