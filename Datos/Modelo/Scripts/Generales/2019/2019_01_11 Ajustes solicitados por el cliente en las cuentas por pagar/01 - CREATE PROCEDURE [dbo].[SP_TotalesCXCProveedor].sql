﻿
/****** Object:  StoredProcedure [dbo].[SP_TotalesCXCProveedor]    Script Date: 11/01/2019 05:25:32 p. m. ******/
DROP PROCEDURE [dbo].[SP_TotalesCXCProveedor]
GO

/****** Object:  StoredProcedure [dbo].[SP_TotalesCXCProveedor]    Script Date: 11/01/2019 05:25:32 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Este procedimiento muestra las sumas de los totales, iva y subtotales de las cuentas que posean el estatus del filtro, agrupadas por proveedor.
--Si @idEstado es null, el sistema mostrará los totales de todas las cuentas.
--
--Es parecido a [dbo].[SP_ZctProveedoresCxP], pero la diferencia es que ese procedimiento almacenado muestra el total acumulado de todas las cuentas de un proveedor que posee al menos una en el estatus solicitado,
--mientras que este procedimiento almacenado solamente suma las que cumplen con el filto.
CREATE PROCEDURE [dbo].[SP_TotalesCXCProveedor]
	@idEstado int = null
as
begin

SELECT
p.Cod_Prov as Cod_Prov, 
p.Nom_Prov, 
SUM(cxp.subtotal) AS subtotal, 
SUM(cxp.iva) AS iva, 
SUM(cxp.total) AS total
FROM
dbo.ZctCatProv AS p 
INNER JOIN dbo.ZctEncOC AS oc
	ON oc.Cod_Prov = p.Cod_Prov 
INNER JOIN dbo.ZctEncCuentasPagar AS cxp
	ON oc.Cod_OC = cxp.Cod_OC
where @idEstado is null
or cxp.idEstado = @idEstado
GROUP BY p.Cod_Prov, p.Nom_Prov

end
GO


