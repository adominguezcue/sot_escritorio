﻿/****** Object:  StoredProcedure [dbo].[SP_ZctCatArt_Busqueda]    Script Date: 28/01/2019 08:37:01 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SP_ZctCatArt_Busqueda]
	@Codigo varchar(20),
	@soloInventariable bit = 0
AS
	BEGIN
		set @Codigo = replace(@Codigo, '''','-' )

		if(@soloInventariable = 0)
			SELECT 
			Cod_Art, 
			Desc_Art, 
			Exist_Art AS ColExist, 
			0 as ColSurt,
			Prec_Art, 
			Cos_Art, 
			Cod_TpArt,
			cod_alm,
			ZctCatMar.Desc_Mar AS marca,
			OmitirIVA
			FROM ZctCatArt 
			LEFT JOIN ZctCatMar 
				ON ZctCatMar.Cod_Mar = ZctCatArt.Cod_Mar  
			WHERE Cod_Art = @Codigo or upc= @Codigo  
		else
			SELECT 
			Cod_Art, 
			Desc_Art, 
			Exist_Art AS ColExist, 
			0 as ColSurt,
			Prec_Art, 
			Cos_Art, 
			ZctCatArt.Cod_TpArt,
			cod_alm,
			ZctCatMar.Desc_Mar AS marca,
			OmitirIVA
			FROM ZctCatArt
			INNER JOIN ZctCatTpArt
				ON ZctCatArt.Cod_TpArt = ZctCatTpArt.Cod_TpArt
			LEFT JOIN ZctCatMar 
				ON ZctCatMar.Cod_Mar = ZctCatArt.Cod_Mar
			WHERE (Cod_Art = @Codigo or upc= @Codigo )
			AND ZctCatTpArt.Inventariable = 1
	END
