﻿alter table dbo.ZctCatTpArt
add Sincronizar bit null
go

update dbo.ZctCatTpArt
set Sincronizar = 1
where Cod_TpArt <> 'AC'

update dbo.ZctCatTpArt
set Sincronizar = 0
where Cod_TpArt = 'AC'