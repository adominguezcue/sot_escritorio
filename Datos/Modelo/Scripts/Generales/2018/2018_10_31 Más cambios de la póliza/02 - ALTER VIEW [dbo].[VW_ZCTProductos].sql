﻿/****** Object:  View [dbo].[VW_ZCTProductos]    Script Date: 31/10/2018 12:51:36 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[VW_ZCTProductos] as
SELECT        
ZctCatArt.Cod_Art, 
Ltrim(ZctCatArt.Desc_Art) + ' ' +  isnull(ZctCatMar.Desc_Mar, '') as Desc_Art, 
ZctCatArt.Cos_Art, 
ZctCatPar.taller, 
ZctCatArt.update_web, 
ZctCatArt.uuid, 
ZctCatArt.desactivar, 
ZctCatArt.tiempo_vida, 
ZctCatArt.cod_alm, 
ZctCatAlm.Desc_CatAlm, 
ZctCatArt.Prec_Art,
ISNULL(CAST(
    CASE ZctCatTpArt.Sincronizar
        WHEN 1 THEN 1
        ELSE 0  
    END AS bit), 0) AS Sincronizar
FROM ZctCatArt 
LEFT OUTER JOIN ZctCatMar 
	ON ZctCatArt.Cod_Mar = ZctCatMar.Cod_Mar AND ZctCatArt.Cod_Mar = ZctCatMar.Cod_Mar
LEFT OUTER JOIN ZctCatAlm 
	ON ZctCatArt.cod_alm = ZctCatAlm.Cod_Alm
INNER JOIN ZctCatTpArt
	ON ZctCatArt.Cod_TpArt = ZctCatTpArt.Cod_TpArt
CROSS JOIN ZctCatPar
						 
GO


