﻿-- Creating table 'MontosNoReembolsablesRenta'
CREATE TABLE [SotSchema].[MontosNoReembolsablesRenta] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdVentaRenta] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'MontosNoReembolsablesRenta'
ALTER TABLE [SotSchema].[MontosNoReembolsablesRenta]
ADD CONSTRAINT [PK_MontosNoReembolsablesRenta]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [IdVentaRenta] in table 'MontosNoReembolsablesRenta'
ALTER TABLE [SotSchema].[MontosNoReembolsablesRenta]
ADD CONSTRAINT [FK_VentaRentaMontoNoReembolsable]
    FOREIGN KEY ([IdVentaRenta])
    REFERENCES [SotSchema].[VentasRenta]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VentaRentaMontoNoReembolsable'
CREATE INDEX [IX_FK_VentaRentaMontoNoReembolsable]
ON [SotSchema].[MontosNoReembolsablesRenta]
    ([IdVentaRenta]);
GO