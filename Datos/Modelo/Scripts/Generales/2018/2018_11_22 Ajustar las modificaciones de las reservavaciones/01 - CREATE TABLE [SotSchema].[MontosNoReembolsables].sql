﻿-- Creating table 'MontosNoReembolsables'
CREATE TABLE [SotSchema].[MontosNoReembolsables] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdReservacion] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [ValorSinIVA] decimal(19,4)  NOT NULL,
    [ValorConIVA] decimal(19,4)  NOT NULL,
    [ValorIVA] decimal(19,4)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'MontosNoReembolsables'
ALTER TABLE [SotSchema].[MontosNoReembolsables]
ADD CONSTRAINT [PK_MontosNoReembolsables]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [IdReservacion] in table 'MontosNoReembolsables'
ALTER TABLE [SotSchema].[MontosNoReembolsables]
ADD CONSTRAINT [FK_ReservacionMontoNoReembolsable]
    FOREIGN KEY ([IdReservacion])
    REFERENCES [SotSchema].[Reservaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservacionMontoNoReembolsable'
CREATE INDEX [IX_FK_ReservacionMontoNoReembolsable]
ON [SotSchema].[MontosNoReembolsables]
    ([IdReservacion]);
GO