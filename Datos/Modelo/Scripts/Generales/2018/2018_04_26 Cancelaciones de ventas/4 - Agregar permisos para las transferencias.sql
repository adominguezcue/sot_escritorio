﻿;with PagosE as
(
select * from Seguridad.PermisosPagos
where IdFormaPago = 1
and Activo = 1
and idRol not in (select IdRol from Seguridad.PermisosPagos
					where IdFormaPago = 8
					and Activo = 1)
)
insert into Seguridad.PermisosPagos
(Activo, IdFormaPago, IdRol, FechaCreacion, FechaModificacion, FechaEliminacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino)
select Activo, 8, IdRol, FechaCreacion, FechaModificacion, FechaEliminacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino
from PagosE
