﻿begin tran t1

;with datosR as
(
select 
	Id as IdReservacion, 
	CodigoReserva as Referencia,
	ValorConIVA as Valor, 
	case when IdEstado = 4 then 0 else 1 end as Activo,
	8 as IdTipoPago,
	convert(varchar(max), NEWID()) as Transaccion,
	FechaCreacion,
	FechaModificacion,
	FechaEliminacion,
	IdUsuarioCreo,
	IdUsuarioModifico,
	IdUsuarioElimino
from SotSchema.Reservaciones
where Id not in (select IdReservacion from SotSchema.PagosReservaciones)
)
insert into SotSchema.PagosReservaciones
(Valor, IdReservacion, Referencia, NumeroTarjeta, IdTipoPago, Activo, Transaccion, IdTipoTarjeta, FechaCreacion, FechaModificacion, FechaEliminacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino)
select Valor, IdReservacion, Referencia, '', IdTipoPago, Activo, Transaccion, NULL, FechaCreacion, FechaModificacion, FechaEliminacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino 
from datosR

;with ventasR as
(
select 
	MIN(convert(int,Activo)) as Activa, 
	1 as Correcta, 
	case when MIN(convert(int,Activo)) = 1 then 0 else 1 end as Cancelada,
	0 as Subida,
	min(FechaCreacion) as FechaCreacion,
	min(FechaModificacion) as FechaModificacion,
	min(FechaEliminacion) as FechaEliminacion,
	min(FechaEliminacion) as FechaCancelacion,
	min(IdUsuarioCreo) as IdUsuarioCreo,
	min(IdUsuarioModifico) as IdUsuarioModifico,
	min(IdUsuarioElimino) as IdUsuarioElimino,
	min(IdUsuarioElimino) as IdUsuarioCancelo,
	sum(Valor) / 1.16 as ValorSinIVA,
	sum(Valor) as ValorConIVA,
	(sum(Valor) / 1.16) * 0.16 as ValorIVA,
	sum(Valor) as SumaPagos,
	0 as MontoIgnorable,
	Transaccion,
	'RE' as SerieTicket,
	ROW_NUMBER() over (order by Transaccion) as FolioTicket,
	0 as IdCorteTurno,
	6 as IdClasificacionVenta,
	NULL as ErrorUltimoIntento,
	0 as EsErrorSubida,
	0.16 as IVAEnCurso
from SotSchema.PagosReservaciones
where Transaccion not in (select Transaccion from SotSchema.Ventas)
group by Transaccion
)
insert into SotSchema.Ventas
(Activa, Correcta, Cancelada, Subida, FechaCreacion, FechaModificacion, FechaEliminacion, FechaCancelacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino, IdUsuarioCancelo, ValorSinIVA, ValorConIVA, ValorIVA, SumaPagos, MontoIgnorable, Transaccion, SerieTicket, FolioTicket, IdCorteTurno, IdClasificacionVenta, ErrorUltimoIntento, EsErrorSubida, IVAEnCurso)
select 
	Activa, Correcta, Cancelada, Subida, FechaCreacion, FechaModificacion, FechaEliminacion, FechaCancelacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino, IdUsuarioCancelo, ValorSinIVA, ValorConIVA, ValorIVA, SumaPagos, MontoIgnorable, Transaccion, SerieTicket, FolioTicket, c.Id, IdClasificacionVenta, vr.ErrorUltimoIntento, vr.EsErrorSubida, IVAEnCurso
from ventasR vr
left join SotSchema.CortesTurno c
	on vr.FechaCreacion between c.FechaInicio and isnull(c.FechaCorte, GETDATE())

select * from SotSchema.Ventas
where IdClasificacionVenta = 6

insert into SotSchema.Preventas
(Activa, FolioTicket, IdClasificacionVenta, SerieTicket)
select Activa, FolioTicket, IdClasificacionVenta, SerieTicket from SotSchema.Ventas
where IdClasificacionVenta = 6

update SotSchema.FoliosClasificacionVenta
set FolioActual = (select MAX(isnull(v.FolioTicket, 0)) from SotSchema.Ventas v
				   where IdClasificacionVenta = 6)
where IdClasificacionVenta = 6

select * from SotSchema.FoliosClasificacionVenta

rollback tran t1
--commit tran t1