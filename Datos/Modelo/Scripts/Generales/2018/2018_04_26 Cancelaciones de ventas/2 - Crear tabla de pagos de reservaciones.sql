﻿-- Creating table 'PagosReservaciones'
CREATE TABLE [SotSchema].[PagosReservaciones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] decimal(19,4)  NOT NULL,
    [IdReservacion] int  NOT NULL,
    [Referencia] nvarchar(max)  NOT NULL,
    [NumeroTarjeta] nvarchar(max)  NOT NULL,
    [IdTipoPago] int  NOT NULL,
    [Activo] bit  NOT NULL,
    [Transaccion] nvarchar(max)  NOT NULL,
    [IdTipoTarjeta] int  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating primary key on [Id] in table 'PagosReservaciones'
ALTER TABLE [SotSchema].[PagosReservaciones]
ADD CONSTRAINT [PK_PagosReservaciones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating foreign key on [IdReservacion] in table 'PagosReservaciones'
ALTER TABLE [SotSchema].[PagosReservaciones]
ADD CONSTRAINT [FK_ReservacionPagoReservacion]
    FOREIGN KEY ([IdReservacion])
    REFERENCES [SotSchema].[Reservaciones]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservacionPagoReservacion'
CREATE INDEX [IX_FK_ReservacionPagoReservacion]
ON [SotSchema].[PagosReservaciones]
    ([IdReservacion]);
GO