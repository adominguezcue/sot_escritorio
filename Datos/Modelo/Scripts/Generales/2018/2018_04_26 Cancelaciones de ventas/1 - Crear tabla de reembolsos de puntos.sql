﻿begin tran t1

-- Creating table 'ReembolsosPuntos'
CREATE TABLE [SotSchema].[ReembolsosPuntos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Tarjeta] nvarchar(max)  NOT NULL,
    [MontoReembolsar] decimal(19,4)  NOT NULL,
    [Activo] bit  NOT NULL,
    [Abonado] bit  NOT NULL,
    [ErrorControlado] bit  NOT NULL,
    [MotivoError] nvarchar(max)  NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [NumeroServicio] nvarchar(max)  NOT NULL
);

-- Creating primary key on [Id] in table 'ReembolsosPuntos'
ALTER TABLE [SotSchema].[ReembolsosPuntos]
ADD CONSTRAINT [PK_ReembolsosPuntos]
    PRIMARY KEY CLUSTERED ([Id] ASC);

commit tran t1