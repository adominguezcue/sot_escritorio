﻿INSERT INTO [SotSchema].[FoliosClasificacionVenta]
           ([IdClasificacionVenta]
           ,[Serie]
           ,[Descripcion]
           ,[FolioActual])
     VALUES
           (1, 'H', 'Habitación', 0),
           (2, 'RS', 'Room Service', 0),
           (3, 'R', 'Restaurante', 0),
           (4, 'TP', 'Tarjeta de puntos', 0),
           (5, 'T', 'Taxi', 0)
GO