﻿begin tran t1

delete SotSchema.HistorialesComanda
delete SotSchema.ArticulosComandas
delete SotSchema.PagosComandas
delete SotSchema.Comandas

delete SotSchema.HistorialesConsumoInterno
delete SotSchema.ArticulosConsumoInterno
delete SotSchema.PagosConsumoInterno
delete SotSchema.ConsumosInternos

delete SotSchema.HistorialesOrdenRestaurante
delete SotSchema.ArticulosOrdenesRestaurante
delete SotSchema.PagosOcupacionMesa
delete SotSchema.OrdenesRestaurante
delete SotSchema.OcupacionesMesa
delete SotSchema.OrdenesTaxis

delete SotSchema.AbonosPuntos
delete SotSchema.AutomovilesRenta
delete SotSchema.PaquetesRenta
delete SotSchema.Extensiones
delete SotSchema.PersonasExtra
delete SotSchema.DetallesPago
delete SotSchema.PagosRenta
delete SotSchema.VentasRenta
delete SotSchema.Rentas

delete  SotSchema.Ventas

delete SotSchema.PagosTarjetaPuntos
update SotSchema.TarjetasPuntos
set Activo = 1, IdEstado = 1

update SotSchema.Mesas
set IdEstado = 1

update SotSchema.Habitaciones
set IdEstadoHabitacion = 1
where IdEstadoHabitacion = 2
or IdEstadoHabitacion = 7

update SotSchema.FoliosClasificacionVenta
set FolioActual = 0

commit tran t1