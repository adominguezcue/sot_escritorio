﻿IF OBJECT_ID(N'[SotSchema].[BloqueadoresFolios]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[BloqueadoresFolios];
GO

-- Creating table 'BloqueadoresFolios'
CREATE TABLE [SotSchema].[BloqueadoresFolios] (
    [IdClasificacionVenta] int  NOT NULL,
    [UUID] uniqueidentifier  NOT NULL,
    [Fecha] datetime  NOT NULL
);
GO

-- Creating primary key on [IdClasificacionVenta] in table 'BloqueadoresFolios'
ALTER TABLE [SotSchema].[BloqueadoresFolios]
ADD CONSTRAINT [PK_BloqueadoresFolios]
    PRIMARY KEY CLUSTERED ([IdClasificacionVenta] ASC);
GO






IF OBJECT_ID(N'[SotSchema].[FoliosClasificacionVenta]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[FoliosClasificacionVenta];
GO

-- Creating table 'FoliosClasificacionVenta'
CREATE TABLE [SotSchema].[FoliosClasificacionVenta] (
    [IdClasificacionVenta] int  NOT NULL,
    [Serie] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [FolioActual] int  NOT NULL
);
GO

-- Creating primary key on [IdClasificacionVenta] in table 'FoliosClasificacionVenta'
ALTER TABLE [SotSchema].[FoliosClasificacionVenta]
ADD CONSTRAINT [PK_FoliosClasificacionVenta]
    PRIMARY KEY CLUSTERED ([IdClasificacionVenta] ASC);
GO




IF OBJECT_ID(N'[SotSchema].[Preventas]', 'U') IS NOT NULL
    DROP TABLE [SotSchema].[Preventas];
GO

-- Creating table 'Preventas'
CREATE TABLE [SotSchema].[Preventas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activa] bit  NOT NULL,
    [FolioTicket] int  NOT NULL,
    [IdClasificacionVenta] int  NOT NULL,
    [SerieTicket] nvarchar(max)  NOT NULL
);
GO

-- Creating primary key on [Id] in table 'Preventas'
ALTER TABLE [SotSchema].[Preventas]
ADD CONSTRAINT [PK_Preventas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.Ventas
	DROP CONSTRAINT FK_CorteTurnoVenta
GO
ALTER TABLE SotSchema.CortesTurno SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE SotSchema.Tmp_Ventas
	(
	Id int NOT NULL IDENTITY (1, 1),
	Activa bit NOT NULL,
	Correcta bit NOT NULL,
	Cancelada bit NOT NULL,
	Subida bit NOT NULL,
	FechaCreacion datetime NOT NULL,
	FechaModificacion datetime NOT NULL,
	FechaEliminacion datetime NULL,
	FechaCancelacion datetime NULL,
	IdUsuarioCreo int NOT NULL,
	IdUsuarioModifico int NOT NULL,
	IdUsuarioElimino int NULL,
	IdUsuarioCancelo int NULL,
	ValorSinIVA decimal(19, 4) NOT NULL,
	ValorConIVA decimal(19, 4) NOT NULL,
	ValorIVA decimal(19, 4) NOT NULL,
	SumaPagos decimal(19, 4) NOT NULL,
	MontoIgnorable decimal(19, 4) NOT NULL,
	Transaccion nvarchar(MAX) NOT NULL,
	SerieTicket nvarchar(MAX) NOT NULL,
	FolioTicket int NOT NULL,
	IdCorteTurno int NOT NULL,
	IdClasificacionVenta int NOT NULL,
	ErrorUltimoIntento nvarchar(MAX) NULL,
	EsErrorSubida bit NOT NULL,
	IVAEnCurso decimal(19, 4) NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE SotSchema.Tmp_Ventas SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT SotSchema.Tmp_Ventas ON
GO
IF EXISTS(SELECT * FROM SotSchema.Ventas)
	 EXEC('INSERT INTO SotSchema.Tmp_Ventas (Id, Activa, Correcta, Cancelada, Subida, FechaCreacion, FechaModificacion, FechaEliminacion, FechaCancelacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino, IdUsuarioCancelo, ValorSinIVA, ValorConIVA, ValorIVA, SumaPagos, MontoIgnorable, Transaccion, SerieTicket, FolioTicket, IdCorteTurno, IdClasificacionVenta, ErrorUltimoIntento, EsErrorSubida, IVAEnCurso)
		SELECT Id, Activa, Correcta, Cancelada, Subida, FechaCreacion, FechaModificacion, FechaEliminacion, FechaCancelacion, IdUsuarioCreo, IdUsuarioModifico, IdUsuarioElimino, IdUsuarioCancelo, ValorSinIVA, ValorConIVA, ValorIVA, SumaPagos, MontoIgnorable, Transaccion, '''', Ticket, IdCorteTurno, IdClasificacionVenta, ErrorUltimoIntento, EsErrorSubida, IVAEnCurso FROM SotSchema.Ventas WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT SotSchema.Tmp_Ventas OFF
GO
DROP TABLE SotSchema.Ventas
GO
EXECUTE sp_rename N'SotSchema.Tmp_Ventas', N'Ventas', 'OBJECT' 
GO
ALTER TABLE SotSchema.Ventas ADD CONSTRAINT
	PK_Ventas PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_FK_CorteTurnoVenta ON SotSchema.Ventas
	(
	IdCorteTurno
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE SotSchema.Ventas ADD CONSTRAINT
	FK_CorteTurnoVenta FOREIGN KEY
	(
	IdCorteTurno
	) REFERENCES SotSchema.CortesTurno
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
