﻿/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCancelacionesTurno]    Script Date: 14/08/2018 01:04:39 p. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerCancelacionesTurno]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerCancelacionesTurno]    Script Date: 14/08/2018 01:04:39 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [SotSchema].[SP_ObtenerCancelacionesTurno]
	@idCorteTurno int
AS
/*
public enum ClasificacionesVenta 
{ 
    Habitacion = 1,
    RoomService = 2,
    Restaurante = 3,
    TarjetaPuntos = 4,
    Taxi = 5,
    Reservacion = 6
}
*/

;with Restaurante
as
(
--consumos internos
select
3 as IdClasificacionVenta, --Restaurante
Sum(isnull(v.ValorConIVA, 0)) as Total,
COUNT(v.Id) as Cantidad
from SotSchema.ConsumosInternos v
inner join SotSchema.CortesTurno c
	on v.IdCorteTurno = c.Id
where v.IdEstado = 6 --cancelada
and c.Id = @idCorteTurno
and (c.FechaCorte is NULL
or (v.FechaEliminacion is not NULL
and v.FechaEliminacion <= c.FechaCorte))
group by c.Id
union
--órdenes de restaurante
select
3 as IdClasificacionVenta, --Restaurante
Sum(isnull(v.ValorConIVA, 0)) as Total,
COUNT(v.Id) as Cantidad
from SotSchema.OrdenesRestaurante v
inner join SotSchema.OcupacionesMesa pre
	on v.IdOcupacionMesa = pre.Id
inner join SotSchema.CortesTurno c
	on pre.IdCorte = c.Id
where v.IdEstado = 6--cancelada   --3 --ocupación cancelada
and c.Id = @idCorteTurno
and (c.FechaCorte is NULL
or (v.FechaEliminacion is not NULL
and v.FechaEliminacion <= c.FechaCorte))
group by c.Id
)
select IdClasificacionVenta, Sum(isnull(Total,0)) as Total, Sum(isnull(Cantidad, 0)) as Cantidad from Restaurante
group by IdClasificacionVenta
union
--habitaciones
select
1 as IdClasificacionVenta, --Habitación
Sum(isnull(v.ValorConIVA, 0)) as Total,
COUNT(v.Id) as Cantidad
from SotSchema.VentasRenta v
inner join SotSchema.CortesTurno c
	on v.FechaCreacion between c.FechaInicio and c.FechaCorte
where v.Activo = 0
and c.Id = @idCorteTurno
and (c.FechaCorte is NULL
or (v.FechaEliminacion is not NULL
and v.FechaEliminacion <= c.FechaCorte))
group by c.Id
union
--comandas
select
2 as IdClasificacionVenta, --Room Service
Sum(isnull(v.ValorConIVA, 0)) as Total,
COUNT(v.Id) as Cantidad
from SotSchema.Comandas v
inner join SotSchema.CortesTurno c
	on v.IdCorte = c.Id
where v.IdEstado = 6 --cancelada
and c.Id = @idCorteTurno
and (c.FechaCorte is NULL
or (v.FechaEliminacion is not NULL
and v.FechaEliminacion <= c.FechaCorte))
group by c.Id
union
--taxis
select
5 as IdClasificacionVenta, --Taxi
Sum(isnull(v.Precio, 0)) as Total,
COUNT(v.Id) as Cantidad
from SotSchema.OrdenesTaxis v
inner join SotSchema.CortesTurno c
	on v.FechaCreacion between c.FechaInicio and c.FechaCorte
where v.IdEstado = 3 --cancelada
and c.Id = @idCorteTurno
and (c.FechaCorte is NULL
or (v.FechaEliminacion is not NULL
and v.FechaEliminacion <= c.FechaCorte))
group by c.Id

--Las tarjetas de puntos y las reservaciones no se toman en cuenta porque no poseen estados intermedios entre la alta y el cobro
GO


