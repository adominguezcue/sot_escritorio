﻿  begin tran t1

  update dbo.ZctDetCuentasPagar
  set codtipo_pago = 1
  where codtipo_pago in (2,3,4)

  delete dbo.ZctCatTipoPago
  where codtipo_pago in (2,3,4)

  update dbo.ZctCatTipoPago
  set desc_pago = 'CAJA'
  where codtipo_pago = 1

  commit tran t1