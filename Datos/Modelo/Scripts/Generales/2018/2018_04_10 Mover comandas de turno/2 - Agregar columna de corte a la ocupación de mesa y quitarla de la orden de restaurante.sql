﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD
	IdCorte int NULL
GO
ALTER TABLE SotSchema.OcupacionesMesa SET (LOCK_ESCALATION = TABLE)
GO

update om
set om.IdCorte = ct.Id 
from SotSchema.OcupacionesMesa om
inner join SotSchema.CortesTurno ct
	on om.FechaCreacion between ct.FechaInicio and ISNULL(ct.FechaCorte, GETDATE())

COMMIT



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_DatosFiscalesOcupacionMesa
GO
ALTER TABLE SotSchema.DatosFiscales SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_EmpleadoOcupacionMesa
GO
ALTER TABLE SotSchema.Empleados SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_MesaOcupacionMesa
GO
ALTER TABLE SotSchema.Mesas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE SotSchema.Tmp_OcupacionesMesa
	(
	Id int NOT NULL IDENTITY (1, 1),
	IdMesa int NOT NULL,
	IdMesero int NOT NULL,
	Activa bit NOT NULL,
	FechaCreacion datetime NOT NULL,
	FechaModificacion datetime NOT NULL,
	FechaEliminacion datetime NULL,
	FechaCobro datetime NULL,
	IdUsuarioModifico int NOT NULL,
	IdUsuarioCreo int NOT NULL,
	IdUsuarioElimino int NULL,
	IdEstado int NOT NULL,
	IdDatosFiscales int NULL,
	Transaccion nvarchar(MAX) NOT NULL,
	IdPreventa int NOT NULL,
	IdCorte int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE SotSchema.Tmp_OcupacionesMesa SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT SotSchema.Tmp_OcupacionesMesa ON
GO
IF EXISTS(SELECT * FROM SotSchema.OcupacionesMesa)
	 EXEC('INSERT INTO SotSchema.Tmp_OcupacionesMesa (Id, IdMesa, IdMesero, Activa, FechaCreacion, FechaModificacion, FechaEliminacion, FechaCobro, IdUsuarioModifico, IdUsuarioCreo, IdUsuarioElimino, IdEstado, IdDatosFiscales, Transaccion, IdPreventa, IdCorte)
		SELECT Id, IdMesa, IdMesero, Activa, FechaCreacion, FechaModificacion, FechaEliminacion, FechaCobro, IdUsuarioModifico, IdUsuarioCreo, IdUsuarioElimino, IdEstado, IdDatosFiscales, Transaccion, IdPreventa, IdCorte FROM SotSchema.OcupacionesMesa WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT SotSchema.Tmp_OcupacionesMesa OFF
GO
ALTER TABLE SotSchema.OrdenesRestaurante
	DROP CONSTRAINT FK_OcupacionMesaOrdenRestaurante
GO
ALTER TABLE SotSchema.PagosOcupacionMesa
	DROP CONSTRAINT FK_OcupacionMesaPagoOcupacionMesa
GO
DROP TABLE SotSchema.OcupacionesMesa
GO
EXECUTE sp_rename N'SotSchema.Tmp_OcupacionesMesa', N'OcupacionesMesa', 'OBJECT' 
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	PK_OcupacionesMesa PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_FK_MesaOcupacionMesa ON SotSchema.OcupacionesMesa
	(
	IdMesa
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_FK_EmpleadoOcupacionMesa ON SotSchema.OcupacionesMesa
	(
	IdMesero
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_FK_DatosFiscalesOcupacionMesa ON SotSchema.OcupacionesMesa
	(
	IdDatosFiscales
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_MesaOcupacionMesa FOREIGN KEY
	(
	IdMesa
	) REFERENCES SotSchema.Mesas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_EmpleadoOcupacionMesa FOREIGN KEY
	(
	IdMesero
	) REFERENCES SotSchema.Empleados
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_DatosFiscalesOcupacionMesa FOREIGN KEY
	(
	IdDatosFiscales
	) REFERENCES SotSchema.DatosFiscales
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.PagosOcupacionMesa ADD CONSTRAINT
	FK_OcupacionMesaPagoOcupacionMesa FOREIGN KEY
	(
	IdOcupacionMesa
	) REFERENCES SotSchema.OcupacionesMesa
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.PagosOcupacionMesa SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OrdenesRestaurante ADD CONSTRAINT
	FK_OcupacionMesaOrdenRestaurante FOREIGN KEY
	(
	IdOcupacionMesa
	) REFERENCES SotSchema.OcupacionesMesa
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OrdenesRestaurante SET (LOCK_ESCALATION = TABLE)
GO
COMMIT





-----

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_DatosFiscalesOcupacionMesa
GO
ALTER TABLE SotSchema.DatosFiscales SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_EmpleadoOcupacionMesa
GO
ALTER TABLE SotSchema.Empleados SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_MesaOcupacionMesa
GO
ALTER TABLE SotSchema.Mesas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OrdenesRestaurante
	DROP CONSTRAINT FK_CorteTurnoOrdenRestaurante
GO
ALTER TABLE SotSchema.CortesTurno SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OrdenesRestaurante
	DROP CONSTRAINT FK_OcupacionMesaOrdenRestaurante
GO
ALTER TABLE SotSchema.PagosOcupacionMesa
	DROP CONSTRAINT FK_OcupacionMesaPagoOcupacionMesa
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_MesaOcupacionMesa FOREIGN KEY
	(
	IdMesa
	) REFERENCES SotSchema.Mesas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_EmpleadoOcupacionMesa FOREIGN KEY
	(
	IdMesero
	) REFERENCES SotSchema.Empleados
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_DatosFiscalesOcupacionMesa FOREIGN KEY
	(
	IdDatosFiscales
	) REFERENCES SotSchema.DatosFiscales
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.PagosOcupacionMesa ADD CONSTRAINT
	FK_OcupacionMesaPagoOcupacionMesa FOREIGN KEY
	(
	IdOcupacionMesa
	) REFERENCES SotSchema.OcupacionesMesa
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.PagosOcupacionMesa SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
DROP INDEX IX_FK_CorteTurnoOrdenRestaurante ON SotSchema.OrdenesRestaurante
GO
ALTER TABLE SotSchema.OrdenesRestaurante ADD CONSTRAINT
	FK_OcupacionMesaOrdenRestaurante FOREIGN KEY
	(
	IdOcupacionMesa
	) REFERENCES SotSchema.OcupacionesMesa
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OrdenesRestaurante
	DROP COLUMN IdCorte
GO
ALTER TABLE SotSchema.OrdenesRestaurante SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
