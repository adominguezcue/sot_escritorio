﻿
/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerHistorialesComandas]    Script Date: 07/02/2018 06:20:41 p. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerHistorialesComandas]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerHistorialesComandas]    Script Date: 07/02/2018 06:20:41 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date: 19 de enero de 2018
-- Description:	Retorna los historiales de las comandas dentro de las fechas especificadas
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerHistorialesComandas]
	-- Add the parameters for the stored procedure here
	@fechaInicio datetime,
	@fechaFin datetime,
	@ordenTurno int = NULL,
	@idTipo int = NULL,
	@idEstado int = NULL,
	@idLinea int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

;with datos as
(
	select 
	1 as IdTipo,
	c.Id as IdComanda,
	c.IdCorte,
	c.IdPreventa,
	(case when c.Transaccion is NULL OR c.Transaccion = '' then 'RS-' + convert(varchar, c.Id) else c.Transaccion end) as Transaccion,
	c.FechaCreacion,
	c.FechaInicio as Fecha, 
	c.ValorConIVA as Monto,
	c.IdEstado,
	hc.IdEstado as IdEstadoHistorico,
	hc.FechaInicio as FechaHistorica,
	cast((case when c.FechaCreacion <> c.FechaInicio then 1 else 0 end) as bit) as Modificada,
	c.FechaModificacion,
	isnull(
	substring(
		(
			select ', '+ cast(pgc.IdTipoPago as varchar)  as [text()]
			from SotSchema.PagosComandas pgc
			where pgc.IdComanda  = c.Id
			group by pgc.IdTipoPago
			order by pgc.IdTipoPago
			for xml path (''), root('FormaP'), type
		).value('/FormaP[1]', 'varchar(max)'), 2, 1000), '') FormasPago,
	c.IdEmpleadoCobro
	from SotSchema.Comandas c
	left join SotSchema.HistorialesComanda hc
		on c.Id = hc.IdComanda
	cross apply (select top 1 Id from SotSchema.ArticulosComandas aCom
			     inner join dbo.ZctCatArt art
					on aCom.IdArticulo = art.Cod_Art
				 where aCom.IdComanda = c.Id and (@idLinea is NULL or art.Cod_Linea = @idLinea)) artCom
	where c.FechaCreacion between @fechaInicio and @fechaFin
	union all
	select 
	2 as IdTipo,
	c.Id as IdComanda,
	om.IdCorte,
	om.IdPreventa,
	(case when om.Transaccion is NULL OR om.Transaccion = '' then 'R-' + convert(varchar, c.Id) else om.Transaccion end) as Transaccion,
	c.FechaCreacion,
	c.FechaInicio as Fecha, 
	c.ValorConIVA as Monto,
	c.IdEstado,
	hc.IdEstado as IdEstadoHistorico,
	hc.FechaInicio as FechaHistorica,
	cast((case when c.FechaCreacion <> c.FechaInicio then 1 else 0 end) as bit) as Modificada,
	c.FechaModificacion,
	isnull(
	substring(
		(
			select ', '+ cast(pgc.IdTipoPago as varchar)  as [text()]
			from SotSchema.PagosOcupacionMesa pgc
			where pgc.IdOcupacionMesa = om.Id
			group by pgc.IdTipoPago
			order by pgc.IdTipoPago
			for xml path (''), root('FormaP'), type
		).value('/FormaP[1]', 'varchar(max)'), 2, 1000), '') FormasPago,
	om.IdMesero as IdEmpleadoCobro
	from SotSchema.OrdenesRestaurante c
	inner join SotSchema.OcupacionesMesa om
		on c.IdOcupacionMesa = om.Id
	left join SotSchema.HistorialesOrdenRestaurante hc
		on c.Id = hc.IdOrden
	cross apply (select top 1 Id from SotSchema.ArticulosOrdenesRestaurante aCom
			     inner join dbo.ZctCatArt art
					on aCom.IdArticulo = art.Cod_Art
				 where aCom.IdOrdenRestaurante = c.Id and (@idLinea is NULL or art.Cod_Linea = @idLinea)) artCom
	where c.FechaCreacion between @fechaInicio and @fechaFin
	union all
	select
	3 as IdTipo,
	c.Id as IdComanda,
	c.IdCorteTurno as IdCorte,
	c.IdPreventa,
	(case when c.Transaccion is NULL OR c.Transaccion = '' then 'CI-' + convert(varchar, c.Id) else c.Transaccion end) as Transaccion,
	c.FechaCreacion,
	c.FechaInicio as Fecha, 
	c.ValorConIVA as Monto,
	c.IdEstado,
	hc.IdEstado as IdEstadoHistorico,
	hc.FechaInicio as FechaHistorica,
	cast((case when c.FechaCreacion <> c.FechaInicio then 1 else 0 end) as bit) as Modificada,
	c.FechaModificacion,
	isnull(
	substring(
		(
			select ', '+ cast(pgc.IdTipoPago as varchar)  as [text()]
			from SotSchema.PagosConsumoInterno pgc
			where pgc.IdConsumoInterno = c.Id
			group by pgc.IdTipoPago
			order by pgc.IdTipoPago
			for xml path (''), root('FormaP'), type
		).value('/FormaP[1]', 'varchar(max)'), 2, 1000), '') FormasPago,
	c.IdMesero as IdEmpleadoCobro
	from SotSchema.ConsumosInternos c
	left join SotSchema.HistorialesConsumoInterno hc
		on c.Id = hc.IdConsumoInterno
	cross apply (select top 1 Id from SotSchema.ArticulosConsumoInterno aCom
			     inner join dbo.ZctCatArt art
					on aCom.IdArticulo = art.Cod_Art
				 where aCom.IdConsumoInterno = c.Id and (@idLinea is NULL or art.Cod_Linea = @idLinea)) artCom
	where c.FechaCreacion between @fechaInicio and @fechaFin
)
select
	c.IdTipo,
	c.Transaccion,
	c.IdComanda,
	configct.Orden as Turno,
	c.FechaCreacion,
	c.Fecha, 
	ISNULL(v.FolioTicket, 0) as FolioTicket,
	ISNULL(v.SerieTicket,'') as SerieTicket,
	c.Monto,
	(case when emp.Id is not NULL then emp.Nombre + ' ' + emp.ApellidoPaterno + ' ' + emp.ApellidoMaterno else '' end) as Mesero,
	c.IdEstado,
	c.IdEstadoHistorico,
	c.FechaHistorica,
	c.Modificada,
	c.FechaModificacion,
	c. FormasPago
	from datos c
	inner join SotSchema.CortesTurno ct
		on c.IdCorte = ct.Id
	inner join SotSchema.ConfiguracionesTurno configct
		on ct.IdConfiguracionTurno = configct.Id
	left join SotSchema.Preventas v
		on c.IdPreventa = v.Id
	left join SotSchema.Empleados emp
		on c.IdEmpleadoCobro = emp.Id
	where (@ordenTurno is NULL or configct.Orden = @ordenTurno)
	and (@idTipo is NULL or c.IdTipo = @idTipo)
	and (@idEstado is NULL or c.IdEstado = @idEstado)
END
GO


