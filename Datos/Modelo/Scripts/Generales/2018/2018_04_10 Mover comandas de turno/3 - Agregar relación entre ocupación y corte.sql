﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_DatosFiscalesOcupacionMesa
GO
ALTER TABLE SotSchema.DatosFiscales SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_EmpleadoOcupacionMesa
GO
ALTER TABLE SotSchema.Empleados SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.CortesTurno SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OcupacionesMesa
	DROP CONSTRAINT FK_MesaOcupacionMesa
GO
ALTER TABLE SotSchema.Mesas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OrdenesRestaurante
	DROP CONSTRAINT FK_OcupacionMesaOrdenRestaurante
GO
ALTER TABLE SotSchema.PagosOcupacionMesa
	DROP CONSTRAINT FK_OcupacionMesaPagoOcupacionMesa
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_MesaOcupacionMesa FOREIGN KEY
	(
	IdMesa
	) REFERENCES SotSchema.Mesas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_EmpleadoOcupacionMesa FOREIGN KEY
	(
	IdMesero
	) REFERENCES SotSchema.Empleados
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_DatosFiscalesOcupacionMesa FOREIGN KEY
	(
	IdDatosFiscales
	) REFERENCES SotSchema.DatosFiscales
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_OcupacionesMesa_CortesTurno FOREIGN KEY
	(
	IdCorte
	) REFERENCES SotSchema.CortesTurno
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OcupacionesMesa SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.PagosOcupacionMesa ADD CONSTRAINT
	FK_OcupacionMesaPagoOcupacionMesa FOREIGN KEY
	(
	IdOcupacionMesa
	) REFERENCES SotSchema.OcupacionesMesa
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.PagosOcupacionMesa SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.OrdenesRestaurante ADD CONSTRAINT
	FK_OcupacionMesaOrdenRestaurante FOREIGN KEY
	(
	IdOcupacionMesa
	) REFERENCES SotSchema.OcupacionesMesa
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.OrdenesRestaurante SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
