﻿
/****** Object:  StoredProcedure [SotSchema].[SP_MoverComandas]    Script Date: 10/04/2018 07:02:36 p. m. ******/
DROP PROCEDURE [SotSchema].[SP_MoverComandas]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_MoverComandas]    Script Date: 10/04/2018 07:02:36 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date: 10 de abril de 2018
-- Description:	Mueve las comandas, consumos internos y órdenes de restaurante al turno que está abierto, también reenumera estos elementos
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_MoverComandas]
	@idCorte int
AS
BEGIN
	
	declare @idCorteActual int

	select @idCorteActual = ISNULL(MAX(Id), 0) from SotSchema.CortesTurno
	where IdEstado = 0

	if(@idCorte <> @idCorteActual and @idCorteActual <> 0)
	begin

		declare @ordenComanda int

		declare @estadosComandas table
		(
			IdEstado int
		)

		declare @estadosOcupaciones table
		(
			IdEstado int
		)

		declare @pendientes table
		(
			Id int identity(1,1),
			Orden int,
			IdComanda int null,
			IdConsumoInterno int null,
			IdOrdenRestaurante int null,
			IdOcupacion int null
		)

		insert into @estadosComandas
		values
		(1),--Preparación
		(2),--Por entregar
		(3)--Por cobrar
		--(4),--Por pagar
		--(5),--Cobrada
		--(6)--Cancelada

		insert into @estadosOcupaciones
		values
		(1)--En proceso

		;with foliosComandas as
		(
		select ISNULL(MAX(Orden), 0) as Orden from SotSchema.Comandas
		where IdCorte = @idCorteActual
		union
		select ISNULL(MAX(Orden), 0) as Orden from SotSchema.ConsumosInternos
		where IdCorteTurno = @idCorteActual
		union
		select ISNULL(MAX(ord.Orden), 0) as Orden from SotSchema.OrdenesRestaurante ord
		inner join SotSchema.OcupacionesMesa om
			on ord.IdOcupacionMesa = om.Id
		where om.IdCorte = @idCorteActual
		)
		select @ordenComanda = MAX(Orden) from foliosComandas
		-----------------------------------------------------------------------------------------
		;with ordenes as
		(
		select Orden, Id as IdComanda, NULL as IdConsumoInterno, NULL as IdOrdenRestaurante, NULL as IdOcupacion from SotSchema.Comandas
		where IdCorte = @idCorte
		and IdEstado in (select * from @estadosComandas)
		union all
		select Orden, NULL, Id, NULL, NULL from SotSchema.ConsumosInternos
		where IdCorteTurno = @idCorte
		and IdEstado in (select * from @estadosComandas)
		union all
		select Orden, NULL, NULL, ord.Id, om.Id from SotSchema.OrdenesRestaurante ord
		inner join SotSchema.OcupacionesMesa om
			on ord.IdOcupacionMesa = om.Id
		where om.IdCorte = @idCorte
		and om.IdEstado in (select * from @estadosOcupaciones)
		)
		insert into @pendientes
		(Orden, IdComanda, IdConsumoInterno, IdOrdenRestaurante, IdOcupacion)
		select Orden, IdComanda, IdConsumoInterno, IdOrdenRestaurante, IdOcupacion from ordenes
		order by Orden
		-------------------------------------------------------------------------------------
		update c
		set c.IdCorte = @idCorteActual,
			c.Orden = pen.Id + @ordenComanda
		from SotSchema.Comandas c
		inner join @pendientes pen
			on c.Id = pen.IdComanda

		update c
		set c.IdCorteTurno = @idCorteActual,
			c.Orden = pen.Id + @ordenComanda
		from SotSchema.ConsumosInternos c
		inner join @pendientes pen
			on c.Id = pen.IdConsumoInterno

		update c
		set	c.Orden = pen.Id + @ordenComanda
		from SotSchema.OrdenesRestaurante c
		inner join @pendientes pen
			on c.Id = pen.IdOrdenRestaurante

		update c
		set	c.IdCorte = @idCorteActual
		from SotSchema.OcupacionesMesa c
		inner join @pendientes pen
			on c.Id = pen.IdOcupacion

		delete @pendientes
		delete @estadosOcupaciones
		delete @estadosComandas
	end
END
GO


