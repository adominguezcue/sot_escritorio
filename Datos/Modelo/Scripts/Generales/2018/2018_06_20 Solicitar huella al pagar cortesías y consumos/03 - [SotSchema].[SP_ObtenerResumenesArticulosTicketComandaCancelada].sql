﻿USE [SOT_PROD]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketComandaCancelada]    Script Date: 21/06/2018 06:03:21 p. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketComandaCancelada]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketComandaCancelada]    Script Date: 21/06/2018 06:03:21 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date:     21 de junio de 2018
-- Description:	Retorna un resumen con la cantidad total (con iva) y concepto por artículo
--				perteneciente a la comanda con el id porporcionado
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketComandaCancelada]
	@idComanda int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @IVA decimal(19,3)
	declare @fechaCancelacion datetime

    select @IVA = Iva_CatPar from dbo.ZctCatPar

	select @fechaCancelacion = FechaEliminacion from SotSchema.Comandas
	where Id = @idComanda

    select Cantidad, Desc_Art as Nombre, ROUND(PrecioUnidad * (1 + @IVA), 2) as PrecioUnidad, lin.IdLineaBase, EsCortesia, art.Cod_Art as Codigo
	from SotSchema.ArticulosComandas aCom
    left join dbo.ZctCatArt art
	   on aCom.IdArticulo = art.Cod_Art
	left join dbo.ZctCatLinea lin
		on art.Cod_Linea = lin.Cod_Linea
    where Activo = 0
	and FechaEliminacion >= @fechaCancelacion
    and IdComanda = @idComanda
END


GO


