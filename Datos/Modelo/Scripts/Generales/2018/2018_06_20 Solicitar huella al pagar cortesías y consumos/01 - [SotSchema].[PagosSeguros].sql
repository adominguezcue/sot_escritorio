﻿DROP TABLE [SotSchema].[PagosSeguros]
GO

/****** Object:  Table [SotSchema].[PagosSeguros]    Script Date: 20/06/2018 09:45:57 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [SotSchema].[PagosSeguros](
	[IdTipoPago] [int] NOT NULL,
 CONSTRAINT [PK_PagosSeguros] PRIMARY KEY CLUSTERED 
(
	[IdTipoPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


