﻿/****** Object:  View [dbo].[VW_ZctCatPar]    Script Date: 21/06/2018 11:58:46 a. m. ******/
DROP VIEW [dbo].[VW_ZctCatPar]
GO

/****** Object:  View [dbo].[VW_ZctCatPar]    Script Date: 21/06/2018 11:58:46 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/****** Script para el comando SelectTopNRows de SSMS  ******/
CREATE VIEW [dbo].[VW_ZctCatPar]
AS
SELECT *, GETDATE() as FechaModificacion FROM dbo.ZctCatPar
GO