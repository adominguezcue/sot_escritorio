﻿USE [SOT_PROD]
GO

/****** Object:  View [SotSchema].[VW_VentasCorrectas]    Script Date: 22/06/2018 11:58:10 a. m. ******/
DROP VIEW [SotSchema].[VW_VentasCorrectas]
GO

/****** Object:  View [SotSchema].[VW_VentasCorrectas]    Script Date: 22/06/2018 11:58:10 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [SotSchema].[VW_VentasCorrectas]
AS
/*
public enum ClasificacionesVenta 
{ 
    Habitacion = 1,
    RoomService = 2,
    Restaurante = 3,
    TarjetaPuntos = 4,
    Taxi = 5,
    Reservacion = 6
}
*/

--declare @ordenTurno int = null, 
--		@fechaInicio datetime = null, 
--		@fechaFin datetime = null

--ventas
select
v.Id,
NULL as IdVentaRenta,
NULL as IdComanda,
NULL as IdConsumoInterno,
NULL as IdOcupacionMesa,
NULL as IdOrdenTaxi,
c.NumeroCorte,
v.FolioTicket,
v.SerieTicket,
v.Cancelada,
Cast(1 as bit) as Cobrada,
IdClasificacionVenta,
ValorConIVA,
FechaCreacion,
coalesce(
    substring(
        (
            select ','+ pagos.IdP  as [text()]
            from 
			(
			select 
				convert(varchar(max), pc.IdTipoPago) as IdP
			from SotSchema.PagosComandas pc
			where pc.Transaccion = v.Transaccion
			union
			select 
				convert(varchar(max), pc.IdTipoPago) as IdP
			from SotSchema.PagosConsumoInterno pc
			where pc.Transaccion = v.Transaccion
			union
			select 
				convert(varchar(max), pc.IdTipoPago) as IdP
			from SotSchema.PagosOcupacionMesa pc
			where pc.Transaccion = v.Transaccion
			union
			select 
				convert(varchar(max), pc.IdTipoPago) as IdP
			from SotSchema.PagosRenta pc
			where pc.Transaccion = v.Transaccion
			union
			select 
				convert(varchar(max), pc.IdTipoPago) as IdP
			from SotSchema.PagosReservaciones pc
			where pc.Transaccion = v.Transaccion
			union
			select 
				convert(varchar(max), pc.IdTipoPago) as IdP
			from SotSchema.PagosTarjetaPuntos pc
			where pc.Transaccion = v.Transaccion
			)
			as pagos
            for xml path (''), root('MyString'), type
        ).value('/MyString[1]', 'varchar(max)'), 2, 1000), '') Pagos,
v.MotivoCancelacion
from SotSchema.Ventas v
inner join SotSchema.CortesTurno c
	on v.IdCorteTurno = c.Id
where v.Correcta = 1
union
--habitaciones
select
NULL as Id,
v.Id as IdVentaRenta,
NULL as IdComanda,
NULL as IdConsumoInterno,
NULL as IdOcupacionMesa,
NULL as IdOrdenTaxi,
c.NumeroCorte,
pre.FolioTicket,
pre.SerieTicket,
Cast(1 as bit) as Cancelada,
v.Cobrada,
1 as IdClasificacionVenta, --Habitación
v.ValorConIVA,
v.FechaCreacion,
'' as Pagos,
v.MotivoCancelacion
from SotSchema.VentasRenta v
inner join SotSchema.CortesTurno c
	on v.FechaCreacion between c.FechaInicio and c.FechaCorte
inner join SotSchema.Preventas pre
	on v.IdPreventa = pre.Id
outer apply 
(
select count(*) as cantidad from SotSchema.Ventas vf
where vf.SerieTicket = pre.SerieTicket
and vf.FolioTicket = pre.FolioTicket
) as ventaF
where v.Activo = 0
and v.Cobrada = 0
and ventaF.cantidad = 0
union
--comandas
select
NULL as Id,
NULL as IdVentaRenta,
v.Id as IdComanda,
NULL as IdConsumoInterno,
NULL as IdOcupacionMesa,
NULL as IdOrdenTaxi,
c.NumeroCorte,
pre.FolioTicket,
pre.SerieTicket,
Cast(1 as bit) as Cancelada,
Cast(0 as bit) as Cobrada,
2 as IdClasificacionVenta, --Room Service
v.ValorConIVA,
v.FechaCreacion,
'' as Pagos,
v.MotivoCancelacion
from SotSchema.Comandas v
inner join SotSchema.CortesTurno c
	on v.IdCorte = c.Id
inner join SotSchema.Preventas pre
	on v.IdPreventa = pre.Id
outer apply 
(
select count(*) as cantidad from SotSchema.Ventas vf
where vf.SerieTicket = pre.SerieTicket
and vf.FolioTicket = pre.FolioTicket
) as ventaF
where v.IdEstado = 6 --cancelada
and ventaF.cantidad = 0
union
--consumos internos
select
NULL as Id,
NULL as IdVentaRenta,
NULL as IdComanda,
v.Id as IdConsumoInterno,
NULL as IdOcupacionMesa,
NULL as IdOrdenTaxi,
c.NumeroCorte,
pre.FolioTicket,
pre.SerieTicket,
Cast(1 as bit) as Cancelada,
Cast(0 as bit) as Cobrada,
3 as IdClasificacionVenta, --Restaurante
v.ValorConIVA,
v.FechaCreacion,
'' as Pagos,
v.MotivoCancelacion
from SotSchema.ConsumosInternos v
inner join SotSchema.CortesTurno c
	on v.IdCorteTurno = c.Id
inner join SotSchema.Preventas pre
	on v.IdPreventa = pre.Id
outer apply 
(
select count(*) as cantidad from SotSchema.Ventas vf
where vf.SerieTicket = pre.SerieTicket
and vf.FolioTicket = pre.FolioTicket
) as ventaF
where v.IdEstado = 6 --cancelada
and ventaF.cantidad = 0
union
--órdenes de restaurante
select
NULL as Id,
NULL as IdVentaRenta,
NULL as IdComanda,
NULL as IdConsumoInterno,
v.Id as IdOcupacionMesa,
NULL as IdOrdenTaxi,
c.NumeroCorte,
pre.FolioTicket,
pre.SerieTicket,
Cast(1 as bit) as Cancelada,
Cast(0 as bit) as Cobrada,
3 as IdClasificacionVenta, --Restaurante
ordenesR.ValorConIVA,
v.FechaCreacion,
'' as Pagos,
v.MotivoCancelacion
from SotSchema.OcupacionesMesa v
inner join SotSchema.CortesTurno c
	on v.IdCorte = c.Id
inner join SotSchema.Preventas pre
	on v.IdPreventa = pre.Id
cross apply 
(
select sum(ValorConIVA) as ValorConIVA from SotSchema.OrdenesRestaurante ors
where ors.IdOcupacionMesa = v.Id
and ors.IdEstado = 6--cancelada
) as ordenesR
outer apply 
(
select count(*) as cantidad from SotSchema.Ventas vf
where vf.SerieTicket = pre.SerieTicket
and vf.FolioTicket = pre.FolioTicket
) as ventaF
where v.IdEstado = 3 --ocupación cancelada
and ventaF.cantidad = 0
and ordenesR.ValorConIVA is not NULL
union
--taxis
select
NULL as Id,
NULL as IdVentaRenta,
NULL as IdComanda,
NULL as IdConsumoInterno,
NULL as IdOcupacionMesa,
v.Id as IdOrdenTaxi,
c.NumeroCorte,
pre.FolioTicket,
pre.SerieTicket,
Cast(1 as bit) as Cancelada,
Cast(0 as bit) as Cobrada,
5 as IdClasificacionVenta, --Taxi
v.Precio as ValorConIVA,
v.FechaCreacion,
'' as Pagos,
v.MotivoCancelacion
from SotSchema.OrdenesTaxis v
inner join SotSchema.CortesTurno c
	on v.FechaCreacion between c.FechaInicio and c.FechaCorte
inner join SotSchema.Preventas pre
	on v.IdPreventa = pre.Id
outer apply 
(
select count(*) as cantidad from SotSchema.Ventas vf
where vf.SerieTicket = pre.SerieTicket
and vf.FolioTicket = pre.FolioTicket
) as ventaF
where v.IdEstado = 3 --cancelada
and ventaF.cantidad = 0

--Las tarjetas de puntos y las reservaciones no se toman en cuenta porque no poseen estados intermedios entre la alta y el cobro
GO


