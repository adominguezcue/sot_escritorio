﻿
/****** Object:  StoredProcedure [dbo].[SP_ZctInvEncFis]    Script Date: 07/12/2018 09:35:13 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arturo Hernandez
-- Create date: 15-09-2009
-- Description:	Inventario Físico
--A6580009-9A5C-446D-B13D-51631B8B36FA.zct
--Updated: 07-12-2018
-- =============================================
ALTER PROCEDURE [dbo].[SP_ZctInvEncFis]
	@TpConsulta Int,
	@Folio varchar(10),
	@Cod_Alm int,
	@InvFis_EncFis bit,
	@InvAle_EncFis bit,
	@Apl_AjustCosto bit,
	@Apl_AjustExist bit,
	@Termino_EncFis bit,
	@Cod_Dpto int = null,
	@Cod_Linea int = null
		
AS
BEGIN
	if (@TpConsulta = '1')
		BEGIN
			IF (@Folio  = 0)
					Select Folio, Cod_Alm ,InvFis_EncFis, InvAle_EncFis , Apl_AjustCosto ,Apl_AjustExist, Termino_EncFis  FROM ZctInvEncFis where Folio = @Folio
				ELSE
					Select Folio, Cod_Alm as Almacén ,InvFis_EncFis as Físico, InvAle_EncFis as Aleatorio, Termino_EncFis as Terminado FROM ZctInvEncFis 
		END
	ELSE
		if (@TpConsulta = '2')
			BEGIN
				if not exists(Select Folio FROM ZctInvEncFis where Folio = @Folio )
					INSERT INTO ZctInvEncFis (Folio, Cod_Alm, InvFis_EncFis, InvAle_EncFis, Apl_AjustCosto, Apl_AjustExist, Termino_EncFis, Cod_Dpto, Cod_Linea) 
					VALUES (@Folio, @Cod_Alm, @InvFis_EncFis, @InvAle_EncFis, @Apl_AjustCosto, @Apl_AjustExist, @Termino_EncFis, @Cod_Dpto, @Cod_Linea)
				ELSE
					BEGIN
						UPDATE ZctInvEncFis  SET 
						Cod_Alm = @Cod_Alm , 
						InvFis_EncFis = @InvFis_EncFis, 
						InvAle_EncFis = @InvAle_EncFis, 
						Apl_AjustCosto = @Apl_AjustCosto, 
						Apl_AjustExist = @Apl_AjustExist, 
						Termino_EncFis = @Termino_EncFis,
						Cod_Dpto = @Cod_Dpto,
						Cod_Linea = @Cod_Linea
						WHERE Folio = @Folio

						declare @year  as integer
						declare  @month as integer
						Select @year= year(FchyHrReg), @month=month(FchyHrReg) FROM ZctInvEncFis where Folio = @Folio

						EXEC SP_REPORTEGENERALWEB @year, @month
					END
				
			END
		else
			if (@TpConsulta = '3')
				DELETE FROM ZctInvEncFis WHERE Folio = @Folio
END
