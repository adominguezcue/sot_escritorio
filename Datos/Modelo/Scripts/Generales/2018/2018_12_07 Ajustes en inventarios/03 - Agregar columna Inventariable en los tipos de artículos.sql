﻿SELECT *
  FROM [dbo].[ZctCatTpArt]

  alter table [dbo].[ZctCatTpArt]
  add Inventariable bit null
  go

  update [dbo].[ZctCatTpArt]
  set Inventariable = 0
  where Cod_TpArt = 'AC' 
  
  update [dbo].[ZctCatTpArt]
  set Inventariable = 1
  where Cod_TpArt <> 'AC'