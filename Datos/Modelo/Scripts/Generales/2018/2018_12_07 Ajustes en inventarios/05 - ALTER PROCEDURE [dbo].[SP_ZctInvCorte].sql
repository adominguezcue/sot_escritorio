/****** Object:  StoredProcedure [dbo].[SP_ZctInvCorte]    Script Date: 07/12/2018 01:06:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arturo Hernández Lomelí
-- Create date: 17/Mayo/2008
-- Description:	Obtiene el corte para el inventario
-- 88568137-43AB-49C9-ABF4-38B4057A0767.zct
-- Updated: 07/Dic/2018
-- =============================================
ALTER PROCEDURE [dbo].[SP_ZctInvCorte]
	@TpConsulta int,
	@Folio varchar(10),
    @Cod_Alm  int, 
	@Cod_Linea int = 0,
	@Cod_Dpto int= 0   
AS
BEGIN
	DECLARE @Cod_Art varchar(20),
		@Exist_Art int,
		@CostoProm_Art money
 


	if not exists (select Folio from ZctInvCorteFis where @Folio = Folio  AND  Cod_Alm =@Cod_Alm  )
		BEGIN
			--Inventario General
			if  (@TpConsulta = 1) 
			--DECLARAMOS EL CURSOR
				if @Cod_Linea > 0 
					DECLARE CorteInventario cursor for
					SELECT Cod_Art FROM  ZctCatArt 
					where Cod_Dpto = @Cod_Dpto 
					AND Cod_Linea = @Cod_Linea
					AND Cod_TpArt in (select Cod_TpArt from dbo.ZctCatTpArt where Inventariable = 1)
				else
					if @Cod_Dpto > 0 
						DECLARE CorteInventario cursor for
						SELECT Cod_Art FROM  ZctCatArt  
						where Cod_Dpto = @Cod_Dpto
						AND Cod_TpArt in (select Cod_TpArt from dbo.ZctCatTpArt where Inventariable = 1)
					else
						DECLARE CorteInventario cursor for
						SELECT Cod_Art FROM  ZctCatArt
						WHERE Cod_TpArt in (select Cod_TpArt from dbo.ZctCatTpArt where Inventariable = 1)
			else	
				--Inventario Aleatorio
				DECLARE CorteInventario cursor for
				SELECT TOP 10 Cod_Art FROM  ZctCatArt 
				WHERE Cod_TpArt in (select Cod_TpArt from dbo.ZctCatTpArt where Inventariable = 1)
				ORDER BY NEWID()

				  
				--ABRIMOS EL CURSOR
				 OPEN CorteInventario
				 
				--LEEMOS EL PRIMER REGISTRO
				 FETCH NEXT FROM CorteInventario INTO  @Cod_Art
				 --MIENTRAS EL STATUS SEA 0
				 WHILE @@fetch_status = 0
					BEGIN
						--REALIZAMOS LAS ACCIONES QUE QUERAMOS
						set @Exist_Art = 0 
						set @CostoProm_Art =0 
						SELECT @Exist_Art = isnull(Exist_Art, 0) FROM ZctArtXAlm WHERE (Cod_Art = @Cod_Art AND Cod_Alm = @Cod_Alm)
						if @Exist_Art >0
							SELECT  @CostoProm_Art = isnull(CostoProm_Art/Exist_Art, 0) FROM ZctArtXAlm WHERE (Cod_Art = @Cod_Art AND Cod_Alm = @Cod_Alm)
						if @CostoProm_Art <=0
							SELECT  @CostoProm_Art = isnull(Cos_Art, 0) FROM ZctCatArt Where Cod_Art = @Cod_Art
						

						INSERT INTO ZctInvCorteFis (Folio , Cod_Alm , Cod_Art , Exist_Art , CostoProm_Art, Conteo1_Art	, Conteo2_Art, Conteo3_Art, Diferencia_Art)
						values (@Folio , @Cod_Alm , @Cod_Art , @Exist_Art , @CostoProm_Art,0 ,0 ,0,0)
				 
						FETCH NEXT FROM CorteInventario INTO  @Cod_Art
					END
				 
				CLOSE CorteInventario
				DEALLOCATE CorteInventario
 
   
	
	END
END


