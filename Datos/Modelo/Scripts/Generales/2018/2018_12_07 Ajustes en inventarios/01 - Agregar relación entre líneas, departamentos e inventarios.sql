﻿alter table ZctInvEncFis
add Cod_Dpto int null,
	Cod_Linea int null
go

ALTER TABLE ZctInvEncFis
ADD CONSTRAINT FK_ZctCatDpto_ZctInvEncFis
FOREIGN KEY (Cod_Dpto) REFERENCES ZctCatDpto(Cod_Dpto)
GO

ALTER TABLE ZctInvEncFis
ADD CONSTRAINT FK_ZctCatLinea_ZctInvEncFis
FOREIGN KEY (Cod_Linea) REFERENCES ZctCatLinea(Cod_Linea)
GO
