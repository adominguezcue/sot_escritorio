/****** Script for SelectTopNRows command from SSMS  ******/
begin tran t1

;with detallesInventario as
(SELECT Folio, art.Cod_Dpto, art.Cod_Linea, count(*) as CantidadArticulos

FROM [dbo].[ZctInvCorteFis] invcort
join dbo.ZctCatArt art
	on invcort.Cod_Art = art.Cod_Art
group by Folio, art.Cod_Dpto, art.Cod_Linea)
, detallesFinos as
(
select Folio, COUNT(DISTINCT Cod_Dpto) as CantidadDepartamentos, COUNT(DISTINCT Cod_Linea) as CantidadLineas, MAX(Cod_Dpto) as CodigoDep, max(Cod_Linea) as CodigoLin, SUM(CantidadArticulos) as CantidadArticulos from detallesInventario
group by Folio
)
--select * from detallesInventario
--order by Folio, CantidadArticulos
update inv
set inv.Cod_Dpto = (case when df.CantidadDepartamentos = 1 then CodigoDep else null end),
	inv.Cod_Linea = (case when df.CantidadLineas = 1 then CodigoLin else null end)
from [dbo].[ZctInvEncFis] inv
inner join detallesFinos df
	on inv.Folio = df.Folio
where inv.InvAle_EncFis = 0

select * from dbo.[ZctInvEncFis]

rollback tran t1