﻿delete cred from Seguridad.Credenciales cred
inner join Seguridad.Usuarios usu
	on cred.IdUsuario = usu.Id
left join SotSchema.Empleados em
	on usu.IdEmpleado =em.Id
where (em.Id is NULL or em.Activo = 0) and usu.Activo = 1

update usu 
set usu.Activo = 0,
FechaEliminacion = ISNULL(em.FechaEliminacion, GETDATE()),
FechaModificacion = ISNULL(em.FechaModificacion, GETDATE()),
IdUsuarioElimino = ISNULL(em.IdUsuarioElimino, 0),
IdUsuarioModifico = ISNULL(em.IdUsuarioModifico, 0)
from Seguridad.Usuarios usu
left join SotSchema.Empleados em
	on usu.IdEmpleado =em.Id
where (em.Id is NULL or em.Activo = 0) and usu.Activo = 1

select * from Seguridad.Credenciales cred
inner join Seguridad.Usuarios usu
	on cred.IdUsuario = usu.Id
left join SotSchema.Empleados em
	on usu.IdEmpleado =em.Id
where (em.Id is NULL or em.Activo = 0) and usu.Activo = 1

select * from Seguridad.Usuarios usu
left join SotSchema.Empleados em
	on usu.IdEmpleado =em.Id
where (em.Id is NULL or em.Activo = 0) and usu.Activo = 1