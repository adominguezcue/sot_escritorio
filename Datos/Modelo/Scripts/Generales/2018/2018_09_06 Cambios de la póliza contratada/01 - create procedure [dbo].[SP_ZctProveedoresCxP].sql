﻿/****** Object:  StoredProcedure [dbo].[SP_ZctProveedoresCxP]    Script Date: 06/09/2018 10:01:44 a. m. ******/
DROP PROCEDURE [dbo].[SP_ZctProveedoresCxP]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctProveedoresCxP]    Script Date: 06/09/2018 10:01:44 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[SP_ZctProveedoresCxP]
	@idEstado int
as
begin

;with datos as
(
SELECT
p.Cod_Prov as Cod_Prov, 
p.Nom_Prov, 
SUM(cxp.subtotal) AS subtotal, 
SUM(cxp.iva) AS iva, 
SUM(cxp.total) AS total,
MAX(case when cxp.IdEstado = @idEstado then 1 else 0 end) as ContieneEstado
FROM
dbo.ZctCatProv AS p 
INNER JOIN dbo.ZctEncOC AS oc
	ON oc.Cod_Prov = p.Cod_Prov 
INNER JOIN dbo.ZctEncCuentasPagar AS cxp
	ON oc.Cod_OC = cxp.Cod_OC
GROUP BY p.Cod_Prov, p.Nom_Prov
)
select Cod_Prov, Nom_Prov, subtotal, iva, total from datos
where ContieneEstado = 1

end
GO


