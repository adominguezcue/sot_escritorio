﻿select * from SotSchema.OrdenesRestaurante
where ValorIVA > ValorSinIVA

select * from SotSchema.OrdenesRestaurante
where ValorIVA < ValorSinIVA

declare @ivasOR table
(
	IdOrden int,
	ValorIVA decimal(19,4) not null,
	ValorSinIVA decimal(19,4) not null,
	ValorConIVA decimal(19,4) not null
)

begin transaction t1

insert into @ivasOR
(IdOrden, ValorIVA, ValorSinIVA, ValorConIVA)
select Id, ValorSinIVA, ValorIVA, ValorConIVA from SotSchema.OrdenesRestaurante
where ValorIVA > ValorSinIVA

update ord
set ord.ValorIVA = ivas.ValorIVA,
	ord.ValorSinIVA = ivas.ValorSinIVA
from SotSchema.OrdenesRestaurante ord
inner join @ivasOR ivas
	on ord.Id = ivas.IdOrden


select * from SotSchema.OrdenesRestaurante
where ValorIVA > ValorSinIVA

select * from SotSchema.OrdenesRestaurante
where ValorIVA < ValorSinIVA

select * from SotSchema.OrdenesRestaurante ord
inner join @ivasOR ivas
	on ord.Id = ivas.IdOrden
where ord.ValorConIVA <> ivas.ValorConIVA

rollback transaction t1