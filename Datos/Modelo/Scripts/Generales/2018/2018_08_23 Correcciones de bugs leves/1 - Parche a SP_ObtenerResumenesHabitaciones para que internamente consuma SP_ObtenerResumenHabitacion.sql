﻿USE [SOT_PROD]
GO
/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesHabitaciones]    Script Date: 23/08/2018 01:38:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date: 20 de agosto de 2018
-- Description:	Retorna un resumen del estatus de las habitaciones activas (estado, recamareras, tiempo, etc)
-- =============================================
ALTER PROCEDURE [SotSchema].[SP_ObtenerResumenesHabitaciones]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



declare @estado_Libre int = 1,
		@estado_PendienteCobro int = 2,
		@estado_Preparada int = 3,
		@estado_Reservada int = 4,
		@estado_PreparadaReservada int = 5,
		@estado_Bloqueada int = 6,
		@estado_Ocupada int = 7,
		@estado_Sucia int = 8,
		@estado_Limpieza int = 9,
		@estado_Supervision int = 10,
		@estado_Mantenimiento int = 11
/*
declare @resumen table
(
	IdHabitacion int,
	Posicion int,
	Piso int,
	IdTipoHabitacion int,
	NombreTipo varchar(max),

	txtNumero varchar(max),
	txtTipo varchar(max),
	estado int,
	fechaModificacion datetime,

	txtDetalle varchar(max),
	estadoComanda int,
	esVencida bit,
	tieneTarjetaV bit,

	fechaFin datetime,
	txtAuto varchar(max),
	idRenta int,

	idTipoLimpieza int,
	fechaTarea datetime,
	minutosLimpieza int--,
	--idTarea int
)*/

declare @resumen table
(
	Id int identity(1,1),
	IdHabitacion int,
	IdTipoHabitacion int,
	NombreTipo varchar(max),
	NumeroHabitacion varchar(max),
	DatoBase varchar(max),
	Detalle varchar(max),
	IdEstado int,
	EsVencida bit,
	IdEstadoComanda int,
	TieneTarjetaV bit,
	Posicion int,
	Piso int--,

	--fechaModificacion datetime,

	--txtDetalle varchar(max),

	--fechaFin datetime,
	--txtAuto varchar(max),
	--idRenta int,

	--idTipoLimpieza int,
	--fechaTarea datetime,
	--minutosLimpieza int--,
	----idTarea int
)

declare @idsHabitaciones table
(
	id int identity(1,1),
	idH int
)

/*
declare @defaultAuto varchar(5) = 'A pie'
declare @fechaActual datetime = GETDATE()





insert into @resumen
(IdHabitacion, Posicion, Piso, IdTipoHabitacion, NombreTipo, txtNumero, txtTipo, estado, fechaModificacion, minutosLimpieza)
select
	h.Id,
	h.Posicion,
	h.Piso,
	h.IdTipoHabitacion,
	t.Descripcion,
	h.NumeroHabitacion,
	t.Descripcion,
	h.IdEstadoHabitacion,
	h.FechaModificacion,
	t.MinutosSucia
from SotSchema.Habitaciones h
inner join SotSchema.TiposHabitacion t
	on h.IdTipoHabitacion = t.Id
where h.Activo = 1 -- h.Id = @idHabitacion


--declare @idEstadoTarea int = 1

	--declare @idTarea int

--@estado_Libre

update @resumen
set esVencida = 0,
txtDetalle = Format(((Abs(datediff(MINUTE, @fechaActual , fechaModificacion)) % 1440) / 60), '0#') + ':' + Format((Abs(datediff(MINUTE, @fechaActual , fechaModificacion)) % 60), '0#')
where estado = @estado_Libre

--@estado_PendienteCobro

update r
set txtAuto = ISNULL(automovil.Matricula, @defaultAuto),
	fechaFin = ISNULL(ext.FechaFin, renta.FechaFin)
from @resumen r
inner join SotSchema.Rentas as renta
	on r.IdHabitacion = renta.IdHabitacion
left join SotSchema.AutomovilesRenta automovil
	on renta.Id = automovil.IdRenta and automovil.Activa = 1
left join SotSchema.VentasRenta venta
	on renta.Id = venta.IdRenta and venta.Activo = 1
outer apply
(select MAX(FechaFin) as FechaFin from SotSchema.Extensiones as extension where venta.Id = extension.IdVentaRenta and extension.Activa = 1) as ext
where renta.Activa = 1
and r.estado = @estado_PendienteCobro

update @resumen
set txtTipo = txtAuto,
	esVencida = 0,
	txtDetalle = ''
where fechaFin is NULL
and estado = @estado_PendienteCobro

update @resumen
set txtTipo = case when fechaFin <= @fechaActual then 'Vencida' else txtAuto end,
	esVencida = case when fechaFin <= @fechaActual then 1 else 0 end,
	txtDetalle = /*Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + */Format(((Abs(datediff(MINUTE, fechaFin , @fechaActual)) % 1440) / 60), '0#') + ':' + Format((Abs(datediff(MINUTE, fechaFin , @fechaActual)) % 60), '0#')
where fechaFin is not NULL
and estado = @estado_PendienteCobro

--@estado_Preparada

update @resumen
set txtDetalle = 'Preparada',
	esVencida = 0
where estado = @estado_Preparada

--@estado_Reservada

update @resumen
set txtDetalle = 'Reservada',
	esVencida = 0
where estado = @estado_Reservada

--@estado_PreparadaReservada

update r
set r.txtTipo = 'Reservación',
r.esVencida = 0,
r.txtDetalle = res.CodigoReserva
from @resumen r
inner join SotSchema.Reservaciones res
	on r.IdHabitacion = res.IdHabitacion
where r.estado = @estado_PreparadaReservada
and res.IdEstado = 2--Confirmada


--@estado_Bloqueada

update r
set r.txtDetalle = bloqueo.Motivo,
	r.esVencida = 0
from @resumen r
inner join SotSchema.BloqueosHabitaciones bloqueo
	on r.IdHabitacion = bloqueo.IdHabitacion
where r.estado = @estado_Bloqueada
and bloqueo.Activo = 1


--@estado_Ocupada

update r
set 
	r.idRenta = renta.Id,
	r.txtAuto = ISNULL(automovil.Matricula, @defaultAuto),
	r.fechaFin = ISNULL(ext.FechaFin, renta.FechaFin),
	r.tieneTarjetaV = case when renta.NumeroTarjeta is NULL then 0 else 1 end
from @resumen r
inner join SotSchema.Rentas as renta
	on r.IdHabitacion = renta.IdHabitacion
left join SotSchema.AutomovilesRenta automovil
	on renta.Id = automovil.IdRenta and automovil.Activa = 1
left join SotSchema.VentasRenta venta
	on renta.Id = venta.IdRenta and venta.Activo = 1
outer apply
(select MAX(FechaFin) as FechaFin from SotSchema.Extensiones as extension where venta.Id = extension.IdVentaRenta and extension.Activa = 1) as ext
where renta.Activa = 1
and r.estado = @estado_Ocupada

update @resumen 
set txtTipo = txtAuto,
	esVencida = 0,
	txtDetalle = ''
where fechaFin is NULL
and estado = @estado_Ocupada

update r 
set r.txtTipo = case when fechaFin <= @fechaActual then 'Vencida' else txtAuto end,
	r.esVencida = case when fechaFin <= @fechaActual then 1 else 0 end,
	r.txtDetalle = /*Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + */Format(((Abs(datediff(MINUTE, fechaFin , @fechaActual)) % 1440) / 60), '0#') + ':' + Format((Abs(datediff(MINUTE, fechaFin , @fechaActual)) % 60), '0#'),
	r.estadoComanda = c.IdEstado
from @resumen r
outer apply
(select max(IdEstado) as IdEstado from SotSchema.Comandas
		where IdRenta = r.idRenta
		and Activo = 1
		and IdEstado in (1, 2, 3, 4)) as c
where r.fechaFin is not NULL
and r.estado = @estado_Ocupada

--@estado_Sucia

update r
set  
	r.idTipoLimpieza = tarea.IdTipoLimpieza,
	r.fechaTarea = tarea.FechaCreacion,
	r.txtDetalle = /*Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + */Format(((Abs(datediff(MINUTE, @fechaActual, tarea.FechaCreacion)) % 1440) / 60), '0#') + ':' + Format((Abs(datediff(MINUTE, @fechaActual, tarea.FechaCreacion)) % 60), '0#'),
	r.esVencida = case when @fechaActual > dateadd(MINUTE, r.minutosLimpieza, tarea.FechaCreacion) then 1 else 0 end
from @resumen r
left join SotSchema.TareasLimpieza tarea
	on r.IdHabitacion = tarea.IdHabitacion and tarea.Activa = 1
where r.estado = @estado_Sucia
and tarea.IdEstado = 1


--@estado_Limpieza

update r 
set
	--r.idTarea = tarea.Id, 
	r.idTipoLimpieza = tarea.IdTipoLimpieza,
	r.fechaTarea = tarea.FechaInicioLimpieza,
	r.txtDetalle = /*Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + */Format(((Abs(datediff(MINUTE, @fechaActual, tarea.FechaInicioLimpieza)) % 1440) / 60), '0#') + ':' + Format((Abs(datediff(MINUTE, @fechaActual, tarea.FechaInicioLimpieza)) % 60), '0#'),
	r.esVencida = case when @fechaActual > dateadd(MINUTE, isnull(tiempos.minutosLimpieza,0), tarea.FechaInicioLimpieza) then 1 else 0 end,
	r.txtTipo = (case 
							when ISNULL(datosEmpleados.Contador, 0) > 1 then
								Cast(ISNULL(datosEmpleados.Contador, 0) as varchar) + ' Recamareras'
							else
								ISNULL(datosEmpleados.NombreCompleto, '')
						  end)
from @resumen r
left join SotSchema.TareasLimpieza tarea
on r.IdHabitacion = tarea.IdHabitacion and tarea.Activa = 1
outer apply
(
	select max(tl.Minutos) as minutosLimpieza from SotSchema.TiemposLimpieza tl
	where r.IdTipoHabitacion = tl.IdTipoHabitacion
		and tl.Activa = 1
		and tl.IdTipoLimpieza = tarea.IdTipoLimpieza
) as tiempos
outer apply
(
	select count(e.Id) as Contador, Max(e.Nombre + ' ' + e.ApellidoPaterno/* + ' ' + e.ApellidoMaterno*/) as NombreCompleto 
	from SotSchema.LimpiezaEmpleados le
	inner join SotSchema.Empleados e
		on le.IdEmpleado = e.Id
	where le.Activo = 1
	and le.IdTareaLimpieza = tarea.Id
) as datosEmpleados
where r.estado = @estado_Limpieza
and tarea.IdEstado = 2


--@estado_Supervision

update r
set  
	r.idTipoLimpieza = tl.IdTipoLimpieza,
	r.fechaTarea = FechaInicioSupervision,
	r.txtTipo = ISNULL(e.Nombre + ' ' + e.ApellidoPaterno/* + ' ' + e.ApellidoMaterno*/, ''),
	r.txtDetalle = /*Cast((sum(TotalMinutes) / 1440) as varchar) + '.' + */Format(((Abs(datediff(MINUTE, @fechaActual, FechaInicioSupervision)) % 1440) / 60), '0#') + ':' + Format((Abs(datediff(MINUTE, @fechaActual, FechaInicioSupervision)) % 60), '0#'),
	r.esVencida = 0
from @resumen r
left join SotSchema.TareasLimpieza tl
	on r.IdHabitacion = tl.IdHabitacion
left join SotSchema.Empleados e
	on tl.IdEmpleadoSupervisa = e.Id
where r.estado = @estado_Supervision
and tl.Activa = 1
and tl.IdEstado = 3

--@estado_Mantenimiento

update r
set r.txtDetalle = c.Concepto,
	r.esVencida = 0
from @resumen r
inner join SotSchema.Mantenimientos m
on r.IdHabitacion = m.IdHabitacion
inner join SotSchema.ConceptosMantenimiento c
	on m.IdConceptoMantenimiento = c.Id
where m.Activa = 1
and r.estado = @estado_Mantenimiento

select
	ISNULL(IdHabitacion, 0) as IdHabitacion,
	ISNULL(IdTipoHabitacion, 0) as IdTipoHabitacion,
	ISNULL(NombreTipo, '') as NombreTipo,
	ISNULL(txtNumero, '') as NumeroHabitacion, 
	ISNULL(txtTipo, '') as DatoBase, 
	ISNULL(txtDetalle, '') as Detalle,
	ISNULL(estado, 0) as IdEstado,
	ISNULL(esVencida, 0) as EsVencida,
	estadoComanda as IdEstadoComanda,
	ISNULL(tieneTarjetaV, 0) as TieneTarjetaV,
	ISNULL(Posicion, 0) as Posicion,
	ISNULL(Piso, 0) as Piso
	from @resumen

	*/

insert into @idsHabitaciones
select Id from SotSchema.Habitaciones
where Activo = 1 

declare @id int = 1
declare @max int
declare @idHabitacion int

select @max = max(id) from @idsHabitaciones


while(@id <= @max)
begin

select @idHabitacion = idH from @idsHabitaciones where Id = @id

	insert into @resumen
	(IdHabitacion, IdTipoHabitacion, NombreTipo, NumeroHabitacion, DatoBase, Detalle, IdEstado, EsVencida, IdEstadoComanda, TieneTarjetaV, Posicion, Piso)
	EXEC [SotSchema].[SP_ObtenerResumenHabitacion] @idHabitacion

set @id = @id + 1
end

select
	ISNULL(IdHabitacion, 0) as IdHabitacion,
	ISNULL(IdTipoHabitacion, 0) as IdTipoHabitacion,
	ISNULL(NombreTipo, '') as NombreTipo,
	ISNULL(NumeroHabitacion, '') as NumeroHabitacion, 
	ISNULL(DatoBase, '') as DatoBase, 
	ISNULL(Detalle, '') as Detalle,
	ISNULL(IdEstado, 0) as IdEstado,
	ISNULL(EsVencida, 0) as EsVencida,
	IdEstadoComanda,
	ISNULL(TieneTarjetaV, 0) as TieneTarjetaV,
	ISNULL(Posicion, 0) as Posicion,
	ISNULL(Piso, 0) as Piso
	from @resumen

delete @idsHabitaciones
delete @resumen
END
