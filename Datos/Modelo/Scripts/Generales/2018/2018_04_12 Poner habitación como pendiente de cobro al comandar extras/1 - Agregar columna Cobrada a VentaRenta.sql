﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.VentasRenta ADD
	Cobrada bit NULL
GO
ALTER TABLE SotSchema.VentasRenta SET (LOCK_ESCALATION = TABLE)
GO

update vr
set vr.Cobrada = 0
from SotSchema.VentasRenta vr
inner join SotSchema.Rentas r
	on vr.IdRenta = r.Id
inner join SotSchema.Habitaciones h
	on r.IdHabitacion = h.Id
where r.Activa = 1
and vr.IdPreventa IS NOT NULL
and h.IdEstadoHabitacion = 2--PendienteCobro
go

update vr
set vr.Cobrada = 1
from SotSchema.VentasRenta vr
inner join SotSchema.Rentas r
	on vr.IdRenta = r.Id
inner join SotSchema.Habitaciones h
	on r.IdHabitacion = h.Id
where vr.IdPreventa IS NULL
OR h.IdEstadoHabitacion <> 2
go

COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.VentasRenta
	DROP CONSTRAINT FK_EmpleadoVentaRenta
GO
ALTER TABLE SotSchema.Empleados SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.VentasRenta
	DROP CONSTRAINT FK_RentaVentaRenta
GO
ALTER TABLE SotSchema.Rentas SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE SotSchema.Tmp_VentasRenta
	(
	Id int NOT NULL IDENTITY (1, 1),
	IdRenta int NOT NULL,
	ValorSinIVA decimal(19, 4) NOT NULL,
	ValorConIVA decimal(19, 4) NOT NULL,
	ValorIVA decimal(19, 4) NOT NULL,
	Activo bit NOT NULL,
	Transaccion nvarchar(MAX) NOT NULL,
	FechaCreacion datetime NOT NULL,
	FechaModificacion datetime NOT NULL,
	FechaEliminacion datetime NULL,
	IdUsuarioModifico int NOT NULL,
	IdUsuarioCreo int NOT NULL,
	IdUsuarioElimino int NULL,
	EsInicial bit NOT NULL,
	IdValet int NULL,
	IdPreventa int NULL,
	Cobrada bit NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE SotSchema.Tmp_VentasRenta SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT SotSchema.Tmp_VentasRenta ON
GO
IF EXISTS(SELECT * FROM SotSchema.VentasRenta)
	 EXEC('INSERT INTO SotSchema.Tmp_VentasRenta (Id, IdRenta, ValorSinIVA, ValorConIVA, ValorIVA, Activo, Transaccion, FechaCreacion, FechaModificacion, FechaEliminacion, IdUsuarioModifico, IdUsuarioCreo, IdUsuarioElimino, EsInicial, IdValet, IdPreventa, Cobrada)
		SELECT Id, IdRenta, ValorSinIVA, ValorConIVA, ValorIVA, Activo, Transaccion, FechaCreacion, FechaModificacion, FechaEliminacion, IdUsuarioModifico, IdUsuarioCreo, IdUsuarioElimino, EsInicial, IdValet, IdPreventa, Cobrada FROM SotSchema.VentasRenta WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT SotSchema.Tmp_VentasRenta OFF
GO
ALTER TABLE SotSchema.Extensiones
	DROP CONSTRAINT FK_VentaRentaRenovacion
GO
ALTER TABLE SotSchema.PersonasExtra
	DROP CONSTRAINT FK_VentaRentaPersonaExtra
GO
ALTER TABLE SotSchema.PagosRenta
	DROP CONSTRAINT FK_VentaRentaPagoRenta
GO
ALTER TABLE SotSchema.PaquetesRenta
	DROP CONSTRAINT FK_VentaRentaPaqueteRenta
GO
ALTER TABLE SotSchema.DetallesPago
	DROP CONSTRAINT FK_VentaRentaDetallePago
GO
DROP TABLE SotSchema.VentasRenta
GO
EXECUTE sp_rename N'SotSchema.Tmp_VentasRenta', N'VentasRenta', 'OBJECT' 
GO
ALTER TABLE SotSchema.VentasRenta ADD CONSTRAINT
	PK_VentasRenta PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_FK_RentaVentaRenta ON SotSchema.VentasRenta
	(
	IdRenta
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_FK_EmpleadoVentaRenta ON SotSchema.VentasRenta
	(
	IdValet
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE SotSchema.VentasRenta ADD CONSTRAINT
	FK_RentaVentaRenta FOREIGN KEY
	(
	IdRenta
	) REFERENCES SotSchema.Rentas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.VentasRenta ADD CONSTRAINT
	FK_EmpleadoVentaRenta FOREIGN KEY
	(
	IdValet
	) REFERENCES SotSchema.Empleados
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.DetallesPago ADD CONSTRAINT
	FK_VentaRentaDetallePago FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.DetallesPago SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.PaquetesRenta ADD CONSTRAINT
	FK_VentaRentaPaqueteRenta FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.PaquetesRenta SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.PagosRenta ADD CONSTRAINT
	FK_VentaRentaPagoRenta FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.PagosRenta SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.PersonasExtra ADD CONSTRAINT
	FK_VentaRentaPersonaExtra FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.PersonasExtra SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE SotSchema.Extensiones ADD CONSTRAINT
	FK_VentaRentaRenovacion FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE SotSchema.Extensiones SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



