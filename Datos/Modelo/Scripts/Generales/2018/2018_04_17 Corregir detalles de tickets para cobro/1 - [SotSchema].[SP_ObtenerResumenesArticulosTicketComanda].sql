﻿USE [SOT]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketComanda]    Script Date: 17/04/2018 05:51:22 p. m. ******/
DROP PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketComanda]
GO

/****** Object:  StoredProcedure [SotSchema].[SP_ObtenerResumenesArticulosTicketComanda]    Script Date: 17/04/2018 05:51:22 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date:     17 de octubre de 2017
-- Description:	Retorna un resumen con la cantidad total (con iva) y concepto por artículo
--				perteneciente a la comanda con el id porporcionado
-- =============================================
CREATE PROCEDURE [SotSchema].[SP_ObtenerResumenesArticulosTicketComanda]
	@idComanda int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @IVA decimal(19,3)

    select @IVA = Iva_CatPar from dbo.ZctCatPar

    select Cantidad, Desc_Art as Nombre, ROUND(PrecioUnidad * (1 + @IVA), 2) as PrecioUnidad, lin.IdLineaBase, EsCortesia, art.Cod_Art as Codigo
	from SotSchema.ArticulosComandas aCom
    left join dbo.ZctCatArt art
	   on aCom.IdArticulo = art.Cod_Art
	left join dbo.ZctCatLinea lin
		on art.Cod_Linea = lin.Cod_Linea
    where (Activo = 1 or IdEstado = 3)
    and IdComanda = @idComanda
END


GO


