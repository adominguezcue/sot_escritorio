﻿
/****** Object:  StoredProcedure [dbo].[SP_ZctEncMovInv]    Script Date: 25/10/2018 11:17:53 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Arturo Hernandez
-- Create date: 12-02-2006
-- Description:	Catálogo de Marcas
-- =============================================
ALTER PROCEDURE [dbo].[SP_ZctEncMovInv]
	@TpConsulta Int,
	@CodMov_Inv int,
	@TpMov_Inv  char(2),
	@CosMov_Inv money,
	@FolOS_Inv varchar(10),
	@Cod_Art varchar(20),
	@CtdMov_Inv	numeric(18,10),
	@TpOs_Inv char(2),
	@FchMov_Inv smalldatetime,
	@Cod_Alm int

	--<@Param2, sysname, @p2> <Datatype_For_Param2, , int> = <Default_Value_For_Param2, , 0>
AS
BEGIN
	declare 
	@CodArtReal varchar(20),
	@CosMovS_Inv money,
	@CtdMovS_Inv numeric(18,10),
	@surtir bit

	--Para evitar los espacios en blanco
	select @CodArtReal = Cod_Art from ZctCatArt where Cod_Art = @Cod_Art

	SELECT    @surtir =    ZctCatTpArt.Surtir_TpArt
	FROM            ZctCatArt INNER JOIN
							 ZctCatTpArt ON ZctCatArt.Cod_TpArt = ZctCatTpArt.Cod_TpArt
	WHERE        (ZctCatArt.Cod_Art = @CodArtReal)
	
	if @surtir = 1
	begin
			SELECT     @CtdMovS_Inv = SUM(CtdMov_Inv) + @CtdMov_Inv, @CosMovS_Inv = SUM(CosMov_Inv) + @CosMov_Inv
			FROM         ZctEncMovInv
			where Cod_Art = @CodArtReal AND Cod_Alm = @Cod_Alm
			GROUP BY Cod_Art, Cod_Alm
	
			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			--SET NOCOUNT ON;
			if (@TpConsulta = '1')
				BEGIN
					IF (@CodMov_Inv = 0)
							SELECT [CodMov_Inv] ,[FchMov_Inv] ,[TpMov_Inv] ,[CosMov_Inv]  ,[FolOS_Inv],Cod_Art, CtdMov_Inv, TpOs_Inv, Cod_Alm FROM [ZctEncMovInv]

						ELSE	
							SELECT [CodMov_Inv] ,[FchMov_Inv] ,[TpMov_Inv] ,[CosMov_Inv] ,[FolOS_Inv], Cod_Art, CtdMov_Inv, TpOs_Inv, Cod_Alm FROM [ZctEncMovInv] WHERE [CodMov_Inv] = @CodMov_Inv 
				END
			ELSE
				if (@TpConsulta = '2')
					BEGIN
						if not exists(SELECT * FROM [ZctEncMovInv] WHERE [CodMov_Inv] = @CodMov_Inv ) 
							INSERT INTO [ZctEncMovInv] ([FchMov_Inv] ,[TpMov_Inv] ,[CosMov_Inv]  ,[FolOS_Inv],Cod_Art, CtdMov_Inv, TpOs_Inv, ExistAct_Inv, CostoAct_Inv, Cod_Alm)
							VALUES(@FchMov_Inv ,@TpMov_Inv ,@CosMov_Inv  ,@FolOS_Inv, @CodArtReal, @CtdMov_Inv, @TpOs_Inv,@CtdMovS_Inv, @CosMovS_Inv, @Cod_Alm)
						ELSE
							UPDATE [ZctEncMovInv] SET FchMov_Inv  = @FchMov_Inv  ,[TpMov_Inv] = @TpMov_Inv ,[CosMov_Inv] = @CosMov_Inv,  ExistAct_Inv = @CtdMovS_Inv, CostoAct_Inv = @CosMovS_Inv, @Cod_Alm = Cod_Alm
							,[FolOS_Inv] = @FolOS_Inv, Cod_Art = @CodArtReal, CtdMov_Inv = @CtdMov_Inv, TpOs_Inv = @TpOs_Inv WHERE [CodMov_Inv] = @CodMov_Inv 
					END
				else
					if (@TpConsulta = '3')
						DELETE FROM [ZctEncMovInv] WHERE CodMov_Inv = @CodMov_Inv 
	end
END



