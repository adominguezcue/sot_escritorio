﻿begin tran t1

update [dbo].[ZctArtXAlm]
set Cod_Art = LTRIM(RTRIM(Cod_Art))

update [dbo].[ZctEncMovInv]
set Cod_Art = LTRIM(RTRIM(Cod_Art))

rollback tran t1
--commit tran t1