﻿EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZctCatConversionesArticulos', @level2type=N'CONSTRAINT',@level2name=N'FK_ZctCatConversiones_ZctCatArt_Salida'
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_Description' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZctCatConversionesArticulos', @level2type=N'CONSTRAINT',@level2name=N'FK_ZctCatConversiones_ZctCatArt_Entrada'
GO

ALTER TABLE [dbo].[ZctCatConversionesArticulos] DROP CONSTRAINT [FK_ZctCatConversiones_ZctCatArt_Salida]
GO

ALTER TABLE [dbo].[ZctCatConversionesArticulos] DROP CONSTRAINT [FK_ZctCatConversiones_ZctCatArt_Entrada]
GO

/****** Object:  Table [dbo].[ZctCatConversionesArticulos]    Script Date: 05/07/2018 02:29:28 p. m. ******/
DROP TABLE [dbo].[ZctCatConversionesArticulos]
GO

/****** Object:  Table [dbo].[ZctCatConversionesArticulos]    Script Date: 05/07/2018 02:29:28 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctCatConversionesArticulos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CodigoArticuloEntrada] [varchar](20) NOT NULL,
	[CodigoArticuloSalida] [varchar](20) NOT NULL,
	[CantidadEntrada] [decimal](19, 4) NOT NULL,
	[CantidadSalida] [decimal](19, 4) NOT NULL,
 CONSTRAINT [PK_ZctCatConversiones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZctCatConversionesArticulos]  WITH CHECK ADD  CONSTRAINT [FK_ZctCatConversiones_ZctCatArt_Entrada] FOREIGN KEY([CodigoArticuloEntrada])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctCatConversionesArticulos] CHECK CONSTRAINT [FK_ZctCatConversiones_ZctCatArt_Entrada]
GO

ALTER TABLE [dbo].[ZctCatConversionesArticulos]  WITH CHECK ADD  CONSTRAINT [FK_ZctCatConversiones_ZctCatArt_Salida] FOREIGN KEY([CodigoArticuloSalida])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctCatConversionesArticulos] CHECK CONSTRAINT [FK_ZctCatConversiones_ZctCatArt_Salida]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relación con el artículo de entrada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZctCatConversionesArticulos', @level2type=N'CONSTRAINT',@level2name=N'FK_ZctCatConversiones_ZctCatArt_Entrada'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relación con el artículo de salida' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ZctCatConversionesArticulos', @level2type=N'CONSTRAINT',@level2name=N'FK_ZctCatConversiones_ZctCatArt_Salida'
GO


