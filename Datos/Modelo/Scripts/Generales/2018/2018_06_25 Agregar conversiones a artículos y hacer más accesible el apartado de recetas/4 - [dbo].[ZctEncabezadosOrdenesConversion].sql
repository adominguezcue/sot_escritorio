﻿/****** Object:  Table [dbo].[ZctEncabezadosOrdenesConversion]    Script Date: 10/07/2018 02:42:59 p. m. ******/
DROP TABLE [dbo].[ZctEncabezadosOrdenesConversion]
GO

/****** Object:  Table [dbo].[ZctEncabezadosOrdenesConversion]    Script Date: 10/07/2018 02:42:59 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctEncabezadosOrdenesConversion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FechaOperacion] [datetime] NOT NULL,
	[EsEntrada] [bit] NOT NULL,
	[FolioTipo] [varchar](20) NOT NULL,
 CONSTRAINT [PK_EncabezadosOrdenesConversion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


