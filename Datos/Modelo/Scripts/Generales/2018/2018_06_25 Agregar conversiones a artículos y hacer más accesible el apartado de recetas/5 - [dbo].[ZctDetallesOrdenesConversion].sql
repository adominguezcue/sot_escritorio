﻿ALTER TABLE [dbo].[ZctDetallesOrdenesConversion] DROP CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctEncabezadosOrdenesConversion]
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion] DROP CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion] DROP CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctCatAlm]
GO

/****** Object:  Table [dbo].[ZctDetallesOrdenesConversion]    Script Date: 10/07/2018 02:42:49 p. m. ******/
DROP TABLE [dbo].[ZctDetallesOrdenesConversion]
GO

/****** Object:  Table [dbo].[ZctDetallesOrdenesConversion]    Script Date: 10/07/2018 02:42:49 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctDetallesOrdenesConversion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdEncabezadoOrdenConversion] [int] NOT NULL,
	[Cantidad] [numeric](18, 10) NOT NULL,
	[Costo] [numeric](18, 2) NOT NULL,
	[CodigoArticulo] [varchar](20) NOT NULL,
	[IdAlmacen] [int] NOT NULL,
	[AnioFiscal] [int] NULL,
	[MesFiscal] [int] NULL,
 CONSTRAINT [PK_ZctDetallesOrdenesConversion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion]  WITH CHECK ADD  CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctCatAlm] FOREIGN KEY([IdAlmacen])
REFERENCES [dbo].[ZctCatAlm] ([Cod_Alm])
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion] CHECK CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctCatAlm]
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion]  WITH CHECK ADD  CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctCatArt] FOREIGN KEY([CodigoArticulo])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion] CHECK CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion]  WITH CHECK ADD  CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctEncabezadosOrdenesConversion] FOREIGN KEY([IdEncabezadoOrdenConversion])
REFERENCES [dbo].[ZctEncabezadosOrdenesConversion] ([Id])
GO

ALTER TABLE [dbo].[ZctDetallesOrdenesConversion] CHECK CONSTRAINT [FK_ZctDetallesOrdenesConversion_ZctEncabezadosOrdenesConversion]
GO


