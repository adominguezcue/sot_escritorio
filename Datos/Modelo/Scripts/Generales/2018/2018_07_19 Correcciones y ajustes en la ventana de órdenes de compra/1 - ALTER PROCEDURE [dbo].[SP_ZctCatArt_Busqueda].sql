﻿/****** Object:  StoredProcedure [dbo].[SP_ZctCatArt_Busqueda]    Script Date: 19/07/2018 12:49:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SP_ZctCatArt_Busqueda]
	@Codigo varchar(20)
AS
	BEGIN
		set @Codigo = replace(@Codigo, '''','-' )

		SELECT 
		Cod_Art, 
		Desc_Art, 
		Exist_Art AS ColExist, 
		0 as ColSurt,
		Prec_Art, 
		Cos_Art, 
		Cod_TpArt,
		cod_alm,
		ZctCatMar.Desc_Mar AS marca,
		OmitirIVA
		FROM ZctCatArt 
		LEFT JOIN ZctCatMar 
			ON ZctCatMar.Cod_Mar = ZctCatArt.Cod_Mar  
		WHERE Cod_Art = @Codigo or upc= @Codigo  
	END
