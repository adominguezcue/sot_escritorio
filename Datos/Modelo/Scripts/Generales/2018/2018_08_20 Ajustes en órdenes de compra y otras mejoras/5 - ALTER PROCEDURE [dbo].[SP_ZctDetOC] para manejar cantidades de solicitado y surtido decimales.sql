﻿/****** Object:  StoredProcedure [dbo].[SP_ZctDetOC]    Script Date: 22/08/2018 11:08:21 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Arturo Hernandez
-- Create date: 12-02-2006
-- Description:	Catálogo de Marcas
-- =============================================
ALTER PROCEDURE [dbo].[SP_ZctDetOC]
		@TpConsulta Int,	
		@Cod_OC	int,
		@CodDet_Oc int,
		@Cod_Art varchar(20),
		@Ctd_Art decimal(19, 4),
		@Cos_Art decimal(19, 4),
		@CtdStdDet_OC decimal(19, 4),
		@FchAplMov smalldatetime,
		@Cat_Alm int,
		@omitirIVA bit

AS
BEGIN
	DECLARE @CtdStdOld INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	if (@TpConsulta = '1')
		BEGIN
			IF ( @Cod_OC  = 0)

					SELECT Cod_OC, CodDet_Oc, Cod_Art, Ctd_Art, Cos_Art, CtdStdDet_OC , Cat_Alm, OmitirIVA  FROM [ZctDetOC]
				ELSE
					SELECT ZctDetOC.Cod_OC, ZctDetOC.CodDet_OC, ZctDetOC.Cod_Art,ZctCatArt.upc, ZctDetOC.Ctd_Art, ZctDetOC.Cos_Art, ZctDetOC.CtdStdDet_OC, ZctCatArt.Desc_Art, 
                      ZctCatArt.Exist_Art, ZctDetOC.Cat_Alm,ZctCatMar.Desc_Mar as marca, ZctDetOC.OmitirIVA
					  FROM ZctDetOC INNER JOIN
                      ZctCatArt ON ZctDetOC.Cod_Art = ZctCatArt.Cod_Art 
					  LEFT JOIN ZctCatMar ON ZctCatMar.Cod_Mar = ZctCatArt.Cod_Mar  
					  WHERE ZctDetOC.Cod_OC = @Cod_OC
					   
					--SELECT  CodDet_Oc, Cod_Art, Ctd_Art, Cos_Art, CtdStdDet_OC FROM ZctDetOC WHERE Cod_OC = @Cod_OC
					--SELECT Cod_EncOT , Cod_DetOT, Cod_Art, Ctd_Art, CosArt_DetOT, PreArt_DetOT, CtdStd_DetOT, Fch_DetOT FROM [ZctDetOT] WHERE Cod_EncOT = @Cod_EncOT 
		END
	ELSE
		if (@TpConsulta = '2')
			BEGIN
				declare @anio int
				declare @mes int 

				exec dbo.sp_get_anio_mes @FchAplMov, @anio output, @mes output
 


				if not exists(SELECT  CodDet_Oc  FROM [ZctDetOc] WHERE CodDet_Oc = @CodDet_OC  ) 
					BEGIN
						INSERT INTO [ZctDetOC] ( Cod_OC , Cod_Art, Ctd_Art, Cos_Art, CtdStdDet_OC, Cat_Alm, OmitirIVA, anio_fiscal, mes_fiscal,update_web , uuid )
						VALUES(@Cod_OC , @Cod_Art, @Ctd_Art, @Cos_Art, @CtdStdDet_OC , @Cat_Alm , @omitirIVA, @anio, @mes, null, newid())

						
						SET @Cos_Art = (@Cos_Art  * @CtdStdDet_OC )
						--Modificar lo del catálogo de almacenes
						EXECUTE SP_ZctEncMovInv 2,0, 'EN', @Cos_Art  , @Cod_OC, @Cod_Art  , @CtdStdDet_OC, 'OC', @FchAplMov, @Cat_Alm

					END
				ELSE
					BEGIN
						SELECT @CtdStdOld =  CtdStdDet_OC FROM [ZctDetOC] WHERE CodDet_OC = @CodDet_OC
						UPDATE [ZctDetOC] SET   
							Ctd_Art = @Ctd_Art, 
							Cos_Art = @Cos_Art , 
							CtdStdDet_OC = @CtdStdDet_OC, 
							Cat_Alm = @Cat_Alm, 
							anio_fiscal = @anio, 
							mes_fiscal = @mes, 
							update_web = null,
							OmitirIVA = @omitirIVA
						WHERE CodDet_Oc = @CodDet_Oc 
						
						if (@CtdStdOld > @CtdStdDet_OC ) 
							BEGIN
								--DEVOLUCION
								SET @CtdStdDet_OC =  (@CtdStdOld - @CtdStdDet_OC ) * (-1)
								SET @Cos_Art = @CtdStdDet_OC * @Cos_Art
								EXECUTE SP_ZctEncMovInv 2,0, 'SA', @Cos_Art , @Cod_OC, @Cod_Art, @CtdStdDet_OC, 'OC', @FchAplMov, @Cat_Alm
							END
						ELSE
							if (@CtdStdOld < @CtdStdDet_OC ) 
								BEGIN
									SET @CtdStdDet_OC =  @CtdStdDet_OC - @CtdStdOld
									SET @Cos_Art = @Cos_Art * @CtdStdDet_OC
									
									EXECUTE SP_ZctEncMovInv 2,0, 'EN', @Cos_Art , @Cod_OC, @Cod_Art  , @CtdStdDet_OC, 'OC', @FchAplMov, @Cat_Alm
								END	
						
						
					END

			END
		else
			if (@TpConsulta = '3')
				DELETE FROM [ZctDetOC] WHERE CodDet_OC = @CodDet_OC
END

