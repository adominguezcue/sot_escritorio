﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

ALTER TABLE dbo.ZctDetOC
	DROP CONSTRAINT FK_ZctDetOC_ZctCatArt

ALTER TABLE dbo.ZctCatArt SET (LOCK_ESCALATION = TABLE)

ALTER TABLE dbo.ZctDetOC
	DROP CONSTRAINT FK_ZctDetOC_ZctEncOC

ALTER TABLE dbo.ZctEncOC SET (LOCK_ESCALATION = TABLE)


ALTER TABLE dbo.ZctDetOC
	DROP CONSTRAINT FK_ZctDetOC_ZctCatAlm

ALTER TABLE dbo.ZctCatAlm SET (LOCK_ESCALATION = TABLE)


ALTER TABLE dbo.ZctDetOC
	DROP CONSTRAINT DF__ZctDetOC__uuid__1DA648AE

CREATE TABLE dbo.Tmp_ZctDetOC
	(
	Cod_OC int NOT NULL,
	CodDet_OC int NOT NULL IDENTITY (1, 1),
	Cod_Art varchar(20) NULL,
	Ctd_Art decimal(19, 4) NULL,
	Cos_Art decimal(19, 4) NULL,
	CtdStdDet_OC decimal(19, 4) NULL,
	Cat_Alm int NULL,
	anio_fiscal int NULL,
	mes_fiscal int NULL,
	update_web bit NULL,
	uuid uniqueidentifier NULL,
	OmitirIVA bit NOT NULL
	)  ON [PRIMARY]

ALTER TABLE dbo.Tmp_ZctDetOC SET (LOCK_ESCALATION = TABLE)

ALTER TABLE dbo.Tmp_ZctDetOC ADD CONSTRAINT
	DF__ZctDetOC__uuid__1DA648AE DEFAULT (newid()) FOR uuid

SET IDENTITY_INSERT dbo.Tmp_ZctDetOC ON

IF EXISTS(SELECT * FROM dbo.ZctDetOC)
	 EXEC('INSERT INTO dbo.Tmp_ZctDetOC (Cod_OC, CodDet_OC, Cod_Art, Ctd_Art, Cos_Art, CtdStdDet_OC, Cat_Alm, anio_fiscal, mes_fiscal, update_web, uuid, OmitirIVA)
		SELECT Cod_OC, CodDet_OC, Cod_Art, CONVERT(decimal(19, 4), Ctd_Art), CONVERT(decimal(19, 4), Cos_Art), CONVERT(decimal(19, 4), CtdStdDet_OC), Cat_Alm, anio_fiscal, mes_fiscal, update_web, uuid, OmitirIVA FROM dbo.ZctDetOC WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_ZctDetOC OFF

DROP TABLE dbo.ZctDetOC

EXECUTE sp_rename N'dbo.Tmp_ZctDetOC', N'ZctDetOC', 'OBJECT' 

ALTER TABLE dbo.ZctDetOC ADD CONSTRAINT
	PK_ZctDetOC PRIMARY KEY CLUSTERED 
	(
	Cod_OC,
	CodDet_OC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


ALTER TABLE dbo.ZctDetOC ADD CONSTRAINT
	FK_ZctDetOC_ZctCatAlm FOREIGN KEY
	(
	Cat_Alm
	) REFERENCES dbo.ZctCatAlm
	(
	Cod_Alm
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE dbo.ZctDetOC ADD CONSTRAINT
	FK_ZctDetOC_ZctEncOC FOREIGN KEY
	(
	Cod_OC
	) REFERENCES dbo.ZctEncOC
	(
	Cod_OC
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE dbo.ZctDetOC ADD CONSTRAINT
	FK_ZctDetOC_ZctCatArt FOREIGN KEY
	(
	Cod_Art
	) REFERENCES dbo.ZctCatArt
	(
	Cod_Art
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

COMMIT
