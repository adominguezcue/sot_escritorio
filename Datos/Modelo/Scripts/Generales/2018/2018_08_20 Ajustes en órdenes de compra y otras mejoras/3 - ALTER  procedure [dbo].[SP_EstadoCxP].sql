﻿/****** Object:  StoredProcedure [dbo].[SP_EstadoCxP]    Script Date: 21/08/2018 03:43:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  procedure [dbo].[SP_EstadoCxP] 
   @OC int,
   @estado int
   as
   begin
   declare @cuentapago int 
   
   set @cuentapago = isnull((select  cod_cuenta_pago from ZctEncCuentasPagar where Cod_OC = @OC), 0)

   update ZctEncCuentasPagar set idEstado =@estado where Cod_OC =@OC 
   update ZctDetCuentasPagar set idEstado=@estado where cod_cuenta_pago=@cuentapago  
   end 