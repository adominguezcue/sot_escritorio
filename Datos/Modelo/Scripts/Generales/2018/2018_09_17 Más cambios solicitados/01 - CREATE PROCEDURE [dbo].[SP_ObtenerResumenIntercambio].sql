﻿/****** Object:  StoredProcedure [dbo].[SP_ObtenerResumenIntercambio]    Script Date: 17/09/2018 10:59:02 a. m. ******/
DROP PROCEDURE [dbo].[SP_ObtenerResumenIntercambio]
GO

/****** Object:  StoredProcedure [dbo].[SP_ObtenerResumenIntercambio]    Script Date: 17/09/2018 10:59:02 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Eduardo Abraham Vázquez Rosado
-- Create date: 17 de septiembre de 2018
-- Description:	Retorna dos conjuntos de resultados, el primero incluye el folio, el nombre del almacén de salida
--              y el de entrada, el segundo conjunto incluye datos de los artículos involucrados (código, nombre, cantidad,
--				intercambiada, costo unitario y costo total del movimiento) y fecha de la operación
-- =============================================
CREATE PROCEDURE [dbo].[SP_ObtenerResumenIntercambio]
	@idFolio int
AS
BEGIN

select @idFolio as Folio, salida.Desc_CatAlm as AlmacenSalida, entrada.Desc_CatAlm as AlmacenEntrada, salida.FchMov_Inv as FechaMovimiento from
(select top 1 al.Desc_CatAlm, e.FchMov_Inv from ZctEncMovInv e
inner join ZctCatAlm al
	on al.Cod_Alm = e.Cod_Alm
where TpOs_Inv = 'IA'
and TpMov_Inv = 'SA'
and FolOS_Inv = @idFolio) as salida
cross join
(select top 1 al.Desc_CatAlm from ZctEncMovInv e
inner join ZctCatAlm al
	on al.Cod_Alm = e.Cod_Alm
where TpOs_Inv = 'IA'
and TpMov_Inv = 'EN'
and FolOS_Inv = @idFolio) as entrada

Select 
e.Cod_Art as CodigoArticulo,
a.Desc_Art as DescripcionArticulo,
e.FchMov_Inv as FechaMovimiento,
e.CtdMov_Inv as CantidadIntercambiada,
(e.CosMov_Inv / e.CtdMov_Inv) as CostoUnitario,
e.CosMov_Inv as CostoMovimiento
from ZctEncMovInv e
inner join ZctCatArt a
	on e.Cod_Art = a.Cod_Art
where TpOs_Inv = 'IA'
and TpMov_Inv = 'EN'
and FolOS_Inv = @idFolio

END
GO


