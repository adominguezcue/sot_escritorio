﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION


ALTER TABLE SotSchema.Preventas SET (LOCK_ESCALATION = TABLE)


ALTER TABLE SotSchema.VentasRenta ADD CONSTRAINT
	FK_VentasRenta_Preventas FOREIGN KEY
	(
	IdPreventa
	) REFERENCES SotSchema.Preventas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.VentasRenta SET (LOCK_ESCALATION = TABLE)




ALTER TABLE SotSchema.Preventas SET (LOCK_ESCALATION = TABLE)


ALTER TABLE SotSchema.ConsumosInternos ADD CONSTRAINT
	FK_ConsumosInternos_Preventas FOREIGN KEY
	(
	IdPreventa
	) REFERENCES SotSchema.Preventas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.ConsumosInternos SET (LOCK_ESCALATION = TABLE)



ALTER TABLE SotSchema.Preventas SET (LOCK_ESCALATION = TABLE)




ALTER TABLE SotSchema.Preventas SET (LOCK_ESCALATION = TABLE)


ALTER TABLE SotSchema.OrdenesTaxis ADD CONSTRAINT
	FK_OrdenesTaxis_Preventas FOREIGN KEY
	(
	IdPreventa
	) REFERENCES SotSchema.Preventas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.OrdenesTaxis SET (LOCK_ESCALATION = TABLE)


ALTER TABLE SotSchema.Comandas ADD CONSTRAINT
	FK_Comandas_Preventas FOREIGN KEY
	(
	IdPreventa
	) REFERENCES SotSchema.Preventas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.Comandas SET (LOCK_ESCALATION = TABLE)



ALTER TABLE SotSchema.Preventas SET (LOCK_ESCALATION = TABLE)


ALTER TABLE SotSchema.OcupacionesMesa ADD CONSTRAINT
	FK_OcupacionesMesa_Preventas FOREIGN KEY
	(
	IdPreventa
	) REFERENCES SotSchema.Preventas
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.OcupacionesMesa SET (LOCK_ESCALATION = TABLE)

COMMIT
