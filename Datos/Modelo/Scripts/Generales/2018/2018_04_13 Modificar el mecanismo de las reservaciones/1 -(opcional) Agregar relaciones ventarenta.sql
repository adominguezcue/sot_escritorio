﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

ALTER TABLE SotSchema.VentasRenta SET (LOCK_ESCALATION = TABLE)



ALTER TABLE SotSchema.Extensiones ADD CONSTRAINT
	FK_Extensiones_VentasRenta FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.Extensiones SET (LOCK_ESCALATION = TABLE)


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

ALTER TABLE SotSchema.VentasRenta SET (LOCK_ESCALATION = TABLE)



ALTER TABLE SotSchema.PagosRenta ADD CONSTRAINT
	FK_PagosRenta_VentasRenta FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.PagosRenta SET (LOCK_ESCALATION = TABLE)




ALTER TABLE SotSchema.VentasRenta SET (LOCK_ESCALATION = TABLE)



ALTER TABLE SotSchema.DetallesPago ADD CONSTRAINT
	FK_DetallesPago_VentasRenta FOREIGN KEY
	(
	IdVentaRenta
	) REFERENCES SotSchema.VentasRenta
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	

ALTER TABLE SotSchema.DetallesPago SET (LOCK_ESCALATION = TABLE)


COMMIT


