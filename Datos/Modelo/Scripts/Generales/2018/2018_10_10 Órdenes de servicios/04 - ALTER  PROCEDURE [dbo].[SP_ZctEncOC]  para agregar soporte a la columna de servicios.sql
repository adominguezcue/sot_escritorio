﻿/****** Object:  StoredProcedure [dbo].[SP_ZctEncOC]    Script Date: 10/10/2018 09:55:15 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[SP_ZctEncOC] 
	@TpConsulta Int,
	@Cod_OC int,
	@Cod_Prov int,
	@Fch_OC smalldatetime,-- insertaba getDate()
	@FchAplMov smalldatetime,
	@Fac_OC varchar(20),
	@FchFac_OC smalldatetime,
	@estado varchar(20)='',
	@servicio bit = null
AS
BEGIN
	if (@TpConsulta = '1')
		BEGIN
			IF (@Cod_OC = 0)
					SELECT Cod_OC, Cod_Prov, Fch_OC, FchAplMov, Fac_OC, FchFac_OC,"status", Servicios FROM [ZctEncOC]
				ELSE	
					SELECT Cod_OC, Cod_Prov, Fch_OC, FchAplMov, Fac_OC, FchFac_OC,"status", Servicios FROM [ZctEncOC] WHERE Cod_OC = @Cod_OC
		END
	ELSE
		if (@TpConsulta = '2')
			BEGIN
				if not exists(SELECT Cod_OC, Cod_Prov, Fch_OC FROM [ZctEncOC] WHERE Cod_OC = @Cod_OC) 
					INSERT INTO [ZctEncOC] (Cod_OC, Cod_Prov, Fch_OC, FchAplMov, Fac_OC, FchFac_OC, Servicios)
					VALUES(@Cod_OC , @Cod_Prov, GETDATE(), @FchAplMov, @Fac_OC, @FchFac_OC, @servicio)
				ELSE
					UPDATE [ZctEncOC] 
					SET Cod_Prov = @Cod_Prov, 
						Fch_OC = @Fch_OC, 
						FchAplMov = @FchAplMov, 
						Fac_OC = @Fac_OC, 
						FchFac_OC = @FchFac_OC,
						"status"=@estado
						--no puedes cambiar la bandera de servicio, se queda como se creó
					WHERE Cod_OC = @Cod_OC
			END
		else
			if (@TpConsulta = '3')
				DELETE FROM [ZctEncOC] WHERE Cod_OC = @Cod_OC
END