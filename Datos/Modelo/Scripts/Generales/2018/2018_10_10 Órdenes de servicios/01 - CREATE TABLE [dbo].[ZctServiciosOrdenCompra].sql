﻿/****** Object:  Table [dbo].[ZctServiciosOrdenCompra]    Script Date: 10/10/2018 09:36:42 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctServiciosOrdenCompra](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CodigoOrdenCompra] [int] NOT NULL,
	[Descripcion] [varchar](max) NOT NULL,
	[Costo] [decimal](19, 4) NOT NULL,
	[AnioFiscal] [int] NULL,
	[MesFiscal] [int] NULL,
	[UpdateWeb] [bit] NULL,
	[Uuid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ZctServiciosOrdenCompra] PRIMARY KEY CLUSTERED 
(
	[CodigoOrdenCompra] ASC,
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZctServiciosOrdenCompra] ADD  CONSTRAINT [DF__ZctServiciosOrdenCompra__Uuid__1DA648AE]  DEFAULT (newid()) FOR [Uuid]
GO

ALTER TABLE [dbo].[ZctServiciosOrdenCompra]  WITH CHECK ADD  CONSTRAINT [FK_ZctServiciosOrdenCompra_ZctEncOC] FOREIGN KEY([CodigoOrdenCompra])
REFERENCES [dbo].[ZctEncOC] ([Cod_OC])
GO

ALTER TABLE [dbo].[ZctServiciosOrdenCompra] CHECK CONSTRAINT [FK_ZctServiciosOrdenCompra_ZctEncOC]
GO


