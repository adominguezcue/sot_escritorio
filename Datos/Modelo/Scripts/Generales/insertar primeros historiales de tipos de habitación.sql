﻿begin transaction t1

declare @id int
declare @fecha datetime = GETDATE()

select @id = isnull(MAX(Id), 0) from SotSchema.TiposHabitacionSincronizacion

insert into SotSchema.TiposHabitacionSincronizacion
(Clave, Nombre, Fecha, PorcentajeIVA, IdMovimiento, EsErrorSubida, ErrorUltimoIntento, Sincronizado)
select Clave, Descripcion, @fecha, 0.16, 1, 0, NULL, 0 from SotSchema.TiposHabitacion
where Activo = 1

--set @id = SCOPE_IDENTITY()

set @fecha = DATEADD(second, 10, @fecha)

insert into SotSchema.PreciosTipoHabitacionSincronizacion
(IdTipoSincronizacion, IdTarifa, Precio)
select ths.Id, cfgTr.IdTarifa, cfgTi.Precio from SotSchema.TiposHabitacionSincronizacion ths
inner join SotSchema.TiposHabitacion th
	on ths.Clave = th.Clave and ths.Id > @id
inner join SotSchema.ConfiguracionesTipos cfgTi
	on th.Id = cfgTi.IdTipoHabitacion
inner join SotSchema.ConfiguracionesTarifas cfgTr
	on cfgTi.IdConfiguracionNegocio = cfgTr.Id

select @id = isnull(MAX(Id), 0) from SotSchema.TiposHabitacionSincronizacion

insert into SotSchema.TiposHabitacionSincronizacion
(Clave, Nombre, Fecha, PorcentajeIVA, IdMovimiento, EsErrorSubida, ErrorUltimoIntento, Sincronizado)
select Clave, Descripcion, @fecha, 0.16, 2, 0, NULL, 0 from SotSchema.TiposHabitacion
where Activo = 1

--set @id = SCOPE_IDENTITY()

insert into SotSchema.PreciosTipoHabitacionSincronizacion
(IdTipoSincronizacion, IdTarifa, Precio)
select ths.Id, cfgTr.IdTarifa, cfgTi.Precio from SotSchema.TiposHabitacionSincronizacion ths
inner join SotSchema.TiposHabitacion th
	on ths.Clave = th.Clave and ths.Id > @id
inner join SotSchema.ConfiguracionesTipos cfgTi
	on th.Id = cfgTi.IdTipoHabitacion
inner join SotSchema.ConfiguracionesTarifas cfgTr
	on cfgTi.IdConfiguracionNegocio = cfgTr.Id


select * from SotSchema.TiposHabitacionSincronizacion
select * from SotSchema.PreciosTipoHabitacionSincronizacion

rollback transaction t1