﻿USE [SOT]
GO

/****** Object:  View [SotSchema].[VW_ArticulosVendidos]    Script Date: 19/01/2018 06:45:36 p. m. ******/
DROP VIEW [SotSchema].[VW_ArticulosVendidos]
GO

/****** Object:  View [SotSchema].[VW_ArticulosVendidos]    Script Date: 19/01/2018 06:45:36 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [SotSchema].[VW_ArticulosVendidos]
AS
select
	(cast(com.Id as varchar) + '-' + cast(ac.Id as varchar)) as Id,
	configct.Orden as Turno, 
	ac.FechaCreacion as Fecha, 
	ac.IdArticulo as Codigo, 
	dpto.Desc_Dpto as Categoria, 
	lin.Desc_Linea as Subcategoria, 
	art.Desc_Art as Descripcion, 
	ac.Cantidad, 
	ac.PrecioUnidadFinal as PrecioUnidadConIVA, 
	(ac.Cantidad * ac.PrecioUnidadFinal) as Total
from SotSchema.ArticulosComandas ac
inner join SotSchema.Comandas com
	on ac.IdComanda = com.Id
inner join SotSchema.CortesTurno ct
	on com.IdCorte = ct.Id
inner join SotSchema.ConfiguracionesTurno configct
	on ct.IdConfiguracionTurno = configct.Id
inner join dbo.ZctCatArt art
	on ac.IdArticulo = art.Cod_Art
left join dbo.ZctCatLinea lin
	on art.Cod_Linea = lin.Cod_Linea
left join dbo.ZctCatDpto dpto
	on art.Cod_Dpto = dpto.Cod_Dpto
where ac.IdEstado = 3--Entregado
and com.IdEstado = 5--Cobrada
union all
select 
	(cast(com.Id as varchar) + '-' + cast(ac.Id as varchar)) as Id,
	configct.Orden as Turno, 
	ac.FechaCreacion as Fecha, 
	ac.IdArticulo as Codigo, 
	dpto.Desc_Dpto as Categoria, 
	lin.Desc_Linea as Subcategoria, 
	art.Desc_Art as Descripcion, 
	ac.Cantidad, 
	ac.PrecioUnidadFinal as PrecioUnidadConIVA, 
	(ac.Cantidad * ac.PrecioUnidadFinal) as Total
from SotSchema.ArticulosOrdenesRestaurante ac
inner join SotSchema.OrdenesRestaurante com
	on ac.IdOrdenRestaurante = com.Id
--inner join SotSchema.OcupacionesMesa ocm
--	on com.IdOcupacionMesa = ocm.Id
inner join SotSchema.CortesTurno ct
	on com.IdCorte = ct.Id
inner join SotSchema.ConfiguracionesTurno configct
	on ct.IdConfiguracionTurno = configct.Id
inner join dbo.ZctCatArt art
	on ac.IdArticulo = art.Cod_Art
left join dbo.ZctCatLinea lin
	on art.Cod_Linea = lin.Cod_Linea
left join dbo.ZctCatDpto dpto
	on art.Cod_Dpto = dpto.Cod_Dpto
where ac.IdEstado = 3--Entregado
and com.IdEstado = 5--Cobrada
--and ocm.IdEstado = 2--Cobrada
union all
select 
	(cast(com.Id as varchar) + '-' + cast(ac.Id as varchar)) as Id,
	configct.Orden as Turno, 
	ac.FechaCreacion as Fecha, 
	ac.IdArticulo as Codigo, 
	dpto.Desc_Dpto as Categoria, 
	lin.Desc_Linea as Subcategoria, 
	art.Desc_Art as Descripcion, 
	ac.Cantidad, 
	ac.PrecioUnidadFinal as PrecioUnidadConIVA, 
	(ac.Cantidad * ac.PrecioUnidadFinal) as Total
from SotSchema.ArticulosConsumoInterno ac
inner join SotSchema.ConsumosInternos com
	on ac.IdConsumoInterno = com.Id
inner join SotSchema.CortesTurno ct
	on com.IdCorteTurno = ct.Id
inner join SotSchema.ConfiguracionesTurno configct
	on ct.IdConfiguracionTurno = configct.Id
inner join dbo.ZctCatArt art
	on ac.IdArticulo = art.Cod_Art
left join dbo.ZctCatLinea lin
	on art.Cod_Linea = lin.Cod_Linea
left join dbo.ZctCatDpto dpto
	on art.Cod_Dpto = dpto.Cod_Dpto
where ac.IdEstado = 3--Entregado
and com.IdEstado = 5--Entregado
GO


