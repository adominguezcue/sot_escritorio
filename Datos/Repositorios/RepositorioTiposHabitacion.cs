﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;
using System.Data;
using Modelo.Dtos;
using System.Data.SqlClient;

namespace Datos.Repositorios
{
    public class RepositorioTiposHabitacion : Repositorio<TipoHabitacion>, IRepositorioTiposHabitacion
    {
        public RepositorioTiposHabitacion() : base(new Contextos.SOTContext())
        {
        }

        public List<int> ObtenerTarifasSoportadas(int idTipoHabitacion)
        {
            return (from configuracionTipo in contexto.Set<ConfiguracionTipo>()
                    where configuracionTipo.Activa
                    && configuracionTipo.TipoHabitacion.Activo
                    && configuracionTipo.IdTipoHabitacion == idTipoHabitacion
                    select configuracionTipo.ConfiguracionTarifa.IdTarifa).ToList();
        }

        public TipoHabitacion ObtenerTipoConTiemposLimpiezaPorHabitacion(int idHabitacion)
        {
            return (from habitacion in contexto.Set<Habitacion>()
                    where habitacion.Id == idHabitacion
                    && habitacion.TipoHabitacion.Activo
                    select new
                    {
                        tipo = habitacion.TipoHabitacion,
                        tiempos = habitacion.TipoHabitacion.TiemposLimpieza.Where(m => m.Activa)
                    }).ToList().Select(m => m.tipo).FirstOrDefault();
        }


        public List<TipoHabitacion> ObtenerTiposActivosConConfiguracion(ConfiguracionTarifa.Tarifas tarifa)
        {
            int idTarifa = (int)tarifa;

            return (from tipo in contexto.Set<TipoHabitacion>()
                    where tipo.Activo
                    && tipo.ConfiguracionesTipos.Any(m => m.Activa && m.ConfiguracionTarifa.Activa
                                                     && m.ConfiguracionTarifa.IdTarifa == idTarifa)
                    select new
                    {
                        tipo,
                        configuracionTipo = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa
                                                     && m.ConfiguracionTarifa.IdTarifa == idTarifa),
                        configuracionTarifa = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa
                                                     && m.ConfiguracionTarifa.IdTarifa == idTarifa).Select(m => m.ConfiguracionTarifa),
                        tiemposExtra = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa
                                                     && m.ConfiguracionTarifa.IdTarifa == idTarifa).SelectMany(m => m.TiemposHospedaje).Where(m => m.Activo),
                        configuracionNegocio = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa
                                                     && m.ConfiguracionTarifa.IdTarifa == idTarifa).Select(m => m.ConfiguracionTarifa),
                    }).ToList().Select(m => m.tipo).ToList();
        }


        public List<TipoHabitacion> ObtenerTiposActivosConConfiguraciones()
        {
            return (from tipo in contexto.Set<TipoHabitacion>()
                    where tipo.Activo
                    select new
                    {
                        tipo,
                        configuracionTipo = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa),
                        configuracionTarifa = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa).Select(m => m.ConfiguracionTarifa),
                        tiemposExtra = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa).SelectMany(m => m.TiemposHospedaje).Where(m => m.Activo),
                        configuracionNegocio = tipo.ConfiguracionesTipos.Where(m => m.Activa && m.ConfiguracionTarifa.Activa).Select(m => m.ConfiguracionTarifa),
                    }).ToList().Select(m => m.tipo).ToList();
        }

        public List<DtoResumenTipoHabitacion> ObtenerTiposActivosCargadosDataTable()
        {
            var resultado = (from tipo in contexto.Set<TipoHabitacion>()
                             where tipo.Activo
                             && tipo.ConfiguracionesTipos.Any(m => m.Activa && m.ConfiguracionTarifa.Activa)
                             select new
                             {
                                 Id = tipo.Id,
                                 descripcion = tipo.Descripcion,
                                 descripcionExt = tipo.DescripcionExtendida,
                                 tarifas = tipo.ConfiguracionesTipos.Where(m => m.Activa).Select(m => new
                                 {
                                     tarifa = m.ConfiguracionTarifa.IdTarifa,
                                     precio = m.Precio,
                                     tiempos = m.TiemposHospedaje.Select(t => new
                                     {
                                         t.Orden,
                                         t.Minutos
                                     }),
                                 }),
                                 limpieza = tipo.TiemposLimpieza.Where(m => m.Activa).Select(m => new { m.IdTipoLimpieza, m.Minutos }),
                             }).ToList();

            if (resultado.Count == 0)
                return new List<DtoResumenTipoHabitacion>();

            var retorno = new List<DtoResumenTipoHabitacion>();

            foreach (var item in resultado) 
            {
                var resumen = new DtoResumenTipoHabitacion
                {
                    Id = item.Id,
                    Descripcion = item.descripcion,
                    DescripcionExtendida = item.descripcionExt,
                };

                foreach (var tipoLimpieza in item.limpieza) 
                {
                    resumen.ResumenesLimpiezaHabitacion.Add(new DtoResumenTipoHabitacion.DtoResumenLimpiezaHabitacion
                    {
                        TipoLimpieza = (TareaLimpieza.TiposLimpieza)tipoLimpieza.IdTipoLimpieza,
                        Minutos = tipoLimpieza.Minutos
                    });
                }

                foreach (var tarifa in item.tarifas) 
                {
                    var resumenT = new DtoResumenTipoHabitacion.DtoResumenTarifa
                    {
                        Tarifa = (ConfiguracionTarifa.Tarifas)tarifa.tarifa,
                        Precio = tarifa.precio,
                    };

                    foreach (var tiempo in tarifa.tiempos) 
                    {
                        resumenT.ResumenesTiempo.Add(new DtoResumenTipoHabitacion.DtoResumenTiempo
                        {
                            Orden = tiempo.Orden,
                            Minutos = tiempo.Minutos
                        });
                    }

                    resumen.ResumenesTarifas.Add(resumenT);
                }

                retorno.Add(resumen);
            }

            return retorno;
        }

        public TipoHabitacion ObtenerTipoActivoCargado(int idTipoHabitacion)
        {
            var resultado = (from tipo in contexto.Set<TipoHabitacion>()
                             where tipo.Id == idTipoHabitacion
                             && tipo.Activo
                             select new
                             {
                                 tipo,
                                 tiempos = tipo.TiemposLimpieza.Where(m => m.Activa),
                                 tarifas = tipo.ConfiguracionesTipos.Where(m => m.Activa),
                                 extensiones = tipo.ConfiguracionesTipos.Where(m => m.Activa).SelectMany(m => m.TiemposHospedaje).Where(m => m.Activo),
                                 tarifasTmp = tipo.ConfiguracionesTipos.Where(m => m.Activa).Select(m => new { m.Id, m.ConfiguracionTarifa.IdTarifa })
                             }).ToList().FirstOrDefault();

            if (resultado == null)
                return null;

            foreach (var config in resultado.tarifas)
            {
                config.TarifaTmp = (ConfiguracionTarifa.Tarifas)resultado.tarifasTmp.First(m => m.Id == config.Id).IdTarifa;
            }

            return resultado.tipo;
        }


        public List<DtoCantidadesHabitacionTipoEstado> SP_ObtenerCantidadHabitacionesPorEstado(DateTime fecha)
        {
            var p_fecha = new SqlParameter("@fecha", fecha);
            p_fecha.SqlDbType = SqlDbType.DateTime;

            return contexto.ConsultaSql<DtoCantidadesHabitacionTipoEstado>("exec SotSchema.SP_ObtenerCantidadHabitacionesPorEstado @fecha", p_fecha);
        }
    }
}
