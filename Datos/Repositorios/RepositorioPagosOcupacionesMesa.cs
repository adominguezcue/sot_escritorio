﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPagosOcupacionesMesa : Repositorio<PagoOcupacionMesa>, IRepositorioPagosOcupacionesMesa
    {
        public RepositorioPagosOcupacionesMesa() : base(new Contextos.SOTContext()) { }

        public List<PagoOcupacionMesa> ObtenerPagosPorTransaccion(string transaccion)
        {
            return (from pago in contexto.Set<PagoOcupacionMesa>()
                    where pago.Activo && pago.Transaccion == transaccion
                    select pago).ToList();
        }
    }
}
