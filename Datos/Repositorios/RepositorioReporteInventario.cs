﻿using System;
using System.Collections.Generic;
using Modelo.Repositorios;
using Modelo.Entidades.Parciales;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using Datos.Nucleo.Contextos;

namespace Datos.Repositorios
{
    public class RepositorioReporteInventario : IRepositorioReporteInventario
    {

        List<CatalogoAlmacen> IRepositorioReporteInventario.ObtieneCatalogoAlmacen(string depto, string linea)
        {
            List<CatalogoAlmacen> respuesta = new List<CatalogoAlmacen>();
            DataSet dataSet = new DataSet();
            var Insumos = new SqlParameter("@vc_TipoArticulos", "I");
            Insumos.SqlDbType = System.Data.SqlDbType.VarChar;
            var departamentos = new SqlParameter("@vc_Deptos", depto);
            departamentos.SqlDbType = System.Data.SqlDbType.VarChar;
            var lineas  = new SqlParameter("@vc_lineas", linea);
            lineas.SqlDbType = System.Data.SqlDbType.VarChar;
            dataSet = EjecutarComandoSqlSP1("SotSchema.SP_ConCatalogoAlmacen", Insumos, departamentos, lineas);
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    CatalogoAlmacen catalogoalm = new CatalogoAlmacen();
                    catalogoalm.idCodigo= Convert.ToInt32(fila["codigo"]);
                    catalogoalm.DescripcionAlmacen = fila["descripcion"].ToString();
                    respuesta.Add(catalogoalm);
                }
            }
            return respuesta;

        }

        List<CatalogoLinea> IRepositorioReporteInventario.ObtienecatalogoLinea(string deptos)
        {
            List<CatalogoLinea> respuesta = new List<CatalogoLinea>();
            DataSet dataSet = new DataSet();
            var Insumos = new SqlParameter("@vc_TipoArticulos", "I");
            Insumos.SqlDbType = System.Data.SqlDbType.VarChar;
            var departamento = new SqlParameter("@vc_Deptos", deptos);
            departamento.SqlDbType = System.Data.SqlDbType.VarChar;
            dataSet = EjecutarComandoSqlSP1("SotSchema.SP_ConCatalogoLinea", Insumos, departamento);
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    CatalogoLinea catalogoalm = new CatalogoLinea();
                    catalogoalm.codigolinea = Convert.ToInt32(fila["codigo"]);
                    catalogoalm.codigodepto= Convert.ToInt32(fila["depto"]);
                    catalogoalm.descripcionlinea = fila["descripcion"].ToString();
                    respuesta.Add(catalogoalm);
                }
            }
            return respuesta;

        }

        List<CatalogoDepartamento> IRepositorioReporteInventario.ObtieneCatalogoDepto(string deptos)
        {
            List<CatalogoDepartamento> respuesta = new List<CatalogoDepartamento>();
            DataSet dataSet = new DataSet();
            var Insumos = new SqlParameter("@vc_TipoArticulos", "I");
            Insumos.SqlDbType = System.Data.SqlDbType.VarChar;
            var departamento = new SqlParameter("@vc_Deptos", deptos);
            departamento.SqlDbType = System.Data.SqlDbType.VarChar;
            dataSet = EjecutarComandoSqlSP1("SotSchema.SP_ConCatalogoDepto", Insumos, departamento);
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    CatalogoDepartamento catalogoalm = new CatalogoDepartamento();
                    catalogoalm.codigoDepto = Convert.ToInt32(fila["codigo"]);
                    catalogoalm.descripciondepto = fila["descripcion"].ToString();
                    respuesta.Add(catalogoalm);
                }
            }
            return respuesta;
        }


        private DataSet EjecutarComandoSqlSP1(string sql, params object[] parametros)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlConnection connection1 = new SqlConnection();
            DataSet respuesta = new DataSet();
            try
            {

                connection1.ConnectionString = ConexionHelper.CadenaConexion.ToString();
                connection1.Open();
                SqlCommand cmd = new SqlCommand(sql, connection1);
                cmd.CommandType = CommandType.StoredProcedure;
                if (parametros != null && parametros.Length > 0)
                    foreach (var item in parametros)
                    {
                        cmd.Parameters.Add(new SqlParameter(((System.Data.SqlClient.SqlParameter)item).ParameterName, ((System.Data.SqlClient.SqlParameter)item).Value));
                    }
                da.SelectCommand = cmd;
                da.Fill(respuesta);
                connection1.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            return respuesta;
        }

        List<ArticulosInventarioActual> IRepositorioReporteInventario.ObtieneArticulosInvActual(string almacen, string articulos, string linea, string depto)
        {
            List<ArticulosInventarioActual> respuesta = new List<ArticulosInventarioActual>();
            var vcAlmacenes = new SqlParameter("@vc_codigosalmacen", almacen);
            vcAlmacenes.SqlDbType = System.Data.SqlDbType.VarChar;
            var  vcArticulos = new SqlParameter("@vc_Codigosarticulos", articulos);
            vcArticulos.SqlDbType = System.Data.SqlDbType.VarChar;
            var vclinea = new SqlParameter("@vc_codigosLineas", linea);
            vclinea.SqlDbType = System.Data.SqlDbType.VarChar;
            var vcdepto = new SqlParameter("@vc_codigodeptos", depto);
            vcdepto.SqlDbType = System.Data.SqlDbType.VarChar;
            DataSet dataSet = new DataSet();
            dataSet = EjecutarComandoSqlSP1("SotSchema.SP_ConExistenciaProductosReporteActual", vcAlmacenes, vcArticulos, vclinea, vcdepto);
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    ArticulosInventarioActual articulo = new ArticulosInventarioActual();
                    articulo.CodigoArticulo = fila["CodigoProd"].ToString();
                    articulo.CodigoAlmacen = Convert.ToInt32(fila["CodigoAlmacen"]);
                    articulo.Des_Almacen= fila["Desc_Almacen"].ToString();  
                    articulo.CodigoLigea = Convert.ToInt32(fila["Codigolinea"]);
                    articulo.Des_Linea = fila["Desc_Linea"].ToString();
                    articulo.CodigoDepto = Convert.ToInt32(fila["CodigoDepto"]);
                    articulo.Des_Depto = fila["Desc_Depto"].ToString();
                    articulo.Des_Articulo = fila["Desc_prod"].ToString();
                    articulo.Existencia = Convert.ToInt32(fila["existencias"]);
                    articulo.CostoPromedio = Convert.ToDecimal(fila["costopromedio"]);
                    if (articulo.Existencia != 0)
                        articulo.CostoUnitario = articulo.CostoPromedio / articulo.Existencia;
                    else
                        articulo.CostoUnitario = 0;

                    respuesta.Add(articulo);
                }
            }


            return respuesta;
        }
    }
}
