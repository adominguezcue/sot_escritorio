﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioPaquetes : Repositorio<Paquete>, IRepositorioPaquetes
    {
        public RepositorioPaquetes() : base(new Contextos.SOTContext()) { }

        public List<Paquete> ObtenerPaquetesActivosConProgramas()
        {
            return (from paquete in contexto.Set<Paquete>()
                    where paquete.Activo
                    select new
                    {
                        paquete,
                        tipoH = paquete.TipoHabitacion,
                        programas = paquete.ProgramasPaquete.Where(m => m.Activo)
                    }).ToList().Select(m => m.paquete).ToList();
        }


        public List<Paquete> ObtenerPaquetesActivosConProgramasPorTipoHabitacion(int idTipoHabitacion)
        {
            return (from paquete in contexto.Set<Paquete>()
                    where paquete.Activo
                    && paquete.IdTipoHabitacion == idTipoHabitacion
                    select new
                    {
                        paquete,
                        tipoH = paquete.TipoHabitacion,
                        programas = paquete.ProgramasPaquete.Where(m => m.Activo)
                    }).ToList().Select(m => m.paquete).ToList();
        }


        public Paquete ObtenerPaqueteActivoConProgramas(int idPaquete)
        {
            return (from paquete in contexto.Set<Paquete>()
                    where paquete.Activo
                    && paquete.Id == idPaquete
                    select new
                    {
                        paquete,
                        programas = paquete.ProgramasPaquete.Where(m => m.Activo)
                    }).ToList().Select(m => m.paquete).FirstOrDefault();
        }
    }
}
