﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPremios : Repositorio<Premio>, IRepositorioPremios
    {
        public RepositorioPremios() : base(new Contextos.SOTContext()) { }

        public Premio ObtenerPorNumeroCargado(string numeroPremio)
        {
            return (from promo in contexto.Set<Premio>()
                    where promo.Activa && promo.Numero == numeroPremio
                    select new
                    {
                        promo,
                        arts = promo.ArticulosPremio.Where(m => m.Activo)
                    }).ToList().Select(m => m.promo).FirstOrDefault();
        }
    }
}
