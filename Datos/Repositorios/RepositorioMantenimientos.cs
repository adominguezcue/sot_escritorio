﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioMantenimientos : Repositorio<Mantenimiento>, IRepositorioMantenimientos
    {
        public RepositorioMantenimientos() : base(new Contextos.SOTContext()) { }

        public List<DtoResumenMantenimiento> ObtenerMantenimientosHabitacion(int idHabitacion, int?  idconcepto = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0)
        {
            List<DtoResumenMantenimiento> respuesta = new List<DtoResumenMantenimiento>();
            //RepositorioMantenimientos.ObtenerElementos(m => m.IdHabitacion == idHabitacion, m => m.FechaCreacion, pagina, elementos).ToList();

            Expression<Func<Mantenimiento, bool>> filtro = null;
			
            if (idHabitacion != 0)
                filtro = filtro = m => m.IdHabitacion == idHabitacion;

            if(idconcepto!= 0)
            {
                if (filtro == null)
                    filtro = filtro = m => m.IdConceptoMantenimiento == idconcepto;
                else
                    filtro = filtro.Compose(m => m.IdConceptoMantenimiento == idconcepto, Expression.AndAlso);
            }

            if (fechaInicio.HasValue && fechaFin.HasValue ) 
            {
                if (filtro == null)
                    filtro = m => m.FechaCreacion >= fechaInicio && m.FechaCreacion <= fechaFin;
                else
                    filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio && m.FechaCreacion <= fechaFin, Expression.AndAlso);
            }								  
            // filtro.Compose(m => fechaInicio <= m.FechaCreacion, Expression.AndAlso);
         /*   if (fechaFin.HasValue)
            {
                if (filtro == null)
                    filtro = m => m.FechaCreacion <= fechaFin;
                else
                    filtro = filtro.Compose(m => fechaFin <= m.FechaCreacion, Expression.AndAlso);
            }*/
            //filtro.Compose(m => fechaFin >= m.FechaCreacion, Expression.AndAlso);

            var resultado = (from mantenimiento in contexto.Set<Mantenimiento>().Where(filtro)
                             orderby mantenimiento.FechaCreacion
                             select new
                             {
                                 mantenimiento.IdHabitacion,															
                                 mantenimiento.IdTareaMantenimiento,
                                 mantenimiento.FechaCreacion,
                                 mantenimiento.IdUsuarioCreo,
                                 mantenimiento.ConceptoMantenimiento.Concepto,
                                 mantenimiento.IdEstado,
                                 mantenimiento.TareaMantenimiento.Descripcion
                             });

            if (pagina == 0 && elementos == 0)
            {			 
                respuesta = resultado.ToList().Select(m => new DtoResumenMantenimiento
                {
                    IdHabitacion = m.IdHabitacion,												  
                    IdTarea = m.IdTareaMantenimiento,
                    Concepto = m.Concepto,
                    Descripcion = m.Descripcion,
                    Fecha = m.FechaCreacion,
                    IdUsuario = m.IdUsuarioCreo,
                    Estado = (Mantenimiento.Estados)m.IdEstado
                }).ToList();

            }
            else				
            {

                respuesta = resultado.Skip(pagina * elementos).Take(elementos).ToList().Select(m => new DtoResumenMantenimiento
                {
                    IdHabitacion = m.IdHabitacion,										
					IdTarea = m.IdTareaMantenimiento,
					Concepto = m.Concepto,
					Descripcion = m.Descripcion,
					Fecha = m.FechaCreacion,
					IdUsuario = m.IdUsuarioCreo,
					Estado = (Mantenimiento.Estados)m.IdEstado
                }).ToList();
            }
			
			
            respuesta = ObtieneNumeroDehabitacion(respuesta);
			
            return respuesta;
        }
        private List<DtoResumenMantenimiento> ObtieneNumeroDehabitacion(List<DtoResumenMantenimiento>  respuesta1)
        {
            List<DtoResumenMantenimiento> respuesta2 = new List<DtoResumenMantenimiento>();
            foreach (DtoResumenMantenimiento datos in respuesta1)
            {

                var resultado = (from habitacion in contexto.Set<Habitacion>()
                                 where habitacion.Id == datos.IdHabitacion
                                 select new 
                                 {
                                     habitacion.NumeroHabitacion
                                 }).FirstOrDefault();
                string habitacion1 = resultado.NumeroHabitacion;
                datos.NumeroHabitacio = habitacion1;
                respuesta2.Add(datos);

            }

            return respuesta2;
        }																												  			 
    }
}
