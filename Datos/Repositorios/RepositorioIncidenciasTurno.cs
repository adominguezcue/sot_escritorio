﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioIncidenciasTurno : Repositorio<IncidenciaTurno>, IRepositorioIncidenciasTurno
    {
        public RepositorioIncidenciasTurno() : base(new Contextos.SOTContext()) { }

        public List<DtoIncidenciaTurno> ObtenerIncidenciasFiltradas(DateTime? fechaInicial, DateTime? fechaFinal, int? idCorte = null, string area=null)
        {
            Expression<Func<IncidenciaTurno, bool>> filtro = m => m.Activa;

            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m=>m.FechaCreacion >= fechaInicial, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFinal, Expression.AndAlso);
            if (idCorte.HasValue)
                filtro = filtro.Compose(m => m.IdCorteTurno == idCorte, Expression.AndAlso);
            if (area != null && area.Length>1)
                filtro = filtro.Compose(m => m.AreaIncidencia.Nombre == area, Expression.AndAlso);
            //if (idEmpleado.HasValue)
            //    filtro = filtro.Compose(m => m.IdEmpleadoReporta == idEmpleado, Expression.AndAlso);

            var resultado = (from incidencia in contexto.Set<IncidenciaTurno>().Where(filtro)
                             select new
                             {
                                 incidencia,
                                 Area = incidencia.AreaIncidencia.Nombre,
                                 IdUsuario = incidencia.IdUsuarioCreo,
                                 NumeroCorte = incidencia.CorteTurno.NumeroCorte,
                             }).ToList().Select(m => new DtoIncidenciaTurno
                             {
                                 Id = m.incidencia.Id,
                                 EstaCancelada = m.incidencia.Cancelada,
                                 Nombre = m.incidencia.Nombre,
                                 Descripcion = m.incidencia.Descripcion,
                                 Area = m.Area,
                                 FechaCreacion = m.incidencia.FechaCreacion,
                                 IdUsuario = m.IdUsuario,
                                 NumeroCorte = m.NumeroCorte
                             }).ToList();

            return resultado;
        }
    }
}
