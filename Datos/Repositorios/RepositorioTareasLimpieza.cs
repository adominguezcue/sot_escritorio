﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;
using System.Linq.Expressions;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioTareasLimpieza : Repositorio<TareaLimpieza>, IRepositorioTareasLimpieza
    {
        public RepositorioTareasLimpieza() : base(new Contextos.SOTContext())
        {
        }

        public TareaLimpieza ObtenerConEmpleados(int idHabitacion)
        {
            return (from limpieza in contexto.Set<TareaLimpieza>()
                    where limpieza.Activa
                    && limpieza.IdHabitacion == idHabitacion
                    select new
                    {
                        limpieza,
                        empleadosLimpieza = limpieza.LimpiezaEmpleados.Where(m => m.Activo),
                        empleados = limpieza.LimpiezaEmpleados.Where(m => m.Activo).Select(m => m.Empleado),
                        supervisor = limpieza.EmpleadoSupervisor
                    }).ToList().Select(m => m.limpieza).FirstOrDefault();
        }


        public List<LimpiezaEmpleado> ObtenerEmpleadosLimpiezaActivos()
        {
            var datos = (from empleadoLimpieza in contexto.Set<LimpiezaEmpleado>()
                         where empleadoLimpieza.Activo
                         select new
                         {
                             empleadoLimpieza,
                             numeroHabitacion = empleadoLimpieza.TareaLimpieza.Habitacion.NumeroHabitacion
                         }).ToList();

            datos.ForEach(m => m.empleadoLimpieza.NumeroHabitacionActual = m.numeroHabitacion);

            return datos.Select(m => m.empleadoLimpieza).ToList();
        }


        public List<Modelo.Dtos.DtoDetallesTareaImpuro> ObtenerDetallesImpuros(int? ordenTurno = null, int? idEmpleado = null, int? idTipoHabitacion = null, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            int idEstadoCreada = (int)TareaLimpieza.Estados.Creada;

            Expression<Func<TareaLimpieza, bool>> filtro = m => m.IdEstado != idEstadoCreada && m.FechaInicioLimpieza.HasValue;

            if (ordenTurno.HasValue)
                filtro = filtro.Compose(m => m.CorteTurno.ConfiguracionTurno.Orden == ordenTurno, Expression.AndAlso);

            if (idEmpleado.HasValue)
                filtro = filtro.Compose(m => m.LimpiezaEmpleados.Any(e => (e.Activo || e.TerminoLimpieza) && e.IdEmpleado == idEmpleado) || m.IdEmpleadoSupervisa == idEmpleado, Expression.AndAlso);

            if (idTipoHabitacion.HasValue)
                filtro = filtro.Compose(m => m.Habitacion.IdTipoHabitacion == idTipoHabitacion, Expression.AndAlso);

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaInicioLimpieza >= fechaInicio, Expression.AndAlso);

            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaInicioLimpieza <= fechaFin, Expression.AndAlso);



            return (from tareasL in contexto.Set<TareaLimpieza>().Where(filtro)
                         select new
                         {
                             FolioCorte = tareasL.CorteTurno.ConfiguracionTurno.Orden,
                             IdHabitacion = tareasL.IdHabitacion,
                             IdTipo = tareasL.IdTipoLimpieza,
                             IdEstado = tareasL.IdEstado,
                             TipoHabitacion = tareasL.Habitacion.TipoHabitacion.Descripcion,
                             Habitacion = tareasL.Habitacion.NumeroHabitacion,// + " - " + tareasL.Habitacion.TipoHabitacion.Descripcion,
                             FechaCreacion = tareasL.FechaCreacion,
                             FechaInicioLimpieza = tareasL.FechaInicioLimpieza,
                             FechaFinLimpieza = tareasL.FechaFinLimpieza,
                             FechaInicioSupervision = tareasL.FechaInicioSupervision,
                             FechaFinSupervision = tareasL.FechaFinSupervision,
                             empleados = tareasL.LimpiezaEmpleados.Where(m => m.Activo || m.TerminoLimpieza).Select(m => new { m.Empleado.Nombre, m.Empleado.ApellidoPaterno, m.Empleado.ApellidoMaterno }),
                             NombreSupervisor = tareasL.IdEmpleadoSupervisa.HasValue ? tareasL.EmpleadoSupervisor.Nombre : null,
                             ApellidoPSupervisor = tareasL.IdEmpleadoSupervisa.HasValue ? tareasL.EmpleadoSupervisor.ApellidoPaterno : null,
                             ApellidoMSupervisor = tareasL.IdEmpleadoSupervisa.HasValue ? tareasL.EmpleadoSupervisor.ApellidoMaterno : null,
                             MotivoLiberacion = tareasL.Motivo.Concepto,
                             Observaciones = tareasL.Observaciones
                         }).ToList().Select(m => new Modelo.Dtos.DtoDetallesTareaImpuro
                         {
                             FolioCorte = m.FolioCorte,
                             IdHabitacion = m.IdHabitacion,
                             Tipo = (TareaLimpieza.TiposLimpieza)m.IdTipo,
                             Estado = (TareaLimpieza.Estados)m.IdEstado,
                             TipoHabitacion = m.TipoHabitacion,
                             Habitacion = m.Habitacion,
                             FechaCreacion = m.FechaCreacion,
                             FechaInicioLimpieza = m.FechaInicioLimpieza,
                             FechaFinLimpieza = m.FechaFinLimpieza,
                             FechaInicioSupervision = m.FechaInicioSupervision,
                             FechaFinSupervision = m.FechaFinSupervision,
                             NombresRecamareras = m.empleados.Select(e => string.Join(" ", e.Nombre, e.ApellidoPaterno, e.ApellidoMaterno)).ToList(),
                             NombreSupervisor = string.Join(" ", m.NombreSupervisor, m.ApellidoPSupervisor, m.ApellidoMSupervisor),
                             MotivoLiberacion = string.IsNullOrWhiteSpace(m.Observaciones) ? m.MotivoLiberacion : (m.MotivoLiberacion + Environment.NewLine + Environment.NewLine + m.Observaciones)
                         }).ToList();
        }
    }
}
