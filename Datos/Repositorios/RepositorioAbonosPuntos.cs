﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioAbonosPuntos : Repositorio<AbonoPuntos>, IRepositorioAbonosPuntos
    {
        public RepositorioAbonosPuntos() : base(new Contextos.SOTContext()) { }

        public List<AbonoPuntos> ObtenerAbonosFallidos(int? idHabitacion, DateTime? fechaInicial, DateTime? fechaFinal)
        {
            Expression<Func<AbonoPuntos, bool>> filtro = m => m.Activo && !m.Abierto && !m.Abonado;

            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m => m.FechaModificacion >= fechaInicial, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaModificacion <= fechaFinal, Expression.AndAlso);
            if (idHabitacion.HasValue)
                filtro = filtro.Compose(m => m.Renta.IdHabitacion == idHabitacion, Expression.AndAlso);

            var resultado = (from abono in contexto.Set<AbonoPuntos>().Where(filtro)
                             select new
                             {
                                 abono,
                                 NombreH = abono.Renta.Habitacion.NumeroHabitacion + " " + abono.Renta.Habitacion.TipoHabitacion.Descripcion
                             }).ToList();

            resultado.ForEach(m => m.abono.NumeroHabitacionTmp = m.NombreH);

            return resultado.Select(m => m.abono).ToList();
        }
    }
}
