﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioCentrosCostos : Repositorio<CentroCostos>, IRepositorioCentrosCostos
    {
        public RepositorioCentrosCostos() : base(new Contextos.SOTContext()) { }

        public List<CentroCostos> ObtenerCentrosCostosConNombreCategoria()
        {
            var respuesta = (from centroC in contexto.Set<CentroCostos>()
                             where centroC.Activo
                             select new
                             {
                                 centroC,
                                 nombreC = centroC.CategoriaCentroCostos.Nombre
                             }).ToList();

            respuesta.ForEach(m => m.centroC.NombreCategoriaTmp = m.nombreC);

            return respuesta.Select(m => m.centroC).ToList();
        }

        public List<CentroCostos> ObtenerCentrosCostosFiltrados(string nombreCentro, int? idCategoriaCentro)
        {
            Expression<Func<CentroCostos, bool>> filtro = m => m.Activo;

            if (!string.IsNullOrEmpty(nombreCentro))
            {
                var nombreU = nombreCentro.Trim().ToUpper();
                filtro = filtro.Compose(m => m.Nombre.Trim().ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (idCategoriaCentro.HasValue)
                filtro = filtro.Compose(m => m.IdCategoria == idCategoriaCentro, Expression.AndAlso);

            var respuesta = (from centroC in contexto.Set<CentroCostos>().Where(filtro)
                             select new
                             {
                                 centroC,
                                 nombreC = centroC.CategoriaCentroCostos.Nombre
                             }).ToList();

            respuesta.ForEach(m => m.centroC.NombreCategoriaTmp = m.nombreC);

            return respuesta.Select(m => m.centroC).ToList();
        }


        //public decimal ObtenerPresupuestoRestante(int idCentroCostos, int? idConceptoIgnorar)
        //{
        //    Expression<Func<ConceptoGasto, bool>> filtro = m => m.Activo;

        //    if (idConceptoIgnorar.HasValue)
        //        filtro = filtro.Compose(m => m.Id != idConceptoIgnorar, Expression.AndAlso);

        //    //var filtro2 = filtro.Compile();

        //    return (from centro in contexto.Set<CentroCostos>()
        //            where centro.Activo && centro.Id == idCentroCostos
        //            select new
        //            {
        //                topeCentro = centro.TopePresupuestal,
        //                topesConceptos = centro.ConceptosGasto.AsQueryable().Where(filtro).Select(m => m.TopePresupuestal)
        //            }).ToList().Select(m => m.topeCentro - m.topesConceptos.ToList().Sum()).FirstOrDefault();
        //}


        //public decimal ObtenerPresupuestoGastos(int idCentroCostos)
        //{
        //    return (from centro in contexto.Set<CentroCostos>()
        //            where centro.Activo && centro.Id == idCentroCostos
        //            select new
        //            {
        //                topesConceptos = centro.ConceptosGasto.Where(m=>m.Activo).Select(m => m.TopePresupuestal)
        //            }).ToList().Select(m => m.topesConceptos.ToList().Sum()).FirstOrDefault();
        //}
    }
}