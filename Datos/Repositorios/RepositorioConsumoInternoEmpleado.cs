﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Datos.Nucleo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Transversal.Extensiones;
using System.Text;
using System.Threading.Tasks;
using Modelo.Dtos;
using System.Data.SqlClient;
using System.Data;

namespace Datos.Repositorios
{
    public class RepositorioConsumoInternoEmpleado : Repositorio<VW_ConsumoInternoEmpleado>, IRepositorioConsumoInternoEmpleado
    {
        public RepositorioConsumoInternoEmpleado() : base(new Contextos.SOTContext())
        { }

        public List<DtoConsumoInternosEmpleado> ObtieneConsumoInternoempleado(string numeroempleado, DateTime fechainicial, DateTime fechafinal, bool Esfechashablitadas)
        {
            List<DtoConsumoInternosEmpleado> respuesta = new List<DtoConsumoInternosEmpleado>();

            var vcNumempleado = new SqlParameter("@vi_NumeroEmpleado", numeroempleado);
            vcNumempleado.SqlDbType = System.Data.SqlDbType.VarChar;
            DataSet dataSet = new DataSet();
            if (Esfechashablitadas)
            {
                fechafinal = fechafinal.AddDays(1);
                fechafinal = fechafinal.AddMinutes(-1);
                var fdfechainicial = new SqlParameter("@vd_fechaInicial", fechainicial);
                fdfechainicial.SqlDbType = System.Data.SqlDbType.DateTime;
                var fdfechafinal = new SqlParameter("@vd_fechaFinal", fechafinal);
                fdfechafinal.SqlDbType = System.Data.SqlDbType.DateTime;
                dataSet = contexto.EjecutarComandoSqlSP("SotSchema.SP_ConConsumoInternoEmpleado", vcNumempleado, fdfechainicial, fdfechafinal);
                if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        DtoConsumoInternosEmpleado dtoConsumoInternosEmpleado = new DtoConsumoInternosEmpleado();
                        dtoConsumoInternosEmpleado.codigoArticulo= fila["CodigoArticulo"].ToString();
                        dtoConsumoInternosEmpleado.NumeroEmpleado = numeroempleado;
                        dtoConsumoInternosEmpleado.Articulodescripcion = fila["ArticuloDescripcion"].ToString();
                        dtoConsumoInternosEmpleado.cantidad = Convert.ToInt32(fila["Cantidad"]);
                        dtoConsumoInternosEmpleado.total = Convert.ToDecimal(fila["Total"]);
                        respuesta.Add(dtoConsumoInternosEmpleado);
                    }
                }
            }
            else
            {
                dataSet = contexto.EjecutarComandoSqlSP("SotSchema.SP_ConConsumoInternoEmpleado", vcNumempleado);
                if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
                {
                    foreach (DataRow fila in dataSet.Tables[0].Rows)
                    {
                        DtoConsumoInternosEmpleado dtoConsumoInternosEmpleado = new DtoConsumoInternosEmpleado();
                        dtoConsumoInternosEmpleado.NumeroEmpleado = numeroempleado;
                        dtoConsumoInternosEmpleado.codigoArticulo = fila["CodigoArticulo"].ToString();
                        dtoConsumoInternosEmpleado.Articulodescripcion = fila["ArticuloDescripcion"].ToString();
                        dtoConsumoInternosEmpleado.cantidad = Convert.ToInt32(fila["Cantidad"]);
                        dtoConsumoInternosEmpleado.total = Convert.ToDecimal(fila["Total"]);
                        respuesta.Add(dtoConsumoInternosEmpleado);
                    }
                }
            }

            return respuesta;
        }

        public List<VW_ConsumoInternoEmpleado> ObtieneEmpleados(string nombreempleado)
        {
            List<VW_ConsumoInternoEmpleado> respuesta = new List<VW_ConsumoInternoEmpleado>();

         /* Expression<Func<VW_ConsumoInternoEmpleado, bool>> filtro = null;

            if(!nombreempleado.Equals(""))
            {
                if (filtro == null)
                    filtro = m => m.NombreCompleto == nombreempleado;
                else
                    filtro = filtro.Compose(m => m.NombreCompleto == nombreempleado, Expression.AndAlso);
            }*/

            if (!nombreempleado.Equals(""))
            {
                var ListaempleadosconsumoInterno = contexto.Set<VW_ConsumoInternoEmpleado>().Where(m=>m.NombreCompleto.Contains(nombreempleado)).ToList();
                respuesta = ListaempleadosconsumoInterno as List<VW_ConsumoInternoEmpleado>;

            }
            else
            {
                var ListaempleadosconsumoInterno = contexto.Set<VW_ConsumoInternoEmpleado>().ToList();
                respuesta = ListaempleadosconsumoInterno as List<VW_ConsumoInternoEmpleado>;
            }
            return respuesta;
        }


    }
}
