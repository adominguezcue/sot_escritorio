﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPagosRenta : Repositorio<PagoRenta>, IRepositorioPagosRenta
    {
        public RepositorioPagosRenta() : base(new Contextos.SOTContext()) { }

        public List<PagoRenta> ObtenerPagosPorTransaccion(string transaccion, bool incluirCancelada/*, DateTime? fechaFin*/)
        {
            //if (fechaFin.HasValue)
            //    return (from pago in contexto.Set<PagoRenta>()
            //            where (pago.Activo || (pago.FechaEliminacion.HasValue && pago.FechaEliminacion > fechaFin))
            //            && pago.Transaccion == transaccion
            //            select pago).ToList();

            if (incluirCancelada)
                return (from pago in contexto.Set<PagoRenta>()
                        where pago.Transaccion == transaccion
                        select pago).ToList();

            return (from pago in contexto.Set<PagoRenta>()
                    where pago.Activo && pago.Transaccion == transaccion
                    select pago).ToList();
        }

        public VentaRenta ObtenerVentaConConceptos(int idVentaRenta)
        {
            var datos = (from venta in contexto.Set<VentaRenta>()
                         where venta.Activo && venta.Id == idVentaRenta
                         select new
                         {
                             venta,
                             claveTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.Clave,
                             Descripcion = venta.Renta.ConfiguracionTipo.TipoHabitacion.Descripcion,
                             claveSATTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.ClaveSAT,
                             //horasE = venta.TiemposExtra.Where(m => m.Activa),
                             montosNoR = venta.MontosNoReembolsablesRenta.Where(m => m.Activo),
                             personasE = venta.PersonasExtra.Where(m => m.Activa && m.Precio !=0),
                             //renovaciones = venta.Renovaciones.Where(m => m.Activa),
                             extensiones = venta.Extensiones.Where(m => m.Activa),
                             paquetesRenta = venta.PaquetesRenta.Where(m => m.Activo),
                             paquetes = venta.PaquetesRenta.Where(m => m.Activo).Select(m => m.Paquete),
                             precioHotel = venta.EsInicial ? venta.Renta.PrecioHabitacion : 0
                         }).ToList().FirstOrDefault();

            if (datos == null)
                return null;

            datos.venta.TipoHabitacionTmp = datos.Descripcion;
            datos.venta.PrecioHotelTmp = datos.precioHotel;
            datos.venta.ClaveTipoHabitacionTmp = datos.claveTipo;
            datos.venta.ClaveSATTipoHabitacionTmp = datos.claveSATTipo;

            return datos.venta;
        }



        public VentaRenta ObtenerVentaCanceladaConConceptos(int idVentaRenta)
        {
            var datos = (from venta in contexto.Set<VentaRenta>()
                          where venta.Id == idVentaRenta
                          && !venta.Activo
                          select new
                          {
                              venta,
                              claveTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.Clave,
                              Descripcion = venta.Renta.ConfiguracionTipo.TipoHabitacion.Descripcion,
                              claveSATTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.ClaveSAT,
                              //horasE = venta.TiemposExtra.Where(m => m.Activa),
                              personasE = venta.PersonasExtra.Where(m => m.Activa || m.FechaEliminacion >= venta.FechaEliminacion),
                              //renovaciones = venta.Renovaciones.Where(m => m.Activa),
                              extensiones = venta.Extensiones.Where(m => m.Activa || m.FechaEliminacion >= venta.FechaEliminacion),
                              paquetesRenta = venta.PaquetesRenta.Where(m => m.Activo || m.FechaEliminacion >= venta.FechaEliminacion),
                              mnr = venta.MontosNoReembolsablesRenta.Where(m => m.Activo || m.FechaEliminacion >= venta.FechaEliminacion),
                              paquetes = venta.PaquetesRenta.Where(m => m.Activo || m.FechaEliminacion >= venta.FechaEliminacion).Select(m => m.Paquete),
                              precioHotel = venta.EsInicial ? venta.Renta.PrecioHabitacion : 0
                          }).ToList().FirstOrDefault();

            if (datos == null)
                return null;

            datos.venta.TipoHabitacionTmp = datos.Descripcion;
            datos.venta.PrecioHotelTmp = datos.precioHotel;
            datos.venta.ClaveTipoHabitacionTmp = datos.claveTipo;
            datos.venta.ClaveSATTipoHabitacionTmp = datos.claveSATTipo;

            return datos.venta;
        }

        public VentaRenta ObtenerVentaConConceptos(string transaccion, bool incluirCancelada)
        {
            var datos = incluirCancelada ?
                         (from venta in contexto.Set<VentaRenta>()
                          where venta.Transaccion == transaccion
                          select new
                          {
                              venta,
                              claveTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.Clave,
                              Descripcion = venta.Renta.ConfiguracionTipo.TipoHabitacion.Descripcion,
                              claveSATTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.ClaveSAT,
                              //horasE = venta.TiemposExtra.Where(m => m.Activa),
                              personasE = venta.PersonasExtra.Where(m => m.Activa || m.FechaEliminacion >= venta.FechaEliminacion),
                              //renovaciones = venta.Renovaciones.Where(m => m.Activa),
                              extensiones = venta.Extensiones.Where(m => m.Activa || m.FechaEliminacion >= venta.FechaEliminacion),
                              paquetesRenta = venta.PaquetesRenta.Where(m => m.Activo || m.FechaEliminacion >= venta.FechaEliminacion),
                              mnr = venta.MontosNoReembolsablesRenta.Where(m => m.Activo || m.FechaEliminacion >= venta.FechaEliminacion),
                              paquetes = venta.PaquetesRenta.Where(m => m.Activo || m.FechaEliminacion >= venta.FechaEliminacion).Select(m => m.Paquete),
                              precioHotel = venta.EsInicial ? venta.Renta.PrecioHabitacion : 0
                          }).ToList().FirstOrDefault() :
                (from venta in contexto.Set<VentaRenta>()
                 where venta.Activo && venta.Transaccion == transaccion
                 select new
                 {
                     venta,
                     claveTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.Clave,
                     Descripcion = venta.Renta.ConfiguracionTipo.TipoHabitacion.Descripcion,
                     claveSATTipo = venta.Renta.ConfiguracionTipo.TipoHabitacion.ClaveSAT,
                     //horasE = venta.TiemposExtra.Where(m => m.Activa),
                     personasE = venta.PersonasExtra.Where(m => m.Activa && m.Precio !=0),
                     //renovaciones = venta.Renovaciones.Where(m => m.Activa),
                     extensiones = venta.Extensiones.Where(m => m.Activa),
                     paquetesRenta = venta.PaquetesRenta.Where(m => m.Activo),
                     mnr = venta.MontosNoReembolsablesRenta.Where(m => m.Activo),
                     paquetes = venta.PaquetesRenta.Where(m => m.Activo).Select(m => m.Paquete),
                     precioHotel = venta.EsInicial ? venta.Renta.PrecioHabitacion : 0
                 }).ToList().FirstOrDefault();

            if (datos == null)
                return null;

            datos.venta.TipoHabitacionTmp = datos.Descripcion;
            datos.venta.PrecioHotelTmp = datos.precioHotel;
            datos.venta.ClaveTipoHabitacionTmp = datos.claveTipo;
            datos.venta.ClaveSATTipoHabitacionTmp = datos.claveSATTipo;

            return datos.venta;
        }
    }
}
