﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Modelo.Dtos;

namespace Datos.Repositorios
{
    public class RepositorioEmpleados : Repositorio<Empleado>, IRepositorioEmpleados
    {
        public RepositorioEmpleados() : base(new Contextos.SOTContext())
        {
        }

        //public List<Empleado> ObtenerPorPuesto(string puesto, bool soloHabilitados)
        //{
        //    if (soloHabilitados)
        //        return (from empleado in contexto.Set<Empleado>()
        //                where empleado.Puesto.Nombre == puesto
        //                && empleado.Activo
        //                && empleado.Habilitado
        //                select empleado).ToList();

        //    return (from empleado in contexto.Set<Empleado>()
        //            where empleado.Puesto.Nombre == puesto
        //            && empleado.Activo
        //            select empleado).ToList();
        //}

        public List<Empleado> ObtenerPorPuesto(bool soloHabilitados, /*int? idTurno, */params int[]idsaPuestos)
        {
            Expression<Func<Empleado, bool>> filtro = empleado => idsaPuestos.Contains(empleado.IdPuesto) && empleado.Activo;

            if (soloHabilitados)
                filtro = filtro.Compose(m => m.Habilitado, Expression.AndAlso);

            //if (idTurno.HasValue)
            //{
            //    int idConfiguracionTurno = contexto.Set<CorteTurno>().Where(m => m.Id == idTurno).Select(m => m.IdConfiguracionTurno).FirstOrDefault();

            //    filtro = filtro.Compose(m => m.IdTurno == idConfiguracionTurno, Expression.AndAlso);
            //}

            //if (soloHabilitados)
            //    return (from empleado in contexto.Set<Empleado>()
            //            where empleado.IdPuesto == idPuesto
            //            && empleado.Activo
            //            && empleado.Habilitado
            //            && empleado.IdTurno == idTurno
            //            select empleado).ToList();

            //return (from empleado in contexto.Set<Empleado>()
            //        where empleado.IdPuesto == idPuesto
            //        && empleado.Activo
            //        && empleado.IdTurno == idTurno
            //        select empleado).ToList();

            var resultado = (from empleado in contexto.Set<Empleado>().Where(filtro)
                             select new
                             {
                                empleado,
                                puesto = empleado.Puesto.Nombre
                             }).ToList();

            resultado.ForEach(m => m.empleado.NombrePuesto = m.puesto);

            return resultado.Select(m => m.empleado).ToList();
        }

        public List<Empleado> ObtenerConNombrePuesto(bool soloHabilitados)
        {
            Expression<Func<Empleado, bool>> filtro = empleado => empleado.Activo;

            if (soloHabilitados)
                filtro = filtro.Compose(m => m.Habilitado, Expression.AndAlso);

            var resultado = (from empleado in contexto.Set<Empleado>().Where(filtro)
                             select new
                             {
                                 empleado,
                                 puesto = empleado.Puesto.Nombre
                             }).ToList();

            resultado.ForEach(m => m.empleado.NombrePuesto = m.puesto);

            return resultado.Select(m => m.empleado).ToList();
        }

        public Empleado ObtenerConCantidadTareas(int idEmpleado)
        {
            var query = (from empleado in contexto.Set<Empleado>()
                         where empleado.Id == idEmpleado
                         && empleado.Activo
                         && empleado.Habilitado
                         select new
                         {
                             empleado,
                             cantidadTareasLimpieza = empleado.LimpiezaEmpleados.Count(m => m.Activo)
                         }).ToList();

            foreach (var item in query)
                item.empleado.CantidadTareas = item.cantidadTareasLimpieza;

            return query.Select(m => m.empleado).FirstOrDefault();
        }


        public List<Empleado> ObtenerEmpleadosFiltrados(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaRegistro = null)
        {
            Expression<Func<Empleado, bool>> filtro = m => m.Activo;

            if(!string.IsNullOrEmpty(nombre))
            {
                var nombreU = nombre.Trim().ToUpper();
                filtro = filtro.Compose(m => m.Nombre.ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoPaterno))
            {
                var apellidoPaternoU = apellidoPaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.ApellidoPaterno.ToUpper().Contains(apellidoPaternoU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoMaterno))
            {
                var apellidoMaternoU = apellidoMaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.ApellidoMaterno.ToUpper().Contains(apellidoMaternoU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(telefono))
            {
                filtro = filtro.Compose(m => m.Telefono.Contains(telefono) || m.Celular.Contains(telefono), Expression.AndAlso);
            }
            if (fechaRegistro.HasValue)
            {
                filtro = filtro.Compose(m => m.FechaIngreso.Day == fechaRegistro.Value.Day && m.FechaIngreso.Month == fechaRegistro.Value.Month && m.FechaIngreso.Year == fechaRegistro.Value.Year, Expression.AndAlso);
            }

            var datos = (from empleado in contexto.Set<Empleado>().Where(filtro)
                         select new
                         {
                             empleado = empleado,
                             nombreP = empleado.Puesto.Nombre,
                             nombreA = empleado.Puesto.AreaActual.Nombre
                         }).ToList();

            if (datos.Count == 0)
                return new List<Empleado>();

            foreach (var dato in datos) 
            {
                dato.empleado.NombrePuesto = dato.nombreP;
                dato.empleado.NombreAreaTmp = dato.nombreA;
            }

            //datos.ForEach(m => m.empleado.NombrePuesto = m.nombreP);

            return datos.Select(m => m.empleado).ToList();
        }

        public List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreas()
        {
            return (from empleado in contexto.Set<Empleado>()
                    where empleado.Activo
                    select new
                    {
                        empleado.Id,
                        empleado.Nombre,
                        empleado.ApellidoPaterno,
                        empleado.ApellidoMaterno,
                        Puesto = empleado.Puesto.Nombre,
                        Area = empleado.Puesto.AreaActual.Nombre
                    }).ToList().Select(m => new DtoEmpleadoAreaPuesto
                    {
                        IdEmpleado = m.Id,
                        NombreEmpleado = string.Join(" ", m.Nombre, m.ApellidoPaterno, m.ApellidoMaterno),
                        Puesto = m.Puesto,
                        Area = m.Area
                    }).ToList();
        }

        public List<DtoEmpleadoPuesto> ObtenerNombreEmpleadoPuestoPorIdsConInactivos(params int[] idsEmpleados) 
        {
            if (idsEmpleados.Length == 0)
                return new List<DtoEmpleadoPuesto>();

            return (from empleado in contexto.Set<Empleado>()
                    where /*empleado.Activo
                    && */idsEmpleados.Contains(empleado.Id)
                    select new
                    {
                        empleado.Id,
                        empleado.Nombre,
                        empleado.ApellidoPaterno,
                        empleado.ApellidoMaterno,
                        Puesto = empleado.Puesto.Nombre,
                    }).ToList().Select(m => new DtoEmpleadoPuesto
                    {
                        IdEmpleado = m.Id,
                        NombreEmpleado = string.Join(" ", m.Nombre.Trim(), m.ApellidoPaterno.Trim()),
                        Puesto = m.Puesto,
                    }).ToList();
        }


        public DtoEmpleadoPuesto ObtenerResumenEmpleadoPuestoPorId(int idEmpleado)
        {
            return (from empleado in contexto.Set<Empleado>()
                    where /*empleado.Activo
                    && */empleado.Id == idEmpleado
                    select new
                    {
                        empleado.Id,
                        empleado.Nombre,
                        empleado.ApellidoPaterno,
                        empleado.ApellidoMaterno,
                        Puesto = empleado.Puesto.Nombre,
                    }).ToList().Select(m => new DtoEmpleadoPuesto
                    {
                        IdEmpleado = m.Id,
                        NombreEmpleado = string.Join(" ", m.Nombre, m.ApellidoPaterno, m.ApellidoMaterno),
                        Puesto = m.Puesto,
                    }).FirstOrDefault();
        }
    }
}
