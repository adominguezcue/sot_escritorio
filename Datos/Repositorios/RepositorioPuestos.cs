﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioPuestos : Repositorio<Puesto>, IRepositorioPuestos
    {
        public RepositorioPuestos() : base(new Contextos.SOTContext())
        {
        }
    }
}
