﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioConfiguracionesMantenimientoH : Repositorio<ConfiguracionHMantenimiento>, IRepositorioConfiguracionesMantenimientoH
    {
        public RepositorioConfiguracionesMantenimientoH() : base ( new Contextos.SOTContext()){}

        public bool EsInsertaRegistroPorCorteTurno(int idcorte)
        {
            bool respuesta = false;
            var idcorteturno = new SqlParameter("@idCorteTurno", idcorte);
            idcorteturno.SqlDbType = System.Data.SqlDbType.Int;
            DataSet dataSet = new DataSet();
            dataSet = contexto.EjecutarComandoSqlSP("SotSchema.SP_ConInsertaDatosMediciones", idcorteturno);
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    respuesta = Convert.ToBoolean(fila["Respuesta"]);
                }
            }
         return respuesta;
        }
    }
}
