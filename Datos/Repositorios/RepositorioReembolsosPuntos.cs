﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioReembolsosPuntos : Repositorio<ReembolsoPuntos>, IRepositorioReembolsosPuntos
    {
        public RepositorioReembolsosPuntos() : base(new Contextos.SOTContext()) { }

        public List<ReembolsoPuntos> ObtenerReembolsosFallidos(int? idHabitacion, DateTime? fechaInicial, DateTime? fechaFinal)
        {
            Expression<Func<ReembolsoPuntos, bool>> filtro = m => m.Activo && !m.Abonado;
            Expression<Func<Renta, bool>> filtroR = m => m.Id != 0;

            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m => m.FechaModificacion >= fechaInicial, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaModificacion <= fechaFinal, Expression.AndAlso);
            if (idHabitacion.HasValue)
                filtroR = filtroR.Compose(m => m.IdHabitacion == idHabitacion, Expression.AndAlso);

            var resultado = (from reembolso in contexto.Set<ReembolsoPuntos>().Where(filtro)
                             join renta in contexto.Set<Renta>().Where(filtroR) on reembolso.NumeroServicio equals renta.NumeroServicio
                             select new
                             {
                                 reembolso = reembolso,
                                 NombreH = renta.Habitacion.NumeroHabitacion + " " + renta.Habitacion.TipoHabitacion.Descripcion
                             }).ToList();

            resultado.ForEach(m => m.reembolso.NumeroHabitacionTmp = m.NombreH);

            return resultado.Select(m => m.reembolso).ToList();
        }
    }
}
