﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;
using System.Data.SqlClient;
using Modelo.Dtos;

namespace Datos.Repositorios
{
    public class RepositorioHabitaciones : Repositorio<Habitacion>, IRepositorioHabitaciones
    {
        public RepositorioHabitaciones() : base(new Contextos.SOTContext())
        {
        }

        public override IEnumerable<Habitacion> ObtenerTodo()
        {
            return contexto.Set<Habitacion>();
        }

        public List<Habitacion> ObtenerActivasConTipos()
        {
            List<Habitacion> respuesta = new List<Habitacion>();
            var query = (from habitacion in contexto.Set<Habitacion>()
                         where habitacion.Activo
                         select new
                         {
                             habitacion,
                             tipo = habitacion.TipoHabitacion
                         }).ToList();
						 
            return query.Select(m=>m.habitacion).ToList();
        }


        public Habitacion ObtenerConMotivoBloqueo(int idHabitacion)
        {
            var query = (from habitacion in contexto.Set<Habitacion>()
                         where habitacion.Activo
                         && habitacion.Id == idHabitacion
                         select new
                         {
                             habitacion,
                             bloqueo = habitacion.BloqueosHabitaciones.Where(m=>m.Activo)
                         }).ToList();

            return query.Select(m => m.habitacion).FirstOrDefault();
        }


        public Habitacion ObtenerConReservacionConfirmada(int idHabitacion)
        {
            int idEstadoConfirmada = (int)Reservacion.EstadosReservacion.Confirmada;

            var query = (from habitacion in contexto.Set<Habitacion>()
                         where habitacion.Activo
                         && habitacion.Id == idHabitacion
                         select new
                         {
                             habitacion,
                             reservaciones = habitacion.Reservaciones.Where(m => m.Activo && m.IdEstado == idEstadoConfirmada)
                         }).ToList();

            return query.Select(m => m.habitacion).FirstOrDefault();
        }


        public List<Habitacion> ObtenerPreparadasOHabilitadasConTipos()
        {
            var idEstadoH = (int)Habitacion.EstadosHabitacion.Libre;
            var idEstadoP = (int)Habitacion.EstadosHabitacion.Preparada;

            var query = (from habitacion in contexto.Set<Habitacion>()
                         where habitacion.Activo
                         && (habitacion.IdEstadoHabitacion == idEstadoH || habitacion.IdEstadoHabitacion == idEstadoP)
                         select new
                         {
                             habitacion,
                             tipo = habitacion.TipoHabitacion
                         }).ToList();

            return query.Select(m => m.habitacion).ToList();
        }


        public DtoResumenHabitacion SP_ObtenerResumenHabitacion(int idHabitacion, int? cambiosup)
        {
            var parametro = new SqlParameter("@idHabitacion", idHabitacion);
            parametro.SqlDbType = System.Data.SqlDbType.Int;
            var parametro1 = new SqlParameter("@EsCambiodeSup", 0);
            if (cambiosup.HasValue)
                parametro1 = new SqlParameter("@EsCambiodeSup", cambiosup);
            parametro1.SqlDbType = System.Data.SqlDbType.Int;
            return contexto.ConsultaSql<Modelo.Dtos.DtoResumenHabitacion>("exec SotSchema.SP_ObtenerResumenHabitacion @idHabitacion,@EsCambiodeSup", parametro, parametro1).FirstOrDefault();
        }

        public List<DtoResumenHabitacion> SP_ObtenerResumenesHabitaciones()
        {
            return contexto.ConsultaSql<DtoResumenHabitacion>("exec SotSchema.SP_ObtenerResumenesHabitaciones").ToList();
        }

        public List<DtoEstadisticasHabitacion> ObtenerEstadisticasHabitaciones() 
        {
            var resultado = (from habitacion in contexto.Set<Habitacion>()
                             where habitacion.Activo
                             group habitacion by habitacion.IdEstadoHabitacion into gh
                             select new
                             {
                                IdEstado = gh.Key,
                                Cantidad = gh.Count()
                             }).ToList();

            if(resultado.Count == 0)
                return new List<DtoEstadisticasHabitacion>();

            //var cantidad = resultado.Sum(m=> m.Cantidad);

            return resultado.Select(m => new DtoEstadisticasHabitacion { Cantidad = m.Cantidad/* / cantidad*/, IdEstadoHabitacion = m.IdEstado }).ToList();
        }
    }
}
