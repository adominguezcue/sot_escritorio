﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Datos.Nucleo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioOrdenesTaxi : Repositorio<OrdenTaxi>, IRepositorioOrdenesTaxi
    {
        public RepositorioOrdenesTaxi() : base(new Contextos.SOTContext())
        {
        }

        public List<OrdenTaxi> ObtenerOrdenesPendientes()
        {
            int idEstadoPendiente = (int)OrdenTaxi.Estados.Pendiente;

            return contexto.Set<OrdenTaxi>().Where(m => m.Activo && m.IdEstado == idEstadoPendiente).ToList();
        }


        public int ObtenerCantidadOrdenesPendientes()
        {
            int idEstadoPendiente = (int)OrdenTaxi.Estados.Pendiente;

            return contexto.Set<OrdenTaxi>().Count(m => m.Activo && m.IdEstado == idEstadoPendiente);
        }


        public List<OrdenTaxi> ObtenerOrdenesCobradasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idEstadoCobrado = (int)OrdenTaxi.Estados.Cobrada;

            Expression<Func<OrdenTaxi, bool>> filtro = m => m.IdEstado == idEstadoCobrado;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaModificacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaModificacion <= fechaFin.Value, Expression.AndAlso);

            return contexto.Set<OrdenTaxi>().Where(filtro).ToList();
        }
    }
}
