﻿using Modelo;
using Modelo.Dtos;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using System.Data.SqlClient;
using Dominio.Nucleo.Entidades;

namespace Datos.Repositorios
{
    public class RepositorioComandas : Repositorio<Comanda>, IRepositorioComandas
    {
        public RepositorioComandas() : base(new Contextos.SOTContext())
        {
        }

        public Comanda ObtenerParaPagar(int idComanda)
        {
            //var idEstadoElaborada = (int)Comanda.Estados.Elaborada;

            return (from comanda in contexto.Set<Comanda>()
                    where comanda.Activo
                    && comanda.Id == idComanda
                    //&& comanda.IdEstado == idEstadoElaborada
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosComanda.Where(m => m.Activo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }


        public Comanda ObtenerPorArticulos(int idComanda, List<int> idsArticulosComandas)
        {
            //var idEstadoElaborada = (int)Comanda.Estados.Elaborada;

            return (from comanda in contexto.Set<Comanda>()
                    where comanda.Activo
                    && comanda.Id == idComanda
                    && comanda.ArticulosComanda.Count(m => m.Activo && idsArticulosComandas.Contains(m.Id)) == idsArticulosComandas.Count
                    //&& comanda.IdEstado == idEstadoElaborada
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosComanda.Where(m => m.Activo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }


        public List<Comanda> ObtenerComandasPendientesCargadas(int idRenta)
        {
            var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar, (int)Comanda.Estados.PorPagar };

            return (from comanda in contexto.Set<Comanda>()
                    where comanda.Activo
                    && comanda.IdRenta == idRenta
                    && idsEstadoElaborada.Contains(comanda.IdEstado)
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosComanda.Where(m => m.Activo),
                        //articulos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo),
                        //tipos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo.TipoArticulo)
                    }).ToList().Select(m => m.comanda).ToList();
        }


        public List<Comanda> ObtenerComandasCobradasCargadas(int idRenta)
        {
            var idEstadoCobrada = (int)Comanda.Estados.Cobrada;
            var idEstadoElaborado = (int)ArticuloComanda.Estados.Entregado;

            return (from comanda in contexto.Set<Comanda>()
                    where comanda.IdRenta == idRenta
                    && comanda.IdEstado == idEstadoCobrada
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosComanda.Where(m => m.IdEstado == idEstadoElaborado)
                    }).ToList().Select(m => m.comanda).ToList();
        }

        public decimal ObtenerPagosComandasPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PagoComanda, bool>> filtro = m => m.Activo;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

#if DEBUG
            var query = contexto.Set<PagoComanda>().Where(filtro).ToString();

#endif

            var valores = contexto.Set<PagoComanda>().Where(filtro).Select(m => m.Valor).ToList();

            if (valores.Count == 0)
                return 0;

            return valores.Sum();
        }


        public Dictionary<TiposPago, decimal> ObtenerPagosComandasPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PagoComanda, bool>> filtro = m => m.Activo;

            if (idsTiposPago.Count > 0)
                filtro = filtro.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from pagoRenta in contexto.Set<PagoComanda>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoPago into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposPago)m.Key, m => m.valor);
        }

        public List<Comanda> ObtenerDetallesComandasArticulosPreparadosOEnPreparacion(List<string> idsArticulos)
        {
            //var idArea = (int)area;
            var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;
            var idEstadoPorEntregar = (int)ArticuloComanda.Estados.PorEntregar;

            var resultado = (from comanda in contexto.Set<Comanda>()
                             where comanda.Activo
                             && comanda.ArticulosComanda.Any(m => m.Activo && (m.IdEstado == idEstadoPreparacion || m.IdEstado == idEstadoPorEntregar) && idsArticulos.Contains(m.IdArticulo))
                             select new
                             {
                                 comanda,
                                 numeroHabitacion = comanda.Renta.Habitacion.NumeroHabitacion,
                                 articulosComanda = comanda.ArticulosComanda.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)),
                                 //articulos = comanda.ArticulosComanda.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo),
                                 //tipos = comanda.ArticulosComanda.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo.TipoArticulo)
                             }).ToList();

            if (resultado.Count == 0)
                return new List<Comanda>();

            resultado.ForEach(m => m.comanda.NumeroHabitacionTmp = m.numeroHabitacion);

            return resultado.Select(m => m.comanda).ToList();
        }

        public List<DtoResumenComandaExtendido> ObtenerDetallesComandasArticulosPreparadosOEnPreparacion()
        {
            //var idArea = (int)area;
            var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;
            var idEstadoPorEntregar = (int)ArticuloComanda.Estados.PorEntregar;

            var resultado = (from comanda in contexto.Set<Comanda>()
                             where comanda.Activo
                             && comanda.ArticulosComanda.Any(m => m.Activo && (m.IdEstado == idEstadoPreparacion || m.IdEstado == idEstadoPorEntregar))
                             select new
                             {
                                 comanda.Id,
                                 comanda.IdEstado,
                                 comanda.FechaInicio,
                                 comanda.Orden,
                                 numeroHabitacion = comanda.Renta.Habitacion.NumeroHabitacion,
                                 articulosComanda = comanda.ArticulosComanda.Where(m => m.Activo)
                                 .Select(a => new
                                 {
                                     a.Id,
                                     a.Activo,
                                     a.Cantidad,
                                     a.EsCortesia,
                                     a.IdEstado,
                                     a.IdArticulo,
                                     a.Observaciones,
                                     a.PrecioUnidad
                                 }),
                             }).ToList();

            if (resultado.Count == 0)
                return new List<DtoResumenComandaExtendido>();

            return resultado.Select(m => new DtoResumenComandaExtendido
            {
                Id = m.Id,
                Estado = (Modelo.Entidades.Comanda.Estados)m.IdEstado,
                FechaInicio = m.FechaInicio,
                Orden = m.Orden,
                Destino = m.numeroHabitacion,
                ArticulosComanda = m.articulosComanda.Select(a => new DtoResumenArticuloComanda
                {
                    Id = a.Id,
                    Activo = a.Activo,
                    Cantidad = a.Cantidad,
                    EsCortesia = a.EsCortesia,
                    Estado = (Modelo.Entidades.ArticuloComanda.Estados)a.IdEstado,
                    IdArticulo = a.IdArticulo,
                    Observaciones = a.Observaciones,
                    PrecioUnidad = a.PrecioUnidad
                }).ToList()
            }).ToList();
        }

        public List<Comanda> ObtenerComandasCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            int idEstadoFinalizado = (int)Comanda.Estados.Cobrada;

            Expression<Func<Comanda, bool>> filtro = m => m.IdEstado == idEstadoFinalizado;
            Expression<Func<PagoComanda, bool>> filtroPago = m => m.Activo;


            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCobro >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCobro <= fechaFin.Value, Expression.AndAlso);
            if (formasPago.HasValue)
            {
                var idFormaPago = (int)formasPago;

                filtroPago = filtroPago.Compose(m => m.IdTipoPago == idFormaPago, Expression.AndAlso);
            }

            //var filtroPagoCompilado = filtroPago.Compile();

            filtro = filtro.Compose(m => m.PagosComanda.AsQueryable().Any(filtroPago), Expression.AndAlso);

            var resultado = (from comanda in contexto.Set<Comanda>().Where(filtro)
                             select new
                             {
                                 renta = comanda,
                                 mesero = comanda.EmpleadoCobro,
                                 //habitacion = renta.Habitacion,
                                 pagos = comanda.PagosComanda.AsQueryable().Where(filtroPago),
                                 //detalles = renta.DetallesPago.Where(m => m.Activo)
                             }).ToList().Select(m => m.renta).ToList();

            return resultado;
        }


        public Comanda ObtenerComandaPagadaConDetalles(int idComanda)
        {
            var idEstadoCobrada = (int)Comanda.Estados.Cobrada;
            var idEstadoArtEntregado = (int)ArticuloComanda.Estados.Entregado;

            return (from comanda in contexto.Set<Comanda>()
                    where comanda.IdEstado == idEstadoCobrada
                    && comanda.Id == idComanda
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosComanda.Where(m => m.IdEstado == idEstadoArtEntregado),
                        //articulo = comanda.ArticulosComanda.Where(m => m.IdEstado == idEstadoArtEntregado).Select(m=>m.Articulo),
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }

        public List<DtoResumenComanda> ObtenerResumenesComandasPorRenta(int idRenta)
        {
            var idEstadoCobrada = (int)Comanda.Estados.Cobrada;
            var idEstadoArtEntregado = (int)ArticuloComanda.Estados.Entregado;

            //List<int> formasPagoIngresos = new List<int>() { (int)TiposPago.Efectivo, (int)TiposPago.Reservacion, (int)TiposPago.TarjetaCredito };
            List<int> formasPagoDescuentos = new List<int>() { (int)TiposPago.Consumo, (int)TiposPago.VPoints };
            List<int> formasPagoCortesia = new List<int>() { (int)TiposPago.Cortesia, (int)TiposPago.Cupon };

            return (from comanda in contexto.Set<Comanda>()
                    where (comanda.IdEstado == idEstadoCobrada || comanda.Activo)
                    && comanda.IdRenta == idRenta
                    select new
                    {
                        Id = comanda.Id,
                        idEstado = comanda.IdEstado,
                        fecha = comanda.FechaCreacion,
                        subTotal = comanda.ValorConIVA,
                        cortesias = comanda.PagosComanda.Where(m => m.Activo && formasPagoCortesia.Contains(m.IdTipoPago)).Select(m => m.Valor),
                        descuentos = comanda.PagosComanda.Where(m => m.Activo && formasPagoDescuentos.Contains(m.IdTipoPago)).Select(m => m.Valor),

                        articulosComanda = comanda.ArticulosComanda.Where(m => m.IdEstado == idEstadoArtEntregado || m.Activo).Select(m => new { m.IdArticulo, m.Cantidad, m.EsCortesia }),
                        //articulo = comanda.ArticulosComanda.Where(m => m.IdEstado == idEstadoArtEntregado).Select(m=>m.Articulo),
                    }).ToList().Select(c => new DtoResumenComanda 
                    { 
                        Id = c.Id,
                        Estado = (Comanda.Estados)c.idEstado,
                        Fecha = c.fecha,
                        SubTotal = c.subTotal,
                        Cortesias = c.cortesias.ToList().Sum(),
                        Descuentos = c.descuentos.ToList().Sum(),
                        Articulos = c.articulosComanda.Select(a => new DtoArticuloTicket
                                    {
                                        Cantidad = a.Cantidad,
                                        Nombre = a.IdArticulo,
                                        EsCortesia = a.EsCortesia
                                    }).ToList()
                    }).ToList();
        }

        public List<DtoEmpleadoLineaArticuloValor> ObtenerArticulosPorEmpleado(List<DtoIdArticuloIdLinea> articulos, List<int> idsCortes)
        {
            int idEstadoElaborado = (int)ArticuloComanda.Estados.Entregado;
            int idEstadoCobrado = (int)Comanda.Estados.Cobrada;

            var idsArticulo = articulos.Select(m => m.IdArticulo).Distinct().ToList();

            Expression<Func<ArticuloComanda, bool>> filtro = m => m.IdEstado == idEstadoElaborado
                                                                && m.Comanda.IdEstado == idEstadoCobrado
                                                                && idsCortes.Contains(m.Comanda.IdCorte)
                                                                && idsArticulo.Contains(m.IdArticulo);

            //if (fechaInicio.HasValue)
            //    filtro = filtro.Compose(m => m.Comanda.FechaModificacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.Comanda.FechaModificacion <= fechaFin.Value, Expression.AndAlso);



            var pre = (from articuloComanda in contexto.Set<ArticuloComanda>().Where(filtro)
                       //join item in articulos.Select(m=> new { m.IdArticulo, m.IdLinea }) on articuloComanda.IdArticulo equals item.IdArticulo
                       group articuloComanda by new { articuloComanda.Comanda.EmpleadoCobro, articuloComanda.IdArticulo } into grupo
                       select new
                       {
                           grupo.Key,
                           valor = grupo.Sum(m => m.PrecioUnidadFinal * m.Cantidad)
                       }).ToList();


            return (from articuloComanda in pre
                    join item in articulos on articuloComanda.Key.IdArticulo equals item.IdArticulo
                    group articuloComanda by new { articuloComanda.Key.EmpleadoCobro, item.IdLinea } into grupo
                    select new
                    {
                        grupo.Key,
                        valor = grupo.Sum(m => m.valor)
                    }).ToList().Select(m => new DtoEmpleadoLineaArticuloValor
                    {
                        IdEmpleado = m.Key.EmpleadoCobro.Id,
                        IdLineaArticulo = m.Key.IdLinea,
                        Nombre = m.Key.EmpleadoCobro.NombreCompleto,
                        ValorConIVA = m.valor
                    }).ToList();
        }

        public List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketComanda(int idComanda) 
        {
            SqlParameter sp_idComanda = new SqlParameter("@idComanda", idComanda);
            sp_idComanda.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<DtoArticuloTicket>("exec [SotSchema].[SP_ObtenerResumenesArticulosTicketComanda] @idComanda", sp_idComanda).ToList();
        }


        public string ObtenerNumeroHabitacionPorTransaccion(string numeroTransaccion)
        {
            return (from comanda in contexto.Set<Comanda>()
                    where comanda.Transaccion == numeroTransaccion
                    select comanda.Renta.Habitacion.NumeroHabitacion).FirstOrDefault();
        }

        public List<DtoHistorialesComanda> SP_ObtenerHistorialesComandas(DateTime fechaInicio, DateTime fechaFin, int? ordenTurno = null, int? idLinea = null, Comanda.Tipos? tipo = null, Comanda.Estados? estado = null)
        {
            var sp_fechaInicio = new SqlParameter("@fechaInicio", fechaInicio);
            sp_fechaInicio.DbType = System.Data.DbType.DateTime;

            var sp_fechaFin = new SqlParameter("@fechaFin", fechaFin);
            sp_fechaFin.DbType = System.Data.DbType.DateTime;

            var sp_ordenTurno = new SqlParameter("@ordenTurno", (object)ordenTurno ?? DBNull.Value);
            sp_ordenTurno.DbType = System.Data.DbType.Int32;
            sp_ordenTurno.IsNullable = true;

            var idTipo = (int?)tipo;

            var sp_idTipo = new SqlParameter("@idTipo", (object)idTipo ?? (object)DBNull.Value);
            sp_idTipo.DbType = System.Data.DbType.Int32;
            sp_idTipo.IsNullable = true;

            var idEstado = (int?)estado;

            var sp_idEstado = new SqlParameter("@idEstado", (object)idEstado ?? (object)DBNull.Value);
            sp_idEstado.DbType = System.Data.DbType.Int32;
            sp_idEstado.IsNullable = true;

            var sp_idLinea = new SqlParameter("@idLinea", (object)idLinea ?? (object)DBNull.Value);
            sp_idLinea.DbType = System.Data.DbType.Int32;
            sp_idLinea.IsNullable = true;

            return contexto.ConsultaSql<DtoHistorialesComanda>("exec SotSchema.SP_ObtenerHistorialesComandas @fechaInicio, @fechaFin, @ordenTurno, @idTipo, @idEstado, @idLinea",
                                                                sp_fechaInicio, sp_fechaFin, sp_ordenTurno, sp_idTipo, sp_idEstado, sp_idLinea);
        }

        public Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosComandasTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoT = (int)TiposPago.TarjetaCredito;

            Expression<Func<PagoComanda, bool>> filtro = m => m.Activo && m.IdTipoPago == idTipoPagoT;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var res= (from pagoComanda in contexto.Set<PagoComanda>().Where(filtro)
                    group pagoComanda by pagoComanda.IdTipoTarjeta into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList();

            if (res.Count == 0)
                return new Dictionary<TiposTarjeta, decimal>();

            return res.ToDictionary(m => (TiposTarjeta)m.Key, m => m.valor);
        }



        public List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketComandaCancelada(int idComanda, bool? Escambio = false)
        {

            List<DtoArticuloTicket> respuesta = new List<DtoArticuloTicket>();
            if (Escambio == false)
            {
                SqlParameter sp_idComanda = new SqlParameter("@idComanda", idComanda);
                sp_idComanda.DbType = System.Data.DbType.Int32;
                respuesta = contexto.ConsultaSql<DtoArticuloTicket>("exec [SotSchema].[SP_ObtenerResumenesArticulosTicketComandaCancelada] @idComanda", sp_idComanda).ToList();
            }
            else
            {
                SqlParameter sp_idComanda = new SqlParameter("@idComanda", idComanda);
                sp_idComanda.DbType = System.Data.DbType.Int32;
                respuesta = contexto.ConsultaSql<DtoArticuloTicket>("exec [SotSchema].[SP_ObtenerResumenesArticulosTicketComandaCambiada] @idComanda", sp_idComanda).ToList();
            }


            return respuesta;
        }


        public int ObtenerCantidadArticulosComandasPorCodigoArticulo(string codigoArticulo)
        {
            var codigoU = codigoArticulo.Trim().ToUpper();

            return contexto.Set<ArticuloComanda>().Count(m => m.IdArticulo.Trim().ToUpper().Equals(codigoU));
        }
    }
}
