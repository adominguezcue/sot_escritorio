﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Data;
using System.Data.SqlClient;
using Datos.Nucleo.Contextos;							  
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioCortesTurno : Repositorio<CorteTurno>, IRepositorioCortesTurno
    {
        public RepositorioCortesTurno() : base(new Contextos.SOTContext())
        {
        }

        //public int ObtenerUltimoNumeroCorte() 
        //{
        //    return (from corteTurno in contexto.Set<CorteTurno>()
        //            orderby corteTurno.NumeroCorte descending
        //            select corteTurno.NumeroCorte).FirstOrDefault();
        //}

        //public DateTime? ObtenerFechaUltimoCorte()
        //{
        //    return (from corteTurno in contexto.Set<CorteTurno>()
        //            orderby corteTurno.NumeroCorte descending
        //            select (DateTime?)corteTurno.FechaCorte).FirstOrDefault();
        //}


        public CorteTurno ObtenerUltimoCorte()
        {
            var idEstadoAbierto = (int)CorteTurno.Estados.Abierto;

            return (from corteTurno in contexto.Set<CorteTurno>()
                    where corteTurno.IdEstado == idEstadoAbierto
                    orderby corteTurno.NumeroCorte descending
                    select corteTurno).FirstOrDefault();
        }

        public CorteTurno ObtenerCorteEnRevision(int idCorteTurno)
        {
            var idEstadoRevision = (int)CorteTurno.Estados.EnRevision;

            return (from corteTurno in contexto.Set<CorteTurno>()
                    where corteTurno.IdEstado == idEstadoRevision
                    && corteTurno.Id == idCorteTurno
                    orderby corteTurno.NumeroCorte descending
                    select corteTurno).FirstOrDefault();
        }


        //public List<DtoCorteTurno> ObtenerResumenesCortesFinalizadosPorRango(DateTime? fechaInicial, DateTime? fechaFinal)
        //{
        //    int idEstadoCerrado = (int)CorteTurno.Estados.Cerrado;

        //    Expression<Func<CorteTurno, bool>> filtro = m => m.IdEstado == idEstadoCerrado;

        //    if (fechaInicial.HasValue)
        //        filtro = filtro.Compose(m => m.FechaCorte >= fechaInicial, Expression.AndAlso);
        //    if (fechaFinal.HasValue)
        //            filtro = filtro.Compose(m => m.FechaCorte <= fechaFinal, Expression.AndAlso);

        //    return (from corteTurno in contexto.Set<CorteTurno>().Where(filtro)
        //            select new
        //            {
        //                corteTurno.NumeroCorte,
        //                corteTurno.FechaCorte,
        //                corteTurno.TotalHabitacion,
        //                corteTurno.TotalRoomService,
        //                corteTurno.TotalRestaurante,
        //                corteTurno.CortesiasHabitacion,
        //                corteTurno.CortesiasRoomService,
        //                corteTurno.CortesiasRestautante,
        //                corteTurno.Gastos,
        //                corteTurno.Total,
        //                corteTurno.IdUsuarioCerro
        //            }).ToList().Select(m => new DtoCorteTurno 
        //            {
        //                IdUsuarioCerro = m.IdUsuarioCerro,
        //                Corte = m.NumeroCorte,
        //                Fecha = m.FechaCorte,
        //                Habitaciones = m.TotalHabitacion,
        //                RoomService = m.TotalRoomService,
        //                Restaurante = m.TotalRestaurante,
        //                Cortesias = m.CortesiasHabitacion + m.CortesiasRoomService + m.CortesiasRestautante,
        //                Gastos = m.Gastos,
        //                Total = m.Total
        //            }).ToList();
        //}

        public List<DtoCorteTurno> ObtenerResumenesCortesPorRango(DateTime? fechaInicial, DateTime? fechaFinal)
        {
            //int idEstadoCerrado = (int)CorteTurno.Estados.Cerrado;

            Expression<Func<CorteTurno, bool>> filtro = m => true;//m.IdEstado == idEstadoCerrado;

            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m => m.FechaInicio >= fechaInicial, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaInicio <= fechaFinal, Expression.AndAlso);

            return (from corteTurno in contexto.Set<CorteTurno>().Where(filtro)
                    select new
                    {
                        corteTurno.IdEstado,
                        corteTurno.NumeroCorte,
                        corteTurno.FechaInicio,
                        corteTurno.FechaCorte,
                        corteTurno.TotalHabitacion,
                        corteTurno.TotalRoomService,
                        corteTurno.TotalRestaurante,
                        corteTurno.CortesiasHabitacion,
                        corteTurno.CortesiasRoomService,
                        corteTurno.CortesiasRestautante,
                        corteTurno.Gastos,
                        corteTurno.Total,
                        corteTurno.IdUsuarioCerro
                    }).ToList().Select(m => new DtoCorteTurno
                    {
                        EstadoCorte = (CorteTurno.Estados)m.IdEstado,
                        IdUsuarioCerro = m.IdUsuarioCerro,
                        Corte = m.NumeroCorte,
                        FechaInicio = m.FechaInicio,
                        FechaCorte = m.FechaCorte,
                        Habitaciones = m.TotalHabitacion,
                        RoomService = m.TotalRoomService,
                        Restaurante = m.TotalRestaurante,
                        Cortesias = m.CortesiasHabitacion + m.CortesiasRoomService + m.CortesiasRestautante,
                        Gastos = m.Gastos,
                        Total = m.Total
                    }).ToList();
        }

        //public CorteTurno ObtenerUltimoCorteCerrado()
        //{
        //    return (from corteTurno in contexto.Set<CorteTurno>()
        //            where corteTurno.EsCerrado
        //            orderby corteTurno.NumeroCorte descending
        //            select corteTurno).FirstOrDefault();
        //}

        public List<DtoAreas> ObtieneAreas()
        {
            List<DtoAreas> respuesta = new List<DtoAreas>();
            DataSet dataSet = new DataSet();
            dataSet = EjecutarComandoSqlSP1("SotSchema.SP_ConCatalogoArea");
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    DtoAreas area = new DtoAreas();
                    area.Id= Convert.ToInt32(fila["Id"]);
                    area.NombreArea= fila["Nombre"].ToString();
                    respuesta.Add(area);
                }
            }
            return respuesta;
        }												 

        public List<DtoResumenCorteTurno> ObtenerResumenesCortesEnRevision()
        {
            var idEstadoRevision = (int)CorteTurno.Estados.EnRevision;

            return (from corteTurno in contexto.Set<CorteTurno>()
                    where corteTurno.IdEstado == idEstadoRevision
                    orderby corteTurno.NumeroCorte
                    select new
                    {
                        corteTurno.Id,
                        corteTurno.NumeroCorte,
                        corteTurno.FechaInicio,
                        corteTurno.FechaCorte
                    }).ToList().Select(m => new DtoResumenCorteTurno
                    {
                        Id = m.Id,
                        NumeroCorte = m.NumeroCorte,
                        FechaInicio = m.FechaInicio,
                        FechaCorte = m.FechaCorte
                    }).ToList();
        }

        public string ObtieneSiglaTurno(int idconfigTurno)
        {
            string respuesta = string.Empty;

            var consulta = (from configuracionturno in contexto.Set<ConfiguracionTurno>()
                            where configuracionturno.Id == idconfigTurno
                            select new
                            {
                                configuracionturno.Nombre
                            }).FirstOrDefault();
            respuesta = consulta.Nombre.ToString();
            return respuesta;
        }
        public string UsuarioCerroReporte(int idUsuariocerro)
        {
            string respuesta = string.Empty;
            DataSet dataSet = new DataSet();
            var idusuariocerro = new SqlParameter("@vi_idusuariocerro", idUsuariocerro);
            idusuariocerro.SqlDbType = System.Data.SqlDbType.Int;
            dataSet = EjecutarComandoSqlSP1("SotSchema.SP_ConUsuarioCerroCorte", idusuariocerro);
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    respuesta = fila["Alias"].ToString();
                }
            }
         return respuesta;
        }

        private DataSet EjecutarComandoSqlSP1(string sql, params object[] parametros)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            SqlConnection connection1 = new SqlConnection();
            DataSet respuesta = new DataSet();
            try
            {

                connection1.ConnectionString = ConexionHelper.CadenaConexion.ToString();
                connection1.Open();
                SqlCommand cmd = new SqlCommand(sql, connection1);
                cmd.CommandType = CommandType.StoredProcedure;
                if (parametros != null && parametros.Length > 0)
                    foreach (var item in parametros)
                    {
                        cmd.Parameters.Add(new SqlParameter(((System.Data.SqlClient.SqlParameter)item).ParameterName, ((System.Data.SqlClient.SqlParameter)item).Value));
                    }
                da.SelectCommand = cmd;
                da.Fill(respuesta);
                connection1.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            return respuesta;
        }

        public void SP_MoverComandas(int idCorte) 
        {
            var sp_idCorte = new SqlParameter("@idCorteActual", idCorte);
            sp_idCorte.DbType = System.Data.DbType.Int32;

            contexto.EjecutarComandoSql("exec SotSchema.SP_MoverComandas @idCorteActual", sp_idCorte);
        }

        public void Sp_PateaComandaOrdeResConcumoInte(int idcoret1, int idcorte2)
        {
            var sp_idCorte = new SqlParameter("@idCorte1", idcoret1);
            sp_idCorte.DbType = System.Data.DbType.Int32;

            var sp_idCorte1 = new SqlParameter("@idCorte2", idcorte2);
            sp_idCorte1.DbType = System.Data.DbType.Int32;

            contexto.EjecutarComandoSql("exec SotSchema.SP_PateadorComandaOrdenesConsumo @IdCorte1, @idCorte2 ", sp_idCorte, sp_idCorte1);
        }
    
    }
}
