﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioArticulosConceptosGasto : Repositorio<ArticuloConceptoGasto>, IRepositorioArticulosConceptosGasto
    {
        public RepositorioArticulosConceptosGasto() : base(new Contextos.SOTContext()) { }

    }
}
