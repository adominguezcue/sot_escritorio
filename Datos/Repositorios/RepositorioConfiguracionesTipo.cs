﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioConfiguracionesTipo : Repositorio<ConfiguracionTipo>, IRepositorioConfiguracionesTipo
    {
        public RepositorioConfiguracionesTipo() : base(new Contextos.SOTContext())
        {
        }

        public ConfiguracionTipo ObtenerConfiguracionCargada(int idConfiguracionTipo)
        {
            return (from configuracionTipo in contexto.Set<ConfiguracionTipo>()
                    where configuracionTipo.Activa
                    && configuracionTipo.Id == idConfiguracionTipo
                    select new
                    {
                        configuracionTipo,
                        tiemposExtra = configuracionTipo.TiemposHospedaje.Where(m => m.Activo),
                        configuracionNegocio = configuracionTipo.ConfiguracionTarifa,
                        tipo = configuracionTipo.TipoHabitacion
                    }).ToList().Select(m => m.configuracionTipo).FirstOrDefault();
        }

        public ConfiguracionTipo ObtenerConfiguracionTipo(int idTipoHabitacion, ConfiguracionTarifa.Tarifas tarifa)
        {
            int idTarifa = (int)tarifa;

            return (from configuracionTipo in contexto.Set<ConfiguracionTipo>()
                    where configuracionTipo.Activa
                    && configuracionTipo.ConfiguracionTarifa.Activa
                    && configuracionTipo.TipoHabitacion.Activo
                    && configuracionTipo.IdTipoHabitacion == idTipoHabitacion
                    && configuracionTipo.ConfiguracionTarifa.IdTarifa == idTarifa
                    select new
                    {
                        configuracionTipo,
                        tiemposExtra = configuracionTipo.TiemposHospedaje.Where(m => m.Activo),
                        configuracionNegocio = configuracionTipo.ConfiguracionTarifa,
                        tipo = configuracionTipo.TipoHabitacion
                    }).ToList().Select(m => m.configuracionTipo).FirstOrDefault();
        }
    }
}
