﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Dtos;
using Datos.Nucleo.Repositorios;
using System.Data.SqlClient;
using System.Linq.Expressions;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioTareasMantenimiento : Repositorio<TareaMantenimiento>, IRepositorioTareasMantenimiento
    {
        public RepositorioTareasMantenimiento() : base(new Contextos.SOTContext())
        {
        }

        public List<TareaMantenimiento> ObtenerTareasPorMes(DateTime fecha, int idHabitacion = 0)
        {
            if (idHabitacion == 0)
            {
                var resultado = (from tarea in contexto.Set<TareaMantenimiento>()
                                 where tarea.Activa
                                 //&& tarea.FechaInicio.Day == fecha.Day
                                 && tarea.FechaInicio.Month == fecha.Month
                                 && tarea.FechaInicio.Year == fecha.Year
                                 select new
                                 {
                                     tarea,
                                     Concepto = tarea.ConceptoMantenimiento.Concepto
                                 }).ToList();

                resultado.ForEach(m => m.tarea.ConceptoStrTmp = m.Concepto);

                return resultado.Select(m => m.tarea).ToList();
            }
            else
            {
                var resultado = (from tarea in contexto.Set<TareaMantenimiento>()
                                 where tarea.Activa
                                 //&& tarea.FechaInicio.Day == fecha.Day
                                 && tarea.FechaInicio.Month == fecha.Month
                                 && tarea.FechaInicio.Year == fecha.Year
                                 && tarea.IdHabitacion == idHabitacion
                                 select new
                                 {
                                     tarea,
                                     Concepto = tarea.ConceptoMantenimiento.Concepto
                                 }).ToList();

                resultado.ForEach(m => m.tarea.ConceptoStrTmp = m.Concepto);

                return resultado.Select(m => m.tarea).ToList();
            }
        }

        public int SP_ObtenerCantidadTareasPendientes(int idHabitacion = 0)
        {
            SqlParameter sp_idHabitacion = new SqlParameter("@idHabitacion", idHabitacion);
            sp_idHabitacion.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<int>("exec [SotSchema].[SP_ObtenerCantidadTareasPendientes] @idHabitacion", sp_idHabitacion).FirstOrDefault();
        }

        public List<DtoTareaMantenimientoBase> SP_ObtenerTareasMes(int mes, int anio, int idHabitacion = 0)
        {
            SqlParameter sp_mes = new SqlParameter("@mes", mes);
            sp_mes.DbType = System.Data.DbType.Int32;

            SqlParameter sp_anio = new SqlParameter("@anio", anio);
            sp_anio.DbType = System.Data.DbType.Int32;

            SqlParameter sp_idHabitacion = new SqlParameter("@idHabitacion", idHabitacion);
            sp_idHabitacion.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<DtoTareaMantenimientoBase>("exec [SotSchema].[SP_ObtenerTareasMes] @mes, @anio, @idHabitacion", sp_mes, sp_anio, sp_idHabitacion).ToList();
        }

        public List<DtoResumenMantenimiento> ObtenerMantenimientosPendientes(int idHabitacion, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0)
        {
            //RepositorioMantenimientos.ObtenerElementos(m => m.IdHabitacion == idHabitacion, m => m.FechaCreacion, pagina, elementos).ToList();

            //Expression<Func<TareaMantenimiento, bool>> filtro = m => m.IdHabitacion == idHabitacion && m.Activa;
			
            List<DtoResumenMantenimiento> respuesta = new List<DtoResumenMantenimiento>();
            Expression<Func<TareaMantenimiento, bool>> filtro = null;
            if (idHabitacion != 0)
                filtro = filtro = m => m.IdHabitacion == idHabitacion;

            if (fechaInicio.HasValue)
            {
                if (filtro == null)
                    filtro = m => m.FechaSiguiente >= fechaInicio;
                else
                    filtro = filtro.Compose(m => m.FechaSiguiente  >= fechaInicio, Expression.AndAlso);

               // filtro.Compose(m => fechaInicio <= m.FechaSiguiente, Expression.AndAlso);
            }
            if (fechaFin.HasValue)
            {
                if (filtro == null)
                    filtro = m => m.FechaSiguiente <= fechaFin;
                else
                    filtro = filtro.Compose(m => m.FechaSiguiente <= fechaFin, Expression.AndAlso);

                //filtro.Compose(m => fechaFin >= m.FechaSiguiente, Expression.AndAlso);
            }																							   

            var resultado = (from mantenimiento in contexto.Set<TareaMantenimiento>().Where(filtro)
                             orderby mantenimiento.FechaCreacion
                             select new
                             {
                                 mantenimiento.Id,
                                 mantenimiento.FechaCreacion,
                                 mantenimiento.IdUsuarioCreo,
                                 mantenimiento.ConceptoMantenimiento.Concepto,
                                 mantenimiento.Descripcion
                             });

            if (pagina == 0 && elementos == 0)
                respuesta = resultado.ToList().Select(m => new DtoResumenMantenimiento
                {
                    IdTarea = m.Id,
                    Concepto = m.Concepto,
                    Descripcion = m.Descripcion,
                    Fecha = m.FechaCreacion,
                    IdUsuario = m.IdUsuarioCreo,
                    //EstadoStr = "Pendiente"
                }).ToList();

            respuesta = resultado.Skip(pagina * elementos).Take(elementos).ToList().Select(m => new DtoResumenMantenimiento
            {
                IdTarea = m.Id,
                Concepto = m.Concepto,
                Descripcion = m.Descripcion,
                Fecha = m.FechaCreacion,
                IdUsuario = m.IdUsuarioCreo,
                //EstadoStr = "Pendiente"
            }).ToList();


            respuesta = ObtieneNumeroDehabitacion(respuesta);

            return respuesta;
        }


        private List<DtoResumenMantenimiento> ObtieneNumeroDehabitacion(List<DtoResumenMantenimiento> respuesta1)
        {
            List<DtoResumenMantenimiento> respuesta2 = new List<DtoResumenMantenimiento>();
            foreach (DtoResumenMantenimiento datos in respuesta1)
            {

                var resultado = (from habitacion in contexto.Set<Habitacion>()
                                 where habitacion.Id == datos.IdHabitacion
                                 select new
                                 {
                                     habitacion.NumeroHabitacion
                                 }).FirstOrDefault();
                string habitacion1 = resultado.NumeroHabitacion;
                datos.NumeroHabitacio = habitacion1;
                respuesta2.Add(datos);

            }

            return respuesta2;						  
        }
    }
}
