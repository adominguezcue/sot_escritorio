﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Dtos;
using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using Transversal.Extensiones;
using System.Linq.Expressions;

namespace Datos.Repositorios
{
    public class RepositorioVW_ConceptosCancelacionesYCortesias :  Repositorio<VW_ConceptoCancelacionesYCortesias>, IRepositorioVW_ConceptosCancelacionesYCortesias
    {
        public RepositorioVW_ConceptosCancelacionesYCortesias(): base(new Contextos.SOTContext())
        { }

    }
}
