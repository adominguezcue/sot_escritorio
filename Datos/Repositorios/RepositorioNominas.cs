﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioNominas : Repositorio<Nomina>, IRepositorioNominas
    {
        public RepositorioNominas() : base(new Contextos.SOTContext()) { }

        public List<Nomina> ObtenerNominasFiltradas(string nombre, string apellidoPaterno, string apellidoMaterno, DateTime? fechaInicial, DateTime? fechaFinal)
        {
            Expression<Func<Nomina, bool>> filtro = m => m.Activa;

            if (!string.IsNullOrWhiteSpace(nombre))
            {
                var nombreU = nombre.Trim().ToUpper();

                filtro = filtro.Compose(m => m.Empleado.Nombre.ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (!string.IsNullOrWhiteSpace(apellidoPaterno))
            {
                var apellidoPU = apellidoPaterno.Trim().ToUpper();

                filtro = filtro.Compose(m => m.Empleado.ApellidoPaterno.ToUpper().Contains(apellidoPU), Expression.AndAlso);
            }
            if (!string.IsNullOrWhiteSpace(apellidoMaterno))
            {
                var qapellidoMU = apellidoMaterno.Trim().ToUpper();

                filtro = filtro.Compose(m => m.Empleado.ApellidoMaterno.ToUpper().Contains(qapellidoMU), Expression.AndAlso);
            }
            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m => m.FechaInicio >= fechaInicial.Value, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaFin <= fechaFinal.Value, Expression.AndAlso);

            var resultado = (from nomina in contexto.Set<Nomina>().Where(filtro)
                             select new
                             {
                                 nomina,
                                 NombreE = nomina.Empleado.Nombre,
                                 ApellidoP = nomina.Empleado.ApellidoPaterno,
                                 ApellidoM = nomina.Empleado.ApellidoMaterno,
                                 Area = nomina.Empleado.Puesto.AreaActual.Nombre,
                             }).ToList();

            foreach (var item in resultado) 
            {
                item.nomina.NombreEmpleadoTmp = string.Join(" ", item.NombreE, item.ApellidoP, item.ApellidoM);
                item.nomina.AreaTmp = item.Area;
            }

            return resultado.Select(m => m.nomina).ToList();
        }
    }
}