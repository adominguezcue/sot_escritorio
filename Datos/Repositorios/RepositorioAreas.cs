﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioAreas : Repositorio<Area>, IRepositorioAreas
    {
        public RepositorioAreas() : base(new Contextos.SOTContext()) { }

        public List<Area> ObtenerAreasActivasConPuestos()
        {
            return (from area in contexto.Set<Area>()
                    where area.Activa
                    select new
                    {
                        area,
                        puestos = area.Puestos.Where(m => m.Activo)
                    }).ToList().Select(m => m.area).ToList();
        }
    }
}
