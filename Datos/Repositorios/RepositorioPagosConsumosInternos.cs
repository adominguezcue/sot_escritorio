﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPagosConsumosInternos : Repositorio<PagoConsumoInterno>, IRepositorioPagosConsumosInternos
    {
        public RepositorioPagosConsumosInternos() : base(new Contextos.SOTContext()) { }

        public List<PagoConsumoInterno> ObtenerPagosPorTransaccion(string transaccion)
        {
            return (from pago in contexto.Set<PagoConsumoInterno>()
                    where pago.Activo && pago.Transaccion == transaccion
                    select pago).ToList();
        }
    }
}
