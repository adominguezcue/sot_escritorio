﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using System.Data.SqlClient;
using System.Data;			  
using Dominio.Nucleo.Entidades;

namespace Datos.Repositorios
{
    public class RepositorioReservaciones : Repositorio<Reservacion>, IRepositorioReservaciones
    {
        public RepositorioReservaciones()
            : base(new Contextos.SOTContext())
        {
        }

        public Reservacion ObtenerReservacionParaEditar(int idReservacion)
        {
            return (from reservacion in contexto.Set<Reservacion>()
                    where reservacion.Activo
                    && reservacion.Id == idReservacion
                    select new
                    {
                        reservacion,
                        paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo),
                        pagos = reservacion.PagosReservacion.Where(m => m.Activo),
                        montosNR = reservacion.MontosNoReembolsables.Where(m => m.Activo)
                        //datosF = reservacion.DatosFiscales//.Where(m => m.Activo)
                    }).ToList().Select(m => m.reservacion).FirstOrDefault();
        }


        //public Reservacion ObtenerReservacionParaCancelar(int idReservacion)
        //{
        //    return (from reservacion in contexto.Set<Reservacion>()
        //            where reservacion.Activo
        //            && reservacion.Id == idReservacion
        //            select new
        //            {
        //                reservacion,
        //                paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo),
        //                pagos = reservacion.PagosReservacion.Where(m => m.Activo),
        //                //datosF = reservacion.DatosFiscales//.Where(m => m.Activo)
        //            }).ToList().Select(m => m.reservacion).FirstOrDefault();
        //}
        /*
        public DtoValor ObtenerValorReservacionesPendientesPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var idsEstadosPendientes = new List<int>() { (int)Reservacion.EstadosReservacion.Pendiente, (int)Reservacion.EstadosReservacion.Confirmada };

            Expression<Func<Reservacion, bool>> filtro = m => m.Activo && idsEstadosPendientes.Contains(m.IdEstado);

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var valores = contexto.Set<Reservacion>().Where(filtro).Select(m => new { m.ValorSinIVA, m.ValorIVA, m.ValorConIVA }).ToList();

            if (valores.Count == 0)
                return new DtoValor();

            return new DtoValor
            {
                ValorSinIVA = valores.Sum(m => m.ValorSinIVA),
                ValorIVA = valores.Sum(m => m.ValorIVA),
                ValorConIVA = valores.Sum(m => m.ValorConIVA)
            };
        }*/

        public List<DtoReservacion> ObtenerReservacionesPorFechaCreacion(DateTime fechaInicio , DateTime fechafin, string Estado, int idTipoHabitacion = 0)
        {
            List<DtoReservacion> respuesta = new List<DtoReservacion>();
            Expression<Func<Reservacion, bool>> filtro = m => m.FechaCreacion >= fechaInicio && m.FechaCreacion <= fechafin;

            if (idTipoHabitacion > 0)
                filtro = filtro.Compose(m => m.ConfiguracionTipo.IdTipoHabitacion == idTipoHabitacion, Expression.AndAlso);

            int idEstadoCancelado = (int)Reservacion.EstadosReservacion.Cancelada;
            var filtro2 = filtro.Compose(m => m.IdEstado != idEstadoCancelado, Expression.AndAlso);
            var filtro3 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
            switch (Estado)
            {
                case "Todos":
                    filtro2 = filtro.Compose(m => m.IdEstado != idEstadoCancelado, Expression.AndAlso);
                    filtro3 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Confirmada":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Confirmada;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Consumida":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Consumida;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Pendiente":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Pendiente;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Cancelada":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Cancelada;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                default:
                    break;

            }

            var lista = (from reservacion in contexto.Set<Reservacion>().Where(filtro2)
                         select new
                         {
                             Id = reservacion.Id,
                             LeyendaDescuento = reservacion.LeyendaDescuento,
                             CodigoReserva = reservacion.CodigoReserva,
                             Nombre = reservacion.Nombre,
                             FechaEntrada = reservacion.FechaEntrada,
                             FechaSalida = reservacion.FechaSalida,
                             Noches = reservacion.Noches,
                             PersonasExtra = reservacion.PersonasExtra,
                             FechaCreacion = reservacion.FechaCreacion,
                             Observaciones = reservacion.Observaciones,
                             IdEstado = reservacion.IdEstado,
                             ValorSinIVA = reservacion.ValorSinIVA,
                             ValorIVA = reservacion.ValorIVA,
                             ValorConIVA = reservacion.ValorConIVA,
                             Paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Select(m => m.Paquete.Nombre),
                             CantidadPaquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Select(m => m.Cantidad),
                             CantidadDescuentos = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Descuento > 0).Select(m => m.Cantidad),
                             NombreTipo = reservacion.ConfiguracionTipo.TipoHabitacion.Descripcion
                         }).ToList().Select(m => new DtoReservacion
                         {
                             Id = m.Id,
                             LeyendaDescuento = m.LeyendaDescuento,
                             CodigoReserva = m.CodigoReserva,
                             Nombre = m.Nombre,
                             FechaEntrada = m.FechaEntrada,
                             FechaSalida = m.FechaSalida,
                             Noches = m.Noches,
                             Estado = (Reservacion.EstadosReservacion)m.IdEstado,
                             PersonasExtra = m.PersonasExtra,
                             FechaCreacion = m.FechaCreacion,
                             Observaciones = m.Observaciones,
                             NombreTipo = m.NombreTipo,
                             NombreEstado = ((Reservacion.EstadosReservacion)m.IdEstado).Descripcion(),
                             ValorSinIVA = m.ValorSinIVA,
                             ValorIVA = m.ValorIVA,
                             ValorConIVA = m.ValorConIVA,
                             CantidadPaquetes = m.CantidadPaquetes.Sum(),
                             Paquetes = m.Paquetes.ToList(),
                             TurnoDia = ObtieneTurnoDia(m.FechaCreacion),
                             FormaDePago = ObtieneFormaPago(m.Id),
                             CantidadDescuentos = m.CantidadDescuentos.Sum()
                         }).ToList();

            if (Estado.Equals("Todos"))
            {
                lista.AddRange((from reservacion in contexto.Set<Reservacion>().Where(filtro3)
                                select new
                                {
                                    Id = reservacion.Id,
                                    LeyendaDescuento = reservacion.LeyendaDescuento,
                                    CodigoReserva = reservacion.CodigoReserva,
                                    Nombre = reservacion.Nombre,
                                    FechaEntrada = reservacion.FechaEntrada,
                                    FechaSalida = reservacion.FechaSalida,
                                    Noches = reservacion.Noches,
                                    PersonasExtra = reservacion.PersonasExtra,
                                    FechaCreacion = reservacion.FechaCreacion,
                                    Observaciones = reservacion.Observaciones,
                                    IdEstado = reservacion.IdEstado,
                                    ValorSinIVA = reservacion.ValorSinIVA,
                                    ValorIVA = reservacion.ValorIVA,
                                    ValorConIVA = reservacion.ValorConIVA,

                                    Paquetes = reservacion.PaquetesReservaciones.Where(m => m.FechaEliminacion == reservacion.FechaEliminacion && m.Precio > 0).Select(m => m.Paquete.Nombre),
                                    CantidadPaquetes = reservacion.PaquetesReservaciones.Where(m => m.FechaEliminacion == reservacion.FechaEliminacion && m.Precio > 0).Select(m => m.Cantidad),
                                    CantidadDescuentos = reservacion.PaquetesReservaciones.Where(m => m.FechaEliminacion == reservacion.FechaEliminacion && m.Descuento > 0).Select(m => m.Cantidad),
                                    NombreTipo = reservacion.ConfiguracionTipo.TipoHabitacion.Descripcion
                                }).ToList().Select(m => new DtoReservacion
                                {
                                    Id = m.Id,
                                    LeyendaDescuento = m.LeyendaDescuento,
                                    CodigoReserva = m.CodigoReserva,
                                    Nombre = m.Nombre,
                                    FechaEntrada = m.FechaEntrada,
                                    FechaSalida = m.FechaSalida,
                                    Noches = m.Noches,
                                    Estado = (Reservacion.EstadosReservacion)m.IdEstado,
                                    PersonasExtra = m.PersonasExtra,
                                    FechaCreacion = m.FechaCreacion,
                                    Observaciones = m.Observaciones,
                                    NombreTipo = m.NombreTipo,
                                    NombreEstado = ((Reservacion.EstadosReservacion)m.IdEstado).Descripcion(),
                                    ValorSinIVA = m.ValorSinIVA,
                                    ValorIVA = m.ValorIVA,
                                    ValorConIVA = m.ValorConIVA,
                                    CantidadPaquetes = m.CantidadPaquetes.Sum(),
                                    Paquetes = m.Paquetes.ToList(),
                                    FormaDePago = ObtieneFormaPago(m.Id),
                                    TurnoDia = ObtieneTurnoDia(m.FechaCreacion),
                                    CantidadDescuentos = m.CantidadDescuentos.Sum()
                                }).ToList());
            }
            respuesta = lista.OrderBy(m => m.FechaEntrada).ToList() ;
            return respuesta;
        }
        public List<DtoReservacion> ObtenerReservaciones(string reservacionstr)
        {
            List<DtoReservacion> respuesta = new List<DtoReservacion>();



            var lista = (from reservacion in contexto.Set<Reservacion>()
                         where reservacion.CodigoReserva.Contains(reservacionstr)
                         select new
                         {
                             Id = reservacion.Id,
                             LeyendaDescuento = reservacion.LeyendaDescuento,
                             CodigoReserva = reservacion.CodigoReserva,
                             Nombre = reservacion.Nombre,
                             FechaEntrada = reservacion.FechaEntrada,
                             FechaSalida = reservacion.FechaSalida,
                             Noches = reservacion.Noches,
                             PersonasExtra = reservacion.PersonasExtra,
                             FechaCreacion = reservacion.FechaCreacion,
                             Observaciones = reservacion.Observaciones,
                             IdEstado = reservacion.IdEstado,
                             ValorSinIVA = reservacion.ValorSinIVA,
                             ValorIVA = reservacion.ValorIVA,
                             ValorConIVA = reservacion.ValorConIVA,
                             Paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Select(m => m.Paquete.Nombre),
                             CantidadPaquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Select(m => m.Cantidad),
                             CantidadDescuentos = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Descuento > 0).Select(m => m.Cantidad),
                             NombreTipo = reservacion.ConfiguracionTipo.TipoHabitacion.Descripcion
                         }).ToList().Select(m => new DtoReservacion
                         {
                             Id = m.Id,
                             LeyendaDescuento = m.LeyendaDescuento,
                             CodigoReserva = m.CodigoReserva,
                             Nombre = m.Nombre,
                             FechaEntrada = m.FechaEntrada,
                             FechaSalida = m.FechaSalida,
                             Noches = m.Noches,
                             Estado = (Reservacion.EstadosReservacion)m.IdEstado,
                             PersonasExtra = m.PersonasExtra,
                             FechaCreacion = m.FechaCreacion,
                             Observaciones = m.Observaciones,
                             NombreTipo = m.NombreTipo,
                             NombreEstado = ((Reservacion.EstadosReservacion)m.IdEstado).Descripcion(),
                             ValorSinIVA = m.ValorSinIVA,
                             ValorIVA = m.ValorIVA,
                             ValorConIVA = m.ValorConIVA,
                             CantidadPaquetes = m.CantidadPaquetes.Sum(),
                             Paquetes = m.Paquetes.ToList(),
                             TurnoDia = ObtieneTurnoDia(m.FechaCreacion),
                             FormaDePago = ObtieneFormaPago(m.Id),
                             CantidadDescuentos = m.CantidadDescuentos.Sum()
                         }).ToList();
            respuesta = lista;
            return respuesta;
        }																																						   

        public List<DtoReservacion> ObtenerReservacionesFiltradas(DateTime fechaInicio, DateTime fechaFin,string Estado, int idTipoHabitacion = 0)
        {
            Expression<Func<Reservacion, bool>> filtro = m => m.FechaEntrada >= fechaInicio && m.FechaEntrada <= fechaFin;

            if (idTipoHabitacion > 0)
                filtro = filtro.Compose(m => m.ConfiguracionTipo.IdTipoHabitacion == idTipoHabitacion, Expression.AndAlso);

            /*
             
                     }"></DataGridTextColumn>
                     }"></DataGridTextColumn>
                     , StringFormat=dd/MM/yyyy hh:mm tt}"></DataGridTextColumn>
                     , StringFormat=dd/MM/yyyy hh:mm tt}"></DataGridTextColumn>
                     }"></DataGridTextColumn>
                     }"></DataGridTextColumn>
                     }"></DataGridTextColumn>
                     }"></DataGridTextColumn>
                     CantidadPaquetes}"></DataGridTextColumn>
                     CantidadDescuentos}"></DataGridTextColumn>
                     , StringFormat=dd/MM/yyyy hh:mm tt}"></DataGridTextColumn>
                     }" MaxWidth="200"></DataGridTextColumn>
             
             */

            int idEstadoCancelado = (int)Reservacion.EstadosReservacion.Cancelada;

            var filtro2 = filtro.Compose(m => m.IdEstado != idEstadoCancelado, Expression.AndAlso);
            var filtro3 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);

            switch (Estado)
            {
                case "Todos":
                      filtro2 = filtro.Compose(m => m.IdEstado != idEstadoCancelado, Expression.AndAlso);
                      filtro3 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Confirmada":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Confirmada;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Consumida":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Consumida;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Pendiente":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Pendiente;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                case "Cancelada":
                    idEstadoCancelado = (int)Reservacion.EstadosReservacion.Cancelada;
                    filtro2 = filtro.Compose(m => m.IdEstado == idEstadoCancelado, Expression.AndAlso);
                    break;
                default:
                    break;

            }						   
            var lista = (from reservacion in contexto.Set<Reservacion>().Where(filtro2)
                         select new
                         {
                             Id = reservacion.Id,
                             LeyendaDescuento = reservacion.LeyendaDescuento,
                             CodigoReserva = reservacion.CodigoReserva,
                             Nombre = reservacion.Nombre,
                             FechaEntrada = reservacion.FechaEntrada,
                             FechaSalida = reservacion.FechaSalida,
                             Noches = reservacion.Noches,
                             PersonasExtra = reservacion.PersonasExtra,
                             FechaCreacion = reservacion.FechaCreacion,
                             Observaciones = reservacion.Observaciones,
                             IdEstado = reservacion.IdEstado,
                             ValorSinIVA = reservacion.ValorSinIVA,
                             ValorIVA = reservacion.ValorIVA,
                             ValorConIVA = reservacion.ValorConIVA,
                             Paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Select(m=>m.Paquete.Nombre),
                             CantidadPaquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Select(m => m.Cantidad),
                             CantidadDescuentos = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Descuento > 0).Select(m=>m.Cantidad),
                             NombreTipo = reservacion.ConfiguracionTipo.TipoHabitacion.Descripcion
                         }).ToList().Select(m => new DtoReservacion
                             {
                                 Id = m.Id,
                                 LeyendaDescuento = m.LeyendaDescuento,
                                 CodigoReserva = m.CodigoReserva,
                                 Nombre = m.Nombre,
                                 FechaEntrada = m.FechaEntrada,
                                 FechaSalida = m.FechaSalida,
                                 Noches = m.Noches,
                                 Estado = (Reservacion.EstadosReservacion)m.IdEstado,
                                 PersonasExtra = m.PersonasExtra,
                                 FechaCreacion = m.FechaCreacion,
                                 Observaciones = m.Observaciones,
                                 NombreTipo = m.NombreTipo,
                                 NombreEstado = ((Reservacion.EstadosReservacion)m.IdEstado).Descripcion(),
                                 ValorSinIVA = m.ValorSinIVA,
                                 ValorIVA = m.ValorIVA,
                                 ValorConIVA = m.ValorConIVA,
                                 CantidadPaquetes = m.CantidadPaquetes.Sum(),
                                 Paquetes = m.Paquetes.ToList(),
                                 TurnoDia = ObtieneTurnoDia(m.FechaCreacion),
                                 FormaDePago = ObtieneFormaPago(m.Id),																																			  
                                 CantidadDescuentos = m.CantidadDescuentos.Sum()
                             }).ToList();

            if (Estado.Equals("Todos"))
                {									   			 
            lista.AddRange((from reservacion in contexto.Set<Reservacion>().Where(filtro3)
                            select new
                            {
                                Id = reservacion.Id,
                                LeyendaDescuento = reservacion.LeyendaDescuento,
                                CodigoReserva = reservacion.CodigoReserva,
                                Nombre = reservacion.Nombre,
                                FechaEntrada = reservacion.FechaEntrada,
                                FechaSalida = reservacion.FechaSalida,
                                Noches = reservacion.Noches,
                                PersonasExtra = reservacion.PersonasExtra,
                                FechaCreacion = reservacion.FechaCreacion,
                                Observaciones = reservacion.Observaciones,
                                IdEstado = reservacion.IdEstado,
                                ValorSinIVA = reservacion.ValorSinIVA,
                                ValorIVA = reservacion.ValorIVA,
                                ValorConIVA = reservacion.ValorConIVA,
                                Paquetes = reservacion.PaquetesReservaciones.Where(m => m.FechaEliminacion == reservacion.FechaEliminacion && m.Precio > 0).Select(m => m.Paquete.Nombre),
                                CantidadPaquetes = reservacion.PaquetesReservaciones.Where(m => m.FechaEliminacion == reservacion.FechaEliminacion && m.Precio > 0).Select(m => m.Cantidad),
                                CantidadDescuentos = reservacion.PaquetesReservaciones.Where(m => m.FechaEliminacion == reservacion.FechaEliminacion && m.Descuento > 0).Select(m => m.Cantidad),
                                NombreTipo = reservacion.ConfiguracionTipo.TipoHabitacion.Descripcion
                                   }).ToList().Select(m => new DtoReservacion
                                {
                                    Id = m.Id,
                                    LeyendaDescuento = m.LeyendaDescuento,
                                    CodigoReserva = m.CodigoReserva,
                                    Nombre = m.Nombre,
                                    FechaEntrada = m.FechaEntrada,
                                    FechaSalida = m.FechaSalida,
                                    Noches = m.Noches,
                                    Estado = (Reservacion.EstadosReservacion)m.IdEstado,
                                    PersonasExtra = m.PersonasExtra,
                                    FechaCreacion = m.FechaCreacion,
                                    Observaciones = m.Observaciones,
                                    NombreTipo = m.NombreTipo,
                                    NombreEstado = ((Reservacion.EstadosReservacion)m.IdEstado).Descripcion(),
                                    ValorSinIVA = m.ValorSinIVA,
                                    ValorIVA = m.ValorIVA,
                                    ValorConIVA = m.ValorConIVA,
                                    CantidadPaquetes = m.CantidadPaquetes.Sum(),
                                    Paquetes = m.Paquetes.ToList(),
                                    FormaDePago = ObtieneFormaPago(m.Id),
                                    TurnoDia = ObtieneTurnoDia(m.FechaCreacion),
                                    CantidadDescuentos = m.CantidadDescuentos.Sum()
                                }).ToList());
            }
            return lista.OrderBy(m => m.FechaEntrada).ToList(); ;
        }
		
		
		


        private string ObtieneFormaPago(int idreserva)
        {
            string respuesta = "";
            var vireservacion = new SqlParameter("@viReservacaion", idreserva);
            vireservacion.SqlDbType = System.Data.SqlDbType.Int;

            respuesta = contexto.ConsultaSql<string>("exec SotSchema.SP_ConTipoPagoReservacion @viReservacaion", vireservacion).FirstOrDefault();
											
            return respuesta;
         }

         private string  ObtieneTurnoDia (DateTime fechacreacion)
         {
             string respuesta = "";
             var vdFechacreacion = new SqlParameter("@vdFechaCreacion", fechacreacion);
             vdFechacreacion.SqlDbType = System.Data.SqlDbType.DateTime;

             respuesta = contexto.ConsultaSql<string>("exec SotSchema.SP_ConTurnoPorFechaReservacion @vdFechaCreacion", vdFechacreacion).FirstOrDefault();
		 
																					

             return respuesta;
         }																 																

        public Reservacion ObtenerReservacionConfimada(int idHabitacion)
        {
            int idEstadoConfirmado = (int)Reservacion.EstadosReservacion.Confirmada;

            return (from reservacion in contexto.Set<Reservacion>()
                    where reservacion.Activo
                    && reservacion.IdEstado == idEstadoConfirmado
                    && reservacion.IdHabitacion == idHabitacion
                    select new
                    {
                        reservacion,
                        paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo),
                        configuracionTipo = reservacion.ConfiguracionTipo,
                        configuracionTarifa = reservacion.ConfiguracionTipo.ConfiguracionTarifa,
                        montosNoReembolsables = reservacion.MontosNoReembolsables.Where(m=> m.Activo)
                        //datosF = reservacion.DatosFiscales.Where(m => m.Activo)
                    }).ToList().Select(m => m.reservacion).FirstOrDefault();
        }

        public int SP_ObtenerCantidadCruzadas(DateTime fechaEntrada, DateTime fechaSalida, int idReservacion, int idConfiguracionTipoHabitacion)
        {
            /*
             @fechaEntrada datetime,
	@fechaSalida datetime,
	@idReservacion int,
	@idConfiguracionTipoHabitacion int
             */
            var p_fechaEntrada = new SqlParameter("@fechaEntrada", fechaEntrada);
            p_fechaEntrada.SqlDbType = System.Data.SqlDbType.DateTime;

            var p_fechaSalida = new SqlParameter("@fechaSalida", fechaSalida);
            p_fechaSalida.SqlDbType = System.Data.SqlDbType.DateTime;

            var p_idReservacion = new SqlParameter("@idReservacion", idReservacion);
            p_idReservacion.SqlDbType = System.Data.SqlDbType.Int;

            var p_idConfiguracionTipoHabitacion = new SqlParameter("@idConfiguracionTipoHabitacion", idConfiguracionTipoHabitacion);
            p_idConfiguracionTipoHabitacion.SqlDbType = System.Data.SqlDbType.Int;

            return contexto.ConsultaSql<int>("exec SotSchema.SP_ObtenerCantidadCruzadas @fechaEntrada, @fechaSalida, @idReservacion, @idConfiguracionTipoHabitacion", p_fechaEntrada, p_fechaSalida, p_idReservacion, p_idConfiguracionTipoHabitacion).FirstOrDefault();
        }

        public Dictionary<TiposPago, decimal> ObtenerPagosPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PagoReservacion, bool>> filtro = m => m.Activo;

            if (fechaFin.HasValue)
            {
                filtro = m => m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value); 
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            if (idsTiposPago.Count > 0)
                filtro = filtro.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

//#if DEBUG
//            var x = contexto.Set<PagoReservacion>().Where(filtro).ToString();
//#endif
            //var test = (from pagoRenta in contexto.Set<PagoReservacion>().Where(filtro)
            //            select pagoRenta).ToList();

            return (from pagoRenta in contexto.Set<PagoReservacion>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoPago into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposPago)m.Key, m => m.valor);
        }

        public Dictionary<TiposPago, decimal> ObtenerPagosDeConsumidasPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoR = (int)TiposPago.Reservacion;

            Expression<Func<PagoRenta, bool>> filtro = m => m.Activo && m.IdTipoPago == idTipoPagoR;
            Expression<Func<PagoReservacion, bool>> filtroR = m => m.Activo;

            if (fechaFin.HasValue)
            {
                filtro = m => (m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value)) && m.IdTipoPago == idTipoPagoR;
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            if (idsTiposPago.Count > 0)
                filtroR = filtroR.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            //var test = (from pagoRenta in contexto.Set<PagoReservacion>().Where(filtro)
            //            select pagoRenta).ToList();

            return (from pagoRenta in contexto.Set<PagoRenta>().Where(filtro)
                    join pagoReservacion in contexto.Set<PagoReservacion>().Where(filtroR) on pagoRenta.Referencia equals pagoReservacion.Reservacion.CodigoReserva
                    group pagoReservacion by pagoReservacion.IdTipoPago into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposPago)m.Key, m => m.valor);
        }

        public Dictionary<TiposTarjeta, decimal> ObtenerPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoT = (int)TiposPago.TarjetaCredito;

            Expression<Func<PagoReservacion, bool>> filtro = m => m.Activo && m.IdTipoPago == idTipoPagoT;

            if (fechaFin.HasValue)
            {
                filtro = m => (m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value)) && m.IdTipoPago == idTipoPagoT; 
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

#if DEBUG
            var x = (from pagoRenta in contexto.Set<PagoReservacion>().Where(filtro)
                     group pagoRenta by pagoRenta.IdTipoTarjeta into p
                     select new
                     {
                         p.Key,
                         valor = p.Sum(m => m.Valor)
                     });

#endif

            var res = (from pagoRenta in contexto.Set<PagoReservacion>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoTarjeta into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList();

            if (res.Count == 0)
                return new Dictionary<TiposTarjeta, decimal>();
            
            return res.ToDictionary(m => (TiposTarjeta)m.Key, m => m.valor);
        }

        public Dictionary<TiposTarjeta, decimal> ObtenerPagosTarjetaDeConsumidasPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoT = (int)TiposPago.TarjetaCredito;
            int idTipoPagoR = (int)TiposPago.Reservacion;

            Expression<Func<PagoRenta, bool>> filtro = m => m.Activo && m.IdTipoPago == idTipoPagoR;
            Expression<Func<PagoReservacion, bool>> filtroR = m => m.Activo && m.IdTipoPago == idTipoPagoT;

            if (fechaFin.HasValue)
            {
                filtro = m => (m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value)) && m.IdTipoPago == idTipoPagoR;
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);



            var res = (from pagoRenta in contexto.Set<PagoRenta>().Where(filtro)
                       join pagoReservacion in contexto.Set<PagoReservacion>().Where(filtroR) on pagoRenta.Referencia equals pagoReservacion.Reservacion.CodigoReserva
                       group pagoReservacion by pagoReservacion.IdTipoTarjeta into p
                       select new
                       {
                           p.Key,
                           valor = p.Sum(m => m.Valor)
                       }).ToList();

            if (res.Count == 0)
                return new Dictionary<TiposTarjeta, decimal>();

            return res.ToDictionary(m => (TiposTarjeta)m.Key, m => m.valor);
        }

        public Reservacion ObtenerPorCodigoConMontosNoReembolsables(string codigoReservacion)
        {
            return (from reservacion in contexto.Set<Reservacion>()
                    where reservacion.CodigoReserva == codigoReservacion
                    select new
                    {
                        reservacion,
                        mnr = reservacion.MontosNoReembolsables.Where(m => m.Activo)
                    }).ToList().Select(m => m.reservacion).FirstOrDefault();
        }
    }
}
