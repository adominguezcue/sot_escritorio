﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioConfiguracionesTurno : Repositorio<ConfiguracionTurno>, IRepositorioConfiguracionesTurno
    {
        public RepositorioConfiguracionesTurno()
            : base(new Contextos.SOTContext())
        {
        }

        public List<ConfiguracionTurno> ObtenerConfiguracionesTurno(bool soloActivas = true)
        {
            if (soloActivas)
                return contexto.Set<ConfiguracionTurno>().Where(m => m.Activa).OrderBy(m => m.Orden).ToList();

            return contexto.Set<ConfiguracionTurno>().OrderBy(m => m.Orden).ToList();
        }


        public string ObtenerNombreTurnoPorCorte(int idCorteTurno)
        {
            return (from corte in contexto.Set<CorteTurno>()
                    where corte.Id == idCorteTurno
                    select corte.ConfiguracionTurno.Nombre).FirstOrDefault();
        }
    }
}
