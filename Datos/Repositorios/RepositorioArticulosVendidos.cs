﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;
using System.Data.Entity;
using Modelo.Almacen.Entidades;
using System.Linq.Expressions;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioArticulosVendidos : Repositorio<VW_ArticuloVendido>, IRepositorioArticulosVendidos
    {
		List<VW_ArticuloVendido> respuestalista = new List<VW_ArticuloVendido>();																					
        public RepositorioArticulosVendidos() : base(new Contextos.SOTContext())
        {
        }

        public List<VW_ArticuloVendido> ObtenerArticulosVendidos(int? ordenTurno = null, string categoria = null, string subCategoria = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, bool? agrupar = null)
        {
            Expression<Func<VW_ArticuloVendido, bool>> filtro = null;

            List<VW_ArticuloVendido> respuesta = new List<VW_ArticuloVendido>();
            respuesta = null;				 
			
            if (ordenTurno.HasValue)
            {
                filtro = m => m.Turno == ordenTurno;
            }
            if (!string.IsNullOrWhiteSpace(categoria))
            {
                if (filtro == null)
                    filtro = m => m.Categoria == categoria;
                else
                    filtro = filtro.Compose(m => m.Categoria == categoria, Expression.AndAlso);
            }
            if (!string.IsNullOrWhiteSpace(subCategoria))
            {
                if (filtro == null)
                    filtro = m => m.Subcategoria == subCategoria;
                else
                    filtro = filtro.Compose(m => m.Subcategoria == subCategoria, Expression.AndAlso);
            }

            if (fechaInicio.HasValue)
            {
                //Encontrar fecha de corte
                Expression<Func<CorteTurno, bool>> filtroCortesini = m => true;
                filtroCortesini = filtroCortesini.Compose(m => m.FechaCorte >= fechaInicio, Expression.AndAlso);
                DateTime fechacortesfin = new DateTime();
                fechacortesfin = fechaInicio.Value.AddHours(8);
                filtroCortesini = filtroCortesini.Compose(m => m.FechaCorte <= fechacortesfin, Expression.AndAlso);
                var fechaTurnoCorteini = (from fechaCorteTurno in contexto.Set<CorteTurno>().Where(filtroCortesini)
                                          select fechaCorteTurno.FechaCorte).FirstOrDefault();

                if (fechaTurnoCorteini != null)
                    fechaInicio = Convert.ToDateTime(fechaTurnoCorteini);

				
                if (filtro == null)
                    filtro = m => m.Fecha >= fechaInicio;
                else
                    filtro = filtro.Compose(m => m.Fecha >= fechaInicio, Expression.AndAlso);
            }
            if (fechaFin.HasValue)
            {

                Expression<Func<CorteTurno, bool>> filtroCortesfin = m => true;
                filtroCortesfin = filtroCortesfin.Compose(m => m.FechaCorte >= fechaFin, Expression.AndAlso);
                DateTime fechacortesfin = new DateTime();
                fechacortesfin = fechaFin.Value.AddHours(8);
                filtroCortesfin = filtroCortesfin.Compose(m => m.FechaCorte <= fechacortesfin, Expression.AndAlso);
                var fechaTurnoCortefin = (from fechaCorteTurno in contexto.Set<CorteTurno>().Where(filtroCortesfin)
                                          select fechaCorteTurno.FechaCorte).FirstOrDefault();
                if (fechaTurnoCortefin != null)
                    fechaFin = Convert.ToDateTime(fechaTurnoCortefin);
				
				
                if (filtro == null)
                    filtro = m => m.Fecha <= fechaFin;
                else
                    filtro = filtro.Compose(m => m.Fecha <= fechaFin, Expression.AndAlso);
            }

            respuesta = contexto.Set<VW_ArticuloVendido>().Where(filtro).OrderBy(m => m.Fecha).ToList();
            if (agrupar == true)
            {
                agruparproductos(respuesta);
                respuesta = respuestalista;
            }		 

            return respuesta;
        }
        private void agruparproductos(List<VW_ArticuloVendido> listaagrupar)
        {

            if (listaagrupar != null && listaagrupar.Count > 0)
            {
                List<VW_ArticuloVendido> listauxiliar = new List<VW_ArticuloVendido>();
                listauxiliar = listaagrupar;
                for (int i = 0; i < listaagrupar.Count; i++)
                {
                    VW_ArticuloVendido nuevo = new VW_ArticuloVendido();
                    nuevo = listaagrupar[i];
                    int cantidadnuevo = listaagrupar[i].Cantidad;
                    decimal precioventaiva = listaagrupar[i].PrecioUnidadConIVA;
                    decimal total = Convert.ToDecimal(listaagrupar[i].Total);

                    for (int j = 0; j <  listauxiliar.Count; j++)
                    {
                        if (listaagrupar[i].Id != listauxiliar[j].Id)
                        {
                            if (listaagrupar[i].Codigo.Equals(listauxiliar[j].Codigo))
                            {
                                cantidadnuevo = cantidadnuevo + listauxiliar[j].Cantidad;
                                precioventaiva = precioventaiva + listauxiliar[j].PrecioUnidadConIVA;
                                total = total + Convert.ToDecimal(listauxiliar[j].Total);
                            }
                        }
                    }
                    nuevo.Cantidad = cantidadnuevo;
                    nuevo.PrecioUnidadConIVA = precioventaiva;
                    nuevo.Total = total;
                    respuestalista.Add(nuevo);
                    //listaagrupar.RemoveAt(i);
                    //banderadeparo
                    if (listaagrupar.Count>0)
                        listaagrupar = EliminacodigodeLista(listaagrupar, listaagrupar[i].Codigo);

                    agruparproductos(listaagrupar);
                }
            }
        }
        private List<VW_ArticuloVendido> EliminacodigodeLista(List<VW_ArticuloVendido> listaeliminada, string codigo)
        {
            List<VW_ArticuloVendido> respuesta = new List<VW_ArticuloVendido>();
            respuesta = listaeliminada;
            for (int i = 0; i < listaeliminada.Count; i++)
            {
                    if(respuesta[i].Codigo.Equals(codigo))
                    {
                        respuesta.RemoveAt(i);
                        respuesta = EliminacodigodeLista(respuesta, codigo);
                    }
            }
            return respuesta;
        }
		
		
    }
}
