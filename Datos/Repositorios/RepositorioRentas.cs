﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Dominio.Nucleo.Entidades;

namespace Datos.Repositorios
{
    public class RepositorioRentas : Repositorio<Renta>, IRepositorioRentas
    {
        public RepositorioRentas()
            : base(new Contextos.SOTContext())
        {
        }

        public Renta ObtenerRentaActualPorHabitacion(int idHabitacion)
        {
            var resultado = (from renta in contexto.Set<Renta>()
                             where renta.Activa
                             && renta.IdHabitacion == idHabitacion
                             select new
                             {
                                 renta,
                                 tarifa = renta.ConfiguracionTipo.ConfiguracionTarifa.IdTarifa,
                                 //rentaHabitacion = renta.RentaHabitaciones.Where(m => m.Activa && m.IdHabitacion == idHabitacion),
                                 rentaAuto = renta.RentaAutomoviles.Where(m => m.Activa),
                                 ventas = renta.VentasRenta.Where(m => m.Activo),
                                 extensiones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa),
                                 //tiempos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.TiemposExtra).Where(m => m.Activa),
                                 //renovaciones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Renovaciones).Where(m => m.Activa),
                                 personasExtra = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PersonasExtra).Where(m => m.Activa),
                                 paquetes = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PaquetesRenta).Where(m => m.Activo),
                                 pagos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Pagos).Where(m => m.Activo),
                                 //comandas = renta.Comandas.Where(m=>m.Activo)
                             }).ToList().FirstOrDefault();

            if (resultado == null)
                return null;

            resultado.renta.TarifaTmp = (ConfiguracionTarifa.Tarifas)resultado.tarifa;

            return resultado.renta;
        }

        public Renta ObtenerCargada(int idRenta)
        {
            return (from renta in contexto.Set<Renta>()
                    where renta.Activa
                    && renta.Id == idRenta
                    select new
                    {
                        renta,
                        //rentaHabitacion = renta.RentaHabitaciones.Where(m => m.Activa && m.IdHabitacion == idHabitacion),
                        rentaAuto = renta.RentaAutomoviles.Where(m => m.Activa),
                        ventas = renta.VentasRenta.Where(m => m.Activo),
                        extensiones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa),
                        //tiempos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.TiemposExtra).Where(m => m.Activa),
                        //renovaciones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Renovaciones).Where(m => m.Activa),
                        personasExtra = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PersonasExtra).Where(m => m.Activa),
                        mnr = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.MontosNoReembolsablesRenta).Where(m => m.Activo),
                        paquetes = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PaquetesRenta).Where(m => m.Activo),
                        pagos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Pagos).Where(m => m.Activo),
                    }).ToList().Select(m => m.renta).FirstOrDefault();
        }


        public List<AutomovilRenta> ObtenerAutomovilesUltimaRenta(int idHabitacion)
        {
            return (from renta in contexto.Set<Renta>()
                    where renta.IdHabitacion == idHabitacion
                    orderby renta.Id descending
                    select renta.RentaAutomoviles.Where(m => m.Activa)).ToList().Select(m => m.ToList()).FirstOrDefault() ?? new List<AutomovilRenta>();
        }

        public Renta ObtenerUltimaRentaPorHabitacionCargada(int idHabitacion)
        {
            int idRenta = (from renta in contexto.Set<Renta>()
                           where renta.IdHabitacion == idHabitacion
                           orderby renta.FechaInicio descending, renta.Id descending
                           select renta.Id).FirstOrDefault();

            if (idRenta <= 0)
                return null;

            return (from renta in contexto.Set<Renta>()
                    where renta.Id == idRenta
                    select new
                    {
                        renta,
                        rentaAuto = renta.RentaAutomoviles.Where(m => m.Activa),
                        ventas = renta.VentasRenta.Where(m => m.Activo),
                        extensiones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa),
                        //tiempos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.TiemposExtra).Where(m => m.Activa),
                        //renovaciones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Renovaciones).Where(m => m.Activa),
                        personasExtra = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PersonasExtra).Where(m => m.Activa),
                        paquetes = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PaquetesRenta).Where(m => m.Activo),
                        pagos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Pagos).Where(m => m.Activo),
                    }).ToList().Select(m => m.renta).FirstOrDefault();
        }

        public List<DtoResumenVentaRenta> ObtenerResumenVentas(int idRenta, decimal porcentajeIVA)
        {
            var datosRenta = (from renta in contexto.Set<Renta>()
                              where renta.Id == idRenta
                              orderby renta.FechaInicio descending, renta.Id descending
                              select
                              new
                              {
                                  renta.Id,
                                  renta.NumeroServicio,
                                  renta.Habitacion.NumeroHabitacion
                              }).FirstOrDefault();

            if (datosRenta == null)
                return new List<DtoResumenVentaRenta>();

            int idConceptoPersonasExtra = (int)DetallePago.ConceptosPago.PersonasExtra;
            int idConceptoRenovacion = (int)DetallePago.ConceptosPago.Renovacion;
            int idConceptoHorasExtra = (int)DetallePago.ConceptosPago.HorasExtra;
            int idConceptoHabitacion = (int)DetallePago.ConceptosPago.Habitacion;
            int idConceptoPaquetes = (int)DetallePago.ConceptosPago.Paquetes;
            int idConceptoDescuentos = (int)DetallePago.ConceptosPago.Descuentos;

            //List<int> formasPagoIngresos = new List<int>() { (int)TiposPago.Efectivo, (int)TiposPago.Reservacion, (int)TiposPago.TarjetaCredito };
            List<int> formasPagoDescuentos = new List<int>() { (int)TiposPago.Consumo, (int)TiposPago.VPoints };
            List<int> formasPagoCortesia = new List<int>() { (int)TiposPago.Cortesia, (int)TiposPago.Cupon };

            var elementos = (from venta in contexto.Set<VentaRenta>()
                             where venta.IdRenta == datosRenta.Id
                             && venta.Activo
                             && venta.Cobrada
                             select new
                             {
                                 Id = venta.Id,
                                 Cobrada = venta.Cobrada,
                                 fecha = venta.FechaCreacion,
                                 personasExtra = venta.DetallesPago.Where(m => m.Activo && m.IdConceptoPago == idConceptoPersonasExtra).Select(m => m.ValorConIVA),
                                 renovaciones = venta.DetallesPago.Where(m => m.Activo && m.IdConceptoPago == idConceptoRenovacion).Select(m => m.ValorConIVA),
                                 horasExtra = venta.DetallesPago.Where(m => m.Activo && m.IdConceptoPago == idConceptoHorasExtra).Select(m => m.ValorConIVA),
                                 habitacion = venta.DetallesPago.Where(m => m.Activo && m.IdConceptoPago == idConceptoHabitacion).Select(m => m.ValorConIVA),
                                 paquetes = venta.DetallesPago.Where(m => m.Activo && m.IdConceptoPago == idConceptoPaquetes).Select(m => m.ValorConIVA),
                                 descuentos = venta.DetallesPago.Where(m => m.Activo && m.IdConceptoPago == idConceptoDescuentos).Select(m => m.ValorConIVA),
                                 descuentosP = venta.Pagos.Where(m => m.Activo && formasPagoDescuentos.Contains(m.IdTipoPago)).Select(m => m.Valor),
                                 cortesias = venta.Pagos.Where(m => m.Activo && formasPagoCortesia.Contains(m.IdTipoPago)).Select(m => m.Valor),
                             }).ToList().Select(m => new DtoResumenVentaRenta
                             {
                                 Id = m.Id,
                                 Cobrada = m.Cobrada,
                                 Servicio = datosRenta.NumeroServicio,
                                 NumeroHabitacion = datosRenta.NumeroHabitacion,
                                 Fecha = m.fecha,
                                 Habitacion = m.habitacion.ToList().Sum(),
                                 PersonasExtra = m.personasExtra.ToList().Sum(),
                                 Renovaciones = m.renovaciones.ToList().Sum(),
                                 HorasExtra = m.horasExtra.ToList().Sum(),
                                 Paquetes = m.paquetes.ToList().Sum(),
                                 DescuentosPorPaquete = m.descuentosP.ToList().Sum(),
                                 Descuentos = m.descuentos.ToList().Sum() + m.descuentosP.ToList().Sum(),
                                 Cortesias = m.cortesias.ToList().Sum()
                             }).ToList();

            var ids = elementos.Select(m => m.Id).ToList();

            elementos.AddRange((from venta in contexto.Set<VentaRenta>()
                                where venta.IdRenta == datosRenta.Id
                                && venta.Activo
                                && !venta.Cobrada
                                && !ids.Contains(venta.Id)
                                select new
                                {
                                    Id = venta.Id,
                                    Cobrada = venta.Cobrada,
                                    fecha = venta.FechaCreacion,
                                    personasExtra = venta.PersonasExtra.Where(m => m.Activa).Select(m => m.Precio),
                                    renovaciones = venta.Extensiones.Where(m => m.EsRenovacion && m.Activa).Select(m => m.Precio),
                                    horasExtra = venta.Extensiones.Where(m => !m.EsRenovacion && m.Activa).Select(m => m.Precio),
                                    habitacion = venta.EsInicial ? venta.Renta.PrecioHabitacion : 0,
                                    paquetes = venta.PaquetesRenta.Where(m => m.Activo).Select(m => m.Precio),
                                    descuentos = venta.PaquetesRenta.Where(m => m.Activo).Select(m => m.Descuento),
                                    descuentosP = 0,//venta.Pagos.Where(m => m.Activo && formasPagoDescuentos.Contains(m.IdTipoPago)).Select(m => m.Valor),
                                    cortesias = 0,//venta.Pagos.Where(m => m.Activo && formasPagoCortesia.Contains(m.IdTipoPago)).Select(m => m.Valor),
                                }).ToList().Select(m => new DtoResumenVentaRenta
                                {
                                    Id = m.Id,
                                    Cobrada = m.Cobrada,
                                    Servicio = datosRenta.NumeroServicio,
                                    NumeroHabitacion = datosRenta.NumeroHabitacion,
                                    Fecha = m.fecha,
                                    Habitacion = Math.Round(m.habitacion * (1 + porcentajeIVA), 2),//.ToList().Sum(),
                                    PersonasExtra = m.personasExtra.ToList().Select(p => Math.Round(p * (1 + porcentajeIVA), 2)).Sum(),
                                    Renovaciones = m.renovaciones.ToList().Select(p => Math.Round(p * (1 + porcentajeIVA), 2)).Sum(),
                                    HorasExtra = m.horasExtra.ToList().Select(p => Math.Round(p * (1 + porcentajeIVA), 2)).Sum(),
                                    Paquetes = m.paquetes.ToList().Select(p => Math.Round(p * (1 + porcentajeIVA), 2)).Sum(),
                                    DescuentosPorPaquete = m.descuentosP,//.ToList().Sum(),
                                    Descuentos = m.descuentos.ToList().Select(p => Math.Round(p * (1 + porcentajeIVA), 2)).Sum() + m.descuentosP,//.ToList().Sum(),
                                    Cortesias = m.cortesias//.ToList().Sum()
                                }));

            return elementos;

        }

        public Renta ObtenerUltimaRentaPorHabitacionConDatosFiscales(int idHabitacion)
        {
            return (from renta in contexto.Set<Renta>()
                    where renta.IdHabitacion == idHabitacion
                    orderby renta.FechaInicio descending, renta.Id descending
                    select new
                    {
                        renta,
                        datosF = renta.DatosFiscales
                    }).ToList().Select(m => m.renta).FirstOrDefault();
        }

        public Renta ObtenerUltimaRentaPorHabitacion(int idHabitacion)
        {
            return (from renta in contexto.Set<Renta>()
                    where renta.IdHabitacion == idHabitacion
                    orderby renta.FechaRegistro descending, renta.Id descending
                    select renta).FirstOrDefault();
        }


        public List<DetallePago> ObtenerDetallesPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            Expression<Func<DetallePago, bool>> filtro = m => m.Activo;

            if (fechaFin.HasValue)
            {
                filtro = m => m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value);
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            return contexto.Set<DetallePago>().Where(filtro).ToList();
        }


        public Dictionary<string, List<DetallePago>> ObtenerDetallesPagosPorFechaYTipo(DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            Expression<Func<DetallePago, bool>> filtro = m => m.Activo;

            if (fechaFin.HasValue)
            {
                filtro = m => m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value);
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from detalle in contexto.Set<DetallePago>().Where(filtro)
                    group detalle by detalle.VentaRenta.Renta.Habitacion.TipoHabitacion.Descripcion into g
                    select g).ToList().ToDictionary(m => m.Key, m => m.ToList());
        }

        public Dictionary<TiposPago, decimal> ObtenerPagosHabitacionPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PagoRenta, bool>> filtro = m => m.Activo;

            if (fechaFin.HasValue)
            {
                filtro = m => m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value);
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            if (idsTiposPago.Count > 0)
                filtro = filtro.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from pagoRenta in contexto.Set<PagoRenta>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoPago into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposPago)m.Key, m => m.valor);
        }

        public decimal ObtenerMontoNoReembolsablePorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoR = (int)TiposPago.Reservacion;

            Expression<Func<Venta, bool>> filtro = m => m.Activa;

            if (fechaFin.HasValue)
            {
                filtro = m => m.Activa || (m.FechaCancelacion.HasValue && m.FechaCancelacion > fechaFin.Value);
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            //if (idsTiposPago.Count > 0)
            //    filtro = filtro.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);

            filtro = filtro.Compose(m => m.Correcta, Expression.AndAlso);

            //var filtroCompilado = filtro.Compile();

            var r = (from venta in contexto.Set<Venta>().Where(filtro)
                     join ventaRenta in contexto.Set<VentaRenta>() on venta.Transaccion equals ventaRenta.Transaccion
                     where ventaRenta.MontosNoReembolsablesRenta.Any(m => m.Activo || m.FechaEliminacion >= ventaRenta.FechaEliminacion)
                     select ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo || m.FechaEliminacion >= ventaRenta.FechaEliminacion).Sum(m => m.ValorConIVA)).ToList();

            if (r == null || r.Count == 0)
                return 0;

            return r.Sum();
        }


        public List<PagoRenta> ObtenerPagosRentasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            //int idEstadoFinalizado = (int)Renta.Estados.Finalizada;

            //Expression<Func<Renta, bool>> filtro = m => true;//m.IdEstado == idEstadoFinalizado;
            Expression<Func<PagoRenta, bool>> filtroPago = m => m.Activo;

            if (fechaFin.HasValue)
            {
                filtroPago = m => m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value);
                //filtroPago = filtroPago.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtroPago = filtroPago.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtroPago = filtroPago.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtroPago = filtroPago.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            if (formasPago.HasValue)
            {
                var idFormaPago = (int)formasPago;

                filtroPago = filtroPago.Compose(m => m.IdTipoPago == idFormaPago, Expression.AndAlso);
            }

            //var filtroPagoCompilado = filtroPago.Compile();

            //filtro = filtro.Compose(m => m.Pagos.AsQueryable().Any(filtroPago), Expression.AndAlso);

            return contexto.Set<PagoRenta>().Where(filtroPago).ToList();
        }

        //public List<PersonaExtra> ObtenerPersonasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        //{
        //    Expression<Func<PersonaExtra, bool>> filtro = m => m.Activa;

        //    if (fechaFin.HasValue)
        //    {
        //        filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
        //        filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
        //    }
        //    if (fechaInicio.HasValue)
        //        filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
        //    //if (fechaFin.HasValue)
        //    //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

        //    return contexto.Set<PersonaExtra>().Where(filtro).ToList();
        //}

        //public List<Extension> ObtenerHorasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        //{
        //    Expression<Func<Extension, bool>> filtro = m => m.Activa && !m.EsRenovacion;

        //    if (fechaInicio.HasValue)
        //        filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
        //    if (fechaFin.HasValue)
        //        filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

        //    return contexto.Set<Extension>().Where(filtro).ToList();
        //}


        public List<PaqueteRenta> ObtenerPaquetesRentaPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PaqueteRenta, bool>> filtro = m => m.Activo;

            if (fechaFin.HasValue)
            {
                filtro = m => m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value);
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var datos = (from paqueteRenta in contexto.Set<PaqueteRenta>().Where(filtro)
                         select new
                         {
                             paqueteRenta,
                             NombePaquete = paqueteRenta.Paquete.Nombre
                         }).ToList();

            if (datos.Count == 0)
                return new List<PaqueteRenta>();

            datos.ForEach(m => m.paqueteRenta.NombrePaquete = m.NombePaquete);

            return datos.Select(m => m.paqueteRenta).ToList();
        }


        public Dictionary<TiposTarjeta, decimal> ObtenerPagosHabitacionTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoT = (int)TiposPago.TarjetaCredito;

            Expression<Func<PagoRenta, bool>> filtro = m => m.Activo && m.IdTipoPago == idTipoPagoT;

            if (fechaFin.HasValue)
            {
                filtro = m => (m.Activo || (m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value)) && m.IdTipoPago == idTipoPagoT;
                //filtro = filtro.Compose(m => m.FechaEliminacion.HasValue && m.FechaEliminacion > fechaFin.Value, XDXDXD);
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var res = (from pagoRenta in contexto.Set<PagoRenta>().Where(filtro)
                       group pagoRenta by pagoRenta.IdTipoTarjeta into p
                       select new
                       {
                           p.Key,
                           valor = p.Sum(m => m.Valor)
                       }).ToList();

            if (res.Count == 0)
                return new Dictionary<TiposTarjeta, decimal>();

            return res.ToDictionary(m => (TiposTarjeta)m.Key, m => m.valor);
        }



        public List<DetallePago> ObtenerDetallesPorTransaccion(string transaccion, DateTime? fechaFin)
        {
            if (fechaFin.HasValue)
                return contexto.Set<DetallePago>().Where(m => m.Transaccion == transaccion && (m.Activo || m.FechaEliminacion > fechaFin)).ToList();

            return contexto.Set<DetallePago>().Where(m => m.Transaccion == transaccion && m.Activo).ToList();
        }


        public List<DtoEnvoltorioEmpleado> ObtenerInformacionValetsPorIdVentaRenta(List<int> idsVentasRenta)
        {
            return (from ventaRenta in contexto.Set<VentaRenta>()
                    where idsVentasRenta.Contains(ventaRenta.Id)
                    select new
                    {
                        IdRenta = ventaRenta.Id,
                        NombreValet = ventaRenta.Valet.Nombre,
                        ApellidoP = ventaRenta.Valet.ApellidoPaterno,
                        ApellidoM = ventaRenta.Valet.ApellidoMaterno,
                        IdValet = ventaRenta.IdValet,
                    }).ToList().Select(m => new DtoEnvoltorioEmpleado
                    {
                        IdEntidadPadre = m.IdRenta,
                        IdEmpleado = m.IdValet,
                        NombreEmpleado = m.IdValet.HasValue ? string.Join(" ", m.NombreValet, m.ApellidoP, m.ApellidoM) : ""
                    }).ToList();
        }


        public DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion)
        {
            var df = (from ventaR in contexto.Set<VentaRenta>()
                      where ventaR.Transaccion == transaccion
                      select ventaR.Renta.DatosFiscales).FirstOrDefault();

            if (df == null)
            {
                df = (from ventaR in contexto.Set<PagoComanda>()
                      where ventaR.Transaccion == transaccion
                      select ventaR.Comanda.Renta.DatosFiscales).FirstOrDefault();
            }

            return df;
        }


        public Renta ObtenerConExtensiones(int idRenta)
        {
            return (from renta in contexto.Set<Renta>()
                    where renta.Activa
                    && renta.Id == idRenta
                    select new
                    {
                        renta,
                        ventas = renta.VentasRenta.Where(m => m.Activo),
                        extensiones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa),
                        //tiempos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.TiemposExtra).Where(m => m.Activa),
                        //renovaciones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Renovaciones).Where(m => m.Activa),
                    }).ToList().Select(m => m.renta).FirstOrDefault();
        }


        public Renta ObtenerRentaParaCancelar(int idHabitacion)
        {
            var resultado = (from renta in contexto.Set<Renta>()
                             where renta.Activa
                             && renta.IdHabitacion == idHabitacion
                             select new
                             {
                                 renta,
                                 rentaAuto = renta.RentaAutomoviles.Where(m => m.Activa),
                                 ventasRentas = renta.VentasRenta.Where(m => m.Activo),
                                 preventas = renta.VentasRenta.Where(m => m.Activo).Select(m => m.Preventa),
                                 detalles = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.DetallesPago).Where(m => m.Activo),
                                 extensiones = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa),
                                 personasExtra = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PersonasExtra).Where(m => m.Activa),
                                 paquetes = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PaquetesRenta).Where(m => m.Activo),
                                 pagos = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Pagos).Where(m => m.Activo),
                             }).ToList().FirstOrDefault();

            if (resultado == null)
                return null;

            return resultado.renta;
        }


        public DtoAutomovil ObtenerDatosAutomovil(string matricula)
        {
            var matriculaU = matricula.Trim().ToUpper();

            var r = (from auto in contexto.Set<AutomovilRenta>()
                     where auto.Matricula.Trim().ToUpper().Equals(matriculaU)
                     orderby auto.FechaModificacion descending
                     select new
                     {
                         auto.Color,
                         auto.Marca,
                         auto.Matricula,
                         auto.Modelo
                     }).FirstOrDefault();

            if (r == null)
                return null;

            return new DtoAutomovil
            {
                Color = r.Color,
                Marca = r.Marca,
                Matricula = r.Matricula,
                Modelo = r.Modelo
            };
        }

        public string ObtieneCodigoReservacion(DateTime fecharegistro, int idHabitacion)
        {
            string respuesta = "";
            var resultado = (from reservaciones in contexto.Set<Reservacion>()
                             where reservaciones.FechaModificacion >= fecharegistro
                             && reservaciones.IdHabitacion == idHabitacion
                             select new
                             {
                                 reservaciones.CodigoReserva
                             }).Select(m => m.CodigoReserva).FirstOrDefault();

            if (resultado == null)
                return respuesta;


            return resultado;

        }
    }
}
