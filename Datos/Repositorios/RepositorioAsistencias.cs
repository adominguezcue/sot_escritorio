﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Dtos;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioAsistencias : Repositorio<Asistencia>, IRepositorioAsistencias
    {
        public RepositorioAsistencias() : base(new Contextos.SOTContext()) { }

        public List<Asistencia> ObtenerAsistenciasFiltadas(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            Expression<Func<Asistencia, bool>> filtro = m => m.Activa;

            if (!string.IsNullOrEmpty(nombre))
            {
                var nombreU = nombre.Trim().ToUpper();
                filtro = filtro.Compose(m => m.Empleado.Nombre.ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoPaterno))
            {
                var apellidoPU = apellidoPaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.Empleado.ApellidoPaterno.ToUpper().Contains(apellidoPU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoMaterno))
            {
                var apellidoMU = apellidoMaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.Empleado.ApellidoMaterno.ToUpper().Contains(apellidoMU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(telefono))
            {
                var telefonoU = telefono.Trim().ToUpper();
                filtro = filtro.Compose(m => m.Empleado.Nombre.ToUpper().Contains(telefonoU), Expression.AndAlso);
            }
            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaEntrada >= fechaInicio, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaEntrada <= fechaFin, Expression.AndAlso);

            var resultado = (from asistencia in contexto.Set<Asistencia>().Where(filtro)
                             select new
                             {
                                 asistencia,
                                 Nombre = asistencia.Empleado.Nombre,
                                 ApellidoP = asistencia.Empleado.ApellidoPaterno,
                                 ApellidoM = asistencia.Empleado.ApellidoMaterno,
                                 Turno = asistencia.ConfiguracionTurno.Nombre
                             }).ToList();

            foreach (var item in resultado)
            {
                item.asistencia.NombreEmpleadoTmp = string.Join(" ", item.Nombre, item.ApellidoP, item.ApellidoM);
                item.asistencia.TurnoTmp = item.Turno;
            }

            return resultado.Select(m => m.asistencia).ToList();
        }

        public Asistencia ObtenerUltimaSinSalida(int idEmpleado)
        {
            return (from asistencia in contexto.Set<Asistencia>()
                    where asistencia.Activa
                    && asistencia.IdEmpleado == idEmpleado
                    && !asistencia.FechaSalida.HasValue
                    orderby asistencia.FechaSalidaTentativa descending
                    select asistencia).FirstOrDefault();
        }

        public DtoResumenAsistencia ObtenerResumenUltimaAsistenciaAbierta(string numeroEmpleado)
        {
            var resumen = (from asistencia in contexto.Set<Asistencia>()
                           where asistencia.Activa
                           && asistencia.Empleado.NumeroEmpleado.Equals(numeroEmpleado)
                           && !asistencia.FechaSalida.HasValue
                           orderby asistencia.FechaSalidaTentativa descending
                           select new
                           {
                               idEmpleado = asistencia.IdEmpleado,
                               nombre = asistencia.Empleado.Nombre,
                               apellidoP = asistencia.Empleado.ApellidoPaterno,
                               apellidoM = asistencia.Empleado.ApellidoMaterno,
                               entrada = asistencia.FechaEntrada,
                               salida = asistencia.FechaSalida,
                               salidaComer = asistencia.InicioComida,
                               regresoComer = asistencia.FinComida
                           }).ToList().Select(m => new DtoResumenAsistencia
                            {
                                IdEmpleado = m.idEmpleado,
                                Empleado = string.Join(" ", m.nombre, m.apellidoP, m.apellidoM),
                                Entrada = m.entrada,
                                Salida = m.salida,
                                InicioComida = m.salidaComer,
                                FinComida = m.regresoComer
                            }).FirstOrDefault();

            if (resumen == null)
                resumen = (from empleado in contexto.Set<Empleado>()
                           where empleado.NumeroEmpleado.Equals(numeroEmpleado)
                           select new
                           {
                               empleado.Id,
                               empleado.Nombre,
                               empleado.ApellidoPaterno,
                               empleado.ApellidoMaterno,
                           }).ToList().Select(m => new DtoResumenAsistencia
                           {
                               IdEmpleado = m.Id,
                               Empleado = string.Join(" ", m.Nombre, m.ApellidoPaterno, m.ApellidoMaterno),
                           }).FirstOrDefault();

            return resumen;
        }
    }
}
