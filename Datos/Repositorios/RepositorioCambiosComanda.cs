﻿using Modelo;
using Modelo.Dtos;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using System.Data.SqlClient;
using Dominio.Nucleo.Entidades;

namespace Datos.Repositorios
{
    public class RepositorioCambiosComanda: Repositorio<CambiosComandas>, IRepositorioCambiosComanda
    {
        public RepositorioCambiosComanda() : base(new Contextos.SOTContext())
        {
        }
    }
}
