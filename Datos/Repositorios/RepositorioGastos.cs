﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioGastos : Repositorio<Gasto>, IRepositorioGastos
    {
        public RepositorioGastos() : base(new Contextos.SOTContext()) { }

        public List<Gasto> ObtenerGastosNoSincronizados(int idCorteIgnorar)
        {
            var resultado = (from gasto in contexto.Set<Gasto>()
                             where gasto.Activo
                             && !gasto.Sincronizado
                             && gasto.IdCorteTurno != idCorteIgnorar
                             select new
                             {
                                 gasto,
                                 concepto = gasto.ConceptoGasto.Concepto,
                                 centro = gasto.ConceptoGasto.CentroCostos.Nombre
                             }).ToList();

            if (resultado.Count == 0)
                return new List<Gasto>();

            foreach (var item in resultado) 
            {
                item.gasto.ConceptoTmp = item.concepto;
                item.gasto.CentroCostosTmp = item.centro;
            }

            return resultado.Select(m => m.gasto).ToList();
        }
    }
}
