﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioSolicitudesFalta : Repositorio<SolicitudFalta>, IRepositorioSolicitudesFalta
    {
        public RepositorioSolicitudesFalta() : base(new Contextos.SOTContext()) { }

        public List<DtoSolicitudFalta> ObtenerSolicitudesFaltas(string nombre, string apellidoPaterno, string apellidoMaterno, DateTime? fechaInicial, DateTime? fechaFinal)
        {
            Expression<Func<SolicitudFalta, bool>> filtro = m => m.Activa;

            if (!string.IsNullOrWhiteSpace(nombre))
            {
                var nombreU = nombre.Trim().ToUpper();

                filtro = filtro.Compose(m => m.EmpleadoSolicita.Nombre.ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (!string.IsNullOrWhiteSpace(apellidoPaterno))
            {
                var apellidoPU = apellidoPaterno.Trim().ToUpper();

                filtro = filtro.Compose(m => m.EmpleadoSolicita.ApellidoPaterno.ToUpper().Contains(apellidoPU), Expression.AndAlso);
            }
            if (!string.IsNullOrWhiteSpace(apellidoMaterno))
            {
                var qapellidoMU = apellidoMaterno.Trim().ToUpper();

                filtro = filtro.Compose(m => m.EmpleadoSolicita.ApellidoMaterno.ToUpper().Contains(qapellidoMU), Expression.AndAlso);
            }
            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m => m.FechaAplicacion >= fechaInicial.Value, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaAplicacion <= fechaFinal.Value, Expression.AndAlso);

            return (from solicitud in contexto.Set<SolicitudFalta>().Where(filtro)
                    select new
                    {
                        Id = solicitud.Id,
                        nombreE = solicitud.EmpleadoSolicita.Nombre,
                        apellidoP = solicitud.EmpleadoSolicita.ApellidoPaterno,
                        apellidoM = solicitud.EmpleadoSolicita.ApellidoMaterno,
                        area = solicitud.EmpleadoSolicita.Puesto.AreaActual.Nombre,
                        //turno = solicitud.EmpleadoSolicita.Turno.Nombre,
                        fechaA = solicitud.FechaAplicacion,
                        motivo = solicitud.Motivo,
                        idFormaP = solicitud.IdFormaPago,
                        nombreSE = solicitud.IdEmpleadoSuplente.HasValue ? solicitud.Suplente.Nombre : null,
                        apellidoSP = solicitud.IdEmpleadoSuplente.HasValue ? solicitud.Suplente.ApellidoPaterno : null,
                        apellidoSM = solicitud.IdEmpleadoSuplente.HasValue ? solicitud.Suplente.ApellidoMaterno: null,
                    }).ToList().Select(m => new DtoSolicitudFalta
                    {
                        Id = m.Id,
                        NombreEmpleado = string.Join(" ", m.nombreE, m.apellidoP, m.apellidoM),
                        Area = m.area,
                        FechaAplicacion = m.fechaA,
                        Motivo = m.motivo,
                        FormaPago = (SolicitudFalta.FormasPago)m.idFormaP,
                        NombreSuplente = string.Join(" ", m.nombreSE, m.apellidoSP, m.apellidoSM),
                    }).ToList();
        }


        public List<DtoSolicitudFalta> ObtenerSolicitudesFaltaSuplenciaPorFecha(int idEmpleado, DateTime? fechaInicial, DateTime? fechaFinal)
        {
            Expression<Func<SolicitudFalta, bool>> filtro = m => m.Activa && m.IdEmpleadoSuplente == idEmpleado;

            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m => m.FechaAplicacion >= fechaInicial.Value, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaAplicacion <= fechaFinal.Value, Expression.AndAlso);

            return (from solicitud in contexto.Set<SolicitudFalta>().Where(filtro)
                    select new
                    {
                        Id = solicitud.Id,
                        nombreE = solicitud.EmpleadoSolicita.Nombre,
                        apellidoP = solicitud.EmpleadoSolicita.ApellidoPaterno,
                        apellidoM = solicitud.EmpleadoSolicita.ApellidoMaterno,
                        area = solicitud.EmpleadoSolicita.Puesto.AreaActual.Nombre,
                        //turno = solicitud.EmpleadoSolicita.Turno.Nombre,
                        fechaA = solicitud.FechaAplicacion,
                        motivo = solicitud.Motivo,
                        idFormaP = solicitud.IdFormaPago,
                        nombreSE = solicitud.IdEmpleadoSuplente.HasValue ? solicitud.Suplente.Nombre : null,
                        apellidoSP = solicitud.IdEmpleadoSuplente.HasValue ? solicitud.Suplente.ApellidoPaterno : null,
                        apellidoSM = solicitud.IdEmpleadoSuplente.HasValue ? solicitud.Suplente.ApellidoMaterno : null,
                    }).ToList().Select(m => new DtoSolicitudFalta
                    {
                        Id = m.Id,
                        NombreEmpleado = string.Join(" ", m.nombreE, m.apellidoP, m.apellidoM),
                        Area = m.area,
                        FechaAplicacion = m.fechaA,
                        Motivo = m.motivo,
                        FormaPago = (SolicitudFalta.FormasPago)m.idFormaP,
                        NombreSuplente = string.Join(" ", m.nombreSE, m.apellidoSP, m.apellidoSM),
                    }).ToList();
        }
    }
}
