﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioFajillas : Repositorio<Fajilla>, IRepositorioFajillas
    {
        public RepositorioFajillas() : base(new Contextos.SOTContext())
        {
        }

        public List<Fajilla> ObtenerFajillasPorTurno(int idCorte, bool soloAutorizadas)
        {
            if (soloAutorizadas)
                return (from fajilla in contexto.Set<Fajilla>()
                        where fajilla.Activa
                        && fajilla.Autorizada
                        && !fajilla.EsSobrante
                        && fajilla.IdCorteTurno == idCorte
                        select fajilla).ToList();

            return (from fajilla in contexto.Set<Fajilla>()
                    where fajilla.Activa
                    && fajilla.IdCorteTurno == idCorte
                        && !fajilla.EsSobrante
                        && fajilla.IdCorteTurno == idCorte
                    select fajilla).ToList();

        }


        public int ObtenerUltimoNumeroFajilla(int idCorte)
        {
            return (from fajilla in contexto.Set<Fajilla>()
                    where !fajilla.EsSobrante
                    && fajilla.IdCorteTurno == idCorte
                    orderby fajilla.Numero descending
                    select fajilla.Numero).FirstOrDefault();
        }


        public List<Fajilla> ObtenerFajillasAutorizadasConMontosPorCorte(int idCorte)
        {
            //.ObtenerElementos(m => m.Activa && m.IdCorteTurno == idCorte && m.Autorizada && !m.EsSobrante)
            return (from fajilla in contexto.Set<Fajilla>()
                    where !fajilla.EsSobrante
                    && fajilla.Autorizada
                    && fajilla.Activa
                    && fajilla.IdCorteTurno == idCorte
                    orderby fajilla.Numero descending
                    select new
                    {
                        fajilla,
                        montos = fajilla.MontosFajilla.Where(m => m.Activa)
                    }).ToList().Select(m => m.fajilla).ToList();
        }

        public Fajilla ObtenerFajillaCargada(int idFajilla)
        {
            return (from fajilla in contexto.Set<Fajilla>()
                    where fajilla.Activa
                    && fajilla.Id == idFajilla
                    select new
                    {
                        fajilla,
                        montosFajilla = fajilla.MontosFajilla.Where(m => m.Activa)
                    }).ToList().Select(m => m.fajilla).FirstOrDefault();
        }

        public Fajilla ObtenerSobrantePorTurno(int idCorte)
        {
            return (from fajilla in contexto.Set<Fajilla>()
                    where fajilla.Activa
                    && fajilla.IdCorteTurno == idCorte
                    && fajilla.EsSobrante
                    select new
                    {
                        fajilla,
                        montosFajilla = fajilla.MontosFajilla.Where(m => m.Activa)
                    }).ToList().Select(m=>m.fajilla).FirstOrDefault();
        }

        public List<Fajilla> ObtenerFajillasYSobranteCargadosNoSincronizados()
        {
            var idEstadoCerrado = (int)CorteTurno.Estados.Cerrado;

            return (from fajilla in contexto.Set<Fajilla>()
                    where fajilla.Activa
                    && !fajilla.Sincronizada
                    && fajilla.CorteTurno.IdEstado == idEstadoCerrado
                    select new
                    {
                        fajilla,
                        montosFajilla = fajilla.MontosFajilla.Where(m => m.Activa)
                    }).ToList().Select(m => m.fajilla).ToList();
        }
    }
}
