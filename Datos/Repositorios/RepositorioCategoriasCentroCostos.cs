﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioCategoriasCentroCostos : Repositorio<CategoriaCentroCostos>, IRepositorioCategoriasCentroCostos
    {
        public RepositorioCategoriasCentroCostos() : base(new Contextos.SOTContext()) { }
    }
}
