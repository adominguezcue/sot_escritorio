﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Dtos;
using System.Data.SqlClient;
using Modelo.Repositorios;
using System.Data;

namespace Datos.Repositorios
{
    public class RepositorioMatriculaAuto : Repositorio<AutomovilRenta>, IRepositorioMatriculaAuto
    {
        public RepositorioMatriculaAuto() : base(new Contextos.SOTContext())
        { }


        public List<AutomovilRenta> ObtieneMatriculasAutos(string matricula = null)
        {
            List<AutomovilRenta> respuesta = new List<AutomovilRenta>();

            if (matricula != null && !matricula.Equals(""))
            {
                var grupomatricula = (from automovilrenta in contexto.Set<AutomovilRenta>()
                                      where automovilrenta.Matricula.Equals(matricula)
                                      group automovilrenta by automovilrenta.Matricula
                                       ).ToList();

                foreach (var group in grupomatricula)
                {
                    bool bandera = true;
                    foreach (AutomovilRenta item in group)
                    {
                        if (bandera == true)
                            respuesta.Add(item);

                        bandera = false;
                    }
                }
            }

            return respuesta;
        }


        public List<DtoHabitacionesxMatricula> ObtieneHabitacionesPorMatricula(string matricula)
        {
            List<DtoHabitacionesxMatricula> respuesta = new List<DtoHabitacionesxMatricula>();

            var vcNumMatricula = new SqlParameter("@vcNumMatricula", matricula);
            vcNumMatricula.SqlDbType = System.Data.SqlDbType.VarChar;
            DataSet dataSet = new DataSet();
            dataSet = contexto.EjecutarComandoSqlSP("SotSchema.SP_ConDatosHabitacionPorMatricula", vcNumMatricula);
            if (dataSet.Tables[0].Rows.Count > 0 && dataSet != null && dataSet.Tables.Count > 0)
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    DtoHabitacionesxMatricula habitacionesxMatricula = new DtoHabitacionesxMatricula();
                    habitacionesxMatricula.Idrenta = Convert.ToInt32(fila["IdRenta"]);
                    habitacionesxMatricula.Habitacion = Convert.ToInt32(fila["Habitacion"]);
                    habitacionesxMatricula.fechaRegistro = DateTime.Parse(fila["FechaRegistro"].ToString());
                    habitacionesxMatricula.PrecioHabitacion = Convert.ToDecimal(fila["PrecioHabi"]);
                    habitacionesxMatricula.PrecioPersonaExtra = Convert.ToDecimal(fila["PrecioPersonaExtra"]);
                    habitacionesxMatricula.PrecioNochesExtra= Convert.ToDecimal(fila["PrecioNoches"]);
                    habitacionesxMatricula.PrecioHorasExtras= Convert.ToDecimal(fila["PrecioHoras"]);
                    habitacionesxMatricula.PrecioPaquetes = Convert.ToDecimal(fila["PrecioPaquetes"]);
                    habitacionesxMatricula.PrecioCortesias= Convert.ToDecimal(fila["PrecioCortesias"]);
                    habitacionesxMatricula.PrecioDescuentos= Convert.ToDecimal(fila["PrecioDescuentos"]);
                    habitacionesxMatricula.EsReservacion = Convert.ToBoolean(fila["Esreservacion"]);
                    respuesta.Add(habitacionesxMatricula);
                }
            }
            return respuesta;
        }



        public List<DtTotalComandasxRenta> ObtieneComandasPorRenta(int idrenta)
        {
            List<DtTotalComandasxRenta> respuesta = new List<DtTotalComandasxRenta>();

            var virenta = new SqlParameter("@viRentaId", idrenta);
            virenta.SqlDbType = System.Data.SqlDbType.Int;
            DataSet dataSet = new DataSet();
            dataSet = contexto.EjecutarComandoSqlSP("SotSchema.SP_CoComandasPorRenta", virenta);
            if (dataSet != null && dataSet.Tables.Count>0 )
            {
                foreach (DataRow fila in dataSet.Tables[0].Rows)
                {
                    DtTotalComandasxRenta totalcomandasrenta = new DtTotalComandasxRenta();

                    totalcomandasrenta.Idrenta = Convert.ToInt32(fila["idrenta"]);
                    totalcomandasrenta.Totalcomandas = Convert.ToInt32(fila["comandas"]);
                    totalcomandasrenta.TotalPrecioComandas = Convert.ToDecimal(fila["total"]);
                    respuesta.Add(totalcomandasrenta);
                }
            }

            return respuesta;
        }
    }
}
