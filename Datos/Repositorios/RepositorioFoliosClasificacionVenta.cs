﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioFoliosClasificacionVenta : Repositorio<FolioClasificacionVenta>, IRepositorioFoliosClasificacionVenta
    {
        public RepositorioFoliosClasificacionVenta() : base(new Contextos.SOTContext()) { }

    }
}
