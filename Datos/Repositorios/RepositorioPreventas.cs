﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPreventas : Repositorio<Preventa>, IRepositorioPreventas
    {
        public RepositorioPreventas() : base(new Contextos.SOTContext()) { }

        //public int? ObtenerUltimoTicket()
        //{
        //    return (from Preventa in contexto.Set<Preventa>()
        //            orderby Preventa.Ticket descending
        //            select Preventa.Ticket).FirstOrDefault();
        //}
    }
}
