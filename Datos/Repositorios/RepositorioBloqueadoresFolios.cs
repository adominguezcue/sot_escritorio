﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Dtos;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;
using Datos.Nucleo.Contextos;

namespace Datos.Repositorios
{
    public class RepositorioBloqueadoresFolios : Repositorio<BloqueadorFolios>, IRepositorioBloqueadoresFolios
    {
        public RepositorioBloqueadoresFolios() : base(new Contextos.SOTContext())
        {
        }
    }
}
