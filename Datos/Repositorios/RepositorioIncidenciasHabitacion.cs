﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioIncidenciasHabitacion : Repositorio<IncidenciaHabitacion>, IRepositorioIncidenciasHabitacion
    {
        public RepositorioIncidenciasHabitacion() : base(new Contextos.SOTContext()) { }

        public List<DtoIncidenciaHabitacion> ObtenerIncidenciasFiltradas(DateTime? fechaInicial, DateTime? fechaFinal, int? idHabitacion, int? idEmpleado)
        {
            Expression<Func<IncidenciaHabitacion, bool>> filtro = m => m.Activa;

            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m=>m.FechaCreacion >= fechaInicial, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFinal, Expression.AndAlso);
            if (idHabitacion.HasValue)
                filtro = filtro.Compose(m => m.IdHabitacion == idHabitacion, Expression.AndAlso);
            if (idEmpleado.HasValue)
                filtro = filtro.Compose(m => m.IdEmpleadoReporta == idEmpleado, Expression.AndAlso);

            var resultado = (from incidencia in contexto.Set<IncidenciaHabitacion>().Where(filtro)
                             select new
                             {
                                 incidencia,
                                 NombreE = incidencia.EmpleadoReporta.Nombre,
                                 ApellidoP = incidencia.EmpleadoReporta.ApellidoPaterno,
                                 ApellidoM = incidencia.EmpleadoReporta.ApellidoMaterno,
                                 Habitacion = incidencia.Habitacion.NumeroHabitacion,
                                 TipoH = incidencia.Habitacion.TipoHabitacion.Descripcion
                             }).ToList().Select(m => new DtoIncidenciaHabitacion
                             {
                                 Id = m.incidencia.Id,
                                 EstaCancelada = m.incidencia.Cancelada,
                                 Nombre = m.incidencia.Nombre,
                                 Descripcion = m.incidencia.Descripcion,
                                 Zonas = m.incidencia.Zonas,
                                 FechaCreacion = m.incidencia.FechaCreacion,
                                 NombreEmpleado = string.Join(" ", m.NombreE, m.ApellidoP, m.ApellidoM),
                                 EtiquetaHabitacion = m.Habitacion
                             }).ToList();

            return resultado;
        }
    }
}
