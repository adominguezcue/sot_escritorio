﻿using System;
using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioAutorizaMinutosVenta : Repositorio<AutorizaMinutosVenta>, IRepositorioAutorizaMinutosVenta
    {
        public RepositorioAutorizaMinutosVenta()  :base (new Contextos.SOTContext()) { }
    }
}
