﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioConfiguracionesFajillas : Repositorio<ConfiguracionFajilla>, IRepositorioConfiguracionesFajillas
    {
        public RepositorioConfiguracionesFajillas() : base(new Contextos.SOTContext())
        {
        }

        public ConfiguracionFajilla ObtenerConfiguracionFajillasCargada()
        {
            return (from configuracion in contexto.Set<ConfiguracionFajilla>()
                    where configuracion.Activa
                    select new
                    {
                        configuracion,
                        montos = configuracion.MontosConfiguracionFajilla.Where(m => m.Activa),
                        monedas = configuracion.MonedasExtranjeras.Where(m => m.Activa)
                    }).ToList().Select(m => m.configuracion).FirstOrDefault();
        }
    }
}
