﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioConceptosGastos : Repositorio<ConceptoGasto>, IRepositorioConceptosGastos
    {
        public RepositorioConceptosGastos() : base(new Contextos.SOTContext()) { }

        public List<ConceptoGasto> ObtenerConceptosPagablesEnCaja()
        {
            var resultado = (from conceptoG in contexto.Set<ConceptoGasto>()
                             where conceptoG.Activo
                             && conceptoG.PagableEnCaja
                             select new
                             {
                                 conceptoG,
                                 NombreCentro = conceptoG.CentroCostos.Nombre
                             }).ToList();

            resultado.ForEach(m => m.conceptoG.NombreCentroCostosTmp = m.NombreCentro);

            return resultado.Select(m => m.conceptoG).ToList();
        }

        public List<ConceptoGasto> ObtenerConceptosActivos()
        {
            var resultado = (from conceptoG in contexto.Set<ConceptoGasto>()
                             where conceptoG.Activo
                             select new
                             {
                                 conceptoG,
                                 NombreCentro = conceptoG.CentroCostos.Nombre
                             }).ToList();

            resultado.ForEach(m => m.conceptoG.NombreCentroCostosTmp = m.NombreCentro);

            return resultado.Select(m => m.conceptoG).ToList();
        }


        //public decimal ObtenerTotalTopesCentro(int idCentroCostos)
        //{
        //    return (from conceptoG in contexto.Set<ConceptoGasto>()
        //            where conceptoG.Activo
        //            select conceptoG.TopePresupuestal).ToList().Sum(m => m);
        //}


        public ConceptoGasto ObtenerConceptoConRestante(int idConceptoGasto, int? idGasto)
        {
            Expression<Func<Gasto, bool>> filtro = m => m.Activo;

            if (idGasto.HasValue)
                filtro = filtro.Compose(m => m.Id != idGasto, Expression.AndAlso);

            //var filtro2 = filtro.Compile();

            var resultado = (from concepto in contexto.Set<ConceptoGasto>()
                             where concepto.Activo && concepto.Id == idConceptoGasto
                             select new
                             {
                                 concepto,
                                 topesGastos = concepto.Gastos.AsQueryable().Where(filtro).Select(m => m.Valor)
                             }).ToList().FirstOrDefault();

            if (resultado == null)
                return null;

            resultado.concepto.PresupuestoRestanteTmp = resultado.topesGastos.ToList().Sum();

            return resultado.concepto;
        }
    }
}
