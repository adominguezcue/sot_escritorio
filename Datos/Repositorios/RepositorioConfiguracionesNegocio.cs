﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioConfiguracionesNegocio : Repositorio<ConfiguracionTarifa>, IRepositorioConfiguracionesNegocio
    {
        public RepositorioConfiguracionesNegocio() : base(new Contextos.SOTContext())
        {
        }
    }
}
