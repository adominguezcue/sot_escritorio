﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioVentas : Repositorio<Venta>, IRepositorioVentas
    {
        public RepositorioVentas() : base(new Contextos.SOTContext()) { }

        //public int? ObtenerUltimoTicket()
        //{
        //    return (from venta in contexto.Set<Venta>()
        //            orderby venta.Ticket descending
        //            select venta.Ticket).FirstOrDefault();
        //}

        public List<Venta> ObtenerVentasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
#if DEBUG
            Expression<Func<Venta, bool>> filtro = m => m.Correcta;
#else
            Expression<Func<Venta, bool>> filtro = m => m.Activa && m.Correcta;
#endif
            if (ordenTurno.HasValue)
            {
                var idsTurnos = (from turno in contexto.Set<CorteTurno>()
                               where turno.ConfiguracionTurno.Orden == ordenTurno
                               select turno.Id).ToList();

                filtro = filtro.Compose(m => idsTurnos.Contains(m.IdCorteTurno), Expression.AndAlso);
            }

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio, Expression.AndAlso);

            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin, Expression.AndAlso);

            var resultado = (from venta in contexto.Set<Venta>().Where(filtro)
                             select new
                             {
                                 venta,
                                 folio = venta.CorteTurno.NumeroCorte
                             }).ToList();

            resultado.ForEach(m => m.venta.FolioCorteTmp = m.folio);

            return resultado.Select(m => m.venta).ToList();
        }

        public List<VW_VentaCorrecta> ObtenerVentasCorrectasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            Expression<Func<VW_VentaCorrecta, bool>> filtro = m => true;

            if (ordenTurno.HasValue)
            {
                var idsTurnos = (from turno in contexto.Set<CorteTurno>()
                               where turno.ConfiguracionTurno.Orden == ordenTurno
                               select turno.NumeroCorte).ToList();

                filtro = filtro.Compose(m => idsTurnos.Contains(m.NumeroCorte), Expression.AndAlso);
            }

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio, Expression.AndAlso);

            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin, Expression.AndAlso);

            return contexto.Set<VW_VentaCorrecta>().Where(filtro).ToList();
        }


        public List<DtoResumenCancelaciones> SP_ObtenerCancelacionesTurno(int idCorteTurno)
        {
            SqlParameter p_idCorteTurno = new SqlParameter("@idCorteTurno", idCorteTurno);
            p_idCorteTurno.SqlDbType = System.Data.SqlDbType.Int;

            return contexto.ConsultaSql<DtoResumenCancelaciones>("EXEC [SotSchema].[SP_ObtenerCancelacionesTurno] @idCorteTurno", p_idCorteTurno);
        }
    }
}
