﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioOcupacionesMesa : Repositorio<OcupacionMesa>, IRepositorioOcupacionesMesa
    {
        public RepositorioOcupacionesMesa() : base(new Contextos.SOTContext())
        {
        }

        public OcupacionMesa ObtenerOcupacionActualPorMesa(int idMesa)
        {
            return (from ocupacion in contexto.Set<OcupacionMesa>()
                    where ocupacion.Activa
                    && ocupacion.IdMesa == idMesa
                    select new
                    {
                        renta = ocupacion,
                        pagos = ocupacion.PagosOcupacionMesa.Where(m => m.Activo),
                        mesero = ocupacion.Mesero
                        //comandas = renta.Comandas.Where(m=>m.Activo)
                    }).ToList().Select(m => m.renta).FirstOrDefault();
        }
        public OcupacionMesa ObtenerParaCancelar(int idOcupacion)
        {
            var idEstadoPorPagar = (int)Comanda.Estados.PorPagar;

            return (from ocupacion in contexto.Set<OcupacionMesa>()
                    where ocupacion.Activa
                        //&& comanda.IdEstado == idEstadoElaborada
                    && ocupacion.Id == idOcupacion
                    select new
                    {
                        ocupacion,
                        ordenes = ocupacion.OrdenesRestaurante.Where(m => m.Activo || m.IdEstado == idEstadoPorPagar)
                    }).ToList().Select(m => m.ocupacion).FirstOrDefault();
        }

        public OcupacionMesa ObtenerParaCobrar(int idMesa)
        {
            var idEstadoEntregadoC = (int)Comanda.Estados.PorPagar;
            var idEstadoArtEnt = (int)ArticuloComanda.Estados.Entregado;

            return (from ocupacion in contexto.Set<OcupacionMesa>()
                    where ocupacion.Activa
                        //&& comanda.IdEstado == idEstadoElaborada
                    && ocupacion.IdMesa == idMesa
                    select new
                    {
                        ocupacion,
                        pagos = ocupacion.PagosOcupacionMesa.Where(m => m.Activo),
                        mesero = ocupacion.Mesero,
                        ordenes = ocupacion.OrdenesRestaurante.Where(m => m.IdEstado == idEstadoEntregadoC),
                        articulosComanda = ocupacion.OrdenesRestaurante.Where(m => m.IdEstado == idEstadoEntregadoC).SelectMany(m => m.ArticulosOrdenRestaurante).Where(m => m.IdEstado == idEstadoArtEnt)
                    }).ToList().Select(m => m.ocupacion).FirstOrDefault();
        }

        public OcupacionMesa ObtenerOcupacionFinalizadaConDetalles(int idOcupacion)
        {
            var idEstadoEntregadoC = (int)Comanda.Estados.Cobrada;
            var idEstadoArtEnt = (int)ArticuloComanda.Estados.Entregado;

            return (from ocupacion in contexto.Set<OcupacionMesa>()
                    where ocupacion.Id == idOcupacion
                    && ocupacion.Id == idOcupacion
                    select new
                    {
                        ocupacion,
                        ordenes = ocupacion.OrdenesRestaurante.Where(m => m.IdEstado == idEstadoEntregadoC),
                        articulosO = ocupacion.OrdenesRestaurante.Where(m => m.IdEstado == idEstadoEntregadoC).
                                    SelectMany(m=>m.ArticulosOrdenRestaurante).Where(m=>m.IdEstado == idEstadoArtEnt),
                        //articulos = ocupacion.OrdenesRestaurante.Where(m => m.IdEstado == idEstadoEntregadoC).
                        //            SelectMany(m => m.ArticulosOrdenRestaurante).Where(m => m.IdEstado == idEstadoArtEnt).Select(m=>m.Articulo),
                    }).ToList().Select(m => m.ocupacion).FirstOrDefault();
        }


        public DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion)
        {
            return (from ventaR in contexto.Set<PagoOcupacionMesa>()
                    where ventaR.Transaccion == transaccion
                    select ventaR.OcupacionMesa.DatosFiscales).FirstOrDefault();
        }


        public string ObtenerNumeroMesaPorTransaccion(string numeroTransaccion)
        {
            return (from ocupacion in contexto.Set<OcupacionMesa>()
                    where ocupacion.Transaccion == numeroTransaccion
                    select ocupacion.Mesa.Clave).FirstOrDefault();
        }
    }
}
