﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioObjetosOlvidados : Repositorio<ObjetoOlvidado>, IRepositorioObjetosOlvidados
    {
        public RepositorioObjetosOlvidados() : base(new Contextos.SOTContext()) { }

        public List<DtoObjetoOlvidado> ObtenerObjetosOlvidadosFiltrados(DateTime? fechaInicial, DateTime? fechaFinal, int? idHabitacion, int? idEmpleado)
        {
            Expression<Func<ObjetoOlvidado, bool>> filtro = m => m.Activo;

            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m=>m.FechaCreacion >= fechaInicial, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFinal, Expression.AndAlso);
            if (idHabitacion.HasValue)
                filtro = filtro.Compose(m => m.IdHabitacion == idHabitacion, Expression.AndAlso);
            if (idEmpleado.HasValue)
                filtro = filtro.Compose(m => m.IdEmpleadoReporta == idEmpleado, Expression.AndAlso);

            var resultado = (from incidencia in contexto.Set<ObjetoOlvidado>().Where(filtro)
                             select new
                             {
                                 incidencia,
                                 NombreE = incidencia.EmpleadoReporta.Nombre,
                                 ApellidoP = incidencia.EmpleadoReporta.ApellidoPaterno,
                                 ApellidoM = incidencia.EmpleadoReporta.ApellidoMaterno,
                                 Habitacion = incidencia.Habitacion.NumeroHabitacion,
                                 TipoH = incidencia.Habitacion.TipoHabitacion.Descripcion
                             }).ToList().Select(m => new DtoObjetoOlvidado
                             {
                                 Id = m.incidencia.Id,
                                 EsCancelado = m.incidencia.Cancelado,
                                 Nombre = m.incidencia.Nombre,
                                 Descripcion = m.incidencia.Descripcion,
                                 FechaCreacion = m.incidencia.FechaCreacion,
                                 NombreEmpleado = string.Join(" ", m.NombreE, m.ApellidoP, m.ApellidoM),
                                 EtiquetaHabitacion = m.Habitacion + " - " + m.TipoH
                             }).ToList();

            return resultado;
        }
    }
}
