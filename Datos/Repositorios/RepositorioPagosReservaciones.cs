﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPagosReservaciones : Repositorio<PagoReservacion>, IRepositorioPagosReservaciones
    {
        public RepositorioPagosReservaciones() : base(new Contextos.SOTContext()) { }

        public List<PagoReservacion> ObtenerPagosPorTransaccion(string transaccion, bool incluirCancelada)//DateTime? fechaFin)
        {
            //if (fechaFin.HasValue)
            //    return (from pago in contexto.Set<PagoReservacion>()
            //            where (pago.Activo || (pago.FechaEliminacion.HasValue && pago.FechaEliminacion > fechaFin))
            //            && pago.Transaccion == transaccion
            //            select pago).ToList();

            if (incluirCancelada)
                return (from pago in contexto.Set<PagoReservacion>()
                        where pago.Transaccion == transaccion
                        select pago).ToList();

            return (from pago in contexto.Set<PagoReservacion>()
                    where pago.Activo && pago.Transaccion == transaccion
                    select pago).ToList();
        }


        public List<PagoReservacion> ObtenerPagosPorCodigosReserva(List<string> codigosReservaciones)
        {
            return (from reservacion in contexto.Set<Reservacion>()
                    where codigosReservaciones.Contains(reservacion.CodigoReserva)
                    select reservacion.PagosReservacion.Where(m => m.Activo || m.FechaEliminacion >= reservacion.FechaEliminacion))
                    .ToList().SelectMany(m => m).ToList();
                    
        }
    }
}
