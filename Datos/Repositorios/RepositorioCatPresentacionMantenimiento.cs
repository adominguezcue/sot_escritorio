﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;
using Modelo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioCatPresentacionMantenimiento : Repositorio<CatPresentacionMantenimiento>, IRepositorioCatPresentacionMantenimiento
    {
        public RepositorioCatPresentacionMantenimiento() : base(new Contextos.SOTContext()) { }

        public List<CatPresentacionMantenimiento> ObtienePresentacionMantenimiento()
        {
            return (from catlogomantenimeinto in contexto.Set<CatPresentacionMantenimiento>()
                    where catlogomantenimeinto.Activa
                    select new { catlogomantenimeinto }
                    ).ToList().Select(m => m.catlogomantenimeinto).ToList();
                    
        }
    }
}
