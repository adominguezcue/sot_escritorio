﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Datos.Nucleo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioReportesMatriculas : Repositorio<ReporteMatricula>, IRepositorioReportesMatriculas
    {
        public RepositorioReportesMatriculas() : base(new Contextos.SOTContext())
        {
        }

        public List<ReporteMatricula> ObtenerReportesMatricula(string matricula)
        {
            matricula = matricula.Trim().ToUpper();

            return contexto.Set<ReporteMatricula>().Where(m=>m.Matricula.Trim().ToUpper().Equals(matricula)).ToList();
        }
    }
}
