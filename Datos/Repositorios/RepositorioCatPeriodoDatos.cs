﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioCatPeriodoDatos : Repositorio<CatPeriodoDatos>, IRepositorioCatPeriodo
    {
        public RepositorioCatPeriodoDatos() : base(new Contextos.SOTContext()) { }

        public List<CatPeriodoDatos> ObtienePeriodosFiltrados()
        {
            return (from peridosactivos in contexto.Set<CatPeriodoDatos>()
                    where peridosactivos.Activa == true
                    select new
                    {
                        peridosactivos
                    }).ToList().Select(m => m.peridosactivos).ToList();
        }
    }
}
