﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioPropinas: Repositorio<Propina>, IRepositorioPropinas
    {
        public RepositorioPropinas() : base(new Contextos.SOTContext())
        {
        }

        public Dictionary<Propina.TiposPropina, decimal> ObtenerTotalPropinasPorTipo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<Propina, bool>> filtro = m=>m.Activo;

            if(fechaInicio.HasValue)
                filtro = filtro.Compose(m=>m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if(fechaFin.HasValue)
                filtro = filtro.Compose(m=>m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from propina in contexto.Set<Propina>().Where(filtro)
                    group propina by propina.IdTipoPropina into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (Propina.TiposPropina)m.Key, m => m.valor);
        }

        public Dictionary<TiposTarjeta, decimal> ObtenerTotalPropinasPorTarjeta(DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<Propina, bool>> filtro = m=>m.Activo;

            if(fechaInicio.HasValue)
                filtro = filtro.Compose(m=>m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if(fechaFin.HasValue)
                filtro = filtro.Compose(m=>m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from propina in contexto.Set<Propina>().Where(filtro)
                    group propina by propina.IdTipoTarjeta into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposTarjeta)m.Key, m => m.valor);
        }

        public List<Propina> ObtenerPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, params Propina.TiposPropina[] tipos)
        {
            Expression<Func<Propina, bool>> filtro = m => m.Activo;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            if (tipos.Length > 0) 
            { 
                var idsTipos = tipos.Select(m=> (int)m).ToList();

                filtro = filtro.Compose(m => idsTipos.Contains(m.IdTipoPropina), Expression.AndAlso);
            }

            return (from propina in contexto.Set<Propina>().Where(filtro)
                    select new 
                    { 
                        propina,
                        mesero = propina.Empleado,
                    }).ToList().Select(m => m.propina).ToList();
        }


        public List<Modelo.Dtos.DtoResumenPropina> ObtenerResumenesPorTransaccion(string transaccion)
        {
            return (from propina in contexto.Set<Propina>()
                    where propina.Activo
                    && propina.Transaccion.Equals(transaccion)
                    select new
                    {
                        propina.IdTipoTarjeta,
                        propina.IdTipoPropina,
                        propina.Valor,
                        propina.Referencia,
                        propina.NumeroTarjeta,
                        propina.Transaccion
                    }).ToList().Select(m => new Modelo.Dtos.DtoResumenPropina
                    {
                        NumeroTarjeta = m.NumeroTarjeta,
                        Referencia = m.Referencia,
                        TipoPropina = (Modelo.Entidades.Propina.TiposPropina)m.IdTipoPropina,
                        TipoTarjeta = (TiposTarjeta)m.IdTipoTarjeta,
                        Transaccion = m.Transaccion,
                        Valor = m.Valor
                    }).ToList();
        }
    }
}
