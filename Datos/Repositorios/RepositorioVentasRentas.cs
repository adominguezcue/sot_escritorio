﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioVentasRentas : Repositorio<VentaRenta>, IRepositorioVentasRentas
    {
        public RepositorioVentasRentas() : base(new Contextos.SOTContext()) { }

        public string ObtenerNumeroHabitacionPorTransaccion(string numeroTransaccion)
        {
            return (from venta in contexto.Set<VentaRenta>()
                    where venta.Transaccion == numeroTransaccion
                    select venta.Renta.Habitacion.NumeroHabitacion).FirstOrDefault();
        }


        public string ObtenerNumeroHabitacionPorIdComanda(int idComanda)
        {
            return (from comanda in contexto.Set<Comanda>()
                    where comanda.Id == idComanda
                    select comanda.Renta.Habitacion.NumeroHabitacion).FirstOrDefault();
        }


        public string ObtenerNumeroHabitacionPorIdRenta(int idRenta)
        {
            return (from comanda in contexto.Set<Renta>()
                    where comanda.Id == idRenta
                    select comanda.Habitacion.NumeroHabitacion).FirstOrDefault();
        }

        public VentaRenta ObtenerVentaRentaPendienteParaCancelar(int idVentaRenta)
        {
            var resultado = (from ventaRenta in contexto.Set<VentaRenta>()
                             where ventaRenta.Activo
                             && !ventaRenta.Cobrada
                             && ventaRenta.Id == idVentaRenta
                             select new
                             {
                                 ventaRenta = ventaRenta,
                                 preventas = ventaRenta.Preventa,
                                 montosNoR = ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo),
                                 detalles = ventaRenta.DetallesPago.Where(m => m.Activo),
                                 extensiones = ventaRenta.Extensiones.Where(m => m.Activa),
                                 personasExtra = ventaRenta.PersonasExtra.Where(m => m.Activa),
                                 paquetes = ventaRenta.PaquetesRenta.Where(m => m.Activo),
                                 pagos = ventaRenta.Pagos.Where(m => m.Activo),
                             }).ToList().FirstOrDefault();

            if (resultado == null)
                return null;

            return resultado.ventaRenta;
        }

        public List<DtoDatosPersonaExtrasRepCorteTurno> ObtienePersonasExtrasPorCorte(int folioCorte)
        {
            List<DtoDatosPersonaExtrasRepCorteTurno> respuesta = new List<DtoDatosPersonaExtrasRepCorteTurno>();
            var resultado = (from personasextras in contexto.Set<PersonaExtra>()
                             join VentaRentas in contexto.Set<VentaRenta>() on personasextras.IdVentaRenta equals VentaRentas.Id
                             join Ventas in contexto.Set<Venta>() on VentaRentas.Transaccion equals Ventas.Transaccion
                             join Ccturno in contexto.Set<CorteTurno>() on Ventas.IdCorteTurno equals Ccturno.Id
                             where Ccturno.NumeroCorte == folioCorte && personasextras.Precio !=0
                             select new 
                             {
                                 personasextras.Precio
                             }).ToList();

            foreach (var grupo in resultado.GroupBy(m => m.Precio).OrderBy(m => m.Key))
            {
                respuesta.Add(new DtoDatosPersonaExtrasRepCorteTurno
                {
                    Cantidad = grupo.Count(), //grupo.Sum(m => m.Cantidad),//Count(),
                    Precio = grupo.Key// * (1 + configGlobal.Iva_CatPar)
                });
            }


            return respuesta;
        }

    }
}
