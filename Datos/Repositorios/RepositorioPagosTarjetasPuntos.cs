﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPagosTarjetasPuntos : Repositorio<PagoTarjetaPuntos>, IRepositorioPagosTarjetasPuntos
    {
        public RepositorioPagosTarjetasPuntos() : base(new Contextos.SOTContext()) { }

        public List<PagoTarjetaPuntos> ObtenerPagosPorTransaccion(string transaccion)
        {
            return (from pago in contexto.Set<PagoTarjetaPuntos>()
                    where pago.Activo && pago.Transaccion == transaccion
                    select pago).ToList();
        }
    }
}
