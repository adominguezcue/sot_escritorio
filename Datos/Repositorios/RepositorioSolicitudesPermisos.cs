﻿using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioSolicitudesPermisos : Repositorio<SolicitudPermiso>, IRepositorioSolicitudesPermisos
    {
        public RepositorioSolicitudesPermisos() : base(new Contextos.SOTContext()) { }

        public List<DtoSolicitudPermiso> ObtenerSolicitudesPermiso(string nombre, string apellidoPaterno, string apellidoMaterno, DateTime? fechaInicial, DateTime? fechaFinal)
        {
            Expression<Func<SolicitudPermiso, bool>> filtro = m => m.Activa;

            if (!string.IsNullOrWhiteSpace(nombre))
            {
                var nombreU = nombre.Trim().ToUpper();

                filtro = filtro.Compose(m => m.Empleado.Nombre.ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (!string.IsNullOrWhiteSpace(apellidoPaterno))
            {
                var apellidoPU = apellidoPaterno.Trim().ToUpper();

                filtro = filtro.Compose(m => m.Empleado.ApellidoPaterno.ToUpper().Contains(apellidoPU), Expression.AndAlso);
            }
            if (!string.IsNullOrWhiteSpace(apellidoMaterno))
            {
                var qapellidoMU = apellidoMaterno.Trim().ToUpper();

                filtro = filtro.Compose(m => m.Empleado.ApellidoMaterno.ToUpper().Contains(qapellidoMU), Expression.AndAlso);
            }
            if (fechaInicial.HasValue)
                filtro = filtro.Compose(m => m.FechaAplicacion >= fechaInicial.Value, Expression.AndAlso);
            if (fechaFinal.HasValue)
                filtro = filtro.Compose(m => m.FechaAplicacion <= fechaFinal.Value, Expression.AndAlso);

            return (from solicitud in contexto.Set<SolicitudPermiso>().Where(filtro)
                    select new
                    {
                        Id = solicitud.Id,
                        nombreE = solicitud.Empleado.Nombre,
                        apellidoP = solicitud.Empleado.ApellidoPaterno,
                        apellidoM = solicitud.Empleado.ApellidoMaterno,
                        area = solicitud.Empleado.Puesto.AreaActual.Nombre,
                        fechaAplicacion = solicitud.FechaAplicacion,
                        fechaCreacion = solicitud.FechaCreacion,
                        motivo = solicitud.Motivo,
                        esSalida = solicitud.EsSalida,
                        anticipada = solicitud.SolicitudAnticipada
                    }).ToList().Select(m => new DtoSolicitudPermiso
                    {
                        Id = m.Id,
                        NombreEmpleado = string.Join(" ", m.nombreE, m.apellidoP, m.apellidoM),
                        Area = m.area,
                        FechaAplicacion = m.fechaAplicacion,
                        FechaCreacion = m.fechaCreacion,
                        Motivo = m.motivo,
                        EsSalida = m.esSalida,
                        SolicitudAnticipada = m.anticipada
                    }).ToList();
        }
    }
}
