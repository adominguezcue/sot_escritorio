﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Dtos;
using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using Transversal.Extensiones;
using System.Linq.Expressions;

namespace Datos.Repositorios
{
    public class RepositorioCortesiasCancelaciones :  Repositorio<VW_ConsCancelacionesYCortesias>, IRepositorioCortesiaCancelaciones
    {
        public RepositorioCortesiasCancelaciones (): base(new Contextos.SOTContext())
        { }


        public List<DtoConceptoCortesiaCancelacion> ObtieneConceptos()
        {
            List<DtoConceptoCortesiaCancelacion> respuesta = new List<DtoConceptoCortesiaCancelacion>();
            var concepto = (from m in contexto.Set<VW_ConsCancelacionesYCortesias>() select m.Concepto).Distinct().ToList();
            foreach (var item in concepto)
            {
                DtoConceptoCortesiaCancelacion dto = new DtoConceptoCortesiaCancelacion();
                dto.Concepto = item;//.Key.ToString();
                respuesta.Add(dto);
            }
            return respuesta;
        }

        public List<DtoCortesiasCancelaciones> ObtieneCortesiasCancelaciones(string concepto, DateTime fechainicio, DateTime fechafin)
        {
            List<DtoCortesiasCancelaciones> respuesta = new List<DtoCortesiasCancelaciones>();
            Expression<Func<VW_ConsCancelacionesYCortesias, bool>> filtro = null;
            if (!string.IsNullOrWhiteSpace(concepto))
            {
                if (filtro == null)
                    filtro = m => m.Concepto == concepto;
                else
                    filtro = filtro.Compose(m => m.Concepto == concepto, Expression.AndAlso);
            }
            if (fechainicio != null)
            {
                if (filtro == null)
                    filtro = m => m.FechaModificacion >= fechainicio;
                else
                    filtro = filtro.Compose(m => m.FechaModificacion >= fechainicio, Expression.AndAlso);
            }
            if(fechafin != null)
            {
                if (filtro == null)
                    filtro = m => m.FechaModificacion <= fechafin;
                else
                    filtro = filtro.Compose(m => m.FechaModificacion <= fechafin, Expression.AndAlso);
            }

            respuesta = (from m in contexto.Set<VW_ConsCancelacionesYCortesias>().Where(filtro)
                                 select new DtoCortesiasCancelaciones()
                                 {
                                     idVentaCorrecta= m.idVentaCorrecta,
                                     Ticket = m.Ticket,
                                     FechaModificacion = m.FechaModificacion,
                                     Precio = m.Precio,
                                     Concepto = m.Concepto,
                                     Habitacion = m.Habitacion,
                                     MotivoCancelacion = m.MotivoCancelacion,
                                     UsuarioCancelacion = m.UsuarioCancelo,
                                     Turno= m.Turno
                                 }).ToList();

                return respuesta;
        }

        public List<VW_VentaCorrecta> ObtieneVentaCorrecta(int ticketseleccionado, string tipoid)
        {
            List<VW_VentaCorrecta> respuesta = new List<VW_VentaCorrecta>();

            Expression<Func<VW_VentaCorrecta, bool>> filtro = null;
            if(!string.IsNullOrWhiteSpace(tipoid))
            {
                switch(tipoid)
                {
                    case "Comanda Cancelada":
                        if (filtro == null)
                            filtro = m => m.IdComanda == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.IdComanda == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Consumo Interno Cancelado":
                        if (filtro == null)
                            filtro = m => m.IdConsumoInterno == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.IdConsumoInterno == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Cortesia Comandas":
                        if (filtro == null)
                            filtro = m => m.Id == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.Id == ticketseleccionado, Expression.AndAlso);
                        break;

                    case "Cortesia Habitacion":
                        if (filtro == null)
                            filtro = m => m.Id == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.Id == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Cortesia Restaurante":
                        if (filtro == null)
                            filtro = m => m.Id == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.Id == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Orden Restaurante Cancelado":
                        if (filtro == null)
                            filtro = m => m.IdOcupacionMesa == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.IdOcupacionMesa == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Ordene Taxi cancelada":
                        if (filtro == null)
                            filtro = m => m.IdOrdenTaxi == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.IdOrdenTaxi == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Renta de Habitación Cancelada":
                        if (filtro == null)
                            filtro = m => m.Id == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.Id == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Reservación Cancelada":
                        if (filtro == null)
                            filtro = m => m.Id == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.Id == ticketseleccionado, Expression.AndAlso);
                        break;
                    case "Comanda Cambios":
                        if (filtro == null)
                            filtro = m => m.IdComanda == ticketseleccionado;
                        else
                            filtro = filtro.Compose(m => m.Id == ticketseleccionado, Expression.AndAlso);
                        break;
                    default:
                            if (filtro == null)
                                filtro = m => m.Id == ticketseleccionado;
                            else
                                filtro = filtro.Compose(m => m.Id == ticketseleccionado, Expression.AndAlso);
                        break;
                }

            }

            return contexto.Set<VW_VentaCorrecta>().Where(filtro).ToList();
        }

    }
}
