﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;

namespace Datos.Repositorios
{
    public class RepositorioMedicionesMantenimiento : Repositorio<MedicionMantenimiento>, IRepositorioMedicionesMantenimiento
    {
        public RepositorioMedicionesMantenimiento() : base(new Contextos.SOTContext()) { }


        public List<MedicionMantenimiento> ObtieneLecturasActivas()
        {
            List<MedicionMantenimiento> respuesta = new List<MedicionMantenimiento>();

            respuesta = (from mantenimientos in contexto.Set<MedicionMantenimiento>()
                         where mantenimientos.Activa
                         select new
                         {
                             mantenimientos
                         }
                         ).Select(m => m.mantenimientos).ToList();

            return respuesta;
        }


        public List<MedicionMantenimiento> ObtieneApuntadoresAnteriores()
        {
            List<MedicionMantenimiento> idAsetear = new List<MedicionMantenimiento>();
            var catlogoactivo = (from catalogoActivo in contexto.Set<CatPresentacionMantenimiento>()
                                 where catalogoActivo.Activa == true
                                 select new
                                 {
                                     catalogoActivo
                                 }).Select(m => m.catalogoActivo).ToList();
            if(catlogoactivo != null && catlogoactivo.Count>0)
            {
                foreach (CatPresentacionMantenimiento catacti in catlogoactivo)
                {
                    var medicioneschidas = (from mediciones in contexto.Set<MedicionMantenimiento>()
                                            where mediciones.Activa == false
                                            && mediciones.EsAnterior == false
                                            && mediciones.CatPresentacionMantenimientoId == catacti.Id
                                            orderby mediciones.FechaCreacion descending
                                            select new
                                            {
                                                mediciones
                                            }).First();

                    idAsetear.Add(medicioneschidas.mediciones);
                }
            }
            return idAsetear;
        }

        public List<MedicionMantenimiento> ObtieneLecturasAnteriores()
        {

           return   (from medicion in contexto.Set<MedicionMantenimiento>()
                         where medicion.EsAnterior==true
                         select new
                         {
                             medicion
                         }
            ).Select(m => m.medicion).ToList();
        }

        public List<DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradas(int? idCorte)
        {
            int idcorteentero = 0;
            if (idCorte.HasValue)
            {
                idcorteentero = Convert.ToInt32(idCorte);
            }

            List<DtoMedicionMantenimientoGestion> respuesta = new List<DtoMedicionMantenimientoGestion>();
            respuesta = (from medicion in contexto.Set<MedicionMantenimiento>()
                         join presentacion in contexto.Set<CatPresentacionMantenimiento>() on medicion.CatPresentacionMantenimientoId equals presentacion.Id
                         join empleado in contexto.Set<Empleado>() on medicion.IdEmpleadoRegistro equals empleado.Id
                         join corteturno in contexto.Set<CorteTurno>() on medicion.IdCorteTurno equals corteturno.Id
                         where /*medicion.Activa == true  &&*/ presentacion.Activa && medicion.IdCorteTurno == idcorteentero
                         select new
                         {
                             NumeroDeCorte = corteturno.NumeroCorte
                             ,fechareg = medicion.FechaCreacion
                             ,NombreEmpleado = empleado.Nombre + " "+ empleado.ApellidoPaterno +" "+ empleado.ApellidoMaterno
                             ,decripcionpresntaciounidad = presentacion.Presentacion
                             ,medicionvalorchido  = medicion.Valor
                             ,catdescripcion = presentacion.Descripcion
                         }
                         ).ToList().Select(m => new DtoMedicionMantenimientoGestion
                         {
                             NumeroCorte = m.NumeroDeCorte
                            ,FechaRegistro = m.fechareg
                            ,NombreEmpleado = m.NombreEmpleado
                            ,Presentacion = m.decripcionpresntaciounidad
                            ,Valorregistrado = m.medicionvalorchido
                            ,DescripcionCatPresentacion = m.catdescripcion

                         }).ToList();

            return respuesta;
        }

        public List<MedicionMantenimiento> ObtieneLecturasPorCorte(int? idCorte)
        {
            List<MedicionMantenimiento> respuesta = new List<MedicionMantenimiento>();
            int idcorteentero = 0;
            if (idCorte.HasValue)
            {
                idcorteentero = Convert.ToInt32(idCorte);
            }
            respuesta = (from medicion in contexto.Set<MedicionMantenimiento>()
                           where medicion.IdCorteTurno == idcorteentero
                         select new
                         {
                             medicion
                         }
             ).Select(m => m.medicion).ToList();

            return respuesta;
        }

        public List<DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradasPorFecha(DateTime? fechaInicio)
        {
            List<DtoMedicionMantenimientoGestion> respuesta = new List<DtoMedicionMantenimientoGestion>();
            var fechaIncio1 = new DateTime();
            fechaIncio1 = DateTime.Today;
            if (fechaInicio.HasValue)
            {
                fechaIncio1 = fechaInicio.Value;
            }
            respuesta = (from medicion in contexto.Set<MedicionMantenimiento>()
                             join presentacion in contexto.Set<CatPresentacionMantenimiento>() on medicion.CatPresentacionMantenimientoId equals presentacion.Id
                             join empleado in contexto.Set<Empleado>() on medicion.IdEmpleadoRegistro equals empleado.Id
                             join corteturno in contexto.Set<CorteTurno>() on medicion.IdCorteTurno equals corteturno.Id
                             where /*medicion.Activa == true  &&*/ presentacion.Activa && medicion.FechaCreacion>= fechaInicio
                             select new
                             {NumeroDeCorte = corteturno.NumeroCorte
                               ,fechareg = medicion.FechaCreacion
                               ,NombreEmpleado = empleado.Nombre + " " + empleado.ApellidoPaterno + " " + empleado.ApellidoMaterno
                               ,decripcionpresntaciounidad = presentacion.Presentacion
                               ,medicionvalorchido = medicion.Valor
                               ,catdescripcion = presentacion.Descripcion
                             }
                    ).ToList().Select(m => new DtoMedicionMantenimientoGestion
                    {
                        NumeroCorte = m.NumeroDeCorte
                        ,FechaRegistro = m.fechareg
                        ,NombreEmpleado = m.NombreEmpleado
                        ,Presentacion = m.decripcionpresntaciounidad
                        ,Valorregistrado = m.medicionvalorchido
                        ,DescripcionCatPresentacion = m.catdescripcion
                    }).ToList();
            

            return respuesta;
        }
    }
}
