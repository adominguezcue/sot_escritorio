﻿using Datos.Nucleo.Repositorios;
using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioConsumosInternosHabitacion : Repositorio<ConsumoInternoHabitacion>, IRepositorioConsumosInternosHabitacion
    {
        public RepositorioConsumosInternosHabitacion() : base(new Contextos.SOTContext()) { }
    }
}
