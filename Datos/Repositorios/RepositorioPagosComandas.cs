﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioPagosComandas : Repositorio<PagoComanda>, IRepositorioPagosComandas
    {
        public RepositorioPagosComandas() : base(new Contextos.SOTContext()) { }

        public List<PagoComanda> ObtenerPagosPorTransaccion(string transaccion)
        {
            return (from pago in contexto.Set<PagoComanda>()
                    where pago.Activo && pago.Transaccion == transaccion
                    select pago).ToList();
        }
    }
}
