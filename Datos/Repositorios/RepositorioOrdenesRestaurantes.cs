﻿using Modelo;
using Modelo.Dtos;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using System.Data.SqlClient;
using Dominio.Nucleo.Entidades;

namespace Datos.Repositorios
{
    public class RepositorioOrdenesRestaurantes : Repositorio<OrdenRestaurante>, IRepositorioOrdenesRestaurantes
    {
        public RepositorioOrdenesRestaurantes() : base(new Contextos.SOTContext())
        {
        }

        public OrdenRestaurante ObtenerParaCobrar(int idOrdenRestaurante)
        {
            return (from orden in contexto.Set<OrdenRestaurante>()
                    where orden.Activo
                    //&& comanda.IdEstado == idEstadoElaborada
                    && orden.Id == idOrdenRestaurante
                    select new
                    {
                        orden,
                        articulosComanda = orden.ArticulosOrdenRestaurante.Where(m => m.Activo)
                    }).ToList().Select(m => m.orden).FirstOrDefault();
        }

        public OrdenRestaurante ObtenerParaCancelar(int idOrdenRestaurante)
        {
            var idEstadoPorPagar = (int)Comanda.Estados.PorPagar;
            var idEstadoCobrada = (int)Comanda.Estados.Cobrada;
            var idEstadoEntregado = (int)ArticuloComanda.Estados.Entregado;

            return (from orden in contexto.Set<OrdenRestaurante>()
                    where (orden.Activo || orden.IdEstado == idEstadoPorPagar || orden.IdEstado == idEstadoCobrada)
                        //&& comanda.IdEstado == idEstadoElaborada
                    && orden.Id == idOrdenRestaurante
                    select new
                    {
                        orden,
                        articulosComanda = orden.ArticulosOrdenRestaurante.Where(m => m.Activo || m.IdEstado == idEstadoEntregado)
                    }).ToList().Select(m => m.orden).FirstOrDefault();
        }

        public OrdenRestaurante ObtenerParaEntregarCliente(int idOrdenRestaurante)
        {
            return (from orden in contexto.Set<OrdenRestaurante>()
                    where orden.Activo
                        //&& comanda.IdEstado == idEstadoElaborada
                    && orden.Id == idOrdenRestaurante
                    select new
                    {
                        orden,
                        ocupacion = orden.OcupacionMesa,
                        articulosComanda = orden.ArticulosOrdenRestaurante.Where(m => m.Activo)
                    }).ToList().Select(m => m.orden).FirstOrDefault();
        }

        public List<OrdenRestaurante> ObtenerOrdenesPendientesCargadas(int idMesa)
        {
            var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar};

            return (from orden in contexto.Set<OrdenRestaurante>()
                    where orden.Activo
                    && orden.OcupacionMesa.IdMesa == idMesa
                    && idsEstadoElaborada.Contains(orden.IdEstado)
                    select new
                    {
                        comanda = orden,
                        articulosComanda = orden.ArticulosOrdenRestaurante.Where(m => m.Activo),
                        //articulos = orden.ArticulosOrdenRestaurante.Where(m => m.Activo).Select(m => m.Articulo),
                        //tipos = orden.ArticulosOrdenRestaurante.Where(m => m.Activo).Select(m => m.Articulo.TipoArticulo),
                        ocupacion = orden.OcupacionMesa,
                        mesa = orden.OcupacionMesa.Mesa
                    }).ToList().Select(m => m.comanda).ToList();
        }


        public OrdenRestaurante ObtenerPorArticulos(int idOrden, List<int> idsArticulosOrden)
        {
            return (from orden in contexto.Set<OrdenRestaurante>()
                    where orden.Activo
                    && orden.Id == idOrden
                    && orden.ArticulosOrdenRestaurante.Count(m => m.Activo && idsArticulosOrden.Contains(m.Id)) == idsArticulosOrden.Count
                    //&& comanda.IdEstado == idEstadoElaborada
                    select new
                    {
                        comanda = orden,
                        articulosComanda = orden.ArticulosOrdenRestaurante.Where(m => m.Activo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }

        //public List<DtoArticuloPrepararConsulta> ObtenerArticulosPorPreparar()
        //{
        //    var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;

        //    return (from articuloOrden in contexto.Set<ArticuloOrdenRestaurante>()
        //            where articuloOrden.Activo
        //            && articuloOrden.IdEstado == idEstadoPreparacion
        //            && articuloOrden.OrdenRestaurante.Activo
        //            select new
        //            {
        //                id = articuloOrden.Id,
        //                idArticulo = articuloOrden.IdArticulo,
        //                //nombre = articuloOrden.Articulo.Descripcion,
        //                //tipo = articuloOrden.Articulo.TipoArticulo.Nombre,
        //                precio = articuloOrden.PrecioUnidad,
        //                cantidad = articuloOrden.Cantidad,
        //                observaciones = articuloOrden.Observaciones
        //            }).ToList().Select(m => new DtoArticuloPrepararConsulta
        //            {
        //                Id = m.id,
        //                IdArticulo = m.idArticulo,
        //                //NombreTipo = m.tipo,
        //                //Nombre = m.nombre,
        //                Cantidad = m.cantidad,
        //                Precio = m.precio,
        //                Observaciones = m.observaciones,
        //                Clasificacion = DtoArticuloPrepararConsulta.Clasificaciones.OrdenRestaurante
        //            }).ToList();
        //}


        public decimal ObtenerPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            Expression<Func<PagoOcupacionMesa, bool>> filtro = m => m.Activo;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var valores = contexto.Set<PagoOcupacionMesa>().Where(filtro).Select(m=>m.Valor).ToList();

            if (valores.Count == 0)
                return 0;

            return valores.Sum();
        }


        public Dictionary<TiposPago, decimal> ObtenerPagosMesaPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PagoOcupacionMesa, bool>> filtro = m => m.Activo;

            if (idsTiposPago.Count > 0)
                filtro = filtro.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from pagoRenta in contexto.Set<PagoOcupacionMesa>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoPago into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposPago)m.Key, m => m.valor);
        }


        public List<DtoEmpleadoLineaArticuloValor> ObtenerArticulosPorEmpleado(List<DtoIdArticuloIdLinea> articulos, List<int> idsCortes)
        {
            int idEstadoEntregado = (int)ArticuloComanda.Estados.Entregado;
            int idEstadoEntregada = (int)Comanda.Estados.Cobrada;
            int idEstadoCobrado = (int)OcupacionMesa.Estados.Cobrada;

            var idsArticulo = articulos.Select(m => m.IdArticulo).Distinct().ToList();

            Expression<Func<ArticuloOrdenRestaurante, bool>> filtro = m => m.IdEstado == idEstadoEntregado
                                                                           && m.OrdenRestaurante.IdEstado == idEstadoEntregada
                                                                           && m.OrdenRestaurante.OcupacionMesa.IdEstado == idEstadoCobrado
                                                                           && idsCortes.Contains(m.OrdenRestaurante.OcupacionMesa.IdCorte)
                                                                           && idsArticulo.Contains(m.IdArticulo);

            //if (fechaInicio.HasValue)
            //    filtro = filtro.Compose(m => m.OrdenRestaurante.OcupacionMesa.FechaModificacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.OrdenRestaurante.OcupacionMesa.FechaModificacion <= fechaFin.Value, Expression.AndAlso);

            //return (from articuloOrden in contexto.Set<ArticuloOrdenRestaurante>().Where(filtro)
            //        join item in articulos on articuloOrden.IdArticulo equals item.IdArticulo
            //        group articuloOrden by new { articuloOrden.OrdenRestaurante.OcupacionMesa.Mesero, item.IdLinea } into grupo
            //        select new
            //        {
            //            grupo.Key,
            //            valor = grupo.Sum(m => m.PrecioUnidad * m.Cantidad)
            //        }).ToList().Select(m => new DtoEmpleadoLineaArticuloValor
            //        {
            //            IdLineaArticulo = m.Key.IdLinea,
            //            Nombre = m.Key.Mesero.NombreCompleto,
            //            Valor = m.valor
            //        }).ToList();

            var pre = (from articuloComanda in contexto.Set<ArticuloOrdenRestaurante>().Where(filtro)
                       //join item in articulos.Select(m=> new { m.IdArticulo, m.IdLinea }) on articuloComanda.IdArticulo equals item.IdArticulo
                       group articuloComanda by new { articuloComanda.OrdenRestaurante.OcupacionMesa.Mesero, articuloComanda.IdArticulo } into grupo
                       select new
                       {
                           grupo.Key,
                           valor = grupo.Sum(m => m.PrecioUnidadFinal * m.Cantidad)
                       }).ToList();


            return (from articuloComanda in pre
                    join item in articulos on articuloComanda.Key.IdArticulo equals item.IdArticulo
                    group articuloComanda by new { articuloComanda.Key.Mesero, item.IdLinea } into grupo
                    select new
                    {
                        grupo.Key,
                        valor = grupo.Sum(m => m.valor)
                    }).ToList().Select(m => new DtoEmpleadoLineaArticuloValor
                    {
                        IdEmpleado = m.Key.Mesero.Id,
                        IdLineaArticulo = m.Key.IdLinea,
                        Nombre = m.Key.Mesero.NombreCompleto,
                        ValorConIVA = m.valor
                    }).ToList();

        }


        public List<OcupacionMesa> ObtenerOcupacionesCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            int idEstadoFinalizado = (int)OcupacionMesa.Estados.Cobrada;

            Expression<Func<OcupacionMesa, bool>> filtro = m => m.IdEstado == idEstadoFinalizado;
            Expression<Func<PagoOcupacionMesa, bool>> filtroPago = m => m.Activo;


            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCobro >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCobro <= fechaFin.Value, Expression.AndAlso);
            if (formasPago.HasValue)
            {
                var idFormaPago = (int)formasPago;

                filtroPago = filtroPago.Compose(m => m.IdTipoPago == idFormaPago, Expression.AndAlso);
            }

            //var filtroPagoCompilado = filtroPago.Compile();

            filtro = filtro.Compose(m => m.PagosOcupacionMesa.AsQueryable().Any(filtroPago), Expression.AndAlso);

            var resultado = (from ocupacion in contexto.Set<OcupacionMesa>().Where(filtro)
                             select new
                             {
                                 renta = ocupacion,
                                 mesero = ocupacion.Mesero,
                                 //habitacion = renta.Habitacion,
                                 pagos = ocupacion.PagosOcupacionMesa.AsQueryable().Where(filtroPago),
                                 //detalles = renta.DetallesPago.Where(m => m.Activo)
                             }).ToList().Select(m => m.renta).ToList();

            return resultado;
        }


        public Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosMesaTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoT = (int)TiposPago.TarjetaCredito;

            Expression<Func<PagoOcupacionMesa, bool>> filtro = m => m.Activo && m.IdTipoPago == idTipoPagoT;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var res = (from pagoRenta in contexto.Set<PagoOcupacionMesa>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoTarjeta into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList();

            if (res.Count == 0)
                return new Dictionary<TiposTarjeta, decimal>();

            return res.ToDictionary(m => (TiposTarjeta)m.Key, m => m.valor);
        }


        public List<ArticuloOrdenRestaurante> ObtenerDetallesOrdenesEntregadasCliente(int idOcupacion)
        {
            var idsEstadoEntregado = (int)Comanda.Estados.PorPagar;
            var idEstadoEntregado = (int)ArticuloComanda.Estados.Entregado;

            var datos = (from articuloOrden in contexto.Set<ArticuloOrdenRestaurante>()
                         where articuloOrden.OrdenRestaurante.IdOcupacionMesa == idOcupacion
                         && articuloOrden.OrdenRestaurante.IdEstado == idsEstadoEntregado
                         && articuloOrden.IdEstado == idEstadoEntregado
                         select new
                         {
                             //comanda = articuloOrden,
                             //articulosComanda = articuloOrden.ArticulosOrdenRestaurante.Where(m => m.Activo),
                             articuloOrden,
                             articuloOrden.OrdenRestaurante.Orden
                             //articulos = articuloOrden.Articulo,
                             //tipos = articuloOrden.Articulo.TipoArticulo
                         }).ToList()/*.Select(m => m.articuloOrden).ToList()*/;

            datos.ForEach(m => m.articuloOrden.OrdenTMP = m.Orden);

            return datos.Select(m => m.articuloOrden).ToList();
        }


        //public List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosEnPreparacionArea(List<string> idsArticulos)
        //{
        //    //var idArea = (int)areaPreparacion;

        //    var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;

        //    var resultado = (from comanda in contexto.Set<OrdenRestaurante>()
        //                     where comanda.Activo
        //                     && comanda.ArticulosOrdenRestaurante.Any(m => m.Activo && m.IdEstado == idEstadoPreparacion && idsArticulos.Contains(m.IdArticulo))
        //                     select new
        //                     {
        //                         comanda,
        //                         numeroMesa = comanda.OcupacionMesa.Mesa.Clave,
        //                         articulosComanda = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)),
        //                         //articulos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo),
        //                         //tipos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo.TipoArticulo)
        //                     }).ToList();

        //    if (resultado.Count == 0)
        //        return new List<OrdenRestaurante>();

        //    resultado.ForEach(m => m.comanda.NumeroMesaTmp = m.numeroMesa);

        //    return resultado.Select(m => m.comanda).ToList();
        //}

        //public List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosPreparadosArea(List<string> idsArticulos)
        //{
        //    //var idArea = (int)areaPreparacion;

        //    var idEstadoPorEntregar = (int)ArticuloComanda.Estados.PorEntregar;

        //    var resultado = (from comanda in contexto.Set<OrdenRestaurante>()
        //                     where comanda.Activo
        //                     && comanda.ArticulosOrdenRestaurante.Any(m => m.Activo && m.IdEstado == idEstadoPorEntregar && idsArticulos.Contains(m.IdArticulo))
        //                     select new
        //                     {
        //                         comanda,
        //                         numeroMesa = comanda.OcupacionMesa.Mesa.Clave,
        //                         articulosComanda = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)),
        //                         //articulos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo),
        //                         //tipos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo.TipoArticulo)
        //                     }).ToList();

        //    if (resultado.Count == 0)
        //        return new List<OrdenRestaurante>();

        //    resultado.ForEach(m => m.comanda.NumeroMesaTmp = m.numeroMesa);

        //    return resultado.Select(m => m.comanda).ToList();
        //}

        public List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(List<string> idsArticulos)
        {
            //var idArea = (int)areaPreparacion;
            var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;
            var idEstadoPorEntregar = (int)ArticuloComanda.Estados.PorEntregar;

            var resultado = (from comanda in contexto.Set<OrdenRestaurante>()
                             where comanda.Activo
                             && comanda.ArticulosOrdenRestaurante.Any(m => m.Activo && (m.IdEstado == idEstadoPreparacion || m.IdEstado == idEstadoPorEntregar) && idsArticulos.Contains(m.IdArticulo))
                             select new
                             {
                                 comanda,
                                 numeroMesa = comanda.OcupacionMesa.Mesa.Clave,
                                 articulosComanda = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)),
                                 //articulos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo),
                                 //tipos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo.TipoArticulo)
                             }).ToList();

            if (resultado.Count == 0)
                return new List<OrdenRestaurante>();

            resultado.ForEach(m => m.comanda.NumeroMesaTmp = m.numeroMesa);

            return resultado.Select(m => m.comanda).ToList();
        }

        public List<DtoResumenComandaExtendido> ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion()
        {
            //var idArea = (int)area;
            var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;
            var idEstadoPorEntregar = (int)ArticuloComanda.Estados.PorEntregar;

            var resultado = (from comanda in contexto.Set<OrdenRestaurante>()
                             where comanda.Activo
                             && comanda.ArticulosOrdenRestaurante.Any(m => m.Activo && (m.IdEstado == idEstadoPreparacion || m.IdEstado == idEstadoPorEntregar))
                             select new
                             {
                                 comanda.Id,
                                 comanda.IdEstado,
                                 comanda.FechaInicio,
                                 comanda.Orden,
                                 numeroMesa = comanda.OcupacionMesa.Mesa.Clave,
                                 articulosComanda = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo)
                                 .Select(a => new
                                 {
                                     a.Id,
                                     a.Activo,
                                     a.Cantidad,
                                     a.EsCortesia,
                                     a.IdEstado,
                                     a.IdArticulo,
                                     a.Observaciones,
                                     a.PrecioUnidad
                                 }),
                             }).ToList();

            if (resultado.Count == 0)
                return new List<DtoResumenComandaExtendido>();

            return resultado.Select(m => new DtoResumenComandaExtendido
            {
                Id = m.Id,
                Estado = (Modelo.Entidades.Comanda.Estados)m.IdEstado,
                FechaInicio = m.FechaInicio,
                Orden = m.Orden,
                Destino = m.numeroMesa,
                ArticulosComanda = m.articulosComanda.Select(a => new DtoResumenArticuloComanda
                {
                    Id = a.Id,
                    Activo = a.Activo,
                    Cantidad = a.Cantidad,
                    EsCortesia = a.EsCortesia,
                    Estado = (Modelo.Entidades.ArticuloComanda.Estados)a.IdEstado,
                    IdArticulo = a.IdArticulo,
                    Observaciones = a.Observaciones,
                    PrecioUnidad = a.PrecioUnidad
                }).ToList()
            }).ToList();
        }

        public List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketOrdenRestaurante(int idOrdenRestaurante)
        {
            SqlParameter sp_idOrdenRestaurante = new SqlParameter("@idOrdenRestaurante", idOrdenRestaurante);
            sp_idOrdenRestaurante.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<DtoArticuloTicket>("exec [SotSchema].[SP_ObtenerResumenesArticulosTicketOrdenRestaurante] @idOrdenRestaurante", sp_idOrdenRestaurante).ToList();
        }

        public List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketOrdenRestauranteCancelada(int idOrdenRestaurante)
        {
            SqlParameter sp_idOrdenRestaurante = new SqlParameter("@idOrdenRestaurante", idOrdenRestaurante);
            sp_idOrdenRestaurante.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<DtoArticuloTicket>("exec [SotSchema].[SP_ObtenerResumenesArticulosTicketOrdenRestauranteCancelada] @idOrdenRestaurante", sp_idOrdenRestaurante).ToList();
        }

        public int ObtenerCantidadArticulosOrdenesRestaurantePorCodigoArticulo(string codigoArticulo)
        {
            var codigoU = codigoArticulo.Trim().ToUpper();

            return contexto.Set<ArticuloOrdenRestaurante>().Count(m => m.IdArticulo.Trim().ToUpper().Equals(codigoU));
        }
    }
}
