﻿using Datos.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Repositorios
{
    public class RepositorioConfiguracionesPropinas : Repositorio<ConfiguracionPropinas>, IRepositorioConfiguracionesPropinas
    {
        public RepositorioConfiguracionesPropinas() : base(new Contextos.SOTContext())
        {
        }

        public ConfiguracionPropinas ObtenerConfiguracionActiva()
        {
            return (from configuracion in contexto.Set<ConfiguracionPropinas>()
                    where configuracion.Activa
                    select new
                    {
                        configuracion,
                        procentajesTipo = configuracion.PorcentajesPropinaTipo.Where(m => m.Activo),
                        //tipos = configuracion.PorcentajesPropinaTipo.Where(m => m.Activo).Select(m => m.TipoArticulo),
                        porcentajesTarjetas = configuracion.PorcentajesTarjeta.Where(m => m.Activo),
                        fondosDia = configuracion.FondosDia.Where(m=>m.Activo)
                    }).ToList().Select(m => m.configuracion).FirstOrDefault();
        }
    }
}
