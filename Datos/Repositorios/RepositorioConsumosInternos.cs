﻿using Datos.Nucleo.Repositorios;
using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioConsumosInternos : Repositorio<ConsumoInterno>, IRepositorioConsumosInternos
    {
        public RepositorioConsumosInternos() : base(new Contextos.SOTContext()) { }

        public Dictionary<TiposPago, decimal> ObtenerPagosConsumoInternoPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PagoConsumoInterno, bool>> filtro = m => m.Activo;

            if (idsTiposPago.Count > 0)
                filtro = filtro.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from pagoRenta in contexto.Set<PagoConsumoInterno>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoPago into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposPago)m.Key, m => m.valor);
        }

        public decimal ObtenerPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            Expression<Func<PagoConsumoInterno, bool>> filtro = m => m.Activo;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);

            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var valores = contexto.Set<PagoConsumoInterno>().Where(filtro).Select(m => m.Valor).ToList();

            if (valores.Count == 0)
                return 0;

            return valores.Sum();
        }

        public List<ConsumoInterno> ObtenerConsumosInternosCargadosPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            int idEstadoFinalizado = (int)Comanda.Estados.Cobrada;

            Expression<Func<ConsumoInterno, bool>> filtro = m => m.IdEstado == idEstadoFinalizado;
            Expression<Func<PagoConsumoInterno, bool>> filtroPago = m => m.Activo;


            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaEntrega >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaEntrega <= fechaFin.Value, Expression.AndAlso);
            if (formasPago.HasValue)
            {
                var idFormaPago = (int)formasPago;

                filtroPago = filtroPago.Compose(m => m.IdTipoPago == idFormaPago, Expression.AndAlso);
            }

            //var filtroPagoCompilado = filtroPago.Compile();

            filtro = filtro.Compose(m => m.PagosConsumoInterno.AsQueryable().Any(filtroPago), Expression.AndAlso);

            var resultado = (from ocupacion in contexto.Set<ConsumoInterno>().Where(filtro)
                             select new
                             {
                                 renta = ocupacion,
                                 mesero = ocupacion.Mesero,
                                 //habitacion = renta.Habitacion,
                                 pagos = ocupacion.PagosConsumoInterno.AsQueryable().Where(filtroPago),
                                 //detalles = renta.DetallesPago.Where(m => m.Activo)
                             }).ToList().Select(m => m.renta).ToList();

            return resultado;
        }


        public ConsumoInterno ObtenerParaPagar(int idConsumoInterno)
        {
            //var idEstadoElaborada = (int)Comanda.Estados.Elaborada;

            return (from comanda in contexto.Set<ConsumoInterno>()
                    where comanda.Activo
                    && comanda.Id == idConsumoInterno
                    //&& comanda.IdEstado == idEstadoElaborada
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.Activo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }


        public ConsumoInterno ObtenerPorArticulos(int idConsumoInterno, List<int> idsArticulosConsumosInternos)
        {
            //var idEstadoElaborada = (int)Comanda.Estados.Elaborada;

            return (from comanda in contexto.Set<ConsumoInterno>()
                    where comanda.Activo
                    && comanda.Id == idConsumoInterno
                    && comanda.ArticulosConsumoInterno.Count(m => m.Activo && idsArticulosConsumosInternos.Contains(m.Id)) == idsArticulosConsumosInternos.Count
                    //&& comanda.IdEstado == idEstadoElaborada
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.Activo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }


        public List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketConsumoInterno(int idConsumoInterno)
        {
            SqlParameter sp_idConsumoInterno = new SqlParameter("@idConsumoInterno", idConsumoInterno);
            sp_idConsumoInterno.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<DtoArticuloTicket>("exec [SotSchema].[SP_ObtenerResumenesArticulosTicketConsumoInterno] @idConsumoInterno", sp_idConsumoInterno).ToList();
        }


        public ConsumoInterno ObtenerConsumoInternoEntregadoConDetalles(int idConsumoInterno)
        {
            var idEstadoCobrada = (int)Comanda.Estados.Cobrada;
            var idEstadoArtEntregado = (int)ArticuloComanda.Estados.Entregado;

            return (from comanda in contexto.Set<ConsumoInterno>()
                    where comanda.IdEstado == idEstadoCobrada
                    && comanda.Id == idConsumoInterno
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.IdEstado == idEstadoArtEntregado),
                        //articulo = comanda.ArticulosComanda.Where(m => m.IdEstado == idEstadoArtEntregado).Select(m=>m.Articulo),
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }

        public int ObtenerCantidadEnCurso(string nombre, string apellidoPaterno, string apellidoMaterno)
        {
            var estadosProhibidos = new List<int>() { (int)Comanda.Estados.Cancelada, (int)Comanda.Estados.Cobrada };
            
            Expression<Func<ConsumoInterno, bool>> filtro = m => !estadosProhibidos.Contains(m.IdEstado);

            if (!string.IsNullOrEmpty(nombre))
            {
                var nombreU = nombre.Trim().ToUpper();
                filtro = filtro.Compose(m => m.EmpleadoConsume.Nombre.ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoPaterno))
            {
                var apellidoPaternoU = apellidoPaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.EmpleadoConsume.ApellidoPaterno.ToUpper().Contains(apellidoPaternoU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoMaterno))
            {
                var apellidoMaternoU = apellidoMaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.EmpleadoConsume.ApellidoMaterno.ToUpper().Contains(apellidoMaternoU), Expression.AndAlso);
            }

            return contexto.Set<ConsumoInterno>().Count(filtro);
        }
        public List<ConsumoInterno> ObtenerConsumosInternosFiltrados(string nombre, string apellidoPaterno, string apellidoMaterno, bool soloEnCurso, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            Expression<Func<ConsumoInterno, bool>> filtro = m => true;

            if (!string.IsNullOrEmpty(nombre))
            {
                var nombreU = nombre.Trim().ToUpper();
                filtro = filtro.Compose(m => m.EmpleadoConsume.Nombre.ToUpper().Contains(nombreU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoPaterno))
            {
                var apellidoPaternoU = apellidoPaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.EmpleadoConsume.ApellidoPaterno.ToUpper().Contains(apellidoPaternoU), Expression.AndAlso);
            }
            if (!string.IsNullOrEmpty(apellidoMaterno))
            {
                var apellidoMaternoU = apellidoMaterno.Trim().ToUpper();
                filtro = filtro.Compose(m => m.EmpleadoConsume.ApellidoMaterno.ToUpper().Contains(apellidoMaternoU), Expression.AndAlso);
            }
            if (soloEnCurso) 
            {
                var estadosProhibidos = new List<int>() { (int)Comanda.Estados.Cancelada, (int)Comanda.Estados.Cobrada };
                filtro = filtro.Compose(m => !estadosProhibidos.Contains(m.IdEstado), Expression.AndAlso);
            }
            if (fechaInicial.HasValue)
            {
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicial.Value, Expression.AndAlso);
            }
            if (fechaFinal.HasValue)
            {
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFinal.Value, Expression.AndAlso);
            }

            var datos = (from consumoInterno in contexto.Set<ConsumoInterno>().Where(filtro)
                         select new
                         {
                             consumo = consumoInterno,
                             folioTurno = consumoInterno.CorteTurno.NumeroCorte,
                             nombreConsumidor = consumoInterno.EmpleadoConsume.Nombre,
                             apellidoPConsumidor = consumoInterno.EmpleadoConsume.ApellidoPaterno,
                             apellidoMConsumidor = consumoInterno.EmpleadoConsume.ApellidoMaterno,
                             nombreP = consumoInterno.EmpleadoConsume.Puesto.Nombre,
                         }).ToList();

            if (datos.Count == 0)
                return new List<ConsumoInterno>();

            /*
                  <DataGridTextColumn Header="Personal" Binding="{Binding NombreConsumidorTmp}"/>
                    <DataGridTextColumn Header="Puesto" Binding="{Binding NombrePuestoTmp}"/>
                    <DataGridTextColumn Header="Turno" Binding="{Binding FolioTurnoTmp}"/>
                    <DataGridTextColumn Header="Tipo de consumo" Binding="{Binding TipoConsumoTmp}"/>
                    <DataGridTextColumn Header="Motivo" Binding="{Binding Motivo}"/>
                    <DataGridTextColumn Header="Fecha de ingreso" Binding="{Binding FechaCreacion, StringFormat=dd/MM/yyyy hh:mm tt}"/>
             */

            foreach (var dato in datos)
            {
                dato.consumo.NombreConsumidorTmp = string.Join(" ", dato.nombreConsumidor, dato.apellidoPConsumidor, dato.apellidoMConsumidor).Trim();
                dato.consumo.NombrePuestoTmp = dato.nombreP;
                dato.consumo.FolioTurnoTmp = dato.folioTurno;
            }

            //datos.ForEach(m => m.empleado.NombrePuesto = m.nombreP);

            return datos.Select(m => m.consumo).ToList();
        }


        public ConsumoInterno ObtenerConsumoPendienteCargado(int idConsumoInterno)
        {
            var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar/*, (int)Comanda.Estados.PorPagar*/ };

            return (from comanda in contexto.Set<ConsumoInterno>()
                    where comanda.Activo
                    && comanda.Id == idConsumoInterno
                    && idsEstadoElaborada.Contains(comanda.IdEstado)
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.Activo),
                        //articulos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo),
                        //tipos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo.TipoArticulo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }

        public ConsumoInterno ObtenerConsumoFinalizadoCargado(int idConsumoInterno)
        {
            //var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar/*, (int)Comanda.Estados.PorPagar*/ };
            var idEstadoF = (int)Comanda.Estados.Cobrada;
            var idEstadoArt = (int)ArticuloComanda.Estados.Entregado;

            return (from comanda in contexto.Set<ConsumoInterno>()
                    where comanda.Id == idConsumoInterno
                    && comanda.IdEstado == idEstadoF
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.IdEstado == idEstadoArt),
                        //articulos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo),
                        //tipos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo.TipoArticulo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }

        public ConsumoInterno ObtenerConsumoCanceladoCargado(int idConsumoInterno)
        {
            //var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar/*, (int)Comanda.Estados.PorPagar*/ };
            var idEstadoF = (int)Comanda.Estados.Cancelada;
            //var idEstadoArt = (int)ArticuloComanda.Estados.Entregado;

            return (from comanda in contexto.Set<ConsumoInterno>()
                    where comanda.Id == idConsumoInterno
                    && comanda.IdEstado == idEstadoF
                    select new
                    {
                        comanda,
                        articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.FechaEliminacion == comanda.FechaEliminacion),
                        //articulos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo),
                        //tipos = comanda.ArticulosComanda.Where(m => m.Activo).Select(m => m.Articulo.TipoArticulo)
                    }).ToList().Select(m => m.comanda).FirstOrDefault();
        }


        public List<ConsumoInterno> ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion(List<string> idsArticulos)
        {
            var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;
            var idEstadoPorEntregar = (int)ArticuloComanda.Estados.PorEntregar;

            var resultado = (from comanda in contexto.Set<ConsumoInterno>()
                             where comanda.Activo
                             && comanda.ArticulosConsumoInterno.Any(m => m.Activo && (m.IdEstado == idEstadoPreparacion || m.IdEstado == idEstadoPorEntregar) && idsArticulos.Contains(m.IdArticulo))
                             select new
                             {
                                 comanda,
                                 nombreEmpleadoConsume = comanda.EmpleadoConsume.Nombre,
                                 apellidoPEmpleadoConsume = comanda.EmpleadoConsume.ApellidoPaterno,
                                 apellidoMEmpleadoConsume = comanda.EmpleadoConsume.ApellidoMaterno,
                                 articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)),
                                 //articulos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo),
                                 //tipos = comanda.ArticulosOrdenRestaurante.Where(m => m.Activo && idsArticulos.Contains(m.IdArticulo)).Select(m => m.Articulo.TipoArticulo)
                             }).ToList();

            if (resultado.Count == 0)
                return new List<ConsumoInterno>();

            resultado.ForEach(m => m.comanda.NombreConsumidorTmp = string.Join(" ", m.nombreEmpleadoConsume, m.apellidoPEmpleadoConsume, m.apellidoMEmpleadoConsume));

            return resultado.Select(m => m.comanda).ToList();
        }

        public List<DtoResumenComandaExtendido> ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion()
        {
            //var idArea = (int)area;
            var idEstadoPreparacion = (int)ArticuloComanda.Estados.EnProceso;
            var idEstadoPorEntregar = (int)ArticuloComanda.Estados.PorEntregar;

            var resultado = (from comanda in contexto.Set<ConsumoInterno>()
                             where comanda.Activo
                             && comanda.ArticulosConsumoInterno.Any(m => m.Activo && (m.IdEstado == idEstadoPreparacion || m.IdEstado == idEstadoPorEntregar))
                             select new
                             {
                                 comanda.Id,
                                 comanda.IdEstado,
                                 comanda.FechaInicio,
                                 comanda.Orden,
                                 nombreEmpleadoConsume = comanda.EmpleadoConsume.Nombre,
                                 apellidoPEmpleadoConsume = comanda.EmpleadoConsume.ApellidoPaterno,
                                 apellidoMEmpleadoConsume = comanda.EmpleadoConsume.ApellidoMaterno,
                                 articulosComanda = comanda.ArticulosConsumoInterno.Where(m => m.Activo)
                                 .Select(a => new
                                 {
                                     a.Id,
                                     a.Activo,
                                     a.Cantidad,
                                     a.IdEstado,
                                     a.IdArticulo,
                                     a.Observaciones,
                                     a.PrecioUnidad
                                 }),
                             }).ToList();

            if (resultado.Count == 0)
                return new List<DtoResumenComandaExtendido>();

            return resultado.Select(m => new DtoResumenComandaExtendido
            {
                Id = m.Id,
                Estado = (Modelo.Entidades.Comanda.Estados)m.IdEstado,
                FechaInicio = m.FechaInicio,
                Orden = m.Orden,
                Destino = string.Join(" ", m.nombreEmpleadoConsume, m.apellidoPEmpleadoConsume, m.apellidoMEmpleadoConsume),
                ArticulosComanda = m.articulosComanda.Select(a => new DtoResumenArticuloComanda
                {
                    Id = a.Id,
                    Activo = a.Activo,
                    Cantidad = a.Cantidad,
                    Estado = (Modelo.Entidades.ArticuloComanda.Estados)a.IdEstado,
                    IdArticulo = a.IdArticulo,
                    Observaciones = a.Observaciones,
                    PrecioUnidad = a.PrecioUnidad
                }).ToList()
            }).ToList();
        }


        public List<DtoEmpleadoLineaArticuloValor> ObtenerArticulosPorEmpleado(List<Modelo.Almacen.Entidades.Dtos.DtoIdArticuloIdLinea> articulos, List<int> idsCortes)
        {
            int idEstadoEntregado = (int)ArticuloComanda.Estados.Entregado;
            int idEstadoEntregada = (int)Comanda.Estados.Cobrada;

            var idsArticulo = articulos.Select(m => m.IdArticulo).Distinct().ToList();

            Expression<Func<ArticuloConsumoInterno, bool>> filtro = m => m.IdEstado == idEstadoEntregado
                                                                           && m.ConsumoInterno.IdEstado == idEstadoEntregada
                                                                           && idsArticulo.Contains(m.IdArticulo)
                                                                           && idsCortes.Contains(m.ConsumoInterno.IdCorteTurno);

            //if (fechaInicio.HasValue)
            //    filtro = filtro.Compose(m => m.ConsumoInterno.FechaModificacion >= fechaInicio.Value, Expression.AndAlso);
            //if (fechaFin.HasValue)
            //    filtro = filtro.Compose(m => m.ConsumoInterno.FechaModificacion <= fechaFin.Value, Expression.AndAlso);

            //return (from articuloOrden in contexto.Set<ArticuloOrdenRestaurante>().Where(filtro)
            //        join item in articulos on articuloOrden.IdArticulo equals item.IdArticulo
            //        group articuloOrden by new { articuloOrden.OrdenRestaurante.OcupacionMesa.Mesero, item.IdLinea } into grupo
            //        select new
            //        {
            //            grupo.Key,
            //            valor = grupo.Sum(m => m.PrecioUnidad * m.Cantidad)
            //        }).ToList().Select(m => new DtoEmpleadoLineaArticuloValor
            //        {
            //            IdLineaArticulo = m.Key.IdLinea,
            //            Nombre = m.Key.Mesero.NombreCompleto,
            //            Valor = m.valor
            //        }).ToList();

            var pre = (from articuloComanda in contexto.Set<ArticuloConsumoInterno>().Where(filtro)
                       //join item in articulos.Select(m=> new { m.IdArticulo, m.IdLinea }) on articuloComanda.IdArticulo equals item.IdArticulo
                       group articuloComanda by new { articuloComanda.ConsumoInterno.Mesero, articuloComanda.IdArticulo } into grupo
                       select new
                       {
                           grupo.Key,
                           valor = grupo.Sum(m => m.PrecioUnidadFinal * m.Cantidad)
                       }).ToList();


            return (from articuloComanda in pre
                    join item in articulos on articuloComanda.Key.IdArticulo equals item.IdArticulo
                    group articuloComanda by new { articuloComanda.Key.Mesero, item.IdLinea } into grupo
                    select new
                    {
                        grupo.Key,
                        valor = grupo.Sum(m => m.valor)
                    }).ToList().Select(m => new DtoEmpleadoLineaArticuloValor
                    {
                        IdEmpleado = m.Key.Mesero.Id,
                        IdLineaArticulo = m.Key.IdLinea,
                        Nombre = m.Key.Mesero.NombreCompleto,
                        ValorConIVA = m.valor
                    }).ToList();
        }


        public List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketConsumoInternoCancelado(int idConsumoInterno)
        {
            SqlParameter sp_idConsumoInterno = new SqlParameter("@idConsumoInterno", idConsumoInterno);
            sp_idConsumoInterno.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<DtoArticuloTicket>("exec [SotSchema].[SP_ObtenerResumenesArticulosTicketConsumoInternoCancelado] @idConsumoInterno", sp_idConsumoInterno).ToList();
        }

        public int ObtenerCantidadArticulosConsumosInternosPorCodigoArticulo(string codigoArticulo)
        {
            var codigoU = codigoArticulo.Trim().ToUpper();

            return contexto.Set<ArticuloConsumoInterno>().Count(m => m.IdArticulo.Trim().ToUpper().Equals(codigoU));
        }

        public bool ExisteDeVerdadConsumosInternos(List<int> Estados)
        {
            bool respuesta = false;

            var datos = (from consumoInterno in contexto.Set<ConsumoInterno>() where Estados.Contains(consumoInterno.IdEstado)
                         select new
                         {
                             consumo = consumoInterno
                         }).ToList();
            if (datos.Count > 0)
                respuesta = true;

            return respuesta;
        }
    }
}
