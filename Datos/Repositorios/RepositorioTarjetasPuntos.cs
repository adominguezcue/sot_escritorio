﻿using Datos.Nucleo.Repositorios;
using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Repositorios
{
    public class RepositorioTarjetasPuntos : Repositorio<TarjetaPuntos>, IRepositorioTarjetasPuntos
    {
        public RepositorioTarjetasPuntos() : base(new Contextos.SOTContext()) { }

        public Dictionary<TiposPago, decimal> ObtenerDetallesPagosPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin)
        {
            Expression<Func<PagoTarjetaPuntos, bool>> filtro = m => m.Activo;

            if (idsTiposPago.Count > 0)
                filtro = filtro.Compose(m => idsTiposPago.Contains(m.IdTipoPago), Expression.AndAlso);

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            return (from pagoRenta in contexto.Set<PagoTarjetaPuntos>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoPago into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList().ToDictionary(m => (TiposPago)m.Key, m => m.valor);
        }


        public List<TarjetaPuntos> ObtenerTarjetas(DateTime? fechaInicio = null, DateTime? fechaFin = null, TarjetaPuntos.EstadosTarjeta? estado = null)
        {
            Expression<Func<TarjetaPuntos, bool>> filtro = m => m.Activo;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);
            if (estado.HasValue)
            {
                var idEstado = (int)estado.Value;

                filtro = filtro.Compose(m => m.IdEstado == idEstado, Expression.AndAlso);
            }

            return contexto.Set<TarjetaPuntos>().Where(filtro).ToList();
        }


        public Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            int idTipoPagoT = (int)TiposPago.TarjetaCredito;

            Expression<Func<PagoTarjetaPuntos, bool>> filtro = m => m.Activo && m.IdTipoPago == idTipoPagoT;

            if (fechaInicio.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion >= fechaInicio.Value, Expression.AndAlso);
            if (fechaFin.HasValue)
                filtro = filtro.Compose(m => m.FechaCreacion <= fechaFin.Value, Expression.AndAlso);

            var res = (from pagoRenta in contexto.Set<PagoTarjetaPuntos>().Where(filtro)
                    group pagoRenta by pagoRenta.IdTipoTarjeta into p
                    select new
                    {
                        p.Key,
                        valor = p.Sum(m => m.Valor)
                    }).ToList();

            if (res.Count == 0)
                return new Dictionary<TiposTarjeta, decimal>();

            return res.ToDictionary(m => (TiposTarjeta)m.Key, m => m.valor);
        }
    }
}
