Imports ZctSQL
Imports ZctSQL.Implementacion

Public Class ZctListInvCorte
    Inherits SortedDictionary(Of String, ZctInvUnidad)

    Implements ZctSQL.IZctDatos

    Public Event ErrorAdd(ByVal Sender As Object, ByVal e As Exception)

    Private _TpInventario As Integer = 1
    ''' <summary>
    ''' Tipo de inventario 
    ''' 1= Genera
    ''' 2= Aleatorio
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TpInventario() As Integer
        Get
            Return _TpInventario
        End Get
        Set(ByVal value As Integer)
            _TpInventario = value
        End Set
    End Property

    Private _Folio As Integer
    ''' <summary>
    ''' Folio del Invenario
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks> 
    Public Property Folio() As Integer
        Get
            Return _Folio
        End Get
        Set(ByVal value As Integer)
            _Folio = value
        End Set
    End Property

    Private _CodAlm As String
    ''' <summary>
    ''' C�digo de almac�n
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodAlm() As String
        Get
            Return _CodAlm
        End Get
        Set(ByVal value As String)
            _CodAlm = value
        End Set
    End Property

    Public Property CodLinea As Integer = 0

    Public Property CodDpto As Integer = 0

    Public Sub GetMe(ByVal DataReader As System.Data.SqlClient.SqlDataReader) Implements ZctSQL.IZctDatos.GetMe
        While DataReader.Read


            If Not Me.ContainsKey(DataReader("Cod_Art").ToString()) Then
                Me.Add(DataReader("Cod_Art").ToString(), New ZctInvUnidad(DataReader("Cod_Art").ToString(), DataReader("Desc_Art").ToString(), GetGeneric(Of Integer)(DataReader("Exist_Art")), GetGeneric(Of Double)(DataReader("CostoProm_Art")), GetGeneric(Of Integer)(DataReader("Conteo1_Art")), GetGeneric(Of Integer)(DataReader("Conteo2_Art")), GetGeneric(Of Integer)(DataReader("Conteo3_Art")), GetGeneric(Of Boolean)(DataReader("Ajuste_Art"))))
            Else
                Dim art As ZctInvUnidad = Me.Item(DataReader("Cod_Art").ToString)
                art.Desc_Art = DataReader("Desc_Art").ToString
                art.Existencia += GetGeneric(Of Integer)(DataReader("Exist_Art"))
                art.Costo += GetGeneric(Of Double)(DataReader("CostoProm_Art"))
                art.Conteo1 += GetGeneric(Of Integer)(DataReader("Conteo1_Art"))
                art.Conteo2 += GetGeneric(Of Integer)(DataReader("Conteo2_Art"))
            End If
        End While
    End Sub

    Public ReadOnly Property SQLElimina() As String Implements ZctSQL.IZctDatos.SQLElimina
        Get

        End Get
    End Property

    Private _Accion As Integer = 0
    ''' <summary>
    ''' 1 Corte de Inventario
    ''' 2 Grabar cambios
    ''' 3 Realizar ajustes
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Accion() As Integer
        Get
            Return _Accion
        End Get
        Set(ByVal value As Integer)
            _Accion = value
        End Set
    End Property

    Public ReadOnly Property SQLGraba() As String Implements ZctSQL.IZctDatos.SQLGraba
        Get
            'If _Accion = 0 Then Throw New Exception("Debe Indicar el tipo de acci�n a realizar")
            'Select Case _Accion
            '    'Corte de inventario
            '    Case 1
            '        _Accion = 0
            '        Return String.Format("SP_ZctInvCorte {0}, {1}, '{2}' ", _TpInventario, _Folio, _CodAlm)
            '        'Graba Edici�n Actual
            '    Case 2
            '        _Accion = 0
            '        'Realiza ajustes
            '    Case 3
            '        _Accion = 0
            Return String.Format("SP_ZctInvCorte {0}, {1}, '{2}', {3}, {4} ", _TpInventario, _Folio, _CodAlm, CodLinea, CodDpto)
            '    Case Else
            '        Return ""
            'End Select

        End Get
    End Property

    Public ReadOnly Property SQLObtiene() As String Implements ZctSQL.IZctDatos.SQLObtiene
        Get
            Return String.Format("SELECT 
                                    art.Cod_Art,
                                    art.Desc_Art,
                                    inv.Exist_Art, 
                                    inv.CostoProm_Art, 
                                    inv.CostoProm_Art, 
                                    inv.Conteo1_Art, 
                                    inv.Conteo2_Art, 
                                    inv.Conteo3_Art, 
                                    inv.Diferencia_Art, 
                                    inv.Ajuste_Art
                                    FROM ZctInvCorteFis inv
                                    left join ZctCatArt art
	                                    on inv.Cod_Art = art.Cod_Art 
                                    WHERE art.desactivar=0 AND  (Folio = {0}) AND (inv.Cod_Alm = {1})", _Folio, _CodAlm)
        End Get
    End Property

    Public Sub SetConteo(ByVal Conteo As Integer, ByVal Valores As List(Of Captura), ByVal BD As ZctDB)

        For Each Cap As Captura In Valores
            Try
                If Me.ContainsKey(Cap.pCodigo) Then
                    If Conteo = 1 Then
                        Me.Item(Cap.pCodigo).Conteo1 += Cap.pCantidad
                    Else
                        Me.Item(Cap.pCodigo).Conteo2 += Cap.pCantidad
                    End If
                Else
                    If _TpInventario = 1 Then

                        Dim Unidad As ZctInvUnidad
                        If Conteo = 1 Then
                            Unidad = New ZctInvUnidad(Cap.pCodigo, "", 0, 0, Cap.pCantidad, 0, 0)
                        Else
                            Unidad = New ZctInvUnidad(Cap.pCodigo, "", 0, 0, 0, Cap.pCantidad, 0)
                        End If

                        GenDatos(Of ZctInvUnidad)(BD, Unidad)
                        'Si es aleatorio no hay que hacer esto
                        Me.Add(Cap.pCodigo, Unidad)
                    End If
                End If
            Catch ex As Exception
                RaiseEvent ErrorAdd(Cap, ex)
            End Try
        Next
    End Sub

    Public Sub Grabar(ByVal BD As ZctDB)
        Try
            For Each Unidad As ZctInvUnidad In Me.Values
                If Unidad.Conteo1 > 0 Or Unidad.Conteo2 > 0 Then
                    BD.CrearComando(String.Format("SP_ZctInvCorteFis '{0}', '{1}', '{2}','{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}'", _TpInventario, _Folio, _CodAlm, Unidad.Existencia, Unidad.Costo, Unidad.Cod_Art, Unidad.Conteo1, Unidad.Conteo2, Unidad.Conteo3, Unidad.Diferencia, Unidad.Ajuste))
                    BD.EjecutarComando()
                End If
            Next
        Catch ex As BaseDatosException
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        End Try


    End Sub

    Public Sub Eliminar(ByVal BD As ZctDB)
        Try

            BD.CrearComando(String.Format("SP_ZctEliminaInventario '{0}'", _Folio))
            BD.EjecutarComando()
        Catch ex As BaseDatosException
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        End Try


    End Sub

    Public Sub GrabarSinAjustes(ByVal BD As ZctDB)
        Try
            For Each Unidad As ZctInvUnidad In Me.Values
                If Unidad.Conteo1 > 0 Or Unidad.Conteo2 > 0 Then
                    BD.CrearComando(String.Format("SP_ZctInvCorteFis '{0}', '{1}', '{2}','{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}'", _TpInventario, _Folio, _CodAlm, Unidad.Existencia, Unidad.Costo, Unidad.Cod_Art, Unidad.Conteo1, Unidad.Conteo2, Unidad.Conteo3, Unidad.Diferencia, (Unidad.Ajuste And Unidad.Realizado)))
                    BD.EjecutarComando()
                End If
            Next
        Catch ex As BaseDatosException
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        End Try


    End Sub

    'Ajuste
    Public Sub Ajuste(ByVal BD As ZctDB)
        Try
            For Each Unidad As ZctInvUnidad In Me.Values
                If Unidad.Ajuste AndAlso Not Unidad.Realizado Then
                    BD.CrearComando(String.Format("EXECUTE SP_ZctEncMovInv 2, 0, 'AJ', {0}, '{1}', '{2}', {3}, '{4}', '{5:s}', '{6}'", Unidad.Costo * CDbl(Unidad.Diferencia), _Folio, Unidad.Cod_Art, Unidad.Diferencia, "AJ", Now, _CodAlm))
                    'Dim ssql As String = "EXECUTE SP_ZctEncMovInv @TpConsulta Int, @CodMov_Inv int, @TpMov_Inv  char(2), @CosMov_Inv money, @FolOS_Inv varchar(10), @Cod_Art  varchar(20), @CtdMov_Inv	int, @TpOs_Inv char(2), @FchMov_Inv smalldatetime, @Cod_Alm int"
                    Unidad.Realizado = True
                    BD.EjecutarComando()
                End If
            Next
        Catch ex As BaseDatosException
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
        End Try


    End Sub

End Class


Public Class ZctInvUnidad
    Implements ZctSQL.IZctDatos

    

    Private _Cod_Art As String
    ''' <summary>
    ''' C�digo de art�culo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Cod_Art() As String
        Get
            Return _Cod_Art
        End Get
        Set(ByVal value As String)
            _Cod_Art = value
        End Set
    End Property
    ''' <summary>
    ''' Descripci�n del art�culo
    ''' </summary>
    ''' <returns></returns>
    Public Property Desc_Art As String

    Private _Existencia As Integer = 0
    ''' <summary>
    ''' Existencia de la unidad
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Existencia() As Integer
        Get
            Return _Existencia
        End Get
        Set(ByVal value As Integer)
            _Existencia = value
        End Set
    End Property

    Private _Costo As Double = 0
    ''' <summary>
    ''' Costo promedio de la unidad
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Costo() As Double
        Get
            Return _Costo
        End Get
        Set(ByVal value As Double)
            _Costo = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal Cod_Art As String, ByVal Desc_Art As String, ByVal Existencia As Integer, ByVal Costo As Double)
        _Cod_Art = Cod_Art
        Me.Desc_Art = Desc_Art
        _Existencia = Existencia
        _Costo = Costo

    End Sub

    Public Sub New(ByVal Cod_Art As String, ByVal Desc_Art As String, ByVal Existencia As Integer, ByVal Costo As Double, ByVal Conteo1 As Integer, ByVal Conteo2 As Integer, ByVal Conteo3 As Integer, Optional ByVal Ajuste As Boolean = False)
        _Cod_Art = Cod_Art
        Me.Desc_Art = Desc_Art
        _Existencia = Existencia
        _Costo = Costo
        _Conteo1 = Conteo1
        _Conteo2 = Conteo2
        _Conteo3 = Conteo3
        _Ajuste = Ajuste
        _Realizado = Ajuste
    End Sub

    Private _Conteo1 As Integer = 0
    ''' <summary>
    ''' Valor del conteo1
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Conteo1() As Integer
        Get
            Return _Conteo1
        End Get
        Set(ByVal value As Integer)
            If _Realizado Then Exit Property
            _Conteo1 = value
            If _Conteo1 > 0 And _Conteo1 = Conteo2 Then
                _Conteo3 = _Conteo2
            Else
                _Conteo3 = 0
            End If
        End Set
    End Property

    Private _Conteo2 As Integer = 0

    Public Property Conteo2() As Integer
        Get
            Return _Conteo2
        End Get
        Set(ByVal value As Integer)
            If _Realizado Then Exit Property
            _Conteo2 = value
            If _Conteo2 > 0 And _Conteo1 = Conteo2 Then
                _Conteo3 = _Conteo2
            Else
                _Conteo3 = 0
            End If
        End Set
    End Property

    Private _Conteo3 As Integer = 0
    Public Property Conteo3() As Integer
        Get
            Return _Conteo3
        End Get
        Set(ByVal value As Integer)
            If _Realizado Then Exit Property
            _Conteo3 = value
        End Set
    End Property


    Public ReadOnly Property Diferencia() As Integer
        Get
            Return _Conteo3 - _Existencia
        End Get
    End Property

    Private _Ajuste As Boolean = False

    Public Property Ajuste() As Boolean
        Get
            Return _Ajuste
        End Get
        Set(ByVal value As Boolean)
            'If _Conteo3 = Nothing Then _Conteo3 = 0 : Throw New ExZctInventario("No se ha ingresado el conteo n�mero 3.")
            If _Realizado Then Exit Property
            _Ajuste = value
        End Set
    End Property
    Private _Realizado As Boolean = False
    ''' <summary>
    ''' Indica si el ajuste ya fue completado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Realizado() As Boolean
        Get
            Return _Realizado
        End Get
        Set(ByVal value As Boolean)
            _Realizado = value
        End Set
    End Property


    Public Sub GetMe(ByVal DataReader As System.Data.SqlClient.SqlDataReader) Implements ZctSQL.IZctDatos.GetMe
        If Not DataReader.HasRows Then
            Throw New BaseDatosException(String.Format("El art�culo {0} no existe.", _Cod_Art))
        End If
    End Sub

    Public ReadOnly Property SQLElimina() As String Implements ZctSQL.IZctDatos.SQLElimina
        Get
            Return ""
        End Get
    End Property

    Public ReadOnly Property SQLGraba() As String Implements ZctSQL.IZctDatos.SQLGraba
        Get

        End Get
    End Property

    Public ReadOnly Property SQLObtiene() As String Implements ZctSQL.IZctDatos.SQLObtiene
        Get
            Return String.Format("SELECT Cod_Art FROM ZctCatArt WHERE  (Cod_Art = '{0}')", _Cod_Art)
        End Get
    End Property
End Class