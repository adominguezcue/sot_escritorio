Imports ZctSQL
Imports ZctSQL.Implementacion

Public Class ZctInventario
    Implements ZctSQL.IZctDatos


#Region "Propiedades"

    Private _Folio As Integer
    ''' <summary>
    ''' Folio del inventario
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Folio() As Integer
        Get
            Return _Folio
        End Get
        Set(ByVal value As Integer)
            _Folio = value
        End Set
    End Property
    Private _Almacen As String
    ''' <summary>
    ''' Almac�n sobre el cual se realizar� el inventario
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Almacen() As String
        Get
            Return _Almacen
        End Get
        Set(ByVal value As String)
            _Almacen = value
        End Set
    End Property


    Private _Aleatorio As Boolean

    ''' <summary>
    ''' Indica si el inventario es aleatorio
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Aleatorio() As Boolean
        Get
            Return _Aleatorio
        End Get
        Set(ByVal value As Boolean)
            _Aleatorio = value
        End Set
    End Property

    Private _General As Boolean
    ''' <summary>
    ''' Indica que el invenatio es un invetario General
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property General() As Boolean
        Get
            Return _General
        End Get
        Set(ByVal value As Boolean)
            _General = value
        End Set
    End Property


    Private _Termino As Boolean
    ''' <summary>
    ''' Indica el termino de los conteos
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Termino() As Boolean
        Get
            Return _Termino
        End Get
        Set(ByVal value As Boolean)
            _Termino = value
        End Set
    End Property

    Private _AplicaAjuste As Boolean
    Public Property AplicaAjuste() As Boolean
        Get
            Return _AplicaAjuste
        End Get
        Set(ByVal value As Boolean)
            _AplicaAjuste = value
        End Set
    End Property

	
    Public Property CodigoDepartamento As Integer?
    Public Property CodigoLinea As Integer?

#End Region

    Public Sub New(ByVal Folio As Integer)
        _Folio = Folio
    End Sub


    Public Sub GetMe(ByVal DataReader As System.Data.SqlClient.SqlDataReader) Implements ZctSQL.IZctDatos.GetMe
        If DataReader.Read() Then
            _Folio = GetGeneric(Of Integer)(DataReader("Folio"))
            _Almacen = DataReader("Cod_Alm").ToString
            _General = GetGeneric(Of Boolean)(DataReader("InvFis_EncFis"))
            _Aleatorio = GetGeneric(Of Boolean)(DataReader("InvAle_EncFis"))
            _Termino = GetGeneric(Of Boolean)(DataReader("Termino_EncFis"))
            _AplicaAjuste = GetGeneric(Of Boolean)(DataReader("Apl_AjustExist"))
            CodigoDepartamento = If(DataReader("Cod_Dpto") Is DBNull.Value, GetGenericC(Of Integer?)(DataReader("Cod_Dpto")), GetGeneric(Of Integer)(DataReader("Cod_Dpto")))
            CodigoLinea = If(DataReader("Cod_Linea") Is DBNull.Value, GetGenericC(Of Integer?)(DataReader("Cod_Linea")), GetGeneric(Of Integer)(DataReader("Cod_Linea")))
        End If
    End Sub

    Public ReadOnly Property SQLElimina() As String Implements ZctSQL.IZctDatos.SQLElimina
        Get
            Return ""
        End Get
    End Property

    Public ReadOnly Property SQLGraba() As String Implements ZctSQL.IZctDatos.SQLGraba
        Get

            If CodigoLinea.HasValue Then
                Return String.Format("SP_ZctInvEncFis 2, '{0}','{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}'",
             _Folio, _Almacen, _General, _Aleatorio, False, _AplicaAjuste, _Termino, CodigoDepartamento, CodigoLinea)
            Else
                Return String.Format("SP_ZctInvEncFis 2, '{0}','{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}'",
            _Folio, _Almacen, _General, _Aleatorio, False, _AplicaAjuste, _Termino, CodigoDepartamento)
            End If

        End Get
    End Property

    Public ReadOnly Property SQLObtiene() As String Implements ZctSQL.IZctDatos.SQLObtiene
        Get
            Return String.Format("Select 
                                    Folio, 
                                    Cod_Alm,
                                    InvFis_EncFis, 
                                    InvAle_EncFis, 
                                    Apl_AjustCosto,
                                    Apl_AjustExist, 
                                    Termino_EncFis,
                                    Cod_Dpto,
	                                Cod_Linea
                                  FROM ZctInvEncFis where Folio = '{0}'", _Folio)
        End Get
    End Property
End Class

