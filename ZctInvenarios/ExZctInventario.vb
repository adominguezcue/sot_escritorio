Public Class ExZctInventario

    Inherits ApplicationException

    ''' <summary>
    ''' Construye una instancia en base a un mensaje de error y la una excepci�n original.
    ''' </summary>
    ''' <param name="mensaje">El mensaje de error.</param>
    ''' <param name="original">La excepci�n original.</param>
    Public Sub New(ByVal mensaje As String, ByVal original As Exception)
        MyBase.New(mensaje, original)
        EscribeLog(mensaje & vbTab & original.Message.ToString)

    End Sub

    ''' <summary>
    ''' Construye una instancia en base a un mensaje de error.
    ''' </summary>
    ''' <param name="mensaje">El mensaje de error.</param>
    Public Sub New(ByVal mensaje As String)
        MyBase.New(mensaje)
        EscribeLog(mensaje)
    End Sub

    Public Sub New(ByVal mensaje As String, ByVal original As Exception, ByVal Forma As String, ByVal Procedimiento As String)
        MyBase.New(mensaje, original)
        EscribeLog(mensaje & vbTab & original.Message.ToString & vbTab & Forma & Procedimiento)

    End Sub

    Private Sub EscribeLog(ByVal Texto As String)

        Dim oFile As System.IO.File
        Dim oWrite As System.IO.StreamWriter
        oWrite = oFile.CreateText("Error.Log")
        oWrite.WriteLine(Texto & vbTab & Now.Date & vbCrLf)
        oWrite.Close()


    End Sub
End Class