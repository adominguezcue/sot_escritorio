Imports ZctSQL

Public Class Captura

    Private almacen As String
    Private codigo As String
    Private cantidad As String
 
    <ZctDBAtributoOrigen("A")> _
    Public Property pCodigo() As String
        Get
            Return codigo
        End Get
        Set(ByVal value As String)
            codigo = value
        End Set
    End Property
    <ZctDBAtributoOrigen("B")> _
    Public Property pCantidad() As String
        Get
            Return cantidad
        End Get
        Set(ByVal value As String)
            cantidad = value
        End Set
    End Property

    <ZctDBAtributoOrigen("C")> _
 Public Property pAlmacen() As String
        Get
            Return almacen
        End Get
        Set(ByVal value As String)
            almacen = value
        End Set
    End Property
End Class