﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;



namespace ConfiguracionIni
{
    public class ConfiguracionIni
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern uint GetPrivateProfileString(
            string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString,
            uint nSize, string lpFileName);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern bool WritePrivateProfileString(
            string lpAppName, string lpKeyName, string lpString, string lpFileName);
      
        public static string LeerConfiguracion(string IniRuta, string section, string key)
        {
            var retVal = new StringBuilder(255);

            GetPrivateProfileString(section, key, "", retVal, 255, IniRuta);

            return Desencriptar (retVal.ToString());
        }
        public static   void EscribirConfiguracion(string iniFilePath, string section, string key, string value)
        {
            WritePrivateProfileString(section, key, encriptar(value), iniFilePath);
        }
        public static string encriptar(string linea)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(linea);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Desencriptar(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
