﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Entidades;
using System.Configuration;
using Negocio.Seguridad.Roles;
using Modelo.Seguridad.Dtos;
using Transversal.Excepciones;
using Transversal.Seguridad;
using Transversal.Extensiones;
using Negocio.Seguridad.Usuarios;
using Transversal.Dependencias;
using Modelo.Seguridad.Repositorios;
using Negocio.Compartido.Empleados;
using Negocio.Seguridad.Permisos;

namespace Negocio.Seguridad.Sesiones
{
    public class ServicioSesiones : IServicioSesiones
    {


        #region services

        private IRepositorioSesiones RepositorioSesiones
        {
            get { return _repositorioSesiones.Value; }
        }

        Lazy<IRepositorioSesiones> _repositorioSesiones = new Lazy<IRepositorioSesiones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioSesiones>(); });


        private IServicioUsuariosInterno BusinessServicioUsuarios
        {
            get { return _businessServicioUsuarios.Value; }
        }

        Lazy<IServicioUsuariosInterno> _businessServicioUsuarios = new Lazy<IServicioUsuariosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuariosInterno>(); });


        private IServicioPermisos BusinessServicioPermisos
        {
            get { return _businessServicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _businessServicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        private IServicioEmpleadosCompartido BusinessServicioEmpleadosCompartido
        {
            get { return _businessServicioEmpleadosCompartido.Value; }
        }

        Lazy<IServicioEmpleadosCompartido> _businessServicioEmpleadosCompartido = new Lazy<IServicioEmpleadosCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosCompartido>(); });


        private IServicioRoles BusinessServicioRoles
        {
            get { return _businessServicioRoles.Value; }
        }

        Lazy<IServicioRoles> _businessServicioRoles = new Lazy<IServicioRoles>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRoles>(); });
        


        #endregion

        public DtoUsuario ObtenerPorToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new SOTException("Token inválido");

            var sesion = RepositorioSesiones.Obtener(m => m.Activo && m.Token == token);

            if (sesion == null)
                throw new SOTException("No existe una sesión con el token proporcionado");

            var dbUsuario = BusinessServicioUsuarios.ObtenerUsuarioPorId(sesion.IdUsuario);

            //var token = GenerateTimeToken();
            var dto = dbUsuario;
            //var date = this.GetTimeFromToken(token);
            //if(usuario.UserSessions==null) usuario.UserSessions=new TrackableCollection<UserSession>();

            //var userSession = new Sesion()
            //{
            //    Id = 0,
            //    Activo = true,
            //    Token = token,
            //    FechaCreacion = date,
            //    IdUsuario = dbUsuario.IdUsuario,
            //};

            //RepositorioSesiones.Agregar(userSession);

            //RepositorioSesiones.GuardarCambios();

            dto.InicioSesion = sesion.FechaCreacion;
            dto.TokenSesion = token;
            dto.NombreCompleto = BusinessServicioEmpleadosCompartido.ObtenerNombreCompletoEmpleado(dto.Id);

            return dto;
        }

        public DtoUsuario IniciarSesion(DtoCredencial credencial)
        {
            var dbUsuario = BusinessServicioPermisos.ObtenerUsuarioPorCredencial(credencial);


            var token = GenerateTimeToken();
            var dto = dbUsuario;
            var date = this.GetTimeFromToken(token);
            //if(usuario.UserSessions==null) usuario.UserSessions=new TrackableCollection<UserSession>();


            var userSession = new Sesion()
            {
                Id = 0,
                Activo = true,
                Token = token,
                FechaCreacion = date,
                IdUsuario = dbUsuario.IdUsuario,
            };

            RepositorioSesiones.Agregar(userSession);

            RepositorioSesiones.GuardarCambios();


            dto.InicioSesion = date;
            dto.TokenSesion = token;
            dto.NombreCompleto = BusinessServicioEmpleadosCompartido.ObtenerNombreCompletoEmpleado(dto.Id);

            return dto;
        }

        public void CerrarSesion(string sessionToken)
        {
            if (string.IsNullOrEmpty(sessionToken))
                throw new ArgumentNullException();

            var userSession = RepositorioSesiones.Obtener(m => m.Token == sessionToken && m.Activo);

            if (userSession == null)
                return;

            userSession.Activo = false;
            userSession.FechaEliminacion = DateTime.Now;

            RepositorioSesiones.Modificar(userSession);

            RepositorioSesiones.GuardarCambios();
        }

        /// <summary>
        /// METHOD
        /// <para>26/10/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        private string GenerateTimeToken()
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            //string token = Convert.ToBase64String(time.Concat(key).ToArray());
            string token = (time.Concat(key).ToArray()).ToBase32String();

            return token;

        }

        /// <summary>
        /// METHOD
        /// <para>26/10/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        private DateTime GetTimeFromToken(string token)
        {
            //byte[] data = Convert.FromBase64String(token);
            byte[] data = token.FromBase32String();
            DateTime when;

            try
            {

                when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
                if (when < DateTime.UtcNow.AddHours(-24))
                {
                    // too old
                }

            }
            catch (Exception e)
            {
                throw e;
            }

            return when;
        }
        /*
        public void GenerateRecoveryPasswordRequest(DtoUsuario DtoUsuario)
        {
            if (DtoUsuario == null)
                throw new ArgumentNullException();

            var usuario = RepositoryUsers.ObtenerElementos(m => m.Email == DtoUsuario.Email && m.Active, m => m.Rol).FirstOrDefault();

            if(usuario==null)
                throw new SOTException(Recursos.Sesiones.email_not_found_exception);

            var token = GenerateTimeToken();
            var currentDate = DateTime.Now;//this.GetTimeFromToken(token);
            //if(usuario.UserSessions==null) usuario.UserSessions=new TrackableCollection<UserSession>();


            var recoverySession = new RecoverySession()
            {
                Active = true,
                Token = token,
                FechaCreacion = currentDate,
                FechaModificacion = currentDate,
                SellByDate = currentDate.AddDays(1),
                IdUser = usuario.Id,
            };

            this.RepositoryRecoverySessions.Add(recoverySession);

            this.RepositoryRecoverySessions.GuardarCambios();

            BusinessServiceNotifications.SendNotifications("Para cambiar su contraseña haga clic en el siguiente enlace:<br><br>" +
                                 ConfigurationManager.AppSettings["urlRecoveryPass"] + "" + token, usuario.Email, "Recuperación de Contraseña", null);
        }

        public void RecoverPassword(DtoUsuario DtoUsuario, string token)
        {

            
            if (DtoUsuario == null)
                throw new ArgumentNullException();

            

            if(string.IsNullOrEmpty(DtoUsuario.Password) )
                throw new SOTException(Recursos.Usuarios.invalid_password_format_exception);

            var pass = DtoUsuario.Password;
            DtoUsuario.Password = this.BusinessServicioUsuarios.GenerateEncryptedPassword(DtoUsuario.Password);
            this.BusinessServicioUsuarios.ValidatePassword(DtoUsuario.Password, pass);

            var currentDate = DateTime.Now;

            //var usuario = RepositoryUsers.ObtenerElementos(m => m.Email == DtoUsuario.Email && m.Active, m => m.Rol).FirstOrDefault();

            var nate = this.RepositoryRecoverySessions.Get(m => m.Token == token &&m.Active, m => m.Usuario);


            Usuario usuario;

            if(nate==null)
                throw new SOTException(Recursos.Sesiones.token_not_found_exception);
            else
                usuario= nate.Usuario;

            var recoverySession = RepositoryRecoverySessions.Get(m => m.Active && m.IdUser == usuario.Id);

            if (recoverySession.SellByDate < currentDate)
                throw new SOTException(Recursos.Sesiones.recovery_session_expires_exception);

            recoverySession.FechaModificacion = currentDate;
            recoverySession.Active = false;

            this.RepositoryRecoverySessions.Modify(recoverySession);
            this.RepositoryRecoverySessions.GuardarCambios();


            var user1 = usuario;
            usuario = this.RepositoryUsers.Get(m => m.Id == user1.Id);
            usuario.Password = DtoUsuario.Password;
                //this.BusinessServicioUsuarios.GenerateEncryptedPassword(usuario.Password);

            this.RepositoryUsers.Modify(usuario);
            this.RepositoryUsers.GuardarCambios();

            


            
            //Notifier.SendMessage("Para cambiar su contraseña haga click en el siguiente enlace:<br><br>" + ConfigurationManager.AppSettings["urlRecoveryPass"] + token, usuario.Email, "Recuperación de contraseña");
        }
        */
        //public Sesion GetSession(string token)
        //{
        //    var sesion = RepositorioSesiones.Obtener(m => m.Token == token,m=>m.Usuario.Rol);

        //    //var rol = this.ServiceRoles.GetRole(sesion.Usuario.IdRole);
            
            


        //    if (sesion == null)
        //        return null;

        //    if (sesion.Activo)
        //        return sesion;

        //    return null;

        //}
    }
}
