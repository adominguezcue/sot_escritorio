﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Seguridad.Sesiones
{
    public interface IServicioSesiones
    {
        DtoUsuario ObtenerPorToken(string token);
        DtoUsuario IniciarSesion(DtoCredencial credencial);

        void CerrarSesion(string sessionToken);
        /*
        void GenerateRecoveryPasswordRequest(DtoUsuario DtoUsuario);
        void RecoverPassword(DtoUsuario DtoUsuario, string token);
        */
    }
}
