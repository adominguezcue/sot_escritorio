﻿using Modelo.Seguridad.Entidades;
using Negocio.Seguridad.ValidaroresCredenciales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Excepciones;

namespace Negocio.Seguridad.Fabricas
{
    public class FabricaValidadorCredenciales
    {
        public static IValidadorCredenciales ObtenerValidador(Credencial.TiposCredencial tipoCredencial) 
        {
            switch (tipoCredencial)
            { 
                case Credencial.TiposCredencial.Contrasena:
                    return new ValidadorContrasenas();
                case Credencial.TiposCredencial.Huella:
                    return new ValidadorHuella();
                default:
                    throw new SOTException("Tipo de credencial no soportado");
            }
        }
    }
}
