﻿using Modelo;
using Modelo.Seguridad.Dtos;
using Modelo.Seguridad.Repositorios;
using Negocio.Seguridad.Fabricas;
using Negocio.Seguridad.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Negocio.Seguridad.Extensiones;
using Transversal.Excepciones.Seguridad;
using Negocio.Compartido.Empleados;
using Negocio.Seguridad.Roles;

namespace Negocio.Seguridad.Permisos
{
    public class ServicioPermisos : IServicioPermisos
    {
        private IServicioEmpleadosCompartido BusinessServicioEmpleadosCompartido
        {
            get { return _businessServicioEmpleadosCompartido.Value; }
        }

        Lazy<IServicioEmpleadosCompartido> _businessServicioEmpleadosCompartido = new Lazy<IServicioEmpleadosCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosCompartido>(); });

        Lazy<IServicioRolesInterno> _businessServiceRoles = new Lazy<IServicioRolesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRolesInterno>(); });

        private IServicioRolesInterno BusinessServiceRoles
        {
            get { return _businessServiceRoles.Value; }
        }

        private IRepositorioSesiones RepositorioSesiones
        {
            get { return _repositorioSesiones.Value; }
        }

        Lazy<IRepositorioSesiones> _repositorioSesiones = new Lazy<IRepositorioSesiones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioSesiones>(); });

        //private IRepositorioPermisos RepositorioPermisos
        //{
        //    get { return _repositorioPermisos.Value; }
        //}

        //Lazy<IRepositorioPermisos> _repositorioPermisos = new Lazy<IRepositorioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPermisos>(); });

        private IRepositorioUsuarios RepositorioUsuarios
        {
            get { return _repositorioUsuarios.Value; }
        }

        Lazy<IRepositorioUsuarios> _repositorioUsuarios = new Lazy<IRepositorioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioUsuarios>(); });

        private IServicioUsuariosInterno ServicioUsuarios
        {
            get { return _ServicioUsuarios.Value; }
        }

        Lazy<IServicioUsuariosInterno> _ServicioUsuarios = new Lazy<IServicioUsuariosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuariosInterno>(); });

        private IServicioRolesInterno ServicioRoles
        {
            get { return _ServicioRoles.Value; }
        }

        Lazy<IServicioRolesInterno> _ServicioRoles = new Lazy<IServicioRolesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRolesInterno>(); });

        public void ValidarPermisos()
        {
#warning No valida nada
        }

        public bool VerificarPermisos(DtoUsuario usuario, Modelo.Seguridad.Dtos.DtoPermisos permisos) 
        {
            return usuario.Permisos != null && usuario.Permisos.CompararPermisos(permisos).Count() == 0;
        }

        public DtoUsuario ObtenerUsuarioPorCredencial(DtoCredencial credencial)
        {

            if (credencial.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena)
            {
                if (string.IsNullOrWhiteSpace(credencial.IdentificadorUsuario))
                    throw new SOTException(Recursos.Permisos.alias_requerido_excepcion);

                return ServicioUsuarios.ObtenerUsuarioPorCredencial(credencial);
            }
            else if (credencial.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella) 
            {
                var credenciales = RepositorioUsuarios.ObtenerCredencialesActivas(credencial.TipoCredencial);

                var validador = FabricaValidadorCredenciales.ObtenerValidador(credencial.TipoCredencial);

                foreach (var credencialDb in credenciales)
                {
                    try
                    {
                        validador.ValidarCredencial(credencialDb, credencial);
                    }
                    catch 
                    {
                        continue;
                    }

                    var usuario = RepositorioUsuarios.ObtenerUsuarioConRol(credencialDb.IdUsuario);

                    var idRol = BusinessServicioEmpleadosCompartido.ObtenerIdRol(usuario.IdEmpleado);

                    usuario.RolTmp = this.BusinessServiceRoles.ObtenerRolConPermisos(idRol);

                    return (DtoUsuario)usuario;
                }
            }

            throw new SOTException(Recursos.Usuarios.invalid_user_or_credentials_exception);
        }

        public DtoUsuario ObtenerUsuarioPorCredencial()
        {
            return ObtenerUsuarioPorCredencial(GestorCredenciales.Instancia.ObtenerCredencial());
        }

        public void ValidarPermisos(DtoUsuario usuario, Modelo.Seguridad.Dtos.DtoPermisos permisos)
        {
            if (usuario == null)
                throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);
#if !DEBUG_TEST
            List<string> permisosFaltantes = null;

            if (usuario.Permisos == null || (permisosFaltantes = usuario.Permisos.CompararPermisos(permisos).ToList()).Count > 0)
                throw new SOTPermisosException(Recursos.Permisos.no_permission_exception) { Permisos = permisosFaltantes };
#endif
        }

        public void ValidarPermisos(int idRol, Modelo.Seguridad.Dtos.DtoPermisos permisos)
        {
            var rol = ServicioRoles.ObtenerRol(idRol);

            if (rol == null)
                throw new SOTException(Recursos.Roles.role_null_exception);

            var permisosRol = Newtonsoft.Json.JsonConvert.DeserializeObject<DtoPermisos>(rol.Permisos);

            List<string> permisosFaltantes = null;

            if (permisosRol == null || (permisosFaltantes = permisosRol.CompararPermisos(permisos).ToList()).Count > 0)
                throw new SOTPermisosException(Recursos.Permisos.no_permission_exception) { Permisos = permisosFaltantes };

        }

        //public List<Modelo.Seguridad.Entidades.Permisos> ObtenerPermisos(DtoUsuario usuario)
        //{
        //    var permisos =  RepositorioPermisos.ObtenerTodo(m => m.Rol).ToList();

        //    foreach (var permiso in permisos)
        //        permiso.RolTmp = permiso.Rol.Nombre;

        //    return permisos;
        //}
    }
}
