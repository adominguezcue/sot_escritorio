﻿using Modelo;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Negocio.Seguridad.Permisos
{
    public interface IServicioPermisos
    {
        //[Obsolete("Marcado como obsoleto de una vez, reemplazar por uno que en verdad valide permisos")]
        //void ValidarPermisos();
        void ValidarPermisos(DtoUsuario usuario, DtoPermisos permisos);
        bool VerificarPermisos(DtoUsuario usuario, DtoPermisos permisos);
        DtoUsuario ObtenerUsuarioPorCredencial(DtoCredencial credencial);
        DtoUsuario ObtenerUsuarioPorCredencial();

        //List<Modelo.Seguridad.Entidades.Permisos> ObtenerPermisos(DtoUsuario usuario);

        void ValidarPermisos(int idRol, DtoPermisos permisos);
    }
}
