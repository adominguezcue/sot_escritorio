﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Seguridad.Permisos
{
    public abstract class GestorCredenciales
    {
        private static object bloqueador = new object();
        private static GestorCredenciales _instancia;

        public abstract Modelo.Seguridad.Dtos.DtoCredencial ObtenerCredencial();

        internal static GestorCredenciales Instancia 
        {
            get 
            {
                lock (bloqueador)
                {
                    return _instancia ?? (_instancia = Transversal.Dependencias.FabricaDependencias.Instancia.Resolver<GestorCredenciales>());
                }
            }
        }
    }
}
