﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Entidades;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Modelo.Seguridad.Dtos;
using System.Transactions;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Negocio.Seguridad.Usuarios;
using Modelo.Seguridad.Repositorios;
using Transversal.Dependencias;
using Transversal.Extensiones;
using Newtonsoft;
using Transversal.Utilidades;
using Negocio.Seguridad.Permisos;
using Dominio.Nucleo.Entidades;
using Negocio.Compartido.Empleados;

namespace Negocio.Seguridad.Roles
{
    public class ServicioRoles : IServicioRolesInterno
    {
        #region services

        private IServicioEmpleadosCompartido BusinessServicioEmpleadosCompartido
        {
            get { return _businessServicioEmpleadosCompartido.Value; }
        }

        Lazy<IServicioEmpleadosCompartido> _businessServicioEmpleadosCompartido = new Lazy<IServicioEmpleadosCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosCompartido>(); });


        private IServicioUsuarios BusinessServicioUsuarios 
        {
            get { return _businessServicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _businessServicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });

        private IServicioPermisos BusinessServicioPermisos
        {
            get { return _businessServicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _businessServicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });
        

        private IRepositorioRoles RepositorioRoles
        {
            get { return _repositoryRoles.Value; }
        }

        Lazy<IRepositorioRoles> _repositoryRoles = new Lazy<IRepositorioRoles>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioRoles>(); });


        #endregion

        /// <summary>
        /// METHOD
        /// <para>27/09/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        public List<Rol> GetRoles()
        {
            var roles = this.RepositorioRoles.ObtenerElementos(m => m.Activo);
            return roles.ToList();
        }

        /// <summary>
        /// METHOD
        /// <para>06/11/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        public void CrearRol(Rol rol, DtoUsuario usuario)
        {
            if (rol == null)
                throw new SOTException(Recursos.Roles.role_null_exception);

            BusinessServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearRoles = true });

            this.ValidateRole(rol);

            var fechaActual = DateTime.Now;

            rol.Activo = true;
            rol.FechaCreacion = fechaActual;
            rol.FechaModificacion = fechaActual;

            rol.IdUsuarioCreo = usuario.Id;
            rol.IdUsuarioModifico = usuario.Id;

            foreach (var permisoPago in rol.PermisosPagos.Where(m => m.Activo))
            {
                permisoPago.FechaCreacion = fechaActual;
                permisoPago.FechaModificacion = fechaActual;

                permisoPago.IdUsuarioCreo = usuario.Id;
                permisoPago.IdUsuarioModifico = usuario.Id;
            }

            //if (string.IsNullOrWhiteSpace(rol.Description))
            //    rol.Description = "";


            this.RepositorioRoles.Agregar(rol);

            this.RepositorioRoles.GuardarCambios();

            //var r2 = this.RepositorioRoles.Obtener(m => m.Id == rol.Id);

            //var permission = r2.Permisos.ToObject<DtoPermisos>();
            //permission.IdRol = r2.Id;


            //r2.Permisos = permission.ToJsonString();


            //this.RepositorioRoles.Modificar(r2);

            //this.RepositorioRoles.GuardarCambios();
            //r2.Permissions.



        }

        /// <summary>
        /// METHOD
        /// <para>06/11/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        public void ModificarRol(Rol rolM, DtoUsuario usuario)
        {
            BusinessServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarRoles = true });


            this.ValidateRole(rolM);


            var rol = RepositorioRoles.ObtenerRolConPermisos(rolM.Id);

            if (rol == null)
                throw new SOTException(Recursos.Roles.role_null_exception);

            rol.SincronizarPrimitivas(rolM);

            //rol.Permisos.SincronizarPrimitivas(rolM.Permisos);

            #region permisos pagos

            foreach (var par in (from ctOriginal in rol.PermisosPagos
                                 join ctMod in rolM.PermisosPagos on ctOriginal.Id equals ctMod.Id
                                 select new
                                 {
                                     ctOriginal,
                                     ctMod
                                 }).ToList())
            {
                par.ctOriginal.SincronizarPrimitivas(par.ctMod);
            }
            foreach (var cNuevo in rolM.PermisosPagos.Where(m => m.Activo && m.Id == 0))
            {
                var nuevoPP = new PermisoPago();

                nuevoPP.SincronizarPrimitivas(cNuevo);

                rol.PermisosPagos.Add(nuevoPP);
            }

            #endregion

            //if (!rol.Activo)
            //{
            //    this.DeleteRole(rol.Id, usuario);
            //    return;
            //}

            //roleReal.Name = rol.Name;
            //roleReal.Permissions = roleReal.Permissions;
            var fechaActual = DateTime.Now;

            rol.FechaModificacion = fechaActual;
            rol.IdUsuarioModifico = usuario.Id;

            //rol.Permisos.EntidadEstado = EntidadEstados.Modificado;

            //var realRole = this.RepositorioRoles.Obtener(m => m.Id == rol.Id);

            //realRole.Synchronize(rol);

            foreach (var permisoPago in rol.PermisosPagos.Where(m => m.Id != 0 || (m.Id == 0 && m.Activo)))
            {
                if (permisoPago.Id == 0)
                {
                    permisoPago.FechaCreacion = fechaActual;
                    permisoPago.IdUsuarioCreo = usuario.Id;
                }
                else if (!permisoPago.Activo)
                {
                    permisoPago.FechaEliminacion = fechaActual;
                    permisoPago.IdUsuarioElimino = usuario.Id;
                }

                permisoPago.IdUsuarioModifico = usuario.Id;
                permisoPago.FechaModificacion = fechaActual;

                permisoPago.EntidadEstado = permisoPago.Id == 0 ? EntidadEstados.Creado : EntidadEstados.Modificado;
            }

            this.RepositorioRoles.Modificar(rol);
            this.RepositorioRoles.GuardarCambios();

        }

        ///// <summary>
        ///// METHOD
        ///// <para>09/11/2015</para>
        ///// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        ///// </summary>
        //public void EditPermisionRole(Modelo.Seguridad.Entidades.Permisos permisos, DtoUsuario DtoUsuario)
        //{
        //    if (!DtoUsuario.Permisos.ModificarPermisos)
        //        throw new SOTException(Recursos.Permisos.no_permission_exception);

        //    var rol = this.RepositorioRoles.Obtener(m => m.Id == permisos.IdRol, m => m.Permisos);

        //    if (rol.Permisos != null)
        //        rol.Permisos.SincronizarPrimitivas(permisos);
        //    else
        //        rol.Permisos = permisos;

        //    this.ModificarRol(rol, DtoUsuario);
        //}

        public Rol ObtenerRolConPermisos(int idRol, DtoUsuario usuario)
        {
            this.BusinessServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarRoles = true });

            var rol = this.RepositorioRoles.ObtenerRolConPermisos(idRol);

            return rol;
        }

        public Rol GetRole(string name, DtoUsuario DtoUsuario)
        {
            BusinessServicioPermisos.ValidarPermisos(DtoUsuario, new DtoPermisos { ConsultarRoles = true });

            var upperName = name.Trim().ToUpper();

            var rol = this.RepositorioRoles.Obtener(m => m.Nombre.ToUpper() == upperName && m.Activo);

            return rol;
        }

        /// <summary>
        /// Recibe una instancia de DtoPaginationRequest con los datos necesarios para realizar una
        /// consulta, y retorna un DtoPaginationResponse con los resultados
        /// </summary>
        /// <param name="dtoPaginationRequest"></param>
        /// <param name="DtoUsuario"></param>
        /// <returns></returns>
        public List<Rol> ObtenerRoles(DtoUsuario DtoUsuario, string nombre = null, int pagina = 0, int elementos = 0)
        {
            //buscarle un uso al usuario
            if (DtoUsuario == null)
                throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

            BusinessServicioPermisos.ValidarPermisos(DtoUsuario, new DtoPermisos { ConsultarRoles = true });

            Expression<Func<Rol, bool>> filter = m => m.Activo;

            if (!string.IsNullOrWhiteSpace(nombre))
                filter = filter.Compose(m => m.Nombre.ToUpper().Contains(nombre.ToUpper()), Expression.AndAlso);

            var roles = pagina == 0 && elementos == 0 ?
                this.RepositorioRoles.ObtenerElementos(filter).OrderBy(m => m.Nombre) :
                this.RepositorioRoles.ObtenerElementos(filter, m => m.Nombre, pagina, elementos);

            return roles.ToList();
        }

        ///// <summary>
        ///// METHOD
        ///// <para>09/11/2015</para>
        ///// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        ///// </summary>
        //private void DeleteRole(int idRol, DtoUsuario usuario)
        //{
        //    if (!usuario.Permisos.EliminarRoles)
        //        throw new SOTException(Recursos.Permisos.no_permission_exception);

        //    var realRole = this.RepositorioRoles.Obtener(m => m.Id == idRol);

        //    ValidateDependencies(realRole);

        //    realRole.Activo = false;
        //    realRole.FechaEliminacion = DateTime.Now;
        //    realRole.IdUsuarioElimino = usuario.Id;

        //    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
        //    {
                

        //        this.RepositorioRoles.Modificar(realRole);
        //        this.RepositorioRoles.GuardarCambios();

        //        scope.Complete();
        //    }
        //}

        private void ValidateDependencias(Rol rol)
        {
            if (BusinessServicioEmpleadosCompartido.VerificarRolEnUso(rol.Id))//BusinessServicioUsuarios.VerificarRolEnUso(rol.Id))
                throw new SOTException(Recursos.Generales.can_not_delete_exception, rol.Nombre);
        }


        /// <summary>
        /// METHOD
        /// <para>06/11/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        private void ValidateRole(Rol rol)
        {
            if (string.IsNullOrEmpty(rol.Nombre))
                throw new SOTException(Recursos.Generales.empty_field_exception, "Nombre");

            //if (string.IsNullOrEmpty(rol.Description))
            //    throw new SOTException(Recursos.General.empty_field_exception, "Descripción");

            if (!UtilidadesRegex.SoloLetras(rol.Nombre))//, "/^[a-zA-ZÑñÁ-Úá-úüÜ]+$/"))
                throw new SOTException(Recursos.Roles.invalid_name_exception);

            var name = rol.Nombre.Trim().ToUpper();
            
            if (this.RepositorioRoles.Alguno(m => m.Nombre.ToUpper() == name && m.Id != rol.Id && m.Activo))
                throw new SOTException(Recursos.Roles.duplicate_exception, rol.Nombre);
        }


        public void ValidateRole(int idRol)
        {
            if (!this.RepositorioRoles.Alguno(m => m.Id == idRol && m.Activo))
                throw new SOTException(Recursos.Roles.role_null_exception);
        }


        public void EliminarRol(int idRol, DtoUsuario usuario)
        {
            BusinessServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarRoles = true });

            var rol = RepositorioRoles.ObtenerRolConPermisos(idRol);

            if (rol == null)
                throw new SOTException(Recursos.Roles.role_null_exception);

            ValidateDependencias(rol);
            
            var fechaActual = DateTime.Now;

            rol.Activo = false;
            rol.FechaEliminacion = fechaActual;
            rol.FechaModificacion = fechaActual;
            rol.IdUsuarioElimino = usuario.Id;
            rol.IdUsuarioModifico = usuario.Id;

            foreach (var permisoPago in rol.PermisosPagos)
            {
                permisoPago.Activo = false;
                permisoPago.FechaEliminacion = fechaActual;
                permisoPago.FechaModificacion = fechaActual;
                permisoPago.IdUsuarioElimino = usuario.Id;
                permisoPago.IdUsuarioModifico = usuario.Id;
            }

            RepositorioRoles.Modificar(rol);
            RepositorioRoles.GuardarCambios();
        }

        #region métodos internal

        Rol IServicioRolesInterno.ObtenerRolConPermisos(int idRol)
        {
            //if (!DtoUsuario.Permission.VerRoles)
            //    throw new SOTException(Recursos.Permisos.no_permission_exception);

            var rol = this.RepositorioRoles.ObtenerRolConPermisos(idRol);

            return rol;
        }

        Rol IServicioRolesInterno.ObtenerRol(int idRol)
        {
            //if (!DtoUsuario.Permission.VerRoles)
            //    throw new SOTException(Recursos.Permisos.no_permission_exception);

            var rol = this.RepositorioRoles.Obtener(m=> m.Id == idRol);

            return rol;
        }

        #endregion
    }
}
