﻿using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Seguridad.Roles
{
    internal interface IServicioRolesInterno : IServicioRoles
    {
        Rol ObtenerRolConPermisos(int idRol);
        Rol ObtenerRol(int idRol);
    }
}
