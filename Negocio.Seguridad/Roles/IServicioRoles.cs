﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Seguridad.Roles
{
    public interface IServicioRoles
    {

        /// <summary>
        /// METHOD
        /// <para>27/09/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        List<Rol> GetRoles();

        /// <summary>
        /// METHOD
        /// <para>06/11/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        void CrearRol(Rol rol, DtoUsuario DtoUsuario);

        /// <summary>
        /// METHOD
        /// <para>06/11/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        void ModificarRol(Rol rol, DtoUsuario DtoUsuario);

        ///// <summary>
        ///// METHOD
        ///// <para>09/11/2015</para>
        ///// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        ///// </summary>
        //void EditPermisionRole(Modelo.Seguridad.Entidades.Permisos permision, DtoUsuario DtoUsuario);

        /// <summary>
        /// Retorna el rol activo con el id proporcionado cargado con sus permisos
        /// </summary>
        /// <param name="idRol"></param>
        /// <param name="DtoUsuario"></param>
        /// <returns></returns>
        Rol ObtenerRolConPermisos(int idRol, DtoUsuario DtoUsuario);

        Rol GetRole(string name, DtoUsuario DtoUsuario);

        /// <summary>
        /// Recibe una instancia de DtoPaginationRequest con los datos necesarios para realizar una
        /// consulta, y retorna un DtoPaginationResponse con los resultados
        /// </summary>
        /// <param name="DtoUsuario"></param>
        /// <param name="nombre"></param>
        /// <returns></returns>
        List<Rol> ObtenerRoles(DtoUsuario DtoUsuario, string nombre = null, int pagina = 0, int elementos = 0);

        void ValidateRole(int idRole);
        /// <summary>
        /// Marca un rol como eliminado, siempre y cuando no esté ligado a un usuario activo
        /// </summary>
        /// <param name="idRol"></param>
        /// <param name="UsuarioActual"></param>
        void EliminarRol(int idRol, DtoUsuario UsuarioActual);
    }
}
