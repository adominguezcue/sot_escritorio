﻿using Modelo.Seguridad.Dtos;
using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Excepciones;
using Transversal.Seguridad;
using Transversal.Utilidades;

namespace Negocio.Seguridad.ValidaroresCredenciales
{
    public class ValidadorContrasenas : IValidadorCredenciales
    {
        public void ValidarCredencial(Credencial credencial, DtoCredencial valorConfirmacion)
        {
            ValidatePassword(credencial.Valor, valorConfirmacion.ValorVerificarStr);
        }

        void ValidatePassword(byte[] encryptedPassword, string passwordConfirmation)
        {
            if (encryptedPassword == null || encryptedPassword.Length == 0)
                throw new SOTException(Recursos.Generales.empty_field_exception, "Contraseña");

            if (string.IsNullOrEmpty(passwordConfirmation))
                throw new SOTException(Recursos.Generales.empty_field_exception, "Confirmar contraseña");

            if (!Encriptador.Comparar(passwordConfirmation, encryptedPassword))
                throw new SOTException(Recursos.Usuarios.invalid_password_confirmation_exception);

#warning definir un formato de contraseña
            //if (!UtilidadesRegex.Contrasena(passwordConfirmation))
            //    throw new SOTException(Recursos.Usuarios.invalid_password_format_exception);
        }
    }
}
