﻿using Modelo.Seguridad.Dtos;
using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Excepciones;
using Transversal.Seguridad;

namespace Negocio.Seguridad.ValidaroresCredenciales
{
    public class ValidadorHuella : IValidadorCredenciales
    {
        public void ValidarCredencial(Credencial credencial, DtoCredencial valorConfirmacion)
        {
#if DEBUG_TEST
#warning DEBUG_TEST no valida las huellas digitales, esto es así porque se necesitaba ejecutar código de inserción de huellas.
#else
            if (!Transversal.Seguridad.GestorHuella.Comparar(credencial.Valor, valorConfirmacion.ValorVerificarBin))
                throw new SOTException(Recursos.Usuarios.invalid_fingerprint_confirmation_exception);
#endif
        }
    }
}
