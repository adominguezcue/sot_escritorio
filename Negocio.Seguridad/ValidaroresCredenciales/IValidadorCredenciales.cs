﻿using Modelo.Seguridad.Dtos;
using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Seguridad.ValidaroresCredenciales
{
    public interface IValidadorCredenciales
    {
        void ValidarCredencial(Credencial credencial, DtoCredencial valorConfirmacion);
    }
}
