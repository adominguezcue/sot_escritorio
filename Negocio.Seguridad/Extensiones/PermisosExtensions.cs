﻿

using System;
using System.Collections.Generic;
using Transversal.Extensiones;

namespace Negocio.Seguridad.Extensiones
{
    public static class PermisosExtensions
    {
        public static IEnumerable<string> CompararPermisos(this Modelo.Seguridad.Dtos.DtoPermisos item, Modelo.Seguridad.Dtos.DtoPermisos comparador)
        {
            if(comparador.AccesoModuloHotel && !item.AccesoModuloHotel)
                yield return comparador.GetDescripcion(m=> m.AccesoModuloHotel);
            if(comparador.AccesoModuloRestaurante && !item.AccesoModuloRestaurante)
                yield return comparador.GetDescripcion(m=> m.AccesoModuloRestaurante);
            if(comparador.AccesoModuloCocina && !item.AccesoModuloCocina)
                yield return comparador.GetDescripcion(m=> m.AccesoModuloCocina);
            if(comparador.AccesoModuloBar && !item.AccesoModuloBar)
                yield return comparador.GetDescripcion(m=> m.AccesoModuloBar);
            if(comparador.ConsultarTiposHabitacion && !item.ConsultarTiposHabitacion)
                yield return comparador.GetDescripcion(m=> m.ConsultarTiposHabitacion);
            if(comparador.CrearTiposHabitacion && !item.CrearTiposHabitacion)
                yield return comparador.GetDescripcion(m=> m.CrearTiposHabitacion);
            if(comparador.ModificarTiposHabitacion && !item.ModificarTiposHabitacion)
                yield return comparador.GetDescripcion(m=> m.ModificarTiposHabitacion);
            if(comparador.EliminarTiposHabitacion && !item.EliminarTiposHabitacion)
                yield return comparador.GetDescripcion(m=> m.EliminarTiposHabitacion);
            if(comparador.ConsultarHabitacionesTodas && !item.ConsultarHabitacionesTodas)
                yield return comparador.GetDescripcion(m=> m.ConsultarHabitacionesTodas);
            if(comparador.ConsultarHabitacionesHabilitadas && !item.ConsultarHabitacionesHabilitadas)
                yield return comparador.GetDescripcion(m=> m.ConsultarHabitacionesHabilitadas);
            if(comparador.ConsultarHabitacionesPreparadas && !item.ConsultarHabitacionesPreparadas)
                yield return comparador.GetDescripcion(m=> m.ConsultarHabitacionesPreparadas);
            if(comparador.AdministrarHabitaciones && !item.AdministrarHabitaciones)
                yield return comparador.GetDescripcion(m=> m.AdministrarHabitaciones);
            if(comparador.ModificarHabitaciones && !item.ModificarHabitaciones)
                yield return comparador.GetDescripcion(m=> m.ModificarHabitaciones);
            if(comparador.AsignarHabitaciones && !item.AsignarHabitaciones)
                yield return comparador.GetDescripcion(m=> m.AsignarHabitaciones);
            if(comparador.CobrarHabitaciones && !item.CobrarHabitaciones)
                yield return comparador.GetDescripcion(m=> m.CobrarHabitaciones);
            if(comparador.PonerHabitacionesLimpieza && !item.PonerHabitacionesLimpieza)
                yield return comparador.GetDescripcion(m=> m.PonerHabitacionesLimpieza);
            if(comparador.PonerHabitacionesSupervision && !item.PonerHabitacionesSupervision)
                yield return comparador.GetDescripcion(m=> m.PonerHabitacionesSupervision);
            if(comparador.DesocuparHabitaciones && !item.DesocuparHabitaciones)
                yield return comparador.GetDescripcion(m=> m.DesocuparHabitaciones);
            if(comparador.LimpiarHabitaciones && !item.LimpiarHabitaciones)
                yield return comparador.GetDescripcion(m=> m.LimpiarHabitaciones);
            if(comparador.SupervisarHabitaciones && !item.SupervisarHabitaciones)
                yield return comparador.GetDescripcion(m=> m.SupervisarHabitaciones);
            if(comparador.CancelarOcupacionesHabitacion && !item.CancelarOcupacionesHabitacion)
                yield return comparador.GetDescripcion(m=> m.CancelarOcupacionesHabitacion);
            if(comparador.BloquearHabitaciones && !item.BloquearHabitaciones)
                yield return comparador.GetDescripcion(m=> m.BloquearHabitaciones);
            if(comparador.DesbloquearHabitaciones && !item.DesbloquearHabitaciones)
                yield return comparador.GetDescripcion(m=> m.DesbloquearHabitaciones);
            if(comparador.HabilitarVentasHabitacion && !item.HabilitarVentasHabitacion)
                yield return comparador.GetDescripcion(m=> m.HabilitarVentasHabitacion);
            if(comparador.ConsultarDetallesHabitacion && !item.ConsultarDetallesHabitacion)
                yield return comparador.GetDescripcion(m=> m.ConsultarDetallesHabitacion);
            if(comparador.PrepararHabitaciones && !item.PrepararHabitaciones)
                yield return comparador.GetDescripcion(m=> m.PrepararHabitaciones);
            if(comparador.MarcarHabitacionComoReservada && !item.MarcarHabitacionComoReservada)
                yield return comparador.GetDescripcion(m=> m.MarcarHabitacionComoReservada);
            if(comparador.AsignarPaquetes && !item.AsignarPaquetes)
                yield return comparador.GetDescripcion(m=> m.AsignarPaquetes);
            if(comparador.CobrarPaquetes && !item.CobrarPaquetes)
                yield return comparador.GetDescripcion(m=> m.CobrarPaquetes);
            if(comparador.CrearPaquetes && !item.CrearPaquetes)
                yield return comparador.GetDescripcion(m=> m.CrearPaquetes);
            if(comparador.ModificarPaquetes && !item.ModificarPaquetes)
                yield return comparador.GetDescripcion(m=> m.ModificarPaquetes);
            if(comparador.EliminarPaquetes && !item.EliminarPaquetes)
                yield return comparador.GetDescripcion(m=> m.EliminarPaquetes);
            if(comparador.ConsultarPaquetes && !item.ConsultarPaquetes)
                yield return comparador.GetDescripcion(m=> m.ConsultarPaquetes);
            if(comparador.AsignarPersonasExtra && !item.AsignarPersonasExtra)
                yield return comparador.GetDescripcion(m=> m.AsignarPersonasExtra);
            if(comparador.AsignarPersonasExtraSobreMax && !item.AsignarPersonasExtraSobreMax)
                yield return comparador.GetDescripcion(m=> m.AsignarPersonasExtraSobreMax);
            if(comparador.AsignarPersonaExtraPosterior && !item.AsignarPersonaExtraPosterior)
                yield return comparador.GetDescripcion(m=> m.AsignarPersonaExtraPosterior);
            if(comparador.CobrarPersonasExtra && !item.CobrarPersonasExtra)
                yield return comparador.GetDescripcion(m=> m.CobrarPersonasExtra);
            if(comparador.AsignarHorasExtra && !item.AsignarHorasExtra)
                yield return comparador.GetDescripcion(m=> m.AsignarHorasExtra);
            if(comparador.CrearReservaciones && !item.CrearReservaciones)
                yield return comparador.GetDescripcion(m=> m.CrearReservaciones);
            if(comparador.ConsultarReservaciones && !item.ConsultarReservaciones)
                yield return comparador.GetDescripcion(m=> m.ConsultarReservaciones);
            if(comparador.ModificarReservaciones && !item.ModificarReservaciones)
                yield return comparador.GetDescripcion(m=> m.ModificarReservaciones);
            if(comparador.EliminarReservaciones && !item.EliminarReservaciones)
                yield return comparador.GetDescripcion(m=> m.EliminarReservaciones);
            if(comparador.CambiarHabitacionReservacion && !item.CambiarHabitacionReservacion)
                yield return comparador.GetDescripcion(m=> m.CambiarHabitacionReservacion);
            if(comparador.AsignarReservaciones && !item.AsignarReservaciones)
                yield return comparador.GetDescripcion(m=> m.AsignarReservaciones);
            if(comparador.EliminarAnclajeReservaciones && !item.EliminarAnclajeReservaciones)
                yield return comparador.GetDescripcion(m=> m.EliminarAnclajeReservaciones);
            if (comparador.PermiteReservacionMismoDia && !item.PermiteReservacionMismoDia)
                yield return comparador.GetDescripcion(m => m.PermiteReservacionMismoDia);
            if (comparador.CrearComandas && !item.CrearComandas)
                yield return comparador.GetDescripcion(m=> m.CrearComandas);
            if(comparador.ConsultarComandas && !item.ConsultarComandas)
                yield return comparador.GetDescripcion(m=> m.ConsultarComandas);
            if(comparador.ModificarComandas && !item.ModificarComandas)
                yield return comparador.GetDescripcion(m=> m.ModificarComandas);
            if(comparador.EliminarComandas && !item.EliminarComandas)
                yield return comparador.GetDescripcion(m=> m.EliminarComandas);
            if(comparador.Meserear && !item.Meserear)
                yield return comparador.GetDescripcion(m=> m.Meserear);
            if(comparador.PrepararProductos && !item.PrepararProductos)
                yield return comparador.GetDescripcion(m=> m.PrepararProductos);
            if(comparador.CobrarComanda && !item.CobrarComanda)
                yield return comparador.GetDescripcion(m=> m.CobrarComanda);
            if(comparador.AsignarRecamarerasLimpieza && !item.AsignarRecamarerasLimpieza)
                yield return comparador.GetDescripcion(m=> m.AsignarRecamarerasLimpieza);
            if(comparador.AsignarSupervisorLimpieza && !item.AsignarSupervisorLimpieza)
                yield return comparador.GetDescripcion(m=> m.AsignarSupervisorLimpieza);
            if(comparador.FinalizarLimpieza && !item.FinalizarLimpieza)
                yield return comparador.GetDescripcion(m=> m.FinalizarLimpieza);
            if(comparador.ConsultarTareaLimpieza && !item.ConsultarTareaLimpieza)
                yield return comparador.GetDescripcion(m=> m.ConsultarTareaLimpieza);
            if(comparador.CrearUsuarios && !item.CrearUsuarios)
                yield return comparador.GetDescripcion(m=> m.CrearUsuarios);
            if(comparador.ModificarUsuarios && !item.ModificarUsuarios)
                yield return comparador.GetDescripcion(m=> m.ModificarUsuarios);
            if(comparador.ConsultarUsuarios && !item.ConsultarUsuarios)
                yield return comparador.GetDescripcion(m=> m.ConsultarUsuarios);
            if(comparador.EliminarUsuarios && !item.EliminarUsuarios)
                yield return comparador.GetDescripcion(m=> m.EliminarUsuarios);
            if(comparador.CrearRoles && !item.CrearRoles)
                yield return comparador.GetDescripcion(m=> m.CrearRoles);
            if(comparador.ModificarRoles && !item.ModificarRoles)
                yield return comparador.GetDescripcion(m=> m.ModificarRoles);
            if(comparador.ConsultarRoles && !item.ConsultarRoles)
                yield return comparador.GetDescripcion(m=> m.ConsultarRoles);
            if(comparador.EliminarRoles && !item.EliminarRoles)
                yield return comparador.GetDescripcion(m=> m.EliminarRoles);
            if(comparador.CrearPermisos && !item.CrearPermisos)
                yield return comparador.GetDescripcion(m=> m.CrearPermisos);
            if(comparador.ModificarPermisos && !item.ModificarPermisos)
                yield return comparador.GetDescripcion(m=> m.ModificarPermisos);
            if(comparador.ConsultarPermisos && !item.ConsultarPermisos)
                yield return comparador.GetDescripcion(m=> m.ConsultarPermisos);
            if(comparador.EliminarPermisos && !item.EliminarPermisos)
                yield return comparador.GetDescripcion(m=> m.EliminarPermisos);
            if(comparador.CrearEmpleado && !item.CrearEmpleado)
                yield return comparador.GetDescripcion(m=> m.CrearEmpleado);
            if(comparador.ModificarEmpleado && !item.ModificarEmpleado)
                yield return comparador.GetDescripcion(m=> m.ModificarEmpleado);
            if(comparador.ConsultarEmpleado && !item.ConsultarEmpleado)
                yield return comparador.GetDescripcion(m=> m.ConsultarEmpleado);
            if(comparador.EliminarEmpleado && !item.EliminarEmpleado)
                yield return comparador.GetDescripcion(m=> m.EliminarEmpleado);
            if(comparador.CrearIncidenciaTurno && !item.CrearIncidenciaTurno)
                yield return comparador.GetDescripcion(m=> m.CrearIncidenciaTurno);
            if(comparador.ModificarIncidenciaTurno && !item.ModificarIncidenciaTurno)
                yield return comparador.GetDescripcion(m=> m.ModificarIncidenciaTurno);
            if(comparador.CancelarIncidenciaTurno && !item.CancelarIncidenciaTurno)
                yield return comparador.GetDescripcion(m=> m.CancelarIncidenciaTurno);
            if(comparador.EliminarIncidenciaTurno && !item.EliminarIncidenciaTurno)
                yield return comparador.GetDescripcion(m=> m.EliminarIncidenciaTurno);
            if(comparador.ConsultarIncidenciaTurno && !item.ConsultarIncidenciaTurno)
                yield return comparador.GetDescripcion(m=> m.ConsultarIncidenciaTurno);
            if(comparador.CrearOrdenTrabajo && !item.CrearOrdenTrabajo)
                yield return comparador.GetDescripcion(m=> m.CrearOrdenTrabajo);
            if(comparador.ModificarOrdenTrabajo && !item.ModificarOrdenTrabajo)
                yield return comparador.GetDescripcion(m=> m.ModificarOrdenTrabajo);
            if(comparador.EliminarOrdenTrabajo && !item.EliminarOrdenTrabajo)
                yield return comparador.GetDescripcion(m=> m.EliminarOrdenTrabajo);
            if(comparador.ConsultarOrdenTrabajo && !item.ConsultarOrdenTrabajo)
                yield return comparador.GetDescripcion(m=> m.ConsultarOrdenTrabajo);
            if(comparador.AutorizarOrdenTrabajo && !item.AutorizarOrdenTrabajo)
                yield return comparador.GetDescripcion(m=> m.AutorizarOrdenTrabajo);
            if(comparador.CrearOrdenCompra && !item.CrearOrdenCompra)
                yield return comparador.GetDescripcion(m=> m.CrearOrdenCompra);
            if(comparador.ModificarOrdenCompra && !item.ModificarOrdenCompra)
                yield return comparador.GetDescripcion(m=> m.ModificarOrdenCompra);
            if(comparador.EliminarOrdenCompra && !item.EliminarOrdenCompra)
                yield return comparador.GetDescripcion(m=> m.EliminarOrdenCompra);
            if(comparador.ConsultarOrdenCompra && !item.ConsultarOrdenCompra)
                yield return comparador.GetDescripcion(m=> m.ConsultarOrdenCompra);
            if(comparador.CerrarOrdenCompra && !item.CerrarOrdenCompra)
                yield return comparador.GetDescripcion(m=> m.CerrarOrdenCompra);
            if(comparador.CrearProveedor && !item.CrearProveedor)
                yield return comparador.GetDescripcion(m=> m.CrearProveedor);
            if(comparador.ModificarProveedor && !item.ModificarProveedor)
                yield return comparador.GetDescripcion(m=> m.ModificarProveedor);
            if(comparador.EliminarProveedor && !item.EliminarProveedor)
                yield return comparador.GetDescripcion(m=> m.EliminarProveedor);
            if(comparador.ConsultarProveedor && !item.ConsultarProveedor)
                yield return comparador.GetDescripcion(m=> m.ConsultarProveedor);
            if(comparador.ConsultarPagosProveedores && !item.ConsultarPagosProveedores)
                yield return comparador.GetDescripcion(m=> m.ConsultarPagosProveedores);
            if(comparador.CrearInventario && !item.CrearInventario)
                yield return comparador.GetDescripcion(m=> m.CrearInventario);
            if(comparador.ModificarInventario && !item.ModificarInventario)
                yield return comparador.GetDescripcion(m=> m.ModificarInventario);
            if(comparador.EliminarInventario && !item.EliminarInventario)
                yield return comparador.GetDescripcion(m=> m.EliminarInventario);
            if(comparador.ConsultarInventario && !item.ConsultarInventario)
                yield return comparador.GetDescripcion(m=> m.ConsultarInventario);
            if(comparador.SalidaSinInventario && !item.SalidaSinInventario)
                yield return comparador.GetDescripcion(m=> m.SalidaSinInventario);
            if(comparador.ReabirInventario && !item.ReabirInventario)
                yield return comparador.GetDescripcion(m=> m.ReabirInventario);
            if(comparador.CerrarInventario && !item.CerrarInventario)
                yield return comparador.GetDescripcion(m=> m.CerrarInventario);
            if(comparador.ConsultarReporteInventario && !item.ConsultarReporteInventario)
                yield return comparador.GetDescripcion(m=> m.ConsultarReporteInventario);
            if(comparador.CrearFichas && !item.CrearFichas)
                yield return comparador.GetDescripcion(m=> m.CrearFichas);
            if(comparador.ModificarFichas && !item.ModificarFichas)
                yield return comparador.GetDescripcion(m=> m.ModificarFichas);
            if(comparador.EliminarFichas && !item.EliminarFichas)
                yield return comparador.GetDescripcion(m=> m.EliminarFichas);
            if(comparador.ConsultarFichas && !item.ConsultarFichas)
                yield return comparador.GetDescripcion(m=> m.ConsultarFichas);
            if(comparador.CrearIntercambiosAlmacenes && !item.CrearIntercambiosAlmacenes)
                yield return comparador.GetDescripcion(m=> m.CrearIntercambiosAlmacenes);
            if(comparador.ModificarIntercambiosAlmacenes && !item.ModificarIntercambiosAlmacenes)
                yield return comparador.GetDescripcion(m=> m.ModificarIntercambiosAlmacenes);
            if(comparador.EliminarIntercambiosAlmacenes && !item.EliminarIntercambiosAlmacenes)
                yield return comparador.GetDescripcion(m=> m.EliminarIntercambiosAlmacenes);
            if(comparador.ConsultarIntercambiosAlmacenes && !item.ConsultarIntercambiosAlmacenes)
                yield return comparador.GetDescripcion(m=> m.ConsultarIntercambiosAlmacenes);
            if(comparador.CrearCentroCostos && !item.CrearCentroCostos)
                yield return comparador.GetDescripcion(m=> m.CrearCentroCostos);
            if(comparador.ModificarCentroCostos && !item.ModificarCentroCostos)
                yield return comparador.GetDescripcion(m=> m.ModificarCentroCostos);
            if(comparador.EliminarCentroCostos && !item.EliminarCentroCostos)
                yield return comparador.GetDescripcion(m=> m.EliminarCentroCostos);
            if(comparador.ConsultarCentroCostos && !item.ConsultarCentroCostos)
                yield return comparador.GetDescripcion(m=> m.ConsultarCentroCostos);
            if(comparador.CrearObjetoOlvidado && !item.CrearObjetoOlvidado)
                yield return comparador.GetDescripcion(m=> m.CrearObjetoOlvidado);
            if(comparador.ModificarObjetoOlvidado && !item.ModificarObjetoOlvidado)
                yield return comparador.GetDescripcion(m=> m.ModificarObjetoOlvidado);
            if(comparador.ConsultarObjetoOlvidado && !item.ConsultarObjetoOlvidado)
                yield return comparador.GetDescripcion(m=> m.ConsultarObjetoOlvidado);
            if(comparador.CancelarObjetoOlvidado && !item.CancelarObjetoOlvidado)
                yield return comparador.GetDescripcion(m=> m.CancelarObjetoOlvidado);
            if(comparador.EliminarObjetoOlvidado && !item.EliminarObjetoOlvidado)
                yield return comparador.GetDescripcion(m=> m.EliminarObjetoOlvidado);
            if(comparador.CrearIncidenciaHabitacion && !item.CrearIncidenciaHabitacion)
                yield return comparador.GetDescripcion(m=> m.CrearIncidenciaHabitacion);
            if(comparador.ConsultarIncidenciaHabitacion && !item.ConsultarIncidenciaHabitacion)
                yield return comparador.GetDescripcion(m=> m.ConsultarIncidenciaHabitacion);
            if(comparador.CancelarIncidenciaHabitacion && !item.CancelarIncidenciaHabitacion)
                yield return comparador.GetDescripcion(m=> m.CancelarIncidenciaHabitacion);
            if(comparador.EliminarIncidenciaHabitacion && !item.EliminarIncidenciaHabitacion)
                yield return comparador.GetDescripcion(m=> m.EliminarIncidenciaHabitacion);
            if(comparador.ModificarIncidenciaHabitacion && !item.ModificarIncidenciaHabitacion)
                yield return comparador.GetDescripcion(m=> m.ModificarIncidenciaHabitacion);
            if(comparador.CrearGastos && !item.CrearGastos)
                yield return comparador.GetDescripcion(m=> m.CrearGastos);
            if(comparador.ModificarGastos && !item.ModificarGastos)
                yield return comparador.GetDescripcion(m=> m.ModificarGastos);
            if(comparador.ConsultarGastos && !item.ConsultarGastos)
                yield return comparador.GetDescripcion(m=> m.ConsultarGastos);
            if(comparador.EliminarGastos && !item.EliminarGastos)
                yield return comparador.GetDescripcion(m=> m.EliminarGastos);
            if(comparador.CrearConceptosGastos && !item.CrearConceptosGastos)
                yield return comparador.GetDescripcion(m=> m.CrearConceptosGastos);
            if(comparador.ModificarConceptosGastos && !item.ModificarConceptosGastos)
                yield return comparador.GetDescripcion(m=> m.ModificarConceptosGastos);
            if(comparador.EliminarConceptosGastos && !item.EliminarConceptosGastos)
                yield return comparador.GetDescripcion(m=> m.EliminarConceptosGastos);
            if(comparador.ConsultarConceptosGastos && !item.ConsultarConceptosGastos)
                yield return comparador.GetDescripcion(m=> m.ConsultarConceptosGastos);
            if(comparador.CrearFajillas && !item.CrearFajillas)
                yield return comparador.GetDescripcion(m=> m.CrearFajillas);
            if(comparador.ConsultarFajillas && !item.ConsultarFajillas)
                yield return comparador.GetDescripcion(m=> m.ConsultarFajillas);
            if(comparador.AutorizarFajillas && !item.AutorizarFajillas)
                yield return comparador.GetDescripcion(m=> m.AutorizarFajillas);
            if(comparador.ConsultarConfiguracionFajillas && !item.ConsultarConfiguracionFajillas)
                yield return comparador.GetDescripcion(m=> m.ConsultarConfiguracionFajillas);
            if(comparador.ModificarConfiguracionFajillas && !item.ModificarConfiguracionFajillas)
                yield return comparador.GetDescripcion(m=> m.ModificarConfiguracionFajillas);
            if(comparador.ConsultarTiposArticulos && !item.ConsultarTiposArticulos)
                yield return comparador.GetDescripcion(m=> m.ConsultarTiposArticulos);
            if(comparador.CrearArticulos && !item.CrearArticulos)
                yield return comparador.GetDescripcion(m=> m.CrearArticulos);
            if(comparador.ModificarArticulos && !item.ModificarArticulos)
                yield return comparador.GetDescripcion(m=> m.ModificarArticulos);
            if(comparador.EliminarArticulos && !item.EliminarArticulos)
                yield return comparador.GetDescripcion(m=> m.EliminarArticulos);
            if(comparador.ConsultarArticulos && !item.ConsultarArticulos)
                yield return comparador.GetDescripcion(m=> m.ConsultarArticulos);
            if(comparador.CrearTaxis && !item.CrearTaxis)
                yield return comparador.GetDescripcion(m=> m.CrearTaxis);
            if(comparador.ModificarTaxis && !item.ModificarTaxis)
                yield return comparador.GetDescripcion(m=> m.ModificarTaxis);
            if(comparador.EliminarTaxis && !item.EliminarTaxis)
                yield return comparador.GetDescripcion(m=> m.EliminarTaxis);
            if(comparador.ConsultarTaxis && !item.ConsultarTaxis)
                yield return comparador.GetDescripcion(m=> m.ConsultarTaxis);
            if(comparador.CobrarTaxis && !item.CobrarTaxis)
                yield return comparador.GetDescripcion(m=> m.CobrarTaxis);
            if(comparador.CrearAsistencias && !item.CrearAsistencias)
                yield return comparador.GetDescripcion(m=> m.CrearAsistencias);
            if(comparador.ModificarAsistencias && !item.ModificarAsistencias)
                yield return comparador.GetDescripcion(m=> m.ModificarAsistencias);
            if(comparador.EliminarAsistencias && !item.EliminarAsistencias)
                yield return comparador.GetDescripcion(m=> m.EliminarAsistencias);
            if(comparador.ConsultarAsistencias && !item.ConsultarAsistencias)
                yield return comparador.GetDescripcion(m=> m.ConsultarAsistencias);
            if(comparador.JustificarRetardoFalta && !item.JustificarRetardoFalta)
                yield return comparador.GetDescripcion(m=> m.JustificarRetardoFalta);
            if(comparador.ConsultarConfiguracionImpresoras && !item.ConsultarConfiguracionImpresoras)
                yield return comparador.GetDescripcion(m=> m.ConsultarConfiguracionImpresoras);
            if(comparador.ModificarConfiguracionImpresoras && !item.ModificarConfiguracionImpresoras)
                yield return comparador.GetDescripcion(m=> m.ModificarConfiguracionImpresoras);
            if(comparador.ConsultarConfiguracionPropinas && !item.ConsultarConfiguracionPropinas)
                yield return comparador.GetDescripcion(m=> m.ConsultarConfiguracionPropinas);
            if(comparador.ModificarConfiguracionPropinas && !item.ModificarConfiguracionPropinas)
                yield return comparador.GetDescripcion(m=> m.ModificarConfiguracionPropinas);
            if(comparador.ConsultarPropinas && !item.ConsultarPropinas)
                yield return comparador.GetDescripcion(m=> m.ConsultarPropinas);
            if(comparador.RealizarCorteTurno && !item.RealizarCorteTurno)
                yield return comparador.GetDescripcion(m=> m.RealizarCorteTurno);
            if(comparador.ConsultarDatosCorte && !item.ConsultarDatosCorte)
                yield return comparador.GetDescripcion(m=> m.ConsultarDatosCorte);
            if(comparador.RevisarCorteTurno && !item.RevisarCorteTurno)
                yield return comparador.GetDescripcion(m=> m.RevisarCorteTurno);
            if(comparador.FinalizarCorteTurno && !item.FinalizarCorteTurno)
                yield return comparador.GetDescripcion(m=> m.FinalizarCorteTurno);
            if(comparador.ConsultarVentas && !item.ConsultarVentas)
                yield return comparador.GetDescripcion(m=> m.ConsultarVentas);
            if(comparador.ConsultaConsumosInternosEmpleado && !item.ConsultaConsumosInternosEmpleado)
                yield return comparador.GetDescripcion(m=> m.ConsultaConsumosInternosEmpleado);
            if(comparador.ConsultaCortesiaCancelaciones && !item.ConsultaCortesiaCancelaciones)
                yield return comparador.GetDescripcion(m=> m.ConsultaCortesiaCancelaciones);
            if(comparador.GestionarPuntosLealtad && !item.GestionarPuntosLealtad)
                yield return comparador.GetDescripcion(m=> m.GestionarPuntosLealtad);
            if(comparador.VenderTarjetasLealtad && !item.VenderTarjetasLealtad)
                yield return comparador.GetDescripcion(m=> m.VenderTarjetasLealtad);
            if(comparador.CanjearCupones && !item.CanjearCupones)
                yield return comparador.GetDescripcion(m=> m.CanjearCupones);
            if(comparador.ConsultarCupones && !item.ConsultarCupones)
                yield return comparador.GetDescripcion(m=> m.ConsultarCupones);
            if(comparador.ConsultarSaldoTarjetasLealtad && !item.ConsultarSaldoTarjetasLealtad)
                yield return comparador.GetDescripcion(m=> m.ConsultarSaldoTarjetasLealtad);
            if(comparador.ConsultarTarjetasLealtad && !item.ConsultarTarjetasLealtad)
                yield return comparador.GetDescripcion(m=> m.ConsultarTarjetasLealtad);
            if(comparador.ModificarTarjetasLealtad && !item.ModificarTarjetasLealtad)
                yield return comparador.GetDescripcion(m=> m.ModificarTarjetasLealtad);
            if(comparador.EliminarTarjetasLealtad && !item.EliminarTarjetasLealtad)
                yield return comparador.GetDescripcion(m=> m.EliminarTarjetasLealtad);
            if(comparador.CrearTarjetasLealtad && !item.CrearTarjetasLealtad)
                yield return comparador.GetDescripcion(m=> m.CrearTarjetasLealtad);
            if(comparador.ModificarPagos && !item.ModificarPagos)
                yield return comparador.GetDescripcion(m=> m.ModificarPagos);
            if(comparador.CrearDatosFiscalesClientes && !item.CrearDatosFiscalesClientes)
                yield return comparador.GetDescripcion(m=> m.CrearDatosFiscalesClientes);
            if(comparador.ModificarDatosFiscalesClientes && !item.ModificarDatosFiscalesClientes)
                yield return comparador.GetDescripcion(m=> m.ModificarDatosFiscalesClientes);
            if(comparador.ConsultarDatosFiscalesClientes && !item.ConsultarDatosFiscalesClientes)
                yield return comparador.GetDescripcion(m=> m.ConsultarDatosFiscalesClientes);
            if(comparador.VincularDatosFiscalesClientes && !item.VincularDatosFiscalesClientes)
                yield return comparador.GetDescripcion(m=> m.VincularDatosFiscalesClientes);
            if(comparador.CrearDatosFiscales && !item.CrearDatosFiscales)
                yield return comparador.GetDescripcion(m=> m.CrearDatosFiscales);
            if(comparador.ModificarDatosFiscales && !item.ModificarDatosFiscales)
                yield return comparador.GetDescripcion(m=> m.ModificarDatosFiscales);
            if(comparador.EliminarDatosFiscales && !item.EliminarDatosFiscales)
                yield return comparador.GetDescripcion(m=> m.EliminarDatosFiscales);
            if(comparador.ConsultarDatosFiscales && !item.ConsultarDatosFiscales)
                yield return comparador.GetDescripcion(m=> m.ConsultarDatosFiscales);
            if(comparador.CrearTareaMantenimiento && !item.CrearTareaMantenimiento)
                yield return comparador.GetDescripcion(m=> m.CrearTareaMantenimiento);
            if(comparador.ModificarTareaMantenimiento && !item.ModificarTareaMantenimiento)
                yield return comparador.GetDescripcion(m=> m.ModificarTareaMantenimiento);
            if(comparador.EliminarTareaMantenimiento && !item.EliminarTareaMantenimiento)
                yield return comparador.GetDescripcion(m=> m.EliminarTareaMantenimiento);
            if(comparador.ConsultarTareaMantenimiento && !item.ConsultarTareaMantenimiento)
                yield return comparador.GetDescripcion(m=> m.ConsultarTareaMantenimiento);
            if(comparador.FinalizarMantenimiento && !item.FinalizarMantenimiento)
                yield return comparador.GetDescripcion(m=> m.FinalizarMantenimiento);
            if(comparador.IniciarMantenimiento && !item.IniciarMantenimiento)
                yield return comparador.GetDescripcion(m=> m.IniciarMantenimiento);
            if(comparador.ConsultarMantenimientos && !item.ConsultarMantenimientos)
                yield return comparador.GetDescripcion(m=> m.ConsultarMantenimientos);
            if(comparador.ConsultarConceptosMantenimiento && !item.ConsultarConceptosMantenimiento)
                yield return comparador.GetDescripcion(m=> m.ConsultarConceptosMantenimiento);
            if(comparador.ConsultarPuestos && !item.ConsultarPuestos)
                yield return comparador.GetDescripcion(m=> m.ConsultarPuestos);
            if(comparador.EliminarPuestos && !item.EliminarPuestos)
                yield return comparador.GetDescripcion(m=> m.EliminarPuestos);
            if(comparador.ModificarPuestos && !item.ModificarPuestos)
                yield return comparador.GetDescripcion(m=> m.ModificarPuestos);
            if(comparador.CrearPuestos && !item.CrearPuestos)
                yield return comparador.GetDescripcion(m=> m.CrearPuestos);
            if(comparador.CrearEtiquetas && !item.CrearEtiquetas)
                yield return comparador.GetDescripcion(m=> m.CrearEtiquetas);
            if(comparador.ModificarEtiquetas && !item.ModificarEtiquetas)
                yield return comparador.GetDescripcion(m=> m.ModificarEtiquetas);
            if(comparador.EliminarEtiquetas && !item.EliminarEtiquetas)
                yield return comparador.GetDescripcion(m=> m.EliminarEtiquetas);
            if(comparador.ConsultarEtiquetas && !item.ConsultarEtiquetas)
                yield return comparador.GetDescripcion(m=> m.ConsultarEtiquetas);
            if(comparador.ModificarRenta && !item.ModificarRenta)
                yield return comparador.GetDescripcion(m=> m.ModificarRenta);
            if(comparador.AbrirMesa && !item.AbrirMesa)
                yield return comparador.GetDescripcion(m=> m.AbrirMesa);
            if(comparador.CancelarOcupacionesMesas && !item.CancelarOcupacionesMesas)
                yield return comparador.GetDescripcion(m=> m.CancelarOcupacionesMesas);
            if(comparador.CrearOrdenRestaurante && !item.CrearOrdenRestaurante)
                yield return comparador.GetDescripcion(m=> m.CrearOrdenRestaurante);
            if(comparador.ModificarOrdenRestaurante && !item.ModificarOrdenRestaurante)
                yield return comparador.GetDescripcion(m=> m.ModificarOrdenRestaurante);
            if(comparador.EliminarOrdenRestaurante && !item.EliminarOrdenRestaurante)
                yield return comparador.GetDescripcion(m=> m.EliminarOrdenRestaurante);
            if(comparador.ConsultarMesas && !item.ConsultarMesas)
                yield return comparador.GetDescripcion(m=> m.ConsultarMesas);
            if(comparador.ConsultarDetallesMesa && !item.ConsultarDetallesMesa)
                yield return comparador.GetDescripcion(m=> m.ConsultarDetallesMesa);
            if(comparador.CambiarMesa && !item.CambiarMesa)
                yield return comparador.GetDescripcion(m=> m.CambiarMesa);
            if(comparador.MarcarMesaPorCobrar && !item.MarcarMesaPorCobrar)
                yield return comparador.GetDescripcion(m=> m.MarcarMesaPorCobrar);
            if(comparador.MarcarEntregadaOrden && !item.MarcarEntregadaOrden)
                yield return comparador.GetDescripcion(m=> m.MarcarEntregadaOrden);
            if(comparador.AdministrarMesas && !item.AdministrarMesas)
                yield return comparador.GetDescripcion(m=> m.AdministrarMesas);
            if(comparador.ConsultarPresentaciones && !item.ConsultarPresentaciones)
                yield return comparador.GetDescripcion(m=> m.ConsultarPresentaciones);
            if(comparador.CrearPresentaciones && !item.CrearPresentaciones)
                yield return comparador.GetDescripcion(m=> m.CrearPresentaciones);
            if(comparador.ModificarPresentaciones && !item.ModificarPresentaciones)
                yield return comparador.GetDescripcion(m=> m.ModificarPresentaciones);
            if(comparador.EliminarPresentaciones && !item.EliminarPresentaciones)
                yield return comparador.GetDescripcion(m=> m.EliminarPresentaciones);
            if(comparador.ConsultarConsumosInternos && !item.ConsultarConsumosInternos)
                yield return comparador.GetDescripcion(m=> m.ConsultarConsumosInternos);
            if(comparador.CrearConsumosInternos && !item.CrearConsumosInternos)
                yield return comparador.GetDescripcion(m=> m.CrearConsumosInternos);
            if(comparador.ModificarConsumosInternos && !item.ModificarConsumosInternos)
                yield return comparador.GetDescripcion(m=> m.ModificarConsumosInternos);
            if(comparador.EliminarConsumosInternos && !item.EliminarConsumosInternos)
                yield return comparador.GetDescripcion(m=> m.EliminarConsumosInternos);
            if(comparador.MarcarEntregadoConsumoInterno && !item.MarcarEntregadoConsumoInterno)
                yield return comparador.GetDescripcion(m=> m.MarcarEntregadoConsumoInterno);
            if(comparador.ConsultarAlmacenes && !item.ConsultarAlmacenes)
                yield return comparador.GetDescripcion(m=> m.ConsultarAlmacenes);
            if(comparador.ModificarAlmacenes && !item.ModificarAlmacenes)
                yield return comparador.GetDescripcion(m=> m.ModificarAlmacenes);
            if(comparador.CrearAlmacenes && !item.CrearAlmacenes)
                yield return comparador.GetDescripcion(m=> m.CrearAlmacenes);
            if(comparador.EliminarAlmacenes && !item.EliminarAlmacenes)
                yield return comparador.GetDescripcion(m=> m.EliminarAlmacenes);
            if(comparador.CrearNomina && !item.CrearNomina)
                yield return comparador.GetDescripcion(m=> m.CrearNomina);
            if(comparador.ConsultarNomina && !item.ConsultarNomina)
                yield return comparador.GetDescripcion(m=> m.ConsultarNomina);
            if(comparador.ModificarNomina && !item.ModificarNomina)
                yield return comparador.GetDescripcion(m=> m.ModificarNomina);
            if(comparador.EliminarNomina && !item.EliminarNomina)
                yield return comparador.GetDescripcion(m=> m.EliminarNomina);
            if(comparador.ConsultarReportesMatricula && !item.ConsultarReportesMatricula)
                yield return comparador.GetDescripcion(m=> m.ConsultarReportesMatricula);
            if(comparador.CrearReportesMatricula && !item.CrearReportesMatricula)
                yield return comparador.GetDescripcion(m=> m.CrearReportesMatricula);
            if(comparador.EliminarReportesMatricula && !item.EliminarReportesMatricula)
                yield return comparador.GetDescripcion(m=> m.EliminarReportesMatricula);
            if(comparador.ConsultarRequisiciones && !item.ConsultarRequisiciones)
                yield return comparador.GetDescripcion(m=> m.ConsultarRequisiciones);
            if(comparador.CrearRespaldosBaseDatos && !item.CrearRespaldosBaseDatos)
                yield return comparador.GetDescripcion(m=> m.CrearRespaldosBaseDatos);
            if(comparador.ConsultarConceptosSistema && !item.ConsultarConceptosSistema)
                yield return comparador.GetDescripcion(m=> m.ConsultarConceptosSistema);
            if(comparador.CrearConceptosSistema && !item.CrearConceptosSistema)
                yield return comparador.GetDescripcion(m=> m.CrearConceptosSistema);
            if(comparador.ModificarConceptosSistema && !item.ModificarConceptosSistema)
                yield return comparador.GetDescripcion(m=> m.ModificarConceptosSistema);
            if(comparador.EliminarConceptosSistema && !item.EliminarConceptosSistema)
                yield return comparador.GetDescripcion(m=> m.EliminarConceptosSistema);
            if(comparador.ConsultaClientesMatricula && !item.ConsultaClientesMatricula)
                yield return comparador.GetDescripcion(m=> m.ConsultaClientesMatricula);
            if(comparador.CrearSolicitudesPermisosFaltas && !item.CrearSolicitudesPermisosFaltas)
                yield return comparador.GetDescripcion(m=> m.CrearSolicitudesPermisosFaltas);
            if(comparador.ConsultarSolicitudesPermisosFaltas && !item.ConsultarSolicitudesPermisosFaltas)
                yield return comparador.GetDescripcion(m=> m.ConsultarSolicitudesPermisosFaltas);
            if(comparador.ModificarSolicitudesPermisosFaltas && !item.ModificarSolicitudesPermisosFaltas)
                yield return comparador.GetDescripcion(m=> m.ModificarSolicitudesPermisosFaltas);
            if(comparador.EliminarSolicitudesPermisosFaltas && !item.EliminarSolicitudesPermisosFaltas)
                yield return comparador.GetDescripcion(m=> m.EliminarSolicitudesPermisosFaltas);
            if(comparador.ConsultarRecetas && !item.ConsultarRecetas)
                yield return comparador.GetDescripcion(m=> m.ConsultarRecetas);
            if(comparador.EditarRecetas && !item.EditarRecetas)
                yield return comparador.GetDescripcion(m=> m.EditarRecetas);
            if(comparador.ConsultarDepartamentos && !item.ConsultarDepartamentos)
                yield return comparador.GetDescripcion(m=> m.ConsultarDepartamentos);
            if(comparador.CrearDepartamento && !item.CrearDepartamento)
                yield return comparador.GetDescripcion(m=> m.CrearDepartamento);
            if(comparador.ModificarDepartamento && !item.ModificarDepartamento)
                yield return comparador.GetDescripcion(m=> m.ModificarDepartamento);
            if(comparador.EliminarDepartamento && !item.EliminarDepartamento)
                yield return comparador.GetDescripcion(m=> m.EliminarDepartamento);
            if(comparador.ConsultarLineas && !item.ConsultarLineas)
                yield return comparador.GetDescripcion(m=> m.ConsultarLineas);
            if(comparador.CrearLinea && !item.CrearLinea)
                yield return comparador.GetDescripcion(m=> m.CrearLinea);
            if(comparador.ModificarLinea && !item.ModificarLinea)
                yield return comparador.GetDescripcion(m=> m.ModificarLinea);
            if(comparador.EliminarLinea && !item.EliminarLinea)
                yield return comparador.GetDescripcion(m=> m.EliminarLinea);
            if(comparador.ConsultarPresupuestos && !item.ConsultarPresupuestos)
                yield return comparador.GetDescripcion(m=> m.ConsultarPresupuestos);
            if(comparador.CrearPresupuestos && !item.CrearPresupuestos)
                yield return comparador.GetDescripcion(m=> m.CrearPresupuestos);
            if(comparador.ModificarPresupuestos && !item.ModificarPresupuestos)
                yield return comparador.GetDescripcion(m=> m.ModificarPresupuestos);
            if(comparador.EliminarPresupuestos && !item.EliminarPresupuestos)
                yield return comparador.GetDescripcion(m=> m.EliminarPresupuestos);
            if(comparador.RecibirPropinas && !item.RecibirPropinas)
                yield return comparador.GetDescripcion(m=> m.RecibirPropinas);
            if(comparador.Aparcar && !item.Aparcar)
                yield return comparador.GetDescripcion(m=> m.Aparcar);
            if(comparador.ReimprimirRecibos && !item.ReimprimirRecibos)
                yield return comparador.GetDescripcion(m=> m.ReimprimirRecibos);
            if(comparador.ConfigurarParametrosSistema && !item.ConfigurarParametrosSistema)
                yield return comparador.GetDescripcion(m=> m.ConfigurarParametrosSistema);
            if(comparador.EditarConfiguracionPuntosLealtad && !item.EditarConfiguracionPuntosLealtad)
                yield return comparador.GetDescripcion(m=> m.EditarConfiguracionPuntosLealtad);
            if(comparador.EliminarConfiguracionPuntosLealtad && !item.EliminarConfiguracionPuntosLealtad)
                yield return comparador.GetDescripcion(m=> m.EliminarConfiguracionPuntosLealtad);
            if(comparador.ConfigurarTurnos && !item.ConfigurarTurnos)
                yield return comparador.GetDescripcion(m=> m.ConfigurarTurnos);
            if(comparador.ConsultarCaja && !item.ConsultarCaja)
                yield return comparador.GetDescripcion(m=> m.ConsultarCaja);
            if(comparador.AplicarDescuentoCaja && !item.AplicarDescuentoCaja)
                yield return comparador.GetDescripcion(m=> m.AplicarDescuentoCaja);
            if(comparador.CancelarCaja && !item.CancelarCaja)
                yield return comparador.GetDescripcion(m=> m.CancelarCaja);
            yield break;    

        }
    }
}
