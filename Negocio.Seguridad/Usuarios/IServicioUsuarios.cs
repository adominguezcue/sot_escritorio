﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Seguridad.Usuarios
{
    public interface IServicioUsuarios
    {
        void CrearUsuario(Usuario usuario, List<DtoCredencial> confirmaciones, DtoUsuario DtoUsuario);
        //List<DtoUsuario> ObtenerUsuariosPaginados(DtoUsuario DtoUsuario, string filtro = null, int pagina = 0, int elementos = 0);
        //bool VerificarRolEnUso(int idRole);
        List<DtoUsuario> ObtenerUsuariosActivos();
        //DtoUsuario ObtenerUsuarioPorNombre(string userName);

        //string ObtenerAlias(int idUsuario);

        DtoUsuario ObtenerUsuarioPorCredencial(int idEmpleado, DtoCredencial datosCredencial);
        /// <summary>
        /// Valida que exista un usuario activo con el id proporcionado
        /// </summary>
        /// <param name="idUsuario"></param>
        void ValidaUsuario(int idUsuario);

        Usuario ObtenerUsuarioPorEmpleado(int idEmpleado, DtoUsuario usuario);

        void ModificarUsuario(int idUsuario, string nuevoAlias, List<Credencial> credencialesCrear, List<Credencial> credencialesEliminar, List<DtoCredencial> comprobaciones, DtoUsuario UsuarioActual);

        void EliminarUsuario(int idUsuario, DtoUsuario DtoUsuario);
        void EliminarUsuarioPorEmpleado(int idEmpleado, DtoUsuario DtoUsuario);
        List<Credencial.TiposCredencial> ObtenerTiposCredencialesExistentes(int idUsuario, DtoUsuario usuario);
    }
}
