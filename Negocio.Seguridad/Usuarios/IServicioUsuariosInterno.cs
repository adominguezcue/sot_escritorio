﻿using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Seguridad.Usuarios
{
    internal interface IServicioUsuariosInterno : IServicioUsuarios
    {
        DtoUsuario ObtenerUsuarioPorId(int idUsuario);
        DtoUsuario ObtenerUsuarioPorCredencial(DtoCredencial credencial);
    }
}
