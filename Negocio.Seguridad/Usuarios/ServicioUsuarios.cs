﻿using Negocio.Seguridad.Roles;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Dtos;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Transversal.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Transversal.Seguridad;
using Modelo.Seguridad.Repositorios;
using Transversal.Dependencias;
using Negocio.Seguridad.Fabricas;
using Negocio.Seguridad.Permisos;
using Negocio.Compartido.Empleados;

namespace Negocio.Seguridad.Usuarios
{
    public class ServicioUsuarios : IServicioUsuariosInterno
    {

        #region Services

        private IServicioEmpleadosCompartido BusinessServicioEmpleadosCompartido
        {
            get { return _businessServicioEmpleadosCompartido.Value; }
        }

        Lazy<IServicioEmpleadosCompartido> _businessServicioEmpleadosCompartido = new Lazy<IServicioEmpleadosCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosCompartido>(); });


        Lazy<IServicioRolesInterno> _businessServiceRoles = new Lazy<IServicioRolesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRolesInterno>(); });

        private IServicioRolesInterno BusinessServiceRoles
        {
            get { return _businessServiceRoles.Value; }
        }

        private IServicioPermisos _businessServicePermisos;

        private IServicioPermisos BusinessServicePermisos
        {
            get { return _businessServicePermisos ?? (_businessServicePermisos = new ServicioPermisos()); }
        }

        #endregion Services

        #region repositories

        private IRepositorioUsuarios RepositorioUsuarios
        {
            get { return _repositorioUsuarios.Value; }
        }

        Lazy<IRepositorioUsuarios> _repositorioUsuarios = new Lazy<IRepositorioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioUsuarios>(); });

        private IRepositorioCredenciales RepositorioCredenciales
        {
            get { return _repositorioCredenciales.Value; }
        }

        Lazy<IRepositorioCredenciales> _repositorioCredenciales = new Lazy<IRepositorioCredenciales>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCredenciales>(); });

        #endregion repositories

        public void CrearUsuario(Usuario usuario, List<DtoCredencial> confirmaciones, DtoUsuario DtoUsuario)
        {
            if (usuario == null)
                throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

            //if (usuario.IdRol <= 0)
            //    throw new SOTException(Recursos.Usuarios.no_role_exception);

            BusinessServicePermisos.ValidarPermisos(DtoUsuario, new DtoPermisos { CrearUsuarios = true });

            if (confirmaciones.GroupBy(m => m.TipoCredencial).Count() != confirmaciones.Count)
                throw new SOTException("Existen confirmaciones del mismo tipo");

            if (usuario.Credenciales.GroupBy(m => m.TipoCredencial).Count() != usuario.Credenciales.Count)
                throw new SOTException("Existen credenciales del mismo tipo");

            //if (usuario.ApellidoMaterno == null)
            //    usuario.ApellidoMaterno = string.Empty;

            ValidateMandatoryFields(usuario);

            this.ValidateAlias(usuario.Alias);
            this.ValidateUser(usuario.IdEmpleado);

            foreach (var credencialComp in confirmaciones)
            {
                var credencial = usuario.Credenciales.FirstOrDefault(m => m.TipoCredencial == credencialComp.TipoCredencial);

                if (credencial == null)
                    throw new SOTException("No se han proporcionado credenciales de tipo " + credencialComp.TipoCredencial.Descripcion());

                var validador = FabricaValidadorCredenciales.ObtenerValidador(credencial.TipoCredencial);

                validador.ValidarCredencial(credencial, credencialComp);
            }

            //BusinessServiceRoles.ValidateRole(usuario.IdRol);

            var date = DateTime.Now;

            usuario.FechaCreacion = date;
            usuario.FechaModificacion = date;
            usuario.Activo = true;
            usuario.IdUsuarioCreo = DtoUsuario.Id;
            usuario.IdUsuarioModifico = DtoUsuario.Id;

            //usuario.Credenciales = new Dominio.Nucleo.Utilidades.SotCollection<Credencial>();
            //usuario.Credenciales.Add(credencial);

            this.RepositorioUsuarios.Agregar(usuario);
            this.RepositorioUsuarios.GuardarCambios();
        }

        private void ValidateMandatoryFields(Usuario usuario)
        {
            //if (string.IsNullOrEmpty(usuario.Nombre))
            //    throw new SOTException(Recursos.Generales.empty_field_exception, "Nombre");
            //if (string.IsNullOrEmpty(usuario.ApellidoPaterno))
            //    throw new SOTException(Recursos.Generales.empty_field_exception, "Apellido paterno");
            if (string.IsNullOrEmpty(usuario.Alias))
                throw new SOTException(Recursos.Generales.empty_field_exception, "Alias");
        }

        //public bool VerificarRolEnUso(int idRol)
        //{
        //    return RepositorioUsuarios.Alguno(m => m.Activo && m.IdRol == idRol);
        //}

        public List<DtoUsuario> ObtenerUsuariosActivos()
        {
            return RepositorioUsuarios.ObtenerElementos(m => m.Activo).ToList().Select(m => (DtoUsuario)m).ToList();
        }

        //public DtoUsuario ObtenerUsuarioPorNombre(string userName)
        //{
        //    var usuario = this.RepositorioUsuarios.Obtener(m => (m.Nombre + " " + m.ApellidoPaterno
        //                                             + (m.ApellidoMaterno != null && m.ApellidoMaterno != "" ? " " + m.ApellidoMaterno : "")) == userName, m => m.Rol);
        //    //.Get(m=>m.Id==idUser,m=>m.UserCountries);

        //    return (DtoUsuario)usuario;
        //}

        ///// <summary>
        ///// Recibe una instancia de DtoPaginationRequest con los datos necesarios para realizar una
        ///// consulta, y retorna un DtoPaginationResponse con los resultados
        ///// </summary>
        ///// <param name="dtoPaginationRequest"></param>
        ///// <param name="DtoUsuario"></param>
        ///// <returns></returns>
        //public List<DtoUsuario> ObtenerUsuariosPaginados(DtoUsuario DtoUsuario, string filtro = null, int pagina = 0, int elementos = 0)
        //{
        //    //buscarle un uso al usuario
        //    if (DtoUsuario == null)
        //        throw new SOTException(Recursos.Usuarios.null_exception);

        //    if (!DtoUsuario.Permisos.VerUsuarios)
        //        throw new SOTException(Recursos.Permisos.no_permission_exception);

        //    Expression<Func<Usuario, bool>> filter = m => m.Activo && m.Rol.Activo;

        //    if (!string.IsNullOrWhiteSpace(filtro))
        //        filter = filter.Compose(m => m.Nombre.Contains(filtro)
        //            || m.ApellidoPaterno.Contains(filtro)
        //            || m.ApellidoMaterno.Contains(filtro)
        //            , Expression.AndAlso);

        //    var usuarios = elementos == 0 && pagina == 0 ?
        //        this.RepositorioUsuarios.ObtenerElementos(filter, m => m.Rol).OrderBy(m=>m.Nombre) : 
        //        this.RepositorioUsuarios.ObtenerElementos(filter, m => m.Rol, m=>m.Nombre, pagina, elementos);

        //    return usuarios.Select(m => (DtoUsuario)m).ToList();
        //}

        /// <summary>
        /// METHOD
        /// <para>05/10/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        public void EliminarUsuario(int idUsuario, DtoUsuario DtoUsuario)
        {
            var usuario = this.RepositorioUsuarios.Obtener(m => m.Activo && m.Id == idUsuario);

            if (usuario == null)
                throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

            if (usuario.Id == DtoUsuario.IdUsuario)
                throw new SOTException(Recursos.Usuarios.can_not_delete_current_user_exception);

            BusinessServicePermisos.ValidarPermisos(DtoUsuario, new DtoPermisos { EliminarUsuarios = true });

            var credenciales = RepositorioCredenciales.ObtenerCredencialesUsuario(idUsuario);

            var date = DateTime.Now;

            usuario.Activo = false;
            usuario.IdUsuarioModifico = DtoUsuario.Id;
            usuario.IdUsuarioElimino = DtoUsuario.Id;
            usuario.FechaModificacion = date;
            usuario.FechaEliminacion = date;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                foreach (var credencial in credenciales)
                    RepositorioCredenciales.Eliminar(credencial); RepositorioCredenciales.GuardarCambios();

                this.RepositorioUsuarios.Modificar(usuario);
                this.RepositorioUsuarios.GuardarCambios();

                scope.Complete();
            }
        }

        public void EliminarUsuarioPorEmpleado(int idEmpleado, DtoUsuario DtoUsuario)
        {
            var usuario = this.RepositorioUsuarios.Obtener(m => m.Activo && m.IdEmpleado == idEmpleado);

            if (usuario == null)
                //throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);
                return;

            if (usuario.Id == DtoUsuario.IdUsuario)
                throw new SOTException(Recursos.Usuarios.can_not_delete_current_user_exception);

            BusinessServicePermisos.ValidarPermisos(DtoUsuario, new DtoPermisos { EliminarUsuarios = true });

            var credenciales = RepositorioCredenciales.ObtenerCredencialesUsuario(usuario.Id);

            var date = DateTime.Now;

            usuario.Activo = false;
            usuario.IdUsuarioModifico = DtoUsuario.Id;
            usuario.IdUsuarioElimino = DtoUsuario.Id;
            usuario.FechaModificacion = date;
            usuario.FechaEliminacion = date;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                foreach (var credencial in credenciales)
                    RepositorioCredenciales.Eliminar(credencial); RepositorioCredenciales.GuardarCambios();

                this.RepositorioUsuarios.Modificar(usuario);
                this.RepositorioUsuarios.GuardarCambios();

                scope.Complete();
            }
        }

        /// <summary>
        /// METHOD
        /// <para>28/09/2015</para>
        /// <para>by Mauricio Alberto Perez Poot - Zeruel01</para>
        /// </summary>
        private void ValidateAlias(string Alias, int userID = 0)
        {
            var toUpper = Alias.Trim().ToUpper();

            if (string.IsNullOrEmpty(Alias))
                throw new SOTException(Recursos.Generales.empty_field_exception, "Alias");

            if (!UtilidadesRegex.LetrasONumeros(Alias))//, "/^[a-zA-Z0-9]+@[a-zA-Z]+[.]{0,1}[a-zA-Z]*$/"))
                throw new SOTException(Recursos.Usuarios.invalid_alias_format_exception);

            if (this.RepositorioUsuarios.Alguno(m => m.Activo && m.Alias.ToUpper() == toUpper && m.Id != userID))
                throw new SOTException(Recursos.Usuarios.alias_in_use_exception, Alias);
        }

        
        private void ValidateUser(/*string Nombre, string ApellidoPaterno, string ApellidoMaterno, */int idEmpleado, int id = 0)
        {
            if (this.RepositorioUsuarios.Alguno(m => m.IdEmpleado == idEmpleado &&
                                                     m.Id != id && m.Activo))
                throw new SOTException(Recursos.Usuarios.usuario_duplicado_excepcion);
        }

        

        public DtoUsuario ObtenerUsuarioPorCredencial(int idEmpleado, DtoCredencial credencial)
        {
            //if (alias == null)
            //    throw new ArgumentNullException();

            if (credencial == null || credencial.SinValor)
                throw new ArgumentNullException();

            var usuario = RepositorioUsuarios.ObtenerUsuarioConRolCredencial(idEmpleado, credencial.TipoCredencial);

            var idRol = BusinessServicioEmpleadosCompartido.ObtenerIdRol(usuario.IdEmpleado);

            usuario.RolTmp = this.BusinessServiceRoles.ObtenerRolConPermisos(idRol);

            Credencial credencialContrasena;

            if (usuario == null || (credencialContrasena = usuario.Credenciales.FirstOrDefault(m => m.TipoCredencial == credencial.TipoCredencial)) == null)
                throw new SOTException(Recursos.Usuarios.invalid_user_or_credentials_exception);

            var validador = FabricaValidadorCredenciales.ObtenerValidador(credencialContrasena.TipoCredencial);

            validador.ValidarCredencial(credencialContrasena, credencial);

            return (DtoUsuario)usuario;
        }

        //public string ObtenerAlias(int idUsuario)
        //{
        //    return RepositorioUsuarios.ObtenerElementos(m => m.Id == idUsuario).Select(m => m.Alias).FirstOrDefault();
        //}


        public void ValidaUsuario(int idUsuario)
        {
            if (!RepositorioUsuarios.Alguno(m => m.Id == idUsuario && m.Activo))
                throw new SOTException(Recursos.Usuarios.id_usuario_no_encontrado_excepcion, idUsuario.ToString());
        }


        public Usuario ObtenerUsuarioPorEmpleado(int idEmpleado, DtoUsuario usuario)
        {
            BusinessServicePermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarUsuarios = true });

            return RepositorioUsuarios.Obtener(m => m.Activo && m.IdEmpleado == idEmpleado);
        }


        public void ModificarUsuario(int idUsuario, string nuevoAlias, List<Credencial> credencialesCrear, List<Credencial> credencialesEliminar, List<DtoCredencial> comprobaciones, DtoUsuario UsuarioActual)
        {
            var usuario = RepositorioUsuarios.Obtener(m => m.Activo && m.Id == idUsuario);

            if (usuario == null)
                throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

            //if (usuario.IdRol <= 0)
            //    throw new SOTException(Recursos.Usuarios.no_role_exception);

            BusinessServicePermisos.ValidarPermisos(UsuarioActual, new DtoPermisos { ModificarUsuarios = true });

            if (comprobaciones.GroupBy(m => m.TipoCredencial).Count() != comprobaciones.Count)
                throw new SOTException("Existen confirmaciones del mismo tipo");

            if (usuario.Credenciales.GroupBy(m => m.TipoCredencial).Count() != usuario.Credenciales.Count)
                throw new SOTException("Existen credenciales del mismo tipo");

            //if (usuario.ApellidoMaterno == null)
            //    usuario.ApellidoMaterno = string.Empty;

            ValidateMandatoryFields(usuario);

            this.ValidateAlias(nuevoAlias, usuario.Id);
            //this.ValidateUser(usuario.IdEmpleado);

            var credencialesOriginales = RepositorioCredenciales.ObtenerCredencialesUsuario(usuario.Id);

            foreach (var credencialComp in comprobaciones)
            {
                var credencial = credencialesCrear.FirstOrDefault(m => m.TipoCredencial == credencialComp.TipoCredencial);

                if (credencial == null)
                    throw new SOTException("No se han proporcionado credenciales de tipo " + credencialComp.TipoCredencial.Descripcion());

                var validador = FabricaValidadorCredenciales.ObtenerValidador(credencial.TipoCredencial);

                validador.ValidarCredencial(credencial, credencialComp);
                credencial.IdUsuario = usuario.Id;

                var credencialOriginal = credencialesOriginales.FirstOrDefault(m => m.TipoCredencial == credencial.TipoCredencial);

                if (credencialOriginal != null)
                {
                    credencialOriginal.SincronizarPrimitivas(credencial);
                    RepositorioCredenciales.Modificar(credencialOriginal);
                }
                else
                {
                    RepositorioCredenciales.Agregar(credencial);
                }
            }

            foreach (var credencial in credencialesOriginales)
            {
                if (credencialesEliminar.Any(m => m.TipoCredencial == credencial.TipoCredencial))
                    RepositorioCredenciales.Eliminar(credencial);
            }

            //BusinessServiceRoles.ValidateRole(usuario.IdRol);

            var date = DateTime.Now;

            usuario.Alias = nuevoAlias;
            usuario.FechaModificacion = date;
            usuario.IdUsuarioModifico = UsuarioActual.Id;

            //usuario.Credenciales = new Dominio.Nucleo.Utilidades.SotCollection<Credencial>();
            //usuario.Credenciales.Add(credencial);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioCredenciales.GuardarCambios();

                this.RepositorioUsuarios.Modificar(usuario);
                this.RepositorioUsuarios.GuardarCambios();

                scope.Complete();

            }
        }


        public List<Credencial.TiposCredencial> ObtenerTiposCredencialesExistentes(int idUsuario, DtoUsuario usuario)
        {
            return RepositorioCredenciales.ObtenerElementos(m => m.IdUsuario == idUsuario).Select(m => m.IdTipoCredencial).ToList().Select(m => (Credencial.TiposCredencial)m).ToList();
        }

        #region métodos internal

        DtoUsuario IServicioUsuariosInterno.ObtenerUsuarioPorCredencial(DtoCredencial credencial)
        {
            if (string.IsNullOrWhiteSpace(credencial.IdentificadorUsuario))
                throw new SOTException(Recursos.Permisos.alias_requerido_excepcion);

            if (credencial == null || credencial.SinValor)
                throw new ArgumentNullException();

            var usuario = RepositorioUsuarios.ObtenerUsuarioConRolCredencial(credencial.IdentificadorUsuario, credencial.TipoCredencial);

            if (usuario == null)
                throw new SOTException(Recursos.Usuarios.invalid_user_or_credentials_exception);

            var idRol = BusinessServicioEmpleadosCompartido.ObtenerIdRol(usuario.IdEmpleado);

            usuario.RolTmp = this.BusinessServiceRoles.ObtenerRolConPermisos(idRol);

            Credencial credencialContrasena;

            if (usuario == null || (credencialContrasena = usuario.Credenciales.FirstOrDefault(m => m.TipoCredencial == credencial.TipoCredencial)) == null)
                throw new SOTException(Recursos.Usuarios.invalid_user_or_credentials_exception);

            var validador = FabricaValidadorCredenciales.ObtenerValidador(credencialContrasena.TipoCredencial);

            validador.ValidarCredencial(credencialContrasena, credencial);

            return (DtoUsuario)usuario;
        }

        DtoUsuario IServicioUsuariosInterno.ObtenerUsuarioPorId(int idUsuario) 
        { 
   
            var usuario = RepositorioUsuarios.Obtener(m=> m.Activo && m.Id == idUsuario);

            if (usuario == null)
                throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

            var idRol = BusinessServicioEmpleadosCompartido.ObtenerIdRol(usuario.IdEmpleado);

            usuario.RolTmp = this.BusinessServiceRoles.ObtenerRolConPermisos(idRol);

            return (DtoUsuario)usuario;
        
        }

        #endregion
    }
}