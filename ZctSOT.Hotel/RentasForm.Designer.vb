﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RentasForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.nudHabitaciones = New System.Windows.Forms.NumericUpDown()
        Me.nudPersonasExtra = New System.Windows.Forms.NumericUpDown()
        Me.nudPaquetes = New System.Windows.Forms.NumericUpDown()
        Me.nudHospedajeExtra = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblHabitacionPUnidad = New System.Windows.Forms.Label()
        Me.lblHospedajeExtraPUnidad = New System.Windows.Forms.Label()
        Me.lblPersonasExtraPUnidad = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAceptar = New ExtraComponents.GelButton()
        Me.btnVPoints = New ExtraComponents.GelButton()
        Me.btnCancelarEntrada = New ExtraComponents.GelButton()
        Me.btnSolicitarCancelacion = New ExtraComponents.GelButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtTarjeta = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.chbAPie = New System.Windows.Forms.CheckBox()
        Me.txtMatricula = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cbTarifa = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.txtColor = New System.Windows.Forms.TextBox()
        Me.txtMarca = New System.Windows.Forms.TextBox()
        Me.txtModelo = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblPersonasExtraTotal = New System.Windows.Forms.Label()
        Me.lblHospedajeExtraTotal = New System.Windows.Forms.Label()
        Me.lblHabitacionTotal = New System.Windows.Forms.Label()
        Me.lblPaqueteTotal = New System.Windows.Forms.Label()
        Me.lblDescuentosTotal = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblDatosHabitacion = New System.Windows.Forms.Label()
        Me.lblDatosHospedaje = New System.Windows.Forms.Label()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        CType(Me.nudHabitaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPersonasExtra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPaquetes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudHospedajeExtra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'nudHabitaciones
        '
        Me.nudHabitaciones.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.nudHabitaciones.Location = New System.Drawing.Point(15, 193)
        Me.nudHabitaciones.Name = "nudHabitaciones"
        Me.nudHabitaciones.ReadOnly = True
        Me.nudHabitaciones.Size = New System.Drawing.Size(39, 22)
        Me.nudHabitaciones.TabIndex = 0
        '
        'nudPersonasExtra
        '
        Me.nudPersonasExtra.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.nudPersonasExtra.Location = New System.Drawing.Point(15, 219)
        Me.nudPersonasExtra.Name = "nudPersonasExtra"
        Me.nudPersonasExtra.ReadOnly = True
        Me.nudPersonasExtra.Size = New System.Drawing.Size(39, 22)
        Me.nudPersonasExtra.TabIndex = 1
        '
        'nudPaquetes
        '
        Me.nudPaquetes.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.nudPaquetes.Location = New System.Drawing.Point(15, 271)
        Me.nudPaquetes.Name = "nudPaquetes"
        Me.nudPaquetes.ReadOnly = True
        Me.nudPaquetes.Size = New System.Drawing.Size(39, 22)
        Me.nudPaquetes.TabIndex = 3
        '
        'nudHospedajeExtra
        '
        Me.nudHospedajeExtra.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.nudHospedajeExtra.Location = New System.Drawing.Point(15, 245)
        Me.nudHospedajeExtra.Name = "nudHospedajeExtra"
        Me.nudHospedajeExtra.ReadOnly = True
        Me.nudHospedajeExtra.Size = New System.Drawing.Size(39, 22)
        Me.nudHospedajeExtra.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(60, 197)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 15)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Habitación (+)"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(60, 223)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 15)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Personas extra (+)"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(60, 249)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 15)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Hospedaje extra (+)"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(60, 275)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 15)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Paquetes (+)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(60, 301)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 15)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Descuentos (-)"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHabitacionPUnidad
        '
        Me.lblHabitacionPUnidad.AutoSize = True
        Me.lblHabitacionPUnidad.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHabitacionPUnidad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.lblHabitacionPUnidad.Location = New System.Drawing.Point(200, 197)
        Me.lblHabitacionPUnidad.Name = "lblHabitacionPUnidad"
        Me.lblHabitacionPUnidad.Size = New System.Drawing.Size(14, 15)
        Me.lblHabitacionPUnidad.TabIndex = 21
        Me.lblHabitacionPUnidad.Text = "$"
        Me.lblHabitacionPUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblHospedajeExtraPUnidad
        '
        Me.lblHospedajeExtraPUnidad.AutoSize = True
        Me.lblHospedajeExtraPUnidad.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHospedajeExtraPUnidad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.lblHospedajeExtraPUnidad.Location = New System.Drawing.Point(200, 249)
        Me.lblHospedajeExtraPUnidad.Name = "lblHospedajeExtraPUnidad"
        Me.lblHospedajeExtraPUnidad.Size = New System.Drawing.Size(14, 15)
        Me.lblHospedajeExtraPUnidad.TabIndex = 22
        Me.lblHospedajeExtraPUnidad.Text = "$"
        Me.lblHospedajeExtraPUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPersonasExtraPUnidad
        '
        Me.lblPersonasExtraPUnidad.AutoSize = True
        Me.lblPersonasExtraPUnidad.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPersonasExtraPUnidad.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.lblPersonasExtraPUnidad.Location = New System.Drawing.Point(200, 223)
        Me.lblPersonasExtraPUnidad.Name = "lblPersonasExtraPUnidad"
        Me.lblPersonasExtraPUnidad.Size = New System.Drawing.Size(14, 15)
        Me.lblPersonasExtraPUnidad.TabIndex = 23
        Me.lblPersonasExtraPUnidad.Text = "$"
        Me.lblPersonasExtraPUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.Panel1.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel1.Location = New System.Drawing.Point(0, 392)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(765, 85)
        Me.Panel1.TabIndex = 6
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoScroll = True
        Me.FlowLayoutPanel1.Controls.Add(Me.btnAceptar)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnVPoints)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancelarEntrada)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnSolicitarCancelacion)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(765, 85)
        Me.FlowLayoutPanel1.TabIndex = 12
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.BackColor = System.Drawing.Color.White
        Me.btnAceptar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnAceptar.Checkable = False
        Me.btnAceptar.Checked = False
        Me.btnAceptar.DisableHighlight = False
        Me.btnAceptar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnAceptar.GelColor = System.Drawing.Color.White
        Me.btnAceptar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnAceptar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnAceptar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_icono_habilitada
        Me.btnAceptar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnAceptar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnAceptar.Location = New System.Drawing.Point(657, 0)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(104, 80)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'btnVPoints
        '
        Me.btnVPoints.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVPoints.BackColor = System.Drawing.Color.White
        Me.btnVPoints.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnVPoints.Checkable = False
        Me.btnVPoints.Checked = False
        Me.btnVPoints.DisableHighlight = False
        Me.btnVPoints.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnVPoints.GelColor = System.Drawing.Color.White
        Me.btnVPoints.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnVPoints.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnVPoints.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnVPoints.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnVPoints.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnVPoints.Location = New System.Drawing.Point(545, 0)
        Me.btnVPoints.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.btnVPoints.Name = "btnVPoints"
        Me.btnVPoints.Size = New System.Drawing.Size(104, 80)
        Me.btnVPoints.TabIndex = 12
        Me.btnVPoints.Text = "V POINTS"
        Me.btnVPoints.UseVisualStyleBackColor = False
        '
        'btnCancelarEntrada
        '
        Me.btnCancelarEntrada.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelarEntrada.BackColor = System.Drawing.Color.White
        Me.btnCancelarEntrada.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCancelarEntrada.Checkable = False
        Me.btnCancelarEntrada.Checked = False
        Me.btnCancelarEntrada.DisableHighlight = False
        Me.btnCancelarEntrada.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelarEntrada.GelColor = System.Drawing.Color.White
        Me.btnCancelarEntrada.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCancelarEntrada.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCancelarEntrada.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnCancelarEntrada.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnCancelarEntrada.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCancelarEntrada.Location = New System.Drawing.Point(433, 0)
        Me.btnCancelarEntrada.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.btnCancelarEntrada.Name = "btnCancelarEntrada"
        Me.btnCancelarEntrada.Size = New System.Drawing.Size(104, 80)
        Me.btnCancelarEntrada.TabIndex = 10
        Me.btnCancelarEntrada.Text = "CANCELAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ENTRADA"
        Me.btnCancelarEntrada.UseVisualStyleBackColor = False
        '
        'btnSolicitarCancelacion
        '
        Me.btnSolicitarCancelacion.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSolicitarCancelacion.BackColor = System.Drawing.Color.White
        Me.btnSolicitarCancelacion.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnSolicitarCancelacion.Checkable = False
        Me.btnSolicitarCancelacion.Checked = False
        Me.btnSolicitarCancelacion.DisableHighlight = False
        Me.btnSolicitarCancelacion.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnSolicitarCancelacion.GelColor = System.Drawing.Color.White
        Me.btnSolicitarCancelacion.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnSolicitarCancelacion.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnSolicitarCancelacion.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnSolicitarCancelacion.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnSolicitarCancelacion.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnSolicitarCancelacion.Location = New System.Drawing.Point(321, 0)
        Me.btnSolicitarCancelacion.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.btnSolicitarCancelacion.Name = "btnSolicitarCancelacion"
        Me.btnSolicitarCancelacion.Size = New System.Drawing.Size(104, 80)
        Me.btnSolicitarCancelacion.TabIndex = 11
        Me.btnSolicitarCancelacion.Text = "SOLICITAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CANCELACION"
        Me.btnSolicitarCancelacion.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtTarjeta)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.chbAPie)
        Me.GroupBox1.Controls.Add(Me.txtMatricula)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.cbTarifa)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.txtColor)
        Me.GroupBox1.Controls.Add(Me.txtMarca)
        Me.GroupBox1.Controls.Add(Me.txtModelo)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(443, 111)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(310, 266)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de entrada"
        '
        'txtTarjeta
        '
        Me.txtTarjeta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTarjeta.Enabled = False
        Me.txtTarjeta.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtTarjeta.Location = New System.Drawing.Point(159, 49)
        Me.txtTarjeta.Name = "txtTarjeta"
        Me.txtTarjeta.Size = New System.Drawing.Size(127, 22)
        Me.txtTarjeta.TabIndex = 43
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(81, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 15)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Tarjeta V"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chbAPie
        '
        Me.chbAPie.AutoSize = True
        Me.chbAPie.Checked = True
        Me.chbAPie.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chbAPie.Location = New System.Drawing.Point(159, 93)
        Me.chbAPie.Name = "chbAPie"
        Me.chbAPie.Size = New System.Drawing.Size(53, 19)
        Me.chbAPie.TabIndex = 40
        Me.chbAPie.Text = "A pie"
        Me.chbAPie.UseVisualStyleBackColor = True
        '
        'txtMatricula
        '
        Me.txtMatricula.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMatricula.Enabled = False
        Me.txtMatricula.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtMatricula.Location = New System.Drawing.Point(159, 126)
        Me.txtMatricula.Name = "txtMatricula"
        Me.txtMatricula.Size = New System.Drawing.Size(127, 22)
        Me.txtMatricula.TabIndex = 38
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(77, 130)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 15)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Matrícula"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(98, 24)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(36, 15)
        Me.Label14.TabIndex = 36
        Me.Label14.Text = "Tarifa"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbTarifa
        '
        Me.cbTarifa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTarifa.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.cbTarifa.FormattingEnabled = True
        Me.cbTarifa.Location = New System.Drawing.Point(159, 20)
        Me.cbTarifa.Name = "cbTarifa"
        Me.cbTarifa.Size = New System.Drawing.Size(127, 23)
        Me.cbTarifa.TabIndex = 35
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_entrada
        Me.PictureBox1.Location = New System.Drawing.Point(7, 192)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(65, 65)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 31
        Me.PictureBox1.TabStop = False
        '
        'txtColor
        '
        Me.txtColor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtColor.Enabled = False
        Me.txtColor.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtColor.Location = New System.Drawing.Point(159, 238)
        Me.txtColor.Name = "txtColor"
        Me.txtColor.Size = New System.Drawing.Size(127, 22)
        Me.txtColor.TabIndex = 6
        '
        'txtMarca
        '
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.Enabled = False
        Me.txtMarca.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtMarca.Location = New System.Drawing.Point(159, 154)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.Size = New System.Drawing.Size(127, 22)
        Me.txtMarca.TabIndex = 4
        '
        'txtModelo
        '
        Me.txtModelo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModelo.Enabled = False
        Me.txtModelo.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtModelo.Location = New System.Drawing.Point(159, 182)
        Me.txtModelo.Name = "txtModelo"
        Me.txtModelo.Size = New System.Drawing.Size(127, 22)
        Me.txtModelo.TabIndex = 3
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(98, 242)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(36, 15)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "Color"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(94, 158)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(40, 15)
        Me.Label18.TabIndex = 23
        Me.Label18.Text = "Marca"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label19.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label19.Location = New System.Drawing.Point(86, 186)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(48, 15)
        Me.Label19.TabIndex = 22
        Me.Label19.Text = "Modelo"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPersonasExtraTotal
        '
        Me.lblPersonasExtraTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPersonasExtraTotal.ForeColor = System.Drawing.Color.Navy
        Me.lblPersonasExtraTotal.Location = New System.Drawing.Point(340, 223)
        Me.lblPersonasExtraTotal.Name = "lblPersonasExtraTotal"
        Me.lblPersonasExtraTotal.Size = New System.Drawing.Size(80, 15)
        Me.lblPersonasExtraTotal.TabIndex = 28
        Me.lblPersonasExtraTotal.Text = "$0.00"
        Me.lblPersonasExtraTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHospedajeExtraTotal
        '
        Me.lblHospedajeExtraTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHospedajeExtraTotal.ForeColor = System.Drawing.Color.Navy
        Me.lblHospedajeExtraTotal.Location = New System.Drawing.Point(340, 249)
        Me.lblHospedajeExtraTotal.Name = "lblHospedajeExtraTotal"
        Me.lblHospedajeExtraTotal.Size = New System.Drawing.Size(80, 15)
        Me.lblHospedajeExtraTotal.TabIndex = 27
        Me.lblHospedajeExtraTotal.Text = "$0.00"
        Me.lblHospedajeExtraTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHabitacionTotal
        '
        Me.lblHabitacionTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHabitacionTotal.ForeColor = System.Drawing.Color.Navy
        Me.lblHabitacionTotal.Location = New System.Drawing.Point(340, 197)
        Me.lblHabitacionTotal.Name = "lblHabitacionTotal"
        Me.lblHabitacionTotal.Size = New System.Drawing.Size(80, 15)
        Me.lblHabitacionTotal.TabIndex = 26
        Me.lblHabitacionTotal.Text = "$0.00"
        Me.lblHabitacionTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPaqueteTotal
        '
        Me.lblPaqueteTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPaqueteTotal.ForeColor = System.Drawing.Color.Navy
        Me.lblPaqueteTotal.Location = New System.Drawing.Point(340, 275)
        Me.lblPaqueteTotal.Name = "lblPaqueteTotal"
        Me.lblPaqueteTotal.Size = New System.Drawing.Size(80, 15)
        Me.lblPaqueteTotal.TabIndex = 30
        Me.lblPaqueteTotal.Text = "$0.00"
        Me.lblPaqueteTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescuentosTotal
        '
        Me.lblDescuentosTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblDescuentosTotal.ForeColor = System.Drawing.Color.Navy
        Me.lblDescuentosTotal.Location = New System.Drawing.Point(340, 301)
        Me.lblDescuentosTotal.Name = "lblDescuentosTotal"
        Me.lblDescuentosTotal.Size = New System.Drawing.Size(80, 15)
        Me.lblDescuentosTotal.TabIndex = 29
        Me.lblDescuentosTotal.Text = "$0.00"
        Me.lblDescuentosTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(200, 356)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(33, 15)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "Total"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Lato", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblTotal.ForeColor = System.Drawing.Color.Navy
        Me.lblTotal.Location = New System.Drawing.Point(292, 349)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(128, 28)
        Me.lblTotal.TabIndex = 38
        Me.lblTotal.Text = "$0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDatosHabitacion
        '
        Me.lblDatosHabitacion.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblDatosHabitacion.ForeColor = System.Drawing.Color.Gray
        Me.lblDatosHabitacion.Location = New System.Drawing.Point(447, 21)
        Me.lblDatosHabitacion.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDatosHabitacion.Name = "lblDatosHabitacion"
        Me.lblDatosHabitacion.Size = New System.Drawing.Size(310, 37)
        Me.lblDatosHabitacion.TabIndex = 39
        Me.lblDatosHabitacion.Text = "Habitación X"
        Me.lblDatosHabitacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDatosHospedaje
        '
        Me.lblDatosHospedaje.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblDatosHospedaje.ForeColor = System.Drawing.Color.Gray
        Me.lblDatosHospedaje.Location = New System.Drawing.Point(447, 65)
        Me.lblDatosHospedaje.Margin = New System.Windows.Forms.Padding(0)
        Me.lblDatosHospedaje.Name = "lblDatosHospedaje"
        Me.lblDatosHospedaje.Size = New System.Drawing.Size(310, 37)
        Me.lblDatosHospedaje.TabIndex = 40
        Me.lblDatosHospedaje.Text = "Tiempo de hospedaje - X"
        Me.lblDatosHospedaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pbLogo
        '
        Me.pbLogo.Font = New System.Drawing.Font("Lato Black", 45.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.pbLogo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.pbLogo.Image = Global.ZctSOT.Hotel.My.Resources.Resources.logo_VBoutique
        Me.pbLogo.Location = New System.Drawing.Point(0, 0)
        Me.pbLogo.Margin = New System.Windows.Forms.Padding(0)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(399, 146)
        Me.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbLogo.TabIndex = 31
        Me.pbLogo.TabStop = False
        Me.pbLogo.Text = "V Motel  Boutique"
        '
        'RentasForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(764, 477)
        Me.Controls.Add(Me.lblDatosHospedaje)
        Me.Controls.Add(Me.lblDatosHabitacion)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.pbLogo)
        Me.Controls.Add(Me.lblPaqueteTotal)
        Me.Controls.Add(Me.lblDescuentosTotal)
        Me.Controls.Add(Me.lblPersonasExtraTotal)
        Me.Controls.Add(Me.lblHospedajeExtraTotal)
        Me.Controls.Add(Me.lblHabitacionTotal)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblPersonasExtraPUnidad)
        Me.Controls.Add(Me.lblHospedajeExtraPUnidad)
        Me.Controls.Add(Me.lblHabitacionPUnidad)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.nudPaquetes)
        Me.Controls.Add(Me.nudHospedajeExtra)
        Me.Controls.Add(Me.nudPersonasExtra)
        Me.Controls.Add(Me.nudHabitaciones)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "RentasForm"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.nudHabitaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPersonasExtra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPaquetes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudHospedajeExtra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents nudHabitaciones As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudPersonasExtra As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudPaquetes As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudHospedajeExtra As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblHabitacionPUnidad As System.Windows.Forms.Label
    Friend WithEvents lblHospedajeExtraPUnidad As System.Windows.Forms.Label
    Friend WithEvents lblPersonasExtraPUnidad As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPersonasExtraTotal As System.Windows.Forms.Label
    Friend WithEvents lblHospedajeExtraTotal As System.Windows.Forms.Label
    Friend WithEvents lblHabitacionTotal As System.Windows.Forms.Label
    Friend WithEvents lblPaqueteTotal As System.Windows.Forms.Label
    Friend WithEvents lblDescuentosTotal As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtColor As System.Windows.Forms.TextBox
    Friend WithEvents txtMarca As System.Windows.Forms.TextBox
    Friend WithEvents txtModelo As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblDatosHabitacion As System.Windows.Forms.Label
    Friend WithEvents lblDatosHospedaje As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As GelButton
    Friend WithEvents btnCancelarEntrada As GelButton
    Friend WithEvents cbTarifa As System.Windows.Forms.ComboBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnSolicitarCancelacion As GelButton
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtMatricula As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chbAPie As CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnVPoints As ExtraComponents.GelButton
    Friend WithEvents txtTarjeta As System.Windows.Forms.TextBox
End Class
