﻿Public Class CapturaHuellaDigitalForm

    Private _huellaDigital As Byte() = Nothing

    Public Property HuellaDigital As Byte()
        Get
            Return _huellaDigital
        End Get
        Private Set(value As Byte())
            _huellaDigital = value
        End Set
    End Property

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        _huellaDigital = New Byte(1) {}
        Close()
    End Sub
End Class