﻿Imports Transversal.Excepciones
Imports ZctSOT.Hotel.Controladores
Imports System.Collections.Generic
Imports System.Linq
Imports Transversal.Extensiones
Imports Transversal.Dtos

Public Class HorariosPreciosForm

    Private esNuevo As Boolean = False
    Private tipoActual As Modelo.Entidades.TipoHabitacion

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        panelEdicion.Hide()
        esNuevo = False

        btnNuevo.Enabled = True
        btnModificar.Enabled = True
        btnEliminar.Enabled = True
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        'Dim controlador As New ControladorGlobal

        'tipoActual.Nombre = txtNombrePaquete.Text
        'tipoActual.LeyendaDescuento = txtLeyenda.Text
        'tipoActual.Precio = nudPrecio.Value
        'tipoActual.Descuento = nudDescuento.Value

        'If clbTipos.CheckedItems.Count = 0 Then
        '    Throw New SOTException(My.Resources.Errores.tipo_habitacion_no_seleccionado_exception)
        'End If

        'Dim item As Modelo.TipoHabitacion = CType(clbTipos.CheckedItems(0), Modelo.TipoHabitacion)

        'tipoActual.IdTipoHabitacion = item.Id

        'If esNuevo Then
        '    controlador.AplicacionServicioPaquetes.CrearPaquete(tipoActual, controlador.UsuarioActual)
        'Else
        '    controlador.AplicacionServicioPaquetes.ModificarPaquete(tipoActual, controlador.UsuarioActual)
        'End If

        ConfiguracionPaquetesForm_Load(Nothing, Nothing)

        btnNuevo.Enabled = True
        btnModificar.Enabled = True
        btnEliminar.Enabled = True

        If esNuevo Then
            MessageBox.Show(My.Resources.Mensajes.creacion_paquete_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show(My.Resources.Mensajes.modificacion_paquete_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        'txtNombrePaquete.Clear()
        'txtLeyenda.Clear()
        'LimpiarChecksTipos(-1)

        'nudDescuento.Value = 0
        'nudPrecio.Value = 0

        esNuevo = True

        'paqueteActual = New Modelo.Paquete

        panelEdicion.Show()

        btnNuevo.Enabled = False
        btnModificar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If dgvTiposHabitaciones.SelectedRows.Count <> 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_paquetes_seleccionados)
        End If

        Dim tipo As Modelo.Entidades.TipoHabitacion = CType(dgvTiposHabitaciones.SelectedRows(0).DataBoundItem, Modelo.Entidades.TipoHabitacion)

        'txtNombrePaquete.Text = paquete.Nombre
        'txtLeyenda.Text = paquete.LeyendaDescuento

        'For ix As Integer = 0 To clbTipos.Items.Count - 1

        '    Dim item As Modelo.TipoHabitacion = CType(clbTipos.Items(ix), Modelo.TipoHabitacion)

        '    clbTipos.SetItemChecked(ix, item.Id = paquete.IdTipoHabitacion)
        '    clbTipos.SetSelected(ix, item.Id = paquete.IdTipoHabitacion)
        'Next

        'nudDescuento.Value = paquete.Descuento
        'nudPrecio.Value = paquete.Precio

        esNuevo = False

        'paqueteActual = paquete

        panelEdicion.Show()

        btnNuevo.Enabled = False
        btnModificar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvTiposHabitaciones.SelectedRows.Count <> 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_paquetes_seleccionados)
        End If

        Dim paquete As Modelo.Entidades.Paquete = CType(dgvTiposHabitaciones.SelectedRows(0).DataBoundItem, Modelo.Entidades.Paquete)

        If MessageBox.Show(String.Format(My.Resources.Mensajes.eliminar_paquete, paquete.Nombre, paquete.NombreTipo), _
                           My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim controlador As New ControladorGlobal

            controlador.AplicacionServicioPaquetes.EliminarPaquete(paquete.Id, controlador.UsuarioActual)

            MessageBox.Show(My.Resources.Mensajes.eliminacion_paquete_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)

            ConfiguracionPaquetesForm_Load(Nothing, Nothing)
        End If
    End Sub

    Private Sub CargarTipos()
        dgvTiposHabitaciones.DataSource = New Controladores.ControladorGlobal().AplicacionServicioTiposHabitaciones.ObtenerTiposActivos()
    End Sub

    Private Sub ConfiguracionPaquetesForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarTipos()
        panelEdicion.Hide()
    End Sub

    Private Sub panelEdicion_VisibleChanged(sender As Object, e As EventArgs) Handles panelEdicion.VisibleChanged
        If panelEdicion.Visible Then
            dgvTiposHabitaciones.Size = dgvTiposHabitaciones.MinimumSize
        Else
            dgvTiposHabitaciones.Size = FlowLayoutPanel1.Size
        End If
    End Sub

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().



        Dim tarifas As List(Of DtoEnum) = EnumExtensions.ComoListaDto(Of Modelo.Entidades.ConfiguracionTarifa.Tarifas)()

        cbTarifas.DataSource = tarifas
        cbTarifas.DisplayMember = ObjectExtensions.NombrePropiedad(Of DtoEnum, String)(Function(m) m.Nombre)
        cbTarifas.ValueMember = ObjectExtensions.NombrePropiedad(Of DtoEnum, Object)(Function(m) m.Valor)
    End Sub
End Class