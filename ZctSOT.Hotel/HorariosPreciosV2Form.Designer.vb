﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HorariosPreciosV2Form
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbTarifas = New System.Windows.Forms.ComboBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.dgvTiposHabitaciones = New System.Windows.Forms.DataGridView()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosLimpiezaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosLavadoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosDetalladoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosEntradaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosSuciaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoHabitacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.panelEdicion = New System.Windows.Forms.Panel()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        CType(Me.dgvTiposHabitaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipoHabitacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.panelEdicion.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbTarifas
        '
        Me.cbTarifas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTarifas.FormattingEnabled = True
        Me.cbTarifas.Location = New System.Drawing.Point(5, 35)
        Me.cbTarifas.Name = "cbTarifas"
        Me.cbTarifas.Size = New System.Drawing.Size(162, 23)
        Me.cbTarifas.TabIndex = 42
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CheckBox1.ForeColor = System.Drawing.Color.Black
        Me.CheckBox1.Location = New System.Drawing.Point(149, 218)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(199, 19)
        Me.CheckBox1.TabIndex = 43
        Me.CheckBox1.Text = "Omitir en reporte de recamareras"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'dgvTiposHabitaciones
        '
        Me.dgvTiposHabitaciones.AllowUserToAddRows = False
        Me.dgvTiposHabitaciones.AllowUserToDeleteRows = False
        Me.dgvTiposHabitaciones.AutoGenerateColumns = False
        Me.dgvTiposHabitaciones.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.dgvTiposHabitaciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTiposHabitaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTiposHabitaciones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DescripcionDataGridViewTextBoxColumn, Me.MinutosLimpiezaDataGridViewTextBoxColumn, Me.MinutosLavadoDataGridViewTextBoxColumn, Me.MinutosDetalladoDataGridViewTextBoxColumn, Me.MinutosEntradaDataGridViewTextBoxColumn, Me.MinutosSuciaDataGridViewTextBoxColumn})
        Me.dgvTiposHabitaciones.DataSource = Me.TipoHabitacionBindingSource
        Me.dgvTiposHabitaciones.Location = New System.Drawing.Point(3, 316)
        Me.dgvTiposHabitaciones.MinimumSize = New System.Drawing.Size(1027, 346)
        Me.dgvTiposHabitaciones.Name = "dgvTiposHabitaciones"
        Me.dgvTiposHabitaciones.ReadOnly = True
        Me.dgvTiposHabitaciones.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        Me.dgvTiposHabitaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTiposHabitaciones.Size = New System.Drawing.Size(1027, 346)
        Me.dgvTiposHabitaciones.TabIndex = 49
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Descripción"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosLimpiezaDataGridViewTextBoxColumn
        '
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.DataPropertyName = "MinutosLimpieza"
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.HeaderText = "Limpieza"
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.Name = "MinutosLimpiezaDataGridViewTextBoxColumn"
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosLavadoDataGridViewTextBoxColumn
        '
        Me.MinutosLavadoDataGridViewTextBoxColumn.DataPropertyName = "MinutosLavado"
        Me.MinutosLavadoDataGridViewTextBoxColumn.HeaderText = "Lavado"
        Me.MinutosLavadoDataGridViewTextBoxColumn.Name = "MinutosLavadoDataGridViewTextBoxColumn"
        Me.MinutosLavadoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosDetalladoDataGridViewTextBoxColumn
        '
        Me.MinutosDetalladoDataGridViewTextBoxColumn.DataPropertyName = "MinutosDetallado"
        Me.MinutosDetalladoDataGridViewTextBoxColumn.HeaderText = "Detallado"
        Me.MinutosDetalladoDataGridViewTextBoxColumn.Name = "MinutosDetalladoDataGridViewTextBoxColumn"
        Me.MinutosDetalladoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosEntradaDataGridViewTextBoxColumn
        '
        Me.MinutosEntradaDataGridViewTextBoxColumn.DataPropertyName = "MinutosEntrada"
        Me.MinutosEntradaDataGridViewTextBoxColumn.HeaderText = "T Entrada"
        Me.MinutosEntradaDataGridViewTextBoxColumn.Name = "MinutosEntradaDataGridViewTextBoxColumn"
        Me.MinutosEntradaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosSuciaDataGridViewTextBoxColumn
        '
        Me.MinutosSuciaDataGridViewTextBoxColumn.DataPropertyName = "MinutosSucia"
        Me.MinutosSuciaDataGridViewTextBoxColumn.HeaderText = "T Sucia"
        Me.MinutosSuciaDataGridViewTextBoxColumn.Name = "MinutosSuciaDataGridViewTextBoxColumn"
        Me.MinutosSuciaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TipoHabitacionBindingSource
        '
        Me.TipoHabitacionBindingSource.DataSource = GetType(Modelo.Entidades.TipoHabitacion)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(3, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 15)
        Me.Label1.TabIndex = 50
        Me.Label1.Text = "Tipo"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupBox3.Controls.Add(Me.btnModificar)
        Me.GroupBox3.Controls.Add(Me.btnNuevo)
        Me.GroupBox3.Controls.Add(Me.btnEliminar)
        Me.GroupBox3.Location = New System.Drawing.Point(10, 33)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox3.Size = New System.Drawing.Size(1188, 704)
        Me.GroupBox3.TabIndex = 51
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Horarios y precios por tipo de habitación"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.panelEdicion)
        Me.FlowLayoutPanel1.Controls.Add(Me.dgvTiposHabitaciones)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(7, 24)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1027, 676)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'panelEdicion
        '
        Me.panelEdicion.BackColor = System.Drawing.Color.Transparent
        Me.panelEdicion.Controls.Add(Me.TextBox7)
        Me.panelEdicion.Controls.Add(Me.Label8)
        Me.panelEdicion.Controls.Add(Me.TextBox6)
        Me.panelEdicion.Controls.Add(Me.Label7)
        Me.panelEdicion.Controls.Add(Me.TextBox5)
        Me.panelEdicion.Controls.Add(Me.Label6)
        Me.panelEdicion.Controls.Add(Me.TextBox4)
        Me.panelEdicion.Controls.Add(Me.Label5)
        Me.panelEdicion.Controls.Add(Me.TextBox3)
        Me.panelEdicion.Controls.Add(Me.Label4)
        Me.panelEdicion.Controls.Add(Me.TextBox2)
        Me.panelEdicion.Controls.Add(Me.Label3)
        Me.panelEdicion.Controls.Add(Me.TextBox1)
        Me.panelEdicion.Controls.Add(Me.Label2)
        Me.panelEdicion.Controls.Add(Me.btnCancelar)
        Me.panelEdicion.Controls.Add(Me.Label1)
        Me.panelEdicion.Controls.Add(Me.btnAceptar)
        Me.panelEdicion.Controls.Add(Me.CheckBox1)
        Me.panelEdicion.Controls.Add(Me.cbTarifas)
        Me.panelEdicion.Location = New System.Drawing.Point(0, 0)
        Me.panelEdicion.Margin = New System.Windows.Forms.Padding(0)
        Me.panelEdicion.Name = "panelEdicion"
        Me.panelEdicion.Size = New System.Drawing.Size(1027, 313)
        Me.panelEdicion.TabIndex = 1
        '
        'TextBox7
        '
        Me.TextBox7.Location = New System.Drawing.Point(3, 216)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(97, 22)
        Me.TextBox7.TabIndex = 64
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 199)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 15)
        Me.Label8.TabIndex = 63
        Me.Label8.Text = "Reservaciones"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(123, 156)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(218, 22)
        Me.TextBox6.TabIndex = 62
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(147, 139)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(125, 15)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Descripción extendida"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(3, 156)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(97, 22)
        Me.TextBox5.TabIndex = 60
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 139)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 15)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "Puntos Detallado"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(244, 96)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(97, 22)
        Me.TextBox4.TabIndex = 58
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(244, 79)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 15)
        Me.Label5.TabIndex = 57
        Me.Label5.Text = "Puntos Detallado"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(123, 96)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(97, 22)
        Me.TextBox3.TabIndex = 56
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(123, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 15)
        Me.Label4.TabIndex = 55
        Me.Label4.Text = "T Limpieza"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(3, 96)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(97, 22)
        Me.TextBox2.TabIndex = 54
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 15)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "T Detallado"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(179, 36)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(162, 22)
        Me.TextBox1.TabIndex = 52
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(179, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 15)
        Me.Label2.TabIndex = 51
        Me.Label2.Text = "Descripción"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelar.Location = New System.Drawing.Point(755, 263)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(127, 40)
        Me.btnCancelar.TabIndex = 11
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnAceptar.Location = New System.Drawing.Point(889, 263)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(127, 40)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnModificar.Location = New System.Drawing.Point(1051, 75)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(127, 40)
        Me.btnModificar.TabIndex = 3
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnNuevo.Location = New System.Drawing.Point(1051, 24)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(127, 40)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnEliminar.Location = New System.Drawing.Point(1051, 126)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(127, 40)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'HorariosPreciosV2Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1209, 733)
        Me.Controls.Add(Me.GroupBox3)
        Me.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "HorariosPreciosV2Form"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Horarios y precios"
        CType(Me.dgvTiposHabitaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipoHabitacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.panelEdicion.ResumeLayout(False)
        Me.panelEdicion.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbTarifas As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents dgvTiposHabitaciones As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioHotelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioMotelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdConfiguracionNegocioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosPrimerHospedajeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosSegundoHospedajeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosLimpiezaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosLavadoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosDetalladoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosEntradaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosSuciaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoHabitacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents panelEdicion As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
