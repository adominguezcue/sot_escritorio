﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Negocio.Habitaciones
Imports Transversal.Dependencias
Imports Microsoft.Practices.Unity.Configuration
Imports Modelo
Imports Negocio.TiposHabitacion
Imports Negocio.Paquetes
Imports Negocio.Reservaciones
Imports Negocio.Rentas
Imports Negocio.TareasMantenimiento
Imports Negocio.TareasLimpieza
Imports Negocio.Empleados
Imports Negocio.Articulos
Imports Negocio.ConfiguracionesGlobales
Imports Negocio.Taxis

Namespace Controladores
    Public Class ControladorGlobal
        Inherits ControladorBase

        Public ReadOnly Property AplicacionServicioConfiguracionesGlobales() As IServicioConfiguracionesGlobales
            Get
                Return _aplicacionServicioConfiguracionesGlobales.Value
            End Get
        End Property

        Private _aplicacionServicioConfiguracionesGlobales As New Lazy(Of IServicioConfiguracionesGlobales)(Function()
                                                                                                                Dim item As IServicioConfiguracionesGlobales = FabricaDependencias.Instancia.Resolver(Of IServicioConfiguracionesGlobales)()
                                                                                                                Return item
                                                                                                            End Function)



        Public ReadOnly Property AplicacionServicioEmpleados() As IServicioEmpleados
            Get
                Return _aplicacionServicioEmpleados.Value
            End Get
        End Property

        Private _aplicacionServicioEmpleados As New Lazy(Of IServicioEmpleados)(Function()
                                                                                    Dim item As IServicioEmpleados = FabricaDependencias.Instancia.Resolver(Of IServicioEmpleados)()
                                                                                    Return item
                                                                                End Function)

        Public ReadOnly Property AplicacionServicioTareasMantenimiento() As IServicioTareasMantenimiento
            Get
                Return _aplicacionServicioTareasMantenimiento.Value
            End Get
        End Property

        Private _aplicacionServicioTareasMantenimiento As New Lazy(Of IServicioTareasMantenimiento)(Function()
                                                                                                        Dim item As IServicioTareasMantenimiento = FabricaDependencias.Instancia.Resolver(Of IServicioTareasMantenimiento)()
                                                                                                        Return item
                                                                                                    End Function)

        Public ReadOnly Property AplicacionServicioTiposHabitaciones() As IServicioTiposHabitaciones
            Get
                Return _aplicacionServicioTiposHabitaciones.Value
            End Get
        End Property

        Private _aplicacionServicioTiposHabitaciones As New Lazy(Of IServicioTiposHabitaciones)(Function()
                                                                                                    Dim item As IServicioTiposHabitaciones = FabricaDependencias.Instancia.Resolver(Of IServicioTiposHabitaciones)()
                                                                                                    Return item
                                                                                                End Function)

        Public ReadOnly Property AplicacionServicioPaquetes() As IServicioPaquetes
            Get
                Return _aplicacionServicioPaquetes.Value
            End Get
        End Property

        Private _aplicacionServicioPaquetes As New Lazy(Of IServicioPaquetes)(Function()
                                                                                  Dim item As IServicioPaquetes = FabricaDependencias.Instancia.Resolver(Of IServicioPaquetes)()
                                                                                  Return item
                                                                              End Function)

        Public ReadOnly Property AplicacionServicioReservaciones() As IServicioReservaciones
            Get
                Return _aplicacionServicioReservaciones.Value
            End Get
        End Property

        Private _aplicacionServicioReservaciones As New Lazy(Of IServicioReservaciones)(Function()
                                                                                            Dim item As IServicioReservaciones = FabricaDependencias.Instancia.Resolver(Of IServicioReservaciones)()
                                                                                            Return item
                                                                                        End Function)
    End Class
End Namespace