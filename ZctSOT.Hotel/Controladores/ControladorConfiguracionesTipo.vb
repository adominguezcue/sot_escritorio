﻿Imports Negocio.ConfiguracionesTipo
Imports Transversal.Dependencias

Namespace Controladores

    Public Class ControladorConfiguracionesTipo
        Inherits ControladorBase

        Public ReadOnly Property AplicacionServicioConfiguracionesTipo() As IServicioConfiguracionesTipo
            Get
                Return _aplicacionServicioConfiguracionesTipo.Value
            End Get
        End Property

        Private _aplicacionServicioConfiguracionesTipo As New Lazy(Of IServicioConfiguracionesTipo)(Function()
                                                                                                        Dim item As IServicioConfiguracionesTipo = FabricaDependencias.Instancia.Resolver(Of IServicioConfiguracionesTipo)()
                                                                                                        Return item
                                                                                                    End Function)

        Function ObtenerConfiguracionTipo(idConfiguracionTipo As Integer, tarifaElegida As Modelo.Entidades.ConfiguracionTarifa.Tarifas) As Modelo.Entidades.ConfiguracionTipo
            Return AplicacionServicioConfiguracionesTipo.ObtenerConfiguracionTipo(idConfiguracionTipo, tarifaElegida)
        End Function


    End Class

End Namespace