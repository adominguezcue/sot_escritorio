﻿Imports Modelo
Imports Negocio.TareasLimpieza
Imports Transversal.Dependencias
Imports Modelo.Entidades

Namespace Controladores
    Public Class ControladorTareasLimpieza
        Inherits ControladorBase
        Private ReadOnly Property AplicacionServicioTareasLimpieza() As IServicioTareasLimpieza
            Get
                Return _aplicacionServicioTareasLimpieza.Value
            End Get
        End Property

        Private _aplicacionServicioTareasLimpieza As New Lazy(Of IServicioTareasLimpieza)(Function()
                                                                                              Dim item As IServicioTareasLimpieza = FabricaDependencias.Instancia.Resolver(Of IServicioTareasLimpieza)()
                                                                                              Return item
                                                                                          End Function)

        Function ObtenerTareaActualConEmpleados(idHabitacion As Integer) As TareaLimpieza
            Return AplicacionServicioTareasLimpieza.ObtenerTareaActualConEmpleados(idHabitacion, UsuarioActual)
        End Function

        Function ObtenerDetallesTarea(idHabitacion As Integer) As Dtos.DtoDetallesTarea
            Return AplicacionServicioTareasLimpieza.ObtenerDetallesTarea(idHabitacion, UsuarioActual)
        End Function

        Sub FinalizarLimpieza(idHabitacion As Integer, detalles As String)
            AplicacionServicioTareasLimpieza.FinalizarLimpieza(idHabitacion, detalles, UsuarioActual)
        End Sub

        Sub AsignarEmpleadosATarea(idHabitacion As Integer, idsEmpleados As List(Of Integer), tipoLimpieza As TareaLimpieza.TiposLimpieza)
            AplicacionServicioTareasLimpieza.AsignarEmpleadosATarea(idHabitacion, idsEmpleados, tipoLimpieza, UsuarioActual)
        End Sub

        Sub CambiarEmpleadosTarea(idHabitacion As Integer, idsEmpleados As List(Of Integer), tipoLimpieza As TareaLimpieza.TiposLimpieza)
            AplicacionServicioTareasLimpieza.CambiarEmpleadosTarea(idHabitacion, idsEmpleados, tipoLimpieza, UsuarioActual)
        End Sub

        Sub AsignarSupervisorATarea(idHabitacion As Integer, idsEmpleado As Integer)
            AplicacionServicioTareasLimpieza.AsignarSupervisorATarea(idHabitacion, idsEmpleado, UsuarioActual)
        End Sub

        Sub CambiarSupervisorTarea(idHabitacion As Integer, idsEmpleado As Integer)
            AplicacionServicioTareasLimpieza.CambiarSupervisorTarea(idHabitacion, idsEmpleado, UsuarioActual)
        End Sub

        Function ObtenerEmpleadosLimpiezaActivos() As List(Of LimpiezaEmpleado)
            Return AplicacionServicioTareasLimpieza.ObtenerEmpleadosLimpiezaActivos(UsuarioActual)
        End Function

    End Class
End Namespace