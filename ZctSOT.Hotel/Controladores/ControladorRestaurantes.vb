﻿Imports Negocio.Restaurantes
Imports Transversal.Dependencias

Namespace Controladores

    Public Class ControladorRestaurantes
        Inherits ControladorBase


        Public ReadOnly Property AplicacionServicioRestaurantes() As IServicioRestaurantes
            Get
                Return _aplicacionServicioRestaurantes.Value
            End Get
        End Property

        Private _aplicacionServicioRestaurantes As New Lazy(Of IServicioRestaurantes)(Function()
                                                                                          Dim item As IServicioRestaurantes = FabricaDependencias.Instancia.Resolver(Of IServicioRestaurantes)()
                                                                                          Return item
                                                                                      End Function)

        Sub CrearOrden(orden As Modelo.Entidades.OrdenRestaurante)
            AplicacionServicioRestaurantes.CrearOrden(orden, 0, UsuarioActual)
        End Sub

        Sub CancelarOrden(idOrden As Integer, motivo As String)
            Dim huellaDigital As Byte() = ObtenerHuella()

            AplicacionServicioRestaurantes.CancelarOrden(idOrden, motivo, huellaDigital)
        End Sub

        Sub EntregarProductos(idOrden As Integer, idsArticulosOrden As List(Of Integer), idMesero As Integer)

            AplicacionServicioRestaurantes.EntregarProductos(idOrden, idsArticulosOrden, idMesero, UsuarioActual)
        End Sub

        Sub EntregarOrdenACliente(idOrden As Integer)

            AplicacionServicioRestaurantes.EntregarOrdenACliente(idOrden, UsuarioActual)
        End Sub

        Sub FinalizarElaboracionProductos(idOrden As Integer, idsArticulosOrden As List(Of Integer))

            AplicacionServicioRestaurantes.FinalizarElaboracionProductos(idOrden, idsArticulosOrden, UsuarioActual)
        End Sub

        Function ObtenerDetallesOrdenesPendientes() As List(Of Modelo.Entidades.OrdenRestaurante)
            Return AplicacionServicioRestaurantes.ObtenerDetallesOrdenesPendientes(0, UsuarioActual)
        End Function

        Function ObtenerArticulosPrepararOrdenes() As List(Of Modelo.Dtos.DtoArticuloPrepararConsulta)
            Return AplicacionServicioRestaurantes.ObtenerArticulosPrepararOrdenes(UsuarioActual)
        End Function

    End Class

End Namespace