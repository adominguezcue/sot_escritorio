﻿Imports Modelo
Imports Transversal.Excepciones
Imports Modelo.Seguridad.Dtos


Public MustInherit Class ControladorBase
    Property UsuarioActual() As DtoUsuario
        Get
            Return New DtoUsuario()
        End Get
        Set(value As DtoUsuario)

        End Set
    End Property

    Function ObtenerHuella() As Byte()
        Dim chF As New CapturaHuellaDigitalForm
        chF.ShowDialog()

        If IsNothing(chF.HuellaDigital) Then
            Throw New SOTException("Operación cancelada")
        End If

        Return chF.HuellaDigital
    End Function
End Class
