﻿Imports ZctSOT.Hotel.Generadores
Imports Transversal.Dependencias
Imports ZctSOT.Hotel.Interfaces

Namespace Controladores
    Public Class ControladorFormasPago
        Private ReadOnly Property ZctSOTSelectorPago() As IGeneradorSelectorPago
            Get
                Return _zctSOTSelectorPago.Value
            End Get
        End Property

        Private _zctSOTSelectorPago As New Lazy(Of IGeneradorSelectorPago)(Function()
                                                                               Dim item As IGeneradorSelectorPago = FabricaDependencias.Instancia.Resolver(Of IGeneradorSelectorPago)()
                                                                               Return item
                                                                           End Function)

        Public Function ObtenerSelectorPago(valorPago As Decimal, Optional incluirReservacion As Boolean = False, Optional ocultarPropina As Boolean = False) As ISelectorPago
            Return ZctSOTSelectorPago.ObtenerInstancia(valorPago, incluirReservacion, ocultarPropina)
        End Function
    End Class
End Namespace
