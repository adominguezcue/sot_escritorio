﻿Imports Negocio.Rentas
Imports Transversal.Dependencias

Namespace Controladores

    Public Class ControladorRentas
        Inherits ControladorBase

        Private ReadOnly Property AplicacionServicioRentas() As IServicioRentas
            Get
                Return _aplicacionServicioRentas.Value
            End Get
        End Property

        Private _aplicacionServicioRentas As New Lazy(Of IServicioRentas)(Function()
                                                                              Dim item As IServicioRentas = FabricaDependencias.Instancia.Resolver(Of IServicioRentas)()
                                                                              Return item
                                                                          End Function)

        Function ObtenerRentaActualPorHabitacion(idHabitacion As Integer) As Modelo.Entidades.Renta
            Return AplicacionServicioRentas.ObtenerRentaActualPorHabitacion(idHabitacion)
        End Function

        Function ObtenerMaximoEstadoComandasPendientes(idRenta As Integer) As Modelo.Entidades.Comanda.Estados?
            Return AplicacionServicioRentas.ObtenerMaximoEstadoComandasPendientes(idRenta)
        End Function

        Sub CrearComanda(nuevaComanda As Modelo.Entidades.Comanda)
            AplicacionServicioRentas.CrearComanda(nuevaComanda, UsuarioActual)
        End Sub

        Function ObtenerDetallesComandasPendientes(idRenta As Integer) As List(Of Modelo.Entidades.Comanda)
            Return AplicacionServicioRentas.ObtenerDetallesComandasPendientes(idRenta, UsuarioActual)
        End Function

        Sub CrearRenta(rentaNueva As Modelo.Entidades.Renta, requierePermisosEspeciales As Boolean)
            If requierePermisosEspeciales Then
                AplicacionServicioRentas.CrearRenta(rentaNueva, ObtenerHuella())
            Else
                AplicacionServicioRentas.CrearRenta(rentaNueva, UsuarioActual)
            End If

        End Sub

        Sub CancelarComanda(idComanda As Integer, motivo As String)

            Dim huellaDigital = ObtenerHuella()

            AplicacionServicioRentas.CancelarComanda(idComanda, motivo, huellaDigital)
        End Sub

        'Sub AgregarHorasExtra(idRenta As Integer, cantidadExtensiones As Integer, informacionPagos As List(Of Modelo.Dtos.DtoInformacionPago))

        '    Dim huellaDigital = ObtenerHuella()

        '    AplicacionServicioRentas.AgregarHorasExtra(idRenta, cantidadExtensiones, informacionPagos, huellaDigital)
        'End Sub

        Sub EntregarProductos(idComanda As Integer, idsArticulosComandas As List(Of Integer), idEmpleadoCobro As Integer)
            AplicacionServicioRentas.EntregarProductos(idComanda, idsArticulosComandas, idEmpleadoCobro, UsuarioActual)
        End Sub

        Sub PagarComanda(idComanda As Integer, pagos As List(Of Modelo.Dtos.DtoInformacionPago), propina As Modelo.Entidades.Propina)
            AplicacionServicioRentas.PagarComanda(idComanda, pagos, propina, UsuarioActual)
        End Sub

        Sub CobrarComanda(idComanda As Integer)
            AplicacionServicioRentas.CobrarComanda(idComanda, UsuarioActual)
        End Sub

        'Sub AgregarPersonasExtra(idRenta As Integer, cantidadPersonasExtra As Integer, pagos As List(Of Modelo.Dtos.DtoInformacionPago))
        '    Dim huellaDigital = ObtenerHuella()

        '    AplicacionServicioRentas.AgregarPersonasExtra(idRenta, cantidadPersonasExtra, pagos, huellaDigital)
        'End Sub

        Function ObtenerArticulosPrepararComandas() As List(Of Modelo.Dtos.DtoArticuloPrepararConsulta)
            Return AplicacionServicioRentas.ObtenerArticulosPrepararComandas(UsuarioActual)
        End Function

        Public Sub ModificarComanda(idComanda As Integer, articulos As List(Of Modelo.Dtos.DtoArticuloPrepararGeneracion))
            AplicacionServicioRentas.ModificarComanda(idComanda, articulos, UsuarioActual)
        End Sub

        Sub FinalizarElaboracionProductos(idComanda As Integer, idsArticulosComanda As List(Of Integer))
            AplicacionServicioRentas.FinalizarElaboracionProductos(idComanda, idsArticulosComanda, UsuarioActual)
        End Sub

        Function ObtenerAutomovilesUltimaRenta(idHabitacion As Integer) As List(Of Modelo.Entidades.AutomovilRenta)
            Return AplicacionServicioRentas.ObtenerAutomovilesUltimaRenta(idHabitacion)
        End Function

        'Sub ActualizarDatosAutomoviles(idRenta As Integer, automoviles As List(Of Modelo.Dtos.DtoAutomovil))
        '    AplicacionServicioRentas.ActualizarDatosAutomoviles(idRenta, automoviles, UsuarioActual)
        'End Sub

        Sub ActualizarDatosHabitacion(idRenta As Integer, cantidadExtensiones As Integer, cantidadPersonasExtra As Integer, _
                                       automoviles As List(Of Modelo.Dtos.DtoAutomovil), pagos As List(Of Modelo.Dtos.DtoInformacionPago))
            Dim huellaDigital = ObtenerHuella()

            AplicacionServicioRentas.ActualizarDatosHabitacion(idRenta, cantidadExtensiones, cantidadPersonasExtra, automoviles, pagos, huellaDigital)
        End Sub
    End Class

End Namespace