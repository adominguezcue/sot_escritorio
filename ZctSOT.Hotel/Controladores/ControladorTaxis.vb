﻿Imports Modelo
Imports Transversal.Dependencias
Imports Negocio.Taxis
Imports Modelo.Entidades

Namespace Controladores
    Public Class ControladorTaxis
        Inherits ControladorBase

        Private ReadOnly Property AplicacionServicioTaxis() As IServicioTaxis
            Get
                Return _aplicacionServicioTaxis.Value
            End Get
        End Property

        Private _aplicacionServicioTaxis As New Lazy(Of IServicioTaxis)(Function()
                                                                            Dim item As IServicioTaxis = FabricaDependencias.Instancia.Resolver(Of IServicioTaxis)()
                                                                            Return item
                                                                        End Function)

        Sub CobrarOrden(IdOrdenTaxi As Integer, idEmpleado As Integer)
            AplicacionServicioTaxis.CobrarOrden(IdOrdenTaxi, idEmpleado, UsuarioActual)
        End Sub

        Function ObtenerCantidadOrdenesPendientes() As Integer
            Return AplicacionServicioTaxis.ObtenerCantidadOrdenesPendientes(UsuarioActual)
        End Function

        Sub CrearOrden(orden As OrdenTaxi)
            AplicacionServicioTaxis.CrearOrden(orden, UsuarioActual)
        End Sub

        Sub CancelarOrden(idOrden As Integer, motivo As String)

            Dim huellaDigital As Byte() = ObtenerHuella()

            AplicacionServicioTaxis.CancelarOrden(idOrden, motivo, huellaDigital)
        End Sub

        Function ObtenerOrdenesPendientes() As List(Of OrdenTaxi)
            Return AplicacionServicioTaxis.ObtenerOrdenesPendientes(UsuarioActual)
        End Function

        Sub ModificarPrecio(idOrdenTaxi As Integer, nuevoPrecio As Decimal)
            AplicacionServicioTaxis.ModificarPrecioTaxi(idOrdenTaxi, nuevoPrecio, UsuarioActual)
        End Sub

    End Class
End Namespace