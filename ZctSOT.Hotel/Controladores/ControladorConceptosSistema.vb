﻿Imports Modelo
Imports Modelo.Entidades
Imports Negocio.ConceptosSistema
Imports Transversal.Dependencias

Namespace Controladores
    Public Class ControladorConceptosSistema
        Inherits ControladorBase

        Public ReadOnly Property AplicacionServicioConceptosSistema() As IServicioConceptosSistema
            Get
                Return _aplicacionServicioConceptosSistema.Value
            End Get
        End Property

        Private _aplicacionServicioConceptosSistema As New Lazy(Of IServicioConceptosSistema)(Function()
                                                                                                  Dim item As IServicioConceptosSistema = FabricaDependencias.Instancia.Resolver(Of IServicioConceptosSistema)()
                                                                                                  Return item
                                                                                              End Function)

        Function ObtenerConceptos() As List(Of Modelo.Entidades.ConceptoSistema)
            Return AplicacionServicioConceptosSistema.ObtenerConceptos()
        End Function

        Function ObtenerTiposConcepto() As List(Of Modelo.Entidades.ConceptoSistema.TiposConceptos)
            Return AplicacionServicioConceptosSistema.ObtenerTiposConcepto()
        End Function

        Friend Sub CrearConcepto(conceptoSistema As ConceptoSistema)
            AplicacionServicioConceptosSistema.CrearConcepto(conceptoSistema, UsuarioActual)
        End Sub

        Friend Sub ModificarConcepto(conceptoSistema As ConceptoSistema)
            AplicacionServicioConceptosSistema.ModificarConcepto(conceptoSistema, UsuarioActual)
        End Sub

        Friend Sub EliminarConcepto(idConceptoSistema As Integer)
            AplicacionServicioConceptosSistema.EliminarConcepto(idConceptoSistema, UsuarioActual)
        End Sub
    End Class
End Namespace