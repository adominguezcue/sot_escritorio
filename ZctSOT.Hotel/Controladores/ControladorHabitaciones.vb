﻿Imports Negocio.Habitaciones
Imports Transversal.Dependencias

Namespace Controladores
    Public Class ControladorHabitaciones
        Inherits ControladorBase

        Private ReadOnly Property AplicacionServicioHabitaciones() As IServicioHabitaciones
            Get
                Return _aplicacionServicioHabitaciones.Value
            End Get
        End Property

        Private _aplicacionServicioHabitaciones As New Lazy(Of IServicioHabitaciones)(Function()
                                                                                          Dim item As IServicioHabitaciones = FabricaDependencias.Instancia.Resolver(Of IServicioHabitaciones)()
                                                                                          Return item
                                                                                      End Function)

        Function ObtenerBloqueoActual(idHabitacion As Integer) As Modelo.Entidades.BloqueoHabitacion
            Return AplicacionServicioHabitaciones.ObtenerBloqueoActual(idHabitacion, UsuarioActual)
        End Function

        Function ObtenerHabitacion(idHabitacion As Integer) As Modelo.Entidades.Habitacion
            Return AplicacionServicioHabitaciones.ObtenerHabitacion(idHabitacion)
        End Function

        Function ObtenerActivasConTipos() As List(Of Modelo.Entidades.Habitacion)
            Return AplicacionServicioHabitaciones.ObtenerActivasConTipos()
        End Function

        Function ObtenerDatosHabitacion(numeroHabitacion As String) As Modelo.Dtos.DtoDatosHabitacion
            Return AplicacionServicioHabitaciones.ObtenerDatosHabitacion(numeroHabitacion, UsuarioActual)
        End Function

        Sub PrepararHabitacion(idHabitacion As Integer)
            AplicacionServicioHabitaciones.PrepararHabitacion(idHabitacion, UsuarioActual)
        End Sub

        Sub BloquearHabitacion(idHabitacion As Integer, motivo As String)
            AplicacionServicioHabitaciones.BloquearHabitacion(idHabitacion, motivo, UsuarioActual)
        End Sub

        Sub PonerEnMantenimiento(idHabitacion As Integer, descripcion As String)
            AplicacionServicioHabitaciones.PonerEnMantenimiento(idHabitacion, descripcion, UsuarioActual)
        End Sub

        Sub FinalizarMantenimiento(idHabitacion As Integer)
            AplicacionServicioHabitaciones.FinalizarMantenimiento(idHabitacion, UsuarioActual)
        End Sub

        Sub FinalizarOcupacion(idHabitacion As Integer)
            AplicacionServicioHabitaciones.FinalizarOcupacion(idHabitacion, UsuarioActual)
        End Sub

        Sub CancelarPreparacionHabitacion(idHabitacion As Integer)
            AplicacionServicioHabitaciones.CancelarPreparacionHabitacion(idHabitacion, UsuarioActual)
        End Sub


    End Class
End Namespace