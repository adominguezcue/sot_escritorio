﻿Imports Negocio.ReportesMatriculas
Imports Transversal.Dependencias

Namespace Controladores

    Public Class ControladorReportesMatriculas
        Inherits ControladorBase

        Public ReadOnly Property AplicacionServicioReportesMatriculas() As IServicioReportesMatriculas
            Get
                Return _aplicacionServicioReportesMatriculas.Value
            End Get
        End Property

        Private _aplicacionServicioReportesMatriculas As New Lazy(Of IServicioReportesMatriculas)(Function()
                                                                                                      Dim item As IServicioReportesMatriculas = FabricaDependencias.Instancia.Resolver(Of IServicioReportesMatriculas)()
                                                                                                      Return item
                                                                                                  End Function)
        Function ObtenerReportesMatricula(matricula As String) As List(Of Modelo.Entidades.ReporteMatricula)
            Return AplicacionServicioReportesMatriculas.ObtenerReportesMatricula(matricula, UsuarioActual)
        End Function

        Sub CrearReporte(reporte As Modelo.Entidades.ReporteMatricula)
            AplicacionServicioReportesMatriculas.CrearReporte(reporte, UsuarioActual)
        End Sub

        Sub EliminarReporte(idReporte As Integer)
            AplicacionServicioReportesMatriculas.EliminarReporte(idReporte, UsuarioActual)
        End Sub

        Function PoseeReportes(matricula As String) As Boolean
            Return AplicacionServicioReportesMatriculas.PoseeReportes(matricula)
        End Function

    End Class

End Namespace