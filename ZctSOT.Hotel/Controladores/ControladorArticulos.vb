﻿Imports Negocio.Articulos
Imports Transversal.Dependencias

Namespace Controladores

    Public Class ControladorArticulos
        Inherits ControladorBase

        Public ReadOnly Property AplicacionServicioArticulos() As IServicioArticulos
            Get
                Return _aplicacionServicioArticulos.Value
            End Get
        End Property

        Private _aplicacionServicioArticulos As New Lazy(Of IServicioArticulos)(Function()
                                                                                    Dim item As IServicioArticulos = FabricaDependencias.Instancia.Resolver(Of IServicioArticulos)()
                                                                                    Return item
                                                                                End Function)

        Function ObtenerTiposConArticulos(filtro As String) As List(Of Modelo.Entidades.TipoArticulo)
            Return AplicacionServicioArticulos.ObtenerTiposConArticulos(UsuarioActual, filtro)
        End Function

        Function ObtenerArticulosPorTipo(nombreTipo As String) As List(Of Modelo.Entidades.Articulo)
            Return AplicacionServicioArticulos.ObtenerArticulosPorTipo(nombreTipo, UsuarioActual)
        End Function


    End Class

End Namespace