﻿Imports Negocio.Pagos
Imports Transversal.Dependencias

Namespace Controladores

    Public Class ControladorPagos
        Inherits ControladorBase

        Public ReadOnly Property AplicacionServicioPagos() As IServicioPagos
            Get
                Return _aplicacionServicioPagos.Value
            End Get
        End Property

        Private _aplicacionServicioPagos As New Lazy(Of IServicioPagos)(Function()
                                                                            Dim item As IServicioPagos = FabricaDependencias.Instancia.Resolver(Of IServicioPagos)()
                                                                            Return item
                                                                        End Function)

        Function ObtenerTiposValidosConMixtos(incluirReservacion As Boolean) As List(Of Modelo.Dtos.DtoGrupoPago)
            Return AplicacionServicioPagos.ObtenerTiposValidosConMixtos()
        End Function
    End Class

End Namespace