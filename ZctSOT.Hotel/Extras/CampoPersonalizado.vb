﻿Imports System.ComponentModel
Imports System.Drawing.Design

Public Class CampoPersonalizado

    Public Sub New()
        InitializeComponent()
    End Sub

    <Editor("System.ComponentModel.Design.MultilineStringEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", GetType(UITypeEditor))>
    <SettingsBindable(True)>
    Public Property Title As String
        Get
            Return Titulo.Text
        End Get
        Set(value As String)
            Titulo.Text = value
        End Set
    End Property

    <EditorBrowsable>
    Public Property CharacterCasing As CharacterCasing
        Get
            Return txt.CharacterCasing
        End Get
        Set(value As CharacterCasing)
            txt.CharacterCasing = value
        End Set
    End Property

    Public Property TipoCampo As TiposCampo
        Get
            Return _tipoCampo
        End Get
        Set(value As TiposCampo)
            _tipoCampo = value
        End Set
    End Property

    'Public Overrides Property Font As Font
    '    Get
    '        Return MyBase.Font
    '    End Get
    '    Set(value As Font)
    '        MyBase.Font = value
    '        Titulo.Font = MyBase.Font
    '        txt.Font = MyBase.Font
    '    End Set
    'End Property

    Private _tipoCampo As TiposCampo

    <Editor>
    Public Property Value As String
        Get
            Return txt.Text
        End Get
        Set(value As String)
            txt.Text = value
        End Set
    End Property

    Public Enum TiposCampo
        Texto
        Calendario
        Numero
    End Enum
End Class
