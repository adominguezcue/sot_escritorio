﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CampoPersonalizado
    Inherits System.Windows.Forms.UserControl

    ''' <summary> 
    ''' Variable del diseñador requerida.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary> 
    ''' Limpiar los recursos que se estén utilizando.
    ''' </summary>
    ''' <param name="disposing">true si los recursos administrados se deben desechar false en caso contrario.</param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If (disposing And Not components Is Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Código generado por el Diseñador de componentes"

    ''' <summary> 
    ''' Método necesario para admitir el Diseñador. No se puede modificar 
    ''' el contenido del método con el editor de código.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.txt = New System.Windows.Forms.TextBox()
        Me.Titulo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txt
        '
        Me.txt.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt.Location = New System.Drawing.Point(0, 22)
        Me.txt.Margin = New System.Windows.Forms.Padding(0)
        Me.txt.Multiline = True
        Me.txt.Name = "txt"
        Me.txt.Size = New System.Drawing.Size(135, 27)
        Me.txt.TabIndex = 7
        '
        'Titulo
        '
        Me.Titulo.AutoSize = True
        Me.Titulo.Location = New System.Drawing.Point(0, 0)
        Me.Titulo.Margin = New System.Windows.Forms.Padding(0)
        Me.Titulo.Name = "Titulo"
        Me.Titulo.Size = New System.Drawing.Size(38, 15)
        Me.Titulo.TabIndex = 8
        Me.Titulo.Text = "Título"
        '
        'CampoPersonalizado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Controls.Add(Me.Titulo)
        Me.Controls.Add(Me.txt)
        Me.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Name = "CampoPersonalizado"
        Me.Size = New System.Drawing.Size(135, 50)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents txt As TextBox
    Friend WithEvents Titulo As Label

#End Region

End Class
