﻿Public Class CreacionOrdenTaxiForm

    Private _ordenTaxi As Modelo.Entidades.OrdenTaxi

    Public Sub New(Optional ordenTaxi As Modelo.Entidades.OrdenTaxi = Nothing)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        _ordenTaxi = ordenTaxi
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim controlador As New Controladores.ControladorTaxis

        If IsNothing(_ordenTaxi) Then

            Dim orden As New Modelo.Entidades.OrdenTaxi
            orden.Activo = True
            orden.Precio = nudPrecio.Value

            controlador.CrearOrden(orden)

        Else
            controlador.ModificarPrecio(_ordenTaxi.Id, nudPrecio.Value)
        End If

        Close()
    End Sub

    Private Sub CreacionOrdenTaxiForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim controladorT As New Controladores.ControladorTaxis

        If IsNothing(_ordenTaxi) Then
            Dim controlador As New Controladores.ControladorGlobal

            Dim configuracionGlobal As Modelo.Entidades.ConfiguracionGlobal = controlador.AplicacionServicioConfiguracionesGlobales.ObtenerConfiguracionGlobal()

            nudPrecio.Value = configuracionGlobal.PrecioPorDefectoTaxi
        Else
            nudPrecio.Value = _ordenTaxi.Precio
        End If



    End Sub
End Class