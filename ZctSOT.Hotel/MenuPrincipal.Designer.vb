﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuPrincipal))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.contenedorBotones = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnMenu = New GelButton()
        Me.btnCorteTurno = New GelButton()
        Me.btnFajillas = New GelButton()
        Me.btnGastos = New GelButton()
        Me.btnReportarMatricula = New GelButton()
        Me.btnHabilitarValets = New GelButton()
        Me.btnHabilitarMeseros = New GelButton()
        Me.btnHabilitarRecamareras = New GelButton()
        Me.btnFacturar = New GelButton()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.contenedorBotones.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Lato", 21.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(983, 84)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Menú principal"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.contenedorBotones, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(983, 173)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'contenedorBotones
        '
        Me.contenedorBotones.AutoScroll = True
        Me.contenedorBotones.BackColor = System.Drawing.Color.Transparent
        Me.contenedorBotones.Controls.Add(Me.btnMenu)
        Me.contenedorBotones.Controls.Add(Me.btnCorteTurno)
        Me.contenedorBotones.Controls.Add(Me.btnFajillas)
        Me.contenedorBotones.Controls.Add(Me.btnGastos)
        Me.contenedorBotones.Controls.Add(Me.btnReportarMatricula)
        Me.contenedorBotones.Controls.Add(Me.btnHabilitarValets)
        Me.contenedorBotones.Controls.Add(Me.btnHabilitarMeseros)
        Me.contenedorBotones.Controls.Add(Me.btnHabilitarRecamareras)
        Me.contenedorBotones.Controls.Add(Me.btnFacturar)
        Me.contenedorBotones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.contenedorBotones.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.contenedorBotones.Location = New System.Drawing.Point(0, 88)
        Me.contenedorBotones.Margin = New System.Windows.Forms.Padding(0, 4, 4, 0)
        Me.contenedorBotones.Name = "contenedorBotones"
        Me.contenedorBotones.Size = New System.Drawing.Size(979, 100)
        Me.contenedorBotones.TabIndex = 1
        '
        'btnMenu
        '
        Me.btnMenu.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMenu.BackColor = System.Drawing.Color.Transparent
        Me.btnMenu.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnMenu.Checkable = False
        Me.btnMenu.Checked = False
        Me.btnMenu.DisableHighlight = False
        Me.btnMenu.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnMenu.GelColor = System.Drawing.Color.White
        Me.btnMenu.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnMenu.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnMenu.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_menu
        Me.btnMenu.ImageLocation = New System.Drawing.Point(6, 0)
        Me.btnMenu.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnMenu.Location = New System.Drawing.Point(916, 3)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(60, 80)
        Me.btnMenu.TabIndex = 1
        Me.btnMenu.Text = "MENÚ"
        Me.btnMenu.UseVisualStyleBackColor = False
        '
        'btnCorteTurno
        '
        Me.btnCorteTurno.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnCorteTurno.BackColor = System.Drawing.Color.White
        Me.btnCorteTurno.BackgroundImage = CType(resources.GetObject("btnCorteTurno.BackgroundImage"), System.Drawing.Image)
        Me.btnCorteTurno.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCorteTurno.Checkable = False
        Me.btnCorteTurno.Checked = False
        Me.btnCorteTurno.DisableHighlight = False
        Me.btnCorteTurno.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCorteTurno.GelColor = System.Drawing.Color.White
        Me.btnCorteTurno.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCorteTurno.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCorteTurno.Image = CType(resources.GetObject("btnCorteTurno.Image"), System.Drawing.Image)
        Me.btnCorteTurno.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnCorteTurno.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCorteTurno.Location = New System.Drawing.Point(806, 3)
        Me.btnCorteTurno.Name = "btnCorteTurno"
        Me.btnCorteTurno.Size = New System.Drawing.Size(104, 80)
        Me.btnCorteTurno.TabIndex = 2
        Me.btnCorteTurno.Text = "CORTE DE TURNO"
        Me.btnCorteTurno.UseVisualStyleBackColor = False
        '
        'btnFajillas
        '
        Me.btnFajillas.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnFajillas.BackColor = System.Drawing.Color.White
        Me.btnFajillas.BackgroundImage = CType(resources.GetObject("btnFajillas.BackgroundImage"), System.Drawing.Image)
        Me.btnFajillas.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnFajillas.Checkable = False
        Me.btnFajillas.Checked = False
        Me.btnFajillas.DisableHighlight = False
        Me.btnFajillas.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnFajillas.GelColor = System.Drawing.Color.White
        Me.btnFajillas.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnFajillas.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnFajillas.Image = CType(resources.GetObject("btnFajillas.Image"), System.Drawing.Image)
        Me.btnFajillas.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnFajillas.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnFajillas.Location = New System.Drawing.Point(696, 3)
        Me.btnFajillas.Name = "btnFajillas"
        Me.btnFajillas.Size = New System.Drawing.Size(104, 80)
        Me.btnFajillas.TabIndex = 3
        Me.btnFajillas.Text = "FAJILLAS"
        Me.btnFajillas.UseVisualStyleBackColor = False
        '
        'btnGastos
        '
        Me.btnGastos.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnGastos.BackColor = System.Drawing.Color.White
        Me.btnGastos.BackgroundImage = CType(resources.GetObject("btnGastos.BackgroundImage"), System.Drawing.Image)
        Me.btnGastos.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnGastos.Checkable = False
        Me.btnGastos.Checked = False
        Me.btnGastos.DisableHighlight = False
        Me.btnGastos.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnGastos.GelColor = System.Drawing.Color.White
        Me.btnGastos.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnGastos.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnGastos.Image = CType(resources.GetObject("btnGastos.Image"), System.Drawing.Image)
        Me.btnGastos.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnGastos.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnGastos.Location = New System.Drawing.Point(586, 3)
        Me.btnGastos.Name = "btnGastos"
        Me.btnGastos.Size = New System.Drawing.Size(104, 80)
        Me.btnGastos.TabIndex = 4
        Me.btnGastos.Text = "GASTOS"
        Me.btnGastos.UseVisualStyleBackColor = False
        '
        'btnReportarMatricula
        '
        Me.btnReportarMatricula.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnReportarMatricula.BackColor = System.Drawing.Color.White
        Me.btnReportarMatricula.BackgroundImage = CType(resources.GetObject("btnReportarMatricula.BackgroundImage"), System.Drawing.Image)
        Me.btnReportarMatricula.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnReportarMatricula.Checkable = False
        Me.btnReportarMatricula.Checked = False
        Me.btnReportarMatricula.DisableHighlight = False
        Me.btnReportarMatricula.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnReportarMatricula.GelColor = System.Drawing.Color.White
        Me.btnReportarMatricula.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnReportarMatricula.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnReportarMatricula.Image = CType(resources.GetObject("btnReportarMatricula.Image"), System.Drawing.Image)
        Me.btnReportarMatricula.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnReportarMatricula.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnReportarMatricula.Location = New System.Drawing.Point(476, 3)
        Me.btnReportarMatricula.Name = "btnReportarMatricula"
        Me.btnReportarMatricula.Size = New System.Drawing.Size(104, 80)
        Me.btnReportarMatricula.TabIndex = 5
        Me.btnReportarMatricula.Text = "REPORTAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MATRÍCULA"
        Me.btnReportarMatricula.UseVisualStyleBackColor = False
        '
        'btnHabilitarValets
        '
        Me.btnHabilitarValets.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnHabilitarValets.BackColor = System.Drawing.Color.White
        Me.btnHabilitarValets.BackgroundImage = CType(resources.GetObject("btnHabilitarValets.BackgroundImage"), System.Drawing.Image)
        Me.btnHabilitarValets.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnHabilitarValets.Checkable = False
        Me.btnHabilitarValets.Checked = False
        Me.btnHabilitarValets.DisableHighlight = False
        Me.btnHabilitarValets.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnHabilitarValets.GelColor = System.Drawing.Color.White
        Me.btnHabilitarValets.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnHabilitarValets.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnHabilitarValets.Image = CType(resources.GetObject("btnHabilitarValets.Image"), System.Drawing.Image)
        Me.btnHabilitarValets.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnHabilitarValets.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnHabilitarValets.Location = New System.Drawing.Point(366, 3)
        Me.btnHabilitarValets.Name = "btnHabilitarValets"
        Me.btnHabilitarValets.Size = New System.Drawing.Size(104, 80)
        Me.btnHabilitarValets.TabIndex = 6
        Me.btnHabilitarValets.Text = "HABILITAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "VALETS"
        Me.btnHabilitarValets.UseVisualStyleBackColor = False
        '
        'btnHabilitarMeseros
        '
        Me.btnHabilitarMeseros.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnHabilitarMeseros.BackColor = System.Drawing.Color.White
        Me.btnHabilitarMeseros.BackgroundImage = CType(resources.GetObject("btnHabilitarMeseros.BackgroundImage"), System.Drawing.Image)
        Me.btnHabilitarMeseros.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnHabilitarMeseros.Checkable = False
        Me.btnHabilitarMeseros.Checked = False
        Me.btnHabilitarMeseros.DisableHighlight = False
        Me.btnHabilitarMeseros.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnHabilitarMeseros.GelColor = System.Drawing.Color.White
        Me.btnHabilitarMeseros.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnHabilitarMeseros.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnHabilitarMeseros.Image = CType(resources.GetObject("btnHabilitarMeseros.Image"), System.Drawing.Image)
        Me.btnHabilitarMeseros.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnHabilitarMeseros.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnHabilitarMeseros.Location = New System.Drawing.Point(256, 3)
        Me.btnHabilitarMeseros.Name = "btnHabilitarMeseros"
        Me.btnHabilitarMeseros.Size = New System.Drawing.Size(104, 80)
        Me.btnHabilitarMeseros.TabIndex = 7
        Me.btnHabilitarMeseros.Text = "HABILITAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MESEROS"
        Me.btnHabilitarMeseros.UseVisualStyleBackColor = False
        '
        'btnHabilitarRecamareras
        '
        Me.btnHabilitarRecamareras.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnHabilitarRecamareras.BackColor = System.Drawing.Color.White
        Me.btnHabilitarRecamareras.BackgroundImage = CType(resources.GetObject("btnHabilitarRecamareras.BackgroundImage"), System.Drawing.Image)
        Me.btnHabilitarRecamareras.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnHabilitarRecamareras.Checkable = False
        Me.btnHabilitarRecamareras.Checked = False
        Me.btnHabilitarRecamareras.DisableHighlight = False
        Me.btnHabilitarRecamareras.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnHabilitarRecamareras.GelColor = System.Drawing.Color.White
        Me.btnHabilitarRecamareras.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnHabilitarRecamareras.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnHabilitarRecamareras.Image = CType(resources.GetObject("btnHabilitarRecamareras.Image"), System.Drawing.Image)
        Me.btnHabilitarRecamareras.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnHabilitarRecamareras.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnHabilitarRecamareras.Location = New System.Drawing.Point(146, 3)
        Me.btnHabilitarRecamareras.Name = "btnHabilitarRecamareras"
        Me.btnHabilitarRecamareras.Size = New System.Drawing.Size(104, 80)
        Me.btnHabilitarRecamareras.TabIndex = 8
        Me.btnHabilitarRecamareras.Text = "HABILITAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "RECAMARERAS"
        Me.btnHabilitarRecamareras.UseVisualStyleBackColor = False
        '
        'btnFacturar
        '
        Me.btnFacturar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnFacturar.BackColor = System.Drawing.Color.White
        Me.btnFacturar.BackgroundImage = CType(resources.GetObject("btnFacturar.BackgroundImage"), System.Drawing.Image)
        Me.btnFacturar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnFacturar.Checkable = False
        Me.btnFacturar.Checked = False
        Me.btnFacturar.DisableHighlight = False
        Me.btnFacturar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnFacturar.GelColor = System.Drawing.Color.White
        Me.btnFacturar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnFacturar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnFacturar.Image = CType(resources.GetObject("btnFacturar.Image"), System.Drawing.Image)
        Me.btnFacturar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnFacturar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnFacturar.Location = New System.Drawing.Point(36, 3)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(104, 80)
        Me.btnFacturar.TabIndex = 9
        Me.btnFacturar.Text = "FACTURAR"
        Me.btnFacturar.UseVisualStyleBackColor = False
        '
        'MenuPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.ClientSize = New System.Drawing.Size(983, 173)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "MenuPrincipal"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Menú"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.contenedorBotones.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents contenedorBotones As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnMenu As GelButton
    Friend WithEvents btnCorteTurno As GelButton
    Friend WithEvents btnFajillas As GelButton
    Friend WithEvents btnGastos As GelButton
    Friend WithEvents btnReportarMatricula As GelButton
    Friend WithEvents btnHabilitarValets As GelButton
    Friend WithEvents btnHabilitarMeseros As GelButton
    Friend WithEvents btnHabilitarRecamareras As GelButton
    Friend WithEvents btnFacturar As GelButton
End Class
