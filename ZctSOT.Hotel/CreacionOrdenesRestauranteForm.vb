﻿Imports ZctSOT.Hotel.CreacionComandasForm
Imports System.Linq
Imports ExtraComponents

Public Class CreacionOrdenesRestauranteForm
    Private BotonesArticulosDiccionario As Dictionary(Of GelButton, Modelo.Entidades.Articulo)

    Private _procesoExitoso As Boolean = False

    Public Property ProcesoExitoso As Boolean
        Get
            Return _procesoExitoso
        End Get
        Private Set(value As Boolean)
            _procesoExitoso = value
        End Set
    End Property

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        BotonesArticulosDiccionario = New Dictionary(Of GelButton, Modelo.Entidades.Articulo)
        ResumenArticuloBindingSource.DataSource = New List(Of ResumenArticulo)
    End Sub

    Private Sub CreacionOrdenesRestauranteForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarArticulos()
    End Sub

    Private Sub CargarArticulos(Optional filtro As String = Nothing)
        tcProductos.TabPages.Clear()

        Dim controlador As New Controladores.ControladorArticulos

        Dim tipos = controlador.ObtenerTiposConArticulos(filtro)

        For Each tipo As Modelo.Entidades.TipoArticulo In tipos
            AgregarPagina(tipo)
        Next

    End Sub

    Private Sub AgregarAOrdenRestaurante(sender As Object, e As EventArgs)
        Dim botonActual As GelButton = CType(sender, GelButton)

        Dim articulo As Modelo.Entidades.Articulo = BotonesArticulosDiccionario(botonActual)

        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Then
            origenes = New List(Of ResumenArticulo)
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))
        End If

        If origenes.Any(Function(m)
                            Return m.IdArticulo = articulo.Id
                        End Function) Then
            Dim filaActual As ResumenArticulo = origenes.First(Function(m)
                                                                   Return m.IdArticulo = articulo.Id
                                                               End Function)
            filaActual.Cantidad += 1
        Else
            Dim nuevoArticulo As New ResumenArticulo
            nuevoArticulo.IdArticulo = articulo.Id
            nuevoArticulo.Nombre = articulo.Nombre
            nuevoArticulo.Tipo = articulo.Tipo
            nuevoArticulo.Cantidad = 1
            nuevoArticulo.Precio = articulo.Precio

            origenes.Add(nuevoArticulo)
        End If

        ResumenArticuloBindingSource.DataSource = Nothing
        ResumenArticuloBindingSource.DataSource = origenes

        'ResumenArticuloBindingSource.RaiseListChangedEvents = True
        ResumenArticuloBindingSource.ResetBindings(False)
        'dgvProductos.Refresh()
    End Sub

    Private Sub ResumenArticuloBindingSource_DataSourceChanged(sender As Object, e As EventArgs) Handles ResumenArticuloBindingSource.DataSourceChanged, ResumenArticuloBindingSource.ListChanged
        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Or Not (TypeOf ResumenArticuloBindingSource.DataSource Is List(Of ResumenArticulo)) Then
            lblCortesia.Text = "$" + (0).ToString("#,##0.00")
            lblDescuento.Text = "$" + (0).ToString("#,##0.00")
            lblSubtotal.Text = "$" + (0).ToString("#,##0.00")
            lblTotal.Text = "$" + (0).ToString("#,##0.00")
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))

            Dim subtotal As Decimal
            Dim cortesia As Decimal = 0
            Dim descuento As Decimal = 0
            If (origenes.Count > 0) Then
                subtotal = origenes.Sum(Function(m)
                                            Return m.Total
                                        End Function)

            Else
                subtotal = 0
            End If

            lblSubtotal.Text = "$" + subtotal.ToString("#,##0.00")
            lblCortesia.Text = "$" + cortesia.ToString("#,##0.00")
            lblDescuento.Text = "$" + descuento.ToString("#,##0.00")
            lblTotal.Text = "$" + (subtotal - cortesia - descuento).ToString("#,##0.00")

        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Then
            origenes = New List(Of ResumenArticulo)
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))
        End If

        Dim subtotal As Decimal
        Dim cortesia As Decimal = 0
        Dim descuento As Decimal = 0
        If (origenes.Count > 0) Then
            subtotal = origenes.Sum(Function(m)
                                        Return m.Total
                                    End Function)

        Else
            subtotal = 0
        End If

        Dim nuevaOrdenRestaurante As New Modelo.Entidades.OrdenRestaurante
        nuevaOrdenRestaurante.Activo = True
        nuevaOrdenRestaurante.Valor = subtotal - descuento

        'Dim selectorFormaPago As New SelectorFormaPagoForm(nuevaOrdenRestaurante.Valor)

        'selectorFormaPago.ShowDialog(Me)

        'If Not selectorFormaPago.ProcesoExitoso Then
        '    Exit Sub
        'End If

        'For Each tipoPago In selectorFormaPago.FormasPagoSeleccionadas
        '    Dim nuevoPago As New Modelo.Entidades.PagoOrdenRestaurante

        '    nuevoPago.Activo = True
        '    nuevoPago.Referencia = tipoPago.Referencia
        '    nuevoPago.TipoPago = tipoPago.TipoPago
        '    nuevoPago.Valor = tipoPago.Valor

        '    nuevaOrdenRestaurante.PagosOrdenRestaurante.Add(nuevoPago)
        'Next

        For Each item As ResumenArticulo In origenes
            Dim nuevoArticuloOrdenRestaurante As New Modelo.Entidades.ArticuloOrdenRestaurante
            nuevoArticuloOrdenRestaurante.Activo = True
            nuevoArticuloOrdenRestaurante.IdArticulo = item.IdArticulo
            nuevoArticuloOrdenRestaurante.Cantidad = item.Cantidad
            nuevoArticuloOrdenRestaurante.PrecioUnidad = item.Precio

            nuevaOrdenRestaurante.ArticulosOrdenRestaurante.Add(nuevoArticuloOrdenRestaurante)
        Next

        Dim controlador As New Controladores.ControladorRestaurantes

        controlador.CrearOrden(nuevaOrdenRestaurante)

        ProcesoExitoso = True

        Close()

    End Sub

    Private Sub AgregarPagina(tipo As Modelo.Entidades.TipoArticulo)
        Dim pagina As New TabPage
        pagina.Text = tipo.Nombre

        Dim flContenedor As New FlowLayoutPanel
        flContenedor.FlowDirection = FlowDirection.TopDown
        flContenedor.AutoScroll = True

        For Each articuloActual In tipo.Articulos

            If Not articuloActual.Activo Then
                Continue For
            End If

            Dim gelBtn As New GelButton()
            gelBtn.Width = 150
            gelBtn.Height = 30
            gelBtn.Text = articuloActual.Nombre
            gelBtn.TextAlign = ContentAlignment.MiddleRight
            gelBtn.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
            gelBtn.Margin = New Padding(0)
            gelBtn.Padding = gelBtn.Margin
            gelBtn.GradientBottom = Color.White
            gelBtn.GradientTop = Color.FromArgb(252, 217, 252)
            AddHandler gelBtn.Click, AddressOf AgregarAOrdenRestaurante

            flContenedor.Controls.Add(gelBtn)
            BotonesArticulosDiccionario.Add(gelBtn, articuloActual)
        Next

        pagina.Controls.Add(flContenedor)
        flContenedor.Dock = DockStyle.Fill

        tcProductos.TabPages.Add(pagina)
    End Sub

End Class