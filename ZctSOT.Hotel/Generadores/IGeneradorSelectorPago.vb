﻿Imports ZctSOT.Hotel.Interfaces

Namespace Generadores
    Public Interface IGeneradorSelectorPago
        Function ObtenerInstancia(valorPago As Decimal, Optional incluirReservacion As Boolean = False, Optional ocultarPropina As Boolean = False) As ISelectorPago
    End Interface
End Namespace