﻿Imports ZctSOT.Hotel.Interfaces

Namespace Generadores
    Public Class GeneradorSelectorPagoZctSOT
        Implements IGeneradorSelectorPago

        Friend Function ObtenerInstancia(valorPago As Decimal, Optional incluirReservacion As Boolean = False, Optional ocultarPropina As Boolean = False) As ISelectorPago Implements IGeneradorSelectorPago.ObtenerInstancia
            Return New SelectorFormaDePagoForm(valorPago, incluirReservacion, ocultarPropina)
        End Function
    End Class
End Namespace