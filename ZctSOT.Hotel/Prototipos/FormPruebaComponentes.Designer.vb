﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormPruebaComponentes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GradientPanel1 = New ExtraComponents.GradientPanel()
        Me.ZctHabitacion1 = New ZctSOT.Hotel.ZctHabitacion(Me.components)
        Me.SuspendLayout()
        '
        'GradientPanel1
        '
        Me.GradientPanel1.Alpha = 0
        Me.GradientPanel1.BackColor = System.Drawing.Color.White
        Me.GradientPanel1.BackgroundImage = Global.ZctSOT.Hotel.My.Resources.Resources.icono_reservacion_cancelar
        Me.GradientPanel1.CustomColor = System.Drawing.Color.LightGreen
        Me.GradientPanel1.CustomGradient = True
        Me.GradientPanel1.GradientTop = 10
        Me.GradientPanel1.Location = New System.Drawing.Point(50, 161)
        Me.GradientPanel1.Name = "GradientPanel1"
        Me.GradientPanel1.Size = New System.Drawing.Size(627, 247)
        Me.GradientPanel1.TabIndex = 10
        '
        'ZctHabitacion1
        '
        Me.ZctHabitacion1.Alpha = 0
        Me.ZctHabitacion1.BackColor = System.Drawing.Color.Transparent
        Me.ZctHabitacion1.Congelar = False
        Me.ZctHabitacion1.CustomColor = System.Drawing.Color.Teal
        Me.ZctHabitacion1.CustomGradient = True
        Me.ZctHabitacion1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZctHabitacion1.GradientTop = 0
        Me.ZctHabitacion1.IntervaloRecarga = 30000
        Me.ZctHabitacion1.Location = New System.Drawing.Point(120, 29)
        Me.ZctHabitacion1.Margin = New System.Windows.Forms.Padding(0)
        Me.ZctHabitacion1.Name = "ZctHabitacion1"
        Me.ZctHabitacion1.RecargaAutomatica = False
        Me.ZctHabitacion1.Size = New System.Drawing.Size(151, 102)
        Me.ZctHabitacion1.TabIndex = 9
        '
        'FormPruebaComponentes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 488)
        Me.Controls.Add(Me.ZctHabitacion1)
        Me.Controls.Add(Me.GradientPanel1)
        Me.DoubleBuffered = True
        Me.Name = "FormPruebaComponentes"
        Me.Text = "FormPruebaComponentes"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ZctHabitacion1 As ZctSOT.Hotel.ZctHabitacion
    Friend WithEvents GradientPanel1 As ExtraComponents.GradientPanel
End Class
