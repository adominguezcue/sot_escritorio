﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectorModoAplicacionForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnRestaurante = New ExtraComponents.GelButton()
        Me.btnHotel = New ExtraComponents.GelButton()
        Me.btnCocina = New ExtraComponents.GelButton()
        Me.SuspendLayout()
        '
        'btnRestaurante
        '
        Me.btnRestaurante.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRestaurante.BackColor = System.Drawing.Color.White
        Me.btnRestaurante.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnRestaurante.Checkable = False
        Me.btnRestaurante.Checked = False
        Me.btnRestaurante.DisableHighlight = False
        Me.btnRestaurante.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnRestaurante.GelColor = System.Drawing.Color.White
        Me.btnRestaurante.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnRestaurante.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnRestaurante.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnRestaurante.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnRestaurante.Location = New System.Drawing.Point(219, 9)
        Me.btnRestaurante.Margin = New System.Windows.Forms.Padding(0)
        Me.btnRestaurante.Name = "btnRestaurante"
        Me.btnRestaurante.Size = New System.Drawing.Size(104, 80)
        Me.btnRestaurante.TabIndex = 5
        Me.btnRestaurante.Text = "RESTAURANTE"
        Me.btnRestaurante.UseVisualStyleBackColor = False
        '
        'btnHotel
        '
        Me.btnHotel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnHotel.BackColor = System.Drawing.Color.White
        Me.btnHotel.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnHotel.Checkable = False
        Me.btnHotel.Checked = False
        Me.btnHotel.DisableHighlight = False
        Me.btnHotel.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnHotel.GelColor = System.Drawing.Color.White
        Me.btnHotel.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnHotel.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnHotel.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnHotel.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnHotel.Location = New System.Drawing.Point(9, 9)
        Me.btnHotel.Margin = New System.Windows.Forms.Padding(0)
        Me.btnHotel.Name = "btnHotel"
        Me.btnHotel.Size = New System.Drawing.Size(104, 80)
        Me.btnHotel.TabIndex = 6
        Me.btnHotel.Text = "HOTEL"
        Me.btnHotel.UseVisualStyleBackColor = False
        '
        'btnCocina
        '
        Me.btnCocina.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCocina.BackColor = System.Drawing.Color.White
        Me.btnCocina.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCocina.Checkable = False
        Me.btnCocina.Checked = False
        Me.btnCocina.DisableHighlight = False
        Me.btnCocina.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCocina.GelColor = System.Drawing.Color.White
        Me.btnCocina.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCocina.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCocina.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnCocina.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCocina.Location = New System.Drawing.Point(114, 104)
        Me.btnCocina.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCocina.Name = "btnCocina"
        Me.btnCocina.Size = New System.Drawing.Size(104, 80)
        Me.btnCocina.TabIndex = 7
        Me.btnCocina.Text = "COCINA"
        Me.btnCocina.UseVisualStyleBackColor = False
        '
        'SelectorModoAplicacionForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(333, 193)
        Me.Controls.Add(Me.btnCocina)
        Me.Controls.Add(Me.btnHotel)
        Me.Controls.Add(Me.btnRestaurante)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "SelectorModoAplicacionForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione la aplicacion"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRestaurante As GelButton
    Friend WithEvents btnHotel As GelButton
    Friend WithEvents btnCocina As GelButton
End Class
