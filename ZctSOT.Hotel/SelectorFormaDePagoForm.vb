﻿Imports Transversal.Extensiones
Imports Transversal.Excepciones
Imports System.Linq
Imports ZctSOT.Hotel.Interfaces

Public Class SelectorFormaDePagoForm
    Implements ISelectorPago

    Private _procesoExitoso As Boolean = False
    Private _valorPago As Decimal
    Private _incluirReservacion As Boolean
    Private _ocultarPropina As Boolean

    Private _formasPagoSeleccionadas As List(Of Modelo.Dtos.DtoInformacionPago) = New List(Of Modelo.Dtos.DtoInformacionPago)
    Private _propinaGenerada As Modelo.Entidades.Propina


    Public ReadOnly Property ProcesoExitoso As Boolean Implements ISelectorPago.ProcesoExitoso
        Get
            Return _procesoExitoso
        End Get
        'Private Set(value As Boolean)
        '    _procesoExitoso = value
        'End Set
    End Property

    Public ReadOnly Property PropinaGenerada As Modelo.Entidades.Propina Implements ISelectorPago.PropinaGenerada
        Get
            If _ocultarPropina OrElse IsNothing(_propinaGenerada) OrElse _propinaGenerada.Valor <= 0 Then
                Return Nothing
            End If

            Return _propinaGenerada
        End Get
    End Property

    Public ReadOnly Property FormasPagoSeleccionadas As List(Of Modelo.Dtos.DtoInformacionPago) Implements ISelectorPago.FormasPagoSeleccionadas
        Get
            Return _formasPagoSeleccionadas
        End Get
        'Private Set(value As List(Of Modelo.Dtos.DtoInformacionPago))
        '    _formasPagoSeleccionadas = value
        'End Set
    End Property

    Public Sub New(valorPago As Decimal, Optional incluirReservacion As Boolean = False, Optional ocultarPropina As Boolean = False)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        _valorPago = valorPago
        _incluirReservacion = incluirReservacion
        _ocultarPropina = ocultarPropina

        lblTotal.Text = _valorPago.ToString("C")

        nudPagoTarjeta.Maximum = _valorPago
        nudPagoEfectivo.Value = _valorPago
    End Sub

    Public Sub MostrarEnDialogo(owner As System.Windows.Forms.IWin32Window) Implements ISelectorPago.MostrarEnDialogo
        ShowDialog(owner)
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If cbFormasPago.SelectedIndex < 0 Then
            Throw New SOTException(My.Resources.Errores.forma_pago_no_seleccionada_exception)
        End If

        'Dim itemElegido As New Modelo.Dtos.DtoInformacionPago
        'itemElegido.Referencia = txtReferencia.Text
        'itemElegido.TipoPago = CType(cbFormasPago.SelectedValue, Modelo.PagoRenta.TiposPago)
        'itemElegido.Valor = _valorPago

        _formasPagoSeleccionadas = CType(cbFormasPago.SelectedValue, Modelo.Dtos.DtoInformacionPago()).ToList()
        'FormasPagoSeleccionadas.Add(itemElegido)

        If FormasPagoSeleccionadas.Count > 1 Then

            Dim pagoTarjeta = FormasPagoSeleccionadas.FirstOrDefault(Function(m) m.TipoPago = Modelo.Entidades.TiposPago.TarjetaCredito)
            Dim pagoEfectivo = FormasPagoSeleccionadas.FirstOrDefault(Function(m) m.TipoPago = Modelo.Entidades.TiposPago.Efectivo)

            If Not IsNothing(pagoTarjeta) Then

                If _propinaGenerada.IdTipoTarjeta <= 0 Then
                    Throw New SOTException("Seleccione un tipo de tarjeta")
                End If

                pagoTarjeta.Referencia = mtxtNumeroOperacion.Text
                pagoTarjeta.Valor = nudPagoTarjeta.Value
                pagoTarjeta.TipoTarjeta = _propinaGenerada.TipoTarjeta

                _propinaGenerada.Referencia = mtxtNumeroOperacion.Text
            End If

            If Not IsNothing(pagoEfectivo) Then
                pagoEfectivo.Referencia = String.Empty
                pagoEfectivo.Valor = nudPagoEfectivo.Value
            End If

            _propinaGenerada.Valor = nudPropina.Value

        ElseIf FormasPagoSeleccionadas.Count = 1 Then
            Dim primerPago = FormasPagoSeleccionadas(0)

            primerPago.Valor = _valorPago

            Select Case primerPago.TipoPago
                Case Modelo.Entidades.TiposPago.Cortesia
                    primerPago.Referencia = txtCortesia.Text

                    Exit Select
                    'Case Modelo.Entidades.TiposPago.Cupon
                    '    primerPago.Referencia = txtCupon.Text

                    '    Exit Select
                Case Modelo.Entidades.TiposPago.Efectivo
                    primerPago.Referencia = String.Empty

                    Exit Select
                Case Modelo.Entidades.TiposPago.Reservacion
                    primerPago.Referencia = "Supongo que aqui va algo relacionado con la reservación"

                    Exit Select
                Case Modelo.Entidades.TiposPago.TarjetaCredito
                    If _propinaGenerada.IdTipoTarjeta <= 0 Then
                        Throw New SOTException("Seleccione un tipo de tarjeta")
                    End If

                    primerPago.Referencia = mtxtNumeroOperacion.Text
                    primerPago.TipoTarjeta = _propinaGenerada.TipoTarjeta

                    _propinaGenerada.Referencia = mtxtNumeroOperacion.Text




                    _propinaGenerada.Valor = nudPropina.Value

                    Exit Select
                    'Case Modelo.Entidades.TiposPago.VPoints
                    '    primerPago.Referencia = txtVPoint.Text

                    '    Exit Select
            End Select

        End If

        _procesoExitoso = True
        Close()
    End Sub

    Private Sub SelectorFormaPagoForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim controladorPagos As New Controladores.ControladorPagos()

        Helpers.HelperDataSource.AgregarDataSource(cbFormasPago, controladorPagos.ObtenerTiposValidosConMixtos(_incluirReservacion),
                                                   Function(x) x.Nombre, Function(x) x.Pagos)

        'Dim elementos = EnumExtensions.ComoListaDto(Of Modelo.PagoRenta.TiposPago)()

        'cbFormasPago.DataSource = elementos
        'cbFormasPago.DisplayMember = ObjectExtensions.NombrePropiedad(Of Transversal.Dtos.DtoEnum, String)(Function(m) m.Nombre)
        'cbFormasPago.ValueMember = ObjectExtensions.NombrePropiedad(Of Transversal.Dtos.DtoEnum, Object)(Function(m) m.Valor)
        cbFormasPago.SelectedIndex = -1
    End Sub

    Private Sub cbFormasPago_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbFormasPago.SelectedIndexChanged
        Dim elementoSelecionado = TryCast(cbFormasPago.SelectedItem, Modelo.Dtos.DtoGrupoPago)

        If IsNothing(elementoSelecionado) OrElse elementoSelecionado.Pagos.Length = 0 Then
            OcultarTodo()
        ElseIf elementoSelecionado.Pagos.Length > 1 Then
            tpCortesia.Hide()
            'tpCupon.Hide()
            'tpVPoints.Hide()

            tpTarjeta.Show()

            lblPagoTarjeta.Show()
            nudPagoTarjeta.Show()
            lblPagoEfectivo.Show()
            nudPagoEfectivo.Show()

            lblPropinaTarjeta.Visible = Not _ocultarPropina
            nudPropina.Visible = Not _ocultarPropina

            _propinaGenerada = New Modelo.Entidades.Propina()
        Else
            Dim primerPago = elementoSelecionado.Pagos(0)

            Select Case primerPago.TipoPago
                Case Modelo.Entidades.TiposPago.Cortesia
                    tpCortesia.Show()
                    'tpCupon.Hide()
                    tpTarjeta.Hide()
                    'tpVPoints.Hide()

                    _propinaGenerada = Nothing
                    Exit Select
                Case Modelo.Entidades.TiposPago.Cupon
                    tpCortesia.Hide()
                    'tpCupon.Show()
                    tpTarjeta.Hide()
                    'tpVPoints.Hide()

                    _propinaGenerada = Nothing
                    Exit Select
                Case Modelo.Entidades.TiposPago.Efectivo
                    OcultarTodo()

                    _propinaGenerada = Nothing
                    Exit Select
                Case Modelo.Entidades.TiposPago.Reservacion
                    OcultarTodo()

                    _propinaGenerada = Nothing
                    Exit Select
                Case Modelo.Entidades.TiposPago.TarjetaCredito
                    tpCortesia.Hide()
                    'tpCupon.Hide()
                    'tpVPoints.Hide()

                    tpTarjeta.Show()

                    lblPagoTarjeta.Hide()
                    nudPagoTarjeta.Hide()
                    lblPagoEfectivo.Hide()
                    nudPagoEfectivo.Hide()

                    lblPropinaTarjeta.Visible = Not _ocultarPropina
                    nudPropina.Visible = Not _ocultarPropina

                    _propinaGenerada = New Modelo.Entidades.Propina()

                    Exit Select
                Case Modelo.Entidades.TiposPago.VPoints
                    tpCortesia.Hide()
                    'tpCupon.Hide()
                    tpTarjeta.Hide()
                    'tpVPoints.Show()

                    _propinaGenerada = Nothing
                    Exit Select
            End Select

        End If
    End Sub

    Private Sub OcultarTodo()
        tpCortesia.Hide()
        'tpCupon.Hide()
        tpTarjeta.Hide()
        'tpVPoints.Hide()
    End Sub

    Private Sub SelectorFormaPagoForm_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        OcultarTodo()
    End Sub

    Private Sub btnVisa_Click(sender As Object, e As EventArgs) Handles btnVisa.Click
        btnVisa.GradientBottom = Color.Gray
        btnAmericanExpress.GradientBottom = Color.White
        btnMasterCard.GradientBottom = Color.White

        _propinaGenerada.TipoTarjeta = Modelo.Entidades.TiposTarjeta.Visa
    End Sub

    Private Sub btnMasterCard_Click(sender As Object, e As EventArgs) Handles btnMasterCard.Click
        btnVisa.GradientBottom = Color.White
        btnAmericanExpress.GradientBottom = Color.White
        btnMasterCard.GradientBottom = Color.Gray

        _propinaGenerada.TipoTarjeta = Modelo.Entidades.TiposTarjeta.MasterCard
    End Sub

    Private Sub btnAmericanExpress_Click(sender As Object, e As EventArgs) Handles btnAmericanExpress.Click
        btnVisa.GradientBottom = Color.White
        btnAmericanExpress.GradientBottom = Color.Gray
        btnMasterCard.GradientBottom = Color.White

        _propinaGenerada.TipoTarjeta = Modelo.Entidades.TiposTarjeta.AmericanExpress
    End Sub

    Private Sub nudPagoTarjeta_ValueChanged(sender As Object, e As EventArgs) Handles nudPagoTarjeta.ValueChanged
        nudPagoEfectivo.Value = _valorPago - nudPagoTarjeta.Value
    End Sub
End Class