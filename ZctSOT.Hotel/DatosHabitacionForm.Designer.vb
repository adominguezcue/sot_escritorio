﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DatosHabitacionForm
    Inherits System.Windows.Forms.UserControl

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pbEstado = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblMatricula = New System.Windows.Forms.Label()
        Me.lblSalida = New System.Windows.Forms.Label()
        Me.lblEntrada = New System.Windows.Forms.Label()
        Me.lblTarjetaV = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblHabitacion = New System.Windows.Forms.Label()
        Me.lblServicioRestaurante = New System.Windows.Forms.Label()
        Me.lblPaquetes = New System.Windows.Forms.Label()
        Me.lblCortesia = New System.Windows.Forms.Label()
        Me.lblHospedajeExtra = New System.Windows.Forms.Label()
        Me.lblPersonasExtra = New System.Windows.Forms.Label()
        Me.lblServicio = New System.Windows.Forms.Label()
        Me.imgEstadoLimpieza = New System.Windows.Forms.PictureBox()
        Me.DtoDatosHabitacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.pbEstado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgEstadoLimpieza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtoDatosHabitacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label1.Font = New System.Drawing.Font("Lato", 22.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(29, 9)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(221, 26)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Datos de la habitación"
        Me.Label1.UseMnemonic = False
        '
        'Button1
        '
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Image = Global.ZctSOT.Hotel.My.Resources.Resources.close
        Me.Button1.Location = New System.Drawing.Point(412, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(13, 13)
        Me.Button1.TabIndex = 4
        Me.Button1.UseVisualStyleBackColor = False
        '
        'pbEstado
        '
        Me.pbEstado.Location = New System.Drawing.Point(26, 46)
        Me.pbEstado.Name = "pbEstado"
        Me.pbEstado.Size = New System.Drawing.Size(48, 48)
        Me.pbEstado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbEstado.TabIndex = 5
        Me.pbEstado.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(87, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Habitación:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(87, 71)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 15)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Tipo:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(87, 92)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 15)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Estado:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(87, 113)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 15)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Tarjeta v:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(87, 134)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 15)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Hora de entrada:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(87, 155)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 15)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Hora de salida:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(87, 176)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 15)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Matricula:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(264, 155)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 15)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Ser. Restaurante:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(302, 134)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 15)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Paquetes:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(274, 113)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(86, 15)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Cortesias Hab.:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(274, 92)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(86, 15)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Hospedaje Ext:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(283, 71)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(77, 15)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Personas Ext:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(289, 50)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(71, 15)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Servicio No:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblMatricula
        '
        Me.lblMatricula.AutoSize = True
        Me.lblMatricula.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblMatricula.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblMatricula.Location = New System.Drawing.Point(154, 176)
        Me.lblMatricula.Name = "lblMatricula"
        Me.lblMatricula.Size = New System.Drawing.Size(14, 15)
        Me.lblMatricula.TabIndex = 26
        Me.lblMatricula.Text = "0"
        '
        'lblSalida
        '
        Me.lblSalida.AutoSize = True
        Me.lblSalida.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblSalida.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblSalida.Location = New System.Drawing.Point(177, 155)
        Me.lblSalida.Name = "lblSalida"
        Me.lblSalida.Size = New System.Drawing.Size(14, 15)
        Me.lblSalida.TabIndex = 25
        Me.lblSalida.Text = "0"
        '
        'lblEntrada
        '
        Me.lblEntrada.AutoSize = True
        Me.lblEntrada.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblEntrada.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblEntrada.Location = New System.Drawing.Point(188, 134)
        Me.lblEntrada.Name = "lblEntrada"
        Me.lblEntrada.Size = New System.Drawing.Size(14, 15)
        Me.lblEntrada.TabIndex = 24
        Me.lblEntrada.Text = "0"
        '
        'lblTarjetaV
        '
        Me.lblTarjetaV.AutoSize = True
        Me.lblTarjetaV.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblTarjetaV.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblTarjetaV.Location = New System.Drawing.Point(148, 113)
        Me.lblTarjetaV.Name = "lblTarjetaV"
        Me.lblTarjetaV.Size = New System.Drawing.Size(14, 15)
        Me.lblTarjetaV.TabIndex = 23
        Me.lblTarjetaV.Text = "0"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblEstado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblEstado.Location = New System.Drawing.Point(136, 92)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(14, 15)
        Me.lblEstado.TabIndex = 22
        Me.lblEstado.Text = "0"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblTipo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblTipo.Location = New System.Drawing.Point(123, 71)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(14, 15)
        Me.lblTipo.TabIndex = 21
        Me.lblTipo.Text = "0"
        '
        'lblHabitacion
        '
        Me.lblHabitacion.AutoSize = True
        Me.lblHabitacion.Font = New System.Drawing.Font("Lato Black", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHabitacion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblHabitacion.Location = New System.Drawing.Point(161, 47)
        Me.lblHabitacion.Name = "lblHabitacion"
        Me.lblHabitacion.Size = New System.Drawing.Size(17, 18)
        Me.lblHabitacion.TabIndex = 20
        Me.lblHabitacion.Text = "0"
        '
        'lblServicioRestaurante
        '
        Me.lblServicioRestaurante.AutoSize = True
        Me.lblServicioRestaurante.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblServicioRestaurante.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblServicioRestaurante.Location = New System.Drawing.Point(364, 155)
        Me.lblServicioRestaurante.Name = "lblServicioRestaurante"
        Me.lblServicioRestaurante.Size = New System.Drawing.Size(14, 15)
        Me.lblServicioRestaurante.TabIndex = 32
        Me.lblServicioRestaurante.Text = "0"
        '
        'lblPaquetes
        '
        Me.lblPaquetes.AutoSize = True
        Me.lblPaquetes.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPaquetes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblPaquetes.Location = New System.Drawing.Point(364, 134)
        Me.lblPaquetes.Name = "lblPaquetes"
        Me.lblPaquetes.Size = New System.Drawing.Size(14, 15)
        Me.lblPaquetes.TabIndex = 31
        Me.lblPaquetes.Text = "0"
        '
        'lblCortesia
        '
        Me.lblCortesia.AutoSize = True
        Me.lblCortesia.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblCortesia.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblCortesia.Location = New System.Drawing.Point(364, 113)
        Me.lblCortesia.Name = "lblCortesia"
        Me.lblCortesia.Size = New System.Drawing.Size(14, 15)
        Me.lblCortesia.TabIndex = 30
        Me.lblCortesia.Text = "0"
        '
        'lblHospedajeExtra
        '
        Me.lblHospedajeExtra.AutoSize = True
        Me.lblHospedajeExtra.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHospedajeExtra.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblHospedajeExtra.Location = New System.Drawing.Point(364, 92)
        Me.lblHospedajeExtra.Name = "lblHospedajeExtra"
        Me.lblHospedajeExtra.Size = New System.Drawing.Size(14, 15)
        Me.lblHospedajeExtra.TabIndex = 29
        Me.lblHospedajeExtra.Text = "0"
        '
        'lblPersonasExtra
        '
        Me.lblPersonasExtra.AutoSize = True
        Me.lblPersonasExtra.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPersonasExtra.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblPersonasExtra.Location = New System.Drawing.Point(364, 71)
        Me.lblPersonasExtra.Name = "lblPersonasExtra"
        Me.lblPersonasExtra.Size = New System.Drawing.Size(14, 15)
        Me.lblPersonasExtra.TabIndex = 28
        Me.lblPersonasExtra.Text = "0"
        '
        'lblServicio
        '
        Me.lblServicio.AutoSize = True
        Me.lblServicio.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lblServicio.ForeColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.lblServicio.Location = New System.Drawing.Point(364, 50)
        Me.lblServicio.Name = "lblServicio"
        Me.lblServicio.Size = New System.Drawing.Size(14, 15)
        Me.lblServicio.TabIndex = 27
        Me.lblServicio.Text = "0"
        '
        'imgEstadoLimpieza
        '
        Me.imgEstadoLimpieza.Location = New System.Drawing.Point(3, 143)
        Me.imgEstadoLimpieza.Name = "imgEstadoLimpieza"
        Me.imgEstadoLimpieza.Size = New System.Drawing.Size(48, 48)
        Me.imgEstadoLimpieza.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.imgEstadoLimpieza.TabIndex = 33
        Me.imgEstadoLimpieza.TabStop = False
        '
        'DtoDatosHabitacionBindingSource
        '
        Me.DtoDatosHabitacionBindingSource.DataSource = GetType(Modelo.Dtos.DtoDatosHabitacion)
        '
        'DatosHabitacionForm
        '
        Me.AllowDrop = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Controls.Add(Me.imgEstadoLimpieza)
        Me.Controls.Add(Me.lblServicioRestaurante)
        Me.Controls.Add(Me.lblPaquetes)
        Me.Controls.Add(Me.lblCortesia)
        Me.Controls.Add(Me.lblHospedajeExtra)
        Me.Controls.Add(Me.lblPersonasExtra)
        Me.Controls.Add(Me.lblServicio)
        Me.Controls.Add(Me.lblMatricula)
        Me.Controls.Add(Me.lblSalida)
        Me.Controls.Add(Me.lblEntrada)
        Me.Controls.Add(Me.lblTarjetaV)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.lblHabitacion)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.pbEstado)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.MaximumSize = New System.Drawing.Size(440, 194)
        Me.MinimumSize = New System.Drawing.Size(440, 194)
        Me.Name = "DatosHabitacionForm"
        Me.Size = New System.Drawing.Size(440, 194)
        CType(Me.pbEstado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgEstadoLimpieza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtoDatosHabitacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DtoDatosHabitacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents pbEstado As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblMatricula As System.Windows.Forms.Label
    Friend WithEvents lblSalida As System.Windows.Forms.Label
    Friend WithEvents lblEntrada As System.Windows.Forms.Label
    Friend WithEvents lblTarjetaV As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents lblHabitacion As System.Windows.Forms.Label
    Friend WithEvents lblServicioRestaurante As System.Windows.Forms.Label
    Friend WithEvents lblPaquetes As System.Windows.Forms.Label
    Friend WithEvents lblCortesia As System.Windows.Forms.Label
    Friend WithEvents lblHospedajeExtra As System.Windows.Forms.Label
    Friend WithEvents lblPersonasExtra As System.Windows.Forms.Label
    Friend WithEvents lblServicio As System.Windows.Forms.Label
    Friend WithEvents imgEstadoLimpieza As System.Windows.Forms.PictureBox
End Class
