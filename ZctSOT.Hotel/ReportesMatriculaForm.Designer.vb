﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportesMatriculaForm
    Inherits FormBase

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FixedHeaderPanel2 = New ExtraComponents.FixedHeaderPanel()
        Me.btnReportar = New System.Windows.Forms.Button()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.FixedHeaderPanel1 = New ExtraComponents.FixedHeaderPanel()
        Me.txtDescripcionLectura = New System.Windows.Forms.TextBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtFecha = New System.Windows.Forms.MaskedTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.panelReportes = New ExtraComponents.FixedHeaderPanel()
        Me.listaReportes = New System.Windows.Forms.ListBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.txtMatricula = New System.Windows.Forms.TextBox()
        Me.btnInhabilitar = New ExtraComponents.GelButton()
        Me.panelBotonera.SuspendLayout()
        Me.FixedHeaderPanel2.SuspendLayout()
        Me.FixedHeaderPanel1.SuspendLayout()
        Me.panelReportes.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Controls.Add(Me.btnInhabilitar)
        Me.panelBotonera.Location = New System.Drawing.Point(0, 552)
        Me.panelBotonera.Size = New System.Drawing.Size(915, 84)
        Me.panelBotonera.Controls.SetChildIndex(Me.btnAceptar, 0)
        Me.panelBotonera.Controls.SetChildIndex(Me.btnInhabilitar, 0)
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_icono_habilitada
        Me.btnAceptar.Location = New System.Drawing.Point(809, 2)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(0)
        '
        'FixedHeaderPanel2
        '
        Me.FixedHeaderPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FixedHeaderPanel2.Controls.Add(Me.btnReportar)
        Me.FixedHeaderPanel2.Controls.Add(Me.txtDescripcion)
        Me.FixedHeaderPanel2.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.FixedHeaderPanel2.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.FixedHeaderPanel2.Location = New System.Drawing.Point(314, 284)
        Me.FixedHeaderPanel2.Name = "FixedHeaderPanel2"
        Me.FixedHeaderPanel2.Padding = New System.Windows.Forms.Padding(10)
        Me.FixedHeaderPanel2.Size = New System.Drawing.Size(586, 256)
        Me.FixedHeaderPanel2.TabIndex = 2
        Me.FixedHeaderPanel2.Text = "Generar nuevo reporte"
        '
        'btnReportar
        '
        Me.btnReportar.Location = New System.Drawing.Point(444, 209)
        Me.btnReportar.Name = "btnReportar"
        Me.btnReportar.Size = New System.Drawing.Size(127, 32)
        Me.btnReportar.TabIndex = 5
        Me.btnReportar.Text = "Reportar matrícula"
        Me.btnReportar.UseVisualStyleBackColor = True
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescripcion.Location = New System.Drawing.Point(13, 40)
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(558, 153)
        Me.txtDescripcion.TabIndex = 4
        '
        'FixedHeaderPanel1
        '
        Me.FixedHeaderPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FixedHeaderPanel1.Controls.Add(Me.txtDescripcionLectura)
        Me.FixedHeaderPanel1.Controls.Add(Me.txtUsuario)
        Me.FixedHeaderPanel1.Controls.Add(Me.Label2)
        Me.FixedHeaderPanel1.Controls.Add(Me.txtFecha)
        Me.FixedHeaderPanel1.Controls.Add(Me.Label1)
        Me.FixedHeaderPanel1.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.FixedHeaderPanel1.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.FixedHeaderPanel1.Location = New System.Drawing.Point(314, 21)
        Me.FixedHeaderPanel1.Name = "FixedHeaderPanel1"
        Me.FixedHeaderPanel1.Padding = New System.Windows.Forms.Padding(10)
        Me.FixedHeaderPanel1.Size = New System.Drawing.Size(586, 256)
        Me.FixedHeaderPanel1.TabIndex = 1
        Me.FixedHeaderPanel1.Text = "Reporte anterior"
        '
        'txtDescripcionLectura
        '
        Me.txtDescripcionLectura.Location = New System.Drawing.Point(13, 88)
        Me.txtDescripcionLectura.Multiline = True
        Me.txtDescripcionLectura.Name = "txtDescripcionLectura"
        Me.txtDescripcionLectura.ReadOnly = True
        Me.txtDescripcionLectura.Size = New System.Drawing.Size(558, 153)
        Me.txtDescripcionLectura.TabIndex = 2
        '
        'txtUsuario
        '
        Me.txtUsuario.BackColor = System.Drawing.SystemColors.Control
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.Location = New System.Drawing.Point(339, 46)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.ReadOnly = True
        Me.txtUsuario.Size = New System.Drawing.Size(220, 15)
        Me.txtUsuario.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(287, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Usuario:"
        '
        'txtFecha
        '
        Me.txtFecha.BackColor = System.Drawing.SystemColors.Control
        Me.txtFecha.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFecha.Location = New System.Drawing.Point(55, 46)
        Me.txtFecha.Mask = "00/00/0000 00:00"
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.ReadOnly = True
        Me.txtFecha.Size = New System.Drawing.Size(150, 15)
        Me.txtFecha.TabIndex = 2
        Me.txtFecha.ValidatingType = GetType(Date)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 46)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fecha:"
        '
        'panelReportes
        '
        Me.panelReportes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panelReportes.Controls.Add(Me.listaReportes)
        Me.panelReportes.Controls.Add(Me.PictureBox1)
        Me.panelReportes.Controls.Add(Me.txtMatricula)
        Me.panelReportes.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.panelReportes.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.panelReportes.Location = New System.Drawing.Point(14, 21)
        Me.panelReportes.Name = "panelReportes"
        Me.panelReportes.Padding = New System.Windows.Forms.Padding(10)
        Me.panelReportes.Size = New System.Drawing.Size(291, 519)
        Me.panelReportes.TabIndex = 0
        Me.panelReportes.Text = "Reportes de matrícula"
        '
        'listaReportes
        '
        Me.listaReportes.BackColor = System.Drawing.SystemColors.Control
        Me.listaReportes.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.listaReportes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.listaReportes.FormattingEnabled = True
        Me.listaReportes.ItemHeight = 15
        Me.listaReportes.Location = New System.Drawing.Point(14, 131)
        Me.listaReportes.Name = "listaReportes"
        Me.listaReportes.Size = New System.Drawing.Size(262, 360)
        Me.listaReportes.TabIndex = 2
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_reportar_matricula
        Me.PictureBox1.Location = New System.Drawing.Point(13, 39)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(64, 36)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'txtMatricula
        '
        Me.txtMatricula.Location = New System.Drawing.Point(98, 46)
        Me.txtMatricula.Name = "txtMatricula"
        Me.txtMatricula.Size = New System.Drawing.Size(178, 22)
        Me.txtMatricula.TabIndex = 0
        '
        'btnInhabilitar
        '
        Me.btnInhabilitar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInhabilitar.BackColor = System.Drawing.Color.White
        Me.btnInhabilitar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnInhabilitar.Checkable = False
        Me.btnInhabilitar.Checked = False
        Me.btnInhabilitar.DisableHighlight = False
        Me.btnInhabilitar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnInhabilitar.GelColor = System.Drawing.Color.White
        Me.btnInhabilitar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnInhabilitar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnInhabilitar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnInhabilitar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnInhabilitar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnInhabilitar.Location = New System.Drawing.Point(593, 2)
        Me.btnInhabilitar.Margin = New System.Windows.Forms.Padding(0)
        Me.btnInhabilitar.Name = "btnInhabilitar"
        Me.btnInhabilitar.Size = New System.Drawing.Size(104, 80)
        Me.btnInhabilitar.TabIndex = 5
        Me.btnInhabilitar.Text = "INHABILITAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "REPORTE"
        Me.btnInhabilitar.UseVisualStyleBackColor = False
        '
        'ReportesMatriculaForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(915, 636)
        Me.Controls.Add(Me.FixedHeaderPanel2)
        Me.Controls.Add(Me.FixedHeaderPanel1)
        Me.Controls.Add(Me.panelReportes)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "ReportesMatriculaForm"
        Me.Text = "Reportes de matrículas"
        Me.Controls.SetChildIndex(Me.panelReportes, 0)
        Me.Controls.SetChildIndex(Me.FixedHeaderPanel1, 0)
        Me.Controls.SetChildIndex(Me.FixedHeaderPanel2, 0)
        Me.Controls.SetChildIndex(Me.panelBotonera, 0)
        Me.panelBotonera.ResumeLayout(False)
        Me.FixedHeaderPanel2.ResumeLayout(False)
        Me.FixedHeaderPanel2.PerformLayout()
        Me.FixedHeaderPanel1.ResumeLayout(False)
        Me.FixedHeaderPanel1.PerformLayout()
        Me.panelReportes.ResumeLayout(False)
        Me.panelReportes.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelReportes As FixedHeaderPanel
    Friend WithEvents FixedHeaderPanel1 As FixedHeaderPanel
    Friend WithEvents FixedHeaderPanel2 As FixedHeaderPanel
    Friend WithEvents txtMatricula As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtFecha As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnReportar As System.Windows.Forms.Button
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionLectura As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnInhabilitar As GelButton
    Friend WithEvents listaReportes As System.Windows.Forms.ListBox
End Class
