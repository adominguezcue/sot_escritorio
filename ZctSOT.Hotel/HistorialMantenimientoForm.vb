﻿Imports ZctSOT.Controladores

Public Class HistorialMantenimientoForm

    Sub BuscarHistorial(idHabitacion As Integer)
        Dim origen = New Controladores.ControladorGlobal().AplicacionServicioTareasMantenimiento.ObtenerTareasHabitacion(idHabitacion, New Controladores.ControladorGlobal().UsuarioActual)
        dgvMantenimiento.DataSource = origen
    End Sub

End Class