﻿Imports Transversal.Extensiones
Imports Transversal.Excepciones

Public Class ReservacionesForm

    Private Sub btnCrearReservacion_Click(sender As Object, e As EventArgs) Handles btnCrearReservacion.Click
        Dim reservacionF As New ReservacionForm
        reservacionF.ShowDialog(Me)

        If reservacionF.ProcesoExitoso Then
            MessageBox.Show("Reservación agregada con éxito", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
        MyBase.OnClosing(e)
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim reservaciones = New Controladores.ControladorGlobal().AplicacionServicioReservaciones.ObtenerReservacionesFiltradas(dtpInicio.Value.Date, dtpLimite.Value.AddDays(1).Date.AddSeconds(-1), CInt(cbTiposHabitaciones.SelectedValue))

        dgvReservaciones.DataSource = reservaciones
    End Sub

    Private Sub ReservacionesForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbTiposHabitaciones.DataSource = New Controladores.ControladorGlobal().AplicacionServicioTiposHabitaciones.ObtenerTiposActivos()
        cbTiposHabitaciones.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.TipoHabitacion, String)(Function(m) m.Descripcion)
        cbTiposHabitaciones.ValueMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.TipoHabitacion, Integer)(Function(m) m.Id)
    End Sub

    Private Sub btnModificarReservacion_Click(sender As Object, e As EventArgs) Handles btnModificarReservacion.Click

        If dgvReservaciones.SelectedRows.Count <> 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_reservaciones_seleccionadas)
        End If

        Dim reservacionSeleccionada As Modelo.Entidades.Reservacion = CType(dgvReservaciones.SelectedRows(0).DataBoundItem, Modelo.Entidades.Reservacion)

        Dim reservacionF As New ReservacionForm
        reservacionF.CargarReservacion(reservacionSeleccionada.Id)
        reservacionF.ShowDialog(Me)

        If reservacionF.ProcesoExitoso Then
            MessageBox.Show("Reservación modificada con éxito", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class