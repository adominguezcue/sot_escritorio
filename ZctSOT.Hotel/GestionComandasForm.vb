﻿Public Class GestionComandasForm

    Private idRentaActual As Integer = 0

    Public Sub New(idRenta As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        btnAceptar.Visible = False

        idRentaActual = idRenta
    End Sub


    Private Sub CargarComandas()

        For Each c As Control In flpComandas.Controls

            If TypeOf (c) Is Button Then
                RemoveHandler CType(c, Button).Click, AddressOf AgregarComanda
            ElseIf TypeOf (c) Is GestionComanda Then
                RemoveHandler CType(c, GestionComanda).PostCobro, AddressOf CargarComandas
            End If

            c.Dispose()
        Next

        flpComandas.Controls.Clear()




        Dim controlador As New Controladores.ControladorGlobal
        Dim controladorR As New Controladores.ControladorRentas

        Dim comandas As List(Of Modelo.Entidades.Comanda) = controladorR.ObtenerDetallesComandasPendientes(idRentaActual)

        Dim configuracionGlobal As Modelo.Entidades.ConfiguracionGlobal = controlador.AplicacionServicioConfiguracionesGlobales.ObtenerConfiguracionGlobal()

        For i As Integer = 0 To configuracionGlobal.MaximoComandasSimultaneas - 1
            If i < comandas.Count Then

                Dim gComanda As New GestionComanda
                gComanda.CargarDetallesComanda(comandas(i), i + 1)

                flpComandas.Controls.Add(gComanda)
                gComanda.Size = New Size(300, 440)
                gComanda.Anchor = AnchorStyles.Right Or AnchorStyles.Bottom
                gComanda.Margin = New Padding(5, 0, 5, 0)

                AddHandler gComanda.PostCobro, AddressOf CargarComandas
            Else
                Dim botonAgregar As New Button()
                botonAgregar.Text = "Agregar"
                AddHandler botonAgregar.Click, AddressOf AgregarComanda

                flpComandas.Controls.Add(botonAgregar)
                botonAgregar.Size = New Size(300, 440)
                botonAgregar.Anchor = AnchorStyles.Right Or AnchorStyles.Bottom
                botonAgregar.Margin = New Padding(5, 0, 5, 0)

            End If
        Next

    End Sub

    Private Sub AgregarComanda(sender As Object, e As EventArgs)

        timerCargaComandas.Enabled = False
        Try
            Dim comandasF As New CreacionComandasForm(idRentaActual)

            comandasF.ShowDialog(Me)
        Finally
            CargarComandas()
            timerCargaComandas.Enabled = True
        End Try
    End Sub

    Private Sub GestionComandasForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarComandas()
        timerCargaComandas.Enabled = True
    End Sub

    Private Sub timerCargaComandas_Tick(sender As Object, e As EventArgs) Handles timerCargaComandas.Tick
        CargarComandas()
    End Sub
End Class