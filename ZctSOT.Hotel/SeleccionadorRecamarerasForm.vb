﻿Imports Transversal.Excepciones
Imports System.Linq
Imports ExtraComponents

Public Class SeleccionadorRecamarerasForm

    Private Shared ReadOnly COLOR_DISPONIBLE As Color = Color.FromArgb(252, 217, 252)
    Private Shared ReadOnly COLOR_ASIGNADA As Color = Color.FromArgb(220, 252, 239)

    Private _procesoExitoso As Boolean = False
    Private HabitacionActual As Modelo.Entidades.Habitacion

    'Private Property HabitacionActual As Modelo.Entidades.Habitacion
    '    Get
    '        Return _habitacionActual
    '    End Get
    '    Set(value As Modelo.Entidades.Habitacion)
    '        _habitacionActual = value
    '        CargarRecamareras()
    '    End Set
    'End Property

    Public Property ProcesoExitoso As Boolean
        Get
            Return _procesoExitoso
        End Get
        Private Set(value As Boolean)
            _procesoExitoso = value
        End Set
    End Property

    Private BotonesRecamarerasDiccionario As Dictionary(Of GelButton, Modelo.Entidades.Empleado)

    Public Sub New(habitacion As Modelo.Entidades.Habitacion)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        BotonesRecamarerasDiccionario = New Dictionary(Of GelButton, Modelo.Entidades.Empleado)

        HabitacionActual = habitacion
    End Sub

    Private Sub CargarRecamareras()
        For Each gelBtn As GelButton In flpDisponibles.Controls
            RemoveHandler gelBtn.Click, AddressOf CambiarPadre
            gelBtn.Dispose()
        Next

        flpDisponibles.Controls.Clear()

        For Each gelBtn As GelButton In flpAsignadas.Controls
            RemoveHandler gelBtn.Click, AddressOf CambiarPadre
            gelBtn.Dispose()
        Next

        flpAsignadas.Controls.Clear()

        BotonesRecamarerasDiccionario.Clear()

        If Not IsNothing(HabitacionActual) Then

            Dim controlador As New Controladores.ControladorGlobal()
            Dim controladorTareasL As New Controladores.ControladorTareasLimpieza

            Dim empleados As List(Of Modelo.Entidades.Empleado) = controlador.AplicacionServicioEmpleados.ObtenerRecamareras(controlador.UsuarioActual)

            Dim tareaActual As Modelo.Entidades.TareaLimpieza = controladorTareasL.ObtenerTareaActualConEmpleados(HabitacionActual.Id)

            If Not IsNothing(tareaActual) Then
                Dim empleadosAsignados As List(Of Modelo.Entidades.Empleado) = tareaActual.LimpiezaEmpleados.Where(Function(m)
                                                                                                                       Return m.Activo
                                                                                                                   End Function) _
                                                                                                  .Select(Function(m)
                                                                                                              Return m.Empleado
                                                                                                          End Function).ToList()

                CrearBotones(flpAsignadas, empleadosAsignados, New List(Of Modelo.Entidades.LimpiezaEmpleado), BotonesRecamarerasDiccionario, New EventHandler(AddressOf CambiarPadre), COLOR_ASIGNADA)

                empleados = empleados.Where(Function(m)
                                                Return Not empleadosAsignados.Any(Function(e)
                                                                                      Return e.Id = m.Id
                                                                                  End Function)
                                            End Function).ToList()

                If tareaActual.Tipo = Modelo.Entidades.TareaLimpieza.TiposLimpieza.Normal Then
                    btnNormal.Checked = True
                Else
                    btnDetallado.Checked = True
                End If

            Else
                btnNormal.Checked = True
            End If
            If HabitacionActual.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia Or HabitacionActual.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza Then
                Dim empleadosTareas As List(Of Modelo.Entidades.LimpiezaEmpleado) = controladorTareasL.ObtenerEmpleadosLimpiezaActivos()

                CrearBotones(flpDisponibles, empleados, empleadosTareas, BotonesRecamarerasDiccionario, New EventHandler(AddressOf CambiarPadre), COLOR_DISPONIBLE)
            Else
                Throw New SOTException(My.Resources.Errores.habitacion_no_sucia_o_limpieza_exception, HabitacionActual.NumeroHabitacion)
            End If
        End If
    End Sub

    Private Shared Sub CrearBotones(flp As FlowLayoutPanel, empleados As List(Of Modelo.Entidades.Empleado), empleadosTareas As List(Of Modelo.Entidades.LimpiezaEmpleado), _
                                    botonesRecamarerasDiccionario As Dictionary(Of GelButton, Modelo.Entidades.Empleado), delegado As EventHandler,
                                    colorBoton As Color)

        For Each datosEmpleado In (From empleado In empleados
                                   Group Join empleadoLimpieza In empleadosTareas On empleado.Id Equals empleadoLimpieza.IdEmpleado Into Group
                                   From empleadoLimpieza In Group.DefaultIfEmpty()
                                   Select empleado, empleadoLimpieza)
            Dim gelBtn As New GelButton()
            gelBtn.Width = flp.Width - 20
            gelBtn.Height = 30

            If IsNothing(datosEmpleado.empleadoLimpieza) Then
                gelBtn.Text = datosEmpleado.empleado.NombreCompleto
            Else
                gelBtn.Text = datosEmpleado.empleado.NombreCompleto + " - " + datosEmpleado.empleadoLimpieza.NumeroHabitacionActual
            End If

            gelBtn.TextAlign = ContentAlignment.MiddleRight
            gelBtn.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
            gelBtn.Margin = New Padding(0)
            gelBtn.Padding = gelBtn.Margin
            gelBtn.GradientBottom = Color.White
            gelBtn.GradientTop = colorBoton
            AddHandler gelBtn.Click, delegado

            flp.Controls.Add(gelBtn)
            botonesRecamarerasDiccionario.Add(gelBtn, datosEmpleado.empleado)

        Next
    End Sub

    Private Sub SeleccionadorRecamarerasForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarRecamareras()
    End Sub

    Private Sub btnDetallado_Check(sender As Object, e As GelButton.CheckGelButtonEventArgs) Handles btnDetallado.Check
        If e.Value Then
            'btnLavado.Checked = False
            btnNormal.Checked = False
        End If
    End Sub

    Private Sub btnLavado_Check(sender As Object, e As GelButton.CheckGelButtonEventArgs)
        If e.Value Then
            btnDetallado.Checked = False
            btnNormal.Checked = False
        End If
    End Sub

    Private Sub btnNormal_Check(sender As Object, e As GelButton.CheckGelButtonEventArgs) Handles btnNormal.Check
        If e.Value Then
            'btnLavado.Checked = False
            btnDetallado.Checked = False
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If Not IsNothing(HabitacionActual) Then

            Dim tipoLimpieza As Modelo.Entidades.TareaLimpieza.TiposLimpieza

            If btnDetallado.Checked Then
                tipoLimpieza = Modelo.Entidades.TareaLimpieza.TiposLimpieza.Detallado
                'ElseIf btnLavado.Checked Then
                '    tipoLimpieza = Modelo.Entidades.TareaLimpieza.TiposLimpieza.Lavado
            ElseIf btnNormal.Checked Then
                tipoLimpieza = Modelo.Entidades.TareaLimpieza.TiposLimpieza.Normal
            Else
                Throw New SOTException(My.Resources.Errores.tipo_limpieza_no_seleccionada_exception)
            End If

            Dim idsEmpleados As New List(Of Integer)

            For Each key As GelButton In BotonesRecamarerasDiccionario.Keys
                If Not key.Parent.Equals(flpAsignadas) Then
                    Continue For
                End If

                idsEmpleados.Add(BotonesRecamarerasDiccionario(key).Id)
            Next

            Dim controlador As New Controladores.ControladorTareasLimpieza

            If HabitacionActual.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia Then
                controlador.AsignarEmpleadosATarea(HabitacionActual.Id, idsEmpleados, tipoLimpieza)
            ElseIf HabitacionActual.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza Then
                controlador.CambiarEmpleadosTarea(HabitacionActual.Id, idsEmpleados, tipoLimpieza)
            End If

            ProcesoExitoso = True

        End If

        Close()
    End Sub

    Private Sub CambiarPadre(sender As Object, e As EventArgs)
        Dim btn As GelButton = TryCast(sender, GelButton)

        If IsNothing(btn) Then
            Return
        End If

        If btn.Parent.Equals(flpAsignadas) Then
            flpDisponibles.Controls.Add(btn)
            btn.GradientTop = COLOR_DISPONIBLE
        ElseIf btn.Parent.Equals(flpDisponibles) Then
            flpAsignadas.Controls.Add(btn)
            btn.GradientTop = COLOR_ASIGNADA
        End If
    End Sub
End Class