﻿Imports Transversal.Extensiones
Imports Transversal.Excepciones

Public Class SeleccionadorMeseroForm

    Private _procesoExitoso As Boolean = False

    Public Property ProcesoExitoso As Boolean
        Get
            Return _procesoExitoso
        End Get
        Private Set(value As Boolean)
            _procesoExitoso = value
        End Set
    End Property

    Private WithEvents listaMarcable As CheckedListBox

    Private _empleadoActual As Modelo.Entidades.Empleado

    Public ReadOnly Property EmpleadoSeleccionado As Modelo.Entidades.Empleado
        Get
            Return _empleadoActual
        End Get
    End Property

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        listaMarcable = New CheckedListBox
    End Sub

    Private Sub CobroComandaForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim controlador As New Controladores.ControladorGlobal

        Dim empleados As List(Of Modelo.Entidades.Empleado) = controlador.AplicacionServicioEmpleados.ObtenerMeseros(controlador.UsuarioActual)

        listaMarcable.SelectionMode = SelectionMode.One
        listaMarcable.CheckOnClick = True

        listaMarcable.DataSource = empleados
        listaMarcable.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.Empleado, String)(Function(m) m.NombreCompleto)
        listaMarcable.Margin = New Padding(0)

        flpDisponibles.Controls.Add(listaMarcable)
        listaMarcable.Size = flpDisponibles.Size

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If listaMarcable.CheckedItems.Count <= 0 Then
            Throw New SOTException(My.Resources.Errores.mesero_no_seleccionado_exception)
        End If

        _empleadoActual = CType(listaMarcable.CheckedItems(0), Modelo.Entidades.Empleado)

        If Not IsNothing(_empleadoActual) Then
            ProcesoExitoso = True
        End If

        Close()
    End Sub
End Class