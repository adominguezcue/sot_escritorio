﻿Imports Transversal.Excepciones
Imports Transversal.Extensiones

Public Class SeleccionadorSupervisor
    Private _procesoExitoso As Boolean = False

    Private WithEvents listaMarcable As CheckedListBox

    Private HabitacionActual As Modelo.Entidades.Habitacion

    Public Property ProcesoExitoso As Boolean
        Get
            Return _procesoExitoso
        End Get
        Private Set(value As Boolean)
            _procesoExitoso = value
        End Set
    End Property

    'Private BotonesSupervisoresDiccionario As Dictionary(Of GelButton, Modelo.Entidades.Empleado)
    'Private empleadoActual As Modelo.Entidades.Empleado = Nothing

    Public Sub New(habitacion As Modelo.Entidades.Habitacion)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        listaMarcable = New CheckedListBox

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        'BotonesSupervisoresDiccionario = New Dictionary(Of GelButton, Modelo.Entidades.Empleado)

        HabitacionActual = habitacion
    End Sub

    Private Sub SeleccionadorSupervisorForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not IsNothing(HabitacionActual) Then

            Dim controlador As New Controladores.ControladorGlobal()
            Dim controladorTareasL As New Controladores.ControladorTareasLimpieza

            Dim empleados As List(Of Modelo.Entidades.Empleado) = controlador.AplicacionServicioEmpleados.ObtenerSupervisores(controlador.UsuarioActual)


            listaMarcable.SelectionMode = SelectionMode.One
            listaMarcable.CheckOnClick = True

            listaMarcable.DataSource = empleados
            listaMarcable.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.Empleado, String)(Function(m) m.NombreCompleto)
            listaMarcable.Margin = New Padding(0)

            flpDisponibles.Controls.Add(listaMarcable)
            listaMarcable.Size = flpDisponibles.Size

            Dim tareaActual As Modelo.Entidades.TareaLimpieza = controladorTareasL.ObtenerTareaActualConEmpleados(HabitacionActual.Id)

            If Not IsNothing(tareaActual) AndAlso tareaActual.IdEmpleadoSupervisa.HasValue Then
                Dim indice As Integer = empleados.FindIndex(Function(m)
                                                                Return m.Id = tareaActual.IdEmpleadoSupervisa.Value
                                                            End Function)

                listaMarcable.SetItemChecked(indice, True)
            End If
        End If
    End Sub

    Private Sub LimpiarChecksTipos(index As Integer)
        For ix As Integer = 0 To listaMarcable.Items.Count - 1
            If ix <> index Then
                listaMarcable.SetItemChecked(ix, False)
                listaMarcable.SetSelected(ix, False)
            End If
        Next
    End Sub

    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles listaMarcable.ItemCheck
        LimpiarChecksTipos(e.Index)
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If Not IsNothing(HabitacionActual) Then

            If listaMarcable.CheckedItems.Count = 0 Then
                Throw New SOTException(My.Resources.Errores.supervisor_no_seleccionado_exception)
            End If

            Dim empleadoActual As Modelo.Entidades.Empleado = TryCast(listaMarcable.CheckedItems(0), Modelo.Entidades.Empleado)

            Dim controlador As New Controladores.ControladorTareasLimpieza

            If HabitacionActual.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza Then
                controlador.AsignarSupervisorATarea(HabitacionActual.Id, empleadoActual.Id)
            ElseIf HabitacionActual.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Supervision Then
                controlador.CambiarSupervisorTarea(HabitacionActual.Id, empleadoActual.Id)
            Else
                Throw New SOTException(My.Resources.Errores.habitacion_no_limpieza_o_supervision_exception, HabitacionActual.NumeroHabitacion)
            End If

            ProcesoExitoso = True
        End If

        Close()
    End Sub

    Private Sub SeleccionarSupervisor(sender As Object, e As EventArgs)
        'empleadoActual = BotonesSupervisoresDiccionario(sender)
    End Sub

End Class