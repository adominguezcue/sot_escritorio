﻿Public Class GestionOrdenesForm

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

#If DEBUG Then
        Text += " " + System.Configuration.ConfigurationManager.AppSettings("versionAplicacion")
#End If

    End Sub

    Public Sub CargarOrdenes()

        For Each c As Control In flpGestiones.Controls

            If TypeOf (c) Is Button Then
                RemoveHandler CType(c, Button).Click, AddressOf AgregarOrden
            ElseIf TypeOf (c) Is GestionOrden Then
                RemoveHandler CType(c, GestionOrden).PostCobro, AddressOf CargarOrdenes
            End If

            c.Dispose()
        Next

        flpGestiones.Controls.Clear()

        Dim controlador As New Controladores.ControladorGlobal
        Dim controladorR As New Controladores.ControladorRestaurantes

        Dim ordenes As List(Of Modelo.Entidades.OrdenRestaurante) = controladorR.ObtenerDetallesOrdenesPendientes()

        'Dim configuracionGlobal As Modelo.ConfiguracionGlobal = controlador.AplicacionServicioConfiguracionesGlobales.ObtenerConfiguracionGlobal()

        For Each orden In ordenes
            Dim gOrden As New GestionOrden
            gOrden.CargarDetallesOrden(orden)

            flpGestiones.Controls.Add(gOrden)
            gOrden.Margin = New Padding(5, 0, 5, 0)

            AddHandler gOrden.PostCobro, AddressOf CargarOrdenes
        Next

        Dim botonAgregar As New Button()
        botonAgregar.Text = "Agregar"
        AddHandler botonAgregar.Click, AddressOf AgregarOrden

        flpGestiones.Controls.Add(botonAgregar)
        botonAgregar.Size = New Size(361, 600)
        botonAgregar.Margin = New Padding(5, 0, 5, 0)

    End Sub

    Private Sub AgregarOrden(sender As Object, e As EventArgs)
        Dim controlador As New Controladores.ControladorGlobal

        Dim comandasF As New CreacionOrdenesRestauranteForm

        comandasF.ShowDialog(Me)

        CargarOrdenes()
    End Sub

    Private Sub GestionOrdenesForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarOrdenes()
    End Sub
End Class