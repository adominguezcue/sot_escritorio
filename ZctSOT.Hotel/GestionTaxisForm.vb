﻿Public Class GestionTaxisForm
    Private Sub CargarOrdenesTaxis()

        For Each c As Control In FlowLayoutPanel1.Controls

            If TypeOf (c) Is Button Then
                RemoveHandler CType(c, Button).Click, AddressOf AgregarOrdenTaxi
            ElseIf TypeOf (c) Is GestionTaxi Then
                RemoveHandler CType(c, GestionTaxi).PostCobro, AddressOf CargarOrdenesTaxis
            End If

            c.Dispose()
        Next

        FlowLayoutPanel1.Controls.Clear()

        Dim controlador As New Controladores.ControladorGlobal
        Dim controladorT As New Controladores.ControladorTaxis

        Dim ordenes As List(Of Modelo.Entidades.OrdenTaxi) = controladorT.ObtenerOrdenesPendientes()

        Dim configuracionGlobal As Modelo.Entidades.ConfiguracionGlobal = controlador.AplicacionServicioConfiguracionesGlobales.ObtenerConfiguracionGlobal()

        For i As Integer = 0 To configuracionGlobal.MaximoTaxisSimultaneos - 1
            If i < ordenes.Count Then

                Dim gTaxi As New GestionTaxi
                gTaxi.CargarDetalles(ordenes(i))

                FlowLayoutPanel1.Controls.Add(gTaxi)
                gTaxi.Margin = New Padding(5, 0, 5, 0)

                AddHandler gTaxi.PostCobro, AddressOf CargarOrdenesTaxis
            Else
                Dim botonAgregar As New Button()
                botonAgregar.Text = "Agregar"
                botonAgregar.Size = New Size(273, 368)
                AddHandler botonAgregar.Click, AddressOf AgregarOrdenTaxi

                FlowLayoutPanel1.Controls.Add(botonAgregar)
                botonAgregar.Margin = New Padding(5, 0, 5, 0)

            End If
        Next

    End Sub

    Private Sub AgregarOrdenTaxi(sender As Object, e As EventArgs)
        Dim controlador As New Controladores.ControladorGlobal

        Dim taxisF As New CreacionOrdenTaxiForm

        taxisF.ShowDialog(Me)

        CargarOrdenesTaxis()
    End Sub

    Private Sub GestionTaxisForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarOrdenesTaxis()
    End Sub
End Class