﻿Public Class FormBase

    Private Const CP_NOCLOSE_BUTTON As Integer = &H200

    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim myCp As CreateParams = MyBase.CreateParams
            myCp.ClassStyle = myCp.ClassStyle Or CP_NOCLOSE_BUTTON
            Return myCp
        End Get
    End Property

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Close()
    End Sub

    Private Sub btnAceptar_LocationChanged(sender As Object, e As EventArgs) Handles btnAceptar.LocationChanged
        AcomodarBtnCancelar()
    End Sub

    Private Sub btnAceptar_VisibleChanged(sender As Object, e As EventArgs) Handles btnAceptar.VisibleChanged
        AcomodarBtnCancelar()
    End Sub

    Private Sub AcomodarBtnCancelar()
        If btnAceptar.Visible Then
            btnCancelar.Location = New Point(btnAceptar.Location.X - btnCancelar.Width - 4, btnAceptar.Location.Y)
        Else
            btnCancelar.Location = btnAceptar.Location
        End If
    End Sub

End Class