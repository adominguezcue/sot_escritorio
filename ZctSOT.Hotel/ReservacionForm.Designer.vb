﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReservacionForm
    Inherits System.Windows.Forms.Form


    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        Me.btnAceptar = New ExtraComponents.GelButton()
        Me.grupoAdicionales = New ExtraComponents.FixedHeaderPanel()
        Me.grupoFacturacion = New ExtraComponents.FixedHeaderPanel()
        Me.grupoDatosReservacion = New ExtraComponents.FixedHeaderPanel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.nudPExtra = New System.Windows.Forms.NumericUpDown()
        Me.nudNoches = New System.Windows.Forms.NumericUpDown()
        Me.lblDesc = New System.Windows.Forms.Label()
        Me.nudDescuento = New ExtraComponents.CurrencyUpDown()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lblDescuentosTotal = New System.Windows.Forms.Label()
        Me.lblPaquetesTotal = New System.Windows.Forms.Label()
        Me.lblHospExtraTotal = New System.Windows.Forms.Label()
        Me.lblPExtraTotal = New System.Windows.Forms.Label()
        Me.lblHabitacionTotal = New System.Windows.Forms.Label()
        Me.lblHospExtraPUnidad = New System.Windows.Forms.Label()
        Me.lblPExtraPUnidad = New System.Windows.Forms.Label()
        Me.lblHabitacionPUnidad = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblDescuentosContador = New System.Windows.Forms.Label()
        Me.lblPaquetesContador = New System.Windows.Forms.Label()
        Me.lblHospExtraContador = New System.Windows.Forms.Label()
        Me.lblPExtraContador = New System.Windows.Forms.Label()
        Me.lblHabitacionContador = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.clbPaquetes = New System.Windows.Forms.CheckedListBox()
        Me.cbTiposHabitaciones = New System.Windows.Forms.ComboBox()
        Me.dtpFechaSalida = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaEntrada = New System.Windows.Forms.DateTimePicker()
        Me.btnCancelar = New ExtraComponents.GelButton()
        Me.txtObservaciones = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtEmailFacturacion = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtCiudad = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtEstado = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtCP = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtNumInt = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtCalle = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtNumExt = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtColonia = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtRazonSocial = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtRfc = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtLeyendaDescuento = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtTelefono = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtEmail = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtIdCliente = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtNombre = New ZctSOT.Hotel.CampoPersonalizado()
        Me.txtCodigoReserva = New ZctSOT.Hotel.CampoPersonalizado()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grupoAdicionales.SuspendLayout()
        Me.grupoFacturacion.SuspendLayout()
        Me.grupoDatosReservacion.SuspendLayout()
        CType(Me.nudPExtra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudNoches, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pbLogo
        '
        Me.pbLogo.Font = New System.Drawing.Font("Lato Black", 45.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.pbLogo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.pbLogo.Image = Global.ZctSOT.Hotel.My.Resources.Resources.logo_VBoutique
        Me.pbLogo.Location = New System.Drawing.Point(0, 0)
        Me.pbLogo.Margin = New System.Windows.Forms.Padding(0)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(399, 146)
        Me.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbLogo.TabIndex = 32
        Me.pbLogo.TabStop = False
        Me.pbLogo.Text = "V Motel  Boutique"
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.BackColor = System.Drawing.Color.White
        Me.btnAceptar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnAceptar.Checkable = False
        Me.btnAceptar.Checked = False
        Me.btnAceptar.DisableHighlight = False
        Me.btnAceptar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnAceptar.GelColor = System.Drawing.Color.White
        Me.btnAceptar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnAceptar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnAceptar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_icono_habilitada
        Me.btnAceptar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnAceptar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnAceptar.Location = New System.Drawing.Point(806, 511)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(0)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(104, 80)
        Me.btnAceptar.TabIndex = 33
        Me.btnAceptar.Text = "ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'grupoAdicionales
        '
        Me.grupoAdicionales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grupoAdicionales.Controls.Add(Me.txtObservaciones)
        Me.grupoAdicionales.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.grupoAdicionales.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.grupoAdicionales.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.grupoAdicionales.Location = New System.Drawing.Point(483, 373)
        Me.grupoAdicionales.Name = "grupoAdicionales"
        Me.grupoAdicionales.Size = New System.Drawing.Size(427, 127)
        Me.grupoAdicionales.TabIndex = 2
        Me.grupoAdicionales.Text = "Adicionales"
        '
        'grupoFacturacion
        '
        Me.grupoFacturacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grupoFacturacion.Controls.Add(Me.txtEmailFacturacion)
        Me.grupoFacturacion.Controls.Add(Me.txtCiudad)
        Me.grupoFacturacion.Controls.Add(Me.txtEstado)
        Me.grupoFacturacion.Controls.Add(Me.txtCP)
        Me.grupoFacturacion.Controls.Add(Me.txtNumInt)
        Me.grupoFacturacion.Controls.Add(Me.txtCalle)
        Me.grupoFacturacion.Controls.Add(Me.txtNumExt)
        Me.grupoFacturacion.Controls.Add(Me.txtColonia)
        Me.grupoFacturacion.Controls.Add(Me.txtRazonSocial)
        Me.grupoFacturacion.Controls.Add(Me.txtRfc)
        Me.grupoFacturacion.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.grupoFacturacion.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.grupoFacturacion.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.grupoFacturacion.Location = New System.Drawing.Point(482, 150)
        Me.grupoFacturacion.Name = "grupoFacturacion"
        Me.grupoFacturacion.Size = New System.Drawing.Size(428, 216)
        Me.grupoFacturacion.TabIndex = 1
        Me.grupoFacturacion.Text = "Información de facturación"
        '
        'grupoDatosReservacion
        '
        Me.grupoDatosReservacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grupoDatosReservacion.Controls.Add(Me.Label6)
        Me.grupoDatosReservacion.Controls.Add(Me.Label5)
        Me.grupoDatosReservacion.Controls.Add(Me.nudPExtra)
        Me.grupoDatosReservacion.Controls.Add(Me.nudNoches)
        Me.grupoDatosReservacion.Controls.Add(Me.lblDesc)
        Me.grupoDatosReservacion.Controls.Add(Me.nudDescuento)
        Me.grupoDatosReservacion.Controls.Add(Me.lblTotal)
        Me.grupoDatosReservacion.Controls.Add(Me.Label23)
        Me.grupoDatosReservacion.Controls.Add(Me.lblDescuentosTotal)
        Me.grupoDatosReservacion.Controls.Add(Me.lblPaquetesTotal)
        Me.grupoDatosReservacion.Controls.Add(Me.lblHospExtraTotal)
        Me.grupoDatosReservacion.Controls.Add(Me.lblPExtraTotal)
        Me.grupoDatosReservacion.Controls.Add(Me.lblHabitacionTotal)
        Me.grupoDatosReservacion.Controls.Add(Me.lblHospExtraPUnidad)
        Me.grupoDatosReservacion.Controls.Add(Me.lblPExtraPUnidad)
        Me.grupoDatosReservacion.Controls.Add(Me.lblHabitacionPUnidad)
        Me.grupoDatosReservacion.Controls.Add(Me.Label10)
        Me.grupoDatosReservacion.Controls.Add(Me.Label11)
        Me.grupoDatosReservacion.Controls.Add(Me.Label12)
        Me.grupoDatosReservacion.Controls.Add(Me.Label13)
        Me.grupoDatosReservacion.Controls.Add(Me.Label14)
        Me.grupoDatosReservacion.Controls.Add(Me.lblDescuentosContador)
        Me.grupoDatosReservacion.Controls.Add(Me.lblPaquetesContador)
        Me.grupoDatosReservacion.Controls.Add(Me.lblHospExtraContador)
        Me.grupoDatosReservacion.Controls.Add(Me.lblPExtraContador)
        Me.grupoDatosReservacion.Controls.Add(Me.lblHabitacionContador)
        Me.grupoDatosReservacion.Controls.Add(Me.Label4)
        Me.grupoDatosReservacion.Controls.Add(Me.Label3)
        Me.grupoDatosReservacion.Controls.Add(Me.Label1)
        Me.grupoDatosReservacion.Controls.Add(Me.clbPaquetes)
        Me.grupoDatosReservacion.Controls.Add(Me.txtLeyendaDescuento)
        Me.grupoDatosReservacion.Controls.Add(Me.cbTiposHabitaciones)
        Me.grupoDatosReservacion.Controls.Add(Me.dtpFechaSalida)
        Me.grupoDatosReservacion.Controls.Add(Me.dtpFechaEntrada)
        Me.grupoDatosReservacion.Controls.Add(Me.txtTelefono)
        Me.grupoDatosReservacion.Controls.Add(Me.txtEmail)
        Me.grupoDatosReservacion.Controls.Add(Me.txtIdCliente)
        Me.grupoDatosReservacion.Controls.Add(Me.txtNombre)
        Me.grupoDatosReservacion.Controls.Add(Me.txtCodigoReserva)
        Me.grupoDatosReservacion.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.grupoDatosReservacion.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.grupoDatosReservacion.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.grupoDatosReservacion.Location = New System.Drawing.Point(12, 150)
        Me.grupoDatosReservacion.Name = "grupoDatosReservacion"
        Me.grupoDatosReservacion.Size = New System.Drawing.Size(464, 441)
        Me.grupoDatosReservacion.TabIndex = 0
        Me.grupoDatosReservacion.Text = "Datos de reservación"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label6.Location = New System.Drawing.Point(363, 125)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 15)
        Me.Label6.TabIndex = 45
        Me.Label6.Text = "P. Extra"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label5.Location = New System.Drawing.Point(271, 125)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 15)
        Me.Label5.TabIndex = 44
        Me.Label5.Text = "Noches"
        '
        'nudPExtra
        '
        Me.nudPExtra.Location = New System.Drawing.Point(366, 144)
        Me.nudPExtra.Name = "nudPExtra"
        Me.nudPExtra.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.nudPExtra.Size = New System.Drawing.Size(83, 22)
        Me.nudPExtra.TabIndex = 8
        Me.nudPExtra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nudNoches
        '
        Me.nudNoches.Location = New System.Drawing.Point(274, 144)
        Me.nudNoches.Name = "nudNoches"
        Me.nudNoches.ReadOnly = True
        Me.nudNoches.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.nudNoches.Size = New System.Drawing.Size(83, 22)
        Me.nudNoches.TabIndex = 7
        Me.nudNoches.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDesc
        '
        Me.lblDesc.AutoSize = True
        Me.lblDesc.Location = New System.Drawing.Point(177, 168)
        Me.lblDesc.Name = "lblDesc"
        Me.lblDesc.Size = New System.Drawing.Size(64, 15)
        Me.lblDesc.TabIndex = 41
        Me.lblDesc.Text = "Descuento"
        '
        'nudDescuento
        '
        Me.nudDescuento.DecimalPlaces = 2
        Me.nudDescuento.Location = New System.Drawing.Point(180, 189)
        Me.nudDescuento.Name = "nudDescuento"
        Me.nudDescuento.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.nudDescuento.Size = New System.Drawing.Size(89, 22)
        Me.nudDescuento.TabIndex = 10
        Me.nudDescuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTotal
        '
        Me.lblTotal.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblTotal.Font = New System.Drawing.Font("Lato", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblTotal.Location = New System.Drawing.Point(308, 398)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(141, 27)
        Me.lblTotal.TabIndex = 39
        Me.lblTotal.Text = "0"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Lato", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label23.Location = New System.Drawing.Point(199, 398)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(78, 27)
        Me.Label23.TabIndex = 38
        Me.Label23.Text = "Total"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescuentosTotal
        '
        Me.lblDescuentosTotal.BackColor = System.Drawing.Color.Transparent
        Me.lblDescuentosTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblDescuentosTotal.Location = New System.Drawing.Point(372, 356)
        Me.lblDescuentosTotal.Name = "lblDescuentosTotal"
        Me.lblDescuentosTotal.Size = New System.Drawing.Size(78, 17)
        Me.lblDescuentosTotal.TabIndex = 37
        Me.lblDescuentosTotal.Text = "$0.00"
        Me.lblDescuentosTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPaquetesTotal
        '
        Me.lblPaquetesTotal.BackColor = System.Drawing.Color.Transparent
        Me.lblPaquetesTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPaquetesTotal.Location = New System.Drawing.Point(372, 334)
        Me.lblPaquetesTotal.Name = "lblPaquetesTotal"
        Me.lblPaquetesTotal.Size = New System.Drawing.Size(78, 17)
        Me.lblPaquetesTotal.TabIndex = 36
        Me.lblPaquetesTotal.Text = "$0.00"
        Me.lblPaquetesTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHospExtraTotal
        '
        Me.lblHospExtraTotal.BackColor = System.Drawing.Color.Transparent
        Me.lblHospExtraTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHospExtraTotal.Location = New System.Drawing.Point(372, 312)
        Me.lblHospExtraTotal.Name = "lblHospExtraTotal"
        Me.lblHospExtraTotal.Size = New System.Drawing.Size(78, 17)
        Me.lblHospExtraTotal.TabIndex = 35
        Me.lblHospExtraTotal.Text = "$0.00"
        Me.lblHospExtraTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPExtraTotal
        '
        Me.lblPExtraTotal.BackColor = System.Drawing.Color.Transparent
        Me.lblPExtraTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPExtraTotal.Location = New System.Drawing.Point(372, 290)
        Me.lblPExtraTotal.Name = "lblPExtraTotal"
        Me.lblPExtraTotal.Size = New System.Drawing.Size(78, 17)
        Me.lblPExtraTotal.TabIndex = 34
        Me.lblPExtraTotal.Text = "$0.00"
        Me.lblPExtraTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHabitacionTotal
        '
        Me.lblHabitacionTotal.BackColor = System.Drawing.Color.Transparent
        Me.lblHabitacionTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHabitacionTotal.Location = New System.Drawing.Point(372, 268)
        Me.lblHabitacionTotal.Name = "lblHabitacionTotal"
        Me.lblHabitacionTotal.Size = New System.Drawing.Size(78, 17)
        Me.lblHabitacionTotal.TabIndex = 33
        Me.lblHabitacionTotal.Text = "$0.00"
        Me.lblHabitacionTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHospExtraPUnidad
        '
        Me.lblHospExtraPUnidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblHospExtraPUnidad.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHospExtraPUnidad.Location = New System.Drawing.Point(309, 312)
        Me.lblHospExtraPUnidad.Name = "lblHospExtraPUnidad"
        Me.lblHospExtraPUnidad.Size = New System.Drawing.Size(59, 17)
        Me.lblHospExtraPUnidad.TabIndex = 32
        Me.lblHospExtraPUnidad.Text = "0"
        Me.lblHospExtraPUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPExtraPUnidad
        '
        Me.lblPExtraPUnidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblPExtraPUnidad.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPExtraPUnidad.Location = New System.Drawing.Point(309, 290)
        Me.lblPExtraPUnidad.Name = "lblPExtraPUnidad"
        Me.lblPExtraPUnidad.Size = New System.Drawing.Size(59, 17)
        Me.lblPExtraPUnidad.TabIndex = 31
        Me.lblPExtraPUnidad.Text = "0"
        Me.lblPExtraPUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHabitacionPUnidad
        '
        Me.lblHabitacionPUnidad.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblHabitacionPUnidad.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHabitacionPUnidad.Location = New System.Drawing.Point(309, 268)
        Me.lblHabitacionPUnidad.Name = "lblHabitacionPUnidad"
        Me.lblHabitacionPUnidad.Size = New System.Drawing.Size(59, 17)
        Me.lblHabitacionPUnidad.TabIndex = 30
        Me.lblHabitacionPUnidad.Text = "0"
        Me.lblHabitacionPUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label10.Location = New System.Drawing.Point(227, 356)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(78, 17)
        Me.Label10.TabIndex = 29
        Me.Label10.Text = "Descuento"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label11.Location = New System.Drawing.Point(227, 334)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(78, 17)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "Paquetes"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label12.Location = New System.Drawing.Point(227, 312)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(78, 17)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Hosp. Extra"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label13.Location = New System.Drawing.Point(227, 290)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(78, 17)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "Per. Extra"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label14.Location = New System.Drawing.Point(227, 268)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(78, 17)
        Me.Label14.TabIndex = 25
        Me.Label14.Text = "Habitación"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescuentosContador
        '
        Me.lblDescuentosContador.BackColor = System.Drawing.Color.FromArgb(CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer), CType(CType(197, Byte), Integer))
        Me.lblDescuentosContador.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblDescuentosContador.Location = New System.Drawing.Point(199, 356)
        Me.lblDescuentosContador.Name = "lblDescuentosContador"
        Me.lblDescuentosContador.Size = New System.Drawing.Size(24, 17)
        Me.lblDescuentosContador.TabIndex = 24
        Me.lblDescuentosContador.Text = "0"
        Me.lblDescuentosContador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPaquetesContador
        '
        Me.lblPaquetesContador.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblPaquetesContador.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPaquetesContador.Location = New System.Drawing.Point(199, 334)
        Me.lblPaquetesContador.Name = "lblPaquetesContador"
        Me.lblPaquetesContador.Size = New System.Drawing.Size(24, 17)
        Me.lblPaquetesContador.TabIndex = 23
        Me.lblPaquetesContador.Text = "0"
        Me.lblPaquetesContador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHospExtraContador
        '
        Me.lblHospExtraContador.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblHospExtraContador.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHospExtraContador.Location = New System.Drawing.Point(199, 312)
        Me.lblHospExtraContador.Name = "lblHospExtraContador"
        Me.lblHospExtraContador.Size = New System.Drawing.Size(24, 17)
        Me.lblHospExtraContador.TabIndex = 22
        Me.lblHospExtraContador.Text = "0"
        Me.lblHospExtraContador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPExtraContador
        '
        Me.lblPExtraContador.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblPExtraContador.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPExtraContador.Location = New System.Drawing.Point(199, 290)
        Me.lblPExtraContador.Name = "lblPExtraContador"
        Me.lblPExtraContador.Size = New System.Drawing.Size(24, 17)
        Me.lblPExtraContador.TabIndex = 21
        Me.lblPExtraContador.Text = "0"
        Me.lblPExtraContador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblHabitacionContador
        '
        Me.lblHabitacionContador.BackColor = System.Drawing.Color.FromArgb(CType(CType(203, Byte), Integer), CType(CType(223, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.lblHabitacionContador.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblHabitacionContador.Location = New System.Drawing.Point(199, 268)
        Me.lblHabitacionContador.Name = "lblHabitacionContador"
        Me.lblHabitacionContador.Size = New System.Drawing.Size(24, 17)
        Me.lblHabitacionContador.TabIndex = 20
        Me.lblHabitacionContador.Text = "0"
        Me.lblHabitacionContador.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label4.Location = New System.Drawing.Point(142, 125)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 15)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Fecha de salida"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label3.Location = New System.Drawing.Point(16, 125)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(97, 15)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Fecha de entrada"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.Location = New System.Drawing.Point(16, 168)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 15)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Tipo de habitación"
        '
        'clbPaquetes
        '
        Me.clbPaquetes.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.clbPaquetes.FormattingEnabled = True
        Me.clbPaquetes.Location = New System.Drawing.Point(16, 268)
        Me.clbPaquetes.Name = "clbPaquetes"
        Me.clbPaquetes.Size = New System.Drawing.Size(149, 157)
        Me.clbPaquetes.TabIndex = 13
        '
        'cbTiposHabitaciones
        '
        Me.cbTiposHabitaciones.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTiposHabitaciones.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.cbTiposHabitaciones.FormattingEnabled = True
        Me.cbTiposHabitaciones.Location = New System.Drawing.Point(16, 189)
        Me.cbTiposHabitaciones.Name = "cbTiposHabitaciones"
        Me.cbTiposHabitaciones.Size = New System.Drawing.Size(149, 23)
        Me.cbTiposHabitaciones.TabIndex = 9
        '
        'dtpFechaSalida
        '
        Me.dtpFechaSalida.CustomFormat = "dd/MM/yyyy"
        Me.dtpFechaSalida.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.dtpFechaSalida.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaSalida.Location = New System.Drawing.Point(145, 144)
        Me.dtpFechaSalida.Name = "dtpFechaSalida"
        Me.dtpFechaSalida.Size = New System.Drawing.Size(120, 22)
        Me.dtpFechaSalida.TabIndex = 6
        '
        'dtpFechaEntrada
        '
        Me.dtpFechaEntrada.CustomFormat = "dd/MM/yyyy"
        Me.dtpFechaEntrada.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.dtpFechaEntrada.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaEntrada.Location = New System.Drawing.Point(16, 144)
        Me.dtpFechaEntrada.Name = "dtpFechaEntrada"
        Me.dtpFechaEntrada.Size = New System.Drawing.Size(120, 22)
        Me.dtpFechaEntrada.TabIndex = 5
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.BackColor = System.Drawing.Color.White
        Me.btnCancelar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCancelar.Checkable = False
        Me.btnCancelar.Checked = False
        Me.btnCancelar.DisableHighlight = False
        Me.btnCancelar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelar.GelColor = System.Drawing.Color.White
        Me.btnCancelar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCancelar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCancelar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.icono_cancelar
        Me.btnCancelar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnCancelar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCancelar.Location = New System.Drawing.Point(698, 511)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(104, 80)
        Me.btnCancelar.TabIndex = 34
        Me.btnCancelar.Text = "CANCELAR"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'txtObservaciones
        '
        Me.txtObservaciones.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtObservaciones.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtObservaciones.Location = New System.Drawing.Point(4, 33)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(418, 89)
        Me.txtObservaciones.TabIndex = 0
        Me.txtObservaciones.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtObservaciones.Title = "Observaciones"
        Me.txtObservaciones.Value = ""
        '
        'txtEmailFacturacion
        '
        Me.txtEmailFacturacion.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtEmailFacturacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtEmailFacturacion.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtEmailFacturacion.Location = New System.Drawing.Point(5, 169)
        Me.txtEmailFacturacion.Name = "txtEmailFacturacion"
        Me.txtEmailFacturacion.Size = New System.Drawing.Size(179, 44)
        Me.txtEmailFacturacion.TabIndex = 9
        Me.txtEmailFacturacion.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtEmailFacturacion.Title = "Email"
        Me.txtEmailFacturacion.Value = ""
        '
        'txtCiudad
        '
        Me.txtCiudad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtCiudad.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCiudad.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtCiudad.Location = New System.Drawing.Point(5, 124)
        Me.txtCiudad.Name = "txtCiudad"
        Me.txtCiudad.Size = New System.Drawing.Size(133, 44)
        Me.txtCiudad.TabIndex = 6
        Me.txtCiudad.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtCiudad.Title = "Ciudad"
        Me.txtCiudad.Value = ""
        '
        'txtEstado
        '
        Me.txtEstado.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtEstado.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtEstado.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtEstado.Location = New System.Drawing.Point(144, 124)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(179, 44)
        Me.txtEstado.TabIndex = 7
        Me.txtEstado.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtEstado.Title = "Estado"
        Me.txtEstado.Value = ""
        '
        'txtCP
        '
        Me.txtCP.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtCP.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCP.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtCP.Location = New System.Drawing.Point(329, 124)
        Me.txtCP.Name = "txtCP"
        Me.txtCP.Size = New System.Drawing.Size(94, 44)
        Me.txtCP.TabIndex = 8
        Me.txtCP.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtCP.Title = "CP"
        Me.txtCP.Value = ""
        '
        'txtNumInt
        '
        Me.txtNumInt.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtNumInt.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNumInt.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtNumInt.Location = New System.Drawing.Point(203, 79)
        Me.txtNumInt.Name = "txtNumInt"
        Me.txtNumInt.Size = New System.Drawing.Size(53, 44)
        Me.txtNumInt.TabIndex = 4
        Me.txtNumInt.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtNumInt.Title = "Num Int"
        Me.txtNumInt.Value = ""
        '
        'txtCalle
        '
        Me.txtCalle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtCalle.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCalle.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtCalle.Location = New System.Drawing.Point(5, 79)
        Me.txtCalle.Name = "txtCalle"
        Me.txtCalle.Size = New System.Drawing.Size(133, 44)
        Me.txtCalle.TabIndex = 2
        Me.txtCalle.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtCalle.Title = "Calle"
        Me.txtCalle.Value = ""
        '
        'txtNumExt
        '
        Me.txtNumExt.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtNumExt.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNumExt.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtNumExt.Location = New System.Drawing.Point(144, 79)
        Me.txtNumExt.Name = "txtNumExt"
        Me.txtNumExt.Size = New System.Drawing.Size(53, 44)
        Me.txtNumExt.TabIndex = 3
        Me.txtNumExt.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtNumExt.Title = "Num Ext"
        Me.txtNumExt.Value = ""
        '
        'txtColonia
        '
        Me.txtColonia.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtColonia.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtColonia.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtColonia.Location = New System.Drawing.Point(262, 79)
        Me.txtColonia.Name = "txtColonia"
        Me.txtColonia.Size = New System.Drawing.Size(161, 44)
        Me.txtColonia.TabIndex = 5
        Me.txtColonia.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtColonia.Title = "Colonia"
        Me.txtColonia.Value = ""
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtRazonSocial.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtRazonSocial.Location = New System.Drawing.Point(144, 34)
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(279, 44)
        Me.txtRazonSocial.TabIndex = 1
        Me.txtRazonSocial.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtRazonSocial.Title = "Nombre o razón social"
        Me.txtRazonSocial.Value = ""
        '
        'txtRfc
        '
        Me.txtRfc.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtRfc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRfc.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtRfc.Location = New System.Drawing.Point(5, 34)
        Me.txtRfc.Name = "txtRfc"
        Me.txtRfc.Size = New System.Drawing.Size(133, 44)
        Me.txtRfc.TabIndex = 0
        Me.txtRfc.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtRfc.Title = "RFC"
        Me.txtRfc.Value = ""
        '
        'txtLeyendaDescuento
        '
        Me.txtLeyendaDescuento.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtLeyendaDescuento.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtLeyendaDescuento.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtLeyendaDescuento.Location = New System.Drawing.Point(284, 168)
        Me.txtLeyendaDescuento.Name = "txtLeyendaDescuento"
        Me.txtLeyendaDescuento.Size = New System.Drawing.Size(166, 44)
        Me.txtLeyendaDescuento.TabIndex = 11
        Me.txtLeyendaDescuento.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtLeyendaDescuento.Title = "Leyenda de descuento"
        Me.txtLeyendaDescuento.Value = ""
        '
        'txtTelefono
        '
        Me.txtTelefono.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtTelefono.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtTelefono.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtTelefono.Location = New System.Drawing.Point(15, 79)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(138, 44)
        Me.txtTelefono.TabIndex = 2
        Me.txtTelefono.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtTelefono.Title = "Teléfono"
        Me.txtTelefono.Value = ""
        '
        'txtEmail
        '
        Me.txtEmail.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtEmail.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtEmail.Location = New System.Drawing.Point(159, 79)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(146, 44)
        Me.txtEmail.TabIndex = 3
        Me.txtEmail.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtEmail.Title = "Email"
        Me.txtEmail.Value = ""
        '
        'txtIdCliente
        '
        Me.txtIdCliente.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtIdCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtIdCliente.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtIdCliente.Location = New System.Drawing.Point(311, 79)
        Me.txtIdCliente.Name = "txtIdCliente"
        Me.txtIdCliente.Size = New System.Drawing.Size(138, 44)
        Me.txtIdCliente.TabIndex = 4
        Me.txtIdCliente.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtIdCliente.Title = "Id de cliente"
        Me.txtIdCliente.Value = ""
        '
        'txtNombre
        '
        Me.txtNombre.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtNombre.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtNombre.Location = New System.Drawing.Point(209, 34)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(241, 44)
        Me.txtNombre.TabIndex = 1
        Me.txtNombre.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtNombre.Title = "Nombre"
        Me.txtNombre.Value = ""
        '
        'txtCodigoReserva
        '
        Me.txtCodigoReserva.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.txtCodigoReserva.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.txtCodigoReserva.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.txtCodigoReserva.Location = New System.Drawing.Point(15, 34)
        Me.txtCodigoReserva.Name = "txtCodigoReserva"
        Me.txtCodigoReserva.Size = New System.Drawing.Size(185, 44)
        Me.txtCodigoReserva.TabIndex = 0
        Me.txtCodigoReserva.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.txtCodigoReserva.Title = "Código de reserva"
        Me.txtCodigoReserva.Value = ""
        '
        'ReservacionForm
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(922, 599)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.pbLogo)
        Me.Controls.Add(Me.grupoAdicionales)
        Me.Controls.Add(Me.grupoFacturacion)
        Me.Controls.Add(Me.grupoDatosReservacion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "ReservacionForm"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reservación de habitación"
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grupoAdicionales.ResumeLayout(False)
        Me.grupoFacturacion.ResumeLayout(False)
        Me.grupoDatosReservacion.ResumeLayout(False)
        Me.grupoDatosReservacion.PerformLayout()
        CType(Me.nudPExtra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudNoches, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents grupoDatosReservacion As FixedHeaderPanel
    Private WithEvents grupoFacturacion As FixedHeaderPanel
    Private WithEvents grupoAdicionales As FixedHeaderPanel
    Private WithEvents txtCodigoReserva As CampoPersonalizado
    Private WithEvents txtTelefono As CampoPersonalizado
    Private WithEvents txtEmail As CampoPersonalizado
    Private WithEvents txtIdCliente As CampoPersonalizado
    Private WithEvents txtNombre As CampoPersonalizado
    Private WithEvents txtLeyendaDescuento As CampoPersonalizado
    Private WithEvents cbTiposHabitaciones As System.Windows.Forms.ComboBox
    Private WithEvents dtpFechaSalida As System.Windows.Forms.DateTimePicker
    Private WithEvents dtpFechaEntrada As System.Windows.Forms.DateTimePicker
    Private WithEvents clbPaquetes As System.Windows.Forms.CheckedListBox
    Private WithEvents txtEmailFacturacion As CampoPersonalizado
    Private WithEvents txtCiudad As CampoPersonalizado
    Private WithEvents txtEstado As CampoPersonalizado
    Private WithEvents txtCP As CampoPersonalizado
    Private WithEvents txtNumInt As CampoPersonalizado
    Private WithEvents txtCalle As CampoPersonalizado
    Private WithEvents txtNumExt As CampoPersonalizado
    Private WithEvents txtColonia As CampoPersonalizado
    Private WithEvents txtRazonSocial As CampoPersonalizado
    Private WithEvents txtRfc As CampoPersonalizado
    Private WithEvents txtObservaciones As CampoPersonalizado
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblHospExtraPUnidad As System.Windows.Forms.Label
    Friend WithEvents lblPExtraPUnidad As System.Windows.Forms.Label
    Friend WithEvents lblHabitacionPUnidad As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblDescuentosContador As System.Windows.Forms.Label
    Friend WithEvents lblPaquetesContador As System.Windows.Forms.Label
    Friend WithEvents lblHospExtraContador As System.Windows.Forms.Label
    Friend WithEvents lblPExtraContador As System.Windows.Forms.Label
    Friend WithEvents lblHabitacionContador As System.Windows.Forms.Label
    Friend WithEvents lblDescuentosTotal As System.Windows.Forms.Label
    Friend WithEvents lblPaquetesTotal As System.Windows.Forms.Label
    Friend WithEvents lblHospExtraTotal As System.Windows.Forms.Label
    Friend WithEvents lblPExtraTotal As System.Windows.Forms.Label
    Friend WithEvents lblHabitacionTotal As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lblDesc As System.Windows.Forms.Label
    Friend WithEvents nudDescuento As ExtraComponents.CurrencyUpDown
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents nudPExtra As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudNoches As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnAceptar As GelButton
    Friend WithEvents btnCancelar As GelButton
End Class
