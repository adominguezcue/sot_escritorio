﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SeleccionadorRecamarerasForm
    Inherits FormBase

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panelRecamarerasDisponibles = New ExtraComponents.FixedHeaderPanel()
        Me.flpDisponibles = New System.Windows.Forms.FlowLayoutPanel()
        Me.panelRecamarerasSeleccionadas = New ExtraComponents.FixedHeaderPanel()
        Me.flpAsignadas = New System.Windows.Forms.FlowLayoutPanel()
        Me.brnAgregar = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnNormal = New ExtraComponents.GelButton()
        Me.btnDetallado = New ExtraComponents.GelButton()
        Me.panelBotonera.SuspendLayout()
        Me.panelRecamarerasDisponibles.SuspendLayout()
        Me.panelRecamarerasSeleccionadas.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Location = New System.Drawing.Point(0, 575)
        Me.panelBotonera.Size = New System.Drawing.Size(543, 84)
        Me.panelBotonera.TabIndex = 11
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_icono_habilitada
        Me.btnAceptar.Location = New System.Drawing.Point(437, 2)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(0)
        '
        'panelRecamarerasDisponibles
        '
        Me.panelRecamarerasDisponibles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panelRecamarerasDisponibles.Controls.Add(Me.flpDisponibles)
        Me.panelRecamarerasDisponibles.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.panelRecamarerasDisponibles.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.panelRecamarerasDisponibles.Location = New System.Drawing.Point(14, 99)
        Me.panelRecamarerasDisponibles.Name = "panelRecamarerasDisponibles"
        Me.panelRecamarerasDisponibles.Padding = New System.Windows.Forms.Padding(0, 25, 0, 0)
        Me.panelRecamarerasDisponibles.Size = New System.Drawing.Size(233, 367)
        Me.panelRecamarerasDisponibles.TabIndex = 0
        Me.panelRecamarerasDisponibles.Text = "Recamareras disponibles"
        '
        'flpDisponibles
        '
        Me.flpDisponibles.AutoScroll = True
        Me.flpDisponibles.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpDisponibles.Location = New System.Drawing.Point(0, 25)
        Me.flpDisponibles.Margin = New System.Windows.Forms.Padding(0)
        Me.flpDisponibles.Name = "flpDisponibles"
        Me.flpDisponibles.Size = New System.Drawing.Size(231, 340)
        Me.flpDisponibles.TabIndex = 0
        '
        'panelRecamarerasSeleccionadas
        '
        Me.panelRecamarerasSeleccionadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panelRecamarerasSeleccionadas.Controls.Add(Me.flpAsignadas)
        Me.panelRecamarerasSeleccionadas.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.panelRecamarerasSeleccionadas.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.panelRecamarerasSeleccionadas.Location = New System.Drawing.Point(296, 99)
        Me.panelRecamarerasSeleccionadas.Name = "panelRecamarerasSeleccionadas"
        Me.panelRecamarerasSeleccionadas.Padding = New System.Windows.Forms.Padding(0, 25, 0, 0)
        Me.panelRecamarerasSeleccionadas.Size = New System.Drawing.Size(233, 367)
        Me.panelRecamarerasSeleccionadas.TabIndex = 1
        Me.panelRecamarerasSeleccionadas.Text = "Recamareras selecionadas"
        '
        'flpAsignadas
        '
        Me.flpAsignadas.AutoScroll = True
        Me.flpAsignadas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpAsignadas.Location = New System.Drawing.Point(0, 25)
        Me.flpAsignadas.Margin = New System.Windows.Forms.Padding(0)
        Me.flpAsignadas.Name = "flpAsignadas"
        Me.flpAsignadas.Size = New System.Drawing.Size(231, 340)
        Me.flpAsignadas.TabIndex = 1
        '
        'brnAgregar
        '
        Me.brnAgregar.Location = New System.Drawing.Point(258, 209)
        Me.brnAgregar.Name = "brnAgregar"
        Me.brnAgregar.Size = New System.Drawing.Size(29, 29)
        Me.brnAgregar.TabIndex = 2
        Me.brnAgregar.Text = ">"
        Me.brnAgregar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Location = New System.Drawing.Point(258, 267)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(29, 29)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "<"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnNormal
        '
        Me.btnNormal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNormal.BackColor = System.Drawing.Color.White
        Me.btnNormal.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnNormal.Checkable = True
        Me.btnNormal.Checked = False
        Me.btnNormal.DisableHighlight = False
        Me.btnNormal.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnNormal.GelColor = System.Drawing.Color.White
        Me.btnNormal.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnNormal.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnNormal.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_limpieza_normal
        Me.btnNormal.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnNormal.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnNormal.Location = New System.Drawing.Point(360, 476)
        Me.btnNormal.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNormal.Name = "btnNormal"
        Me.btnNormal.Size = New System.Drawing.Size(104, 80)
        Me.btnNormal.TabIndex = 9
        Me.btnNormal.Text = "NORMAL"
        Me.btnNormal.UseVisualStyleBackColor = False
        '
        'btnDetallado
        '
        Me.btnDetallado.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDetallado.BackColor = System.Drawing.Color.White
        Me.btnDetallado.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnDetallado.Checkable = True
        Me.btnDetallado.Checked = False
        Me.btnDetallado.DisableHighlight = False
        Me.btnDetallado.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnDetallado.GelColor = System.Drawing.Color.White
        Me.btnDetallado.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnDetallado.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnDetallado.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_limpieza_detallado
        Me.btnDetallado.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnDetallado.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnDetallado.Location = New System.Drawing.Point(78, 476)
        Me.btnDetallado.Margin = New System.Windows.Forms.Padding(0)
        Me.btnDetallado.Name = "btnDetallado"
        Me.btnDetallado.Size = New System.Drawing.Size(104, 80)
        Me.btnDetallado.TabIndex = 8
        Me.btnDetallado.Text = "DETALLADO"
        Me.btnDetallado.UseVisualStyleBackColor = False
        '
        'SeleccionadorRecamarerasForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(544, 659)
        Me.Controls.Add(Me.btnNormal)
        Me.Controls.Add(Me.btnDetallado)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.brnAgregar)
        Me.Controls.Add(Me.panelRecamarerasSeleccionadas)
        Me.Controls.Add(Me.panelRecamarerasDisponibles)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "SeleccionadorRecamarerasForm"
        Me.Text = "Limpieza de habitación"
        Me.Controls.SetChildIndex(Me.panelRecamarerasDisponibles, 0)
        Me.Controls.SetChildIndex(Me.panelRecamarerasSeleccionadas, 0)
        Me.Controls.SetChildIndex(Me.brnAgregar, 0)
        Me.Controls.SetChildIndex(Me.btnEliminar, 0)
        Me.Controls.SetChildIndex(Me.panelBotonera, 0)
        Me.Controls.SetChildIndex(Me.btnDetallado, 0)
        Me.Controls.SetChildIndex(Me.btnNormal, 0)
        Me.panelBotonera.ResumeLayout(False)
        Me.panelRecamarerasDisponibles.ResumeLayout(False)
        Me.panelRecamarerasSeleccionadas.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelRecamarerasDisponibles As ExtraComponents.FixedHeaderPanel
    Friend WithEvents flpDisponibles As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents panelRecamarerasSeleccionadas As ExtraComponents.FixedHeaderPanel
    Friend WithEvents flpAsignadas As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents brnAgregar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnNormal As GelButton
    Friend WithEvents btnDetallado As GelButton
End Class
