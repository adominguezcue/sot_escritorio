﻿Imports Transversal.Excepciones

Public Class ReservacionesMiniForm

    Public ReadOnly Property EsFolioValido As Boolean
        Get
            Return _esFolioValido
        End Get
    End Property

    Private _esFolioValido As Boolean

    Public ReadOnly Property CodigoReservacion As String
        Get
            Return txtFolio.Text
        End Get
    End Property

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If dgvReservaciones.RowCount = 0 Then
            Throw New SOTException(My.Resources.Errores.no_reservaciones_seleccionadas)
        ElseIf dgvReservaciones.RowCount > 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_reservaciones_seleccionadas)
        Else
            _esFolioValido = True
        End If

        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        _esFolioValido = False
        Me.Close()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        dgvReservaciones.DataSource = New Controladores.ControladorGlobal().AplicacionServicioReservaciones.ObtenerReservacionesPorCodigo(txtFolio.Text)
    End Sub



End Class