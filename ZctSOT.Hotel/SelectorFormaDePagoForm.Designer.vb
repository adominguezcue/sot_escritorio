﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SelectorFormaDePagoForm
    Inherits FormBase

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbFormasPago = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tcFormulariosPago = New System.Windows.Forms.TabControl()
        Me.tpTarjeta = New System.Windows.Forms.TabPage()
        Me.mtxtTarjeta = New System.Windows.Forms.MaskedTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.nudPagoTarjeta = New ExtraComponents.CurrencyUpDown()
        Me.nudPropina = New ExtraComponents.CurrencyUpDown()
        Me.nudPagoEfectivo = New ExtraComponents.CurrencyUpDown()
        Me.lblPropinaTarjeta = New System.Windows.Forms.Label()
        Me.lblPagoEfectivo = New System.Windows.Forms.Label()
        Me.lblPagoTarjeta = New System.Windows.Forms.Label()
        Me.mtxtNumeroOperacion = New System.Windows.Forms.MaskedTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAmericanExpress = New ExtraComponents.GelButton()
        Me.btnMasterCard = New ExtraComponents.GelButton()
        Me.btnVisa = New ExtraComponents.GelButton()
        Me.tpCortesia = New System.Windows.Forms.TabPage()
        Me.txtCortesia = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.panelBotonera.SuspendLayout()
        Me.tcFormulariosPago.SuspendLayout()
        Me.tpTarjeta.SuspendLayout()
        CType(Me.nudPagoTarjeta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPropina, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPagoEfectivo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpCortesia.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Location = New System.Drawing.Point(0, 369)
        Me.panelBotonera.Size = New System.Drawing.Size(321, 84)
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(215, 2)
        '
        'cbFormasPago
        '
        Me.cbFormasPago.BackColor = System.Drawing.Color.White
        Me.cbFormasPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbFormasPago.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.cbFormasPago.ForeColor = System.Drawing.Color.Black
        Me.cbFormasPago.FormattingEnabled = True
        Me.cbFormasPago.Location = New System.Drawing.Point(149, 58)
        Me.cbFormasPago.Name = "cbFormasPago"
        Me.cbFormasPago.Size = New System.Drawing.Size(147, 23)
        Me.cbFormasPago.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(21, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 15)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Forma de pago"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tcFormulariosPago
        '
        Me.tcFormulariosPago.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.tcFormulariosPago.Controls.Add(Me.tpTarjeta)
        Me.tcFormulariosPago.Controls.Add(Me.tpCortesia)
        Me.tcFormulariosPago.ItemSize = New System.Drawing.Size(0, 1)
        Me.tcFormulariosPago.Location = New System.Drawing.Point(24, 100)
        Me.tcFormulariosPago.Margin = New System.Windows.Forms.Padding(0)
        Me.tcFormulariosPago.Name = "tcFormulariosPago"
        Me.tcFormulariosPago.SelectedIndex = 0
        Me.tcFormulariosPago.Size = New System.Drawing.Size(272, 246)
        Me.tcFormulariosPago.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.tcFormulariosPago.TabIndex = 1
        '
        'tpTarjeta
        '
        Me.tpTarjeta.BackColor = System.Drawing.Color.White
        Me.tpTarjeta.Controls.Add(Me.mtxtTarjeta)
        Me.tpTarjeta.Controls.Add(Me.Label5)
        Me.tpTarjeta.Controls.Add(Me.nudPagoTarjeta)
        Me.tpTarjeta.Controls.Add(Me.nudPropina)
        Me.tpTarjeta.Controls.Add(Me.nudPagoEfectivo)
        Me.tpTarjeta.Controls.Add(Me.lblPropinaTarjeta)
        Me.tpTarjeta.Controls.Add(Me.lblPagoEfectivo)
        Me.tpTarjeta.Controls.Add(Me.lblPagoTarjeta)
        Me.tpTarjeta.Controls.Add(Me.mtxtNumeroOperacion)
        Me.tpTarjeta.Controls.Add(Me.Label3)
        Me.tpTarjeta.Controls.Add(Me.btnAmericanExpress)
        Me.tpTarjeta.Controls.Add(Me.btnMasterCard)
        Me.tpTarjeta.Controls.Add(Me.btnVisa)
        Me.tpTarjeta.Location = New System.Drawing.Point(4, 5)
        Me.tpTarjeta.Name = "tpTarjeta"
        Me.tpTarjeta.Padding = New System.Windows.Forms.Padding(3)
        Me.tpTarjeta.Size = New System.Drawing.Size(264, 237)
        Me.tpTarjeta.TabIndex = 0
        Me.tpTarjeta.Text = "TabPage1"
        '
        'mtxtTarjeta
        '
        Me.mtxtTarjeta.Location = New System.Drawing.Point(111, 65)
        Me.mtxtTarjeta.Mask = "9999"
        Me.mtxtTarjeta.Name = "mtxtTarjeta"
        Me.mtxtTarjeta.Size = New System.Drawing.Size(147, 22)
        Me.mtxtTarjeta.TabIndex = 54
        Me.mtxtTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(6, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(42, 15)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Tarjeta"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudPagoTarjeta
        '
        Me.nudPagoTarjeta.DecimalPlaces = 2
        Me.nudPagoTarjeta.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
        Me.nudPagoTarjeta.Location = New System.Drawing.Point(111, 135)
        Me.nudPagoTarjeta.Maximum = New Decimal(New Integer() {-1530494976, 232830, 0, 0})
        Me.nudPagoTarjeta.Name = "nudPagoTarjeta"
        Me.nudPagoTarjeta.Size = New System.Drawing.Size(147, 22)
        Me.nudPagoTarjeta.TabIndex = 4
        Me.nudPagoTarjeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nudPropina
        '
        Me.nudPropina.DecimalPlaces = 2
        Me.nudPropina.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
        Me.nudPropina.Location = New System.Drawing.Point(111, 205)
        Me.nudPropina.Maximum = New Decimal(New Integer() {-1530494976, 232830, 0, 0})
        Me.nudPropina.Name = "nudPropina"
        Me.nudPropina.Size = New System.Drawing.Size(147, 22)
        Me.nudPropina.TabIndex = 6
        Me.nudPropina.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'nudPagoEfectivo
        '
        Me.nudPagoEfectivo.DecimalPlaces = 2
        Me.nudPagoEfectivo.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
        Me.nudPagoEfectivo.Location = New System.Drawing.Point(111, 170)
        Me.nudPagoEfectivo.Maximum = New Decimal(New Integer() {-1530494976, 232830, 0, 0})
        Me.nudPagoEfectivo.Name = "nudPagoEfectivo"
        Me.nudPagoEfectivo.ReadOnly = True
        Me.nudPagoEfectivo.Size = New System.Drawing.Size(147, 22)
        Me.nudPagoEfectivo.TabIndex = 5
        Me.nudPagoEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPropinaTarjeta
        '
        Me.lblPropinaTarjeta.AutoSize = True
        Me.lblPropinaTarjeta.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPropinaTarjeta.ForeColor = System.Drawing.Color.Black
        Me.lblPropinaTarjeta.Location = New System.Drawing.Point(6, 201)
        Me.lblPropinaTarjeta.Name = "lblPropinaTarjeta"
        Me.lblPropinaTarjeta.Size = New System.Drawing.Size(63, 30)
        Me.lblPropinaTarjeta.TabIndex = 52
        Me.lblPropinaTarjeta.Text = "Propina" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "con tarjeta"
        Me.lblPropinaTarjeta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPagoEfectivo
        '
        Me.lblPagoEfectivo.AutoSize = True
        Me.lblPagoEfectivo.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPagoEfectivo.ForeColor = System.Drawing.Color.Black
        Me.lblPagoEfectivo.Location = New System.Drawing.Point(6, 166)
        Me.lblPagoEfectivo.Name = "lblPagoEfectivo"
        Me.lblPagoEfectivo.Size = New System.Drawing.Size(65, 30)
        Me.lblPagoEfectivo.TabIndex = 50
        Me.lblPagoEfectivo.Text = "Pago" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "en efectivo"
        Me.lblPagoEfectivo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPagoTarjeta
        '
        Me.lblPagoTarjeta.AutoSize = True
        Me.lblPagoTarjeta.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPagoTarjeta.ForeColor = System.Drawing.Color.Black
        Me.lblPagoTarjeta.Location = New System.Drawing.Point(6, 131)
        Me.lblPagoTarjeta.Name = "lblPagoTarjeta"
        Me.lblPagoTarjeta.Size = New System.Drawing.Size(63, 30)
        Me.lblPagoTarjeta.TabIndex = 48
        Me.lblPagoTarjeta.Text = "Pago" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "con tarjeta"
        Me.lblPagoTarjeta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'mtxtNumeroOperacion
        '
        Me.mtxtNumeroOperacion.Location = New System.Drawing.Point(111, 100)
        Me.mtxtNumeroOperacion.Mask = "99999999999999"
        Me.mtxtNumeroOperacion.Name = "mtxtNumeroOperacion"
        Me.mtxtNumeroOperacion.Size = New System.Drawing.Size(147, 22)
        Me.mtxtNumeroOperacion.TabIndex = 3
        Me.mtxtNumeroOperacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(6, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 30)
        Me.Label3.TabIndex = 46
        Me.Label3.Text = "Número" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de aprobación"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnAmericanExpress
        '
        Me.btnAmericanExpress.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAmericanExpress.BackColor = System.Drawing.Color.White
        Me.btnAmericanExpress.ButtonRectangleOrigin = New System.Drawing.Point(0, 0)
        Me.btnAmericanExpress.Checkable = False
        Me.btnAmericanExpress.Checked = False
        Me.btnAmericanExpress.DisableHighlight = False
        Me.btnAmericanExpress.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnAmericanExpress.GelColor = System.Drawing.Color.White
        Me.btnAmericanExpress.GradientBottom = System.Drawing.Color.White
        Me.btnAmericanExpress.GradientTop = System.Drawing.Color.White
        Me.btnAmericanExpress.Image = Global.ZctSOT.Hotel.My.Resources.Resources.tarjeta_american_express
        Me.btnAmericanExpress.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAmericanExpress.ImageLocation = New System.Drawing.Point(2, 2)
        Me.btnAmericanExpress.ImageSize = New System.Drawing.Size(64, 41)
        Me.btnAmericanExpress.Location = New System.Drawing.Point(193, 8)
        Me.btnAmericanExpress.Margin = New System.Windows.Forms.Padding(0)
        Me.btnAmericanExpress.Name = "btnAmericanExpress"
        Me.btnAmericanExpress.Size = New System.Drawing.Size(68, 45)
        Me.btnAmericanExpress.TabIndex = 2
        Me.btnAmericanExpress.UseVisualStyleBackColor = False
        '
        'btnMasterCard
        '
        Me.btnMasterCard.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMasterCard.BackColor = System.Drawing.Color.White
        Me.btnMasterCard.ButtonRectangleOrigin = New System.Drawing.Point(0, 0)
        Me.btnMasterCard.Checkable = False
        Me.btnMasterCard.Checked = False
        Me.btnMasterCard.DisableHighlight = False
        Me.btnMasterCard.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnMasterCard.GelColor = System.Drawing.Color.White
        Me.btnMasterCard.GradientBottom = System.Drawing.Color.White
        Me.btnMasterCard.GradientTop = System.Drawing.Color.White
        Me.btnMasterCard.Image = Global.ZctSOT.Hotel.My.Resources.Resources.tarjeta_master_card
        Me.btnMasterCard.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMasterCard.ImageLocation = New System.Drawing.Point(2, 2)
        Me.btnMasterCard.ImageSize = New System.Drawing.Size(64, 41)
        Me.btnMasterCard.Location = New System.Drawing.Point(98, 8)
        Me.btnMasterCard.Margin = New System.Windows.Forms.Padding(0)
        Me.btnMasterCard.Name = "btnMasterCard"
        Me.btnMasterCard.Size = New System.Drawing.Size(68, 45)
        Me.btnMasterCard.TabIndex = 1
        Me.btnMasterCard.UseVisualStyleBackColor = False
        '
        'btnVisa
        '
        Me.btnVisa.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnVisa.BackColor = System.Drawing.Color.White
        Me.btnVisa.ButtonRectangleOrigin = New System.Drawing.Point(0, 0)
        Me.btnVisa.Checkable = False
        Me.btnVisa.Checked = False
        Me.btnVisa.DisableHighlight = False
        Me.btnVisa.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnVisa.GelColor = System.Drawing.Color.White
        Me.btnVisa.GradientBottom = System.Drawing.Color.White
        Me.btnVisa.GradientTop = System.Drawing.Color.White
        Me.btnVisa.Image = Global.ZctSOT.Hotel.My.Resources.Resources.tarjeta_visa
        Me.btnVisa.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVisa.ImageLocation = New System.Drawing.Point(2, 2)
        Me.btnVisa.ImageSize = New System.Drawing.Size(64, 41)
        Me.btnVisa.Location = New System.Drawing.Point(3, 8)
        Me.btnVisa.Margin = New System.Windows.Forms.Padding(0)
        Me.btnVisa.Name = "btnVisa"
        Me.btnVisa.Size = New System.Drawing.Size(68, 45)
        Me.btnVisa.TabIndex = 0
        Me.btnVisa.UseVisualStyleBackColor = False
        '
        'tpCortesia
        '
        Me.tpCortesia.BackColor = System.Drawing.Color.White
        Me.tpCortesia.Controls.Add(Me.txtCortesia)
        Me.tpCortesia.Controls.Add(Me.Label2)
        Me.tpCortesia.Location = New System.Drawing.Point(4, 5)
        Me.tpCortesia.Name = "tpCortesia"
        Me.tpCortesia.Size = New System.Drawing.Size(264, 237)
        Me.tpCortesia.TabIndex = 4
        Me.tpCortesia.Text = "TabPage1"
        '
        'txtCortesia
        '
        Me.txtCortesia.Location = New System.Drawing.Point(111, 17)
        Me.txtCortesia.Name = "txtCortesia"
        Me.txtCortesia.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtCortesia.Size = New System.Drawing.Size(147, 22)
        Me.txtCortesia.TabIndex = 50
        Me.txtCortesia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(6, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 30)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Número" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de cortesía"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(21, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 15)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Total a pagar"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblTotal.ForeColor = System.Drawing.Color.Black
        Me.lblTotal.Location = New System.Drawing.Point(149, 28)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(147, 23)
        Me.lblTotal.TabIndex = 48
        Me.lblTotal.Text = "$0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'SelectorFormaDePagoForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(321, 453)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tcFormulariosPago)
        Me.Controls.Add(Me.cbFormasPago)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "SelectorFormaDePagoForm"
        Me.Text = "Seleccione una forma de pago"
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.cbFormasPago, 0)
        Me.Controls.SetChildIndex(Me.tcFormulariosPago, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.lblTotal, 0)
        Me.Controls.SetChildIndex(Me.panelBotonera, 0)
        Me.panelBotonera.ResumeLayout(False)
        Me.tcFormulariosPago.ResumeLayout(False)
        Me.tpTarjeta.ResumeLayout(False)
        Me.tpTarjeta.PerformLayout()
        CType(Me.nudPagoTarjeta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPropina, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPagoEfectivo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpCortesia.ResumeLayout(False)
        Me.tpCortesia.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbFormasPago As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tcFormulariosPago As System.Windows.Forms.TabControl
    Friend WithEvents tpTarjeta As System.Windows.Forms.TabPage
    Friend WithEvents tpCortesia As System.Windows.Forms.TabPage
    Friend WithEvents btnAmericanExpress As GelButton
    Friend WithEvents btnMasterCard As GelButton
    Friend WithEvents btnVisa As GelButton
    Friend WithEvents lblPropinaTarjeta As System.Windows.Forms.Label
    Friend WithEvents lblPagoEfectivo As System.Windows.Forms.Label
    Friend WithEvents lblPagoTarjeta As System.Windows.Forms.Label
    Friend WithEvents mtxtNumeroOperacion As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents nudPropina As CurrencyUpDown
    Friend WithEvents nudPagoEfectivo As CurrencyUpDown
    Friend WithEvents nudPagoTarjeta As CurrencyUpDown
    Friend WithEvents txtCortesia As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents mtxtTarjeta As MaskedTextBox
    Friend WithEvents Label5 As Label
End Class
