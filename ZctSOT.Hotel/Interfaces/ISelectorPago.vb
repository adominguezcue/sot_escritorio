﻿Namespace Interfaces
    Public Interface ISelectorPago
        ReadOnly Property ProcesoExitoso As Boolean
        ReadOnly Property FormasPagoSeleccionadas As List(Of Modelo.Dtos.DtoInformacionPago)
        ReadOnly Property PropinaGenerada As Modelo.Entidades.Propina
        Sub MostrarEnDialogo(owner As System.Windows.Forms.IWin32Window)
    End Interface
End Namespace
