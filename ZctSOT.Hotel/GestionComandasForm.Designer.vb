﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GestionComandasForm
    Inherits FormBase

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.flpComandas = New System.Windows.Forms.FlowLayoutPanel()
        Me.timerCargaComandas = New System.Windows.Forms.Timer(Me.components)
        Me.panelBotonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Location = New System.Drawing.Point(0, 447)
        Me.panelBotonera.Size = New System.Drawing.Size(954, 84)
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(844, 2)
        Me.btnAceptar.Visible = False
        '
        'flpComandas
        '
        Me.flpComandas.AutoScroll = True
        Me.flpComandas.BackColor = System.Drawing.Color.Transparent
        Me.flpComandas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flpComandas.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpComandas.Location = New System.Drawing.Point(0, 0)
        Me.flpComandas.Margin = New System.Windows.Forms.Padding(0)
        Me.flpComandas.Name = "flpComandas"
        Me.flpComandas.Size = New System.Drawing.Size(954, 447)
        Me.flpComandas.TabIndex = 0
        '
        'timerCargaComandas
        '
        Me.timerCargaComandas.Interval = 15000
        '
        'GestionComandasForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(954, 531)
        Me.Controls.Add(Me.flpComandas)
        Me.Name = "GestionComandasForm"
        Me.Padding = New System.Windows.Forms.Padding(0, 0, 0, 84)
        Me.Text = "Gestión de comandas"
        Me.Controls.SetChildIndex(Me.flpComandas, 0)
        Me.Controls.SetChildIndex(Me.panelBotonera, 0)
        Me.panelBotonera.ResumeLayout(False)
        Me.ResumeLayout(False)

End Sub
    Friend WithEvents flpComandas As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents timerCargaComandas As System.Windows.Forms.Timer
End Class
