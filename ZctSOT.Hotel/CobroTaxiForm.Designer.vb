﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CobroTaxiForm
    Inherits FormBase

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panelValets = New FixedHeaderPanel()
        Me.flpDisponibles = New System.Windows.Forms.FlowLayoutPanel()
        Me.panelBotonera.SuspendLayout()
        Me.panelValets.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Location = New System.Drawing.Point(0, 296)
        Me.panelBotonera.Size = New System.Drawing.Size(266, 84)
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(160, 2)
        '
        'panelValets
        '
        Me.panelValets.Controls.Add(Me.flpDisponibles)
        Me.panelValets.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.panelValets.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.panelValets.Location = New System.Drawing.Point(12, 15)
        Me.panelValets.Name = "panelValets"
        Me.panelValets.Size = New System.Drawing.Size(242, 267)
        Me.panelValets.TabIndex = 39
        Me.panelValets.Text = "Indique el valet que cobra"
        '
        'flpDisponibles
        '
        Me.flpDisponibles.AutoScroll = True
        Me.flpDisponibles.Location = New System.Drawing.Point(3, 27)
        Me.flpDisponibles.Margin = New System.Windows.Forms.Padding(0)
        Me.flpDisponibles.Name = "flpDisponibles"
        Me.flpDisponibles.Size = New System.Drawing.Size(236, 237)
        Me.flpDisponibles.TabIndex = 0
        '
        'CobroTaxiForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(266, 380)
        Me.Controls.Add(Me.panelValets)
        Me.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "CobroTaxiForm"
        Me.Text = "Cobro de taxi"
        Me.Controls.SetChildIndex(Me.panelValets, 0)
        Me.Controls.SetChildIndex(Me.panelBotonera, 0)
        Me.panelBotonera.ResumeLayout(False)
        Me.panelValets.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelValets As FixedHeaderPanel
    Friend WithEvents flpDisponibles As System.Windows.Forms.FlowLayoutPanel
End Class
