﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CreacionOrdenTaxiForm
    Inherits FormBase

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.nudPrecio = New System.Windows.Forms.NumericUpDown()
        Me.lblPrecio = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.panelBotonera.SuspendLayout()
        CType(Me.nudPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Location = New System.Drawing.Point(0, 175)
        Me.panelBotonera.Size = New System.Drawing.Size(275, 84)
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(167, 2)
        '
        'nudPrecio
        '
        Me.nudPrecio.DecimalPlaces = 2
        Me.nudPrecio.Location = New System.Drawing.Point(174, 90)
        Me.nudPrecio.Maximum = New Decimal(New Integer() {10000000, 0, 0, 0})
        Me.nudPrecio.Name = "nudPrecio"
        Me.nudPrecio.Size = New System.Drawing.Size(92, 22)
        Me.nudPrecio.TabIndex = 8
        Me.nudPrecio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudPrecio.ThousandsSeparator = True
        '
        'lblPrecio
        '
        Me.lblPrecio.Location = New System.Drawing.Point(99, 90)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(73, 22)
        Me.lblPrecio.TabIndex = 9
        Me.lblPrecio.Text = "Importe"
        Me.lblPrecio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ZctSOT.Hotel.My.Resources.Resources.icono_taxi
        Me.PictureBox1.Location = New System.Drawing.Point(14, 64)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(75, 74)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'CreacionOrdenTaxiForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(275, 259)
        Me.Controls.Add(Me.lblPrecio)
        Me.Controls.Add(Me.nudPrecio)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "CreacionOrdenTaxiForm"
        Me.Text = "Taxi"
        Me.Controls.SetChildIndex(Me.PictureBox1, 0)
        Me.Controls.SetChildIndex(Me.nudPrecio, 0)
        Me.Controls.SetChildIndex(Me.lblPrecio, 0)
        Me.Controls.SetChildIndex(Me.panelBotonera, 0)
        Me.panelBotonera.ResumeLayout(False)
        CType(Me.nudPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents nudPrecio As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPrecio As System.Windows.Forms.Label
End Class
