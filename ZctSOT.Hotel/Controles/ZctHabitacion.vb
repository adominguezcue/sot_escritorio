﻿Imports Modelo
Imports System.Linq
Imports System.ComponentModel
Imports Modelo.Entidades

Public Class ZctHabitacion

    Dim habitacion As Habitacion

    Public Event Recargar(sender As Object, e As EventArgs)


    Public ReadOnly Property HabitacionInterna() As Habitacion
        Get
            Return habitacion
        End Get
    End Property

    <Editor>
    Public Property RecargaAutomatica As Boolean
        Get
            Return timerRecarga.Enabled
        End Get
        Set(value As Boolean)
            timerRecarga.Enabled = value
        End Set
    End Property

    <Editor>
    Public Property IntervaloRecarga As Integer
        Get
            Return timerRecarga.Interval
        End Get
        Set(value As Integer)
            timerRecarga.Interval = value
        End Set
    End Property

    'Dim _intervaloRecarga As Integer = 30000

    'Dim timerRecarga As Timer = New Timer()
    'Dim timerResaltar As Timer = New Timer()

    Dim incrementoAlpha As Integer = 1

    Private bloqueo() As Boolean = {False}

    Public Property Congelar As Boolean
        Get
            SyncLock bloqueo
                Return bloqueo(0)
            End SyncLock
        End Get
        Set(value As Boolean)
            SyncLock bloqueo
                bloqueo(0) = value
            End SyncLock
        End Set
    End Property

    Private Sub timerRecarga_Tick(sender As Object, e As EventArgs) Handles timerRecarga.Tick
        If Not Congelar Then
            RecargarHabitacion()
        End If
    End Sub

    Private Sub timerResaltar_Tick(sender As Object, e As EventArgs) Handles timerResaltar.Tick
        If Alpha = 255 OrElse Alpha = 0 Then
            incrementoAlpha = -incrementoAlpha
        End If

        Alpha += incrementoAlpha

    End Sub

    Public Sub New(ByRef h As Habitacion)

        MyBase.New()
        InitializeComponent()
        CargarInformacion(h)
    End Sub

    Private Sub CargarInformacion(ByRef h As Habitacion)
        timerResaltar.Stop()
        Alpha = 0

        lbl_numero.Text = h.NumeroHabitacion.ToString()
        lbl_tipo.Text = h.TipoHabitacion.Descripcion
        lbl_detalle.Text = String.Empty

        Select Case h.EstadoHabitacion
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Libre
                pb_estado.Image = My.Resources.estado_icono_habilitada
                CustomColor = ColorTranslator.FromHtml("#38d731")

                Dim tiempo As TimeSpan = (DateTime.Now - h.FechaModificacion)
                lbl_detalle.Text = tiempo.ToString("hh\:mm")

                timerRecarga.Start()
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada
                pb_estado.Image = My.Resources.estado_icono_preparada
                CustomColor = ColorTranslator.FromHtml("#38d731")
                lbl_detalle.Text = "Preparada"
                timerRecarga.Stop()
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia
                pb_estado.Image = My.Resources.estado_icono_sucia
                CustomColor = ColorTranslator.FromHtml("#f2d017")

                Dim controlador As New Controladores.ControladorTareasLimpieza()

                Dim ultimaLimpieza As TareaLimpieza = controlador.ObtenerTareaActualConEmpleados(h.Id)

                If Not IsNothing(ultimaLimpieza) Then
                    Dim tiempo As TimeSpan = (DateTime.Now - ultimaLimpieza.FechaCreacion)
                    lbl_detalle.Text = tiempo.ToString("hh\:mm")
                End If


                timerRecarga.Start()

            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada
                pb_estado.Image = My.Resources.estado_icono_ocupada
                CustomColor = ColorTranslator.FromHtml("#ea927e")

                Dim controlador As New Controladores.ControladorRentas()

                Dim ultimaRenta As Renta = controlador.ObtenerRentaActualPorHabitacion(h.Id)

                If Not IsNothing(ultimaRenta) Then

                    Dim fechaActual = DateTime.Now

                    If (ultimaRenta.TiemposExtra.Any(Function(m)
                                                         Return m.Activa
                                                     End Function)) Then
                        fechaActual = fechaActual.AddMinutes(-ultimaRenta.TiemposExtra.Where(Function(m)
                                                                                                 Return m.Activa
                                                                                             End Function).Sum(Function(m)
                                                                                                                   Return m.Minutos
                                                                                                               End Function))
                    End If

                    Dim auto = ultimaRenta.RentaAutomoviles.FirstOrDefault(Function(x) x.Activa)

                    If IsNothing(auto) Then
                        lbl_tipo.Text = "A pie"
                    Else
                        lbl_tipo.Text = auto.Matricula
                    End If

                    If controlador.ObtenerMaximoEstadoComandasPendientes(ultimaRenta.Id).HasValue Then
                        pb_estado.Image = My.Resources.estado_icono_comanda
                    End If

                    Dim tiempo As TimeSpan = (ultimaRenta.FechaFin - fechaActual)

                    If tiempo.TotalSeconds <= 0 Then
                        pb_estado.Image = My.Resources.estado_icono_ocupada_exceso
                        lbl_detalle.Text = "Vencida"

                        timerResaltar.Start()
                    Else
                        lbl_detalle.Text = tiempo.ToString("d\.hh\:mm")
                    End If

                End If

                timerRecarga.Start()

            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Reservada
                pb_estado.Image = My.Resources.estado_icono_reservada
                CustomColor = ColorTranslator.FromHtml("#dabfff")
                timerRecarga.Stop()
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Supervision
                pb_estado.Image = My.Resources.estado_icono_revision
                CustomColor = ColorTranslator.FromHtml("#94bffe")

                Dim controlador As New Controladores.ControladorTareasLimpieza

                Dim detalles As Modelo.Dtos.DtoDetallesTarea = controlador.ObtenerDetallesTarea(h.Id)

                If Not IsNothing(detalles) Then
                    lbl_tipo.Text = detalles.Detalle
                    lbl_detalle.Text = detalles.Tiempo.ToString("d\.hh\:mm")
                End If

                timerRecarga.Start()
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza
                pb_estado.Image = My.Resources.estado_icono_limpieza
                CustomColor = ColorTranslator.FromHtml("#94bffe")

                Dim controlador As New Controladores.ControladorTareasLimpieza

                Dim detalles As Modelo.Dtos.DtoDetallesTarea = controlador.ObtenerDetallesTarea(h.Id)

                If Not IsNothing(detalles) Then
                    lbl_tipo.Text = detalles.Detalle
                    lbl_detalle.Text = detalles.Tiempo.ToString("d\.hh\:mm")
                End If

                timerRecarga.Start()

            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Mantenimiento
                pb_estado.Image = My.Resources.estado_icono_mantenimiento
                CustomColor = ColorTranslator.FromHtml("#cacaca")

                Dim controlador As New Controladores.ControladorGlobal()
                Dim ultimoMantenimiento As Modelo.Entidades.TareaMantenimiento = controlador.AplicacionServicioTareasMantenimiento.ObtenerTareaActual(h.Id, controlador.UsuarioActual)

                lbl_detalle.Text = ultimoMantenimiento.Descripcion

                timerRecarga.Stop()
                'Case Modelo.Habitacion.EstadosHabitacion.Comanda
                '    pb_estado.Image = My.Resources.estado_icono_ocupada
                '    BackgroundColor1 = ColorTranslator.FromHtml("#ea927e")
                '    timerRecarga.Start()
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Bloqueada
                pb_estado.Image = My.Resources.estado_icono_bloqueada
                CustomColor = ColorTranslator.FromHtml("#dabfff")

                Dim controlador As New Controladores.ControladorHabitaciones()
                Dim ultimoBloqueo As Modelo.Entidades.BloqueoHabitacion = controlador.ObtenerBloqueoActual(h.Id)

                lbl_detalle.Text = ultimoBloqueo.Motivo

                timerRecarga.Stop()
        End Select



        Me.habitacion = h
    End Sub

    Private Sub superClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pb_estado.Click, lbl_tipo.Click, lbl_numero.Click, lbl_detalle.Click
        Me.OnClick(e)
    End Sub

    Sub RecargarHabitacion()

        If habitacion IsNot Nothing Then

            Dim controlador As New Controladores.ControladorHabitaciones

            Dim h = controlador.ObtenerHabitacion(habitacion.Id)
            CargarInformacion(h)

            RaiseEvent Recargar(Me, EventArgs.Empty)
        End If
    End Sub

    Protected Overrides Sub OnPaint(e As PaintEventArgs)
        'MyBase.OnPaint(e)
    End Sub

End Class
