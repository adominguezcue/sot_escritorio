﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctHabitaciones
    Inherits System.Windows.Forms.UserControl

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.ShadowPanel1 = New ExtraComponents.ShadowPanel()
        Me.panelIzquierdo = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnActivarVCard = New ExtraComponents.GelButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.panelReloj = New System.Windows.Forms.Panel()
        Me.lblReloj = New ExtraComponents.CurveLabel()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.btnTaxi = New ExtraComponents.GelButton()
        Me.btnFajilla = New ExtraComponents.GelButton()
        Me.panelBotones = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnMenu = New ExtraComponents.GelButton()
        Me.btnReservacion = New ExtraComponents.GelButton()
        Me.btnDeshabilitar = New ExtraComponents.GelButton()
        Me.btnMantenimiento = New ExtraComponents.GelButton()
        Me.btnEntradaHabitacion = New ExtraComponents.GelButton()
        Me.btnVPoints = New ExtraComponents.GelButton()
        Me.btnRestaurante = New ExtraComponents.GelButton()
        Me.btnHorasExtra = New ExtraComponents.GelButton()
        Me.btnSalida = New ExtraComponents.GelButton()
        Me.btnEstadisticaRec = New ExtraComponents.GelButton()
        Me.btnReportarMatricula = New ExtraComponents.GelButton()
        Me.btnCambio = New ExtraComponents.GelButton()
        Me.btnLiberarHabitacion = New ExtraComponents.GelButton()
        Me.btnHistorialMantenimento = New ExtraComponents.GelButton()
        Me.btnEnviarLimpieza = New ExtraComponents.GelButton()
        Me.btnMantenimientoTerminado = New ExtraComponents.GelButton()
        Me.btnFacturar = New ExtraComponents.GelButton()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        Me.panelHabitaciones = New System.Windows.Forms.TableLayoutPanel()
        Me.contenedor = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.panelIzquierdo.SuspendLayout()
        Me.panelReloj.SuspendLayout()
        Me.panelBotones.SuspendLayout()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.contenedor.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 530.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.ShadowPanel1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.panelIzquierdo, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pbLogo, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1021, 195)
        Me.TableLayoutPanel1.TabIndex = 5
        '
        'ShadowPanel1
        '
        Me.ShadowPanel1.AutoSize = True
        Me.ShadowPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ShadowPanel1.BackColor = System.Drawing.Color.Transparent
        Me.ShadowPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ShadowPanel1.Location = New System.Drawing.Point(1015, 0)
        Me.ShadowPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShadowPanel1.Name = "ShadowPanel1"
        Me.ShadowPanel1.Padding = New System.Windows.Forms.Padding(3)
        Me.ShadowPanel1.Size = New System.Drawing.Size(6, 195)
        Me.ShadowPanel1.TabIndex = 4
        '
        'panelIzquierdo
        '
        Me.panelIzquierdo.AutoScroll = True
        Me.panelIzquierdo.BackColor = System.Drawing.Color.Transparent
        Me.panelIzquierdo.Controls.Add(Me.Label2)
        Me.panelIzquierdo.Controls.Add(Me.btnActivarVCard)
        Me.panelIzquierdo.Controls.Add(Me.Label1)
        Me.panelIzquierdo.Controls.Add(Me.Panel1)
        Me.panelIzquierdo.Controls.Add(Me.panelReloj)
        Me.panelIzquierdo.Controls.Add(Me.lblUsuario)
        Me.panelIzquierdo.Controls.Add(Me.btnTaxi)
        Me.panelIzquierdo.Controls.Add(Me.btnFajilla)
        Me.panelIzquierdo.Controls.Add(Me.panelBotones)
        Me.panelIzquierdo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelIzquierdo.Location = New System.Drawing.Point(0, 0)
        Me.panelIzquierdo.Margin = New System.Windows.Forms.Padding(0)
        Me.panelIzquierdo.Name = "panelIzquierdo"
        Me.panelIzquierdo.Size = New System.Drawing.Size(530, 195)
        Me.panelIzquierdo.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Lato", 35.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(454, 50)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "V Motel Boutique"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnActivarVCard
        '
        Me.btnActivarVCard.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnActivarVCard.BackColor = System.Drawing.Color.White
        Me.btnActivarVCard.ButtonRectangleOrigin = New System.Drawing.Point(0, 0)
        Me.btnActivarVCard.Checkable = False
        Me.btnActivarVCard.Checked = False
        Me.btnActivarVCard.DisableHighlight = False
        Me.btnActivarVCard.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnActivarVCard.GelColor = System.Drawing.Color.White
        Me.btnActivarVCard.GradientBottom = System.Drawing.Color.White
        Me.btnActivarVCard.GradientTop = System.Drawing.Color.White
        Me.btnActivarVCard.Image = Global.ZctSOT.Hotel.My.Resources.Resources.tarjetaV_activar
        Me.btnActivarVCard.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnActivarVCard.ImageLocation = New System.Drawing.Point(0, 0)
        Me.btnActivarVCard.ImageSize = New System.Drawing.Size(70, 50)
        Me.btnActivarVCard.Location = New System.Drawing.Point(460, 0)
        Me.btnActivarVCard.Margin = New System.Windows.Forms.Padding(0)
        Me.btnActivarVCard.Name = "btnActivarVCard"
        Me.btnActivarVCard.Size = New System.Drawing.Size(70, 50)
        Me.btnActivarVCard.TabIndex = 21
        Me.btnActivarVCard.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Lato", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.ForeColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label1.Location = New System.Drawing.Point(3, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(454, 23)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Suites & Villas"
        Me.Label1.UseMnemonic = False
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(0, 73)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(473, 34)
        Me.Panel1.TabIndex = 6
        '
        'panelReloj
        '
        Me.panelReloj.BackColor = System.Drawing.Color.Transparent
        Me.panelReloj.Controls.Add(Me.lblReloj)
        Me.panelReloj.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.panelReloj.Location = New System.Drawing.Point(0, 107)
        Me.panelReloj.Margin = New System.Windows.Forms.Padding(0)
        Me.panelReloj.Name = "panelReloj"
        Me.panelReloj.Size = New System.Drawing.Size(257, 60)
        Me.panelReloj.TabIndex = 17
        '
        'lblReloj
        '
        Me.lblReloj.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.lblReloj.BorderSize = 2
        Me.lblReloj.BordersWidth = 10
        Me.lblReloj.Font = New System.Drawing.Font("Lato", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblReloj.Location = New System.Drawing.Point(10, 0)
        Me.lblReloj.Margin = New System.Windows.Forms.Padding(20, 0, 30, 0)
        Me.lblReloj.MinimumSize = New System.Drawing.Size(40, 40)
        Me.lblReloj.Name = "lblReloj"
        Me.lblReloj.Size = New System.Drawing.Size(217, 60)
        Me.lblReloj.TabIndex = 0
        Me.lblReloj.Text = "Fecha" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Hora"
        Me.lblReloj.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUsuario
        '
        Me.lblUsuario.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblUsuario.ImageAlign = System.Drawing.ContentAlignment.BottomRight
        Me.lblUsuario.Location = New System.Drawing.Point(257, 107)
        Me.lblUsuario.Margin = New System.Windows.Forms.Padding(0)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(122, 60)
        Me.lblUsuario.TabIndex = 18
        Me.lblUsuario.Text = "SISTEMAS"
        Me.lblUsuario.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnTaxi
        '
        Me.btnTaxi.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnTaxi.BackColor = System.Drawing.Color.Transparent
        Me.btnTaxi.ButtonRectangleOrigin = New System.Drawing.Point(0, 0)
        Me.btnTaxi.Checkable = False
        Me.btnTaxi.Checked = False
        Me.btnTaxi.DisableHighlight = True
        Me.btnTaxi.Font = New System.Drawing.Font("Lato", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnTaxi.ForeColor = System.Drawing.Color.Red
        Me.btnTaxi.GelColor = System.Drawing.Color.White
        Me.btnTaxi.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(154, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnTaxi.GradientTop = System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(154, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.btnTaxi.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnTaxi.ImageLocation = New System.Drawing.Point(0, 0)
        Me.btnTaxi.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnTaxi.Location = New System.Drawing.Point(382, 107)
        Me.btnTaxi.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.btnTaxi.Name = "btnTaxi"
        Me.btnTaxi.Size = New System.Drawing.Size(60, 60)
        Me.btnTaxi.TabIndex = 19
        Me.btnTaxi.Text = "TAXI" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "0"
        Me.btnTaxi.UseVisualStyleBackColor = False
        '
        'btnFajilla
        '
        Me.btnFajilla.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnFajilla.BackColor = System.Drawing.Color.Transparent
        Me.btnFajilla.ButtonRectangleOrigin = New System.Drawing.Point(0, 0)
        Me.btnFajilla.Checkable = False
        Me.btnFajilla.Checked = False
        Me.btnFajilla.DisableHighlight = True
        Me.btnFajilla.Font = New System.Drawing.Font("Lato", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnFajilla.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnFajilla.GelColor = System.Drawing.Color.White
        Me.btnFajilla.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnFajilla.GradientTop = System.Drawing.Color.FromArgb(CType(CType(21, Byte), Integer), CType(CType(125, Byte), Integer), CType(CType(46, Byte), Integer))
        Me.btnFajilla.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnFajilla.ImageLocation = New System.Drawing.Point(0, 0)
        Me.btnFajilla.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnFajilla.Location = New System.Drawing.Point(448, 107)
        Me.btnFajilla.Margin = New System.Windows.Forms.Padding(3, 0, 3, 0)
        Me.btnFajilla.Name = "btnFajilla"
        Me.btnFajilla.Size = New System.Drawing.Size(60, 60)
        Me.btnFajilla.TabIndex = 20
        Me.btnFajilla.Text = "FAJILLA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "0"
        Me.btnFajilla.UseVisualStyleBackColor = False
        '
        'panelBotones
        '
        Me.panelBotones.AutoScroll = True
        Me.panelBotones.BackColor = System.Drawing.Color.Transparent
        Me.panelBotones.Controls.Add(Me.btnMenu)
        Me.panelBotones.Controls.Add(Me.btnReservacion)
        Me.panelBotones.Controls.Add(Me.btnDeshabilitar)
        Me.panelBotones.Controls.Add(Me.btnMantenimiento)
        Me.panelBotones.Controls.Add(Me.btnEntradaHabitacion)
        Me.panelBotones.Controls.Add(Me.btnVPoints)
        Me.panelBotones.Controls.Add(Me.btnRestaurante)
        Me.panelBotones.Controls.Add(Me.btnHorasExtra)
        Me.panelBotones.Controls.Add(Me.btnSalida)
        Me.panelBotones.Controls.Add(Me.btnEstadisticaRec)
        Me.panelBotones.Controls.Add(Me.btnReportarMatricula)
        Me.panelBotones.Controls.Add(Me.btnCambio)
        Me.panelBotones.Controls.Add(Me.btnLiberarHabitacion)
        Me.panelBotones.Controls.Add(Me.btnHistorialMantenimento)
        Me.panelBotones.Controls.Add(Me.btnEnviarLimpieza)
        Me.panelBotones.Controls.Add(Me.btnMantenimientoTerminado)
        Me.panelBotones.Controls.Add(Me.btnFacturar)
        Me.panelBotones.Location = New System.Drawing.Point(1, 168)
        Me.panelBotones.Margin = New System.Windows.Forms.Padding(1)
        Me.panelBotones.Name = "panelBotones"
        Me.panelBotones.Size = New System.Drawing.Size(507, 86)
        Me.panelBotones.TabIndex = 3
        '
        'btnMenu
        '
        Me.btnMenu.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMenu.BackColor = System.Drawing.Color.White
        Me.btnMenu.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnMenu.Checkable = False
        Me.btnMenu.Checked = False
        Me.btnMenu.DisableHighlight = False
        Me.btnMenu.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnMenu.GelColor = System.Drawing.Color.White
        Me.btnMenu.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnMenu.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnMenu.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_menu
        Me.btnMenu.ImageLocation = New System.Drawing.Point(6, 0)
        Me.btnMenu.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnMenu.Location = New System.Drawing.Point(3, 3)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(60, 80)
        Me.btnMenu.TabIndex = 0
        Me.btnMenu.Text = "MENÚ"
        Me.btnMenu.UseVisualStyleBackColor = False
        '
        'btnReservacion
        '
        Me.btnReservacion.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnReservacion.BackColor = System.Drawing.Color.White
        Me.btnReservacion.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnReservacion.Checkable = False
        Me.btnReservacion.Checked = False
        Me.btnReservacion.DisableHighlight = False
        Me.btnReservacion.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnReservacion.GelColor = System.Drawing.Color.White
        Me.btnReservacion.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnReservacion.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnReservacion.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_reservacion
        Me.btnReservacion.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnReservacion.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnReservacion.Location = New System.Drawing.Point(69, 3)
        Me.btnReservacion.Name = "btnReservacion"
        Me.btnReservacion.Size = New System.Drawing.Size(104, 80)
        Me.btnReservacion.TabIndex = 1
        Me.btnReservacion.Text = "RESERVACIÓN"
        Me.btnReservacion.UseVisualStyleBackColor = False
        '
        'btnDeshabilitar
        '
        Me.btnDeshabilitar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnDeshabilitar.BackColor = System.Drawing.Color.White
        Me.btnDeshabilitar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnDeshabilitar.Checkable = False
        Me.btnDeshabilitar.Checked = False
        Me.btnDeshabilitar.DisableHighlight = False
        Me.btnDeshabilitar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnDeshabilitar.GelColor = System.Drawing.Color.White
        Me.btnDeshabilitar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnDeshabilitar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnDeshabilitar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_deshabilitar
        Me.btnDeshabilitar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnDeshabilitar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnDeshabilitar.Location = New System.Drawing.Point(179, 3)
        Me.btnDeshabilitar.Name = "btnDeshabilitar"
        Me.btnDeshabilitar.Size = New System.Drawing.Size(104, 80)
        Me.btnDeshabilitar.TabIndex = 2
        Me.btnDeshabilitar.Text = "DESHABILITAR"
        Me.btnDeshabilitar.UseVisualStyleBackColor = False
        '
        'btnMantenimiento
        '
        Me.btnMantenimiento.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMantenimiento.BackColor = System.Drawing.Color.White
        Me.btnMantenimiento.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnMantenimiento.Checkable = False
        Me.btnMantenimiento.Checked = False
        Me.btnMantenimiento.DisableHighlight = False
        Me.btnMantenimiento.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnMantenimiento.GelColor = System.Drawing.Color.White
        Me.btnMantenimiento.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnMantenimiento.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnMantenimiento.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_mantenimiento
        Me.btnMantenimiento.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnMantenimiento.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnMantenimiento.Location = New System.Drawing.Point(289, 3)
        Me.btnMantenimiento.Name = "btnMantenimiento"
        Me.btnMantenimiento.Size = New System.Drawing.Size(104, 80)
        Me.btnMantenimiento.TabIndex = 3
        Me.btnMantenimiento.Text = "MANTENIMIENTO"
        Me.btnMantenimiento.UseVisualStyleBackColor = False
        '
        'btnEntradaHabitacion
        '
        Me.btnEntradaHabitacion.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnEntradaHabitacion.BackColor = System.Drawing.Color.White
        Me.btnEntradaHabitacion.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnEntradaHabitacion.Checkable = False
        Me.btnEntradaHabitacion.Checked = False
        Me.btnEntradaHabitacion.DisableHighlight = False
        Me.btnEntradaHabitacion.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnEntradaHabitacion.GelColor = System.Drawing.Color.White
        Me.btnEntradaHabitacion.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnEntradaHabitacion.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnEntradaHabitacion.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_entrada
        Me.btnEntradaHabitacion.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEntradaHabitacion.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnEntradaHabitacion.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnEntradaHabitacion.Location = New System.Drawing.Point(399, 3)
        Me.btnEntradaHabitacion.Name = "btnEntradaHabitacion"
        Me.btnEntradaHabitacion.Size = New System.Drawing.Size(104, 80)
        Me.btnEntradaHabitacion.TabIndex = 4
        Me.btnEntradaHabitacion.Text = "ENTRADA DE HAB"
        Me.btnEntradaHabitacion.UseVisualStyleBackColor = False
        '
        'btnVPoints
        '
        Me.btnVPoints.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnVPoints.BackColor = System.Drawing.Color.White
        Me.btnVPoints.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnVPoints.Checkable = False
        Me.btnVPoints.Checked = False
        Me.btnVPoints.DisableHighlight = False
        Me.btnVPoints.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnVPoints.GelColor = System.Drawing.Color.White
        Me.btnVPoints.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnVPoints.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnVPoints.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnVPoints.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnVPoints.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnVPoints.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnVPoints.Location = New System.Drawing.Point(3, 89)
        Me.btnVPoints.Name = "btnVPoints"
        Me.btnVPoints.Size = New System.Drawing.Size(104, 80)
        Me.btnVPoints.TabIndex = 5
        Me.btnVPoints.Text = "V POINTS"
        Me.btnVPoints.UseVisualStyleBackColor = False
        '
        'btnRestaurante
        '
        Me.btnRestaurante.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnRestaurante.BackColor = System.Drawing.Color.White
        Me.btnRestaurante.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnRestaurante.Checkable = False
        Me.btnRestaurante.Checked = False
        Me.btnRestaurante.DisableHighlight = False
        Me.btnRestaurante.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnRestaurante.GelColor = System.Drawing.Color.White
        Me.btnRestaurante.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnRestaurante.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnRestaurante.Image = Global.ZctSOT.Hotel.My.Resources.Resources.barra_icono_restaurante
        Me.btnRestaurante.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnRestaurante.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnRestaurante.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnRestaurante.Location = New System.Drawing.Point(113, 89)
        Me.btnRestaurante.Name = "btnRestaurante"
        Me.btnRestaurante.Size = New System.Drawing.Size(104, 80)
        Me.btnRestaurante.TabIndex = 6
        Me.btnRestaurante.Text = "RESTAURANTE"
        Me.btnRestaurante.UseVisualStyleBackColor = False
        '
        'btnHorasExtra
        '
        Me.btnHorasExtra.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnHorasExtra.BackColor = System.Drawing.Color.White
        Me.btnHorasExtra.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnHorasExtra.Checkable = False
        Me.btnHorasExtra.Checked = False
        Me.btnHorasExtra.DisableHighlight = False
        Me.btnHorasExtra.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnHorasExtra.GelColor = System.Drawing.Color.White
        Me.btnHorasExtra.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnHorasExtra.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnHorasExtra.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnHorasExtra.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnHorasExtra.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnHorasExtra.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnHorasExtra.Location = New System.Drawing.Point(223, 89)
        Me.btnHorasExtra.Name = "btnHorasExtra"
        Me.btnHorasExtra.Size = New System.Drawing.Size(104, 80)
        Me.btnHorasExtra.TabIndex = 7
        Me.btnHorasExtra.Text = "GESTIONAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "RENTA"
        Me.btnHorasExtra.UseVisualStyleBackColor = False
        '
        'btnSalida
        '
        Me.btnSalida.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSalida.BackColor = System.Drawing.Color.White
        Me.btnSalida.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnSalida.Checkable = False
        Me.btnSalida.Checked = False
        Me.btnSalida.DisableHighlight = False
        Me.btnSalida.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnSalida.GelColor = System.Drawing.Color.White
        Me.btnSalida.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnSalida.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnSalida.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnSalida.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnSalida.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnSalida.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnSalida.Location = New System.Drawing.Point(333, 89)
        Me.btnSalida.Name = "btnSalida"
        Me.btnSalida.Size = New System.Drawing.Size(104, 80)
        Me.btnSalida.TabIndex = 8
        Me.btnSalida.Text = "SALIDA"
        Me.btnSalida.UseVisualStyleBackColor = False
        '
        'btnEstadisticaRec
        '
        Me.btnEstadisticaRec.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnEstadisticaRec.BackColor = System.Drawing.Color.White
        Me.btnEstadisticaRec.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnEstadisticaRec.Checkable = False
        Me.btnEstadisticaRec.Checked = False
        Me.btnEstadisticaRec.DisableHighlight = False
        Me.btnEstadisticaRec.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnEstadisticaRec.GelColor = System.Drawing.Color.White
        Me.btnEstadisticaRec.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnEstadisticaRec.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnEstadisticaRec.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnEstadisticaRec.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEstadisticaRec.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnEstadisticaRec.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnEstadisticaRec.Location = New System.Drawing.Point(3, 175)
        Me.btnEstadisticaRec.Name = "btnEstadisticaRec"
        Me.btnEstadisticaRec.Size = New System.Drawing.Size(104, 80)
        Me.btnEstadisticaRec.TabIndex = 9
        Me.btnEstadisticaRec.Text = "ESTADISTICAS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "DE RECAMARERAS"
        Me.btnEstadisticaRec.UseVisualStyleBackColor = False
        '
        'btnReportarMatricula
        '
        Me.btnReportarMatricula.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnReportarMatricula.BackColor = System.Drawing.Color.White
        Me.btnReportarMatricula.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnReportarMatricula.Checkable = False
        Me.btnReportarMatricula.Checked = False
        Me.btnReportarMatricula.DisableHighlight = False
        Me.btnReportarMatricula.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnReportarMatricula.GelColor = System.Drawing.Color.White
        Me.btnReportarMatricula.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnReportarMatricula.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnReportarMatricula.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnReportarMatricula.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnReportarMatricula.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnReportarMatricula.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnReportarMatricula.Location = New System.Drawing.Point(113, 175)
        Me.btnReportarMatricula.Name = "btnReportarMatricula"
        Me.btnReportarMatricula.Size = New System.Drawing.Size(104, 80)
        Me.btnReportarMatricula.TabIndex = 10
        Me.btnReportarMatricula.Text = "REPORTAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MATRÍCULA"
        Me.btnReportarMatricula.UseVisualStyleBackColor = False
        '
        'btnCambio
        '
        Me.btnCambio.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnCambio.BackColor = System.Drawing.Color.White
        Me.btnCambio.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCambio.Checkable = False
        Me.btnCambio.Checked = False
        Me.btnCambio.DisableHighlight = False
        Me.btnCambio.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCambio.GelColor = System.Drawing.Color.White
        Me.btnCambio.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCambio.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCambio.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnCambio.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCambio.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnCambio.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCambio.Location = New System.Drawing.Point(223, 175)
        Me.btnCambio.Name = "btnCambio"
        Me.btnCambio.Size = New System.Drawing.Size(104, 80)
        Me.btnCambio.TabIndex = 11
        Me.btnCambio.Text = "CAMBIO"
        Me.btnCambio.UseVisualStyleBackColor = False
        '
        'btnLiberarHabitacion
        '
        Me.btnLiberarHabitacion.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnLiberarHabitacion.BackColor = System.Drawing.Color.White
        Me.btnLiberarHabitacion.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnLiberarHabitacion.Checkable = False
        Me.btnLiberarHabitacion.Checked = False
        Me.btnLiberarHabitacion.DisableHighlight = False
        Me.btnLiberarHabitacion.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnLiberarHabitacion.GelColor = System.Drawing.Color.White
        Me.btnLiberarHabitacion.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnLiberarHabitacion.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnLiberarHabitacion.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnLiberarHabitacion.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnLiberarHabitacion.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnLiberarHabitacion.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnLiberarHabitacion.Location = New System.Drawing.Point(333, 175)
        Me.btnLiberarHabitacion.Name = "btnLiberarHabitacion"
        Me.btnLiberarHabitacion.Size = New System.Drawing.Size(104, 80)
        Me.btnLiberarHabitacion.TabIndex = 12
        Me.btnLiberarHabitacion.Text = "LIBERAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "HABITACIÓN"
        Me.btnLiberarHabitacion.UseVisualStyleBackColor = False
        '
        'btnHistorialMantenimento
        '
        Me.btnHistorialMantenimento.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnHistorialMantenimento.BackColor = System.Drawing.Color.White
        Me.btnHistorialMantenimento.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnHistorialMantenimento.Checkable = False
        Me.btnHistorialMantenimento.Checked = False
        Me.btnHistorialMantenimento.DisableHighlight = False
        Me.btnHistorialMantenimento.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnHistorialMantenimento.GelColor = System.Drawing.Color.White
        Me.btnHistorialMantenimento.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnHistorialMantenimento.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnHistorialMantenimento.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnHistorialMantenimento.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnHistorialMantenimento.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnHistorialMantenimento.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnHistorialMantenimento.Location = New System.Drawing.Point(3, 261)
        Me.btnHistorialMantenimento.Name = "btnHistorialMantenimento"
        Me.btnHistorialMantenimento.Size = New System.Drawing.Size(104, 80)
        Me.btnHistorialMantenimento.TabIndex = 13
        Me.btnHistorialMantenimento.Text = "HISTORIAL DE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MANTENIMIENTO"
        Me.btnHistorialMantenimento.UseVisualStyleBackColor = False
        '
        'btnEnviarLimpieza
        '
        Me.btnEnviarLimpieza.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnEnviarLimpieza.BackColor = System.Drawing.Color.White
        Me.btnEnviarLimpieza.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnEnviarLimpieza.Checkable = False
        Me.btnEnviarLimpieza.Checked = False
        Me.btnEnviarLimpieza.DisableHighlight = False
        Me.btnEnviarLimpieza.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnEnviarLimpieza.GelColor = System.Drawing.Color.White
        Me.btnEnviarLimpieza.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnEnviarLimpieza.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnEnviarLimpieza.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnEnviarLimpieza.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnEnviarLimpieza.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnEnviarLimpieza.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnEnviarLimpieza.Location = New System.Drawing.Point(113, 261)
        Me.btnEnviarLimpieza.Name = "btnEnviarLimpieza"
        Me.btnEnviarLimpieza.Size = New System.Drawing.Size(104, 80)
        Me.btnEnviarLimpieza.TabIndex = 14
        Me.btnEnviarLimpieza.Text = "ENVIAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A LIMPIEZA"
        Me.btnEnviarLimpieza.UseVisualStyleBackColor = False
        '
        'btnMantenimientoTerminado
        '
        Me.btnMantenimientoTerminado.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnMantenimientoTerminado.BackColor = System.Drawing.Color.White
        Me.btnMantenimientoTerminado.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnMantenimientoTerminado.Checkable = False
        Me.btnMantenimientoTerminado.Checked = False
        Me.btnMantenimientoTerminado.DisableHighlight = False
        Me.btnMantenimientoTerminado.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnMantenimientoTerminado.GelColor = System.Drawing.Color.White
        Me.btnMantenimientoTerminado.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnMantenimientoTerminado.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnMantenimientoTerminado.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnMantenimientoTerminado.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMantenimientoTerminado.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnMantenimientoTerminado.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnMantenimientoTerminado.Location = New System.Drawing.Point(223, 261)
        Me.btnMantenimientoTerminado.Name = "btnMantenimientoTerminado"
        Me.btnMantenimientoTerminado.Size = New System.Drawing.Size(104, 80)
        Me.btnMantenimientoTerminado.TabIndex = 15
        Me.btnMantenimientoTerminado.Text = "MANTENIMIENTO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TERMINADO"
        Me.btnMantenimientoTerminado.UseVisualStyleBackColor = False
        '
        'btnFacturar
        '
        Me.btnFacturar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnFacturar.BackColor = System.Drawing.Color.White
        Me.btnFacturar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnFacturar.Checkable = False
        Me.btnFacturar.Checked = False
        Me.btnFacturar.DisableHighlight = False
        Me.btnFacturar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnFacturar.GelColor = System.Drawing.Color.White
        Me.btnFacturar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnFacturar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnFacturar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnFacturar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnFacturar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnFacturar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnFacturar.Location = New System.Drawing.Point(333, 261)
        Me.btnFacturar.Name = "btnFacturar"
        Me.btnFacturar.Size = New System.Drawing.Size(104, 80)
        Me.btnFacturar.TabIndex = 16
        Me.btnFacturar.Text = "FACTURAR"
        Me.btnFacturar.UseVisualStyleBackColor = False
        '
        'pbLogo
        '
        Me.pbLogo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pbLogo.Font = New System.Drawing.Font("Lato Black", 45.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.pbLogo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.pbLogo.Image = Global.ZctSOT.Hotel.My.Resources.Resources.logo_VBoutique
        Me.pbLogo.Location = New System.Drawing.Point(530, 0)
        Me.pbLogo.Margin = New System.Windows.Forms.Padding(0)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(485, 195)
        Me.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbLogo.TabIndex = 2
        Me.pbLogo.TabStop = False
        Me.pbLogo.Text = "V Motel  Boutique"
        '
        'panelHabitaciones
        '
        Me.panelHabitaciones.AutoSize = True
        Me.panelHabitaciones.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.panelHabitaciones.BackColor = System.Drawing.Color.White
        Me.panelHabitaciones.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.panelHabitaciones.Location = New System.Drawing.Point(0, 0)
        Me.panelHabitaciones.Margin = New System.Windows.Forms.Padding(0)
        Me.panelHabitaciones.Name = "panelHabitaciones"
        Me.panelHabitaciones.Size = New System.Drawing.Size(0, 0)
        Me.panelHabitaciones.TabIndex = 6
        '
        'contenedor
        '
        Me.contenedor.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.contenedor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.contenedor.BackColor = System.Drawing.Color.Transparent
        Me.contenedor.Controls.Add(Me.panelHabitaciones)
        Me.contenedor.Location = New System.Drawing.Point(0, 195)
        Me.contenedor.Margin = New System.Windows.Forms.Padding(0)
        Me.contenedor.Name = "contenedor"
        Me.contenedor.Size = New System.Drawing.Size(1021, 568)
        Me.contenedor.TabIndex = 7
        '
        'ZctHabitaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.contenedor)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "ZctHabitaciones"
        Me.Size = New System.Drawing.Size(1024, 768)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.panelIzquierdo.ResumeLayout(False)
        Me.panelReloj.ResumeLayout(False)
        Me.panelBotones.ResumeLayout(False)
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.contenedor.ResumeLayout(False)
        Me.contenedor.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Private WithEvents panelHabitaciones As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents panelIzquierdo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents panelBotones As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnEntradaHabitacion As GelButton
    Friend WithEvents btnMantenimiento As GelButton
    Friend WithEvents btnDeshabilitar As GelButton
    Friend WithEvents btnReservacion As GelButton
    Friend WithEvents btnMenu As GelButton
    Friend WithEvents ShadowPanel1 As ShadowPanel
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents contenedor As System.Windows.Forms.Panel
    Friend WithEvents btnVPoints As GelButton
    Friend WithEvents btnRestaurante As GelButton
    Friend WithEvents btnHorasExtra As GelButton
    Friend WithEvents btnSalida As GelButton
    Friend WithEvents btnEstadisticaRec As GelButton
    Friend WithEvents btnReportarMatricula As GelButton
    Friend WithEvents btnCambio As GelButton
    Friend WithEvents btnLiberarHabitacion As GelButton
    Friend WithEvents btnHistorialMantenimento As GelButton
    Friend WithEvents btnEnviarLimpieza As GelButton
    Friend WithEvents btnMantenimientoTerminado As GelButton
    Friend WithEvents btnFacturar As GelButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents panelReloj As System.Windows.Forms.Panel
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents btnTaxi As GelButton
    Friend WithEvents btnFajilla As GelButton
    Friend WithEvents btnActivarVCard As GelButton
    Friend WithEvents lblReloj As CurveLabel
End Class
