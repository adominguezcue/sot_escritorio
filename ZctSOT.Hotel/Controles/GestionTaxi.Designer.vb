﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GestionTaxi
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panelPago = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblPrecioTaxi = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblDescuento = New System.Windows.Forms.Label()
        Me.lblCortesia = New System.Windows.Forms.Label()
        Me.lblSubtotal = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.panelBotonera = New System.Windows.Forms.Panel()
        Me.btnModificarPrecio = New ExtraComponents.GelButton()
        Me.btnCancelar = New ExtraComponents.GelButton()
        Me.btnCobrar = New ExtraComponents.GelButton()
        Me.panelPago.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.panelBotonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelPago
        '
        Me.panelPago.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelPago.Controls.Add(Me.SplitContainer1)
        Me.panelPago.Location = New System.Drawing.Point(0, 0)
        Me.panelPago.Name = "panelPago"
        Me.panelPago.Size = New System.Drawing.Size(273, 303)
        Me.panelPago.TabIndex = 3
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblTotal)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblDescuento)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblCortesia)
        Me.SplitContainer1.Panel2.Controls.Add(Me.lblSubtotal)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Size = New System.Drawing.Size(273, 303)
        Me.SplitContainer1.SplitterDistance = 199
        Me.SplitContainer1.TabIndex = 1
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label5)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.lblPrecioTaxi)
        Me.SplitContainer2.Size = New System.Drawing.Size(273, 199)
        Me.SplitContainer2.SplitterDistance = 136
        Me.SplitContainer2.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(130, 23)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "1 Taxi"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPrecioTaxi
        '
        Me.lblPrecioTaxi.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblPrecioTaxi.Location = New System.Drawing.Point(0, 0)
        Me.lblPrecioTaxi.Name = "lblPrecioTaxi"
        Me.lblPrecioTaxi.Size = New System.Drawing.Size(130, 23)
        Me.lblPrecioTaxi.TabIndex = 1
        Me.lblPrecioTaxi.Text = "$0.00"
        Me.lblPrecioTaxi.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblTotal.Location = New System.Drawing.Point(121, 81)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(141, 15)
        Me.lblTotal.TabIndex = 7
        Me.lblTotal.Text = "$0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDescuento
        '
        Me.lblDescuento.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblDescuento.Location = New System.Drawing.Point(121, 62)
        Me.lblDescuento.Name = "lblDescuento"
        Me.lblDescuento.Size = New System.Drawing.Size(141, 15)
        Me.lblDescuento.TabIndex = 6
        Me.lblDescuento.Text = "$0.00"
        Me.lblDescuento.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCortesia
        '
        Me.lblCortesia.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblCortesia.Location = New System.Drawing.Point(121, 43)
        Me.lblCortesia.Name = "lblCortesia"
        Me.lblCortesia.Size = New System.Drawing.Size(141, 15)
        Me.lblCortesia.TabIndex = 5
        Me.lblCortesia.Text = "$0.00"
        Me.lblCortesia.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSubtotal
        '
        Me.lblSubtotal.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lblSubtotal.Location = New System.Drawing.Point(121, 24)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(141, 15)
        Me.lblSubtotal.TabIndex = 4
        Me.lblSubtotal.Text = "$0.00"
        Me.lblSubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label4.Location = New System.Drawing.Point(70, 81)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 15)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Total"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label3.Location = New System.Drawing.Point(39, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Descuento"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label2.Location = New System.Drawing.Point(53, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Cortesía"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.Location = New System.Drawing.Point(51, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Subtotal"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'panelBotonera
        '
        Me.panelBotonera.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelBotonera.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.panelBotonera.Controls.Add(Me.btnModificarPrecio)
        Me.panelBotonera.Controls.Add(Me.btnCancelar)
        Me.panelBotonera.Controls.Add(Me.btnCobrar)
        Me.panelBotonera.Location = New System.Drawing.Point(0, 304)
        Me.panelBotonera.Margin = New System.Windows.Forms.Padding(0)
        Me.panelBotonera.Name = "panelBotonera"
        Me.panelBotonera.Size = New System.Drawing.Size(273, 64)
        Me.panelBotonera.TabIndex = 13
        '
        'btnModificarPrecio
        '
        Me.btnModificarPrecio.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnModificarPrecio.BackColor = System.Drawing.Color.White
        Me.btnModificarPrecio.ButtonRectangleOrigin = New System.Drawing.Point(0, 18)
        Me.btnModificarPrecio.Checkable = False
        Me.btnModificarPrecio.Checked = False
        Me.btnModificarPrecio.DisableHighlight = False
        Me.btnModificarPrecio.Font = New System.Drawing.Font("Lato", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnModificarPrecio.GelColor = System.Drawing.Color.White
        Me.btnModificarPrecio.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnModificarPrecio.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnModificarPrecio.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnModificarPrecio.ImageLocation = New System.Drawing.Point(21, 0)
        Me.btnModificarPrecio.ImageSize = New System.Drawing.Size(36, 36)
        Me.btnModificarPrecio.Location = New System.Drawing.Point(97, 2)
        Me.btnModificarPrecio.Margin = New System.Windows.Forms.Padding(0)
        Me.btnModificarPrecio.Name = "btnModificarPrecio"
        Me.btnModificarPrecio.Size = New System.Drawing.Size(78, 60)
        Me.btnModificarPrecio.TabIndex = 7
        Me.btnModificarPrecio.Text = "MODIFICAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PRECIO"
        Me.btnModificarPrecio.UseVisualStyleBackColor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.BackColor = System.Drawing.Color.White
        Me.btnCancelar.ButtonRectangleOrigin = New System.Drawing.Point(0, 18)
        Me.btnCancelar.Checkable = False
        Me.btnCancelar.Checked = False
        Me.btnCancelar.DisableHighlight = False
        Me.btnCancelar.Font = New System.Drawing.Font("Lato", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelar.GelColor = System.Drawing.Color.White
        Me.btnCancelar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCancelar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCancelar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.default_imagen
        Me.btnCancelar.ImageLocation = New System.Drawing.Point(21, 0)
        Me.btnCancelar.ImageSize = New System.Drawing.Size(36, 36)
        Me.btnCancelar.Location = New System.Drawing.Point(4, 2)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(78, 60)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "SOLICITAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CANCELACIÓN"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnCobrar
        '
        Me.btnCobrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCobrar.BackColor = System.Drawing.Color.White
        Me.btnCobrar.ButtonRectangleOrigin = New System.Drawing.Point(0, 18)
        Me.btnCobrar.Checkable = False
        Me.btnCobrar.Checked = False
        Me.btnCobrar.DisableHighlight = False
        Me.btnCobrar.Font = New System.Drawing.Font("Lato", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCobrar.GelColor = System.Drawing.Color.White
        Me.btnCobrar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCobrar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCobrar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_icono_habilitada
        Me.btnCobrar.ImageLocation = New System.Drawing.Point(21, 0)
        Me.btnCobrar.ImageSize = New System.Drawing.Size(36, 36)
        Me.btnCobrar.Location = New System.Drawing.Point(190, 2)
        Me.btnCobrar.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCobrar.Name = "btnCobrar"
        Me.btnCobrar.Size = New System.Drawing.Size(78, 60)
        Me.btnCobrar.TabIndex = 4
        Me.btnCobrar.Text = "ACEPTAR"
        Me.btnCobrar.UseVisualStyleBackColor = False
        '
        'GestionTaxi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.panelBotonera)
        Me.Controls.Add(Me.panelPago)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "GestionTaxi"
        Me.Size = New System.Drawing.Size(273, 368)
        Me.panelPago.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.panelBotonera.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelPago As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblDescuento As System.Windows.Forms.Label
    Friend WithEvents lblCortesia As System.Windows.Forms.Label
    Friend WithEvents lblSubtotal As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents panelBotonera As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As GelButton
    Friend WithEvents btnCobrar As GelButton
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblPrecioTaxi As System.Windows.Forms.Label
    Friend WithEvents btnModificarPrecio As GelButton

End Class
