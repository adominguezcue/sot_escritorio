﻿Imports System.Linq
Imports ZctSOT.Hotel.CreacionComandasForm
Imports Transversal.Extensiones

Public Class GestionComanda

    Private comandaActual As Modelo.Entidades.Comanda

    Public Event PostCobro()

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        ResumenArticuloBindingSource.DataSource = New List(Of ResumenArticulo)
    End Sub

    Public Sub CargarDetallesComanda(detalles As Modelo.Entidades.Comanda, numeroOrden As Integer)
        Dim controlador As New Controladores.ControladorGlobal

        Dim resumenes As New List(Of ResumenArticulo)

        comandaActual = detalles

        If Not IsNothing(comandaActual) Then

            For Each articuloComanda As Modelo.Entidades.ArticuloComanda In comandaActual.ArticulosComanda
                If Not articuloComanda.Activo Then
                    Continue For
                End If

                Dim nuevoResumen As New ResumenArticulo
                nuevoResumen.IdArticulo = articuloComanda.IdArticulo
                nuevoResumen.Nombre = articuloComanda.Articulo.Nombre
                nuevoResumen.Tipo = articuloComanda.Articulo.Tipo
                nuevoResumen.Cantidad = articuloComanda.Cantidad
                nuevoResumen.Precio = articuloComanda.PrecioUnidad

                resumenes.Add(nuevoResumen)
            Next

            If comandaActual.Estado = Modelo.Entidades.Comanda.Estados.Preparacion OrElse comandaActual.Estado = Modelo.Entidades.Comanda.Estados.PorEntregar Then

                Dim tiempo = Date.Now - comandaActual.FechaCreacion

                Select Case tiempo.TotalSeconds
                    Case Is < 600
                        panelPago.GradientBottom = Color.Green
                        Exit Select
                    Case Is < 900
                        panelPago.GradientBottom = Color.Yellow
                        Exit Select
                    Case Else
                        panelPago.GradientBottom = Color.Red
                        Exit Select
                End Select
            End If

            panelPago.Text = "Orden " & numeroOrden & ": " + detalles.Estado.Descripcion()
        End If
        ResumenArticuloBindingSource.DataSource = resumenes
    End Sub

    Private Sub ResumenArticuloBindingSource_DataSourceChanged(sender As Object, e As EventArgs) Handles ResumenArticuloBindingSource.DataSourceChanged, ResumenArticuloBindingSource.ListChanged
        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Or Not (TypeOf ResumenArticuloBindingSource.DataSource Is List(Of ResumenArticulo)) Then
            lblCortesia.Text = "$" + (0).ToString("#,##0.00")
            lblDescuento.Text = "$" + (0).ToString("#,##0.00")
            lblSubtotal.Text = "$" + (0).ToString("#,##0.00")
            lblTotal.Text = "$" + (0).ToString("#,##0.00")
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))

            Dim subtotal As Decimal
            Dim cortesia As Decimal = 0
            Dim descuento As Decimal = 0
            If (origenes.Count > 0) Then
                subtotal = origenes.Sum(Function(m)
                                            Return m.Total
                                        End Function)

            Else
                subtotal = 0
            End If

            lblSubtotal.Text = "$" + subtotal.ToString("#,##0.00")
            lblCortesia.Text = "$" + cortesia.ToString("#,##0.00")
            lblDescuento.Text = "$" + descuento.ToString("#,##0.00")
            lblTotal.Text = "$" + (subtotal - cortesia - descuento).ToString("#,##0.00")

        End If
    End Sub

    Private Sub btnCobrar_Click(sender As Object, e As EventArgs) Handles btnCobrar.Click
        
        If Not IsNothing(comandaActual) Then
            Dim controladorRent As New Controladores.ControladorRentas

            'If comandaActual.Estado = Modelo.Entidades.Comanda.Estados.PorEntregar Then
            '    Dim seleccionadorMeseroF As New SeleccionadorMeseroForm
            '    seleccionadorMeseroF.ShowDialog(Me)

            '    If (seleccionadorMeseroF.ProcesoExitoso) Then
            '        controladorRent.EntregarProductos(comandaActual.Id, seleccionadorMeseroF.EmpleadoSeleccionado.Id)
            '    End If
            'Else
            If comandaActual.Estado = Modelo.Entidades.Comanda.Estados.PorCobrar Then

                Dim controladorF As New Controladores.ControladorFormasPago()

                Dim cobroF = controladorF.ObtenerSelectorPago(comandaActual.Valor) 'As New SelectorFormaPagoForm(comandaActual.Valor)

                cobroF.MostrarEnDialogo(Me)

                If cobroF.ProcesoExitoso Then
                    controladorRent.PagarComanda(comandaActual.Id, cobroF.FormasPagoSeleccionadas, cobroF.PropinaGenerada)
                End If
            Else
                Exit Sub
            End If
            RaiseEvent PostCobro()
        End If
    End Sub

    Private Sub btnSolicitarCambio_Click(sender As Object, e As EventArgs) Handles btnSolicitarCambio.Click

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Dim motivoF As New CapturaMotivo
        motivoF.Motivo = "cancelación"
        motivoF.TextoAuxiliar = "Detalles"

        Dim eh As New CapturaMotivo.AceptarEventHandler(Sub()
                                                            If Not IsNothing(comandaActual) Then
                                                                Dim controlador As New Controladores.ControladorRentas
                                                                controlador.CancelarComanda(comandaActual.Id, motivoF.DescripcionMotivo)
                                                            End If

                                                            RaiseEvent PostCobro()
                                                        End Sub)

        Try
            AddHandler motivoF.Aceptar, eh

            motivoF.ShowDialog(Me)
        Finally
            RemoveHandler motivoF.Aceptar, eh
        End Try
    End Sub
End Class
