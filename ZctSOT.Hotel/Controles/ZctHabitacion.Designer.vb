﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ZctHabitacion
    Inherits ExtraComponents.GradientPanel

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Requerido para la compatibilidad con el Diseñador de composiciones de clases Windows.Forms
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'El Diseñador de componentes requiere esta llamada.
        InitializeComponent()

    End Sub

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.timerRecarga = New System.Windows.Forms.Timer(Me.components)
        Me.timerResaltar = New System.Windows.Forms.Timer(Me.components)
        Me.lbl_tipo = New System.Windows.Forms.Label()
        Me.pb_estado = New System.Windows.Forms.PictureBox()
        Me.lbl_detalle = New System.Windows.Forms.Label()
        Me.lbl_numero = New System.Windows.Forms.Label()
        CType(Me.pb_estado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'timerRecarga
        '
        Me.timerRecarga.Interval = 30000
        '
        'timerResaltar
        '
        Me.timerResaltar.Interval = 5
        '
        'lbl_tipo
        '
        Me.lbl_tipo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_tipo.BackColor = System.Drawing.Color.Transparent
        Me.lbl_tipo.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lbl_tipo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.lbl_tipo.Location = New System.Drawing.Point(0, 35)
        Me.lbl_tipo.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_tipo.Name = "lbl_tipo"
        Me.lbl_tipo.Size = New System.Drawing.Size(96, 12)
        Me.lbl_tipo.TabIndex = 14
        Me.lbl_tipo.Text = "Tipo H"
        Me.lbl_tipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pb_estado
        '
        Me.pb_estado.BackColor = System.Drawing.Color.Transparent
        Me.pb_estado.Location = New System.Drawing.Point(6, 0)
        Me.pb_estado.Margin = New System.Windows.Forms.Padding(0)
        Me.pb_estado.Name = "pb_estado"
        Me.pb_estado.Size = New System.Drawing.Size(32, 32)
        Me.pb_estado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pb_estado.TabIndex = 9
        Me.pb_estado.TabStop = False
        '
        'lbl_detalle
        '
        Me.lbl_detalle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_detalle.BackColor = System.Drawing.Color.Transparent
        Me.lbl_detalle.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.lbl_detalle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.lbl_detalle.Location = New System.Drawing.Point(0, 51)
        Me.lbl_detalle.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_detalle.Name = "lbl_detalle"
        Me.lbl_detalle.Size = New System.Drawing.Size(96, 21)
        Me.lbl_detalle.TabIndex = 15
        Me.lbl_detalle.Text = "Detalle"
        Me.lbl_detalle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lbl_numero
        '
        Me.lbl_numero.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl_numero.AutoSize = True
        Me.lbl_numero.BackColor = System.Drawing.Color.Transparent
        Me.lbl_numero.Font = New System.Drawing.Font("Lato Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel)
        Me.lbl_numero.Location = New System.Drawing.Point(68, 14)
        Me.lbl_numero.Margin = New System.Windows.Forms.Padding(0)
        Me.lbl_numero.Name = "lbl_numero"
        Me.lbl_numero.Size = New System.Drawing.Size(28, 15)
        Me.lbl_numero.TabIndex = 13
        Me.lbl_numero.Text = "000"
        Me.lbl_numero.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ZctHabitacion
        '
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.lbl_tipo)
        Me.Controls.Add(Me.pb_estado)
        Me.Controls.Add(Me.lbl_detalle)
        Me.Controls.Add(Me.lbl_numero)
        Me.CustomColor = System.Drawing.Color.Black
        Me.CustomGradient = True
        Me.GradientTop = 21
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Size = New System.Drawing.Size(96, 72)
        CType(Me.pb_estado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents pb_estado As System.Windows.Forms.PictureBox
    'Friend WithEvents panelGradiente As GradientPanel
    Private WithEvents lbl_detalle As System.Windows.Forms.Label
    Private WithEvents lbl_tipo As System.Windows.Forms.Label
    Private WithEvents lbl_numero As System.Windows.Forms.Label
    Friend WithEvents timerRecarga As System.Windows.Forms.Timer
    Friend WithEvents timerResaltar As System.Windows.Forms.Timer

End Class
