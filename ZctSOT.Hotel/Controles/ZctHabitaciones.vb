﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms
Imports Modelo
Imports Modelo.Dtos
Imports ZctSOT.Hotel.Controladores
Imports Transversal.Excepciones
Imports System.Transactions
Imports ExtraComponents
Imports Modelo.Entidades

Public Class ZctHabitaciones

    Dim uc_habitaciones As List(Of ZctHabitacion)

    Dim datosHabitacionForm As DatosHabitacionForm
    Dim panelHabitacionesX As Integer
    Dim panelHabitacionesY As Integer

    Dim habitacionSeleccionada As ZctHabitacion
    Dim diccionarioBotones As Dictionary(Of Habitacion.EstadosHabitacion, List(Of GelButton))
    Dim menuP As MenuPrincipal

    Dim _intervaloRecarga As Integer = 30000

    Dim timerRecarga As System.Windows.Forms.Timer = New System.Windows.Forms.Timer()

    Private Sub timerRecarga_Tick(sender As Object, e As EventArgs)
        Dim hora As Date = DateTime.Now
        lblReloj.Text = hora.ToString("dddd, dd \de MMMM, yyyy") + System.Environment.NewLine + System.Environment.NewLine + hora.ToString("hh\:mm tt")
    End Sub

    Public Sub New()

        InitializeComponent()

        panelHabitacionesX = 0
        panelHabitacionesY = 0

        uc_habitaciones = New List(Of ZctHabitacion)

        SetStyle(ControlStyles.SupportsTransparentBackColor, True)

        datosHabitacionForm = New DatosHabitacionForm()
        datosHabitacionForm.Visible = True
        ShadowPanel1.Controls.Add(datosHabitacionForm)

        datosHabitacionForm.Visible = False

        AddHandler datosHabitacionForm.VisibleChanged, AddressOf datos_VisibleChanged

        diccionarioBotones = New Dictionary(Of Habitacion.EstadosHabitacion, List(Of GelButton))

        diccionarioBotones(Habitacion.EstadosHabitacion.Bloqueada) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Bloqueada).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Bloqueada).Add(btnReservacion)
        diccionarioBotones(Habitacion.EstadosHabitacion.Bloqueada).Add(btnDeshabilitar)
        diccionarioBotones(Habitacion.EstadosHabitacion.Bloqueada).Add(btnMantenimiento)
        diccionarioBotones(Habitacion.EstadosHabitacion.Bloqueada).Add(btnEntradaHabitacion)
        'diccionarioBotones(Habitacion.EstadosHabitacion.Comanda) = New List(Of GelButton)
        'diccionarioBotones(Habitacion.EstadosHabitacion.Comanda).Add(btnMenu)
        'diccionarioBotones(Habitacion.EstadosHabitacion.Comanda).Add(btnVPoints)
        'diccionarioBotones(Habitacion.EstadosHabitacion.Comanda).Add(btnRestaurante)
        'diccionarioBotones(Habitacion.EstadosHabitacion.Comanda).Add(btnHorasExtra)
        'diccionarioBotones(Habitacion.EstadosHabitacion.Comanda).Add(btnSalida)
        diccionarioBotones(Habitacion.EstadosHabitacion.Limpieza) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Limpieza).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Limpieza).Add(btnEstadisticaRec)
        diccionarioBotones(Habitacion.EstadosHabitacion.Limpieza).Add(btnReportarMatricula)
        diccionarioBotones(Habitacion.EstadosHabitacion.Limpieza).Add(btnCambio)
        diccionarioBotones(Habitacion.EstadosHabitacion.Limpieza).Add(btnLiberarHabitacion)
        diccionarioBotones(Habitacion.EstadosHabitacion.Supervision) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Supervision).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Supervision).Add(btnEstadisticaRec)
        diccionarioBotones(Habitacion.EstadosHabitacion.Supervision).Add(btnReportarMatricula)
        diccionarioBotones(Habitacion.EstadosHabitacion.Supervision).Add(btnCambio)
        diccionarioBotones(Habitacion.EstadosHabitacion.Supervision).Add(btnLiberarHabitacion)
        diccionarioBotones(Habitacion.EstadosHabitacion.Libre) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Libre).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Libre).Add(btnReservacion)
        diccionarioBotones(Habitacion.EstadosHabitacion.Libre).Add(btnDeshabilitar)
        diccionarioBotones(Habitacion.EstadosHabitacion.Libre).Add(btnMantenimiento)
        diccionarioBotones(Habitacion.EstadosHabitacion.Libre).Add(btnEntradaHabitacion)
        diccionarioBotones(Habitacion.EstadosHabitacion.Mantenimiento) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Mantenimiento).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Mantenimiento).Add(btnHistorialMantenimento)
        diccionarioBotones(Habitacion.EstadosHabitacion.Mantenimiento).Add(btnMantenimiento)
        diccionarioBotones(Habitacion.EstadosHabitacion.Mantenimiento).Add(btnEnviarLimpieza)
        diccionarioBotones(Habitacion.EstadosHabitacion.Mantenimiento).Add(btnMantenimientoTerminado)
        diccionarioBotones(Habitacion.EstadosHabitacion.Ocupada) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Ocupada).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Ocupada).Add(btnVPoints)
        diccionarioBotones(Habitacion.EstadosHabitacion.Ocupada).Add(btnRestaurante)
        diccionarioBotones(Habitacion.EstadosHabitacion.Ocupada).Add(btnHorasExtra)
        diccionarioBotones(Habitacion.EstadosHabitacion.Ocupada).Add(btnSalida)
        diccionarioBotones(Habitacion.EstadosHabitacion.Preparada) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Preparada).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Preparada).Add(btnReservacion)
        diccionarioBotones(Habitacion.EstadosHabitacion.Preparada).Add(btnDeshabilitar)
        diccionarioBotones(Habitacion.EstadosHabitacion.Preparada).Add(btnMantenimiento)
        diccionarioBotones(Habitacion.EstadosHabitacion.Preparada).Add(btnEntradaHabitacion)
        diccionarioBotones(Habitacion.EstadosHabitacion.Sucia) = New List(Of GelButton)
        diccionarioBotones(Habitacion.EstadosHabitacion.Sucia).Add(btnMenu)
        diccionarioBotones(Habitacion.EstadosHabitacion.Sucia).Add(btnEstadisticaRec)
        diccionarioBotones(Habitacion.EstadosHabitacion.Sucia).Add(btnReportarMatricula)
        diccionarioBotones(Habitacion.EstadosHabitacion.Sucia).Add(btnFacturar)
        diccionarioBotones(Habitacion.EstadosHabitacion.Sucia).Add(btnEnviarLimpieza)

        diccionarioBotones(Habitacion.EstadosHabitacion.Reservada) = New List(Of GelButton)

        panelBotones.Controls.Clear()
        panelBotones.Visible = False

        menuP = New MenuPrincipal()

        panelIzquierdo.AutoScroll = False

        timerRecarga_Tick(Nothing, Nothing)
        ActualizarHabitaciones()

        timerRecarga.Interval = _intervaloRecarga
        AddHandler timerRecarga.Tick, AddressOf timerRecarga_Tick
        timerRecarga.Start()
    End Sub

    Public Sub ActualizarHabitaciones()

        Dim controlador As New ControladorHabitaciones

        Dim habitaciones As List(Of Habitacion) = controlador.ObtenerActivasConTipos()

        datosHabitacionForm.Hide()

        panelHabitaciones.SuspendLayout()

        panelHabitaciones.Controls.Clear()
        panelHabitaciones.RowStyles.Clear()
        panelHabitaciones.ColumnStyles.Clear()

        For Each uc_habitacion As ZctHabitacion In uc_habitaciones
            RemoveHandler uc_habitacion.Click, AddressOf habitacion_Click
            RemoveHandler uc_habitacion.Recargar, AddressOf CargarHabitacion
            uc_habitacion.Dispose()
        Next

        uc_habitaciones.Clear()

        If habitaciones.Count > 0 Then
            For i As Integer = 1 To habitaciones.Max(Function(m) m.Posicion)
                panelHabitaciones.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 98))
            Next

            Dim pisoMaximo = habitaciones.Max(Function(m) m.Piso)

            For i As Integer = 1 To pisoMaximo
                panelHabitaciones.RowStyles.Add(New RowStyle(SizeType.Absolute, 76))
            Next
            For Each habitacion As Habitacion In habitaciones

                Dim nh As ZctHabitacion = New ZctHabitacion(habitacion)

                uc_habitaciones.Add(nh)
                nh.Margin = New Padding(1, 2, 1, 2)

                AddHandler uc_habitaciones.Last().Click, AddressOf habitacion_Click
                AddHandler uc_habitaciones.Last().Recargar, AddressOf CargarHabitacion

                panelHabitaciones.Controls.Add(nh, habitacion.Posicion - 1, pisoMaximo - habitacion.Piso)
            Next
            panelHabitaciones.ResumeLayout()
        End If
    End Sub

    Private Sub habitacion_Click(sender As Object, e As EventArgs)

        habitacionSeleccionada = TryCast(sender, ZctHabitacion)
        CargarHabitacion(sender, e)
    End Sub

    Private Sub CargarHabitacion(sender As Object, e As EventArgs)

        If (IsNothing(sender) AndAlso IsNothing(habitacionSeleccionada)) Or (Not IsNothing(sender) AndAlso sender.Equals(habitacionSeleccionada)) Then

            MostrarBotonesPorEstado()

            If habitacionSeleccionada Is Nothing Then
                Return
            End If

            Dim controlador As New ControladorHabitaciones

            Dim habitacion As DtoDatosHabitacion = controlador.ObtenerDatosHabitacion(habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)

            datosHabitacionForm.ActualizarDatos(habitacion)

            datosHabitacionForm.Show()
        End If
    End Sub

    Private Sub datos_VisibleChanged(sender As Object, e As EventArgs)
        If datosHabitacionForm.Visible Then
            panelBotones.Visible = True

            panelReloj.Visible = False
            lblUsuario.Visible = False
            btnTaxi.Visible = False
            btnFajilla.Visible = False
        Else

            habitacionSeleccionada = Nothing
            panelBotones.Visible = False

            panelReloj.Visible = True
            lblUsuario.Visible = True
            btnTaxi.Visible = True
            btnFajilla.Visible = True

            'For Each c As Control In panelBotones.Controls
            '    c.Visible = False
            'Next

        End If

        pbLogo.Visible = Not datosHabitacionForm.Visible

        ShadowPanel1.Invalidate()
    End Sub

    Private Sub btnReservacion_Click(sender As Object, e As EventArgs) Handles btnReservacion.Click

    End Sub

    Private Sub MostrarBotonesPorEstado()
        panelBotones.SuspendLayout()

        panelBotones.Controls.Clear()
        'For Each c As Control In
        '    c.Visible = False
        'Next

        If Not IsNothing(habitacionSeleccionada) Then

            For Each c As Control In diccionarioBotones(habitacionSeleccionada.HabitacionInterna.EstadoHabitacion)
                panelBotones.Controls.Add(c)
            Next
        End If

        panelBotones.ResumeLayout()
    End Sub

    Private Sub panelHabitaciones_MouseDown(sender As Object, e As MouseEventArgs) Handles panelHabitaciones.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            panelHabitacionesX = e.Location.X
            panelHabitacionesY = e.Location.Y
        End If
    End Sub

    Private Sub panelHabitaciones_MouseMove(sender As Object, e As MouseEventArgs) Handles panelHabitaciones.MouseMove

        If e.Button = Windows.Forms.MouseButtons.Left Then


            Dim yDistance As Integer = panelHabitacionesY - e.Location.Y
            Dim xDistance As Integer = panelHabitacionesX - e.Location.X

            If xDistance = 0 And yDistance = 0 Then
                Return
            End If

            If panelHabitaciones.Height <= panelHabitaciones.Parent.ClientRectangle.Height Then
                yDistance = 0
            ElseIf yDistance < 0 Then
                If panelHabitaciones.Location.Y - yDistance <= 0 Then
                    yDistance = panelHabitaciones.Location.Y - yDistance
                Else
                    yDistance = 0
                    'panelHabitacionesY = yDistance
                End If
            ElseIf yDistance > 0 Then
                If panelHabitaciones.Location.Y + panelHabitaciones.Height - yDistance >= panelHabitaciones.Parent.ClientRectangle.Height Then
                    yDistance = panelHabitaciones.Location.Y - yDistance
                Else
                    yDistance = panelHabitaciones.Parent.ClientRectangle.Height - panelHabitaciones.Height
                    'panelHabitacionesY = yDistance
                End If
            End If

            If panelHabitaciones.Width <= panelHabitaciones.Parent.ClientRectangle.Width Then
                xDistance = 0
            ElseIf xDistance < 0 Then
                If panelHabitaciones.Location.X - xDistance <= 0 Then
                    xDistance = panelHabitaciones.Location.X - xDistance
                Else
                    xDistance = 0
                    'panelHabitacionesX = xDistance
                End If
            ElseIf xDistance > 0 Then
                If panelHabitaciones.Location.X + panelHabitaciones.Width - xDistance >= panelHabitaciones.Parent.ClientRectangle.Width Then
                    xDistance = panelHabitaciones.Location.X - xDistance
                Else
                    xDistance = panelHabitaciones.Parent.ClientRectangle.Width - panelHabitaciones.Width
                    'panelHabitacionesX = xDistance
                End If
            End If

            panelHabitaciones.Location = New Point(xDistance, yDistance)
        End If

    End Sub

    Private Sub btnEntrada_Click(sender As Object, e As EventArgs) Handles btnEntradaHabitacion.Click

        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre _
        Or habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Bloqueada Then
            Dim controlador As New Controladores.ControladorHabitaciones()
            controlador.PrepararHabitacion(habitacionSeleccionada.HabitacionInterna.Id)

            habitacionSeleccionada.RecargarHabitacion()
        ElseIf habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Preparada Then
            Dim rentasForm As RentasForm = New RentasForm()

            rentasForm.ActualizarHabitacion(habitacionSeleccionada.HabitacionInterna, ZctSOT.Hotel.RentasForm.Modos.Creacion)

            rentasForm.ShowDialog(Me)

            habitacionSeleccionada.RecargarHabitacion()
        End If
    End Sub

    Private Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        menuP.ShowDialog(Me)
    End Sub

    Private Sub btnActivarVCard_Click(sender As Object, e As EventArgs) Handles btnActivarVCard.Click

        Dim activacionVF As ActivacionTarjetaVFrom = New ActivacionTarjetaVFrom()
        activacionVF.ShowDialog(Me)
    End Sub

    Private Sub btnDeshabilitar_Click(sender As Object, e As EventArgs) Handles btnDeshabilitar.Click

        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion <> Habitacion.EstadosHabitacion.Libre Then
            Throw New SOTException(My.Resources.Errores.habitacion_no_libre_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
        End If

        Dim capturaMotivoForm As CapturaMotivo = New CapturaMotivo()
        capturaMotivoForm.Motivo = My.Resources.Acciones.Bloqueo
        capturaMotivoForm.TextoAuxiliar = String.Format(My.Resources.Etiquetas.RentasForm_InformacionHabitacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion, habitacionSeleccionada.HabitacionInterna.NombreTipo)

        Dim eh As CapturaMotivo.AceptarEventHandler = Sub()
                                                          Dim controlador As New ControladorHabitaciones()
                                                          controlador.BloquearHabitacion(habitacionSeleccionada.HabitacionInterna.Id, capturaMotivoForm.DescripcionMotivo)
                                                          habitacionSeleccionada.RecargarHabitacion()
                                                      End Sub

        AddHandler capturaMotivoForm.Aceptar, eh

        Try
            capturaMotivoForm.ShowDialog(Me)
        Finally
            RemoveHandler capturaMotivoForm.Aceptar, eh
        End Try
    End Sub

    Private Sub btnMantenimiento_Click(sender As Object, e As EventArgs) Handles btnMantenimiento.Click

        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre Then

            Dim capturaMotivoForm As CapturaMotivo = New CapturaMotivo()
            capturaMotivoForm.Motivo = My.Resources.Acciones.Mantenimiento
            capturaMotivoForm.TextoAuxiliar = String.Format(My.Resources.Etiquetas.RentasForm_InformacionHabitacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion, habitacionSeleccionada.HabitacionInterna.NombreTipo)

            Dim eh As CapturaMotivo.AceptarEventHandler = Sub()
                                                              Dim controlador As New Controladores.ControladorHabitaciones()
                                                              controlador.PonerEnMantenimiento(habitacionSeleccionada.HabitacionInterna.Id, capturaMotivoForm.DescripcionMotivo)
                                                              habitacionSeleccionada.RecargarHabitacion()
                                                          End Sub

            AddHandler capturaMotivoForm.Aceptar, eh

            Try
                capturaMotivoForm.ShowDialog(Me)
            Finally
                RemoveHandler capturaMotivoForm.Aceptar, eh
            End Try

        ElseIf habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Mantenimiento Then
            Dim capturaMotivoForm As CapturaMotivo = New CapturaMotivo()
            capturaMotivoForm.Motivo = My.Resources.Acciones.Mantenimiento
            capturaMotivoForm.TextoAuxiliar = String.Format(My.Resources.Etiquetas.RentasForm_InformacionHabitacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion, habitacionSeleccionada.HabitacionInterna.NombreTipo)
            capturaMotivoForm.SoloLectura = True

            Dim mantenimiento As TareaMantenimiento = New Controladores.ControladorGlobal().AplicacionServicioTareasMantenimiento.ObtenerTareaActual(habitacionSeleccionada.HabitacionInterna.Id, New Controladores.ControladorGlobal().UsuarioActual)

            If IsNothing(mantenimiento) Then

            Else
                capturaMotivoForm.DescripcionMotivo = mantenimiento.Descripcion
            End If

            capturaMotivoForm.ShowDialog(Me)
        Else
            Throw New SOTException(My.Resources.Errores.habitacion_no_libre_para_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
        End If
    End Sub

    Private Sub btnHistorialMantenimento_Click(sender As Object, e As EventArgs) Handles btnHistorialMantenimento.Click
        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        Dim historialMantenimientoF As HistorialMantenimientoForm = New HistorialMantenimientoForm()
        historialMantenimientoF.BuscarHistorial(habitacionSeleccionada.HabitacionInterna.Id)

        historialMantenimientoF.ShowDialog(Me)
    End Sub

    Private Sub btnMantenimientoTerminado_Click(sender As Object, e As EventArgs) Handles btnMantenimientoTerminado.Click

        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Mantenimiento Then
            'Dim limpiezaF As LimpiezaForm = New LimpiezaForm()
            'limpiezaF.IdHabitacion = habitacionSeleccionada.HabitacionInterna.Id
            ''limpiezaF.Motivo = My.Resources.Acciones.Mantenimiento
            ''limpiezaF.TextoAuxiliar = String.Format(My.Resources.Etiquetas.RentasForm_InformacionHabitacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion, habitacionSeleccionada.HabitacionInterna.NombreTipo)
            ''limpiezaF.SoloLectura = True

            'limpiezaF.ShowDialog(Me)

            If MessageBox.Show(String.Format(My.Resources.Mensajes.finalizar_mantenimiento, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim controlador As New Controladores.ControladorHabitaciones()
                controlador.FinalizarMantenimiento(habitacionSeleccionada.HabitacionInterna.Id)

                habitacionSeleccionada.RecargarHabitacion()
            End If
        Else
            Throw New SOTException(My.Resources.Errores.habitacion_no_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
        End If
    End Sub

    Private Sub btnEnviarLimpieza_Click(sender As Object, e As EventArgs) Handles btnEnviarLimpieza.Click

        Try
            habitacionSeleccionada.Congelar = True

            Dim options As New TransactionOptions
            options.IsolationLevel = Transactions.IsolationLevel.ReadCommitted
            options.Timeout = TransactionManager.DefaultTimeout

            Using scope As New TransactionScope(TransactionScopeOption.Required, options)
                If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Mantenimiento Then
                    If MessageBox.Show(String.Format(My.Resources.Mensajes.finalizar_mantenimiento, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        Dim controlador As New Controladores.ControladorHabitaciones()
                        controlador.FinalizarMantenimiento(habitacionSeleccionada.HabitacionInterna.Id)
                        habitacionSeleccionada.RecargarHabitacion()
                    Else
                        Exit Sub
                    End If
                End If

                If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Sucia Then


                    Dim selectorRecamareras As New SeleccionadorRecamarerasForm(habitacionSeleccionada.HabitacionInterna)

                    selectorRecamareras.ShowDialog(Me)

                    If selectorRecamareras.ProcesoExitoso Then
                        scope.Complete()
                    End If

                Else
                    Throw New SOTException(My.Resources.Errores.habitacion_no_sucia_o_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
                End If
            End Using
        Finally
            habitacionSeleccionada.Congelar = False
            habitacionSeleccionada.RecargarHabitacion()
        End Try
    End Sub

    Private Sub btnLiberarHabitacion_Click(sender As Object, e As EventArgs) Handles btnLiberarHabitacion.Click

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Limpieza Then

            Dim selectorSupervisor As New SeleccionadorSupervisor(habitacionSeleccionada.HabitacionInterna)

            selectorSupervisor.ShowDialog(Me)

            If selectorSupervisor.ProcesoExitoso Then
                habitacionSeleccionada.RecargarHabitacion()
            End If

        ElseIf habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Supervision Then

            Dim capturaMotivoForm As CapturaMotivo = New CapturaMotivo()
            capturaMotivoForm.Motivo = String.Format(My.Resources.Acciones.Liberacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
            capturaMotivoForm.TextoAuxiliar = My.Resources.TitulosComponentes.Detalles

            Dim eh As CapturaMotivo.AceptarEventHandler = Sub()
                                                              Dim controlador As New Controladores.ControladorTareasLimpieza()
                                                              controlador.FinalizarLimpieza(habitacionSeleccionada.HabitacionInterna.Id, capturaMotivoForm.DescripcionMotivo)
                                                              habitacionSeleccionada.RecargarHabitacion()
                                                          End Sub

            AddHandler capturaMotivoForm.Aceptar, eh

            Try
                capturaMotivoForm.ShowDialog(Me)
            Finally
                RemoveHandler capturaMotivoForm.Aceptar, eh
            End Try
        End If
    End Sub

    Private Sub btnRestaurante_Click(sender As Object, e As EventArgs) Handles btnRestaurante.Click
        If IsNothing(habitacionSeleccionada) Then
            Throw New SOTException(My.Resources.Errores.habitacion_no_seleccionada_exception)
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion <> Habitacion.EstadosHabitacion.Ocupada Then
            Throw New SOTException(My.Resources.Errores.habitacion_no_ocupada_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
        End If

        Dim controlador As New ControladorRentas

        Dim rentaActual As Modelo.Entidades.Renta = controlador.ObtenerRentaActualPorHabitacion(habitacionSeleccionada.HabitacionInterna.Id)

        If controlador.ObtenerMaximoEstadoComandasPendientes(rentaActual.Id) Then

            Dim comandasF As New GestionComandasForm(rentaActual.Id)
            comandasF.ShowDialog(Me)
        Else
            Dim comandasF As New CreacionComandasForm(rentaActual.Id)
            comandasF.ShowDialog(Me)
        End If
        habitacionSeleccionada.RecargarHabitacion()

    End Sub

    Private Sub btnSalida_Click(sender As Object, e As EventArgs) Handles btnSalida.Click
        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Ocupada Then

            If MessageBox.Show(String.Format(My.Resources.Mensajes.finalizar_renta, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim controlador As New Controladores.ControladorHabitaciones()
                controlador.FinalizarOcupacion(habitacionSeleccionada.HabitacionInterna.Id)

                habitacionSeleccionada.RecargarHabitacion()
            End If
        Else
            Throw New SOTException(My.Resources.Errores.habitacion_no_ocupada_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
        End If
    End Sub

    Private Sub btnTaxi_Click(sender As Object, e As EventArgs) Handles btnTaxi.Click
        Dim gestionTaxisF As New GestionTaxisForm

        gestionTaxisF.ShowDialog(Me)

        Dim controlador As New ControladorTaxis

        Dim cantidadTaxis As Integer = controlador.ObtenerCantidadOrdenesPendientes()

        btnTaxi.Text = "TAXI" + Environment.NewLine & cantidadTaxis
    End Sub

    Private Sub ZctHabitaciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim controlador As New ControladorTaxis

        Dim cantidadTaxis As Integer = controlador.ObtenerCantidadOrdenesPendientes()

        btnTaxi.Text = "TAXI" + Environment.NewLine & cantidadTaxis
    End Sub

    Private Sub btnCambio_Click(sender As Object, e As EventArgs) Handles btnCambio.Click
        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Limpieza Then
            Dim selectorRecamareraF As New SeleccionadorRecamarerasForm(habitacionSeleccionada.HabitacionInterna)

            selectorRecamareraF.ShowDialog(Me)

            If (selectorRecamareraF.ProcesoExitoso) Then
                habitacionSeleccionada.RecargarHabitacion()
            End If

        ElseIf habitacionSeleccionada.HabitacionInterna.EstadoHabitacion = Habitacion.EstadosHabitacion.Supervision Then
            Dim selectorSupervisor As New SeleccionadorSupervisor(habitacionSeleccionada.HabitacionInterna)

            selectorSupervisor.ShowDialog(Me)

            If selectorSupervisor.ProcesoExitoso Then
                habitacionSeleccionada.RecargarHabitacion()
            End If
        Else
            Throw New SOTException(My.Resources.Errores.habitacion_no_limpieza_o_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
        End If
    End Sub

    'Private Sub contenedor_Resize(sender As Object, e As EventArgs) Handles contenedor.Resize
    '    panelHabitaciones.Scale(New SizeF(2, 2))
    'End Sub

    Private Sub btnEstadisticaRec_Click(sender As Object, e As EventArgs) Handles btnEstadisticaRec.Click
        Dim recamarerasF As New ListaRecamarerasForm

        recamarerasF.ShowDialog(Me)
    End Sub

    Private Sub btnHorasExtra_Click(sender As Object, e As EventArgs) Handles btnHorasExtra.Click
        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        If habitacionSeleccionada.HabitacionInterna.EstadoHabitacion <> Habitacion.EstadosHabitacion.Ocupada Then
            Throw New SOTException(My.Resources.Errores.habitacion_no_ocupada_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion)
        End If

        Dim rentasForm As RentasForm = New RentasForm()

        rentasForm.ActualizarHabitacion(habitacionSeleccionada.HabitacionInterna, ZctSOT.Hotel.RentasForm.Modos.Edicion)

        rentasForm.ShowDialog(Me)

        habitacionSeleccionada.RecargarHabitacion()

    End Sub

    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click

        Dim timbra As New FacturacionElectronica.Procesa.TimbrarCFDPrueba(AddressOf TimbrarCFD)
        Dim cancela As New FacturacionElectronica.Procesa.CancelarCFDI(AddressOf CancelarCFDI)

        Dim conexion = System.Configuration.ConfigurationManager.AppSettings("cadenaConexion")

        Dim fact As New ResTotal.Vista.frmFacturacion(conexion, 1, timbra, cancela)

        fact.ShowDialog(Me)

    End Sub

    Private Function TimbrarCFD(usuario As String, password As String, xml As String) As String()
        Dim ServicioFY As New WSFY.WS_TFD_FYSoapClient
        Dim Respuesta As New WSFY.ArrayOfString

        Respuesta = ServicioFY.TimbrarCFD(usuario, password, xml, "")
        'Respuesta = ServicioFY.TimbrarCFDPrueba(usuario, password, xml)
        Return Respuesta.ToArray()
    End Function

    Private Function CancelarCFDI(ByVal usuario As String, ByVal password As String, ByVal RFC As String, ByVal listaCFDI As String(), cadena As String, password_pfx As String) As String()
        Dim ServicioFY As New WSFY.WS_TFD_FYSoapClient
        Dim Respuesta As New WSFY.ArrayOfString 'Se recibe la respuesta
        Dim lista As New WSFY.ArrayOfString
        lista.AddRange(listaCFDI)
        Respuesta = ServicioFY.CancelarCFDI(usuario, password, RFC, lista, cadena, password_pfx)
        Return listaCFDI
    End Function

    Private Sub btnReportarMatricula_Click(sender As Object, e As EventArgs) Handles btnReportarMatricula.Click
        If habitacionSeleccionada Is Nothing Then
            Throw New SOTException("Seleccione una habitación")
        End If

        Dim controlador As New Controladores.ControladorRentas

        Dim matricula As String = controlador.ObtenerAutomovilesUltimaRenta(habitacionSeleccionada.HabitacionInterna.Id).Select(Function(m) m.Matricula).FirstOrDefault()

        Dim reporteadorMatriculas As New ReportesMatriculaForm(matricula)
        reporteadorMatriculas.ShowDialog(Me)

    End Sub
End Class