﻿Public Class GestionTaxi

    Public Event PostCobro()

    Private _orden As Modelo.Entidades.OrdenTaxi

    Public Sub CargarDetalles(detalles As Modelo.Entidades.OrdenTaxi)

        _orden = detalles

        If IsNothing(_orden) Then
            lblPrecioTaxi.Text = "$" + (0).ToString("#,##0.00")
            lblSubtotal.Text = "$" + (0).ToString("#,##0.00")
            lblTotal.Text = "$" + (0).ToString("#,##0.00")
        Else
            lblPrecioTaxi.Text = "$" + detalles.Precio.ToString("#,##0.00")
            lblSubtotal.Text = "$" + detalles.Precio.ToString("#,##0.00")
            lblTotal.Text = "$" + detalles.Precio.ToString("#,##0.00")
        End If
    End Sub

    Private Sub btnCobrar_Click(sender As Object, e As EventArgs) Handles btnCobrar.Click

        Dim idOrden = 0

        If Not IsNothing(_orden) Then
            idOrden = _orden.Id
        End If

        Dim cobroF As New CobroTaxiForm(idOrden)
        cobroF.ShowDialog(Me)

        RaiseEvent PostCobro()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click


        Dim motivoF As New CapturaMotivo
        motivoF.Motivo = "cancelación"
        motivoF.TextoAuxiliar = "Detalles"

        Dim idOrden = 0

        If Not IsNothing(_orden) Then
            idOrden = _orden.Id
        End If

        Dim eh As New CapturaMotivo.AceptarEventHandler(Sub()
                                                            Dim controlador As New Controladores.ControladorTaxis
                                                            controlador.CancelarOrden(idOrden, motivoF.DescripcionMotivo)

                                                            RaiseEvent PostCobro()
                                                        End Sub)

        Try
            AddHandler motivoF.Aceptar, eh

            motivoF.ShowDialog(Me)
        Finally
            RemoveHandler motivoF.Aceptar, eh
        End Try


    End Sub

    Private Sub btnModificarPrecio_Click(sender As Object, e As EventArgs) Handles btnModificarPrecio.Click
        Dim taxisF As New CreacionOrdenTaxiForm(_orden)

        taxisF.ShowDialog(Me)

        RaiseEvent PostCobro()
    End Sub
End Class
