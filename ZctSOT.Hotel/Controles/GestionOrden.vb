﻿Imports ZctSOT.Hotel.CreacionComandasForm
Imports System.Linq
Imports Transversal.Extensiones

Public Class GestionOrden
    Private ordenActual As Modelo.Entidades.OrdenRestaurante

    Public Event PostCobro()

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        ResumenArticuloBindingSource.DataSource = New List(Of ResumenArticulo)
    End Sub

    Public Sub CargarDetallesOrden(detalles As Modelo.Entidades.OrdenRestaurante)
        Dim controlador As New Controladores.ControladorGlobal

        Dim resumenes As New List(Of ResumenArticulo)

        ordenActual = detalles

        If Not IsNothing(ordenActual) Then

            For Each articuloOrden In ordenActual.ArticulosOrdenRestaurante
                If Not articuloOrden.Activo Then
                    Continue For
                End If

                Dim nuevoResumen As New ResumenArticulo
                nuevoResumen.IdArticulo = articuloOrden.IdArticulo
                nuevoResumen.Nombre = articuloOrden.Articulo.Nombre
                nuevoResumen.Tipo = articuloOrden.Articulo.Tipo
                nuevoResumen.Cantidad = articuloOrden.Cantidad
                nuevoResumen.Precio = articuloOrden.PrecioUnidad

                resumenes.Add(nuevoResumen)
            Next

            panelPago.Text = "Orden: " + detalles.Estado.Descripcion()
        End If
        ResumenArticuloBindingSource.DataSource = resumenes
    End Sub

    Private Sub ResumenArticuloBindingSource_DataSourceChanged(sender As Object, e As EventArgs) Handles ResumenArticuloBindingSource.DataSourceChanged, ResumenArticuloBindingSource.ListChanged
        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Or Not (TypeOf ResumenArticuloBindingSource.DataSource Is List(Of ResumenArticulo)) Then
            lblCortesia.Text = "$" + (0).ToString("#,##0.00")
            lblDescuento.Text = "$" + (0).ToString("#,##0.00")
            lblSubtotal.Text = "$" + (0).ToString("#,##0.00")
            lblTotal.Text = "$" + (0).ToString("#,##0.00")
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))

            Dim subtotal As Decimal
            Dim cortesia As Decimal = 0
            Dim descuento As Decimal = 0
            If (origenes.Count > 0) Then
                subtotal = origenes.Sum(Function(m)
                                            Return m.Total
                                        End Function)

            Else
                subtotal = 0
            End If

            lblSubtotal.Text = "$" + subtotal.ToString("#,##0.00")
            lblCortesia.Text = "$" + cortesia.ToString("#,##0.00")
            lblDescuento.Text = "$" + descuento.ToString("#,##0.00")
            lblTotal.Text = "$" + (subtotal - cortesia - descuento).ToString("#,##0.00")

        End If
    End Sub

    Private Sub btnCobrar_Click(sender As Object, e As EventArgs) Handles btnCobrar.Click
        'If Not IsNothing(ordenActual) Then
        '    Dim controladorRest As New Controladores.ControladorRestaurantes

        '    If ordenActual.Estado = Modelo.Entidades.OrdenRestaurante.Estados.PorEntregar Then
        '        controladorRest.EntregarOrdenRestaurante(ordenActual.Id)
        '    End If

        'End If
        RaiseEvent PostCobro()
    End Sub

    Private Sub btnSolicitarCambio_Click(sender As Object, e As EventArgs) Handles btnSolicitarCambio.Click

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Dim motivoF As New CapturaMotivo
        motivoF.Motivo = "cancelación"
        motivoF.TextoAuxiliar = "Detalles"

        Dim eh As New CapturaMotivo.AceptarEventHandler(Sub()
                                                            If Not IsNothing(ordenActual) Then
                                                                Dim controlador As New Controladores.ControladorRestaurantes
                                                                controlador.CancelarOrden(ordenActual.Id, motivoF.DescripcionMotivo)

                                                            End If

                                                            RaiseEvent PostCobro()
                                                        End Sub)

        Try
            AddHandler motivoF.Aceptar, eh

            motivoF.ShowDialog(Me)
        Finally
            RemoveHandler motivoF.Aceptar, eh
        End Try
    End Sub
End Class
