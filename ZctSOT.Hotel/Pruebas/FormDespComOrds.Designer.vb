﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormDespComOrds
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chlbProductos = New System.Windows.Forms.CheckedListBox()
        Me.btnTodos = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnGenerarSeleccionados = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'chlbProductos
        '
        Me.chlbProductos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chlbProductos.FormattingEnabled = True
        Me.chlbProductos.Location = New System.Drawing.Point(12, 104)
        Me.chlbProductos.Name = "chlbProductos"
        Me.chlbProductos.Size = New System.Drawing.Size(405, 364)
        Me.chlbProductos.TabIndex = 0
        '
        'btnTodos
        '
        Me.btnTodos.Location = New System.Drawing.Point(13, 13)
        Me.btnTodos.Name = "btnTodos"
        Me.btnTodos.Size = New System.Drawing.Size(116, 23)
        Me.btnTodos.TabIndex = 1
        Me.btnTodos.Text = "Seleccionar todos"
        Me.btnTodos.UseVisualStyleBackColor = True
        '
        'btnActualizar
        '
        Me.btnActualizar.Location = New System.Drawing.Point(13, 71)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(116, 23)
        Me.btnActualizar.TabIndex = 2
        Me.btnActualizar.Text = "Actualizar"
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'btnGenerarSeleccionados
        '
        Me.btnGenerarSeleccionados.Location = New System.Drawing.Point(13, 42)
        Me.btnGenerarSeleccionados.Name = "btnGenerarSeleccionados"
        Me.btnGenerarSeleccionados.Size = New System.Drawing.Size(116, 23)
        Me.btnGenerarSeleccionados.TabIndex = 3
        Me.btnGenerarSeleccionados.Text = "Generar"
        Me.btnGenerarSeleccionados.UseVisualStyleBackColor = True
        '
        'FormDespComOrds
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(429, 482)
        Me.Controls.Add(Me.btnGenerarSeleccionados)
        Me.Controls.Add(Me.btnActualizar)
        Me.Controls.Add(Me.btnTodos)
        Me.Controls.Add(Me.chlbProductos)
        Me.Name = "FormDespComOrds"
        Me.Text = "Preparador de productos (solamente para pruebas)"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chlbProductos As System.Windows.Forms.CheckedListBox
    Friend WithEvents btnTodos As System.Windows.Forms.Button
    Friend WithEvents btnActualizar As System.Windows.Forms.Button
    Friend WithEvents btnGenerarSeleccionados As System.Windows.Forms.Button
End Class
