﻿Imports System.Reflection
Imports Transversal.Extensiones

Public Class FormDespComOrds

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

#If DEBUG Then
        Text += " " + Assembly.GetExecutingAssembly().GetName().Version.ToString() 'System.Configuration.ConfigurationManager.AppSettings("versionAplicacion")
#End If

    End Sub

    Public Sub CargarArticulos()
        Dim controladorRest As New Controladores.ControladorRestaurantes
        Dim controladorRent As New Controladores.ControladorRentas

        Dim articulosPreparar = controladorRent.ObtenerArticulosPrepararComandas()
        articulosPreparar.AddRange(controladorRest.ObtenerArticulosPrepararOrdenes())

        chlbProductos.DataSource = articulosPreparar
        chlbProductos.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Dtos.DtoArticuloPrepararConsulta, String)(Function(x) x.Resumen)
    End Sub

    Private Sub FormDespComOrds_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarArticulos()
    End Sub

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        CargarArticulos()
    End Sub

    Private Sub btnGenerarSeleccionados_Click(sender As Object, e As EventArgs) Handles btnGenerarSeleccionados.Click
        Dim listaComandas As New List(Of Modelo.Dtos.DtoArticuloPrepararConsulta)
        Dim listaOrdenes As New List(Of Modelo.Dtos.DtoArticuloPrepararConsulta)

        For Each item In chlbProductos.CheckedItems
            Dim articuloPreparar = CType(item, Modelo.Dtos.DtoArticuloPrepararConsulta)

            If articuloPreparar.Clasificacion = Modelo.Dtos.DtoArticuloPrepararConsulta.Clasificaciones.Comanda Then
                listaComandas.Add(articuloPreparar)
            ElseIf articuloPreparar.Clasificacion = Modelo.Dtos.DtoArticuloPrepararConsulta.Clasificaciones.OrdenRestaurante Then
                listaOrdenes.Add(articuloPreparar)
            End If
        Next

        Dim controladorRest As New Controladores.ControladorRestaurantes
        Dim controladorRent As New Controladores.ControladorRentas

        'For Each articuloComanda In listaComandas
        '    controladorRent.FinalizarElaboracionProductos(articuloComanda.IdPadre, articuloComanda.Id)
        'Next

        'For Each articuloOrden In listaOrdenes
        '    controladorRest.FinalizarElaboracionProducto(articuloOrden.Id)
        'Next

        CargarArticulos()

    End Sub

    Private Sub btnTodos_Click(sender As Object, e As EventArgs) Handles btnTodos.Click
        For i As Integer = 0 To chlbProductos.Items.Count - 1
            chlbProductos.SetItemChecked(i, True)
        Next
    End Sub
End Class