﻿Imports ZctSOT.Hotel.Controladores
Imports Transversal.Extensiones
Imports System.Linq
Imports Transversal.Dtos

Public Class ReservacionForm
    Public _procesoExitoso As Boolean = False

    Public Property ProcesoExitoso As Boolean
        Get
            Return _procesoExitoso
        End Get
        Private Set(value As Boolean)
            _procesoExitoso = value
        End Set
    End Property

    Private reservacionActual As Modelo.Entidades.Reservacion
    Private configuracionTipo As Modelo.Entidades.ConfiguracionTipo

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        Helpers.HelperDataSource.AgregarDataSource(cbTiposHabitaciones, New Controladores.ControladorGlobal().AplicacionServicioTiposHabitaciones.ObtenerTiposActivos(), _
                                                   Function(m) m.Descripcion, Function(m) m.Id)

        'cbTiposHabitaciones.DataSource = New Controladores.ControladorGlobal().AplicacionServicioTiposHabitaciones.ObtenerTiposActivos()
        'cbTiposHabitaciones.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.TipoHabitacion, String)(Function(m) m.Descripcion)
        'cbTiposHabitaciones.ValueMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.TipoHabitacion, Integer)(Function(m) m.Id)

        cbTiposHabitaciones.SelectedIndex = -1

        Dim controladorP As New Controladores.ControladorPagos

        dtpFechaEntrada.MinDate = Date.Now.Date
        dtpFechaSalida.MinDate = dtpFechaEntrada.Value.AddDays(1)

        'cbFormasPago.DataSource = [Enum].GetValues(GetType(Modelo.Entidades.PagoRenta.TiposPago)).Cast(Of Modelo.Entidades.PagoRenta.TiposPago).ToList()
        'Helpers.HelperDataSource.AgregarDataSource(cbFormasPago, controladorP.ObtenerTiposValidosConMixtos(), _
        '                                           Function(x) x.Nombre, Function(x) x.Pagos)
    End Sub

    Public Sub CargarReservacion(idReservacion As Integer)
        Dim controlador As New ControladorGlobal

        reservacionActual = controlador.AplicacionServicioReservaciones.ObtenerReservacionParaEditar(idReservacion, controlador.UsuarioActual)

        If Not IsNothing(reservacionActual) Then
            dtpFechaEntrada.MinDate = reservacionActual.FechaEntrada

            txtCodigoReserva.Value = reservacionActual.CodigoReserva
            txtNombre.Value = reservacionActual.Nombre
            txtTelefono.Value = reservacionActual.Telefono
            txtEmail.Value = reservacionActual.Email
            txtIdCliente.Value = reservacionActual.IdCliente
            dtpFechaEntrada.Value = reservacionActual.FechaEntrada
            dtpFechaSalida.Value = reservacionActual.FechaSalida
            nudNoches.Value = reservacionActual.Noches
            nudPExtra.Value = reservacionActual.PersonasExtra

            dtpFechaSalida.MinDate = dtpFechaEntrada.Value.AddDays(1)
            'reservacionActual.IdTipoHabitacion = CInt(cbTiposHabitaciones.SelectedValue)
            'reservacionActual.IdTipoPago = CInt(cbFormasPago.SelectedValue)
            nudDescuento.Value = reservacionActual.Descuento
            txtLeyendaDescuento.Value = reservacionActual.LeyendaDescuento

            Dim datoF = reservacionActual.DatosFiscales.FirstOrDefault(Function(m) m.Activo)

            If Not IsNothing(datoF) Then

                txtRfc.Value = datoF.RFC
                txtRazonSocial.Value = datoF.NombreRazonSocial
                txtCalle.Value = datoF.Calle
                txtNumExt.Value = datoF.NumeroExterior
                txtNumInt.Value = datoF.NumeroInterior
                txtColonia.Value = datoF.Colonia
                txtCiudad.Value = datoF.Ciudad
                txtEstado.Value = datoF.Estado
                txtCP.Value = datoF.CP
                txtEmailFacturacion.Value = datoF.Email
                txtObservaciones.Value = reservacionActual.Observaciones

            End If

            'cbTiposHabitaciones.SelectedValue = reservacionActual.IdTipoHabitacion
            'cbFormasPago.SelectedItem = reservacionActual.IdTipoPago
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If reservacionActual Is Nothing OrElse reservacionActual.Id = 0 Then
            reservacionActual = New Modelo.Entidades.Reservacion()
            reservacionActual.Activo = True

            For Each paquete As Modelo.Entidades.Paquete In clbPaquetes.CheckedItems
                Dim paqueteReservacion As New Modelo.Entidades.PaqueteReservacion()
                paqueteReservacion.Activo = True
                paqueteReservacion.IdPaquete = paquete.Id
                paqueteReservacion.Precio = paquete.Precio

                reservacionActual.PaquetesReservaciones.Add(paqueteReservacion)
            Next
        Else
            Dim paquetesSeleccionables As List(Of Modelo.Entidades.Paquete) = clbPaquetes.CheckedItems.Cast(Of Modelo.Entidades.Paquete)().ToList()

            For Each paqueteReservacion As Modelo.Entidades.PaqueteReservacion In reservacionActual.PaquetesReservaciones
                If (Not paquetesSeleccionables.Any(Function(m)
                                                       Return m.Id = paqueteReservacion.IdPaquete
                                                   End Function)) Then
                    paqueteReservacion.Activo = False
                End If
            Next

            For Each paqueteSeleccionable As Modelo.Entidades.Paquete In paquetesSeleccionables
                If (Not reservacionActual.PaquetesReservaciones.Any(Function(m)
                                                                        Return m.IdPaquete = paqueteSeleccionable.Id
                                                                    End Function)) Then
                    Dim paqueteHabitacion As New Modelo.Entidades.PaqueteReservacion
                    paqueteHabitacion.Activo = True
                    paqueteHabitacion.IdPaquete = paqueteSeleccionable.Id
                    paqueteHabitacion.Precio = paqueteHabitacion.Precio

                End If
            Next

        End If

        reservacionActual.CodigoReserva = txtCodigoReserva.Value
        reservacionActual.Nombre = txtNombre.Value
        reservacionActual.Telefono = txtTelefono.Value
        reservacionActual.Email = txtEmail.Value
        reservacionActual.IdCliente = txtIdCliente.Value
        reservacionActual.FechaEntrada = dtpFechaEntrada.Value
        reservacionActual.FechaSalida = dtpFechaSalida.Value
        reservacionActual.Noches = nudNoches.Value
        reservacionActual.PersonasExtra = nudPExtra.Value

        Dim datoF = reservacionActual.DatosFiscales.FirstOrDefault(Function(m) m.Activo)

        If IsNothing(datoF) Then
            datoF = New Modelo.Entidades.DatosFiscalesReservacion
            reservacionActual.DatosFiscales.Add(datoF)
        End If

        'reservacionActual.IdTipoHabitacion = CInt(cbTiposHabitaciones.SelectedValue)
        'reservacionActual.IdTipoPago = CType(cbFormasPago.SelectedValue, Modelo.Entidades.Dtos.DtoInformacionPago)
        reservacionActual.Descuento = nudDescuento.Value
        reservacionActual.LeyendaDescuento = txtLeyendaDescuento.Value

        datoF.RFC = txtRfc.Value
        datoF.NombreRazonSocial = txtRazonSocial.Value
        datoF.Calle = txtCalle.Value
        datoF.NumeroExterior = txtNumExt.Value
        datoF.NumeroInterior = txtNumInt.Value
        datoF.Colonia = txtColonia.Value
        datoF.Ciudad = txtCiudad.Value
        datoF.Estado = txtEstado.Value
        datoF.CP = txtCP.Value
        datoF.Email = txtEmailFacturacion.Value
        reservacionActual.Observaciones = txtObservaciones.Value

        'Ejecutar aqui alguna acción para crear la reservación

        If reservacionActual.Id = 0 Then
            Dim controlador As New ControladorGlobal
            controlador.AplicacionServicioReservaciones.CrearReservacion(reservacionActual, controlador.UsuarioActual)
        Else
            Dim controlador As New ControladorGlobal
            controlador.AplicacionServicioReservaciones.ModificarReservacion(reservacionActual, controlador.UsuarioActual)
        End If

        ProcesoExitoso = True
        Me.Close()
    End Sub

    Private Sub cbTiposHabitaciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTiposHabitaciones.SelectedIndexChanged

        For i As Integer = 0 To clbPaquetes.Items.Count - 1
            clbPaquetes.SetItemChecked(i, False)
        Next


        If cbTiposHabitaciones.SelectedIndex >= 0 Then
            Dim controlador As New Controladores.ControladorGlobal
            Dim controladorCT As New ControladorConfiguracionesTipo

            Dim idTipoSeleccionado As Integer = CInt(cbTiposHabitaciones.SelectedValue)

            Dim tarifaElegida = Modelo.Entidades.ConfiguracionTarifa.Tarifas.Motel

            configuracionTipo = controladorCT.ObtenerConfiguracionTipo(idTipoSeleccionado, tarifaElegida)

            Helpers.HelperDataSource.AgregarDataSource(clbPaquetes, controlador.AplicacionServicioPaquetes.ObtenerPaquetesPorTipoHabitacion(idTipoSeleccionado), _
                                                       Function(m) m.Nombre, Function(m) m.Id)


        ElseIf Not IsNothing(clbPaquetes.DataSource) Then
            clbPaquetes.DataSource = Nothing
            configuracionTipo = Nothing
            'cbTarifa.DataSource = Nothing
        End If

        RecargarCantidades(-1, 0)
    End Sub

    Private Sub clbPaquetes_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles clbPaquetes.ItemCheck
        RecargarCantidades(e.Index, e.NewValue)
    End Sub

    Private Sub nudDescuento_ValueChanged(sender As Object, e As EventArgs) Handles nudDescuento.ValueChanged, nudNoches.ValueChanged, nudPExtra.ValueChanged
        RecargarCantidades(-1, 0)
    End Sub

    Private Sub RecargarCantidades(indice As Integer, state As CheckState)

        Dim contadorHabitaciones As Integer = IIf(nudNoches.Value > 0, 1, 0)
        Dim contadorExtras As Integer = nudNoches.Value - contadorHabitaciones

        lblHabitacionContador.Text = contadorHabitaciones
        lblHospExtraContador.Text = contadorExtras

        Dim precioHotel As Decimal
        Dim precioPExtra As Decimal
        Dim precioHExtra As Decimal

        If IsNothing(configuracionTipo) Then
            precioHotel = 0
            precioPExtra = 0
            precioHExtra = 0
        Else
            precioHotel = configuracionTipo.Precio
            precioPExtra = configuracionTipo.PrecioPersonaExtra
            precioHExtra = configuracionTipo.PrecioHospedajeExtra
        End If

        lblHabitacionPUnidad.Text = precioHotel.ToString("C2")
        Dim totalHab As Decimal = contadorHabitaciones * precioHotel
        lblHabitacionTotal.Text = totalHab.ToString("C2")
        lblPExtraContador.Text = nudPExtra.Value

        lblPExtraPUnidad.Text = precioPExtra.ToString("C2")

        Dim totalPExtra As Decimal = nudPExtra.Value * precioPExtra

        lblPExtraTotal.Text = totalPExtra.ToString("C2")

        'lblHospExtraContador.Text = nudh.Value
        lblHospExtraPUnidad.Text = precioHExtra.ToString("C2")
        Dim totalHospExt As Decimal = precioHExtra * contadorExtras
        lblHospExtraTotal.Text = totalHospExt.ToString("C2")

        Dim precionPaquetes As Decimal = 0
        Dim contadorPaquete As Integer = 0

        For i As Integer = 0 To clbPaquetes.Items.Count - 1

            If (i <> indice AndAlso clbPaquetes.CheckedIndices.Contains(i)) OrElse (i = indice AndAlso state = CheckState.Checked) Then
                precionPaquetes += CType(clbPaquetes.Items(i), Modelo.Entidades.Paquete).Precio
                contadorPaquete += 1
            End If


        Next

        lblPaquetesContador.Text = contadorPaquete

        lblPaquetesTotal.Text = precionPaquetes.ToString("C2")

        If nudDescuento.Value <> 0 Then
            lblDescuentosContador.Text = 1
        Else
            lblDescuentosContador.Text = 0
        End If

        lblTotal.Text = (totalHab + totalPExtra + totalHospExt + precionPaquetes - nudDescuento.Value).ToString("C2")

    End Sub

    'Private Sub cbTarifa_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim controlador As New ControladorConfiguracionesTipo

    '    If cbTiposHabitaciones.SelectedIndex >= 0 AndAlso cbTarifa.SelectedIndex >= 0 Then
    '        Dim idTipoSeleccionado As Integer = CInt(cbTiposHabitaciones.SelectedValue)

    '        Dim tarifaElegida As Modelo.Entidades.ConfiguracionNegocio.Tarifas = CType(cbTarifa.SelectedValue, Modelo.Entidades.ConfiguracionNegocio.Tarifas)

    '        configuracionTipo = controlador.ObtenerConfiguracionTipo(idTipoSeleccionado, tarifaElegida)
    '    Else
    '        configuracionTipo = Nothing
    '    End If

    '    RecargarCantidades(-1, 0)
    'End Sub

    Private Sub dtpFechaEntrada_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaEntrada.ValueChanged
        dtpFechaSalida.MinDate = dtpFechaEntrada.Value.AddDays(1)
    End Sub

    Private Sub dtpFechaSalida_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaSalida.ValueChanged
        nudNoches.Value = (dtpFechaSalida.Value.Date - dtpFechaEntrada.Value.Date).TotalDays
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Close()
    End Sub
End Class