﻿Imports System.Linq
Imports ExtraComponents

Public Class CreacionComandasForm

    Private BotonesArticulosDiccionario As Dictionary(Of GelButton, Modelo.Entidades.Articulo)

    Private _procesoExitoso As Boolean = False

    Private idRentaActual As Integer

    Private comandaActual As Modelo.Entidades.Comanda

    Public Property ProcesoExitoso As Boolean
        Get
            Return _procesoExitoso
        End Get
        Private Set(value As Boolean)
            _procesoExitoso = value
        End Set
    End Property

    Public Sub New(idRenta As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        BotonesArticulosDiccionario = New Dictionary(Of GelButton, Modelo.Entidades.Articulo)
        ResumenArticuloBindingSource.DataSource = New List(Of ResumenArticulo)

        idRentaActual = idRenta
    End Sub

    Public Sub New(comanda As Modelo.Entidades.Comanda)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        BotonesArticulosDiccionario = New Dictionary(Of GelButton, Modelo.Entidades.Articulo)
        ResumenArticuloBindingSource.DataSource = New List(Of ResumenArticulo)

        comandaActual = comanda

        If Not IsNothing(comandaActual) Then
            idRentaActual = comandaActual.IdRenta
        Else
            idRentaActual = 0
        End If
    End Sub

    Private Sub CreacionComandasForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarArticulos()
    End Sub



    Private Sub AgregarAComanda(sender As Object, e As EventArgs)
        Dim botonActual As GelButton = CType(sender, GelButton)

        Dim articulo As Modelo.Entidades.Articulo = BotonesArticulosDiccionario(botonActual)

        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Then
            origenes = New List(Of ResumenArticulo)
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))
        End If

        If origenes.Any(Function(m)
                            Return m.IdArticulo = articulo.Id
                        End Function) Then
            Dim filaActual As ResumenArticulo = origenes.First(Function(m)
                                                                   Return m.IdArticulo = articulo.Id
                                                               End Function)
            filaActual.Cantidad += 1
        Else
            Dim nuevoArticulo As New ResumenArticulo
            nuevoArticulo.IdArticulo = articulo.Id
            nuevoArticulo.Nombre = articulo.Nombre
            nuevoArticulo.Tipo = articulo.Tipo
            nuevoArticulo.Cantidad = 1
            nuevoArticulo.Precio = articulo.Precio

            origenes.Add(nuevoArticulo)
        End If

        ResumenArticuloBindingSource.DataSource = Nothing
        ResumenArticuloBindingSource.DataSource = origenes

        'ResumenArticuloBindingSource.RaiseListChangedEvents = True
        ResumenArticuloBindingSource.ResetBindings(False)
        'dgvProductos.Refresh()
    End Sub

    Public Class ResumenArticulo
        Public Property IdArticulo As Integer
        Public Property Nombre As String
        Public Property Tipo As String
        Public Property Precio As Decimal
        Public Property Cantidad As Integer
        Public ReadOnly Property Total As Decimal
            Get
                Return Precio * Cantidad
            End Get
        End Property
    End Class

    Private Sub ResumenArticuloBindingSource_DataSourceChanged(sender As Object, e As EventArgs) Handles ResumenArticuloBindingSource.DataSourceChanged, ResumenArticuloBindingSource.ListChanged
        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Or Not (TypeOf ResumenArticuloBindingSource.DataSource Is List(Of ResumenArticulo)) Then
            lblCortesia.Text = "$" + (0).ToString("#,##0.00")
            lblDescuento.Text = "$" + (0).ToString("#,##0.00")
            lblSubtotal.Text = "$" + (0).ToString("#,##0.00")
            lblTotal.Text = "$" + (0).ToString("#,##0.00")
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))

            Dim subtotal As Decimal
            Dim cortesia As Decimal = 0
            Dim descuento As Decimal = 0
            If (origenes.Count > 0) Then
                subtotal = origenes.Sum(Function(m)
                                            Return m.Total
                                        End Function)

            Else
                subtotal = 0
            End If

            lblSubtotal.Text = "$" + subtotal.ToString("#,##0.00")
            lblCortesia.Text = "$" + cortesia.ToString("#,##0.00")
            lblDescuento.Text = "$" + descuento.ToString("#,##0.00")
            lblTotal.Text = "$" + (subtotal - cortesia - descuento).ToString("#,##0.00")

        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim origenes As List(Of ResumenArticulo)

        If IsNothing(ResumenArticuloBindingSource.DataSource) Then
            origenes = New List(Of ResumenArticulo)
        Else
            origenes = CType(ResumenArticuloBindingSource.DataSource, List(Of ResumenArticulo))
        End If

        Dim subtotal As Decimal
        Dim cortesia As Decimal = 0
        Dim descuento As Decimal = 0
        If (origenes.Count > 0) Then
            subtotal = origenes.Sum(Function(m)
                                        Return m.Total
                                    End Function)

        Else
            subtotal = 0
        End If

        If IsNothing(comandaActual) Then
            comandaActual = New Modelo.Entidades.Comanda

            comandaActual.Activo = True
            comandaActual.Valor = subtotal - descuento
            comandaActual.IdRenta = idRentaActual

            For Each item As ResumenArticulo In origenes
                Dim nuevoArticuloComanda As New Modelo.Entidades.ArticuloComanda
                nuevoArticuloComanda.Activo = True
                nuevoArticuloComanda.IdArticulo = item.IdArticulo
                nuevoArticuloComanda.Cantidad = item.Cantidad
                nuevoArticuloComanda.PrecioUnidad = item.Precio

                comandaActual.ArticulosComanda.Add(nuevoArticuloComanda)
            Next

            Dim controlador As New Controladores.ControladorRentas

            controlador.CrearComanda(comandaActual)

        Else

        End If

        ProcesoExitoso = True

        Close()

    End Sub

    Private Sub txtBusqueda_Leave(sender As Object, e As EventArgs) Handles txtBusqueda.Leave

        If Not String.IsNullOrWhiteSpace(txtBusqueda.Text) Then
            CargarArticulos(txtBusqueda.Text)
        Else
            CargarArticulos()
        End If
    End Sub

    Private Sub CargarArticulos(Optional filtro As String = Nothing)

        Dim controlador As New Controladores.ControladorArticulos

        Dim tipos = controlador.ObtenerTiposConArticulos(filtro)

        For Each tabP As TabPage In tcProductos.TabPages
            tabP.Dispose()
        Next

        tcProductos.TabPages.Clear()

        For Each tipoArticulo As Modelo.Entidades.TipoArticulo In tipos

            Dim pagina As New TabPage
            pagina.Text = tipoArticulo.Nombre

            Dim flContenedor As New FlowLayoutPanel
            flContenedor.FlowDirection = FlowDirection.TopDown
            flContenedor.AutoScroll = True

            For Each articuloActual As Modelo.Entidades.Articulo In tipoArticulo.Articulos
                Dim gelBtn As New GelButton()
                gelBtn.Width = 150
                gelBtn.Height = 30
                gelBtn.Text = articuloActual.Nombre
                gelBtn.TextAlign = ContentAlignment.MiddleRight
                gelBtn.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
                gelBtn.Margin = New Padding(0)
                gelBtn.Padding = gelBtn.Margin
                gelBtn.GradientBottom = Color.White
                gelBtn.GradientTop = Color.FromArgb(252, 217, 252)
                AddHandler gelBtn.Click, AddressOf AgregarAComanda

                flContenedor.Controls.Add(gelBtn)
                BotonesArticulosDiccionario.Add(gelBtn, articuloActual)
            Next

            pagina.Controls.Add(flContenedor)
            flContenedor.Dock = DockStyle.Fill

            tcProductos.TabPages.Add(pagina)
        Next
    End Sub

    Private Sub txtBusqueda_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBusqueda.KeyPress
        If (e.KeyChar = ChrW(Keys.Enter)) Then
            panelBotonera.Focus()
        End If
    End Sub
End Class