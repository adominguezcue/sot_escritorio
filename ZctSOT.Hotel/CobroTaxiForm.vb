﻿Imports Transversal.Excepciones
Imports Transversal.Extensiones

Public Class CobroTaxiForm

    Private WithEvents listaMarcable As CheckedListBox

    Private IdOrdenTaxi As Integer = 0

    Public Sub New(idOrden As Integer)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        listaMarcable = New CheckedListBox

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        IdOrdenTaxi = idOrden
    End Sub

    Private Sub CobroTaxiForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If IdOrdenTaxi > 0 Then

            Dim controlador As New Controladores.ControladorGlobal()
            Dim controladorTareasL As New Controladores.ControladorTareasLimpieza

            Dim empleados As List(Of Modelo.Entidades.Empleado) = controlador.AplicacionServicioEmpleados.ObtenerValets(controlador.UsuarioActual)


            listaMarcable.SelectionMode = SelectionMode.One
            listaMarcable.CheckOnClick = True

            listaMarcable.DataSource = empleados
            listaMarcable.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.Empleado, String)(Function(m) m.NombreCompleto)
            listaMarcable.Margin = New Padding(0)

            flpDisponibles.Controls.Add(listaMarcable)
            listaMarcable.Size = flpDisponibles.Size

        End If
    End Sub

    Private Sub LimpiarChecksTipos(index As Integer)
        For ix As Integer = 0 To listaMarcable.Items.Count - 1
            If ix <> index Then
                listaMarcable.SetItemChecked(ix, False)
                listaMarcable.SetSelected(ix, False)
            End If
        Next
    End Sub

    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles listaMarcable.ItemCheck
        LimpiarChecksTipos(e.Index)
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If IdOrdenTaxi > 0 Then
            If listaMarcable.CheckedItems.Count = 0 Then
                Throw New SOTException(My.Resources.Errores.valet_no_seleccionado_exception)
            End If

            Dim empleadoActual As Modelo.Entidades.Empleado = TryCast(listaMarcable.CheckedItems(0), Modelo.Entidades.Empleado)

            Dim controlador As New Controladores.ControladorTaxis

            controlador.CobrarOrden(IdOrdenTaxi, empleadoActual.Id)

        End If
        Close()
    End Sub
End Class