﻿Public Class MenuPrincipal

    Public Sub AgregarControl(ByRef item As Control, Optional limpiar As Boolean = False)
        If limpiar Then
            contenedorBotones.Controls.Clear()
        End If

        contenedorBotones.Controls.Add(item)
    End Sub

    Private Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        Me.Close()
    End Sub

    Private Sub btnReportarMatricula_Click(sender As Object, e As EventArgs) Handles btnReportarMatricula.Click
        Dim reporteadorMatriculas As New ReportesMatriculaForm
        reporteadorMatriculas.ShowDialog(Me)
    End Sub
End Class