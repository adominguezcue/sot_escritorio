﻿Imports System.Collections.Generic
Imports System.Linq
Imports Transversal.Excepciones
Imports Transversal.Extensiones
Imports Transversal
Imports Transversal.Dtos

Public Class RentasForm

    Private codigoMetodoPago As String = ""

    Private descuento As Decimal = 0
    Private configuracionTipo As Modelo.Entidades.ConfiguracionTipo

    Private _habitacion As Modelo.Entidades.Habitacion

    Private rentaActual As Modelo.Entidades.Renta

    ''Private tarifaSeleccionada As Modelo.Entidades.ConfiguracionNegocio.Tarifas

    Private Sub MostrarParaCrear()
        nudHabitaciones.Enabled = False
        nudPersonasExtra.Enabled = True
        nudHospedajeExtra.Enabled = True
        nudPaquetes.Enabled = False
        'nudDescuentos.Enabled = False
        cbTarifa.Enabled = True
        'cbFormasPago.Enabled = True
        'txtVPoint.Enabled = True
        chbAPie.Enabled = True
        'txtMatricula.Enabled = True
        'txtMarca.Enabled = True
        'txtLinea.Enabled = True
        'txtColor.Enabled = True
        txtTarjeta.ReadOnly = False

        btnSolicitarCancelacion.Hide()
        btnCancelarEntrada.Show()
        'btnVPoints.Show()
        'btnCupon.Show()
        'btnHorasExtra.Hide()
        'btnPersonasExtra.Hide()
        'btnCambiarMatricula.Hide()
        btnAceptar.Show()
    End Sub

    Private Sub MostrarParaEditar()
        nudHabitaciones.Enabled = False
        nudPersonasExtra.Enabled = True
        nudHospedajeExtra.Enabled = True
        nudPaquetes.Enabled = False
        'nudDescuentos.Enabled = False
        cbTarifa.Enabled = False
        'cbFormasPago.Enabled = True
        'txtVPoint.Enabled = True
        chbAPie.Enabled = True
        'txtMatricula.Enabled = True
        'txtMarca.Enabled = True
        'txtLinea.Enabled = True
        'txtColor.Enabled = True
        txtTarjeta.ReadOnly = True

        btnSolicitarCancelacion.Show()
        btnCancelarEntrada.Hide()
        'btnVPoints.Show()
        'btnCupon.Show()
        'btnHorasExtra.Show()
        'btnPersonasExtra.Show()
        'btnCambiarMatricula.Show()
        btnAceptar.Show()
    End Sub

    Private Sub MostrarParaCobrar()
        nudHabitaciones.Enabled = False
        nudPersonasExtra.Enabled = True
        nudHospedajeExtra.Enabled = True
        nudPaquetes.Enabled = False
        'nudDescuentos.Enabled = False
        cbTarifa.Enabled = True
        'cbFormasPago.Enabled = True
        chbAPie.Enabled = True
        'txtVPoint.Enabled = True
        'txtMarca.Enabled = True
        'txtLinea.Enabled = True
        'txtColor.Enabled = True
        txtTarjeta.ReadOnly = True

        btnSolicitarCancelacion.Hide()
        btnCancelarEntrada.Show()
        'btnVPoints.Show()
        'btnCupon.Show()
        'btnHorasExtra.Hide()
        'btnPersonasExtra.Show()
        'btnCambiarMatricula.Hide()
        btnAceptar.Show()
    End Sub

    Public Sub ActualizarHabitacion(habitacion As Modelo.Entidades.Habitacion, modo As Modos)
        If IsNothing(habitacion) Then
            Throw New SOTException(My.Resources.Errores.renta_habitacion_nula_exception)
        End If

        _habitacion = habitacion

        lblDatosHabitacion.Text = String.Format(My.Resources.Etiquetas.RentasForm_InformacionHabitacion, habitacion.NumeroHabitacion, habitacion.NombreTipo)


        Dim controlador As New Controladores.ControladorGlobal
        Dim tarifas As List(Of DtoEnum) = controlador.AplicacionServicioTiposHabitaciones.ObtenerTarifasSoportadasComoDto(_habitacion.IdTipoHabitacion)

        Helpers.HelperDataSource.AgregarDataSource(cbTarifa, tarifas, Function(m) m.Nombre, Function(m) m.Valor)

        'cbTarifa.DataSource = tarifas
        'cbTarifa.DisplayMember = ObjectExtensions.NombrePropiedad(Of DtoEnum, String)(Function(m) m.Nombre)
        'cbTarifa.ValueMember = ObjectExtensions.NombrePropiedad(Of DtoEnum, Object)(Function(m) m.Valor)
        cbTarifa.SelectedIndex = -1
        cbTarifa.SelectedIndex = 0

        If habitacion.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada Then

            lblDatosHospedaje.Text = String.Empty

            MostrarParaCrear()
            nudHabitaciones.Value = 1
        ElseIf habitacion.EstadoHabitacion = Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada Then

            Dim controladorR As New Controladores.ControladorRentas

            rentaActual = controladorR.ObtenerRentaActualPorHabitacion(habitacion.Id)

            Dim fechaFin = rentaActual.FechaFin

            If (rentaActual.TiemposExtra.Any(Function(m)
                                                 Return m.Activa
                                             End Function)) Then
                fechaFin = fechaFin.AddMinutes(rentaActual.TiemposExtra.Where(Function(m)
                                                                                  Return m.Activa
                                                                              End Function).Sum(Function(m)
                                                                                                    Return m.Minutos
                                                                                                End Function))
            End If

            Dim tiempo As TimeSpan = (fechaFin - rentaActual.FechaInicio)

            lblDatosHospedaje.Text = String.Format(My.Resources.Etiquetas.RentasForm_InformacionHospedaje, tiempo.ToString("hh\:mm"))

            Select Case modo
                Case Modos.Edicion

                    Dim automovil = rentaActual.RentaAutomoviles.FirstOrDefault(Function(m)
                                                                                    Return m.Activa
                                                                                End Function)

                    chbAPie.Checked = IsNothing(automovil)

                    If Not chbAPie.Checked Then
                        txtMatricula.Text = automovil.Matricula
                        txtModelo.Text = automovil.Modelo
                        txtMarca.Text = automovil.Marca
                        txtColor.Text = automovil.Color
                    End If

                    MostrarParaEditar()
                    Exit Select
                Case Modos.Cobro
                    MostrarParaCobrar()
                    Exit Select
            End Select

        End If
    End Sub

    Private Sub RentasForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim controladorPagos As New Controladores.ControladorPagos

        'cbFormasPago.DataSource = [Enum].GetValues(GetType(Modelo.Entidades.EnumsIndividuales.TiposPago)).Cast(Of Modelo.Entidades.EnumsIndividuales.TiposPago).ToList()

        'Helpers.HelperDataSource.AgregarDataSource(cbFormasPago, controladorPagos.ObtenerTiposValidosConMixtos(), _
        '                                           Function(x) x.Nombre, Function(x) x.Pagos)

        'cbFormasPago.SelectedIndex = -1
        'AddHandler cbFormasPago.SelectedIndexChanged, AddressOf cbFormasPago_SelectedIndexChanged


    End Sub

    'Private Sub cbFormasPago_SelectedIndexChanged(sender As Object, e As EventArgs)
    'Select Case CType(cbFormasPago.SelectedItem, Modelo.Entidades.PagoRenta.TiposPago)
    '    Case Modelo.Entidades.PagoRenta.TiposPago.Reservacion
    '        'Dim rsmForm As ReservacionesMiniForm = New ReservacionesMiniForm
    '        'rsmForm.ShowDialog(Me)
    '        'If rsmForm.EsFolioValido Then
    '        '    txtFolio.Text = rsmForm.CodigoReservacion
    '        '    codigoMetodoPago = txtFolio.Text
    '        'Else
    '        '    txtFolio.Text = ""
    '        '    codigoMetodoPago = ""
    '        'End If
    '    Case Modelo.Entidades.PagoRenta.TiposPago.TarjetaCredito
    '        Dim rsmForm As PagoTarjetaForm = New PagoTarjetaForm
    '        rsmForm.ShowDialog(Me)
    '        If rsmForm.EsValida Then
    '            txtFolio.Text = rsmForm.NumeroTarjeta
    '        End If
    '        txtFolio.Text = ""
    '    Case Else
    '        codigoMetodoPago = ""
    'End Select
    'End Sub

    Private Sub cbTarifa_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTarifa.SelectedIndexChanged
        Dim controlador As New Controladores.ControladorConfiguracionesTipo

        If cbTarifa.SelectedIndex >= 0 Then

            Dim tarifaElegida As Modelo.Entidades.ConfiguracionTarifa.Tarifas = CType(cbTarifa.SelectedValue, Modelo.Entidades.ConfiguracionTarifa.Tarifas)

            configuracionTipo = controlador.ObtenerConfiguracionTipo(_habitacion.IdTipoHabitacion, tarifaElegida)
        Else
            configuracionTipo = Nothing
        End If

        If IsNothing(configuracionTipo) OrElse configuracionTipo.ConfiguracionTarifa.RangoFijo Then
            nudHospedajeExtra.Value = 0
            nudHospedajeExtra.Enabled = False
        Else
            nudHospedajeExtra.Enabled = True
            nudHospedajeExtra.Maximum = configuracionTipo.TiemposHospedaje.Where(Function(m)
                                                                                     Return m.Activo
                                                                                 End Function).Count()
        End If

        RecargarCantidades()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If IsNothing(rentaActual) Then
            CrearRenta()
        Else
            ModificarRenta()
        End If
    End Sub

    Private Sub nudHabitaciones_ValueChanged(sender As Object, e As EventArgs) Handles nudHabitaciones.ValueChanged, nudPersonasExtra.ValueChanged, nudHospedajeExtra.ValueChanged, nudPaquetes.ValueChanged
        RecargarCantidades()
    End Sub

    Private Sub RecargarCantidades()

        Dim precioHotel As Decimal
        Dim precioPExtra As Decimal
        Dim precioHExtra As Decimal

        If IsNothing(configuracionTipo) Then
            precioHotel = 0
            precioPExtra = 0
            precioHExtra = 0
        Else
            precioHotel = configuracionTipo.Precio
            precioPExtra = configuracionTipo.PrecioPersonaExtra
            precioHExtra = configuracionTipo.PrecioHospedajeExtra
        End If

        lblHabitacionPUnidad.Text = precioHotel.ToString("C")
        Dim totalHab As Decimal = nudHabitaciones.Value * precioHotel
        lblHabitacionTotal.Text = totalHab.ToString("C")

        lblPersonasExtraPUnidad.Text = precioPExtra.ToString("C")

        Dim totalPExtra As Decimal = nudPersonasExtra.Value * precioPExtra

        lblPersonasExtraTotal.Text = totalPExtra.ToString("C")

        'lblHospExtraContador.Text = nudh.Value
        lblHospedajeExtraPUnidad.Text = precioHExtra.ToString("C")
        Dim totalHospExt As Decimal = nudHospedajeExtra.Value * precioHExtra
        lblHospedajeExtraTotal.Text = totalHospExt.ToString("C")

        Dim precionPaquetes As Decimal = 0

        'For Each paquete As Modelo.Entidades.Paquete In clbPaquetes.CheckedItems
        '    precionPaquetes += paquete.Precio
        'Next

        'lblPaquetesContador.Text = clbPaquetes.CheckedItems.Count

        'precionPaquetes *= clbPaquetes.CheckedItems.Count
        lblPaqueteTotal.Text = precionPaquetes.ToString("C")

        'lblDescuentosTotal.Text = "$" & 

        lblTotal.Text = (totalHab + totalPExtra + totalHospExt + precionPaquetes - descuento).ToString("C")

    End Sub

    Private Sub btnCancelarEntrada_Click(sender As Object, e As EventArgs) Handles btnCancelarEntrada.Click

        If MessageBox.Show(String.Format(My.Resources.Mensajes.cancelar_preparacion, _habitacion.NumeroHabitacion), My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim controlador As New Controladores.ControladorHabitaciones

            controlador.CancelarPreparacionHabitacion(_habitacion.Id)
            Close()
        End If


    End Sub

    Enum Modos
        Creacion = 1
        Cobro = 2
        Edicion = 3
    End Enum

    Private Sub btnHorasExtra_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub btnPersonasExtra_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub chbAPie_CheckedChanged(sender As Object, e As EventArgs) Handles chbAPie.CheckedChanged

        txtMatricula.Enabled = Not chbAPie.Checked
        txtModelo.Enabled = Not chbAPie.Checked
        txtMarca.Enabled = Not chbAPie.Checked
        txtColor.Enabled = Not chbAPie.Checked
    End Sub

    Private Sub btnCambiarMatricula_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub CrearRenta()
        'Dim tarifaSeleccionada As Modelo.Entidades.ConfiguracionNegocio.Tarifas = cbTarifa.SelectedValue
        If IsNothing(_habitacion) OrElse _habitacion.EstadoHabitacion <> Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada Then
            Exit Sub
        End If

        ValidarDatosAutomovil()

        Dim controladorRM As New Controladores.ControladorReportesMatriculas

        If Not String.IsNullOrWhiteSpace(txtMatricula.Text) AndAlso controladorRM.PoseeReportes(txtMatricula.Text) Then
            Dim reporteadorMatriculas As New ReportesMatriculaForm(txtMatricula.Text)
            reporteadorMatriculas.ShowDialog(Me)

            If MessageBox.Show(Me, My.Resources.Mensajes.continuar_proceso_renta, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
                Exit Sub
            End If

        End If

        Dim precioHotel As Decimal
        Dim precioPExtra As Decimal
        Dim precioHExtra As Decimal

        If IsNothing(configuracionTipo) Then
            precioHotel = 0
            precioPExtra = 0
            precioHExtra = 0
        Else
            precioHotel = configuracionTipo.Precio
            precioPExtra = configuracionTipo.PrecioPersonaExtra
            precioHExtra = configuracionTipo.PrecioHospedajeExtra
        End If

        Dim rentaNueva As Modelo.Entidades.Renta = New Modelo.Entidades.Renta()
        rentaNueva.Activa = True
        rentaNueva.IdConfiguracionTipo = configuracionTipo.Id

        If String.IsNullOrWhiteSpace(txtTarjeta.Text) Then
            rentaNueva.NumeroTarjeta = txtTarjeta.Text.Trim()
        End If

        'CType(cbFormasPago.SelectedItem, Modelo.Entidades.PagoRenta.TiposPago)

        'Dim rentaHabitacion As Modelo.Entidades.RentaHabitacion = New Modelo.Entidades.RentaHabitacion()
        'rentaHabitacion.Activa = True
        rentaNueva.IdHabitacion = _habitacion.Id
        rentaNueva.PrecioHabitacion = precioHotel




        If Not configuracionTipo.ConfiguracionTarifa.RangoFijo Then

            Dim extensiones As Integer = nudHospedajeExtra.Value
            Dim contador = 1

            For Each configTiempo In (From t In configuracionTipo.TiemposHospedaje _
                                      Where t.Activo _
                                      Order By t.Orden Ascending)

                If contador > extensiones Then
                    Exit For
                End If

                Dim nuevaExtension As New Modelo.Entidades.TiempoExtra

                nuevaExtension.Activa = True
                nuevaExtension.Precio = precioHExtra
                nuevaExtension.IdTiempoHospedaje = configTiempo.Id
                nuevaExtension.Minutos = configTiempo.Minutos
                rentaNueva.TiemposExtra.Add(nuevaExtension)

                contador += 1
            Next
        End If

        For i As Integer = 1 To nudPersonasExtra.Value
            Dim personaExt As New Modelo.Entidades.PersonaExtra

            personaExt.Activa = True
            personaExt.Precio = precioPExtra
            rentaNueva.PersonasExtra.Add(personaExt)
        Next

        Dim valor As Decimal = rentaNueva.PrecioHabitacion 'cambiar esto

        If rentaNueva.PersonasExtra.Any(Function(m)
                                            Return m.Activa
                                        End Function) Then
            valor += rentaNueva.PersonasExtra.Where(Function(m)
                                                        Return m.Activa
                                                    End Function).Sum(Function(m)
                                                                          Return m.Precio
                                                                      End Function)
        End If

        If rentaNueva.TiemposExtra.Any(Function(m)
                                           Return m.Activa
                                       End Function) Then
            valor += rentaNueva.TiemposExtra.Where(Function(m)
                                                       Return m.Activa
                                                   End Function).Sum(Function(m)
                                                                         Return m.Precio
                                                                     End Function)
        End If

        Dim controladorF As New Controladores.ControladorFormasPago()

        Dim selectorFormaPago = controladorF.ObtenerSelectorPago(valor, True, True)

        'Dim selectorFormaPago As New SelectorFormaPagoForm(valor, True, True)

        selectorFormaPago.MostrarEnDialogo(Me)

        If Not selectorFormaPago.ProcesoExitoso Then
            Exit Sub
        End If

        For Each pagoSeleccionado In selectorFormaPago.FormasPagoSeleccionadas

            Dim pagoRenta As New Modelo.Entidades.PagoRenta()
            pagoRenta.Activo = True
            pagoRenta.TipoPago = pagoSeleccionado.TipoPago
            pagoRenta.Referencia = pagoSeleccionado.Referencia
            pagoRenta.Valor = pagoSeleccionado.Valor
            pagoRenta.TipoTarjeta = pagoSeleccionado.TipoTarjeta
            rentaNueva.Pagos.Add(pagoRenta)

        Next
        'rentaNueva.PrecioTotal = rentaNueva.Pagos.Where(Function(m)
        '                                                    Return m.Activo
        '                                                End Function).Sum(Function(m)
        '                                                                      Return m.Valor
        '                                                                  End Function)


        If Not chbAPie.Checked Then
            Dim rentaAuto As Modelo.Entidades.AutomovilRenta = New Modelo.Entidades.AutomovilRenta()
            rentaAuto.Activa = True
            rentaAuto.Color = txtColor.Text
            rentaAuto.Marca = txtMarca.Text
            rentaAuto.Modelo = txtModelo.Text
            rentaAuto.Matricula = txtMatricula.Text

            rentaNueva.RentaAutomoviles.Add(rentaAuto)
        End If



        Dim controlador As New Controladores.ControladorRentas()
        controlador.CrearRenta(rentaNueva, nudPersonasExtra.Value > configuracionTipo.TipoHabitacion.MaximoPersonasExtra)

        MessageBox.Show(My.Resources.Mensajes.renta_agregada_con_exito, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Close()
    End Sub

    Private Sub ValidarDatosAutomovil()
        If Not chbAPie.Checked Then
            If String.IsNullOrWhiteSpace(txtMatricula.Text) Then
                Throw New SOTException(My.Resources.Errores.matricula_invalida_exception)
            End If

            If String.IsNullOrWhiteSpace(txtMarca.Text) Then
                Throw New SOTException(My.Resources.Errores.marca_invalida_exception)
            End If

            If String.IsNullOrWhiteSpace(txtModelo.Text) Then
                Throw New SOTException(My.Resources.Errores.modelo_invalido_exception)
            End If

            If String.IsNullOrWhiteSpace(txtColor.Text) Then
                Throw New SOTException(My.Resources.Errores.color_invalido_exception)
            End If
        End If
    End Sub

    Private Sub ModificarRenta()

        ValidarDatosAutomovil()

        Dim controladorR As New Controladores.ControladorRentas

        Dim valor = (configuracionTipo.PrecioHospedajeExtra * nudHospedajeExtra.Value) +
                    (configuracionTipo.PrecioPersonaExtra * nudPersonasExtra.Value)

        Dim pagos As New List(Of Modelo.Dtos.DtoInformacionPago)

        If valor > 0 Then
            Dim controladorF As New Controladores.ControladorFormasPago()

            Dim selectorFormaPago = controladorF.ObtenerSelectorPago(valor, ocultarPropina:=True)

            'Dim selectorFormaPago As New SelectorFormaPagoForm(valor, ocultarPropina:=True)

            selectorFormaPago.MostrarEnDialogo(Me)

            If Not selectorFormaPago.ProcesoExitoso Then
                Exit Sub
            End If

            pagos = selectorFormaPago.FormasPagoSeleccionadas
        End If

        Dim autos As New List(Of Modelo.Dtos.DtoAutomovil)

        If Not chbAPie.Checked Then

            If String.IsNullOrWhiteSpace(txtMatricula.Text) Then
                Throw New SOTException(My.Resources.Errores.matricula_invalida_exception)
            End If

            If String.IsNullOrWhiteSpace(txtMarca.Text) Then
                Throw New SOTException(My.Resources.Errores.marca_invalida_exception)
            End If

            If String.IsNullOrWhiteSpace(txtModelo.Text) Then
                Throw New SOTException(My.Resources.Errores.modelo_invalido_exception)
            End If

            If String.IsNullOrWhiteSpace(txtColor.Text) Then
                Throw New SOTException(My.Resources.Errores.color_invalido_exception)
            End If

            Dim rentaAuto As New Modelo.Dtos.DtoAutomovil
            rentaAuto.Color = txtColor.Text
            rentaAuto.Marca = txtMarca.Text
            rentaAuto.Matricula = txtMatricula.Text
            rentaAuto.Modelo = txtModelo.Text

            autos.Add(rentaAuto)
        End If

        controladorR.ActualizarDatosHabitacion(rentaActual.Id, nudHospedajeExtra.Value, nudPersonasExtra.Value, autos, pagos)

        MessageBox.Show(My.Resources.Mensajes.datos_automovil_actualizados_con_exito, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Close()

    End Sub

End Class