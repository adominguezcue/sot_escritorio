﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GestionOrdenesForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.flpGestiones = New System.Windows.Forms.FlowLayoutPanel()
        Me.SuspendLayout()
        '
        'flpGestiones
        '
        Me.flpGestiones.AutoScroll = True
        Me.flpGestiones.BackColor = System.Drawing.Color.Transparent
        Me.flpGestiones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flpGestiones.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flpGestiones.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.flpGestiones.Location = New System.Drawing.Point(0, 0)
        Me.flpGestiones.Name = "flpGestiones"
        Me.flpGestiones.Size = New System.Drawing.Size(1008, 729)
        Me.flpGestiones.TabIndex = 0
        '
        'GestionOrdenesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1008, 729)
        Me.Controls.Add(Me.flpGestiones)
        Me.Name = "GestionOrdenesForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Restaurante"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents flpGestiones As System.Windows.Forms.FlowLayoutPanel
End Class
