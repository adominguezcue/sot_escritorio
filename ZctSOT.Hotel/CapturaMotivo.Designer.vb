﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CapturaMotivo
    Inherits FormBase

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grupoMotivo = New FixedHeaderPanel()
        Me.lblDetalleExtra = New System.Windows.Forms.Label()
        Me.txtMotivo = New System.Windows.Forms.TextBox()
        Me.panelBotonera.SuspendLayout()
        Me.grupoMotivo.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Location = New System.Drawing.Point(0, 228)
        Me.panelBotonera.Size = New System.Drawing.Size(372, 84)
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(264, 2)
        '
        'grupoMotivo
        '
        Me.grupoMotivo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.grupoMotivo.Controls.Add(Me.lblDetalleExtra)
        Me.grupoMotivo.Controls.Add(Me.txtMotivo)
        Me.grupoMotivo.GradientBottom = System.Drawing.SystemColors.ControlDark
        Me.grupoMotivo.GradientTop = System.Drawing.SystemColors.ControlLight
        Me.grupoMotivo.Location = New System.Drawing.Point(13, 13)
        Me.grupoMotivo.Name = "grupoMotivo"
        Me.grupoMotivo.Size = New System.Drawing.Size(347, 210)
        Me.grupoMotivo.TabIndex = 5
        Me.grupoMotivo.Text = "Motivo"
        '
        'lblDetalleExtra
        '
        Me.lblDetalleExtra.Location = New System.Drawing.Point(7, 30)
        Me.lblDetalleExtra.Name = "lblDetalleExtra"
        Me.lblDetalleExtra.Size = New System.Drawing.Size(334, 25)
        Me.lblDetalleExtra.TabIndex = 1
        Me.lblDetalleExtra.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtMotivo
        '
        Me.txtMotivo.Location = New System.Drawing.Point(6, 58)
        Me.txtMotivo.Multiline = True
        Me.txtMotivo.Name = "txtMotivo"
        Me.txtMotivo.Size = New System.Drawing.Size(335, 145)
        Me.txtMotivo.TabIndex = 0
        '
        'CapturaMotivo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(372, 312)
        Me.Controls.Add(Me.grupoMotivo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "CapturaMotivo"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Controls.SetChildIndex(Me.panelBotonera, 0)
        Me.Controls.SetChildIndex(Me.grupoMotivo, 0)
        Me.panelBotonera.ResumeLayout(False)
        Me.grupoMotivo.ResumeLayout(False)
        Me.grupoMotivo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grupoMotivo As FixedHeaderPanel
    Friend WithEvents txtMotivo As System.Windows.Forms.TextBox
    Friend WithEvents lblDetalleExtra As System.Windows.Forms.Label
End Class
