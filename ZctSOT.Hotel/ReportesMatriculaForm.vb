﻿Imports System.Linq
Imports Transversal.Excepciones

Public Class ReportesMatriculaForm

    Public Sub New(Optional matricula As String = Nothing)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        If Not String.IsNullOrWhiteSpace(matricula) Then
            txtMatricula.Text = matricula
        End If

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Close()
    End Sub

    Private Sub txtMatricula_Leave(sender As Object, e As EventArgs) Handles txtMatricula.Leave

        If Not String.IsNullOrWhiteSpace(txtMatricula.Text) Then
            RecargarReportes()
        End If
    End Sub

    Private Sub listaReportes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listaReportes.SelectedIndexChanged
        If listaReportes.SelectedIndex < 0 Then
            LimpiarLectura()
        Else
            Dim item = TryCast(listaReportes.SelectedItem, Modelo.Entidades.ReporteMatricula)

            If IsNothing(item) Then
                LimpiarLectura()
            Else
                txtFecha.Text = item.FechaCreacion
                txtUsuario.Text = item.NombreUsuario
                txtDescripcionLectura.Text = item.Detalles
            End If
        End If
    End Sub

    Sub LimpiarLectura()
        txtFecha.Clear()
        txtUsuario.Clear()
        txtDescripcionLectura.Clear()
    End Sub

    Private Sub btnReportar_Click(sender As Object, e As EventArgs) Handles btnReportar.Click
        Dim nuevoReporte As New Modelo.Entidades.ReporteMatricula

        nuevoReporte.Matricula = txtMatricula.Text
        nuevoReporte.Detalles = txtDescripcion.Text

        Dim controlador As New Controladores.ControladorReportesMatriculas

        controlador.CrearReporte(nuevoReporte)

        RecargarReportes()
    End Sub

    Private Sub RecargarReportes()
        Dim controlador As New Controladores.ControladorReportesMatriculas

        LimpiarLectura()

        Helpers.HelperDataSource.AgregarDataSource(listaReportes, controlador.ObtenerReportesMatricula(txtMatricula.Text), _
                                                   Function(x) x.FechaCreacion, Function(x) x.Id)

        listaReportes.SelectedIndex = -1
    End Sub

    Private Sub btnInhabilitar_Click(sender As Object, e As EventArgs) Handles btnInhabilitar.Click
        If listaReportes.SelectedIndex < 0 Then
            Throw New SOTException(My.Resources.Errores.reporte_no_seleccionado_deshabilitar_exception)
        Else
            Dim item = TryCast(listaReportes.SelectedItem, Modelo.Entidades.ReporteMatricula)

            If IsNothing(item) Then
                Throw New SOTException(My.Resources.Errores.reporte_no_seleccionado_deshabilitar_exception)
            Else

                If item.Activo = False Then
                    Throw New SOTException(My.Resources.Errores.reporte_inactivo_no_desactivable_exception)
                End If

                Dim controlador As New Controladores.ControladorReportesMatriculas

                controlador.EliminarReporte(item.Id)
                RecargarReportes()
            End If
        End If
    End Sub

    Private Sub listaReportes_DrawItem(sender As Object, e As DrawItemEventArgs) Handles listaReportes.DrawItem

        If e.Index >= 0 Then

            Dim item = CType(listaReportes.Items(e.Index), Modelo.Entidades.ReporteMatricula)

            e.DrawBackground()

            Dim g = e.Graphics

            If item.Activo Then
                g.FillRectangle(New SolidBrush(Color.FromArgb(192, 255, 255)), e.Bounds)
            Else
                g.FillRectangle(New SolidBrush(Color.FromArgb(255, 128, 128)), e.Bounds)
            End If


            g.DrawString(listaReportes.GetItemText(item), e.Font, New SolidBrush(e.ForeColor), New PointF(e.Bounds.X, e.Bounds.Y))


            e.DrawFocusRectangle()
        End If
    End Sub

    Private Sub ReportesMatriculaForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not String.IsNullOrWhiteSpace(txtMatricula.Text) Then
            RecargarReportes()
        End If
    End Sub
End Class