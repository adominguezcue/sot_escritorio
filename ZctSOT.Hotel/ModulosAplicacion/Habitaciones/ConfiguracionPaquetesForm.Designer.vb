﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConfiguracionPaquetesForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgvPaquetes = New System.Windows.Forms.DataGridView()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreTipoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descuento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaqueteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.panelEdicion = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.clbTipos = New System.Windows.Forms.CheckedListBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.nudPrecio = New System.Windows.Forms.NumericUpDown()
        Me.nudDescuento = New System.Windows.Forms.NumericUpDown()
        Me.txtLeyenda = New System.Windows.Forms.TextBox()
        Me.txtNombrePaquete = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.dgvPaquetes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PaqueteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.panelEdicion.SuspendLayout()
        CType(Me.nudPrecio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDescuento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvPaquetes
        '
        Me.dgvPaquetes.AllowUserToAddRows = False
        Me.dgvPaquetes.AllowUserToDeleteRows = False
        Me.dgvPaquetes.AllowUserToOrderColumns = True
        Me.dgvPaquetes.AutoGenerateColumns = False
        Me.dgvPaquetes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPaquetes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NombreDataGridViewTextBoxColumn, Me.NombreTipoDataGridViewTextBoxColumn, Me.Precio, Me.Descuento})
        Me.dgvPaquetes.DataSource = Me.PaqueteBindingSource
        Me.dgvPaquetes.Location = New System.Drawing.Point(0, 185)
        Me.dgvPaquetes.Margin = New System.Windows.Forms.Padding(0)
        Me.dgvPaquetes.MinimumSize = New System.Drawing.Size(569, 250)
        Me.dgvPaquetes.Name = "dgvPaquetes"
        Me.dgvPaquetes.ReadOnly = True
        Me.dgvPaquetes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPaquetes.Size = New System.Drawing.Size(569, 250)
        Me.dgvPaquetes.TabIndex = 0
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Nombre del paquete"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreTipoDataGridViewTextBoxColumn
        '
        Me.NombreTipoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NombreTipoDataGridViewTextBoxColumn.DataPropertyName = "NombreTipo"
        Me.NombreTipoDataGridViewTextBoxColumn.HeaderText = "Tipo de habitación"
        Me.NombreTipoDataGridViewTextBoxColumn.Name = "NombreTipoDataGridViewTextBoxColumn"
        Me.NombreTipoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Precio
        '
        Me.Precio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Precio.DataPropertyName = "Precio"
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        '
        'Descuento
        '
        Me.Descuento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Descuento.DataPropertyName = "Descuento"
        Me.Descuento.HeaderText = "Descuento"
        Me.Descuento.Name = "Descuento"
        Me.Descuento.ReadOnly = True
        '
        'PaqueteBindingSource
        '
        Me.PaqueteBindingSource.DataSource = GetType(Modelo.Entidades.Paquete)
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnNuevo.Location = New System.Drawing.Point(583, 21)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(109, 35)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnEliminar.Location = New System.Drawing.Point(583, 109)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(109, 35)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnModificar.Location = New System.Drawing.Point(583, 65)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(109, 35)
        Me.btnModificar.TabIndex = 3
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupBox1.Controls.Add(Me.btnModificar)
        Me.GroupBox1.Controls.Add(Me.btnNuevo)
        Me.GroupBox1.Controls.Add(Me.btnEliminar)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(698, 462)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Detalle de paquetes"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.panelEdicion)
        Me.FlowLayoutPanel1.Controls.Add(Me.dgvPaquetes)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(6, 21)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(569, 435)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'panelEdicion
        '
        Me.panelEdicion.BackColor = System.Drawing.Color.LightSteelBlue
        Me.panelEdicion.Controls.Add(Me.btnCancelar)
        Me.panelEdicion.Controls.Add(Me.btnAceptar)
        Me.panelEdicion.Controls.Add(Me.clbTipos)
        Me.panelEdicion.Controls.Add(Me.Label6)
        Me.panelEdicion.Controls.Add(Me.Label5)
        Me.panelEdicion.Controls.Add(Me.nudPrecio)
        Me.panelEdicion.Controls.Add(Me.nudDescuento)
        Me.panelEdicion.Controls.Add(Me.txtLeyenda)
        Me.panelEdicion.Controls.Add(Me.txtNombrePaquete)
        Me.panelEdicion.Controls.Add(Me.Label4)
        Me.panelEdicion.Controls.Add(Me.Label3)
        Me.panelEdicion.Controls.Add(Me.Label2)
        Me.panelEdicion.Controls.Add(Me.Label1)
        Me.panelEdicion.Location = New System.Drawing.Point(0, 0)
        Me.panelEdicion.Margin = New System.Windows.Forms.Padding(0)
        Me.panelEdicion.Name = "panelEdicion"
        Me.panelEdicion.Size = New System.Drawing.Size(569, 185)
        Me.panelEdicion.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelar.Location = New System.Drawing.Point(26, 138)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(109, 35)
        Me.btnCancelar.TabIndex = 11
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnAceptar.Location = New System.Drawing.Point(261, 138)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(109, 35)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'clbTipos
        '
        Me.clbTipos.BackColor = System.Drawing.Color.LightSteelBlue
        Me.clbTipos.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.clbTipos.CheckOnClick = True
        Me.clbTipos.FormattingEnabled = True
        Me.clbTipos.Location = New System.Drawing.Point(402, 20)
        Me.clbTipos.Name = "clbTipos"
        Me.clbTipos.Size = New System.Drawing.Size(164, 153)
        Me.clbTipos.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(256, 38)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(14, 22)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "$"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(23, 87)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(14, 22)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "$"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudPrecio
        '
        Me.nudPrecio.DecimalPlaces = 2
        Me.nudPrecio.Location = New System.Drawing.Point(273, 38)
        Me.nudPrecio.Margin = New System.Windows.Forms.Padding(0)
        Me.nudPrecio.Maximum = New Decimal(New Integer() {-1, -1, -1, 0})
        Me.nudPrecio.Name = "nudPrecio"
        Me.nudPrecio.Size = New System.Drawing.Size(97, 22)
        Me.nudPrecio.TabIndex = 7
        Me.nudPrecio.ThousandsSeparator = True
        '
        'nudDescuento
        '
        Me.nudDescuento.DecimalPlaces = 2
        Me.nudDescuento.Location = New System.Drawing.Point(40, 87)
        Me.nudDescuento.Margin = New System.Windows.Forms.Padding(0)
        Me.nudDescuento.Maximum = New Decimal(New Integer() {-1, -1, -1, 0})
        Me.nudDescuento.Name = "nudDescuento"
        Me.nudDescuento.Size = New System.Drawing.Size(97, 22)
        Me.nudDescuento.TabIndex = 6
        Me.nudDescuento.ThousandsSeparator = True
        '
        'txtLeyenda
        '
        Me.txtLeyenda.Location = New System.Drawing.Point(176, 87)
        Me.txtLeyenda.Name = "txtLeyenda"
        Me.txtLeyenda.Size = New System.Drawing.Size(194, 22)
        Me.txtLeyenda.TabIndex = 5
        '
        'txtNombrePaquete
        '
        Me.txtNombrePaquete.Location = New System.Drawing.Point(26, 38)
        Me.txtNombrePaquete.Name = "txtNombrePaquete"
        Me.txtNombrePaquete.Size = New System.Drawing.Size(194, 22)
        Me.txtNombrePaquete.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 15)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Descuento"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(270, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Precio"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(173, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(128, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Leyenda del descuento"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nombre del paquete"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ConfiguracionPaquetesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(722, 486)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "ConfiguracionPaquetesForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Configuración de Paquetes"
        CType(Me.dgvPaquetes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PaqueteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.panelEdicion.ResumeLayout(False)
        Me.panelEdicion.PerformLayout()
        CType(Me.nudPrecio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDescuento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvPaquetes As System.Windows.Forms.DataGridView
    Friend WithEvents PaqueteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents panelEdicion As System.Windows.Forms.Panel
    Friend WithEvents clbTipos As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents nudPrecio As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudDescuento As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtLeyenda As System.Windows.Forms.TextBox
    Friend WithEvents txtNombrePaquete As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents NombreDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreTipoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descuento As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
