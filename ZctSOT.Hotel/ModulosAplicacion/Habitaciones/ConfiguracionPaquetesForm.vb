﻿Imports ZctSOT.Hotel.Controladores
Imports Transversal.Extensiones
Imports Transversal.Excepciones

Public Class ConfiguracionPaquetesForm

    Private esNuevo As Boolean = False
    Private paqueteActual As Modelo.Entidades.Paquete

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        txtNombrePaquete.Clear()
        txtLeyenda.Clear()
        LimpiarChecksTipos(-1)

        nudDescuento.Value = 0
        nudPrecio.Value = 0

        esNuevo = True

        paqueteActual = New Modelo.Entidades.Paquete

        panelEdicion.Show()

        btnNuevo.Enabled = False
        btnModificar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If dgvPaquetes.SelectedRows.Count <> 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_paquetes_seleccionados)
        End If

        Dim paquete As Modelo.Entidades.Paquete = CType(dgvPaquetes.SelectedRows(0).DataBoundItem, Modelo.Entidades.Paquete)

        txtNombrePaquete.Text = paquete.Nombre
        txtLeyenda.Text = paquete.LeyendaDescuento

        For ix As Integer = 0 To clbTipos.Items.Count - 1

            Dim item As Modelo.Entidades.TipoHabitacion = CType(clbTipos.Items(ix), Modelo.Entidades.TipoHabitacion)

            clbTipos.SetItemChecked(ix, item.Id = paquete.IdTipoHabitacion)
            clbTipos.SetSelected(ix, item.Id = paquete.IdTipoHabitacion)
        Next

        nudDescuento.Value = paquete.Descuento
        nudPrecio.Value = paquete.Precio

        esNuevo = False

        paqueteActual = paquete

        panelEdicion.Show()

        btnNuevo.Enabled = False
        btnModificar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub ConfiguracionPaquetesForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarPaquetes()
        CargarTipos()

        panelEdicion.Hide()
    End Sub

    Private Sub CargarPaquetes()
        Dim paquetes As List(Of Modelo.Entidades.Paquete) = New Controladores.ControladorGlobal().AplicacionServicioPaquetes.ObtenerPaquetesActivos()

        dgvPaquetes.DataSource = paquetes

        dgvPaquetes.Columns(2).DefaultCellStyle.Format = "c2"
        dgvPaquetes.Columns(3).DefaultCellStyle.Format = "c2"
    End Sub

    Private Sub CargarTipos()
        clbTipos.DataSource = New Controladores.ControladorGlobal().AplicacionServicioTiposHabitaciones.ObtenerTiposActivos()
        clbTipos.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.TipoHabitacion, String)(Function(m) m.Descripcion)
        clbTipos.ValueMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.TipoHabitacion, Integer)(Function(m) m.Id)
    End Sub

    Private Sub LimpiarChecksTipos(index As Integer)
        For ix As Integer = 0 To clbTipos.Items.Count - 1
            If ix <> index Then
                clbTipos.SetItemChecked(ix, False)
                clbTipos.SetSelected(ix, False)
            End If
        Next
    End Sub

    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles clbTipos.ItemCheck
        LimpiarChecksTipos(e.Index)
    End Sub

    Private Sub panelEdicion_VisibleChanged(sender As Object, e As EventArgs) Handles panelEdicion.VisibleChanged
        If panelEdicion.Visible Then
            dgvPaquetes.Size = dgvPaquetes.MinimumSize
        Else
            dgvPaquetes.Size = FlowLayoutPanel1.Size
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        panelEdicion.Hide()
        esNuevo = False

        btnNuevo.Enabled = True
        btnModificar.Enabled = True
        btnEliminar.Enabled = True
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim controlador As New ControladorGlobal

        paqueteActual.Nombre = txtNombrePaquete.Text
        paqueteActual.LeyendaDescuento = txtLeyenda.Text
        paqueteActual.Precio = nudPrecio.Value
        paqueteActual.Descuento = nudDescuento.Value

        If clbTipos.CheckedItems.Count = 0 Then
            Throw New SOTException(My.Resources.Errores.tipo_habitacion_no_seleccionado_exception)
        End If

        Dim item As Modelo.Entidades.TipoHabitacion = CType(clbTipos.CheckedItems(0), Modelo.Entidades.TipoHabitacion)

        paqueteActual.IdTipoHabitacion = item.Id

        If esNuevo Then
            controlador.AplicacionServicioPaquetes.CrearPaquete(paqueteActual, controlador.UsuarioActual)
        Else
            controlador.AplicacionServicioPaquetes.ModificarPaquete(paqueteActual, controlador.UsuarioActual)
        End If

        ConfiguracionPaquetesForm_Load(Nothing, Nothing)

        btnNuevo.Enabled = True
        btnModificar.Enabled = True
        btnEliminar.Enabled = True

        If esNuevo Then
            MessageBox.Show(My.Resources.Mensajes.creacion_paquete_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show(My.Resources.Mensajes.modificacion_paquete_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvPaquetes.SelectedRows.Count <> 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_paquetes_seleccionados)
        End If

        Dim paquete As Modelo.Entidades.Paquete = CType(dgvPaquetes.SelectedRows(0).DataBoundItem, Modelo.Entidades.Paquete)

        If MessageBox.Show(String.Format(My.Resources.Mensajes.eliminar_paquete, paquete.Nombre, paquete.NombreTipo),
                           My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim controlador As New ControladorGlobal

            controlador.AplicacionServicioPaquetes.EliminarPaquete(paquete.Id, controlador.UsuarioActual)

            MessageBox.Show(My.Resources.Mensajes.eliminacion_paquete_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)

            ConfiguracionPaquetesForm_Load(Nothing, Nothing)
        End If
    End Sub
End Class