﻿Public Class CapturaMotivo

    Public Event Aceptar()

    Private _motivo As String = ""
    Private ReadOnly motivoOriginal As String

    Property Motivo As String
        Get
            Return _motivo
        End Get
        Set(value As String)
            _motivo = value
            If Not String.IsNullOrWhiteSpace(_motivo) Then
                grupoMotivo.Text = String.Format(My.Resources.TitulosComponentes.TituloMotivo, _motivo)
            Else
                grupoMotivo.Text = motivoOriginal
            End If
        End Set
    End Property

    Property DescripcionMotivo As String
        Get
            Return txtMotivo.Text
        End Get
        Set(value As String)
            txtMotivo.Text = value
        End Set
    End Property

    Property TextoAuxiliar As String
        Get
            Return lblDetalleExtra.Text
        End Get
        Set(value As String)
            lblDetalleExtra.Text = value
        End Set
    End Property

    Property SoloLectura As Boolean
        Get
            Return txtMotivo.ReadOnly
        End Get
        Set(value As Boolean)
            txtMotivo.ReadOnly = value
        End Set
    End Property

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        motivoOriginal = grupoMotivo.Text
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        RaiseEvent Aceptar()
        Me.Close()
    End Sub
End Class