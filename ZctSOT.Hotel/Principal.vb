﻿Imports Microsoft.Win32
Imports System.Configuration
Imports ZctSOT.Hotel.My.Resources

Public Class Principal

    'Public ParametrosSistema As New Datos.Clases.Sistema.ZctCatPar

    'Private DAOSistema As New Datos.DAO.ClProcesaCatalogos()

    Private _habitacionesForm As ZctHabitaciones

    Public Sub New()

        ' Llamada necesaria para el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().


        _habitacionesForm = New ZctHabitaciones

#If DEBUG Then
        Text += " " + System.Configuration.ConfigurationManager.AppSettings("versionAplicacion")
#End If

    End Sub

    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            'Dim splash As New ZctIntro
            CreaLlaves()
            'Actualizaciones

            'splash.ShowDialog()

            'DESCOMENTAR EL BLOQUE DE ABAJO
            'Dim fLogin As New ZctLogin
            'fLogin.Cod_Aut = 0
            'fLogin.ShowDialog(Me)
            'If fLogin.Valida = False Then
            '    Me.Dispose()
            'End If


            '_Controlador = New Permisos.Controlador.Controlador(Datos.DAO.Coneccion.CadenaConexion)
            'If (IsNumeric(_UsuAct)) Then
            '    _Controlador.Recorrermenu(_UsuAct, Me.MenuStrip)
            'End If
            'MenuStrip.Visible = True
            'ActualizacionesIniciales()

            '' ''DESCOMETAR LA LÍNEA DE ABAJO
            '' ''ActualizaParametros()

            'GetPendientesPorEnviar()
            'If _EsVendedor Then
            '    CreaCaja(0)
            'End If

            '_habitacionesForm.TopLevel = False
            _habitacionesForm.Visible = True
            '_habitacionesForm.FormBorderStyle = Windows.Forms.FormBorderStyle.None
            contenedor.Controls.Add(_habitacionesForm)
            '_habitacionesForm.WindowState = FormWindowState.Maximized
            _habitacionesForm.Dock = DockStyle.Fill
            _habitacionesForm.AutoSize = True
            _habitacionesForm.AutoSizeMode = Windows.Forms.AutoSizeMode.GrowAndShrink
            '_habitacionesForm.ActualizarHabitaciones()
            'AddHandler _habitacionesForm.MenuClick, AddressOf CambiarVisibilidadMenuPrincipal

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CreaLlaves()
        Try
            'Dim ruta As String = "Software\ODBC\ODBC.INI\ODBC File DSN"
            'HKEY_LOCAL_MACHINE\Software 
            Dim key As RegistryKey = Registry.LocalMachine.OpenSubKey("Software", True)
            If key.OpenSubKey("ZctSOT") Is Nothing Then
                Dim newkey As RegistryKey = key.CreateSubKey("ZctSOT")
                newkey.SetValue("Server", ConfigurationManager.AppSettings.Get("Server"))
                newkey.SetValue("Database", ConfigurationManager.AppSettings.Get("Database"))
                newkey.SetValue("User", ConfigurationManager.AppSettings.Get("User"))
            End If
        Catch ex As Exception
            Trace.Write(ex.Message)
            Trace.Flush()
        End Try

    End Sub

    'Public Sub ActualizaParametros()
    '    If ParametrosSistema Is Nothing Then ParametrosSistema = New Datos.Clases.Sistema.ZctCatPar
    '    DAOSistema.CargaDatos(ParametrosSistema)
    'End Sub

    Private Sub tsmiHorariosPrecios_Click(sender As Object, e As EventArgs) Handles tsmiHorariosPrecios.Click
        Dim hpf As HorariosPreciosForm = New HorariosPreciosForm()
        hpf.ShowDialog(Me)
    End Sub

    Private Sub tsmiSalirSistema_Click(sender As Object, e As EventArgs) Handles tsmiSalirSistema.Click
        Close()
    End Sub

    Private Sub tsmiPaquetes_Click(sender As Object, e As EventArgs) Handles tsmiPaquetes.Click
        Dim configuracionPaquetesF As ConfiguracionPaquetesForm = New ConfiguracionPaquetesForm()
        configuracionPaquetesF.ShowDialog(Me)
    End Sub

    Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
        If MessageBox.Show(Mensajes.salir_sistema, TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
            e.Cancel = True
        End If
        MyBase.OnClosing(e)
    End Sub

    Private Sub tsmiReservaciones_Click(sender As Object, e As EventArgs) Handles tsmiReservaciones.Click
        Dim reservacionesForm = New ReservacionesForm()

        reservacionesForm.ShowDialog(Me)
    End Sub
End Class