﻿Imports System.Linq
Imports ExtraComponents

Public Class ListaRecamarerasForm

    Public Sub ListaRecamarerasForm_Load() Handles MyBase.Load
        CargarRecamareras()
    End Sub

    Private Sub CargarRecamareras()
        For Each gelBtn As Control In flpDisponibles.Controls
            gelBtn.Dispose()
        Next

        flpDisponibles.Controls.Clear()

        Dim controlador As New Controladores.ControladorGlobal()
        Dim controladorTareasL As New Controladores.ControladorTareasLimpieza

        Dim empleados As List(Of Modelo.Entidades.Empleado) = controlador.AplicacionServicioEmpleados.ObtenerRecamareras(controlador.UsuarioActual)
        Dim empleadosTareas As List(Of Modelo.Entidades.LimpiezaEmpleado) = controladorTareasL.ObtenerEmpleadosLimpiezaActivos()


        CrearBotones(flpDisponibles, empleados, empleadosTareas, Color.FromArgb(252, 217, 252))

    End Sub

    Private Shared Sub CrearBotones(flp As FlowLayoutPanel, empleados As List(Of Modelo.Entidades.Empleado), empleadosTareas As List(Of Modelo.Entidades.LimpiezaEmpleado), colorBoton As Color)

        For Each datosEmpleado In (From empleado In empleados
                                   Group Join empleadoLimpieza In empleadosTareas On empleado.Id Equals empleadoLimpieza.IdEmpleado Into Group
                                   From empleadoLimpieza In Group.DefaultIfEmpty()
                                   Select empleado, empleadoLimpieza)
            Dim gelBtn As New GelButton()
            gelBtn.Width = flp.Width - 20
            gelBtn.Height = 30

            If IsNothing(datosEmpleado.empleadoLimpieza) Then
                gelBtn.Text = datosEmpleado.empleado.NombreCompleto
            Else
                gelBtn.Text = datosEmpleado.empleado.NombreCompleto + " - " + datosEmpleado.empleadoLimpieza.NumeroHabitacionActual
            End If

            gelBtn.TextAlign = ContentAlignment.MiddleRight
            gelBtn.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
            gelBtn.Margin = New Padding(0)
            gelBtn.Padding = gelBtn.Margin
            gelBtn.GradientBottom = Color.White
            gelBtn.GradientTop = colorBoton
            gelBtn.Enabled = False
            flp.Controls.Add(gelBtn)

        Next
    End Sub

End Class