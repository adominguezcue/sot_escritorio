﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HorariosPreciosForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbTarifas = New System.Windows.Forms.ComboBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.gbHotel = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvTiposHabitaciones = New System.Windows.Forms.DataGridView()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioHotelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioMotelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosPrimerHospedajeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosSegundoHospedajeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosLimpiezaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosLavadoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosDetalladoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosEntradaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MinutosSuciaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoHabitacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.panelEdicion = New System.Windows.Forms.Panel()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.CampoPersonalizado16 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado17 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado13 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado14 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado15 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado10 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado11 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado12 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado1 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado2 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado3 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado7 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado6 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado8 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado5 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado9 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.CampoPersonalizado4 = New ZctSOT.Hotel.CampoPersonalizado()
        Me.gbHotel.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvTiposHabitaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TipoHabitacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.panelEdicion.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbTarifas
        '
        Me.cbTarifas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbTarifas.FormattingEnabled = True
        Me.cbTarifas.Location = New System.Drawing.Point(4, 30)
        Me.cbTarifas.Name = "cbTarifas"
        Me.cbTarifas.Size = New System.Drawing.Size(173, 21)
        Me.cbTarifas.TabIndex = 42
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CheckBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CheckBox1.Location = New System.Drawing.Point(6, 228)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(199, 19)
        Me.CheckBox1.TabIndex = 43
        Me.CheckBox1.Text = "Omitir en reporte de recamareras"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'gbHotel
        '
        Me.gbHotel.Controls.Add(Me.CampoPersonalizado10)
        Me.gbHotel.Controls.Add(Me.CampoPersonalizado11)
        Me.gbHotel.Controls.Add(Me.CampoPersonalizado12)
        Me.gbHotel.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.gbHotel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.gbHotel.Location = New System.Drawing.Point(417, 8)
        Me.gbHotel.Name = "gbHotel"
        Me.gbHotel.Size = New System.Drawing.Size(152, 205)
        Me.gbHotel.TabIndex = 44
        Me.gbHotel.TabStop = False
        Me.gbHotel.Text = "Hotel"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CampoPersonalizado16)
        Me.GroupBox2.Controls.Add(Me.CampoPersonalizado17)
        Me.GroupBox2.Controls.Add(Me.CampoPersonalizado13)
        Me.GroupBox2.Controls.Add(Me.CampoPersonalizado14)
        Me.GroupBox2.Controls.Add(Me.CampoPersonalizado15)
        Me.GroupBox2.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.GroupBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.GroupBox2.Location = New System.Drawing.Point(591, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(280, 205)
        Me.GroupBox2.TabIndex = 45
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Motel"
        '
        'dgvTiposHabitaciones
        '
        Me.dgvTiposHabitaciones.AllowUserToAddRows = False
        Me.dgvTiposHabitaciones.AllowUserToDeleteRows = False
        Me.dgvTiposHabitaciones.AutoGenerateColumns = False
        Me.dgvTiposHabitaciones.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.dgvTiposHabitaciones.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTiposHabitaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTiposHabitaciones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DescripcionDataGridViewTextBoxColumn, Me.PrecioHotelDataGridViewTextBoxColumn, Me.PrecioMotelDataGridViewTextBoxColumn, Me.MinutosPrimerHospedajeDataGridViewTextBoxColumn, Me.MinutosSegundoHospedajeDataGridViewTextBoxColumn, Me.MinutosLimpiezaDataGridViewTextBoxColumn, Me.MinutosLavadoDataGridViewTextBoxColumn, Me.MinutosDetalladoDataGridViewTextBoxColumn, Me.MinutosEntradaDataGridViewTextBoxColumn, Me.MinutosSuciaDataGridViewTextBoxColumn})
        Me.dgvTiposHabitaciones.DataSource = Me.TipoHabitacionBindingSource
        Me.dgvTiposHabitaciones.Location = New System.Drawing.Point(3, 274)
        Me.dgvTiposHabitaciones.MinimumSize = New System.Drawing.Size(880, 300)
        Me.dgvTiposHabitaciones.Name = "dgvTiposHabitaciones"
        Me.dgvTiposHabitaciones.ReadOnly = True
        Me.dgvTiposHabitaciones.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        Me.dgvTiposHabitaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTiposHabitaciones.Size = New System.Drawing.Size(880, 300)
        Me.dgvTiposHabitaciones.TabIndex = 49
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Descripción"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PrecioHotelDataGridViewTextBoxColumn
        '
        Me.PrecioHotelDataGridViewTextBoxColumn.DataPropertyName = "PrecioHotel"
        Me.PrecioHotelDataGridViewTextBoxColumn.HeaderText = "Precio Hotel"
        Me.PrecioHotelDataGridViewTextBoxColumn.Name = "PrecioHotelDataGridViewTextBoxColumn"
        Me.PrecioHotelDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PrecioMotelDataGridViewTextBoxColumn
        '
        Me.PrecioMotelDataGridViewTextBoxColumn.DataPropertyName = "PrecioMotel"
        Me.PrecioMotelDataGridViewTextBoxColumn.HeaderText = "Precio Motel"
        Me.PrecioMotelDataGridViewTextBoxColumn.Name = "PrecioMotelDataGridViewTextBoxColumn"
        Me.PrecioMotelDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosPrimerHospedajeDataGridViewTextBoxColumn
        '
        Me.MinutosPrimerHospedajeDataGridViewTextBoxColumn.DataPropertyName = "MinutosPrimerHospedaje"
        Me.MinutosPrimerHospedajeDataGridViewTextBoxColumn.HeaderText = "Hospedaje 1"
        Me.MinutosPrimerHospedajeDataGridViewTextBoxColumn.Name = "MinutosPrimerHospedajeDataGridViewTextBoxColumn"
        Me.MinutosPrimerHospedajeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosSegundoHospedajeDataGridViewTextBoxColumn
        '
        Me.MinutosSegundoHospedajeDataGridViewTextBoxColumn.DataPropertyName = "MinutosSegundoHospedaje"
        Me.MinutosSegundoHospedajeDataGridViewTextBoxColumn.HeaderText = "Hospedaje 2"
        Me.MinutosSegundoHospedajeDataGridViewTextBoxColumn.Name = "MinutosSegundoHospedajeDataGridViewTextBoxColumn"
        Me.MinutosSegundoHospedajeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosLimpiezaDataGridViewTextBoxColumn
        '
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.DataPropertyName = "MinutosLimpieza"
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.HeaderText = "Limpieza"
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.Name = "MinutosLimpiezaDataGridViewTextBoxColumn"
        Me.MinutosLimpiezaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosLavadoDataGridViewTextBoxColumn
        '
        Me.MinutosLavadoDataGridViewTextBoxColumn.DataPropertyName = "MinutosLavado"
        Me.MinutosLavadoDataGridViewTextBoxColumn.HeaderText = "Lavado"
        Me.MinutosLavadoDataGridViewTextBoxColumn.Name = "MinutosLavadoDataGridViewTextBoxColumn"
        Me.MinutosLavadoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosDetalladoDataGridViewTextBoxColumn
        '
        Me.MinutosDetalladoDataGridViewTextBoxColumn.DataPropertyName = "MinutosDetallado"
        Me.MinutosDetalladoDataGridViewTextBoxColumn.HeaderText = "Detallado"
        Me.MinutosDetalladoDataGridViewTextBoxColumn.Name = "MinutosDetalladoDataGridViewTextBoxColumn"
        Me.MinutosDetalladoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosEntradaDataGridViewTextBoxColumn
        '
        Me.MinutosEntradaDataGridViewTextBoxColumn.DataPropertyName = "MinutosEntrada"
        Me.MinutosEntradaDataGridViewTextBoxColumn.HeaderText = "T Entrada"
        Me.MinutosEntradaDataGridViewTextBoxColumn.Name = "MinutosEntradaDataGridViewTextBoxColumn"
        Me.MinutosEntradaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'MinutosSuciaDataGridViewTextBoxColumn
        '
        Me.MinutosSuciaDataGridViewTextBoxColumn.DataPropertyName = "MinutosSucia"
        Me.MinutosSuciaDataGridViewTextBoxColumn.HeaderText = "T Sucia"
        Me.MinutosSuciaDataGridViewTextBoxColumn.Name = "MinutosSuciaDataGridViewTextBoxColumn"
        Me.MinutosSuciaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TipoHabitacionBindingSource
        '
        Me.TipoHabitacionBindingSource.DataSource = GetType(Modelo.Entidades.TipoHabitacion)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 15)
        Me.Label1.TabIndex = 50
        Me.Label1.Text = "Tipo"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupBox3.Controls.Add(Me.btnModificar)
        Me.GroupBox3.Controls.Add(Me.btnNuevo)
        Me.GroupBox3.Controls.Add(Me.btnEliminar)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 29)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(0)
        Me.GroupBox3.Size = New System.Drawing.Size(1018, 610)
        Me.GroupBox3.TabIndex = 51
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Horarios y precios por tipo de habitación"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.panelEdicion)
        Me.FlowLayoutPanel1.Controls.Add(Me.dgvTiposHabitaciones)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(6, 21)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(880, 586)
        Me.FlowLayoutPanel1.TabIndex = 4
        '
        'panelEdicion
        '
        Me.panelEdicion.BackColor = System.Drawing.Color.Transparent
        Me.panelEdicion.Controls.Add(Me.btnCancelar)
        Me.panelEdicion.Controls.Add(Me.Label1)
        Me.panelEdicion.Controls.Add(Me.GroupBox2)
        Me.panelEdicion.Controls.Add(Me.btnAceptar)
        Me.panelEdicion.Controls.Add(Me.gbHotel)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado1)
        Me.panelEdicion.Controls.Add(Me.CheckBox1)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado2)
        Me.panelEdicion.Controls.Add(Me.cbTarifas)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado3)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado7)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado6)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado8)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado5)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado9)
        Me.panelEdicion.Controls.Add(Me.CampoPersonalizado4)
        Me.panelEdicion.Location = New System.Drawing.Point(0, 0)
        Me.panelEdicion.Margin = New System.Windows.Forms.Padding(0)
        Me.panelEdicion.Name = "panelEdicion"
        Me.panelEdicion.Size = New System.Drawing.Size(880, 271)
        Me.panelEdicion.TabIndex = 1
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelar.Location = New System.Drawing.Point(647, 228)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(109, 35)
        Me.btnCancelar.TabIndex = 11
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnAceptar.Location = New System.Drawing.Point(762, 228)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(109, 35)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnModificar.Location = New System.Drawing.Point(901, 65)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(109, 35)
        Me.btnModificar.TabIndex = 3
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnNuevo.Location = New System.Drawing.Point(901, 21)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(109, 35)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnEliminar.Location = New System.Drawing.Point(901, 109)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(109, 35)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'CampoPersonalizado16
        '
        Me.CampoPersonalizado16.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado16.Location = New System.Drawing.Point(144, 80)
        Me.CampoPersonalizado16.Name = "CampoPersonalizado16"
        Me.CampoPersonalizado16.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado16.TabIndex = 45
        Me.CampoPersonalizado16.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado16.Title = "T Hospedaje 2"
        Me.CampoPersonalizado16.Value = ""
        '
        'CampoPersonalizado17
        '
        Me.CampoPersonalizado17.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado17.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado17.Location = New System.Drawing.Point(144, 26)
        Me.CampoPersonalizado17.Name = "CampoPersonalizado17"
        Me.CampoPersonalizado17.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado17.TabIndex = 44
        Me.CampoPersonalizado17.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado17.Title = "T Hospedaje 1"
        Me.CampoPersonalizado17.Value = ""
        '
        'CampoPersonalizado13
        '
        Me.CampoPersonalizado13.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado13.Location = New System.Drawing.Point(13, 80)
        Me.CampoPersonalizado13.Name = "CampoPersonalizado13"
        Me.CampoPersonalizado13.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado13.TabIndex = 43
        Me.CampoPersonalizado13.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado13.Title = "Persona extra"
        Me.CampoPersonalizado13.Value = ""
        '
        'CampoPersonalizado14
        '
        Me.CampoPersonalizado14.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado14.Location = New System.Drawing.Point(13, 26)
        Me.CampoPersonalizado14.Name = "CampoPersonalizado14"
        Me.CampoPersonalizado14.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado14.TabIndex = 42
        Me.CampoPersonalizado14.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado14.Title = "Precio"
        Me.CampoPersonalizado14.Value = ""
        '
        'CampoPersonalizado15
        '
        Me.CampoPersonalizado15.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado15.Location = New System.Drawing.Point(13, 134)
        Me.CampoPersonalizado15.Name = "CampoPersonalizado15"
        Me.CampoPersonalizado15.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado15.TabIndex = 41
        Me.CampoPersonalizado15.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado15.Title = "Hospedaje extra"
        Me.CampoPersonalizado15.Value = ""
        '
        'CampoPersonalizado10
        '
        Me.CampoPersonalizado10.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado10.Location = New System.Drawing.Point(13, 80)
        Me.CampoPersonalizado10.Name = "CampoPersonalizado10"
        Me.CampoPersonalizado10.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado10.TabIndex = 43
        Me.CampoPersonalizado10.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado10.Title = "Persona extra"
        Me.CampoPersonalizado10.Value = ""
        '
        'CampoPersonalizado11
        '
        Me.CampoPersonalizado11.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado11.Location = New System.Drawing.Point(13, 26)
        Me.CampoPersonalizado11.Name = "CampoPersonalizado11"
        Me.CampoPersonalizado11.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado11.TabIndex = 42
        Me.CampoPersonalizado11.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado11.Title = "Precio"
        Me.CampoPersonalizado11.Value = ""
        '
        'CampoPersonalizado12
        '
        Me.CampoPersonalizado12.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado12.Location = New System.Drawing.Point(13, 134)
        Me.CampoPersonalizado12.Name = "CampoPersonalizado12"
        Me.CampoPersonalizado12.Size = New System.Drawing.Size(125, 45)
        Me.CampoPersonalizado12.TabIndex = 41
        Me.CampoPersonalizado12.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado12.Title = "Hospedaje extra"
        Me.CampoPersonalizado12.Value = ""
        '
        'CampoPersonalizado1
        '
        Me.CampoPersonalizado1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado1.Location = New System.Drawing.Point(4, 168)
        Me.CampoPersonalizado1.Name = "CampoPersonalizado1"
        Me.CampoPersonalizado1.Size = New System.Drawing.Size(239, 45)
        Me.CampoPersonalizado1.TabIndex = 33
        Me.CampoPersonalizado1.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado1.Title = "Descripción extendida"
        Me.CampoPersonalizado1.Value = ""
        '
        'CampoPersonalizado2
        '
        Me.CampoPersonalizado2.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado2.Location = New System.Drawing.Point(202, 8)
        Me.CampoPersonalizado2.Name = "CampoPersonalizado2"
        Me.CampoPersonalizado2.Size = New System.Drawing.Size(165, 45)
        Me.CampoPersonalizado2.TabIndex = 34
        Me.CampoPersonalizado2.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado2.Title = "Descripción"
        Me.CampoPersonalizado2.Value = ""
        '
        'CampoPersonalizado3
        '
        Me.CampoPersonalizado3.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado3.Location = New System.Drawing.Point(252, 168)
        Me.CampoPersonalizado3.Name = "CampoPersonalizado3"
        Me.CampoPersonalizado3.Size = New System.Drawing.Size(115, 45)
        Me.CampoPersonalizado3.TabIndex = 35
        Me.CampoPersonalizado3.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado3.Title = "Reservaciones"
        Me.CampoPersonalizado3.Value = ""
        '
        'CampoPersonalizado7
        '
        Me.CampoPersonalizado7.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado7.Location = New System.Drawing.Point(128, 114)
        Me.CampoPersonalizado7.Name = "CampoPersonalizado7"
        Me.CampoPersonalizado7.Size = New System.Drawing.Size(115, 45)
        Me.CampoPersonalizado7.TabIndex = 41
        Me.CampoPersonalizado7.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado7.Title = "Puntos Limpieza"
        Me.CampoPersonalizado7.Value = ""
        '
        'CampoPersonalizado6
        '
        Me.CampoPersonalizado6.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado6.Location = New System.Drawing.Point(4, 60)
        Me.CampoPersonalizado6.Name = "CampoPersonalizado6"
        Me.CampoPersonalizado6.Size = New System.Drawing.Size(115, 45)
        Me.CampoPersonalizado6.TabIndex = 36
        Me.CampoPersonalizado6.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado6.Title = "T Detallado"
        Me.CampoPersonalizado6.Value = ""
        '
        'CampoPersonalizado8
        '
        Me.CampoPersonalizado8.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado8.Location = New System.Drawing.Point(252, 114)
        Me.CampoPersonalizado8.Name = "CampoPersonalizado8"
        Me.CampoPersonalizado8.Size = New System.Drawing.Size(115, 45)
        Me.CampoPersonalizado8.TabIndex = 40
        Me.CampoPersonalizado8.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado8.Title = "Puntos Lavado"
        Me.CampoPersonalizado8.Value = ""
        '
        'CampoPersonalizado5
        '
        Me.CampoPersonalizado5.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado5.Location = New System.Drawing.Point(252, 60)
        Me.CampoPersonalizado5.Name = "CampoPersonalizado5"
        Me.CampoPersonalizado5.Size = New System.Drawing.Size(115, 45)
        Me.CampoPersonalizado5.TabIndex = 37
        Me.CampoPersonalizado5.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado5.Title = "T Lavado"
        Me.CampoPersonalizado5.Value = ""
        '
        'CampoPersonalizado9
        '
        Me.CampoPersonalizado9.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado9.Location = New System.Drawing.Point(4, 114)
        Me.CampoPersonalizado9.Name = "CampoPersonalizado9"
        Me.CampoPersonalizado9.Size = New System.Drawing.Size(115, 45)
        Me.CampoPersonalizado9.TabIndex = 39
        Me.CampoPersonalizado9.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado9.Title = "Puntos Detallado"
        Me.CampoPersonalizado9.Value = ""
        '
        'CampoPersonalizado4
        '
        Me.CampoPersonalizado4.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.CampoPersonalizado4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(62, Byte), Integer), CType(CType(71, Byte), Integer))
        Me.CampoPersonalizado4.Location = New System.Drawing.Point(128, 60)
        Me.CampoPersonalizado4.Name = "CampoPersonalizado4"
        Me.CampoPersonalizado4.Size = New System.Drawing.Size(115, 45)
        Me.CampoPersonalizado4.TabIndex = 38
        Me.CampoPersonalizado4.TipoCampo = ZctSOT.Hotel.CampoPersonalizado.TiposCampo.Texto
        Me.CampoPersonalizado4.Title = "T Limpieza"
        Me.CampoPersonalizado4.Value = ""
        '
        'HorariosPreciosForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1036, 648)
        Me.Controls.Add(Me.GroupBox3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "HorariosPreciosForm"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Horarios y precios"
        Me.gbHotel.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvTiposHabitaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TipoHabitacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.panelEdicion.ResumeLayout(False)
        Me.panelEdicion.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CampoPersonalizado1 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado2 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado3 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado4 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado5 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado6 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado7 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado8 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado9 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents cbTarifas As System.Windows.Forms.ComboBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents gbHotel As System.Windows.Forms.GroupBox
    Friend WithEvents CampoPersonalizado10 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado11 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado12 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CampoPersonalizado16 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado17 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado13 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado14 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents CampoPersonalizado15 As ZctSOT.Hotel.CampoPersonalizado
    Friend WithEvents dgvTiposHabitaciones As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioHotelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioMotelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdConfiguracionNegocioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosPrimerHospedajeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosSegundoHospedajeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosLimpiezaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosLavadoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosDetalladoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosEntradaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinutosSuciaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoHabitacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents panelEdicion As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
End Class
