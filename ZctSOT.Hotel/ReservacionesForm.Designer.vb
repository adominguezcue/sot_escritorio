﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReservacionesForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgvReservaciones = New System.Windows.Forms.DataGridView()
        Me.CodigoReservaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaEntradaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaSalidaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NochesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EstadoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreTipoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PersonasExtraDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaquetesReservaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaCreacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Observaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReservacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnModificarReservacion = New ExtraComponents.GelButton()
        Me.btnCancelarReservacion = New ExtraComponents.GelButton()
        Me.btnCrearReservacion = New ExtraComponents.GelButton()
        Me.btnImprimir = New ExtraComponents.GelButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpInicio = New System.Windows.Forms.DateTimePicker()
        Me.dtpLimite = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbTiposHabitaciones = New System.Windows.Forms.ComboBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        CType(Me.dgvReservaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReservacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvReservaciones
        '
        Me.dgvReservaciones.AllowUserToAddRows = False
        Me.dgvReservaciones.AllowUserToDeleteRows = False
        Me.dgvReservaciones.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvReservaciones.AutoGenerateColumns = False
        Me.dgvReservaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReservaciones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodigoReservaDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.FechaEntradaDataGridViewTextBoxColumn, Me.FechaSalidaDataGridViewTextBoxColumn, Me.NochesDataGridViewTextBoxColumn, Me.EstadoDataGridViewTextBoxColumn, Me.NombreTipoDataGridViewTextBoxColumn, Me.PersonasExtraDataGridViewTextBoxColumn, Me.PaquetesReservaciones, Me.FechaCreacion, Me.Observaciones})
        Me.dgvReservaciones.DataSource = Me.ReservacionBindingSource
        Me.dgvReservaciones.Location = New System.Drawing.Point(14, 193)
        Me.dgvReservaciones.Margin = New System.Windows.Forms.Padding(5)
        Me.dgvReservaciones.Name = "dgvReservaciones"
        Me.dgvReservaciones.ReadOnly = True
        Me.dgvReservaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReservaciones.Size = New System.Drawing.Size(856, 300)
        Me.dgvReservaciones.TabIndex = 4
        '
        'CodigoReservaDataGridViewTextBoxColumn
        '
        Me.CodigoReservaDataGridViewTextBoxColumn.DataPropertyName = "CodigoReserva"
        Me.CodigoReservaDataGridViewTextBoxColumn.HeaderText = "Reservación"
        Me.CodigoReservaDataGridViewTextBoxColumn.Name = "CodigoReservaDataGridViewTextBoxColumn"
        Me.CodigoReservaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Huésped"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FechaEntradaDataGridViewTextBoxColumn
        '
        Me.FechaEntradaDataGridViewTextBoxColumn.DataPropertyName = "FechaEntrada"
        Me.FechaEntradaDataGridViewTextBoxColumn.HeaderText = "Entrada"
        Me.FechaEntradaDataGridViewTextBoxColumn.Name = "FechaEntradaDataGridViewTextBoxColumn"
        Me.FechaEntradaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FechaSalidaDataGridViewTextBoxColumn
        '
        Me.FechaSalidaDataGridViewTextBoxColumn.DataPropertyName = "FechaSalida"
        Me.FechaSalidaDataGridViewTextBoxColumn.HeaderText = "Salida"
        Me.FechaSalidaDataGridViewTextBoxColumn.Name = "FechaSalidaDataGridViewTextBoxColumn"
        Me.FechaSalidaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NochesDataGridViewTextBoxColumn
        '
        Me.NochesDataGridViewTextBoxColumn.DataPropertyName = "Noches"
        Me.NochesDataGridViewTextBoxColumn.HeaderText = "Noches"
        Me.NochesDataGridViewTextBoxColumn.Name = "NochesDataGridViewTextBoxColumn"
        Me.NochesDataGridViewTextBoxColumn.ReadOnly = True
        '
        'EstadoDataGridViewTextBoxColumn
        '
        Me.EstadoDataGridViewTextBoxColumn.DataPropertyName = "Estado"
        Me.EstadoDataGridViewTextBoxColumn.HeaderText = "Estado"
        Me.EstadoDataGridViewTextBoxColumn.Name = "EstadoDataGridViewTextBoxColumn"
        Me.EstadoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreTipoDataGridViewTextBoxColumn
        '
        Me.NombreTipoDataGridViewTextBoxColumn.DataPropertyName = "NombreTipo"
        Me.NombreTipoDataGridViewTextBoxColumn.HeaderText = "Tipo"
        Me.NombreTipoDataGridViewTextBoxColumn.Name = "NombreTipoDataGridViewTextBoxColumn"
        Me.NombreTipoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PersonasExtraDataGridViewTextBoxColumn
        '
        Me.PersonasExtraDataGridViewTextBoxColumn.DataPropertyName = "PersonasExtra"
        Me.PersonasExtraDataGridViewTextBoxColumn.HeaderText = "P. Extra"
        Me.PersonasExtraDataGridViewTextBoxColumn.Name = "PersonasExtraDataGridViewTextBoxColumn"
        Me.PersonasExtraDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PaquetesReservaciones
        '
        Me.PaquetesReservaciones.DataPropertyName = "PaquetesReservaciones.Count"
        Me.PaquetesReservaciones.HeaderText = "Paquetes"
        Me.PaquetesReservaciones.Name = "PaquetesReservaciones"
        Me.PaquetesReservaciones.ReadOnly = True
        '
        'FechaCreacion
        '
        Me.FechaCreacion.DataPropertyName = "FechaCreacion"
        Me.FechaCreacion.HeaderText = "Fecha Reservación"
        Me.FechaCreacion.Name = "FechaCreacion"
        Me.FechaCreacion.ReadOnly = True
        '
        'Observaciones
        '
        Me.Observaciones.DataPropertyName = "Observaciones"
        Me.Observaciones.HeaderText = "Observaciones"
        Me.Observaciones.Name = "Observaciones"
        Me.Observaciones.ReadOnly = True
        '
        'ReservacionBindingSource
        '
        Me.ReservacionBindingSource.DataSource = GetType(Modelo.Entidades.Reservacion)
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Panel1.Controls.Add(Me.btnModificarReservacion)
        Me.Panel1.Controls.Add(Me.btnCancelarReservacion)
        Me.Panel1.Controls.Add(Me.btnCrearReservacion)
        Me.Panel1.Controls.Add(Me.btnImprimir)
        Me.Panel1.Location = New System.Drawing.Point(0, 498)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(886, 94)
        Me.Panel1.TabIndex = 5
        '
        'btnModificarReservacion
        '
        Me.btnModificarReservacion.BackColor = System.Drawing.SystemColors.Control
        Me.btnModificarReservacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnModificarReservacion.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnModificarReservacion.Checkable = False
        Me.btnModificarReservacion.Checked = False
        Me.btnModificarReservacion.DisableHighlight = False
        Me.btnModificarReservacion.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnModificarReservacion.GelColor = System.Drawing.Color.White
        Me.btnModificarReservacion.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnModificarReservacion.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnModificarReservacion.Image = Global.ZctSOT.Hotel.My.Resources.Resources.icono_reservacion_editar
        Me.btnModificarReservacion.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnModificarReservacion.ImageLocation = New System.Drawing.Point(25, 0)
        Me.btnModificarReservacion.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnModificarReservacion.Location = New System.Drawing.Point(239, 3)
        Me.btnModificarReservacion.Margin = New System.Windows.Forms.Padding(0)
        Me.btnModificarReservacion.MaximumSize = New System.Drawing.Size(136, 105)
        Me.btnModificarReservacion.Name = "btnModificarReservacion"
        Me.btnModificarReservacion.Size = New System.Drawing.Size(104, 80)
        Me.btnModificarReservacion.TabIndex = 2
        Me.btnModificarReservacion.Text = "MODIFICAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "RESERVACIÓN"
        Me.btnModificarReservacion.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnModificarReservacion.UseVisualStyleBackColor = False
        '
        'btnCancelarReservacion
        '
        Me.btnCancelarReservacion.BackColor = System.Drawing.SystemColors.Control
        Me.btnCancelarReservacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnCancelarReservacion.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCancelarReservacion.Checkable = False
        Me.btnCancelarReservacion.Checked = False
        Me.btnCancelarReservacion.DisableHighlight = False
        Me.btnCancelarReservacion.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelarReservacion.GelColor = System.Drawing.Color.White
        Me.btnCancelarReservacion.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCancelarReservacion.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCancelarReservacion.Image = Global.ZctSOT.Hotel.My.Resources.Resources.icono_reservacion_cancelar
        Me.btnCancelarReservacion.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCancelarReservacion.ImageLocation = New System.Drawing.Point(25, 0)
        Me.btnCancelarReservacion.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCancelarReservacion.Location = New System.Drawing.Point(351, 3)
        Me.btnCancelarReservacion.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCancelarReservacion.MaximumSize = New System.Drawing.Size(136, 105)
        Me.btnCancelarReservacion.Name = "btnCancelarReservacion"
        Me.btnCancelarReservacion.Size = New System.Drawing.Size(104, 80)
        Me.btnCancelarReservacion.TabIndex = 3
        Me.btnCancelarReservacion.Text = "CANCELAR" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "RESERVACIÓN"
        Me.btnCancelarReservacion.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCancelarReservacion.UseVisualStyleBackColor = False
        '
        'btnCrearReservacion
        '
        Me.btnCrearReservacion.BackColor = System.Drawing.SystemColors.Control
        Me.btnCrearReservacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnCrearReservacion.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCrearReservacion.Checkable = False
        Me.btnCrearReservacion.Checked = False
        Me.btnCrearReservacion.DisableHighlight = False
        Me.btnCrearReservacion.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCrearReservacion.GelColor = System.Drawing.Color.White
        Me.btnCrearReservacion.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCrearReservacion.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCrearReservacion.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_icono_reservada
        Me.btnCrearReservacion.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCrearReservacion.ImageLocation = New System.Drawing.Point(25, 0)
        Me.btnCrearReservacion.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCrearReservacion.Location = New System.Drawing.Point(127, 3)
        Me.btnCrearReservacion.Margin = New System.Windows.Forms.Padding(0)
        Me.btnCrearReservacion.MaximumSize = New System.Drawing.Size(136, 105)
        Me.btnCrearReservacion.Name = "btnCrearReservacion"
        Me.btnCrearReservacion.Size = New System.Drawing.Size(104, 80)
        Me.btnCrearReservacion.TabIndex = 1
        Me.btnCrearReservacion.Text = "RESERVACIÓN"
        Me.btnCrearReservacion.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCrearReservacion.UseVisualStyleBackColor = False
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.SystemColors.Control
        Me.btnImprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnImprimir.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnImprimir.Checkable = False
        Me.btnImprimir.Checked = False
        Me.btnImprimir.DisableHighlight = False
        Me.btnImprimir.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnImprimir.GelColor = System.Drawing.Color.White
        Me.btnImprimir.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnImprimir.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnImprimir.Image = Global.ZctSOT.Hotel.My.Resources.Resources.icono_imprimir
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnImprimir.ImageLocation = New System.Drawing.Point(25, 0)
        Me.btnImprimir.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnImprimir.Location = New System.Drawing.Point(15, 3)
        Me.btnImprimir.Margin = New System.Windows.Forms.Padding(0)
        Me.btnImprimir.MaximumSize = New System.Drawing.Size(136, 105)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(104, 80)
        Me.btnImprimir.TabIndex = 0
        Me.btnImprimir.Text = "IMPRIMIR"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label1.Location = New System.Drawing.Point(12, 166)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 15)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Fecha Inicio"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpInicio
        '
        Me.dtpInicio.CustomFormat = "dd/MM/yyyy"
        Me.dtpInicio.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpInicio.Location = New System.Drawing.Point(88, 162)
        Me.dtpInicio.Name = "dtpInicio"
        Me.dtpInicio.Size = New System.Drawing.Size(113, 22)
        Me.dtpInicio.TabIndex = 0
        '
        'dtpLimite
        '
        Me.dtpLimite.CustomFormat = "dd/MM/yyyy"
        Me.dtpLimite.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.dtpLimite.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpLimite.Location = New System.Drawing.Point(297, 162)
        Me.dtpLimite.Name = "dtpLimite"
        Me.dtpLimite.Size = New System.Drawing.Size(113, 22)
        Me.dtpLimite.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label2.Location = New System.Drawing.Point(218, 166)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 15)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Fecha Límite"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Label3.Location = New System.Drawing.Point(431, 166)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Tipo"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbTiposHabitaciones
        '
        Me.cbTiposHabitaciones.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.cbTiposHabitaciones.FormattingEnabled = True
        Me.cbTiposHabitaciones.Location = New System.Drawing.Point(468, 162)
        Me.cbTiposHabitaciones.Name = "cbTiposHabitaciones"
        Me.cbTiposHabitaciones.Size = New System.Drawing.Size(143, 23)
        Me.cbTiposHabitaciones.TabIndex = 2
        '
        'btnBuscar
        '
        Me.btnBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuscar.Location = New System.Drawing.Point(794, 162)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(76, 23)
        Me.btnBuscar.TabIndex = 3
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'pbLogo
        '
        Me.pbLogo.Font = New System.Drawing.Font("Lato Black", 45.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.pbLogo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(74, Byte), Integer), CType(CType(74, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.pbLogo.Image = Global.ZctSOT.Hotel.My.Resources.Resources.logo_VBoutique
        Me.pbLogo.Location = New System.Drawing.Point(0, 0)
        Me.pbLogo.Margin = New System.Windows.Forms.Padding(0)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(399, 146)
        Me.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbLogo.TabIndex = 3
        Me.pbLogo.TabStop = False
        Me.pbLogo.Text = "V Motel  Boutique"
        '
        'ReservacionesForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(884, 592)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.cbTiposHabitaciones)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpLimite)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpInicio)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.dgvReservaciones)
        Me.Controls.Add(Me.pbLogo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "ReservacionesForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reservaciones"
        CType(Me.dgvReservaciones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReservacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents dgvReservaciones As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CodigoReservaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaEntradaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaSalidaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NochesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EstadoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreTipoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PersonasExtraDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaquetesReservaciones As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaCreacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Observaciones As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReservacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpLimite As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbTiposHabitaciones As System.Windows.Forms.ComboBox
    Friend WithEvents btnModificarReservacion As GelButton
    Friend WithEvents btnCancelarReservacion As GelButton
    Friend WithEvents btnCrearReservacion As GelButton
    Friend WithEvents btnImprimir As GelButton
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
End Class
