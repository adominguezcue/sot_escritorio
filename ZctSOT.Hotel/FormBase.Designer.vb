﻿Imports ExtraComponents

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormBase
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panelBotonera = New System.Windows.Forms.Panel()
        Me.btnCancelar = New ExtraComponents.GelButton()
        Me.btnAceptar = New ExtraComponents.GelButton()
        Me.panelBotonera.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelBotonera
        '
        Me.panelBotonera.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelBotonera.AutoScroll = True
        Me.panelBotonera.BackColor = System.Drawing.SystemColors.ScrollBar
        Me.panelBotonera.Controls.Add(Me.btnCancelar)
        Me.panelBotonera.Controls.Add(Me.btnAceptar)
        Me.panelBotonera.Location = New System.Drawing.Point(0, 309)
        Me.panelBotonera.Margin = New System.Windows.Forms.Padding(0)
        Me.panelBotonera.Name = "panelBotonera"
        Me.panelBotonera.Padding = New System.Windows.Forms.Padding(2)
        Me.panelBotonera.Size = New System.Drawing.Size(681, 84)
        Me.panelBotonera.TabIndex = 13
        '
        'btnCancelar
        '
        Me.btnCancelar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancelar.BackColor = System.Drawing.Color.White
        Me.btnCancelar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnCancelar.Checkable = False
        Me.btnCancelar.Checked = False
        Me.btnCancelar.DisableHighlight = False
        Me.btnCancelar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnCancelar.GelColor = System.Drawing.Color.White
        Me.btnCancelar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnCancelar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnCancelar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.icono_cancelar
        Me.btnCancelar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnCancelar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnCancelar.Location = New System.Drawing.Point(465, 2)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(104, 80)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "CANCELAR"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnAceptar
        '
        Me.btnAceptar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAceptar.BackColor = System.Drawing.Color.White
        Me.btnAceptar.ButtonRectangleOrigin = New System.Drawing.Point(0, 24)
        Me.btnAceptar.Checkable = False
        Me.btnAceptar.Checked = False
        Me.btnAceptar.DisableHighlight = False
        Me.btnAceptar.Font = New System.Drawing.Font("Lato", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.btnAceptar.GelColor = System.Drawing.Color.White
        Me.btnAceptar.GradientBottom = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.btnAceptar.GradientTop = System.Drawing.Color.Gainsboro
        Me.btnAceptar.Image = Global.ZctSOT.Hotel.My.Resources.Resources.estado_icono_habilitada
        Me.btnAceptar.ImageLocation = New System.Drawing.Point(28, 0)
        Me.btnAceptar.ImageSize = New System.Drawing.Size(48, 48)
        Me.btnAceptar.Location = New System.Drawing.Point(573, 2)
        Me.btnAceptar.Margin = New System.Windows.Forms.Padding(5, 0, 0, 0)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(104, 80)
        Me.btnAceptar.TabIndex = 4
        Me.btnAceptar.Text = "ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'FormBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(681, 393)
        Me.Controls.Add(Me.panelBotonera)
        Me.Font = New System.Drawing.Font("Lato", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.Name = "FormBase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.panelBotonera.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Protected WithEvents panelBotonera As System.Windows.Forms.Panel
    Protected WithEvents btnAceptar As GelButton
    Private WithEvents btnCancelar As ExtraComponents.GelButton
End Class
