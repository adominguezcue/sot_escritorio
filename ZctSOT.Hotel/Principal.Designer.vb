﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.contenedor = New System.Windows.Forms.Panel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SistemaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HabilitarRecamarerasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HabilitarMeserosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ConfiguraciónDePropinasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguraciónDeFajillasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ConceptosDeCancelaciónYGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ImpresorasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DispositivoDeCapturaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FormatoDeVideoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetecciónDeMovimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmiSalirSistema = New System.Windows.Forms.ToolStripMenuItem()
        Me.HabitacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiHorariosPrecios = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeHabitaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromocionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiPaquetes = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.AmenidadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompraDeAmenidadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AjusteDeAmenidadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransferenciasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.BlancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConceptosDeMantenimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReporteDeRentasDeHabitacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteActualDeHabitacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.CambioDeHabitaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelarHabitaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RestauranteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiReportarMatricula = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiHistorialCliente = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesFrecuentesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmiCancelarFactura = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiReporteFacturacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator13 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmiReservaciones = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiCambioHabitacionReservada = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiNoShowReservacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator14 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmiHistorialVPoints = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiActivacionTarjetas = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiModelosCupon = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiCuponesHabitaciones = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiCuponesRestaurante = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesRealesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeArtículosVendidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeArtículosPorMeseroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.CorteVirtualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReporteDeRentasDeHabitaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDePaquetesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeReservasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeRecamarerasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeEntradasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ReporteGeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MantenimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'contenedor
        '
        Me.contenedor.AutoSize = True
        Me.contenedor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.contenedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.contenedor.Location = New System.Drawing.Point(0, 24)
        Me.contenedor.Margin = New System.Windows.Forms.Padding(0)
        Me.contenedor.Name = "contenedor"
        Me.contenedor.Size = New System.Drawing.Size(1008, 705)
        Me.contenedor.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SistemaToolStripMenuItem, Me.HabitacionesToolStripMenuItem, Me.RestauranteToolStripMenuItem, Me.ClientesToolStripMenuItem, Me.CortesToolStripMenuItem, Me.ReportesRealesToolStripMenuItem, Me.MantenimientoToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1008, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SistemaToolStripMenuItem
        '
        Me.SistemaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuariosToolStripMenuItem, Me.CambiarUsuarioToolStripMenuItem, Me.HabilitarRecamarerasToolStripMenuItem, Me.HabilitarMeserosToolStripMenuItem, Me.ToolStripSeparator4, Me.ConfiguraciónDePropinasToolStripMenuItem, Me.ConfiguraciónDeFajillasToolStripMenuItem, Me.ToolStripSeparator5, Me.ConceptosDeCancelaciónYGastosToolStripMenuItem, Me.ToolStripSeparator6, Me.ImpresorasToolStripMenuItem, Me.DispositivoDeCapturaToolStripMenuItem, Me.FormatoDeVideoToolStripMenuItem, Me.DetecciónDeMovimientoToolStripMenuItem, Me.ToolStripSeparator7, Me.tsmiSalirSistema})
        Me.SistemaToolStripMenuItem.Name = "SistemaToolStripMenuItem"
        Me.SistemaToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.SistemaToolStripMenuItem.Text = "Sistema"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Enabled = False
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios..."
        '
        'CambiarUsuarioToolStripMenuItem
        '
        Me.CambiarUsuarioToolStripMenuItem.Enabled = False
        Me.CambiarUsuarioToolStripMenuItem.Name = "CambiarUsuarioToolStripMenuItem"
        Me.CambiarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.CambiarUsuarioToolStripMenuItem.Text = "Cambiar Usuario"
        '
        'HabilitarRecamarerasToolStripMenuItem
        '
        Me.HabilitarRecamarerasToolStripMenuItem.Enabled = False
        Me.HabilitarRecamarerasToolStripMenuItem.Name = "HabilitarRecamarerasToolStripMenuItem"
        Me.HabilitarRecamarerasToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.HabilitarRecamarerasToolStripMenuItem.Text = "Habilitar Recamareras..."
        '
        'HabilitarMeserosToolStripMenuItem
        '
        Me.HabilitarMeserosToolStripMenuItem.Enabled = False
        Me.HabilitarMeserosToolStripMenuItem.Name = "HabilitarMeserosToolStripMenuItem"
        Me.HabilitarMeserosToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.HabilitarMeserosToolStripMenuItem.Text = "Habilitar Meseros..."
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(268, 6)
        '
        'ConfiguraciónDePropinasToolStripMenuItem
        '
        Me.ConfiguraciónDePropinasToolStripMenuItem.Enabled = False
        Me.ConfiguraciónDePropinasToolStripMenuItem.Name = "ConfiguraciónDePropinasToolStripMenuItem"
        Me.ConfiguraciónDePropinasToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.ConfiguraciónDePropinasToolStripMenuItem.Text = "Configuración de propinas..."
        '
        'ConfiguraciónDeFajillasToolStripMenuItem
        '
        Me.ConfiguraciónDeFajillasToolStripMenuItem.Enabled = False
        Me.ConfiguraciónDeFajillasToolStripMenuItem.Name = "ConfiguraciónDeFajillasToolStripMenuItem"
        Me.ConfiguraciónDeFajillasToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.ConfiguraciónDeFajillasToolStripMenuItem.Text = "Configuración de fajillas..."
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(268, 6)
        '
        'ConceptosDeCancelaciónYGastosToolStripMenuItem
        '
        Me.ConceptosDeCancelaciónYGastosToolStripMenuItem.Enabled = False
        Me.ConceptosDeCancelaciónYGastosToolStripMenuItem.Name = "ConceptosDeCancelaciónYGastosToolStripMenuItem"
        Me.ConceptosDeCancelaciónYGastosToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.ConceptosDeCancelaciónYGastosToolStripMenuItem.Text = "Conceptos de Cancelación y Gastos..."
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(268, 6)
        '
        'ImpresorasToolStripMenuItem
        '
        Me.ImpresorasToolStripMenuItem.Enabled = False
        Me.ImpresorasToolStripMenuItem.Name = "ImpresorasToolStripMenuItem"
        Me.ImpresorasToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.ImpresorasToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.ImpresorasToolStripMenuItem.Text = "Impresoras..."
        '
        'DispositivoDeCapturaToolStripMenuItem
        '
        Me.DispositivoDeCapturaToolStripMenuItem.Enabled = False
        Me.DispositivoDeCapturaToolStripMenuItem.Name = "DispositivoDeCapturaToolStripMenuItem"
        Me.DispositivoDeCapturaToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.DispositivoDeCapturaToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.DispositivoDeCapturaToolStripMenuItem.Text = "Dispositivo de Captura..."
        '
        'FormatoDeVideoToolStripMenuItem
        '
        Me.FormatoDeVideoToolStripMenuItem.Enabled = False
        Me.FormatoDeVideoToolStripMenuItem.Name = "FormatoDeVideoToolStripMenuItem"
        Me.FormatoDeVideoToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.FormatoDeVideoToolStripMenuItem.Text = "Formato de Video..."
        '
        'DetecciónDeMovimientoToolStripMenuItem
        '
        Me.DetecciónDeMovimientoToolStripMenuItem.Enabled = False
        Me.DetecciónDeMovimientoToolStripMenuItem.Name = "DetecciónDeMovimientoToolStripMenuItem"
        Me.DetecciónDeMovimientoToolStripMenuItem.Size = New System.Drawing.Size(271, 22)
        Me.DetecciónDeMovimientoToolStripMenuItem.Text = "Formato de Video..."
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(268, 6)
        '
        'tsmiSalirSistema
        '
        Me.tsmiSalirSistema.Name = "tsmiSalirSistema"
        Me.tsmiSalirSistema.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.tsmiSalirSistema.Size = New System.Drawing.Size(271, 22)
        Me.tsmiSalirSistema.Text = "Salir del Sistema"
        '
        'HabitacionesToolStripMenuItem
        '
        Me.HabitacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiHorariosPrecios, Me.TiposDeHabitaciónToolStripMenuItem, Me.PromocionesToolStripMenuItem, Me.tsmiPaquetes, Me.ToolStripSeparator8, Me.AmenidadesToolStripMenuItem, Me.CompraDeAmenidadesToolStripMenuItem, Me.AjusteDeAmenidadesToolStripMenuItem, Me.TransferenciasToolStripMenuItem, Me.ReporteDeComprasToolStripMenuItem, Me.ToolStripSeparator9, Me.BlancosToolStripMenuItem, Me.ConceptosDeMantenimientoToolStripMenuItem, Me.ToolStripSeparator10, Me.ReporteDeRentasDeHabitacionesToolStripMenuItem, Me.ReporteActualDeHabitacionesToolStripMenuItem, Me.ReporteHistorialDeHabitaciónToolStripMenuItem1, Me.ToolStripSeparator11, Me.CambioDeHabitaciónToolStripMenuItem, Me.CancelarHabitaciónToolStripMenuItem})
        Me.HabitacionesToolStripMenuItem.Name = "HabitacionesToolStripMenuItem"
        Me.HabitacionesToolStripMenuItem.Size = New System.Drawing.Size(88, 20)
        Me.HabitacionesToolStripMenuItem.Text = "Habitaciones"
        '
        'tsmiHorariosPrecios
        '
        Me.tsmiHorariosPrecios.Enabled = False
        Me.tsmiHorariosPrecios.Name = "tsmiHorariosPrecios"
        Me.tsmiHorariosPrecios.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.tsmiHorariosPrecios.Size = New System.Drawing.Size(266, 22)
        Me.tsmiHorariosPrecios.Text = "Horarios y precios"
        '
        'TiposDeHabitaciónToolStripMenuItem
        '
        Me.TiposDeHabitaciónToolStripMenuItem.Enabled = False
        Me.TiposDeHabitaciónToolStripMenuItem.Name = "TiposDeHabitaciónToolStripMenuItem"
        Me.TiposDeHabitaciónToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.TiposDeHabitaciónToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.TiposDeHabitaciónToolStripMenuItem.Text = "Tipos de habitación..."
        '
        'PromocionesToolStripMenuItem
        '
        Me.PromocionesToolStripMenuItem.Enabled = False
        Me.PromocionesToolStripMenuItem.Name = "PromocionesToolStripMenuItem"
        Me.PromocionesToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.PromocionesToolStripMenuItem.Text = "Promociones..."
        '
        'tsmiPaquetes
        '
        Me.tsmiPaquetes.Name = "tsmiPaquetes"
        Me.tsmiPaquetes.Size = New System.Drawing.Size(266, 22)
        Me.tsmiPaquetes.Text = "Paquetes..."
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(263, 6)
        '
        'AmenidadesToolStripMenuItem
        '
        Me.AmenidadesToolStripMenuItem.Enabled = False
        Me.AmenidadesToolStripMenuItem.Name = "AmenidadesToolStripMenuItem"
        Me.AmenidadesToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.AmenidadesToolStripMenuItem.Text = "Amenidades..."
        '
        'CompraDeAmenidadesToolStripMenuItem
        '
        Me.CompraDeAmenidadesToolStripMenuItem.Enabled = False
        Me.CompraDeAmenidadesToolStripMenuItem.Name = "CompraDeAmenidadesToolStripMenuItem"
        Me.CompraDeAmenidadesToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.CompraDeAmenidadesToolStripMenuItem.Text = "Compra de Amenidades..."
        '
        'AjusteDeAmenidadesToolStripMenuItem
        '
        Me.AjusteDeAmenidadesToolStripMenuItem.Enabled = False
        Me.AjusteDeAmenidadesToolStripMenuItem.Name = "AjusteDeAmenidadesToolStripMenuItem"
        Me.AjusteDeAmenidadesToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.AjusteDeAmenidadesToolStripMenuItem.Text = "Ajuste de Amenidades..."
        '
        'TransferenciasToolStripMenuItem
        '
        Me.TransferenciasToolStripMenuItem.Enabled = False
        Me.TransferenciasToolStripMenuItem.Name = "TransferenciasToolStripMenuItem"
        Me.TransferenciasToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.TransferenciasToolStripMenuItem.Text = "Transferencias..."
        '
        'ReporteDeComprasToolStripMenuItem
        '
        Me.ReporteDeComprasToolStripMenuItem.Enabled = False
        Me.ReporteDeComprasToolStripMenuItem.Name = "ReporteDeComprasToolStripMenuItem"
        Me.ReporteDeComprasToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeComprasToolStripMenuItem.Text = "Reporte de Compras..."
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(263, 6)
        '
        'BlancosToolStripMenuItem
        '
        Me.BlancosToolStripMenuItem.Enabled = False
        Me.BlancosToolStripMenuItem.Name = "BlancosToolStripMenuItem"
        Me.BlancosToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.BlancosToolStripMenuItem.Text = "Blancos..."
        '
        'ConceptosDeMantenimientoToolStripMenuItem
        '
        Me.ConceptosDeMantenimientoToolStripMenuItem.Enabled = False
        Me.ConceptosDeMantenimientoToolStripMenuItem.Name = "ConceptosDeMantenimientoToolStripMenuItem"
        Me.ConceptosDeMantenimientoToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ConceptosDeMantenimientoToolStripMenuItem.Text = "Conceptos de Mantenimiento..."
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(263, 6)
        '
        'ReporteDeRentasDeHabitacionesToolStripMenuItem
        '
        Me.ReporteDeRentasDeHabitacionesToolStripMenuItem.Enabled = False
        Me.ReporteDeRentasDeHabitacionesToolStripMenuItem.Name = "ReporteDeRentasDeHabitacionesToolStripMenuItem"
        Me.ReporteDeRentasDeHabitacionesToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeRentasDeHabitacionesToolStripMenuItem.Text = "Reporte de Rentas de Habitaciones..."
        '
        'ReporteActualDeHabitacionesToolStripMenuItem
        '
        Me.ReporteActualDeHabitacionesToolStripMenuItem.Enabled = False
        Me.ReporteActualDeHabitacionesToolStripMenuItem.Name = "ReporteActualDeHabitacionesToolStripMenuItem"
        Me.ReporteActualDeHabitacionesToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteActualDeHabitacionesToolStripMenuItem.Text = "Reporte Actual de Habitaciones..."
        '
        'ReporteHistorialDeHabitaciónToolStripMenuItem1
        '
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem1.Enabled = False
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem1.Name = "ReporteHistorialDeHabitaciónToolStripMenuItem1"
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem1.Size = New System.Drawing.Size(266, 22)
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem1.Text = "Reporte Historial de Habitación..."
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(263, 6)
        '
        'CambioDeHabitaciónToolStripMenuItem
        '
        Me.CambioDeHabitaciónToolStripMenuItem.Enabled = False
        Me.CambioDeHabitaciónToolStripMenuItem.Name = "CambioDeHabitaciónToolStripMenuItem"
        Me.CambioDeHabitaciónToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.CambioDeHabitaciónToolStripMenuItem.Text = "Cambio de Habitación"
        '
        'CancelarHabitaciónToolStripMenuItem
        '
        Me.CancelarHabitaciónToolStripMenuItem.Enabled = False
        Me.CancelarHabitaciónToolStripMenuItem.Name = "CancelarHabitaciónToolStripMenuItem"
        Me.CancelarHabitaciónToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.CancelarHabitaciónToolStripMenuItem.Text = "Cancelar Habitación"
        '
        'RestauranteToolStripMenuItem
        '
        Me.RestauranteToolStripMenuItem.Name = "RestauranteToolStripMenuItem"
        Me.RestauranteToolStripMenuItem.Size = New System.Drawing.Size(81, 20)
        Me.RestauranteToolStripMenuItem.Text = "Restaurante"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiReportarMatricula, Me.tsmiHistorialCliente, Me.ClientesFrecuentesToolStripMenuItem, Me.ToolStripSeparator12, Me.tsmiCancelarFactura, Me.tsmiReporteFacturacion, Me.ToolStripSeparator13, Me.tsmiReservaciones, Me.tsmiCambioHabitacionReservada, Me.tsmiNoShowReservacion, Me.ToolStripSeparator14, Me.tsmiHistorialVPoints, Me.tsmiActivacionTarjetas, Me.tsmiModelosCupon, Me.tsmiCuponesHabitaciones, Me.tsmiCuponesRestaurante})
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'tsmiReportarMatricula
        '
        Me.tsmiReportarMatricula.Enabled = False
        Me.tsmiReportarMatricula.Name = "tsmiReportarMatricula"
        Me.tsmiReportarMatricula.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.M), System.Windows.Forms.Keys)
        Me.tsmiReportarMatricula.Size = New System.Drawing.Size(258, 22)
        Me.tsmiReportarMatricula.Text = "Reportar Matrícula..."
        '
        'tsmiHistorialCliente
        '
        Me.tsmiHistorialCliente.Enabled = False
        Me.tsmiHistorialCliente.Name = "tsmiHistorialCliente"
        Me.tsmiHistorialCliente.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.tsmiHistorialCliente.Size = New System.Drawing.Size(258, 22)
        Me.tsmiHistorialCliente.Text = "Historial del Cliente..."
        '
        'ClientesFrecuentesToolStripMenuItem
        '
        Me.ClientesFrecuentesToolStripMenuItem.Enabled = False
        Me.ClientesFrecuentesToolStripMenuItem.Name = "ClientesFrecuentesToolStripMenuItem"
        Me.ClientesFrecuentesToolStripMenuItem.Size = New System.Drawing.Size(258, 22)
        Me.ClientesFrecuentesToolStripMenuItem.Text = "Clientes Frecuentes..."
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(255, 6)
        '
        'tsmiCancelarFactura
        '
        Me.tsmiCancelarFactura.Enabled = False
        Me.tsmiCancelarFactura.Name = "tsmiCancelarFactura"
        Me.tsmiCancelarFactura.Size = New System.Drawing.Size(258, 22)
        Me.tsmiCancelarFactura.Text = "Cancelar Factura..."
        '
        'tsmiReporteFacturacion
        '
        Me.tsmiReporteFacturacion.Enabled = False
        Me.tsmiReporteFacturacion.Name = "tsmiReporteFacturacion"
        Me.tsmiReporteFacturacion.Size = New System.Drawing.Size(258, 22)
        Me.tsmiReporteFacturacion.Text = "Reporte de Facturación..."
        '
        'ToolStripSeparator13
        '
        Me.ToolStripSeparator13.Name = "ToolStripSeparator13"
        Me.ToolStripSeparator13.Size = New System.Drawing.Size(255, 6)
        '
        'tsmiReservaciones
        '
        Me.tsmiReservaciones.Name = "tsmiReservaciones"
        Me.tsmiReservaciones.Size = New System.Drawing.Size(258, 22)
        Me.tsmiReservaciones.Text = "Reservaciones..."
        '
        'tsmiCambioHabitacionReservada
        '
        Me.tsmiCambioHabitacionReservada.Enabled = False
        Me.tsmiCambioHabitacionReservada.Name = "tsmiCambioHabitacionReservada"
        Me.tsmiCambioHabitacionReservada.Size = New System.Drawing.Size(258, 22)
        Me.tsmiCambioHabitacionReservada.Text = "Cambio de Habitación Reservada..."
        '
        'tsmiNoShowReservacion
        '
        Me.tsmiNoShowReservacion.Enabled = False
        Me.tsmiNoShowReservacion.Name = "tsmiNoShowReservacion"
        Me.tsmiNoShowReservacion.Size = New System.Drawing.Size(258, 22)
        Me.tsmiNoShowReservacion.Text = "No Show de Reservación..."
        '
        'ToolStripSeparator14
        '
        Me.ToolStripSeparator14.Name = "ToolStripSeparator14"
        Me.ToolStripSeparator14.Size = New System.Drawing.Size(255, 6)
        '
        'tsmiHistorialVPoints
        '
        Me.tsmiHistorialVPoints.Enabled = False
        Me.tsmiHistorialVPoints.Name = "tsmiHistorialVPoints"
        Me.tsmiHistorialVPoints.Size = New System.Drawing.Size(258, 22)
        Me.tsmiHistorialVPoints.Text = "Historial VPoints..."
        '
        'tsmiActivacionTarjetas
        '
        Me.tsmiActivacionTarjetas.Enabled = False
        Me.tsmiActivacionTarjetas.Name = "tsmiActivacionTarjetas"
        Me.tsmiActivacionTarjetas.Size = New System.Drawing.Size(258, 22)
        Me.tsmiActivacionTarjetas.Text = "Activación de Tarjetas..."
        '
        'tsmiModelosCupon
        '
        Me.tsmiModelosCupon.Enabled = False
        Me.tsmiModelosCupon.Name = "tsmiModelosCupon"
        Me.tsmiModelosCupon.Size = New System.Drawing.Size(258, 22)
        Me.tsmiModelosCupon.Text = "Modelos de Cupón..."
        '
        'tsmiCuponesHabitaciones
        '
        Me.tsmiCuponesHabitaciones.Enabled = False
        Me.tsmiCuponesHabitaciones.Name = "tsmiCuponesHabitaciones"
        Me.tsmiCuponesHabitaciones.Size = New System.Drawing.Size(258, 22)
        Me.tsmiCuponesHabitaciones.Text = "Cupones Habitaciones..."
        '
        'tsmiCuponesRestaurante
        '
        Me.tsmiCuponesRestaurante.Enabled = False
        Me.tsmiCuponesRestaurante.Name = "tsmiCuponesRestaurante"
        Me.tsmiCuponesRestaurante.Size = New System.Drawing.Size(258, 22)
        Me.tsmiCuponesRestaurante.Text = "Cupones Restaurante..."
        '
        'CortesToolStripMenuItem
        '
        Me.CortesToolStripMenuItem.Name = "CortesToolStripMenuItem"
        Me.CortesToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.CortesToolStripMenuItem.Text = "Cortes"
        '
        'ReportesRealesToolStripMenuItem
        '
        Me.ReportesRealesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReporteDeVentasToolStripMenuItem, Me.ReporteDeArtículosVendidosToolStripMenuItem, Me.ReporteDeArtículosPorMeseroToolStripMenuItem, Me.ToolStripSeparator1, Me.CorteVirtualToolStripMenuItem, Me.ToolStripSeparator2, Me.ReporteDeRentasDeHabitaciónToolStripMenuItem, Me.ReporteHistorialDeHabitaciónToolStripMenuItem, Me.ReporteDePaquetesToolStripMenuItem, Me.ReporteDeReservasToolStripMenuItem, Me.ReporteDeRecamarerasToolStripMenuItem, Me.ReporteDeEntradasToolStripMenuItem, Me.ToolStripSeparator3, Me.ReporteGeneralToolStripMenuItem})
        Me.ReportesRealesToolStripMenuItem.Name = "ReportesRealesToolStripMenuItem"
        Me.ReportesRealesToolStripMenuItem.Size = New System.Drawing.Size(101, 20)
        Me.ReportesRealesToolStripMenuItem.Text = "Reportes Reales"
        '
        'ReporteDeVentasToolStripMenuItem
        '
        Me.ReporteDeVentasToolStripMenuItem.Enabled = False
        Me.ReporteDeVentasToolStripMenuItem.Name = "ReporteDeVentasToolStripMenuItem"
        Me.ReporteDeVentasToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeVentasToolStripMenuItem.Text = "Reporte de Ventas..."
        '
        'ReporteDeArtículosVendidosToolStripMenuItem
        '
        Me.ReporteDeArtículosVendidosToolStripMenuItem.Enabled = False
        Me.ReporteDeArtículosVendidosToolStripMenuItem.Name = "ReporteDeArtículosVendidosToolStripMenuItem"
        Me.ReporteDeArtículosVendidosToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeArtículosVendidosToolStripMenuItem.Text = "Reporte de Artículos Vendidos..."
        '
        'ReporteDeArtículosPorMeseroToolStripMenuItem
        '
        Me.ReporteDeArtículosPorMeseroToolStripMenuItem.Enabled = False
        Me.ReporteDeArtículosPorMeseroToolStripMenuItem.Name = "ReporteDeArtículosPorMeseroToolStripMenuItem"
        Me.ReporteDeArtículosPorMeseroToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeArtículosPorMeseroToolStripMenuItem.Text = "Reporte de Artículos por Mesero..."
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(263, 6)
        '
        'CorteVirtualToolStripMenuItem
        '
        Me.CorteVirtualToolStripMenuItem.Enabled = False
        Me.CorteVirtualToolStripMenuItem.Name = "CorteVirtualToolStripMenuItem"
        Me.CorteVirtualToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.CorteVirtualToolStripMenuItem.Text = "Corte Virtual..."
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(263, 6)
        '
        'ReporteDeRentasDeHabitaciónToolStripMenuItem
        '
        Me.ReporteDeRentasDeHabitaciónToolStripMenuItem.Enabled = False
        Me.ReporteDeRentasDeHabitaciónToolStripMenuItem.Name = "ReporteDeRentasDeHabitaciónToolStripMenuItem"
        Me.ReporteDeRentasDeHabitaciónToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeRentasDeHabitaciónToolStripMenuItem.Text = "Reporte de Rentas de Habitaciones..."
        '
        'ReporteHistorialDeHabitaciónToolStripMenuItem
        '
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem.Enabled = False
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem.Name = "ReporteHistorialDeHabitaciónToolStripMenuItem"
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteHistorialDeHabitaciónToolStripMenuItem.Text = "Reporte Historial de Habitación..."
        '
        'ReporteDePaquetesToolStripMenuItem
        '
        Me.ReporteDePaquetesToolStripMenuItem.Enabled = False
        Me.ReporteDePaquetesToolStripMenuItem.Name = "ReporteDePaquetesToolStripMenuItem"
        Me.ReporteDePaquetesToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDePaquetesToolStripMenuItem.Text = "Reporte de Paquetes..."
        '
        'ReporteDeReservasToolStripMenuItem
        '
        Me.ReporteDeReservasToolStripMenuItem.Enabled = False
        Me.ReporteDeReservasToolStripMenuItem.Name = "ReporteDeReservasToolStripMenuItem"
        Me.ReporteDeReservasToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeReservasToolStripMenuItem.Text = "Reporte de Reservas..."
        '
        'ReporteDeRecamarerasToolStripMenuItem
        '
        Me.ReporteDeRecamarerasToolStripMenuItem.Enabled = False
        Me.ReporteDeRecamarerasToolStripMenuItem.Name = "ReporteDeRecamarerasToolStripMenuItem"
        Me.ReporteDeRecamarerasToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeRecamarerasToolStripMenuItem.Text = "Reporte de Recamareras..."
        '
        'ReporteDeEntradasToolStripMenuItem
        '
        Me.ReporteDeEntradasToolStripMenuItem.Enabled = False
        Me.ReporteDeEntradasToolStripMenuItem.Name = "ReporteDeEntradasToolStripMenuItem"
        Me.ReporteDeEntradasToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteDeEntradasToolStripMenuItem.Text = "Reporte de Entradas..."
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(263, 6)
        '
        'ReporteGeneralToolStripMenuItem
        '
        Me.ReporteGeneralToolStripMenuItem.Enabled = False
        Me.ReporteGeneralToolStripMenuItem.Name = "ReporteGeneralToolStripMenuItem"
        Me.ReporteGeneralToolStripMenuItem.Size = New System.Drawing.Size(266, 22)
        Me.ReporteGeneralToolStripMenuItem.Text = "Reporte General..."
        '
        'MantenimientoToolStripMenuItem
        '
        Me.MantenimientoToolStripMenuItem.Name = "MantenimientoToolStripMenuItem"
        Me.MantenimientoToolStripMenuItem.Size = New System.Drawing.Size(101, 20)
        Me.MantenimientoToolStripMenuItem.Text = "Mantenimiento"
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1008, 729)
        Me.Controls.Add(Me.contenedor)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Principal"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sistema de Gestión Hotelera"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents contenedor As System.Windows.Forms.Panel
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SistemaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HabitacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiHorariosPrecios As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RestauranteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesRealesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeArtículosVendidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MantenimientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HabilitarRecamarerasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HabilitarMeserosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ConfiguraciónDePropinasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConfiguraciónDeFajillasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ConceptosDeCancelaciónYGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ImpresorasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DispositivoDeCapturaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FormatoDeVideoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetecciónDeMovimientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiSalirSistema As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeArtículosPorMeseroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CorteVirtualToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ReporteDeRentasDeHabitaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteHistorialDeHabitaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDePaquetesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeReservasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeRecamarerasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeEntradasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ReporteGeneralToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TiposDeHabitaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromocionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiPaquetes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AmenidadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompraDeAmenidadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjusteDeAmenidadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransferenciasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeComprasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BlancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConceptosDeMantenimientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ReporteDeRentasDeHabitacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteActualDeHabitacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteHistorialDeHabitaciónToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CambioDeHabitaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelarHabitaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiReportarMatricula As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiHistorialCliente As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesFrecuentesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiCancelarFactura As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiReporteFacturacion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator13 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiReservaciones As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCambioHabitacionReservada As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiNoShowReservacion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator14 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiHistorialVPoints As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiActivacionTarjetas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiModelosCupon As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCuponesHabitaciones As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCuponesRestaurante As System.Windows.Forms.ToolStripMenuItem
End Class
