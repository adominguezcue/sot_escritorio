﻿Imports ZctSOT.Controladores
Imports Transversal.Extensiones
Imports Transversal.Excepciones

Public Class ConfiguracionConceptosForm

    Private esNuevo As Boolean = False
    Private conceptoActual As Modelo.Entidades.ConceptoSistema

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        txtConcepto.Clear()
        LimpiarChecksTipos(-1)

        esNuevo = True

        conceptoActual = New Modelo.Entidades.ConceptoSistema

        panelEdicion.Show()

        btnNuevo.Enabled = False
        btnModificar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If dgvConceptos.SelectedRows.Count <> 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_conceptos_seleccionados)
        End If

        Dim concepto As Modelo.Entidades.ConceptoSistema = CType(dgvConceptos.SelectedRows(0).DataBoundItem, Modelo.Entidades.ConceptoSistema)

        txtConcepto.Text = concepto.Concepto

        For ix As Integer = 0 To clbTipos.Items.Count - 1

            Dim item As Modelo.Entidades.ConceptoSistema.TiposConceptos = CType(clbTipos.Items(ix), Modelo.Entidades.ConceptoSistema.TiposConceptos)

            clbTipos.SetItemChecked(ix, item.Id = concepto.IdTipoConcepto)
            clbTipos.SetSelected(ix, item.Id = concepto.IdTipoConcepto)
        Next

        esNuevo = False

        conceptoActual = concepto

        panelEdicion.Show()

        btnNuevo.Enabled = False
        btnModificar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub ConfiguracionConceptosForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CargarConceptos()
        CargarTipos()

        panelEdicion.Hide()
    End Sub

    Private Sub CargarConceptos()
        Dim controlador = New Controladores.ControladorConceptosSistema()

        Dim conceptos = controlador.ObtenerConceptos()

        dgvConceptos.DataSource = conceptos


    End Sub

    Private Sub CargarTipos()
        clbTipos.DataSource = New Controladores.ControladorConceptosSistema().ObtenerTiposConcepto()
        clbTipos.DisplayMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.ConceptoSistema.TiposConceptos, String)(Function(m) m.Nombre)
        clbTipos.ValueMember = ObjectExtensions.NombrePropiedad(Of Modelo.Entidades.ConceptoSistema.TiposConceptos, Integer)(Function(m) m.Id)
    End Sub

    Private Sub LimpiarChecksTipos(index As Integer)
        For ix As Integer = 0 To clbTipos.Items.Count - 1
            If ix <> index Then
                clbTipos.SetItemChecked(ix, False)
                clbTipos.SetSelected(ix, False)
            End If
        Next
    End Sub

    Private Sub CheckedListBox1_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles clbTipos.ItemCheck
        LimpiarChecksTipos(e.Index)
    End Sub

    Private Sub panelEdicion_VisibleChanged(sender As Object, e As EventArgs) Handles panelEdicion.VisibleChanged
        If panelEdicion.Visible Then
            dgvConceptos.Size = dgvConceptos.MinimumSize
        Else
            dgvConceptos.Size = FlowLayoutPanel1.Size
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        panelEdicion.Hide()
        esNuevo = False

        btnNuevo.Enabled = True
        btnModificar.Enabled = True
        btnEliminar.Enabled = True
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim controlador As New ControladorConceptosSistema

        conceptoActual.Concepto = txtConcepto.Text

        If clbTipos.CheckedItems.Count = 0 Then
            Throw New SOTException(My.Resources.Errores.tipo_concepto_no_seleccionado_exception)
        End If

        Dim item As Modelo.Entidades.ConceptoSistema.TiposConceptos = CType(clbTipos.CheckedItems(0), Modelo.Entidades.ConceptoSistema.TiposConceptos)

        conceptoActual.IdTipoConcepto = item.Id

        If esNuevo Then
            controlador.CrearConcepto(conceptoActual)
        Else
            controlador.ModificarConcepto(conceptoActual)
        End If

        ConfiguracionConceptosForm_Load(Nothing, Nothing)

        btnNuevo.Enabled = True
        btnModificar.Enabled = True
        btnEliminar.Enabled = True

        If esNuevo Then
            MessageBox.Show(My.Resources.Mensajes.creacion_concepto_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show(My.Resources.Mensajes.modificacion_concepto_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvConceptos.SelectedRows.Count <> 1 Then
            Throw New SOTException(My.Resources.Errores.multiples_conceptos_seleccionados)
        End If

        Dim concepto As Modelo.Entidades.ConceptoSistema = CType(dgvConceptos.SelectedRows(0).DataBoundItem, Modelo.Entidades.ConceptoSistema)

        If MessageBox.Show(String.Format(My.Resources.Mensajes.eliminar_concepto, concepto.Concepto, concepto.Tipo),
                           My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim controlador As New ControladorConceptosSistema

            controlador.EliminarConcepto(concepto.Id)

            MessageBox.Show(My.Resources.Mensajes.eliminacion_concepto_exitosa, My.Resources.TitulosVentanas.Aviso, MessageBoxButtons.OK, MessageBoxIcon.Information)

            ConfiguracionConceptosForm_Load(Nothing, Nothing)
        End If
    End Sub
End Class