﻿Public Class SelectorModoAplicacionForm

    Private Sub btnHotel_Click(sender As Object, e As EventArgs) Handles btnHotel.Click

        Dim principalF As New Principal

        Hide()

        principalF.ShowDialog()

        Close()
    End Sub

    Private Sub btnRestaurante_Click(sender As Object, e As EventArgs) Handles btnRestaurante.Click
        Dim principalF As New GestionOrdenesForm

        Hide()

        principalF.ShowDialog()

        Close()
    End Sub

    Private Sub btnCocina_Click(sender As Object, e As EventArgs) Handles btnCocina.Click
        Dim principalF As New FormDespComOrds

        Hide()

        principalF.ShowDialog()

        Close()
    End Sub
End Class