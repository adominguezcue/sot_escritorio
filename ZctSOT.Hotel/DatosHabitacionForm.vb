﻿Imports Transversal.Extensiones

Public Class DatosHabitacionForm

    'Public Property Cerrar As Boolean
    '    Get
    '    End Get
    '    Set(value As Boolean)

    '    End Set
    'End Property

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Hide()
    End Sub

    Sub ActualizarDatos(habitacion As Modelo.Dtos.DtoDatosHabitacion)

        lblHabitacion.Text = habitacion.Habitacion
        lblTipo.Text = habitacion.Tipo
        lblEstado.Text = habitacion.Estado.Descripcion()
        lblTarjetaV.Text = habitacion.TarjetaV
        lblEntrada.Text = If(habitacion.FechaEntrada.HasValue, habitacion.FechaEntrada.Value.ToString("hh:mm:ss tt"), "")
        lblSalida.Text = If(habitacion.FechaSalida.HasValue, habitacion.FechaSalida.Value.ToString("hh:mm:ss tt"), "")
        lblMatricula.Text = habitacion.Matricula
        lblServicio.Text = habitacion.NumeroServicio
        lblPersonasExtra.Text = habitacion.PersonasExtra
        lblHospedajeExtra.Text = habitacion.HospedajeExtra
        lblCortesia.Text = habitacion.CortesiaHabitacion
        lblPaquetes.Text = habitacion.Paquetes
        lblServicioRestaurante.Text = habitacion.ServicioRestaurante

        imgEstadoLimpieza.Image = Nothing

        Select Case habitacion.Estado
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Libre
                pbEstado.Image = My.Resources.estado_icono_habilitada
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia
                pbEstado.Image = My.Resources.estado_icono_sucia

                Select Case habitacion.TipoLimpieza
                    Case Modelo.Entidades.TareaLimpieza.TiposLimpieza.Detallado
                        imgEstadoLimpieza.Image = My.Resources.estado_limpieza_detallado
                        'Case Modelo.Entidades.TareaLimpieza.TiposLimpieza.Lavado
                        '    imgEstadoLimpieza.Image = My.Resources.estado_limpieza_lavado
                    Case Modelo.Entidades.TareaLimpieza.TiposLimpieza.Normal
                        imgEstadoLimpieza.Image = My.Resources.estado_limpieza_normal
                End Select
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada
                pbEstado.Image = My.Resources.estado_icono_ocupada
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Reservada
                pbEstado.Image = My.Resources.estado_icono_reservada
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza
                pbEstado.Image = My.Resources.estado_icono_limpieza
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Mantenimiento
                pbEstado.Image = My.Resources.estado_icono_mantenimiento
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Supervision
                pbEstado.Image = My.Resources.estado_icono_revision
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Bloqueada
                pbEstado.Image = My.Resources.estado_icono_bloqueada
            Case Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada
                pbEstado.Image = My.Resources.estado_icono_preparada
            Case Else
                pbEstado.Image = Nothing
        End Select

    End Sub

End Class