﻿Imports System.Linq
Imports Transversal.Extensiones

Namespace Helpers

    Public Class HelperDataSource
        Public Shared Sub AgregarDataSource(Of T As {Class}, TDisp, TVal)(cb As ListControl, dataSource As IEnumerable(Of T), _
                                            Optional miembroVisualizar As System.Linq.Expressions.Expression(Of Func(Of T, TDisp)) = Nothing, _
                                            Optional miembroValor As System.Linq.Expressions.Expression(Of Func(Of T, TVal)) = Nothing)
            If Not IsNothing(miembroValor) Then
                cb.ValueMember = ObjectExtensions.NombrePropiedad(Of T, TVal)(miembroValor)
            End If

            If Not IsNothing(miembroVisualizar) Then
                cb.DisplayMember = ObjectExtensions.NombrePropiedad(Of T, TDisp)(miembroVisualizar)
            End If

            cb.DataSource = dataSource
        End Sub

    End Class

End Namespace