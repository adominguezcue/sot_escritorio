﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioServiciosOrdenesCompra : Repositorio<ServicioOrdenCompra>, IRepositorioServiciosOrdenesCompra
    {
        public RepositorioServiciosOrdenesCompra() : base(new Contextos.AlmacenContext()) { }

        public List<DtoServicioOC> ObtenerServiciosOrdenCompra(int codigoOrdenCompra)
        {
            return (from det in contexto.Set<ServicioOrdenCompra>()
                    where det.CodigoOrdenCompra == codigoOrdenCompra
                    select new
                    {
                        det.Id,
                        det.CodigoOrdenCompra,
                        det.Descripcion,
                        det.Costo
                    }).ToList().Select(det => new DtoServicioOC
                    {
                        Id = det.Id,
                        CodigoOrdenCompra = det.CodigoOrdenCompra,
                        Descripcion = det.Descripcion,
                        Costo = det.Costo
                    }).ToList();

            /*
             SELECT ZctDetOC.Cod_OC, ZctDetOC.CodDet_OC, ZctDetOC.Cod_Art,ZctCatArt.upc, ZctDetOC.Ctd_Art, ZctDetOC.Cos_Art, ZctDetOC.CtdStdDet_OC, ZctCatArt.Desc_Art, 
                      ZctCatArt.Exist_Art, ZctDetOC.Cat_Alm,ZctCatMar.Desc_Mar as marca, ZctDetOC.OmitirIVA
					  FROM ZctDetOC INNER JOIN
                      ZctCatArt ON ZctDetOC.Cod_Art = ZctCatArt.Cod_Art 
					  LEFT JOIN ZctCatMar ON ZctCatMar.Cod_Mar = ZctCatArt.Cod_Mar  
					  WHERE ZctDetOC.Cod_OC = @Cod_OC
             */
        }
    }
}
