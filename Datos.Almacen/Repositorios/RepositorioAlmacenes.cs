﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioAlmacenes : Repositorio<Modelo.Almacen.Entidades.Almacen>, IRepositorioAlmacenes
    {
        public RepositorioAlmacenes() : base(new Contextos.AlmacenContext()) { }

        public DtoEncabezadoIntercambio SP_ObtenerResumenIntercambio(int idFolio)
        {
            var p_idFolio = new SqlParameter("@idFolio", idFolio);
            p_idFolio.SqlDbType = System.Data.SqlDbType.Int;

            
            List<DtoDetalleIntercambio> detalles = new List<DtoDetalleIntercambio>();

            var resultado = contexto.ConsultaMultipleSql("exec dbo.SP_ObtenerResumenIntercambio @idFolio", new object[] { p_idFolio }, typeof(DtoEncabezadoIntercambio), typeof(DtoDetalleIntercambio)).ToList();

            if (resultado.Count == 0)
                return null;

            List<DtoEncabezadoIntercambio> encabezados = resultado[0] as List<DtoEncabezadoIntercambio>;

            if (encabezados == null || encabezados.Count == 0)
                return null;

            var encabezado = encabezados.First();

            if (resultado.Count > 1)
                encabezado.Detalles = (resultado[1] as List<DtoDetalleIntercambio>) ?? new List<DtoDetalleIntercambio>();

            return encabezado;

        }
    }
}
