﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioVW_EtiquetasArticulosCompuestos : Repositorio<VW_EtiquetasArticulosCompuestos>, IRepositorioVW_EtiquetasArticulosCompuestos
    {
        public RepositorioVW_EtiquetasArticulosCompuestos() : base(new Contextos.AlmacenContext()) { }

        public List<VW_EtiquetasArticulosCompuestos> ObtenerEtiquetasFiltradas(string nombreArticulo = "", DateTime? diaIngreso = null)
        {
            Expression<Func<VW_EtiquetasArticulosCompuestos, bool>> filtro = null;

            if (!string.IsNullOrWhiteSpace(nombreArticulo))
            { 
                var nombreTrim = nombreArticulo.Trim().ToUpper();

                filtro = m => m.NombreArticulo.Trim().ToUpper().Contains(nombreTrim);
            }
            if (diaIngreso.HasValue) 
            {
                if (filtro == null)
                    filtro = m => m.FechaElaboracion.Value.Year == diaIngreso.Value.Year
                                 && m.FechaElaboracion.Value.Month == diaIngreso.Value.Month
                                 && m.FechaElaboracion.Value.Day == diaIngreso.Value.Day;
                else
                    filtro = filtro.Compose(m => m.FechaElaboracion.Value.Year == diaIngreso.Value.Year
                                 && m.FechaElaboracion.Value.Month == diaIngreso.Value.Month
                                 && m.FechaElaboracion.Value.Day == diaIngreso.Value.Day, Expression.AndAlso);
            }

            if (filtro == null)
                return contexto.Set<VW_EtiquetasArticulosCompuestos>().ToList();

            return contexto.Set<VW_EtiquetasArticulosCompuestos>().Where(filtro).ToList();
        }
    }
}
