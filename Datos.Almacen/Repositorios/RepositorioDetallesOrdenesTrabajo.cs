﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioDetallesOrdenesTrabajo : Repositorio<ZctDetOC>, IRepositorioDetallesOrdenesTrabajo
    {
        public RepositorioDetallesOrdenesTrabajo() : base(new Contextos.AlmacenContext()) { }

        public void SP_ZctDetOT_EDICION(int Cod_EncOT, int Cod_DetOT, string Cod_Art, int Ctd_Art, decimal CosArt_DetOT, decimal PreArt_DetOT, int CtdStd_DetOT,
                       DateTime Fch_DetOT, DateTime FchAplMov, int Cat_Alm, int Cod_Mot = 0, int tiempo_vida = 0, int cod_usu = 0)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(new SqlParameter("@TpConsulta", SqlDbType.Int) { Value = ModalidadesSP.CREACION_O_MODIFICACION });
            parametros.Add(new SqlParameter("@Cod_EncOT", SqlDbType.Int) { Value = Cod_EncOT });
            parametros.Add(new SqlParameter("@Cod_DetOT", SqlDbType.Int) { Value = Cod_DetOT });
            parametros.Add(new SqlParameter("@Cod_Art", SqlDbType.VarChar) { Value = Cod_Art });
            parametros.Add(new SqlParameter("@Ctd_Art", SqlDbType.Int) { Value = Ctd_Art });
            parametros.Add(new SqlParameter("@CosArt_DetOT", SqlDbType.Decimal) { Value = CosArt_DetOT });
            parametros.Add(new SqlParameter("@PreArt_DetOT", SqlDbType.Decimal) { Value = PreArt_DetOT });
            parametros.Add(new SqlParameter("@CtdStd_DetOT", SqlDbType.Int) { Value = CtdStd_DetOT });
            parametros.Add(new SqlParameter("@Fch_DetOT", SqlDbType.SmallDateTime) { Value = Fch_DetOT });
            parametros.Add(new SqlParameter("@FchAplMov", SqlDbType.SmallDateTime) { Value = FchAplMov });
            parametros.Add(new SqlParameter("@Cat_Alm", SqlDbType.Int) { Value = Cat_Alm });
            parametros.Add(new SqlParameter("@Cod_Mot", SqlDbType.Int) { Value = Cod_Mot });
            //parametros.Add(new SqlParameter("@Cod_TpArt", SqlDbType.VarChar) { Value = Cod_TpArt });
            parametros.Add(new SqlParameter("@tiempo_vida", SqlDbType.Int) { Value = tiempo_vida });
            parametros.Add(new SqlParameter("@cod_usu", SqlDbType.Int) { Value = cod_usu });

            var nombresParametros = string.Join(", ", parametros.Select(m => m.ParameterName));

            contexto.EjecutarComandoSql("exec dbo.SP_ZctDetOT " + nombresParametros, parametros.ToArray());
        }
    }
}
