﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioFolios : Repositorio<Folio>, IRepositorioFolios
    {
        public RepositorioFolios() : base(new Contextos.AlmacenContext()) { }

        public DtoFolio SP_ZctCatFolios(string tipoFolio)
        {
            SqlParameter sp_tipoCosulta = new SqlParameter("@TpConsulta", ModalidadesSP.CREACION_O_MODIFICACION);
            sp_tipoCosulta.DbType = System.Data.DbType.Int32;

            SqlParameter sp_tipoFolio = new SqlParameter("@Cod_Folio", tipoFolio);
            sp_tipoFolio.DbType = System.Data.DbType.String;

            return contexto.ConsultaSql<DtoFolio>("exec dbo.SP_ZctCatFolios @TpConsulta, @Cod_Folio", sp_tipoCosulta, sp_tipoFolio).FirstOrDefault();
        }
    }
}
