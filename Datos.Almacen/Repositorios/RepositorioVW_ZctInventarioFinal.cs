﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioVW_ZctInventarioFinal : Repositorio<VW_ZctInventarioFinal>, IRepositorioVW_ZctInventarioFinal
    {
        public RepositorioVW_ZctInventarioFinal() : base(new Contextos.AlmacenContext()) { }
    }
}
