﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioPresentaciones : Repositorio<Presentacion>, IRepositorioPresentaciones
    {
        public RepositorioPresentaciones() : base(new Contextos.AlmacenContext()) { }

        public Presentacion ObtenerConPresentacionesEntrada(int idPresentacion)
        {
            return (from presentacion in contexto.Set<Presentacion>()
                    where presentacion.id == idPresentacion
                    select new
                    {
                        presentacion,
                        conversionesEntrada = presentacion.ConversionesEntrada.Where(m => m.Activa)
                    }).ToList().Select(m => m.presentacion).FirstOrDefault();
        }
    }
}
