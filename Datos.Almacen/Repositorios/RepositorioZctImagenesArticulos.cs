﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioZctImagenesArticulos : Repositorio<ZctImagenesArticulos>, IRepositorioZctImagenesArticulos
    {
        public RepositorioZctImagenesArticulos() : base(new Contextos.AlmacenContext()) { }

        public int ObtenerUltimoNumero(string idArticulo)
        {
            return (from m in contexto.Set<ZctImagenesArticulos>()
                    where m.cod_art == idArticulo
                    orderby m.numimg descending
                    select m.numimg).FirstOrDefault();
        }
    }
}
