﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioConversiones : Repositorio<Conversiones>, IRepositorioConversiones
    {
        public RepositorioConversiones() : base(new Contextos.AlmacenContext()) { }
    }
}
