﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioZctEncMovInv : Repositorio<ZctEncMovInv>,  IRepositorioZctEncMovInv
    {
        public RepositorioZctEncMovInv() : base(new Contextos.AlmacenContext()) { }


        public void SP_ZctEncMovInv_EDICION(int CodMov_Inv, string TpMov_Inv, decimal CosMov_Inv, string FolOS_Inv, string Cod_Art, decimal CtdMov_Inv, string TpOs_Inv, DateTime FchMov_Inv, int Cod_Alm) 
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(new SqlParameter("@TpConsulta", SqlDbType.Int) { Value = ModalidadesSP.CREACION_O_MODIFICACION });
            parametros.Add(new SqlParameter("@CodMov_Inv", SqlDbType.Int) { Value = CodMov_Inv });
            parametros.Add(new SqlParameter("@TpMov_Inv", SqlDbType.Char) { Value = TpMov_Inv });
            parametros.Add(new SqlParameter("@CosMov_Inv", SqlDbType.Money) { Value = CosMov_Inv });
            parametros.Add(new SqlParameter("@FolOS_Inv", SqlDbType.VarChar) { Value = FolOS_Inv });
            parametros.Add(new SqlParameter("@Cod_Art", SqlDbType.VarChar) { Value = Cod_Art });
            parametros.Add(new SqlParameter("@CtdMov_Inv", SqlDbType.Decimal) { Value = CtdMov_Inv });
            parametros.Add(new SqlParameter("@TpOs_Inv", SqlDbType.Char) { Value = TpOs_Inv });
            parametros.Add(new SqlParameter("@FchMov_Inv", SqlDbType.SmallDateTime) { Value = FchMov_Inv });
            parametros.Add(new SqlParameter("@Cod_Alm", SqlDbType.Int) { Value = Cod_Alm });

            var nombresParametros = string.Join(", ", parametros.Select(m => m.ParameterName));

            contexto.EjecutarComandoSql("exec dbo.SP_ZctEncMovInv " + nombresParametros, parametros.ToArray());

            /*
         
             ALTER PROCEDURE [dbo].[SP_ZctEncMovInv]
                @TpConsulta Int,
                @CodMov_Inv int,
                @TpMov_Inv  char(2),
                @CosMov_Inv money,
                @FolOS_Inv varchar(10),
                @Cod_Art varchar(20),
                @CtdMov_Inv	numeric(18,10),
                @TpOs_Inv char(2),
                @FchMov_Inv smalldatetime,
                @Cod_Alm int

                --<@Param2, sysname, @p2> <Datatype_For_Param2, , int> = <Default_Value_For_Param2, , 0>
            AS
         
             */
        }


        public decimal ObtenerUltimoCostoMovimiento(string codigoArticulo)
        {
            return (from movimiento in contexto.Set<ZctEncMovInv>()
                    where movimiento.Cod_Art == codigoArticulo
                    && movimiento.CtdMov_Inv.HasValue
                    && movimiento.CtdMov_Inv != 0
                    orderby movimiento.FchMov_Inv descending
                    select (movimiento.CosMov_Inv ?? 0) / movimiento.CtdMov_Inv.Value).FirstOrDefault();
        }

        public List<DtoItemKardex> SP_ZctKdxInv(string cod_Art, int cod_Alm, DateTime fchIni, DateTime fchFin) 
        {
            var sp_cod_Art = new SqlParameter("@Cod_Art", cod_Art);
            sp_cod_Art.SqlDbType = SqlDbType.VarChar;

            var sp_cod_Alm = new SqlParameter("@Cod_Alm", cod_Alm);
            sp_cod_Alm.SqlDbType = SqlDbType.Int;

            var sp_fchIni = new SqlParameter("@FchIni", fchIni);
            sp_fchIni.SqlDbType = SqlDbType.SmallDateTime;

            var sp_fchFin = new SqlParameter("@FchFin", fchFin);
            sp_fchFin.SqlDbType = SqlDbType.SmallDateTime;

            return contexto.ConsultaSql<DtoItemKardex>("exec dbo.SP_ZctKdxInv @Cod_Art, @Cod_Alm, @FchIni, @FchFin",
                                                        sp_cod_Art, sp_cod_Alm, sp_fchIni, sp_fchFin);

            //
        }
    }
}
