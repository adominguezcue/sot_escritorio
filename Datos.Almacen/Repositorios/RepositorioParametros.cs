﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioParametros : Repositorio<Modelo.Almacen.Entidades.ZctCatPar>, IRepositorioParametros
    {
        public RepositorioParametros() : base(new Contextos.AlmacenContext()) { }
    }
}
