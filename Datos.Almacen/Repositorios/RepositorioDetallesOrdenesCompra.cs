﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioDetallesOrdenesCompra : Repositorio<ZctDetOC>, IRepositorioDetallesOrdenesCompra
    {
        public RepositorioDetallesOrdenesCompra() : base(new Contextos.AlmacenContext()) { }

        //public List<DtoDetalleOC> SP_ZctDetOC_CONSULTA(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, decimal cantidadArticulo, decimal costoArticulo, decimal cantidadEnStock, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA) 
        //{ 
        //    SqlParameter sp_tipoCosulta = new SqlParameter("@TpConsulta", ModalidadesSP.CONSULTA);
        //    sp_tipoCosulta.DbType = System.Data.DbType.Int32;

        //    SqlParameter sp_codigoOrdenCompra = new SqlParameter("@Cod_OC", codigoOrdenCompra);
        //    sp_codigoOrdenCompra.DbType = System.Data.DbType.Int32;

        //    SqlParameter sp_codigoDetalle= new SqlParameter("@CodDet_Oc", codigoDetalle);
        //    sp_codigoDetalle.DbType = System.Data.DbType.Int32;

        //    SqlParameter sp_codigoArticulo = new SqlParameter("@Cod_Art", codigoArticulo);
        //    sp_codigoArticulo.DbType = System.Data.DbType.String;

        //    SqlParameter sp_cantidadArticulo = new SqlParameter("@Ctd_Art", cantidadArticulo);
        //    sp_cantidadArticulo.DbType = System.Data.DbType.Decimal;

        //    SqlParameter sp_costoArticulo = new SqlParameter("@Cos_Art", costoArticulo);
        //    sp_costoArticulo.DbType = System.Data.DbType.Decimal;

        //    SqlParameter cantidadSurtido = new SqlParameter("@CtdStdDet_OC", cantidadEnStock);
        //    cantidadSurtido.DbType = System.Data.DbType.Decimal;

        //    SqlParameter sp_fechaMovimiento = new SqlParameter("@FchAplMov", fechaAplicacion);
        //    sp_fechaMovimiento.DbType = System.Data.DbType.DateTime;

        //    SqlParameter sp_idAlmacen = new SqlParameter("@Cat_Alm", idAlmacen);
        //    sp_idAlmacen.DbType = System.Data.DbType.Int32;

        //    SqlParameter sp_omitirIVA = new SqlParameter("@omitirIVA", omitirIVA);
        //    sp_omitirIVA.DbType = System.Data.DbType.Boolean;

        //    return contexto.ConsultaSql<DtoDetalleOC>("exec dbo.SP_ZctDetOC @TpConsulta, @Cod_OC, @CodDet_Oc, @Cod_Art, @Ctd_Art, @Cos_Art, @CtdStdDet_OC, @FchAplMov, @Cat_Alm, @omitirIVA", 
        //                                              sp_tipoCosulta, sp_codigoOrdenCompra, sp_codigoDetalle, sp_codigoArticulo, sp_cantidadArticulo,
        //                                              sp_costoArticulo, cantidadSurtido, sp_fechaMovimiento, sp_idAlmacen, sp_omitirIVA);
        //}

        public List<DtoDetalleOC> ObtenerDetallesOrdenCompra(int codigoOrdenCompra)
        {
            return (from det in contexto.Set<ZctDetOC>()
                    where det.Cod_OC == codigoOrdenCompra
                    select new
                    {
                        det.Cod_OC,
                        det.CodDet_OC,
                        det.Cod_Art,
                        det.ZctCatArt.upc,
                        det.Ctd_Art,
                        det.Cos_Art,
                        det.CtdStdDet_OC,
                        det.ZctCatArt.Desc_Art,
                        det.ZctCatArt.Exist_Art,
                        det.Cat_Alm,
                        marca = det.ZctCatArt.ZctCatMar != null ? det.ZctCatArt.ZctCatMar.Desc_Mar : null,
                        det.OmitirIVA
                    }).ToList().Select(det => new DtoDetalleOC
                    {
                        Cod_OC = det.Cod_OC,
                        CodDet_Oc = det.CodDet_OC,
                        Cod_Art = det.Cod_Art,
                        upc = det.upc,
                        Ctd_Art = det.Ctd_Art,
                        Cos_Art = det.Cos_Art,
                        CtdStdDet_OC = det.CtdStdDet_OC,
                        Desc_Art = det.Desc_Art,
                        Exist_Art = det.Exist_Art,
                        Cat_Alm = det.Cat_Alm,
                        marca = det.marca,
                        OmitirIVA = det.OmitirIVA
                    }).ToList();

            /*
             SELECT ZctDetOC.Cod_OC, ZctDetOC.CodDet_OC, ZctDetOC.Cod_Art,ZctCatArt.upc, ZctDetOC.Ctd_Art, ZctDetOC.Cos_Art, ZctDetOC.CtdStdDet_OC, ZctCatArt.Desc_Art, 
                      ZctCatArt.Exist_Art, ZctDetOC.Cat_Alm,ZctCatMar.Desc_Mar as marca, ZctDetOC.OmitirIVA
					  FROM ZctDetOC INNER JOIN
                      ZctCatArt ON ZctDetOC.Cod_Art = ZctCatArt.Cod_Art 
					  LEFT JOIN ZctCatMar ON ZctCatMar.Cod_Mar = ZctCatArt.Cod_Mar  
					  WHERE ZctDetOC.Cod_OC = @Cod_OC
             */
        }

        public void SP_ZctDetOC_EDICION(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, decimal cantidadArticulo, decimal costoArticulo, decimal cantidadSurtida, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA, bool _EsCancelado)
        {
            SqlParameter sp_tipoCosulta = new SqlParameter("@TpConsulta", ModalidadesSP.CREACION_O_MODIFICACION);
            sp_tipoCosulta.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoOrdenCompra = new SqlParameter("@Cod_OC", codigoOrdenCompra);
            sp_codigoOrdenCompra.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoDetalle = new SqlParameter("@CodDet_Oc", codigoDetalle);
            sp_codigoDetalle.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoArticulo = new SqlParameter("@Cod_Art", codigoArticulo);
            sp_codigoArticulo.DbType = System.Data.DbType.String;

            SqlParameter sp_cantidadArticulo = new SqlParameter("@Ctd_Art", cantidadArticulo);
            sp_cantidadArticulo.DbType = System.Data.DbType.Decimal;

            SqlParameter sp_costoArticulo = new SqlParameter("@Cos_Art", costoArticulo);
            sp_costoArticulo.DbType = System.Data.DbType.Decimal;

            SqlParameter cantidadSurtido = new SqlParameter("@CtdStdDet_OC", cantidadSurtida);
            cantidadSurtido.DbType = System.Data.DbType.Decimal;

            SqlParameter sp_fechaMovimiento = new SqlParameter("@FchAplMov", fechaAplicacion);
            sp_fechaMovimiento.DbType = System.Data.DbType.DateTime;

            SqlParameter sp_idAlmacen = new SqlParameter("@Cat_Alm", idAlmacen);
            sp_idAlmacen.DbType = System.Data.DbType.Int32;

            SqlParameter sp_omitirIVA = new SqlParameter("@omitirIVA", omitirIVA);
            sp_omitirIVA.DbType = System.Data.DbType.Boolean;

            SqlParameter sp_EsCancelada = new SqlParameter("@EsCancelada", _EsCancelado);
            sp_EsCancelada.DbType = System.Data.DbType.Boolean;

            contexto.EjecutarComandoSql("exec dbo.SP_ZctDetOC @TpConsulta, @Cod_OC, @CodDet_Oc, @Cod_Art, @Ctd_Art, @Cos_Art, @CtdStdDet_OC, @FchAplMov, @Cat_Alm, @omitirIVA, @EsCancelada",
                                                      sp_tipoCosulta, sp_codigoOrdenCompra, sp_codigoDetalle, sp_codigoArticulo, sp_cantidadArticulo,
                                                      sp_costoArticulo, cantidadSurtido, sp_fechaMovimiento, sp_idAlmacen, sp_omitirIVA, sp_EsCancelada);
        }

        public void SP_ZctDetOC_ELIMINACION(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, decimal cantidadArticulo, decimal costoArticulo, decimal cantidadSurtida, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA)
        {
            SqlParameter sp_tipoCosulta = new SqlParameter("@TpConsulta", ModalidadesSP.ELIMINACION);
            sp_tipoCosulta.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoOrdenCompra = new SqlParameter("@Cod_OC", codigoOrdenCompra);
            sp_codigoOrdenCompra.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoDetalle = new SqlParameter("@CodDet_Oc", codigoDetalle);
            sp_codigoDetalle.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoArticulo = new SqlParameter("@Cod_Art", codigoArticulo);
            sp_codigoArticulo.DbType = System.Data.DbType.String;

            SqlParameter sp_cantidadArticulo = new SqlParameter("@Ctd_Art", cantidadArticulo);
            sp_cantidadArticulo.DbType = System.Data.DbType.Decimal;

            SqlParameter sp_costoArticulo = new SqlParameter("@Cos_Art", costoArticulo);
            sp_costoArticulo.DbType = System.Data.DbType.Decimal;

            SqlParameter cantidadSurtido = new SqlParameter("@CtdStdDet_OC", cantidadSurtida);
            cantidadSurtido.DbType = System.Data.DbType.Decimal;

            SqlParameter sp_fechaMovimiento = new SqlParameter("@FchAplMov", fechaAplicacion);
            sp_fechaMovimiento.DbType = System.Data.DbType.DateTime;

            SqlParameter sp_idAlmacen = new SqlParameter("@Cat_Alm", idAlmacen);
            sp_idAlmacen.DbType = System.Data.DbType.Int32;

            SqlParameter sp_omitirIVA = new SqlParameter("@omitirIVA", omitirIVA);
            sp_omitirIVA.DbType = System.Data.DbType.Boolean;

            contexto.EjecutarComandoSql("exec dbo.SP_ZctDetOC @TpConsulta, @Cod_OC, @CodDet_Oc, @Cod_Art, @Ctd_Art, @Cos_Art, @CtdStdDet_OC, @FchAplMov, @Cat_Alm, @omitirIVA",
                                                      sp_tipoCosulta, sp_codigoOrdenCompra, sp_codigoDetalle, sp_codigoArticulo, sp_cantidadArticulo,
                                                      sp_costoArticulo, cantidadSurtido, sp_fechaMovimiento, sp_idAlmacen, sp_omitirIVA);
        }
    }
}
