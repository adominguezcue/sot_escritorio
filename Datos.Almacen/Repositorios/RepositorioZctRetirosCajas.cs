﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioZctRetirosCajas : Repositorio<ZctRetirosCajas>, IRepositorioZctRetirosCajas
    {
        public RepositorioZctRetirosCajas() : base(new Contextos.AlmacenContext()) { }

        public int ObtenerUltimoCodigoRetiro(string Cod_folioCaja)
        {
            return (from r in contexto.Set<ZctRetirosCajas>()
                    where r.cod_folio == Cod_folioCaja
                    orderby r.cod_retiro descending
                    select r.cod_retiro).FirstOrDefault();
        }
    }
}
