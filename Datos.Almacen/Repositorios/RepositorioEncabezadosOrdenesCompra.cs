﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioEncabezadosOrdenesCompra : Repositorio<ZctEncOC>, IRepositorioEncabezadosOrdenesCompra
    {
        public RepositorioEncabezadosOrdenesCompra() : base(new Contextos.AlmacenContext()) { }

        public List<DtoEncabezadoOC> ObtenerResumenesEncabezadosOC()
        {
            //SELECT Cod_OC, Cod_Prov, Fch_OC, FchAplMov, Fac_OC, FchFac_OC,"status", Servicios FROM [ZctEncOC]

            return (from oc in contexto.Set<ZctEncOC>()
                    select new
                    {
                        oc.Cod_OC,
                        oc.Cod_Prov,
                        oc.Fch_OC,
                        oc.FchAplMov,
                        oc.Fac_OC,
                        oc.FchFac_OC,
                        oc.status,
                        oc.Servicios
                    }).ToList().Select(m => new DtoEncabezadoOC
                    {
                        Cod_OC = m.Cod_OC,
                        Cod_Prov = m.Cod_Prov,
                        Fch_OC = m.Fch_OC,
                        FchAplMov = m.FchAplMov,
                        Fac_OC = m.Fac_OC,
                        FchFac_OC = m.FchFac_OC,
                        status = m.status,
                        Servicios = m.Servicios
                    }).ToList();
        }

        public DtoEncabezadoOC ObtenerResumenEncabezadoOC(int codigoOrdenCompra)
        {
            //SELECT Cod_OC, Cod_Prov, Fch_OC, FchAplMov, Fac_OC, FchFac_OC,"status", Servicios FROM [ZctEncOC]

            return (from oc in contexto.Set<ZctEncOC>()
                    where oc.Cod_OC == codigoOrdenCompra
                    select new
                    {
                        oc.Cod_OC,
                        oc.Cod_Prov,
                        oc.Fch_OC,
                        oc.FchAplMov,
                        oc.Fac_OC,
                        oc.FchFac_OC,
                        oc.status,
                        oc.Servicios,
                        oc.IdConceptoGasto
                    }).ToList().Select(m => new DtoEncabezadoOC
                    {
                        Cod_OC = m.Cod_OC,
                        Cod_Prov = m.Cod_Prov,
                        Fch_OC = m.Fch_OC,
                        FchAplMov = m.FchAplMov,
                        Fac_OC = m.Fac_OC,
                        FchFac_OC = m.FchFac_OC,
                        status = m.status,
                        Servicios = m.Servicios,
                        IdConceptoGasto = m.IdConceptoGasto
                    }).FirstOrDefault();
        }

        //public List<DtoEncabezadoOC> SP_ZctEncOC_CONSULTA(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra, DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion)
        //{
        //    SqlParameter sp_tipoCosulta = new SqlParameter("@TpConsulta", ModalidadesSP.CONSULTA);
        //    sp_tipoCosulta.DbType = System.Data.DbType.Int32;

        //    SqlParameter sp_codigoOrdenCompra = new SqlParameter("@Cod_OC", codigoOrdenCompra);
        //    sp_codigoOrdenCompra.DbType = System.Data.DbType.Int32;

        //    SqlParameter sp_codigoProveedor = new SqlParameter("@Cod_Prov", codigoProveedor);
        //    sp_codigoProveedor.DbType = System.Data.DbType.Int32;

        //    SqlParameter sp_fechaOrdenCompra = new SqlParameter("@Fch_OC", fechaOrdenCompra);
        //    sp_fechaOrdenCompra.DbType = System.Data.DbType.DateTime;

        //    SqlParameter sp_fechaMovimiento = new SqlParameter("@FchAplMov", fechaAplicacionMovimiento);
        //    sp_fechaMovimiento.DbType = System.Data.DbType.DateTime;

        //    SqlParameter sp_facturaOrdenCompra = new SqlParameter("@Fac_OC", facturaOrdenCompra);
        //    sp_facturaOrdenCompra.DbType = System.Data.DbType.String;

        //    SqlParameter sp_fechaFacturacion = new SqlParameter("@FchFac_OC", fechaFacturacion);
        //    sp_fechaFacturacion.DbType = System.Data.DbType.DateTime;

        //    return contexto.ConsultaSql<DtoEncabezadoOC>("exec dbo.SP_ZctEncOC @TpConsulta, @Cod_OC, @Cod_Prov, @Fch_OC, @FchAplMov, @Fac_OC, @FchFac_OC", 
        //                                                 sp_tipoCosulta, sp_codigoOrdenCompra, sp_codigoProveedor, sp_fechaOrdenCompra,
        //                                                 sp_fechaMovimiento, sp_facturaOrdenCompra, sp_fechaFacturacion);             
        //}
        
        public void SP_ZctEncOC_EDICION(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra, DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion, string estado, bool? servicio)
        {
            SqlParameter sp_tipoCosulta = new SqlParameter("@TpConsulta", ModalidadesSP.CREACION_O_MODIFICACION);
            sp_tipoCosulta.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoOrdenCompra = new SqlParameter("@Cod_OC", codigoOrdenCompra);
            sp_codigoOrdenCompra.DbType = System.Data.DbType.Int32;

            SqlParameter sp_codigoProveedor = new SqlParameter("@Cod_Prov", codigoProveedor);
            sp_codigoProveedor.DbType = System.Data.DbType.Int32;

            SqlParameter sp_fechaOrdenCompra = new SqlParameter("@Fch_OC", fechaOrdenCompra);
            sp_fechaOrdenCompra.DbType = System.Data.DbType.DateTime;

            SqlParameter sp_fechaMovimiento = new SqlParameter("@FchAplMov", fechaAplicacionMovimiento);
            sp_fechaMovimiento.DbType = System.Data.DbType.DateTime;

            SqlParameter sp_facturaOrdenCompra = new SqlParameter("@Fac_OC", facturaOrdenCompra);
            sp_facturaOrdenCompra.DbType = System.Data.DbType.String;

            SqlParameter sp_fechaFacturacion = new SqlParameter("@FchFac_OC", fechaFacturacion);
            sp_fechaFacturacion.DbType = System.Data.DbType.DateTime;

            SqlParameter sp_estado = new SqlParameter("@estado", estado);
            sp_estado.DbType = System.Data.DbType.String;

            SqlParameter sp_servicio = new SqlParameter("@servicio", (servicio as object) ?? DBNull.Value);
            sp_servicio.DbType = System.Data.DbType.Boolean;
            sp_servicio.IsNullable = true;

            contexto.EjecutarComandoSql("exec dbo.SP_ZctEncOC @TpConsulta, @Cod_OC, @Cod_Prov, @Fch_OC, @FchAplMov, @Fac_OC, @FchFac_OC, @estado, @servicio",
                                                         sp_tipoCosulta, sp_codigoOrdenCompra, sp_codigoProveedor, sp_fechaOrdenCompra,
                                                         sp_fechaMovimiento, sp_facturaOrdenCompra, sp_fechaFacturacion, sp_estado, sp_servicio);
        }
    }
}
