﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioZctEncUbicacionValores : Repositorio<ZctEncUbicacionValores>, IRepositorioZctEncUbicacionValores
    {
        public RepositorioZctEncUbicacionValores() : base(new Contextos.AlmacenContext()) { }

        public int ObtenerUltimoFolio()
        {
            return (from u in contexto.Set<ZctEncUbicacionValores>()
                    orderby u.Cod_encUbicVal descending
                    select u.Cod_encUbicVal).FirstOrDefault();
        }


        public decimal ObtenerTotal(DateTime fecha)
        {
            return (from det in contexto.Set<ZctDetalleUbicacionValores>()
                    where det.ZctEncUbicacionValores.fecha == fecha
                    select det.monto ?? 0).ToList().Sum(m => m);
        }
    }
}
