﻿using Modelo.Almacen;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Datos.Nucleo.Repositorios;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;
using Datos.Almacen.Auxiliares;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioEncabezadosOrdenesTrabajo : Repositorio<ZctEncOT>, IRepositorioEncabezadosOrdenesTrabajo
    {
        public RepositorioEncabezadosOrdenesTrabajo() : base(new Contextos.AlmacenContext()) { }

        public void SP_ZctDetOT_Consulta(int CodEnc_OT, DateTime FchDoc_OT, string Cod_Cte, int Cod_Mot, int Cod_Tec,
                                         DateTime FchEnt, DateTime FchSal, DateTime FchEntRmp, DateTime FchSalRmp,
                                         string ObsOT, DateTime FchAplMov, int UltKm_OT, string Cod_Estatus, string HistEstatus_OT,
                                         int Cod_TpOT) 
        {
            var SP_CodEnc_OT = new SqlParameter("@CodEnc_OT", SqlDbType.Int);
            SP_CodEnc_OT.Value = CodEnc_OT;

            var SP_FchDoc_OT = new SqlParameter("@FchDoc_OT", SqlDbType.SmallDateTime);
            SP_FchDoc_OT.Value = FchDoc_OT;

            var SP_Cod_Cte = new SqlParameter("@Cod_Cte", SqlDbType.VarChar);
            SP_Cod_Cte.Value = Cod_Cte;

            var SP_Cod_Mot = new SqlParameter("@Cod_Mot", SqlDbType.Int);
            SP_Cod_Mot.Value = Cod_Mot;

            var SP_Cod_Tec = new SqlParameter("@Cod_Tec", SqlDbType.Int);
            SP_Cod_Tec.Value = Cod_Tec;

            var SP_FchEnt = new SqlParameter("@FchEnt", SqlDbType.SmallDateTime);
            SP_FchEnt.Value = FchEnt;

            var SP_FchSal = new SqlParameter("@FchSal", SqlDbType.SmallDateTime);
            SP_FchSal.Value = FchSal;

            var SP_FchEntRmp = new SqlParameter("@FchEntRmp", SqlDbType.SmallDateTime);
            SP_FchEntRmp.Value = FchEntRmp;

            var SP_FchSalRmp = new SqlParameter("@FchSalRmp", SqlDbType.SmallDateTime);
            SP_FchSalRmp.Value = FchSalRmp;

            var SP_ObsOT = new SqlParameter("@ObsOT", SqlDbType.Text);
            SP_ObsOT.Value = ObsOT;

            var SP_FchAplMov = new SqlParameter("@FchAplMov", SqlDbType.SmallDateTime);
            SP_FchAplMov.Value = FchAplMov;

            var SP_UltKm_OT = new SqlParameter("@UltKm_OT", SqlDbType.Int);
            SP_UltKm_OT.Value = UltKm_OT;

            var SP_Cod_Estatus = new SqlParameter("@Cod_Estatus", SqlDbType.Int);
            SP_Cod_Estatus.Value = Cod_Estatus;

            var SP_HistEstatus_OT = new SqlParameter("@HistEstatus_OT", SqlDbType.Int);
            SP_HistEstatus_OT.Value = HistEstatus_OT;

            var SP_Cod_TpOT = new SqlParameter("@Cod_TpOT", SqlDbType.Int);
            SP_Cod_TpOT.Value = Cod_TpOT;
 
            //contexto.ConsultaSql<object>("[dbo].[SP_ZctDetOT]")
        }

        public void SP_ZctEncOT_Edicion(int CodEnc_OT, string Cod_folio, DateTime FchDoc_OT, int Cod_Cte, int Cod_Contacto, int Cod_Mot, int Cod_Tec, DateTime FchEnt, DateTime FchSal,
                       DateTime FchEntRmp, DateTime FchSalRmp, string ObsOT, DateTime FchAplMov, int UltKm_OT, int Cod_TpOT, string Cod_Estatus = "N", string HistEstatus_OT = null)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            parametros.Add(new SqlParameter("@TpConsulta", SqlDbType.Int) { Value = ModalidadesSP.CREACION_O_MODIFICACION });

            parametros.Add(new SqlParameter("@CodEnc_OT", SqlDbType.Int) { Value = CodEnc_OT });

            parametros.Add(new SqlParameter("@Cod_folio", SqlDbType.VarChar) { Value = Cod_folio });

            parametros.Add(new SqlParameter("@FchDoc_OT", SqlDbType.SmallDateTime) { Value = FchDoc_OT });

            parametros.Add(new SqlParameter("@Cod_Cte", SqlDbType.VarChar) { Value = Cod_Cte });

            parametros.Add(new SqlParameter("@Cod_Contacto", SqlDbType.Int) { Value = Cod_Contacto });

            parametros.Add(new SqlParameter("@Cod_Mot", SqlDbType.Int) { Value = Cod_Mot });

            parametros.Add(new SqlParameter("@Cod_Tec", SqlDbType.Int) { Value = Cod_Tec });

            parametros.Add(new SqlParameter("@FchEnt", SqlDbType.SmallDateTime) { Value = FchEnt });

            parametros.Add(new SqlParameter("@FchSal", SqlDbType.SmallDateTime) { Value = FchSal });

            parametros.Add(new SqlParameter("@FchEntRmp", SqlDbType.SmallDateTime) { Value = FchEntRmp });

            parametros.Add(new SqlParameter("@FchSalRmp", SqlDbType.SmallDateTime) { Value = FchSalRmp });

            parametros.Add(new SqlParameter("@ObsOT", SqlDbType.Text) { Value = ObsOT });

            parametros.Add(new SqlParameter("@FchAplMov", SqlDbType.SmallDateTime) { Value = FchAplMov });

            parametros.Add(new SqlParameter("@UltKm_OT", SqlDbType.Int) { Value = UltKm_OT });

            parametros.Add(new SqlParameter("@Cod_Estatus", SqlDbType.Char) { Value = Cod_Estatus });

            parametros.Add(new SqlParameter("@HistEstatus_OT", SqlDbType.VarChar) { Value = HistEstatus_OT });

            parametros.Add(new SqlParameter("@Cod_TpOT", SqlDbType.Int) { Value = Cod_TpOT });

            var nombresParametros = string.Join(", ", parametros.Select(m => m.ParameterName));

            contexto.EjecutarComandoSql("exec dbo.SP_ZctEncOT " + nombresParametros, parametros.ToArray());
        }
    }
}
