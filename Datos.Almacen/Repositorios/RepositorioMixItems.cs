﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioMixItems : Repositorio<ZctMixItems>, IRepositorioMixItems
    {
        public RepositorioMixItems() : base(new Contextos.AlmacenContext()) { }

        public List<ZctMixItems> GetMixItems(string codArt)
        {
            var r = (from m in contexto.Set<ZctMixItems>()
                     where m.Cod_Art.Equals(codArt)
                     select new
                     {
                         m,
                         DescArt = m.ArticuloIngrediente.Desc_Art,
                         NombrePresentacion = m.ArticuloIngrediente.ZctCatPresentacion.presentacion
                     }).ToList();

            foreach (var item in r)
            {
                item.m.DescArt = item.DescArt;
                item.m.NombrePresentacionTMP = item.NombrePresentacion;
            }

            return r.Select(m => m.m).ToList();
        }

        public List<ZctMixItems> GetMixItemsCost(string codArt, int codAlm)
        {
            var r = (from m in contexto.Set<ZctMixItems>()
                     join aa in contexto.Set<Modelo.Almacen.Entidades.ArticuloAlmacen>() on m.Cod_Art_Mix equals aa.Cod_Art into items
                     from i in items.DefaultIfEmpty()
                     where m.Cod_Art.Equals(codArt)
                     && (i == null || i.Cod_Alm == codAlm)
                     select new
                     {
                         m,
                         Exist = i == null ? 0 : (decimal)i.Exist_Art,
                         Cost = i == null ? 0 : (decimal)i.CostoProm_Art,
                         costoArt = m.ArticuloIngrediente.Cos_Art ?? 0
                     }).ToList();

            r.ForEach(m =>
            {
                m.m.Exist = m.Exist;
                m.m.Cost = m.Cost;
                m.m.CostoArt = m.costoArt;
            });

            return r.Select(m => m.m).ToList();
        }

        public List<ZctDetOP> GetDetOP(int codOP)
        {
            var r = (from d in contexto.Set<ZctDetOP>()
                     where d.Cod_OP.Equals(codOP)
                     select new
                       {
                           d,
                           //SubTotal = (decimal)d.Cantidad * (decimal)d.Costo,
                           DescArt = d.ZctCatArt.Desc_Art
                       }).ToList();

            r.ForEach(m =>
            {
                m.d.SubTotal = (m.d.Cantidad ?? 0) * (m.d.Costo ?? 0);
                m.d.DescArt = m.DescArt;
            });

            return r.Select(m => m.d).ToList();
        }

        public ZctEncOP GetEncOP(int codOP)
        {
            return (from e in contexto.Set<ZctEncOP>()
                    where e.Cod_OP == codOP
                    select e).FirstOrDefault();
        }



        //internal void SetProportion(ref List<ZctMixItems> listMix)
        //{
        //    foreach (MixItem m in listMix)
        //    {
        //        m.Cantidad = Decimal.Parse(m.Proporcion.Split('/')[0]);
        //        m.Base = Decimal.Parse(m.Proporcion.Split('/')[1]);
        //        //if (m.Proporcion.Split('/')[0].IndexOf(".") >= 0) {
        //        //  m.Cantidad = m.Cantidad * Convert.ToDecimal(Math.Pow(10, m.Proporcion.Split('/')[0].Length - m.Proporcion.Split('/')[0].IndexOf(".")-1));
        //        //  m.Base = m.Base * Convert.ToDecimal(Math.Pow(10, m.Proporcion.Split('/')[0].Length - m.Proporcion.Split('/')[0].IndexOf(".")-1));
        //        //}
        //    }
        //}
        //internal bool SaveMixList(List<ZctMixItems> listMix)
        //{
        //    bool isOk = false;
        //    using (ZctMixDBDataContext db = new ZctMixDBDataContext(_conn))
        //    {
        //        try
        //        {
        //            List<MixItem> prevListMix = (from p in db.MixItems
        //                                         where p.CodArt.Equals(listMix.First().CodArt)
        //                                         select p).ToList();

        //            if (prevListMix != null)
        //                db.MixItems.DeleteAllOnSubmit(prevListMix);
        //            SetProportion(ref listMix);
        //            db.MixItems.InsertAllOnSubmit(listMix);
        //            db.SubmitChanges();
        //            isOk = true;
        //        }
        //        catch
        //        {
        //        }
        //    }
        //    return isOk;
        //}



        //internal DataTable GetData(string sSPName, string sSpVariables)
        //{
        //    try
        //    {
        //        MethodData methodData = ClassGen.GetCallMethod(sSPName, sSpVariables);
        //        MethodInfo method = this.GetType().GetMethod(sSPName, BindingFlags.NonPublic | BindingFlags.Instance, null, methodData.Types, null);

        //        return (DataTable)method.Invoke(this, methodData.Params);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new DataTable();
        //    }
        //}

        //internal DataTable GetData(string sSPName, string sSpVariables, string sSpValues)
        //{
        //    try
        //    {
        //        MethodData methodData = ClassGen.GetCallMethod(sSPName, sSpVariables, sSpValues);
        //        MethodInfo method = this.GetType().GetMethod(sSPName, BindingFlags.NonPublic | BindingFlags.Instance, null, methodData.Types, null);

        //        return (DataTable)method.Invoke(this, methodData.Params);
        //    }
        //    catch (Exception ex)
        //    {
        //        return new DataTable();
        //    }
        //}

        //internal DataTable GetMixItemsTable(string codArt)
        //{
        //    return ClassGen.ToDataTable(GetMixItems(codArt));
        //}
        //internal List<ZctDetOP> SetAnioMes(List<ZctDetOP> detOP, ZctEncOP enc)
        //{
        //    using (ZctTools.Model.ZctSOTToolsDBDataContext db = new ZctTools.Model.ZctSOTToolsDBDataContext(_conn))
        //    {
        //        int? anio = 0; int? mes = 0;
        //        db.sp_get_anio_mes(enc.Fch_OP.Date, ref anio, ref mes);
        //        detOP = detOP.Select(det => { det.anio_fiscal = anio; det.mes_fiscal = mes; return det; }).ToList();
        //    }
        //    return detOP;
        //}
    }
}
