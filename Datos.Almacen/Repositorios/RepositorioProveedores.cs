﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioProveedores : Repositorio<Proveedor>, IRepositorioProveedores
    {
        public RepositorioProveedores() : base(new Contextos.AlmacenContext()) { }

        public int ObtenerUltimoCodigo()
        {
            return (from c in contexto.Set<Proveedor>()
                    orderby c.Cod_Prov descending
                    select c.Cod_Prov).FirstOrDefault();
        }

        public List<DtoResumenProveedor> ObtenerResumenesProveedores(string filtro = null)
        {
            var filtroU = filtro?.Trim().ToUpper();


            return (string.IsNullOrWhiteSpace(filtroU) ?
                (from c in contexto.Set<Proveedor>()
                 orderby c.Cod_Prov descending
                 select new
                 {
                     c.Cod_Prov,
                     c.Nom_Prov,
                     c.RFC_Prov,
                     c.NombreComercial
                 }) :

         (from c in contexto.Set<Proveedor>()
          where c.Nom_Prov.ToUpper().Contains(filtroU)
              || c.RFC_Prov.ToUpper().Contains(filtroU)
              || c.NombreComercial.ToUpper().Contains(filtroU)
          orderby c.Cod_Prov descending
          select new
          {
              c.Cod_Prov,
              c.Nom_Prov,
              c.RFC_Prov,
              c.NombreComercial
          })).ToList().Select(m => new DtoResumenProveedor
          {
              Codigo = m.Cod_Prov,
              Nombre = m.Nom_Prov,
              RFC = m.RFC_Prov,
              NombreComercial = m.NombreComercial
          }).ToList();
        }

        public List<Proveedor> ObtieneProveedoresPorFiltroNombre(string text)
        {
            var respuesta = new List<Proveedor>();
            var filtroU = text?.Trim().ToUpper();
            if (filtroU != null && filtroU.Length > 0)
            {
                respuesta = (from c in contexto.Set<Proveedor>()
                             where c.Nom_Prov.ToUpper().Contains(filtroU)
                            || c.RFC_Prov.ToUpper().Contains(filtroU)
                            || c.NombreComercial.ToUpper().Contains(filtroU)
                            select new { 
                            c}).Select(m=>m.c).ToList();
            }
            else
            {
                respuesta = (from c in contexto.Set<Proveedor>()
                             select new
                             {
                                 c
                             }).Select(m => m.c).ToList();
            }
            return respuesta;
        }
    }
}
