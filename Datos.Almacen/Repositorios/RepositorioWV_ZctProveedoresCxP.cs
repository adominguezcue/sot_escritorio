﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioWV_ZctProveedoresCxP : Repositorio<WV_ZctProveedoresCxP>, IRepositorioWV_ZctProveedoresCxP
    {
        public RepositorioWV_ZctProveedoresCxP() : base(new Contextos.AlmacenContext()) { }

        public List<WV_ZctProveedoresCxP> SP_ZctProveedoresCxP(int idEstado)
        {
            var p_idEstado = new SqlParameter("@idEstado", idEstado);
            p_idEstado.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<WV_ZctProveedoresCxP>("exec dbo.SP_ZctProveedoresCxP @idEstado", p_idEstado).ToList();
        }

        public List<WV_ZctProveedoresCxP> SP_TotalesCXCProveedor(int? idEstado)
        {
            var p_idEstado = new SqlParameter("@idEstado", idEstado);
            p_idEstado.DbType = System.Data.DbType.Int32;

            return contexto.ConsultaSql<WV_ZctProveedoresCxP>("exec dbo.SP_TotalesCXCProveedor @idEstado", p_idEstado).ToList();
        }
    }
}
