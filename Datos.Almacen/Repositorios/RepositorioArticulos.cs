﻿using Modelo.Almacen;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Datos.Nucleo.Repositorios;
using System.Data.Entity;
using Modelo.Almacen.Entidades.Dtos;
using System.Data.SqlClient;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioArticulos : Repositorio<Articulo>, IRepositorioArticulos
    {
        public RepositorioArticulos() : base(new Contextos.AlmacenContext())
        {
        }


        public List<Articulo> ObtenerArticulosPorTipo(string idTipo)
        {
            idTipo = idTipo.Trim();//.ToUpper();

            return (from tipoArticulo in contexto.Set<TipoArticulo>()
                    where/* tipoArticulo.Activo
                    && */tipoArticulo.Cod_TpArt == idTipo
                    select tipoArticulo.Articulos.Where(m => !m.desactivar)).ToList().Select(m => m.ToList()).FirstOrDefault() ?? new List<Articulo>();
        }


        public List<Articulo> ObtenerArticulosPorDepartamento(int idDepartamento)
        {
            return (from tipoArticulo in contexto.Set<Departamento>()
                    where/* tipoArticulo.Activo
                    && */tipoArticulo.Cod_Dpto == idDepartamento
                    select tipoArticulo.ZctCatLinea.SelectMany(m => m.Articulos).Where(m => !m.desactivar)).ToList().Select(m => m.ToList()).FirstOrDefault() ?? new List<Articulo>();
        }


        public List<Articulo> ObtenerArticulosConLineaYPresentacionPorId(List<string> idsArticulos)
        {
            return (from articulo in contexto.Set<Articulo>()
                    where idsArticulos.Contains(articulo.Cod_Art)
                    select new
                    {
                        articulo,
                        linea = articulo.ZctCatLinea,
                        unidad = articulo.ZctCatPresentacion
                    }).ToList().Select(m => m.articulo).ToList();
        }

        public List<DtoExistencias> SP_ConsultarExistencias(string codigoArticulo)
        {
            SqlParameter sp_idComanda = new SqlParameter("@codigoArticulo", codigoArticulo.Trim().ToUpper());
            sp_idComanda.DbType = System.Data.DbType.String;

            return contexto.ConsultaSql<DtoExistencias>("exec [dbo].[SP_ConsultarExistencias] @codigoArticulo", sp_idComanda).ToList();
        }


        public DtoResultadoBusquedaArticulo SP_ZctCatArt_Busqueda(string codigo, bool soloInventariable)
        {
            SqlParameter sp_codigo = new SqlParameter("@Codigo", codigo)
            {
                DbType = System.Data.DbType.String
            };

            SqlParameter sp_soloInventariable = new SqlParameter("@soloInventariable", soloInventariable)
            {
                DbType = System.Data.DbType.Boolean
            };

            return contexto.ConsultaSql<DtoResultadoBusquedaArticulo>("exec [dbo].[SP_ZctCatArt_Busqueda] @Codigo, @soloInventariable", sp_codigo, sp_soloInventariable).FirstOrDefault();
        }

        public List<DtoResultadoBusquedaArticuloSimple> ObtenerResumenesArticulosPorFiltro(string filtro, bool incluirArticulosPresentacion, bool soloInventariables, bool? EsInsumo, bool? _EsgestionArticulos) 
        {

            List<DtoResultadoBusquedaArticuloSimple> respuesta = new List<DtoResultadoBusquedaArticuloSimple>();
            if (string.IsNullOrWhiteSpace(filtro))
            {
                if (EsInsumo == true)
                {
                    if (_EsgestionArticulos == true)
                    {
                        respuesta = (from articulo in contexto.Set<Articulo>()
                                     where (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                     && articulo.Cod_TpArt == "I"
                                     && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                     select new
                                     {
                                         articulo.Cod_Art,
                                         articulo.Desc_Art,
                                         articulo.Prec_Art,
                                         articulo.Cos_Art,
                                         articulo.OmitirIVA,
                                         articulo.ZctCatTpArt.Desc_TpArt,
                                         articulo.ZctCatPresentacion.presentacion
                                     }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                     {
                                         Codigo = m.Cod_Art,
                                         Nombre = m.Desc_Art,
                                         Precio = m.Prec_Art,
                                         Costo = m.Cos_Art,
                                         OmitirIVA = m.OmitirIVA,
                                         Tipo = m.Desc_TpArt,
                                         NombreUnidad = m.presentacion
                                     }).ToList();

                    }
                    else
                    {
                        respuesta = (from articulo in contexto.Set<Articulo>()
                                     where (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                     && articulo.Cod_TpArt == "I"
                                     && articulo.desactivar == false
                                     && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                     select new
                                     {
                                         articulo.Cod_Art,
                                         articulo.Desc_Art,
                                         articulo.Prec_Art,
                                         articulo.Cos_Art,
                                         articulo.OmitirIVA,
                                         articulo.ZctCatTpArt.Desc_TpArt,
                                         articulo.ZctCatPresentacion.presentacion
                                     }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                     {
                                         Codigo = m.Cod_Art,
                                         Nombre = m.Desc_Art,
                                         Precio = m.Prec_Art,
                                         Costo = m.Cos_Art,
                                         OmitirIVA = m.OmitirIVA,
                                         Tipo = m.Desc_TpArt,
                                         NombreUnidad = m.presentacion
                                     }).ToList();
                    }
                }
                else
                {
                    if (_EsgestionArticulos == true)
                    {
                        respuesta = (from articulo in contexto.Set<Articulo>()
                                     where (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                     && articulo.Cod_TpArt != "I"
                                     && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                     select new
                                     {
                                         articulo.Cod_Art,
                                         articulo.Desc_Art,
                                         articulo.Prec_Art,
                                         articulo.Cos_Art,
                                         articulo.OmitirIVA,
                                         articulo.ZctCatTpArt.Desc_TpArt,
                                         articulo.ZctCatPresentacion.presentacion
                                     }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                     {
                                         Codigo = m.Cod_Art,
                                         Nombre = m.Desc_Art,
                                         Precio = m.Prec_Art,
                                         Costo = m.Cos_Art,
                                         OmitirIVA = m.OmitirIVA,
                                         Tipo = m.Desc_TpArt,
                                         NombreUnidad = m.presentacion
                                     }).ToList();
                    }
                    else
                    {
                        respuesta = (from articulo in contexto.Set<Articulo>()
                                     where (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                     && articulo.Cod_TpArt != "I"
                                     && articulo.desactivar== false
                                     && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                     select new
                                     {
                                         articulo.Cod_Art,
                                         articulo.Desc_Art,
                                         articulo.Prec_Art,
                                         articulo.Cos_Art,
                                         articulo.OmitirIVA,
                                         articulo.ZctCatTpArt.Desc_TpArt,
                                         articulo.ZctCatPresentacion.presentacion
                                     }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                     {
                                         Codigo = m.Cod_Art,
                                         Nombre = m.Desc_Art,
                                         Precio = m.Prec_Art,
                                         Costo = m.Cos_Art,
                                         OmitirIVA = m.OmitirIVA,
                                         Tipo = m.Desc_TpArt,
                                         NombreUnidad = m.presentacion
                                     }).ToList();

                    }
                }
            }

            var filtroU = filtro.Trim().ToUpper();
            if (EsInsumo == true)
            {
                if (_EsgestionArticulos == true)
                {
                    respuesta = (from articulo in contexto.Set<Articulo>()
                                 where (articulo.Cod_Art.ToUpper().Contains(filtroU)
                                 || articulo.Desc_Art.ToUpper().Contains(filtroU))
                                 && (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                 && articulo.Cod_TpArt == "I"
											   
                                 && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                 select new
                                 {
                                     articulo.Cod_Art,
                                     articulo.Desc_Art,
                                     articulo.Prec_Art,
                                     articulo.Cos_Art,
                                     articulo.OmitirIVA,
                                     articulo.ZctCatTpArt.Desc_TpArt,
                                     articulo.ZctCatPresentacion.presentacion
                                 }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                 {
                                     Codigo = m.Cod_Art,
                                     Nombre = m.Desc_Art,
                                     Precio = m.Prec_Art,
                                     Costo = m.Cos_Art,
                                     OmitirIVA = m.OmitirIVA,
                                     Tipo = m.Desc_TpArt,
                                     NombreUnidad = m.presentacion
                                 }).ToList();

                }
                else
                {
                    respuesta = (from articulo in contexto.Set<Articulo>()
                                 where (articulo.Cod_Art.ToUpper().Contains(filtroU)
                                 || articulo.Desc_Art.ToUpper().Contains(filtroU))
                                 && (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                 && articulo.Cod_TpArt == "I"
                                 && articulo.desactivar == false
                                 && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                 select new
                                 {
                                     articulo.Cod_Art,
                                     articulo.Desc_Art,
                                     articulo.Prec_Art,
                                     articulo.Cos_Art,
                                     articulo.OmitirIVA,
                                     articulo.ZctCatTpArt.Desc_TpArt,
                                     articulo.ZctCatPresentacion.presentacion
                                 }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                 {
                                     Codigo = m.Cod_Art,
                                     Nombre = m.Desc_Art,
                                     Precio = m.Prec_Art,
                                     Costo = m.Cos_Art,
                                     OmitirIVA = m.OmitirIVA,
                                     Tipo = m.Desc_TpArt,
                                     NombreUnidad = m.presentacion
                                 }).ToList();
				 
			 

                }					 
            }
            else
            {
                if (_EsgestionArticulos == true)
                {
                    respuesta = (from articulo in contexto.Set<Articulo>()
                                 where (articulo.Cod_Art.ToUpper().Contains(filtroU)
                                 || articulo.Desc_Art.ToUpper().Contains(filtroU))
                                 && (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                 && articulo.Cod_TpArt != "I"
                                 && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                 select new
                                 {
                                     articulo.Cod_Art,
                                     articulo.Desc_Art,
                                     articulo.Prec_Art,
                                     articulo.Cos_Art,
                                     articulo.OmitirIVA,
                                     articulo.ZctCatTpArt.Desc_TpArt,
                                     articulo.ZctCatPresentacion.presentacion
                                 }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                 {
                                     Codigo = m.Cod_Art,
                                     Nombre = m.Desc_Art,
                                     Precio = m.Prec_Art,
                                     Costo = m.Cos_Art,
                                     OmitirIVA = m.OmitirIVA,
                                     Tipo = m.Desc_TpArt,
                                     NombreUnidad = m.presentacion
                                 }).ToList();
                }
                else
                {
                    respuesta = (from articulo in contexto.Set<Articulo>()
                                 where (articulo.Cod_Art.ToUpper().Contains(filtroU)
                                 || articulo.Desc_Art.ToUpper().Contains(filtroU))
                                 && (incluirArticulosPresentacion || articulo.CodigoPadre == null)
                                 && articulo.Cod_TpArt != "I"
                                 && articulo.desactivar == false
                                 && (!soloInventariables || articulo.ZctCatTpArt.Inventariable == true)
                                 select new
                                 {
                                     articulo.Cod_Art,
                                     articulo.Desc_Art,
                                     articulo.Prec_Art,
                                     articulo.Cos_Art,
                                     articulo.OmitirIVA,
                                     articulo.ZctCatTpArt.Desc_TpArt,
                                     articulo.ZctCatPresentacion.presentacion
                                 }).ToList().Select(m => new DtoResultadoBusquedaArticuloSimple
                                 {
                                     Codigo = m.Cod_Art,
                                     Nombre = m.Desc_Art,
                                     Precio = m.Prec_Art,
                                     Costo = m.Cos_Art,
                                     OmitirIVA = m.OmitirIVA,
                                     Tipo = m.Desc_TpArt,
                                     NombreUnidad = m.presentacion
                                 }).ToList();
                }
            }
            return respuesta;
			
        }

        public Articulo TraerOGenerarArticuloPorCodigoOUpc(string Cod_art, bool cargarTemporales)
        {
            var upper = (Cod_art ?? "").Trim().ToUpper();

            var item = (from articulo in contexto.Set<Articulo>()
                        where articulo.Cod_Art.Trim().ToUpper().Equals(upper)
                        || articulo.upc.Trim().ToUpper().Equals(upper)
                        select new
                        {
                            articulo,
                            linea = articulo.ZctCatLinea,
                            NombreTipoTmp = articulo.ZctCatTpArt.Desc_TpArt,
                            NombreMarcaTmp = articulo.ZctCatMar != null ? articulo.ZctCatMar.Desc_Mar : "",
                            NombreDepartamentoTmp = articulo.ZctCatDpto != null ? articulo.ZctCatDpto.Desc_Dpto : "",
                            //NombreLineaTmp = articulo.ZctCatLinea != null ? articulo.ZctCatLinea.Desc_Linea : "",
                            NombreRefaccionTmp = ""
                        }).ToList().FirstOrDefault();

            if (item == null)
                return null;


            item.articulo.NombreTipoTmp = item.NombreTipoTmp;
            item.articulo.NombreMarcaTmp = item.NombreMarcaTmp;
            item.articulo.NombreDepartamentoTmp = item.NombreDepartamentoTmp;
            //item.articulo.NombreLineaTmp = item.NombreLineaTmp;
            item.articulo.NombreRefaccionTmp = item.NombreRefaccionTmp;

            return item.articulo;
        }


        public Articulo ObtenerConCostoAcumulado(string codigoArticulo/*, int idAlmacen*/)
        {
            var r = (from art in contexto.Set<Articulo>()
                     join aa in contexto.Set<Modelo.Almacen.Entidades.ArticuloAlmacen>() on art.Cod_Art equals aa.Cod_Art into items
                     from i in items.DefaultIfEmpty()
                     where art.Cod_Art.Equals(codigoArticulo)
                     //&& (i == null || i.Cod_Alm == idAlmacen)
                     select new
                     {
                         art,
                         Exist = i == null ? 0 : (decimal)i.Exist_Art,
                         Cost = i == null ? 0 : (decimal)i.CostoProm_Art,
                     }).ToList();

            if (r.Count == 0)
                return null;

            var articulo = r.First().art;

            foreach (var item in r)
            {
                articulo.ExistenciasTMP += item.Exist;
                articulo.CostoAcumuladoTMP += item.Cost;
            }
            return articulo;
        }
    }
}
