﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioTiposPagos : Repositorio<ZctCatTipoPago>, IRepositorioTiposPagos
    {
        public RepositorioTiposPagos() : base(new Contextos.AlmacenContext()) { }
    }
}
