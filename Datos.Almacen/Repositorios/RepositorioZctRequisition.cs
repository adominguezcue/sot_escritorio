﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioZctRequisition : Repositorio<ZctRequisition>, IRepositorioZctRequisition
    {
        public RepositorioZctRequisition() : base(new Contextos.AlmacenContext()) { }

        public List<ZctRequisition> ObtenerRequisiciones(string status, int? anio, int? mes)
        {
            Expression<Func<ZctRequisition, bool>> filtro = m => true;

            if (!string.IsNullOrEmpty(status))
                filtro = filtro.Compose(m => m.estatus == status, Expression.AndAlso);

            if (anio.HasValue)
                filtro = filtro.Compose(m => m.anio_fiscal == anio, Expression.AndAlso);

            if (mes.HasValue)
                filtro = filtro.Compose(m => m.mes_fiscal == mes, Expression.AndAlso);

            return contexto.Set<ZctRequisition>().Where(filtro).ToList();
        }


        public int ObtenerUltimoFolio()
        {
            return (from req in contexto.Set<ZctRequisition>()
                    orderby req.folio descending
                    select req.folio).FirstOrDefault();
        }
    }
}
