﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Repositorios;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioDetallesCuentasPorPagar : Repositorio<ZctDetCuentasPagar>, IRepositorioDetallesCuentasPorPagar
    {
        public RepositorioDetallesCuentasPorPagar() : base(new Contextos.AlmacenContext()) { }
    }
}
