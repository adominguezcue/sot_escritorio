﻿using Modelo;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Datos.Nucleo.Repositorios;
using System.Linq.Expressions;
using Transversal.Extensiones;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioLineas: Repositorio<Linea>, IRepositorioLineas
    {
        public RepositorioLineas()
            : base(new Contextos.AlmacenContext())
        {
        }

        public List<Linea> ObtenerLineasConArticulos(bool soloComandables = true, bool incluirSalidaNegada = false, string filtro = null)
        {
            Expression<Func<Linea, bool>> filtoE = m => m.Activa;

            if (soloComandables)
                filtoE = filtoE.Compose(m => m.Comandable, Expression.AndAlso);

            if (string.IsNullOrWhiteSpace(filtro))
                return (from tipoArticulo in contexto.Set<Linea>().Where(filtoE)
                        select new
                        {
                            tipoArticulo,
                            articulos = tipoArticulo.Articulos.Where(m => (incluirSalidaNegada || m.NegarSalida != true) && !m.desactivar)
                        }).ToList().Select(m => m.tipoArticulo).ToList();

            filtro = filtro.Trim().ToUpper();

            return (from tipoArticulo in contexto.Set<Linea>().Where(filtoE)
                    select new
                    {
                        tipoArticulo,
                        articulos = tipoArticulo.Articulos.Where(m => (incluirSalidaNegada || m.NegarSalida != true) && !m.desactivar && m.Desc_Art.ToUpper().Contains(filtro))
                    }).ToList().Select(m => m.tipoArticulo).ToList();
        }


        public List<Linea> ObtenerLineas()
        {
            return (from tipoArticulo in contexto.Set<Linea>()
                    where tipoArticulo.Activa
                    select tipoArticulo).ToList();
        }


        public List<Linea> ObtenerConNombreDepartamento(bool incluirInactivas, int cod_dpto)
        {
            Expression<Func<Linea, bool>> filtro = m => m.Activa;

            if (incluirInactivas)
                filtro = null;

            if (cod_dpto > 0)
                filtro = filtro != null ? filtro.Compose(m => m.Cod_Dpto == cod_dpto, Expression.AndAlso):
                    m => m.Cod_Dpto == cod_dpto;

            if (filtro == null)
            {
                var r = (from linea in contexto.Set<Linea>()
                                 select new
                                 {
                                     linea,
                                     NombreD = linea.ZctCatDpto.Desc_Dpto
                                 }).ToList();

                r.ForEach(m => m.linea.NombreDepartamentoTmp = m.NombreD);

                return r.Select(m => m.linea).ToList();
            }

            var respuesta = (from linea in contexto.Set<Linea>().Where(filtro)
                             select new
                             {
                                 linea,
                                 NombreD = linea.ZctCatDpto.Desc_Dpto
                             }).ToList();

            respuesta.ForEach(m => m.linea.NombreDepartamentoTmp = m.NombreD);

            return respuesta.Select(m => m.linea).ToList();
        }
    }
}
