﻿using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioFichas : Repositorio<Ficha>, IRepositorioFichas
    {
        public RepositorioFichas() : base(new Contextos.AlmacenContext()) { }

        public List<Ficha> ObtenerFichasDia(DateTime dia, string nombreArticulo = "")
        {
            if (string.IsNullOrWhiteSpace(nombreArticulo))
            { 
                var resultado = (from ficha in contexto.Set<Ficha>()
                                 where ficha.Activa
                                 && ficha.FechaIngreso.Day == dia.Day
                                 && ficha.FechaIngreso.Month == dia.Month
                                 && ficha.FechaIngreso.Year == dia.Year
                                 select new
                                 {
                                    ficha,
                                    articulo = ficha.ZctCatArt.Desc_Art,
                                    rfcProveedor = ficha.ZctCatProv.RFC_Prov,
                                    nombreProveedor = ficha.ZctCatProv.Nom_Prov
                                 }).ToList();

                foreach(var item in resultado)
                {
                    item.ficha.RFCProveedorTmp = item.rfcProveedor;
                    item.ficha.NombreProveedorTmp = item.nombreProveedor;
                    item.ficha.ActivoTmp = item.articulo;
                }

                return resultado.Select(m=>m.ficha).ToList();
            }

            var resultado2 = (from ficha in contexto.Set<Ficha>()
                             where ficha.Activa
                             && ficha.FechaIngreso.Day == dia.Day
                             && ficha.FechaIngreso.Month == dia.Month
                             && ficha.FechaIngreso.Year == dia.Year
                             && ficha.ZctCatArt.Desc_Art.ToUpper().Contains(nombreArticulo.Trim().ToUpper())
                             select new
                             {
                                 ficha,
                                 articulo = ficha.ZctCatArt.Desc_Art,
                                 rfcProveedor = ficha.ZctCatProv.RFC_Prov,
                                 nombreProveedor = ficha.ZctCatProv.Nom_Prov
                             }).ToList();

            foreach (var item in resultado2)
            {
                item.ficha.RFCProveedorTmp = item.rfcProveedor;
                item.ficha.NombreProveedorTmp = item.nombreProveedor;
                item.ficha.ActivoTmp = item.articulo;
            }

            return resultado2.Select(m => m.ficha).ToList();
        }


        public Ficha ObtenerPorIdConArchivos(int idFicha)
        {
            return (from ficha in contexto.Set<Ficha>()
                    where ficha.Activa
                    && ficha.Id == idFicha
                    select new
                    {
                        ficha,
                        arch = ficha.ArchivosFicha.Where(m => m.Activa)
                    }).ToList().Select(m => m.ficha).FirstOrDefault();
        }
    }
}
