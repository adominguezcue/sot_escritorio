﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Repositorios;
using System.Data.SqlClient;
using System.Linq.Expressions;
using Transversal.Extensiones;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioEncabezadosCuentasPorPagar : Repositorio<ZctEncCuentasPagar>, IRepositorioEncabezadosCuentasPorPagar
    {
        public RepositorioEncabezadosCuentasPorPagar() : base(new Contextos.AlmacenContext()) { }

        public int ObtenerFolioMaximo()
        {
            return (from encabezado in contexto.Set<ZctEncCuentasPagar>()
                    orderby encabezado.cod_cuenta_pago descending
                    select encabezado.cod_cuenta_pago).FirstOrDefault();
        }


        public void SP_EstadoCxP(int OC, ZctEncCuentasPagar.Estados estado)
        {
            SqlParameter sp_OC = new SqlParameter("@OC", OC);
            sp_OC.DbType = System.Data.DbType.Int32;

            SqlParameter sp_estado = new SqlParameter("@estado", (int)estado);
            sp_estado.DbType = System.Data.DbType.Int32;

            contexto.EjecutarComandoSql("exec [dbo].[SP_EstadoCxP] @OC, @estado", sp_OC, sp_estado);
        }


        public ZctEncCuentasPagar ObtenerCargada(int cod_cuenta_pago)
        {
            return (from encabezado in contexto.Set<ZctEncCuentasPagar>()
                    where encabezado.cod_cuenta_pago == cod_cuenta_pago
                    select new
                    {
                        encabezado,
                        detalles = encabezado.ZctDetCuentasPagar,
                        encabezadoOC = encabezado.ZctEncOC,
                        detallesOC = encabezado.ZctEncOC.ZctDetOC,
                        articulos = encabezado.ZctEncOC.ZctDetOC.Select(m=> m.ZctCatArt),
                        proveedor = encabezado.ZctEncOC.ZctCatProv
                    }).ToList().Select(m => m.encabezado).FirstOrDefault();
        }

        public List<ZctEncCuentasPagar> TraeCuentasPorEstado(string filtroProveedor, ZctEncCuentasPagar.Estados? estado, DateTime? fechacreacionini, DateTime? fechacreacionfin, DateTime? fechavencimientoini, DateTime? fechavencimientofin, bool? esfechacreacion,bool? esfechafin)
        {
            Expression<Func<ZctEncCuentasPagar, bool>> filtro = null;

            if (!string.IsNullOrWhiteSpace(filtroProveedor))
            {
                var filtroProveedorTrim = filtroProveedor.Trim().ToUpper();

                filtro = m => m.ZctEncOC.ZctCatProv.Nom_Prov.Trim().ToUpper().Contains(filtroProveedorTrim);
            }
            if (estado.HasValue)
            {
                var idEstado = (int)estado.Value;

                if (filtro == null)
                    filtro = m => m.idEstado == idEstado;
                else
                    filtro = filtro.Compose(m => m.idEstado == idEstado, Expression.AndAlso);
            }
            if (esfechacreacion == true)
            {
                if (fechacreacionini.HasValue)
                {
                    if (filtro == null)
                        filtro = m => m.fecha_creacion >= fechacreacionini;
                    else
                        filtro = filtro.Compose(m => m.fecha_creacion >= fechacreacionini, Expression.AndAlso);
                }
                if (fechacreacionfin.HasValue)
                {
                    if (filtro == null)
                        filtro = m => m.fecha_creacion <= fechacreacionfin;
                    else
                        filtro = filtro.Compose(m => m.fecha_creacion <= fechacreacionfin, Expression.AndAlso);
                }
            }
            if (esfechafin == true)
            {

                if (fechavencimientoini.HasValue)
                {
                    if (filtro == null)
                        filtro = m => m.fecha_vencido >= fechavencimientoini;
                    else
                        filtro = filtro.Compose(m => m.fecha_vencido >= fechavencimientoini, Expression.AndAlso);
                }
                if (fechavencimientofin.HasValue)
                {
                    if (filtro == null)
                        filtro = m => m.fecha_vencido <= fechavencimientofin;
                    else
                        filtro = filtro.Compose(m => m.fecha_vencido <= fechavencimientofin, Expression.AndAlso);
                }
            }
            //    if (estado.HasValue)
            //{
            //    var idEstado = (int)estado.Value;

            //    var datos = (from encabezado in contexto.Set<ZctEncCuentasPagar>()
            //                 where encabezado.idEstado == idEstado
            //                 select new
            //                 {
            //                     encabezado,
            //                     proveedor = encabezado.ZctEncOC.ZctCatProv.Nom_Prov,
            //                     factura = encabezado.ZctEncOC.Fac_OC
            //                 }).ToList();

            //    foreach (var item in datos)
            //    {
            //        item.encabezado.FacturaTMP = item.factura;
            //        item.encabezado.NombreProveedorTMP = item.proveedor;
            //    }

            //    return datos.Select(m => m.encabezado).ToList();
            //}

            var datos2 = (from encabezado in (filtro == null ? contexto.Set<ZctEncCuentasPagar>() : contexto.Set<ZctEncCuentasPagar>().Where(filtro))
                         select new
                         {
                             encabezado,
                             proveedor = encabezado.ZctEncOC.ZctCatProv.Nom_Prov,
                             factura = encabezado.ZctEncOC.Fac_OC
                         }).ToList();

            foreach (var item in datos2)
            {
                item.encabezado.FacturaTMP = item.factura;
                item.encabezado.NombreProveedorTMP = item.proveedor;
            }

            return datos2.Select(m => m.encabezado).ToList();
        }

        public List<ZctEncCuentasPagar> TraeCuentasProveedores(int cod_prov, ZctEncCuentasPagar.Estados? estado)
        {
            if (estado.HasValue)
            {
                var idEstado = (int)estado.Value;

                var datos = (from encabezado in contexto.Set<ZctEncCuentasPagar>()
                             where encabezado.ZctEncOC.Cod_Prov == cod_prov
                             && encabezado.idEstado == idEstado
                             select new
                             {
                                 encabezado,
                                 factura = encabezado.ZctEncOC.Fac_OC
                             }).ToList();

                datos.ForEach(m => m.encabezado.FacturaTMP = m.factura);

                return datos.Select(m => m.encabezado).ToList();
            }

            var datos2 = (from encabezado in contexto.Set<ZctEncCuentasPagar>()
                          where encabezado.ZctEncOC.Cod_Prov == cod_prov
                          select new
                          {
                              encabezado,
                              factura = encabezado.ZctEncOC.Fac_OC
                          }).ToList();

            datos2.ForEach(m => m.encabezado.FacturaTMP = m.factura);

            return datos2.Select(m => m.encabezado).ToList();
        }
    }
}
