﻿using Datos.Almacen.Auxiliares;
using Datos.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioEncabezadosOrdenesProduccion : Repositorio<ZctEncOP>, IRepositorioEncabezadosOrdenesProduccion
    {
        public RepositorioEncabezadosOrdenesProduccion() : base(new Contextos.AlmacenContext()) { }
    }
}
