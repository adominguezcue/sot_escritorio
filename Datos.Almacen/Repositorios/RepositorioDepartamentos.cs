﻿using Modelo;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Datos.Nucleo.Repositorios;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioDepartamentos: Repositorio<Departamento>, IRepositorioDepartamentos
    {
        public RepositorioDepartamentos()
            : base(new Contextos.AlmacenContext())
        {
        }
    }
}
