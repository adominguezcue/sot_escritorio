﻿using Modelo.Almacen;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Datos.Nucleo.Repositorios;
using System.Data.Entity;
using Modelo.Almacen.Entidades.Dtos;
using System.Data.SqlClient;

namespace Datos.Almacen.Repositorios
{
    public class RepositorioArticulosAlmacen : Repositorio<ArticuloAlmacen>, IRepositorioArticulosAlmacen
    {
        public RepositorioArticulosAlmacen()
            : base(new Contextos.AlmacenContext())
        {
        }
    }
}
