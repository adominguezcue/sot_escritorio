﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctDetOP] DROP CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctEncOP]
GO

ALTER TABLE [dbo].[ZctDetOP] DROP CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctCatArt]
GO

ALTER TABLE [dbo].[ZctDetOP] DROP CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctCatAlm]
GO

/****** Object:  Table [dbo].[ZctDetOP]    Script Date: 26/11/2017 06:25:18 p. m. ******/
DROP TABLE [dbo].[ZctDetOP]
GO

/****** Object:  Table [dbo].[ZctDetOP]    Script Date: 26/11/2017 06:25:18 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZctDetOP](
	[Cod_OP] [int] NOT NULL,
	[CodDet_OP] [int] IDENTITY(1,1) NOT NULL,
	[Cod_Art] [varchar](20) NOT NULL,
	[Cantidad] [numeric](18, 10) NULL,
	[Costo] [numeric](18, 2) NULL,
	[Cod_Alm] [int] NOT NULL,
	[anio_fiscal] [int] NULL,
	[mes_fiscal] [int] NULL,
 CONSTRAINT [PK_ZctDetOP_1] PRIMARY KEY CLUSTERED 
(
	[Cod_Art] ASC,
	[CodDet_OP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ZctDetOP]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctCatAlm] FOREIGN KEY([Cod_Alm])
REFERENCES [dbo].[ZctCatAlm] ([Cod_Alm])
GO

ALTER TABLE [dbo].[ZctDetOP] CHECK CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctCatAlm]
GO

ALTER TABLE [dbo].[ZctDetOP]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctCatArt] FOREIGN KEY([Cod_Art])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctDetOP] CHECK CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctCatArt]
GO

ALTER TABLE [dbo].[ZctDetOP]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctEncOP] FOREIGN KEY([Cod_OP])
REFERENCES [dbo].[ZctEncOP] ([Cod_OP])
GO

ALTER TABLE [dbo].[ZctDetOP] CHECK CONSTRAINT [FK_dbo.ZctDetOP_dbo.ZctEncOP]
GO


