﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctRequisitionDetail] DROP CONSTRAINT [FK_ZctRequisitionDetail_ZctRequisition]
GO

ALTER TABLE [dbo].[ZctRequisitionDetail] DROP CONSTRAINT [FK_ZctRequisitionDetail_ZctCatArt]
GO

/****** Object:  Table [dbo].[ZctRequisitionDetail]    Script Date: 19/12/2017 07:52:02 p. m. ******/
DROP TABLE [dbo].[ZctRequisitionDetail]
GO

/****** Object:  Table [dbo].[ZctRequisitionDetail]    Script Date: 19/12/2017 07:52:02 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctRequisitionDetail](
	[uid] [uniqueidentifier] NOT NULL,
	[folio_requisition] [int] NOT NULL,
	[amount] [int] NULL,
	[price] [numeric](18, 2) NULL,
	[cod_art] [varchar](20) NULL,
	[cod_mot] [int] NULL,
	[update_web] [bit] NULL,
	[omitir_iva] [bit] NOT NULL,
 CONSTRAINT [PK_ZctRequisitionDetail] PRIMARY KEY CLUSTERED 
(
	[uid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZctRequisitionDetail]  WITH CHECK ADD  CONSTRAINT [FK_ZctRequisitionDetail_ZctCatArt] FOREIGN KEY([cod_art])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctRequisitionDetail] CHECK CONSTRAINT [FK_ZctRequisitionDetail_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctRequisitionDetail]  WITH CHECK ADD  CONSTRAINT [FK_ZctRequisitionDetail_ZctRequisition] FOREIGN KEY([folio_requisition])
REFERENCES [dbo].[ZctRequisition] ([folio])
GO

ALTER TABLE [dbo].[ZctRequisitionDetail] CHECK CONSTRAINT [FK_ZctRequisitionDetail_ZctRequisition]
GO


