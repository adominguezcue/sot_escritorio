﻿USE [SOT]
GO

/****** Object:  Table [dbo].[ZctEncOP]    Script Date: 26/11/2017 06:25:04 p. m. ******/
DROP TABLE [dbo].[ZctEncOP]
GO

/****** Object:  Table [dbo].[ZctEncOP]    Script Date: 26/11/2017 06:25:04 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZctEncOP](
	[Cod_OP] [int] NOT NULL,
	[Fch_OP] [smalldatetime] NOT NULL,
	[Type_OP] [varchar](20) NOT NULL,
	[Notes] [varchar](150) NULL,
 CONSTRAINT [PK_ZctEncOP_1] PRIMARY KEY CLUSTERED 
(
	[Cod_OP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


