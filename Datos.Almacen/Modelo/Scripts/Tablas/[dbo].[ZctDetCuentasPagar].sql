﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar] DROP CONSTRAINT [FK_tipo_pago]
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar] DROP CONSTRAINT [FK_ENC]
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar] DROP CONSTRAINT [FK_banco_pagos]
GO

/****** Object:  Table [dbo].[ZctDetCuentasPagar]    Script Date: 04/12/2017 09:37:14 p. m. ******/
DROP TABLE [dbo].[ZctDetCuentasPagar]
GO

/****** Object:  Table [dbo].[ZctDetCuentasPagar]    Script Date: 04/12/2017 09:37:14 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZctDetCuentasPagar](
	[cod_cuenta_pago] [int] NOT NULL,
	[consecutivo] [int] IDENTITY(1,1) NOT NULL,
	[referencia] [varchar](100) NOT NULL,
	[codtipo_pago] [int] NULL,
	[monto] [money] NOT NULL,
	[cambio] [money] NOT NULL,
	[idEstado] [int] NOT NULL,
	[fecha_pago] [datetime] NULL,
	[fecha_vencido] [datetime] NOT NULL,
	[cod_banco] [int] NULL,
	[factura] [varchar](50) NULL,
	[fecha_factura] [datetime] NULL,
 CONSTRAINT [PK_ZctDetCuentasPagar_1] PRIMARY KEY CLUSTERED 
(
	[cod_cuenta_pago] ASC,
	[consecutivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar]  WITH CHECK ADD  CONSTRAINT [FK_banco_pagos] FOREIGN KEY([cod_banco])
REFERENCES [dbo].[ZctBancos] ([cod_banco])
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar] CHECK CONSTRAINT [FK_banco_pagos]
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar]  WITH CHECK ADD  CONSTRAINT [FK_ENC] FOREIGN KEY([cod_cuenta_pago])
REFERENCES [dbo].[ZctEncCuentasPagar] ([cod_cuenta_pago])
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar] CHECK CONSTRAINT [FK_ENC]
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar]  WITH CHECK ADD  CONSTRAINT [FK_tipo_pago] FOREIGN KEY([codtipo_pago])
REFERENCES [dbo].[ZctCatTipoPago] ([codtipo_pago])
GO

ALTER TABLE [dbo].[ZctDetCuentasPagar] CHECK CONSTRAINT [FK_tipo_pago]
GO


