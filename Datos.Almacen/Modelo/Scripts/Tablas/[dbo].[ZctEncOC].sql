﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctEncOC] DROP CONSTRAINT [FK_ZctEncOC_ZctCatProv]
GO

ALTER TABLE [dbo].[ZctEncOC] DROP CONSTRAINT [DF__ZctEncOC__status__3434A84B]
GO

/****** Object:  Table [dbo].[ZctEncOC]    Script Date: 04/12/2017 09:37:01 p. m. ******/
DROP TABLE [dbo].[ZctEncOC]
GO

/****** Object:  Table [dbo].[ZctEncOC]    Script Date: 04/12/2017 09:37:01 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZctEncOC](
	[Cod_OC] [int] NOT NULL,
	[Cod_Prov] [int] NULL,
	[Fch_OC] [smalldatetime] NULL,
	[FchAplMov] [smalldatetime] NULL,
	[Fac_OC] [varchar](20) NULL,
	[FchFac_OC] [smalldatetime] NULL,
	[status] [varchar](20) NULL,
 CONSTRAINT [PK_ZctEncOC] PRIMARY KEY CLUSTERED 
(
	[Cod_OC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ZctEncOC] ADD  DEFAULT ('APLICADO') FOR [status]
GO

ALTER TABLE [dbo].[ZctEncOC]  WITH CHECK ADD  CONSTRAINT [FK_ZctEncOC_ZctCatProv] FOREIGN KEY([Cod_Prov])
REFERENCES [dbo].[ZctCatProv] ([Cod_Prov])
GO

ALTER TABLE [dbo].[ZctEncOC] CHECK CONSTRAINT [FK_ZctEncOC_ZctCatProv]
GO


