﻿USE [SOT]
GO

ALTER TABLE [dbo].[Fichas] DROP CONSTRAINT [FK_Fichas_ZctCatProv]
GO

ALTER TABLE [dbo].[Fichas] DROP CONSTRAINT [FK_Fichas_ZctCatArt]
GO

/****** Object:  Table [dbo].[Fichas]    Script Date: 15/11/2017 07:02:13 p. m. ******/
DROP TABLE [dbo].[Fichas]
GO

/****** Object:  Table [dbo].[Fichas]    Script Date: 15/11/2017 07:02:13 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Fichas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Cod_Art] [varchar](20) NOT NULL,
	[Cod_Prov] [int] NOT NULL,
	[Observaciones] [varchar](max) NOT NULL,
	[FechaIngreso] [datetime] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
	[FechaEliminacion] [datetime] NULL,
	[IdUsuarioModifico] [int] NOT NULL,
	[IdUsuarioCreo] [int] NOT NULL,
	[IdUsuarioElimino] [int] NULL,
	[Activa] [bit] NOT NULL,
	[Poliza] [varchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Fichas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Fichas]  WITH CHECK ADD  CONSTRAINT [FK_Fichas_ZctCatArt] FOREIGN KEY([Cod_Art])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[Fichas] CHECK CONSTRAINT [FK_Fichas_ZctCatArt]
GO

ALTER TABLE [dbo].[Fichas]  WITH CHECK ADD  CONSTRAINT [FK_Fichas_ZctCatProv] FOREIGN KEY([Cod_Prov])
REFERENCES [dbo].[ZctCatProv] ([Cod_Prov])
GO

ALTER TABLE [dbo].[Fichas] CHECK CONSTRAINT [FK_Fichas_ZctCatProv]
GO


