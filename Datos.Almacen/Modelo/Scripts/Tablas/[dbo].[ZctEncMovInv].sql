﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctEncMovInv] DROP CONSTRAINT [FK_ZctEncMovInv_ZctCatAlm]
GO

/****** Object:  Table [dbo].[ZctEncMovInv]    Script Date: 19/12/2017 08:31:12 a. m. ******/
DROP TABLE [dbo].[ZctEncMovInv]
GO

/****** Object:  Table [dbo].[ZctEncMovInv]    Script Date: 19/12/2017 08:31:12 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctEncMovInv](
	[CodMov_Inv] [int] IDENTITY(1,1) NOT NULL,
	[FchMov_Inv] [smalldatetime] NULL,
	[TpMov_Inv] [char](2) NULL,
	[Cod_Art] [varchar](20) NULL,
	[CtdMov_Inv] [numeric](18, 10) NULL,
	[CosMov_Inv] [money] NULL,
	[FolOS_Inv] [varchar](10) NULL,
	[TpOs_Inv] [char](2) NULL,
	[ExistAct_Inv] [numeric](18, 10) NULL,
	[CostoAct_Inv] [money] NULL,
	[Cod_Alm] [int] NULL,
 CONSTRAINT [PK_ZctEncMovInv] PRIMARY KEY CLUSTERED 
(
	[CodMov_Inv] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZctEncMovInv]  WITH CHECK ADD  CONSTRAINT [FK_ZctEncMovInv_ZctCatAlm] FOREIGN KEY([Cod_Alm])
REFERENCES [dbo].[ZctCatAlm] ([Cod_Alm])
GO

ALTER TABLE [dbo].[ZctEncMovInv] CHECK CONSTRAINT [FK_ZctEncMovInv_ZctCatAlm]
GO


