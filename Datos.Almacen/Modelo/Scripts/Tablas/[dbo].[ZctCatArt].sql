﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK_ZctCatArt_ZctCatTpArt]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK_ZctCatArt_ZctCatPresentacion]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK_ZctCatArt_ZctCatMoneda]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK_ZctCatArt_ZctCatLinea]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK_ZctCatArt_ZctCatDpto]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK_ZctCatArt_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK_Articulos_Marcas]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [FK__ZctCatArt__Cod_T__1C722D53]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [DF__ZctCatArt__secci__73E5190C]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [DF__ZctCatArt__repis__72F0F4D3]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [DF__ZctCatArt__pasil__71FCD09A]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [DF__ZctCatArt__cod_a__4F52B2DB]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [DF__ZctCatArt__tiemp__4E5E8EA2]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [DF__ZctCatArt__uuid__7795AE5F]
GO

ALTER TABLE [dbo].[ZctCatArt] DROP CONSTRAINT [DF_ZctCatArt_Exist_Art]
GO

/****** Object:  Table [dbo].[ZctCatArt]    Script Date: 26/11/2017 02:13:30 p. m. ******/
DROP TABLE [dbo].[ZctCatArt]
GO

/****** Object:  Table [dbo].[ZctCatArt]    Script Date: 26/11/2017 02:13:30 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZctCatArt](
	[Cod_Art] [varchar](20) NOT NULL,
	[CodigoSAT] [varchar](max) NULL,
	[Desc_Art] [varchar](100) NULL,
	[Prec_Art] [numeric](19, 3) NOT NULL,
	[Cos_Art] [numeric](19, 3) NULL,
	[Cod_Mar] [int] NULL,
	[Exist_Art] [int] NULL,
	[Cod_Linea] [int] NULL,
	[Cod_Dpto] [int] NULL,
	[Cod_TpArt] [varchar](10) NOT NULL,
	[Cod_TpRef] [int] NULL,
	[desactivar] [bit] NOT NULL,
	[update_web] [bit] NULL,
	[uuid] [uniqueidentifier] NULL,
	[tiempo_vida] [int] NULL,
	[cod_alm] [int] NULL,
	[ubicacion] [varchar](50) NULL,
	[upc] [varchar](50) NULL,
	[noArticulo] [int] NULL,
	[pais] [varchar](250) NULL,
	[id_presentacion] [int] NOT NULL,
	[id_cat_moneda] [int] NULL,
	[porcentaje] [numeric](18, 2) NULL,
	[caracteristicas] [text] NULL,
	[pasillo] [varchar](50) NULL,
	[repisa] [varchar](50) NULL,
	[seccion] [varchar](50) NULL,
	[descuento] [numeric](18, 2) NULL,
	[refill] [int] NULL,
 CONSTRAINT [PK_ZctCatArt_1] PRIMARY KEY CLUSTERED 
(
	[Cod_Art] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ZctCatArt] ADD  CONSTRAINT [DF_ZctCatArt_Exist_Art]  DEFAULT ((0)) FOR [Exist_Art]
GO

ALTER TABLE [dbo].[ZctCatArt] ADD  CONSTRAINT [DF__ZctCatArt__uuid__7795AE5F]  DEFAULT (newid()) FOR [uuid]
GO

ALTER TABLE [dbo].[ZctCatArt] ADD  CONSTRAINT [DF__ZctCatArt__tiemp__4E5E8EA2]  DEFAULT ((1)) FOR [tiempo_vida]
GO

ALTER TABLE [dbo].[ZctCatArt] ADD  CONSTRAINT [DF__ZctCatArt__cod_a__4F52B2DB]  DEFAULT ((1)) FOR [cod_alm]
GO

ALTER TABLE [dbo].[ZctCatArt] ADD  CONSTRAINT [DF__ZctCatArt__pasil__71FCD09A]  DEFAULT ('') FOR [pasillo]
GO

ALTER TABLE [dbo].[ZctCatArt] ADD  CONSTRAINT [DF__ZctCatArt__repis__72F0F4D3]  DEFAULT ('') FOR [repisa]
GO

ALTER TABLE [dbo].[ZctCatArt] ADD  CONSTRAINT [DF__ZctCatArt__secci__73E5190C]  DEFAULT ('') FOR [seccion]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH CHECK ADD  CONSTRAINT [FK__ZctCatArt__Cod_T__1C722D53] FOREIGN KEY([Cod_TpRef])
REFERENCES [dbo].[ZctCatTpRef] ([Cod_TpRef])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK__ZctCatArt__Cod_T__1C722D53]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH CHECK ADD  CONSTRAINT [FK_Articulos_Marcas] FOREIGN KEY([Cod_Mar])
REFERENCES [dbo].[ZctCatMar] ([Cod_Mar])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK_Articulos_Marcas]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH CHECK ADD  CONSTRAINT [FK_ZctCatArt_ZctCatArt] FOREIGN KEY([Cod_Mar])
REFERENCES [dbo].[ZctCatMar] ([Cod_Mar])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK_ZctCatArt_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH CHECK ADD  CONSTRAINT [FK_ZctCatArt_ZctCatDpto] FOREIGN KEY([Cod_Dpto])
REFERENCES [dbo].[ZctCatDpto] ([Cod_Dpto])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK_ZctCatArt_ZctCatDpto]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH CHECK ADD  CONSTRAINT [FK_ZctCatArt_ZctCatLinea] FOREIGN KEY([Cod_Linea])
REFERENCES [dbo].[ZctCatLinea] ([Cod_Linea])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK_ZctCatArt_ZctCatLinea]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH CHECK ADD  CONSTRAINT [FK_ZctCatArt_ZctCatMoneda] FOREIGN KEY([id_cat_moneda])
REFERENCES [dbo].[ZctCatMoneda] ([id])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK_ZctCatArt_ZctCatMoneda]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH CHECK ADD  CONSTRAINT [FK_ZctCatArt_ZctCatPresentacion] FOREIGN KEY([id_presentacion])
REFERENCES [dbo].[ZctCatPresentacion] ([id])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK_ZctCatArt_ZctCatPresentacion]
GO

ALTER TABLE [dbo].[ZctCatArt]  WITH NOCHECK ADD  CONSTRAINT [FK_ZctCatArt_ZctCatTpArt] FOREIGN KEY([Cod_TpArt])
REFERENCES [dbo].[ZctCatTpArt] ([Cod_TpArt])
GO

ALTER TABLE [dbo].[ZctCatArt] CHECK CONSTRAINT [FK_ZctCatArt_ZctCatTpArt]
GO


