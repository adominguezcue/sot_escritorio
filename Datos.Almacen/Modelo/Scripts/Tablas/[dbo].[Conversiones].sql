﻿USE [SOT]
GO

ALTER TABLE [dbo].[Conversiones] DROP CONSTRAINT [FK_SegundaPresentacion]
GO

ALTER TABLE [dbo].[Conversiones] DROP CONSTRAINT [FK_PrimeraPresentacion]
GO

/****** Object:  Table [dbo].[Conversiones]    Script Date: 25/11/2017 06:58:43 p. m. ******/
DROP TABLE [dbo].[Conversiones]
GO

/****** Object:  Table [dbo].[Conversiones]    Script Date: 25/11/2017 06:58:43 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Conversiones](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdPrimeraPresentacion] [int] NOT NULL,
	[IdSegundaPresentacion] [int] NOT NULL,
	[Equivalencia] [decimal](19, 3) NOT NULL,
	[Activa] [bit] NOT NULL,
 CONSTRAINT [PK_Conversiones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Conversiones]  WITH CHECK ADD  CONSTRAINT [FK_PrimeraPresentacion] FOREIGN KEY([IdPrimeraPresentacion])
REFERENCES [dbo].[ZctCatPresentacion] ([id])
GO

ALTER TABLE [dbo].[Conversiones] CHECK CONSTRAINT [FK_PrimeraPresentacion]
GO

ALTER TABLE [dbo].[Conversiones]  WITH CHECK ADD  CONSTRAINT [FK_SegundaPresentacion] FOREIGN KEY([IdSegundaPresentacion])
REFERENCES [dbo].[ZctCatPresentacion] ([id])
GO

ALTER TABLE [dbo].[Conversiones] CHECK CONSTRAINT [FK_SegundaPresentacion]
GO


