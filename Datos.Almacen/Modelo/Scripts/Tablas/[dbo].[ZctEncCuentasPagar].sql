﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctEncCuentasPagar] DROP CONSTRAINT [FK_ZctEncCuentasPagar_ZctEncOC]
GO

ALTER TABLE [dbo].[ZctEncCuentasPagar] DROP CONSTRAINT [DF__ZctEncCuen__uuid__1F8E9120]
GO

/****** Object:  Table [dbo].[ZctEncCuentasPagar]    Script Date: 04/12/2017 09:37:08 p. m. ******/
DROP TABLE [dbo].[ZctEncCuentasPagar]
GO

/****** Object:  Table [dbo].[ZctEncCuentasPagar]    Script Date: 04/12/2017 09:37:08 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctEncCuentasPagar](
	[cod_cuenta_pago] [int] IDENTITY(1,1) NOT NULL,
	[Cod_OC] [int] NOT NULL,
	[monto] [money] NOT NULL,
	[iva] [money] NOT NULL,
	[subtotal] [money] NOT NULL,
	[total] [money] NOT NULL,
	[fecha_creacion] [datetime] NOT NULL,
	[fecha_vencido] [datetime] NOT NULL,
	[idEstado] [int] NOT NULL,
	[update_web] [bit] NULL,
	[uuid] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ZctEncCuentasPagar_1] PRIMARY KEY CLUSTERED 
(
	[cod_cuenta_pago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ZctEncCuentasPagar] ADD  CONSTRAINT [DF__ZctEncCuen__uuid__1F8E9120]  DEFAULT (newid()) FOR [uuid]
GO

ALTER TABLE [dbo].[ZctEncCuentasPagar]  WITH CHECK ADD  CONSTRAINT [FK_ZctEncCuentasPagar_ZctEncOC] FOREIGN KEY([Cod_OC])
REFERENCES [dbo].[ZctEncOC] ([Cod_OC])
GO

ALTER TABLE [dbo].[ZctEncCuentasPagar] CHECK CONSTRAINT [FK_ZctEncCuentasPagar_ZctEncOC]
GO


