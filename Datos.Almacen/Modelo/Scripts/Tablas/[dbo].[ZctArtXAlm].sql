﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctArtXAlm] DROP CONSTRAINT [FK_ZctArtXAlm_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctArtXAlm] DROP CONSTRAINT [FK_ZctArtXAlm_ZctCatAlm]
GO

ALTER TABLE [dbo].[ZctArtXAlm] DROP CONSTRAINT [DF__ZctArtXAlm__uuid__5B0920B3]
GO

/****** Object:  Table [dbo].[ZctArtXAlm]    Script Date: 19/12/2017 08:29:53 a. m. ******/
DROP TABLE [dbo].[ZctArtXAlm]
GO

/****** Object:  Table [dbo].[ZctArtXAlm]    Script Date: 19/12/2017 08:29:53 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctArtXAlm](
	[Cod_Art] [varchar](20) NOT NULL,
	[Cod_Alm] [int] NOT NULL,
	[Exist_Art] [numeric](18, 10) NULL,
	[CostoProm_Art] [money] NULL,
	[uuid] [uniqueidentifier] NULL,
	[update_web] [bit] NULL,
 CONSTRAINT [PK_ZctArtXAlm] PRIMARY KEY CLUSTERED 
(
	[Cod_Art] ASC,
	[Cod_Alm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZctArtXAlm] ADD  CONSTRAINT [DF__ZctArtXAlm__uuid__5B0920B3]  DEFAULT (newid()) FOR [uuid]
GO

ALTER TABLE [dbo].[ZctArtXAlm]  WITH CHECK ADD  CONSTRAINT [FK_ZctArtXAlm_ZctCatAlm] FOREIGN KEY([Cod_Alm])
REFERENCES [dbo].[ZctCatAlm] ([Cod_Alm])
GO

ALTER TABLE [dbo].[ZctArtXAlm] CHECK CONSTRAINT [FK_ZctArtXAlm_ZctCatAlm]
GO

ALTER TABLE [dbo].[ZctArtXAlm]  WITH CHECK ADD  CONSTRAINT [FK_ZctArtXAlm_ZctCatArt] FOREIGN KEY([Cod_Art])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctArtXAlm] CHECK CONSTRAINT [FK_ZctArtXAlm_ZctCatArt]
GO


