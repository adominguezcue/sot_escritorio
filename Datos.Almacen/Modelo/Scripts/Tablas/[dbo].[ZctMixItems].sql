﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctMixItems] DROP CONSTRAINT [FK_dbo.ZctMixItems_dbo.ZctCatArt]
GO

/****** Object:  Table [dbo].[ZctMixItems]    Script Date: 26/11/2017 06:24:40 p. m. ******/
DROP TABLE [dbo].[ZctMixItems]
GO

/****** Object:  Table [dbo].[ZctMixItems]    Script Date: 26/11/2017 06:24:40 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZctMixItems](
	[Cod_Art] [varchar](20) NOT NULL,
	[Cod_Art_Mix] [varchar](20) NOT NULL,
	[uuid] [uniqueidentifier] NULL DEFAULT (newid()),
	[proporcion] [varchar](50) NOT NULL,
	[cantidad] [numeric](18, 10) NULL,
	[base] [numeric](18, 2) NULL,
 CONSTRAINT [PK_ZctMixItems_1] PRIMARY KEY CLUSTERED 
(
	[Cod_Art] ASC,
	[Cod_Art_Mix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ZctMixItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ZctMixItems_dbo.ZctCatArt] FOREIGN KEY([Cod_Art_Mix])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctMixItems] CHECK CONSTRAINT [FK_dbo.ZctMixItems_dbo.ZctCatArt]
GO


