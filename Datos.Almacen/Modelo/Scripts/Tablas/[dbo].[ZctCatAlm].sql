﻿USE [SOT]
GO

/****** Object:  Table [dbo].[ZctCatAlm]    Script Date: 01/12/2017 01:57:03 p. m. ******/
DROP TABLE [dbo].[ZctCatAlm]
GO

/****** Object:  Table [dbo].[ZctCatAlm]    Script Date: 01/12/2017 01:57:03 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ZctCatAlm](
	[Cod_Alm] [int] IDENTITY(1,1) NOT NULL,
	[Desc_CatAlm] [varchar](100) NULL,
	[uuid] [uniqueidentifier] NULL DEFAULT (newid()),
	[update_web] [bit] NULL,
	[entrada] [bit] NULL,
	[salida] [bit] NULL,
	[IdCentroCostos] [int] NULL,
 CONSTRAINT [PK_ZctCatAlm] PRIMARY KEY CLUSTERED 
(
	[Cod_Alm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


