﻿USE [SOT]
GO

ALTER TABLE [dbo].[ArchivosFicha] DROP CONSTRAINT [FK_ArchivosFicha_Fichas]
GO

/****** Object:  Table [dbo].[ArchivosFicha]    Script Date: 16/11/2017 01:06:55 p. m. ******/
DROP TABLE [dbo].[ArchivosFicha]
GO

/****** Object:  Table [dbo].[ArchivosFicha]    Script Date: 16/11/2017 01:06:55 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ArchivosFicha](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdFicha] [int] NOT NULL,
	[Valor] [varbinary](max) NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[FechaModificacion] [datetime] NOT NULL,
	[FechaEliminacion] [datetime] NULL,
	[IdUsuarioModifico] [int] NOT NULL,
	[IdUsuarioCreo] [int] NOT NULL,
	[IdUsuarioElimino] [int] NULL,
	[Activa] [bit] NOT NULL,
	[Nombre] [varchar](max) NOT NULL,
 CONSTRAINT [PK_ArchivoFicha] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ArchivosFicha]  WITH CHECK ADD  CONSTRAINT [FK_ArchivosFicha_Fichas] FOREIGN KEY([IdFicha])
REFERENCES [dbo].[Fichas] ([Id])
GO

ALTER TABLE [dbo].[ArchivosFicha] CHECK CONSTRAINT [FK_ArchivosFicha_Fichas]
GO


