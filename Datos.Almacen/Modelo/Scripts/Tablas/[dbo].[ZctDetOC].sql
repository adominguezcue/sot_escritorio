﻿USE [SOT]
GO

ALTER TABLE [dbo].[ZctDetOC] DROP CONSTRAINT [FK_ZctDetOC_ZctEncOC]
GO

ALTER TABLE [dbo].[ZctDetOC] DROP CONSTRAINT [FK_ZctDetOC_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctDetOC] DROP CONSTRAINT [FK_ZctDetOC_ZctCatAlm]
GO

ALTER TABLE [dbo].[ZctDetOC] DROP CONSTRAINT [DF__ZctDetOC__uuid__1DA648AE]
GO

/****** Object:  Table [dbo].[ZctDetOC]    Script Date: 07/12/2017 09:28:44 a. m. ******/
DROP TABLE [dbo].[ZctDetOC]
GO

/****** Object:  Table [dbo].[ZctDetOC]    Script Date: 07/12/2017 09:28:44 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZctDetOC](
	[Cod_OC] [int] NOT NULL,
	[CodDet_OC] [int] IDENTITY(1,1) NOT NULL,
	[Cod_Art] [varchar](20) NULL,
	[Ctd_Art] [int] NULL,
	[Cos_Art] [numeric](19, 3) NULL,
	[CtdStdDet_OC] [int] NULL,
	[Cat_Alm] [int] NULL,
	[anio_fiscal] [int] NULL,
	[mes_fiscal] [int] NULL,
	[update_web] [bit] NULL,
	[uuid] [uniqueidentifier] NULL,
	[OmitirIVA] [bit] NOT NULL,
 CONSTRAINT [PK_ZctDetOC] PRIMARY KEY CLUSTERED 
(
	[Cod_OC] ASC,
	[CodDet_OC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZctDetOC] ADD  CONSTRAINT [DF__ZctDetOC__uuid__1DA648AE]  DEFAULT (newid()) FOR [uuid]
GO

ALTER TABLE [dbo].[ZctDetOC]  WITH CHECK ADD  CONSTRAINT [FK_ZctDetOC_ZctCatAlm] FOREIGN KEY([Cat_Alm])
REFERENCES [dbo].[ZctCatAlm] ([Cod_Alm])
GO

ALTER TABLE [dbo].[ZctDetOC] CHECK CONSTRAINT [FK_ZctDetOC_ZctCatAlm]
GO

ALTER TABLE [dbo].[ZctDetOC]  WITH CHECK ADD  CONSTRAINT [FK_ZctDetOC_ZctCatArt] FOREIGN KEY([Cod_Art])
REFERENCES [dbo].[ZctCatArt] ([Cod_Art])
GO

ALTER TABLE [dbo].[ZctDetOC] CHECK CONSTRAINT [FK_ZctDetOC_ZctCatArt]
GO

ALTER TABLE [dbo].[ZctDetOC]  WITH CHECK ADD  CONSTRAINT [FK_ZctDetOC_ZctEncOC] FOREIGN KEY([Cod_OC])
REFERENCES [dbo].[ZctEncOC] ([Cod_OC])
GO

ALTER TABLE [dbo].[ZctDetOC] CHECK CONSTRAINT [FK_ZctDetOC_ZctEncOC]
GO


