﻿USE [SOT]
GO

/****** Object:  Trigger [InsArt]    Script Date: 19/12/2017 08:25:15 a. m. ******/
DROP TRIGGER [dbo].[InsArt]
GO

/****** Object:  Trigger [dbo].[InsArt]    Script Date: 19/12/2017 08:25:15 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[InsArt]
   ON  [dbo].[ZctEncMovInv]
   AFTER INSERT,UPDATE
AS 
BEGIN
	declare 
	@Cod_Art varchar(20),
	@CosMov_Inv money,
	@CtdMov_Inv numeric(18,10), 
	@Cod_Alm int 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select @Cod_Art  = Cod_Art, @Cod_Alm = Cod_Alm from INSERTED
    -- Insert statements for trigger here
	SELECT     @CtdMov_Inv = SUM(CtdMov_Inv), @CosMov_Inv = SUM(CosMov_Inv) 
	FROM         ZctEncMovInv
	where Cod_Art = @Cod_Art AND Cod_Alm= @Cod_Alm
	GROUP BY Cod_Art, Cod_Alm

	/*UPDATE [ZctSOT].[dbo].[ZctCatArt]
   SET [Exist_Art] = @CtdMov_Inv
	WHERE Cod_Art = @Cod_Art and Cod_Alm = @Cod_Alm*/

	if exists(Select Cod_Art From ZctArtXAlm where Cod_Art = @Cod_Art and Cod_Alm = @Cod_Alm)
		update ZctArtXAlm SET  Exist_Art = @CtdMov_Inv, CostoProm_Art = @CosMov_Inv,   update_web = 0
		WHERE Cod_Art = @Cod_Art and Cod_Alm = @Cod_Alm
	else
		insert into ZctArtXAlm( Cod_Art, Cod_Alm, Exist_Art, CostoProm_Art)
		values(@Cod_Art, @Cod_Alm, @CtdMov_Inv, @CosMov_Inv)
	UPDATE ZctCatArt  SET   update_web = 0 WHERE Cod_Art  = @Cod_Art 
END

GO

ALTER TABLE [dbo].[ZctEncMovInv] ENABLE TRIGGER [InsArt]
GO


