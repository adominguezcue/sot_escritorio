﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_Cortecaja]    Script Date: 10/11/2017 09:21:21 a. m. ******/
DROP PROCEDURE [dbo].[SP_Cortecaja]
GO

/****** Object:  StoredProcedure [dbo].[SP_Cortecaja]    Script Date: 10/11/2017 09:21:21 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_Cortecaja] 
@cod_usu int,
@cod_folio varchar(10)
as
begin 
if exists (select 1 from ZctPagoCaja p where p.corte='false' and p.cod_usu =@cod_usu and p.cod_folio =@cod_folio   or exists(select 1 from ZctRetirosCajas where corte='false' and cod_folio=@cod_folio and ZctRetirosCajas.cod_folio =@cod_folio))
begin 
SELECT        p.cod_folio, p.CodEnc_Caja, desc_pago AS metodo, 'PAGO' AS tipo, p.monto - p.cambio  as monto, fecha_pago AS fecha, cat.minimo_caja, a.monto_inicial, a.cod_usu, a.abierta,s.Alias as Nom_Usu 
FROM            ZctPagoCaja p JOIN
                         ZctCatTipoPago t ON p.codtipo_pago = t .codtipo_pago JOIN
                         ZctEncCaja e ON e.cod_folio = p.cod_folio AND p.CodEnc_Caja = p.CodEnc_Caja CROSS JOIN
                         ZctCatPar cat JOIN
                         ZctAperturasCajas a ON a.cod_folio = p.cod_folio
cross join Seguridad.Usuarios s 
WHERE        p.corte = 'false' AND a.abierta = 'true' and p.cod_usu =@cod_usu and p.cod_folio =@cod_folio  and s.Id=@cod_usu and s.Id =@cod_usu and a.cod_usu =@cod_usu  
UNION
SELECT        @cod_folio, '', 'RETIRO EN EFECTIVO' AS metodo, 'RETIRO' AS tipo, r.monto, r.fecha_retiro AS fecha, minimo_caja, 0, @cod_usu, 'true',s.Alias as Nom_Usu 
FROM            ZctRetirosCajas r CROSS JOIN
                ZctCatPar  cross join Seguridad.Usuarios s 

WHERE        r.corte = 'false' and s.Id=@cod_usu 
end 
else
begin
SELECT        @cod_folio as cod_folio , '' as CodEnc_Caja, 'SIN MOVIMIENTOS' AS metodo, 'SIN MOVIMIENTOS' AS tipo, 0 as monto, GETDATE() AS fecha, minimo_caja, a.monto_inicial, a.cod_usu,0 as abierta,s.Alias as Nom_Usu 
FROM            ZctCatPar, ZctAperturasCajas a
cross join Seguridad.Usuarios s
where a.abierta='true' and s.Id =@cod_usu and a.cod_usu =@cod_usu 
end 
end 

GO


