﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctCatArt_Busqueda_Mix]    Script Date: 19/12/2017 01:06:44 p. m. ******/
DROP PROCEDURE [dbo].[SP_ZctCatArt_Busqueda_Mix]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctCatArt_Busqueda_Mix]    Script Date: 19/12/2017 01:06:44 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Alí Escamilla
-- Create date: 28-09-2017
-- Description:	Catálogo de Marcas
-- =============================================
CREATE PROCEDURE [dbo].[SP_ZctCatArt_Busqueda_Mix]
	@Codigo varchar(20)
AS
	BEGIN
		IF (@Codigo  = '0')
			begin
				SELECT zca.Cod_Art CodArt, zca.Desc_Art DescArt, zca.Exist_Art AS ArtExist, sum((isnull(zaa.CostoProm_Art, zca.Cos_Art)/isnull(zaa.Exist_Art, zca.Cos_Art))*(zmi.cantidad/zmi.base)) Cost, OmitirIVA  
				FROM ZctCatArt zca
				inner join ZctMixItems zmi on zmi.Cod_Art=zca.Cod_Art
				left join ZctArtXAlm zaa on zmi.Cod_Art_Mix=zaa.Cod_Art
				group by zca.Cod_Art, zca.Desc_Art, zca.Exist_Art, OmitirIVA
			end
		else
			begin
				SELECT zca.Cod_Art CodArt, zca.Desc_Art DescArt, zca.Exist_Art AS ArtExist, sum((isnull(zaa.CostoProm_Art, zca.Cos_Art)/isnull(zaa.Exist_Art, zca.Cos_Art))*(zmi.cantidad/zmi.base)) Cost, OmitirIVA
				FROM ZctCatArt zca
				inner join ZctMixItems zmi on zmi.Cod_Art=zca.Cod_Art
				left join ZctArtXAlm zaa on zmi.Cod_Art_Mix=zaa.Cod_Art
				WHERE zca.Cod_Art = @Codigo  
				group by zca.Cod_Art, zca.Desc_Art, zca.Exist_Art, OmitirIVA
			end
	END

GO


