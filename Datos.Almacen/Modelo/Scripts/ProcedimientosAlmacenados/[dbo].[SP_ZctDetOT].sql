﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctDetOT]    Script Date: 04/12/2017 01:10:46 p. m. ******/
DROP PROCEDURE [dbo].[SP_ZctDetOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctDetOT]    Script Date: 04/12/2017 01:10:46 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_ZctDetOT]
		@TpConsulta Int,	
		@Cod_EncOT int,
		@Cod_DetOT int,
		@Cod_Art varchar(20),
		@Ctd_Art int,
		@CosArt_DetOT numeric(18, 2),
		@PreArt_DetOT numeric(18, 2),
		@CtdStd_DetOT int,
		@Fch_DetOT smalldatetime,
		@FchAplMov smalldatetime,
		@Cat_Alm int,
		@Cod_Mot int = 0,
		@Cod_TpArt varchar(10) = 'ART',
		@tiempo_vida int=0,
		@cod_usu int = 0
AS
BEGIN
	DECLARE @CtdStdOld INT
	DECLARE @CatAlmOLD INT
	DECLARE @CosArtOLD numeric(18, 2)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	if (@TpConsulta = '1')
		BEGIN
			IF ( @Cod_EncOT  = 0)
					SELECT Cod_EncOT , Cod_DetOT, Cod_Art, Ctd_Art, CosArt_DetOT, PreArt_DetOT, CtdStd_DetOT, Fch_DetOT , Cat_Alm, Cod_Mot, tiempo_vida , cod_usu FROM [ZctDetOT]
				ELSE	
					SELECT     ZctCatArt.Cod_Art, ZctCatArt.Desc_Art, ZctCatArt.Exist_Art, ZctDetOT.Cod_DetOT, ZctDetOT.Ctd_Art,ZctDetOT.tiempo_vida, 
                    ZctCatArt.Desc_Art,ZctDetOT.CosArt_DetOT, ZctDetOT.PreArt_DetOT, ZctDetOT.CtdStd_DetOT, ZctDetOT.Cat_Alm, ZctDetOT.Cod_Mot, ZctCatArt.Cod_TpArt ,  ZctDetOT.cod_usu
					FROM zctCatArt INNER JOIN
                    ZctDetOT ON ZctCatArt.Cod_Art = ZctDetOT.Cod_Art
					WHERE Cod_EncOT = @Cod_EncOT 
					--SELECT Cod_EncOT , Cod_DetOT, Cod_Art, Ctd_Art, CosArt_DetOT, PreArt_DetOT, CtdStd_DetOT, Fch_DetOT FROM [ZctDetOT] WHERE Cod_EncOT = @Cod_EncOT 
		END
	ELSE
		if (@TpConsulta = '2')
			BEGIN
				declare @anio int
				declare @mes int 
				declare @Surtir_TpArt bit = 0

				select @Surtir_TpArt = tipo.Surtir_TpArt from dbo.ZctCatArt art 
				inner join dbo.ZctCatTpArt tipo
				on art.Cod_TpArt = tipo.Cod_TpArt
				where Cod_Art = @Cod_Art

				if (exists(select  CostoProm_Art from ZctArtXAlm where Cod_Art = @Cod_Art and Cod_Alm = @Cat_Alm and Exist_Art > 0))
					select  @CosArt_DetOT  = (CostoProm_Art/Exist_Art) from ZctArtXAlm where Cod_Art = @Cod_Art and Cod_Alm = @Cat_Alm and Exist_Art > 0
				else
					set @CosArt_DetOT  = 0
				
				

				exec dbo.sp_get_anio_mes @FchAplMov, @anio output, @mes output

				if not exists(SELECT Cod_EncOT , Cod_DetOT, Cod_Art, Ctd_Art, CosArt_DetOT, PreArt_DetOT, CtdStd_DetOT, Fch_DetOT FROM [ZctDetOT] WHERE Cod_DetOT = @Cod_DetOT  ) 
					BEGIN
						INSERT INTO [ZctDetOT] (Cod_EncOT, Cod_Art, Ctd_Art, CosArt_DetOT, PreArt_DetOT, CtdStd_DetOT, Fch_DetOT , Cat_Alm, Cod_Mot, uuid, anio_fiscal, mes_fiscal,tiempo_vida ,  cod_usu)
						VALUES(@Cod_EncOT, @Cod_Art, @Ctd_Art, @CosArt_DetOT, @PreArt_DetOT, @CtdStd_DetOT, GetDate() , @Cat_Alm, @Cod_Mot, NEWID(), @anio, @mes,@tiempo_vida,  @cod_usu )
						if (@Surtir_TpArt = 1)--(@Cod_TpArt  = 'ART')
							BEGIN
								SET @CosArt_DetOT = (@CosArt_DetOT * (-1)) * @CtdStd_DetOT
								SET @CtdStd_DetOT = (@CtdStd_DetOT * (-1))
								EXECUTE SP_ZctEncMovInv 2,0, 'SA', @CosArt_DetOT  , @Cod_EncOT,@Cod_Art  , @CtdStd_DetOT, 'OT', @FchAplMov , @Cat_Alm
							END

					END
				ELSE
					BEGIN
						SELECT @CosArtOLD = CosArt_DetOT,  @CtdStdOld =  CtdStd_DetOT , @CatAlmOLD = Cat_Alm FROM [ZctDetOT] WHERE Cod_DetOT = @Cod_DetOT  
						UPDATE [ZctDetOT] SET update_web = 0, Cod_Art = @Cod_Art, Ctd_Art = @Ctd_Art, CosArt_DetOT = @CosArt_DetOT , PreArt_DetOT = @PreArt_DetOT, CtdStd_DetOT = @CtdStd_DetOT , Cat_Alm = @Cat_Alm, Cod_Mot = @Cod_Mot, anio_fiscal = @anio, mes_fiscal = @mes,tiempo_vida=@tiempo_vida  WHERE Cod_DetOT = @Cod_DetOT 
						if (@CatAlmOLD <> @Cat_Alm) AND (@Surtir_TpArt = 1)--(@Cod_TpArt  = 'ART')
							BEGIN
						
								SET @CosArtOLD =  @CosArtOLD * @CtdStdOld
								EXECUTE SP_ZctEncMovInv 2,0, 'EC', @CosArtOLD   , @Cod_EncOT, @Cod_Art, @CtdStdOld, 'OT', @FchAplMov, @CatAlmOLD

								SET @CosArt_DetOT = (@CosArt_DetOT * (-1)) * @CtdStd_DetOT
								SET @CtdStd_DetOT = (@CtdStd_DetOT * (-1))
								EXECUTE SP_ZctEncMovInv 2,0, 'SA', @CosArt_DetOT  , @Cod_EncOT, @Cod_Art, @CtdStd_DetOT, 'OT', @FchAplMov, @Cat_Alm

						
							END
						ELSE
							if (@CtdStdOld > @CtdStd_DetOT ) AND (@Surtir_TpArt = 1)--(@Cod_TpArt  = 'ART')
								BEGIN
									--DEVOLUCION
									SET @CtdStd_DetOT =  @CtdStdOld - @CtdStd_DetOT 
									SET @CosArt_DetOT = @CtdStd_DetOT * @CosArt_DetOT
									EXECUTE SP_ZctEncMovInv 2,0, 'EN', @CosArt_DetOT  , @Cod_EncOT, @Cod_Art, @CtdStd_DetOT, 'OT', @FchAplMov, @Cat_Alm
								END
							ELSE
								if (@CtdStdOld < @CtdStd_DetOT ) AND (@Surtir_TpArt = 1)--(@Cod_TpArt  = 'ART')
									BEGIN
										SET @CtdStd_DetOT = @CtdStdOld - @CtdStd_DetOT
										SET @CosArt_DetOT = @CosArt_DetOT * @CtdStd_DetOT
										
										EXECUTE SP_ZctEncMovInv 2,0, 'SA', @CosArt_DetOT  , @Cod_EncOT,@Cod_Art  , @CtdStd_DetOT , 'OT',@FchAplMov, @Cat_Alm
									END	
						
						
					END
					EXECUTE SP_ACTUALIZA_SURTIDO_REQUISICION @Cod_EncOT

			END
		else
			if (@TpConsulta = '3')
				DELETE FROM [ZctDetOT] WHERE Cod_DetOT = @Cod_DetOT 
END
GO


