﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctRequisitionDetail]    Script Date: 19/12/2017 07:52:44 p. m. ******/
DROP PROCEDURE [dbo].[SP_ZctRequisitionDetail]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctRequisitionDetail]    Script Date: 19/12/2017 07:52:44 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Arturo Hernández Lomeli>
-- Create date: <07/04/2014>
-- Description:	<Actualiza las requisiciones>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ZctRequisitionDetail]
@uid	uniqueidentifier,
@folio_requisition	int,	
@amount	int	,
@price	numeric(18, 2)	,
@cod_art	varchar(20)	,
@cod_mot	int	,
@update_web	bit,
@omitir_iva bit
		

AS
BEGIN
	if (not exists (select folio_requisition from ZctRequisitionDetail where uid = @uid))
		BEGIN
			insert into ZctRequisitionDetail (uid, folio_requisition, amount, price, cod_art, cod_mot,update_web, omitir_iva)
			VALUES(@uid, @folio_requisition, @amount, @price, @cod_art, @cod_mot,@update_web, @omitir_iva)
		END
	ELSE
		BEGIN
			UPDATE ZctRequisitionDetail 
			SET folio_requisition = @folio_requisition, 
				amount = @amount, 
				price = @price, 
				cod_art = @cod_art, 
				cod_mot = @cod_mot,
				update_web = @update_web ,
				omitir_iva = @omitir_iva
				where uid = @uid 
		END
END
GO


