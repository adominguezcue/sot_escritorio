﻿USE [SOT]
GO
/****** Object:  StoredProcedure [dbo].[SP_ZctRespaldo]    Script Date: 21/11/2017 06:51:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[SP_ZctRespaldo]
	@Ruta as varchar(500)
AS
BEGIN
	
	BACKUP DATABASE [SOT] TO  DISK = @Ruta  WITH NOFORMAT, NOINIT,  NAME = N'SOT-Completa Base de datos Copia de seguridad', SKIP, NOREWIND, NOUNLOAD,  STATS = 10

END


