﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_EstadoCxP]    Script Date: 04/12/2017 09:48:54 p. m. ******/
DROP PROCEDURE [dbo].[SP_EstadoCxP]
GO

/****** Object:  StoredProcedure [dbo].[SP_EstadoCxP]    Script Date: 04/12/2017 09:48:54 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[SP_EstadoCxP] 
   @OC int,
   @estado int
   as
   begin
   declare @cuentapago int 
   set @cuentapago =(select e.cod_cuenta_pago from ZctEncCuentasPagar e)
   update ZctEncCuentasPagar set idEstado =@estado where Cod_OC =@OC 
   update ZctDetCuentasPagar set idEstado=@estado where cod_cuenta_pago=@cuentapago  
   end 
GO


