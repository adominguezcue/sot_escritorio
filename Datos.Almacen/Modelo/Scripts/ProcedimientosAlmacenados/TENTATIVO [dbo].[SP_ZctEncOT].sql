﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctEncOT]    Script Date: 09/11/2017 05:24:58 p. m. ******/
DROP PROCEDURE [dbo].[SP_ZctEncOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctEncOT]    Script Date: 09/11/2017 05:24:58 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:			 Arturo Hernandez
-- Create date:	      12-02-2006
-- Description:	      Cat�logo de Marcas
-- Fecha de modificación: 09 de noviembre de 2017
-- Modifica:			 Eduardo abraham Vázquez Rosado
-- Cambio:		      @Folio ya no es parámetro, ya que es lo mismo que @CodEnc_OT
-- =============================================
CREATE PROCEDURE [dbo].[SP_ZctEncOT] 
	@TpConsulta Int,
	@CodEnc_OT int,	
	@Cod_folio	varchar(10),
	@FchDoc_OT smalldatetime,
	@Cod_Cte varchar(20),
	@Cod_Contacto int,
	@Cod_Mot int,
	@Cod_Tec int,
	@FchEnt smalldatetime,
	@FchSal smalldatetime,
	@FchEntRmp smalldatetime,
	@FchSalRmp smalldatetime,
	@ObsOT text,
	@FchAplMov smalldatetime,
	@UltKm_OT int,
	@Cod_Estatus char(1)= 'N',
	@HistEstatus_OT varchar(100) = NULL,
	@Cod_TpOT int = null
AS
BEGIN

    declare @Folio	int = @CodEnc_OT

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	if (@TpConsulta = '1')
		BEGIN
			IF (@CodEnc_OT = 0)
				
					SELECT CodEnc_OT, Cod_folio, Folio, FchDoc_OT, Cod_Cte, Cod_Contacto,  Cod_Mot, Cod_Tec, FchEnt, FchSal, FchEntRmp, FchSalRmp, ObsOT, FchAplMov, UltKm_OT, Cod_Estatus, HistEstatus_OT, Cod_TpOT FROM [ZctEncOT]
				ELSE	
					SELECT CodEnc_OT, Cod_folio, Folio, FchDoc_OT, Cod_Cte, Cod_Contacto, Cod_Mot, Cod_Tec, FchEnt, FchSal, FchEntRmp, FchSalRmp, ObsOT, FchAplMov, UltKm_OT, Cod_Estatus, HistEstatus_OT, Cod_TpOT FROM [ZctEncOT] WHERE Cod_folio = @Cod_folio and  Folio = @Folio
		END
	ELSE
		if (@TpConsulta = '2')
			BEGIN
				if not exists(SELECT CodEnc_OT, FchDoc_OT, Cod_Cte, Cod_Mot, Cod_Tec FROM [ZctEncOT] WHERE CodEnc_OT = @CodEnc_OT) 
					INSERT INTO [ZctEncOT] (CodEnc_OT,Cod_folio, Folio, FchDoc_OT, Cod_Cte, Cod_Contacto, Cod_Mot, Cod_Tec, FchEnt, FchSal, FchEntRmp, FchSalRmp, ObsOT, FchAplMov, UltKm_OT ,Cod_Estatus, HistEstatus_OT, Cod_TpOT)
					VALUES(@CodEnc_OT, @Cod_folio, @Folio, GetDate(), @Cod_Cte, @Cod_Contacto, @Cod_Mot, @Cod_Tec , @FchEnt, @FchSal, @FchEntRmp, @FchSalRmp, @ObsOT, @FchAplMov, @UltKm_OT, @Cod_Estatus, @HistEstatus_OT, @Cod_TpOT)
				ELSE
					UPDATE [ZctEncOT] SET  Cod_Cte = @Cod_Cte, Cod_Mot = @Cod_Mot, Cod_Tec = @Cod_Tec, FchEnt = @FchEnt, FchSal = @FchSal, FchEntRmp = @FchEntRmp, FchSalRmp = @FchSalRmp, ObsOT=@ObsOT, FchAplMov = @FchAplMov, UltKm_OT =@UltKm_OT, Cod_Estatus = @Cod_Estatus, HistEstatus_OT =@HistEstatus_OT , Cod_TpOT = @Cod_TpOT    WHERE CodEnc_OT = @CodEnc_OT
			END
		else
			if (@TpConsulta = '3')
				DELETE FROM [ZctEncOT] WHERE CodEnc_OT = @CodEnc_OT
END



GO


