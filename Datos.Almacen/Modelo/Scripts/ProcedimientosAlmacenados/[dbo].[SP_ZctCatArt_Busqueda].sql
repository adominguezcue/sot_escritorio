﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctCatArt_Busqueda]    Script Date: 07/12/2017 07:47:14 a. m. ******/
DROP PROCEDURE [dbo].[SP_ZctCatArt_Busqueda]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctCatArt_Busqueda]    Script Date: 07/12/2017 07:47:14 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_ZctCatArt_Busqueda]
	@Codigo varchar(20)
AS
	BEGIN
		set @Codigo = replace(@Codigo, '''','-' )

		SELECT 
		Cod_Art, 
		Desc_Art, 
		Exist_Art AS ColExist, 
		0 as ColSurt,
		Prec_Art, 
		Cos_Art, 
		Cod_TpArt,
		ZctCatMar.Desc_Mar AS marca,
		OmitirIVA
		FROM ZctCatArt 
		LEFT JOIN ZctCatMar 
			ON ZctCatMar.Cod_Mar = ZctCatArt.Cod_Mar  
		WHERE Cod_Art = @Codigo or upc= @Codigo  
	END
GO


