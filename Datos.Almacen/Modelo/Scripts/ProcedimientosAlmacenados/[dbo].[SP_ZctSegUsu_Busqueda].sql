﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctSegUsu_Busqueda]    Script Date: 10/11/2017 08:23:55 a. m. ******/
DROP PROCEDURE [dbo].[SP_ZctSegUsu_Busqueda]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctSegUsu_Busqueda]    Script Date: 10/11/2017 08:23:55 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Arturo Hernandez
-- Create date: 12-02-2006
-- Description:	Catálogo de Marcas
-- =============================================
CREATE PROCEDURE [dbo].[SP_ZctSegUsu_Busqueda]
	@TpConsulta Int,
	@Cod_Usu int,
	@Nom_Usu varchar(10)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	if (@TpConsulta = '1')
		BEGIN
			IF (@Cod_Usu = 0)	
				SELECT Id AS Codigo, Alias AS Usuario FROM Seguridad.Usuarios
			else
				SELECT Id AS Codigo, Alias AS Usuario FROM Seguridad.Usuarios WHERE Id = @Cod_Usu
		END
					
END




GO


