USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ConsultarExistencias]    Script Date: 18/10/2017 06:36:30 p. m. ******/
DROP PROCEDURE [dbo].[SP_ConsultarExistencias]
GO

/****** Object:  StoredProcedure [dbo].[SP_ConsultarExistencias]    Script Date: 18/10/2017 06:36:30 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Eduardo Abraham V�zquez Rosado
-- Create date:	18 de octubre de 2017
-- Description:	Retorna la informaci�n de la existencia de un art�culo en los almacenes
-- =============================================
CREATE PROCEDURE [dbo].[SP_ConsultarExistencias] 
	@codigoArticulo varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
    ZctArtXAlm.Cod_Art as Codigo,
    ZctArtXAlm.Cod_Alm as CodigoAlmacen,
    ZctArtXAlm.Exist_Art as Existencias,
    ZctArtXAlm.CostoProm_Art as CostoPromedio,
    ZctCatAlm.Desc_CatAlm as Almacen
    FROM ZctArtXAlm 
    INNER JOIN ZctCatAlm 
	   ON ZctArtXAlm.Cod_Alm = ZctCatAlm.Cod_Alm 
    WHERE (ZctArtXAlm.Cod_Art = @codigoArticulo)
    ORDER BY ZctArtXAlm.Cod_Art, ZctArtXAlm.Cod_Alm 
END

GO


