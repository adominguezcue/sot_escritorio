﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_traspasarArticulos]    Script Date: 01/02/2018 03:42:42 p. m. ******/
DROP PROCEDURE [dbo].[SP_traspasarArticulos]
GO

/****** Object:  StoredProcedure [dbo].[SP_traspasarArticulos]    Script Date: 01/02/2018 03:42:42 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Miguel Hernández Lomelí
-- Create date: 18/08/10
-- Description:	Traspasa un artículo de almacen
-- =============================================
CREATE PROCEDURE [dbo].[SP_traspasarArticulos]
	
	@Codigo char(25), 
	@Cantidad decimal(19,4),
	@AlmacenEnvia int, --Almacen que envia el articulo
	@AlmacenDest int,	--Almacen que lo recibe
	@Folio varchar(10),
	@Costo money,
	@Fecha smalldatetime
AS
BEGIN

	DECLARE @Existencias decimal(19,4)
	DECLARE @CantidadNegativa decimal(19,4)
	DECLARE @costoNegativo money
	--Comprobamos que se tienen suficientes artículos en el alamacén que envía
	SELECT @Existencias = Exist_Art from ZctArtXAlm where Cod_Art = @Codigo and Cod_Alm = @AlmacenEnvia

	IF @Existencias >= @Cantidad 
	BEGIN
				
		Select @CantidadNegativa = 0-@Cantidad
		set @costoNegativo = (@Costo*(-1))
		--Grabamos el movimiento                              --Lo mismo pero negativo
		EXECUTE SP_ZctEncMovInv 2,0,'SA',@costoNegativo, @Folio,@Codigo,@CantidadNegativa,'IA',@Fecha, @AlmacenEnvia	
		EXECUTE SP_ZctEncMovInv 2,0,'EN',@Costo, @Folio,@Codigo,@Cantidad,'IA',@Fecha, @AlmacenDest	
	END
END
GO


