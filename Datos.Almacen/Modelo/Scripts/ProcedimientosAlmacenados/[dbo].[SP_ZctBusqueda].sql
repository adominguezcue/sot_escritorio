﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctBusqueda]    Script Date: 10/11/2017 08:19:09 a. m. ******/
DROP PROCEDURE [dbo].[SP_ZctBusqueda]
GO

/****** Object:  StoredProcedure [dbo].[SP_ZctBusqueda]    Script Date: 10/11/2017 08:19:09 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_ZctBusqueda]
	@Tabla as varchar(100),
	@Busqueda int = 0
AS
BEGIN
	IF (@Tabla = 'ZctCatEdo')	
		if (@Busqueda = 0)
			SELECT Cod_Edo as Codigo, Desc_Edo as Descripcion FROM [ZctCatEdo] 
		else
			SELECT Cod_Edo as Codigo,Desc_Edo as Descripcion FROM [ZctCatEdo] WHERE Cod_Edo = @Busqueda
	ELSE
	IF (@Tabla = 'ZctCatCiu')	
		if (@Busqueda = 0)
			SELECT Cod_Ciu as Codigo,Desc_Ciu as Descripcion FROM [ZctCatCiu] 
		else
			SELECT Cod_Ciu as Codigo,Desc_Ciu as Descripcion FROM [ZctCatCiu] WHERE Cod_Ciu = @Busqueda
ELSE
IF (@Tabla = 'ZctCatCiuXEdo')	
		if (@Busqueda = 0)
			SELECT Cod_Ciu as Codigo,Desc_Ciu as Descripcion FROM [ZctCatCiu] 
		else
			SELECT Cod_Ciu as Codigo,Desc_Ciu as Descripcion FROM [ZctCatCiu] WHERE Cod_Edo = @Busqueda

ELSE
IF (@Tabla = 'ZctCatCte_Busqueda')	
		if (@Busqueda = 0)
			SELECT [Cod_Cte]  ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) as Descripcion FROM [ZctCatCte] 
		else
			SELECT [Cod_Cte]  ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) as Descripcion FROM [ZctCatCte] where Cod_Cte = @Busqueda 
ELSE
IF (@Tabla = 'ZctCatMar_Verifica')	
		if (@Busqueda = 0)
			SELECT [Cod_Mar] FROM [ZctCatMar]
		ELSE	
			SELECT [Cod_Mar] FROM [ZctCatMar] WHERE Cod_Mar = @Busqueda  
IF (@Tabla = 'ZctCatMot')	
		if (@Busqueda = 0)
				SELECT [Cod_Mot] as Codigo ,[Vin_Mot] as Descripcion FROM [ZctCatMot]
			ELSE	
				SELECT [Cod_Mot] as Codigo ,[Vin_Mot] as Descripcion FROM [ZctCatMot] WHERE Cod_Mot = @Busqueda
			
ELSE
IF (@Tabla = 'ZctCatMar')	
		if (@Busqueda = 0)
				SELECT [Cod_Mar] as Codigo ,Desc_Mar as Descripcion FROM [ZctCatMar]
			ELSE	
				SELECT [Cod_Mar] as Codigo ,Desc_Mar as Descripcion FROM [ZctCatMar] WHERE Cod_Mar = @Busqueda
			
ELSE
IF (@Tabla = 'ZctCatMod')	
		if (@Busqueda = 0)
				SELECT CodMod_Mot as Codigo ,Desc_Mod as Descripcion FROM [ZctCatMod]
			ELSE	
				SELECT CodMod_Mot as Codigo ,Desc_Mod as Descripcion FROM [ZctCatMod] WHERE CodMod_Mot = @Busqueda
			
ELSE
IF (@Tabla = 'ZctCatModXMar')	
		if (@Busqueda = 0)
				SELECT CodMod_Mot as Codigo ,Desc_Mod as Descripcion FROM [ZctCatMod]
			ELSE	
				SELECT CodMod_Mot as Codigo ,Desc_Mod as Descripcion FROM [ZctCatMod] WHERE Cod_Mar = @Busqueda
			
ELSE
IF (@Tabla = 'ZctCatMotXOT')	
		if (@Busqueda = 0)
				SELECT     ZctCatMot.Cod_Mot  AS Codigo, isnull(ZctCatMar.Desc_Mar , space(0)) + SPACE(1) + isnull(ZctCatMod.Desc_Mod , space(0)) + SPACE(1) + isnull(CAST(ZctCatMot.Anno_Mot AS varchar(4)) , space(0)) 
				+ ' VIN: ' + isnull( ZctCatMot.Vin_Mot , space(0)) + ' MOTOR: ' + isnull(ZctCatMot.Motor_Mot , space(0)) + ' PLACAS: ' +  isnull(ZctCatMot.Placas_Mot , space(0) ) AS Descripcion
				FROM ZctCatMar INNER JOIN ZctCatMod ON ZctCatMar.Cod_Mar = ZctCatMod.Cod_Mar INNER JOIN
                ZctCatMot ON ZctCatMod.Cod_Mar = ZctCatMot.Cod_Mar AND ZctCatMod.CodMod_Mot = ZctCatMot.CodMod_Mot
			ELSE	
				SELECT     ZctCatMot.Cod_Mot  AS Codigo, isnull(ZctCatMar.Desc_Mar , space(0)) + SPACE(1) + isnull(ZctCatMod.Desc_Mod , space(0)) + SPACE(1) + isnull(CAST(ZctCatMot.Anno_Mot AS varchar(4)) , space(0)) 
				+ ' VIN: ' + isnull( ZctCatMot.Vin_Mot , space(0)) + ' MOTOR: ' + isnull(ZctCatMot.Motor_Mot , space(0)) + ' PLACAS: ' +  isnull(ZctCatMot.Placas_Mot , space(0) ) AS Descripcion
				FROM ZctCatMar INNER JOIN ZctCatMod ON ZctCatMar.Cod_Mar = ZctCatMod.Cod_Mar INNER JOIN
                ZctCatMot ON ZctCatMod.Cod_Mar = ZctCatMot.Cod_Mar AND ZctCatMod.CodMod_Mot = ZctCatMot.CodMod_Mot WHERE ZctCatMot.Cod_Mot = @Busqueda


			
ELSE
IF (@Tabla = 'ZctCatTec')	
		if (@Busqueda = 0)
				SELECT [Cod_Tec] as Codigo  , isnull([Nom_Tec], space(0)) + space(1) + isnull([ApPat_Tec], space(0)) + space(1) +  isnull([ApMat_Tec], space(0)) as Descripcion   FROM [ZctCatTec]
			ELSE	
				SELECT [Cod_Tec] as Codigo  , isnull([Nom_Tec], space(0)) + space(1) + isnull([ApPat_Tec], space(0)) + space(1) +  isnull([ApMat_Tec], space(0)) as Descripcion   FROM [ZctCatTec] WHERE Cod_Tec = @Busqueda			
ELSE
IF (@Tabla = 'ZctCatProv')	
		if (@Busqueda = 0)
				SELECT [Cod_Prov] as Codigo ,[Nom_Prov] as Descripcion FROM [ZctCatProv]
			ELSE	
				SELECT [Cod_Prov] as Codigo ,[Nom_Prov] as Descripcion FROM [ZctCatProv] WHERE Cod_Prov = @Busqueda			
ELSE
IF (@Tabla = 'ZctRptCatCatego')	
		if (@Busqueda = 0)
				SELECT [IdCatCatego_Rpt] as Codigo ,[NomCatego_Rpt] as Descripcion FROM [ZctRptCatCatego]
			ELSE	
				SELECT [IdCatCatego_Rpt] as Codigo ,[NomCatego_Rpt] as Descripcion FROM [ZctRptCatCatego] WHERE [IdCatCatego_Rpt] =  @Busqueda
ELSE
IF (@Tabla = 'ZctRptCatRpt')	
		if (@Busqueda = 0)
				SELECT [IdCat_Rpt] as Codigo ,[Nom_Rpt] as Descripcion  FROM [ZctRptCatRpt]
			ELSE	
				SELECT [IdCat_Rpt] as Codigo ,[Nom_Rpt] as Descripcion  FROM [ZctRptCatRpt] WHERE [idCatCatego_Rpt] =  @Busqueda
ELSE
IF (@Tabla = 'ZctRptCatTp')	
		if (@Busqueda = 0)
				SELECT [IdCat_TpRpt] as Codigo, [Nom_TpRpt] as Descripcion FROM [ZctRptCatTp]
			ELSE	
				SELECT [IdCat_TpRpt] as Codigo, [Nom_TpRpt] as Descripcion FROM [ZctRptCatTp] WHERE IdCat_TpRpt =  @Busqueda
ELSE
IF (@Tabla = 'ZctCatLinea')	
		if (@Busqueda = 0)
			SELECT Cod_Linea as Codigo, Desc_Linea as Descripcion FROM [ZctCatLinea] 
		else
			SELECT Cod_Linea as Codigo, Desc_Linea as Descripcion FROM [ZctCatLinea] WHERE Cod_Linea = @Busqueda
	
ELSE
IF (@Tabla = 'ZctCatLineaXDpto')	
		if (@Busqueda = 0)
			SELECT Cod_Linea as Codigo, Desc_Linea as Descripcion FROM [ZctCatLinea] 
		else
			SELECT Cod_Linea as Codigo, Desc_Linea as Descripcion FROM [ZctCatLinea] WHERE Cod_Dpto = @Busqueda
ELSE
IF (@Tabla = 'ZctCatDpto')	
		if (@Busqueda = 0)
			SELECT Cod_Dpto as Codigo, Desc_Dpto as Descripcion FROM [ZctCatDpto] 
		else
			SELECT Cod_Dpto as Codigo, Desc_Dpto as Descripcion FROM [ZctCatDpto] WHERE Cod_Dpto = @Busqueda
ELSE
IF (@Tabla = 'ZctCatGte')	
		if (@Busqueda = 0)
				SELECT [Cod_Gte] as Codigo  , [Nom_Gte] as Descripcion   FROM [ZctCatGte]
			ELSE	
				SELECT [Cod_Gte] as Codigo  , [Nom_Gte] as Descripcion   FROM [ZctCatGte] WHERE Cod_Gte = @Busqueda			

eLSE
IF (@Tabla = 'ZctCatAlm')	
		if (@Busqueda = 0)
			SELECT Cod_Alm as Codigo, Desc_CatAlm as Descripcion FROM [ZctCatAlm] 
		else
			SELECT Cod_Alm as Codigo,Desc_CatAlm as Descripcion FROM [ZctCatAlm] WHERE Cod_Alm = @Busqueda
eLSE
IF (@Tabla = 'ZctSegUsu')	
		if (@Busqueda = 0)
			SELECT Id as Codigo, Alias as Descripcion FROM Seguridad.Usuarios
		else
			SELECT Id as Codigo, Alias as Descripcion FROM Seguridad.Usuarios WHERE Id = @Busqueda
else
IF (@Tabla = 'ZctCatTpOT')	
		if (@Busqueda = 0)
			SELECT Cod_TpOT as Codigo, Desc_TpOT as Descripcion FROM ZctCatTpOT
		else
			SELECT Cod_TpOT as Codigo, Desc_TpOT as Descripcion FROM ZctCatTpOT WHERE Cod_TpOT = @Busqueda
ELSE
IF (@Tabla = 'ZctCatTpRef')	
		if (@Busqueda = 0)
			SELECT Cod_TpRef as Codigo, Desc_TpRef as Descripcion FROM [ZctCatTpRef] 
		else
			SELECT Cod_TpRef as Codigo, Desc_TpRef as Descripcion FROM [ZctCatTpRef] WHERE Cod_TpRef = @Busqueda
ELSE
IF (@Tabla = 'ZctCatRep')	
		if (@Busqueda = 0)
			SELECT Cod_Rep as Codigo, Nom_Rep as Descripcion FROM [ZctCatRep] 
		else
			SELECT Cod_Rep as Codigo, Nom_Rep as Descripcion FROM [ZctCatRep] WHERE Cod_Rep = @Busqueda
ELSE
IF (@Tabla = 'ZctCatMarcaTienda')	
		if (@Busqueda = 0)
			SELECT Cod_MarcaTienda as Codigo, Desc_MarcaTienda as Descripcion FROM [ZctCatMarcaTienda] 
		else
			SELECT Cod_MarcaTienda as Codigo,Desc_MarcaTienda as Descripcion FROM [ZctCatMarcaTienda] WHERE Cod_MarcaTienda = @Busqueda
ELSE
	IF (@Tabla = 'ZctCatZona')	
		if (@Busqueda = 0)
			SELECT Cod_Zona as Codigo,Desc_Zona as Descripcion FROM [ZctCatZona] 
		else
			SELECT Cod_Zona as Codigo,Desc_Zona as Descripcion FROM [ZctCatZona] WHERE Cod_Zona = @Busqueda
   IF (@Tabla = 'ZctRoles')	
		if (@Busqueda = 0)
			SELECT Cod_rol as Codigo,nombre as Descripcion FROM [ZctRoles] 
		else
			SELECT cod_rol as Codigo,nombre as Descripcion FROM ZctRoles WHERE Cod_rol = @Busqueda
IF (@Tabla = 'tipospersona')	
		if (@Busqueda = 0)
			SELECT Cod_tipo as Codigo,tipo as Descripcion FROM ZCTcatTiposPersonas 
		else
			SELECT Cod_tipo as codigo,tipo as Descripcion FROM ZCTcatTiposPersonas WHERE cod_tipo = @Busqueda
IF (@Tabla = 'ZctRoles')	
		if (@Busqueda = 0)
			SELECT Cod_rol as Codigo,nombre as Descripcion FROM [ZctRoles] 
		else
			SELECT cod_rol as Codigo,nombre as Descripcion FROM ZctRoles WHERE Cod_rol = @Busqueda
IF (@Tabla = 'ZctCatMoneda')	
		if (@Busqueda = 0)
			SELECT id as Codigo,Moneda as Descripcion FROM ZctCatMoneda 
		else
			SELECT id as Codigo,Moneda as Descripcion FROM ZctCatMoneda WHERE id = @Busqueda 
IF (@Tabla = 'ZctCatPresentacion')	
		if (@Busqueda = 0)
			SELECT id as Codigo,presentacion as Descripcion FROM ZctCatPresentacion 
		else
			SELECT id as Codigo,presentacion as Descripcion FROM ZctCatPresentacion WHERE id = @Busqueda 
END


GO


