﻿USE [SOT]
GO

/****** Object:  StoredProcedure [dbo].[SP_REQUISITION_OT]    Script Date: 20/12/2017 05:08:55 p. m. ******/
DROP PROCEDURE [dbo].[SP_REQUISITION_OT]
GO

/****** Object:  StoredProcedure [dbo].[SP_REQUISITION_OT]    Script Date: 20/12/2017 05:08:55 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_REQUISITION_OT] 
	@Folio int,
	@CodEnc_OT int OUTPUT
AS
BEGIN
	DECLARE 	@TpConsulta Int,
	@FchDoc_OT smalldatetime,
	@Cod_Cte varchar(20),
	@Cod_Mot int,
	@Cod_Tec int,
	@FchEnt smalldatetime,
	@FchSal smalldatetime,
	@FchEntRmp smalldatetime,
	@FchSalRmp smalldatetime,
	@ObsOT varchar(250),
	@FchAplMov smalldatetime,
	@UltKm_OT int,
	@Cod_Estatus char(1)= 'N',
	@HistEstatus_OT varchar(100) = NULL,
	@Cod_TpOT int = null,
	@Cod_DetOT int,
	@Cod_Art varchar(20),
	@Ctd_Art int,
	@CosArt_DetOT numeric(18, 2),
	@PreArt_DetOT numeric(18, 2),
	@CtdStd_DetOT int,
	@Fch_DetOT smalldatetime,
	@Cat_Alm int,
	@Cod_TpArt varchar(10) = 'ART'

	--OBTIENE EL FOLIO
	UPDATE ZctCatFolios SET  Consc_Folio = Consc_Folio + 1 where  Cod_Folio = 'OT'	
	Select  @CodEnc_OT= Consc_Folio FROM ZctCatFolios where  Cod_Folio = 'OT'

	SELECT @TpConsulta = 2,	@FchDoc_OT =GETDATE(), 	@Cod_Cte = Cod_Cte,	@Cod_Mot = NULL, @Cod_Tec = NULL, @FchEnt = GETDATE(), @FchSal = GETDATE(), @FchEntRmp=GETDATE(), @FchSalRmp = GETDATE(), @ObsOT =observaciones,
	@FchAplMov = GETDATE(),	@UltKm_OT = NULL, @Cod_Estatus = 'N', @HistEstatus_OT = '',	@Cod_TpOT = NULL, @Cat_Alm = cod_alm
	FROM  ZctRequisition
	WHERE folio = @Folio
	--GRABA EL ENCABEZADO
	EXECUTE [SP_ZctEncOT] @TpConsulta, @CodEnc_OT,	'OT', @FchDoc_OT,	@Cod_Cte,	0, @Cod_Mot ,	@Cod_Tec ,	@FchEnt ,	@FchSal ,	@FchEntRmp ,	@FchSalRmp ,	@ObsOT ,	@FchAplMov ,	@UltKm_OT ,	@Cod_Estatus ,	@HistEstatus_OT ,	@Cod_TpOT 

   Declare detalles cursor for
    SELECT cod_art,  amount, price, price,0, GETDATE(),GETDATE(), cod_mot, 'ART'
	FROM  ZctRequisitionDetail
	WHERE folio_requisition = @Folio
	OPEN detalles 
	fetch next from detalles into 
	@Cod_Art ,	@Ctd_Art ,	@CosArt_DetOT ,	@PreArt_DetOT ,	@CtdStd_DetOT ,	@Fch_DetOT ,	@FchAplMov  ,	@Cod_Mot  ,	@Cod_TpArt  

	set @Cod_DetOT = 0
	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @Cod_DetOT = @Cod_DetOT +1
	    EXECUTE [SP_ZctDetOT] @TpConsulta,	@CodEnc_OT ,	@Cod_DetOT ,	@Cod_Art ,	@Ctd_Art ,	@CosArt_DetOT ,	@PreArt_DetOT ,	@CtdStd_DetOT ,	@Fch_DetOT ,	@FchAplMov ,	@Cat_Alm ,	@Cod_Mot  ,	@Cod_TpArt  

		fetch next from detalles into 
		@Cod_Art ,	@Ctd_Art ,	@CosArt_DetOT ,	@PreArt_DetOT ,	@CtdStd_DetOT ,	@Fch_DetOT ,	@FchAplMov  ,	@Cod_Mot  ,	@Cod_TpArt  
	END

	CLOSE detalles
	DEALLOCATE detalles

	UPDATE ZctRequisition SET update_web = null , estatus = 'ORDEN TRABAJO GENERADA', Cod_EncOT = @CodEnc_OT where folio = @Folio
	return @CodEnc_OT
END

GO


