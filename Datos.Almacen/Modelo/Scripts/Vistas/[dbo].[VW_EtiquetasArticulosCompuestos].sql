﻿USE [SOT]
GO

/****** Object:  View [dbo].[VW_EtiquetasArticulosCompuestos]    Script Date: 01/02/2018 03:30:32 p. m. ******/
DROP VIEW [dbo].[VW_EtiquetasArticulosCompuestos]
GO

/****** Object:  View [dbo].[VW_EtiquetasArticulosCompuestos]    Script Date: 01/02/2018 03:30:32 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Script para el comando SelectTopNRows de SSMS  ******/
CREATE VIEW [dbo].[VW_EtiquetasArticulosCompuestos]
AS
SELECT        encmov.CodMov_Inv AS Id, art.Cod_Art AS CodigoArticulo, art.Desc_Art AS NombreArticulo, CONVERT(datetime, encmov.FchMov_Inv) AS FechaElaboracion, isnull(encmov.CtdMov_Inv, 0) AS Cantidad
FROM            dbo.ZctEncMovInv AS encmov INNER JOIN
                         dbo.ZctCatArt AS art ON encmov.Cod_Art = art.Cod_Art INNER JOIN
                         dbo.ZctMixItems AS mx ON art.Cod_Art = mx.Cod_Art
WHERE        encmov.TpMov_Inv = 'EN' AND encmov.CtdMov_Inv > 0
GROUP BY encmov.CodMov_Inv, art.Cod_Art, art.Desc_Art, encmov.FchMov_Inv, encmov.CtdMov_Inv

GO


