﻿USE [SOT]
GO

/****** Object:  View [dbo].[VW_ZctCatAlmWeb]    Script Date: 01/12/2017 01:53:00 p. m. ******/
DROP VIEW [dbo].[VW_ZctCatAlmWeb]
GO

/****** Object:  View [dbo].[VW_ZctCatAlmWeb]    Script Date: 01/12/2017 01:53:00 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VW_ZctCatAlmWeb] AS
SELECT  
    alm.Cod_Alm, 
    alm.Desc_CatAlm, 
    alm.uuid, 
    alm.update_web, 
    config.taller,
    cc.Nombre as centro_costos
FROM ZctCatAlm alm
LEFT JOIN SotSchema.CentrosCostos cc
    on alm.IdCentroCostos = cc.Id
CROSS JOIN ZctCatPar config

GO


