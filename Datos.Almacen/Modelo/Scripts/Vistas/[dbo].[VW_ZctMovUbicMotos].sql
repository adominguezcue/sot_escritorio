﻿USE [SOT]
GO

/****** Object:  View [dbo].[VW_ZctMovUbicMotos]    Script Date: 10/11/2017 08:50:09 a. m. ******/
DROP VIEW [dbo].[VW_ZctMovUbicMotos]
GO

/****** Object:  View [dbo].[VW_ZctMovUbicMotos]    Script Date: 10/11/2017 08:50:09 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_ZctMovUbicMotos]
AS
SELECT     dbo.ZctCatMot.Cod_Mot, dbo.ZctCatHisCambMot.Vin_Mot, dbo.ZctCatHisCambMot.Cod_CteNew, dbo.ZctCatCte.Nom_Cte, dbo.ZctCatHisCambMot.fchMod_Hist, 
                      Seguridad.Usuarios.Alias as Nom_Usu, dbo.ZctCatUbicacion.Desc_Ubicacion, dbo.ZctCatHisCambMot.Cod_Ubicacion, dbo.ZctCatHisCambMot.Cod_Usu
FROM         dbo.ZctCatMot INNER JOIN
                      dbo.ZctCatHisCambMot ON dbo.ZctCatMot.Vin_Mot = dbo.ZctCatHisCambMot.Vin_Mot LEFT OUTER JOIN
                      dbo.ZctCatCte ON dbo.ZctCatHisCambMot.Cod_CteNew = dbo.ZctCatCte.Cod_Cte LEFT OUTER JOIN
                      dbo.ZctCatUbicacion ON dbo.ZctCatHisCambMot.Cod_Ubicacion = dbo.ZctCatUbicacion.Cod_Ubicacion LEFT OUTER JOIN
                      Seguridad.Usuarios ON dbo.ZctCatHisCambMot.Cod_Usu = Seguridad.Usuarios.Id
WHERE     (dbo.ZctCatUbicacion.Desc_Ubicacion IS NOT NULL)


GO


