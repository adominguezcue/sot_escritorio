﻿USE [SOT]
GO

/****** Object:  View [dbo].[VW_ZctEncCuentasPagar]    Script Date: 10/12/2017 12:36:38 a. m. ******/
DROP VIEW [dbo].[VW_ZctEncCuentasPagar]
GO

/****** Object:  View [dbo].[VW_ZctEncCuentasPagar]    Script Date: 10/12/2017 12:36:38 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_ZctEncCuentasPagar] AS
SELECT        ZctEncCuentasPagar.cod_cuenta_pago, ZctEncCuentasPagar.Cod_OC, ZctEncCuentasPagar.monto, ZctEncCuentasPagar.iva, ZctEncCuentasPagar.subtotal, ZctEncCuentasPagar.total, 
                         ZctEncCuentasPagar.fecha_creacion, ZctEncCuentasPagar.fecha_vencido, ZctEncCuentasPagar.idEstado, ZctEncCuentasPagar.update_web, ZctEncCuentasPagar.uuid, ZctCatProv.Nom_Prov
FROM            ZctEncCuentasPagar INNER JOIN
                         ZctEncOC ON ZctEncCuentasPagar.Cod_OC = ZctEncOC.Cod_OC INNER JOIN
                         ZctCatProv ON ZctEncOC.Cod_Prov = ZctCatProv.Cod_Prov
GO


