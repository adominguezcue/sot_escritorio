﻿USE [SOT]
GO

/****** Object:  View [dbo].[VW_ZctTicked_Caja]    Script Date: 10/11/2017 09:30:38 a. m. ******/
DROP VIEW [dbo].[VW_ZctTicked_Caja]
GO

/****** Object:  View [dbo].[VW_ZctTicked_Caja]    Script Date: 10/11/2017 09:30:38 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_ZctTicked_Caja]
as
SELECT        enc.CodEnc_Caja, enc.cod_folio, enc.FchEnc_Caja, cte.Cod_Cte, cte.Nom_Cte, cte.ApPat_Cte, cte.ApMat_Cte, u.Alias as Nom_Usu, cte.Dir_Cte, det.Ctd_Art, det.Cod_Art, art.Desc_Art, art.Cos_Art, det.total, det.subtotal, 
                         det.iva, enc.fecha_folio AS fechapago, enc.staus AS estado, det.nodoc_caja, vw_tipo_pago.credito, vw_tipo_pago.contado, vw_tipo_pago.cambio, enc.CodEnc_OT
FROM            ZctEncCaja AS enc INNER JOIN
                         ZctDetCaja AS det ON enc.CodEnc_Caja = det.CodEnc_Caja AND det.cod_folio = enc.cod_folio INNER JOIN
                         ZctEncOT AS ot ON ot.CodEnc_OT = enc.CodEnc_OT INNER JOIN
                         ZctCatCte AS cte ON cte.Cod_Cte = ot.Cod_Cte INNER JOIN
                         Seguridad.Usuarios AS u ON u.Id = enc.cod_usu INNER JOIN
                         ZctCatArt AS art ON art.Cod_Art = det.Cod_Art INNER JOIN
                         vw_tipo_pago ON enc.cod_folio = vw_tipo_pago.cod_folio AND enc.CodEnc_Caja = vw_tipo_pago.CodEnc_Caja
GO


