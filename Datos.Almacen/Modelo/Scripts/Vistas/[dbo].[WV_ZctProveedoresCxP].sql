﻿USE [SOT]
GO

/****** Object:  View [dbo].[WV_ZctProveedoresCxP]    Script Date: 05/12/2017 12:18:00 a. m. ******/
DROP VIEW [dbo].[WV_ZctProveedoresCxP]
GO

/****** Object:  View [dbo].[WV_ZctProveedoresCxP]    Script Date: 05/12/2017 12:18:00 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[WV_ZctProveedoresCxP]
as 
SELECT        p.Cod_Prov as Cod_Prov, p.Nom_Prov, SUM(cxp.subtotal) AS subtotal, SUM(cxp.iva) AS iva, SUM(cxp.total) AS total
FROM            dbo.ZctCatProv AS p INNER JOIN
		      dbo.ZctEncOC AS oc
                          ON oc.Cod_Prov = p.Cod_Prov INNER JOIN
			 dbo.ZctEncCuentasPagar AS cxp
                          ON oc.Cod_OC = cxp.Cod_OC
					 GROUP BY p.Cod_Prov, p.Nom_Prov
GO


