﻿USE [SOT]
GO

/****** Object:  View [dbo].[VW_RptEstatusVentas]    Script Date: 10/11/2017 09:05:05 a. m. ******/
DROP VIEW [dbo].[VW_RptEstatusVentas]
GO

/****** Object:  View [dbo].[VW_RptEstatusVentas]    Script Date: 10/11/2017 09:05:05 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_RptEstatusVentas] as
SELECT        ZctEncCaja.cod_folio, ZctEncCaja.CodEnc_Caja AS Caja, ZctEncCaja.CodEnc_OT AS Transaccion, MAX(ZctFacturaEnc.folio_factura) AS Factura, SUM(ZctDetCaja.total) AS Total, ZctEncCaja.cod_usu, 
                         ZctEncCaja.staus,DATEADD(dd, DATEDIFF(dd, 0, ZctEncCaja.FchEnc_Caja), 0) as FchEnc_Caja, ZctEncCaja.cod_usu_cancela, Seguridad.Usuarios.Alias AS Creo_caja, ZctSegUsu_1.Alias AS Cancelo_caja
FROM            ZctDetCaja INNER JOIN
                         ZctEncCaja ON ZctDetCaja.cod_folio = ZctEncCaja.cod_folio AND ZctDetCaja.CodEnc_Caja = ZctEncCaja.CodEnc_Caja LEFT OUTER JOIN
                         ZctFacturaEnc ON ZctEncCaja.CodEnc_OT = ZctFacturaEnc.CodEnc_OT LEFT OUTER JOIN
                         Seguridad.Usuarios ON ZctEncCaja.cod_usu = Seguridad.Usuarios.Id LEFT OUTER JOIN
                         Seguridad.Usuarios AS ZctSegUsu_1 ON ZctEncCaja.cod_usu_cancela = ZctSegUsu_1.Id
GROUP BY ZctEncCaja.cod_folio, ZctEncCaja.CodEnc_Caja, ZctEncCaja.CodEnc_OT, ZctEncCaja.cod_usu, ZctEncCaja.staus, ZctEncCaja.FchEnc_Caja, ZctEncCaja.cod_usu_cancela, Seguridad.Usuarios.Alias, 
                         ZctSegUsu_1.Alias



GO


