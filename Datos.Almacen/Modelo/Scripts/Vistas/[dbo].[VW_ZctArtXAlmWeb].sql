﻿USE [SOT]
GO

/****** Object:  View [dbo].[VW_ZctArtXAlmWeb]    Script Date: 20/12/2017 08:31:42 a. m. ******/
DROP VIEW [dbo].[VW_ZctArtXAlmWeb]
GO

/****** Object:  View [dbo].[VW_ZctArtXAlmWeb]    Script Date: 20/12/2017 08:31:42 a. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VW_ZctArtXAlmWeb] AS
SELECT        ZctArtXAlm.Cod_Alm, ZctArtXAlm.Cod_Art, Convert(numeric(18, 10), ZctArtXAlm.Exist_Art) as Exist_Art, ZctArtXAlm.CostoProm_Art, ZctArtXAlm.uuid, ZctArtXAlm.update_web, ZctCatPar.taller
FROM            ZctArtXAlm CROSS JOIN
                         ZctCatPar
GO


