﻿ALTER TABLE [dbo].[ZctDetOT] DROP CONSTRAINT [FK_ZctDetOT_ZctSegUsu]
GO

ALTER TABLE [dbo].[ZctAperturasCajas] DROP CONSTRAINT [FK_Zct_AperturasCajas_ZctSegUsu]
GO

ALTER TABLE [dbo].[ZctEncCaja] DROP CONSTRAINT [FK_ZctEncCaja_ZctSegUsu]
GO

ALTER TABLE [dbo].[ZctDetCaja] DROP CONSTRAINT [FK_ZctDetCaja_ZctSegUsu]
GO

