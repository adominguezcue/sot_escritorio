﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Almacen.Auxiliares
{
    internal class ModalidadesSP
    {
        internal const int CONSULTA = 1;
        internal const int CREACION_O_MODIFICACION = 2;
        internal const int ELIMINACION = 3;
    }
}
