﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class toPDF
    Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
    Dim oRDoc As ReportDocument
    Dim expo As New ExportOptions
    Dim sRecSelFormula As String
    Dim oDfDopt As New DiskFileDestinationOptions
    Dim strCrystalReportFilePath As String
    Dim strPdfFileDestinationPath As String

    Private _CrystalReportFileNameFullPath As String
    Public Property CrystalReportFileNameFullPath() As String
        Get
            Return _CrystalReportFileNameFullPath
        End Get
        Set(ByVal value As String)
            _CrystalReportFileNameFullPath = value
        End Set
    End Property

    Private _pdfFileNameFullPath As String
    Public Property pdfFileNameFullPath() As String
        Get
            return _pdfFileNameFullPath
        End Get
        Set(ByVal value As String)
            _pdfFileNameFullPath = value
        End Set
    End Property

    Private _recSelFormula As String
    Public Property recSelFormula() As String
        Get
            return _recSelFormula
        End Get
        Set(ByVal value As String)
            _recSelFormula = value
        End Set
    End Property
    Private crTableLogonInfo As New TableLogOnInfo
    Private loginfo As CrystalDecisions.Shared.ConnectionInfo

    Public Function Transfer(server As String, database As String)
        oRDoc = New ReportDocument
        Try
            Console.WriteLine(_CrystalReportFileNameFullPath)
            oRDoc.Load(_CrystalReportFileNameFullPath) 'loads the crystalreports in to the memory
            oRDoc.RecordSelectionFormula = _recSelFormula   'used if u want pass the query to u r crystal form

            oDfDopt.DiskFileName = _pdfFileNameFullPath    'path of file where u want to locate ur PDF
            expo = oRDoc.ExportOptions

            expo.ExportDestinationType = ExportDestinationType.DiskFile

            expo.ExportFormatType = ExportFormatType.PortableDocFormat
            expo.DestinationOptions = oDfDopt


            loginfo = New CrystalDecisions.Shared.ConnectionInfo

            loginfo.ServerName = server ' ConfigurationManager.AppSettings.Get("Server")
            loginfo.DatabaseName = database  'ConfigurationManager.AppSettings.Get("Database")
            loginfo.UserID = "Arturo" 'ConfigurationManager.AppSettings.Get("User")
            loginfo.Password = "L0k0m0t0r4" 'ConfigurationManager.AppSettings.Get("Password")
            crTableLogonInfo.ConnectionInfo = loginfo

            oRDoc.SetDatabaseLogon("Arturo", "L0k0m0t0r4", server, database, True) 'login for your DataBase
            'oRDoc.SetDatabaseLogon("arrenda", "net", "10.1.1.1", "DaltSoftArren")
            Dim CrTables As Tables
            Dim CrTable As Table
            CrTables = oRDoc.Database.Tables

            For Each CrTable In CrTables
                'crTableLogonInfo = CrTable.LogOnInfo
                'crTableLogonInfo.ConnectionInfo = crConnectionInfo

                CrTable.ApplyLogOnInfo(crTableLogonInfo)


            Next
            oRDoc.VerifyDatabase()
            oRDoc.Refresh()
            oRDoc.Export()
            Return True
        Finally
            oRDoc.Close()
        End Try

    End Function
End Class
