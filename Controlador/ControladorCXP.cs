﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorCXP : Controlador.Controlador<CXP, cxpCollection>
    {
        private IDAOModelo<CXP> dao;
        public ControladorCXP(IDAOModelo<CXP> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override CXP GrabaModeloWeb(string server, string model, string usuario, string password, CXP modelo)
        {
            
            if (modelo.uid.HasValue)
                model = model + "/make_from_xml/" + modelo.uid;
            CXP modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
