﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlador
{
    public interface IDAOModelo<T>
    {
        T BuscaModelo(string busqueda);
        void GrabaModelo(T modelo);
        List<T> ObtieneModelos();
        List<T> ObtieneModelosParaEnvioWeb();
    }
}
