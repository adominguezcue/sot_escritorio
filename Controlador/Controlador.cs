﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
namespace Controlador
{
    public class Controlador<T, C>
    {
        private IDAOModelo<T> _dao;

        public Controlador(IDAOModelo<T> _dao)
        {
            this._dao = _dao;
        }

        public T BuscaModelo(string codigo_Modelo)
        {
            return _dao.BuscaModelo(codigo_Modelo);
        }

        public void GrabaModelo(T _Modelo)
        {
            _dao.GrabaModelo(_Modelo);
        }

        public List<T> ObtieneModelos()
        {
            return _dao.ObtieneModelos();
        }

        public List<T> ObtieneModelosWeb(string server, string usuario, string password, string model, string taller)
        {
            List<T> Modelos = new List<T>();

            string response = GetModel(server, model, usuario, password, taller);
            C collection = convertToType<C>(response);
            foreach (PropertyInfo _prop in collection.GetType().GetProperties())
            {
                if (_prop.PropertyType == typeof(T[]))
                {
                    T[] range = (T[])_prop.GetValue(collection, null);
                    Modelos.AddRange(range);
                }
            }

            return Modelos;

        }
        private string GetModel(string server, string model, string usuario, string password, string taller)
        {
            //string token = Coneccion(server, usuario, password);
            //string response = ProcesaXMLRails.Procesa.GetXmlData(server + "/" + model + "/?user_credentials=" + token);
            string response = ProcesaXMLRails.Procesa.GetXmlData(server + "/" + model + "/get_data_xml/" + taller);
            return response;
        }

        public virtual T GrabaModeloWeb(string server, string model, string usuario, string password, T modelo)
        {
            //string token = Coneccion(server, usuario, password);
            string xml_modelo = convertToXml<T>(modelo);

            //Transversal.Log.Logger.Info(xml_modelo);

            //Console.WriteLine(xml_modelo);
            //string response = ProcesaXMLRails.Procesa.sendXmlData(server + "/" + model + "/?user_credentials=" + token, xml_modelo );

            string response = ProcesaXMLRails.Procesa.sendXmlData(server + "/" + model, xml_modelo);

            Transversal.Log.Logger.Info("---------Recibido: " + response);

            //write_debug(response);
            return convertToType<T>(response);
        }

        private void write_debug(string response)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\debug.txt", true))
            {
                file.WriteLine(response);
            }
        }


        private x convertToType<x>(string response)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(x));
            x _object;
            using (StringReader reader = new StringReader(response))
            {
                _object = (x)serializer.Deserialize(reader);
            }
            return _object;
        }

        private string convertToXml<x>(x element)
        {


            XmlSerializer serial = new XmlSerializer(typeof(x));
            //serial.
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

            //Add an empty namespace and empty value
            ns.Add("", "");

            using (Text.EncodedStringWriter wr = new Text.EncodedStringWriter(Encoding.UTF8))
            //using (StringWriter wr = new StringWriter())
            {

                serial.Serialize(wr, element, ns);


                return wr.ToString();
            }
        }
        //TODO: Con tiempo hay que crear un verdadero singleton
        private string _token = "";
        public string Coneccion(string server, string usuario, string password)
        {
            if (_token == "")
                _token = _Coneccion(server, usuario, password);
            return _token;
        }

        public string _Coneccion(string server, string usuario, string password)
        {
            string ruta = server;
            Modelo.user_session _user = new Modelo.user_session() { username = usuario, password = password };
            //XmlDocument doc = new XmlDocument();
            //XmlSerializer serial = new XmlSerializer(typeof(Modelo.user_session));
            ProcesaXMLRails.Procesa _procesa = new ProcesaXMLRails.Procesa();
            string response;
            //using (Text.EncodedStringWriter wr = new Text.EncodedStringWriter(Encoding.UTF8))
            //{
            //serial.Serialize(wr, _user);
            string xml = convertToXml<Modelo.user_session>(_user);
            response = ProcesaXMLRails.Procesa.sendXmlData(ruta + "user_sessions/", xml);
            //}
            Modelo.user_session session = convertToType<Modelo.user_session>(response);
            return session.single_access_token;

        }

        public List<T> ObtieneModelosParaEnvioWeb()
        {
            return _dao.ObtieneModelosParaEnvioWeb();
        }
    }
}
