﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorReporteGeneral : Controlador.Controlador<ReporteGeneral, ReporteGeneralCollection>
    {
        private IDAOModelo<ReporteGeneral> dao;
        public ControladorReporteGeneral(IDAOModelo<ReporteGeneral> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override ReporteGeneral GrabaModeloWeb(string server, string model, string usuario, string password, ReporteGeneral modelo) {
            
            if (modelo.uid != Guid.Empty)
                model = model + "/make_from_xml/" + modelo.uid;

            ReporteGeneral modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            //modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
