﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controlador
{
    public interface IDAOBusquedaConParametros<T>: IDAOModelo<T>
    {
        List<T> ObtieneModelos(object[] args);

    }
}
