﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public interface IDAOVenta : IDAOModelo<sale>
    {
        string ObtieneFolio(int cash_register_id);
    }
}
