﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorRequisitions : Controlador.Controlador<Requisition, RequisitionsCollection>
    {
        private IDAOModelo<Requisition> dao;
        public ControladorRequisitions(IDAOModelo<Requisition> _dao)
            : base(_dao)
        {
            dao = _dao;
        }

        

        public override Requisition GrabaModeloWeb(string server, string model, string usuario, string password, Requisition modelo) {
            
            if (modelo.uid_web.HasValue)
                model = model + "/make_from_xml/" + modelo.uid_web;

            Requisition modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;

            var codigosArt = modelo_grabar.detail_requisitions_attributes.detail_requisitions.Select(m => m.cod_art).ToList();

            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
