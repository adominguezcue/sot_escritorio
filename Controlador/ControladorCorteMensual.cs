﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorCorteMensuales : Controlador.Controlador<corte_mensual, CorteMensualesCollection>
    {
        private IDAOModelo<corte_mensual> dao;
        public ControladorCorteMensuales(IDAOModelo<corte_mensual> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override corte_mensual GrabaModeloWeb(string server, string model, string usuario, string password, corte_mensual modelo)
        {

            if (modelo.uid_web.HasValue)
                model = model + "/make_from_xml/" + modelo.uid_web;

            corte_mensual modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }


    }
}
