﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorInventarioFinales : Controlador.Controlador<inventario, InventarioFinalesCollection>
    {
        private IDAOModelo<inventario> dao;
        public ControladorInventarioFinales(IDAOModelo<inventario> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override inventario GrabaModeloWeb(string server, string model, string usuario, string password, inventario modelo) {
            
            if (modelo.uid_web.HasValue)
                model = model + "/make_from_xml/" + modelo.uid_web;

            inventario modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
