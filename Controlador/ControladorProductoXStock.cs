﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorProductoXStock : Controlador.Controlador<productxstock, productxstockCollection>
    {
        private IDAOModelo<productxstock> dao;
        public ControladorProductoXStock(IDAOModelo<productxstock> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override productxstock GrabaModeloWeb(string server, string model, string usuario, string password, productxstock modelo)
        {
            
            if (modelo.uid.HasValue)
                model = model + "/make_from_xml/" + modelo.uid;

            productxstock modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
