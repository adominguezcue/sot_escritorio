﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorProducto : Controlador.Controlador<Product, ProductCollection>
    {
        private IDAOModelo<Product> dao;
        public ControladorProducto(IDAOModelo<Product> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override Product GrabaModeloWeb(string server, string model, string usuario, string password, Product modelo)
        {
            
            if (modelo.uid_web.HasValue)
                model = model + "/make_from_xml/" + modelo.uid_web;

            Product modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
