﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorBudgets : Controlador.Controlador<budget, BudgetsCollection>
    {
        private IDAOModelo<budget> dao;
        public ControladorBudgets(IDAOModelo<budget> _dao)
            : base(_dao)
        {
            dao = _dao;
        }

        

        public override budget GrabaModeloWeb(string server, string model, string usuario, string password, budget modelo) {

            if (modelo.uid.HasValue)
                model = model + "/make_from_xml/" + modelo.uid;
            else
                Transversal.Log.Logger.Error(string.Format("Requisición sin uuid, {0} de {1}", modelo.month_Pres, modelo.Anno_Pres));

            budget modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
