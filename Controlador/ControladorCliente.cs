﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorCliente : Controlador.Controlador<customer, CustomerCollection>
    {
        private IDAOModelo<customer> dao;
        public ControladorCliente(IDAOModelo<customer> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override customer GrabaModeloWeb(string server, string model, string usuario, string password, customer modelo)
        {
            
            if (modelo.uid.HasValue)
                model = model + "/make_from_xml/" + modelo.uid;

            customer modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
