﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorRegiones : Controlador.Controlador<region, RegionesCollection>
    {
        private IDAOModelo<region> dao;
        public ControladorRegiones(IDAOModelo<region> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override region GrabaModeloWeb(string server, string model, string usuario, string password, region modelo) {
            
            if (modelo.uid_web.HasValue)
                model = model + "/make_from_xml/" + modelo.uid_web;

            region modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
