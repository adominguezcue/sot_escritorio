﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorStock : Controlador.Controlador<stock, stockCollection>
    {
        private IDAOModelo<stock> dao;
        public ControladorStock(IDAOModelo<stock> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override stock GrabaModeloWeb(string server, string model, string usuario, string password, stock modelo)
        {
            
            if (modelo.uid.HasValue)
                model = model + "/make_from_xml/" + modelo.uid;

            stock modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
