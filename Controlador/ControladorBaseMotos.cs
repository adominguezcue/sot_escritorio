﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;
namespace Controlador
{
    public class ControladorBaseMotos : Controlador.Controlador<base_moto, BaseMotosCollection>
    {
        private IDAOModelo<base_moto> dao;
        public ControladorBaseMotos(IDAOModelo<base_moto> _dao)
            : base(_dao)
        {
            dao = _dao;
        }



        public override base_moto GrabaModeloWeb(string server, string model, string usuario, string password, base_moto modelo) {
            
            if (modelo.uid_web.HasValue)
                model = model + "/make_from_xml/" + modelo.uid_web;

            base_moto modelo_grabar = base.GrabaModeloWeb(server, model, usuario, password, modelo);
            modelo_grabar.actualizado = true;
            this.GrabaModelo(modelo_grabar);
            return modelo_grabar;
        }

        
    }
}
