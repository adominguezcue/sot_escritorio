﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExtraComponentsWPF.Buttons
{
    /// <summary>
    /// Lógica de interacción para FloatingIconButton.xaml
    /// </summary>
    public partial class FloatingIconButton : UserControl
    {
        public string Text 
        {
            get { return ButtonText.Text; }
            set { ButtonText.Text = value; }
        }

        public Size IconSize
        {
            get { return ButtonImage.RenderSize; }
            set 
            {
                ButtonImage.Width = value.Width;
                ButtonImage.Height = value.Height;

                MainGrid.RowDefinitions[0].Height = new GridLength(value.Height / 2);
                MainGrid.RowDefinitions[1].Height = new GridLength(value.Height / 2);

                MainGrid.ColumnDefinitions[1].Width = new GridLength(value.Width / 2);
                MainGrid.ColumnDefinitions[2].Width = new GridLength(value.Width / 2);
            }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(FloatingIconButton),
              new PropertyMetadata(cambio));

        private static void cambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            FloatingIconButton fiButton = target as FloatingIconButton;
            fiButton.ButtonImage.Source = e.NewValue as ImageSource;
        }

        public ImageSource ImageSource 
        {
            //get { return ButtonImage.Source; }
            //set { ButtonImage.Source = value; }

            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        
        }

        public new FontFamily FontFamily 
        {
            get { return ButtonText.FontFamily; }
            set { ButtonText.FontFamily = value; }
        }

        public new double FontSize
        {
            get { return ButtonText.FontSize; }
            set { ButtonText.FontSize = value; }
        }

        public FloatingIconButton()
        {
            InitializeComponent();
            ButtonText.DataContext = this;
        }

        public event RoutedEventHandler Click;

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventHandler handler = Click;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
