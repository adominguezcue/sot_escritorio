﻿
Imports System.IO
Imports System.Text
Imports System.ServiceModel
Imports System.Security.Cryptography.X509Certificates
Imports System.Net
Imports System.Net.Security
Imports FirmaSAT


Public Module Procesa
    Private _path As String

    Public Property path() As String
        Get
            Return _path
        End Get
        Set(ByVal value As String)
            _path = value
        End Set
    End Property

    Private _KeyFile As String
    Public Property KeyFile() As String
        Get
            Return _KeyFile
        End Get
        Set(ByVal value As String)
            _KeyFile = value
        End Set
    End Property
    Private _CerFile As String
    Public Property CerFile() As String
        Get
            Return _CerFile
        End Get
        Set(ByVal value As String)
            _CerFile = value
        End Set
    End Property

    Private _Pipe As String
    Public Property Pipe() As String
        Get
            Return _Pipe
        End Get
        Set(ByVal value As String)
            _Pipe = value
        End Set
    End Property


    Public password_cfdi As String
    Public pfx As String
    Public pfx_path As String
    Public path_factura As String

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="factura"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GeneraXML(ByVal factura As IFactura, ByVal tipo As TipoFactura) As String

        Dim serial As New Xml.Serialization.XmlSerializer(factura.GetType)
        Dim doc As New Xml.XmlDocument()

        Using wr As New Text.EncodedStringWriter(Encoding.UTF8)

            serial.Serialize(wr, factura)

            doc.LoadXml(wr.ToString)
            'Liberar cuando se facture para cfdi
            If tipo = TipoFactura.CFDI Then doc.Prefix = "cfdi"
            Dim root As Xml.XmlElement = doc.DocumentElement()
            If tipo = TipoFactura.CFDI Then root.Prefix = "cfdi"

            If tipo = TipoFactura.CFDI Then
                For Each element As Xml.XmlElement In doc.DocumentElement
                    element.Prefix = "cfdi"
                Next
            End If

            wr.Close()
        End Using
        'liberar esto cuando sea factura por cdfi
        Dim sw As New StringWriter
        Dim xw As New Xml.XmlTextWriter(sw)
        doc.WriteTo(xw)
        Return sw.ToString
    End Function

    Public Delegate Function CancelarCFDI(ByVal usuario As String, ByVal password As String, ByVal RFC As String, ByVal listaCFDI As String(), cadena As String, password_pfx As String) As String()
    Public Delegate Function TimbrarCFDPrueba(ByVal usuario As String, ByVal password As String, ByVal xml As String) As String()

    Public Function CancelaFactura(ByVal RFC As String, ByVal folio_fiscal As String, ByVal cancelar As CancelarCFDI) As Boolean
        Dim listaCFDI As String() = {folio_fiscal}
        'listaCFDI.Add(folio_fiscal)
        'Dim ServicioFY As New WSFY.WS_TFD_FYSoapClient ' .WS_FDSoapClient
        'Dim Respuesta As New WSFY.ArrayOfString
        'Se recibe la respuesta 
        'Respuesta = ServicioFY.CancelarCFDI(usuario, password, RFC, listaCFDI, "Cadena del Pfx en formato Base 64", "Password del Pfx")
        pfx = GetPfx()
        cancelar.Invoke(usuario, password, RFC, listaCFDI, pfx, password_cfdi)

        Return True
    End Function

    Private Function FixUTF8StringForDisplay(ByVal s As String) As String
        Dim b As Byte() = System.Text.Encoding.GetEncoding(28591).GetBytes(s)
        Dim newstr As String = System.Text.Encoding.UTF8.GetString(b)
        Return newstr
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GeneraPipe(ByVal folio As String, ByVal Pipe As String) As String
        Using wr As New System.IO.StreamWriter(path_factura & folio & ".txt")
            wr.Write(Pipe)
            wr.Close()
        End Using
        Return Pipe
    End Function

    ''' <remarks></remarks>
    Private Function GetPfx() As String
        If pfx_path = String.Empty Then Throw New ArgumentException("archivo pfx no esta configurado, comuniquese con el departamento de sistemas.")
        Dim pfx As String
        Using wr As New System.IO.StreamReader(_path & pfx_path)
            pfx = wr.ReadToEnd()
        End Using
        Return pfx
    End Function


    Public Function GetPipe(ByVal factura As String) As String
        Return FirmaSAT.Sat.MakePipeStringFromXml(path_factura & factura & ".xml")
    End Function




    'Function GetSello(ByVal factura As String) As String
    '    Return Sat.GetXmlAttribute(_path & factura & ".xml", "sello", "Comprobante")
    'End Function

    Function GetSello(ByVal factura As String) As String
        'Throw New NotImplementedException
        Return Sat.GetXmlAttribute(path_factura & factura & ".xml", "sello", "Comprobante")
    End Function


    Function GetFolioFiscal(ByVal factura As String) As Guid
        If Dir(path_factura & factura & ".xml") <> "" Then
            Dim uuid As String = Sat.GetXmlAttribute(path_factura & factura & ".xml", "UUID", "TimbreFiscalDigital")
            Return New Guid(uuid)
            'Throw New NotImplementedException
        Else
            Return Guid.Empty
        End If
    End Function

    Function GetFechaFiscal(ByVal factura As String) As Date
        Return Convert.ToDateTime(Sat.GetXmlAttribute(path_factura & factura & ".xml", "FechaTimbrado", "TimbreFiscalDigital"))
        'Throw New NotImplementedException
    End Function
    Function GetSelloDigitalEmisor(ByVal factura As String) As String
        Return Sat.GetXmlAttribute(path_factura & factura & ".xml", "selloCFD", "TimbreFiscalDigital")
        'Throw New NotImplementedException
    End Function
    Function GetSelloDigitalSAT(ByVal factura As String) As String
        Return Sat.GetXmlAttribute(path_factura & factura & ".xml", "selloSAT", "TimbreFiscalDigital")
        'Throw New NotImplementedException
    End Function
    Dim usuario As String = "ORN140327D32" '"DEMO787878FY1"
    Dim password As String = "F5rHwdoObKP$&" '"fuJ&sSet$"
    Public Const Integrator_ID As String = "c427a39b-33d2-40ac-a5c6-9c970bc20493"
    'Public Const RFC As String = "PNU1111251F3"



    Public Function GeneraFactura(ByVal factura As IFactura, ByVal tipo As TipoFactura, ByVal referencia As String, timbrado As TimbrarCFDPrueba) As String
        Dim respuesta As String()
        Dim xml As String = GeneraXML(factura, tipo)
        Dim pathxml As String = path_factura & factura.folio & "_sin_firmar.xml"
        Dim pathxml_final As String = path_factura & factura.folio & ".xml"
        If GetSelloDigitalSAT(factura.folio).Length = 0 Then
            GrabaXMLFactura(pathxml, xml)
            Dim m As Integer = FirmaSAT.Sat.ValidateXml(xml, XmlOption.Default)
            If FirmaSAT.Sat.SignXml(pathxml_final, pathxml, _path + KeyFile, password_cfdi, CerFile) <> 0 Then
                Throw New Exception("No se firmo correctamente la factura " & Sat.LastError & " " & General.ErrorLookup(m))
            End If

            xml = GetXml(pathxml_final)
            respuesta = timbrado.Invoke(usuario, password, xml)
            If respuesta(0) = "" Then
                GrabaXMLFactura(pathxml_final, respuesta(3))
                GrabaXMLFactura(path_factura & factura.folio & "_comprobante.xml", respuesta(4))
            Else
                Throw New ArgumentException(respuesta(1) + " " + respuesta(2))
            End If
        End If

        '    'Temporal por pruebas
        '    'Liberr para cfdi
        Return GetPipe(factura.folio, pathxml_final, tipo)

        'Catch serviceFault As FaultException(Of srvSecurity.FallaServicio)
        '    Throw New ApplicationException(TranslateFault.ToServiceError(serviceFault.Detail).Description)
        'Catch serviceFault As FaultException(Of srvSecurity.FallaSesion)
        '    Throw New ApplicationException(TranslateFault.ToSessionError(serviceFault.Detail).Description)
        '    '(TranslateFault.ToSessionError(sessionFauld.Detail))
        'Catch serviceFault As FaultException(Of srvInvoices.FallaServicio)
        '    Throw New ApplicationException(TranslateFault.ToServiceError(serviceFault.Detail).Description)
        'Catch sessionFauld As FaultException(Of srvInvoices.FallaSesion)
        '    Throw New ApplicationException(TranslateFault.ToSessionError(sessionFauld.Detail).Description)
        'Catch validationFault As FaultException(Of srvInvoices.FallaValidacion)
        '    Throw New ApplicationException(TranslateFault.ToValidationError(validationFault.Detail).Description)
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Function

    'Public Function GetQR(ByVal RFC As String, ByVal folio_fiscal As Guid, ByVal factura As String) As Byte()
    '    Dim transactionID As Long = ObtieneTransactionID()
    '    Dim token As String = ObtieneSesionEcodex(RFC, transactionID, Integrator_ID)
    '    Using timbradoSrv = New SrvTimbrado.TimbradoClient()
    '        Dim infoQR = timbradoSrv.ObtenerQRTimbrado(RFC, token, transactionID, folio_fiscal.ToString)
    '        Dim buffer(16 * 1024) As Byte
    '        Return infoQR.Imagen
    '        'Using ms = New System.IO.MemoryStream(infoQR.Imagen)
    '        '    Dim read As Integer
    '        '    While(read = ms.)
    '        'End Using

    '        ''Dim qr As New System.Drawing.Bitmap(ms)
    '        'Return ms.GetBuffer
    '    End Using
    'End Function

    Public Function GetPipe(ByVal folio As String, ByVal pathxml As String, ByVal tipo As TipoFactura)
        Dim n As Integer = FirmaSAT.Sat.ValidateXml(pathxml)
        If n = 0 Then
            Return FirmaSAT.Sat.MakePipeStringFromXml(pathxml)
        Else
            Throw New Exception("El xml no es valido " & Sat.LastError & " " & General.ErrorLookup(n))
        End If
    End Function



    Private Sub GrabaXMLFactura(ByVal pathxml As String, ByVal XML As String)
        Using wr As New System.IO.StreamWriter(pathxml)

            wr.Write(XML)
            wr.Close()
        End Using
    End Sub

    Private Function GetXml(pathxml_final As String) As String
        Dim xml As String
        Using rd As New System.IO.StreamReader(pathxml_final)
            xml = rd.ReadToEnd()
        End Using
        Return Xml
    End Function


End Module



Namespace Text

    '''<summary>Implements a TextWriter for writing information to a string. The information is stored in an underlying StringBuilder.</summary>
    Public Class EncodedStringWriter
        Inherits StringWriter

        'Private property setter
        Private _Encoding As Encoding

        '''<summary>Default constructor for the EncodedStringWriter class.</summary>
        '''<param name=“sb“>The formatted result to output.</param>
        '''<param name=“Encoding“>A member of the System.Text.Encoding class.</param>
        Public Sub New(ByVal Encoding As Encoding)
            MyBase.New()
            _Encoding = Encoding
        End Sub

        '''<summary>Gets the Encoding in which the output is written.</summary>
        '''<param name=“Encoding“>The Encoding in which the output is written.</param>
        '''<remarks>This property is necessary for some XML scenarios where a header must be written containing the encoding used by the StringWriter. This allows the XML code to consume an arbitrary StringWriter and generate the correct XML header.</remarks>
        Public Overrides ReadOnly Property Encoding() As Encoding
            Get
                Return _Encoding
            End Get
        End Property

    End Class

End Namespace