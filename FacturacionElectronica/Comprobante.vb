﻿Public Class Comprobante

    Property Serie As String

    Property Version As String

    Property Folio As Integer

    Property Fecha As Date

    Property noAprobacion As Integer

    Property anoAprobacion As Integer

    Property formaDePago As String

    Property tipoDeComprobante As String

    Property Total As Decimal


    Property SubTotal() As Decimal

    Property Iva As Double

    Property Id_Emisor As Integer
    Property Id_Receptor As Integer
    Property metodoDePago As String
    Property NumCtaPago As String
    Property noCertificado As String

    Property tipoFactura As TipoFactura


End Class
