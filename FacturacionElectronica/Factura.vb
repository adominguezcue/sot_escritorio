﻿Public Enum TipoFactura
    CFD = 0
    CFDI = 1
End Enum
Public Interface IFactura


    Property Version() As String
    Property Serie() As String
    Property folio() As String

    Property fecha() As Date
    
'CFD
'Private _noAprobacion As Integer
'<Xml.Serialization.XmlAttribute("noAprobacion")> _
'Public Property noAprobacion() As Integer
'    Get
'        Return _noAprobacion
'    End Get
'    Set(ByVal value As Integer)
'        _noAprobacion = value
'    End Set
'End Property

'Private _anoAprobacion As Integer
'<Xml.Serialization.XmlAttribute("anoAprobacion")> _
'Public Property anoAprobacion() As Integer
'    Get
'        Return _anoAprobacion
'    End Get
'    Set(ByVal value As Integer)
'        _anoAprobacion = value
'    End Set
'End Property


    Property formaDePago() As String
    Property subTotal() As Decimal
    Property total() As Decimal
    Property tipoDeComprobante() As String
    Property metodoDePago() As String
    Property LugarExpedicion() As String
    Property NumCtaPago() As String
    'Property FolioFiscalOrig() As String
    'Property SerieFolioFiscalOrig() As String
    'Property FechaFolioFiscalOrig() As DateTime
    'Property MontoFolioFiscalOrig As Decimal
    Property noCertificado() As String
    Property certificado() As String
    Property sello() As String
    Property Emisor() As cEmisor
    Property Receptor() As cReceptor
    Property Conceptos() As List(Of Concepto)
    Property Impuestos() As cImpuestos
    
End Interface

'
<Xml.Serialization.XmlRoot(elementname:="Comprobante", Namespace:="http://www.sat.gob.mx/cfd/3")> _
Public Class cFacturaCFDI
    Implements IFactura
    'Public Sub New()

    'End Sub

    'Public Sub New(Tipo As TipoFactura)
    '    If Tipo = TipoFactura.CFD Then
    '        xsi = "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd"
    '        _Version = "2.2"
    '    Else
    '        xsi = "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd"
    '        _Version = "3.2"
    '    End If
    'End Sub

    'Public xsi As String = "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd"

    'Public xsi As String '= "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd"
    <Xml.Serialization.XmlAttribute("schemaLocation", Namespace:=System.Xml.Schema.XmlSchema.InstanceNamespace)> _
    Public xsi As String = "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd"

    Private _Version As String = "3.2"
    <Xml.Serialization.XmlAttribute("version")> _
    Public Property Version() As String Implements IFactura.Version
        Get
            Return _Version
        End Get
        Set(ByVal value As String)
            _Version = value
        End Set
    End Property

    <Xml.Serialization.XmlAttribute("Moneda")> _
    Public Moneda As String

    <Xml.Serialization.XmlAttribute("TipoCambio")> _
    Public TipoCambio As String

    Private _Serie As String
    <Xml.Serialization.XmlAttribute("serie")> _
    Public Property Serie() As String Implements IFactura.Serie
        Get
            Return _Serie
        End Get
        Set(ByVal value As String)
            _Serie = value
        End Set
    End Property

    Private _folio As String
    <Xml.Serialization.XmlAttribute("folio")> _
    Public Property folio() As String Implements IFactura.folio
        Get
            Return _folio
        End Get
        Set(ByVal value As String)
            _folio = value
        End Set
    End Property

    Private _fecha As Date
    <Xml.Serialization.XmlAttribute("fecha")> _
    Public Property fecha() As Date Implements IFactura.fecha
        Get
            Return _fecha.ToString("yyyy-MM-dd'T'HH:mm:ss")
        End Get
        Set(ByVal value As Date)
            _fecha = value
        End Set
    End Property


    'Public Property fecha_string() As String
    '    Get
    '        'Return String.Format("{0:s}", _fecha) ' 
    '        Return _fecha
    '    End Get

    '    Set(ByVal value As String)

    '    End Set
    'End Property

    'CFD
    'Private _noAprobacion As Integer
    '<Xml.Serialization.XmlAttribute("noAprobacion")> _
    'Public Property noAprobacion() As Integer
    '    Get
    '        Return _noAprobacion
    '    End Get
    '    Set(ByVal value As Integer)
    '        _noAprobacion = value
    '    End Set
    'End Property

    'Private _anoAprobacion As Integer
    '<Xml.Serialization.XmlAttribute("anoAprobacion")> _
    'Public Property anoAprobacion() As Integer
    '    Get
    '        Return _anoAprobacion
    '    End Get
    '    Set(ByVal value As Integer)
    '        _anoAprobacion = value
    '    End Set
    'End Property


    Private _formaDePago As String
    <Xml.Serialization.XmlAttribute("formaDePago")> _
    Public Property formaDePago() As String Implements IFactura.formaDePago
        Get
            Return _formaDePago
        End Get
        Set(ByVal value As String)
            _formaDePago = value
        End Set
    End Property

    Private _subTotal As Decimal
    <Xml.Serialization.XmlAttribute("subTotal")> _
    Public Property subTotal() As Decimal Implements IFactura.subTotal
        Get
            Return _subTotal
        End Get
        Set(ByVal value As Decimal)
            _subTotal = Math.Round(value, 2)
        End Set
    End Property

    Private _total As Decimal
    <Xml.Serialization.XmlAttribute("total")> _
    Public Property total() As Decimal Implements IFactura.total
        Get
            Return _total
        End Get
        Set(ByVal value As Decimal)
            _total = Math.Round(value, 2)
        End Set
    End Property

    Private _tipoDeComprobante As String

    <Xml.Serialization.XmlAttribute("tipoDeComprobante")> _
    Public Property tipoDeComprobante() As String Implements IFactura.tipoDeComprobante
        Get
            Return _tipoDeComprobante
        End Get
        Set(ByVal value As String)
            _tipoDeComprobante = value
        End Set
    End Property

    <Xml.Serialization.XmlAttribute("metodoDePago")> _
    Public Property metodoDePago() As String Implements IFactura.metodoDePago

    <Xml.Serialization.XmlAttribute("LugarExpedicion")> _
    Public Property LugarExpedicion() As String Implements IFactura.LugarExpedicion

    <Xml.Serialization.XmlAttribute("NumCtaPago")> _
    Public Property NumCtaPago() As String Implements IFactura.NumCtaPago

    <Xml.Serialization.XmlAttribute("FolioFiscalOrig")> _
    Public Property FolioFiscalOrig() As String

    <Xml.Serialization.XmlAttribute("SerieFolioFiscalOrig")> _
    Public Property SerieFolioFiscalOrig() As String

    'Al ser opcionales y ser tipos "valor", no puedo dejarlos para que no se impriman en el XML
    <Xml.Serialization.XmlAttribute("FechaFolioFiscalOrig")> _
    Public Property FechaFolioFiscalOrig() As DateTime

    Private _MontoFolioFiscalOrig As Decimal
    <Xml.Serialization.XmlAttribute("MontoFolioFiscalOrig")> _
    Public Property MontoFolioFiscalOrig As Decimal
        Get
            Return Math.Round(_MontoFolioFiscalOrig, 2)
        End Get
        Set(ByVal value As Decimal)
            _MontoFolioFiscalOrig = value
        End Set
    End Property


    Private _noCertificado As String
    <Xml.Serialization.XmlAttribute("noCertificado")> _
    Public Property noCertificado() As String Implements IFactura.noCertificado
        Get
            Return _noCertificado
        End Get
        Set(ByVal value As String)
            _noCertificado = value
        End Set
    End Property

    Private _certificado As String

    <Xml.Serialization.XmlAttribute("certificado")> _
    Public Property certificado() As String Implements IFactura.certificado
        Get
            Return _certificado
        End Get
        Set(ByVal value As String)
            _certificado = value
        End Set
    End Property

    Private _sello As String

    <Xml.Serialization.XmlAttribute("sello")> _
    Public Property sello() As String Implements IFactura.sello
        Get
            Return _sello
        End Get
        Set(ByVal value As String)
            _sello = value
        End Set
    End Property


    Private _Emisor As New cEmisor

    Public Property Emisor() As cEmisor Implements IFactura.Emisor
        Get
            Return _Emisor
        End Get
        Set(ByVal value As cEmisor)
            _Emisor = value
        End Set
    End Property

    Private _Receptor As New cReceptor

    Public Property Receptor() As cReceptor Implements IFactura.Receptor
        Get
            Return _Receptor
        End Get
        Set(ByVal value As cReceptor)
            _Receptor = value
        End Set
    End Property

    Private _Conceptos As New List(Of Concepto)
    Public Property Conceptos() As List(Of Concepto) Implements IFactura.Conceptos
        Get
            Return _Conceptos
        End Get
        Set(ByVal value As List(Of Concepto))
            _Conceptos = value
        End Set
    End Property

    Private _Impuestos As New cImpuestos

    Public Property Impuestos() As cImpuestos Implements IFactura.Impuestos
        Get
            Return _Impuestos
        End Get
        Set(ByVal value As cImpuestos)
            _Impuestos = value
        End Set
    End Property


End Class



<Xml.Serialization.XmlRoot(elementname:="Comprobante", Namespace:="http://www.sat.gob.mx/cfd/2")> _
Public Class cFacturaCFD
    Implements IFactura
    'Public Sub New()

    'End Sub

    'Public Sub New(Tipo As TipoFactura)
    '    If Tipo = TipoFactura.CFD Then
    '        xsi = "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd"
    '        _Version = "2.2"
    '    Else
    '        xsi = "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd"
    '        _Version = "3.2"
    '    End If
    'End Sub

    'Public xsi As String = "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd"

    'Public xsi As String '= "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd"
    <Xml.Serialization.XmlAttribute("schemaLocation", Namespace:=System.Xml.Schema.XmlSchema.InstanceNamespace)> _
    Public xsi As String = "http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd"

    Private _Version As String = "2.2"
    <Xml.Serialization.XmlAttribute("version")> _
    Public Property Version() As String Implements IFactura.Version
        Get
            Return _Version
        End Get
        Set(ByVal value As String)
            _Version = value
        End Set
    End Property

    Private _Serie As String
    <Xml.Serialization.XmlAttribute("serie")> _
    Public Property Serie() As String Implements IFactura.Serie
        Get
            Return _Serie
        End Get
        Set(ByVal value As String)
            _Serie = value
        End Set
    End Property

    Private _folio As String
    <Xml.Serialization.XmlAttribute("folio")> _
    Public Property folio() As String Implements IFactura.folio
        Get
            Return _folio
        End Get
        Set(ByVal value As String)
            _folio = value
        End Set
    End Property

    Private _fecha As Date
    <Xml.Serialization.XmlAttribute("fecha")> _
    Public Property fecha() As Date Implements IFactura.fecha
        Get
            Return _fecha.ToString("yyyy-MM-dd'T'HH:mm:ss")
        End Get
        Set(ByVal value As Date)
            _fecha = value
        End Set
    End Property

    'CFD
    Private _noAprobacion As Integer
    <Xml.Serialization.XmlAttribute("noAprobacion")> _
    Public Property noAprobacion() As Integer
        Get
            Return _noAprobacion
        End Get
        Set(ByVal value As Integer)
            _noAprobacion = value
        End Set
    End Property

    Private _anoAprobacion As Integer
    <Xml.Serialization.XmlAttribute("anoAprobacion")> _
    Public Property anoAprobacion() As Integer
        Get
            Return _anoAprobacion
        End Get
        Set(ByVal value As Integer)
            _anoAprobacion = value
        End Set
    End Property


    Private _formaDePago As String
    <Xml.Serialization.XmlAttribute("formaDePago")> _
    Public Property formaDePago() As String Implements IFactura.formaDePago
        Get
            Return _formaDePago
        End Get
        Set(ByVal value As String)
            _formaDePago = value
        End Set
    End Property

    Private _subTotal As Decimal
    <Xml.Serialization.XmlAttribute("subTotal")> _
    Public Property subTotal() As Decimal Implements IFactura.subTotal
        Get
            Return _subTotal
        End Get
        Set(ByVal value As Decimal)
            _subTotal = Math.Round(value, 2)
        End Set
    End Property

    Private _total As Decimal
    <Xml.Serialization.XmlAttribute("total")> _
    Public Property total() As Decimal Implements IFactura.total
        Get
            Return _total
        End Get
        Set(ByVal value As Decimal)
            _total = Math.Round(value, 2)
        End Set
    End Property

    Private _tipoDeComprobante As String

    <Xml.Serialization.XmlAttribute("tipoDeComprobante")> _
    Public Property tipoDeComprobante() As String Implements IFactura.tipoDeComprobante
        Get
            Return _tipoDeComprobante
        End Get
        Set(ByVal value As String)
            _tipoDeComprobante = value
        End Set
    End Property

    <Xml.Serialization.XmlAttribute("metodoDePago")> _
    Public Property metodoDePago() As String Implements IFactura.metodoDePago

    <Xml.Serialization.XmlAttribute("LugarExpedicion")> _
    Public Property LugarExpedicion() As String Implements IFactura.LugarExpedicion

    <Xml.Serialization.XmlAttribute("NumCtaPago")> _
    Public Property NumCtaPago() As String Implements IFactura.NumCtaPago

    '<Xml.Serialization.XmlAttribute("FolioFiscalOrig")> _
    'Public Property FolioFiscalOrig() As String

    '<Xml.Serialization.XmlAttribute("SerieFolioFiscalOrig")> _
    'Public Property SerieFolioFiscalOrig() As String

    ''Al ser opcionales y ser tipos "valor", no puedo dejarlos para que no se impriman en el XML
    '<Xml.Serialization.XmlAttribute("FechaFolioFiscalOrig")> _
    'Public Property FechaFolioFiscalOrig() As DateTime

    'Private _MontoFolioFiscalOrig As Decimal
    '<Xml.Serialization.XmlAttribute("MontoFolioFiscalOrig")> _
    'Public Property MontoFolioFiscalOrig As Decimal
    '    Get
    '        Return Math.Round(_MontoFolioFiscalOrig, 2)
    '    End Get
    '    Set(ByVal value As Decimal)
    '        _MontoFolioFiscalOrig = value
    '    End Set
    'End Property


    Private _noCertificado As String
    <Xml.Serialization.XmlAttribute("noCertificado")> _
    Public Property noCertificado() As String Implements IFactura.noCertificado
        Get
            Return _noCertificado
        End Get
        Set(ByVal value As String)
            _noCertificado = value
        End Set
    End Property

    Private _certificado As String

    <Xml.Serialization.XmlAttribute("certificado")> _
    Public Property certificado() As String Implements IFactura.certificado
        Get
            Return _certificado
        End Get
        Set(ByVal value As String)
            _certificado = value
        End Set
    End Property

    Private _sello As String

    <Xml.Serialization.XmlAttribute("sello")> _
    Public Property sello() As String Implements IFactura.sello
        Get
            Return _sello
        End Get
        Set(ByVal value As String)
            _sello = value
        End Set
    End Property


    Private _Emisor As New cEmisor

    Public Property Emisor() As cEmisor Implements IFactura.Emisor
        Get
            Return _Emisor
        End Get
        Set(ByVal value As cEmisor)
            _Emisor = value
        End Set
    End Property

    Private _Receptor As New cReceptor

    Public Property Receptor() As cReceptor Implements IFactura.Receptor
        Get
            Return _Receptor
        End Get
        Set(ByVal value As cReceptor)
            _Receptor = value
        End Set
    End Property

    Private _Conceptos As New List(Of Concepto)
    Public Property Conceptos() As List(Of Concepto) Implements IFactura.Conceptos
        Get
            Return _Conceptos
        End Get
        Set(ByVal value As List(Of Concepto))
            _Conceptos = value
        End Set
    End Property

    Private _Impuestos As New cImpuestos

    Public Property Impuestos() As cImpuestos Implements IFactura.Impuestos
        Get
            Return _Impuestos
        End Get
        Set(ByVal value As cImpuestos)
            _Impuestos = value
        End Set
    End Property


End Class
Public Class cEmisor

    Private _nombre As String
    <Xml.Serialization.XmlAttribute("nombre")> _
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Private _RFC As String
    <Xml.Serialization.XmlAttribute("rfc")> _
    Public Property RFC() As String
        Get
            Return _RFC
        End Get
        Set(ByVal value As String)
            _RFC = value
        End Set
    End Property

    Private _Domicilio As New cDomicilio

    Public Property DomicilioFiscal() As cDomicilio
        Get
            Return _Domicilio
        End Get
        Set(ByVal value As cDomicilio)
            _Domicilio = value
        End Set
    End Property

    Private _ExpedidoEn As New cDomicilio
    Public Property ExpedidoEn() As cDomicilio
        Get
            Return _ExpedidoEn
        End Get
        Set(ByVal value As cDomicilio)
            _ExpedidoEn = value
        End Set
    End Property

    Private _RegimenFiscal As New cRegimen
    Public Property RegimenFiscal As cRegimen
        Get
            Return _RegimenFiscal
        End Get
        Set(value As cRegimen)
            _RegimenFiscal = value
        End Set
    End Property
End Class

Public Class cReceptor

    Private _nombre As String
    <Xml.Serialization.XmlAttribute("nombre")> _
    Public Property nombre() As String
        Get
            Return _nombre
        End Get
        Set(ByVal value As String)
            _nombre = value
        End Set
    End Property

    Private _RFC As String
    <Xml.Serialization.XmlAttribute("rfc")> _
    Public Property RFC() As String
        Get
            Return _RFC
        End Get
        Set(ByVal value As String)
            _RFC = value
        End Set
    End Property

    Private _Domicilio As New cDomicilio

    Public Property Domicilio() As cDomicilio
        Get
            Return _Domicilio
        End Get
        Set(ByVal value As cDomicilio)
            _Domicilio = value
        End Set
    End Property

End Class

Public Class cDomicilio
    Private _Calle As String
    <Xml.Serialization.XmlAttribute("calle")> _
    Public Property Calle() As String
        Get
            Return _Calle
        End Get
        Set(ByVal value As String)
            _Calle = value
        End Set
    End Property

    Private _CodigoPostal As String
    <Xml.Serialization.XmlAttribute("codigoPostal")> _
    Public Property CodigoPostal() As String
        Get
            Return _CodigoPostal
        End Get
        Set(ByVal value As String)
            _CodigoPostal = value
        End Set
    End Property


    Private _colonia As String
    <Xml.Serialization.XmlAttribute("colonia")> _
    Public Property colonia() As String
        Get
            Return _colonia
        End Get
        Set(ByVal value As String)
            _colonia = value
        End Set
    End Property

    Private _estado As String
    <Xml.Serialization.XmlAttribute("estado")> _
    Public Property estado() As String
        Get
            Return _estado
        End Get
        Set(ByVal value As String)
            _estado = value
        End Set
    End Property

    Private _localidad As String
    <Xml.Serialization.XmlAttribute("localidad")> _
    Public Property localidad() As String
        Get
            Return _localidad
        End Get
        Set(ByVal value As String)
            _localidad = value
        End Set
    End Property

    Private _municipio As String
    <Xml.Serialization.XmlAttribute("municipio")> _
    Public Property municipio() As String
        Get
            Return _municipio
        End Get
        Set(ByVal value As String)
            _municipio = value
        End Set
    End Property

    Private _noExterior As String

    <Xml.Serialization.XmlAttribute("noExterior")> _
    Public Property noexterior() As String
        Get
            Return _noExterior
        End Get
        Set(ByVal value As String)
            _noExterior = value
        End Set
    End Property

    Private _noInterior As String
    <Xml.Serialization.XmlAttribute("noInterior")> _
    Public Property nointerior() As String
        Get
            Return _noInterior
        End Get
        Set(ByVal value As String)
            _noInterior = value
        End Set
    End Property

    Private _pais As String
    <Xml.Serialization.XmlAttribute("pais")> _
    Public Property pais() As String
        Get
            Return _pais
        End Get
        Set(ByVal value As String)
            _pais = value
        End Set
    End Property
End Class

Public Class Concepto

    Private _cantidad As Integer
    <Xml.Serialization.XmlAttribute("cantidad")> _
    Public Property cantidad() As Integer
        Get
            Return _cantidad
        End Get
        Set(ByVal value As Integer)
            _cantidad = value
        End Set
    End Property

    Private _unidad As String = "NO APLICA"
    <Xml.Serialization.XmlAttribute("unidad")> _
    Public Property unidad() As String
        Get
            Return _unidad
        End Get
        Set(ByVal value As String)
            _unidad = value
        End Set
    End Property
    Private _descripcion As String
    <Xml.Serialization.XmlAttribute("descripcion")> _
    Public Property descripcion() As String
        Get
            Return _descripcion.Trim()
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

    Private _noIdentificacion As String
    <Xml.Serialization.XmlAttribute("noIdentificacion")> _
    Public Property noIdentificacion() As String
        Get
            Return _noIdentificacion
        End Get
        Set(ByVal value As String)
            _noIdentificacion = value
        End Set
    End Property

    Private _importe As Decimal
    <Xml.Serialization.XmlAttribute("importe")> _
    Public Property importe() As Decimal
        Get
            Return _importe
        End Get
        Set(ByVal value As Decimal)
            _importe = Math.Round(value, 2)
        End Set
    End Property


    Private _valorUnitario As Decimal
    <Xml.Serialization.XmlAttribute("valorUnitario")> _
    Public Property valorUnitario() As Decimal
        Get
            Return _valorUnitario
        End Get
        Set(ByVal value As Decimal)
            _valorUnitario = Math.Round(value, 2)
        End Set
    End Property
End Class

Public Class Traslado
    Private _Impuesto As String
    <Xml.Serialization.XmlAttribute("impuesto")> _
    Public Property Impuesto() As String
        Get
            Return _Impuesto
        End Get
        Set(ByVal value As String)
            _Impuesto = value
        End Set
    End Property

    Private _Tasa As Decimal
    <Xml.Serialization.XmlAttribute("tasa")> _
    Public Property Tasa() As Decimal
        Get
            Return _Tasa
        End Get
        Set(ByVal value As Decimal)
            _Tasa = Math.Round(value, 2)
        End Set
    End Property

    Private _Importe As Decimal
    <Xml.Serialization.XmlAttribute("importe")> _
    Public Property Importe() As Decimal
        Get
            Return _Importe
        End Get
        Set(ByVal value As Decimal)
            _Importe = Math.Round(value, 2)
        End Set
    End Property

End Class

Public Class cImpuestos
    Private _Traslados As New List(Of Traslado)
    Public Property Traslados() As List(Of Traslado)
        Get
            Return _Traslados
        End Get
        Set(ByVal value As List(Of Traslado))
            _Traslados = value
        End Set
    End Property

End Class

Public Class cRegimen
    <Xml.Serialization.XmlAttribute("Regimen")> _
    Public Property Regimen As String

End Class