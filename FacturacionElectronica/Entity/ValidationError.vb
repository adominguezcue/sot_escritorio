﻿Namespace Entity
    Public Class ValidationError
        Public Property RFC As String
        Public Property Number As Int32
        Public Property Description As String
        Public Property Node As String
        Public Property Antecedent As String
        Public Property Suggestion As String
        Public Property EventID As String
    End Class
End Namespace
