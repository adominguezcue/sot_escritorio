﻿Namespace Entity
    Public Class ServiceError
        Public Property RFC As String
        Public Property Number As Int32
        Public Property Description As String
        Public Property EventID As String

    End Class

End Namespace
