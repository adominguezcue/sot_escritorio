﻿Imports System.Text
Imports System.Security.Cryptography

Class Security
    Public Shared Function Hash(ByVal ToHash As String) As String
        ' Use UTF8 encoder
        Dim enc As Encoder = System.Text.Encoding.UTF8.GetEncoder()

        ' Create a buffer and convert
        Dim data As Byte() = New Byte(ToHash.Length - 1) {}
        enc.GetBytes(ToHash.ToCharArray(), 0, ToHash.Length, data, 0, True)

        ' Implementation the SHA1 compute
        Using sha1 As SHA1 = New SHA1CryptoServiceProvider()
            Dim result As Byte() = sha1.ComputeHash(data)
            Return BitConverter.ToString(result).Replace("-", "").ToLower()
        End Using
    End Function

End Class
