﻿using Datos.Nucleo.Repositorios;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Seguridad.Repositorios
{
    public class RepositorioCredenciales : Repositorio<Credencial>, IRepositorioCredenciales
    {
        public RepositorioCredenciales() : base(new Contextos.SeguridadContext()) { }

        public List<Credencial> ObtenerCredencialesUsuario(int idUsuario)
        {
            return (from credencial in contexto.Set<Credencial>()
                    where credencial.IdUsuario == idUsuario
                    select credencial).ToList();
        }
    }
}
