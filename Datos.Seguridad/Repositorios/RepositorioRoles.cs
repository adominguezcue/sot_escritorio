﻿using Datos.Nucleo.Repositorios;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Seguridad.Repositorios
{
    public class RepositorioRoles : Repositorio<Rol>, IRepositorioRoles
    {
        public RepositorioRoles() : base(new Contextos.SeguridadContext()) { }

        public Rol ObtenerRolConPermisos(int idRol)
        {
            return (from rol in contexto.Set<Rol>()
                    where rol.Activo
                    && rol.Id == idRol
                    select new
                    {
                        rol,
                        //permisos = rol.Permisos,
                        permisosPagos = rol.PermisosPagos.Where(m => m.Activo)
                    }).ToList().Select(m => m.rol).FirstOrDefault();
        }
    }
}
