﻿using Datos.Nucleo.Repositorios;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Seguridad.Repositorios
{
    public class RepositorioSesiones : Repositorio<Sesion>, IRepositorioSesiones
    {
        public RepositorioSesiones() : base(new Contextos.SeguridadContext()) { }
    }
}
