﻿using Datos.Nucleo.Repositorios;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Seguridad.Repositorios
{
    public class RepositorioUsuarios : Repositorio<Usuario>, IRepositorioUsuarios
    {
        public RepositorioUsuarios() : base(new Contextos.SeguridadContext()) { }

        public Usuario ObtenerUsuarioConRolCredencial(string alias, Credencial.TiposCredencial tiposCredencial)
        {
            var aliasT = alias.Trim().ToUpper();
            int idTipo = (int)tiposCredencial;

            return (from usuario in contexto.Set<Usuario>()
                    where usuario.Activo && usuario.Alias.Trim().ToUpper().Equals(aliasT)
                    select new
                    {
                        usuario,
                        //rol = usuario.Rol,
                        //permisos = usuario.Rol.Permisos,
                        //permisosPagos = usuario.Rol.PermisosPagos.Where(m => m.Activo),
                        credenciales = usuario.Credenciales.Where(m => m.IdTipoCredencial == idTipo)
                    }).ToList().Select(m => m.usuario).FirstOrDefault();
        }

        public Usuario ObtenerUsuarioConRol(int idUsuario)
        {
            return (from usuario in contexto.Set<Usuario>()
                    where usuario.Activo && usuario.Id == idUsuario
                    select new
                    {
                        usuario,
                        //rol = usuario.Rol,
                        //permisos = usuario.Rol.Permisos,
                        //permisosPagos = usuario.Rol.PermisosPagos.Where(m => m.Activo),
                    }).ToList().Select(m => m.usuario).FirstOrDefault();
        }

        public Usuario ObtenerUsuarioConRolCredencial(int idEmpleado, Credencial.TiposCredencial tiposCredencial)
        {
            //var aliasT = alias.Trim().ToUpper();
            int idTipo = (int)tiposCredencial;

            return (from usuario in contexto.Set<Usuario>()
                    where usuario.Activo && usuario.IdEmpleado == idEmpleado//usuario.Alias.Trim().ToUpper().Equals(alias)
                    select new
                    {
                        usuario,
                        //rol = usuario.Rol,
                        credenciales = usuario.Credenciales.Where(m => m.IdTipoCredencial == idTipo)
                    }).ToList().Select(m => m.usuario).FirstOrDefault();
        }


        public List<Credencial> ObtenerCredencialesActivas(Credencial.TiposCredencial tiposCredencial)
        {
            var idTipo = (int)tiposCredencial;

            return contexto.Set<Credencial>().Where(m => m.IdTipoCredencial == idTipo).ToList();
        }
    }
}
