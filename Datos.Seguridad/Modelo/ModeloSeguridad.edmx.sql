
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/21/2017 10:18:13
-- Generated from EDMX file: C:\Users\developer\Source\Trabajo\Git\EngraneDigital\sot_global\Datos.Seguridad\Modelo\ModeloSeguridad.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SOT];
GO
IF SCHEMA_ID(N'Seguridad') IS NULL EXECUTE(N'CREATE SCHEMA [Seguridad]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[Seguridad].[FK_UserSession]', 'F') IS NOT NULL
    ALTER TABLE [Seguridad].[Sesiones] DROP CONSTRAINT [FK_UserSession];
GO
IF OBJECT_ID(N'[Seguridad].[FK_UsuarioCredencial]', 'F') IS NOT NULL
    ALTER TABLE [Seguridad].[Credenciales] DROP CONSTRAINT [FK_UsuarioCredencial];
GO
IF OBJECT_ID(N'[Seguridad].[FK_RolPermisoPago]', 'F') IS NOT NULL
    ALTER TABLE [Seguridad].[PermisosPagos] DROP CONSTRAINT [FK_RolPermisoPago];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[Seguridad].[Sesiones]', 'U') IS NOT NULL
    DROP TABLE [Seguridad].[Sesiones];
GO
IF OBJECT_ID(N'[Seguridad].[Usuarios]', 'U') IS NOT NULL
    DROP TABLE [Seguridad].[Usuarios];
GO
IF OBJECT_ID(N'[Seguridad].[Roles]', 'U') IS NOT NULL
    DROP TABLE [Seguridad].[Roles];
GO
IF OBJECT_ID(N'[Seguridad].[Credenciales]', 'U') IS NOT NULL
    DROP TABLE [Seguridad].[Credenciales];
GO
IF OBJECT_ID(N'[Seguridad].[PermisosPagos]', 'U') IS NOT NULL
    DROP TABLE [Seguridad].[PermisosPagos];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Sesiones'
CREATE TABLE [Seguridad].[Sesiones] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Token] nvarchar(max)  NOT NULL,
    [IdUsuario] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [Activo] bit  NOT NULL
);
GO

-- Creating table 'Usuarios'
CREATE TABLE [Seguridad].[Usuarios] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [Alias] nvarchar(max)  NOT NULL,
    [IdEmpleado] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [Seguridad].[Roles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Activo] bit  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL,
    [Permisos] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Credenciales'
CREATE TABLE [Seguridad].[Credenciales] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdUsuario] int  NOT NULL,
    [IdTipoCredencial] int  NOT NULL,
    [Valor] varbinary(max)  NOT NULL,
    [EsAcceso] bit  NOT NULL
);
GO

-- Creating table 'PermisosPagos'
CREATE TABLE [Seguridad].[PermisosPagos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Activo] bit  NOT NULL,
    [IdFormaPago] int  NOT NULL,
    [IdRol] int  NOT NULL,
    [FechaCreacion] datetime  NOT NULL,
    [FechaModificacion] datetime  NOT NULL,
    [FechaEliminacion] datetime  NULL,
    [IdUsuarioCreo] int  NOT NULL,
    [IdUsuarioModifico] int  NOT NULL,
    [IdUsuarioElimino] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Sesiones'
ALTER TABLE [Seguridad].[Sesiones]
ADD CONSTRAINT [PK_Sesiones]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Usuarios'
ALTER TABLE [Seguridad].[Usuarios]
ADD CONSTRAINT [PK_Usuarios]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Roles'
ALTER TABLE [Seguridad].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Credenciales'
ALTER TABLE [Seguridad].[Credenciales]
ADD CONSTRAINT [PK_Credenciales]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PermisosPagos'
ALTER TABLE [Seguridad].[PermisosPagos]
ADD CONSTRAINT [PK_PermisosPagos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [IdUsuario] in table 'Sesiones'
ALTER TABLE [Seguridad].[Sesiones]
ADD CONSTRAINT [FK_UserSession]
    FOREIGN KEY ([IdUsuario])
    REFERENCES [Seguridad].[Usuarios]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserSession'
CREATE INDEX [IX_FK_UserSession]
ON [Seguridad].[Sesiones]
    ([IdUsuario]);
GO

-- Creating foreign key on [IdUsuario] in table 'Credenciales'
ALTER TABLE [Seguridad].[Credenciales]
ADD CONSTRAINT [FK_UsuarioCredencial]
    FOREIGN KEY ([IdUsuario])
    REFERENCES [Seguridad].[Usuarios]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioCredencial'
CREATE INDEX [IX_FK_UsuarioCredencial]
ON [Seguridad].[Credenciales]
    ([IdUsuario]);
GO

-- Creating foreign key on [IdRol] in table 'PermisosPagos'
ALTER TABLE [Seguridad].[PermisosPagos]
ADD CONSTRAINT [FK_RolPermisoPago]
    FOREIGN KEY ([IdRol])
    REFERENCES [Seguridad].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RolPermisoPago'
CREATE INDEX [IX_FK_RolPermisoPago]
ON [Seguridad].[PermisosPagos]
    ([IdRol]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------