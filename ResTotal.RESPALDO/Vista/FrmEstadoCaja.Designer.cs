﻿namespace ResTotal.Vista
{
    partial class FrmEstadoCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lexedente = new System.Windows.Forms.Label();
            this.Ltotal = new System.Windows.Forms.Label();
            this.Lminimo = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Bcerrar = new System.Windows.Forms.Button();
            this.Lmaximo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Lretiros = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Lexedente
            // 
            this.Lexedente.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lexedente.ForeColor = System.Drawing.Color.Navy;
            this.Lexedente.Location = new System.Drawing.Point(299, 169);
            this.Lexedente.Name = "Lexedente";
            this.Lexedente.Size = new System.Drawing.Size(141, 24);
            this.Lexedente.TabIndex = 16;
            this.Lexedente.Text = "$0.00";
            this.Lexedente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Ltotal
            // 
            this.Ltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ltotal.ForeColor = System.Drawing.Color.Navy;
            this.Ltotal.Location = new System.Drawing.Point(299, 127);
            this.Ltotal.Name = "Ltotal";
            this.Ltotal.Size = new System.Drawing.Size(141, 24);
            this.Ltotal.TabIndex = 15;
            this.Ltotal.Text = "$0.00";
            this.Ltotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lminimo
            // 
            this.Lminimo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lminimo.ForeColor = System.Drawing.Color.Navy;
            this.Lminimo.Location = new System.Drawing.Point(299, 23);
            this.Lminimo.Name = "Lminimo";
            this.Lminimo.Size = new System.Drawing.Size(141, 24);
            this.Lminimo.TabIndex = 14;
            this.Lminimo.Text = "$0.00";
            this.Lminimo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(14, 99);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(161, 52);
            this.label14.TabIndex = 13;
            this.label14.Text = "Total en caja\r\npagos efectivo:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(14, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(179, 27);
            this.label13.TabIndex = 12;
            this.label13.Text = "Minimo en caja:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(14, 167);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(161, 27);
            this.label12.TabIndex = 11;
            this.label12.Text = "Exedente:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(14, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(216, 49);
            this.label2.TabIndex = 17;
            this.label2.Text = "Total Retiros sin \r\ncerrar:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Bcerrar
            // 
            this.Bcerrar.Location = new System.Drawing.Point(90, 276);
            this.Bcerrar.Name = "Bcerrar";
            this.Bcerrar.Size = new System.Drawing.Size(268, 23);
            this.Bcerrar.TabIndex = 19;
            this.Bcerrar.Text = "Cerrar";
            this.Bcerrar.UseVisualStyleBackColor = true;
            this.Bcerrar.Click += new System.EventHandler(this.Bcerrar_Click);
            // 
            // Lmaximo
            // 
            this.Lmaximo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lmaximo.ForeColor = System.Drawing.Color.Navy;
            this.Lmaximo.Location = new System.Drawing.Point(299, 65);
            this.Lmaximo.Name = "Lmaximo";
            this.Lmaximo.Size = new System.Drawing.Size(141, 24);
            this.Lmaximo.TabIndex = 21;
            this.Lmaximo.Text = "$0.00";
            this.Lmaximo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(14, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 27);
            this.label3.TabIndex = 20;
            this.label3.Text = "Maximo en caja:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lretiros
            // 
            this.Lretiros.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lretiros.ForeColor = System.Drawing.Color.Navy;
            this.Lretiros.Location = new System.Drawing.Point(299, 235);
            this.Lretiros.Name = "Lretiros";
            this.Lretiros.Size = new System.Drawing.Size(141, 24);
            this.Lretiros.TabIndex = 22;
            this.Lretiros.Text = "$0.00";
            this.Lretiros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmEstadoCaja
            // 
            this.AcceptButton = this.Bcerrar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 311);
            this.Controls.Add(this.Lretiros);
            this.Controls.Add(this.Lmaximo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Bcerrar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Lexedente);
            this.Controls.Add(this.Ltotal);
            this.Controls.Add(this.Lminimo);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEstadoCaja";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estado Actual Caja";
            this.Load += new System.EventHandler(this.FrmRetiroCaja_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Lexedente;
        private System.Windows.Forms.Label Ltotal;
        private System.Windows.Forms.Label Lminimo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Bcerrar;
        private System.Windows.Forms.Label Lmaximo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Lretiros;

    }
}