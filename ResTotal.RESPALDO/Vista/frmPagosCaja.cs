﻿using System;   
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ResTotal.Vista
{
    public partial class frmPagosCaja : Form
    {
        Modelo.ZctEncCaja _caja;
        Modelo.ZctPagoCaja _pago;
        public frmPagosCaja( Modelo.ZctEncCaja caja , int codtipo_pago, bool limpia, bool monto_enable)
        {
            InitializeComponent();
            _caja = caja;
            if (limpia)
                _caja.ZctPagoCaja.Clear();
            _pago = new Modelo.ZctPagoCaja();
            _pago.cod_folio = _caja.cod_folio;
            _pago.CodEnc_Caja = _caja.CodEnc_Caja;
            _pago.codtipo_pago = codtipo_pago;
            _pago.corte = false;
            if (_pago.codtipo_pago == 2)
            {
                _pago.monto = caja.PorPagar;
                montoTextBox.Enabled = monto_enable;
                cuentaTextBox.Enabled = monto_enable;
                this.Text = "PAGO CON TC";
            }
            else
            {
                _pago.monto = 0;
                this.Text = "PAGO EFECTIVO";
            }
            //_pago.MontoIngresado = caja.Total;
            _pago.fecha_pago = DateTime.Now;
            _caja.ZctPagoCaja.Add(_pago);
            zctPagoCajaBindingSource.DataSource = _pago;

        }

        private void frmPagosCaja_Load(object sender, EventArgs e)
        {
            referenciaTextBox.Enabled = (_pago.codtipo_pago == 2);
            lblReferencia.Enabled = (_pago.codtipo_pago == 2);
            cuentaTextBox.Enabled = (_pago.codtipo_pago == 2);

       }

        private bool Valida() {
            // Esto no se hace
         

            if (_pago.monto <= 0)
            {
                MessageBox.Show("Debe ingresar un monto de pago.");
                return false;
            }
            if (_pago.codtipo_pago == 2 && (referenciaTextBox.Text == string.Empty))
            {
                MessageBox.Show("Debe de ingresar la referencia del pago.");
                return false;
            }
            if (montoTextBox.Text.Length == 0)
            {
                MessageBox.Show("Debe de ingresar el monto del pago.");
                return false;
            }
            decimal v;
            if (!Decimal.TryParse(montoTextBox.Text, out v))
            {
                MessageBox.Show("El monto debe de ser numérico.");
                return false;
            }
            if (_pago.codtipo_pago == 2 && (cuentaTextBox.Text == string.Empty))
            {
                MessageBox.Show("Debe de ingresar los últimos 4 dígitos de la tarjeta.");
                return false;
            }
            return true;
        }
        
        private void Bgrabar_Click(object sender, EventArgs e)
        {
            if (Valida())
            {

                if (_caja.TotalPago > _caja.Total)
                {
                    _pago.cambio = _caja.TotalPago - _caja.Total;
                    _pago.MontoIngresado = _caja.TotalPago - _pago.cambio.GetValueOrDefault();
                }
                else
                {
                    _pago.cambio = 0;
                    _pago.MontoIngresado = _caja.TotalPago;
                }
                _pago.fecha_pago = DateTime.Now;

                //if (_TotalPago > _TotalDet)
                //{
                //    DGVpagos.Rows[e.RowIndex].Cells["cambioDataGridViewTextBoxColumn"].Value = Decimal.Round(_TotalPago - _TotalDet, 2);
                //    DGVpagos.Rows[e.RowIndex].Cells["ColMontoReal"].Value = Decimal.Round(Convert.ToDecimal(Convert.ToDecimal(DGVpagos.Rows[e.RowIndex].Cells["montoDataGridViewTextBoxColumn"].Value)), 2) - Decimal.Round(Convert.ToDecimal(Convert.ToDecimal(DGVpagos.Rows[e.RowIndex].Cells["cambioDataGridViewTextBoxColumn"].Value)), 2);
                //}
                //else
                //{
                //    DGVpagos.Rows[e.RowIndex].Cells["cambioDataGridViewTextBoxColumn"].Value = Decimal.Round(0, 2);
                //    DGVpagos.Rows[e.RowIndex].Cells["ColMontoReal"].Value = Decimal.Round(Convert.ToDecimal(Convert.ToDecimal(DGVpagos.Rows[e.RowIndex].Cells["montoDataGridViewTextBoxColumn"].Value)), 2);
                //}
                this.DialogResult = DialogResult.OK;


           // }
           // else {
           //     return;
            }
        }

        private void Blimpiar_Click(object sender, EventArgs e)
        {
            _caja.ZctPagoCaja.Clear();
            this.Close();
        }

        private void referenciaTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (_pago.codtipo_pago == 2 && (referenciaTextBox.Text == string.Empty))
            {
                MessageBox.Show("Debe de ingresar la referencia del pago.");
                e.Cancel = true;
            }
        }

        private void montoTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (montoTextBox.Text.Length == 0)
            {
                MessageBox.Show("Debe de ingresar el monto del pago.");
                e.Cancel = true;
            }
            decimal v;
            if (!Decimal.TryParse(montoTextBox.Text, out v)) { 
                MessageBox.Show("El monto debe de ser numérico.");
                e.Cancel = true;
            }
        }

        private void cuentaTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (_pago.codtipo_pago == 2 && (cuentaTextBox.Text == string.Empty))
            {
                MessageBox.Show("Debe de ingresar los últimos 4 dígitos de la tarjeta.");
                e.Cancel = true;
            }
        }

        private void frmPagosCaja_Validating(object sender, CancelEventArgs e)
        {
            
        }

     
        

    }
}
