﻿using System.Windows.Forms;
namespace ResTotal.Vista
{
    partial class ZctCatProv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cod_ProvLabel;
            System.Windows.Forms.Label nom_ProvLabel;
            System.Windows.Forms.Label calleLabel;
            System.Windows.Forms.Label coloniaLabel;
            System.Windows.Forms.Label numextLabel;
            System.Windows.Forms.Label numintLabel;
            System.Windows.Forms.Label cpLabel;
            System.Windows.Forms.Label telefonoLabel;
            System.Windows.Forms.Label rFC_ProvLabel;
            System.Windows.Forms.Label emailLabel;
            System.Windows.Forms.Label ciudadLabel1;
            System.Windows.Forms.Label estadoLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lpagos;
            System.Windows.Forms.Label label3;
            this.ZctCatProvBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cod_ProvTextBox = new System.Windows.Forms.TextBox();
            this.nom_ProvTextBox = new System.Windows.Forms.TextBox();
            this.calleTextBox = new System.Windows.Forms.TextBox();
            this.coloniaTextBox = new System.Windows.Forms.TextBox();
            this.numextTextBox = new System.Windows.Forms.TextBox();
            this.TxtFrecPagos = new System.Windows.Forms.TextBox();
            this.cpTextBox = new System.Windows.Forms.TextBox();
            this.telefonoTextBox = new System.Windows.Forms.TextBox();
            this.rFC_ProvTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.Beliminar = new System.Windows.Forms.Button();
            this.Bguardar = new System.Windows.Forms.Button();
            this.Bcancelar = new System.Windows.Forms.Button();
            this.Bsig = new System.Windows.Forms.Button();
            this.Bant = new System.Windows.Forms.Button();
            this.ciudadComboBox = new System.Windows.Forms.ComboBox();
            this.BsCiudad = new System.Windows.Forms.BindingSource(this.components);
            this.estadoComboBox = new System.Windows.Forms.ComboBox();
            this.BSestado = new System.Windows.Forms.BindingSource(this.components);
            this.TxtNumPagos = new System.Windows.Forms.TextBox();
            this.TxtNumInt = new System.Windows.Forms.TextBox();
            this.TxtdiasCred = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            cod_ProvLabel = new System.Windows.Forms.Label();
            nom_ProvLabel = new System.Windows.Forms.Label();
            calleLabel = new System.Windows.Forms.Label();
            coloniaLabel = new System.Windows.Forms.Label();
            numextLabel = new System.Windows.Forms.Label();
            numintLabel = new System.Windows.Forms.Label();
            cpLabel = new System.Windows.Forms.Label();
            telefonoLabel = new System.Windows.Forms.Label();
            rFC_ProvLabel = new System.Windows.Forms.Label();
            emailLabel = new System.Windows.Forms.Label();
            ciudadLabel1 = new System.Windows.Forms.Label();
            estadoLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            lpagos = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ZctCatProvBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsCiudad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSestado)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cod_ProvLabel
            // 
            cod_ProvLabel.AutoSize = true;
            cod_ProvLabel.Location = new System.Drawing.Point(12, 18);
            cod_ProvLabel.Name = "cod_ProvLabel";
            cod_ProvLabel.Size = new System.Drawing.Size(43, 13);
            cod_ProvLabel.TabIndex = 0;
            cod_ProvLabel.Text = "Codigo:";
            // 
            // nom_ProvLabel
            // 
            nom_ProvLabel.AutoSize = true;
            nom_ProvLabel.Location = new System.Drawing.Point(44, 22);
            nom_ProvLabel.Name = "nom_ProvLabel";
            nom_ProvLabel.Size = new System.Drawing.Size(47, 13);
            nom_ProvLabel.TabIndex = 0;
            nom_ProvLabel.Text = "Nombre:";
            // 
            // calleLabel
            // 
            calleLabel.AutoSize = true;
            calleLabel.Location = new System.Drawing.Point(56, 78);
            calleLabel.Name = "calleLabel";
            calleLabel.Size = new System.Drawing.Size(33, 13);
            calleLabel.TabIndex = 6;
            calleLabel.Text = "Calle:";
            // 
            // coloniaLabel
            // 
            coloniaLabel.AutoSize = true;
            coloniaLabel.Location = new System.Drawing.Point(431, 78);
            coloniaLabel.Name = "coloniaLabel";
            coloniaLabel.Size = new System.Drawing.Size(45, 13);
            coloniaLabel.TabIndex = 8;
            coloniaLabel.Text = "Colonia:";
            // 
            // numextLabel
            // 
            numextLabel.AutoSize = true;
            numextLabel.Location = new System.Drawing.Point(5, 130);
            numextLabel.Name = "numextLabel";
            numextLabel.Size = new System.Drawing.Size(84, 13);
            numextLabel.TabIndex = 14;
            numextLabel.Text = "Número exterior:";
            // 
            // numintLabel
            // 
            numintLabel.AutoSize = true;
            numintLabel.Location = new System.Drawing.Point(395, 130);
            numintLabel.Name = "numintLabel";
            numintLabel.Size = new System.Drawing.Size(81, 13);
            numintLabel.TabIndex = 16;
            numintLabel.Text = "Número interior:";
            // 
            // cpLabel
            // 
            cpLabel.AutoSize = true;
            cpLabel.Location = new System.Drawing.Point(65, 104);
            cpLabel.Name = "cpLabel";
            cpLabel.Size = new System.Drawing.Size(24, 13);
            cpLabel.TabIndex = 10;
            cpLabel.Text = "CP:";
            // 
            // telefonoLabel
            // 
            telefonoLabel.AutoSize = true;
            telefonoLabel.Location = new System.Drawing.Point(424, 104);
            telefonoLabel.Name = "telefonoLabel";
            telefonoLabel.Size = new System.Drawing.Size(52, 13);
            telefonoLabel.TabIndex = 12;
            telefonoLabel.Text = "Teléfono:";
            // 
            // rFC_ProvLabel
            // 
            rFC_ProvLabel.AutoSize = true;
            rFC_ProvLabel.Location = new System.Drawing.Point(58, 48);
            rFC_ProvLabel.Name = "rFC_ProvLabel";
            rFC_ProvLabel.Size = new System.Drawing.Size(31, 13);
            rFC_ProvLabel.TabIndex = 2;
            rFC_ProvLabel.Text = "RFC:";
            // 
            // emailLabel
            // 
            emailLabel.AutoSize = true;
            emailLabel.Location = new System.Drawing.Point(379, 48);
            emailLabel.Name = "emailLabel";
            emailLabel.Size = new System.Drawing.Size(97, 13);
            emailLabel.TabIndex = 4;
            emailLabel.Text = "Correo Electrónico:";
            // 
            // ciudadLabel1
            // 
            ciudadLabel1.AutoSize = true;
            ciudadLabel1.Location = new System.Drawing.Point(433, 155);
            ciudadLabel1.Name = "ciudadLabel1";
            ciudadLabel1.Size = new System.Drawing.Size(43, 13);
            ciudadLabel1.TabIndex = 20;
            ciudadLabel1.Text = "Ciudad:";
            // 
            // estadoLabel
            // 
            estadoLabel.AutoSize = true;
            estadoLabel.Location = new System.Drawing.Point(46, 156);
            estadoLabel.Name = "estadoLabel";
            estadoLabel.Size = new System.Drawing.Size(43, 13);
            estadoLabel.TabIndex = 18;
            estadoLabel.Text = "Estado:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(6, 183);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(83, 13);
            label1.TabIndex = 22;
            label1.Text = "Días de crédito:";
            // 
            // lpagos
            // 
            lpagos.AutoSize = true;
            lpagos.Location = new System.Drawing.Point(238, 183);
            lpagos.Name = "lpagos";
            lpagos.Size = new System.Drawing.Size(94, 13);
            lpagos.TabIndex = 24;
            lpagos.Text = "Número de pagos:";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(487, 183);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(143, 13);
            label3.TabIndex = 26;
            label3.Text = "Frecuencia de  pagos (días):";
            // 
            // ZctCatProvBindingSource
            // 
            this.ZctCatProvBindingSource.DataSource = typeof(ResTotal.Modelo.ZctCatProv);
            this.ZctCatProvBindingSource.Filter = "";
            // 
            // cod_ProvTextBox
            // 
            this.cod_ProvTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "Cod_Prov", true));
            this.cod_ProvTextBox.Location = new System.Drawing.Point(55, 15);
            this.cod_ProvTextBox.Name = "cod_ProvTextBox";
            this.cod_ProvTextBox.Size = new System.Drawing.Size(68, 20);
            this.cod_ProvTextBox.TabIndex = 1;
            this.cod_ProvTextBox.TextChanged += new System.EventHandler(this.cod_ProvTextBox_TextChanged);
            this.cod_ProvTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(cod_ProvTextBox_KeyDown);
            // 
            // nom_ProvTextBox
            // 
            this.nom_ProvTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nom_ProvTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "Nom_Prov", true));
            this.nom_ProvTextBox.Location = new System.Drawing.Point(98, 19);
            this.nom_ProvTextBox.Name = "nom_ProvTextBox";
            this.nom_ProvTextBox.Size = new System.Drawing.Size(616, 20);
            this.nom_ProvTextBox.TabIndex = 1;
            // 
            // calleTextBox
            // 
            this.calleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "calle", true));
            this.calleTextBox.Location = new System.Drawing.Point(98, 74);
            this.calleTextBox.Name = "calleTextBox";
            this.calleTextBox.Size = new System.Drawing.Size(214, 20);
            this.calleTextBox.TabIndex = 7;
            // 
            // coloniaTextBox
            // 
            this.coloniaTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.coloniaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "colonia", true));
            this.coloniaTextBox.Location = new System.Drawing.Point(482, 74);
            this.coloniaTextBox.Name = "coloniaTextBox";
            this.coloniaTextBox.Size = new System.Drawing.Size(232, 20);
            this.coloniaTextBox.TabIndex = 9;
            // 
            // numextTextBox
            // 
            this.numextTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "numext", true));
            this.numextTextBox.Location = new System.Drawing.Point(98, 126);
            this.numextTextBox.Name = "numextTextBox";
            this.numextTextBox.Size = new System.Drawing.Size(96, 20);
            this.numextTextBox.TabIndex = 15;
            // 
            // TxtFrecPagos
            // 
            this.TxtFrecPagos.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "frecuencia_pagos", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "1"));
            this.TxtFrecPagos.Location = new System.Drawing.Point(631, 179);
            this.TxtFrecPagos.Name = "TxtFrecPagos";
            this.TxtFrecPagos.Size = new System.Drawing.Size(83, 20);
            this.TxtFrecPagos.TabIndex = 27;
            // 
            // cpTextBox
            // 
            this.cpTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cpTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "cp", true));
            this.cpTextBox.Location = new System.Drawing.Point(98, 100);
            this.cpTextBox.Name = "cpTextBox";
            this.cpTextBox.Size = new System.Drawing.Size(96, 20);
            this.cpTextBox.TabIndex = 11;
            // 
            // telefonoTextBox
            // 
            this.telefonoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "telefono", true));
            this.telefonoTextBox.Location = new System.Drawing.Point(482, 100);
            this.telefonoTextBox.Name = "telefonoTextBox";
            this.telefonoTextBox.Size = new System.Drawing.Size(95, 20);
            this.telefonoTextBox.TabIndex = 13;
            // 
            // rFC_ProvTextBox
            // 
            this.rFC_ProvTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "RFC_Prov", true));
            this.rFC_ProvTextBox.Location = new System.Drawing.Point(98, 45);
            this.rFC_ProvTextBox.Name = "rFC_ProvTextBox";
            this.rFC_ProvTextBox.Size = new System.Drawing.Size(214, 20);
            this.rFC_ProvTextBox.TabIndex = 3;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.emailTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "email", true));
            this.emailTextBox.Location = new System.Drawing.Point(482, 45);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(232, 20);
            this.emailTextBox.TabIndex = 5;
            // 
            // Beliminar
            // 
            this.Beliminar.Location = new System.Drawing.Point(483, 31);
            this.Beliminar.Name = "Beliminar";
            this.Beliminar.Size = new System.Drawing.Size(75, 23);
            this.Beliminar.TabIndex = 0;
            this.Beliminar.Text = "Eliminar";
            this.Beliminar.UseVisualStyleBackColor = true;
            this.Beliminar.Click += new System.EventHandler(this.Beliminar_Click);
            // 
            // Bguardar
            // 
            this.Bguardar.Location = new System.Drawing.Point(569, 31);
            this.Bguardar.Name = "Bguardar";
            this.Bguardar.Size = new System.Drawing.Size(75, 23);
            this.Bguardar.TabIndex = 1;
            this.Bguardar.Text = "Guardar";
            this.Bguardar.UseVisualStyleBackColor = true;
            this.Bguardar.Click += new System.EventHandler(this.Bguardar_Click);
            // 
            // Bcancelar
            // 
            this.Bcancelar.Location = new System.Drawing.Point(651, 31);
            this.Bcancelar.Name = "Bcancelar";
            this.Bcancelar.Size = new System.Drawing.Size(75, 23);
            this.Bcancelar.TabIndex = 2;
            this.Bcancelar.Text = "Cancelar";
            this.Bcancelar.UseVisualStyleBackColor = true;
            this.Bcancelar.Click += new System.EventHandler(this.Bcancelar_Click);
            // 
            // Bsig
            // 
            this.Bsig.Location = new System.Drawing.Point(146, 15);
            this.Bsig.Name = "Bsig";
            this.Bsig.Size = new System.Drawing.Size(20, 21);
            this.Bsig.TabIndex = 3;
            this.Bsig.Text = ">";
            this.Bsig.UseVisualStyleBackColor = true;
            this.Bsig.Click += new System.EventHandler(this.Bsig_Click);
            // 
            // Bant
            // 
            this.Bant.Location = new System.Drawing.Point(125, 15);
            this.Bant.Name = "Bant";
            this.Bant.Size = new System.Drawing.Size(20, 21);
            this.Bant.TabIndex = 2;
            this.Bant.Text = "<";
            this.Bant.UseVisualStyleBackColor = true;
            this.Bant.Click += new System.EventHandler(this.Bant_Click);
            // 
            // ciudadComboBox
            // 
            this.ciudadComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ciudadComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "ciudad", true));
            this.ciudadComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.ZctCatProvBindingSource, "ciudad", true));
            this.ciudadComboBox.DataSource = this.BsCiudad;
            this.ciudadComboBox.DisplayMember = "Desc_Ciu";
            this.ciudadComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ciudadComboBox.FormattingEnabled = true;
            this.ciudadComboBox.Location = new System.Drawing.Point(482, 152);
            this.ciudadComboBox.Name = "ciudadComboBox";
            this.ciudadComboBox.Size = new System.Drawing.Size(232, 21);
            this.ciudadComboBox.TabIndex = 21;
            this.ciudadComboBox.ValueMember = "Desc_Ciu";
            // 
            // BsCiudad
            // 
            this.BsCiudad.DataSource = typeof(ResTotal.Modelo.ZctCatCiu);
            this.BsCiudad.Filter = "";
            // 
            // estadoComboBox
            // 
            this.estadoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BSestado, "Desc_Edo", true));
            this.estadoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.ZctCatProvBindingSource, "estado", true));
            this.estadoComboBox.DataSource = this.BSestado;
            this.estadoComboBox.DisplayMember = "Desc_Edo";
            this.estadoComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.estadoComboBox.FormattingEnabled = true;
            this.estadoComboBox.Location = new System.Drawing.Point(98, 152);
            this.estadoComboBox.Name = "estadoComboBox";
            this.estadoComboBox.Size = new System.Drawing.Size(214, 21);
            this.estadoComboBox.TabIndex = 19;
            this.estadoComboBox.ValueMember = "Desc_Edo";
            // 
            // BSestado
            // 
            this.BSestado.DataSource = typeof(ResTotal.Modelo.ZctCatEdo);
            this.BSestado.Filter = "";
            // 
            // TxtNumPagos
            // 
            this.TxtNumPagos.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "numero_pagos", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0"));
            this.TxtNumPagos.Location = new System.Drawing.Point(336, 179);
            this.TxtNumPagos.Name = "TxtNumPagos";
            this.TxtNumPagos.Size = new System.Drawing.Size(95, 20);
            this.TxtNumPagos.TabIndex = 25;
            this.TxtNumPagos.TextChanged += new System.EventHandler(this.TxtNumPagos_TextChanged);
            // 
            // TxtNumInt
            // 
            this.TxtNumInt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "numint", true));
            this.TxtNumInt.Location = new System.Drawing.Point(482, 126);
            this.TxtNumInt.Name = "TxtNumInt";
            this.TxtNumInt.Size = new System.Drawing.Size(79, 20);
            this.TxtNumInt.TabIndex = 17;
            // 
            // TxtdiasCred
            // 
            this.TxtdiasCred.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "dias_credito", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0"));
            this.TxtdiasCred.Location = new System.Drawing.Point(98, 179);
            this.TxtdiasCred.Name = "TxtdiasCred";
            this.TxtdiasCred.Size = new System.Drawing.Size(83, 20);
            this.TxtdiasCred.TabIndex = 23;
            this.TxtdiasCred.TextChanged += new System.EventHandler(this.TxtdiasCred_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Beliminar);
            this.groupBox1.Controls.Add(this.Bguardar);
            this.groupBox1.Controls.Add(this.Bcancelar);
            this.groupBox1.Location = new System.Drawing.Point(6, 272);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(746, 65);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TxtdiasCred);
            this.groupBox2.Controls.Add(this.TxtFrecPagos);
            this.groupBox2.Controls.Add(this.TxtNumInt);
            this.groupBox2.Controls.Add(label1);
            this.groupBox2.Controls.Add(nom_ProvLabel);
            this.groupBox2.Controls.Add(emailLabel);
            this.groupBox2.Controls.Add(this.nom_ProvTextBox);
            this.groupBox2.Controls.Add(estadoLabel);
            this.groupBox2.Controls.Add(this.emailTextBox);
            this.groupBox2.Controls.Add(rFC_ProvLabel);
            this.groupBox2.Controls.Add(label3);
            this.groupBox2.Controls.Add(this.rFC_ProvTextBox);
            this.groupBox2.Controls.Add(this.estadoComboBox);
            this.groupBox2.Controls.Add(coloniaLabel);
            this.groupBox2.Controls.Add(this.TxtNumPagos);
            this.groupBox2.Controls.Add(this.coloniaTextBox);
            this.groupBox2.Controls.Add(ciudadLabel1);
            this.groupBox2.Controls.Add(calleLabel);
            this.groupBox2.Controls.Add(lpagos);
            this.groupBox2.Controls.Add(this.calleTextBox);
            this.groupBox2.Controls.Add(this.ciudadComboBox);
            this.groupBox2.Controls.Add(telefonoLabel);
            this.groupBox2.Controls.Add(this.numextTextBox);
            this.groupBox2.Controls.Add(numextLabel);
            this.groupBox2.Controls.Add(numintLabel);
            this.groupBox2.Controls.Add(this.cpTextBox);
            this.groupBox2.Controls.Add(cpLabel);
            this.groupBox2.Controls.Add(this.telefonoTextBox);
            this.groupBox2.Location = new System.Drawing.Point(6, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(737, 225);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // ZctCatProv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 343);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Bsig);
            this.Controls.Add(this.Bant);
            this.Controls.Add(cod_ProvLabel);
            this.Controls.Add(this.cod_ProvTextBox);
            this.Name = "ZctCatProv";
            this.Text = "Catálogo de Proveedores";
            this.Load += new System.EventHandler(this.ZctCatProv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ZctCatProvBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsCiudad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSestado)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        void cod_ProvTextBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                _buscando = true;
                ZctCatProvBindingSource.CancelEdit();
                ControladorProveedores.Cancelar();
               cod_ProvTextBox.Text= BuscaProveedor().ToString();
            }
        }

      
        #endregion

        private System.Windows.Forms.BindingSource ZctCatProvBindingSource;
        private System.Windows.Forms.TextBox cod_ProvTextBox;
        private System.Windows.Forms.TextBox nom_ProvTextBox;
        private System.Windows.Forms.TextBox calleTextBox;
        private System.Windows.Forms.TextBox coloniaTextBox;
        private System.Windows.Forms.TextBox numextTextBox;
        private System.Windows.Forms.TextBox TxtFrecPagos;
        private System.Windows.Forms.TextBox cpTextBox;
        private System.Windows.Forms.TextBox telefonoTextBox;
        private System.Windows.Forms.TextBox rFC_ProvTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Button Beliminar;
        private System.Windows.Forms.Button Bguardar;
        private System.Windows.Forms.Button Bcancelar;
        private System.Windows.Forms.Button Bsig;
        private System.Windows.Forms.Button Bant;
        private System.Windows.Forms.ComboBox ciudadComboBox;
        private System.Windows.Forms.BindingSource BsCiudad;
        private System.Windows.Forms.ComboBox estadoComboBox;
        private System.Windows.Forms.BindingSource BSestado;
        private System.Windows.Forms.TextBox TxtNumPagos;
        private System.Windows.Forms.TextBox TxtNumInt;
        private System.Windows.Forms.TextBox TxtdiasCred;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;

    }
}