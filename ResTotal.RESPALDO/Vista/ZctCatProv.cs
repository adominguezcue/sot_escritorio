﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
namespace ResTotal.Vista
{
    public partial class ZctCatProv : Form
    {
        public delegate int DBuscaProveedor();
        int _user, _pos=0;
        string _cnn="";
        bool _buscando = false;
        Permisos.Controlador.Controlador.vistapermisos _permisos;
        Permisos.Controlador.Controlador _ControladorPermisos;
        string sSqlBusqueda = "select Cod_Prov,Nom_Prov,RFC_Prov from ZctCatProv order by Cod_Prov";
        Modelo.ZctCatProv _NuevoProveedor;
        Controlador.ControladorProveedores ControladorProveedores;
        public ZctCatProv(string cnn, int user)
        {
            InitializeComponent();
            _user = user;
            _cnn = cnn;
            this.estadoComboBox.LostFocus += new EventHandler(estadoComboBox_LostFocus);
            ControladorProveedores = new Controlador.ControladorProveedores(cnn);
            cod_ProvTextBox.LostFocus+=new EventHandler(cod_ProvTextBox_LostFocus);
            TxtNumPagos.TextChanged += new EventHandler(TextoMayorAcero);
            TxtFrecPagos.TextChanged += new EventHandler(TextoMayorAcero);
            cpTextBox.KeyPress += new KeyPressEventHandler(cpTextBox_KeyPress);
        }

        void cpTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != 46))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        public event DBuscaProveedor BuscaProveedor;
       
        void estadoComboBox_LostFocus(object sender, EventArgs e)
        {
            if (estadoComboBox.SelectedValue != null)
            {
                ciudadComboBox.DataSource = ControladorProveedores.TraeCiudades(estadoComboBox.SelectedValue.ToString());
            }
            else
            {
                ciudadComboBox.DataSource = ControladorProveedores.TraeCiudades();
            }

        }
        void cod_ProvTextBox_LostFocus(object sender, EventArgs e)
        {
            if (_buscando==false )
            {
            ZctCatProvBindingSource.CancelEdit();
            ControladorProveedores.Cancelar();
            _NuevoProveedor = null;
            }
            try
            {
                if (cod_ProvTextBox.Text  == "")
                {
                    cod_ProvTextBox.Text = "0";
                }
                int Existe = ControladorProveedores.TraeProveedores(cod_ProvTextBox.Text).Count;
                if (Existe == 0)
                {
                    NuevoProveedor();
                    return;
                }
                int cod = Convert.ToInt32(cod_ProvTextBox.Text);
                _pos = ControladorProveedores.proveedorPos(Convert.ToInt32(cod_ProvTextBox.Text));
                ZctCatProvBindingSource.DataSource = ControladorProveedores.TraeProveedores(cod_ProvTextBox.Text);
                cod_ProvTextBox.Enabled = false;
                _buscando = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void NuevoProveedor()
        {
            cod_ProvTextBox.Enabled = true;
            //ZctCatProvBindingSource.AddNew();
            _pos = ControladorProveedores.TraeProveedores().Count;
            _NuevoProveedor = new Modelo.ZctCatProv();
            ZctCatProvBindingSource.DataSource = _NuevoProveedor;
            cod_ProvTextBox.Text = ControladorProveedores.CodigoProveedor();
            nom_ProvTextBox.Focus();
            Beliminar.Enabled = false;
        }
        private void ZctCatProv_Load(object sender, EventArgs e)
        {
            _ControladorPermisos = new Permisos.Controlador.Controlador(_cnn);
            _permisos = _ControladorPermisos.getpermisoventana(_user, this.Text);
            bool Edita = (_permisos.agrega || _permisos.edita);
            Beliminar.Enabled = _permisos.elimina;
            Bguardar.Enabled = Edita;

            BSestado.DataSource = ControladorProveedores.TraeEstados();
            BsCiudad.DataSource = ControladorProveedores.TraeCiudades();
            if (string.IsNullOrEmpty(cod_ProvTextBox.Text))
            {
                NuevoProveedor();
            }
        }
        private void Bant_Click(object sender, EventArgs e)
        {
            ZctCatProvBindingSource.CancelEdit();
            if (_pos <= 0)
            {
                return;
            }
            cod_ProvTextBox.Enabled = false;
            cod_ProvTextBox.Text = "";
            ZctCatProvBindingSource.CancelEdit();
            ZctCatProvBindingSource.DataSource = ControladorProveedores.TraeProveedores();
            ZctCatProvBindingSource.Position = _pos-1;
            _NuevoProveedor = null;
            //ZctCatProvBindingSource.MovePrevious ();
            _pos -= 1;
            Beliminar.Enabled = _permisos.elimina;
        }
        private void Bsig_Click(object sender, EventArgs e)
        {
            ZctCatProvBindingSource.CancelEdit();
            if (_pos + 1 >= ZctCatProvBindingSource.List.Count )
            {
                NuevoProveedor();
                return;
            }
            cod_ProvTextBox.Enabled = false;
            ZctCatProvBindingSource.MoveNext();
            _pos += 1;
            Beliminar.Enabled = _permisos.elimina; 
        }
        private void Bguardar_Click(object sender, EventArgs e)
        {
            try
            {
                string ErrorM = "";
                if (nom_ProvTextBox.Text.Length <= 0)
                {
                    ErrorM += "Debe proporcionar el nombre del proveedor" + System.Environment.NewLine;
                    nom_ProvTextBox.Focus();
                }
                if (rFC_ProvTextBox.Text.Length <= 0)
                {
                    ErrorM += "Debe proporcionar el RFC del proveedor" + System.Environment.NewLine;
                    rFC_ProvTextBox.Focus();
                }
                else if (rFC_ProvTextBox.Text.Length < 12 || rFC_ProvTextBox.Text.Length > 13)
                {
                    ErrorM += "el RFC del proveedor debe ser de minimo 12 caracteres y un maximo de 13 caracteres" + " el actual tiene " + rFC_ProvTextBox.Text.Length.ToString() + " caracteres" + System.Environment.NewLine;
                    rFC_ProvTextBox.Focus();
                }
                 if (emailTextBox .Text.Length <=0)
                {
                    ErrorM += "Debe proporcionar un Email para el proveedor" + System.Environment.NewLine;
                    emailTextBox.Focus();
                }
                 if (calleTextBox.Text.Length <= 0)
                {
                    ErrorM += "Debe proporcionar la calle del proveedor" + System.Environment.NewLine;
                    calleTextBox.Focus();
                }
                 if (coloniaTextBox.Text.Length <= 0)
                {
                    ErrorM += "Debe proporcionar una colonia para el proveedor" + System.Environment.NewLine;
                    coloniaTextBox.Focus();
                }

                 if (TxtFrecPagos.Text.Length <= 0)
                 {
                     ErrorM += "Debe proporcionar un numero interior para el proveedor" + System.Environment.NewLine;
                     TxtFrecPagos.Focus();
                 }
                 if (cpTextBox.Text.Length <= 0)
                {
                    ErrorM += "Debe proporcionar un Codigo Postal para el proveedor" + System.Environment.NewLine;
                    cpTextBox.Focus();
                }
                 if (telefonoTextBox.Text.Length <= 0)
                {
                    ErrorM += "Debe proporcionar un telefono para el proveedor" + System.Environment.NewLine;
                    telefonoTextBox.Focus();
                }
                else if (estadoComboBox.SelectedValue==null)
                {
                    ErrorM += "Debe proporcionar un estado para el proveedor" + System.Environment.NewLine;
                    estadoComboBox.Focus();
                }
                 if (ciudadComboBox.SelectedValue == null)
                {
                    ErrorM += "Debe proporcionar una ciudad para el proveedor" + System.Environment.NewLine;
                    ciudadComboBox.Focus();
                }
                if (ErrorM.Length > 1)
                {
                   string  ErrorM1 = "Debe corregir los siguentes errores antes de guardar el proveedor: " + System.Environment.NewLine + ErrorM;
                    throw new Exception(ErrorM1);
                }
                if (_NuevoProveedor != null)
                {
                    ControladorProveedores.agrega(_NuevoProveedor);

                }
                ControladorProveedores.guardar();
                MessageBox.Show("proveedor guardado correctamente", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                NuevoProveedor();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Bcancelar_Click(object sender, EventArgs e)
        {
            NuevoProveedor();
            cod_ProvTextBox.Enabled = true;
        }
        private void Beliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ZctCatProvBindingSource.CancelEdit();
                DialogResult R = MessageBox.Show("¿Desea Eliminar el proveedor? esta accion no se podra deshacer", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (R == DialogResult.Yes)
                {
                    ControladorProveedores.Elimina((ResTotal.Modelo.ZctCatProv)ZctCatProvBindingSource.Current);
                    ZctCatProvBindingSource.RemoveCurrent();
                    ZctCatProvBindingSource.EndEdit();
                    ControladorProveedores.guardar();
                    ZctCatProvBindingSource.DataSource = ControladorProveedores.TraeProveedores();
                    MessageBox.Show("Proveedor eliminado", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    NuevoProveedor();
                    cod_ProvTextBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void TxtdiasCred_TextChanged(object sender, EventArgs e)
        {
           
        }
        private void TextoMayorAcero(object sender, EventArgs e)
        {
            Control MiControl = (Control)sender;
            if (Convert.ToInt32(MiControl.Text) <= 0)
            {
                MiControl.Text = "1";
            }
        }
        public void CargaProveedor(int cod_prov)
        {
            cod_ProvTextBox.Text = cod_prov.ToString();
            _pos = ControladorProveedores.proveedorPos(cod_prov);
            ZctCatProvBindingSource.DataSource = ControladorProveedores.TraeProveedores(cod_prov.ToString());
            cod_ProvTextBox.Enabled = false;
        }

        private void TxtNumPagos_TextChanged(object sender, EventArgs e)
        {
            if (TxtNumPagos.Text == "")
            {
                TxtNumPagos.Text = "1";
                //TxtNumPagos.Enabled = false;
                //lpagos.visible = false;

                TxtFrecPagos.Text = "0";
                TxtFrecPagos.Enabled = false;
                return;
            }
            if (Convert.ToInt32(TxtNumPagos.Text)<= 0)
            {
                TxtNumPagos.Text = "1";
                //TxtNumPagos.Enabled = false;
                //lpagos.visible = false;

                TxtFrecPagos.Text = "0";
                TxtFrecPagos.Enabled = false;
            }
            else if (Convert.ToInt32(TxtNumPagos.Text) > 1)
            {
                
                    //TxtNumPagos.Text = "1";
                    //TxtNumPagos.Enabled = true;

                    //TxtFrecPagos.Text = "1";
                    TxtFrecPagos.Enabled = true;
                
            }
        }

        private void cod_ProvTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }

}
