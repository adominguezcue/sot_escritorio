﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ResTotal.Vista
{
    public partial class FrmEditaArticuloCantidad : Form
    {
        private Modelo.ZctDetCaja _detalle_caja = new Modelo.ZctDetCaja();
        int cantidad = 0;
        public FrmEditaArticuloCantidad(Modelo.ZctDetCaja detalle_caja)
        {
            InitializeComponent();
            _detalle_caja = detalle_caja;
            cantidad = _detalle_caja.Ctd_Art;
            this.desc_ArtLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "ZctCatArt.Desc_Art", true));
        }

        private void FrmEditaArticuloCantidad_Load(object sender, EventArgs e)
        {
            zctDetCajaBindingSource.DataSource = _detalle_caja;
        }

        private void Bgrabar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK; 

        }

        private void Blimpiar_Click(object sender, EventArgs e)
        {
            _detalle_caja.Ctd_Art = cantidad;
        }

        private void ctd_ArtNumericUpDown_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
