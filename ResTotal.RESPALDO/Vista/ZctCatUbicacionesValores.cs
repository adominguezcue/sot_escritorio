﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Linq;


namespace ResTotal.Vista
{
    public partial class ZctCatUbicacionesValores : Form
    {
        Modelo.ZctEncUbicacionValores EncValores;
        int _User;
        Controlador.ControladorUbicacionValores _ControladorUV;
        public ZctCatUbicacionesValores(string Cnn, int Cod_usu)
        {
            InitializeComponent();
            _User = Cod_usu;
            _ControladorUV = new Controlador.ControladorUbicacionValores(Cnn, Cod_usu);
            Dfecha.ValueChanged += new EventHandler(Dfecha_ValueChanged);
            DGV.CellEndEdit += new DataGridViewCellEventHandler(DGV_CellEndEdit);
        }

        void DGV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            CalculaTotal();
        }

        void Dfecha_ValueChanged(object sender, EventArgs e)
        {
            CargarFecha();
        }
        private void CargarFecha()
        {
            EncValores = null;
            EncValores = _ControladorUV.ValidaFecha(Dfecha.Value.Date, _User);
            if (_ControladorUV.Nuevo == true)
            {
                _ControladorUV.CreaDetalleUbicacion(EncValores);
                Total();
            }
            TxtFolio.Text = EncValores.folio;
            DGV.DataSource = EncValores.ZctDetalleUbicacionValores;
            Bregistrar.Enabled = _ControladorUV.Nuevo;
            Total();
        }

        private void ZctCatUbicacionesValores_Load(object sender, EventArgs e)
        {
            ColUbicacion.DataPropertyName = "DescripcionUbicacion";
            CargarFecha();
        }
        private void Total()
        {
            Ltotal.Text = string.Format("{0:C}",_ControladorUV.Total(Dfecha.Value.Date));

        }
        private void CalculaTotal()
        {
            decimal result = DGV.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDecimal(x.Cells["montoDataGridViewTextBoxColumn"].Value));
            Ltotal.Text = string.Format("{0:C}", result);

        }
        private void Bregistrar_Click(object sender, EventArgs e)
        {
            try
            {
                _ControladorUV.Grabar(EncValores);
                EncValores = null;
                Dfecha.Value = System.DateTime.Now;
                CargarFecha();
                MessageBox.Show("Guardando",  "Hecho",MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error guardando valores: " + Ex.Message, "Atencion",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
        }
     
    }
}
