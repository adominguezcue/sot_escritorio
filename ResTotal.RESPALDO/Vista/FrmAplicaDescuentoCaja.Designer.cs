﻿namespace ResTotal.Vista
{
    partial class FrmAplicaDescuentoCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label costoArticuloOTLabel;
            this.Blimpiar = new System.Windows.Forms.Button();
            this.Bgrabar = new System.Windows.Forms.Button();
            this.txtDescuento = new System.Windows.Forms.NumericUpDown();
            costoArticuloOTLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescuento)).BeginInit();
            this.SuspendLayout();
            // 
            // costoArticuloOTLabel
            // 
            costoArticuloOTLabel.AutoSize = true;
            costoArticuloOTLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            costoArticuloOTLabel.Location = new System.Drawing.Point(12, 32);
            costoArticuloOTLabel.Name = "costoArticuloOTLabel";
            costoArticuloOTLabel.Size = new System.Drawing.Size(106, 24);
            costoArticuloOTLabel.TabIndex = 20;
            costoArticuloOTLabel.Text = "Descuento:";
            // 
            // Blimpiar
            // 
            this.Blimpiar.CausesValidation = false;
            this.Blimpiar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Blimpiar.Location = new System.Drawing.Point(141, 115);
            this.Blimpiar.Name = "Blimpiar";
            this.Blimpiar.Size = new System.Drawing.Size(103, 54);
            this.Blimpiar.TabIndex = 23;
            this.Blimpiar.Text = "Cancelar";
            this.Blimpiar.UseVisualStyleBackColor = true;
            this.Blimpiar.Click += new System.EventHandler(this.Blimpiar_Click);
            // 
            // Bgrabar
            // 
            this.Bgrabar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Bgrabar.Location = new System.Drawing.Point(16, 115);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 54);
            this.Bgrabar.TabIndex = 22;
            this.Bgrabar.Text = "Aplicar";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // txtDescuento
            // 
            this.txtDescuento.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescuento.Location = new System.Drawing.Point(124, 32);
            this.txtDescuento.Name = "txtDescuento";
            this.txtDescuento.Size = new System.Drawing.Size(120, 29);
            this.txtDescuento.TabIndex = 24;
            // 
            // FrmAplicaDescuentoCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 181);
            this.Controls.Add(this.txtDescuento);
            this.Controls.Add(this.Blimpiar);
            this.Controls.Add(this.Bgrabar);
            this.Controls.Add(costoArticuloOTLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAplicaDescuentoCaja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Aplica descuento";
            this.Load += new System.EventHandler(this.FrmAplicaDescuentoCaja_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtDescuento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Blimpiar;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.NumericUpDown txtDescuento;
    }
}