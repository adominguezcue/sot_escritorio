﻿namespace ResTotal.Vista
{
    partial class frmEditaArticulo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cod_ArtLabel;
            System.Windows.Forms.Label ctd_ArtLabel;
            System.Windows.Forms.Label desc_ArtLabel;
            System.Windows.Forms.Label ivaLabel;
            System.Windows.Forms.Label totalLabel;
            System.Windows.Forms.Label costoArticuloOTLabel;
            System.Windows.Forms.Label label1;
            this.zctDetCajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cod_ArtLabel1 = new System.Windows.Forms.Label();
            this.ctd_ArtNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.desc_ArtLabel1 = new System.Windows.Forms.Label();
            this.ivaLabel1 = new System.Windows.Forms.Label();
            this.costoArticuloOTTextBox = new System.Windows.Forms.TextBox();
            this.Blimpiar = new System.Windows.Forms.Button();
            this.Bgrabar = new System.Windows.Forms.Button();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            cod_ArtLabel = new System.Windows.Forms.Label();
            ctd_ArtLabel = new System.Windows.Forms.Label();
            desc_ArtLabel = new System.Windows.Forms.Label();
            ivaLabel = new System.Windows.Forms.Label();
            totalLabel = new System.Windows.Forms.Label();
            costoArticuloOTLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.zctDetCajaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctd_ArtNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // cod_ArtLabel
            // 
            cod_ArtLabel.AutoSize = true;
            cod_ArtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cod_ArtLabel.Location = new System.Drawing.Point(87, 16);
            cod_ArtLabel.Name = "cod_ArtLabel";
            cod_ArtLabel.Size = new System.Drawing.Size(78, 24);
            cod_ArtLabel.TabIndex = 1;
            cod_ArtLabel.Text = "Artículo:";
            // 
            // ctd_ArtLabel
            // 
            ctd_ArtLabel.AutoSize = true;
            ctd_ArtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ctd_ArtLabel.Location = new System.Drawing.Point(76, 79);
            ctd_ArtLabel.Name = "ctd_ArtLabel";
            ctd_ArtLabel.Size = new System.Drawing.Size(89, 24);
            ctd_ArtLabel.TabIndex = 3;
            ctd_ArtLabel.Text = "Cantidad:";
            // 
            // desc_ArtLabel
            // 
            desc_ArtLabel.AutoSize = true;
            desc_ArtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            desc_ArtLabel.Location = new System.Drawing.Point(50, 49);
            desc_ArtLabel.Name = "desc_ArtLabel";
            desc_ArtLabel.Size = new System.Drawing.Size(115, 24);
            desc_ArtLabel.TabIndex = 9;
            desc_ArtLabel.Text = "Descripción:";
            // 
            // ivaLabel
            // 
            ivaLabel.AutoSize = true;
            ivaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            ivaLabel.Location = new System.Drawing.Point(120, 180);
            ivaLabel.Name = "ivaLabel";
            ivaLabel.Size = new System.Drawing.Size(45, 24);
            ivaLabel.TabIndex = 11;
            ivaLabel.Text = "IVA:";
            // 
            // totalLabel
            // 
            totalLabel.AutoSize = true;
            totalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            totalLabel.Location = new System.Drawing.Point(109, 211);
            totalLabel.Name = "totalLabel";
            totalLabel.Size = new System.Drawing.Size(56, 24);
            totalLabel.TabIndex = 13;
            totalLabel.Text = "Total:";
            // 
            // costoArticuloOTLabel
            // 
            costoArticuloOTLabel.AutoSize = true;
            costoArticuloOTLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            costoArticuloOTLabel.Location = new System.Drawing.Point(1, 110);
            costoArticuloOTLabel.Name = "costoArticuloOTLabel";
            costoArticuloOTLabel.Size = new System.Drawing.Size(164, 24);
            costoArticuloOTLabel.TabIndex = 14;
            costoArticuloOTLabel.Text = "Costo por Artículo:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.Location = new System.Drawing.Point(75, 146);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(90, 24);
            label1.TabIndex = 25;
            label1.Text = "SubTotal:";
            // 
            // zctDetCajaBindingSource
            // 
            this.zctDetCajaBindingSource.DataSource = typeof(ResTotal.Modelo.ZctDetCaja);
            // 
            // cod_ArtLabel1
            // 
            this.cod_ArtLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "Cod_Art", true));
            this.cod_ArtLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cod_ArtLabel1.Location = new System.Drawing.Point(171, 17);
            this.cod_ArtLabel1.Name = "cod_ArtLabel1";
            this.cod_ArtLabel1.Size = new System.Drawing.Size(220, 23);
            this.cod_ArtLabel1.TabIndex = 2;
            this.cod_ArtLabel1.Text = "label1";
            // 
            // ctd_ArtNumericUpDown
            // 
            this.ctd_ArtNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.zctDetCajaBindingSource, "Ctd_Art", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.ctd_ArtNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctd_ArtNumericUpDown.Location = new System.Drawing.Point(175, 79);
            this.ctd_ArtNumericUpDown.Name = "ctd_ArtNumericUpDown";
            this.ctd_ArtNumericUpDown.Size = new System.Drawing.Size(128, 29);
            this.ctd_ArtNumericUpDown.TabIndex = 4;
            this.ctd_ArtNumericUpDown.ValueChanged += new System.EventHandler(this.ctd_ArtNumericUpDown_ValueChanged);
            // 
            // desc_ArtLabel1
            // 
            this.desc_ArtLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "ZctCatArt.Desc_Art", true));
            this.desc_ArtLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.desc_ArtLabel1.Location = new System.Drawing.Point(171, 49);
            this.desc_ArtLabel1.Name = "desc_ArtLabel1";
            this.desc_ArtLabel1.Size = new System.Drawing.Size(220, 23);
            this.desc_ArtLabel1.TabIndex = 10;
            this.desc_ArtLabel1.Text = "label2";
            this.desc_ArtLabel1.Click += new System.EventHandler(this.desc_ArtLabel1_Click);
            // 
            // ivaLabel1
            // 
            this.ivaLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "iva", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "C2"));
            this.ivaLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ivaLabel1.Location = new System.Drawing.Point(171, 181);
            this.ivaLabel1.Name = "ivaLabel1";
            this.ivaLabel1.Size = new System.Drawing.Size(100, 23);
            this.ivaLabel1.TabIndex = 12;
            this.ivaLabel1.Text = "label1";
            // 
            // costoArticuloOTTextBox
            // 
            this.costoArticuloOTTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "CostoArticuloOT", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.costoArticuloOTTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.costoArticuloOTTextBox.Location = new System.Drawing.Point(175, 114);
            this.costoArticuloOTTextBox.Name = "costoArticuloOTTextBox";
            this.costoArticuloOTTextBox.Size = new System.Drawing.Size(128, 29);
            this.costoArticuloOTTextBox.TabIndex = 15;
            // 
            // Blimpiar
            // 
            this.Blimpiar.CausesValidation = false;
            this.Blimpiar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Blimpiar.Location = new System.Drawing.Point(296, 251);
            this.Blimpiar.Name = "Blimpiar";
            this.Blimpiar.Size = new System.Drawing.Size(103, 54);
            this.Blimpiar.TabIndex = 21;
            this.Blimpiar.Text = "Cancelar";
            this.Blimpiar.UseVisualStyleBackColor = true;
            this.Blimpiar.Click += new System.EventHandler(this.Blimpiar_Click);
            // 
            // Bgrabar
            // 
            this.Bgrabar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Bgrabar.Location = new System.Drawing.Point(150, 251);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 54);
            this.Bgrabar.TabIndex = 20;
            this.Bgrabar.Text = "Grabar";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "subtotal", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "C2"));
            this.lblSubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.Location = new System.Drawing.Point(171, 146);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(100, 23);
            this.lblSubtotal.TabIndex = 24;
            this.lblSubtotal.Text = "label1";
            // 
            // label2
            // 
            this.label2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "ImporteArticulo", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "C2"));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(171, 212);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 26;
            this.label2.Text = "label1";
            // 
            // frmEditaArticulo
            // 
            this.AcceptButton = this.Bgrabar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Blimpiar;
            this.ClientSize = new System.Drawing.Size(549, 325);
            this.Controls.Add(this.label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.lblSubtotal);
            this.Controls.Add(this.Blimpiar);
            this.Controls.Add(this.Bgrabar);
            this.Controls.Add(costoArticuloOTLabel);
            this.Controls.Add(this.costoArticuloOTTextBox);
            this.Controls.Add(totalLabel);
            this.Controls.Add(ivaLabel);
            this.Controls.Add(this.ivaLabel1);
            this.Controls.Add(desc_ArtLabel);
            this.Controls.Add(this.desc_ArtLabel1);
            this.Controls.Add(ctd_ArtLabel);
            this.Controls.Add(this.ctd_ArtNumericUpDown);
            this.Controls.Add(cod_ArtLabel);
            this.Controls.Add(this.cod_ArtLabel1);
            this.Name = "frmEditaArticulo";
            this.Text = "Modifica Artículo";
            this.Load += new System.EventHandler(this.frmEditaArticulo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.zctDetCajaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctd_ArtNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource zctDetCajaBindingSource;
        private System.Windows.Forms.Label cod_ArtLabel1;
        private System.Windows.Forms.NumericUpDown ctd_ArtNumericUpDown;
        private System.Windows.Forms.Label desc_ArtLabel1;
        private System.Windows.Forms.Label ivaLabel1;
        private System.Windows.Forms.TextBox costoArticuloOTTextBox;
        private System.Windows.Forms.Button Blimpiar;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.Label label2;
    }
}