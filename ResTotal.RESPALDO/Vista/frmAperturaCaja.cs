﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace ResTotal.Vista
{
    public partial class frmAperturaCaja : Form
    {
        int _user = 0;
        string _cod_folio;
        Controlador.Controladorcaja _ControladorCaja;
        public frmAperturaCaja(string cnn, int cod_user,string Cod_folio)
        {
            InitializeComponent();
            _cod_folio = Cod_folio;
            Tmonto.KeyPress += new KeyPressEventHandler(Tmonto_KeyPress);
            _ControladorCaja = new Controlador.Controladorcaja(cnn,_cod_folio,cod_user);
            _user = cod_user;
        }

        void Tmonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != 46))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }


        private void BcancelarRetiro_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel; 
        }

        private void Bretirar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Tmonto.Text))
            {
                Tmonto.Focus();
                throw new Exception("Especifique un monto para iniciar la caja");
            }
            try
            {
                _ControladorCaja.abrecaja("CJ", _user, Convert.ToDecimal(Tmonto.Text));
                MessageBox.Show("la caja se ha abierto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error abriendo caja:" + System.Environment.NewLine + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void frmAperturaCaja_Load(object sender, EventArgs e)
        {
            
        }
    }
}
