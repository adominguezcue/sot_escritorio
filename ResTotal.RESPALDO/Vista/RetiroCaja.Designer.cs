﻿namespace ResTotal.Vista
{
    partial class RetiroCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtMonto = new System.Windows.Forms.TextBox();
            this.BcancelarRetiro = new System.Windows.Forms.Button();
            this.Bretirar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TxtMonto
            // 
            this.TxtMonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMonto.Location = new System.Drawing.Point(271, 18);
            this.TxtMonto.Name = "TxtMonto";
            this.TxtMonto.Size = new System.Drawing.Size(130, 30);
            this.TxtMonto.TabIndex = 27;
            this.TxtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BcancelarRetiro
            // 
            this.BcancelarRetiro.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BcancelarRetiro.Location = new System.Drawing.Point(216, 61);
            this.BcancelarRetiro.Name = "BcancelarRetiro";
            this.BcancelarRetiro.Size = new System.Drawing.Size(100, 23);
            this.BcancelarRetiro.TabIndex = 30;
            this.BcancelarRetiro.Text = "Cancelar";
            this.BcancelarRetiro.UseVisualStyleBackColor = true;
            this.BcancelarRetiro.Click += new System.EventHandler(this.BcancelarRetiro_Click);
            // 
            // Bretirar
            // 
            this.Bretirar.Location = new System.Drawing.Point(94, 61);
            this.Bretirar.Name = "Bretirar";
            this.Bretirar.Size = new System.Drawing.Size(100, 23);
            this.Bretirar.TabIndex = 29;
            this.Bretirar.Text = "Retiro caja";
            this.Bretirar.UseVisualStyleBackColor = true;
            this.Bretirar.Click += new System.EventHandler(this.Bretirar_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(12, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(253, 27);
            this.label2.TabIndex = 28;
            this.label2.Text = "Montoa retirar de caja $:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RetiroCaja
            // 
            this.AcceptButton = this.Bretirar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BcancelarRetiro;
            this.ClientSize = new System.Drawing.Size(410, 91);
            this.Controls.Add(this.TxtMonto);
            this.Controls.Add(this.BcancelarRetiro);
            this.Controls.Add(this.Bretirar);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RetiroCaja";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Retiro Caja";
            this.Load += new System.EventHandler(this.RetiroCaja_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtMonto;
        private System.Windows.Forms.Button BcancelarRetiro;
        private System.Windows.Forms.Button Bretirar;
        private System.Windows.Forms.Label label2;
    }
}