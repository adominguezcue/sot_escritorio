﻿namespace ResTotal.Vista
{
    partial class CxPDetallado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.Bgrabar = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Lrestante = new System.Windows.Forms.Label();
            this.Lstatus = new System.Windows.Forms.Label();
            this.BsCxP = new System.Windows.Forms.BindingSource(this.components);
            this.Lpago = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Ltotal = new System.Windows.Forms.Label();
            this.Liva = new System.Windows.Forms.Label();
            this.Lsubtotal = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Bsig = new System.Windows.Forms.Button();
            this.Bant = new System.Windows.Forms.Button();
            this.TxtFolio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.Lemail = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Ldireccion = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LRFC = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Lcliente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtPedido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DGVpagos = new System.Windows.Forms.DataGridView();
            this.BsPagosCxP = new System.Windows.Forms.BindingSource(this.components);
            this.BsBancos = new System.Windows.Forms.BindingSource(this.components);
            this.BsEstadoPagos = new System.Windows.Forms.BindingSource(this.components);
            this.button10 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DGVdetalle = new System.Windows.Forms.DataGridView();
            this.ArticuloCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctdArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cosArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImporteCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BsDetalleOC = new System.Windows.Forms.BindingSource(this.components);
            this.TiposPagoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Tipo_pago = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tipopagotmp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_banco = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha_factura = new DataColumns.CalendarColumn();
            this.montoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha_pago = new DataColumns.CalendarColumn();
            this.fechavencidoDataGridViewTextBoxColumn1 = new DataColumns.CalendarColumn();
            this.estadocol = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.estadoTmp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BsCxP)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVpagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsPagosCxP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsBancos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsEstadoPagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsDetalleOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TiposPagoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.groupBox8.Controls.Add(this.button6);
            this.groupBox8.Controls.Add(this.Bgrabar);
            this.groupBox8.Location = new System.Drawing.Point(829, 345);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(346, 93);
            this.groupBox8.TabIndex = 22;
            this.groupBox8.TabStop = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(195, 32);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(103, 43);
            this.button6.TabIndex = 17;
            this.button6.Text = "Cerrar";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // Bgrabar
            // 
            this.Bgrabar.Location = new System.Drawing.Point(48, 32);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 43);
            this.Bgrabar.TabIndex = 15;
            this.Bgrabar.Text = "Grabar";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Green;
            this.label21.Location = new System.Drawing.Point(21, 199);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(121, 27);
            this.label21.TabIndex = 9;
            this.label21.Text = "Por pagar:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.groupBox7.Controls.Add(this.Lrestante);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.Lstatus);
            this.groupBox7.Controls.Add(this.Lpago);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.Ltotal);
            this.groupBox7.Controls.Add(this.Liva);
            this.groupBox7.Controls.Add(this.Lsubtotal);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Location = new System.Drawing.Point(829, 107);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(346, 233);
            this.groupBox7.TabIndex = 21;
            this.groupBox7.TabStop = false;
            // 
            // Lrestante
            // 
            this.Lrestante.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lrestante.ForeColor = System.Drawing.Color.Green;
            this.Lrestante.Location = new System.Drawing.Point(168, 199);
            this.Lrestante.Name = "Lrestante";
            this.Lrestante.Size = new System.Drawing.Size(154, 25);
            this.Lrestante.TabIndex = 10;
            this.Lrestante.Text = "$0.00";
            this.Lrestante.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Lstatus
            // 
            this.Lstatus.BackColor = System.Drawing.Color.PaleGreen;
            this.Lstatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lstatus.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BsCxP, "estado", true));
            this.Lstatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lstatus.Location = new System.Drawing.Point(83, 17);
            this.Lstatus.Name = "Lstatus";
            this.Lstatus.Size = new System.Drawing.Size(218, 20);
            this.Lstatus.TabIndex = 0;
            this.Lstatus.Text = "STATUS";
            this.Lstatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BsCxP
            // 
            this.BsCxP.DataSource = typeof(ResTotal.Modelo.ZctEncCuentasPagar);
            // 
            // Lpago
            // 
            this.Lpago.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lpago.ForeColor = System.Drawing.Color.Green;
            this.Lpago.Location = new System.Drawing.Point(138, 162);
            this.Lpago.Name = "Lpago";
            this.Lpago.Size = new System.Drawing.Size(184, 25);
            this.Lpago.TabIndex = 8;
            this.Lpago.Text = "$0.00";
            this.Lpago.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Green;
            this.label19.Location = new System.Drawing.Point(36, 160);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 27);
            this.label19.TabIndex = 7;
            this.label19.Text = "Pago:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Ltotal
            // 
            this.Ltotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BsCxP, "total", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "N2"));
            this.Ltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ltotal.ForeColor = System.Drawing.Color.Navy;
            this.Ltotal.Location = new System.Drawing.Point(139, 125);
            this.Ltotal.Name = "Ltotal";
            this.Ltotal.Size = new System.Drawing.Size(184, 25);
            this.Ltotal.TabIndex = 5;
            this.Ltotal.Text = "$0.00";
            this.Ltotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Liva
            // 
            this.Liva.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BsCxP, "iva", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "N2"));
            this.Liva.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Liva.ForeColor = System.Drawing.Color.Navy;
            this.Liva.Location = new System.Drawing.Point(138, 91);
            this.Liva.Name = "Liva";
            this.Liva.Size = new System.Drawing.Size(184, 23);
            this.Liva.TabIndex = 4;
            this.Liva.Text = "$0.00";
            this.Liva.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Lsubtotal
            // 
            this.Lsubtotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.BsCxP, "subtotal", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0", "C2"));
            this.Lsubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lsubtotal.ForeColor = System.Drawing.Color.Navy;
            this.Lsubtotal.Location = new System.Drawing.Point(139, 54);
            this.Lsubtotal.Name = "Lsubtotal";
            this.Lsubtotal.Size = new System.Drawing.Size(184, 24);
            this.Lsubtotal.TabIndex = 3;
            this.Lsubtotal.Text = "$0.00";
            this.Lsubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(78, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 24);
            this.label14.TabIndex = 2;
            this.label14.Text = "IVA:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(21, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 24);
            this.label13.TabIndex = 1;
            this.label13.Text = "Sub Total:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(64, 125);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 27);
            this.label12.TabIndex = 0;
            this.label12.Text = "Total:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Bsig);
            this.groupBox1.Controls.Add(this.Bant);
            this.groupBox1.Controls.Add(this.TxtFolio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(5, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(326, 49);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Visible = false;
            // 
            // Bsig
            // 
            this.Bsig.Location = new System.Drawing.Point(276, 12);
            this.Bsig.Name = "Bsig";
            this.Bsig.Size = new System.Drawing.Size(37, 29);
            this.Bsig.TabIndex = 3;
            this.Bsig.Text = ">";
            this.Bsig.UseVisualStyleBackColor = true;
            this.Bsig.Visible = false;
            // 
            // Bant
            // 
            this.Bant.Location = new System.Drawing.Point(239, 12);
            this.Bant.Name = "Bant";
            this.Bant.Size = new System.Drawing.Size(37, 29);
            this.Bant.TabIndex = 2;
            this.Bant.Text = "<";
            this.Bant.UseVisualStyleBackColor = true;
            this.Bant.Visible = false;
            // 
            // TxtFolio
            // 
            this.TxtFolio.Location = new System.Drawing.Point(49, 16);
            this.TxtFolio.Name = "TxtFolio";
            this.TxtFolio.Size = new System.Drawing.Size(184, 20);
            this.TxtFolio.TabIndex = 1;
            this.TxtFolio.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folio:";
            this.label1.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.Lemail);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.Ldireccion);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.LRFC);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.Lcliente);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(5, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1170, 79);
            this.groupBox4.TabIndex = 19;
            this.groupBox4.TabStop = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(1036, 9);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 56);
            this.button3.TabIndex = 13;
            this.button3.Text = "Cambiar Datos";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Lemail
            // 
            this.Lemail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lemail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lemail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lemail.Location = new System.Drawing.Point(751, 14);
            this.Lemail.Name = "Lemail";
            this.Lemail.Size = new System.Drawing.Size(238, 22);
            this.Lemail.TabIndex = 12;
            this.Lemail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(710, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Correo:";
            // 
            // Ldireccion
            // 
            this.Ldireccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ldireccion.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Ldireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Ldireccion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ldireccion.Location = new System.Drawing.Point(352, 40);
            this.Ldireccion.Name = "Ldireccion";
            this.Ldireccion.Size = new System.Drawing.Size(637, 28);
            this.Ldireccion.TabIndex = 10;
            this.Ldireccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(291, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Dirección:";
            // 
            // LRFC
            // 
            this.LRFC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LRFC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LRFC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRFC.Location = new System.Drawing.Point(58, 46);
            this.LRFC.Name = "LRFC";
            this.LRFC.Size = new System.Drawing.Size(227, 22);
            this.LRFC.TabIndex = 8;
            this.LRFC.Text = "XXX010101XXX";
            this.LRFC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "RFC:";
            // 
            // Lcliente
            // 
            this.Lcliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lcliente.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lcliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lcliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lcliente.Location = new System.Drawing.Point(75, 14);
            this.Lcliente.Name = "Lcliente";
            this.Lcliente.Size = new System.Drawing.Size(616, 22);
            this.Lcliente.TabIndex = 6;
            this.Lcliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Proveedor:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TxtPedido);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(337, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 49);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Visible = false;
            // 
            // TxtPedido
            // 
            this.TxtPedido.Location = new System.Drawing.Point(59, 16);
            this.TxtPedido.Name = "TxtPedido";
            this.TxtPedido.Size = new System.Drawing.Size(176, 20);
            this.TxtPedido.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pedido:";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.DGVpagos);
            this.groupBox6.Location = new System.Drawing.Point(5, 321);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(818, 217);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            // 
            // DGVpagos
            // 
            this.DGVpagos.AllowUserToAddRows = false;
            this.DGVpagos.AllowUserToDeleteRows = false;
            this.DGVpagos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVpagos.AutoGenerateColumns = false;
            this.DGVpagos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DGVpagos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVpagos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipo_pago,
            this.tipopagotmp,
            this.Referencia,
            this.cod_banco,
            this.factura,
            this.fecha_factura,
            this.montoDataGridViewTextBoxColumn,
            this.fecha_pago,
            this.fechavencidoDataGridViewTextBoxColumn1,
            this.estadocol,
            this.estadoTmp});
            this.DGVpagos.DataSource = this.BsPagosCxP;
            this.DGVpagos.Location = new System.Drawing.Point(6, 14);
            this.DGVpagos.Name = "DGVpagos";
            this.DGVpagos.Size = new System.Drawing.Size(806, 197);
            this.DGVpagos.TabIndex = 0;
            // 
            // BsPagosCxP
            // 
            this.BsPagosCxP.DataSource = typeof(ResTotal.Modelo.ZctDetCuentasPagar);
            // 
            // BsBancos
            // 
            this.BsBancos.DataSource = typeof(ResTotal.Modelo.ZctBancos);
            // 
            // BsEstadoPagos
            // 
            this.BsEstadoPagos.DataSource = typeof(ResTotal.Modelo.ZctEstadosPago);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(593, 13);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(103, 43);
            this.button10.TabIndex = 24;
            this.button10.Text = "Cambiar pedido";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(803, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(241, 49);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Visible = false;
            // 
            // DGVdetalle
            // 
            this.DGVdetalle.AllowUserToAddRows = false;
            this.DGVdetalle.AllowUserToDeleteRows = false;
            this.DGVdetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVdetalle.AutoGenerateColumns = false;
            this.DGVdetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVdetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ArticuloCol,
            this.ctdArtDataGridViewTextBoxColumn,
            this.cosArtDataGridViewTextBoxColumn,
            this.ImporteCol});
            this.DGVdetalle.DataSource = this.BsDetalleOC;
            this.DGVdetalle.Location = new System.Drawing.Point(6, 110);
            this.DGVdetalle.Name = "DGVdetalle";
            this.DGVdetalle.ReadOnly = true;
            this.DGVdetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVdetalle.Size = new System.Drawing.Size(806, 197);
            this.DGVdetalle.TabIndex = 25;
            // 
            // ArticuloCol
            // 
            this.ArticuloCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ArticuloCol.HeaderText = "Articulo";
            this.ArticuloCol.Name = "ArticuloCol";
            this.ArticuloCol.ReadOnly = true;
            this.ArticuloCol.Width = 67;
            // 
            // ctdArtDataGridViewTextBoxColumn
            // 
            this.ctdArtDataGridViewTextBoxColumn.DataPropertyName = "Ctd_Art";
            dataGridViewCellStyle3.Format = "N2";
            this.ctdArtDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.ctdArtDataGridViewTextBoxColumn.HeaderText = "Cantidad";
            this.ctdArtDataGridViewTextBoxColumn.Name = "ctdArtDataGridViewTextBoxColumn";
            this.ctdArtDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cosArtDataGridViewTextBoxColumn
            // 
            this.cosArtDataGridViewTextBoxColumn.DataPropertyName = "Cos_Art";
            dataGridViewCellStyle4.Format = "N2";
            this.cosArtDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.cosArtDataGridViewTextBoxColumn.HeaderText = "Costo";
            this.cosArtDataGridViewTextBoxColumn.Name = "cosArtDataGridViewTextBoxColumn";
            this.cosArtDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ImporteCol
            // 
            dataGridViewCellStyle5.Format = "N2";
            this.ImporteCol.DefaultCellStyle = dataGridViewCellStyle5;
            this.ImporteCol.HeaderText = "Importe";
            this.ImporteCol.Name = "ImporteCol";
            this.ImporteCol.ReadOnly = true;
            // 
            // BsDetalleOC
            // 
            this.BsDetalleOC.DataSource = typeof(ResTotal.Modelo.ZctDetOC);
            // 
            // TiposPagoBindingSource
            // 
            this.TiposPagoBindingSource.DataSource = typeof(ResTotal.Modelo.ZctCatTipoPago);
            // 
            // Tipo_pago
            // 
            this.Tipo_pago.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Tipo_pago.DataPropertyName = "metodo";
            this.Tipo_pago.DataSource = this.BsPagosCxP;
            dataGridViewCellStyle1.NullValue = "CONTADO";
            this.Tipo_pago.DefaultCellStyle = dataGridViewCellStyle1;
            this.Tipo_pago.HeaderText = "Tipo de Pago";
            this.Tipo_pago.Name = "Tipo_pago";
            this.Tipo_pago.Width = 77;
            // 
            // tipopagotmp
            // 
            this.tipopagotmp.DataPropertyName = "metodo";
            this.tipopagotmp.HeaderText = "tipopagotmp";
            this.tipopagotmp.Name = "tipopagotmp";
            this.tipopagotmp.Visible = false;
            // 
            // Referencia
            // 
            this.Referencia.DataPropertyName = "referencia";
            this.Referencia.HeaderText = "Referencia";
            this.Referencia.Name = "Referencia";
            // 
            // cod_banco
            // 
            this.cod_banco.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.cod_banco.DataPropertyName = "cod_banco";
            this.cod_banco.DataSource = this.BsBancos;
            this.cod_banco.DisplayMember = "descripcion";
            this.cod_banco.HeaderText = "Banco";
            this.cod_banco.Name = "cod_banco";
            this.cod_banco.ValueMember = "cod_banco";
            this.cod_banco.Width = 44;
            // 
            // factura
            // 
            this.factura.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.factura.DataPropertyName = "factura";
            this.factura.HeaderText = "factura";
            this.factura.Name = "factura";
            this.factura.Width = 65;
            // 
            // fecha_factura
            // 
            this.fecha_factura.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.fecha_factura.DataPropertyName = "fecha_factura";
            this.fecha_factura.HeaderText = "fecha_factura";
            this.fecha_factura.Name = "fecha_factura";
            this.fecha_factura.Width = 79;
            // 
            // montoDataGridViewTextBoxColumn
            // 
            this.montoDataGridViewTextBoxColumn.DataPropertyName = "monto";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = "0";
            this.montoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.montoDataGridViewTextBoxColumn.HeaderText = "Monto";
            this.montoDataGridViewTextBoxColumn.Name = "montoDataGridViewTextBoxColumn";
            this.montoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fecha_pago
            // 
            this.fecha_pago.DataPropertyName = "fecha_pago";
            this.fecha_pago.HeaderText = "Fecha_pago";
            this.fecha_pago.Name = "fecha_pago";
            this.fecha_pago.ReadOnly = true;
            // 
            // fechavencidoDataGridViewTextBoxColumn1
            // 
            this.fechavencidoDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.fechavencidoDataGridViewTextBoxColumn1.DataPropertyName = "fecha_vencido";
            this.fechavencidoDataGridViewTextBoxColumn1.HeaderText = "Fecha_vencido";
            this.fechavencidoDataGridViewTextBoxColumn1.Name = "fechavencidoDataGridViewTextBoxColumn1";
            this.fechavencidoDataGridViewTextBoxColumn1.ReadOnly = true;
            this.fechavencidoDataGridViewTextBoxColumn1.Width = 87;
            // 
            // estadocol
            // 
            this.estadocol.DataPropertyName = "estado";
            this.estadocol.DataSource = this.BsEstadoPagos;
            this.estadocol.DisplayMember = "descripcion_pago";
            this.estadocol.HeaderText = "Estado de pago";
            this.estadocol.Name = "estadocol";
            this.estadocol.ValueMember = "descripcion_pago";
            // 
            // estadoTmp
            // 
            this.estadoTmp.DataPropertyName = "estado";
            this.estadoTmp.HeaderText = "estadotmp";
            this.estadoTmp.Name = "estadoTmp";
            this.estadoTmp.Visible = false;
            // 
            // CxPDetallado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 545);
            this.Controls.Add(this.DGVdetalle);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "CxPDetallado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Detalle Cuentas Por pagar";
            this.Load += new System.EventHandler(this.CxP_Load);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BsCxP)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVpagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsPagosCxP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsBancos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsEstadoPagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsDetalleOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TiposPagoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label Lrestante;
        private System.Windows.Forms.Label Lpago;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label Ltotal;
        private System.Windows.Forms.Label Liva;
        private System.Windows.Forms.Label Lsubtotal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Bsig;
        private System.Windows.Forms.Button Bant;
        private System.Windows.Forms.TextBox TxtFolio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label Lemail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label Ldireccion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label LRFC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Lcliente;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtPedido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView DGVpagos;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.BindingSource BsCxP;
        private System.Windows.Forms.BindingSource BsPagosCxP;
        private System.Windows.Forms.BindingSource TiposPagoBindingSource;
        private System.Windows.Forms.Label Lstatus;
        private System.Windows.Forms.BindingSource BsDetalleOC;
        private System.Windows.Forms.DataGridViewTextBoxColumn codOCDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn metodoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView DGVdetalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticuloCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctdArtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cosArtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImporteCol;
        private System.Windows.Forms.BindingSource BsEstadoPagos;
        private System.Windows.Forms.BindingSource BsBancos;
        private System.Windows.Forms.DataGridViewComboBoxColumn Tipo_pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipopagotmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewComboBoxColumn cod_banco;
        private System.Windows.Forms.DataGridViewTextBoxColumn factura;
        private DataColumns.CalendarColumn fecha_factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoDataGridViewTextBoxColumn;
        private DataColumns.CalendarColumn fecha_pago;
        private DataColumns.CalendarColumn fechavencidoDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewComboBoxColumn estadocol;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoTmp;
    }
}