﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace ResTotal.Vista.CxP
{
    public partial class NotificadorVencidos : Form
    {
        enum ShowWindowCommands
        {
            /// <summary>
            /// Hides the window and activates another window.
            /// </summary>
            Hide = 0,
            /// <summary>
            /// Activates and displays a window. If the window is minimized or
            /// maximized, the system restores it to its original size and position.
            /// An application should specify this flag when displaying the window
            /// for the first time.
            /// </summary>
            Normal = 1,
            /// <summary>
            /// Activates the window and displays it as a minimized window.
            /// </summary>
            ShowMinimized = 2,
            /// <summary>
            /// Maximizes the specified window.
            /// </summary>
            Maximize = 3, // is this the right value?
            /// <summary>
            /// Activates the window and displays it as a maximized window.
            /// </summary>      
            ShowMaximized = 3,
            /// <summary>
            /// Displays a window in its most recent size and position. This value
            /// is similar to <see cref="Win32.ShowWindowCommand.Normal"/>, except
            /// the window is not activated.
            /// </summary>
            ShowNoActivate = 4,
            /// <summary>
            /// Activates the window and displays it in its current size and position.
            /// </summary>
            Show = 5,
            /// <summary>
            /// Minimizes the specified window and activates the next top-level
            /// window in the Z order.
            /// </summary>
            Minimize = 6,
            /// <summary>
            /// Displays the window as a minimized window. This value is similar to
            /// <see cref="Win32.ShowWindowCommand.ShowMinimized"/>, except the
            /// window is not activated.
            /// </summary>
            ShowMinNoActive = 7,
            /// <summary>
            /// Displays the window in its current size and position. This value is
            /// similar to <see cref="Win32.ShowWindowCommand.Show"/>, except the
            /// window is not activated.
            /// </summary>
            ShowNA = 8,
            /// <summary>
            /// Activates and displays the window. If the window is minimized or
            /// maximized, the system restores it to its original size and position.
            /// An application should specify this flag when restoring a minimized window.
            /// </summary>
            Restore = 9,
            /// <summary>
            /// Sets the show state based on the SW_* value specified in the
            /// STARTUPINFO structure passed to the CreateProcess function by the
            /// program that started the application.
            /// </summary>
            ShowDefault = 10,
            /// <summary>
            ///  <b>Windows 2000/XP:</b> Minimizes a window, even if the thread
            /// that owns the window is not responding. This flag should only be
            /// used when minimizing windows from a different thread.
            /// </summary>
            ForceMinimize = 11
        }
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);
        decimal total = 0;
        ResTotal.Controlador.ControladorCxP _ControladorCxP;
        string _Cnn;
        public NotificadorVencidos(string Cnn, Form MdiContenedor)
        {
            InitializeComponent();

            this.Visible = false;
            this.Activated += new EventHandler(NotificadorVencidos_Activated);
            
            Arbol.NodeMouseClick += new TreeNodeMouseClickEventHandler(Arbol_NodeMouseClick);
            this.MdiParent = MdiContenedor;
            _Cnn = Cnn;
            _ControladorCxP = new Controlador.ControladorCxP(_Cnn);
            Rectangle workingArea = MdiParent.ClientRectangle;
            this.Location = new Point(workingArea.Right - Size.Width-10,
                                      workingArea.Bottom - Size.Height - 60);
            CargarCuentas();
            if (Arbol.Nodes.Count > 0)
            {
                ShowWindow(this.Handle, ShowWindowCommands.ShowNoActivate);
            }
        }

        void Arbol_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (Arbol.SelectedNode.Level == 0)
            {
                
            }
            else
            {

            }
        }

        void NotificadorVencidos_Activated(object sender, EventArgs e)
        {
            CargarCuentas();
        }
        public void CargarCuentas()
        {
            total = 0;
            _ControladorCxP = null;
            _ControladorCxP = new Controlador.ControladorCxP(_Cnn);
            Arbol.Nodes.Clear();
            List<Modelo.WV_ZctProveedoresCxP> PcXp = _ControladorCxP.TraeCuentasVencidas(DateTime.Now);
            foreach (Modelo.WV_ZctProveedoresCxP cuenta in PcXp)
            {
                TreeNode nodoP = new TreeNode(cuenta.Nom_Prov);
                var pagos = _ControladorCxP.TraePagosVencidosProveedor(DateTime.Now, (Convert.ToInt32(cuenta.cod_prov)));
                foreach (Modelo.ZctDetCuentasPagar pago in pagos)
                {
                    TreeNode nodoS = new TreeNode(pago.fecha_vencido + " " + String.Format("{0:C}",pago.monto));

                    nodoP.Nodes.Add(nodoS);
                    total += pago.monto;
                }
                Arbol.Nodes.Add(nodoP);
                Application.DoEvents();
            }
            if (Arbol.Nodes.Count == 0)

            {
                this.Dispose();
            }
            Arbol.ExpandAll();
            Ltotalpagar.Text = String.Format("{0:C}", total);
        }
        private void NotificadorVencidos_Load(object sender, EventArgs e)
        {
          
        }

        private void Arbol_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            CargarCuentas();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
