﻿namespace ResTotal.Vista.CxP
{
    partial class NotificadorVencidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Arbol = new System.Windows.Forms.TreeView();
            this.Ltotalpagar = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // Arbol
            // 
            this.Arbol.Dock = System.Windows.Forms.DockStyle.Top;
            this.Arbol.Location = new System.Drawing.Point(0, 0);
            this.Arbol.Name = "Arbol";
            this.Arbol.Size = new System.Drawing.Size(505, 183);
            this.Arbol.TabIndex = 0;
            this.Arbol.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.Arbol_AfterSelect);
            // 
            // Ltotalpagar
            // 
            this.Ltotalpagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ltotalpagar.ForeColor = System.Drawing.Color.Navy;
            this.Ltotalpagar.Location = new System.Drawing.Point(197, 192);
            this.Ltotalpagar.Name = "Ltotalpagar";
            this.Ltotalpagar.Size = new System.Drawing.Size(184, 26);
            this.Ltotalpagar.TabIndex = 20;
            this.Ltotalpagar.Text = "$0.00";
            this.Ltotalpagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(123, 191);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 27);
            this.label13.TabIndex = 19;
            this.label13.Text = "Total:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 3000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // NotificadorVencidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 224);
            this.Controls.Add(this.Ltotalpagar);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Arbol);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "NotificadorVencidos";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Pagos Vencidos";
            this.Load += new System.EventHandler(this.NotificadorVencidos_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView Arbol;
        private System.Windows.Forms.Label Ltotalpagar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Timer timer1;
    }
}