﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace ResTotal.Vista
{
    public partial class CxP_principal : Form
    {
        CxPDetallado detalles;
        public delegate void DcambiaProveedor(int Cod_prov);
        public event DcambiaProveedor cambiaproveedor;
        string _cnn;
        string _cod_folio;
        int _cod_usu;
        ResTotal.Controlador.ControladorCxP _ControladorCxP;
        public CxP_principal(string Cnn,int Cod_usu,string cod_folio)
        {
            InitializeComponent();
            dgvProveedores.RowEnter += new DataGridViewCellEventHandler(dgvProveedores_RowEnter);
            this.DGVdetalles.CellDoubleClick += new DataGridViewCellEventHandler(DGVdetalles_CellDoubleClick);
            this.DGVdetalles.RowEnter += new DataGridViewCellEventHandler(DGVdetalles_RowEnter);
            CBO_estado.LostFocus += new EventHandler(CBO_estado_LostFocus);
            this.Activated += new EventHandler(CxP_principal_Activated);


            _cod_usu = Cod_usu;
            _cod_folio = cod_folio;
            _cnn = Cnn;
        }

        

        void CboEstadoProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dgvProveedores.RowCount == 0)
            {
                return;
            }
            CambiaEstadoProveedor();
        }
        private void CambiaEstadoProveedor()
        {
            if (dgvProveedores.SelectedRows.Count < 0)
            {
                return;
            }
            BSprovedores.DataSource = _ControladorCxP.TraeProveedores();
            int Cod_prov = Convert.ToInt32(dgvProveedores.SelectedRows[0].Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);
            BScuentas.DataSource = _ControladorCxP.TraeCuentasProveedores(Cod_prov, CBO_estado.Text);
            BScuentas.ResetCurrentItem();
        }
        void CboEstadoProveedor_LostFocus(object sender, EventArgs e)
        {
            CambiaEstadoProveedor();
        }

       

        void CBO_estado_LostFocus(object sender, EventArgs e)
        {
            int Cod_prov = Convert.ToInt32(dgvProveedores.SelectedRows[0].Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);
            BScuentas.DataSource = _ControladorCxP.TraeCuentasProveedores(Cod_prov,CBO_estado.Text);
        }

        void CxP_principal_Activated(object sender, EventArgs e)
        {
            Cargar();
        }

        void DGVdetalles_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            string estado = DGVdetalles.Rows[e.RowIndex].Cells["estadoDataGridViewTextBoxColumn"].Value.ToString();
            Bgrabar.Enabled = (estado == "PENDIENTE" ? true : false);
            var result = DGVdetalles.Rows[e.RowIndex].Cells["totalDataGridViewTextBoxColumn1"].Value;
            ltotaldetalle.Text = string.Format("{0:C}", result);
        }

        void DGVdetalles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DGVdetalles.CurrentRow == null || Bgrabar.Enabled==false )
            {
                return; 
            }
            verdetalles();
        }

        void dgvProveedores_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            int Cod_prov = Convert.ToInt32(dgvProveedores.Rows[e.RowIndex].Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);
            BScuentas.DataSource = _ControladorCxP.TraeCuentasProveedores(Cod_prov,CBO_estado.Text);
            DGVdetalles.DataSource = BScuentas;
        }
        public void Cargar()
        {
            DGVdetalles.DataSource = null;
            _ControladorCxP = new Controlador.ControladorCxP(_cnn);
            BSprovedores.DataSource = _ControladorCxP.TraeProveedores();
            BSprovedores.ResetCurrentItem();
            decimal result = dgvProveedores.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDecimal(x.Cells["totalDataGridViewTextBoxColumn"].Value));
            Ltotalpagar.Text = string.Format("{0:C}", result);
            if (dgvProveedores.CurrentRow !=null)
            {
                int Cod_prov = Convert.ToInt32(dgvProveedores.CurrentRow.Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);
                BScuentas.DataSource = _ControladorCxP.TraeCuentasProveedores(Cod_prov);
                DGVdetalles.DataSource  = BScuentas;
            }
            result = 0;
            if (dgvProveedores.CurrentRow != null)
            {
                result = Convert.ToDecimal(DGVdetalles.CurrentRow.Cells["totalDataGridViewTextBoxColumn1"].Value);
            }
                ltotaldetalle.Text = string.Format("{0:C}", result);
            
        }
        private void CxP_principal_Load(object sender, EventArgs e)
        {
            CBO_estado.SelectedIndex = 0;
            Cargar();
        }

        private void DGVdetalles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var result=DGVdetalles.Rows[e.RowIndex].Cells["totalDataGridViewTextBoxColumn1"].Value;
            ltotaldetalle.Text = string.Format("{0:C}", result);
        }

        private void verdetalles()
        {
            //Bgrabar.Enabled = true;
            int cod_cta = Convert.ToInt32(DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[cod_cuenta_pago.DisplayIndex].Value);
            int cod_oc = Convert.ToInt32(DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[codOCDataGridViewTextBoxColumn.DisplayIndex].Value);
          
            detalles = new CxPDetallado(_cnn, _cod_usu, _cod_folio, cod_cta, cod_oc, Convert.ToDecimal(DGVdetalles.CurrentRow.Cells["totalDataGridViewTextBoxColumn1"].Value));
            detalles.CxPprincipal = this;
            detalles.MdiParent = this.MdiParent;
            detalles.Show();
        }

        private void Bgrabar_Click(object sender, EventArgs e)
        {
            verdetalles();
        }
        public void modificaproveedor(int cod_prov)
        {
            cambiaproveedor(cod_prov);
            detalles.ActualizaProveedor(cod_prov);
           
        }
        private void CBO_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dgvProveedores.RowCount == 0)
            {
                return;
            }
            int Cod_prov = Convert.ToInt32(dgvProveedores.SelectedRows[0].Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);
            BScuentas.DataSource = _ControladorCxP.TraeCuentasProveedores(Cod_prov, CBO_estado.Text);

        }
    }
}
