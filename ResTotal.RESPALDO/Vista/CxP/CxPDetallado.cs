﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ResTotal.Vista
{
    public partial class CxPDetallado : Form
    {
        public CxP_principal CxPprincipal;
        public delegate void DcambiaProveedor(int Cod_prov);
        public event DcambiaProveedor cambiaproveedor;
        public decimal totalPagar;
        ResTotal.Controlador.Controladorcaja _ControladorCaja;
        ResTotal.Modelo.ZctEncCuentasPagar CuentaxPagar;
        ResTotal.Controlador.ControladorCxP _ControladorCxP;
        Permisos.Controlador.Controlador _ControladorPermisos;
        Permisos.Controlador.Controlador.vistapermisos _Permisos;
        string _Cnn;
        int _Cod_usu;
        int _cta;
        int _oc;
        decimal _total;
        string _cod_folio;
        public CxPDetallado(string Cnn, int Cod_usu, string cod_folio, int Cta, int oc, decimal TotalCXP)
        {
            InitializeComponent();
            DGVpagos.DataError += new DataGridViewDataErrorEventHandler(DGVpagos_DataError);
            TxtFolio.LostFocus += new EventHandler(Txt_LostFocus);
            TxtPedido.LostFocus+= new EventHandler(Txt_LostFocus);
            DGVdetalle.RowsAdded += new DataGridViewRowsAddedEventHandler(DGVdetalle_RowsAdded);
            DGVpagos.CellEndEdit += new DataGridViewCellEventHandler(DGVpagos_CellEndEdit);
            DGVpagos.RowsAdded += new DataGridViewRowsAddedEventHandler(DGVpagos_RowsAdded);
            _Cnn = Cnn;
            _Cod_usu = Cod_usu;
            _cod_folio = cod_folio;
            _cta = Cta;
            _oc = oc;
            _total = TotalCXP;
        }
       
        void DGVpagos_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (DGVpagos.Rows[e.RowIndex].Cells["tipo_pago"].Value==null)
            {
                DGVpagos.Rows[e.RowIndex].Cells["tipo_pago"].Value=1;
            }
        }

        void DGVpagos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == estadocol.DisplayIndex)
            {
                DGVpagos.Rows[e.RowIndex].Cells[estadoTmp.DisplayIndex].Value = DGVpagos.Rows[e.RowIndex].Cells[estadocol.DisplayIndex].Value.ToString().Trim();
                if (DGVpagos.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Trim () == "PAGADO")
                {
                    DGVpagos.Rows[e.RowIndex].Cells["estadocol"].ReadOnly = true;
                    DGVpagos.Rows[e.RowIndex].Cells[fecha_pago.DisplayIndex].Value = DateTime.Now;
                }
                else
                {
                    DGVpagos.Rows[e.RowIndex].Cells["estadocol"].ReadOnly = false;
                    DGVpagos.Rows[e.RowIndex].Cells[fecha_pago.DisplayIndex].Value = null;
                }
                ActualzaLabels();
                if (DGVpagos.Rows[e.RowIndex].Cells["Tipo_pago"].Value == null)
                {
                    DGVpagos.Rows[e.RowIndex].Cells["Tipo_pago"].Value = 1;
                    DGVpagos.Rows[e.RowIndex].Cells["Tipopagotmp"].Value = 1;
                }
            }
            if (e.ColumnIndex == Tipo_pago.DisplayIndex)
            {
                DGVpagos.Rows[e.RowIndex].Cells["Tipopagotmp"].Value = DGVpagos.Rows[e.RowIndex].Cells["Tipo_pago"].Value;
            }
            if (DGVpagos.Rows[e.RowIndex].Cells["cod_banco"].Value == null)
            {
                DGVpagos.Rows[e.RowIndex].Cells["cod_banco"].Value = 1;
            }
        }

        void DGVdetalle_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
           //DGVdetalle.Rows[e.RowIndex ].Cells[ArticuloCol.DisplayIndex].Value = CuentaxPagar.ZctEncOC.ZctDetOC[e.RowIndex].ZctCatArt.Desc_Art;
            //
           //DGVdetalle.Rows[e.RowIndex].Cells["colimporte"].Value = (Convert.ToDecimal(DGVdetalle.Rows[e.RowIndex].Cells["cosArtDataGridViewTextBoxColumn"].Value) * Convert.ToDecimal(DGVdetalle.Rows[e.RowIndex].Cells["ctdArtDataGridViewTextBoxColumn"].Value));
       }

        void DGVpagos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            
        }

        void Txt_LostFocus(object sender, EventArgs e)
        {
            Control MiControl = (Control)sender;
            try
            {
                if (MiControl.Text == "0" || string.IsNullOrEmpty(MiControl.Text) || string.IsNullOrWhiteSpace(MiControl.Text))
                {
                    return;
                }
                if (MiControl.Name == TxtFolio.Name )
                {
                    CuentaxPagar = _ControladorCxP.ValidaCxP(Convert.ToInt32(TxtFolio.Text));
                }
                else if (MiControl.Name == TxtPedido.Name)
                {
                    CuentaxPagar = _ControladorCxP.ValidaOcCxP(Convert.ToInt32(TxtPedido.Text));
                }
                if (CuentaxPagar.estado.ToLower() == "cancelado")
                {

                }
                //CargarCuenta();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void CargarCuenta()
        {
            
            TxtFolio.Text = _cta.ToString();
            TxtPedido.Text = _oc.ToString();
            CuentaxPagar= _ControladorCxP.ValidaCxP(_cta);
            BsCxP.DataSource = CuentaxPagar;
            BsPagosCxP.DataSource = CuentaxPagar.ZctDetCuentasPagar;
            BsPagosCxP.DataSource = CuentaxPagar.ZctDetCuentasPagar;
            BsDetalleOC.DataSource = CuentaxPagar.ZctEncOC.ZctDetOC;
            DGVdetalle.DataSource = BsDetalleOC;
            ArticuloCol.DataPropertyName = "ArticuloNombre";
            ImporteCol.DataPropertyName = "Importe";
            Lcliente.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.Nom_Prov;
            LRFC.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.RFC_Prov;
            Lemail.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.email;
            Ldireccion.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.DireccionProveedor;
            totalPagar = _total;
            BsEstadoPagos.DataSource = _ControladorCxP.TraeEstadosPago();
            //CargarEstadoPagos();
            ActualzaLabels();
            
        }

        public void ActualzaLabels()
        {
            try
            {
                decimal pagado = DGVpagos.Rows.Cast<DataGridViewRow>().Where(w => w.Cells["estadoTmp"].Value.ToString().Trim() == "PAGADO").Sum(x => Convert.ToDecimal(x.Cells["montoDataGridViewTextBoxColumn"].Value));
                Lpago.Text = string.Format("{0:C}", pagado);
                Lrestante.Text = string.Format("{0:C}", (totalPagar - pagado));
                if ((totalPagar == pagado))
                {
                    CuentaxPagar.estado = "PAGADO";

                }
                else
                {
                    CuentaxPagar.estado = "PENDIENTE";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion Actualizando labels");
            }
        }

      
        private void CxP_Load(object sender, EventArgs e)
        {
            _ControladorCaja = new Controlador.Controladorcaja(_Cnn,_cod_folio,_Cod_usu);
            _ControladorPermisos = new Permisos.Controlador.Controlador(_Cnn);
            _Permisos = _ControladorPermisos.getpermisoventana(_Cod_usu, this.Text);
            _ControladorCxP = new Controlador.ControladorCxP(_Cnn);
            TiposPagoBindingSource.DataSource = _ControladorCaja.TraeTiposPago();
            BsBancos.DataSource = _ControladorCxP.TraeBancos();
            Tipo_pago.DataPropertyName = "codtipo_pago";
            Tipo_pago.DataSource = TiposPagoBindingSource;
            Tipo_pago.DisplayMember = "desc_pago";
            Tipo_pago.ValueMember = "codtipo_pago";
            CargarCuenta();

            Limpiar();
        }
        private  void Limpiar()
    {
        //TxtFolio.Text = _ControladorCxP.TraeFolioCxP().ToString();
    }

        private void Bgrabar_Click(object sender, EventArgs e)
        {
            try
            {
                _ControladorCxP.Graba(CuentaxPagar);
              DialogResult resp=  MessageBox.Show("Pago guardado correctamente ¿Desea cerrar el detalle de la cuenta por pagar?", "Hecho", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
              if (resp == DialogResult.Yes)
              { 
                  _ControladorCxP.Refresca();
                  CxPprincipal.Cargar();
                  this.Dispose();
              }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error guardando pagos " + ex.Message , "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

       

        public void ActualizaProveedor(int cod_prov)
        {
           CuentaxPagar.ZctEncOC.ZctCatProv1= _ControladorCxP.CambiaProveedor(cod_prov);
           Lcliente.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.Nom_Prov;
           LRFC.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.RFC_Prov;
           Lemail.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.email;
           Ldireccion.Text = CuentaxPagar.ZctEncOC.ZctCatProv1.DireccionProveedor;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (CuentaxPagar != null)
            {
                CxPprincipal.modificaproveedor(CuentaxPagar.ZctEncOC.ZctCatProv1.Cod_Prov);
            }
            else
            {
                MessageBox.Show("Seleccione una CxP", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Bcancelar_Click(object sender, EventArgs e)
        {
            _ControladorCxP.Refresca();
            BsEstadoPagos.ResetCurrentItem();
        }

    }
}
