﻿namespace ResTotal.Vista
{
    partial class frmFacturacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Bsig = new System.Windows.Forms.Button();
            this.Bant = new System.Windows.Forms.Button();
            this.TxtFolio = new System.Windows.Forms.TextBox();
            this.zctFacturaEncBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Lstatus = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmdCambiaDatosCliente = new System.Windows.Forms.Button();
            this.Lemail = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Ldireccion = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LRFC = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Lcliente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.DGVdetalle = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Ltotal = new System.Windows.Forms.Label();
            this.Liva = new System.Windows.Forms.Label();
            this.Lsubtotal = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cmdReenviar = new System.Windows.Forms.Button();
            this.cmdImprimir = new System.Windows.Forms.Button();
            this.cmdLimpiar = new System.Windows.Forms.Button();
            this.cmdCancelar = new System.Windows.Forms.Button();
            this.cmdGrabar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtPedido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.concepto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iVADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctdartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codartDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctFacturaDetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zctFacturaEncBindingSource)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalle)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zctFacturaDetBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Bsig);
            this.groupBox1.Controls.Add(this.Bant);
            this.groupBox1.Controls.Add(this.TxtFolio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(333, 49);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // Bsig
            // 
            this.Bsig.Location = new System.Drawing.Point(276, 12);
            this.Bsig.Name = "Bsig";
            this.Bsig.Size = new System.Drawing.Size(37, 29);
            this.Bsig.TabIndex = 3;
            this.Bsig.Text = ">";
            this.Bsig.UseVisualStyleBackColor = true;
            this.Bsig.Click += new System.EventHandler(this.Bsig_Click);
            // 
            // Bant
            // 
            this.Bant.Location = new System.Drawing.Point(239, 12);
            this.Bant.Name = "Bant";
            this.Bant.Size = new System.Drawing.Size(37, 29);
            this.Bant.TabIndex = 2;
            this.Bant.Text = "<";
            this.Bant.UseVisualStyleBackColor = true;
            this.Bant.Click += new System.EventHandler(this.Bant_Click);
            // 
            // TxtFolio
            // 
            this.TxtFolio.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "folio_factura", true));
            this.TxtFolio.Location = new System.Drawing.Point(49, 16);
            this.TxtFolio.Name = "TxtFolio";
            this.TxtFolio.Size = new System.Drawing.Size(184, 20);
            this.TxtFolio.TabIndex = 1;
            this.TxtFolio.Leave += new System.EventHandler(this.TxtFolio_Leave);
            // 
            // zctFacturaEncBindingSource
            // 
            this.zctFacturaEncBindingSource.DataSource = typeof(ResTotal.Modelo.ZctFacturaEnc);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folio:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Lstatus);
            this.groupBox3.Location = new System.Drawing.Point(724, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(241, 49);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            // 
            // Lstatus
            // 
            this.Lstatus.BackColor = System.Drawing.Color.OrangeRed;
            this.Lstatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lstatus.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "status", true));
            this.Lstatus.DataBindings.Add(new System.Windows.Forms.Binding("BackColor", this.zctFacturaEncBindingSource, "status_color", true));
            this.Lstatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lstatus.Location = new System.Drawing.Point(10, 16);
            this.Lstatus.Name = "Lstatus";
            this.Lstatus.Size = new System.Drawing.Size(218, 20);
            this.Lstatus.TabIndex = 0;
            this.Lstatus.Text = "STATUS";
            this.Lstatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cmdCambiaDatosCliente);
            this.groupBox4.Controls.Add(this.Lemail);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.Ldireccion);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.LRFC);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.Lcliente);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(2, 52);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(969, 132);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            // 
            // cmdCambiaDatosCliente
            // 
            this.cmdCambiaDatosCliente.Location = new System.Drawing.Point(851, 60);
            this.cmdCambiaDatosCliente.Name = "cmdCambiaDatosCliente";
            this.cmdCambiaDatosCliente.Size = new System.Drawing.Size(111, 56);
            this.cmdCambiaDatosCliente.TabIndex = 13;
            this.cmdCambiaDatosCliente.Text = "Cambiar Datos";
            this.cmdCambiaDatosCliente.UseVisualStyleBackColor = true;
            this.cmdCambiaDatosCliente.Click += new System.EventHandler(this.cmdCambiaDatosCliente_Click);
            // 
            // Lemail
            // 
            this.Lemail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lemail.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "ZctCatCte.email", true));
            this.Lemail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lemail.Location = new System.Drawing.Point(58, 77);
            this.Lemail.Name = "Lemail";
            this.Lemail.Size = new System.Drawing.Size(281, 22);
            this.Lemail.TabIndex = 12;
            this.Lemail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Correo:";
            // 
            // Ldireccion
            // 
            this.Ldireccion.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Ldireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Ldireccion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "ZctCatCte.direccion", true));
            this.Ldireccion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ldireccion.Location = new System.Drawing.Point(352, 42);
            this.Ldireccion.Name = "Ldireccion";
            this.Ldireccion.Size = new System.Drawing.Size(481, 78);
            this.Ldireccion.TabIndex = 10;
            this.Ldireccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(291, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Dirección:";
            // 
            // LRFC
            // 
            this.LRFC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LRFC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LRFC.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "ZctCatCte.RFC_Cte", true));
            this.LRFC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRFC.Location = new System.Drawing.Point(58, 46);
            this.LRFC.Name = "LRFC";
            this.LRFC.Size = new System.Drawing.Size(227, 22);
            this.LRFC.TabIndex = 8;
            this.LRFC.Text = "XXX010101XXX";
            this.LRFC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "RFC:";
            // 
            // Lcliente
            // 
            this.Lcliente.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lcliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lcliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "ZctCatCte.nombre", true));
            this.Lcliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lcliente.Location = new System.Drawing.Point(58, 14);
            this.Lcliente.Name = "Lcliente";
            this.Lcliente.Size = new System.Drawing.Size(775, 22);
            this.Lcliente.TabIndex = 6;
            this.Lcliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cliente:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.DGVdetalle);
            this.groupBox5.Location = new System.Drawing.Point(2, 184);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(969, 233);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            // 
            // DGVdetalle
            // 
            this.DGVdetalle.AllowUserToAddRows = false;
            this.DGVdetalle.AllowUserToDeleteRows = false;
            this.DGVdetalle.AutoGenerateColumns = false;
            this.DGVdetalle.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DGVdetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVdetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codartDataGridViewTextBoxColumn,
            this.concepto,
            this.ctdartDataGridViewTextBoxColumn,
            this.subtotalDataGridViewTextBoxColumn,
            this.iVADataGridViewTextBoxColumn,
            this.totalDataGridViewTextBoxColumn});
            this.DGVdetalle.DataSource = this.zctFacturaDetBindingSource1;
            this.DGVdetalle.Location = new System.Drawing.Point(6, 13);
            this.DGVdetalle.MultiSelect = false;
            this.DGVdetalle.Name = "DGVdetalle";
            this.DGVdetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.DGVdetalle.Size = new System.Drawing.Size(957, 214);
            this.DGVdetalle.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Ltotal);
            this.groupBox7.Controls.Add(this.Liva);
            this.groupBox7.Controls.Add(this.Lsubtotal);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Location = new System.Drawing.Point(4, 420);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(346, 123);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            // 
            // Ltotal
            // 
            this.Ltotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "importe_factura", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "C2"));
            this.Ltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ltotal.ForeColor = System.Drawing.Color.Navy;
            this.Ltotal.Location = new System.Drawing.Point(135, 88);
            this.Ltotal.Name = "Ltotal";
            this.Ltotal.Size = new System.Drawing.Size(184, 25);
            this.Ltotal.TabIndex = 5;
            this.Ltotal.Text = "$0.00";
            this.Ltotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Liva
            // 
            this.Liva.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "iva_factura", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "C2"));
            this.Liva.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Liva.ForeColor = System.Drawing.Color.Navy;
            this.Liva.Location = new System.Drawing.Point(134, 54);
            this.Liva.Name = "Liva";
            this.Liva.Size = new System.Drawing.Size(184, 23);
            this.Liva.TabIndex = 4;
            this.Liva.Text = "$0.00";
            this.Liva.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Lsubtotal
            // 
            this.Lsubtotal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "subtotal", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "C2"));
            this.Lsubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lsubtotal.ForeColor = System.Drawing.Color.Navy;
            this.Lsubtotal.Location = new System.Drawing.Point(135, 17);
            this.Lsubtotal.Name = "Lsubtotal";
            this.Lsubtotal.Size = new System.Drawing.Size(184, 24);
            this.Lsubtotal.TabIndex = 3;
            this.Lsubtotal.Text = "$0.00";
            this.Lsubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(74, 52);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 24);
            this.label14.TabIndex = 2;
            this.label14.Text = "IVA:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(17, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 24);
            this.label13.TabIndex = 1;
            this.label13.Text = "Sub Total:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(60, 88);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 27);
            this.label12.TabIndex = 0;
            this.label12.Text = "Total:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cmdReenviar);
            this.groupBox8.Controls.Add(this.cmdImprimir);
            this.groupBox8.Controls.Add(this.cmdLimpiar);
            this.groupBox8.Controls.Add(this.cmdCancelar);
            this.groupBox8.Controls.Add(this.cmdGrabar);
            this.groupBox8.Location = new System.Drawing.Point(400, 437);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(571, 83);
            this.groupBox8.TabIndex = 15;
            this.groupBox8.TabStop = false;
            // 
            // cmdReenviar
            // 
            this.cmdReenviar.Location = new System.Drawing.Point(121, 19);
            this.cmdReenviar.Name = "cmdReenviar";
            this.cmdReenviar.Size = new System.Drawing.Size(103, 43);
            this.cmdReenviar.TabIndex = 21;
            this.cmdReenviar.Text = "Reenviar Correo";
            this.cmdReenviar.UseVisualStyleBackColor = true;
            this.cmdReenviar.Click += new System.EventHandler(this.cmdReenviar_Click);
            // 
            // cmdImprimir
            // 
            this.cmdImprimir.Location = new System.Drawing.Point(8, 19);
            this.cmdImprimir.Name = "cmdImprimir";
            this.cmdImprimir.Size = new System.Drawing.Size(103, 43);
            this.cmdImprimir.TabIndex = 20;
            this.cmdImprimir.Text = "Imprimir";
            this.cmdImprimir.UseVisualStyleBackColor = true;
            this.cmdImprimir.Click += new System.EventHandler(this.cmdImprimir_Click);
            // 
            // cmdLimpiar
            // 
            this.cmdLimpiar.Location = new System.Drawing.Point(454, 19);
            this.cmdLimpiar.Name = "cmdLimpiar";
            this.cmdLimpiar.Size = new System.Drawing.Size(103, 43);
            this.cmdLimpiar.TabIndex = 17;
            this.cmdLimpiar.Text = "Limpiar";
            this.cmdLimpiar.UseVisualStyleBackColor = true;
            this.cmdLimpiar.Click += new System.EventHandler(this.cmdLimpiar_Click);
            // 
            // cmdCancelar
            // 
            this.cmdCancelar.Location = new System.Drawing.Point(345, 19);
            this.cmdCancelar.Name = "cmdCancelar";
            this.cmdCancelar.Size = new System.Drawing.Size(103, 43);
            this.cmdCancelar.TabIndex = 16;
            this.cmdCancelar.Text = "Cancelar";
            this.cmdCancelar.UseVisualStyleBackColor = true;
            this.cmdCancelar.Click += new System.EventHandler(this.cmdCancelar_Click);
            // 
            // cmdGrabar
            // 
            this.cmdGrabar.Location = new System.Drawing.Point(230, 19);
            this.cmdGrabar.Name = "cmdGrabar";
            this.cmdGrabar.Size = new System.Drawing.Size(103, 43);
            this.cmdGrabar.TabIndex = 15;
            this.cmdGrabar.Text = "Grabar";
            this.cmdGrabar.UseVisualStyleBackColor = true;
            this.cmdGrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TxtPedido);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(400, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(247, 49);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // TxtPedido
            // 
            this.TxtPedido.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctFacturaEncBindingSource, "CodEnc_OT", true));
            this.TxtPedido.Location = new System.Drawing.Point(59, 16);
            this.TxtPedido.Name = "TxtPedido";
            this.TxtPedido.Size = new System.Drawing.Size(176, 20);
            this.TxtPedido.TabIndex = 1;
            this.TxtPedido.Leave += new System.EventHandler(this.TxtPedido_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pedido:";
            // 
            // concepto
            // 
            this.concepto.DataPropertyName = "concepto";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue;
            this.concepto.DefaultCellStyle = dataGridViewCellStyle1;
            this.concepto.HeaderText = "Concepto";
            this.concepto.Name = "concepto";
            this.concepto.Width = 300;
            // 
            // totalDataGridViewTextBoxColumn
            // 
            this.totalDataGridViewTextBoxColumn.DataPropertyName = "Total";
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = null;
            this.totalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.totalDataGridViewTextBoxColumn.HeaderText = "Total";
            this.totalDataGridViewTextBoxColumn.Name = "totalDataGridViewTextBoxColumn";
            this.totalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // iVADataGridViewTextBoxColumn
            // 
            this.iVADataGridViewTextBoxColumn.DataPropertyName = "IVA";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.iVADataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.iVADataGridViewTextBoxColumn.HeaderText = "IVA";
            this.iVADataGridViewTextBoxColumn.Name = "iVADataGridViewTextBoxColumn";
            this.iVADataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // subtotalDataGridViewTextBoxColumn
            // 
            this.subtotalDataGridViewTextBoxColumn.DataPropertyName = "subtotal";
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.subtotalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.subtotalDataGridViewTextBoxColumn.HeaderText = "Subtotal";
            this.subtotalDataGridViewTextBoxColumn.Name = "subtotalDataGridViewTextBoxColumn";
            this.subtotalDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ctdartDataGridViewTextBoxColumn
            // 
            this.ctdartDataGridViewTextBoxColumn.DataPropertyName = "ctd_art";
            this.ctdartDataGridViewTextBoxColumn.HeaderText = "Cantidad";
            this.ctdartDataGridViewTextBoxColumn.Name = "ctdartDataGridViewTextBoxColumn";
            this.ctdartDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codartDataGridViewTextBoxColumn
            // 
            this.codartDataGridViewTextBoxColumn.DataPropertyName = "cod_art";
            this.codartDataGridViewTextBoxColumn.HeaderText = "Código";
            this.codartDataGridViewTextBoxColumn.Name = "codartDataGridViewTextBoxColumn";
            this.codartDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // zctFacturaDetBindingSource1
            // 
            this.zctFacturaDetBindingSource1.DataMember = "ZctFacturaDet";
            this.zctFacturaDetBindingSource1.DataSource = this.zctFacturaEncBindingSource;
            // 
            // frmFacturacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 551);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmFacturacion";
            this.Text = "FACTURACION";
            this.Load += new System.EventHandler(this.frmFacturacion_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zctFacturaEncBindingSource)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalle)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zctFacturaDetBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Bsig;
        private System.Windows.Forms.Button Bant;
        private System.Windows.Forms.TextBox TxtFolio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label Lstatus;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Ldireccion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label LRFC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Lcliente;
        private System.Windows.Forms.Label Lemail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button cmdCambiaDatosCliente;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label Lsubtotal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label Ltotal;
        private System.Windows.Forms.Label Liva;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button cmdImprimir;
        private System.Windows.Forms.Button cmdLimpiar;
        private System.Windows.Forms.Button cmdCancelar;
        private System.Windows.Forms.Button cmdGrabar;
        private System.Windows.Forms.Button cmdReenviar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtPedido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource zctFacturaEncBindingSource;
        private System.Windows.Forms.DataGridView DGVdetalle;
        private System.Windows.Forms.DataGridViewTextBoxColumn concepto;
        private System.Windows.Forms.DataGridViewTextBoxColumn codartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctdartDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iVADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource zctFacturaDetBindingSource1;
    }
}