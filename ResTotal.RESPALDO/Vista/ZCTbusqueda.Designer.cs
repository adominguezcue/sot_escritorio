﻿namespace ResTotal.Vista
{
    partial class ZCTbusqueda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmdAceptar = new System.Windows.Forms.Button();
            this.DGBusqueda = new System.Windows.Forms.DataGridView();
            this.bs = new System.Windows.Forms.BindingSource(this.components);
            this.textbusqueda = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Bcancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGBusqueda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bs)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdAceptar
            // 
            this.cmdAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdAceptar.Location = new System.Drawing.Point(599, 481);
            this.cmdAceptar.Name = "cmdAceptar";
            this.cmdAceptar.Size = new System.Drawing.Size(75, 23);
            this.cmdAceptar.TabIndex = 3;
            this.cmdAceptar.Text = "Aceptar";
            this.cmdAceptar.UseVisualStyleBackColor = true;
            this.cmdAceptar.Click += new System.EventHandler(this.cmdAceptar_Click);
            // 
            // DGBusqueda
            // 
            this.DGBusqueda.AllowUserToAddRows = false;
            this.DGBusqueda.AllowUserToDeleteRows = false;
            this.DGBusqueda.AllowUserToResizeRows = false;
            this.DGBusqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGBusqueda.AutoGenerateColumns = false;
            this.DGBusqueda.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DGBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGBusqueda.DataSource = this.bs;
            this.DGBusqueda.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.DGBusqueda.Location = new System.Drawing.Point(3, 64);
            this.DGBusqueda.MultiSelect = false;
            this.DGBusqueda.Name = "DGBusqueda";
            this.DGBusqueda.ReadOnly = true;
            this.DGBusqueda.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DGBusqueda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGBusqueda.Size = new System.Drawing.Size(753, 404);
            this.DGBusqueda.TabIndex = 4;
            this.DGBusqueda.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGBusqueda_CellContentClick);
            // 
            // textbusqueda
            // 
            this.textbusqueda.Enabled = false;
            this.textbusqueda.Location = new System.Drawing.Point(78, 28);
            this.textbusqueda.Name = "textbusqueda";
            this.textbusqueda.Size = new System.Drawing.Size(290, 20);
            this.textbusqueda.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Columna:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Cyan;
            this.label2.Location = new System.Drawing.Point(425, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(249, 26);
            this.label2.TabIndex = 7;
            this.label2.Text = "Doble Click en el encabezado de columna \r\npara seleccionar la busqueda (No datos " +
    "numericos)";
            // 
            // Bcancelar
            // 
            this.Bcancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bcancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Bcancelar.Location = new System.Drawing.Point(676, 481);
            this.Bcancelar.Name = "Bcancelar";
            this.Bcancelar.Size = new System.Drawing.Size(75, 23);
            this.Bcancelar.TabIndex = 8;
            this.Bcancelar.Text = "Cancelar";
            this.Bcancelar.UseVisualStyleBackColor = true;
            this.Bcancelar.Click += new System.EventHandler(this.Bcancelar_Click);
            // 
            // ZCTbusqueda
            // 
            this.AcceptButton = this.cmdAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Bcancelar;
            this.ClientSize = new System.Drawing.Size(758, 506);
            this.Controls.Add(this.Bcancelar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textbusqueda);
            this.Controls.Add(this.cmdAceptar);
            this.Controls.Add(this.DGBusqueda);
            this.Name = "ZCTbusqueda";
            this.Text = "Busqueda";
            ((System.ComponentModel.ISupportInitialize)(this.DGBusqueda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button cmdAceptar;
        internal System.Windows.Forms.DataGridView DGBusqueda;
        private System.Windows.Forms.TextBox textbusqueda;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource bs;
        internal System.Windows.Forms.Button Bcancelar;
    }
}