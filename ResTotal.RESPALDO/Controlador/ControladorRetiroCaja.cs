﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ResTotal.Controlador
{
    class ControladorRetiroCaja
    {
        Modelo.DAODataContext _dao;
        public  ControladorRetiroCaja(string Cnn)
        {
            _dao = new Modelo.DAODataContext(Cnn);
        }
        public Modelo.ZctCatPar TraerParametros()
       {
           return (from Modelo.ZctCatPar p in _dao.ZctCatPar select p).ToList()[0];
       }
        public int RetiroCaja(string Cod_folioCaja, decimal Monto, int cod_usu)
        {
            Modelo.ZctRetirosCajas Retiro = new Modelo.ZctRetirosCajas();
            Retiro.cod_folio= Cod_folioCaja;
            Retiro.cod_usuario = cod_usu;
            Retiro.monto = Monto;
            Retiro.fecha_retiro = DateTime.Now;
            Retiro.corte = false;
            _dao.ZctRetirosCajas.InsertOnSubmit(Retiro);
            
            _dao.SubmitChanges();
            return (from Modelo.ZctRetirosCajas r in _dao.ZctRetirosCajas
                    where r.cod_folio == Cod_folioCaja
                    select r.cod_retiro).Max();
        }
        public decimal TraeTotalRetirosCaja(string cod_folio, int cod_user)
        {
            var Retiros = (from r in _dao.ZctRetirosCajas where r.cod_usuario == cod_user  && r.cod_folio == cod_folio && r.corte==false  select r.monto).ToList();
            if (Retiros.Count == 0)
            {
                return 0;
            }
            return (from r in _dao.ZctRetirosCajas where r.cod_usuario == cod_user && r.cod_folio == cod_folio && r.corte ==false select r.monto).Sum();
        }
    }
}
