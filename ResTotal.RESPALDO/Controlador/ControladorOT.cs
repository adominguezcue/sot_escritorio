﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ResTotal.Controlador
{
    public class ControladorOT
    {
        Modelo.DAODataContext _dao;
        public ControladorOT(string _cnn)
        {
            _dao = new Modelo.DAODataContext(_cnn);
        }
        Modelo.VW_ZctStatusOT ot;
        private Modelo.VW_ZctStatusOT get_ot(int folio)
        {
            if (ot == null || ot.CodEnc_OT != folio)
                ot = (from o in _dao.VW_ZctStatusOT where o.CodEnc_OT == folio select o).FirstOrDefault();
            return ot;
        }
        public string getStatusOT(int folio)
        {
            Modelo.VW_ZctStatusOT ot = get_ot(folio);

            if (ot == null)
                return "Por grabar";
            else
                if (ot.factura == 1 && ot.caja == 1)
                    return "Facturado y pagado";
                else
                    if (ot.factura == 1 && ot.caja == 0)
                        return "Facturado sin pagar";
                    else
                        if (ot.factura == 0 && ot.caja == 0)
                            return "Sin facturar, sin pagar";
                        else
                            if (ot.factura == 0 && ot.caja == 1)
                                return "Sin facturar, pagado";
                            else
                                return "No definido";

        }

        public System.Drawing.Color getColorOT(int folio)
        {
            Modelo.VW_ZctStatusOT ot = get_ot(folio);

            if (ot == null)
                return Color.Silver;
            else
                if (ot.factura == 1 && ot.caja == 1)

                    return Color.FromArgb(128, 255, 128);
                else
                    if (ot.factura == 1 && ot.caja == 0)
                        return Color.FromArgb(255, 255, 128);
                    else
                        if (ot.factura == 0 && ot.caja == 0)
                            return Color.FromArgb(255, 128, 128);
                        else
                            if (ot.factura == 0 && ot.caja == 1)
                                return Color.FromArgb(255, 255, 128);
                            else
                                return Color.IndianRed;

        }

        public Boolean PermiteGrabar(int folio)
        {
            Modelo.VW_ZctStatusOT ot = get_ot(folio);
            if (ot == null)
                return true;
            else
                if (ot.factura == 1 && ot.caja == 1)
                    return false;
                else
                    if (ot.factura == 1 && ot.caja == 0)
                        return false;
                    else
                        if (ot.factura == 0 && ot.caja == 0)
                            return true;
                        else
                            if (ot.factura == 0 && ot.caja == 1)
                                return true;
                            else
                                return false;

        }
        public Boolean PermiteEditarGrid(int folio)
        {
            Modelo.VW_ZctStatusOT ot = get_ot(folio);
            if (ot == null)
                return true;
            else
                if (ot.factura == 1 && ot.caja == 1)
                    return false;
                else
                    if (ot.factura == 1 && ot.caja == 0)
                        return false;
                    else
                        if (ot.factura == 0 && ot.caja == 0)
                            return true;
                        else
                            if (ot.factura == 0 && ot.caja == 1)
                                return false;
                            else
                                return false;

        }
    }
}
