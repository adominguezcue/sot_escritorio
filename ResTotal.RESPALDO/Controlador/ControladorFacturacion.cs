﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Text;
using Gma.QrCodeNet.Encoding.Windows.Forms;

namespace ResTotal.Controlador
{
    public class ControladorFacturacion
    {
        ResTotal.Modelo.DAODataContext dao;
        public ControladorFacturacion(string conn)
        {
            dao = new ResTotal.Modelo.DAODataContext(conn);
        }
        public FacturacionElectronica.Procesa.TimbrarCFDPrueba timbra;
        public Modelo.ZctFacturaEnc obtiene_objeto_factura(int folio_ot, string cod_folio_factura)
        {


            Modelo.ZctFacturaEnc factura = new Modelo.ZctFacturaEnc();
            var facturas = (from f in dao.ZctFacturaEnc where f.CodEnc_OT == folio_ot orderby f.fecha_factura descending select f);
            if (facturas.Count() == 0)
            {

                factura = obtiene_factura_desde_ot(folio_ot, cod_folio_factura);
            }
            else
            {
                factura = facturas.First();
                factura.nueva = false;
            }

            return factura;
        }

        public void metodo_pago(Modelo.ZctFacturaEnc factura)
        {
            var pago = (from p in dao.ZctPagoCaja
                        join c in dao.ZctEncCaja on p.CodEnc_Caja equals c.CodEnc_Caja
                        where c.CodEnc_OT == factura.CodEnc_OT
                        select p).FirstOrDefault();

            if (pago != null)
            {
                //var pago = pagos.First();
                //esto no es correcto y es temporal, espero...
                if (pago.codtipo_pago == 1)
                {
                    factura.metodo_pago = "Efectivo";
                }
                else
                {
                    factura.metodo_pago = "Tarjeta de crédito";
                    factura.cuenta = pago.cuenta;
                }
            }
            else
            {
                factura.metodo_pago = "NO IDENTIFICADO";
            }

        }

        public Modelo.ZctFacturaEnc obtiene_factura_desde_ot(int folio_ot, string cod_folio_factura)
        {
            Modelo.ZctFacturaEnc factura = get_encabezado_factura((from e in dao.ZctEncOT
                                                                   where e.CodEnc_OT == folio_ot
                                                                   select e).SingleOrDefault(), cod_folio_factura);

            metodo_pago(factura);

            factura.ZctFacturaDet.AddRange(get_detalles_factura((from d in dao.ZctDetOT
                                                                 where d.Cod_EncOT == folio_ot && d.CtdStd_DetOT > 0
                                                                 select d).ToList(), cod_folio_factura));


            factura.importe_factura = factura.ZctFacturaDet.Sum(x => x.Total);
            factura.iva_factura = factura.ZctFacturaDet.Sum(x => x.IVA);
            factura.nueva = true;
            return factura;
        }

        public Modelo.ZctFacturaEnc get_encabezado_factura(Modelo.ZctEncOT e, string cod_folio_factura)
        {

            if (e == null)
                throw new ArgumentException("El pedido no se encuentra, verifique por favor.");

            Modelo.ZctFacturaEnc encabezado = nueva_factura(cod_folio_factura);
            encabezado.ZctCatCte = e.ZctCatCte;
            encabezado.ZctEncOT = e;

            encabezado.CodEnc_OT = e.CodEnc_OT;
            encabezado.ZctCatfolios = (from f in dao.ZctCatfolios where f.Cod_Folio == cod_folio_factura select f).First();
            encabezado.cod_cte = e.Cod_Cte.GetValueOrDefault();
            return encabezado;
        }

        public Modelo.ZctFacturaEnc nueva_factura(string cod_folio_factura)
        {
            return new Modelo.ZctFacturaEnc()
            {
                fecha_factura = DateTime.Now,
                cod_folio = cod_folio_factura,
                fhmod_factura = DateTime.Now,
                nueva = true,
                folio_factura = dao.SP_ZctCatFolios(1, cod_folio_factura).SingleOrDefault().Cons.GetValueOrDefault()
            };
        }
        public List<Modelo.ZctFacturaDet> get_detalles_factura(List<Modelo.ZctDetOT> detalle, string cod_folio_factura)
        {
            List<Modelo.ZctFacturaDet> detalles_factura = new List<Modelo.ZctFacturaDet>();
            detalle.ForEach(x => detalles_factura.Add(get_detalle_factura(x, cod_folio_factura)));
            return detalles_factura;
        }
        public Modelo.ZctFacturaDet get_detalle_factura(Modelo.ZctDetOT d, string cod_folio_factura)
        {
            var subtotal = d.PreArt_DetOT.GetValueOrDefault() * d.CtdStd_DetOT.GetValueOrDefault();
            var iva = Helpers.Parametros.calcula_iva(subtotal, dao);

            return new Modelo.ZctFacturaDet
            {
                cod_art = d.Cod_Art,
                ZctCatArt = d.ZctCatArt, //(from a in dao.ZctCatArt where a.Cod_Art == d.Cod_Art select a).First();
                Cod_DetOT = d.Cod_DetOT,
                cod_folio = cod_folio_factura,
                codEnc_OT = d.Cod_EncOT,
                concepto = d.ZctCatArt.Desc_Art,
                ctd_art = d.CtdStd_DetOT.GetValueOrDefault(),
                subtotal = subtotal,
                IVA = iva,
                //Total = detalle.subtotal + detalle.IVA,
                Total = subtotal + iva
            };
        }

        public void inserta_objeto_factura(Modelo.ZctFacturaEnc factura_encabezado)
        {
            valida_modelo_factura(factura_encabezado);
            using (TransactionScope ts = new TransactionScope())
            {
                string pipe = "";
                if (factura_encabezado.nueva)
                {
                    factura_encabezado.folio_factura = dao.SP_ZctCatFolios(2, factura_encabezado.cod_folio).SingleOrDefault().Cons.GetValueOrDefault();
                    factura_encabezado.status_factura = 'F';
                    dao.ZctFacturaEnc.InsertOnSubmit(factura_encabezado);
                }

                pipe = firma_factura(factura_encabezado);
                string sello = FacturacionElectronica.Procesa.GetSello(factura_encabezado.folio_factura.ToString());
                Guid folio_fiscal = FacturacionElectronica.Procesa.GetFolioFiscal(factura_encabezado.folio_factura.ToString());
                DateTime fecha_fiscal = FacturacionElectronica.Procesa.GetFechaFiscal(factura_encabezado.folio_factura.ToString());
                string Sello_Digital_Emisor = FacturacionElectronica.Procesa.GetSelloDigitalEmisor(factura_encabezado.folio_factura.ToString());
                string Sello_Digital_Sat = FacturacionElectronica.Procesa.GetSelloDigitalSAT(factura_encabezado.folio_factura.ToString());
                if (factura_encabezado.ZctFacturaComprobante == null)
                {
                    Modelo.ZctFacturaComprobante comprobante = new Modelo.ZctFacturaComprobante();
                    comprobante.cadena_factura = pipe;
                    comprobante.cod_folio = factura_encabezado.cod_folio;
                    comprobante.fecha_factura = DateTime.Now;
                    comprobante.folio_factura = factura_encabezado.folio_factura;
                    comprobante.folio_fiscal_factura = folio_fiscal;
                    comprobante.sello_factura = sello;
                    comprobante.fyh_fiscal = fecha_fiscal;
                    comprobante.fyh_mod = DateTime.Now;
                    comprobante.sello_digital_emisor = Sello_Digital_Emisor;
                    comprobante.sello_digital_sat = Sello_Digital_Sat;
                    comprobante.status_factura = 'F';
                    dao.ZctFacturaComprobante.InsertOnSubmit(comprobante);
                    dao.SubmitChanges();
                }
                ts.Complete();
            }

        }

        public void genera_imagen_qr(string folio, int factura)
        {
            Modelo.ZctFacturaEnc factura_encabezado = this.obtiene_factura(factura, folio);
            var cat_folio = dao.ZctCatfolios.Where(x => x.Cod_Folio == folio).Single();
            string qr_path = GetRutaFacturas(factura_encabezado) + factura_encabezado.folio_factura.ToString() + ".jpg";
            string qr_path_rpt = factura_encabezado.ZctCatfolios.FactEPath_folio + "qr.jpg";
            System.Data.Linq.Binary file_binary = new System.Data.Linq.Binary(GeneraQR(cadena_qr(factura_encabezado), qr_path, qr_path_rpt, new System.Drawing.Size(350, 350)));
            //System.Drawing.Image imagen = GeneraQR(cadena_qr(factura_encabezado), new System.Drawing.Size(350, 350));

            cat_folio.image_factura = file_binary;
            dao.SubmitChanges();



        }


        private string cadena_qr(Modelo.ZctFacturaEnc factura)
        {
            return string.Format("?re={0}&rr={1}&tt={2}&id={3}", factura.ZctCatCte.RFC_Cte, factura.ZctCatfolios.ZctCatEmisor.RFC, factura.importe_factura.ToString("0000000000.000000"), factura.ZctFacturaComprobante.folio_fiscal_factura);

        }

        private void valida_modelo_factura(Modelo.ZctFacturaEnc factura_encabezado)
        {
            if (factura_encabezado.ZctCatCte.RFC_Cte == "" || factura_encabezado.ZctCatCte.RFC_Cte == null)
            {
                throw new ArgumentException("El RFC del cliente no ha sido proporcionado");
            }

            if (factura_encabezado.ZctCatCte.calle == string.Empty || factura_encabezado.ZctCatCte.calle == null)
            {
                throw new ArgumentException("La calle del cliente no ha sido proporcionada");
            }

            if (factura_encabezado.ZctCatCte.colonia == string.Empty || factura_encabezado.ZctCatCte.colonia == null)
            {
                throw new ArgumentException("La colonia del cliente no ha sido proporcionada");
            }
            if (factura_encabezado.ZctCatCte.numext == string.Empty || factura_encabezado.ZctCatCte.numext == null)
            {
                throw new ArgumentException("El número exterior del cliente no ha sido proporcionada");
            }


            if (factura_encabezado.ZctCatCte.ZctCatCiudad.Desc_Ciu == string.Empty || factura_encabezado.ZctCatCte.ZctCatCiudad.Desc_Ciu == null)
            {
                throw new ArgumentException("La ciudad del cliente no ha sido proporcionada");
            }


            if (factura_encabezado.ZctCatCte.ZctCatEstado.Desc_Edo == string.Empty || factura_encabezado.ZctCatCte.ZctCatEstado.Desc_Edo == null)
            {
                throw new ArgumentException("El estado del cliente no ha sido proporcionada");
            }

            if (factura_encabezado.ZctCatCte.pais == string.Empty || factura_encabezado.ZctCatCte.pais == null)
            {
                throw new ArgumentException("El pais del cliente no ha sido proporcionada");
            }

            if (factura_encabezado.ZctCatCte.cp == "")
            {
                throw new ArgumentException("El CP del cliente no ha sido proporcionada");
            }

            if (factura_encabezado.cod_folio == string.Empty || factura_encabezado.cod_folio == null)
            {
                throw new ArgumentException("El tipo de folio de la factura no ha sido proporcionada");
            }

            if (factura_encabezado.folio_factura == 0)
            {

                throw new ArgumentException("El folio de la factura no ha sido proporcionada");

            }



            if (factura_encabezado.ZctFacturaDet.Count == 0)
            {
                throw new ArgumentException("El detalle de la factura no ha sido proporcionada");
            }



            foreach (Modelo.ZctFacturaDet det in factura_encabezado.ZctFacturaDet.ToList())
            {
                if (det.cod_art == string.Empty || det.cod_art == null)
                    throw new ArgumentException("El detalle de la factura debe de poporcionar un artículo válido");
            }





        }

        public Modelo.ZctFacturaEnc obtiene_factura(int folio, string cod_folio_factura)
        {
            return (from e in dao.ZctFacturaEnc where e.folio_factura == folio && e.cod_folio == cod_folio_factura select e).SingleOrDefault();
        }



        public FacturacionElectronica.cFacturaCFDI obtiene_objeto_factura_cfdi(Modelo.ZctFacturaEnc factura_encabezado)
        {
            FacturacionElectronica.cFacturaCFDI cfdi = new FacturacionElectronica.cFacturaCFDI();
            cfdi.fecha = factura_encabezado.fecha_factura;
            cfdi.FechaFolioFiscalOrig = DateTime.Now;
            cfdi.folio = factura_encabezado.folio_factura.ToString();
            //TODO: definir forma de pago.
            //cdi.formaDePago = factura_encabezado.ZctEncOT.ZctEncCaja.First().ZctPagoCaja.First(
            cfdi.formaDePago = "PAGO EN UNA SOLA EXHIBICION";
            cfdi.MontoFolioFiscalOrig = factura_encabezado.importe_factura;
            cfdi.tipoDeComprobante = "ingreso";

            cfdi.Moneda = "MXP";
            cfdi.TipoCambio = "1.0";
            //cfdi.metodoDePago="NO IDENTIFICADO";
            cfdi.metodoDePago = factura_encabezado.metodo_pago;
            if (factura_encabezado.cuenta != "")
            {
                cfdi.NumCtaPago = factura_encabezado.cuenta;
            }

            cfdi.sello = "";
            cfdi.certificado = "";

            cfdi.noCertificado = factura_encabezado.ZctCatfolios.FactCertxt_folio;
            //cfdi.NumCtaPago = factura_encabezado.num_cta_pago;
            if (factura_encabezado.ZctFacturaComprobante != null)
                cfdi.sello = factura_encabezado.ZctFacturaComprobante.sello_factura;

            cfdi.Serie = factura_encabezado.cod_folio;
            //cfdi.SerieFolioFiscalOrig = factura_encabezado.ZctFacturaComprobante
            cfdi.subTotal = factura_encabezado.subtotal;
            cfdi.total = factura_encabezado.importe_factura;

            cfdi.Emisor = getEmisor(factura_encabezado);
            cfdi.LugarExpedicion = factura_encabezado.ZctCatfolios.ZctCatEmisor.ZctCatCiudad.Desc_Ciu + " " + factura_encabezado.ZctCatfolios.ZctCatEmisor.ZctCatEstado.Desc_Edo;
            cfdi.Receptor = getReceptor(factura_encabezado);
            cfdi.Conceptos = getConceptos(factura_encabezado);
            cfdi.Impuestos.Traslados.AddRange(getImpuestos(factura_encabezado));
            return cfdi;
        }

        private FacturacionElectronica.cEmisor getEmisor(Modelo.ZctFacturaEnc factura_encabezado)
        {

            FacturacionElectronica.cEmisor Emisor = new FacturacionElectronica.cEmisor();
            Modelo.ZctCatEmisor emisor = factura_encabezado.ZctCatfolios.ZctCatEmisor;
            Emisor.nombre = emisor.Nombre;
            Emisor.RFC = emisor.RFC.ToUpper();
            Emisor.DomicilioFiscal.Calle = emisor.Calle;
            Emisor.DomicilioFiscal.CodigoPostal = emisor.CP.GetValueOrDefault().ToString();
            Emisor.DomicilioFiscal.colonia = emisor.Colonia;
            Emisor.DomicilioFiscal.estado = emisor.ZctCatEstado.Desc_Edo;
            Emisor.DomicilioFiscal.localidad = emisor.ZctCatCiudad.Desc_Ciu;
            Emisor.DomicilioFiscal.municipio = emisor.ZctCatCiudad.Desc_Ciu;
            Emisor.DomicilioFiscal.noexterior = emisor.NoExterior;
            if (emisor.NoInterior != null && emisor.NoInterior != "")
                Emisor.DomicilioFiscal.nointerior = emisor.NoInterior;

            Emisor.DomicilioFiscal.pais = emisor.Pais;
            Emisor.RegimenFiscal.Regimen = emisor.regimen;
            Emisor.ExpedidoEn.Calle = emisor.Calle;
            Emisor.ExpedidoEn.CodigoPostal = emisor.CP.GetValueOrDefault().ToString();
            Emisor.ExpedidoEn.colonia = emisor.Colonia;
            Emisor.ExpedidoEn.estado = emisor.ZctCatEstado.Desc_Edo;
            Emisor.ExpedidoEn.municipio = emisor.ZctCatCiudad.Desc_Ciu;
            Emisor.ExpedidoEn.noexterior = emisor.NoInterior;
            Emisor.ExpedidoEn.pais = emisor.Pais;

            return Emisor;

        }

        private FacturacionElectronica.cReceptor getReceptor(Modelo.ZctFacturaEnc factura_encabezado)
        {
            FacturacionElectronica.cReceptor receptor = new FacturacionElectronica.cReceptor();
            receptor.nombre = factura_encabezado.ZctCatCte.nombre;
            receptor.RFC = factura_encabezado.ZctCatCte.RFC_Cte.ToUpper();
            receptor.Domicilio.Calle = factura_encabezado.ZctCatCte.calle;
            receptor.Domicilio.CodigoPostal = factura_encabezado.ZctCatCte.cp.ToString();
            receptor.Domicilio.colonia = factura_encabezado.ZctCatCte.colonia;
            receptor.Domicilio.estado = factura_encabezado.ZctCatCte.ZctCatEstado.Desc_Edo;
            receptor.Domicilio.localidad = factura_encabezado.ZctCatCte.ZctCatCiudad.Desc_Ciu;
            receptor.Domicilio.municipio = factura_encabezado.ZctCatCte.ZctCatCiudad.Desc_Ciu;
            receptor.Domicilio.noexterior = factura_encabezado.ZctCatCte.numext;
            if (factura_encabezado.ZctCatCte.numint != null && factura_encabezado.ZctCatCte.numint != "")
                receptor.Domicilio.nointerior = factura_encabezado.ZctCatCte.numint;
            receptor.Domicilio.pais = factura_encabezado.ZctCatCte.pais;

            return receptor;
        }
        private List<FacturacionElectronica.Traslado> getImpuestos(Modelo.ZctFacturaEnc factura_encabezado)
        {
            List<FacturacionElectronica.Traslado> tras = new List<FacturacionElectronica.Traslado>();
            FacturacionElectronica.Traslado tra = new FacturacionElectronica.Traslado();
            tra.Importe = factura_encabezado.iva_factura;
            tra.Impuesto = "IVA";
            tra.Tasa = Helpers.Parametros.IVA(dao) * 100;
            tras.Add(tra);
            return tras;
        }
        private List<FacturacionElectronica.Concepto> getConceptos(Modelo.ZctFacturaEnc factura_encabezado)
        {
            List<FacturacionElectronica.Concepto> ls = new List<FacturacionElectronica.Concepto>();

            foreach (Modelo.ZctFacturaDet det in factura_encabezado.ZctFacturaDet)
            {
                if (det.ctd_art > 0)
                {
                    FacturacionElectronica.Concepto cp1 = new FacturacionElectronica.Concepto();
                    cp1.cantidad = det.ctd_art;
                    cp1.descripcion = det.concepto.Replace("|", ""); //det.ZctCatArt.Desc_Art.Replace("|", "");
                    cp1.unidad = "NO APLICA";
                    cp1.importe = det.subtotal;   //rowco.Importe - rowco.Iva
                    cp1.valorUnitario = det.subtotal / det.ctd_art;
                    ls.Add(cp1);
                }


            }
            return ls;

        }

        public string firma_factura(Modelo.ZctFacturaEnc factura_encabezado)
        {

            FacturacionElectronica.cFacturaCFDI cfdi = this.obtiene_objeto_factura_cfdi(factura_encabezado);
            FacturacionElectronica.Procesa.path = factura_encabezado.ZctCatfolios.FactEPath_folio;
            FacturacionElectronica.Procesa.KeyFile = factura_encabezado.ZctCatfolios.FactEKey_folio;
            FacturacionElectronica.Procesa.CerFile = factura_encabezado.ZctCatfolios.FactEPath_folio + factura_encabezado.ZctCatfolios.FactECer_folio;
            FacturacionElectronica.Procesa.path_factura = GetRutaFacturas(factura_encabezado);
            FacturacionElectronica.Procesa.password_cfdi = factura_encabezado.ZctCatfolios.FactEPsw_folio;
            return FacturacionElectronica.Procesa.GeneraFactura(cfdi, FacturacionElectronica.TipoFactura.CFDI, factura_encabezado.folio_factura.ToString(), timbra);


        }


        public void cancela_factura(Modelo.ZctFacturaEnc factura)
        {
            FacturacionElectronica.Procesa.path = factura.ZctCatfolios.FactEPath_folio;
            FacturacionElectronica.Procesa.path_factura = GetRutaFacturas(factura);
            FacturacionElectronica.Procesa.password_cfdi = factura.ZctCatfolios.FactEPsw_folio;
            FacturacionElectronica.Procesa.pfx_path = factura.ZctCatfolios.pfx_path;
            FacturacionElectronica.Procesa.CancelaFactura(factura.ZctCatfolios.ZctCatEmisor.RFC, factura.ZctFacturaComprobante.folio_fiscal_factura.ToString(), cancela);
            factura.status_factura = 'C';
            dao.SubmitChanges();

        }

        public string GetRutaFacturas(Modelo.ZctFacturaEnc factura)
        {
            string path = factura.ZctCatfolios.FactEPath_folio + factura.ZctCatfolios.Cod_Folio + '/' + factura.fecha_factura.Year.ToString() + '/' + factura.fecha_factura.Month.ToString() + '/';
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            return path;
        }



        internal Modelo.ZctCatCte carga_cliente(int cod_cte)
        {

            var cte = (from c in dao.ZctCatCte where c.Cod_Cte == cod_cte select c).FirstOrDefault();
            dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, cte);
            return cte;
        }

        public FacturacionElectronica.Procesa.CancelarCFDI cancela { get; set; }

        public byte[] GeneraQR(string CadenaEncriptar, string RutaGuardado, string RutaReparto, System.Drawing.Size Tamaño)
        //public byte[] GeneraQR(string CadenaEncriptar, System.Drawing.Size Tamaño)
        {
#warning optimizar
            System.Drawing.Image imagen;
            QrCodeImgControl QRimagen = new QrCodeImgControl();
            try
            {

                QRimagen.Size = Tamaño;
                QRimagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                QRimagen.Text = CadenaEncriptar;

                imagen = (System.Drawing.Image)QRimagen.Image.Clone();
                imagen.Save(RutaGuardado);
                imagen.Save(RutaReparto);
                //imagen.Dispose();



            }
            finally
            {

                QRimagen.Dispose();
            }
            //catch (Exception ex)
            //{
            //throw ex.Message
            //System.Windows.Forms.MessageBox.Show("Error generando QR " + ex.Message, "ATENCION", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            //}
            return imageToByteArray(imagen);
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
    }
}