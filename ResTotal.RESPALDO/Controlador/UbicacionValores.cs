﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ResTotal.Controlador
{
    class ControladorUbicacionValores
    {
        Boolean _Nuevo;
        public Boolean  Nuevo
        {
            get { return _Nuevo; }
        }
        Modelo.UbicacionValoresDataContext _dao;
        int _user;
        public ControladorUbicacionValores(string Cnn, int Cod_usu)
        {
            _dao = new Modelo.UbicacionValoresDataContext(Cnn);
            _user = Cod_usu;
        }

        public Modelo.ZctEncUbicacionValores ValidaFecha(DateTime Fecha, int cod_usu)
        {
            List<Modelo.ZctEncUbicacionValores> Encabezados = (from E in _dao.ZctEncUbicacionValores where E.fecha == Fecha select E).ToList();
            if (Encabezados.Count > 0)
            {
                _Nuevo = false; 
                return Encabezados[0];
            }
            Modelo.ZctEncUbicacionValores Encabezado = new Modelo.ZctEncUbicacionValores();
            Encabezado.fecha = Fecha.Date;
            Encabezado.folio = FolioEncabezado();
            Encabezado.cod_usu = cod_usu;
            _Nuevo = true;
            return Encabezado;
        }
        public string  FolioEncabezado()
        {
            int folio = (from u in _dao.ZctEncUbicacionValores select u.Cod_encUbicVal).Max(x => (int?)x) ?? 0;
            folio = folio + 1;
            return folio.ToString();
        }
        public decimal Total  (DateTime Fecha)
        {
            //decimal Total =(from v in _dao.ZctDetalleUbicacionValores where v.==Fecha select v.monto).Sum(x => (decimal?)x.monto) ?? 0.0M;
           decimal  Total = (from enc in _dao.ZctEncUbicacionValores
                     join det in _dao.ZctDetalleUbicacionValores
                         on enc.Cod_encUbicVal equals det.Cod_encUbicVal
                     where enc.fecha == Fecha
                     select det.monto).Sum(x => (decimal?)x) ?? 0.0M;
            return Total;
        }
        public void CreaDetalleUbicacion(Modelo.ZctEncUbicacionValores Encabezado)
        {
            List<Modelo.ZctCatUbicacionValores> Ubicaciones = (from u in _dao.ZctCatUbicacionValores select u).ToList();
            foreach (Modelo.ZctCatUbicacionValores u in Ubicaciones)
            {
                Modelo.ZctDetalleUbicacionValores det = Encabezado.ZctDetalleUbicacionValores.Where(d => d.cod_ubicacion == u.cod_ubicacion).FirstOrDefault();
                if (det == null)
                {
                    det = new Modelo.ZctDetalleUbicacionValores();
                    Encabezado.ZctDetalleUbicacionValores.Add(det);
                }
                det.cod_ubicacion = u.cod_ubicacion;
                det.Cod_encUbicVal = Encabezado.Cod_encUbicVal;
                det.monto = 0;
                det.DescripcionUbicacion = u.descripcion;
                det.cod_ubicacion = u.cod_ubicacion; 
            }
        }
        public void Grabar(Modelo.ZctEncUbicacionValores EnCabezado)
        {
            _dao.ZctEncUbicacionValores.InsertOnSubmit(EnCabezado);
            _dao.SubmitChanges();
        }
    }
}
