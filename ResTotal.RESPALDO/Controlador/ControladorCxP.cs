﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ResTotal.Controlador
{
   public class  ControladorCxP
    {
        Modelo.DAODataContext _dao;   
        public ControladorCxP(string _cnn)
        {
            _dao = new Modelo.DAODataContext(_cnn);
        }
        private decimal  DosDecimales(object Cantidad)
        {
            return Decimal.Round(Convert.ToDecimal(Cantidad), 2);
        }
        public Modelo.ZctEncOC ValidaOC(int Cod_OC)
        {
            List<Modelo.ZctEncOC>  OCs;
            OCs=(from OC in _dao.ZctEncOC  where OC.Cod_OC==Cod_OC select OC).ToList();
            if (OCs.Count  == 0)
            {
                throw new ArgumentException ("No existe ninguna Orden de compra con codigo " +Cod_OC.ToString() +" Verifique");
            }
            return OCs[0];
        }
        public int TraeFolioCxP()
        {
           List< int> folios = (from f in _dao.ZctEncCuentasPagar select f.cod_cuenta_pago).ToList();
           if (folios.Count == 0)
           {
               return 1;
           }
           return  (from f in _dao.ZctEncCuentasPagar select f.cod_cuenta_pago).Max()+1;
        }
        public Modelo.ZctCatProv1 ValidaProveedor(int Cod_prov)
        {
            List<Modelo.ZctCatProv1> proveedores = (from p in _dao.ZctCatProv1 where p.Cod_Prov == Cod_prov  select p).ToList();
            if (proveedores.Count == 0)
            {
                throw new Exception("No hay proveedores con folio " + Cod_prov.ToString() + ", Verifique");
            }
            return proveedores[0];
        }
        public Modelo.ZctEncCuentasPagar ValidaCxP(int cod_cuenta_pago)
        {
            List<Modelo.ZctEncCuentasPagar> CxPs = (from cxp in _dao.ZctEncCuentasPagar where cxp.cod_cuenta_pago == cod_cuenta_pago select cxp).ToList();
            if (CxPs.Count == 0)
            {
                throw new Exception("No hay ninguna cuenta por pagar con folio " + cod_cuenta_pago.ToString() + ", Verifique");
            }
            return CxPs[0];
        }
        public Modelo.ZctEncCuentasPagar ValidaOcCxP(int Folio)
        {
            List<Modelo.ZctEncCuentasPagar> CxPs = (from cxp in _dao.ZctEncCuentasPagar where cxp.Cod_OC == Folio select cxp).ToList();
            if (CxPs.Count == 0)
            {
                throw new Exception("No hay ninguna cuenta por pagar con Orden de Compra " + Folio.ToString() + ", Verifique");
            }
            return CxPs[0];
        }
        public Modelo.ZctEncCuentasPagar CreaCuentaPagar(int Cod_OC)
        {
            decimal SubtoTotalOC;
            SubtoTotalOC = DosDecimales((from st in _dao.ZctDetOC where st.Cod_OC == Cod_OC select (st.Cos_Art * st.Ctd_Art)).Sum());
           int Cod_Prov= Convert.ToInt32(ValidaOC(Cod_OC).Cod_Prov);
            Modelo.ZctEncCuentasPagar cuenta;
            List <Modelo.ZctEncCuentasPagar> cuentas=(from c in _dao.ZctEncCuentasPagar where c.Cod_OC ==Cod_OC select c ).ToList();
            if (cuentas.Count == 0)
            {
                 cuenta = new Modelo.ZctEncCuentasPagar();
                 cuenta.Cod_OC = Cod_OC;
                 cuenta.monto = SubtoTotalOC;
            }
            else
            {
                cuenta = cuentas[0];
            }
            decimal IvaPar;
            
            List<Modelo.ZctCatPar> param = (from Param in _dao.ZctCatPar select Param).ToList();
            IvaPar = DosDecimales(param[0].Iva_CatPar);
            decimal IvaOC =DosDecimales(SubtoTotalOC * IvaPar );


            List<int?> lista=(from Modelo.ZctEncOC oc in _dao.ZctEncOC join Modelo.ZctCatProv1 p in _dao.ZctCatProv1 on oc.Cod_Prov equals  p.Cod_Prov  where oc.Cod_OC==Cod_OC  select p.dias_credito ).ToList();
            if (lista.Count==0)
            {
                throw new Exception("Proveedor no encontrado, verifique la OC numero " + Cod_OC.ToString());
            }
           int DiasCredito=Convert.ToInt32(lista[0]);
           DateTime FechaVencido = DateTime.Now.AddDays(DiasCredito);

            
            cuenta.fecha_creacion = DateTime.Now;
            cuenta.fecha_vencido = FechaVencido;
            cuenta.iva =IvaOC;
            cuenta.subtotal = SubtoTotalOC;
            cuenta.total = SubtoTotalOC+IvaOC;
            cuenta.estado="PENDIENTE";


            var Proveedor = ValidaProveedor(Cod_Prov);
            int dias_credito;
            int frecuencia_pago;
            int numero_pagos;
            dias_credito = Convert.ToInt32(Proveedor.dias_credito);
            frecuencia_pago = Convert.ToInt32(Proveedor.frecuencia_pagos);
            numero_pagos = Convert.ToInt32(Proveedor.numero_pagos);
            DateTime FechaVencimientoPago = DateTime.Now.AddDays(dias_credito );

            decimal MontoPagos;
            if (numero_pagos > 0)
            {
                MontoPagos = (cuenta.total / numero_pagos);
            }
            else
            {
                MontoPagos = (cuenta.total);
                numero_pagos = 1;
            }

            if (cuentas.Count == 0)
            {
                _dao.ZctEncCuentasPagar.InsertOnSubmit(cuenta);
                _dao.SubmitChanges();
            }

           //actualizan o agregan pagos a la cxp 
            int pagos = (from p in _dao.ZctDetCuentasPagar where p.cod_cuenta_pago == cuenta.cod_cuenta_pago select p.cod_cuenta_pago).Count();
            for (int p = 1; p <= numero_pagos; p++)
            {
                List <Modelo.ZctDetCuentasPagar > pagoReg=(from pr in _dao.ZctDetCuentasPagar where pr.cod_cuenta_pago==cuenta.cod_cuenta_pago 
                                                               && pr.cod_cuenta_pago ==cuenta.cod_cuenta_pago  select pr ).ToList();
                if (pagoReg.Count == 0)
                {

                    Modelo.ZctDetCuentasPagar pago = new Modelo.ZctDetCuentasPagar();
                    pago.cod_cuenta_pago = cuenta.cod_cuenta_pago ;
                    pago.consecutivo = p;
                    pago.monto = MontoPagos;
                    pago.fecha_vencido = FechaVencido;
                    pago.fecha_factura=cuenta.fecha_vencido;
                    pago.estado="PENDIENTE";
                    pago.referencia = "";
                    pago.fecha_vencido = FechaVencimientoPago.AddDays((p-1 * frecuencia_pago));
                    cuenta.ZctDetCuentasPagar.Add(pago );
                    //_dao.ZctDetCuentasPagar.InsertOnSubmit(pago);
                    
                } 
                else if (pagoReg.Count > 0)
                {
                    Modelo.ZctDetCuentasPagar pago;
                    if (pagoReg.Count >= p)
                    {
                        pago = pagoReg[p-1];
                    }
                    else
                    {
                        pago = new Modelo.ZctDetCuentasPagar();
                        pago.consecutivo = p;
                    }
                    pago.monto = MontoPagos;
                    pago.estado = "PENDIENTE";
                    pago.referencia = "";
                    pago.fecha_vencido = FechaVencimientoPago.AddDays((p-1 * frecuencia_pago));
                    if (pagoReg.Count >= p-1)
                    {
                        cuenta.ZctDetCuentasPagar.Add (pago);
                    }
                }
            }
            
            List<Modelo.ZctDetCuentasPagar> pagoelim = (from pr in _dao.ZctDetCuentasPagar
                                                       where pr.cod_cuenta_pago == cuenta.cod_cuenta_pago
                                                           && pr.consecutivo > numero_pagos 
                                                       select pr).ToList();
            _dao.ZctDetCuentasPagar.DeleteAllOnSubmit(pagoelim);
                //

                ValidaOC(Cod_OC);

           
            _dao.SubmitChanges();
            return cuenta;
        }
        public List< Modelo.ZctEncCuentasPagar> TraeCuentasProveedores(int cod_prov, string Estado="")
        {
            List<Modelo.ZctEncCuentasPagar> Ctas=null ;

            if (Estado == "" || Estado =="TODO")
            {
              Ctas  = (from cta in _dao.ZctEncCuentasPagar
                                                    join oc in _dao.ZctEncOC on cta.Cod_OC equals oc.Cod_OC 
                                                    where oc.Cod_Prov==cod_prov 
                                                    select cta).ToList();
            }
            else if (Estado == "PENDIENTE")
            {
                Ctas = (from cta in _dao.ZctEncCuentasPagar
                         join oc in _dao.ZctEncOC on cta.Cod_OC equals oc.Cod_OC
                         where oc.Cod_Prov == cod_prov && cta.estado == "PENDIENTE"
                         select cta).ToList();
            }
            else if (Estado == "PAGADO")
            {
                Ctas = (from cta in _dao.ZctEncCuentasPagar
                        join oc in _dao.ZctEncOC on cta.Cod_OC equals oc.Cod_OC
                        where oc.Cod_Prov == cod_prov && cta.estado == "PAGADO"
                        select cta).ToList();
            }
            return Ctas;
        }
        public List<Modelo.WV_ZctProveedoresCxP> TraeCuentasVencidas(DateTime fecha)
        {
            return (from cxp in _dao.WV_ZctProveedoresCxP  select cxp).ToList();
        }
        public List<Modelo.WV_ZctProveedoresCxP> TraeProveedores()
        {
            List<Modelo.WV_ZctProveedoresCxP> Prov;
            Prov = (from p in _dao.WV_ZctProveedoresCxP select p).ToList();
            return Prov;
        }
        public List<Modelo.ZctDetCuentasPagar> TraePagosVencidosProveedor(DateTime fecha, int cod_prov)
        {
            return (from p in _dao.ZctDetCuentasPagar 
                    where p.fecha_vencido <=fecha && 
                    p.ZctEncCuentasPagar.ZctEncOC.ZctCatProv1.Cod_Prov ==cod_prov 
                        select p).ToList();
        }
        public Modelo.ZctDetCuentasPagar CreaPago(Modelo.ZctEncCuentasPagar cxp,string Referencia,int Metodo,decimal monto,decimal cambio,DateTime fechapago,DateTime fechavencido)
       {
           Modelo.ZctDetCuentasPagar detpago = new Modelo.ZctDetCuentasPagar();
           detpago.cod_cuenta_pago = cxp.cod_cuenta_pago;
           //detpago.cambio = 0;
           //detpago.fecha_pago = DateTime.Now;
           int Consecutivo = 1;
           int ConsecutivosPagos = (from c in _dao.ZctDetCuentasPagar where c.cod_cuenta_pago == cxp.cod_cuenta_pago select c).Count();
           if (ConsecutivosPagos>0)
           {
               Consecutivo =Convert.ToInt32( (from C in _dao.ZctDetCuentasPagar where C.cod_cuenta_pago == cxp.cod_cuenta_pago select C.consecutivo).Max() + 1);
           }
           detpago.referencia = Referencia;
           detpago.consecutivo = Consecutivo;
           detpago.fecha_vencido = cxp.fecha_vencido;
           detpago.codtipo_pago = Metodo;
           detpago.estado="PENDIENTE";
           detpago.fecha_factura = cxp.fecha_vencido;
           return detpago;
       }
        public void Graba(Modelo.ZctEncCuentasPagar CxP,Modelo.ZctDetCuentasPagar Detalle =null )
        {
            if ( Detalle != null)
            {
                //CxP.ZctDetCuentasPagar.Add(Detalle);
                _dao.ZctDetCuentasPagar.InsertOnSubmit(Detalle);
            }
            CxP.update_web = false;
            _dao.SubmitChanges();
        }
        public void Refresca()
        {
            _dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
        }
        public List <Modelo.ZctEstadosPago > TraeEstadosPago()
        {
            return (from Estado in _dao.ZctEstadosPago select Estado).ToList();
        }
        public void CancelarCxP(int OC)
        {
            _dao.SP_EstadoCxP(OC, "CANCELADO");
        }
        public List<Modelo.ZctBancos> TraeBancos()
        {
            return  (from b in _dao.ZctBancos select b).ToList();
        }
        public Modelo.ZctCatProv1  CambiaProveedor(int cod_prov)
        {
            var prov= (from p in _dao.ZctCatProv1 where p.Cod_Prov == cod_prov select p).ToList()[0];
            _dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, prov);
            return prov; 
        }
    }
}
