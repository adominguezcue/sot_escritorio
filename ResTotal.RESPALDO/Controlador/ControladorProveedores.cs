﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ResTotal.Controlador
{
    class ControladorProveedores
    {
        //
        Modelo.DAOproveedoresDataContext _dao;
        public ControladorProveedores(string cnn)
        {
            _dao = new Modelo.DAOproveedoresDataContext(cnn);
        }
        public List<Modelo.ZctCatProv> TraeProveedores(string codigo="")
       {
           if (codigo == "")
           {
               List<Modelo.ZctCatProv> proveedores = (from p in _dao.ZctCatProv select p).ToList();
               return proveedores;
           } 
           else
           {
               List<Modelo.ZctCatProv> proveedores = (from p in _dao.ZctCatProv where p.Cod_Prov==Convert.ToInt32(codigo) select p).ToList();
               return proveedores;
           }
       }
        public string CodigoProveedor()
        {
            string Codigo = ((from c in _dao.ZctCatProv select c.Cod_Prov).Max() + 1).ToString();
            return Codigo;
        }
        public List<Modelo.SP_ZctCatProvResult> BusquedaProveedores(string nom_Prov, string rFC_Prov, string dir_Prov,
            string calle, string colonia, string  num1nt, string numext, string telefono, string cp, string ciudad, 
            string estado, string email, int Codigo = 0)
        {
          List< Modelo.SP_ZctCatProvResult > p =  _dao.SP_ZctCatProv(1, Codigo, nom_Prov, rFC_Prov, dir_Prov, 1, calle, colonia, 
                num1nt, numext, telefono, cp, ciudad, estado, email).ToList();
          return p;
        }
        public  int proveedorPos(int Cod_proveedor)
        {
            List<Modelo.ZctCatProv> provedores = (from p in _dao.ZctCatProv where p.Cod_Prov == Cod_proveedor select p).ToList();
            if (provedores.Count == 0)
            {
                provedores = null;
                return -1;
            }
            int res= TraeProveedores().IndexOf(provedores[0]);
            return res; 
        }
        public List<Modelo.ZctCatCiu > TraeCiudades(string  estado="")
        {
           List<int>  cod_Edos=new List<int>();
           List<Modelo.ZctCatCiu> ciudades = new List<Modelo.ZctCatCiu>();
               if (estado!="")
               {
              cod_Edos=  (from e in _dao.ZctCatEdo where e.Desc_Edo == estado select e.Cod_Edo).ToList();
               }
           if (cod_Edos.Count == 0)
           {
                ciudades  = (from C in _dao.ZctCatCiu  select C).ToList();
            return ciudades;
           }
         // return  _dao.SP_ZctCatCiu(1,0,"",cod_Edo,0).ToList();
             ciudades = (from C in _dao.ZctCatCiu where C.Cod_Edo == cod_Edos[0] select C).ToList();
            return ciudades;
        }
       public List<Modelo.ZctCatEdo >TraeEstados()
        {
            var edos = (from e in _dao.ZctCatEdo select e).ToList();
            return edos;
        }
       public void agrega(Modelo.ZctCatProv proveedor)
       {
           _dao.ZctCatProv.InsertOnSubmit(proveedor);
       }
        public void Elimina (Modelo.ZctCatProv proveedor)
    {
        _dao.ZctCatProv.DeleteOnSubmit(proveedor );
    }
       public void guardar()
       {
           _dao.SubmitChanges();
       }
       public void Cancelar()
       {
           _dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
       }
       public void Refrescar()
       {
           _dao.Refresh(System.Data.Linq.RefreshMode.KeepCurrentValues);
       }
    }
}
