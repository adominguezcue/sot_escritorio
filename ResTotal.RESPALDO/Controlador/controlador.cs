﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Data.Linq;
namespace ResTotal.Controlador
{
    public class Controladorcaja
    {
        //
        Boolean _NuevaCaja = false;
        Modelo.DAODataContext _dao;
        string _cod_folio;
        int _cod_usu;
        string _Cn;
        int _cod_cliente_default;
        public Boolean CajaNueva
        {
            get { return _NuevaCaja; }
            set { _NuevaCaja = value; }
        }
        public Controladorcaja(string conn, string Cod_folio, int cod_usu)
        {
            _dao = new Modelo.DAODataContext(conn);
            _cod_folio = Cod_folio;
            _cod_usu = cod_usu;
            _Cn = conn;
            
        }
        #region Caja
        public int ValidaFolioCaja(String CodEnc_Caja)
        {
            Boolean EsNumero = false;
            int Folio = 0;
            EsNumero=int.TryParse(CodEnc_Caja, out Folio);
            if (EsNumero == false)
            {
                throw new ArgumentException("El folio de caja debe ser un numero");
            }
           return Folio;
        }
        public Modelo.ZctEncCaja CargaOrden(string Cod_Folio, string OT, int CodEnc_Caja = 0)
        {
            ValidaFolioOT(OT);
            if (OT == "")
            {
                return null ;
            }
            List<Modelo.ZctEncCaja> Cajas;
            //if (CodEnc_Caja > 0)
            //{
                Cajas = (from Modelo.ZctEncCaja C in _dao.ZctEncCaja
                         where C.cod_folio == Cod_Folio && C.CodEnc_OT == Convert.ToInt32(OT) &&C.CodEnc_Caja==CodEnc_Caja 
                         && C.staus=="CANCELADO"
                         select C).ToList();
            //}
            if (Cajas.Count==0)
            {
                Cajas = (from Modelo.ZctEncCaja C in _dao.ZctEncCaja
                         where C.cod_folio == Cod_Folio && C.CodEnc_OT == Convert.ToInt32(OT) 
                         && C.staus=="APLICADO"
                         select C).ToList();
            }
            if(Cajas.Count==0)
            {
                _NuevaCaja = true;
             return    TranformaObjetoCaja(OT,Cod_Folio,_cod_usu,DateTime.Now );
            }
            _NuevaCaja = false;
            return Cajas[0];
        }
        public void ValidaCaja(int CodEnc_OT)
        {
           int Cajas = (from Modelo.ZctEncCaja C in _dao.ZctEncCaja where C.CodEnc_OT == CodEnc_OT select C).Count();
           if (Cajas > 0)
           {
               throw new ArgumentException("La Caja para la Orden de trabajo " + CodEnc_OT.ToString() + " ya existe");
           }
        }
        public List<Modelo.ZctDetOT> BuscaDetalleOT(int CodEnc_OT)
        {
            List<Modelo.ZctDetOT> res = (from d in _dao.ZctDetOT where d.Cod_EncOT == CodEnc_OT select d).ToList();
            _dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, res);
            if (res.Count == 0)
            {
                throw new Exception("El detalle de la OT no se ha encontrado");
            }
            double Total = Convert.ToDouble((from d in _dao.ZctDetOT where d.Cod_EncOT == CodEnc_OT select d.PreArt_DetOT).Sum());
            if (Total <= 0)
            {
                throw new Exception("El total del detalle debe ser mayor a 0");
            }
            return res;
        }
        public List<Modelo.ZctEncOT> ValidaFolioOT(string CodEnc_OT)
        {
            int res = 0;
            bool isint = int.TryParse(CodEnc_OT, out res);
            if (isint == false)
            {
                throw new ArgumentException("El Folio debe ser un numero");
            }

            else
            {
                if (res <= 0)
                {
                    Console.WriteLine("Negativo");
                    throw new ArgumentException("El Folio debe ser mayor a  0");
                }
            }
            List<Modelo.ZctEncOT> Resultado;
            Resultado = (from Enc in _dao.ZctEncOT where Enc.CodEnc_OT == res select Enc).ToList();
            if (Resultado.Count == 0)
            {
                throw new ArgumentException("No se encontro la orden de pago con folio " + CodEnc_OT);
            }
            return Resultado ;
        }
        public int ObtieneFolioCaja(string cod_folio)
        {
           List <ResTotal.Modelo.SP_ZctCatFoliosResult >folios= _dao.SP_ZctCatFolios(1, cod_folio).ToList();
           if (folios.Count == 0)
           {
               throw new ArgumentException("No se ha recibido ningun folio, verifique el codigo de folios");
           }
           if (folios[0].Cons <= 0)
           {
               throw new Exception("El folio de caja debe ser mayor a cero");
           }
            int res=Convert.ToInt32 (folios[0].Cons);
            return res;
        }
        public int GrabaFolioCaja(string cod_folio)
        {
            List<ResTotal.Modelo.SP_ZctCatFoliosResult> folios = _dao.SP_ZctCatFolios(2, cod_folio).ToList();
            if (folios.Count == 0)
            {
                throw new ArgumentException("No se ha recibido ningun folio, verifique el codigo de folios");
            }
            if (folios[0].Cons <= 0)
            {
                throw new Exception("El folio de caja debe ser mayor a cero");
            }
            int res = Convert.ToInt32(folios[0].Cons);
            return res;
        }
        public EntitySet<Modelo.ZctDetCaja> CreaDetalleCaja(int CodEnc_Caja,List<Modelo.ZctDetOT> DetalleOT, string cod_folio, EntitySet<Modelo.ZctDetCaja> res )
        {
            if (DetalleOT.Count == 0)
            {
                throw new  ArgumentException("El detalle no debe estar vacio");
            }
            int Max = 1;
            List <Modelo.ZctCatPar> par =(from p in _dao.ZctCatPar select p).ToList();
            decimal iva= Convert.ToDecimal (par[0].Iva_CatPar);

            
            foreach (Modelo.ZctDetOT d in DetalleOT)
            {
                Modelo.ZctDetCaja detcaja = res.Where(x=> x.codEnc_OT == d.Cod_EncOT && x.Cod_DetOT ==d.Cod_DetOT).FirstOrDefault();
                if (detcaja == null)
                {
                    detcaja = new Modelo.ZctDetCaja();
                    res.Add(detcaja);
                }
                detcaja.Cod_Art = d.Cod_Art;
                detcaja.ZctCatArt = (from a in _dao.ZctCatArt where a.Cod_Art == d.Cod_Art select a).ToList ()[0];
                detcaja.cod_folio = cod_folio;
                detcaja.CodEnc_Caja = CodEnc_Caja;
                detcaja.Ctd_Art = Convert.ToInt32(d.CtdStd_DetOT);
                detcaja.nodoc_caja = Max;
                decimal Importe = decimal.Round((Convert.ToDecimal(d.PreArt_DetOT) * Convert.ToDecimal(d.CtdStd_DetOT)), 2); 
                decimal SubTotal = Importe;
                detcaja.iva = Decimal.Round((Importe * iva),2);
                detcaja.CostoArticuloOT = decimal.Round(Convert.ToDecimal(d.PreArt_DetOT),2);
                detcaja.subtotal = decimal.Round(SubTotal, 2);
                detcaja.total = Importe + detcaja.iva;
                detcaja.codEnc_OT = d.Cod_EncOT;
                detcaja.Cod_DetOT = d.Cod_DetOT;
                
                Max += 1;
            }
           
            return res;
        }
        public int ValidaUsuario(int Cod_usu)
        {
            int C = (from u in _dao.ZctSegUsu where u.Cod_Usu == Cod_usu select u).Count();
            if (C == 0)
            {
                throw new ArgumentException("No existe un usuario con el codigo " + Cod_usu.ToString());
            }
            return Cod_usu;
        }
        public Modelo.ZctEncCaja TranformaObjetoCaja(string CodEnc_OT, string cod_folio,int Cod_usu,DateTime FchEnc_Caja)
        {
            ValidaUsuario(Cod_usu);
            int FolioCaja = ObtieneFolioCaja(cod_folio);
            var Enc_OT =ValidaFolioOT(CodEnc_OT)[0];
            int FolioOT = Enc_OT.CodEnc_OT;
            List<Modelo.ZctDetOT> DetalleOt = BuscaDetalleOT(FolioOT);
            Console.WriteLine(" Total Detalle " + DetalleOt.Count.ToString());
            Modelo.ZctEncCaja res = new Modelo.ZctEncCaja();

            int foliocaja = ObtieneFolioCaja(cod_folio);
            res.cod_folio = cod_folio;
            res.cod_usu = Cod_usu;
            res.CodEnc_Caja = FolioCaja;
            res.CodEnc_OT = FolioOT;
            res.FchEnc_Caja = FchEnc_Caja;
            res.ZctEncOT = Enc_OT;
            res.fecha_folio = DateTime.Now;
            res.cod_cte = Enc_OT.Cod_Cte;
            actualiza_detalle(res);
            //res.ZctDetCaja.AddRange( CreaDetalleCaja(FolioCaja, DetalleOt, cod_folio));
            Console.WriteLine(" Total Detalle Guardado " + res.ZctDetCaja.Count.ToString());
            res.staus = "NUEVO FOLIO SIN GUARDAR";
            //CreaPagoCaja(res, 1, 0);
            
            
            return res;
        }
        #endregion
        #region Pagos
        public void AsignaFolioCaja(Modelo.ZctEncCaja caja)
        {
            int folio = 1;
            //List<int> res = (from f in _dao.ZctEncCaja where f.CodEnc_Caja == caja.CodEnc_Caja && f.CodEnc_OT == caja.CodEnc_OT   select f.CodEnc_Caja).ToList();
            var folionuevo = _dao.SP_ZctCatFolios(1, _cod_folio).ToList();
            folio = Convert.ToInt32(folionuevo[0].Cons);
            if (caja.CodEnc_Caja +1<folio )
            {
                caja.CodEnc_Caja = folio;
                foreach (Modelo.ZctDetCaja detcaja in caja.ZctDetCaja)
                {
                    detcaja.CodEnc_Caja = folio;
                }
            }
            _dao.SP_ZctCatFolios(2, _cod_folio).ToList();
        }
        public void  ValidaMontoPago(Modelo.ZctEncCaja Caja, Decimal MontoPago)
        {
            if (Caja == null)
            {
                throw new ArgumentException("No ha cargado ningun pedido en caja");
            }
            decimal total = (from Modelo.ZctDetCaja det in Caja.ZctDetCaja select det.total).Sum();
            total = decimal.Round(total, 2);
            var Ival = (from Modelo.ZctDetCaja det in Caja.ZctDetCaja select det.iva).Sum();
            if (MontoPago < total)
            {
                throw new ArgumentException("El monto del pago no debe ser menor al total de a pagar le restan " + string.Format ("{0:C}",(total-MontoPago) ));
            }
         
        }
        public void CreaPagoCaja(Modelo.ZctEncCaja Caja, int codtipo_pago, decimal MontoPago)
        {

            int Max = 1;
            List<int> Tipospago = (from Modelo.ZctCatTipoPago p in _dao.ZctCatTipoPago where p.codtipo_pago == codtipo_pago select p.codtipo_pago).ToList();
            if (Tipospago.Count == 0)
            {
                throw new Exception("El Tipo de pago especificado no existe");
            }
            if (Caja.ZctPagoCaja.Count > 0)
            {
                 ValidaMontoPago(Caja, MontoPago);
            }
           
            List<Modelo.ZctPagoCaja> Pagos = (from Modelo.ZctPagoCaja p in _dao.ZctPagoCaja
                                              where p.CodEnc_Caja == Caja.CodEnc_Caja
                                              && Caja.cod_folio == p.cod_folio
                                              select p).ToList();
            if (Pagos.Count > 0)
            {
                Max = (from Modelo.ZctPagoCaja p in _dao.ZctPagoCaja
                       where p.CodEnc_Caja == Caja.CodEnc_Caja
                       && Caja.cod_folio == p.cod_folio
                       select p.nodoc_pago).Max() + 1;
            }

            Modelo.ZctPagoCaja PagoCaja = new Modelo.ZctPagoCaja();
            PagoCaja.cod_folio = Caja.cod_folio;
            PagoCaja.CodEnc_Caja = Caja.CodEnc_Caja;
            PagoCaja.codtipo_pago = codtipo_pago;
            PagoCaja.monto = MontoPago;
            PagoCaja.nodoc_pago = Max;
            Caja.ZctPagoCaja.Add(PagoCaja);
        }
        public Modelo.ZctEncCaja GrabaCaja(Modelo.ZctEncCaja Caja, Decimal MontoPago)
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                ValidaMontoPago(Caja, MontoPago);
                //Modelo.ZctEncOT ot;
                int folio_ot;
                
                if (_NuevaCaja == true)
                {
                    //AsignaFolioCaja(Caja);
                    Caja.CodEnc_Caja = this.GrabaFolioCaja(Caja.cod_folio);
                    //AsignaFolioCaja(Modelo.ZctEncCaja caja);
                }
                if (Caja.ZctEncOT == null)
                {
                    //ot = new Modelo.ZctEncOT();
                    var folionuevo = _dao.SP_ZctCatFolios(2, "OT").ToList();
                    folio_ot = Convert.ToInt32(folionuevo[0].Cons);
                    //ot.Cod_Cte = Caja.
                    //ot.Cod_Cte = Caja.cod_cte;
                    //ot.Cod_Estatus = 'N';
                    //ot.FchAplMov = DateTime.Now;
                    //ot.FchEnt = DateTime.Now;
                    //ot.FchEntRmp = DateTime.Now;
                    //ot.FchSal = DateTime.Now;
                    //ot.FchSalRmp = DateTime.Now;
                    //_dao.ZctEncOT.InsertOnSubmit(ot);
                    //Caja.ZctEncOT = ot;
                    _dao.SP_ZctEncOT(2, folio_ot, DateTime.Now, Caja.cod_cte.GetValueOrDefault().ToString(), 0, 0, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, "", DateTime.Now, 0, 'N', "", 0);
                    Caja.CodEnc_OT = folio_ot;
                    //_dao.SubmitChanges();
                }
                else {
                    folio_ot= Caja.CodEnc_OT;
                }
                
                     
                
                int cont = 1;
                foreach (Modelo.ZctDetCaja detalle in Caja.ZctDetCaja) {
                    detalle.cod_folio = Caja.cod_folio;
                    
                    //detalle.CodEnc_Caja = Caja.CodEnc_Caja;
                    detalle.nodoc_caja = cont;
                    detalle.codEnc_OT = folio_ot;
                    if (detalle.Cod_DetOT == 0) {

                        //Modelo.ZctDetOT det_ot = new Modelo.ZctDetOT();
                        //det_ot.Cod_EncOT = ot.CodEnc_OT;
                        //det_ot.Cod_Art = detalle.Cod_Art;
                        //det_ot.Cat_Alm = get_almacen_salida();
                        //det_ot.CosArt_DetOT = detalle.ZctCatArt.Cos_Art;
                        //det_ot.Ctd_Art = detalle.Ctd_Art;
                        //det_ot.CtdStd_DetOT = 0;
                        //det_ot.Fch_DetOT = DateTime.Now;
                        //det_ot.PreArt_DetOT = detalle.CostoArticuloOT;
                        //_dao.ZctDetOT.InsertOnSubmit(det_ot);
                        //ot.ZctDetOT.Add(det_ot);
                        int? cod_detot = 0;
                        _dao.SP_ZctDetOT_caja(Caja.CodEnc_OT, detalle.Cod_DetOT, detalle.Cod_Art, detalle.Ctd_Art, detalle.ZctCatArt.Cos_Art, detalle.CostoArticuloOT.GetValueOrDefault() * (1 - detalle.descuento.GetValueOrDefault()), detalle.Ctd_Art, DateTime.Now, DateTime.Now, get_almacen_salida(), 0, detalle.ZctCatArt.Cod_TpArt, 0, detalle.cod_usu, ref cod_detot);
                        detalle.Cod_DetOT =cod_detot.GetValueOrDefault();
                        //_dao.SubmitChanges();
                         //= det_ot.Cod_DetOT;

                        
                        //  detalle.codEnc_OT = Caja.CodEnc_OT;
                    }
                    cont++;

                } 
                //_dao.ZctDetCaja.InsertAllOnSubmit(Caja.ZctDetCaja);

               
                //Caja.CodEnc_OT = ot.CodEnc_OT;
                Caja.staus = "APLICADO";
                _dao.ZctEncCaja.InsertOnSubmit(Caja);
                _dao.SubmitChanges();
                transactionScope.Complete();
                _NuevaCaja = false;
                return (Caja);
            }
        }
       public  List<Modelo.ZctCatTipoPago> TraeTiposPago()
        {
           return  (from P in _dao.ZctCatTipoPago select P).ToList();
        }
        #endregion
        #region Recibos
        public void ValidaFolioRecibo(string Cod_folio, int CodEnc_Caja)
        {
            if (String.IsNullOrEmpty(Cod_folio) || Cod_folio.Length==0)
            {
                throw new Exception("El folio no debe estar vacio");
            }
            int folios = (from f in _dao.ZctCatfolios where f.Cod_Folio == Cod_folio select f).Count();
            if (folios == 0)
            {
                throw new Exception("El folio especificado ( " + Cod_folio + " ) no existe");
            }
        }
        public Modelo.ZctEncCaja BuscaRecibo(string Cod_folio, int CodEnc_Caja)
        {
            ValidaFolioRecibo(Cod_folio,CodEnc_Caja );
            List < Modelo.ZctEncCaja >Cajas= (from c in _dao.ZctEncCaja where c.cod_folio == Cod_folio && c.CodEnc_Caja == CodEnc_Caja select c).ToList();
            if (Cajas.Count == 0)
            {
                throw new Exception("No existe una caja con el codigo de folio " + Cod_folio.ToString() + " y con encabezado de caja " + CodEnc_Caja.ToString());
                
            }

            //decimal _iva = get_iva();
            //foreach (Modelo.ZctDetCaja detalle in Cajas[0].ZctDetCaja) {
            //    detalle.tasa_iva = _iva;
            //}
            

            return Cajas[0];
        }

        public string  CancelaRecibo(string Cod_folio, int CodEnc_Caja, int usuario_cancela)
        {
            ValidaFolioRecibo(Cod_folio, CodEnc_Caja);
            List<Modelo.ZctEncCaja> Cajas = (from c in _dao.ZctEncCaja where c.cod_folio == Cod_folio && c.CodEnc_Caja == CodEnc_Caja select c).ToList();
            if (Cajas.Count == 0)
            {
                throw new Exception("No existe una caja con el codigo de folio " + Cod_folio.ToString() + " y con encabezado de caja " + CodEnc_Caja.ToString());
            }

            
            Modelo.ZctEncCaja Caja = Cajas[0];
            Caja.staus = "CANCELADO";
            Caja.cod_usu_cancela = usuario_cancela;
            Caja.ZctEncOT.Cod_Estatus = 'C';
        
            foreach (Modelo.ZctDetCaja detalle in Caja.ZctDetCaja) {
                int? cod_detot = 0;
                _dao.SP_ZctDetOT_caja(Caja.CodEnc_OT, detalle.Cod_DetOT, detalle.Cod_Art, detalle.Ctd_Art, detalle.ZctCatArt.Cos_Art,  detalle.CostoArticuloOT.GetValueOrDefault(), 0, DateTime.Now, DateTime.Now, get_almacen_salida(), 0, detalle.ZctCatArt.Cod_TpArt, 0, detalle.cod_usu, ref cod_detot);
            }
            _dao.SubmitChanges();
            return Cajas[0].staus;

        }
        #endregion

        public decimal TruncateDecimal(decimal value, int precision)
        {
            decimal step = (decimal)Math.Pow(10, precision);
            int tmp = (int)Math.Truncate(step * value);
            return tmp / step;
        }

        public Modelo.VW_ZCTvistaCte TraeDatosClienteOT(int CodEnc_OT)
        {
            List<int?> cod_clientes = (from C in _dao.ZctEncOT where C.CodEnc_OT == CodEnc_OT select C.Cod_Cte).ToList();
            if (cod_clientes.Count == 0)
            {
                throw new Exception("No existe ninguna OT con el codigo " + CodEnc_OT.ToString());
            }
            int Cod_cte = Convert.ToInt32(cod_clientes[0]);

            List<Modelo.VW_ZCTvistaCte> Clientes = (from C in _dao.VW_ZCTvistaCte where C.Cod_Cte == Cod_cte select C).ToList();
            if (Clientes.Count == 0)
            {
                throw new Exception("No existe ningun cliente con el codigo " + Cod_cte.ToString());
            }

            return Clientes[0];
        }
        public Modelo.ZctCatArt TraeDatosArticulo(string Cod_art)
        {
            List<Modelo.ZctCatArt> Articulos = (from A in _dao.ZctCatArt where A.Cod_Art == Cod_art select A).ToList();
            if (Articulos.Count==0)
            {
                throw new Exception("No hay nungun articulo con codigo " + Cod_art.ToString());
            }
            return Articulos[0];
        }
        public int TraeFolioCajaOT(int OT,int Folio)
        {
            List<int> Caja = (from E in _dao.ZctEncCaja where E.CodEnc_OT==OT && E.CodEnc_Caja==Folio select E.CodEnc_Caja).ToList();

            if (Caja.Count == 0)
            {

               var Folios = _dao.SP_ZctCatFolios(1, _cod_folio).ToList();
               return (int) Folios[0].Cons;
            }
            return Caja[0];
        }
        public int TraeFolioCajaOT(int OT)
        {
            List<int> Caja = (from E in _dao.ZctEncCaja where E.CodEnc_OT == OT  select E.CodEnc_Caja).ToList();

            if (Caja.Count == 0)
            {

                return -1;
            }
            return Caja[0];
        } 
        public int TraeOTCajaFolio(string CodEnc_Caja)
        {
           int Folio= ValidaFolioCaja(CodEnc_Caja);
           List<Modelo.ZctEncCaja> Cajas = (from C in _dao.ZctEncCaja where C.CodEnc_Caja == Folio select C).ToList();
           if (Cajas.Count == 0)
           {
               throw new ArgumentException("No existe ninguna caja con folio " + CodEnc_Caja);
           }
           return Cajas[0].CodEnc_OT; 
        }
        public int ConsultaFolioCaja()
        {
            List<Modelo.SP_ZctCatFoliosResult > sp =_dao.SP_ZctCatFolios(1,_cod_folio).ToList();
            if (sp.Count == 0)
            {
                throw new Exception("No existe el folio de caja " +_cod_folio);
            }
           return Convert.ToInt32( sp[0].Cons);
        }
        public decimal TotalDetalle(int Cod_encCaja, string cod_folio)
        {
            decimal Total=(from c in _dao.ZctDetCaja where c.CodEnc_Caja==Cod_encCaja
                           && c.cod_folio == cod_folio
                           select c.subtotal).Sum();
            return Decimal.Round( Total,2);
        }
        public decimal TotalIvaDetalle(int Cod_encCaja, string cod_folio)
        {
            decimal Total = (from c in _dao.ZctDetCaja where c.CodEnc_Caja == Cod_encCaja
                             && c.cod_folio == cod_folio
                             select c.iva).Sum();
            return Decimal.Round(Total, 2);
        }
        public decimal TotalPagoCaja(int Cod_encCaja, string cod_folio)
        {
            List<decimal > pagos = (from c in _dao.ZctPagoCaja
                                              where c.CodEnc_Caja == Cod_encCaja
                                                  && c.cod_folio == cod_folio 
                                              select c.monto).ToList();
            if (pagos.Count == 0)
            {
                return 0;
            }
            decimal Total = (from c in _dao.ZctPagoCaja 
                             where c.CodEnc_Caja == Cod_encCaja
                                 && c.cod_folio == cod_folio && c.monto != null 
                             select c.monto).Sum();
            return Decimal.Round(Total, 2);
        }
        /// <summary>
        /// Obtiene el total actual de caja contabilisando solo pagos en efectivo (1->CONTADO), recibos con status APLICADO
        /// </summary>
        /// <param name="Cod_folio">FOLIO CAJA</param>
        /// <returns></returns>
        public decimal TraerTotalCaja(string Cod_folio,int cod_usu)
        {

            /*
            int detalle = (from Modelo.ZctDetCaja d in _dao.ZctDetCaja
                           where d.cod_folio == Cod_folio 
                           select d).Count();
            if (detalle == 0)
            {
                return 0;
            }*/

            decimal? total = 0;

            total = (from Modelo.VW_ZctCajaPagos d in _dao.VW_ZctCajaPagos
                     where d.staus == "APLICADO" && d.desc_pago == "CONTADO" && d.cod_folio == Cod_folio && d.cod_usu == cod_usu
                     && (d.corte == false || d.corte == null) select d).Sum(x => (decimal?)x.monto) ?? 0.0M;
                     //select d.monto ?? 0).Sum();
            
            decimal subtotal = Decimal.Round(total.GetValueOrDefault(), 2);


            decimal apertura = 0;

            apertura = (from a in _dao.ZctAperturasCajas where  a.cod_usu ==cod_usu  && a.abierta == true select a.monto_inicial).SingleOrDefault();

            decimal Retiros = 0;
            //int retirot = (from Modelo.ZctRetirosCajas d in _dao.ZctRetirosCajas where (d.corte == false || d.corte == null) && d.cod_folio == Cod_folio && d.cod_usuario == cod_usu select d).Count();
            //if (retirot > 0)
            //{
            Retiros = (from Modelo.ZctRetirosCajas r in _dao.ZctRetirosCajas
                       where r.cod_folio == Cod_folio && r.cod_usuario == cod_usu
                           && (r.corte == false || r.corte == null)
                       select r.monto).Sum(x => (decimal?)x) ?? 0.0M;
                Retiros = Decimal.Round(Retiros, 2);
            //}
            return (subtotal - Retiros )+ apertura;
        }
        public void abrecaja(string cod_folio, int cod_usu, decimal montoinicial)
        {
            Modelo.ZctAperturasCajas caja = new Modelo.ZctAperturasCajas();
            caja.abierta = true;
            caja.cod_folio = cod_folio;
            caja.cod_usu = cod_usu;
            caja.fecha = DateTime.Now ;
            caja.monto_inicial = montoinicial;
            _dao.ZctAperturasCajas.InsertOnSubmit(caja);
            _dao.SubmitChanges();
        }
        public int traeCajaAbiertaUsuario(int cod_usu)
        {
            List<Modelo.ZctAperturasCajas >cajas=(from Modelo.ZctAperturasCajas caja in _dao.ZctAperturasCajas 
                                                  where caja.cod_usu ==cod_usu && caja.abierta==true select caja   ).ToList();
            if (cajas.Count == 0)
            {
                return -1;
            }
            return cajas[0].cod_apertura;
        }
        public void cierrecaja(int cod_usu, decimal montofinal)
        {
            List<Modelo.ZctAperturasCajas> cajas = (from Modelo.ZctAperturasCajas caja in _dao.ZctAperturasCajas
                                                    where caja.cod_usu == cod_usu && caja.abierta == true
                                                    select caja).ToList();
            if (cajas.Count == 0)
            {
                throw new Exception("el usuario especificado no tiene cajas abiertas");
            }
          cajas[0].monto_final=montofinal;
          cajas[0].abierta = false;
          cajas[0].fecha_cierre = DateTime.Now;
          _dao.SubmitChanges ();
        }
        public List<Modelo.ZctEncCaja> VerificaReciboAplicado(int OT)
        {
        int  recibos=(from E in _dao.ZctEncCaja where E.CodEnc_OT ==OT && E.staus=="APLICADO" select E).Count();
        if (recibos > 0)
        {
            return (from E in _dao.ZctEncCaja where E.CodEnc_OT == OT && E.staus.ToString() == "APLICADO" select E).ToList();
        }
        else
        {
            return (from E in _dao.ZctEncCaja where E.CodEnc_OT == OT && E.staus == "CANCELADO" select E).ToList();
        }
        }
        public List<Modelo.ZctEncCaja> ObtieneCajas()
        {
            return (from c in _dao.ZctEncCaja select c).ToList();
        }
        public int NumeroDocumentoPago(string Cod_Folio, int Enc_caja)
        {
            int ndoc = 1;
            int Documentos=(from d in _dao.ZctPagoCaja where d.cod_folio==Cod_Folio && d.CodEnc_Caja ==Enc_caja 
                                && d.nodoc_pago==ndoc select d.nodoc_pago ).Count();
            while (Documentos > 0)
            {
                ndoc += 1;
                Documentos = (from d in _dao.ZctPagoCaja
                              where d.cod_folio == Cod_Folio && d.CodEnc_Caja == Enc_caja
                                  && d.nodoc_pago == ndoc
                              select d.nodoc_pago).Count();
            }
            return ndoc;
        }
        internal Modelo.ZctCatCte carga_cliente(int cod_cte)
        {

            var cte = (from c in _dao.ZctCatCte where c.Cod_Cte == cod_cte select c).FirstOrDefault();
            
            _dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, cte);
            return cte;
        }
        //internal EntitySet<Modelo.ZctDetCaja> carga_detalle_caja(int CodEnc_Caja, string cod_enOT, string cod_folio)
        //{

        //    var detalleOT = BuscaDetalleOT(Convert.ToInt32(cod_enOT));
        //    var detalle = CreaDetalleCaja(CodEnc_Caja, detalleOT, cod_folio);

        //    //
        //    //_dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, detalle);
        //    return detalle;
        //}

        internal void actualiza_detalle(Modelo.ZctEncCaja Caja)
        {


            //Caja.ZctDetCaja = new System.Data.Linq.EntitySet<Modelo.ZctDetCaja>();
            //Caja.ZctDetCaja.ToList().ForEach(x => Caja.ZctDetCaja.Remove(x));
            //Caja.ZctDetCaja.Clear();
            var detalleOT = BuscaDetalleOT(Caja.CodEnc_OT);
            //foreach (Modelo.ZctDetOT dot in detalleOT.ToList()) { 
            //        Caja.ZctDetCaja.Where( x=> x.C )
            //}

            Caja.ZctDetCaja = CreaDetalleCaja(Caja.CodEnc_Caja, detalleOT, Caja.cod_folio, Caja.ZctDetCaja);
           // detalle.ForEach(x => x.Ctd_Art = 100);
            //Caja.ZctDetCaja.AddRange(detalle);


        }
        internal void Refresca()
        {
            _dao.Dispose();
            _dao = new Modelo.DAODataContext(_Cn);
        }
        public string NombreUsuario(int Cod_usu)
        {
           List<string > usuarios= (from u in _dao.ZctSegUsu where u.Cod_Usu == Cod_usu select u.Nom_Usu).ToList();
           if (usuarios.Count == 0)
           {
               return "No se pudo obtener el nombre del cajero";
           }
           return usuarios[0];
        }

        internal List<Modelo.ZctSegUsu> ObtieneVendedores() {
            return (from v in _dao.ZctSegUsu select v).ToList();
        }

        public Modelo.ZctCatArt TraeArticulos(string Cod_art)
        {
            Cod_art = Cod_art.Replace('\'', '-');
            Modelo.ZctCatArt articulo = (from Modelo.ZctCatArt a in _dao.ZctCatArt where a.Cod_Art == Cod_art || a.upc == Cod_art select a).SingleOrDefault();
            //if (articulo.Count == 0)
            // {
            //     Modelo.ZctCatArt articuloN = new Modelo.ZctCatArt();
            //     _nuevo = true;
            //     return articuloN;
            // }
            // _nuevo = false;

            return articulo;
        }
        internal void EliminaElemento(Modelo.ZctDetCaja detalle)
        {
            _dao.ZctDetCaja.DeleteOnSubmit(detalle);
        }
        internal Modelo.ZctDetCaja obtiene_detalle_por_articulo(string cod_art, Modelo.ZctSegUsu vendedor)
        {
            //Controlador.ControladorZctCatArt controlador_articulo = new Controlador.ControladorZctCatArt(this._Cn);
            Modelo.ZctCatArt articulo = this.TraeArticulos(cod_art);
            Modelo.ZctSegUsu  usuario = (from u in _dao.ZctSegUsu where u.Cod_Usu == vendedor.Cod_Usu select u).SingleOrDefault();
            if (articulo == null || articulo.Cod_Art == null) {
                throw new ArgumentException("El artículo no fue encontrado");
            }
            decimal _iva = get_iva();
            
            Modelo.ZctDetCaja detalle = new Modelo.ZctDetCaja() {
                ZctCatArt = articulo,
                Cod_Art = articulo.Cod_Art,
                Cod_DetOT = 0,
                CostoArticuloOT = articulo.Prec_Art,
                descuento = articulo.descuento,
                Ctd_Art = 1,
                tasa_iva = _iva + 1,
                iva = articulo.Prec_Art.GetValueOrDefault()*_iva,
                cod_usu = usuario.Cod_Usu,
                ZctSegUsu = usuario
                
            };
            _dao.ZctDetCaja.InsertOnSubmit(detalle);
            return detalle;

        }

        internal decimal get_iva() {
            List<Modelo.ZctCatPar> par = (from p in _dao.ZctCatPar select p).ToList();
            decimal iva = Convert.ToDecimal(par[0].Iva_CatPar);
            return iva;
        }

        internal int get_almacen_salida()
        {
            List<Modelo.ZctCatPar> par = (from p in _dao.ZctCatPar select p).ToList();
            return par[0].cod_alm_salida.GetValueOrDefault();
            
        }

        

        internal Modelo.ZctEncCaja ObtieneCajaNueva(int user)
        {

            //Modelo.ZctCatCte cliente = (from c in _dao.ZctCatCte where c.Cod_Cte == cod_cliente_default select c).Single();

            Modelo.ZctEncCaja _caja_nueva = new Modelo.ZctEncCaja()
            {
                cod_folio = _cod_folio,
                CodEnc_Caja = ObtieneFolioCaja(_cod_folio),
                FchEnc_Caja = DateTime.Now,
                fecha_folio = DateTime.Now,
                //cod_cte = cod_cliente_default,
                cod_usu = user,
                //ZctCatCte = cliente,
                ZctDetCaja = new EntitySet<Modelo.ZctDetCaja>(),
                ZctPagoCaja = new EntitySet<Modelo.ZctPagoCaja>(),
                //ZctEncOT = new Modelo.ZctEncOT()
                staus = "NUEVO FOLIO SIN GUARDAR"
            };

            _NuevaCaja = true;
            return _caja_nueva;
        }

        internal void LimpiaRecibo(Modelo.ZctEncCaja CajaActual)
        {
            if (_NuevaCaja) {
                foreach ( Modelo.ZctDetCaja detalle in  CajaActual.ZctDetCaja.ToList()){
                    if(detalle.nodoc_caja == 0){
                        this.EliminaElemento(detalle);
                    }

                }
            }
        }
    }
}
