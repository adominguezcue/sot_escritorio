﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
namespace ResTotal.Controlador
{
   public  class ControladorZctCatArt
    {
       bool _nuevo = false;
        Modelo.DAODataContext _dao;
        public ControladorZctCatArt(string cnn)
        {
            _dao = new Modelo.DAODataContext(cnn);
        }
       public Boolean  ArticuloNuevo
       {
           get { return _nuevo; }
       }
       public List<Modelo.ZctImagenesArticulos> TraeImagenesArticulo(string cod_art)
       {
           List<Modelo.ZctImagenesArticulos> i = (from I in _dao.ZctImagenesArticulos where I.cod_art == cod_art select I).ToList();
           if (i.Count == 0)
           {
               _nuevo = true;
               return new List<Modelo.ZctImagenesArticulos>();
           }
           _nuevo = false;
           return i;
       }
        public  Modelo.ZctCatArt TraeArticulos(string Cod_art)
        {
            Modelo.ZctCatArt articulo = (from Modelo.ZctCatArt a in _dao.ZctCatArt where a.Cod_Art == Cod_art || a.upc == Cod_art select a).SingleOrDefault();
            if (articulo == null)
            {
                articulo = new Modelo.ZctCatArt();
                articulo.uuid = Guid.NewGuid();
                _nuevo = true;
            }
            else {
                _nuevo = false;
            }
            //if (articulo.Count == 0)
           // {
           //     Modelo.ZctCatArt articuloN = new Modelo.ZctCatArt();
           //     _nuevo = true;
           //     return articuloN;
           // }
            
            
            return articulo ;
        }
        public void AgregaArticulo(Modelo.ZctCatArt Articulo)
        {
            _dao.ZctCatArt.InsertOnSubmit(Articulo);
        }
        public void EliminaFoto(string Cod_Articulo, int Cod_imagen)
        {
            List<Modelo.ZctImagenesArticulos> imagenes = (from i in _dao.ZctImagenesArticulos where i.numimg == Cod_imagen && i.cod_art == Cod_Articulo select i).ToList();
            if (imagenes.Count > 0)
            {
                _dao.ZctImagenesArticulos.DeleteOnSubmit(imagenes[0]);
            }

        }
        public int MaxImg(string Cod_art)
        {
            List <int > i= (from m in _dao.ZctImagenesArticulos where m.cod_art == Cod_art select m.numimg).ToList();
            if (i.Count == 0)
            {
                return 1;
            }

            return (from m in _dao.ZctImagenesArticulos where m.cod_art == Cod_art select m.numimg).Max() + 1;
        }
        public void ModificaFoto(string Cod_art,int cod_imagen, string RutaImagen)
        {
            List<Modelo.ZctImagenesArticulos> imgs = (from Modelo.ZctImagenesArticulos i in _dao.ZctImagenesArticulos
                                                           where i.cod_art == Cod_art && i.cod_imagen == cod_imagen 
                                                select i).ToList();
            if (imgs.Count == 0)
            {
                throw new ArgumentException("no existe ningun articulo con codigo " + Cod_art);
            }
           
            imgs[0].imagen = RutaImagen ;
        }
        public void Graba()
        {
            _dao.SubmitChanges();
            _nuevo = false;
        }
        public void EliminaArticulo(string Cod_art)
        {
            List<Modelo.ZctCatArt> articulo = (from Modelo.ZctCatArt a in _dao.ZctCatArt where a.Cod_Art == Cod_art select a).ToList();
            if (articulo.Count == 0)
            {
                throw new ArgumentException("No existe ningun articulo con codigo " + Cod_art);
            }
           
            List<Modelo.ZctImagenesArticulos > imagenes =(from Modelo.ZctImagenesArticulos i in _dao.ZctImagenesArticulos  where i.cod_art == Cod_art select i).ToList ();
            _dao.ZctImagenesArticulos.DeleteAllOnSubmit(imagenes);
            _dao.ZctCatArt.DeleteAllOnSubmit(articulo);
            _dao.SubmitChanges();
        }
        public string TraeRutaGuardadoImagenes()
        {
            return (from p in _dao.ZctCatPar select p.ruta_imagenes_articulos).ToList()[0];
        }
    }
}
