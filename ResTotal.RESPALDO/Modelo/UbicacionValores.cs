namespace ResTotal.Modelo
{
    partial class ZctDetalleUbicacionValores
    {
        string _DescripcionUbicacion;
        public string DescripcionUbicacion
        {
            get {
                if (this.ZctCatUbicacionValores == null)
                {
                    return _DescripcionUbicacion;
                }
                return this.ZctCatUbicacionValores.descripcion;
             }
            set
            {
                _DescripcionUbicacion = value;
            }
        }
    }
}
