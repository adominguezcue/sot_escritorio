using System;
using System.Data.Linq;
using System.Linq;
namespace ResTotal.Modelo
{
    partial class ZctPagoCaja
    {
        decimal _MontoIngresado;
        public decimal MontoIngresado
        {
            get {
                if (Convert.ToDecimal(this.cambio) + Convert.ToDecimal(this.monto) > 0 && _MontoIngresado==0)
                {
                    return Convert.ToDecimal(this.cambio) + Convert.ToDecimal(this.monto);
                }
                return _MontoIngresado; 
            }
            set { _MontoIngresado = value; }
        }
    }

    partial class ZctDetOC
    {
        
        public string ArticuloNombre
        {
            get
            {
                //try
                //{
                return this.ZctCatArt.Desc_Art;
                //}
                //catch (Exception ex)
                //{
                //    return "";
                //}
            }
        }
        public Decimal Importe
        {
            get
            {
                return Decimal.Round((decimal)this.Cos_Art *(decimal) this.Ctd_Art,2);
            }
        }
        public string ArticuloMarca
        {
            get
            {
                //try
                //{
                return this.ZctCatArt.ZctCatMar.Desc_Mar;
                //}
                //catch (Exception ex)
                //{
                //    return "";
                //}
            }
        }
    }
    partial class ZctDetCaja
    {
        public string ArticuloNombre
        {
            get
            {

                if (this.ZctCatArt != null)
                    return this.ZctCatArt.Desc_Art;
                else
                    return "";
            }
        }
        public string vendedor
        {
            get
            {
                if (this.ZctSegUsu != null)
                    return this.ZctSegUsu.Nom_Usu;
                else
                    return this.cod_usu.ToString();
            }
        }
        public decimal precio_descuento
        {
            get {
                return Math.Round((this.CostoArticuloOT.GetValueOrDefault() * ( this.descuento.GetValueOrDefault())),2);
            }
        }
        public decimal tasa_iva;
        public decimal ImporteArticulo
        {
            get
            {
                if (this.tasa_iva != 0)
                {

                    this.subtotal = Math.Round(Decimal.Round(this.Ctd_Art) * (this.CostoArticuloOT.GetValueOrDefault() * (1-this.descuento.GetValueOrDefault()) ),2);
                    this.iva = Math.Round(this.subtotal * (this.tasa_iva - 1),2);
                    this.total = Math.Round(this.subtotal * this.tasa_iva,2);
                }
                return this.total;
            }
        }
   
    }
    partial class ZctEncCaja
    {
        public decimal Total {
            get {
                if (this.ZctDetCaja != null)
                    return this.ZctDetCaja.Sum(x => x.total);
                else
                    return 0;
            }
        }
        public decimal TotalPago{
            get {
                if (this.ZctPagoCaja != null)
                    return this.ZctPagoCaja.Sum(x => x.monto);
                else
                    return 0;
            }
        }

        public decimal PorPagar {
            get {
                if (this.Total - this.TotalPago > 0)
                    return this.Total - this.TotalPago;
                else
                    return 0;
            }
        }
        public decimal Cambio
        {
            get
            {
                return this.TotalPago - this.Total;
                //if (this.ZctPagoCaja != null)
                //    return this.ZctPagoCaja.Sum(x => x.cambio.GetValueOrDefault());
                //else
                //    return 0;
            }
        }

        public decimal SubTotal
        {
            get
            {
                if (this.ZctDetCaja != null)
                    return this.ZctDetCaja.Sum(x => x.subtotal);
                else
                    return 0;

            }
        }
        public decimal IVA
        {
            get
            {
                if (this.ZctDetCaja != null)
                    return this.ZctDetCaja.Sum(x => x.iva);
                else
                    return 0;

            }
        }
       

        public string NombreCliente
        {
            get
            {
                //try
                //{
                if ( this.ZctCatCte != null)
                {
                    return  this.ZctCatCte.nombre;
                }
                else {
                    return "Cliente Restotal.";
                }
                //return this.staus;
                //}
                //catch (Exception ex)
                //{
                //    return "";
                //}
            }
        }
        public string email
        {
            get
            {
                try
                {
                    if ( this.ZctCatCte != null){
                        if (this.ZctCatCte.email != null)
                        {
                            return this.ZctCatCte.email;
                        }else{
                            return "Registre un Email";
                        }
                    }
                    else
                    {
                        return "Registre un Email";
                    }
                }
                catch (Exception ex)
                {
                    return "";
                }
            }
        }
        public string direccion 
        {
            get
            {
                try
                {
                    if ( this.ZctCatCte != null)
                    {
                        return this.ZctCatCte.calle + " " + this.ZctCatCte.numext + " " + this.ZctCatCte.numint + " " + this.ZctCatCte.colonia + " CP: " + this.ZctCatCte.cp.ToString() + " " + this.ZctCatCte.ZctCatCiudad.Desc_Ciu.ToString() + " " + this.ZctCatCte.ZctCatEstado.Desc_Edo;
                    }
                    else {
                        return "";
                    }
                }
                catch (Exception ex)
                {
                    return "Null verifique la BD";
                }
            }
        }
        public string rfc
        {
            get
            {
                if (this.ZctCatCte != null)
                {
                    if (this.ZctCatCte.RFC_Cte == null || string.IsNullOrEmpty(this.ZctCatCte.RFC_Cte) || string.IsNullOrWhiteSpace(this.ZctCatCte.RFC_Cte) || this.ZctCatCte.RFC_Cte == "")
                    {
                        return "XXX010101XXX";
                    }
                    else
                    {
                        return this.ZctCatCte.RFC_Cte;
                    }
                }
                else {
                    return "";
                }
            }
        }
    }
   
    partial class ZctCatProv1
    {
        public string DireccionProveedor
        {
            get
            {
                try
                {
                    return this.calle + " " + this.numext + " " + this.numint + " " + this.colonia + " CP: " + this.cp.ToString() + " " + this.ciudad + " " + this.estado;
                }
                catch (Exception ex)
                {
                    return "Null verifique la BD";
                }
            }
        }
    }

    partial class DAODataContext
    {
    }
    partial class ZctFacturaEnc {
        public decimal subtotal {
            get {
                return this.importe_factura - this.iva_factura;
            }
        }
        public string status
        {
            get
            {
                switch (this.status_factura)
                {
                    case 'F':
                        return "FACTURADO";

                    case 'C':
                        return "CANCELADO";
                    default:
                        return "POR FACTURAR";
                }
            }
        }

        public System.Drawing.Color status_color
        {
            get
            {
                switch (this.status_factura)
                {
                    case 'F':
                        return System.Drawing.Color.PaleGreen;

                    case 'C':
                        return System.Drawing.Color.OrangeRed;

                    default:
                        return System.Drawing.Color.Tan;
                }
            }
        }
        public bool nueva;
    }
    partial class ZctFacturaDet {
        public string articulo {
            get {
                if (this.ZctCatArt != null)
                    return this.ZctCatArt.Desc_Art;
                else
                    return "";
            }
        }
       
    }
    partial class ZctCatCte {
        public string nombre {
            get {
                return this.Nom_Cte.ToString() + " " + this.ApPat_Cte.ToString() + " " + this.ApMat_Cte.ToString();
            }
        }
        public string direccion
        {
            get
            {
                return this.calle + " " + this.numext + " " + this.numint + " " + this.colonia + " CP: " + this.cp.ToString() + " " + this.ZctCatCiudad.Desc_Ciu + " " + this.ZctCatEstado.Desc_Edo + " " + this.pais;
            }
        }
    }
}
