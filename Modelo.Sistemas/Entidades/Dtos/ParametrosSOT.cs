﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Modelo.Sistemas.Entidades.Dtos
{
    public class ParametrosSOT : INotifyPropertyChanged
    {
        private string _isotipo;

        public string Isotipo
        {
            get { return _isotipo; }
            set
            {
                _isotipo = value;
                NotifyPropertyChanged();
            }
        }

        private string _imagotipo;

        public string Imagotipo
        {
            get { return _imagotipo; }
            set
            {
                _imagotipo = value;
                NotifyPropertyChanged();
            }
        }

        private string isologo;
        public string Isologo
        {
            get { return isologo; }
            set
            {
                isologo = value;
                NotifyPropertyChanged();
            }
        }

        private string imagenReportes;
        public string ImagenReportes
        {
            get { return imagenReportes; }
            set
            {
                imagenReportes = value;
                NotifyPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}