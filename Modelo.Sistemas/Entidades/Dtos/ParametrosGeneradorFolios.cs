﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Sistemas.Entidades.Dtos
{
    public class ParametrosGeneradorFolios : INotifyPropertyChanged
    {
        private string _servidor;
        public string Servidor
        {
            get { return _servidor; }
            set
            {
                _servidor = value;
                NotifyPropertyChanged();
            }
        }
        private int _puerto;
        public int Puerto
        {
            get { return _puerto; }
            set
            {
                _puerto = value;
                NotifyPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
