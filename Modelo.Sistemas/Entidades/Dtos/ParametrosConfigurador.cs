﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Sistemas.Entidades.Dtos
{
    public class ParametrosConfigurador
    {
        public bool ConfiguracionFinalizada { get; set; }
    }
}
