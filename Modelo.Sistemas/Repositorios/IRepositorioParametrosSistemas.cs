﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;
using Modelo.Sistemas.Entidades;

namespace Modelo.Sistemas.Repositorios
{
    public interface IRepositorioParametrosSistemas: IRepositorio<ParametroSistema>
    {
    }
}
