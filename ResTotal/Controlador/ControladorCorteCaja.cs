﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ResTotal.Controlador
{
    class ControladorCorteCaja
    {
        Modelo.DAODataContext _dao;
        int _user;
        string _Cod_folio;
        public ControladorCorteCaja(string cnn, int cod_user,string Cod_folio)
        {
            _dao = new Modelo.DAODataContext(cnn);
            _user = cod_user;
            _Cod_folio = Cod_folio;
        }
        public void CorteCaja(string cod_folio,decimal TotalCaja,decimal TotalCajaFisico)
        {
            Modelo.ZctCortesCajas corte = new Modelo.ZctCortesCajas();
            corte.cod_usu = _user;
            corte.fecha_corte = DateTime.Now;
            corte.total_caja = TotalCaja;
            corte.cod_folio = cod_folio;
            corte.total_caja_fisico = TotalCajaFisico;
            _dao.ZctCortesCajas.InsertOnSubmit(corte);
            //_dao.SP_Cortecaja(_user, _Cod_folio);
            _dao.Sp_ZctCorteCaja(_Cod_folio, _user);
            _dao.SubmitChanges();
        }
    }
}
