﻿namespace ResTotal.Vista
{
    partial class frmPagosCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zctPagoCajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.referenciaTextBox = new System.Windows.Forms.TextBox();
            this.Blimpiar = new System.Windows.Forms.Button();
            this.Bgrabar = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.lblReferencia = new System.Windows.Forms.Label();
            this.montoTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cuentaTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.zctPagoCajaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // zctPagoCajaBindingSource
            // 
            this.zctPagoCajaBindingSource.DataSource = typeof(System.Data.Linq.EntitySet<ResTotal.Modelo.ZctPagoCaja>);
            // 
            // referenciaTextBox
            // 
            this.referenciaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctPagoCajaBindingSource, "referencia", true));
            this.referenciaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.referenciaTextBox.Location = new System.Drawing.Point(68, 101);
            this.referenciaTextBox.Name = "referenciaTextBox";
            this.referenciaTextBox.Size = new System.Drawing.Size(160, 29);
            this.referenciaTextBox.TabIndex = 1;
            this.referenciaTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.referenciaTextBox_Validating);
            // 
            // Blimpiar
            // 
            this.Blimpiar.CausesValidation = false;
            this.Blimpiar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Blimpiar.Location = new System.Drawing.Point(158, 202);
            this.Blimpiar.Name = "Blimpiar";
            this.Blimpiar.Size = new System.Drawing.Size(103, 54);
            this.Blimpiar.TabIndex = 4;
            this.Blimpiar.Text = "Cancelar";
            this.Blimpiar.UseVisualStyleBackColor = true;
            this.Blimpiar.Click += new System.EventHandler(this.Blimpiar_Click);
            // 
            // Bgrabar
            // 
            this.Bgrabar.Location = new System.Drawing.Point(40, 202);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 54);
            this.Bgrabar.TabIndex = 3;
            this.Bgrabar.Text = "Grabar";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Green;
            this.label21.Location = new System.Drawing.Point(102, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 27);
            this.label21.TabIndex = 20;
            this.label21.Text = "Monto:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblReferencia
            // 
            this.lblReferencia.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReferencia.ForeColor = System.Drawing.Color.Black;
            this.lblReferencia.Location = new System.Drawing.Point(90, 71);
            this.lblReferencia.Name = "lblReferencia";
            this.lblReferencia.Size = new System.Drawing.Size(130, 27);
            this.lblReferencia.TabIndex = 21;
            this.lblReferencia.Text = "Referencia:";
            this.lblReferencia.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // montoTextBox
            // 
            this.montoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctPagoCajaBindingSource, "monto", true));
            this.montoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.montoTextBox.Location = new System.Drawing.Point(68, 39);
            this.montoTextBox.Name = "montoTextBox";
            this.montoTextBox.Size = new System.Drawing.Size(160, 29);
            this.montoTextBox.TabIndex = 0;
            this.montoTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.montoTextBox_Validating);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(102, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 27);
            this.label1.TabIndex = 23;
            this.label1.Text = "Cuenta:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cuentaTextBox
            // 
            this.cuentaTextBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.cuentaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctPagoCajaBindingSource, "cuenta", true));
            this.cuentaTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuentaTextBox.Location = new System.Drawing.Point(68, 161);
            this.cuentaTextBox.MaxLength = 4;
            this.cuentaTextBox.Name = "cuentaTextBox";
            this.cuentaTextBox.Size = new System.Drawing.Size(160, 29);
            this.cuentaTextBox.TabIndex = 2;
            this.cuentaTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.cuentaTextBox_Validating);
            // 
            // frmPagosCaja
            // 
            this.AcceptButton = this.Bgrabar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Blimpiar;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(294, 276);
            this.Controls.Add(this.cuentaTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.montoTextBox);
            this.Controls.Add(this.lblReferencia);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Blimpiar);
            this.Controls.Add(this.Bgrabar);
            this.Controls.Add(this.referenciaTextBox);
            this.Name = "frmPagosCaja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pagos";
            this.Load += new System.EventHandler(this.frmPagosCaja_Load);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.frmPagosCaja_Validating);
            ((System.ComponentModel.ISupportInitialize)(this.zctPagoCajaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource zctPagoCajaBindingSource;
        private System.Windows.Forms.TextBox referenciaTextBox;
        private System.Windows.Forms.Button Blimpiar;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblReferencia;
        private System.Windows.Forms.TextBox montoTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cuentaTextBox;
    }
}