﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ResTotal.Vista
{
    public partial class ZCTbusqueda : Form
    {
        string _valor="0";
        object _DS;
        public string valor
        {
            set { _valor = value; }
            get { return _valor; }
        }
        public ZCTbusqueda(Object ds)
        {
            InitializeComponent();
            _DS = ds;
            DGBusqueda.CellContentClick+=new DataGridViewCellEventHandler(DGBusqueda_CellContentClick);
            DGBusqueda.ColumnHeaderMouseDoubleClick += new DataGridViewCellMouseEventHandler(DGBusqueda_ColumnHeaderMouseDoubleClick);
            DGBusqueda.RowEnter += new DataGridViewCellEventHandler(DGBusqueda_RowEnter);

            textbusqueda.LostFocus += new EventHandler(textbusqueda_LostFocus);
            DGBusqueda.AutoGenerateColumns = true;
            this.bs.DataSource = _DS;
        }

        void DGBusqueda_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            
            valor = DGBusqueda.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        void DGBusqueda_ColumnHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            textbusqueda.Name = DGBusqueda.Columns[e.ColumnIndex].HeaderText.ToString();
            textbusqueda.Text = "";
            textbusqueda.Enabled = true;
            textbusqueda.Focus();
        }

        void textbusqueda_LostFocus(object sender, EventArgs e)
        {
            if (textbusqueda.Name != "Columna:" && textbusqueda.Text !="")
             {
                 bs.Filter = textbusqueda.Name + " LIKE '%" + textbusqueda.Text + "%' ";
                 DGBusqueda.DataSource = bs;
             }  
 
        }

        private void DGBusqueda_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            valor = DGBusqueda.CurrentRow.Cells[0].Value.ToString();
            this.DialogResult = DialogResult.OK;
        }

        private void cmdAceptar_Click(object sender, EventArgs e)
        {
            if (DGBusqueda.CurrentRow != null)
            {
                valor = DGBusqueda.CurrentRow.Cells[0].Value.ToString();
            }
            this.DialogResult = DialogResult.OK;
        }

        private void Bcancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

    }
}
