﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Modelo.Almacen.Entidades;
namespace ResTotal.Vista
{
    public partial class ZctCatProv : Form
    {
        public delegate int DBuscaProveedor();
        int _user, _pos = 0;
        string _cnn = "";
        bool _buscando = false;
        global::Modelo.Seguridad.Dtos.DtoPermisos _permisos;
        //Permisos.Controlador.Controlador.vistapermisos _permisos;
        SOTControladores.Controladores.ControladorPermisos _ControladorPermisos;
        //Permisos.Controlador.Controlador _ControladorPermisos;
        string sSqlBusqueda = "select Cod_Prov,Nom_Prov,RFC_Prov from ZctCatProv order by Cod_Prov";
        Proveedor _NuevoProveedor;
        //SOTControladores.Controladores.ControladorProveedores ControladorProveedores;
        public ZctCatProv(string cnn, int user)
        {
            InitializeComponent();
            _user = user;
            _cnn = cnn;
            //this.estadoComboBox.LostFocus += new EventHandler(estadoComboBox_LostFocus);
            //ControladorProveedores = new Controlador.ControladorProveedores(cnn);
            cod_ProvTextBox.LostFocus += new EventHandler(cod_ProvTextBox_LostFocus);
            TxtNumPagos.TextChanged += new EventHandler(TextoMayorAcero);
            TxtFrecPagos.TextChanged += new EventHandler(TextoMayorAcero);
            cpTextBox.KeyPress += new KeyPressEventHandler(cpTextBox_KeyPress);
        }

        void cpTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != 46))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        private event DBuscaProveedor _buscaProveedor;

        public event DBuscaProveedor BuscaProveedor
        {
            add
            {
                if (_buscaProveedor != null)
                {
                    foreach (Delegate existingHandler in _buscaProveedor.GetInvocationList())
                    {
                        if (!existingHandler.Equals(value))
                        {
                            _buscaProveedor += value;
                        }
                    }
                }
                else
                    _buscaProveedor += value;
            }
            remove
            {
                if (_buscaProveedor != null)
                {
                    foreach (Delegate existingHandler in _buscaProveedor.GetInvocationList())
                    {
                        if (existingHandler.Equals(value))
                        {
                            _buscaProveedor -= value;
                        }
                    }
                }
            }
        }

        private bool IsEventHandlerRegistered(Delegate prospectiveHandler)
        {
            
            return false;
        }

        //void estadoComboBox_LostFocus(object sender, EventArgs e)
        //{
        //    var cprov = new SOTControladores.Controladores.ControladorProveedores();
        //    var controladorCiudades = new SOTControladores.Controladores.ControladorCiudades();

        //    var estado = estadoComboBox.SelectedItem as Estado;

        //    if (estado != null)
        //    {
        //        ciudadComboBox.DataSource = controladorCiudades.ObtenerCiudades(estado.Cod_Edo);
        //    }
        //    else
        //    {
        //        ciudadComboBox.DataSource = controladorCiudades.ObtenerCiudades();
        //    }

        //}
        void cod_ProvTextBox_LostFocus(object sender, EventArgs e)
        {
            var cprov = new SOTControladores.Controladores.ControladorProveedores();

            if (_buscando == false)
            {
                ZctCatProvBindingSource.CancelEdit();
                _NuevoProveedor = null;
            }
            try
            {
                if (cod_ProvTextBox.Text == "")
                {
                    cod_ProvTextBox.Text = "0";
                }
                var proveedorExistente = cprov.ObtenerProveedor(Convert.ToInt32(cod_ProvTextBox.Text));
                if (proveedorExistente == null)
                {
                    NuevoProveedor();
                    return;
                }
                int cod = Convert.ToInt32(cod_ProvTextBox.Text);
                _pos = cprov.ObtenerPosicionProveedor(Convert.ToInt32(cod_ProvTextBox.Text));
                ZctCatProvBindingSource.DataSource = proveedorExistente;
                cod_ProvTextBox.Enabled = false;
                _buscando = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void NuevoProveedor()
        {
            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

            cod_ProvTextBox.Enabled = true;
            //ZctCatProvBindingSource.AddNew();
            _pos = ControladorProveedores.ObtenerProveedores().Count;
            _NuevoProveedor = new Proveedor();
            ZctCatProvBindingSource.DataSource = _NuevoProveedor;
            cod_ProvTextBox.Text = ControladorProveedores.ObtenerSiguienteCodigoProveedor();
            nom_ProvTextBox.Focus();
            Beliminar.Enabled = false;
        }
        private void ZctCatProv_Load(object sender, EventArgs e)
        {
            _ControladorPermisos = new SOTControladores.Controladores.ControladorPermisos();//Permisos.Controlador.Controlador(_cnn);
            _permisos = _ControladorPermisos.ObtenerPermisosActuales();//.getpermisoventana(_user, this.Text);

            var controladorEstados = new SOTControladores.Controladores.ControladorEstados();
            var controladorCiudades = new SOTControladores.Controladores.ControladorCiudades();

            bool Edita = (_permisos.CrearProveedor || _permisos.ModificarProveedor);
            Beliminar.Enabled = _permisos.EliminarProveedor;
            Bguardar.Enabled = Edita;

            BSestado.DataSource = controladorEstados.ObtenerEstados();
            BsCiudad.DataSource = controladorCiudades.ObtenerCiudades();
            if (string.IsNullOrEmpty(cod_ProvTextBox.Text))
            {
                NuevoProveedor();
            }
        }
        private void Bant_Click(object sender, EventArgs e)
        {
            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

            ZctCatProvBindingSource.CancelEdit();
            if (_pos <= 0)
            {
                return;
            }
            cod_ProvTextBox.Enabled = false;
            cod_ProvTextBox.Text = "";
            ZctCatProvBindingSource.CancelEdit();
            ZctCatProvBindingSource.DataSource = ControladorProveedores.ObtenerProveedores();
            ZctCatProvBindingSource.Position = _pos - 1;
            _NuevoProveedor = null;
            //ZctCatProvBindingSource.MovePrevious ();
            _pos -= 1;
            Beliminar.Enabled = _permisos.EliminarProveedor;
        }
        private void Bsig_Click(object sender, EventArgs e)
        {
            ZctCatProvBindingSource.CancelEdit();
            if (_pos + 1 >= ZctCatProvBindingSource.List.Count)
            {
                NuevoProveedor();
                return;
            }
            cod_ProvTextBox.Enabled = false;
            ZctCatProvBindingSource.MoveNext();
            _pos += 1;
            Beliminar.Enabled = _permisos.EliminarProveedor;
        }
        private void Bguardar_Click(object sender, EventArgs e)
        {
            try
            {
                var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

                if (_NuevoProveedor != null)
                {
                    ControladorProveedores.CrearProveedor(_NuevoProveedor);

                }
                else 
                {
                    var prov = ZctCatProvBindingSource.Current as Proveedor;
                    if (prov == null)
                        return;

                    ControladorProveedores.ModificarProveedor(prov);
                }

                MessageBox.Show("Proveedor guardado correctamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                NuevoProveedor();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Bcancelar_Click(object sender, EventArgs e)
        {
            NuevoProveedor();
            cod_ProvTextBox.Enabled = true;
        }
        private void Beliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ZctCatProvBindingSource.CancelEdit();
                DialogResult R = MessageBox.Show("¿Desea eliminar el proveedor? Esta acción no se podrá deshacer", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (R == DialogResult.Yes)
                {
                    var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

                    ControladorProveedores.Elimina(((Proveedor)ZctCatProvBindingSource.Current).Cod_Prov);
                    ZctCatProvBindingSource.RemoveCurrent();
                    ZctCatProvBindingSource.EndEdit();
                    ZctCatProvBindingSource.DataSource = ControladorProveedores.ObtenerProveedores();
                    MessageBox.Show("Proveedor eliminado", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    NuevoProveedor();
                    cod_ProvTextBox.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void TxtdiasCred_TextChanged(object sender, EventArgs e)
        {

        }
        private void TextoMayorAcero(object sender, EventArgs e)
        {
            Control MiControl = (Control)sender;
            int res;

            if (!int.TryParse(MiControl.Text, out res) || res <= 0)
                MiControl.Text = "1";
        }
        public void CargaProveedor(int cod_prov)
        {
            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

            cod_ProvTextBox.Text = cod_prov.ToString();
            _pos = ControladorProveedores.ObtenerPosicionProveedor(cod_prov);
            ZctCatProvBindingSource.DataSource = ControladorProveedores.ObtenerProveedor(cod_prov);
            cod_ProvTextBox.Enabled = false;
        }

        private void TxtNumPagos_TextChanged(object sender, EventArgs e)
        {
            //if (TxtNumPagos.Text == "")
            //{
            //    TxtNumPagos.Text = "1";
            //    //TxtNumPagos.Enabled = false;
            //    //lpagos.visible = false;

            //    TxtFrecPagos.Text = "0";
            //    TxtFrecPagos.Enabled = false;
            //    return;
            //}
            int res;

            if (!int.TryParse(TxtNumPagos.Text, out res) || res <= 1)
            {
                if (res < 1)
                {
                    TxtNumPagos.Text = "1";
                    return;
                }
                //TxtNumPagos.Enabled = false;
                //lpagos.visible = false;

                TxtFrecPagos.Text = "0";
                TxtFrecPagos.Enabled = false;

                return;
            }
            else if (res/*Convert.ToInt32(TxtNumPagos.Text)*/ > 1)
            {

                //TxtNumPagos.Text = "1";
                //TxtNumPagos.Enabled = true;

                //TxtFrecPagos.Text = "1";
                TxtFrecPagos.Enabled = true;

            }
        }

        private void cod_ProvTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void telefonoTextBox_TextChanged(object sender, EventArgs e)
        {
            var s = sender as TextBox;

            if (System.Text.RegularExpressions.Regex.IsMatch(s.Text, "  ^ [0-9]"))
            {
                s.Text = "";
            }
        }
        private void telefonoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) /*&& (e.KeyChar != '.')*/)
            {
                e.Handled = true;
            }
        }

        void cod_ProvTextBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                _buscando = true;
                ZctCatProvBindingSource.CancelEdit();
                cod_ProvTextBox.Text = _buscaProveedor().ToString();
            }
        }
    }

}
