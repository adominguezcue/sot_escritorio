﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Modelo.Almacen.Entidades;
namespace ResTotal.Vista
{
    public partial class ZctCatProvV2 : Form
    {
        int _user, _pos = 0;
        string _cnn = "";
        bool _buscando = false;
        global::Modelo.Seguridad.Dtos.DtoPermisos _permisos;
        int idProveedor = 0;

        SOTControladores.Controladores.ControladorPermisos _ControladorPermisos;

        string sSqlBusqueda = "select Cod_Prov,Nom_Prov,RFC_Prov from ZctCatProv order by Cod_Prov";
        Proveedor _NuevoProveedor;

        public ZctCatProvV2(string cnn, int user, int codigoProveedor = 0)
        {
            InitializeComponent();
            _user = user;
            _cnn = cnn;
            idProveedor = codigoProveedor;

            TxtNumPagos.TextChanged += new EventHandler(TextoMayorAcero);
            TxtFrecPagos.TextChanged += new EventHandler(TextoMayorAcero);
            cpTextBox.KeyPress += new KeyPressEventHandler(cpTextBox_KeyPress);
        }

        void cpTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != 46))
            {
                e.Handled = true;
                return;
            }
        }

        private bool IsEventHandlerRegistered(Delegate prospectiveHandler)
        {
            
            return false;
        }

        //void cod_ProvTextBox_LostFocus(object sender, EventArgs e)
        //{
        //    var cprov = new SOTControladores.Controladores.ControladorProveedores();

        //    if (_buscando == false)
        //    {
        //        ZctCatProvBindingSource.CancelEdit();
        //        _NuevoProveedor = null;
        //    }
        //    try
        //    {
        //        var proveedorExistente = cprov.ObtenerProveedor(idProveedor);
        //        if (proveedorExistente == null)
        //        {
        //            NuevoProveedor();
        //            return;
        //        }
        //        _pos = cprov.ObtenerPosicionProveedor(idProveedor);
        //        ZctCatProvBindingSource.DataSource = proveedorExistente;
        //        //cod_ProvTextBox.Enabled = false;
        //        _buscando = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //}
        private void NuevoProveedor()
        {
            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

            //cod_ProvTextBox.Enabled = true;
            //ZctCatProvBindingSource.AddNew();
            _pos = ControladorProveedores.ObtenerProveedores().Count;
            _NuevoProveedor = new Proveedor();
            ZctCatProvBindingSource.DataSource = _NuevoProveedor;
            //cod_ProvTextBox.Text = ControladorProveedores.ObtenerSiguienteCodigoProveedor();
            nom_ProvTextBox.Focus();
            //Beliminar.Enabled = false;
        }
        private void ZctCatProv_Load(object sender, EventArgs e)
        {
            _ControladorPermisos = new SOTControladores.Controladores.ControladorPermisos();//Permisos.Controlador.Controlador(_cnn);
            _permisos = _ControladorPermisos.ObtenerPermisosActuales();//.getpermisoventana(_user, this.Text);

            var controladorEstados = new SOTControladores.Controladores.ControladorEstados();
            var controladorCiudades = new SOTControladores.Controladores.ControladorCiudades();

            bool Edita = (_permisos.CrearProveedor || _permisos.ModificarProveedor);
            //Beliminar.Enabled = _permisos.EliminarProveedor;
            Bguardar.Enabled = Edita;

            BSestado.DataSource = controladorEstados.ObtenerEstados();
            BsCiudad.DataSource = controladorCiudades.ObtenerCiudades();

            if (idProveedor == 0)
                NuevoProveedor();
            else
                CargaProveedor(idProveedor);
        }
        private void Bguardar_Click(object sender, EventArgs e)
        {
            try
            {
                var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

                if (_NuevoProveedor != null)
                {
                    ControladorProveedores.CrearProveedor(_NuevoProveedor);

                }
                else 
                {
                    var prov = ZctCatProvBindingSource.Current as Proveedor;
                    if (prov == null)
                        return;

                    ControladorProveedores.ModificarProveedor(prov);
                }

                MessageBox.Show("Proveedor guardado correctamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                NuevoProveedor();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Bcancelar_Click(object sender, EventArgs e)
        {
            CargaProveedor(idProveedor);
        }
        //private void Beliminar_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ZctCatProvBindingSource.CancelEdit();
        //        DialogResult R = MessageBox.Show("¿Desea eliminar el proveedor? Esta acción no se podrá deshacer", "Atención", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //        if (R == DialogResult.Yes)
        //        {
        //            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

        //            ControladorProveedores.Elimina(((Proveedor)ZctCatProvBindingSource.Current).Cod_Prov);
        //            ZctCatProvBindingSource.RemoveCurrent();
        //            ZctCatProvBindingSource.EndEdit();
        //            ZctCatProvBindingSource.DataSource = ControladorProveedores.ObtenerProveedores();
        //            MessageBox.Show("Proveedor eliminado", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            NuevoProveedor();
        //            cod_ProvTextBox.Enabled = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //}
        private void TxtdiasCred_TextChanged(object sender, EventArgs e)
        {

        }
        private void TextoMayorAcero(object sender, EventArgs e)
        {
            Control MiControl = (Control)sender;
            int res;

            if (!int.TryParse(MiControl.Text, out res) || res <= 0)
                MiControl.Text = "1";
        }
        private void CargaProveedor(int cod_prov)
        {
            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

            //cod_ProvTextBox.Text = cod_prov.ToString();
            _pos = ControladorProveedores.ObtenerPosicionProveedor(cod_prov);
            ZctCatProvBindingSource.DataSource = ControladorProveedores.ObtenerProveedor(cod_prov);
            //cod_ProvTextBox.Enabled = false;
        }

        private void TxtNumPagos_TextChanged(object sender, EventArgs e)
        {
            int res;

            if (!int.TryParse(TxtNumPagos.Text, out res) || res <= 1)
            {
                if (res < 1)
                {
                    TxtNumPagos.Text = "1";
                    return;
                }

                TxtFrecPagos.Text = "0";
                TxtFrecPagos.Enabled = false;

                return;
            }
            else if (res > 1)
            {
                TxtFrecPagos.Enabled = true;

            }
        }

        private void cod_ProvTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void telefonoTextBox_TextChanged(object sender, EventArgs e)
        {
            var s = sender as TextBox;

            if (System.Text.RegularExpressions.Regex.IsMatch(s.Text, "  ^ [0-9]"))
            {
                s.Text = "";
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void telefonoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) /*&& (e.KeyChar != '.')*/)
            {
                e.Handled = true;
            }
        }
    }

}
