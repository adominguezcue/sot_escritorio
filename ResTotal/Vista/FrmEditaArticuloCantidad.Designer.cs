﻿namespace ResTotal.Vista
{
    partial class FrmEditaArticuloCantidad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label costoArticuloOTLabel;
            System.Windows.Forms.Label desc_ArtLabel;
            System.Windows.Forms.Label cod_ArtLabel;
            this.Blimpiar = new System.Windows.Forms.Button();
            this.Bgrabar = new System.Windows.Forms.Button();
            this.desc_ArtLabel1 = new System.Windows.Forms.Label();
            this.cod_ArtLabel1 = new System.Windows.Forms.Label();
            this.ctd_ArtNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.zctDetCajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            costoArticuloOTLabel = new System.Windows.Forms.Label();
            desc_ArtLabel = new System.Windows.Forms.Label();
            cod_ArtLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ctd_ArtNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zctDetCajaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // Blimpiar
            // 
            this.Blimpiar.CausesValidation = false;
            this.Blimpiar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Blimpiar.Location = new System.Drawing.Point(224, 119);
            this.Blimpiar.Name = "Blimpiar";
            this.Blimpiar.Size = new System.Drawing.Size(103, 54);
            this.Blimpiar.TabIndex = 31;
            this.Blimpiar.Text = "Cancelar";
            this.Blimpiar.UseVisualStyleBackColor = true;
            this.Blimpiar.Click += new System.EventHandler(this.Blimpiar_Click);
            // 
            // Bgrabar
            // 
            this.Bgrabar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Bgrabar.Location = new System.Drawing.Point(78, 119);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 54);
            this.Bgrabar.TabIndex = 30;
            this.Bgrabar.Text = "Grabar";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // costoArticuloOTLabel
            // 
            costoArticuloOTLabel.AutoSize = true;
            costoArticuloOTLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            costoArticuloOTLabel.Location = new System.Drawing.Point(72, 79);
            costoArticuloOTLabel.Name = "costoArticuloOTLabel";
            costoArticuloOTLabel.Size = new System.Drawing.Size(89, 24);
            costoArticuloOTLabel.TabIndex = 28;
            costoArticuloOTLabel.Text = "Cantidad:";
            // 
            // desc_ArtLabel
            // 
            desc_ArtLabel.AutoSize = true;
            desc_ArtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            desc_ArtLabel.Location = new System.Drawing.Point(46, 40);
            desc_ArtLabel.Name = "desc_ArtLabel";
            desc_ArtLabel.Size = new System.Drawing.Size(115, 24);
            desc_ArtLabel.TabIndex = 26;
            desc_ArtLabel.Text = "Descripción:";
            // 
            // desc_ArtLabel1
            // 
            this.desc_ArtLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.desc_ArtLabel1.Location = new System.Drawing.Point(167, 41);
            this.desc_ArtLabel1.Name = "desc_ArtLabel1";
            this.desc_ArtLabel1.Size = new System.Drawing.Size(220, 23);
            this.desc_ArtLabel1.TabIndex = 27;
            this.desc_ArtLabel1.Text = "label2";
            // 
            // cod_ArtLabel
            // 
            cod_ArtLabel.AutoSize = true;
            cod_ArtLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            cod_ArtLabel.Location = new System.Drawing.Point(83, 5);
            cod_ArtLabel.Name = "cod_ArtLabel";
            cod_ArtLabel.Size = new System.Drawing.Size(78, 24);
            cod_ArtLabel.TabIndex = 24;
            cod_ArtLabel.Text = "Artículo:";
            // 
            // cod_ArtLabel1
            // 
            this.cod_ArtLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "Cod_Art", true));
            this.cod_ArtLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cod_ArtLabel1.Location = new System.Drawing.Point(167, 6);
            this.cod_ArtLabel1.Name = "cod_ArtLabel1";
            this.cod_ArtLabel1.Size = new System.Drawing.Size(220, 23);
            this.cod_ArtLabel1.TabIndex = 25;
            this.cod_ArtLabel1.Text = "label1";
            // 
            // ctd_ArtNumericUpDown
            // 
            this.ctd_ArtNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.zctDetCajaBindingSource, "Ctd_Art", true));
            this.ctd_ArtNumericUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctd_ArtNumericUpDown.Location = new System.Drawing.Point(167, 74);
            this.ctd_ArtNumericUpDown.Name = "ctd_ArtNumericUpDown";
            this.ctd_ArtNumericUpDown.Size = new System.Drawing.Size(128, 29);
            this.ctd_ArtNumericUpDown.TabIndex = 32;
            // 
            // zctDetCajaBindingSource
            // 
            this.zctDetCajaBindingSource.DataSource = typeof(ResTotal.Modelo.ZctDetCaja);
            // 
            // FrmEditaArticuloCantidad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 180);
            this.Controls.Add(this.ctd_ArtNumericUpDown);
            this.Controls.Add(this.Blimpiar);
            this.Controls.Add(this.Bgrabar);
            this.Controls.Add(costoArticuloOTLabel);
            this.Controls.Add(desc_ArtLabel);
            this.Controls.Add(this.desc_ArtLabel1);
            this.Controls.Add(cod_ArtLabel);
            this.Controls.Add(this.cod_ArtLabel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEditaArticuloCantidad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Modifica Articulo Cantidad";
            this.Load += new System.EventHandler(this.FrmEditaArticuloCantidad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ctd_ArtNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zctDetCajaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Blimpiar;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.Label desc_ArtLabel1;
        private System.Windows.Forms.Label cod_ArtLabel1;
        private System.Windows.Forms.NumericUpDown ctd_ArtNumericUpDown;
        private System.Windows.Forms.BindingSource zctDetCajaBindingSource;

    }
}