﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace ResTotal.Vista
{
    public delegate int ModificaCliente(int cod_cte);
    public delegate void CreaPdf(int folio, string cod_folio, string pathRpt, string pathRpt_ruta, string nombre, string email);
    public delegate void imprime_factura(int folio, string cod_folio, string pathRpt);


    public partial class frmFacturacion : Form
    {
        //ff
        Permisos.Controlador.Controlador.vistapermisos _permisos;
        Permisos.Controlador.Controlador _ControladorPermisos;
        Modelo.ZctFacturaEnc factura = new Modelo.ZctFacturaEnc();
        string folio_facturacion = "FC";
        int _user = 0;

        ResTotal.Controlador.ControladorFacturacion Controlador;

        public frmFacturacion(string cnn, int user, FacturacionElectronica.Procesa.TimbrarCFDPrueba timbra, FacturacionElectronica.Procesa.CancelarCFDI cancela)
        {
            _user = user;
            InitializeComponent();
            //DGVpagos.RowsAdded += new DataGridViewRowsAddedEventHandler(DGVpagos_RowsAdded);
            Controlador = new Controlador.ControladorFacturacion(cnn);
            Controlador.timbra = timbra;
            Controlador.cancela = cancela;
            //DGVpagos.RowsRemoved += new DataGridViewRowsRemovedEventHandler(DGVpagos_RowsRemoved);
            _ControladorPermisos = new Permisos.Controlador.Controlador(cnn);
        }

        private void frmFacturacion_Load(object sender, EventArgs e)
        {

            //    _permisos = _ControladorPermisos.getpermisoventana(_user, this.Text);
            //    DGVpagos.ReadOnly = !_permisos.edita;
            //    Bgrabar.Enabled = (_permisos.edita || _permisos.agrega);

            //    _Consecutivo = ControladorCaja.ConsultaFolioCaja();
            //    TiposPagoBindingSource.DataSource = ControladorCaja.TraeTiposPago();
            //    Tipo_pago.DataPropertyName = "codtipo_pago";
            //    Tipo_pago.DataSource = TiposPagoBindingSource;
            //    Tipo_pago.DisplayMember = "desc_pago";
            //    Tipo_pago.ValueMember = "codtipo_pago";

            Limpiar();

        }

        private void Limpiar()
        {
            controles_default();
            nueva_factura();

        }

        private void nueva_factura()
        {

            factura = Controlador.nueva_factura(folio_facturacion);
            //zctFacturaEncBindingSource.DataSource = factura;
            carga_datasource(factura);
        }

        private void CargaOrden(int folio)
        {
            try
            {
                factura = Controlador.obtiene_objeto_factura(folio, folio_facturacion);
                if (factura != null)
                {
                    //zctFacturaEncBindingSource.DataSource = factura;
                    if (factura.status_factura == 'C' && MessageBox.Show("La factura ha sido cancelada, ¿Desea refacturar la orden?", "Factura Cancelada", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        factura = Controlador.obtiene_factura_desde_ot(folio, folio_facturacion);
                    }
                    carga_datasource(factura);
                    habilita(factura.nueva);
                    deshabilita_folios();
                }
                else
                {
                    MessageBox.Show("El pedido no pudo realizar la factura");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error cargando la orden: " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void deshabilita_folios()
        {
            //TxtPedido.Enabled = false;
            TxtFolio.Enabled = false;
        }

        private void TxtPedido_Leave(object sender, EventArgs e)
        {
            try
            {

                if (TxtPedido.Text != "" && TxtPedido.Text != "0")
                {
                    int folio_ot = Convert.ToInt32(TxtPedido.Text);

                    CargaOrden(folio_ot);
                }
                else
                {
                    nueva_factura();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void Bgrabar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Controlador.inserta_objeto_factura(factura);
                Cursor.Current = Cursors.Default;
                lanza_rpt.Invoke(factura.folio_factura, factura.cod_folio, Controlador.GetRutaFacturas(factura), factura.ZctCatfolios.FactRptPath_folio, factura.ZctCatCte.nombre, factura.ZctCatCte.email);
                MessageBox.Show("La factura se ha guardado correctamente.");
                Limpiar();
            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

        }

        private void procesa_error(Exception ex)
        {
            MessageBox.Show(ex.Message);
        }


        public event ModificaCliente lanza_cliente;
        public event CreaPdf lanza_rpt;
        public event imprime_factura lanza_print;
        private void cmdCambiaDatosCliente_Click(object sender, EventArgs e)
        {
            if (factura != null)
            {
                lanza_cliente(factura.cod_cte);
                factura.ZctCatCte = Controlador.carga_cliente(factura.cod_cte);
                //carga_datasource(factura);
                zctFacturaEncBindingSource.ResetCurrentItem();

            }
            else
            {
                MessageBox.Show("Primero es necesario seleccionar un documento");
            }

        }

        private void TxtFolio_Leave(object sender, EventArgs e)
        {
            try
            {
                if (TxtFolio.Text != "" && TxtFolio.Text != "0")
                {
                    int folio = Convert.ToInt32(TxtFolio.Text);
                    Obtiene_factura(folio);
                }
                else
                {
                    nueva_factura();
                }
            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
        }
        private void Obtiene_factura(int folio)
        {
            factura = Controlador.obtiene_factura(folio, folio_facturacion);
            if (factura == null)
            {
                factura = Controlador.nueva_factura(folio_facturacion);
                factura.nueva = true;
            }
            else
            {
                deshabilita_folios();
                factura.nueva = false;

            }
            habilita(factura.nueva);
            carga_datasource(factura);


        }

        private void carga_datasource(Modelo.ZctFacturaEnc factura)
        {
            zctFacturaEncBindingSource.DataSource = factura;
        }


        private void habilita(bool valor)
        {
            cmdImprimir.Enabled = !valor;
            cmdReenviar.Enabled = !valor;
            cmdGrabar.Enabled = valor;
            cmdCancelar.Enabled = !valor;
            cmdLimpiar.Enabled = true;
            cmdCambiaDatosCliente.Enabled = valor;
            TxtPedido.Enabled = valor;
        }

        private void controles_default()
        {
            habilita(true);
            cmdGrabar.Enabled = false;
            cmdLimpiar.Enabled = false;
            cmdCambiaDatosCliente.Enabled = false;
            TxtPedido.Enabled = true;
            TxtFolio.Enabled = true;
        }

        private void cmdLimpiar_Click(object sender, EventArgs e)
        {
            try
            {
                Limpiar();
            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
        }

        private void cmdCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (factura.status_factura == 'C')
                {
                    MessageBox.Show("La factura ya se encuentra cancelada");
                }
                else
                {
                    if (MessageBox.Show("¿Esta seguro que desea cancelar esta factura?, este movimiento no se podrá revertir.", "Cancelar", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Controlador.cancela_factura(factura);
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show("La factura se ha cancelado correctamente.");
                        nueva_factura();
                    }
                }


            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void cmdReenviar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                lanza_rpt.Invoke(factura.folio_factura, factura.cod_folio, Controlador.GetRutaFacturas(factura), factura.ZctCatfolios.FactRptPath_folio, factura.ZctCatCte.nombre, factura.ZctCatCte.email);
            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void cmdImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                if (lanza_print == null)
                    throw new Exception("El servicio de impresión no se encuentra disponible en estos momentos");

                lanza_print.Invoke(factura.folio_factura, factura.cod_folio, factura.ZctCatfolios.FactRptPath_folio);

            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void Bant_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtFolio.Text != "")
                {
                    int folio = Convert.ToInt32(TxtFolio.Text);
                    if (folio - 1 > 0)
                    {
                        Obtiene_factura(folio - 1);
                    }
                }
            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
        }

        private void Bsig_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtFolio.Text != "")
                {
                    int folio = Convert.ToInt32(TxtFolio.Text);
                    if (folio + 1 > 0)
                    {
                        Obtiene_factura(folio + 1);
                    }
                }
            }
            catch (Exception ex)
            {
                procesa_error(ex);
            }
        }

    }
}
