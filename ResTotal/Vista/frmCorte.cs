﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace ResTotal.Vista
{
    public partial class frmCorte : Form
    {
        int Cod_usu = 0;
        string _cod_folio;
        Controlador.ControladorCorteCaja _controladorcortes;
        Controlador.Controladorcaja _ControladorCaja;
        public delegate void DimprimeCorte(decimal fondocaja, string cod_folio, int cod_usu);
        public event DimprimeCorte ImprimeCorte;
        public frmCorte(string cnn, int cod_user,string cod_folio)
        {
            InitializeComponent();
            _cod_folio = cod_folio;
            Cod_usu = cod_user;
            _controladorcortes = new Controlador.ControladorCorteCaja(cnn, cod_user,_cod_folio);
            _ControladorCaja = new Controlador.Controladorcaja(cnn,_cod_folio,cod_user);
            Tmonto.KeyPress += new KeyPressEventHandler(Tmonto_KeyPress);
            ImprimeCorte += new DimprimeCorte(frmCorte_ImprimeCorte);
        }

        void frmCorte_ImprimeCorte(decimal fondocaja, string cod_folio, int cod_usu)
        {
            
        }

        void Tmonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != 46 ))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }


        private void BcancelarRetiro_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel; 
        }

        private void Bretirar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Tmonto.Text))
            {
                Tmonto.Focus();
                throw new Exception("Especifique un monto en caja fisica");
            }
            try
            {
                ImprimeCorte(Convert.ToDecimal(Tmonto.Text), "CJ", Cod_usu);

                decimal TotalCaja = _ControladorCaja.TraerTotalCaja("CJ", Cod_usu);
                _controladorcortes.CorteCaja("CJ", TotalCaja, Convert.ToDecimal(Tmonto.Text));
                _ControladorCaja.cierrecaja(Cod_usu, TotalCaja);

                MessageBox.Show("El corte de caja se ha Realizando", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error realizando corte de caja:" + System.Environment.NewLine + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
