﻿namespace ResTotal.Vista
{
    partial class frmCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LfechaFolio = new System.Windows.Forms.Label();
            this.Bsig = new System.Windows.Forms.Button();
            this.Bant = new System.Windows.Forms.Button();
            this.TxtFolio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmdAplicaDescuento = new System.Windows.Forms.Button();
            this.BestadoActualCaja = new System.Windows.Forms.Button();
            this.TxtPedido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Lstatus = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmdBuscaCliente = new System.Windows.Forms.Button();
            this.BcambiarCliente = new System.Windows.Forms.Button();
            this.Lemail = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Ldireccion = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LRFC = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Lcliente = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cboVendedor = new System.Windows.Forms.ComboBox();
            this.zctSegUsuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.txtCapturaArticulos = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DGVpagos = new System.Windows.Forms.DataGridView();
            this.tipo_pago = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.TiposPagoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.montoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechapagoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.corteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipopagotmp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColMontoReal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cambioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodEnc_Caja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nodoc_pago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_usu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PagosCajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DGVdetalle = new System.Windows.Forms.DataGridView();
            this.Cod_Art = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArticuloCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CostoArticuloOT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.precio_descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ctd_Art = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iva = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Importe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vendedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codfolioDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codEncCajaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nodoccajaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codArtDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctdArtDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ivaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoArticuloOTDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codEncOTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codDetOTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codusuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctDetOTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctSegUsuDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctEncCajaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctCatArtDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctDetCajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Lrestante = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Lcambio = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Lpago = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Ltotal = new System.Windows.Forms.Label();
            this.Liva = new System.Windows.Forms.Label();
            this.Lsubtotal = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.BeditarCostoArticulo = new System.Windows.Forms.Button();
            this.cmdPagoParcial = new System.Windows.Forms.Button();
            this.cmdPagoTarjeta = new System.Windows.Forms.Button();
            this.BabreCaja = new System.Windows.Forms.Button();
            this.cmdPagoEfectivo = new System.Windows.Forms.Button();
            this.BEditarCantidadArticulo = new System.Windows.Forms.Button();
            this.Bimprimir = new System.Windows.Forms.Button();
            this.Bcancelar = new System.Windows.Forms.Button();
            this.Blimpiar = new System.Windows.Forms.Button();
            this.BcorteCaja = new System.Windows.Forms.Button();
            this.Bretiro = new System.Windows.Forms.Button();
            this.Bgrabar = new System.Windows.Forms.Button();
            this.BCambiarPedido = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codfolioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codEncCajaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nodoccajaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ctdArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoArticuloOTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctEncCajaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctCatArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdCrearPedido = new System.Windows.Forms.Button();
            this.EncabezadoCajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zctSegUsuBindingSource)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVpagos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TiposPagoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagosCajaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zctDetCajaBindingSource)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EncabezadoCajaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.LfechaFolio);
            this.groupBox1.Controls.Add(this.Bsig);
            this.groupBox1.Controls.Add(this.Bant);
            this.groupBox1.Controls.Add(this.TxtFolio);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(4, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(438, 57);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // LfechaFolio
            // 
            this.LfechaFolio.AutoSize = true;
            this.LfechaFolio.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LfechaFolio.Location = new System.Drawing.Point(46, 41);
            this.LfechaFolio.Name = "LfechaFolio";
            this.LfechaFolio.Size = new System.Drawing.Size(48, 14);
            this.LfechaFolio.TabIndex = 23;
            this.LfechaFolio.Text = "sin fecha";
            // 
            // Bsig
            // 
            this.Bsig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Bsig.Location = new System.Drawing.Point(384, 12);
            this.Bsig.Name = "Bsig";
            this.Bsig.Size = new System.Drawing.Size(37, 29);
            this.Bsig.TabIndex = 3;
            this.Bsig.Text = ">";
            this.Bsig.UseVisualStyleBackColor = true;
            this.Bsig.Click += new System.EventHandler(this.Bsig_Click);
            // 
            // Bant
            // 
            this.Bant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Bant.Location = new System.Drawing.Point(347, 12);
            this.Bant.Name = "Bant";
            this.Bant.Size = new System.Drawing.Size(37, 29);
            this.Bant.TabIndex = 2;
            this.Bant.Text = "<";
            this.Bant.UseVisualStyleBackColor = true;
            this.Bant.Click += new System.EventHandler(this.Bant_Click);
            // 
            // TxtFolio
            // 
            this.TxtFolio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtFolio.Location = new System.Drawing.Point(49, 16);
            this.TxtFolio.Name = "TxtFolio";
            this.TxtFolio.Size = new System.Drawing.Size(292, 20);
            this.TxtFolio.TabIndex = 0;
            this.TxtFolio.TextChanged += new System.EventHandler(this.TxtFolio_TextChanged_1);
            this.TxtFolio.LostFocus += new System.EventHandler(this.TxtPierdeElFoco);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Folio:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.cmdAplicaDescuento);
            this.groupBox2.Controls.Add(this.BestadoActualCaja);
            this.groupBox2.Controls.Add(this.TxtPedido);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(460, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(300, 55);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // cmdAplicaDescuento
            // 
            this.cmdAplicaDescuento.ForeColor = System.Drawing.Color.Black;
            this.cmdAplicaDescuento.Location = new System.Drawing.Point(151, 11);
            this.cmdAplicaDescuento.Name = "cmdAplicaDescuento";
            this.cmdAplicaDescuento.Size = new System.Drawing.Size(135, 33);
            this.cmdAplicaDescuento.TabIndex = 11;
            this.cmdAplicaDescuento.Text = "Aplica Descuento";
            this.cmdAplicaDescuento.UseVisualStyleBackColor = true;
            this.cmdAplicaDescuento.Click += new System.EventHandler(this.cmdAplicaDescuento_Click);
            // 
            // BestadoActualCaja
            // 
            this.BestadoActualCaja.ForeColor = System.Drawing.Color.Black;
            this.BestadoActualCaja.Location = new System.Drawing.Point(10, 11);
            this.BestadoActualCaja.Name = "BestadoActualCaja";
            this.BestadoActualCaja.Size = new System.Drawing.Size(135, 33);
            this.BestadoActualCaja.TabIndex = 10;
            this.BestadoActualCaja.Text = "Estado actual de caja";
            this.BestadoActualCaja.UseVisualStyleBackColor = true;
            this.BestadoActualCaja.Click += new System.EventHandler(this.BestadoActualCaja_Click);
            // 
            // TxtPedido
            // 
            this.TxtPedido.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPedido.Location = new System.Drawing.Point(59, 16);
            this.TxtPedido.Name = "TxtPedido";
            this.TxtPedido.Size = new System.Drawing.Size(223, 20);
            this.TxtPedido.TabIndex = 0;
            this.TxtPedido.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pedido:";
            this.label2.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.Lstatus);
            this.groupBox3.Location = new System.Drawing.Point(942, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(241, 53);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            // 
            // Lstatus
            // 
            this.Lstatus.BackColor = System.Drawing.Color.PaleGreen;
            this.Lstatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lstatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lstatus.Location = new System.Drawing.Point(10, 16);
            this.Lstatus.Name = "Lstatus";
            this.Lstatus.Size = new System.Drawing.Size(218, 20);
            this.Lstatus.TabIndex = 0;
            this.Lstatus.Text = "STATUS";
            this.Lstatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.cmdBuscaCliente);
            this.groupBox4.Controls.Add(this.BcambiarCliente);
            this.groupBox4.Controls.Add(this.Lemail);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.Ldireccion);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.LRFC);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.Lcliente);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(2, 57);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1181, 79);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // cmdBuscaCliente
            // 
            this.cmdBuscaCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdBuscaCliente.Location = new System.Drawing.Point(954, 12);
            this.cmdBuscaCliente.Name = "cmdBuscaCliente";
            this.cmdBuscaCliente.Size = new System.Drawing.Size(111, 56);
            this.cmdBuscaCliente.TabIndex = 0;
            this.cmdBuscaCliente.Text = "Buscar Cliente";
            this.cmdBuscaCliente.UseVisualStyleBackColor = true;
            this.cmdBuscaCliente.Click += new System.EventHandler(this.cmdBuscaCliente_Click);
            // 
            // BcambiarCliente
            // 
            this.BcambiarCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BcambiarCliente.Location = new System.Drawing.Point(1066, 12);
            this.BcambiarCliente.Name = "BcambiarCliente";
            this.BcambiarCliente.Size = new System.Drawing.Size(111, 56);
            this.BcambiarCliente.TabIndex = 1;
            this.BcambiarCliente.Text = "Cambiar Datos";
            this.BcambiarCliente.UseVisualStyleBackColor = true;
            this.BcambiarCliente.Click += new System.EventHandler(this.BcambiarCliente_Click);
            // 
            // Lemail
            // 
            this.Lemail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Lemail.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lemail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lemail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lemail.Location = new System.Drawing.Point(667, 14);
            this.Lemail.Name = "Lemail";
            this.Lemail.Size = new System.Drawing.Size(281, 22);
            this.Lemail.TabIndex = 12;
            this.Lemail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(624, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Correo:";
            // 
            // Ldireccion
            // 
            this.Ldireccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Ldireccion.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Ldireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Ldireccion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ldireccion.Location = new System.Drawing.Point(352, 40);
            this.Ldireccion.Name = "Ldireccion";
            this.Ldireccion.Size = new System.Drawing.Size(596, 28);
            this.Ldireccion.TabIndex = 10;
            this.Ldireccion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Ldireccion.Click += new System.EventHandler(this.Ldireccion_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(291, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Dirección:";
            // 
            // LRFC
            // 
            this.LRFC.BackColor = System.Drawing.Color.WhiteSmoke;
            this.LRFC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LRFC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LRFC.Location = new System.Drawing.Point(58, 46);
            this.LRFC.Name = "LRFC";
            this.LRFC.Size = new System.Drawing.Size(227, 22);
            this.LRFC.TabIndex = 8;
            this.LRFC.Text = "XXX010101XXX";
            this.LRFC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "RFC:";
            // 
            // Lcliente
            // 
            this.Lcliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lcliente.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Lcliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Lcliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Lcliente.Location = new System.Drawing.Point(58, 14);
            this.Lcliente.Name = "Lcliente";
            this.Lcliente.Size = new System.Drawing.Size(561, 22);
            this.Lcliente.TabIndex = 6;
            this.Lcliente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cliente:";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.cboVendedor);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.txtCapturaArticulos);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.DGVdetalle);
            this.groupBox5.Location = new System.Drawing.Point(5, 142);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(826, 480);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            // 
            // cboVendedor
            // 
            this.cboVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cboVendedor.DataSource = this.zctSegUsuBindingSource;
            this.cboVendedor.DisplayMember = "Nom_Usu";
            this.cboVendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVendedor.FormattingEnabled = true;
            this.cboVendedor.Location = new System.Drawing.Point(567, 19);
            this.cboVendedor.Name = "cboVendedor";
            this.cboVendedor.Size = new System.Drawing.Size(253, 21);
            this.cboVendedor.TabIndex = 2;
            this.cboVendedor.ValueMember = "Cod_Usu";
            // 
            // zctSegUsuBindingSource
            // 
            this.zctSegUsuBindingSource.DataSource = typeof(ResTotal.Modelo.ZctSegUsu);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(504, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Vendedor:";
            // 
            // txtCapturaArticulos
            // 
            this.txtCapturaArticulos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCapturaArticulos.Location = new System.Drawing.Point(55, 21);
            this.txtCapturaArticulos.Name = "txtCapturaArticulos";
            this.txtCapturaArticulos.Size = new System.Drawing.Size(443, 20);
            this.txtCapturaArticulos.TabIndex = 0;
            this.txtCapturaArticulos.TextChanged += new System.EventHandler(this.txtCapturaArticulos_TextChanged);
            this.txtCapturaArticulos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCapturaArticulos_KeyDown);
            this.txtCapturaArticulos.Leave += new System.EventHandler(this.txtCapturaArticulos_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Artículo:";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.DGVpagos);
            this.groupBox6.Location = new System.Drawing.Point(204, 294);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(518, 166);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Visible = false;
            // 
            // DGVpagos
            // 
            this.DGVpagos.AutoGenerateColumns = false;
            this.DGVpagos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVpagos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tipo_pago,
            this.referencia,
            this.montoDataGridViewTextBoxColumn,
            this.fechapagoDataGridViewTextBoxColumn,
            this.corteDataGridViewTextBoxColumn,
            this.tipopagotmp,
            this.ColMontoReal,
            this.cambioDataGridViewTextBoxColumn,
            this.cod_folio,
            this.CodEnc_Caja,
            this.nodoc_pago,
            this.cod_usu});
            this.DGVpagos.DataSource = this.PagosCajaBindingSource;
            this.DGVpagos.Location = new System.Drawing.Point(3, 16);
            this.DGVpagos.Name = "DGVpagos";
            this.DGVpagos.Size = new System.Drawing.Size(479, 145);
            this.DGVpagos.TabIndex = 1;
            // 
            // tipo_pago
            // 
            this.tipo_pago.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.tipo_pago.DataPropertyName = "codtipo_pago";
            this.tipo_pago.DataSource = this.TiposPagoBindingSource;
            this.tipo_pago.DisplayMember = "desc_pago";
            this.tipo_pago.HeaderText = "Tipo pago";
            this.tipo_pago.Name = "tipo_pago";
            this.tipo_pago.ValueMember = "codtipo_pago";
            this.tipo_pago.Width = 61;
            // 
            // TiposPagoBindingSource
            // 
            this.TiposPagoBindingSource.DataSource = typeof(ResTotal.Modelo.ZctCatTipoPago);
            // 
            // referencia
            // 
            this.referencia.DataPropertyName = "referencia";
            this.referencia.HeaderText = "referencia";
            this.referencia.Name = "referencia";
            // 
            // montoDataGridViewTextBoxColumn
            // 
            dataGridViewCellStyle17.Format = "N2";
            this.montoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle17;
            this.montoDataGridViewTextBoxColumn.HeaderText = "Monto";
            this.montoDataGridViewTextBoxColumn.Name = "montoDataGridViewTextBoxColumn";
            // 
            // fechapagoDataGridViewTextBoxColumn
            // 
            this.fechapagoDataGridViewTextBoxColumn.DataPropertyName = "fecha_pago";
            this.fechapagoDataGridViewTextBoxColumn.HeaderText = "fecha_pago";
            this.fechapagoDataGridViewTextBoxColumn.Name = "fechapagoDataGridViewTextBoxColumn";
            this.fechapagoDataGridViewTextBoxColumn.Visible = false;
            // 
            // corteDataGridViewTextBoxColumn
            // 
            this.corteDataGridViewTextBoxColumn.DataPropertyName = "corte";
            this.corteDataGridViewTextBoxColumn.HeaderText = "corte";
            this.corteDataGridViewTextBoxColumn.Name = "corteDataGridViewTextBoxColumn";
            this.corteDataGridViewTextBoxColumn.Visible = false;
            // 
            // tipopagotmp
            // 
            this.tipopagotmp.DataPropertyName = "codtipo_pago";
            this.tipopagotmp.HeaderText = "codtipo_pago";
            this.tipopagotmp.Name = "tipopagotmp";
            this.tipopagotmp.Visible = false;
            // 
            // ColMontoReal
            // 
            this.ColMontoReal.DataPropertyName = "monto";
            this.ColMontoReal.HeaderText = "montoReal";
            this.ColMontoReal.Name = "ColMontoReal";
            // 
            // cambioDataGridViewTextBoxColumn
            // 
            this.cambioDataGridViewTextBoxColumn.DataPropertyName = "cambio";
            this.cambioDataGridViewTextBoxColumn.HeaderText = "cambio";
            this.cambioDataGridViewTextBoxColumn.Name = "cambioDataGridViewTextBoxColumn";
            // 
            // cod_folio
            // 
            this.cod_folio.DataPropertyName = "cod_folio";
            this.cod_folio.HeaderText = "cod_folio";
            this.cod_folio.Name = "cod_folio";
            this.cod_folio.Visible = false;
            // 
            // CodEnc_Caja
            // 
            this.CodEnc_Caja.DataPropertyName = "CodEnc_Caja";
            this.CodEnc_Caja.HeaderText = "CodEnc_Caja";
            this.CodEnc_Caja.Name = "CodEnc_Caja";
            this.CodEnc_Caja.Visible = false;
            // 
            // nodoc_pago
            // 
            this.nodoc_pago.DataPropertyName = "nodoc_pago";
            this.nodoc_pago.HeaderText = "nodoc_pago";
            this.nodoc_pago.Name = "nodoc_pago";
            this.nodoc_pago.Visible = false;
            // 
            // cod_usu
            // 
            this.cod_usu.DataPropertyName = "cod_usu";
            this.cod_usu.HeaderText = "cod_usu";
            this.cod_usu.Name = "cod_usu";
            this.cod_usu.Visible = false;
            // 
            // PagosCajaBindingSource
            // 
            this.PagosCajaBindingSource.DataSource = typeof(ResTotal.Modelo.ZctPagoCaja);
            // 
            // DGVdetalle
            // 
            this.DGVdetalle.AllowUserToAddRows = false;
            this.DGVdetalle.AllowUserToDeleteRows = false;
            this.DGVdetalle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVdetalle.AutoGenerateColumns = false;
            this.DGVdetalle.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.DGVdetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVdetalle.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cod_Art,
            this.ArticuloCol,
            this.CostoArticuloOT,
            this.precio_descuento,
            this.Ctd_Art,
            this.subtotal,
            this.iva,
            this.Importe,
            this.Vendedor,
            this.codfolioDataGridViewTextBoxColumn1,
            this.codEncCajaDataGridViewTextBoxColumn1,
            this.nodoccajaDataGridViewTextBoxColumn1,
            this.codArtDataGridViewTextBoxColumn1,
            this.ctdArtDataGridViewTextBoxColumn1,
            this.subtotalDataGridViewTextBoxColumn,
            this.ivaDataGridViewTextBoxColumn,
            this.totalDataGridViewTextBoxColumn,
            this.costoArticuloOTDataGridViewTextBoxColumn1,
            this.codEncOTDataGridViewTextBoxColumn,
            this.codDetOTDataGridViewTextBoxColumn,
            this.codusuDataGridViewTextBoxColumn,
            this.zctDetOTDataGridViewTextBoxColumn,
            this.zctSegUsuDataGridViewTextBoxColumn,
            this.zctEncCajaDataGridViewTextBoxColumn1,
            this.zctCatArtDataGridViewTextBoxColumn1});
            this.DGVdetalle.DataSource = this.zctDetCajaBindingSource;
            this.DGVdetalle.Location = new System.Drawing.Point(6, 50);
            this.DGVdetalle.MultiSelect = false;
            this.DGVdetalle.Name = "DGVdetalle";
            this.DGVdetalle.ReadOnly = true;
            this.DGVdetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVdetalle.Size = new System.Drawing.Size(814, 424);
            this.DGVdetalle.TabIndex = 3;
            this.DGVdetalle.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVdetalle_CellContentClick);
            this.DGVdetalle.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVdetalle_CellEndEdit);
            this.DGVdetalle.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVdetalle_CellEnter);
            this.DGVdetalle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DGVdetalle_KeyDown);
            // 
            // Cod_Art
            // 
            this.Cod_Art.DataPropertyName = "Cod_Art";
            this.Cod_Art.HeaderText = "SKU";
            this.Cod_Art.Name = "Cod_Art";
            this.Cod_Art.ReadOnly = true;
            // 
            // ArticuloCol
            // 
            this.ArticuloCol.DataPropertyName = "ArticuloNombre";
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.LightBlue;
            this.ArticuloCol.DefaultCellStyle = dataGridViewCellStyle18;
            this.ArticuloCol.HeaderText = "Artículo";
            this.ArticuloCol.Name = "ArticuloCol";
            this.ArticuloCol.ReadOnly = true;
            this.ArticuloCol.Width = 180;
            // 
            // CostoArticuloOT
            // 
            this.CostoArticuloOT.DataPropertyName = "CostoArticuloOT";
            dataGridViewCellStyle19.Format = "C2";
            dataGridViewCellStyle19.NullValue = null;
            this.CostoArticuloOT.DefaultCellStyle = dataGridViewCellStyle19;
            this.CostoArticuloOT.HeaderText = "Precio";
            this.CostoArticuloOT.Name = "CostoArticuloOT";
            this.CostoArticuloOT.ReadOnly = true;
            this.CostoArticuloOT.Width = 80;
            // 
            // precio_descuento
            // 
            this.precio_descuento.DataPropertyName = "precio_descuento";
            dataGridViewCellStyle20.Format = "C2";
            dataGridViewCellStyle20.NullValue = null;
            this.precio_descuento.DefaultCellStyle = dataGridViewCellStyle20;
            this.precio_descuento.HeaderText = "Descuento";
            this.precio_descuento.Name = "precio_descuento";
            this.precio_descuento.ReadOnly = true;
            this.precio_descuento.Width = 80;
            // 
            // Ctd_Art
            // 
            this.Ctd_Art.DataPropertyName = "Ctd_Art";
            dataGridViewCellStyle21.Format = "N2";
            dataGridViewCellStyle21.NullValue = "0";
            this.Ctd_Art.DefaultCellStyle = dataGridViewCellStyle21;
            this.Ctd_Art.HeaderText = "Cantidad";
            this.Ctd_Art.Name = "Ctd_Art";
            this.Ctd_Art.ReadOnly = true;
            this.Ctd_Art.Width = 60;
            // 
            // subtotal
            // 
            this.subtotal.DataPropertyName = "subtotal";
            dataGridViewCellStyle22.Format = "C2";
            dataGridViewCellStyle22.NullValue = null;
            this.subtotal.DefaultCellStyle = dataGridViewCellStyle22;
            this.subtotal.HeaderText = "Subtotal";
            this.subtotal.Name = "subtotal";
            this.subtotal.ReadOnly = true;
            this.subtotal.Visible = false;
            // 
            // iva
            // 
            this.iva.DataPropertyName = "iva";
            dataGridViewCellStyle23.Format = "C2";
            dataGridViewCellStyle23.NullValue = null;
            this.iva.DefaultCellStyle = dataGridViewCellStyle23;
            this.iva.HeaderText = "IVA";
            this.iva.Name = "iva";
            this.iva.ReadOnly = true;
            this.iva.Visible = false;
            // 
            // Importe
            // 
            this.Importe.DataPropertyName = "ImporteArticulo";
            dataGridViewCellStyle24.Format = "C2";
            dataGridViewCellStyle24.NullValue = null;
            this.Importe.DefaultCellStyle = dataGridViewCellStyle24;
            this.Importe.HeaderText = "Total";
            this.Importe.Name = "Importe";
            this.Importe.ReadOnly = true;
            // 
            // Vendedor
            // 
            this.Vendedor.DataPropertyName = "vendedor";
            this.Vendedor.HeaderText = "Vendedor";
            this.Vendedor.Name = "Vendedor";
            this.Vendedor.ReadOnly = true;
            // 
            // codfolioDataGridViewTextBoxColumn1
            // 
            this.codfolioDataGridViewTextBoxColumn1.DataPropertyName = "cod_folio";
            this.codfolioDataGridViewTextBoxColumn1.HeaderText = "cod_folio";
            this.codfolioDataGridViewTextBoxColumn1.Name = "codfolioDataGridViewTextBoxColumn1";
            this.codfolioDataGridViewTextBoxColumn1.ReadOnly = true;
            this.codfolioDataGridViewTextBoxColumn1.Visible = false;
            // 
            // codEncCajaDataGridViewTextBoxColumn1
            // 
            this.codEncCajaDataGridViewTextBoxColumn1.DataPropertyName = "CodEnc_Caja";
            this.codEncCajaDataGridViewTextBoxColumn1.HeaderText = "CodEnc_Caja";
            this.codEncCajaDataGridViewTextBoxColumn1.Name = "codEncCajaDataGridViewTextBoxColumn1";
            this.codEncCajaDataGridViewTextBoxColumn1.ReadOnly = true;
            this.codEncCajaDataGridViewTextBoxColumn1.Visible = false;
            // 
            // nodoccajaDataGridViewTextBoxColumn1
            // 
            this.nodoccajaDataGridViewTextBoxColumn1.DataPropertyName = "nodoc_caja";
            this.nodoccajaDataGridViewTextBoxColumn1.HeaderText = "nodoc_caja";
            this.nodoccajaDataGridViewTextBoxColumn1.Name = "nodoccajaDataGridViewTextBoxColumn1";
            this.nodoccajaDataGridViewTextBoxColumn1.ReadOnly = true;
            this.nodoccajaDataGridViewTextBoxColumn1.Visible = false;
            // 
            // codArtDataGridViewTextBoxColumn1
            // 
            this.codArtDataGridViewTextBoxColumn1.DataPropertyName = "Cod_Art";
            this.codArtDataGridViewTextBoxColumn1.HeaderText = "Cod_Art";
            this.codArtDataGridViewTextBoxColumn1.Name = "codArtDataGridViewTextBoxColumn1";
            this.codArtDataGridViewTextBoxColumn1.ReadOnly = true;
            this.codArtDataGridViewTextBoxColumn1.Visible = false;
            // 
            // ctdArtDataGridViewTextBoxColumn1
            // 
            this.ctdArtDataGridViewTextBoxColumn1.DataPropertyName = "Ctd_Art";
            this.ctdArtDataGridViewTextBoxColumn1.HeaderText = "Ctd_Art";
            this.ctdArtDataGridViewTextBoxColumn1.Name = "ctdArtDataGridViewTextBoxColumn1";
            this.ctdArtDataGridViewTextBoxColumn1.ReadOnly = true;
            this.ctdArtDataGridViewTextBoxColumn1.Visible = false;
            // 
            // subtotalDataGridViewTextBoxColumn
            // 
            this.subtotalDataGridViewTextBoxColumn.DataPropertyName = "subtotal";
            this.subtotalDataGridViewTextBoxColumn.HeaderText = "subtotal";
            this.subtotalDataGridViewTextBoxColumn.Name = "subtotalDataGridViewTextBoxColumn";
            this.subtotalDataGridViewTextBoxColumn.ReadOnly = true;
            this.subtotalDataGridViewTextBoxColumn.Visible = false;
            // 
            // ivaDataGridViewTextBoxColumn
            // 
            this.ivaDataGridViewTextBoxColumn.DataPropertyName = "iva";
            this.ivaDataGridViewTextBoxColumn.HeaderText = "iva";
            this.ivaDataGridViewTextBoxColumn.Name = "ivaDataGridViewTextBoxColumn";
            this.ivaDataGridViewTextBoxColumn.ReadOnly = true;
            this.ivaDataGridViewTextBoxColumn.Visible = false;
            // 
            // totalDataGridViewTextBoxColumn
            // 
            this.totalDataGridViewTextBoxColumn.DataPropertyName = "total";
            this.totalDataGridViewTextBoxColumn.HeaderText = "total";
            this.totalDataGridViewTextBoxColumn.Name = "totalDataGridViewTextBoxColumn";
            this.totalDataGridViewTextBoxColumn.ReadOnly = true;
            this.totalDataGridViewTextBoxColumn.Visible = false;
            // 
            // costoArticuloOTDataGridViewTextBoxColumn1
            // 
            this.costoArticuloOTDataGridViewTextBoxColumn1.DataPropertyName = "CostoArticuloOT";
            this.costoArticuloOTDataGridViewTextBoxColumn1.HeaderText = "CostoArticuloOT";
            this.costoArticuloOTDataGridViewTextBoxColumn1.Name = "costoArticuloOTDataGridViewTextBoxColumn1";
            this.costoArticuloOTDataGridViewTextBoxColumn1.ReadOnly = true;
            this.costoArticuloOTDataGridViewTextBoxColumn1.Visible = false;
            // 
            // codEncOTDataGridViewTextBoxColumn
            // 
            this.codEncOTDataGridViewTextBoxColumn.DataPropertyName = "codEnc_OT";
            this.codEncOTDataGridViewTextBoxColumn.HeaderText = "codEnc_OT";
            this.codEncOTDataGridViewTextBoxColumn.Name = "codEncOTDataGridViewTextBoxColumn";
            this.codEncOTDataGridViewTextBoxColumn.ReadOnly = true;
            this.codEncOTDataGridViewTextBoxColumn.Visible = false;
            // 
            // codDetOTDataGridViewTextBoxColumn
            // 
            this.codDetOTDataGridViewTextBoxColumn.DataPropertyName = "Cod_DetOT";
            this.codDetOTDataGridViewTextBoxColumn.HeaderText = "Cod_DetOT";
            this.codDetOTDataGridViewTextBoxColumn.Name = "codDetOTDataGridViewTextBoxColumn";
            this.codDetOTDataGridViewTextBoxColumn.ReadOnly = true;
            this.codDetOTDataGridViewTextBoxColumn.Visible = false;
            // 
            // codusuDataGridViewTextBoxColumn
            // 
            this.codusuDataGridViewTextBoxColumn.DataPropertyName = "cod_usu";
            this.codusuDataGridViewTextBoxColumn.HeaderText = "cod_usu";
            this.codusuDataGridViewTextBoxColumn.Name = "codusuDataGridViewTextBoxColumn";
            this.codusuDataGridViewTextBoxColumn.ReadOnly = true;
            this.codusuDataGridViewTextBoxColumn.Visible = false;
            // 
            // zctDetOTDataGridViewTextBoxColumn
            // 
            this.zctDetOTDataGridViewTextBoxColumn.DataPropertyName = "ZctDetOT";
            this.zctDetOTDataGridViewTextBoxColumn.HeaderText = "ZctDetOT";
            this.zctDetOTDataGridViewTextBoxColumn.Name = "zctDetOTDataGridViewTextBoxColumn";
            this.zctDetOTDataGridViewTextBoxColumn.ReadOnly = true;
            this.zctDetOTDataGridViewTextBoxColumn.Visible = false;
            // 
            // zctSegUsuDataGridViewTextBoxColumn
            // 
            this.zctSegUsuDataGridViewTextBoxColumn.DataPropertyName = "ZctSegUsu";
            this.zctSegUsuDataGridViewTextBoxColumn.HeaderText = "ZctSegUsu";
            this.zctSegUsuDataGridViewTextBoxColumn.Name = "zctSegUsuDataGridViewTextBoxColumn";
            this.zctSegUsuDataGridViewTextBoxColumn.ReadOnly = true;
            this.zctSegUsuDataGridViewTextBoxColumn.Visible = false;
            // 
            // zctEncCajaDataGridViewTextBoxColumn1
            // 
            this.zctEncCajaDataGridViewTextBoxColumn1.DataPropertyName = "ZctEncCaja";
            this.zctEncCajaDataGridViewTextBoxColumn1.HeaderText = "ZctEncCaja";
            this.zctEncCajaDataGridViewTextBoxColumn1.Name = "zctEncCajaDataGridViewTextBoxColumn1";
            this.zctEncCajaDataGridViewTextBoxColumn1.ReadOnly = true;
            this.zctEncCajaDataGridViewTextBoxColumn1.Visible = false;
            // 
            // zctCatArtDataGridViewTextBoxColumn1
            // 
            this.zctCatArtDataGridViewTextBoxColumn1.DataPropertyName = "ZctCatArt";
            this.zctCatArtDataGridViewTextBoxColumn1.HeaderText = "ZctCatArt";
            this.zctCatArtDataGridViewTextBoxColumn1.Name = "zctCatArtDataGridViewTextBoxColumn1";
            this.zctCatArtDataGridViewTextBoxColumn1.ReadOnly = true;
            this.zctCatArtDataGridViewTextBoxColumn1.Visible = false;
            // 
            // zctDetCajaBindingSource
            // 
            this.zctDetCajaBindingSource.AllowNew = true;
            this.zctDetCajaBindingSource.DataSource = typeof(ResTotal.Modelo.ZctDetCaja);
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.Lrestante);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.Lcambio);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.Lpago);
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.Ltotal);
            this.groupBox7.Controls.Add(this.Liva);
            this.groupBox7.Controls.Add(this.Lsubtotal);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Location = new System.Drawing.Point(837, 142);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(346, 226);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            // 
            // Lrestante
            // 
            this.Lrestante.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lrestante.ForeColor = System.Drawing.Color.Navy;
            this.Lrestante.Location = new System.Drawing.Point(224, 158);
            this.Lrestante.Name = "Lrestante";
            this.Lrestante.Size = new System.Drawing.Size(118, 25);
            this.Lrestante.TabIndex = 12;
            this.Lrestante.Text = "$0.00";
            this.Lrestante.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(21, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 27);
            this.label5.TabIndex = 11;
            this.label5.Text = "Restante por pagar:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Lcambio
            // 
            this.Lcambio.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lcambio.ForeColor = System.Drawing.Color.Green;
            this.Lcambio.Location = new System.Drawing.Point(158, 194);
            this.Lcambio.Name = "Lcambio";
            this.Lcambio.Size = new System.Drawing.Size(184, 25);
            this.Lcambio.TabIndex = 10;
            this.Lcambio.Text = "$0.00";
            this.Lcambio.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Green;
            this.label21.Location = new System.Drawing.Point(21, 194);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 27);
            this.label21.TabIndex = 9;
            this.label21.Text = "Cambio:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // Lpago
            // 
            this.Lpago.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lpago.ForeColor = System.Drawing.Color.Green;
            this.Lpago.Location = new System.Drawing.Point(158, 121);
            this.Lpago.Name = "Lpago";
            this.Lpago.Size = new System.Drawing.Size(184, 25);
            this.Lpago.TabIndex = 8;
            this.Lpago.Text = "$0.00";
            this.Lpago.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Green;
            this.label19.Location = new System.Drawing.Point(21, 119);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(96, 27);
            this.label19.TabIndex = 7;
            this.label19.Text = "Pago:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Ltotal
            // 
            this.Ltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ltotal.ForeColor = System.Drawing.Color.Navy;
            this.Ltotal.Location = new System.Drawing.Point(158, 84);
            this.Ltotal.Name = "Ltotal";
            this.Ltotal.Size = new System.Drawing.Size(184, 25);
            this.Ltotal.TabIndex = 5;
            this.Ltotal.Text = "$0.00";
            this.Ltotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Liva
            // 
            this.Liva.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Liva.ForeColor = System.Drawing.Color.Navy;
            this.Liva.Location = new System.Drawing.Point(158, 50);
            this.Liva.Name = "Liva";
            this.Liva.Size = new System.Drawing.Size(184, 23);
            this.Liva.TabIndex = 4;
            this.Liva.Text = "$0.00";
            this.Liva.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Lsubtotal
            // 
            this.Lsubtotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lsubtotal.ForeColor = System.Drawing.Color.Navy;
            this.Lsubtotal.Location = new System.Drawing.Point(158, 13);
            this.Lsubtotal.Name = "Lsubtotal";
            this.Lsubtotal.Size = new System.Drawing.Size(184, 24);
            this.Lsubtotal.TabIndex = 3;
            this.Lsubtotal.Text = "$0.00";
            this.Lsubtotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Navy;
            this.label14.Location = new System.Drawing.Point(21, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 24);
            this.label14.TabIndex = 2;
            this.label14.Text = "IVA:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(21, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 24);
            this.label13.TabIndex = 1;
            this.label13.Text = "Sub Total:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Navy;
            this.label12.Location = new System.Drawing.Point(21, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 27);
            this.label12.TabIndex = 0;
            this.label12.Text = "Total:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.groupBox8.Controls.Add(this.BeditarCostoArticulo);
            this.groupBox8.Controls.Add(this.cmdPagoParcial);
            this.groupBox8.Controls.Add(this.cmdPagoTarjeta);
            this.groupBox8.Controls.Add(this.BabreCaja);
            this.groupBox8.Controls.Add(this.cmdPagoEfectivo);
            this.groupBox8.Controls.Add(this.BEditarCantidadArticulo);
            this.groupBox8.Controls.Add(this.Bimprimir);
            this.groupBox8.Controls.Add(this.Bcancelar);
            this.groupBox8.Controls.Add(this.Blimpiar);
            this.groupBox8.Controls.Add(this.BcorteCaja);
            this.groupBox8.Controls.Add(this.Bretiro);
            this.groupBox8.Controls.Add(this.Bgrabar);
            this.groupBox8.Location = new System.Drawing.Point(837, 374);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(342, 252);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            // 
            // BeditarCostoArticulo
            // 
            this.BeditarCostoArticulo.Location = new System.Drawing.Point(123, 134);
            this.BeditarCostoArticulo.Name = "BeditarCostoArticulo";
            this.BeditarCostoArticulo.Size = new System.Drawing.Size(103, 54);
            this.BeditarCostoArticulo.TabIndex = 12;
            this.BeditarCostoArticulo.Text = "Editar Costo Articulo";
            this.BeditarCostoArticulo.UseVisualStyleBackColor = true;
            this.BeditarCostoArticulo.Click += new System.EventHandler(this.BeditarCostoArticulo_Click);
            // 
            // cmdPagoParcial
            // 
            this.cmdPagoParcial.Location = new System.Drawing.Point(232, 72);
            this.cmdPagoParcial.Name = "cmdPagoParcial";
            this.cmdPagoParcial.Size = new System.Drawing.Size(103, 54);
            this.cmdPagoParcial.TabIndex = 5;
            this.cmdPagoParcial.Text = "Pago Parcial";
            this.cmdPagoParcial.UseVisualStyleBackColor = true;
            this.cmdPagoParcial.Click += new System.EventHandler(this.cmdPagoParcial_Click);
            // 
            // cmdPagoTarjeta
            // 
            this.cmdPagoTarjeta.Location = new System.Drawing.Point(14, 72);
            this.cmdPagoTarjeta.Name = "cmdPagoTarjeta";
            this.cmdPagoTarjeta.Size = new System.Drawing.Size(103, 54);
            this.cmdPagoTarjeta.TabIndex = 3;
            this.cmdPagoTarjeta.Text = "Pago Tarjeta";
            this.cmdPagoTarjeta.UseVisualStyleBackColor = true;
            this.cmdPagoTarjeta.Click += new System.EventHandler(this.cmdPagoTarjeta_Click);
            // 
            // BabreCaja
            // 
            this.BabreCaja.ForeColor = System.Drawing.Color.Black;
            this.BabreCaja.Location = new System.Drawing.Point(13, 194);
            this.BabreCaja.Name = "BabreCaja";
            this.BabreCaja.Size = new System.Drawing.Size(103, 54);
            this.BabreCaja.TabIndex = 9;
            this.BabreCaja.Text = "Abre Caja";
            this.BabreCaja.UseVisualStyleBackColor = true;
            this.BabreCaja.Click += new System.EventHandler(this.BabreCaja_Click);
            // 
            // cmdPagoEfectivo
            // 
            this.cmdPagoEfectivo.Location = new System.Drawing.Point(123, 72);
            this.cmdPagoEfectivo.Name = "cmdPagoEfectivo";
            this.cmdPagoEfectivo.Size = new System.Drawing.Size(103, 54);
            this.cmdPagoEfectivo.TabIndex = 4;
            this.cmdPagoEfectivo.Text = "Pago Efectivo";
            this.cmdPagoEfectivo.UseVisualStyleBackColor = true;
            this.cmdPagoEfectivo.Click += new System.EventHandler(this.cmdPagoEfectivo_Click);
            // 
            // BEditarCantidadArticulo
            // 
            this.BEditarCantidadArticulo.Location = new System.Drawing.Point(13, 134);
            this.BEditarCantidadArticulo.Name = "BEditarCantidadArticulo";
            this.BEditarCantidadArticulo.Size = new System.Drawing.Size(103, 54);
            this.BEditarCantidadArticulo.TabIndex = 6;
            this.BEditarCantidadArticulo.Text = "Editar Cantidad Artículo";
            this.BEditarCantidadArticulo.UseVisualStyleBackColor = true;
            this.BEditarCantidadArticulo.Click += new System.EventHandler(this.button2_Click);
            // 
            // Bimprimir
            // 
            this.Bimprimir.Enabled = false;
            this.Bimprimir.Location = new System.Drawing.Point(123, 194);
            this.Bimprimir.Name = "Bimprimir";
            this.Bimprimir.Size = new System.Drawing.Size(103, 54);
            this.Bimprimir.TabIndex = 7;
            this.Bimprimir.Text = "Re Imprimir";
            this.Bimprimir.UseVisualStyleBackColor = true;
            this.Bimprimir.Click += new System.EventHandler(this.bimprimir_Click);
            // 
            // Bcancelar
            // 
            this.Bcancelar.Enabled = false;
            this.Bcancelar.Location = new System.Drawing.Point(232, 194);
            this.Bcancelar.Name = "Bcancelar";
            this.Bcancelar.Size = new System.Drawing.Size(103, 54);
            this.Bcancelar.TabIndex = 11;
            this.Bcancelar.Text = "Devolución";
            this.Bcancelar.UseVisualStyleBackColor = true;
            this.Bcancelar.Click += new System.EventHandler(this.Bcancelar_Click);
            // 
            // Blimpiar
            // 
            this.Blimpiar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Blimpiar.Location = new System.Drawing.Point(232, 134);
            this.Blimpiar.Name = "Blimpiar";
            this.Blimpiar.Size = new System.Drawing.Size(103, 54);
            this.Blimpiar.TabIndex = 8;
            this.Blimpiar.Text = "Cancelar";
            this.Blimpiar.UseVisualStyleBackColor = true;
            this.Blimpiar.Click += new System.EventHandler(this.Blimpiar_Click);
            // 
            // BcorteCaja
            // 
            this.BcorteCaja.Enabled = false;
            this.BcorteCaja.Location = new System.Drawing.Point(232, 14);
            this.BcorteCaja.Name = "BcorteCaja";
            this.BcorteCaja.Size = new System.Drawing.Size(103, 54);
            this.BcorteCaja.TabIndex = 2;
            this.BcorteCaja.Text = "Corte Caja";
            this.BcorteCaja.UseVisualStyleBackColor = true;
            this.BcorteCaja.Click += new System.EventHandler(this.BcorteCaja_Click_1);
            // 
            // Bretiro
            // 
            this.Bretiro.Enabled = false;
            this.Bretiro.ForeColor = System.Drawing.Color.Black;
            this.Bretiro.Location = new System.Drawing.Point(123, 14);
            this.Bretiro.Name = "Bretiro";
            this.Bretiro.Size = new System.Drawing.Size(103, 54);
            this.Bretiro.TabIndex = 1;
            this.Bretiro.Text = "Retiro Caja";
            this.Bretiro.UseVisualStyleBackColor = true;
            this.Bretiro.Click += new System.EventHandler(this.Bretiro_Click_1);
            // 
            // Bgrabar
            // 
            this.Bgrabar.Location = new System.Drawing.Point(14, 14);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 54);
            this.Bgrabar.TabIndex = 0;
            this.Bgrabar.Text = "Total";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // BCambiarPedido
            // 
            this.BCambiarPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BCambiarPedido.Location = new System.Drawing.Point(732, 8);
            this.BCambiarPedido.Name = "BCambiarPedido";
            this.BCambiarPedido.Size = new System.Drawing.Size(103, 43);
            this.BCambiarPedido.TabIndex = 5;
            this.BCambiarPedido.Text = "Cambiar pedido";
            this.BCambiarPedido.UseVisualStyleBackColor = true;
            this.BCambiarPedido.Visible = false;
            this.BCambiarPedido.Click += new System.EventHandler(this.BcambiarClienteBoton_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkRate = 450;
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ArticuloNombre";
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.LightBlue;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle25;
            this.dataGridViewTextBoxColumn1.HeaderText = "Artículo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "CostoArticuloOT";
            dataGridViewCellStyle26.Format = "N2";
            dataGridViewCellStyle26.NullValue = "0";
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle26;
            this.dataGridViewTextBoxColumn2.HeaderText = "Precio";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Ctd_Art";
            dataGridViewCellStyle27.Format = "N2";
            dataGridViewCellStyle27.NullValue = "0";
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn3.HeaderText = "Cantidad";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "ImporteArticulo";
            dataGridViewCellStyle28.Format = "N2";
            dataGridViewCellStyle28.NullValue = "0";
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle28;
            this.dataGridViewTextBoxColumn4.HeaderText = "Importe";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "iva";
            dataGridViewCellStyle29.Format = "N2";
            dataGridViewCellStyle29.NullValue = "0";
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridViewTextBoxColumn5.HeaderText = "iva";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "subtotal";
            dataGridViewCellStyle30.Format = "N2";
            dataGridViewCellStyle30.NullValue = "0";
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle30;
            this.dataGridViewTextBoxColumn6.HeaderText = "subtotal";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "total";
            dataGridViewCellStyle31.Format = "N2";
            dataGridViewCellStyle31.NullValue = "0";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle31;
            this.dataGridViewTextBoxColumn7.HeaderText = "total";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "referencia";
            this.dataGridViewTextBoxColumn8.HeaderText = "referencia";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            dataGridViewCellStyle32.Format = "N2";
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle32;
            this.dataGridViewTextBoxColumn9.HeaderText = "Monto";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "fecha_pago";
            this.dataGridViewTextBoxColumn10.HeaderText = "fecha_pago";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Visible = false;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "corte";
            this.dataGridViewTextBoxColumn11.HeaderText = "corte";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Visible = false;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "codtipo_pago";
            this.dataGridViewTextBoxColumn12.HeaderText = "codtipo_pago";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Visible = false;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "monto";
            this.dataGridViewTextBoxColumn13.HeaderText = "montoReal";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Visible = false;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "cambio";
            this.dataGridViewTextBoxColumn14.HeaderText = "cambio";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "cod_folio";
            this.dataGridViewTextBoxColumn15.HeaderText = "cod_folio";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "CodEnc_Caja";
            this.dataGridViewTextBoxColumn16.HeaderText = "CodEnc_Caja";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "nodoc_pago";
            this.dataGridViewTextBoxColumn17.HeaderText = "nodoc_pago";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // codfolioDataGridViewTextBoxColumn
            // 
            this.codfolioDataGridViewTextBoxColumn.DataPropertyName = "cod_folio";
            this.codfolioDataGridViewTextBoxColumn.HeaderText = "cod_folio";
            this.codfolioDataGridViewTextBoxColumn.Name = "codfolioDataGridViewTextBoxColumn";
            // 
            // codEncCajaDataGridViewTextBoxColumn
            // 
            this.codEncCajaDataGridViewTextBoxColumn.DataPropertyName = "CodEnc_Caja";
            this.codEncCajaDataGridViewTextBoxColumn.HeaderText = "CodEnc_Caja";
            this.codEncCajaDataGridViewTextBoxColumn.Name = "codEncCajaDataGridViewTextBoxColumn";
            // 
            // nodoccajaDataGridViewTextBoxColumn
            // 
            this.nodoccajaDataGridViewTextBoxColumn.DataPropertyName = "nodoc_caja";
            this.nodoccajaDataGridViewTextBoxColumn.HeaderText = "nodoc_caja";
            this.nodoccajaDataGridViewTextBoxColumn.Name = "nodoccajaDataGridViewTextBoxColumn";
            // 
            // codArtDataGridViewTextBoxColumn
            // 
            this.codArtDataGridViewTextBoxColumn.DataPropertyName = "Cod_Art";
            this.codArtDataGridViewTextBoxColumn.HeaderText = "Cod_Art";
            this.codArtDataGridViewTextBoxColumn.Name = "codArtDataGridViewTextBoxColumn";
            // 
            // ctdArtDataGridViewTextBoxColumn
            // 
            this.ctdArtDataGridViewTextBoxColumn.DataPropertyName = "Ctd_Art";
            this.ctdArtDataGridViewTextBoxColumn.HeaderText = "Ctd_Art";
            this.ctdArtDataGridViewTextBoxColumn.Name = "ctdArtDataGridViewTextBoxColumn";
            // 
            // costoArticuloOTDataGridViewTextBoxColumn
            // 
            this.costoArticuloOTDataGridViewTextBoxColumn.DataPropertyName = "CostoArticuloOT";
            this.costoArticuloOTDataGridViewTextBoxColumn.HeaderText = "CostoArticuloOT";
            this.costoArticuloOTDataGridViewTextBoxColumn.Name = "costoArticuloOTDataGridViewTextBoxColumn";
            // 
            // zctEncCajaDataGridViewTextBoxColumn
            // 
            this.zctEncCajaDataGridViewTextBoxColumn.DataPropertyName = "ZctEncCaja";
            this.zctEncCajaDataGridViewTextBoxColumn.HeaderText = "ZctEncCaja";
            this.zctEncCajaDataGridViewTextBoxColumn.Name = "zctEncCajaDataGridViewTextBoxColumn";
            // 
            // zctCatArtDataGridViewTextBoxColumn
            // 
            this.zctCatArtDataGridViewTextBoxColumn.DataPropertyName = "ZctCatArt";
            this.zctCatArtDataGridViewTextBoxColumn.HeaderText = "ZctCatArt";
            this.zctCatArtDataGridViewTextBoxColumn.Name = "zctCatArtDataGridViewTextBoxColumn";
            // 
            // cmdCrearPedido
            // 
            this.cmdCrearPedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCrearPedido.Location = new System.Drawing.Point(837, 8);
            this.cmdCrearPedido.Name = "cmdCrearPedido";
            this.cmdCrearPedido.Size = new System.Drawing.Size(103, 43);
            this.cmdCrearPedido.TabIndex = 6;
            this.cmdCrearPedido.Text = "Crear pedido";
            this.cmdCrearPedido.UseVisualStyleBackColor = true;
            this.cmdCrearPedido.Visible = false;
            this.cmdCrearPedido.Click += new System.EventHandler(this.cmdCrearPedido_Click);
            // 
            // EncabezadoCajaBindingSource
            // 
            this.EncabezadoCajaBindingSource.DataSource = typeof(ResTotal.Modelo.ZctEncCaja);
            // 
            // frmCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.Blimpiar;
            this.ClientSize = new System.Drawing.Size(1191, 634);
            this.Controls.Add(this.cmdCrearPedido);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BCambiarPedido);
            this.Name = "frmCaja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAJA";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCaja_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zctSegUsuBindingSource)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGVpagos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TiposPagoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagosCajaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zctDetCajaBindingSource)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EncabezadoCajaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Bsig;
        private System.Windows.Forms.Button Bant;
        private System.Windows.Forms.TextBox TxtFolio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtPedido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label Lstatus;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Ldireccion;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label LRFC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Lcliente;
        private System.Windows.Forms.Label Lemail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button BcambiarCliente;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView DGVdetalle;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label Lsubtotal;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label Ltotal;
        private System.Windows.Forms.Label Liva;
        private System.Windows.Forms.Label Lpago;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label Lcambio;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button Bimprimir;
        private System.Windows.Forms.Button BcorteCaja;
        private System.Windows.Forms.Button Bretiro;
        private System.Windows.Forms.Button Blimpiar;
        private System.Windows.Forms.Button Bcancelar;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.Button BCambiarPedido;
        private System.Windows.Forms.BindingSource zctDetCajaBindingSource;
        private System.Windows.Forms.BindingSource TiposPagoBindingSource;
        private System.Windows.Forms.BindingSource PagosCajaBindingSource;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.Button BabreCaja;
        private System.Windows.Forms.BindingSource EncabezadoCajaBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn codfolioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codEncCajaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nodoccajaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codArtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctdArtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoArticuloOTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zctEncCajaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zctCatArtDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label Lrestante;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView DGVpagos;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.Button BestadoActualCaja;
        private System.Windows.Forms.Button cmdCrearPedido;
        private System.Windows.Forms.Label LfechaFolio;
        private System.Windows.Forms.Button cmdBuscaCliente;
        private System.Windows.Forms.ComboBox cboVendedor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCapturaArticulos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button cmdPagoParcial;
        private System.Windows.Forms.Button cmdPagoTarjeta;
        private System.Windows.Forms.Button cmdPagoEfectivo;
        private System.Windows.Forms.Button BEditarCantidadArticulo;
        private System.Windows.Forms.BindingSource zctSegUsuBindingSource;
        private System.Windows.Forms.DataGridViewComboBoxColumn tipo_pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn montoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechapagoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn corteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipopagotmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColMontoReal;
        private System.Windows.Forms.DataGridViewTextBoxColumn cambioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodEnc_Caja;
        private System.Windows.Forms.DataGridViewTextBoxColumn nodoc_pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_usu;
        private System.Windows.Forms.Button BeditarCostoArticulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cod_Art;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArticuloCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn CostoArticuloOT;
        private System.Windows.Forms.DataGridViewTextBoxColumn precio_descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ctd_Art;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn iva;
        private System.Windows.Forms.DataGridViewTextBoxColumn Importe;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vendedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn codfolioDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codEncCajaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nodoccajaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codArtDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ctdArtDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ivaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoArticuloOTDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codEncOTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codDetOTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codusuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zctDetOTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zctSegUsuDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zctEncCajaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn zctCatArtDataGridViewTextBoxColumn1;
        private System.Windows.Forms.Button cmdAplicaDescuento;
    }
}