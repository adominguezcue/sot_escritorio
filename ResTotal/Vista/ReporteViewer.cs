﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ResTotal.Vista
{
    public partial class ReporteViewer : Form
    {
        //ResTotal.ReportesRT.CuentasPorPagar.CxP_PorEstado _documentoCR = new ReportesRT.CuentasPorPagar.CxP_PorEstado();

        CrystalDecisions.Windows.Forms.CrystalReportViewer _documentoCR = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
        public ReporteViewer(CrystalDecisions.Windows.Forms.CrystalReportViewer documento)
        {
            InitializeComponent();
            _documentoCR = documento;
            this.Load += new System.EventHandler(this.CrystalReportViewer1_Load);
        }

        private void ReporteViewer_Load(object sender, EventArgs e)
        {


        }

        private void CrystalReportViewer1_Load(object sender, EventArgs e)
        {
            crystalReportViewer1.ReportSource = _documentoCR.ReportSource;
            crystalReportViewer1.Refresh();
        }
    }
}
