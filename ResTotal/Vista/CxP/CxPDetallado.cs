﻿using ResTotal.Vista.CxP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal.Excepciones;

namespace ResTotal.Vista
{
    public partial class CxPDetallado : Form
    {
        private bool actualizarTipoPago;
        internal IGestorCuentasXPagar CxPprincipal;
        public delegate void DcambiaProveedor(int Cod_prov);
        public event DcambiaProveedor cambiaproveedor;
        public decimal totalPagar;
        ResTotal.Controlador.Controladorcaja _ControladorCaja;
        global::Modelo.Almacen.Entidades.ZctEncCuentasPagar CuentaxPagar;
        Permisos.Controlador.Controlador _ControladorPermisos;
        //Permisos.Controlador.Controlador.vistapermisos _Permisos;
        string _Cnn;
        int _Cod_usu;
        int _cta;
        int _oc;
        decimal _total;
        string _cod_folio;
        bool _EsPorEstado = false;
        public CxPDetallado(string Cnn, int Cod_usu, string cod_folio, int Cta, int oc, decimal TotalCXP, bool actualizarTipoPago, bool esPorEstado)
        {
            InitializeComponent();
            DGVpagos.DataError += new DataGridViewDataErrorEventHandler(DGVpagos_DataError);
            TxtFolio.LostFocus += new EventHandler(Txt_LostFocus);
            TxtPedido.LostFocus += new EventHandler(Txt_LostFocus);
            DGVdetalle.RowsAdded += new DataGridViewRowsAddedEventHandler(DGVdetalle_RowsAdded);
            //DGVpagos.CellEndEdit += new DataGridViewCellEventHandler(DGVpagos_CellEndEdit);
            DGVpagos.RowsAdded += new DataGridViewRowsAddedEventHandler(DGVpagos_RowsAdded);
            _Cnn = Cnn;
            _Cod_usu = Cod_usu;
            _cod_folio = cod_folio;
            _cta = Cta;
            _oc = oc;
            _EsPorEstado = esPorEstado;
            _total = TotalCXP;
#warning actualizarTipoPago se recibe como parámetro porque la idea era que solamente desde la ventana de revisión se activara el botón, pero de momento se dejará en true
            this.actualizarTipoPago = true;//actualizarTipoPago;
        }

        void DGVpagos_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (DGVpagos.Rows[e.RowIndex].Cells["tipo_pago"].Value == null)
            {
                DGVpagos.Rows[e.RowIndex].Cells["tipo_pago"].Value = 1;
            }
        }

        void DGVpagos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == estadocol.DisplayIndex)
            {
                DGVpagos.Rows[e.RowIndex].Cells[estadoTmp.DisplayIndex].Value = DGVpagos.Rows[e.RowIndex].Cells[estadocol.DisplayIndex].Value.ToString().Trim();
                if (DGVpagos.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Trim() == "PAGADO")
                {
                    DGVpagos.Rows[e.RowIndex].Cells["estadocol"].ReadOnly = true;
                    DGVpagos.Rows[e.RowIndex].Cells[fecha_pago.DisplayIndex].Value = DateTime.Now;
                }
                else
                {
                    DGVpagos.Rows[e.RowIndex].Cells["estadocol"].ReadOnly = false;
                    DGVpagos.Rows[e.RowIndex].Cells[fecha_pago.DisplayIndex].Value = null;
                }
                ActualizarLabels();
                if (DGVpagos.Rows[e.RowIndex].Cells["Tipo_pago"].Value == null)
                {
                    DGVpagos.Rows[e.RowIndex].Cells["Tipo_pago"].Value = 1;
                    DGVpagos.Rows[e.RowIndex].Cells["Tipopagotmp"].Value = 1;
                }
            }
            if (e.ColumnIndex == Tipo_pago.DisplayIndex)
            {
                DGVpagos.Rows[e.RowIndex].Cells["Tipopagotmp"].Value = DGVpagos.Rows[e.RowIndex].Cells["Tipo_pago"].Value;
            }
            //if (DGVpagos.Rows[e.RowIndex].Cells["cod_banco"].Value == null)
            //{
            //    DGVpagos.Rows[e.RowIndex].Cells["cod_banco"].Value = 0;
            //}
        }

        void DGVdetalle_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //DGVdetalle.Rows[e.RowIndex ].Cells[ArticuloCol.DisplayIndex].Value = CuentaxPagar.ZctEncOC.ZctDetOC[e.RowIndex].ZctCatArt.Desc_Art;
            //
            //DGVdetalle.Rows[e.RowIndex].Cells["colimporte"].Value = (Convert.ToDecimal(DGVdetalle.Rows[e.RowIndex].Cells["cosArtDataGridViewTextBoxColumn"].Value) * Convert.ToDecimal(DGVdetalle.Rows[e.RowIndex].Cells["ctdArtDataGridViewTextBoxColumn"].Value));
        }

        void DGVpagos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        void Txt_LostFocus(object sender, EventArgs e)
        {
            Control MiControl = (Control)sender;
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            try
            {
                if (MiControl.Text == "0" || string.IsNullOrEmpty(MiControl.Text) || string.IsNullOrWhiteSpace(MiControl.Text))
                {
                    return;
                }
                if (MiControl.Name == TxtFolio.Name)
                {
                    CuentaxPagar = _ControladorCxP.ValidarYObtenerCargada(Convert.ToInt32(TxtFolio.Text));
                }
                else if (MiControl.Name == TxtPedido.Name)
                {
                    CuentaxPagar = _ControladorCxP.ValidaOcCxP(Convert.ToInt32(TxtPedido.Text));
                }
                if (CuentaxPagar.Estado == global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.CANCELADO)
                {

                }
                //CargarCuenta();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        public void CargarCuenta()
        {
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            TxtFolio.Text = _cta.ToString();
            TxtPedido.Text = _oc.ToString();
            CuentaxPagar = _ControladorCxP.ValidarYObtenerCargada(_cta);
            BsCxP.DataSource = CuentaxPagar;
            BsPagosCxP.DataSource = CuentaxPagar.ZctDetCuentasPagar;
            BsPagosCxP.DataSource = CuentaxPagar.ZctDetCuentasPagar;
            BsDetalleOC.DataSource = CuentaxPagar.ZctEncOC.ZctDetOC;
            DGVdetalle.DataSource = BsDetalleOC;
            ArticuloCol.DataPropertyName = "ArticuloNombre";
            ImporteCol.DataPropertyName = "Importe";
            Lcliente.Text = CuentaxPagar.ZctEncOC.ZctCatProv.Nom_Prov;
            LRFC.Text = CuentaxPagar.ZctEncOC.ZctCatProv.RFC_Prov;
            Lemail.Text = CuentaxPagar.ZctEncOC.ZctCatProv.email;
            Ldireccion.Text = CuentaxPagar.ZctEncOC.ZctCatProv.Dir_Prov;
            totalPagar = _total;
            BsEstadoPagos.DataSource = _ControladorCxP.TraeEstadosPago();
            //CargarEstadoPagos();
            CargarLabels();

        }

        public void CargarLabels()
        {
            try
            {
                decimal pagado = DGVpagos.Rows.Cast<DataGridViewRow>().Where(w => w.Cells["estadoTmp"].Value.ToString().Trim() == "PAGADO").Sum(x => Convert.ToDecimal(x.Cells["montoDataGridViewTextBoxColumn"].Value));
                Lpago.Text = string.Format("{0:C}", pagado);
                Lrestante.Text = string.Format("{0:C}", (totalPagar - pagado));
                if (CuentaxPagar.Estado == global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.PAGADO ||
                    CuentaxPagar.Estado == global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.CANCELADO)
                {
                    //CuentaxPagar.Estado = global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.PAGADO;
                    Bgrabar.Visible = Bgrabar.Enabled = false;
                    btnCambiarFormasPago.Visible = btnCambiarFormasPago.Enabled = true;
                    btnCambiarProveedor.Enabled = false;

                    foreach (DataGridViewColumn c in DGVpagos.Columns)
                    {
                        if (c == Tipo_pago && actualizarTipoPago)
                            continue;

                        c.ReadOnly = true;
                    }
                }
                else
                {
                    //CuentaxPagar.Estado = global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.PENDIENTE;
                    Bgrabar.Visible = Bgrabar.Enabled = true;
                    btnCambiarFormasPago.Visible = btnCambiarFormasPago.Enabled = false;
                    btnCambiarProveedor.Enabled = true;
                }

                if (!actualizarTipoPago)
                {
                    btnCambiarProveedor.Visible = false;
                    Bgrabar.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención, error al cargar los controles para el pago");
                Close();
            }
        }

        public void ActualizarLabels()
        {
            try
            {
                decimal pagado = DGVpagos.Rows.Cast<DataGridViewRow>().Where(w => w.Cells["estadoTmp"].Value.ToString().Trim() == "PAGADO").Sum(x => Convert.ToDecimal(x.Cells["montoDataGridViewTextBoxColumn"].Value));
                Lpago.Text = string.Format("{0:C}", pagado);
                Lrestante.Text = string.Format("{0:C}", (totalPagar - pagado));
                if (totalPagar == pagado)
                {
                    CuentaxPagar.Estado = global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.PAGADO;

                }
                else
                {
                    CuentaxPagar.Estado = global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.PENDIENTE;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención, actualizando labels");
            }
        }


        private void CxP_Load(object sender, EventArgs e)
        {
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();
            var _ControladorBancos = new SOTControladores.Controladores.ControladorBancos();

            _ControladorCaja = new Controlador.Controladorcaja(_Cnn, _cod_folio, _Cod_usu);
            _ControladorPermisos = new Permisos.Controlador.Controlador(_Cnn);
            //_Permisos = _ControladorPermisos.getpermisoventana(_Cod_usu, this.Text);
            TiposPagoBindingSource.DataSource = _ControladorCaja.TraeTiposPago();
            BsBancos.DataSource = _ControladorBancos.ObtenerBancos();
            Tipo_pago.DataPropertyName = "codtipo_pago";
            Tipo_pago.DataSource = TiposPagoBindingSource;
            Tipo_pago.DisplayMember = "desc_pago";
            Tipo_pago.ValueMember = "codtipo_pago";
            CargarCuenta();
            CargaValoresdefault();
            Limpiar();
        }

        private void CargaValoresdefault()
        {
            if (_EsPorEstado)
            {
                for (int i = 0; i < this.DGVpagos.Rows.Count; i++)
                {
                    DGVpagos.Rows[i].Cells[estadoTmp.DisplayIndex].Value = "PAGADO";
                    DGVpagos.Rows[i].Cells["estadocol"].ReadOnly = true;
                    DGVpagos.Rows[i].Cells[fecha_pago.DisplayIndex].Value = DateTime.Now;
                    ActualizarLabels();
                }
            }
        }
        private void Limpiar()
        {
            //TxtFolio.Text = _ControladorCxP.TraeFolioCxP().ToString();
        }

        private void Bgrabar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != System.Windows.Forms.DialogResult.Yes)
                return;

            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            try
            {
                //ValidarPagos(CuentaxPagar);

                _ControladorCxP.ActualizarPagos(CuentaxPagar);

                CxPprincipal.Cargar();
                this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error guardando pagos " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        //private void ValidarPagos(global::Modelo.Almacen.Entidades.ZctEncCuentasPagar CuentaxPagar)
        //{
        //    foreach (var X in CuentaxPagar.ZctDetCuentasPagar)
        //    {
        //        if (X.codtipo_pago.HasValue && X.codtipo_pago == 0)
        //            throw new SOTException("El tipo de pago seleccionado es inválido");

        //        if (X.cod_banco.HasValue && X.cod_banco == 0)
        //            throw new SOTException("El banco seleccionado es inválido");

        //        //if (X.Estado == global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados.PAGADO && !X.cod_banco.HasValue)
        //        //    throw new SOTException("Seleccione un banco para poder registrar el pago");
        //    }
        //}

        private void button6_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }



        public void ActualizaProveedor(int cod_prov)
        {
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            //CuentaxPagar.ZctEncOC.ToString= _ControladorCxP.CambiaProveedor(cod_prov);
            Lcliente.Text = CuentaxPagar.ZctEncOC.ZctCatProv.Nom_Prov;
            LRFC.Text = CuentaxPagar.ZctEncOC.ZctCatProv.RFC_Prov;
            Lemail.Text = CuentaxPagar.ZctEncOC.ZctCatProv.email;
            Ldireccion.Text = CuentaxPagar.ZctEncOC.ZctCatProv.Dir_Prov;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (CuentaxPagar != null)
            {
                CxPprincipal.ModificarProveedor(CuentaxPagar.ZctEncOC.ZctCatProv.Cod_Prov);
            }
            else
            {
                MessageBox.Show("Seleccione una CxP", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Bcancelar_Click(object sender, EventArgs e)
        {
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            //_ControladorCxP.Refresca();
            BsEstadoPagos.ResetCurrentItem();
        }

    }
}
