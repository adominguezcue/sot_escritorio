﻿using ResTotal.Vista.CxP;
using SOTControladores.Utilidades;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal.Extensiones;
using ResTotal.Helpers;
using ResTotal.DtoReportes;					   

namespace ResTotal.Vista
{
    public partial class CxP_principal : Form, IGestorCuentasXPagar
    {
        CxPDetallado detalles;
        public delegate void DcambiaProveedor(int Cod_prov);
        public event DcambiaProveedor cambiaproveedor;
        string _cnn;
        string _cod_folio;
        int _cod_usu;
        private bool ordenando = false;
        private ColorConverter colorConverter;
        private IBindingListView proveedoresLista;
        private string columnaNombreProveedor = ObjectExtensions.NombrePropiedad<global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP, string>(m => m.Nom_Prov);
        List<global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP> _Proveedores;
        List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar> _DetallesCuentasPorProveedor;
        List<DtosDetallesCxP> _DetalleDatosreporteCxP;

        public CxP_principal(string Cnn, int Cod_usu, string cod_folio)
        {
            InitializeComponent();
            colorConverter = new ColorConverter();
            dgvProveedores.RowEnter += new DataGridViewCellEventHandler(dgvProveedores_RowEnter);
            this.DGVdetalles.CellDoubleClick += new DataGridViewCellEventHandler(DGVdetalles_CellDoubleClick);
            this.DGVdetalles.RowEnter += new DataGridViewCellEventHandler(DGVdetalles_RowEnter);
            CBO_estado.LostFocus += new EventHandler(CBO_estado_LostFocus);
            this.Activated += new EventHandler(CxP_principal_Activated);
			dgvProveedores.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(dgvProveedores_ColumnaClickMouse);																												 

            var estados = Transversal.Extensiones.EnumExtensions.ComoListaDto<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados>().ToList();
            estados.Insert(0, new Transversal.Dtos.DtoEnum { Nombre = "TODOS", Valor = null });

            CBO_estado.DataSource = estados;
            CBO_estado.DisplayMember = "Nombre";
            CBO_estado.ValueMember = "Valor";
            _cod_usu = Cod_usu;
            _cod_folio = cod_folio;
            _cnn = Cnn;
        }

        private void dgvProveedores_ColumnaClickMouse(object sender, DataGridViewCellMouseEventArgs e)
        {
            string _switch = dgvProveedores.Columns[e.ColumnIndex].Name;
            var direccion = dgvProveedores.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection;
            string _strdireccion = direccion.ToString();
            switch (_switch)
            {
                case "codprovDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Proveedores = _Proveedores.OrderBy(o => o.Cod_Prov).ToList();
                    else
                        _Proveedores = _Proveedores.OrderByDescending(o => o.Cod_Prov).ToList();
                    break;
                case "nomProvDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Proveedores = _Proveedores.OrderBy(o => o.Nom_Prov).ToList();
                    else
                        _Proveedores = _Proveedores.OrderByDescending(o => o.Nom_Prov).ToList();
                    break;
                case "subtotalDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Proveedores = _Proveedores.OrderBy(o => o.subtotal).ToList();
                    else
                        _Proveedores = _Proveedores.OrderByDescending(o => o.subtotal).ToList();
                    break;
                case "ivaDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Proveedores = _Proveedores.OrderBy(o => o.iva).ToList();
                    else
                        _Proveedores = _Proveedores.OrderByDescending(o => o.iva).ToList();
                    break;
                case "totalDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Proveedores = _Proveedores.OrderBy(o => o.total).ToList();
                    else
                        _Proveedores = _Proveedores.OrderByDescending(o => o.total).ToList();
                    break;
                default:
                    break;
            }
		}
			
        private global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? ObtenerSeleccionCB()
        {
            return (CBO_estado.SelectedItem as Transversal.Dtos.DtoEnum).Valor as global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados?;
        }



        void CBO_estado_LostFocus(object sender, EventArgs e)
        {
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            if (dgvProveedores.SelectedRows.Count <= 0)
                return;

            int Cod_prov = Convert.ToInt32(dgvProveedores.SelectedRows[0].Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);
            BScuentas.DataSource = GenerarDataSource(_ControladorCxP.TraeCuentasProveedores(Cod_prov, ObtenerSeleccionCB()));
        }

        private IBindingListView GenerarDataSource<T>(List<T> elementos/*, string columnaFiltro = null, string filtro = null*/)
        {
            return new BuscadorBindingList<T>(elementos);

            //if (string.IsNullOrEmpty(columnaFiltro) || string.IsNullOrEmpty(filtro))
            //    return l;

            //FiltrarLista(l, columnaFiltro, filtro);

            //return l;
        }

        void CxP_principal_Activated(object sender, EventArgs e)
        {
#if DEBUG
#warning El retorno solamente debe ser efectivo en Debug
            Debug.WriteLine("Recarga en activated suprimida en modo debug porque dificulta un poco la depuración del form");
            return;
#endif
            Cargar(ObtenerSeleccionCB());
        }

        private void verdetalles()
        {
            if (DGVdetalles.CurrentRow == null)
                return;

            int cod_cta = Convert.ToInt32(DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[cod_cuenta_pago.DisplayIndex].Value);
            int cod_oc = Convert.ToInt32(DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[codOCDataGridViewTextBoxColumn.DisplayIndex].Value);
            bool _Espendiente = false;
            string estado = DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[estadoDataGridViewTextBoxColumn.DisplayIndex].Value.ToString();
            if (estado == "PENDIENTE")
                _Espendiente = true;
            detalles = new CxPDetallado(_cnn, _cod_usu, _cod_folio, cod_cta, cod_oc, Convert.ToDecimal(DGVdetalles.CurrentRow.Cells["totalDataGridViewTextBoxColumn1"].Value), false, _Espendiente)
            {
                CxPprincipal = this,
                MdiParent = this.MdiParent
            };
            detalles.Show();
        }

        void DGVdetalles_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (ordenando)
                return;

            ActualizarBotonDetalles(e.RowIndex);
        }

        private void ActualizarBotonDetalles(int fila)
        {
            if (fila < 0)
            {
                Bgrabar.Enabled = false;
                Bgrabar.Text = "";
                return;
            }

            Bgrabar.Enabled = true;
            string estado = DGVdetalles.Rows[fila].Cells["estadoDataGridViewTextBoxColumn"].Value.ToString();
            Bgrabar.Text = (estado == "PENDIENTE" ? "Realizar pago" : "Ver detalles");
            //var result = DGVdetalles.Rows[e.RowIndex].Cells["totalDataGridViewTextBoxColumn1"].Value;
            //ltotaldetalle.Text = string.Format("{0:C}", result);
        }

        void DGVdetalles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            verdetalles();
        }

        void dgvProveedores_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (ordenando)
                return;

            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            int Cod_prov = Convert.ToInt32(dgvProveedores.Rows[e.RowIndex].Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);
            //BScuentas.DataSource = _ControladorCxP.TraeCuentasProveedores(Cod_prov, ObtenerSeleccionCB());
            //DGVdetalles.DataSource = BScuentas;

            CargarDetalles(_ControladorCxP, Cod_prov, ObtenerSeleccionCB());
        }
        public void Cargar(global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? estado = null)
        {
            Cursor.Current = Cursors.WaitCursor;												
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            DGVdetalles.DataSource = null;

            var proveedores = _ControladorCxP.TraeProveedoresPorEstatusCuenta(txtFiltro.Text, (int?)estado);
            BSprovedores.DataSource = (proveedoresLista = GenerarDataSource(proveedores));
            BSprovedores.ResetCurrentItem();
            _Proveedores = proveedores;
            if (proveedores.Count == 0)
            {
                Bgrabar.Enabled = false;
                Bgrabar.Text = "";
            }

            decimal result = proveedores.Sum(x => x.total ?? 0);
            Ltotalpagar.Text = string.Format("{0:C}", result);

            if (dgvProveedores.CurrentRow != null)
            {
                int Cod_prov = Convert.ToInt32(dgvProveedores.CurrentRow.Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);

                CargarDetalles(_ControladorCxP, Cod_prov, estado);
            }
            Cursor.Current = Cursors.Default;


        }

        private void CargarDetalles(SOTControladores.Controladores.ControladorCuentasPorPagar _ControladorCxP, int Cod_prov, global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? estado)
        {
            decimal result = 0;
            var cuentas = _ControladorCxP.TraeCuentasProveedores(Cod_prov, estado);
            BScuentas.DataSource = GenerarDataSource(cuentas);
            DGVdetalles.DataSource = BScuentas;

            result = cuentas.Sum(m => m.total);


            //if (DGVdetalles.CurrentRow != null)
            //{
            //    result = Convert.ToDecimal(DGVdetalles.CurrentRow.Cells["totalDataGridViewTextBoxColumn1"].Value);
            //}
            ltotaldetalle.Text = string.Format("{0:C}", result);
        }

        private void CxP_principal_Load(object sender, EventArgs e)
        {
            CBO_estado.SelectedIndex = 0;
            Cargar();
        }

        private void DGVdetalles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //var result=DGVdetalles.Rows[e.RowIndex].Cells["totalDataGridViewTextBoxColumn1"].Value;
            //ltotaldetalle.Text = string.Format("{0:C}", result);
        }

        private void Bgrabar_Click(object sender, EventArgs e)
        {
            verdetalles();
        }
        public void ModificarProveedor(int cod_prov)
        {
            cambiaproveedor(cod_prov);
            detalles.ActualizaProveedor(cod_prov);

        }
        private void CBO_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cargar(ObtenerSeleccionCB());
        }

        private void dgvProveedores_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Ordenar(sender as DataGridView, e.ColumnIndex);

            if (dgvProveedores.CurrentRow != null)
            {
                int Cod_prov = Convert.ToInt32(dgvProveedores.CurrentRow.Cells[codprovDataGridViewTextBoxColumn.DisplayIndex].Value);

                CargarDetalles(new SOTControladores.Controladores.ControladorCuentasPorPagar(), Cod_prov, ObtenerSeleccionCB());
            }
        }

        private void Ordenar(DataGridView dgv, int indiceColumna)
        {
            ordenando = true;

            try
            {
                //var dgv = (sender as DataGridView);

                DataGridViewColumn newColumn = dgv.Columns[indiceColumna];
                DataGridViewColumn oldColumn = dgv.SortedColumn;
                ListSortDirection direction;

                // If oldColumn is null, then the DataGridView is not sorted.
                if (oldColumn != null)
                {
                    // Sort the same column again, reversing the SortOrder.
                    if (oldColumn == newColumn &&
                        dgv.SortOrder == SortOrder.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        // Sort a new column and remove the old SortGlyph.
                        direction = ListSortDirection.Ascending;
                        oldColumn.HeaderCell.SortGlyphDirection = SortOrder.None;
                    }
                }
                else
                {
                    direction = ListSortDirection.Ascending;
                }

                // Sort the selected column.



                dgv.Sort(newColumn, direction);
                newColumn.HeaderCell.SortGlyphDirection =
                    direction == ListSortDirection.Ascending ?
                    SortOrder.Ascending : SortOrder.Descending;
            }
            finally
            {
                ordenando = false;
            }
        }

        private void dgvProveedores_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewColumn column in (sender as DataGridView).Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }
        }

        private void DGVdetalles_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Ordenar(sender as DataGridView, e.ColumnIndex);

            ActualizarBotonDetalles(DGVdetalles.CurrentCell?.RowIndex ?? -1);
        }

        private void DGVdetalles_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (Convert.ToBoolean(DGVdetalles.Rows[e.RowIndex].Cells[EsVencida.Index].Value))
            {
                DGVdetalles.Rows[e.RowIndex].DefaultCellStyle.BackColor = (Color)colorConverter.ConvertFromString("#f75151");
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {

            

            //FiltrarLista(proveedoresLista, columnaNombreProveedor, txtFiltro.Text);

            ////if (txtFiltro.Nombre != "Columna:")
            ////{
            //if (string.IsNullOrEmpty(txtFiltro.Text))
            //    proveedoresLista.Filter = null;
            //else
            //    proveedoresLista.Filter = string.Format("{0} != null && {0}.ToUpper().Contains(\"{1}\".ToUpper())", ObjectExtensions.NombrePropiedad<global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP, string>(m=> m.Nom_Prov), txtFiltro.Text);
            ////}
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Cargar(ObtenerSeleccionCB());
        }

        private void Btn_Reporte_Click(object sender, EventArgs e)
        {
            var controladorGlobal = new SOTControladores.Controladores.ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();

            List<DtosReporteCabecero> Encabezado = new List<DtosReporteCabecero>();
            DtosReporteCabecero DatosCabecera = new DtosReporteCabecero();
            DatosCabecera.Hotel = config.Nombre;
            DatosCabecera.Estadocuenta = CBO_estado.Text.ToString();
            DatosCabecera.Direccion = config.Direccion;
            DatosCabecera.Usuario = SOTControladores.Controladores.ControladorBase.UsuarioActual.NombreCompleto;
            Encabezado.Add(DatosCabecera);

            var documentoReporte = new ResTotal.ReportesRT.CuentasPorPagar.CxP_Proveedores();

             documentoReporte.Load();
             //Encabezado
            documentoReporte.SetDataSource(Encabezado);
            //Detalles
            CargarDatos(_Proveedores);
            documentoReporte.Subreports["CxP_Cabecero"].Database.Tables[0].SetDataSource(LlenaDatosReporte(_Proveedores));
            documentoReporte.Subreports["CxP_Cabecero"].Database.Tables[1].SetDataSource(_DetalleDatosreporteCxP);
            //Totales
            documentoReporte.Subreports["Total"].SetDataSource(LlenaTotal());

            CrystalDecisions.Windows.Forms.CrystalReportViewer reporteform = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            reporteform.ReportSource = documentoReporte;
            ReporteViewer reporte = new ReporteViewer(reporteform);
            reporte.Show();

        }
        private void CargarDatos(List<global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP> ProveedoresCxP)
        {
            _DetallesCuentasPorProveedor = new List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar>();
            foreach (global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP item in ProveedoresCxP)
            {
                CargarDetallesReporte(new SOTControladores.Controladores.ControladorCuentasPorPagar(), item.Cod_Prov, ObtenerSeleccionCB());
            }
        }


        private void CargarDetallesReporte(SOTControladores.Controladores.ControladorCuentasPorPagar _ControladorCxP, int Cod_prov, global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? estado)
        {
            var cuentas = _ControladorCxP.TraeCuentasProveedores(Cod_prov, estado);
            foreach (global::Modelo.Almacen.Entidades.ZctEncCuentasPagar item in cuentas)
            {
                _DetallesCuentasPorProveedor.Add(item);
            }
        }
        private void DgvProveedores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private List<DtosReporteDetallesCxP> LlenaDatosReporte(List<global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP> Proeevedores1)
        {
            Cursor.Current = Cursors.WaitCursor;
            List<DtosReporteDetallesCxP> _ListDatosCuentasPorPagar = new List<DtosReporteDetallesCxP>();
            _DetalleDatosreporteCxP = new List<DtosDetallesCxP>();
            foreach (global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP  prov in Proeevedores1)
            {
                DtosReporteDetallesCxP CabeceroYDetalle = new DtosReporteDetallesCxP();
                CabeceroYDetalle.CodigoProveedorCab = prov.Cod_Prov;
                CabeceroYDetalle.NombreProvedorCab = prov.Nom_Prov;
                CabeceroYDetalle.SubtotalCab = Convert.ToDecimal(prov.subtotal);
                CabeceroYDetalle.IvaCab = Convert.ToDecimal(prov.iva);
                CabeceroYDetalle.TotalCab = Convert.ToDecimal(prov.total);
                
               foreach (global::Modelo.Almacen.Entidades.ZctEncCuentasPagar deta in CargaListaDetallesReporte(new SOTControladores.Controladores.ControladorCuentasPorPagar(), prov.Cod_Prov, ObtenerSeleccionCB()))
                {
                        DtosDetallesCxP _detalle = new DtosDetallesCxP();
                        _detalle.LlaveCodProveedo= prov.Cod_Prov;
                        _detalle.CodigoordenCompraDet = deta.Cod_OC;
                        _detalle.SubtotalDet = deta.subtotal;
                        _detalle.IvaDet = deta.iva;
                        _detalle.TotalDet = deta.total;
                        _detalle.Fecha_CreacionDet = deta.fecha_creacion;
                        _detalle.Fecha_VencimientoDet = deta.fecha_vencido;
                        _detalle.NumeroFacturaDet = deta.FacturaTMP;
                        _detalle.EstadoDet = Convert.ToString(deta.Estado);
                        _detalle.Esvencida = deta.EsVencida;
                        _DetalleDatosreporteCxP.Add(_detalle);
                }
                _ListDatosCuentasPorPagar.Add(CabeceroYDetalle);
            }


            Cursor.Current = Cursors.Default;
           return _ListDatosCuentasPorPagar;

        }

        private List<DtosReportePieTotales> LlenaTotal()
        {
            List<DtosReportePieTotales> respuesta = new List<DtosReportePieTotales>();
            DtosReportePieTotales dato = new DtosReportePieTotales();
            dato.Total = Ltotalpagar.Text.ToString();
            respuesta.Add(dato);
            return respuesta;
        }


        private List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar> CargaListaDetallesReporte(SOTControladores.Controladores.ControladorCuentasPorPagar _ControladorCxP, int Cod_prov, global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? estado)
        {
            List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar> respuesta = new List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar>();
            return respuesta= _ControladorCxP.TraeCuentasProveedores(Cod_prov, estado);
        }
        //private void FiltrarLista(IBindingListView lista, string columna, string filtro)
        //{
        //    if (string.IsNullOrEmpty(filtro))
        //        lista.Filter = null;
        //    else
        //        lista.Filter = string.Format("{0} != null && {0}.ToUpper().Contains(\"{1}\".ToUpper())", columna, filtro);
        //}
    }
}
