﻿namespace ResTotal.Vista
{
    partial class CxP_principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvProveedores = new System.Windows.Forms.DataGridView();
            this.codprovDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomProvDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ivaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BSprovedores = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DGVdetalles = new System.Windows.Forms.DataGridView();
            this.codOCDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotalDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ivaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechacreacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechavencidoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_cuenta_pago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facturaDataGridTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EsVencida = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BScuentas = new System.Windows.Forms.BindingSource(this.components);
            this.Bgrabar = new System.Windows.Forms.Button();
            this.Ltotalpagar = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.ltotaldetalle = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CBO_estado = new System.Windows.Forms.ComboBox();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
			this.Btn_Reporte = new System.Windows.Forms.Button();													 
            ((System.ComponentModel.ISupportInitialize)(this.dgvProveedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSprovedores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BScuentas)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvProveedores
            // 
            this.dgvProveedores.AllowUserToAddRows = false;
            this.dgvProveedores.AllowUserToDeleteRows = false;
            this.dgvProveedores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvProveedores.AutoGenerateColumns = false;
            this.dgvProveedores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProveedores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codprovDataGridViewTextBoxColumn,
            this.nomProvDataGridViewTextBoxColumn,
            this.subtotalDataGridViewTextBoxColumn,
            this.ivaDataGridViewTextBoxColumn,
            this.totalDataGridViewTextBoxColumn});
            this.dgvProveedores.DataSource = this.BSprovedores;
            this.dgvProveedores.Location = new System.Drawing.Point(12, 74);
            this.dgvProveedores.Name = "dgvProveedores";
            this.dgvProveedores.ReadOnly = true;
            this.dgvProveedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProveedores.Size = new System.Drawing.Size(896, 175);
            this.dgvProveedores.TabIndex = 0;
			this.dgvProveedores.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvProveedores_CellContentClick);																																	
            this.dgvProveedores.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvProveedores_ColumnHeaderMouseClick);
            this.dgvProveedores.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvProveedores_ColumnHeaderMouseClick);
            this.dgvProveedores.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvProveedores_DataBindingComplete);
            // 
            // codprovDataGridViewTextBoxColumn
            // 
            this.codprovDataGridViewTextBoxColumn.DataPropertyName = "Cod_Prov";
            this.codprovDataGridViewTextBoxColumn.HeaderText = "Código de Proveedor";
            this.codprovDataGridViewTextBoxColumn.Name = "codprovDataGridViewTextBoxColumn";
            this.codprovDataGridViewTextBoxColumn.ReadOnly = true;
            this.codprovDataGridViewTextBoxColumn.Width = 121;
            // 
            // nomProvDataGridViewTextBoxColumn
            // 
            this.nomProvDataGridViewTextBoxColumn.DataPropertyName = "Nom_Prov";
            this.nomProvDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nomProvDataGridViewTextBoxColumn.MinimumWidth = 400;
            this.nomProvDataGridViewTextBoxColumn.Name = "nomProvDataGridViewTextBoxColumn";
            this.nomProvDataGridViewTextBoxColumn.ReadOnly = true;
            this.nomProvDataGridViewTextBoxColumn.Width = 400;
            // 
            // subtotalDataGridViewTextBoxColumn
            // 
            this.subtotalDataGridViewTextBoxColumn.DataPropertyName = "SubtotalNN";
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = "0";
            this.subtotalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.subtotalDataGridViewTextBoxColumn.HeaderText = "Subtotal";
            this.subtotalDataGridViewTextBoxColumn.Name = "subtotalDataGridViewTextBoxColumn";
            this.subtotalDataGridViewTextBoxColumn.ReadOnly = true;
            this.subtotalDataGridViewTextBoxColumn.Width = 71;
            // 
            // ivaDataGridViewTextBoxColumn
            // 
            this.ivaDataGridViewTextBoxColumn.DataPropertyName = "IVANN";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = "0";
            this.ivaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.ivaDataGridViewTextBoxColumn.HeaderText = "Iva";
            this.ivaDataGridViewTextBoxColumn.Name = "ivaDataGridViewTextBoxColumn";
            this.ivaDataGridViewTextBoxColumn.ReadOnly = true;
            this.ivaDataGridViewTextBoxColumn.Width = 47;
            // 
            // totalDataGridViewTextBoxColumn
            // 
            this.totalDataGridViewTextBoxColumn.DataPropertyName = "TotalNN";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = "0";
            this.totalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.totalDataGridViewTextBoxColumn.HeaderText = "Total";
            this.totalDataGridViewTextBoxColumn.Name = "totalDataGridViewTextBoxColumn";
            this.totalDataGridViewTextBoxColumn.ReadOnly = true;
            this.totalDataGridViewTextBoxColumn.Width = 56;
            // 
            // BSprovedores
            // 
            this.BSprovedores.DataSource = typeof(global::Modelo.Almacen.Entidades.WV_ZctProveedoresCxP);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Proveedores";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(373, 305);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Detalle de cuentas";
            // 
            // DGVdetalles
            // 
            this.DGVdetalles.AllowUserToAddRows = false;
            this.DGVdetalles.AllowUserToDeleteRows = false;
            this.DGVdetalles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVdetalles.AutoGenerateColumns = false;
            this.DGVdetalles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGVdetalles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVdetalles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codOCDataGridViewTextBoxColumn,
            this.subtotalDataGridViewTextBoxColumn1,
            this.ivaDataGridViewTextBoxColumn1,
            this.totalDataGridViewTextBoxColumn1,
            this.fechacreacionDataGridViewTextBoxColumn,
            this.fechavencidoDataGridViewTextBoxColumn,
            this.cod_cuenta_pago,
            this.facturaDataGridTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn,
            this.EsVencida});
            this.DGVdetalles.DataSource = this.BScuentas;
            this.DGVdetalles.Location = new System.Drawing.Point(12, 334);
            this.DGVdetalles.Name = "DGVdetalles";
            this.DGVdetalles.ReadOnly = true;
            this.DGVdetalles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVdetalles.Size = new System.Drawing.Size(896, 213);
            this.DGVdetalles.TabIndex = 3;
            this.DGVdetalles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVdetalles_CellContentClick);
            this.DGVdetalles.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DGVdetalles_ColumnHeaderMouseClick);
            this.DGVdetalles.ColumnHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DGVdetalles_ColumnHeaderMouseClick);
            this.DGVdetalles.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvProveedores_DataBindingComplete);
            this.DGVdetalles.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.DGVdetalles_RowPrePaint);
            // 
            // codOCDataGridViewTextBoxColumn
            // 
            this.codOCDataGridViewTextBoxColumn.DataPropertyName = "Cod_OC";
            this.codOCDataGridViewTextBoxColumn.HeaderText = "Código de Orden de Compra";
            this.codOCDataGridViewTextBoxColumn.Name = "codOCDataGridViewTextBoxColumn";
            this.codOCDataGridViewTextBoxColumn.ReadOnly = true;
            this.codOCDataGridViewTextBoxColumn.Width = 119;
            // 
            // subtotalDataGridViewTextBoxColumn1
            // 
            this.subtotalDataGridViewTextBoxColumn1.DataPropertyName = "subtotal";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = "0";
            this.subtotalDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.subtotalDataGridViewTextBoxColumn1.HeaderText = "Subtotal";
            this.subtotalDataGridViewTextBoxColumn1.Name = "subtotalDataGridViewTextBoxColumn1";
            this.subtotalDataGridViewTextBoxColumn1.ReadOnly = true;
            this.subtotalDataGridViewTextBoxColumn1.Width = 71;
            // 
            // ivaDataGridViewTextBoxColumn1
            // 
            this.ivaDataGridViewTextBoxColumn1.DataPropertyName = "iva";
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = "0";
            this.ivaDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle5;
            this.ivaDataGridViewTextBoxColumn1.HeaderText = "IVA";
            this.ivaDataGridViewTextBoxColumn1.Name = "ivaDataGridViewTextBoxColumn1";
            this.ivaDataGridViewTextBoxColumn1.ReadOnly = true;
            this.ivaDataGridViewTextBoxColumn1.Width = 49;
            // 
            // totalDataGridViewTextBoxColumn1
            // 
            this.totalDataGridViewTextBoxColumn1.DataPropertyName = "total";
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "0";
            this.totalDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle6;
            this.totalDataGridViewTextBoxColumn1.HeaderText = "Total";
            this.totalDataGridViewTextBoxColumn1.Name = "totalDataGridViewTextBoxColumn1";
            this.totalDataGridViewTextBoxColumn1.ReadOnly = true;
            this.totalDataGridViewTextBoxColumn1.Width = 56;
            // 
            // fechacreacionDataGridViewTextBoxColumn
            // 
            this.fechacreacionDataGridViewTextBoxColumn.DataPropertyName = "fecha_creacion";
            this.fechacreacionDataGridViewTextBoxColumn.HeaderText = "Fecha de Creación";
            this.fechacreacionDataGridViewTextBoxColumn.Name = "fechacreacionDataGridViewTextBoxColumn";
            this.fechacreacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechacreacionDataGridViewTextBoxColumn.Width = 112;
            // 
            // fechavencidoDataGridViewTextBoxColumn
            // 
            this.fechavencidoDataGridViewTextBoxColumn.DataPropertyName = "fecha_vencido";
            this.fechavencidoDataGridViewTextBoxColumn.HeaderText = "Fecha de Vencido";
            this.fechavencidoDataGridViewTextBoxColumn.Name = "fechavencidoDataGridViewTextBoxColumn";
            this.fechavencidoDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechavencidoDataGridViewTextBoxColumn.Width = 109;
            // 
            // cod_cuenta_pago
            // 
            this.cod_cuenta_pago.DataPropertyName = "cod_cuenta_pago";
            this.cod_cuenta_pago.HeaderText = "Cód de Cuenta de Pago";
            this.cod_cuenta_pago.Name = "cod_cuenta_pago";
            this.cod_cuenta_pago.ReadOnly = true;
            this.cod_cuenta_pago.Visible = false;
            this.cod_cuenta_pago.Width = 111;
            // 
            // facturaDataGridTextBoxColumn
            // 
            this.facturaDataGridTextBoxColumn.DataPropertyName = "FacturaTMP";
            this.facturaDataGridTextBoxColumn.HeaderText = "No Factura";
            this.facturaDataGridTextBoxColumn.MinimumWidth = 200;
            this.facturaDataGridTextBoxColumn.Name = "facturaDataGridTextBoxColumn";
            this.facturaDataGridTextBoxColumn.ReadOnly = true;
            this.facturaDataGridTextBoxColumn.Width = 200;
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.ReadOnly = true;
            this.estadoDataGridViewTextBoxColumn.Width = 65;
            // 
            // EsVencida
            // 
            this.EsVencida.DataPropertyName = "EsVencida";
            this.EsVencida.HeaderText = "EsVencida";
            this.EsVencida.Name = "EsVencida";
            this.EsVencida.ReadOnly = true;
            this.EsVencida.Visible = false;
            this.EsVencida.Width = 64;
            // 
            // BScuentas
            // 
            this.BScuentas.DataSource = typeof(global::Modelo.Almacen.Entidades.ZctEncCuentasPagar);
            // 
            // Bgrabar
            // 
            this.Bgrabar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bgrabar.Location = new System.Drawing.Point(805, 561);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 43);
            this.Bgrabar.TabIndex = 16;
            this.Bgrabar.Text = "Realizar pago";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // Ltotalpagar
            // 
            this.Ltotalpagar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Ltotalpagar.ForeColor = System.Drawing.Color.Navy;
            this.Ltotalpagar.Location = new System.Drawing.Point(560, 264);
            this.Ltotalpagar.Name = "Ltotalpagar";
            this.Ltotalpagar.Size = new System.Drawing.Size(129, 26);
            this.Ltotalpagar.TabIndex = 18;
            this.Ltotalpagar.Text = "$0.00";
            this.Ltotalpagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(231, 263);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(284, 27);
            this.label13.TabIndex = 17;
            this.label13.Text = "Total en cuentas por pagar:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ltotaldetalle
            // 
            this.ltotaldetalle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ltotaldetalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltotaldetalle.ForeColor = System.Drawing.Color.Navy;
            this.ltotaldetalle.Location = new System.Drawing.Point(316, 561);
            this.ltotaldetalle.Name = "ltotaldetalle";
            this.ltotaldetalle.Size = new System.Drawing.Size(184, 32);
            this.ltotaldetalle.TabIndex = 20;
            this.ltotaldetalle.Text = "$0.00";
            this.ltotaldetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(75, 556);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(237, 45);
            this.label4.TabIndex = 19;
            this.label4.Text = "Total detalle proveedor:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.label3.AutoSize = true;							
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(741, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 24);
            this.label3.TabIndex = 1;
            this.label3.Text = "Estado Cuenta:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // CBO_estado
            // 
            this.CBO_estado.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.CBO_estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBO_estado.FormattingEnabled = true;
            this.CBO_estado.Location = new System.Drawing.Point(718, 38);
            this.CBO_estado.Name = "CBO_estado";
            this.CBO_estado.Size = new System.Drawing.Size(190, 21);
            this.CBO_estado.TabIndex = 22;
            this.CBO_estado.SelectedIndexChanged += new System.EventHandler(this.CBO_estado_SelectedIndexChanged);
            // 
            // txtFiltro
            // 
            this.txtFiltro.Location = new System.Drawing.Point(12, 38);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(190, 20);
            this.txtFiltro.TabIndex = 23;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(208, 38);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 20);
            this.btnBuscar.TabIndex = 24;
            this.btnBuscar.Text = "Filtrar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
			this.Btn_Reporte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Reporte.AutoSize = true;
            this.Btn_Reporte.Location = new System.Drawing.Point(696, 561);
            this.Btn_Reporte.Name = "Btn_Reporte";
            this.Btn_Reporte.Size = new System.Drawing.Size(103, 43);
            this.Btn_Reporte.TabIndex = 25;
            this.Btn_Reporte.Text = "Ver Reporte";
            this.Btn_Reporte.UseVisualStyleBackColor = true;
            this.Btn_Reporte.Click += new System.EventHandler(this.Btn_Reporte_Click);																																					     
            // CxP_principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 610);
			this.Controls.Add(this.Btn_Reporte);													
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtFiltro);
            this.Controls.Add(this.CBO_estado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ltotaldetalle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Ltotalpagar);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Bgrabar);
            this.Controls.Add(this.DGVdetalles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvProveedores);
            this.Name = "CxP_principal";
            this.Text = "Cuentas por pagar por proveedor";
            this.Load += new System.EventHandler(this.CxP_principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProveedores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSprovedores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BScuentas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvProveedores;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView DGVdetalles;
        private System.Windows.Forms.BindingSource BSprovedores;
        private System.Windows.Forms.BindingSource BScuentas;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.Label Ltotalpagar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label ltotaldetalle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CBO_estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomProvDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ivaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codprovDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codOCDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ivaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechacreacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechavencidoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_cuenta_pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn facturaDataGridTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EsVencida;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Button btnBuscar;
		private System.Windows.Forms.Button Btn_Reporte;												
    }
}