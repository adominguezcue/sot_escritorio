﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResTotal.Vista.CxP
{
    internal interface IGestorCuentasXPagar
    {
        void Cargar(global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? estado = null);

        void ModificarProveedor(int idProveedor);
    }
}
