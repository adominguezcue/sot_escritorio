﻿namespace ResTotal.Vista
{
    partial class CxP_PorEstado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.DGVdetalles = new System.Windows.Forms.DataGridView();
            this.NombreProveedorTMPdgvtbc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codOCDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechacreacionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechavencidoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subtotalDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ivaDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cod_cuenta_pago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.facturaDataGridTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EsVencidaColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BScuentas = new System.Windows.Forms.BindingSource(this.components);
            this.Bgrabar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.CBO_estado = new System.Windows.Forms.ComboBox();
            this.ltotaldetalle = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtFiltro = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dpFechaCreacionini = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dpFechaVencimientoIni = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.dpfechacracionfin = new System.Windows.Forms.DateTimePicker();
            this.dpfechavencimientofin = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnGeneraReporte = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BScuentas)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lato", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Detalle de cuentas";
            // 
            // DGVdetalles
            // 
            this.DGVdetalles.AllowUserToAddRows = false;
            this.DGVdetalles.AllowUserToDeleteRows = false;
            this.DGVdetalles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DGVdetalles.AutoGenerateColumns = false;
            this.DGVdetalles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.DGVdetalles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVdetalles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NombreProveedorTMPdgvtbc,
            this.codOCDataGridViewTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn,
            this.fechacreacionDataGridViewTextBoxColumn,
            this.fechavencidoDataGridViewTextBoxColumn,
            this.subtotalDataGridViewTextBoxColumn1,
            this.ivaDataGridViewTextBoxColumn1,
            this.totalDataGridViewTextBoxColumn1,
            this.cod_cuenta_pago,
            this.facturaDataGridTextBoxColumn,
            this.EsVencidaColumn});
            this.DGVdetalles.DataSource = this.BScuentas;
            this.DGVdetalles.Location = new System.Drawing.Point(12, 110);
            this.DGVdetalles.Name = "DGVdetalles";
            this.DGVdetalles.ReadOnly = true;
            this.DGVdetalles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVdetalles.Size = new System.Drawing.Size(910, 325);
            this.DGVdetalles.TabIndex = 3;
            this.DGVdetalles.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGVdetalles_CellContentClick);
            this.DGVdetalles.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.DGVdetalles_RowPrePaint);
            // 
            // NombreProveedorTMPdgvtbc
            // 
            this.NombreProveedorTMPdgvtbc.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.NombreProveedorTMPdgvtbc.DataPropertyName = "NombreProveedorTMP";
            this.NombreProveedorTMPdgvtbc.HeaderText = "Nombre del Proveedor";
            this.NombreProveedorTMPdgvtbc.MaxInputLength = 100;
            this.NombreProveedorTMPdgvtbc.MinimumWidth = 200;
            this.NombreProveedorTMPdgvtbc.Name = "NombreProveedorTMPdgvtbc";
            this.NombreProveedorTMPdgvtbc.ReadOnly = true;
            this.NombreProveedorTMPdgvtbc.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.NombreProveedorTMPdgvtbc.Width = 200;
            // 
            // codOCDataGridViewTextBoxColumn
            // 
            this.codOCDataGridViewTextBoxColumn.DataPropertyName = "Cod_OC";
            this.codOCDataGridViewTextBoxColumn.HeaderText = "Código de Orden de Compra";
            this.codOCDataGridViewTextBoxColumn.Name = "codOCDataGridViewTextBoxColumn";
            this.codOCDataGridViewTextBoxColumn.ReadOnly = true;
            this.codOCDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.codOCDataGridViewTextBoxColumn.Width = 119;
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "Estado";
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.ReadOnly = true;
            this.estadoDataGridViewTextBoxColumn.Width = 70;
            // 
            // fechacreacionDataGridViewTextBoxColumn
            // 
            this.fechacreacionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.fechacreacionDataGridViewTextBoxColumn.DataPropertyName = "fecha_creacion";
            this.fechacreacionDataGridViewTextBoxColumn.HeaderText = "Fecha de Creación";
            this.fechacreacionDataGridViewTextBoxColumn.Name = "fechacreacionDataGridViewTextBoxColumn";
            this.fechacreacionDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechacreacionDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fechacreacionDataGridViewTextBoxColumn.Width = 70;
            // 
            // fechavencidoDataGridViewTextBoxColumn
            // 
            this.fechavencidoDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.fechavencidoDataGridViewTextBoxColumn.DataPropertyName = "fecha_vencido";
            this.fechavencidoDataGridViewTextBoxColumn.HeaderText = "Fecha de Vencimiento";
            this.fechavencidoDataGridViewTextBoxColumn.Name = "fechavencidoDataGridViewTextBoxColumn";
            this.fechavencidoDataGridViewTextBoxColumn.ReadOnly = true;
            this.fechavencidoDataGridViewTextBoxColumn.Width = 71;
            // 
            // subtotalDataGridViewTextBoxColumn1
            // 
            this.subtotalDataGridViewTextBoxColumn1.DataPropertyName = "subtotal";
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = "0";
            this.subtotalDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.subtotalDataGridViewTextBoxColumn1.HeaderText = "Subtotal";
            this.subtotalDataGridViewTextBoxColumn1.Name = "subtotalDataGridViewTextBoxColumn1";
            this.subtotalDataGridViewTextBoxColumn1.ReadOnly = true;
            this.subtotalDataGridViewTextBoxColumn1.Width = 71;
            // 
            // ivaDataGridViewTextBoxColumn1
            // 
            this.ivaDataGridViewTextBoxColumn1.DataPropertyName = "iva";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = "0";
            this.ivaDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.ivaDataGridViewTextBoxColumn1.HeaderText = "IVA";
            this.ivaDataGridViewTextBoxColumn1.Name = "ivaDataGridViewTextBoxColumn1";
            this.ivaDataGridViewTextBoxColumn1.ReadOnly = true;
            this.ivaDataGridViewTextBoxColumn1.Width = 49;
            // 
            // totalDataGridViewTextBoxColumn1
            // 
            this.totalDataGridViewTextBoxColumn1.DataPropertyName = "total";
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = "0";
            this.totalDataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.totalDataGridViewTextBoxColumn1.HeaderText = "Total";
            this.totalDataGridViewTextBoxColumn1.Name = "totalDataGridViewTextBoxColumn1";
            this.totalDataGridViewTextBoxColumn1.ReadOnly = true;
            this.totalDataGridViewTextBoxColumn1.Width = 56;
            // 
            // cod_cuenta_pago
            // 
            this.cod_cuenta_pago.DataPropertyName = "cod_cuenta_pago";
            this.cod_cuenta_pago.HeaderText = "cod_cuenta_pago";
            this.cod_cuenta_pago.Name = "cod_cuenta_pago";
            this.cod_cuenta_pago.ReadOnly = true;
            this.cod_cuenta_pago.Visible = false;
            this.cod_cuenta_pago.Width = 119;
            // 
            // facturaDataGridTextBoxColumn
            // 
            this.facturaDataGridTextBoxColumn.DataPropertyName = "FacturaTMP";
            this.facturaDataGridTextBoxColumn.HeaderText = "No Factura";
            this.facturaDataGridTextBoxColumn.MinimumWidth = 100;
            this.facturaDataGridTextBoxColumn.Name = "facturaDataGridTextBoxColumn";
            this.facturaDataGridTextBoxColumn.ReadOnly = true;
            // 
            // EsVencidaColumn
            // 
            this.EsVencidaColumn.DataPropertyName = "EsVencida";
            this.EsVencidaColumn.HeaderText = "EsVencida";
            this.EsVencidaColumn.Name = "EsVencidaColumn";
            this.EsVencidaColumn.ReadOnly = true;
            this.EsVencidaColumn.Visible = false;
            this.EsVencidaColumn.Width = 64;
            // 
            // BScuentas
            // 
            this.BScuentas.DataSource = typeof(global::Modelo.Almacen.Entidades.ZctEncCuentasPagar);
            // 
            // Bgrabar
            // 
            this.Bgrabar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Bgrabar.Location = new System.Drawing.Point(819, 441);
            this.Bgrabar.Name = "Bgrabar";
            this.Bgrabar.Size = new System.Drawing.Size(103, 43);
            this.Bgrabar.TabIndex = 16;
            this.Bgrabar.Text = "Realizar Pago";
            this.Bgrabar.UseVisualStyleBackColor = true;
            this.Bgrabar.Click += new System.EventHandler(this.Bgrabar_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Lato", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 28);
            this.label3.TabIndex = 21;
            this.label3.Text = "Estado Cuenta:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // CBO_estado
            // 
            this.CBO_estado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBO_estado.FormattingEnabled = true;
            this.CBO_estado.Location = new System.Drawing.Point(157, 12);
            this.CBO_estado.Name = "CBO_estado";
            this.CBO_estado.Size = new System.Drawing.Size(190, 21);
            this.CBO_estado.TabIndex = 22;
            this.CBO_estado.SelectedIndexChanged += new System.EventHandler(this.CBO_estado_SelectedIndexChanged);
            // 
            // ltotaldetalle
            // 
            this.ltotaldetalle.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ltotaldetalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltotaldetalle.ForeColor = System.Drawing.Color.Navy;
            this.ltotaldetalle.Location = new System.Drawing.Point(245, 451);
            this.ltotaldetalle.Name = "ltotaldetalle";
            this.ltotaldetalle.Size = new System.Drawing.Size(294, 32);
            this.ltotaldetalle.TabIndex = 24;
            this.ltotaldetalle.Text = "$0.00";
            this.ltotaldetalle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(15, 446);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(237, 45);
            this.label4.TabIndex = 23;
            this.label4.Text = "Total detalle proveedor:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(236, 75);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(565, 29);
            this.btnBuscar.TabIndex = 26;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtFiltro
            // 
            this.txtFiltro.Location = new System.Drawing.Point(243, 42);
            this.txtFiltro.Name = "txtFiltro";
            this.txtFiltro.Size = new System.Drawing.Size(190, 20);
            this.txtFiltro.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lato", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 19);
            this.label1.TabIndex = 27;
            this.label1.Text = "Proveedor";
            // 
            // dpFechaCreacionini
            // 
            this.dpFechaCreacionini.Location = new System.Drawing.Point(477, 23);
            this.dpFechaCreacionini.Name = "dpFechaCreacionini";
            this.dpFechaCreacionini.Size = new System.Drawing.Size(200, 20);
            this.dpFechaCreacionini.TabIndex = 28;
            this.dpFechaCreacionini.ValueChanged += new System.EventHandler(this.DpFechaCreacion_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(405, -1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 16);
            this.label5.TabIndex = 29;
            this.label5.Text = "Fecha de creación";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // dpFechaVencimientoIni
            // 
            this.dpFechaVencimientoIni.Location = new System.Drawing.Point(727, 22);
            this.dpFechaVencimientoIni.Name = "dpFechaVencimientoIni";
            this.dpFechaVencimientoIni.Size = new System.Drawing.Size(200, 20);
            this.dpFechaVencimientoIni.TabIndex = 30;
            this.dpFechaVencimientoIni.ValueChanged += new System.EventHandler(this.DateTimePicker2_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lato", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(670, -1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 16);
            this.label6.TabIndex = 31;
            this.label6.Text = "Fecha de vencimiento";
            this.label6.Click += new System.EventHandler(this.Label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(447, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "De:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(697, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "De:";
            this.label8.Click += new System.EventHandler(this.Label8_Click);
            // 
            // dpfechacracionfin
            // 
            this.dpfechacracionfin.Location = new System.Drawing.Point(477, 49);
            this.dpfechacracionfin.Name = "dpfechacracionfin";
            this.dpfechacracionfin.Size = new System.Drawing.Size(200, 20);
            this.dpfechacracionfin.TabIndex = 34;
            this.dpfechacracionfin.ValueChanged += new System.EventHandler(this.Dpfechacracionfin_ValueChanged);
            // 
            // dpfechavencimientofin
            // 
            this.dpfechavencimientofin.Location = new System.Drawing.Point(727, 44);
            this.dpfechavencimientofin.Name = "dpfechavencimientofin";
            this.dpfechavencimientofin.Size = new System.Drawing.Size(200, 20);
            this.dpfechavencimientofin.TabIndex = 35;
            this.dpfechavencimientofin.ValueChanged += new System.EventHandler(this.Dpfechavencimientofin_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(447, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Al:";
            this.label9.Click += new System.EventHandler(this.Label9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(697, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 37;
            this.label10.Text = "Al:";
            this.label10.Click += new System.EventHandler(this.Label10_Click);
            // 
            // btnGeneraReporte
            // 
            this.btnGeneraReporte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGeneraReporte.Location = new System.Drawing.Point(700, 440);
            this.btnGeneraReporte.Name = "btnGeneraReporte";
            this.btnGeneraReporte.Size = new System.Drawing.Size(113, 43);
            this.btnGeneraReporte.TabIndex = 38;
            this.btnGeneraReporte.Text = "Reporte";
            this.btnGeneraReporte.UseVisualStyleBackColor = true;
            this.btnGeneraReporte.Click += new System.EventHandler(this.Button1_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(520, 0);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 39;
            this.checkBox1.Text = "Aplica Filtro";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(807, 0);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(80, 17);
            this.checkBox2.TabIndex = 40;
            this.checkBox2.Text = "Aplica Filtro";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.CheckBox2_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.MediumTurquoise;
            this.label11.Location = new System.Drawing.Point(16, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(202, 26);
            this.label11.TabIndex = 41;
            this.label11.Text = "Clic en el botón Buscar después después\r\nde validar los filtros.";
            this.label11.Click += new System.EventHandler(this.Label11_Click);
            // 
            // CxP_PorEstado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(934, 500);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.btnGeneraReporte);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.dpfechavencimientofin);
            this.Controls.Add(this.dpfechacracionfin);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dpFechaVencimientoIni);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dpFechaCreacionini);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtFiltro);
            this.Controls.Add(this.ltotaldetalle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CBO_estado);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Bgrabar);
            this.Controls.Add(this.DGVdetalles);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CxP_PorEstado";
            this.Text = "Cuentas por pagar";
            this.Load += new System.EventHandler(this.CxP_principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVdetalles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BScuentas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView DGVdetalles;
        private System.Windows.Forms.BindingSource BScuentas;
        private System.Windows.Forms.Button Bgrabar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox CBO_estado;
        private System.Windows.Forms.Label ltotaldetalle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtFiltro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dpFechaCreacionini;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dpFechaVencimientoIni;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dpfechacracionfin;
        private System.Windows.Forms.DateTimePicker dpfechavencimientofin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnGeneraReporte;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreProveedorTMPdgvtbc;
        private System.Windows.Forms.DataGridViewTextBoxColumn codOCDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechacreacionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechavencidoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subtotalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ivaDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cod_cuenta_pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn facturaDataGridTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EsVencidaColumn;
        private System.Windows.Forms.Label label11;
    }
}