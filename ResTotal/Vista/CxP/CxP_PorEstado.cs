﻿using ResTotal.Vista.CxP;
using SOTControladores.Utilidades;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.ComponentModel;
using System.Windows.Forms;
using ResTotal.DtoReportes;
using ResTotal.Helpers;
namespace ResTotal.Vista
{
    public partial class CxP_PorEstado : Form, IGestorCuentasXPagar
    {
        CxPDetallado detalles;
        public delegate void DcambiaProveedor(int Cod_prov);
        public event DcambiaProveedor cambiaproveedor;
        string _cnn;
        string _cod_folio;
        int _cod_usu;
        bool _EsbuquedaFechaCreacion = false;
        bool _Esbusquedafechafin = false;
        private ColorConverter colorConverter;
        List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar> _Cuentas;
        //ResTotal.Controlador.ControladorCxP _ControladorCxP;

        public CxP_PorEstado(string Cnn, int Cod_usu, string cod_folio)
        {
            InitializeComponent();

            colorConverter = new ColorConverter();
            this.DGVdetalles.CellDoubleClick += new DataGridViewCellEventHandler(DGVdetalles_CellDoubleClick);
            this.DGVdetalles.RowEnter += new DataGridViewCellEventHandler(DGVdetalles_RowEnter);
            this.DGVdetalles.SortCompare += new DataGridViewSortCompareEventHandler(DGVdetalles_sortedcompare);
            this.DGVdetalles.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(DGVdetalles_ColumnaclickMouse);
            this.DGVdetalles.Columns[3].DefaultCellStyle.Format = "dd-MM-yyyy";
            this.DGVdetalles.Columns[4].DefaultCellStyle.Format = "dd-MM-yyyy";
            CBO_estado.LostFocus += new EventHandler(CBO_estado_LostFocus);
            this.Activated += new EventHandler(CxP_principal_Activated);
            _Cuentas = new List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar>();
            var estados = Transversal.Extensiones.EnumExtensions.ComoListaDto<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados>().ToList();
            estados.Insert(0, new Transversal.Dtos.DtoEnum{ Nombre="TODOS", Valor = null});

            CBO_estado.DataSource = estados;
            CBO_estado.DisplayMember = "Nombre";
            CBO_estado.ValueMember = "Valor";
            _cod_usu = Cod_usu;
            _cod_folio = cod_folio;
            _cnn = Cnn;
            checkBox1.Checked = false;
            checkBox2.Checked = false;
            var fechaFinal = DateTime.Now;
            dpFechaCreacionini.Value = fechaFinal;
            dpfechacracionfin.Value = fechaFinal;
            dpFechaVencimientoIni.Value = fechaFinal;
            dpfechavencimientofin.Value = fechaFinal;
            if (checkBox1.Checked == false)
            {
                dpFechaCreacionini.Enabled = false;
                dpfechacracionfin.Enabled = false;
            }
            if (checkBox2.Checked == false)
            {
                dpFechaVencimientoIni.Enabled = false;
                dpfechavencimientofin.Enabled = false;
            }

        }

        private void DGVdetalles_ColumnaclickMouse(object sender, DataGridViewCellMouseEventArgs e)
        {
            string _switch = DGVdetalles.Columns[e.ColumnIndex].Name;
            var direccion = DGVdetalles.Columns[e.ColumnIndex].HeaderCell.SortGlyphDirection;
            string _strdireccion = direccion.ToString();
            switch (_switch)
            {
                case "NombreProveedorTMPdgvtbc":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.NombreProveedorTMP).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.NombreProveedorTMP).ToList();
                    break;
                case "codOCDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.Cod_OC).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.Cod_OC).ToList();
                    break;
                case "subtotalDataGridViewTextBoxColumn1":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.subtotal).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.subtotal).ToList();
                    break;
                case "ivaDataGridViewTextBoxColumn1":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.iva).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.iva).ToList();
                    break;
                case "totalDataGridViewTextBoxColumn1":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.total).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.total).ToList();
                    break;
                case "fechacreacionDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.fecha_creacion).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.fecha_creacion).ToList();
                    break;
                case "fechavencidoDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.fecha_vencido).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.fecha_vencido).ToList();
                    break;
                case "facturaDataGridTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.FacturaTMP).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.FacturaTMP).ToList();
                    break;
                case "estadoDataGridViewTextBoxColumn":
                    if (_strdireccion == "Ascending")
                        _Cuentas = _Cuentas.OrderBy(o => o.Estado).ToList();
                    else
                        _Cuentas = _Cuentas.OrderByDescending(o => o.Estado).ToList();
                    break;
                default:
                    break;
            }

        }

        private void DGVdetalles_sortedcompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            throw new NotImplementedException();
        }

        void CboEstadoProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {
           // CambiaEstado();
        }
        private void CambiaEstado()
        {
            Cursor.Current = Cursors.WaitCursor;
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            var cuentas = _ControladorCxP.TraeCuentasPorEstado(txtFiltro.Text, ObtenerSeleccionCB(), dpFechaCreacionini.Value, dpfechacracionfin.Value, dpFechaVencimientoIni.Value, dpfechavencimientofin.Value, _EsbuquedaFechaCreacion, _Esbusquedafechafin);
            _Cuentas = cuentas;
            BScuentas.DataSource = new BuscadorBindingList<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar>(cuentas);
            BScuentas.ResetCurrentItem();

            var result = cuentas.Sum(m => m.total);

            ltotaldetalle.Text = string.Format("{0:C}", result);
            Cursor.Current = Cursors.Default;
        }

        private global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? ObtenerSeleccionCB()
        {
            return (CBO_estado.SelectedItem as Transversal.Dtos.DtoEnum).Valor as global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados?;
        }
        void CboEstadoProveedor_LostFocus(object sender, EventArgs e)
        {
           // CambiaEstado();
        }

       

        void CBO_estado_LostFocus(object sender, EventArgs e)
        {
            //CambiaEstado();
        }

        void CxP_principal_Activated(object sender, EventArgs e)
        {
#if DEBUG
#warning El retorno solamente debe ser efectivo en Debug
            Debug.WriteLine("Recarga en activated suprimida en modo debug porque dificulta un poco la depuración del form");
            return;
#endif
            Cargar();
        }

        void DGVdetalles_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            string estado = DGVdetalles.Rows[e.RowIndex].Cells["estadoDataGridViewTextBoxColumn"].Value.ToString();
            Bgrabar.Enabled = (estado == "PENDIENTE" ? true : false);
            //var result = DGVdetalles.Rows[e.RowIndex].Cells["totalDataGridViewTextBoxColumn1"].Value;
            //ltotaldetalle.Text = string.Format("{0:C}", result);
        }

        void DGVdetalles_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DGVdetalles.CurrentRow == null || Bgrabar.Enabled==false )
            {
                return; 
            }
            verdetalles();
        }

        public void Cargar(global::Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados? estado = null)
        {
            Cursor.Current = Cursors.WaitCursor;
            var _ControladorCxP = new SOTControladores.Controladores.ControladorCuentasPorPagar();

            DGVdetalles.DataSource = null;

            decimal result = 0;


            //BScuentas.DataSource = _ControladorCxP.TraeCuentasPorEstado(null);
            CambiaEstado();
            DGVdetalles.DataSource = BScuentas;
            //result = 0;
            //if (DGVdetalles.CurrentRow != null)
            //{
            //    result = Convert.ToDecimal(DGVdetalles.CurrentRow.Cells["totalDataGridViewTextBoxColumn1"].Value);
            //}
            //ltotaldetalle.Text = string.Format("{0:C}", result);
            Cursor.Current = Cursors.Default;

        }
        private void CxP_principal_Load(object sender, EventArgs e)
        {
            CBO_estado.SelectedIndex = 0;
            //Cargar();
        }

        private void DGVdetalles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            //var result=DGVdetalles.Rows[e.RowIndex].Cells["totalDataGridViewTextBoxColumn1"].Value;
            //ltotaldetalle.Text = string.Format("{0:C}", result);
        }

        private void verdetalles()
        {
            if (DGVdetalles.CurrentRow == null)
                return;
            //Bgrabar.Enabled = true;
            int cod_cta = Convert.ToInt32(DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[cod_cuenta_pago.DisplayIndex].Value);
            int cod_oc = Convert.ToInt32(DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[codOCDataGridViewTextBoxColumn.DisplayIndex].Value);
            bool _Espendiente = false;
            string estado = DGVdetalles.Rows[DGVdetalles.CurrentRow.Index].Cells[estadoDataGridViewTextBoxColumn.DisplayIndex].Value.ToString();
            if (estado == "PENDIENTE")
                _Espendiente = true;

            detalles = new CxPDetallado(_cnn, _cod_usu, _cod_folio, cod_cta, cod_oc, Convert.ToDecimal(DGVdetalles.CurrentRow.Cells["totalDataGridViewTextBoxColumn1"].Value), false, _Espendiente)
            {
                CxPprincipal = this,
                MdiParent = this.MdiParent
            };
            detalles.Show();
        }

        private void Bgrabar_Click(object sender, EventArgs e)
        {
            verdetalles();
        }
        public void ModificarProveedor(int cod_prov)
        {
            cambiaproveedor(cod_prov);
            detalles.ActualizaProveedor(cod_prov);
           
        }
        private void CBO_estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CambiaEstado();
        }

        private void DGVdetalles_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (Convert.ToBoolean(DGVdetalles.Rows[e.RowIndex].Cells[EsVencidaColumn.Index].Value))
            {
                DGVdetalles.Rows[e.RowIndex].DefaultCellStyle.BackColor = (Color)colorConverter.ConvertFromString("#f75151");
           
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Cargar(ObtenerSeleccionCB());
        }

        private void DateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            //fechavencimientofin

            dpFechaVencimientoIni.Value= Convert.ToDateTime(dpFechaVencimientoIni.Value.ToString("yyyy-MM-dd"));
            //CambiaEstado();
        }

        private void DpFechaCreacion_ValueChanged(object sender, EventArgs e)
        {
            //Fecha Inicial creacion
            dpFechaCreacionini.Value = Convert.ToDateTime(dpFechaCreacionini.Value.ToString("yyyy-MM-dd"));
           // CambiaEstado();

        }

        private void Label6_Click(object sender, EventArgs e)
        {

        }

        private void Dpfechacracionfin_ValueChanged(object sender, EventArgs e)
        {

            DateTime _fechafinal;
            _fechafinal = Convert.ToDateTime(dpfechacracionfin.Value.ToString("yyyy-MM-dd"));
            _fechafinal = _fechafinal.AddDays(1).Date.AddSeconds(-1);
            dpfechacracionfin.Value = _fechafinal;
           // CambiaEstado();
        }

        private void Dpfechavencimientofin_ValueChanged(object sender, EventArgs e)
        {
            DateTime _fechafinal;
            _fechafinal = Convert.ToDateTime(dpfechavencimientofin.Value.ToString("yyyy-MM-dd"));
            _fechafinal = _fechafinal.AddDays(1).Date.AddSeconds(-1);
            dpfechavencimientofin.Value = _fechafinal;
            //CambiaEstado();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //Generaciondereporte
            if (_Cuentas!= null && _Cuentas.Count > 0)
            {
                var controladorGlobal = new SOTControladores.Controladores.ControladorConfiguracionGlobal();
                var config = controladorGlobal.ObtenerConfiguracionGlobal();

                List<DtosReporteCabecero> Encabezado = new List<DtosReporteCabecero>();
                DtosReporteCabecero DatosCabecera = new DtosReporteCabecero();
                DatosCabecera.Hotel = config.Nombre;
                DatosCabecera.FechaInicioCreacion = dpFechaCreacionini.Value;
                DatosCabecera.FechaFinCreacion = dpfechacracionfin.Value;
                DatosCabecera.FechaIniciovencimiento = dpFechaVencimientoIni.Value;
                DatosCabecera.FechaFinvencimiento = dpfechavencimientofin.Value;
                DatosCabecera.Estadocuenta = CBO_estado.Text.ToString();
                DatosCabecera.Direccion = config.Direccion;
                DatosCabecera.Usuario = SOTControladores.Controladores.ControladorBase.UsuarioActual.NombreCompleto;
                Encabezado.Add(DatosCabecera);

                var documentoReporte = new ResTotal.ReportesRT.CuentasPorPagar.CxP_PorEstado();
                documentoReporte.Load();
                //Encabezado
                documentoReporte.SetDataSource(Encabezado);
                //Detalles
                ColoreaVencidas(_Cuentas);
                documentoReporte.Subreports["CuentasPorPagas"].SetDataSource(LlenaDatosReporte(_Cuentas));
                //PiedePagina(Totales)
                documentoReporte.Subreports["Totales"].SetDataSource(LlenaTotal());

                CrystalDecisions.Windows.Forms.CrystalReportViewer reporteform = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
                reporteform.ReportSource = documentoReporte;
                ReporteViewer reporte = new ReporteViewer(reporteform);
                reporte.Show();
            }
            else
            {
                 MessageBox.Show("Elija un filtro ó Busque todos los proveedores", "Aviso", MessageBoxButtons.OK);
                return;
            }

        }

        private List<DtosReporteCuentasPorPagar> LlenaDatosReporte(List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar> cuentasporpagar)
        {
            List<DtosReporteCuentasPorPagar> _ListDatosCuentasPorPagar = new List<DtosReporteCuentasPorPagar>();
            foreach (global::Modelo.Almacen.Entidades.ZctEncCuentasPagar item in cuentasporpagar)
            {
                DtosReporteCuentasPorPagar Cxpagar = new DtosReporteCuentasPorPagar();
                Cxpagar.NombreDelProveedor = item.NombreProveedorTMP;
                Cxpagar.CodigoordenCompra = item.Cod_OC;
                Cxpagar.Subtotal = item.subtotal;
                Cxpagar.Total = item.total;
                Cxpagar.Iva = item.iva;
                Cxpagar.Fecha_Creacion = item.fecha_creacion;
                Cxpagar.Fecha_Vencimiento = item.fecha_vencido;
                Cxpagar.NumeroFactura = item.FacturaTMP;
                Cxpagar.Estado = item.Estado.ToString();
                Cxpagar.EsVencida = item.EsVencida;
                _ListDatosCuentasPorPagar.Add(Cxpagar);
            }
            return _ListDatosCuentasPorPagar;

        }

        private List<DtosReportePieTotales> LlenaTotal()
        {
            List<DtosReportePieTotales> respuesta = new List<DtosReportePieTotales>();
            DtosReportePieTotales dato = new DtosReportePieTotales();
            dato.Total = ltotaldetalle.Text.ToString();
            respuesta.Add(dato);
            return respuesta;
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                _EsbuquedaFechaCreacion = true;
                dpFechaCreacionini.Enabled = true;
                dpfechacracionfin.Enabled = true;

            }
            else 
            {
                _EsbuquedaFechaCreacion = false;
                dpFechaCreacionini.Enabled = false;
                dpfechacracionfin.Enabled = false;
                var fechaFinal = DateTime.Now;
                dpFechaCreacionini.Value = fechaFinal;
                dpfechacracionfin.Value = fechaFinal;
            }

            //Cargar(ObtenerSeleccionCB());
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                _Esbusquedafechafin = true;
                dpfechavencimientofin.Enabled = true;
                dpFechaVencimientoIni.Enabled = true;

            }
            else
            {
                _Esbusquedafechafin = false;
                dpfechavencimientofin.Enabled = false;
                dpFechaVencimientoIni.Enabled = false;
                var fechaFinal = DateTime.Now;
                dpFechaVencimientoIni.Value = fechaFinal;
                dpfechavencimientofin.Value = fechaFinal;
            }
            //Cargar(ObtenerSeleccionCB());

        }

        private void ColoreaVencidas(List<global::Modelo.Almacen.Entidades.ZctEncCuentasPagar> listaacolorear)
        {

        }

        private void Label8_Click(object sender, EventArgs e)
        {

        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void Label10_Click(object sender, EventArgs e)
        {

        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void Label11_Click(object sender, EventArgs e)
        {

        }
    }
}
