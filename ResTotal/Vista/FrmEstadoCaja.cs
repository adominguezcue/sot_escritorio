﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using SOTControladores.Controladores;

namespace ResTotal.Vista
{
    public partial class FrmEstadoCaja : Form
    {
        private global::Modelo.Almacen.Entidades.ZctCatPar parametros;
        decimal _Exedente = 0;
        private ControladorRetirosCaja _ControladorRetiroCaja;
        private ResTotal.Controlador.Controladorcaja _ControladorCaja;
        public string FolioCaja = "";
        int _user;
        string _foliocaja;
        public FrmEstadoCaja( string cnn, int usuario,string FolioCaja)
        {
            InitializeComponent();
            _user = usuario ;
            _foliocaja = FolioCaja;
            _ControladorRetiroCaja = new ControladorRetirosCaja();//Controlador.ControladorRetiroCaja(cnn);
            _ControladorCaja = new Controlador.Controladorcaja(cnn, FolioCaja,usuario);
            var controladorG = new ControladorConfiguracionGlobal();

            parametros = controladorG.ObtenerConfiguracionGlobal();
        }

        void Tretiro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != 46))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void FrmRetiroCaja_Load(object sender, EventArgs e)
        {
            ActualizaLabels();
            var controladorG = new ControladorConfiguracionGlobal();

            parametros = controladorG.ObtenerConfiguracionGlobal();
        }
        private void ActualizaLabels()
        {
            var controladorG = new ControladorConfiguracionGlobal();

            parametros = controladorG.ObtenerConfiguracionGlobal();
            decimal _Maximo=Decimal.Round( Convert.ToDecimal (parametros.maximo_caja),2);
            decimal _Total=Decimal.Round(_ControladorCaja.TraerTotalCaja(_foliocaja,_user),2);
            _Exedente = _Total-_Maximo ;

    
            Lminimo.Text =  parametros.minimo_caja.ToString();
            Lmaximo.Text = String.Format("{0:C}", _Maximo);
            Ltotal.Text = String.Format("{0:C}", _Total);
            Lexedente.Text = String.Format("{0:C}", _Exedente > 0 ? _Exedente:0);
            Lminimo.Text = String.Format("{0:C}", parametros.minimo_caja);
            Lmaximo.Text = String.Format("{0:C}", parametros.maximo_caja);
            Lretiros.Text =String.Format("{0:C}", _ControladorRetiroCaja.TraeTotalRetirosCaja(_foliocaja));
        }

        //private void Bretirar_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //if (Convert.ToDecimal(Tretiro.Text) < _Exedente)
        //        //{
        //        //    Tretiro.Focus();
        //        //    throw new Exception("El monto a retirar no debe ser menor al monto exedente en caja");

        //        //}
        //        //if ( _ControladorCaja.TraerTotalCaja("CJ")-Convert.ToDecimal(Tretiro.Text) < parametros.minimo_caja )
        //        //{
        //        //    Tretiro.Focus();
        //        //    throw new Exception("El monto de la caja no debe ser menor al minimo permitido en caja");

        //        //}
        //        int FolioRetiro= _ControladorRetiroCaja.RetiroCaja("CJ", Convert.ToDecimal(Tretiro.Text), _user );
        //        MessageBox.Show("Retiro de caja realizado", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        this.DialogResult = DialogResult.OK;
        //        UseWaitCursor = true;
        //        this.DialogResult = DialogResult.OK;
        //        System.Reflection.Assembly myDllAssembly = System.Reflection.Assembly.LoadFile(Application.StartupPath + "\\ZctSOT.exe");
        //        System.Type MyDLLFormType = myDllAssembly.GetType("ZctSOT.PkVisorRpt");
        //        Form visor = (Form)myDllAssembly.CreateInstance("ZctSOT.PkVisorRpt", false);
        //        FieldInfo prop = MyDLLFormType.GetField("sRpt");
        //        prop.SetValue(visor, "c:\\Reportes\\rptRetirosCajas.rpt");
        //        prop = MyDLLFormType.GetField("sSQLV");
        //        prop.SetValue(visor, "{ZctRetirosCajas.cod_retiro} = " + Convert.ToInt32(FolioRetiro));
        //        visor.WindowState = FormWindowState.Maximized;
        //        visor.ShowDialog(this);
        //        UseWaitCursor = false;
        //        //-------------------
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Error realizando retiro en caja: " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        //    }
        //}

      

        private void Bcerrar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
