﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ResTotal.Vista
{
    public partial class FrmEditaArticuloCosto : Form
    {
        private Modelo.ZctDetCaja _detalle_caja = new Modelo.ZctDetCaja();
        decimal costo = 0;
        public FrmEditaArticuloCosto(Modelo.ZctDetCaja detalle_caja)
        {
            InitializeComponent();
            costoArticuloOTTextBox.KeyPress += new KeyPressEventHandler(costoArticuloOTTextBox_KeyPress);
            this.desc_ArtLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zctDetCajaBindingSource, "ZctCatArt.Desc_Art", true));
            _detalle_caja = detalle_caja;
            _detalle_caja = detalle_caja;
            costo = detalle_caja.CostoArticuloOT.GetValueOrDefault();
        }

        void costoArticuloOTTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
                    {
                        e.Handled = true;
                    }
        }

        private void FrmEditaArticuloCosto_Load(object sender, EventArgs e)
        {
            zctDetCajaBindingSource.DataSource = _detalle_caja;
        }

        private void Blimpiar_Click(object sender, EventArgs e)
        {
            _detalle_caja.CostoArticuloOT = costo;
        }

        private void Bgrabar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
