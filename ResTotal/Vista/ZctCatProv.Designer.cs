﻿using System.Windows.Forms;
using ModeloAlmacen = global::Modelo.Almacen;

namespace ResTotal.Vista
{
    partial class ZctCatProv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label cod_ProvLabel;
            System.Windows.Forms.Label nom_ProvLabel;
            System.Windows.Forms.Label calleLabel;
            System.Windows.Forms.Label coloniaLabel;
            System.Windows.Forms.Label numextLabel;
            System.Windows.Forms.Label numintLabel;
            System.Windows.Forms.Label cpLabel;
            System.Windows.Forms.Label telefonoLabel;
            System.Windows.Forms.Label rFC_ProvLabel;
            System.Windows.Forms.Label emailLabel;
            System.Windows.Forms.Label ciudadLabel1;
            System.Windows.Forms.Label estadoLabel;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label lpagos;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label4;
            this.ZctCatProvBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cod_ProvTextBox = new System.Windows.Forms.TextBox();
            this.nom_ProvTextBox = new System.Windows.Forms.TextBox();
            this.calleTextBox = new System.Windows.Forms.TextBox();
            this.coloniaTextBox = new System.Windows.Forms.TextBox();
            this.numextTextBox = new System.Windows.Forms.TextBox();
            this.TxtFrecPagos = new System.Windows.Forms.TextBox();
            this.cpTextBox = new System.Windows.Forms.TextBox();
            this.telefonoTextBox = new System.Windows.Forms.TextBox();
            this.rFC_ProvTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.Beliminar = new System.Windows.Forms.Button();
            this.Bguardar = new System.Windows.Forms.Button();
            this.Bcancelar = new System.Windows.Forms.Button();
            this.Bsig = new System.Windows.Forms.Button();
            this.Bant = new System.Windows.Forms.Button();
            this.BsCiudad = new System.Windows.Forms.BindingSource(this.components);
            this.BSestado = new System.Windows.Forms.BindingSource(this.components);
            this.TxtNumPagos = new System.Windows.Forms.TextBox();
            this.TxtNumInt = new System.Windows.Forms.TextBox();
            this.TxtdiasCred = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCiudad = new System.Windows.Forms.TextBox();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.txtContacto = new System.Windows.Forms.TextBox();
            this.txtNombreComercial = new System.Windows.Forms.TextBox();
            cod_ProvLabel = new System.Windows.Forms.Label();
            nom_ProvLabel = new System.Windows.Forms.Label();
            calleLabel = new System.Windows.Forms.Label();
            coloniaLabel = new System.Windows.Forms.Label();
            numextLabel = new System.Windows.Forms.Label();
            numintLabel = new System.Windows.Forms.Label();
            cpLabel = new System.Windows.Forms.Label();
            telefonoLabel = new System.Windows.Forms.Label();
            rFC_ProvLabel = new System.Windows.Forms.Label();
            emailLabel = new System.Windows.Forms.Label();
            ciudadLabel1 = new System.Windows.Forms.Label();
            estadoLabel = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            lpagos = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ZctCatProvBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsCiudad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSestado)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cod_ProvLabel
            // 
            cod_ProvLabel.AutoSize = true;
            cod_ProvLabel.Location = new System.Drawing.Point(12, 18);
            cod_ProvLabel.Name = "cod_ProvLabel";
            cod_ProvLabel.Size = new System.Drawing.Size(43, 13);
            cod_ProvLabel.TabIndex = 0;
            cod_ProvLabel.Text = "Codigo:";
            // 
            // nom_ProvLabel
            // 
            nom_ProvLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            nom_ProvLabel.AutoSize = true;
            nom_ProvLabel.Location = new System.Drawing.Point(27, 6);
            nom_ProvLabel.Name = "nom_ProvLabel";
            nom_ProvLabel.Size = new System.Drawing.Size(81, 13);
            nom_ProvLabel.TabIndex = 0;
            nom_ProvLabel.Text = "Razón social(*):";
            // 
            // calleLabel
            // 
            calleLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            calleLabel.AutoSize = true;
            calleLabel.Location = new System.Drawing.Point(65, 84);
            calleLabel.Name = "calleLabel";
            calleLabel.Size = new System.Drawing.Size(43, 13);
            calleLabel.TabIndex = 6;
            calleLabel.Text = "Calle(*):";
            // 
            // coloniaLabel
            // 
            coloniaLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            coloniaLabel.AutoSize = true;
            coloniaLabel.Location = new System.Drawing.Point(431, 84);
            coloniaLabel.Name = "coloniaLabel";
            coloniaLabel.Size = new System.Drawing.Size(55, 13);
            coloniaLabel.TabIndex = 8;
            coloniaLabel.Text = "Colonia(*):";
            // 
            // numextLabel
            // 
            numextLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            numextLabel.AutoSize = true;
            numextLabel.Location = new System.Drawing.Point(14, 136);
            numextLabel.Name = "numextLabel";
            numextLabel.Size = new System.Drawing.Size(94, 13);
            numextLabel.TabIndex = 14;
            numextLabel.Text = "Número exterior(*):";
            // 
            // numintLabel
            // 
            numintLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            numintLabel.AutoSize = true;
            numintLabel.Location = new System.Drawing.Point(405, 136);
            numintLabel.Name = "numintLabel";
            numintLabel.Size = new System.Drawing.Size(81, 13);
            numintLabel.TabIndex = 16;
            numintLabel.Text = "Número interior:";
            // 
            // cpLabel
            // 
            cpLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            cpLabel.AutoSize = true;
            cpLabel.Location = new System.Drawing.Point(74, 110);
            cpLabel.Name = "cpLabel";
            cpLabel.Size = new System.Drawing.Size(34, 13);
            cpLabel.TabIndex = 10;
            cpLabel.Text = "CP(*):";
            // 
            // telefonoLabel
            // 
            telefonoLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            telefonoLabel.AutoSize = true;
            telefonoLabel.Location = new System.Drawing.Point(424, 110);
            telefonoLabel.Name = "telefonoLabel";
            telefonoLabel.Size = new System.Drawing.Size(62, 13);
            telefonoLabel.TabIndex = 12;
            telefonoLabel.Text = "Teléfono(*):";
            // 
            // rFC_ProvLabel
            // 
            rFC_ProvLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            rFC_ProvLabel.AutoSize = true;
            rFC_ProvLabel.Location = new System.Drawing.Point(67, 58);
            rFC_ProvLabel.Name = "rFC_ProvLabel";
            rFC_ProvLabel.Size = new System.Drawing.Size(41, 13);
            rFC_ProvLabel.TabIndex = 2;
            rFC_ProvLabel.Text = "RFC(*):";
            // 
            // emailLabel
            // 
            emailLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            emailLabel.AutoSize = true;
            emailLabel.Location = new System.Drawing.Point(380, 58);
            emailLabel.Name = "emailLabel";
            emailLabel.Size = new System.Drawing.Size(106, 13);
            emailLabel.TabIndex = 4;
            emailLabel.Text = "Correo electrónico(*):";
            // 
            // ciudadLabel1
            // 
            ciudadLabel1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            ciudadLabel1.AutoSize = true;
            ciudadLabel1.Location = new System.Drawing.Point(433, 162);
            ciudadLabel1.Name = "ciudadLabel1";
            ciudadLabel1.Size = new System.Drawing.Size(53, 13);
            ciudadLabel1.TabIndex = 20;
            ciudadLabel1.Text = "Ciudad(*):";
            // 
            // estadoLabel
            // 
            estadoLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            estadoLabel.AutoSize = true;
            estadoLabel.Location = new System.Drawing.Point(55, 162);
            estadoLabel.Name = "estadoLabel";
            estadoLabel.Size = new System.Drawing.Size(53, 13);
            estadoLabel.TabIndex = 18;
            estadoLabel.Text = "Estado(*):";
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(25, 189);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(83, 13);
            label1.TabIndex = 22;
            label1.Text = "Días de crédito:";
            // 
            // lpagos
            // 
            lpagos.AutoSize = true;
            lpagos.Location = new System.Drawing.Point(238, 183);
            lpagos.Name = "lpagos";
            lpagos.Size = new System.Drawing.Size(94, 13);
            lpagos.TabIndex = 24;
            lpagos.Text = "Número de pagos:";
            lpagos.Visible = false;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(470, 183);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(152, 13);
            label3.TabIndex = 26;
            label3.Text = "Frecuencia de  pagos en días:";
            label3.Visible = false;
            // 
            // label2
            // 
            label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(3, 32);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(105, 13);
            label2.TabIndex = 24;
            label2.Text = "Nombre comercial(*):";
            // 
            // label4
            // 
            label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(423, 189);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(63, 13);
            label4.TabIndex = 23;
            label4.Text = "Contacto(*):";
            // 
            // ZctCatProvBindingSource
            // 
            this.ZctCatProvBindingSource.DataSource = typeof(global::Modelo.Almacen.Entidades.Proveedor);
            this.ZctCatProvBindingSource.Filter = "";
            // 
            // cod_ProvTextBox
            // 
            this.cod_ProvTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "Cod_Prov", true));
            this.cod_ProvTextBox.Location = new System.Drawing.Point(55, 15);
            this.cod_ProvTextBox.Name = "cod_ProvTextBox";
            this.cod_ProvTextBox.Size = new System.Drawing.Size(68, 20);
            this.cod_ProvTextBox.TabIndex = 0;
            this.cod_ProvTextBox.TextChanged += new System.EventHandler(this.cod_ProvTextBox_TextChanged);
            this.cod_ProvTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cod_ProvTextBox_KeyDown);
            // 
            // nom_ProvTextBox
            // 
            this.nom_ProvTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.nom_ProvTextBox, 3);
            this.nom_ProvTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "Nom_Prov", true));
            this.nom_ProvTextBox.Location = new System.Drawing.Point(114, 3);
            this.nom_ProvTextBox.Name = "nom_ProvTextBox";
            this.nom_ProvTextBox.Size = new System.Drawing.Size(639, 20);
            this.nom_ProvTextBox.TabIndex = 0;
            // 
            // calleTextBox
            // 
            this.calleTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.calleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "calle", true));
            this.calleTextBox.Location = new System.Drawing.Point(114, 81);
            this.calleTextBox.Name = "calleTextBox";
            this.calleTextBox.Size = new System.Drawing.Size(214, 20);
            this.calleTextBox.TabIndex = 4;
            // 
            // coloniaTextBox
            // 
            this.coloniaTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.coloniaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "colonia", true));
            this.coloniaTextBox.Location = new System.Drawing.Point(492, 81);
            this.coloniaTextBox.Name = "coloniaTextBox";
            this.coloniaTextBox.Size = new System.Drawing.Size(261, 20);
            this.coloniaTextBox.TabIndex = 5;
            // 
            // numextTextBox
            // 
            this.numextTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.numextTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "numext", true));
            this.numextTextBox.Location = new System.Drawing.Point(114, 133);
            this.numextTextBox.Name = "numextTextBox";
            this.numextTextBox.Size = new System.Drawing.Size(96, 20);
            this.numextTextBox.TabIndex = 8;
            // 
            // TxtFrecPagos
            // 
            this.TxtFrecPagos.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "frecuencia_pagos", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "1"));
            this.TxtFrecPagos.Location = new System.Drawing.Point(631, 179);
            this.TxtFrecPagos.Name = "TxtFrecPagos";
            this.TxtFrecPagos.Size = new System.Drawing.Size(83, 20);
            this.TxtFrecPagos.TabIndex = 27;
            this.TxtFrecPagos.Visible = false;
            // 
            // cpTextBox
            // 
            this.cpTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cpTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "cp", true));
            this.cpTextBox.Location = new System.Drawing.Point(114, 107);
            this.cpTextBox.Name = "cpTextBox";
            this.cpTextBox.Size = new System.Drawing.Size(136, 20);
            this.cpTextBox.TabIndex = 6;
            // 
            // telefonoTextBox
            // 
            this.telefonoTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.telefonoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "telefono", true));
            this.telefonoTextBox.Location = new System.Drawing.Point(492, 107);
            this.telefonoTextBox.Name = "telefonoTextBox";
            this.telefonoTextBox.Size = new System.Drawing.Size(136, 20);
            this.telefonoTextBox.TabIndex = 7;
            this.telefonoTextBox.TextChanged += new System.EventHandler(this.telefonoTextBox_TextChanged);
            this.telefonoTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.telefonoTextBox_KeyPress);
            // 
            // rFC_ProvTextBox
            // 
            this.rFC_ProvTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.rFC_ProvTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.rFC_ProvTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "RFC_Prov", true));
            this.rFC_ProvTextBox.Location = new System.Drawing.Point(114, 55);
            this.rFC_ProvTextBox.Name = "rFC_ProvTextBox";
            this.rFC_ProvTextBox.Size = new System.Drawing.Size(214, 20);
            this.rFC_ProvTextBox.TabIndex = 2;
            // 
            // emailTextBox
            // 
            this.emailTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.emailTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "email", true));
            this.emailTextBox.Location = new System.Drawing.Point(492, 55);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(261, 20);
            this.emailTextBox.TabIndex = 3;
            // 
            // Beliminar
            // 
            this.Beliminar.Location = new System.Drawing.Point(519, 19);
            this.Beliminar.Name = "Beliminar";
            this.Beliminar.Size = new System.Drawing.Size(75, 23);
            this.Beliminar.TabIndex = 0;
            this.Beliminar.Text = "Eliminar";
            this.Beliminar.UseVisualStyleBackColor = true;
            this.Beliminar.Click += new System.EventHandler(this.Beliminar_Click);
            // 
            // Bguardar
            // 
            this.Bguardar.Location = new System.Drawing.Point(605, 19);
            this.Bguardar.Name = "Bguardar";
            this.Bguardar.Size = new System.Drawing.Size(75, 23);
            this.Bguardar.TabIndex = 1;
            this.Bguardar.Text = "Guardar";
            this.Bguardar.UseVisualStyleBackColor = true;
            this.Bguardar.Click += new System.EventHandler(this.Bguardar_Click);
            // 
            // Bcancelar
            // 
            this.Bcancelar.Location = new System.Drawing.Point(687, 19);
            this.Bcancelar.Name = "Bcancelar";
            this.Bcancelar.Size = new System.Drawing.Size(75, 23);
            this.Bcancelar.TabIndex = 2;
            this.Bcancelar.Text = "Cancelar";
            this.Bcancelar.UseVisualStyleBackColor = true;
            this.Bcancelar.Click += new System.EventHandler(this.Bcancelar_Click);
            // 
            // Bsig
            // 
            this.Bsig.Location = new System.Drawing.Point(146, 15);
            this.Bsig.Name = "Bsig";
            this.Bsig.Size = new System.Drawing.Size(20, 21);
            this.Bsig.TabIndex = 2;
            this.Bsig.Text = ">";
            this.Bsig.UseVisualStyleBackColor = true;
            this.Bsig.Click += new System.EventHandler(this.Bsig_Click);
            // 
            // Bant
            // 
            this.Bant.Location = new System.Drawing.Point(125, 15);
            this.Bant.Name = "Bant";
            this.Bant.Size = new System.Drawing.Size(20, 21);
            this.Bant.TabIndex = 1;
            this.Bant.Text = "<";
            this.Bant.UseVisualStyleBackColor = true;
            this.Bant.Click += new System.EventHandler(this.Bant_Click);
            // 
            // BsCiudad
            // 
            this.BsCiudad.DataSource = typeof(global::Modelo.Almacen.Entidades.Ciudad);
            this.BsCiudad.Filter = "";
            // 
            // BSestado
            // 
            this.BSestado.DataSource = typeof(global::Modelo.Almacen.Entidades.Estado);
            this.BSestado.Filter = "";
            // 
            // TxtNumPagos
            // 
            this.TxtNumPagos.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "numero_pagos", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0"));
            this.TxtNumPagos.Location = new System.Drawing.Point(336, 179);
            this.TxtNumPagos.Name = "TxtNumPagos";
            this.TxtNumPagos.Size = new System.Drawing.Size(95, 20);
            this.TxtNumPagos.TabIndex = 25;
            this.TxtNumPagos.Visible = false;
            this.TxtNumPagos.TextChanged += new System.EventHandler(this.TxtNumPagos_TextChanged);
            // 
            // TxtNumInt
            // 
            this.TxtNumInt.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TxtNumInt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "numint", true));
            this.TxtNumInt.Location = new System.Drawing.Point(492, 133);
            this.TxtNumInt.Name = "TxtNumInt";
            this.TxtNumInt.Size = new System.Drawing.Size(96, 20);
            this.TxtNumInt.TabIndex = 9;
            // 
            // TxtdiasCred
            // 
            this.TxtdiasCred.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.TxtdiasCred.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "dias_credito", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "0"));
            this.TxtdiasCred.Location = new System.Drawing.Point(114, 185);
            this.TxtdiasCred.Name = "TxtdiasCred";
            this.TxtdiasCred.Size = new System.Drawing.Size(96, 20);
            this.TxtdiasCred.TabIndex = 12;
            this.TxtdiasCred.TextChanged += new System.EventHandler(this.telefonoTextBox_TextChanged);
            this.TxtdiasCred.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.telefonoTextBox_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Beliminar);
            this.groupBox1.Controls.Add(this.Bguardar);
            this.groupBox1.Controls.Add(this.Bcancelar);
            this.groupBox1.Location = new System.Drawing.Point(6, 289);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(769, 60);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel1);
            this.groupBox2.Controls.Add(this.TxtFrecPagos);
            this.groupBox2.Controls.Add(label3);
            this.groupBox2.Controls.Add(this.TxtNumPagos);
            this.groupBox2.Controls.Add(lpagos);
            this.groupBox2.Location = new System.Drawing.Point(6, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(769, 242);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtCiudad, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtEstado, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtContacto, 3, 7);
            this.tableLayoutPanel1.Controls.Add(label4, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.TxtNumInt, 3, 5);
            this.tableLayoutPanel1.Controls.Add(numextLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.emailTextBox, 3, 2);
            this.tableLayoutPanel1.Controls.Add(ciudadLabel1, 2, 6);
            this.tableLayoutPanel1.Controls.Add(emailLabel, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.coloniaTextBox, 3, 3);
            this.tableLayoutPanel1.Controls.Add(cpLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.nom_ProvTextBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(numintLabel, 2, 5);
            this.tableLayoutPanel1.Controls.Add(telefonoLabel, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.telefonoTextBox, 3, 4);
            this.tableLayoutPanel1.Controls.Add(label1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(calleLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(rFC_ProvLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.rFC_ProvTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(estadoLabel, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.TxtdiasCred, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.numextTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.cpTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.calleTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(coloniaLabel, 2, 3);
            this.tableLayoutPanel1.Controls.Add(nom_ProvLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtNombreComercial, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(756, 209);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // txtCiudad
            // 
            this.txtCiudad.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtCiudad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtCiudad.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCiudad.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "ciudad", true));
            this.txtCiudad.Location = new System.Drawing.Point(492, 159);
            this.txtCiudad.Name = "txtCiudad";
            this.txtCiudad.Size = new System.Drawing.Size(136, 20);
            this.txtCiudad.TabIndex = 11;
            // 
            // txtEstado
            // 
            this.txtEstado.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtEstado.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtEstado.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtEstado.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "estado", true));
            this.txtEstado.Location = new System.Drawing.Point(114, 159);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(136, 20);
            this.txtEstado.TabIndex = 10;
            // 
            // txtContacto
            // 
            this.txtContacto.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtContacto.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "Contacto", true));
            this.txtContacto.Location = new System.Drawing.Point(492, 185);
            this.txtContacto.Name = "txtContacto";
            this.txtContacto.Size = new System.Drawing.Size(260, 20);
            this.txtContacto.TabIndex = 13;
            // 
            // txtNombreComercial
            // 
            this.txtNombreComercial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.txtNombreComercial, 3);
            this.txtNombreComercial.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ZctCatProvBindingSource, "NombreComercial", true));
            this.txtNombreComercial.Location = new System.Drawing.Point(114, 29);
            this.txtNombreComercial.Name = "txtNombreComercial";
            this.txtNombreComercial.Size = new System.Drawing.Size(639, 20);
            this.txtNombreComercial.TabIndex = 1;
            // 
            // ZctCatProv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 356);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Bsig);
            this.Controls.Add(this.Bant);
            this.Controls.Add(cod_ProvLabel);
            this.Controls.Add(this.cod_ProvTextBox);
            this.Name = "ZctCatProv";
            this.Text = "Catálogo de Proveedores";
            this.Load += new System.EventHandler(this.ZctCatProv_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ZctCatProvBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BsCiudad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSestado)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

      
        #endregion

        private System.Windows.Forms.BindingSource ZctCatProvBindingSource;
        private System.Windows.Forms.TextBox cod_ProvTextBox;
        private System.Windows.Forms.TextBox nom_ProvTextBox;
        private System.Windows.Forms.TextBox calleTextBox;
        private System.Windows.Forms.TextBox coloniaTextBox;
        private System.Windows.Forms.TextBox numextTextBox;
        private System.Windows.Forms.TextBox TxtFrecPagos;
        private System.Windows.Forms.TextBox cpTextBox;
        private System.Windows.Forms.TextBox telefonoTextBox;
        private System.Windows.Forms.TextBox rFC_ProvTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Button Beliminar;
        private System.Windows.Forms.Button Bguardar;
        private System.Windows.Forms.Button Bcancelar;
        private System.Windows.Forms.Button Bsig;
        private System.Windows.Forms.Button Bant;
        private System.Windows.Forms.BindingSource BsCiudad;
        private System.Windows.Forms.BindingSource BSestado;
        private System.Windows.Forms.TextBox TxtNumPagos;
        private System.Windows.Forms.TextBox TxtNumInt;
        private System.Windows.Forms.TextBox TxtdiasCred;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private TableLayoutPanel tableLayoutPanel1;
        private TextBox txtNombreComercial;
        private TextBox txtContacto;
        private TextBox txtCiudad;
        private TextBox txtEstado;

    }
}