﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using SOTControladores.Controladores;

namespace ResTotal.Vista
{
    public partial class RetiroCaja : Form
    {
        public delegate void DimprimeRetiro(int folio_retiro);
        public event DimprimeRetiro ImprimeRetiro;
        private ControladorRetirosCaja _ControladorRetiroCaja;
        private ResTotal.Controlador.Controladorcaja _ControladorCaja;

        private string _Cnn, _Cod_Folio;
        private int _user;

        public RetiroCaja(string Cnn, int user, string Cod_Folio)
        {
            InitializeComponent();

            _ControladorCaja = new Controlador.Controladorcaja(Cnn, Cod_Folio, user);
            _ControladorRetiroCaja = new ControladorRetirosCaja();//Controlador.ControladorRetiroCaja(Cnn);
            _Cnn = Cnn;
            _Cod_Folio = Cod_Folio;
            _user = user;
            
        }

        private void Bretirar_Click(object sender, EventArgs e)
        {
            int FolioRetiro = _ControladorRetiroCaja.RetiroCaja(_Cod_Folio, Convert.ToDecimal(TxtMonto.Text));

            UseWaitCursor = true;
            this.DialogResult = DialogResult.OK;
            //System.Reflection.Assembly myDllAssembly = System.Reflection.Assembly.LoadFile(Application.StartupPath + "\\ZctSOT.exe");
            //System.Type MyDLLFormType = myDllAssembly.GetType("ZctSOT.PkVisorRpt");
            //Form visor = (Form)myDllAssembly.CreateInstance("ZctSOT.PkVisorRpt", false);
            //FieldInfo prop = MyDLLFormType.GetField("sRpt");
            //prop.SetValue(visor, "c:\\Reportes\\rptRetirosCajas.rpt");
            //prop = MyDLLFormType.GetField("sSQLV");
            //prop.SetValue(visor, "{ZctRetirosCajas.cod_retiro} = " + Convert.ToInt32(FolioRetiro));
            //visor.WindowState = FormWindowState.Maximized;
            //visor.ShowDialog(this);
            //UseWaitCursor = false;
            ImprimeRetiro(FolioRetiro) ;
            this.DialogResult = DialogResult.OK;
        }

        private void BcancelarRetiro_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void RetiroCaja_Load(object sender, EventArgs e)
        {
            var controladorG = new ControladorConfiguracionGlobal();
            var parametros = controladorG.ObtenerConfiguracionGlobal();

            //var parametros = _ControladorRetiroCaja.TraerParametros();
            decimal _Maximo = Decimal.Round(Convert.ToDecimal(parametros.maximo_caja), 2);
            decimal _Total = Decimal.Round(_ControladorCaja.TraerTotalCaja(_Cod_Folio,_user), 2);
            decimal _Minimo = Decimal.Round(Convert.ToDecimal(parametros.minimo_caja), 2);
            decimal _Retiro = _Total - _Minimo;
            TxtMonto.Text = _Retiro.ToString(); 
        }
    }
}
