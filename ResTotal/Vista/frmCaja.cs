﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using SOTControladores.Controladores;
namespace ResTotal.Vista
{
    public partial class frmCaja : Form
    {
        Boolean Edita;
        #region delaraciones
        //private ResTotal.Controlador.ControladorRetiroCaja _ControladorRetiroCaja;
        public Boolean   _PermisoCancelar;
        public delegate int ModificaOT(long cod_OT);
        public delegate int BuscaCliente();   
        public delegate int CreatOT();
        public delegate void DlanzaImprecion(string Cod_folio, int folio);
        public event DlanzaImprecion Lanzaimpresion;
        public delegate void DImpimeRetiro(int FolioRetiro);
        public delegate void DImpimeCorte(decimal fondocaja, string cod_folio, int cod_usu);
        public delegate object DLanzaBusqueda();
        public event BuscaCliente  LanzaBuscaDeCliente;
        public event ModificaOT LanzaOT;
        public event CreatOT CreaOT;
        public event ModificaCliente LanzaCliente;
        public delegate void Dautoriza(global::Modelo.Seguridad.Dtos.DtoPermisos permisos, frmCaja Padre);
        public event Dautoriza Autoriza;
        public event DImpimeRetiro ImprimeRetiro;
        public event DImpimeCorte ImprimeCorte;
        public event DLanzaBusqueda LanzaBusquedaArticulos;
        private int cajaabierta = 0;
        private ControladorRetirosCaja _ControladorRetiro;
        Permisos.Controlador.Controlador.vistapermisos _permisos;
        Permisos.Controlador.Controlador _ControladorPermisos;
        private ResTotal.Controlador.Controladorcaja _ControladorCaja;
        private int PosCaja;
        private string _Cnn = "";
        decimal _TotalDet = 0;
        decimal _TotalPago = 0;
        decimal _Cambio = 0;
        decimal _Subtotal = 0;
        decimal _iva = 0;
        int _Consecutivo = 0;
        int _user = 0;
        string _Cod_Folio;
        Modelo.ZctEncCaja CajaActual = new Modelo.ZctEncCaja();
        bool aviso_de_cierre = true;
        bool _pagando = false;
        int _cod_cte_default;
        #endregion
        public frmCaja(string cnn, int user, string Cod_Folio, int cod_cte_default)
        {
            _user = user;
            _Cnn = cnn;
            _Cod_Folio = Cod_Folio;
           _cod_cte_default = cod_cte_default;
            InitializeComponent();
            //_ControladorRetiroCaja = new Controlador.ControladorRetiroCaja(cnn);
            
            this.TxtFolio.LostFocus += new EventHandler(TxtPierdeElFoco);
            this.TxtPedido.LostFocus += new EventHandler(TxtPierdeElFoco);
            this.DGVpagos.CellEndEdit += new DataGridViewCellEventHandler(DGVpagos_CellEndEdit);
            this.DGVpagos.DataError += new DataGridViewDataErrorEventHandler(DGVpagos_DataError);
            this.DGVpagos.RowsRemoved += new DataGridViewRowsRemovedEventHandler(DGVpagos_RowsRemoved);
            this.FormClosing += new FormClosingEventHandler(frmCaja_FormClosing);
            this.TxtFolio.KeyPress += new KeyPressEventHandler(txtCaracter_KeyPress);
            this.TxtPedido.KeyPress += new KeyPressEventHandler(txtCaracter_KeyPress);
            this.TxtPedido.KeyDown += new KeyEventHandler(Control_KeyDown);
            this.TxtFolio.KeyDown += new KeyEventHandler(Control_KeyDown);
            this.DGVpagos.KeyDown += new KeyEventHandler(Control_KeyDown);
            TxtFolio.TextChanged += new EventHandler(TxtFolio_TextChanged);
        }

        public void habilita_botones(bool nuevo){
            txtCapturaArticulos.Enabled = nuevo;
            Bgrabar.Enabled = Edita; 
            //Bretiro
            //BcorteCaja
            cmdAplicaDescuento.Enabled = nuevo;
            cmdPagoTarjeta.Enabled = nuevo;
            cmdPagoEfectivo.Enabled = nuevo;
            cmdPagoParcial.Enabled = nuevo;
            BEditarCantidadArticulo.Enabled = nuevo;
            BeditarCostoArticulo.Enabled = nuevo;
            Bimprimir.Enabled = !nuevo;
            Blimpiar.Enabled = true;
            //BabreCaja
            //BestadoActualCaja
            Bcancelar.Enabled = !nuevo;
            cmdBuscaCliente.Enabled = nuevo;
            BcambiarCliente.Enabled = nuevo;
            
        }

        void TxtFolio_TextChanged(object sender, EventArgs e)
        {
            if (TxtFolio.Text == "")
            {
                TxtPedido.Text = "";
            }
            else if( Convert.ToInt32( TxtFolio.Text )<_Consecutivo )
            {
                TxtPedido.Text = _ControladorCaja.BuscaRecibo(_Cod_Folio, Convert.ToInt32(TxtFolio.Text)).CodEnc_OT.ToString();
            }
        }
        private void CompruebaCaja()
        {
            cajaabierta = _ControladorCaja.traeCajaAbiertaUsuario(_user);
            if (cajaabierta > 0)
            {
                BabreCaja.Enabled = false;
                BcorteCaja.Enabled = true;
            }
            else
            {
                BabreCaja.Enabled = true;
                BcorteCaja.Enabled = false;
            }
        }
        void DGVpagos_Click(object sender, EventArgs e)
        {

        }
        void DGVpagos_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            ActualizaLabels();
        }
        void DGVpagos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
        }
        void frmCaja_Load(object sender, EventArgs e)
        {
            try
            {
                _ControladorCaja = new Controlador.Controladorcaja(_Cnn, _Cod_Folio, _user);
                _ControladorPermisos = new Permisos.Controlador.Controlador(_Cnn);
                _ControladorRetiro = new ControladorRetirosCaja();//Controlador.ControladorRetiroCaja(_Cnn);
                _permisos = _ControladorPermisos.getpermisoventana(_user, this.Text);
                zctSegUsuBindingSource.DataSource = _ControladorCaja.ObtieneVendedores();
                Edita =(_permisos.edita || _permisos.agrega);
                Bgrabar.Enabled = Edita;
                    
                var cajas = _ControladorCaja.ObtieneCajas();
                cajaabierta = _ControladorCaja.traeCajaAbiertaUsuario(_user);
                TiposPagoBindingSource.DataSource = _ControladorCaja.TraeTiposPago();
                Limpia();
                CompruebaCaja();
                BabreCaja.Enabled = !(cajaabierta > 0);
                RetiroCaja();
                montoDataGridViewTextBoxColumn.DataPropertyName = "MontoIngresado";
                this.Text = "CAJA [" + _ControladorCaja.NombreUsuario(_user) + "]";
                ///CajaActual = _ControladorCaja.ObtieneCajaNueva(_user);
                //EnlazaCaja();
                //SetClienteDefault();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error cargando la caja: " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void RetiroCaja()
        {
            decimal totalcaja = _ControladorCaja.TraerTotalCaja(_Cod_Folio,_user);

            var Parametros = new ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            //var Parametros = _ControladorRetiro.TraerParametros();
            if (Parametros.maximo_caja < totalcaja)
            {
                MessageBox.Show("Se ha exedido el maximo de caja, se recomienda relizar un retiro", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                errorProvider.SetIconPadding(Bretiro, -20);
                errorProvider.SetIconAlignment(Bretiro, ErrorIconAlignment.MiddleRight);
                errorProvider.SetError(Bretiro, "Se ha exedido el maximo de caja, se recomienda relizar un retiro");
                Bretiro.Enabled = true;
            }
            else
            {
                errorProvider.Clear();
                Bretiro.Enabled = false;
            }
        }
        private void Limpia()
        {
            Lstatus.Text = "NUEVO FOLIO SIN GUARDAR";
            Lstatus.BackColor = Color.Yellow;

            LfechaFolio.DataBindings.Clear();
            this.Lcliente.DataBindings.Clear();
            this.Ldireccion.DataBindings.Clear();
            this.Lemail.DataBindings.Clear();
            this.LRFC.DataBindings.Clear();
            this.Lstatus.DataBindings.Clear();
            this.LfechaFolio.DataBindings.Clear();

            this.Lcliente.Text = "";
            this.Ldireccion.Text = "";
            this.Lemail.Text = "";
            this.LRFC.Text = "";
            this.LfechaFolio.Text = "";

            DGVdetalle.DataSource = null;
            DGVpagos.DataSource = null;
            EncabezadoCajaBindingSource.DataSource = null;
            LfechaFolio.DataBindings.Clear();
            TxtPedido.DataBindings.Clear();
            _Consecutivo = _ControladorCaja.ConsultaFolioCaja();
             TxtFolio.Text = _Consecutivo.ToString();
             TxtPedido.Text = "";
            Bcancelar.Enabled = false;
            Bgrabar.Enabled = Edita ;
            // button6.Enabled = false;
            CajaActual = _ControladorCaja.ObtieneCajaNueva(_user);
            EnlazaCaja();
            
            ActualizaLabels();
            //TxtPedido.Focus();
            txtCapturaArticulos.Focus();
            habilita_botones(true);
            SetClienteDefault();
            
        }

        private void SetClienteDefault() {
            
            if (CajaActual != null)
            {
                //var Datos = _ControladorCaja.TraeDatosClienteOT(Convert.ToInt32(TxtPedido.Text));
                int cod_cte = _cod_cte_default;
                if (cod_cte > 0){
                    CajaActual.cod_cte = cod_cte;
                    Lcliente.Text = "Cliente generico";
                 //   CajaActual.ZctCatCte = _ControladorCaja.carga_cliente(cod_cte);
                }   
                EncabezadoCajaBindingSource.ResetCurrentItem();
            }
           
        }


        private Boolean EsEntero(string Valor)
        {
            int i = 0;
            return int.TryParse(Valor, out i); //i now = 108
        }
        public void CargaFolio(int pedido) {
            if (pedido <= 0)
            {
                return;
            }
                TxtPedido.Text = pedido.ToString();
                _pagando = true;
                CargaCaja(TxtPedido);
    }

        private void CargaCaja(Control MiControl)
        {
            try
            {

                if (Convert.ToInt32((TxtFolio.Text)) >= _Consecutivo && EsEntero(TxtPedido.Text) == false)
                {
                    return;
                }
                _ControladorCaja.Refresca();
                if (EsEntero(TxtFolio.Text) && !EsEntero(TxtPedido.Text))
                {
                    CajaActual = _ControladorCaja.BuscaRecibo(_Cod_Folio, Convert.ToInt32(TxtFolio.Text));
                }
                else
                {
                    if (CajaActual.CodEnc_Caja != Convert.ToInt32(TxtFolio.Text) || _pagando ==true )
                       CajaActual = _ControladorCaja.CargaOrden(_Cod_Folio, TxtPedido.Text, Convert.ToInt32(TxtFolio.Text));
                    _pagando = false;
                }

                EnlazaCaja();
                _ControladorCaja.carga_cliente(Convert.ToInt32 (  CajaActual.cod_cte ?? 0) );
            //    if (CajaActual.ZctDetCaja.Count>0)
            //        habilita_botones(false);
              
                habilita_botones(_ControladorCaja.CajaNueva);
           }


            catch (Exception ex)
            {
                MessageBox.Show("Error cargando caja " + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void EnlazaCaja()
        {
            TxtFolio.Text = CajaActual.CodEnc_Caja.ToString();
            TxtPedido.Text = CajaActual.CodEnc_OT.ToString();

            EncabezadoCajaBindingSource.DataSource = CajaActual;
            zctDetCajaBindingSource.DataSource = CajaActual.ZctDetCaja;
            PagosCajaBindingSource.DataSource = CajaActual.ZctPagoCaja;

            DGVdetalle.DataSource = zctDetCajaBindingSource;
            DGVpagos.DataSource = PagosCajaBindingSource;
            zctDetCajaBindingSource.DataSource = CajaActual.ZctDetCaja;

            LfechaFolio.DataBindings.Clear();
            this.LfechaFolio.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.EncabezadoCajaBindingSource, "fecha_folio", true));
            this.Lcliente.DataBindings.Clear();
            this.Lcliente.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.EncabezadoCajaBindingSource, "NombreCliente", true));
            this.Ldireccion.DataBindings.Clear();
            this.Ldireccion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.EncabezadoCajaBindingSource, "direccion", true));
            this.Lemail.DataBindings.Clear();
            this.Lemail.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.EncabezadoCajaBindingSource, "email", true));
            this.LRFC.DataBindings.Clear();
            this.LRFC.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.EncabezadoCajaBindingSource, "rfc", true));
            this.Lstatus.DataBindings.Clear();
            this.Lstatus.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.EncabezadoCajaBindingSource, "staus", true));

            ActualizaLabels();

            if (CajaActual.staus == "CANCELADO")
            {
                Bgrabar.Enabled = false;
                DGVpagos.AllowUserToAddRows = false;
                Lstatus.BackColor = Color.FromArgb(243, 159, 024);
                Bimprimir.Enabled = true;

            }
            else if (CajaActual.staus == "APLICADO")
            {
                Bgrabar.Enabled = false; //(_permisos.edita || _permisos.agrega);
                DGVpagos.AllowUserToAddRows = (_TotalPago < _TotalDet);
                Lstatus.BackColor = Color.PaleGreen;
                Bimprimir.Enabled = true;
            }
            else if (CajaActual.staus == "NUEVO FOLIO SIN GUARDAR")
            {
                Bgrabar.Enabled = (_permisos.edita || _permisos.agrega);
                DGVpagos.AllowUserToAddRows = (_TotalPago < _TotalDet);
                Lstatus.BackColor = Color.Yellow;
                Bimprimir.Enabled = false;
            }
            if (_ControladorCaja.CajaNueva == true || CajaActual.staus == "CANCELADO" || Lstatus.Text == "NUEVO FOLIO SIN GUARDAR")
            {
                Bcancelar.Enabled = false;
            }
            else
            {
                Bcancelar.Enabled = true;
            }
            Lstatus.Text = CajaActual.staus;
        }
        private void Habilita(Boolean Habilitado)
        {
            TxtFolio.Enabled = Habilitado;
            TxtPedido.Enabled = Habilitado;
            //Bgrabar.Enabled = !Habilitado;
        }
        private void TxtPierdeElFoco(object sender, EventArgs e)
        {
            Control MiControl = (Control)sender;
            CargaCaja(MiControl);
        }
        private void ActualizaLabels()
        {
            _TotalPago = 0;
            _TotalDet = CajaActual.Total;  //this.DGVdetalle.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDecimal(x.Cells["totalDataGridViewTextBoxColumn"].Value));
            _Subtotal = CajaActual.SubTotal;//Decimal.Round(this.DGVdetalle.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDecimal(x.Cells["subtotalDataGridViewTextBoxColumn"].Value)), 2);
            _iva = CajaActual.IVA;//this.DGVdetalle.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDecimal(x.Cells["ivaDataGridViewTextBoxColumn"].Value));
            _TotalPago = CajaActual.TotalPago;  // decimal.Round(this.DGVpagos.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDecimal(x.Cells["montoDataGridViewTextBoxColumn"].Value)), 2);
            _Cambio = CajaActual.Cambio; //decimal.Round(this.DGVpagos.Rows.Cast<DataGridViewRow>().Sum(x => Convert.ToDecimal(x.Cells["cambioDataGridViewTextBoxColumn"].Value)), 2);

            Ltotal.Text = string.Format("{0:C}", _TotalDet);
            this.Lsubtotal.Text = string.Format("{0:C}", _Subtotal);
            this.Liva.Text = string.Format("{0:C}", _iva);
            this.Lpago.Text = string.Format("{0:C}", _TotalPago);
            this.Lcambio.Text = string.Format("{0:C}", _Cambio);
            this.Lrestante.Text = string.Format("{0:C}", CajaActual.PorPagar);
            
            
            //if (_TotalDet - _TotalPago > 0)
            //{
            //    this.Lrestante.Text = string.Format("{0:C}", _TotalDet - _TotalPago);
            //}
            //else
            //{
            //    this.Lrestante.Text = string.Format("{0:C}",0);
            //}
            //DGVpagos.AllowUserToAddRows = !(_TotalPago >= _TotalDet && _TotalPago >0);
            if (_ControladorCaja == null)
            {
                return;
            }

            //this.Lstatus.Text = CajaActual.staus ?? "NUEVA CAJA SIN GUARDAR"; 
            DGVpagos.AllowUserToAddRows = !(_TotalPago >= _TotalDet && _ControladorCaja.CajaNueva == false);
        }
        #region Clicks
        private void Bant_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(TxtFolio.Text) - 1 <= 0)
                {
                    return;
                }
                //_Consecutivo -= 1;
                TxtFolio.Text = (Convert.ToInt32(TxtFolio.Text) - 1).ToString();
                TxtPedido.Text = _ControladorCaja.BuscaRecibo(_Cod_Folio, Convert.ToInt32(TxtFolio.Text)).CodEnc_OT.ToString();
                CargaCaja(TxtFolio);
                //EnlazaCaja();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void Bsig_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(TxtFolio.Text) + 1 >= _Consecutivo)
                {
                    Limpia();
                    
                    EnlazaCaja();
                    Habilita(true);
                    return;
                }
                //_Consecutivo += 1;
                TxtFolio.Text = TxtFolio.Text = (Convert.ToInt32(TxtFolio.Text) + 1).ToString();
                TxtPedido.Text = _ControladorCaja.BuscaRecibo(_Cod_Folio, Convert.ToInt32(TxtFolio.Text)).CodEnc_OT.ToString();
                CargaCaja(TxtFolio);
                //EnlazaCaja();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Bgrabar_Click(object sender, EventArgs e)
        {
            try
            {
            
                if (valida_datos())
                {
                    foreach (DataGridViewRow R in DGVpagos.Rows)
                    {
                        if (Convert.ToDecimal(R.Cells["montoDataGridViewTextBoxColumn"].Value) <= 0)
                        {
                            continue;
                        }
                        R.Cells["nodoc_pago"].Value = R.Index + 1;
                        R.Cells["cod_folio"].Value = _Cod_Folio;
                        R.Cells["CodEnc_Caja"].Value = TxtFolio.Text;
                        R.Cells["cod_usu"].Value = _user;
                    }
                    zctDetCajaBindingSource.EndEdit();
                    PagosCajaBindingSource.EndEdit();
                    _ControladorCaja.ValidaMontoPago(CajaActual, _TotalPago);
                    _ControladorCaja.GrabaCaja(CajaActual, _TotalPago);
                    this.Lanzaimpresion(_Cod_Folio, CajaActual.CodEnc_Caja);
                    Limpia();
                    Habilita(true);
                    _TotalPago = 0;
                    Bcancelar.Enabled = true;
                    MessageBox.Show("Caja Guardada", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    RetiroCaja();
                    EnlazaCaja();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool valida_datos()
        {
            if (CajaActual.cod_cte == null || CajaActual.cod_cte == 0)
            {
                MessageBox.Show("Debe de proporcionar el cliente a cobrar");
                return false;
            }
            if (CajaActual.ZctDetCaja == null || CajaActual.ZctDetCaja.Count == 0)
            {
                MessageBox.Show("Debe de insertar artículos a cobrar");
                return false;
            }

            return true;
        }
        private void BcambiarClienteBoton_Click(object sender, EventArgs e)
        {
            if (TxtPedido.Text == "" || Convert.ToInt64(TxtPedido.Text) <= 0)
            {
                MessageBox.Show("Ingrese primero un numero de pedido", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TxtPedido.Focus();
                return;
            }
            LanzaOT(Convert.ToInt64(TxtPedido.Text));
            _ControladorCaja.actualiza_detalle(CajaActual);
            zctDetCajaBindingSource.DataSource = null;
            zctDetCajaBindingSource.DataSource = CajaActual.ZctDetCaja;
            DGVdetalle.DataSource = null;
            DGVdetalle.DataSource = zctDetCajaBindingSource;
            ActualizaLabels();
        }
        private void BcambiarCliente_Click(object sender, EventArgs e)
        {
            if (CajaActual != null &&  CajaActual.ZctCatCte != null)
            {
                //var Datos = _ControladorCaja.TraeDatosClienteOT(Convert.ToInt32(TxtPedido.Text));
               LanzaCliente(Convert.ToInt32(CajaActual.cod_cte));
               CajaActual.ZctCatCte= _ControladorCaja.carga_cliente(CajaActual.ZctCatCte.Cod_Cte);
               EncabezadoCajaBindingSource.ResetCurrentItem();
            }
            else
            {
                MessageBox.Show("Seleccione un documento de folio caja primero ó un cliente.", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BabreCaja_Click(object sender, EventArgs e)
        {
            try
            {
                frmAperturaCaja abrecaja = new frmAperturaCaja(_Cnn, _user, _Cod_Folio);
                if (abrecaja.ShowDialog(this) == DialogResult.OK)
                {
                    CompruebaCaja();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error abriendo caja:" + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }
        private void Bretiro_Click_1(object sender, EventArgs e)
        {
            try{

                //Autoriza(14, this);
                Autoriza(new global::Modelo.Seguridad.Dtos.DtoPermisos { ReimprimirRecibos = true }, this);
                if (_PermisoCancelar == false)
                {
                    throw new Exception("No esta autorizado para retirar de caja.");
                }
                RetiroCaja retiro = new RetiroCaja(_Cnn, _user, _Cod_Folio);
                retiro.ImprimeRetiro += new RetiroCaja.DimprimeRetiro(retiro_ImprimeRetiro);
                if (retiro.ShowDialog(this) == DialogResult.OK)
                {
               
                    MessageBox.Show("Retiro de caja realizado", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.DialogResult = DialogResult.OK;
                    RetiroCaja();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        void retiro_ImprimeRetiro( int folio_retiro)
        {
            ImprimeRetiro(folio_retiro);
        }
        private void BcorteCaja_Click_1(object sender, EventArgs e)
        {
            try
            {
                frmCorte corte = new frmCorte(_Cnn, _user, _Cod_Folio);
                corte.ImprimeCorte += new frmCorte.DimprimeCorte(corte_ImprimeCorte);
                if (corte.ShowDialog(this) == DialogResult.OK)
                {
                    RetiroCaja();
                    CompruebaCaja();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error realizando corte de caja:" + ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        void corte_ImprimeCorte(decimal fondocaja, string cod_folio, int cod_usu)
        {
            ImprimeCorte(fondocaja, cod_folio, cod_usu);
        }
        private void Bcancelar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult resp = MessageBox.Show("¿Realmente desea cancelar el recibo?, esta accion no se podra deshacer", "Pregunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resp == DialogResult.No)
                {
                    return;
                }
                //Autoriza(7,this);
                Autoriza(new global::Modelo.Seguridad.Dtos.DtoPermisos { CancelarCaja = true }, this);
                if (_PermisoCancelar==false )
                {
                    throw new Exception  ("No esta autorizado para cancelar folios de caja");
                }
                _ControladorCaja.CancelaRecibo(CajaActual.cod_folio, CajaActual.CodEnc_Caja, _user);
                CargaCaja(TxtFolio);
                RetiroCaja();
                MessageBox.Show("Recibo Cancelado", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void Blimpiar_Click(object sender, EventArgs e)
        {
            try
            {
                _ControladorCaja.LimpiaRecibo(CajaActual);
                
                Limpia();
                EnlazaCaja();
                Habilita(true);
                MessageBox.Show("Edicion Cancelada", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
        #endregion
        #region "pagos"
        private void DGVpagos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ActualizaLabels();
                if (e.ColumnIndex == this.montoDataGridViewTextBoxColumn.DisplayIndex)
                {
                    if (DGVpagos.Rows[e.RowIndex].Cells["montoDataGridViewTextBoxColumn"].Value == null)
                    {
                        DGVpagos.CancelEdit();
                        return;
                    }
                    decimal v;
                    if (!Decimal.TryParse(DGVpagos.Rows[e.RowIndex].Cells["montoDataGridViewTextBoxColumn"].Value.ToString(), out v))
                    {
                        DGVpagos.CancelEdit();
                        throw new Exception("El monto de pago debe ser un numero");
                    }
                    decimal Totalpagar = (_TotalDet);
                    if (_TotalPago > _TotalDet)
                    {
                        DGVpagos.Rows[e.RowIndex].Cells["cambioDataGridViewTextBoxColumn"].Value = Decimal.Round(_TotalPago - _TotalDet, 2);
                        DGVpagos.Rows[e.RowIndex].Cells["ColMontoReal"].Value = Decimal.Round(Convert.ToDecimal(Convert.ToDecimal(DGVpagos.Rows[e.RowIndex].Cells["montoDataGridViewTextBoxColumn"].Value)), 2) - Decimal.Round(Convert.ToDecimal(Convert.ToDecimal(DGVpagos.Rows[e.RowIndex].Cells["cambioDataGridViewTextBoxColumn"].Value)), 2);
                    }
                    else
                    {
                        DGVpagos.Rows[e.RowIndex].Cells["cambioDataGridViewTextBoxColumn"].Value = Decimal.Round(0, 2);
                        DGVpagos.Rows[e.RowIndex].Cells["ColMontoReal"].Value = Decimal.Round(Convert.ToDecimal(Convert.ToDecimal(DGVpagos.Rows[e.RowIndex].Cells["montoDataGridViewTextBoxColumn"].Value)), 2);
                    }
                }

                if (e.ColumnIndex == this.tipo_pago.DisplayIndex)
                {

                    DGVpagos.Rows[e.RowIndex].Cells["Tipopagotmp"].Value = DGVpagos.Rows[e.RowIndex].Cells["Tipo_pago"].Value;
                }
                //tipo_pago 
                if (Convert.ToInt32(DGVpagos.Rows[e.RowIndex].Cells["tipo_pago"].Value) < 1)
                {
                    DGVpagos.Rows[e.RowIndex].Cells["tipo_pago"].Value = 1;
                }
                if (Convert.ToInt32(DGVpagos.Rows[e.RowIndex].Cells["nodoc_pago"].Value) <= 0)
                {
                    DGVpagos.Rows[e.RowIndex].Cells["nodoc_pago"].Value = _ControladorCaja.NumeroDocumentoPago(_Cod_Folio, Convert.ToInt32(TxtFolio.Text));
                }
                DGVpagos.Rows[e.RowIndex].Cells["fechapagoDataGridViewTextBoxColumn"].Value = DateTime.Now;
                DGVpagos.Rows[e.RowIndex].Cells["corteDataGridViewTextBoxColumn"].Value = false;
                ActualizaLabels();
            }
            catch (Exception ex)
            {
                DGVpagos.CurrentCell = DGVpagos.Rows[e.RowIndex].Cells[e.ColumnIndex];
                DGVpagos.BeginEdit(true); 
                MessageBox.Show("Error Editando pago: " + ex.Message , "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }       
        #endregion
        void frmCaja_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cajaabierta > 0 && aviso_de_cierre == true)
            {
                DialogResult resp = MessageBox.Show("tiene una caja abierta ¿desea realizar el corte de caja y cerrarla?", "Atencion", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (resp == DialogResult.Yes)
                {
                    e.Cancel = true;
                    frmCorte corte = new frmCorte(_Cnn, _user, _Cod_Folio);
                    if (corte.ShowDialog(this) == DialogResult.OK)
                    {
                        CompruebaCaja();
                        this.Close();
                    }
                }

            }
        }
        private void txtCaracter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        private void Control_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }
        private void label21_Click(object sender, EventArgs e)
        {
        }
        private void Ldireccion_Click(object sender, EventArgs e)
        {

        }

        private void bimprimir_Click(object sender, EventArgs e)
        {

            try
            {
                //Autoriza(8, this);
                //Autoriza(13, this);
                Autoriza(new global::Modelo.Seguridad.Dtos.DtoPermisos { ReimprimirRecibos = true }, this);
                if (_PermisoCancelar == false)
                { 
                    throw new Exception("No esta autorizado para reimprimir los recibos");
                }
                this.Lanzaimpresion(_Cod_Folio,Convert.ToInt32(TxtFolio.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BestadoActualCaja_Click(object sender, EventArgs e)
        {
            try
            {
                //Autoriza(8, this);
                Autoriza(new global::Modelo.Seguridad.Dtos.DtoPermisos { ConsultarCaja = true }, this);
                if (_PermisoCancelar == false)
                {
                    throw new Exception("No esta autorizado para ver el estado de caja");
                }
                FrmEstadoCaja estado = new FrmEstadoCaja(_Cnn,_user,_Cod_Folio);
                estado.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cmdCrearPedido_Click(object sender, EventArgs e)
        {
            aviso_de_cierre = false;
            CreaOT();
            this.Close();
        }

        private void DGVdetalle_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try{
         
                
                
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtCapturaArticulos_Leave(object sender, EventArgs e)
        {

            try
            {
                if (txtCapturaArticulos.Text != "")
                {
                    string articulo = txtCapturaArticulos.Text;
                    Modelo.ZctDetCaja nuevo = _ControladorCaja.obtiene_detalle_por_articulo(articulo, (Modelo.ZctSegUsu) cboVendedor.SelectedItem);

                    //zctDetCajaBindingSource.Find("Cod_Art", nuevo.Cod_Art);
                    //CajaActual.ZctDetCaja.Add(nuevo);
                    
                    zctDetCajaBindingSource.Add(nuevo);
                    //zctDetCajaBindingSource.ResetCurrentItem();
                    ActualizaLabels();
                    EncabezadoCajaBindingSource.ResetCurrentItem();
                    Console.Beep();
                    //Console.WriteLine(CajaActual.Total);
                    prepara_carga_articulo();
                    int RowIndex = DGVdetalle.Rows.Count - 1;
                    DGVdetalle.ClearSelection();
                    DGVdetalle.Rows[RowIndex ].Selected = true;
                    DGVdetalle.FirstDisplayedScrollingRowIndex = RowIndex;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                prepara_carga_articulo();
            }
        }

        private void prepara_carga_articulo()
        {
            txtCapturaArticulos.Text = "";
            txtCapturaArticulos.Focus();
        }

        private void DGVdetalle_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cmdPagoTarjeta_Click(object sender, EventArgs e)
        {
            try
            {
                carga_pantalla_pagos(2, true, false);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmdPagoEfectivo_Click(object sender, EventArgs e)
        {
            try{
                carga_pantalla_pagos(1, true, false);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void carga_pantalla_pagos(int tipo_pago, bool limpia_pago, bool monto_enable)
        {
            frmPagosCaja pago = new frmPagosCaja(CajaActual, tipo_pago, limpia_pago, monto_enable);
            pago.ShowDialog(this);
            PagosCajaBindingSource.DataSource = null;
            PagosCajaBindingSource.DataSource = CajaActual.ZctPagoCaja;
            PagosCajaBindingSource.ResetCurrentItem();
        }

        private void cmdPagoParcial_Click(object sender, EventArgs e)
        {
            try
            {
                carga_pantalla_pagos(2, true, true);
                carga_pantalla_pagos(1, false , false);
                
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            

        }

        private void cmdBuscaCliente_Click(object sender, EventArgs e)
        {
            try
            {
                if (CajaActual != null )
                {
                    //var Datos = _ControladorCaja.TraeDatosClienteOT(Convert.ToInt32(TxtPedido.Text));
                    int cod_cte = LanzaBuscaDeCliente();
                    if (cod_cte > 0)
                        CajaActual.ZctCatCte = _ControladorCaja.carga_cliente(cod_cte);

                    EncabezadoCajaBindingSource.ResetCurrentItem();
                }
                else
                {
                    MessageBox.Show("Seleccione un documento de folio caja primero", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVdetalle.SelectedRows.Count == 0)
                {
                    throw new Exception("No ha cargado una OC o seleccionado un Articulo en el detalle.");
                }
                
                //Autoriza(12, this);
                //if (_PermisoCancelar == false)
                //{
                //    throw new Exception("No esta autorizado para editar el artículo.");
                //}
                if (_current_row == null) {
                    throw new Exception("Debe de seleccionar un artículo para editarlo.");
                }
                if(_current_row.tasa_iva == 0){
                    _current_row.tasa_iva = _ControladorCaja.get_iva();
                }
                FrmEditaArticuloCantidad edita = new FrmEditaArticuloCantidad(_current_row);
               if  (edita.ShowDialog(this)==DialogResult.OK)
               {
                zctDetCajaBindingSource.ResetCurrentItem();
                //EnlazaCaja();
                ActualizaLabels();
               }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private Modelo.ZctDetCaja _current_row;
        private void DGVdetalle_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                _current_row = (Modelo.ZctDetCaja)DGVdetalle.CurrentRow.DataBoundItem;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            

        }


        private void txtCapturaArticulos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                System.Windows.Forms.SendKeys.SendWait("{TAB}");
            }
            if (e.KeyCode == Keys.F3)
            {
                txtCapturaArticulos.Text = LanzaBusquedaArticulos().ToString();
                System.Windows.Forms.SendKeys.SendWait("{TAB}");
            }
        }


        private void BeditarCostoArticulo_Click(object sender, EventArgs e)
        {
            try
            {
                if (DGVdetalle.SelectedRows.Count == 0)
                {
                    throw new Exception("No ha cargado una OC o seleccionado un Articulo en el detalle.");
                }
                //Autoriza(9, this);
                Autoriza(new global::Modelo.Seguridad.Dtos.DtoPermisos { ModificarArticulos = true }, this);
                if (_PermisoCancelar == false)
                {
                    throw new Exception("No esta autorizado para editar el artículo.");
                }
                if (_current_row == null)
                {
                    throw new Exception("Debe de seleccionar un artículo para editarlo.");
                }
                if (_current_row.tasa_iva == 0)
                {
                    _current_row.tasa_iva = _ControladorCaja.get_iva();
                }
                FrmEditaArticuloCosto edita = new FrmEditaArticuloCosto(_current_row);
                if (edita.ShowDialog(this)==DialogResult.OK)
                {
                    zctDetCajaBindingSource.ResetCurrentItem();
                    //EnlazaCaja();
                    ActualizaLabels();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void DGVdetalle_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete ){
                if (!DGVdetalle.CurrentRow.IsNewRow) {
                    if( MessageBox.Show("¿Desea eliminar este artículo?", "Eliminar", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes){
                        _ControladorCaja.EliminaElemento((Modelo.ZctDetCaja)zctDetCajaBindingSource.Current);
                        zctDetCajaBindingSource.Remove(zctDetCajaBindingSource.Current);
                            
                        //DGVdetalle.Rows.Remove(DGVdetalle.CurrentRow);
                        zctDetCajaBindingSource.ResetCurrentItem();
                        //EnlazaCaja();
                        ActualizaLabels();
                    }
                }
            }
        }

        private void cmdAplicaDescuento_Click(object sender, EventArgs e)
        {
            try
            {
                //Autoriza(10, this);
                Autoriza(new global::Modelo.Seguridad.Dtos.DtoPermisos { AplicarDescuentoCaja = true }, this);
                if (_PermisoCancelar == false)
                {
                    throw new Exception("No esta autorizado para asignar descuentos.");
                }
                if (CajaActual.ZctDetCaja.Count() == 0) {
                    throw new ArgumentException("No esta autorizado para editar el artículo.");
                }
                FrmAplicaDescuentoCaja edita = new FrmAplicaDescuentoCaja(CajaActual);
                if (edita.ShowDialog(this) == DialogResult.OK)
                {
                    zctDetCajaBindingSource.ResetCurrentItem();
                    //EnlazaCaja();
                    ActualizaLabels();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void TxtFolio_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void txtCapturaArticulos_TextChanged(object sender, EventArgs e)
        {

        }
    }
}