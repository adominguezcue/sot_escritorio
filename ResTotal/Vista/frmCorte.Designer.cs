﻿namespace ResTotal.Vista
{
    partial class frmCorte
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tmonto = new System.Windows.Forms.TextBox();
            this.BcancelarRetiro = new System.Windows.Forms.Button();
            this.Bretirar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Tmonto
            // 
            this.Tmonto.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Tmonto.Location = new System.Drawing.Point(243, 23);
            this.Tmonto.Name = "Tmonto";
            this.Tmonto.Size = new System.Drawing.Size(130, 30);
            this.Tmonto.TabIndex = 0;
            this.Tmonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // BcancelarRetiro
            // 
            this.BcancelarRetiro.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BcancelarRetiro.Location = new System.Drawing.Point(201, 77);
            this.BcancelarRetiro.Name = "BcancelarRetiro";
            this.BcancelarRetiro.Size = new System.Drawing.Size(100, 23);
            this.BcancelarRetiro.TabIndex = 26;
            this.BcancelarRetiro.Text = "Cancelar";
            this.BcancelarRetiro.UseVisualStyleBackColor = true;
            this.BcancelarRetiro.Click += new System.EventHandler(this.BcancelarRetiro_Click);
            // 
            // Bretirar
            // 
            this.Bretirar.Location = new System.Drawing.Point(79, 77);
            this.Bretirar.Name = "Bretirar";
            this.Bretirar.Size = new System.Drawing.Size(100, 23);
            this.Bretirar.TabIndex = 25;
            this.Bretirar.Text = "Corte caja";
            this.Bretirar.UseVisualStyleBackColor = true;
            this.Bretirar.Click += new System.EventHandler(this.Bretirar_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(19, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 27);
            this.label2.TabIndex = 24;
            this.label2.Text = "Monto Actual Caja $:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmCorte
            // 
            this.AcceptButton = this.Bretirar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BcancelarRetiro;
            this.ClientSize = new System.Drawing.Size(381, 122);
            this.Controls.Add(this.Tmonto);
            this.Controls.Add(this.BcancelarRetiro);
            this.Controls.Add(this.Bretirar);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmCorte";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Corte Caja";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Tmonto;
        private System.Windows.Forms.Button BcancelarRetiro;
        private System.Windows.Forms.Button Bretirar;
        private System.Windows.Forms.Label label2;
    }
}