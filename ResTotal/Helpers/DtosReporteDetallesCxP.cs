﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResTotal.Helpers
{
    public class DtosReporteDetallesCxP
    {
        public int CodigoProveedorCab { get; set; }
        public string NombreProvedorCab { get; set; }

        public decimal SubtotalCab { get; set; }
        public decimal IvaCab { get; set; }
        public decimal TotalCab { get; set; }
        public List<DtosDetallesCxP> DetallesCxP { get; set; }

    }

    public class DtosDetallesCxP
    {
        public int LlaveCodProveedo { get; set; }
        public int CodigoordenCompraDet { get; set; }
        public decimal SubtotalDet { get; set; }
        public decimal IvaDet { get; set; }
        public decimal TotalDet { get; set; }
        public DateTime Fecha_CreacionDet { get; set; }
        public DateTime Fecha_VencimientoDet { get; set; }
        public string NumeroFacturaDet { get; set; }
        public string EstadoDet { get; set; }

        public bool Esvencida { get; set; }
    }


}
