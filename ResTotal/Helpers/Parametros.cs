﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ResTotal.Helpers
{
    public class Parametros
    {
        public static decimal IVA(Modelo.DAODataContext dao) {
            return (from i in dao.ZctCatPar select i.Iva_CatPar).SingleOrDefault().GetValueOrDefault();
        }

        public static decimal calcula_iva(decimal subtotal, Modelo.DAODataContext dao)
        {
            decimal iva = IVA(dao);
            return subtotal * iva;
        }

        
    }
}
