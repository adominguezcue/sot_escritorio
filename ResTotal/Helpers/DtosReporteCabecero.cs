﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResTotal.DtoReportes
{
    public class DtosReporteCabecero
    {
        public string Hotel { get; set; }
        public string Direccion { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaInicioCreacion { get; set; }
        public DateTime FechaFinCreacion { get; set; }
        public DateTime FechaIniciovencimiento { get; set; }
        public DateTime FechaFinvencimiento { get; set; }
        public string Estadocuenta { get; set; }
    }
}

