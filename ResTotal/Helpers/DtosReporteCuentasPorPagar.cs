﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResTotal.Helpers
{
    public class DtosReporteCuentasPorPagar
    {
        public string NombreDelProveedor { get; set; }

        public int CodigoordenCompra { get; set; }

        public decimal Subtotal { get; set; }
        public decimal Iva { get; set; }
        public decimal Total { get; set; }

        public DateTime Fecha_Creacion { get; set; }

        public DateTime Fecha_Vencimiento { get; set; }


        public string NumeroFactura { get; set; }

        public string Estado { get; set; }

        public bool EsVencida { get; set; }

    }
}
