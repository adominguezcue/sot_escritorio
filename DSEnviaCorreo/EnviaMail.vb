﻿Imports System
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.IO

Namespace EnviaMail

    Public Interface IMail
        Function EnviaCorreo() As Boolean
        Function EnviaCorreoPar(ByVal De As String, ByVal Para As String, ByVal Mensaje As String, ByVal Asunto As String, ByVal SMTP As String) As Boolean
        Property De() As String
        Property Para() As String
        Property SMTP() As String
        Property Mensaje() As String
        Property Asunto() As String
        Property bcc() As String
        Property replayto() As String
        Property User() As String
        Property Password() As String
        Property puerto() As Integer


    End Interface


    Public Class SendMail
        Implements IMail

        Private _Para As String
        Private _SMTP As String
        Private _Mensaje As String
        Private _Asunto As String
        Private _bcc As String
        Private _replayto As String
        Private _User As String
        Private _Password As String

        Dim msgMail As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage

        Private _De As String
        ''' <summary>
        '''  Dirección de la cual se esta enviando el correo
        ''' </summary>
        ''' <value>Dirección de correo</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property De() As String Implements IMail.De
            Get
                Return _De
            End Get
            Set(ByVal value As String)
                _De = value
            End Set
        End Property


        Public Property Edad() As Integer
            Get

            End Get
            Set(ByVal value As Integer)

            End Set
        End Property




        ''' <summary>
        '''  Direcciones a las cuales se les va enviar el correo, en caso de que sean varias direcciones se pueden separar con puntos y coma (;)
        ''' </summary>
        ''' <value>Destinatarios del correo</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Para() As String Implements IMail.Para
            Get
                Return _Para
            End Get
            Set(ByVal value As String)
                _Para = value
            End Set
        End Property



        ''' <summary>
        '''  Servidor de correos que va a utilizar para envíar el Correo
        ''' </summary>
        ''' <value>Servidor de correis</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property SMTP() As String Implements IMail.SMTP
            Get
                Return _SMTP
            End Get
            Set(ByVal value As String)
                _SMTP = value
            End Set

        End Property



        ''' <summary>
        '''  Cuerpo del mensaje del correo
        ''' </summary>
        ''' <value>Cuerpo del mensaje del correo</value>
        ''' <returns></returns>
        ''' <remarks></remarks> 
        Public Property Mensaje() As String Implements IMail.Mensaje
            Get
                Return _Mensaje
            End Get
            Set(ByVal value As String)
                _Mensaje = value
            End Set

        End Property


        ''' <summary>
        '''  Asunto del correo
        ''' </summary>
        ''' <value>Asunto del correo</value>
        ''' <returns></returns>
        ''' <remarks></remarks> 
        Public Property Asunto() As String Implements IMail.Asunto
            Get
                Return _Asunto
            End Get
            Set(ByVal value As String)
                _Asunto = value
            End Set
        End Property

        ''' <summary> 
        ''' Input an email address and test it meets basic formatting standards like including an at sign a dot and min length requirements. 
        ''' </summary> 
        ''' <param name="strEmailAddress">One email address</param> 
        ''' <returns>True if OK and False if not</returns> 
        ''' <remarks></remarks> 
        Public Function ValidateEmailAddressFormat(ByRef strEmailAddress As String) As Boolean
            Dim strEmail As String = strEmailAddress
            Dim intMinLength As Integer = 6
            Dim arrInvalidChars() As String = {",", ";", " "}

            Try
                ' Cleanup Spaces and control characters before or after the email address if found 
                strEmail = strEmail.Trim
                strEmail = strEmail.Replace(vbCrLf, "")
                strEmail = strEmail.Replace(vbCr, "")
                strEmail = strEmail.Replace(vbLf, "")
                strEmail = strEmail.Replace(vbTab, "")

                ' Invalid Character check 
                For i As Integer = 0 To arrInvalidChars.GetUpperBound(0)
                    If strEmail.IndexOf(arrInvalidChars(i)) <> -1 Then
                        Return False
                    End If
                Next

                ' Minimum length check EX= a@a.us  min length = 6 
                If strEmail.Length < intMinLength Then
                    Return False
                End If

                ' Return that it is ok 
                Return True

            Catch ex As Exception
                Throw ex    
            Finally
                ' Return the email by reference 
                strEmailAddress = strEmail
            End Try

        End Function

        Public Sub sendmsg(ByVal strEmailaddressTo As String, _
                     ByVal strEmailaddressCC As String, _
                     ByVal strEmailaddressBCC As String, _
                     ByVal strEmailaddressFrom As String, _
                     ByVal strSubjectOfMessage As String, _
                     ByVal strBodyOfMessage As String, _
                      ByVal files() As String, _
                     Optional ByVal ewsBodyType As ExchangeServices.BodyTypeType = ExchangeServices.BodyTypeType.HTML)

            Dim emailToSave As ExchangeServices.CreateItemType
            Dim response As ExchangeServices.CreateItemResponseType
            Dim emailMessage As ExchangeServices.MessageType
            Dim UnreadCount As Integer = 0
            Dim arrEmailAddressType(1) As ExchangeServices.EmailAddressType
            Dim arrEmailAddressTypeCC(1) As ExchangeServices.EmailAddressType
            Dim arrEmailAddressTypeBCC(1) As ExchangeServices.EmailAddressType
            Dim arrEmail(1) As ExchangeServices.ItemType

            Dim esb As New ExchangeServices.ExchangeServiceBinding
            Dim arrEmailTo(1) As String
            Dim arrEmailCC(1) As String
            Dim arrEmailBCC(1) As String

            Try
                'Setup the exchange binding 
                esb.RequestServerVersionValue = New ExchangeServices.RequestServerVersion
                esb.RequestServerVersionValue.Version = ExchangeServices.ExchangeVersionType.Exchange2010
                esb.Credentials = New NetworkCredential(_User, _Password)
                esb.Url = "https://10.1.1.20/EWS/Exchange.asmx"
                'ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateCertificate)
                
                emailMessage = New ExchangeServices.MessageType()
                'Dim attachments As New ExchangeServices.CreateAttachmentType()

                
                'emailMessage.Attachments = 
                
                ' Add the To recipient(s) to the email message 
                If strEmailaddressTo.Trim <> "" Then
                    arrEmailTo = strEmailaddressTo.Replace(";", ",").Split(",")
                    ReDim arrEmailAddressType(arrEmailTo.GetUpperBound(0))

                    emailMessage.ToRecipients = arrEmailAddressType

                    If arrEmailTo.Length <> 0 Then
                        For i As Integer = 0 To arrEmailTo.GetUpperBound(0)
                            If ValidateEmailAddressFormat(arrEmailTo(i)) = True Then
                                emailMessage.ToRecipients(i) = New ExchangeServices.EmailAddressType
                                emailMessage.ToRecipients(i).EmailAddress = arrEmailTo(i).ToString.Trim
                            Else
                                Throw New Exception("The following email address is invalid in format: " & arrEmailTo(i))
                            End If
                        Next
                    End If
                End If

                ' Add the CC recipient(s) to the email message 
                If strEmailaddressCC.Trim <> "" Then
                    arrEmailCC = strEmailaddressCC.Replace(";", ",").Split(",")
                    ReDim arrEmailAddressTypeCC(arrEmailCC.GetUpperBound(0))

                    emailMessage.CcRecipients = arrEmailAddressTypeCC

                    If arrEmailCC.Length <> 0 Then
                        For i As Integer = 0 To arrEmailCC.GetUpperBound(0)
                            If ValidateEmailAddressFormat(arrEmailCC(i)) = True Then
                                emailMessage.CcRecipients(i) = New ExchangeServices.EmailAddressType
                                emailMessage.CcRecipients(i).EmailAddress = arrEmailCC(i).ToString.Trim
                            Else
                                Throw New Exception("The following email address is invalid in format: " & arrEmailCC(i))
                            End If
                        Next
                    End If
                End If

                ' Add the BCC recipient(s) to the email message 
                If strEmailaddressBCC.Trim <> "" Then
                    arrEmailBCC = strEmailaddressBCC.Replace(";", ",").Split(",")
                    ReDim arrEmailAddressTypeBCC(arrEmailBCC.GetUpperBound(0))

                    emailMessage.BccRecipients = arrEmailAddressTypeBCC

                    If arrEmailBCC.Length <> 0 Then
                        For i As Integer = 0 To arrEmailBCC.GetUpperBound(0)
                            If ValidateEmailAddressFormat(arrEmailBCC(i)) = True Then
                                emailMessage.BccRecipients(i) = New ExchangeServices.EmailAddressType
                                emailMessage.BccRecipients(i).EmailAddress = arrEmailBCC(i).ToString.Trim
                            Else
                                Throw New Exception("The following email address is invalid in format: " & arrEmailBCC(i))
                            End If
                        Next
                    End If
                End If

                ' Add the sender to the email message 
                emailMessage.From = New ExchangeServices.SingleRecipientType() '  set up a single sender 
                emailMessage.From.Item = New ExchangeServices.EmailAddressType()
                emailMessage.From.Item.EmailAddress = strEmailaddressFrom
                emailMessage.Subject = strSubjectOfMessage
                emailMessage.Body = New ExchangeServices.BodyType()
                emailMessage.Body.BodyType1 = ewsBodyType  ' specify HTML or plain Text 
                emailMessage.Body.Value = strBodyOfMessage

                ' The next step is to place the email message inside of a CreateItemType object. 
                emailToSave = New ExchangeServices.CreateItemType
                emailToSave.Items = New ExchangeServices.NonEmptyArrayOfAllItemsType()

                emailToSave.Items.Items = arrEmail
                emailToSave.Items.Items(0) = emailMessage
                emailToSave.MessageDisposition = ExchangeServices.MessageDispositionType.SaveOnly
                emailToSave.MessageDispositionSpecified = True

                System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf validarCertificado)
                'esb.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials

                response = esb.CreateItem(emailToSave)
                Dim rmta() As ExchangeServices.ResponseMessageType = response.ResponseMessages.Items
                Dim emailResponseMessage As ExchangeServices.ItemInfoResponseMessageType = rmta(0)

                
                'Envia el Email
                Dim si As New ExchangeServices.SendItemType
                Dim itemsids(1) As ExchangeServices.BaseItemIdType
                si.ItemIds = itemsids
                si.SavedItemFolderId = New ExchangeServices.TargetFolderIdType()

                Dim count As Integer = 0
                If files.Length > 0 Then
                    Dim attachments(files.Length) As ExchangeServices.AttachmentType

                    For Each file As String In files

                        Dim fileAttach As New ExchangeServices.FileAttachmentType()

                        If file.Contains("pdf") Then
                            Using FileStream As New FileStream(file, FileMode.Open, FileAccess.Read)
                                Dim filesize As Long = FileStream.Length
                                Dim content(filesize) As Byte
                                FileStream.Read(content, 0, filesize)

                                fileAttach.Content = content
                            End Using
                        Else
                            Dim s As String = System.IO.File.ReadAllText(file, System.Text.Encoding.UTF8)
                            fileAttach.Content = System.Text.Encoding.UTF8.GetBytes(s)

                            
                        End If

                        'If inf
                        'Using FileStream As New FileStream(file, FileMode.Open, FileAccess.Read)
                        '    Dim filesize As Long = FileStream.Length
                        '    Dim content(filesize) As Byte
                        '    FileStream.Read(content, 0, filesize)

                        '    fileAttach.Content = content
                        'End Using

                        'Dim s As String = System.IO.File.ReadAllText(file, System.Text.Encoding.UTF8)
                        'fileAttach.Content = System.Text.Encoding.UTF8.GetBytes(s)

                        Dim info As New System.IO.FileInfo(file)
                        fileAttach.Name = info.Name
                        If info.Extension.Contains("pdf") Then
                            fileAttach.ContentType = "application/pdf"
                        ElseIf info.Extension.Contains("xml") Then
                            fileAttach.ContentType = "text/xml"
                        End If

                        'attachments(count) = New ExchangeServices.AttachmentType()

                        attachments(count) = fileAttach
                        count = count + 1
                        'attachments(count) = fileAttach
                        'emailMessage.Attachments.SetValue(fileAttach)
                    Next
                    If attachments.Length > 0 Then
                        Dim attachmentRequest As New ExchangeServices.CreateAttachmentType()
                        attachmentRequest.Attachments = attachments
                        attachmentRequest.ParentItemId = emailResponseMessage.Items.Items(0).ItemId

                        Dim attachmentResponse As ExchangeServices.CreateAttachmentResponseType = esb.CreateAttachment(attachmentRequest)
                        Dim attachmentResponseMessage As ExchangeServices.AttachmentInfoResponseMessageType = attachmentResponse.ResponseMessages.Items(0)


                        Dim attachmentItemId As New ExchangeServices.ItemIdType()
                        attachmentItemId.ChangeKey = attachmentResponseMessage.Attachments(0).AttachmentId.RootItemChangeKey
                        attachmentItemId.Id = attachmentResponseMessage.Attachments(0).AttachmentId.RootItemId
                        si.ItemIds(0) = attachmentItemId
                    End If

                End If
                
                

                Dim siSentItemsFolder = New ExchangeServices.DistinguishedFolderIdType()
                siSentItemsFolder.Id = ExchangeServices.DistinguishedFolderIdNameType.sentitems
                si.SavedItemFolderId.Item = siSentItemsFolder
                si.SaveItemToFolder = True

                Dim siSendItemResponse As ExchangeServices.SendItemResponseType = esb.SendItem(si)


            Catch ex As Exception
                Throw ex
            Finally
                If esb IsNot Nothing Then esb.Dispose() : esb = Nothing

                emailMessage = Nothing
                response = Nothing
                emailToSave = Nothing
            End Try
        End Sub

        Private Function validarCertificado(ByVal sender As Object, ByVal certificado As System.Security.Cryptography.X509Certificates.X509Certificate, ByVal cadena As System.Security.Cryptography.X509Certificates.X509Chain, ByVal sslErrores As System.Net.Security.SslPolicyErrors) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function EnviaCorreo() As Boolean Implements IMail.EnviaCorreo
            Me.sendmsg(_Para, _replayto, _bcc, _De, _Asunto, _Mensaje, Nothing)

            'Try
            '    '_Para = "daltoninternet@gmail.com"
            '    If _replayto = "" Then Throw New Exception("No se ha encontrado el reply to, verifique por favor")
            '    msgMail.To.Clear()
            '    msgMail.From = New System.Net.Mail.MailAddress(_De)
            '    Dim iMails() As String = Split(_Para, ";")
            '    For Each mail As String In iMails
            '        msgMail.To.Add(mail)
            '    Next
            '    If _bcc <> "" Then
            '        Dim iMailsbcc() As String = Split(_bcc, ";")
            '        For Each mail As String In iMailsbcc
            '            msgMail.Bcc.Add(mail)
            '        Next

            '    End If

            '    'msgMail.Bcc.Add("daltoninternet@gmail.com")
            '    msgMail.DeliveryNotificationOptions = Net.Mail.DeliveryNotificationOptions.OnFailure
            '    Dim iMailsReplyTo() As String = Split(_replayto, ";")
            '    For Each mailReply As String In iMailsReplyTo
            '        msgMail.ReplyTo = New System.Net.Mail.MailAddress(mailReply)
            '    Next

            '    msgMail.Subject = _Asunto
            '    msgMail.Body = _Mensaje
            '    msgMail.IsBodyHtml = True
            '    msgMail.Priority = System.Net.Mail.MailPriority.Normal

            '    'Dim EmailServer As New System.Net.Mail.SmtpClient("daltonexcmail1") 
            '    'EmailServer.Credentials = New System.Net.NetworkCredential("daltoninternet@gmail.com", "th3b3s7", "daltonexcmail1")
            '    'EmailServer.SendAsync()


            '    Dim SmtpMail As New System.Net.Mail.SmtpClient(_SMTP, _puerto)
            '    If _User <> "" Then SmtpMail.Credentials = New System.Net.NetworkCredential(_User, _Password)

            '    'SmtpMail.UseDefaultCredentials = True
            '    'TODO: INDICAR QUE SE PUEDA MANDAR EL MAIL ANTES DE REALIZAR UNA PUBLICACION
            '    SmtpMail.Send(msgMail)

            '    Return True
            'Catch ex As System.Net.Mail.SmtpException

            '    Throw New Exception("No se ha encontrado el smtp, verifique por favor", ex.InnerException)
            'Catch ex As Exception

            '    Throw New Exception(ex.Message, ex.InnerException)

            'End Try

        End Function

        Public Function EnviaCorreoPar(ByVal De As String, ByVal Para As String, ByVal Mensaje As String, ByVal Asunto As String, ByVal SMTP As String) As Boolean Implements IMail.EnviaCorreoPar
            Try
                If De.Trim() <> "" Then
                    msgMail.From = New System.Net.Mail.MailAddress(De)
                    Dim iMails() As String = Split(Para, ";")
                    For Each mail As String In iMails
                        msgMail.To.Add(mail)
                    Next
                    Dim iMailsbcc() As String = Split(_bcc, ";")
                    For Each mail As String In iMailsbcc
                        If (mail <> "") Then
                            msgMail.Bcc.Add(mail)
                        End If
                    Next
                    If replayto <> "" Then
                        msgMail.ReplyTo = New System.Net.Mail.MailAddress(_replayto)
                    End If

                    msgMail.Subject = Asunto
                    msgMail.Body = Mensaje
                    msgMail.IsBodyHtml = True
                    msgMail.Priority = System.Net.Mail.MailPriority.Normal

                    Dim SmtpMail As New System.Net.Mail.SmtpClient(SMTP, _puerto)
                    SmtpMail.Credentials = New System.Net.NetworkCredential(_User, _Password)
                    SmtpMail.Send(msgMail)
                    Return True
                Else
                    Return True
                End If
            Catch ex As System.Net.Mail.SmtpException

                Throw New Exception("No se ha encontrado el smtp, verifique por favor", ex.InnerException)
            Catch ex As Exception

                Throw New Exception(ex.Message, ex.InnerException)

            End Try
        End Function



        Public Function EnviaCorreoPar(ByVal De As String, ByVal Para As String, ByVal Mensaje As String, ByVal Asunto As String, ByVal SMTP As String, ByVal files() As String, ByVal replayto As String) As Boolean
            Try
                msgMail.Attachments.Clear()
                For Each file As String In files
                    Dim data As New Attachment(file)
                    Dim disposition As ContentDisposition = data.ContentDisposition
                    disposition.CreationDate = System.IO.File.GetCreationTime(file)
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(file)
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(file)

                    msgMail.Attachments.Add(data)
                Next

                msgMail.From = New MailAddress(De)
                Dim iMails() As String = Split(Para, ";")
                For Each mail As String In iMails
                    msgMail.To.Add(mail)
                Next

                Dim iMailsbcc() As String = Split(_bcc, ";")
                For Each mail As String In iMailsbcc
                    msgMail.Bcc.Add(mail)
                Next
                msgMail.ReplyTo = New MailAddress(replayto)
                msgMail.Subject = Asunto
                msgMail.Body = Mensaje
                msgMail.IsBodyHtml = True
                msgMail.Priority = System.Net.Mail.MailPriority.Normal

                Dim SmtpMail As New System.Net.Mail.SmtpClient(SMTP, _puerto)

                SmtpMail.Credentials = New System.Net.NetworkCredential(_User, _Password)
                SmtpMail.Send(msgMail)
                Return True
            Catch ex As System.Net.Mail.SmtpException

                Throw New Exception("No se ha encontrado el smtp, verifique por favor", ex.InnerException)
            Catch ex As Exception

                Throw New Exception(ex.Message, ex.InnerException)
            Finally
                Try
                    For Each file As Attachment In msgMail.Attachments
                        If file IsNot Nothing Then file.Dispose()
                    Next
                Catch ex As Exception

                End Try


            End Try
        End Function

        Public Property bcc() As String Implements IMail.bcc
            Get
                Return _bcc
            End Get
            Set(ByVal value As String)
                _bcc = value
            End Set
        End Property

        Public Property replayto() As String Implements IMail.replayto
            Get
                Return _replayto
            End Get
            Set(ByVal value As String)
                _replayto = value
            End Set
        End Property

        Public Property Password() As String Implements IMail.Password
            Get
                Return _Password
            End Get
            Set(ByVal value As String)
                _Password = value
            End Set
        End Property

        Public Property User() As String Implements IMail.User
            Get
                Return _User
            End Get
            Set(ByVal value As String)
                _User = value
            End Set
        End Property
        Private _puerto As Integer = 25
        Public Property puerto() As Integer Implements IMail.puerto
            Get
                Return _puerto
            End Get
            Set(ByVal value As Integer)
                _puerto = value
            End Set
        End Property


        Public Shared Function EnviaMailHtml(ByVal Para As String, ByVal De As String, ByVal Subject As String, ByVal Body As String) As Boolean

            Dim msgMail As New System.Net.Mail.MailMessage
            'Dim smtpServer As String =' 'Recuperamos el fichero que vamos a enviar por mail
            Dim SmtpMail As SmtpClient
            Try
                'msgMail.From = New MailAddress("Dalton@colomoscountry.com.mx", "DaltonWeb")
                msgMail.From = New MailAddress("dalton@colomoscountry.com.mx", "Dalton")

                'MailErrores()
                '
                ' msgMail.To.Add("daltoninternet@gmail.com")
                'msgMail.To.Add("daltoninternet@gmail.com")
                Dim MatTo As String() = Para.Split(CChar(";"))
                For Each mailto As String In MatTo
                    msgMail.To.Add(mailto.ToString.Trim)
                Next
                msgMail.Bcc.Add("daltoninternet@gmail.com")
                msgMail.Subject = Subject '"Correo de prueba"
                msgMail.Body = Body '"<h1>Hola mundo</h1>" & Now.ToString
                msgMail.IsBodyHtml = True
                msgMail.Priority = MailPriority.Normal
                SmtpMail = New SmtpClient("daltonexcmail1", 25)
                SmtpMail.Send(msgMail)
                Return True
            Finally
                msgMail.Dispose()
                SmtpMail = Nothing
            End Try
        End Function
    End Class
End Namespace