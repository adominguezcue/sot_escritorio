
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/22/2017 12:27:44
-- Generated from EDMX file: C:\Users\developer\Source\Trabajo\Git\EngraneDigital\sot_global\Datos.SAT\Modelo\ModeloSAT.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SOT];
GO
IF SCHEMA_ID(N'SAT') IS NULL EXECUTE(N'CREATE SCHEMA [SAT]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[SAT].[ClavesProductosServicios]', 'U') IS NOT NULL
    DROP TABLE [SAT].[ClavesProductosServicios];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ClavesProductosServicios'
CREATE TABLE [SAT].[ClavesProductosServicios] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CodigoClaveProductoServicio] nvarchar(max)  NULL,
    [Descripcion] nvarchar(255)  NULL,
    [FechaInicioVigencia] datetime  NULL,
    [FechaFinVigencia] nvarchar(255)  NULL,
    [IncluirIVATrasladado] bit  NULL,
    [IncluirIEPSTrasladado] bit  NULL,
    [ComplementoIncluir] nvarchar(255)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ClavesProductosServicios'
ALTER TABLE [SAT].[ClavesProductosServicios]
ADD CONSTRAINT [PK_ClavesProductosServicios]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------