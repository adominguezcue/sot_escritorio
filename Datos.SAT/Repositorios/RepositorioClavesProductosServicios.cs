﻿using Datos.Nucleo.Repositorios;
using Modelo.SAT.Entidades;
using Modelo.SAT.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.SAT.Repositorios
{
    public class RepositorioClavesProductosServicios : Repositorio<ClaveProductoServicio>, IRepositorioClavesProductosServicios
    {
        public RepositorioClavesProductosServicios() : base(new Contextos.SATContext()) { }
    }
}
