﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Transversal.Excepciones;

namespace SOTWCF.ExceptionHandlers
{
    public class SOTExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            try
            {
                if (!typeof(SOTException).IsAssignableFrom(context.Exception.GetType()))
                {

                    var currentResult = (System.Web.Http.Results.ExceptionResult)context.Result;

                    context.Result = new System.Web.Http.Results.ExceptionResult(new SOTException("Ha ocurrido un error inesperado en el sistema"),
                                                                                 currentResult.IncludeErrorDetail,
                                                                                 currentResult.ContentNegotiator,
                                                                                 context.ExceptionContext.Request,
                                                                                 currentResult.Formatters);


                }
            }
            catch { }
        }

        private class SOTErrorResult : IHttpActionResult
        {
            public HttpRequestMessage Request { get; set; }

            public string Content { get; set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(Content);
                response.RequestMessage = Request;
                return Task.FromResult(response);
            }
        }
    }
}