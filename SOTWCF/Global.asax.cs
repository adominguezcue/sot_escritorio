﻿using SOTWCF.ExceptionHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SOTWCF
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public WebApiApplication()
        {
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            GlobalConfiguration.Configuration.Services.Replace(typeof(IExceptionHandler), new SOTExceptionHandler());
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

            //HttpConfiguration config = GlobalConfiguration.Configuration;
            //config.Formatters.JsonFormatter.SerializerSettings.Formatting =
            //    Newtonsoft.Json.Formatting.Indented;

            //config.Formatters.JsonFormatter.SerializerSettings.Converters.Add
            //    (new Newtonsoft.Json.Converters.StringEnumConverter());
        }

        //public static void RegisterRoutes(RouteCollection routes)
        //{
        //    routes.MapHttpRoute(name: "DefaultApi", routeTemplate: "api/{controller}/{id}", defaults: new { id = RouteParameter.Optional });
        //    routes.MapHttpRoute(name: "DefaultValuesApi", routeTemplate: "api/{controller}/{filtro}", defaults: new { filtro = RouteParameter.Optional });
        //    routes.MapHttpRoute(name: "Default", routeTemplate: "{controller}/{action}/{id}", defaults: new { controller = "Home", action = "Index", id = RouteParameter.Optional });
        //}
    }
}
