﻿using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Sesiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Transversal.Dependencias;
using SOTWCF.Extensiones;
using SOTWCF.Nucleo;

namespace SOTWCF.Controllers.Seguridad
{
    [RoutePrefix("api/Sesiones")]
    public class SesionesController : ApiController
    {
        private IServicioSesiones AplicacionServicioSesiones
        {
            get { return _aplicacionServicioSesiones.Value; }
        }

        private Lazy<IServicioSesiones> _aplicacionServicioSesiones = new Lazy<IServicioSesiones>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioSesiones>();
        });

        //// GET: api/Sesiones
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET: api/Sesiones/5
        //public string Get(string token)
        //{
        //    return "value";
        //}
        //[HttpOptions]
        //[Route("")]
        //public HttpResponseMessage Options()
        //{
        //    var r = new HttpResponseMessage { StatusCode = HttpStatusCode.OK };

        //    r.Headers.Add("Access-Control-Allow-Origin", "*");

        //    return r;
        //}

        // POST: api/Sesiones
        //[HttpPost]
        [Route("")]
        public DtoUsuario Post(DtoCredencial credencial)
        {
            return AplicacionServicioSesiones.IniciarSesion(credencial);
        }

        // PUT: api/Sesiones/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE: api/Sesiones/5
        [HttpDelete]
        [Route("{token}")]
        public string Delete(string token)
        {
            AplicacionServicioSesiones.CerrarSesion(token);
            ApiControllerExtension.QuitarSesion(token);

            return "Sesión cerrada";
        }
    }
}
