﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Gastos
{
    /// <summary>
    /// Lógica de interacción para CreacionGastosUC.xaml
    /// </summary>
    public partial class CreacionGastosUC : UserControl
    {
        private static readonly RoutedEvent CreacionExitosaEvent =
            EventManager.RegisterRoutedEvent("CreacionExitosa", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(CreacionGastosUC));

        /// <summary>The ValueChanged event is called when the TextBoxValue of the control changes.</summary>
        public event RoutedEventHandler CreacionExitosa
        {
            add { AddHandler(CreacionExitosaEvent, value); }
            remove { RemoveHandler(CreacionExitosaEvent, value); }
        }



        public bool EsRevisionCorte
        {
            get { return (bool)GetValue(EsRevisionCorteProperty); }
            set { SetValue(EsRevisionCorteProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EsRevisionCorte.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EsRevisionCorteProperty =
            DependencyProperty.Register("EsRevisionCorte", typeof(bool), typeof(CreacionGastosUC), new PropertyMetadata(false));

        public CreacionGastosUC()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                CargarConceptos();
        }

        private void CargarConceptos()
        {
            var controlador = new ControladorConceptosGastos();

            var conceptos = controlador.ObtenerConceptosPagablesEnCaja();

            lbGastos.ItemsSource = conceptos;
        }

        internal void Aceptar(int? idCorteRevision = null)
        {
            var concepto = lbGastos.SelectedItem as Modelo.Entidades.ConceptoGasto;

            if (concepto != null) 
            {
                var gasto = new Modelo.Entidades.Gasto
                {
                    Activo = true,
                    IdConceptoGasto = concepto.Id,
                    Valor = nudImporte.Number//nudImporte.Value.HasValue ? (decimal)nudImporte.Value : 0
                };

                if (EsRevisionCorte && gasto != null)
                    gasto.IdCorteTurno = idCorteRevision ?? 0;

                var controlador = new ControladorGastos();

                controlador.Crear(gasto, EsRevisionCorte);

                nudImporte.Number = 0;

                RaiseEvent(new RoutedEventArgs(CreacionExitosaEvent));
            }
        }
    }
}
