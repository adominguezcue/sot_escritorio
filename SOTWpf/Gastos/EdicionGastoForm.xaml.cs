﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Gastos
{
    /// <summary>
    /// Lógica de interacción para EdicionGastoForm.xaml
    /// </summary>
    public partial class EdicionGastoForm : Window
    {
        public EdicionGastoForm(Modelo.Entidades.Gasto gasto = null)
        {
            InitializeComponent();

            DataContext = gasto;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var gasto = DataContext as Modelo.Entidades.Gasto;
            
            if (gasto != null)
            {
                var controladorG = new SOTControladores.Controladores.ControladorGastos();
                controladorG.ModificarGasto(gasto.Id, gasto.Valor);

                MessageBox.Show(Textos.Mensajes.modificacion_gasto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                Close();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
