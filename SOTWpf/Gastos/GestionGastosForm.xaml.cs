﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Gastos
{
    /// <summary>
    /// Lógica de interacción para GestionGastosForm.xaml
    /// </summary>
    public partial class GestionGastosForm : Window
    {
        public GestionGastosForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //CargarConceptos();
            CargarGastos();
        }

        private void CargarGastos()
        {
            var controladorCortes = new ControladorCortesTurno();

            var corte = controladorCortes.ObtenerUltimoCorte();

            var controlador = new ControladorGastos();

            var gastos = controlador.ObtenerGastosPorTurno(corte.Id, true);

            lbRegistrados.ItemsSource = gastos;
        }

        //private void CargarConceptos()
        //{
        //    var controlador = new ControladorConceptosGastos();

        //    var conceptos = controlador.ObtenerConceptosPagablesEnCaja();

        //    lbGastos.ItemsSource = conceptos;
        //}

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            //var concepto = lbGastos.SelectedItem as Modelo.Entidades.ConceptoGasto;

            //if (concepto != null) 
            //{
            //    var gasto = new Modelo.Entidades.Gasto
            //    {
            //        Activo = true,
            //        IdConceptoGasto = concepto.Id,
            //        Valor = nudImporte.Number//nudImporte.Value.HasValue ? (decimal)nudImporte.Value : 0
            //    };

            //    var controlador = new ControladorGastos();

            //    controlador.Crear(gasto, false);

            //    nudImporte.Number = 0;

            //    CargarGastos();
            //}

            creacionG_UC.Aceptar();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnCancelarGasto_Click(object sender, RoutedEventArgs e)
        {
            var gasto = lbRegistrados.SelectedItem as Modelo.Entidades.Gasto;

            if (gasto != null && MessageBox.Show(string.Format(Textos.Mensajes.confirmar_cancelacion_gasto, gasto.ConceptoInstantaneo, gasto.Valor.ToString("C")), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorGastos();

                controlador.CancelarGasto(gasto.Id);

                MessageBox.Show(Textos.Mensajes.cancelacion_gasto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarGastos();
            }
        }

        private void creacionG_UC_CreacionExitosa(object sender, RoutedEventArgs e)
        {
            CargarGastos();
        }

        private void CreacionG_UC_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
