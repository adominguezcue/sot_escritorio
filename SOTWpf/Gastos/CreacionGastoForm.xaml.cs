﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Gastos
{
    /// <summary>
    /// Lógica de interacción para CreacionGastoForm.xaml
    /// </summary>
    public partial class CreacionGastoForm : Window
    {
        int? idCorteRevision;

        public CreacionGastoForm(int? idCorteRevision)
        {
            InitializeComponent();

            this.idCorteRevision = idCorteRevision;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            creacionG_UC.Aceptar(idCorteRevision);
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void creacionG_UC_CreacionExitosa(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
