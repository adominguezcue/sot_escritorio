﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SOTWpf.TareasMantenimiento.SeccionesCalendarioMantenimientoForm
{
    public class TMRoutedEventArgs : RoutedEventArgs
    {
        public DateTime Fecha { get; private set; }
        public TMRoutedEventArgs(DateTime fechaItemEliminado) 
        {
            Fecha = fechaItemEliminado;
        }
    }
}
