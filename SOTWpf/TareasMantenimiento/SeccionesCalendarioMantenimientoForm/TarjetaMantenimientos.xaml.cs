﻿using MaterialDesignThemes.Wpf;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.TareasMantenimiento.SeccionesCalendarioMantenimientoForm
{
    /// <summary>
    /// Lógica de interacción para TarjetaMantenimientos.xaml
    /// </summary>
    public partial class TarjetaMantenimientos : UserControl
    {
        public delegate void TMEventHandler(object sender, TMRoutedEventArgs e);

        private static readonly RoutedEvent TareaEliminadaEvent =
            EventManager.RegisterRoutedEvent("TareaEliminada", RoutingStrategy.Bubble,
            typeof(TMEventHandler), typeof(TarjetaMantenimientos));

        public event TMEventHandler TareaEliminada
        {
            add { AddHandler(TareaEliminadaEvent, value); }
            remove { RemoveHandler(TareaEliminadaEvent, value); }
        }

        public DateTime Fecha { get; private set; }

        int idHabitacion;

        public TarjetaMantenimientos(DateTime fecha, List<Modelo.Dtos.DtoTareaMantenimientoBase> tareas, int idHabitacion = 0)
        {
            InitializeComponent();
            txtDia.Text = fecha.Day.ToString();
            this.Fecha = fecha;
            this.idHabitacion = idHabitacion;

            icTareas.ItemsSource = tareas;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        internal void RecargarTareas(List<Modelo.Dtos.DtoTareaMantenimientoBase> tareas)
        {
            icTareas.ItemsSource = tareas;
        }

        private async void MostrarEditor(int idTarea)
        {

            var editorForm = new EdicionTareaMantenimientoForm(idTarea) { Width = 600, Height = 350 };
            //editorForm.EdicionRealizada += BotoneraBase_AceptarClick;
            //editorForm.EdicionCancelada += BotoneraBase_CancelarClick;

            await DialogHost.Show(editorForm, "dialogHostPrincipal");
        }

        //private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        //{
        //    if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(null))
        //        ((ICommand)DialogHost.CloseDialogCommand).Execute(false);  
        //}

        //private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        //{
        //    if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(null))
        //        ((ICommand)DialogHost.CloseDialogCommand).Execute(true);  
        //}

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var tarea = ((Control)sender).DataContext as Modelo.Dtos.DtoTareaMantenimientoBase;

            MostrarEditor(tarea != null ? tarea.Id : 0);
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var tarea = ((Control)sender).DataContext as Modelo.Dtos.DtoTareaMantenimientoBase;

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_tarea_mantenimiento, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorTareas = new ControladorTareasMantenimiento();
                controladorTareas.EliminarTarea(tarea.Id);

                //if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(tarea.FechaInicio))
                //    ((ICommand)DialogHost.CloseDialogCommand).Execute(tarea.FechaInicio);

                var args = new TMRoutedEventArgs(tarea.FechaSiguiente);
                args.RoutedEvent = TareaEliminadaEvent;

                RaiseEvent(args);
            }
        }
    }
}
