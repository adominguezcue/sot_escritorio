﻿using Modelo.Dtos;
using SOTControladores.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.TareasMantenimiento
{
    /// <summary>
    /// Lógica de interacción para SelectorTareaMantenimiento.xaml
    /// </summary>
    public partial class SelectorTareaMantenimientoForm : Window
    {
        int idHabitacion;

        ObservableCollection<CheckableItemWrapper<DtoTareaMantenimientoBase>> lista = new ObservableCollection<CheckableItemWrapper<DtoTareaMantenimientoBase>>();

        public SelectorTareaMantenimientoForm(int idHabitacion)
        {
            InitializeComponent();

            this.idHabitacion = idHabitacion;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!lista.Any(m => m.IsChecked))
                throw new SOTException("Seleccione un mantenimiento a realizar");

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            foreach (var item in lista)
            {
                item.IsChecked = false;
            }

            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var controladorTareasMantenimiento = new SOTControladores.Controladores.ControladorTareasMantenimiento();

            var fecha = DateTime.Now;

            foreach (var tarea in controladorTareasMantenimiento.ObtenerTareasPorMes(fecha.Month, fecha.Year, idHabitacion))
                lista.Add(new CheckableItemWrapper<DtoTareaMantenimientoBase> { Value = tarea });

            listaMarcable.ItemsSource = lista;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as CheckableItemWrapper<DtoTareaMantenimientoBase>;

            foreach (var item in lista)
            {
                if (item == marcado)
                    continue;

                item.IsChecked = false;
            }
        }

        public int? IdTareaSeleccionada 
        {
            get { return lista.Where(m => m.IsChecked).Select(m => (int?)m.Value.Id).FirstOrDefault(); }
        }
    }
}
