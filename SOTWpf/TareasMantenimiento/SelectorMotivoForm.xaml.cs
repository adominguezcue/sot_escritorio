﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.TareasMantenimiento
{
    /// <summary>
    /// Lógica de interacción para SelectorMotivoForm.xaml
    /// </summary>
    public partial class EnvioMantenimientoForm : Window
    {
        public bool EsValido
        {
            get;
            private set;
        }

        public int IdConceptoMantenimiento
        {
            get 
            {
                var concepto = cbConceptosMantenimiento.SelectedItem as ConceptoMantenimiento;
                return (concepto != null && EsValido) ? concepto.Id : 0;
            }
            //set { txtMotivo.Text = value; }
        }

        public string Descripcion
        {
            get { return EsValido ? txtDescripcion.Text : null; }
        }

        public EnvioMantenimientoForm()
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();

            var conceptos = new ControladorConceptosMantenimiento().ObtenerConceptos();

            cbConceptosMantenimiento.ItemsSource = conceptos;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            EsValido = true;
            this.Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
