﻿using ReportesSOT.Dtos;					   
using SOTControladores.Controladores;
using SOTWpf.Dtos;				  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.TareasMantenimiento
{
    /// <summary>
    /// Lógica de interacción para HistorialMantenimientoForm.xaml
    /// </summary>
    public partial class HistorialMantenimientoForm : Window
    {
        int idHabitacion;
        List<Modelo.Dtos.DtoResumenMantenimiento> ListaMantenimiento = new List<Modelo.Dtos.DtoResumenMantenimiento>();
        List<Modelo.Dtos.DtoResumenHabitacion> Listahabitacion = new List<Modelo.Dtos.DtoResumenHabitacion>();												   
        public HistorialMantenimientoForm(int idHabitacion)
        {
            InitializeComponent();

            this.idHabitacion = idHabitacion;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorMantenimientos();
            var controladorHabitra = new ControladorHabitaciones();
            ListaMantenimiento = controlador.ObtenerMantenimientosHabitacion(idHabitacion);
            ListaMantenimiento = ListaMantenimiento.OrderBy(m => m.Fecha).Reverse().ToList();
            Listahabitacion.Add(controladorHabitra.SP_ObtenerResumenHabitacion(idHabitacion,0));
            CargaHabitacion();
            dgvMantenimiento.ItemsSource = ListaMantenimiento;
        }

        private void CargaHabitacion()
        {
          if(ListaMantenimiento.Count>0 && Listahabitacion.Count>0)
            {
                foreach (Modelo.Dtos.DtoResumenHabitacion habi in Listahabitacion)
                {
                    titulohabion.Text = habi.NumeroHabitacion.ToString();
                }
            }
        }

        private void BtnReporte_Click(object sender, RoutedEventArgs e)
        {
            if(ListaMantenimiento.Count>0 && Listahabitacion.Count>0)
            {
                List<Modelo.Dtos.DtoMantenimientoHabitacionReporte> reporte = new List<Modelo.Dtos.DtoMantenimientoHabitacionReporte>();
                foreach (Modelo.Dtos.DtoResumenHabitacion habi in Listahabitacion)
                {
                    Modelo.Dtos.DtoMantenimientoHabitacionReporte dato = new Modelo.Dtos.DtoMantenimientoHabitacionReporte();
                    dato.Habitacion = Convert.ToInt32(habi.NumeroHabitacion);
                    dato.TipoHabitacion = habi.NombreTipo;
                    reporte.Add(dato);
                }
                List<Modelo.Dtos.DtoMantenimientoResumenReporte> reporte1 = new List<Modelo.Dtos.DtoMantenimientoResumenReporte>();
                foreach (Modelo.Dtos.DtoResumenMantenimiento datos in ListaMantenimiento)
                {
                    Modelo.Dtos.DtoMantenimientoResumenReporte dato1 = new Modelo.Dtos.DtoMantenimientoResumenReporte();
                    dato1.Fecha = datos.Fecha;
                    dato1.NombreUsuario = datos.NombreUsuario;
                    dato1.Descripcion = datos.Descripcion;
                    dato1.Concepto = datos.Concepto;
                    reporte1.Add(dato1);
                }
                var documentoReporte = new SOTWpf.Reportes.IncidenciasHabitacion.HistorialMentenimientoHabitacion();
                documentoReporte.Load();
                var controladorGlobal = new ControladorConfiguracionGlobal();
                var config = controladorGlobal.ObtenerConfiguracionGlobal();
                System.DateTime localDate = System.DateTime.Now;
                documentoReporte.SetDataSource(new List<Dtos.DtoCabeceraReporte> {new Dtos.DtoCabeceraReporte
                    {
                        Direccion=config.Direccion,
                        FechaInicio = localDate,
                        FechaFin = localDate,
                        Hotel = config.Nombre,
                        Usuario = ControladorBase.UsuarioActual.NombreCompleto
                  } });
                documentoReporte.Subreports["Habitacion"].SetDataSource(reporte);
                documentoReporte.Subreports["DetalleMantenimiento"].SetDataSource(reporte1);
                var visorR = new VisorReportes();
                visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                visorR.UpdateLayout();
                Utilidades.Dialogos.MostrarDialogos(this, visorR);
            }
        }

        private void DgvMantenimiento_Sorting(object sender, DataGridSortingEventArgs e)
        {
            bool Orden = false;
            string _switch = "";
            _switch = e.Column.Header.ToString();

            if (e.Column.SortDirection == null || e.Column.SortDirection.Value.ToString() == "Descending")
                Orden = false;
            else if (e.Column.SortDirection.Value.ToString() == "Ascending")
                Orden = true;
            switch (_switch)
            {
                case "FECHA":
                    if (Orden == true)
                        ListaMantenimiento = ListaMantenimiento.OrderByDescending(o => o.Fecha).ToList();
                    else
                        ListaMantenimiento = ListaMantenimiento.OrderBy(o => o.Fecha).ToList();
                    break;
                case "CONCEPTO":
                    if (Orden == true)
                        ListaMantenimiento = ListaMantenimiento.OrderByDescending(o => o.Concepto).ToList();
                    else
                        ListaMantenimiento = ListaMantenimiento.OrderBy(o => o.Concepto).ToList();
                    break;
                case "DESCRIPCIÓN":
                    if (Orden == true)
                        ListaMantenimiento = ListaMantenimiento.OrderByDescending(o => o.Descripcion).ToList();
                    else
                        ListaMantenimiento = ListaMantenimiento.OrderBy(o => o.Descripcion).ToList();
                    break;
                default:
                    break;
            }
        }
    }
}
