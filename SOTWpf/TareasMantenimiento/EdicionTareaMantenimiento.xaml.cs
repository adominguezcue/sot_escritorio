﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.TareasMantenimiento
{
    /// <summary>
    /// Lógica de interacción para EdicionTareaMantenimiento.xaml
    /// </summary>
    public partial class EdicionTareaMantenimiento : UserControl
    {
        int idTarea;
        bool esPrimeraCarga = true;

        public DateTime? _ultimaFecha;
        public DateTime? UltimaFecha
        {
            get { return _ultimaFecha ?? dpFechaInicio.SelectedDate; }
            private set { _ultimaFecha = value; }
        }

        private Modelo.Entidades.TareaMantenimiento tareaActual
        {
            get
            {
                return DataContext as Modelo.Entidades.TareaMantenimiento;
            }
            set
            {
                DataContext = value;
            }
        }

        public EdicionTareaMantenimiento()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicio.Language = lang;
            dpFechaFin.Language = lang;

            //dpFechaFin.SelectedDate = dpFechaInicio.SelectedDate = DateTime.Now;
        }

        public void Aceptar()
        {
            var controladorTareas = new ControladorTareasMantenimiento();
            tareaActual.TipoMantenimiento = (Modelo.Entidades.TareaMantenimiento.TiposMantenimiento)(cbTiposMantenimiento.SelectedItem ?? 0);
            tareaActual.Periodicidad = chbRepetir.IsChecked.Value ? cbPeriodicidades.SelectedValue as Modelo.Entidades.Periodicidades? : null;

            if (chbRepetir.IsChecked.Value != tareaActual.Periodicidad.HasValue)
                throw new SOTException(tareaActual.Periodicidad.HasValue ?
                    Textos.Errores.periodicidad_tarea_no_necesaria_exception :
                    Textos.Errores.periodicidad_tarea_necesaria_exception);

            if (!chbRepetir.IsChecked.Value)
                tareaActual.FechaFin = null;

            //var item = cbTiempoAnticipacion.SelectedItem as ComboBoxItem;
            //if (item != null)
            //    tareaActual.MinutosAnticipacion = Convert.ToInt32(item.Tag) * (int)nudContadorTiempo.Value;

            if (tareaActual.Id == 0)
            {
                controladorTareas.CrearTarea(tareaActual.ObtenerCopiaDePrimitivas());
            }
            else
            {
                var nueva = new Modelo.Entidades.TareaMantenimiento { Id = tareaActual.Id };
                nueva.SincronizarPrimitivas(tareaActual);
                controladorTareas.ModificarTarea(nueva);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (!esPrimeraCarga)
                    return;

                CargarTarea();

                esPrimeraCarga = false;
            }
        }

        internal void CargarTarea(int idTarea)
        {
            this.idTarea = idTarea;
        }

        private void CargarTarea()
        {
            var controladorHabitaciones = new ControladorHabitaciones();

            cbHabitaciones.ItemsSource = controladorHabitaciones.ObtenerActivasConTipos();

            var controlador = new ControladorConceptosMantenimiento();

            var conceptos = controlador.ObtenerConceptos();

            cbConceptosMantenimiento.ItemsSource = conceptos;
            cbTiposMantenimiento.ItemsSource = Transversal.Extensiones.EnumExtensions.ComoLista<Modelo.Entidades.TareaMantenimiento.TiposMantenimiento>();
            cbPeriodicidades.ItemsSource = Transversal.Extensiones.EnumExtensions.ComoLista<Modelo.Entidades.Periodicidades>();


            if (idTarea == 0)
            {
                tareaActual = new Modelo.Entidades.TareaMantenimiento { FechaInicio = DateTime.Now };

                _ultimaFecha = null;
            }
            else
            {
                var controladorTareasMantenimiento = new ControladorTareasMantenimiento();

                var tarea = controladorTareasMantenimiento.ObtenerTareaPorId(idTarea);

                if (tarea == null)
                    throw new SOTException(Textos.Errores.tarea_mantenimiento_no_modificable_exception, idTarea.ToString());

                tareaActual = tarea;
                cbTiposMantenimiento.SelectedItem = tareaActual.TipoMantenimiento;
                //tareaActual.TipoMantenimiento = (Modelo.Entidades.TareaMantenimiento.TiposMantenimiento)(cbTiposMantenimiento.SelectedItem ?? 0);
                cbPeriodicidades.SelectedValue = tareaActual.Periodicidad;
                chbRepetir.IsChecked = tareaActual.Periodicidad.HasValue;
                //tareaActual.Periodicidad = chbRepetir.IsChecked.Value ? cbPeriodicidades.SelectedValue as Modelo.Entidades.Periodicidades? : null;

                //if (!chbRepetir.IsChecked.Value)
                //    tareaActual.FechaFin = null;

                //var item = cbTiempoAnticipacion.SelectedItem as ComboBoxItem;
                //if (item != null)
                //    tareaActual.MinutosAnticipacion = Convert.ToInt32(item.Tag) * (int)nudContadorTiempo.Value;
                _ultimaFecha = tareaActual.FechaInicio;
            }
        }

        //private void NumericUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    if (!e.NewValue.HasValue)
        //        nudContadorTiempo.Value = 0;
        //}
    }
}
