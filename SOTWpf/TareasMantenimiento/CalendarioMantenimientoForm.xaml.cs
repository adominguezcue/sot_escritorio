﻿using MaterialDesignThemes.Wpf;
using SOTControladores.Controladores;
using SOTWpf.TareasMantenimiento.SeccionesCalendarioMantenimientoForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SOTWpf.TareasMantenimiento
{
    /// <summary>
    /// Lógica de interacción para CalendarioMantenimientoForm.xaml
    /// </summary>
    public partial class CalendarioMantenimientoForm : Window
    {
        private DispatcherTimer timerRecarga;

        int idHabitacion = -1;

        public CalendarioMantenimientoForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            calendarioNavegacion.Language = lang;
            calendarioNavegacion.DisplayDate = DateTime.Now;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                cbHabitaciones.ItemsSource = controladorHabitaciones.ObtenerActivasConTipos();

                timerRecarga = new DispatcherTimer();
                timerRecarga.Interval = new TimeSpan();
                timerRecarga.Tick += timerRecarga_Tick;

                timerRecarga.Start();
            }
        }

        private void timerRecarga_Tick(object sender, EventArgs e)
        {
            timerRecarga.Interval = TimeSpan.FromMinutes(5);

            RecargarPendientes();
        }

        private void RecargarPendientes()
        {
            var controladorTareasMantenimiento = new SOTControladores.Controladores.ControladorTareasMantenimiento();

            var cantidad = controladorTareasMantenimiento.ObtenerCantidadTareasPendientes();

            if (cantidad != 1)
                txtPendientes.Text = cantidad + " MANTENIMIENTOS PENDIENTES";
            else
                txtPendientes.Text = cantidad + " MANTENIMIENTO PENDIENTE";
        }

        private void btnMenu_Checked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
                cbHabitaciones.Visibility = calendarioNavegacion.Visibility = System.Windows.Visibility.Visible;
            //t.SelectedIndex = 1;
        }

        private void btnMenu_Unchecked(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
                //t.SelectedIndex = 0;
                cbHabitaciones.Visibility = calendarioNavegacion.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void calendarioNavegacion_DisplayDateChanged(object sender, CalendarDateChangedEventArgs e)
        {
            DateTime? fechaI = e.RemovedDate;
            DateTime? fechaF = e.AddedDate;

            if (fechaF.HasValue && (!fechaI.HasValue || fechaI.Value != fechaF.Value))// fechaI.Value.Year != fechaF.Value.Year || fechaI.Value.Month != fechaF.Value.Month))
            {
                var controladorTareasMantenimiento = new SOTControladores.Controladores.ControladorTareasMantenimiento();

                var tareas = controladorTareasMantenimiento.ObtenerTareasPorMes(fechaF.Value.Month, fechaF.Value.Year, idHabitacion);

                var diaSemana = new DateTime(fechaF.Value.Year, fechaF.Value.Month, 1).DayOfWeek;

                int index;

                switch (diaSemana)
                {
                    case DayOfWeek.Monday:
                        index = 0;
                        break;
                    case DayOfWeek.Tuesday:
                        index = 1;
                        break;
                    case DayOfWeek.Wednesday:
                        index = 2;
                        break;
                    case DayOfWeek.Thursday:
                        index = 3;
                        break;
                    case DayOfWeek.Friday:
                        index = 4;
                        break;
                    case DayOfWeek.Saturday:
                        index = 5;
                        break;
                    case DayOfWeek.Sunday:
                        index = 6;
                        break;
                    default:
                        index = -1;
                        break;
                }

                int diaInicial = 1, fila = 0;
                var diasMes = DateTime.DaysInMonth(fechaF.Value.Year, fechaF.Value.Month);

                foreach (TarjetaMantenimientos item in calendario.Children) 
                {
                    item.TareaEliminada -= tarjeta_TareaEliminada;
                }

                calendario.Children.Clear();
                calendario.RowDefinitions.Clear();

                while (diaInicial <= diasMes)
                {
                    calendario.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

                    for (; index < 7 && diaInicial <= diasMes; index++, diaInicial++)
                    {
                        var tarjeta = new TarjetaMantenimientos(fechaF.Value.AddDays(diaInicial - fechaF.Value.Day), tareas.Where(m => m.FechaSiguiente.Day == diaInicial).ToList());
                        tarjeta.TareaEliminada += tarjeta_TareaEliminada;
                        Grid.SetColumn(tarjeta, index);
                        Grid.SetRow(tarjeta, fila);

                        calendario.Children.Add(tarjeta);
                    }
                    fila++;
                    index = 0;
                }
            }
        }

        private void tarjeta_TareaEliminada(object sender, TMRoutedEventArgs e)
        {
            RecargarTareas(e.Fecha);
        }

        //private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        //{
        //    //editor.Aceptar();
        //    //pubAgregar.IsPopupOpen = false;
        //    if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(null))
        //        ((ICommand)DialogHost.CloseDialogCommand).Execute(true);    
        //}

        //private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        //{
        //    //pubAgregar.IsPopupOpen = false;
        //    if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(null))
        //        ((ICommand)DialogHost.CloseDialogCommand).Execute(false);
        //}

        private void RecargarTareas(DateTime ultimaFecha, bool forzar = false)
        {
            if (forzar || (/*editor.UltimaFecha.HasValue && editor.UltimaFecha.Value*/ultimaFecha.Year == calendarioNavegacion.DisplayDate.Year && /*editor.UltimaFecha.Value*/ultimaFecha.Month == calendarioNavegacion.DisplayDate.Month))
            {
                var controladorTareasMantenimiento = new SOTControladores.Controladores.ControladorTareasMantenimiento();

                var tareas = controladorTareasMantenimiento.ObtenerTareasPorMes(calendarioNavegacion.DisplayDate.Month, calendarioNavegacion.DisplayDate.Year, idHabitacion);

                foreach (TarjetaMantenimientos tarjeta in calendario.Children)
                {
                    tarjeta.RecargarTareas(tareas.Where(m => m.FechaSiguiente.Date == tarjeta.Fecha.Date).ToList());
                }
            }

            RecargarPendientes();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            MostrarEditor();
        }

        private async void MostrarEditor()
        {
            var editorForm = new EdicionTareaMantenimientoForm { Width = 600, Height = 350};
            //editorForm.EdicionRealizada += BotoneraBase_AceptarClick;
            //editorForm.EdicionCancelada += BotoneraBase_CancelarClick;

            await DialogHost.Show(editorForm, "dialogHostPrincipal");
        }

        private void dialogHostPrincipal_DialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            var ultimaFecha = eventArgs.Parameter as DateTime?;

            if (ultimaFecha.HasValue)
                RecargarTareas(ultimaFecha.Value);
        }

        private void cbHabitaciones_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var habitacion = cbHabitaciones.SelectedItem as Modelo.Entidades.Habitacion;

            if (habitacion != null)
                idHabitacion = habitacion.Id;
                
            RecargarTareas(calendarioNavegacion.DisplayDate, true);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (timerRecarga != null)
            {
                timerRecarga.Stop();
                timerRecarga.Tick -= timerRecarga_Tick;
            }
        }
    }
}
