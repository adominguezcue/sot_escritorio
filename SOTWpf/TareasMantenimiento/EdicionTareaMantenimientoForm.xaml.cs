﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.TareasMantenimiento
{
    /// <summary>
    /// Lógica de interacción para EdicionTareaMantenimientoForm.xaml
    /// </summary>
    public partial class EdicionTareaMantenimientoForm : UserControl
    {
        //private static readonly RoutedEvent EdicionCanceladaEvent =
        //    EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble,
        //    typeof(RoutedEventHandler), typeof(EdicionTareaMantenimiento));

        //public event RoutedEventHandler EdicionCancelada
        //{
        //    add { AddHandler(EdicionCanceladaEvent, value); }
        //    remove { RemoveHandler(EdicionCanceladaEvent, value); }
        //}

        //private static readonly RoutedEvent EdicionRealizadaEvent =
        //    EventManager.RegisterRoutedEvent("EdicionRealizada", RoutingStrategy.Bubble,
        //    typeof(RoutedEventHandler), typeof(EdicionTareaMantenimiento));

        //public event RoutedEventHandler EdicionRealizada
        //{
        //    add { AddHandler(EdicionRealizadaEvent, value); }
        //    remove { RemoveHandler(EdicionRealizadaEvent, value); }
        //}

        public EdicionTareaMantenimientoForm(int idTareaMantenimiento = 0)
        {
            InitializeComponent();

            editor.CargarTarea(idTareaMantenimiento);
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            editor.Aceptar();
            EjecutarComando(editor.UltimaFecha);
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            EjecutarComando();
        }

        private void EjecutarComando(DateTime? fechaRecarga = null)
        {
            if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(fechaRecarga))
                ((ICommand)DialogHost.CloseDialogCommand).Execute(fechaRecarga);    
        }
    }
}
