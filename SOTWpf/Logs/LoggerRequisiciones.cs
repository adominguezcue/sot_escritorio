﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Targets;

namespace SOTWpf.Logs
{
    [Target("LogRequisiciones")]
    public class LoggerRequisiciones : TargetWithLayout
    {
        internal static Action<string> EscribirLog;

        protected override void Write(LogEventInfo logEvent)
        {
            string logMessage = this.Layout.Render(logEvent);

            if (EscribirLog != null)
                EscribirLog.Invoke(logMessage);
        }
    }
}
