﻿using MaterialDesignThemes.Wpf;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Habitaciones
{
    /// <summary>
    /// Lógica de interacción para EdicionHabitacionesUC.xaml
    /// </summary>
    public partial class EdicionHabitacionesUC : UserControl
    {
        public static readonly RoutedEvent EdicionAceptadaEvent = EventManager.RegisterRoutedEvent("EdicionAceptada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionHabitacionesUC));
        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionHabitacionesUC));

        public event RoutedEventHandler EdicionAceptada
        {
            add { AddHandler(EdicionAceptadaEvent, value); }
            remove { RemoveHandler(EdicionAceptadaEvent, value); }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add { AddHandler(EdicionCanceladaEvent, value); }
            remove { RemoveHandler(EdicionCanceladaEvent, value); }
        }

        private Habitacion HabitacionActual
        {
            get { return DataContext as Habitacion; }
            set { DataContext = value; }
        }

        public EdicionHabitacionesUC()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var hActual = HabitacionActual;

            if (hActual != null)
            {
                var controladorHabitaciones = new SOTControladores.Controladores.ControladorHabitaciones();

                if (hActual.Id == 0)
                    controladorHabitaciones.CrearHabitacion(hActual);
                else
                    controladorHabitaciones.ModificarHabitacion(hActual);
            }

            RaiseEvent(new RoutedEventArgs(EdicionAceptadaEvent));
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controlador = new SOTControladores.Controladores.ControladorTiposHabitacion();
                cbTiposHabitaciones.ItemsSource = controlador.ObtenerTiposActivos();
            }
        }
    }
}
