﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Habitaciones
{
    /// <summary>
    /// Lógica de interacción para RadigrafiaHotelForm.xaml
    /// </summary>
    public partial class RadiografiaHotelForm : Window
    {
        public RadiografiaHotelForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFecha.Language = lang;
            tpHora.Language = lang;

            var fechaActual = DateTime.Now;
            dpFecha.SelectedDate = fechaActual;
            tpHora.DataContext = fechaActual;
        }

        private void dpFecha_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (e.AddedItems.Count > 0)
            //{
            //    var fechaI = e.AddedItems[0] as DateTime?;

            //    if (fechaI.HasValue)
            //    {
            //        if (e.RemovedItems.Count > 0)
            //            fechaI = e.RemovedItems[0] as DateTime?;
            //        else
            //            fechaI = null;

            //        if (fechaI.HasValue && dpFecha.SelectedDate != fechaI.Value)
            //            dpFecha.SelectedDate = dpFecha.SelectedDate.Value.AddHours(fechaI.Value.Hour - dpFecha.SelectedDate.Value.Hour)
            //                                                              .AddMinutes(fechaI.Value.Minute - dpFecha.SelectedDate.Value.Minute)
            //                                                              .AddSeconds(fechaI.Value.Second - dpFecha.SelectedDate.Value.Second);
            //    }
            //}

            CargarResumenes();
        }

        private void CargarResumenes()
        {
            if (IsLoaded)
            {
                dgvResumenes.ItemsSource = dpFecha.SelectedDate.HasValue && tpHora.SelectedTime.HasValue ?
                                           new SOTControladores.Controladores.ControladorTiposHabitacion().ObtenerEstadosPorTipoFecha(new DateTime
                                               (
                                                dpFecha.SelectedDate.Value.Year,
                                                dpFecha.SelectedDate.Value.Month,
                                                dpFecha.SelectedDate.Value.Day,
                                                tpHora.SelectedTime.Value.Hour,
                                                tpHora.SelectedTime.Value.Minute,
                                                tpHora.SelectedTime.Value.Second
                                               )) :
                                           new List<Modelo.Dtos.DtoCantidadesHabitacionesTipo>();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarResumenes();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnReporte_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new SOTWpf.Reportes.Habitaciones.RadiografiaHotel();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = dpFecha.SelectedDate.HasValue ? dpFecha.SelectedDate.Value : new DateTime(),
                FechaFin = dpFecha.SelectedDate.HasValue ? dpFecha.SelectedDate.Value : new DateTime(),
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            var items = dgvResumenes.ItemsSource as List<Modelo.Dtos.DtoCantidadesHabitacionesTipo>;

            documentoReporte.Subreports["Resumenes"].SetDataSource(items);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }

        private void tpHora_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null && e.OldValue != null)
                (sender as Control).DataContext = e.OldValue;
            else if((e.NewValue as DateTime?) != (e.OldValue as DateTime?))
                CargarResumenes();
        }
    }
}
