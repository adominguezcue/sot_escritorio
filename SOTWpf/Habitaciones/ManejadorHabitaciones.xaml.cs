﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Empleados;
using SOTWpf.Fajillas;
using SOTWpf.TareasLimpieza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;
using SOTWpf.Rentas;
using SOTWpf.Comandas.Manejadores;
using SOTWpf.ConceptosSistema;
using SOTWpf.Matriculas;
using SOTWpf.TareasMantenimiento;
using SOTWpf.VPoints;
using SOTWpf.Reservaciones;
using SOTWpf.Facturacion;
using SOTWpf.OrdenesTrabajo;
using SOTWpf.Dtos;
using Transversal.Extensiones;
using System.Windows.Threading;
using System.Timers;
using System.Threading;

namespace SOTWpf.Habitaciones
{
    /// <summary>
    /// Lógica de interacción para ManejadorHabitaciones.xaml
    /// </summary>
    public partial class ManejadorHabitaciones : UserControl
    {
        private readonly object bloqueador = new object();
        private bool recargando = false;

        
        UCHabitacion habitacionSeleccionada
        {
            get { return panelHabitaciones.HabitacionSeleccionada; }
            set { panelHabitaciones.HabitacionSeleccionada = value; }
        }

        Dictionary<Habitacion.EstadosHabitacion, List<Componentes.Buttons.FloatingIconButton>> diccionarioBotones;
        Action<UCHabitacion> accionCambio = null;

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MenuAbiertoProperty =
            DependencyProperty.Register("MenuAbierto", typeof(bool), typeof(ManejadorHabitaciones),
              new PropertyMetadata(false));

        internal bool TimerHabilitado
        {
            get { return panelHabitaciones.TimerHabilitado; }
            set
            {
                panelHabitaciones.TimerHabilitado = value;
            }
        }

        public bool MenuAbierto
        {
            get
            {
                return (bool)GetValue(MenuAbiertoProperty);
            }
            set
            {
                SetValue(MenuAbiertoProperty, value);
            }
        }

        public ManejadorHabitaciones()
        {
            InitializeComponent();

            btnVentaTarjetas.Flags = btnVPoints.Flags = new Componentes.Buttons.HideableButton.FlagsList() { App.GestionarPuntosLealtad };

            //uc_habitaciones = new List<UCHabitacion>();

            datosHabitacionP.Visibility = Visibility.Collapsed;

            diccionarioBotones = new Dictionary<Habitacion.EstadosHabitacion, List<Componentes.Buttons.FloatingIconButton>>();

            diccionarioBotones[Habitacion.EstadosHabitacion.Bloqueada] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Bloqueada].Add(btnReservacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Bloqueada].Add(btnDeshabilitar);
            diccionarioBotones[Habitacion.EstadosHabitacion.Bloqueada].Add(btnMantenimiento);
            diccionarioBotones[Habitacion.EstadosHabitacion.Bloqueada].Add(btnEntradaHabitacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Limpieza] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Limpieza].Add(btnEstadisticaRec);
            diccionarioBotones[Habitacion.EstadosHabitacion.Limpieza].Add(btnReportarMatricula);
            diccionarioBotones[Habitacion.EstadosHabitacion.Limpieza].Add(btnCambio);
            diccionarioBotones[Habitacion.EstadosHabitacion.Limpieza].Add(btnEnviarSupervisor);
            diccionarioBotones[Habitacion.EstadosHabitacion.Supervision] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Supervision].Add(btnEstadisticaRec);
            diccionarioBotones[Habitacion.EstadosHabitacion.Supervision].Add(btnReportarMatricula);
            diccionarioBotones[Habitacion.EstadosHabitacion.Supervision].Add(btnCambio);
            diccionarioBotones[Habitacion.EstadosHabitacion.Supervision].Add(btnLiberarHabitacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Libre] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Libre].Add(btnReservacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Libre].Add(btnDeshabilitar);
            diccionarioBotones[Habitacion.EstadosHabitacion.Libre].Add(btnMantenimiento);
            diccionarioBotones[Habitacion.EstadosHabitacion.Libre].Add(btnEntradaHabitacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.PendienteCobro] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.PendienteCobro].Add(btnReservacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.PendienteCobro].Add(btnDeshabilitar);
            diccionarioBotones[Habitacion.EstadosHabitacion.PendienteCobro].Add(btnRestaurante);//btnMantenimiento
            diccionarioBotones[Habitacion.EstadosHabitacion.PendienteCobro].Add(btnEntradaHabitacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Mantenimiento] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Mantenimiento].Add(btnHistorialMantenimento);
            diccionarioBotones[Habitacion.EstadosHabitacion.Mantenimiento].Add(btnMantenimiento);
            diccionarioBotones[Habitacion.EstadosHabitacion.Mantenimiento].Add(btnEnviarLimpieza);
            diccionarioBotones[Habitacion.EstadosHabitacion.Mantenimiento].Add(btnMantenimientoTerminado);
            diccionarioBotones[Habitacion.EstadosHabitacion.Ocupada] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Ocupada].Add(btnVPoints);
            diccionarioBotones[Habitacion.EstadosHabitacion.Ocupada].Add(btnRestaurante);
            diccionarioBotones[Habitacion.EstadosHabitacion.Ocupada].Add(btnHorasExtra);
            diccionarioBotones[Habitacion.EstadosHabitacion.Ocupada].Add(btnSalida);
            diccionarioBotones[Habitacion.EstadosHabitacion.Preparada] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Preparada].Add(btnReservacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Preparada].Add(btnDeshabilitar);
            diccionarioBotones[Habitacion.EstadosHabitacion.Preparada].Add(btnCancelarEntrada);
            diccionarioBotones[Habitacion.EstadosHabitacion.Preparada].Add(btnEntradaHabitacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Sucia] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Sucia].Add(btnEstadisticaRec);
            diccionarioBotones[Habitacion.EstadosHabitacion.Sucia].Add(btnReportarMatricula);
            //diccionarioBotones[Habitacion.EstadosHabitacion.Sucia].Add(btnFacturar);
            diccionarioBotones[Habitacion.EstadosHabitacion.Sucia].Add(btnEnviarLimpieza);
            diccionarioBotones[Habitacion.EstadosHabitacion.Reservada] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.Reservada].Add(btnReservacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.Reservada].Add(btnDeshabilitar);
            diccionarioBotones[Habitacion.EstadosHabitacion.Reservada].Add(btnCancelarEntrada);
            diccionarioBotones[Habitacion.EstadosHabitacion.Reservada].Add(btnEntradaHabitacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.PreparadaReservada] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Habitacion.EstadosHabitacion.PreparadaReservada].Add(btnReservacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.PreparadaReservada].Add(btnDesasignarReservacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.PreparadaReservada].Add(btnCambiarHabitacion);
            diccionarioBotones[Habitacion.EstadosHabitacion.PreparadaReservada].Add(btnEntradaHabitacion);

            panelBotones.Children.Clear();
            panelBotones.Visibility = Visibility.Collapsed;

            btnMenu.DataContext = this;
        }

        private void ValidarSeleccion()
        {
            if (habitacionSeleccionada == null)
                throw new SOTException(Textos.Errores.habitacion_no_seleccionada_exception);
        }


        /*public void ActualizarHabitaciones()
        {
            lock (bloqueador)
            {
                if (recargando)
                    return;

                recargando = true;
            }

            List<DtoResumenHabitacion> habitaciones;

            try
            {
                ControladorHabitaciones controlador = new ControladorHabitaciones();

                habitaciones = controlador.SP_ObtenerResumenesHabitaciones();
            }
            catch (Exception ex)
            {
                Transversal.Log.Logger.Error(ex.Message);
                return;
            }
            finally
            {
                lock (bloqueador)
                    recargando = false;
            }

            Dispatcher.BeginInvoke((ThreadStart)delegate
            {

                try
                {
                    //datosHabitacionP.Visibility = Visibility.Collapsed;

                    panelHabitaciones.Children.Clear();
                    panelHabitaciones.RowDefinitions.Clear();
                    panelHabitaciones.ColumnDefinitions.Clear();

                    foreach (UCHabitacion uc_habitacion in uc_habitaciones.Where(m => !habitaciones.Any(a => a.IdHabitacion == m.HabitacionInterna.IdHabitacion)))
                    {
                        uc_habitacion.Click -= habitacion_Click;
                        uc_habitacion.Recargar -= CargarHabitacion;
                    }

                    uc_habitaciones.RemoveAll(m => !habitaciones.Any(a => a.IdHabitacion == m.HabitacionInterna.IdHabitacion));

                    if (habitaciones.Count > 0)
                    {
                        for (int i = 1; i <= habitaciones.Max(m => m.Posicion); i++)
                            panelHabitaciones.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(98) });

                        var pisoMaximo = habitaciones.Max(m => m.Piso);

                        for (int i = 1; i <= pisoMaximo; i++)
                            panelHabitaciones.RowDefinitions.Add(new RowDefinition { Height = new GridLength(76) });

                        foreach (var habitacion in habitaciones)
                        {
                            var existente = uc_habitaciones.FirstOrDefault(m => m.HabitacionInterna.IdHabitacion == habitacion.IdHabitacion);

                            var nh = existente ??
                                     new UCHabitacion(habitacion.IdHabitacion);

                            if (existente == null)
                            {
                                uc_habitaciones.Add(nh);
                                nh.Margin = new Thickness(0.5);
                                Grid.SetColumn(nh, habitacion.Posicion - 1);
                                Grid.SetRow(nh, pisoMaximo - habitacion.Piso);

                                uc_habitaciones.Last().Click += habitacion_Click;
                                uc_habitaciones.Last().Recargar += CargarHabitacion;
                            }
                            else
                                nh.RecargarHabitacion(habitacion);

                            panelHabitaciones.Children.Add(nh);
                        }
                    }

                    CalcularPorcentajesEstados();
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Error(ex.Message);
                }
                finally
                {
                    lock (bloqueador)
                        recargando = false;
                }
            });
        }*/

        private void CambiarHabitacion(UCHabitacion habitacionNueva)
        {
            try
            {
                if (habitacionSeleccionada != null && habitacionNueva != null)
                {
                    if (habitacionSeleccionada == habitacionNueva)
                        throw new SOTException(Textos.Errores.intercambio_misma_habitacion_invalido_excepcion);

                    if (MessageBox.Show(string.Format(Textos.Mensajes.confirmar_intercambio_habitacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion, habitacionNueva.HabitacionInterna.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        var controladorR = new ControladorReservaciones();

                        controladorR.CambiarHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion, habitacionNueva.HabitacionInterna.IdHabitacion);

                        habitacionSeleccionada.RecargarHabitacion();
                        habitacionNueva.RecargarHabitacion();
                    }
                }
            }
            finally
            {
                accionCambio = null;
                btnCambiarHabitacion.Animar = false;
            }
        }


        private void PrevioRecargar(object sender, RoutedEventArgs e)
        {
            if (accionCambio != null)
                accionCambio.Invoke(e.OriginalSource as UCHabitacion);
        }

        //private void habitacion_Click(object sender, RoutedEventArgs e)
        //{
            

        //    habitacionSeleccionada = sender as UCHabitacion;
        //    CargarHabitacion(sender, e);
        //}

        private void CargarHabitacion(object sender, RoutedEventArgs e)
        {

            /*if ((sender == null && habitacionSeleccionada == null) || (sender != null && sender.Equals(habitacionSeleccionada)))
            {*/
                MostrarBotonesPorEstado();

                if (habitacionSeleccionada == null)
                    return;

                datosHabitacionP.ActualizarDatos(habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);
            /*}*/

            //CalcularPorcentajesEstados();
        }

        private void CalcularPorcentajesEstados(object sender, PorcentajesEstadosRoutedEventArgs e)
        {
            var r = e.Estadisticas;
            r.RemoveAll(m => m.Estado == Habitacion.EstadosHabitacion.EnCreacion);
            datosDuros.ItemsSource = r;/*//new ControladorHabitaciones().ObtenerEstadisticasHabitaciones().Select(m => (DtoEstadisticasEstadoHabitacionExtendida)m).ToList();
                EnumExtensions.ComoLista<Habitacion.EstadosHabitacion>().Select(m => new DtoEstadisticasEstadoHabitacionExtendida
            {
                Cantidad = uc_habitaciones.Count(h => h.HabitacionInterna.EstadoHabitacion == m),
                Estado = m
            }).ToList();*/
        }

        private void datos_VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (datosHabitacionP.Visibility == System.Windows.Visibility.Visible)
            {
                panelBotones.Visibility = System.Windows.Visibility.Visible;

                panelReloj.Visibility = btnTaxi.Visibility = btnFajillas.Visibility = btnMesas.Visibility 
                                      = System.Windows.Visibility.Collapsed;

            }
            else
            {
                habitacionSeleccionada = null;
                panelBotones.Visibility = System.Windows.Visibility.Collapsed;

                panelReloj.Visibility = btnTaxi.Visibility = btnFajillas.Visibility = btnMesas.Visibility
                                      = System.Windows.Visibility.Visible;
            }
        }

        private void MostrarBotonesPorEstado()
        {
            panelBotones.Children.Clear();

            if (habitacionSeleccionada != null && diccionarioBotones.ContainsKey(habitacionSeleccionada.HabitacionInterna.EstadoHabitacion))
            {
                foreach (var c in diccionarioBotones[habitacionSeleccionada.HabitacionInterna.EstadoHabitacion])
                {
                    panelBotones.Children.Add(c);
                }
            }
        }

        private void btnEntrada_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Libre || habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Bloqueada)
            {
                var controladorReservaciones = new ControladorReservaciones();

                int contador = controladorReservaciones.ObtenerCantidadReservacionesDia(habitacionSeleccionada.HabitacionInterna.IdTipoHabitacion);

                var respuesta = MessageBoxResult.No;

                if (contador > 0)
                    respuesta = MessageBox.Show(string.Format(Textos.Mensajes.reservaciones_pendientes, contador), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                if (respuesta == MessageBoxResult.Cancel)
                    return;
                else if (respuesta == MessageBoxResult.Yes)
                {
                    var bloqueoF = new BloqueoHabitacionReservacionForm(new Dtos.DtoDatosHabitacionBasicos
                    {
                        IdTipoHabitacion = habitacionSeleccionada.HabitacionInterna.IdTipoHabitacion,
                        NombreTipo = habitacionSeleccionada.HabitacionInterna.NombreTipo,
                        IdHabitacion = habitacionSeleccionada.HabitacionInterna.IdHabitacion,
                        NumeroHabitacion = habitacionSeleccionada.HabitacionInterna.NumeroHabitacion
                    });

                    Utilidades.Dialogos.MostrarDialogos(App.Current.MainWindow, bloqueoF);
                }
                else
                {
                    var controlador = new ControladorHabitaciones();
                    controlador.PrepararHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion);
                }

                habitacionSeleccionada.RecargarHabitacion();
            }
            else if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Preparada || habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.PreparadaReservada)
            {
                var rentasForm = new RentasFormV2(habitacionSeleccionada.HabitacionInterna);//, RentasForm.Modos.Creacion);

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, rentasForm);

                habitacionSeleccionada.RecargarHabitacion();

            }
            else if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.PendienteCobro)
            {
                var rentasForm = new RentasFormV2(habitacionSeleccionada.HabitacionInterna);//, RentasForm.Modos.Cobro);

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, rentasForm);

                habitacionSeleccionada.RecargarHabitacion();

            }

        }

        private void btnActivarVCard_Click(object sender, RoutedEventArgs e)
        {
            var activacionVF = new ActivacionTarjetaVFrom();
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, activacionVF);
        }


        private void btnDeshabilitar_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion != Habitacion.EstadosHabitacion.Libre)
                throw new SOTException(Textos.Errores.habitacion_no_libre_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);

            var capturaMotivoForm = new CapturaMotivo
            {
                Motivo = Textos.Acciones.Bloqueo,
                TextoAuxiliar = string.Format(Textos.Etiquetas.RentasForm_InformacionHabitacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion, habitacionSeleccionada.HabitacionInterna.NombreTipo)
            };

            CapturaMotivo.AceptarEventHandler eh = (s, ea) =>
            {
                ControladorHabitaciones controlador = new ControladorHabitaciones();
                controlador.BloquearHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion, capturaMotivoForm.DescripcionMotivo);
                habitacionSeleccionada.RecargarHabitacion();
            };

            capturaMotivoForm.Aceptar += eh;

            try
            {
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, capturaMotivoForm);
            }
            finally
            {
                capturaMotivoForm.Aceptar -= eh;
            }
        }

        private void btnMantenimiento_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Libre)
            {
                //var r = MessageBox.Show(Textos.Mensajes.enviar_mantenimiento_directo, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                var r = MessageBoxResult.Yes;
                if (r == MessageBoxResult.Yes)
                {
                    var selector = new TareasMantenimiento.EnvioMantenimientoForm();

                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selector);

                    if (selector.EsValido) 
                    {
                        var controlador = new ControladorTareasMantenimiento();
                        controlador.CrearTareaInstantanea(habitacionSeleccionada.HabitacionInterna.IdHabitacion, selector.IdConceptoMantenimiento, selector.Descripcion);
                        habitacionSeleccionada.RecargarHabitacion();
                    }
                }
                else if(r == MessageBoxResult.No)
                {
                    var selectorT = new SelectorTareaMantenimientoForm(habitacionSeleccionada.HabitacionInterna.IdHabitacion);
                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorT);

                    int? idTareaSeleccionada = selectorT.IdTareaSeleccionada;

                    if (idTareaSeleccionada.HasValue)
                    {
                        var controlador = new ControladorHabitaciones();
                        controlador.PonerEnMantenimiento(habitacionSeleccionada.HabitacionInterna.IdHabitacion, idTareaSeleccionada.Value);
                        habitacionSeleccionada.RecargarHabitacion();
                    }
                }


            }
            else if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Mantenimiento)
            {
                var capturaMotivoForm = new CapturaMotivo
                {
                    Motivo = Textos.Acciones.Mantenimiento,
                    TextoAuxiliar = string.Format(Textos.Etiquetas.RentasForm_InformacionHabitacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion, habitacionSeleccionada.HabitacionInterna.NombreTipo),
                    SoloLectura = true
                };

                var controlador = new ControladorMantenimientos();

                var mantenimiento = controlador.ObtenerMantenimientoActual(habitacionSeleccionada.HabitacionInterna.IdHabitacion);

                if (mantenimiento != null)
                    capturaMotivoForm.DescripcionMotivo = mantenimiento.ConceptoMantenimiento.Concepto;

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, capturaMotivoForm);
            }
            else
                throw new SOTException(Textos.Errores.habitacion_no_libre_para_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);
        }

        private void btnHistorialMantenimento_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            var historialMantenimientoF = new HistorialMantenimientoForm(habitacionSeleccionada.HabitacionInterna.IdHabitacion);

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, historialMantenimientoF);

        }


        private void btnMantenimientoTerminado_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Mantenimiento)
            {
                //if (MessageBox.Show(string.Format(Textos.Mensajes.finalizar_mantenimiento, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                //{
                //var selectorOT = new SelectorOrdenesTrabajoForm();

                //Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorOT);

                var encabezados = new List<int>();//selectorOT.Encabezados.ToList();

                //if (encabezados.Count > 0 || MessageBox.Show(Textos.Mensajes.ordenes_trabajo_no_seleccionadas, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                //{
                var controlador = new ControladorHabitaciones();
                controlador.FinalizarMantenimiento(habitacionSeleccionada.HabitacionInterna.IdHabitacion, encabezados);

                habitacionSeleccionada.RecargarHabitacion();
                //}
                //}
            }
            else
            {
                throw new SOTException(Textos.Errores.habitacion_no_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);
            }
        }


        private void btnEnviarLimpieza_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Mantenimiento)
                    {
                        if (MessageBox.Show(string.Format(Textos.Mensajes.finalizar_mantenimiento, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            //var selectorOT = new SelectorOrdenesTrabajoForm();

                            //Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorOT);

                            var encabezados = new List<int>();//selectorOT.Encabezados.ToList();

                            //if (encabezados.Count > 0 || MessageBox.Show(Textos.Mensajes.ordenes_trabajo_no_seleccionadas, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            //{
                                var controlador = new ControladorHabitaciones();
                                controlador.FinalizarMantenimiento(habitacionSeleccionada.HabitacionInterna.IdHabitacion, encabezados);

                                habitacionSeleccionada.RecargarHabitacion();
                            //}
                            //else
                            //    return;
                        }
                        else
                            return;
                    }

                    if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Sucia)
                    {
                        if (Limpiar())
                            scope.Complete();
                    }
                    else
                        throw new SOTException(Textos.Errores.habitacion_no_sucia_o_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);
                }
            }
            finally
            {
                habitacionSeleccionada.RecargarHabitacion();
            }
        }

        private bool Limpiar()
        {
            var selectorRecamareras = new SeleccionadorRecamarerasForm(habitacionSeleccionada.HabitacionInterna);

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorRecamareras);

            return selectorRecamareras.ProcesoExitoso;
        }


        private void btnLiberarHabitacion_Click(object sender, RoutedEventArgs e)
        {
            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Supervision)
            {
                var capturaMotivoForm = new SelectorMotivoForm(ConceptoSistema.TiposConceptos.LiberacionHabitacion, true);
                capturaMotivoForm.Motivo = string.Format(Textos.Acciones.Liberacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);
                capturaMotivoForm.TextoAuxiliar = Textos.TitulosComponentes.Detalles;

                SelectorMotivoForm.AceptarEventHandler eh = (s, ea) =>
                {
                    var controlador = new ControladorTareasLimpieza();
                    controlador.FinalizarLimpieza(habitacionSeleccionada.HabitacionInterna.IdHabitacion, capturaMotivoForm.IdMotivo, capturaMotivoForm.Observaciones);
                    habitacionSeleccionada.RecargarHabitacion();
                };

                capturaMotivoForm.Aceptar += eh;

                try
                {
                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, capturaMotivoForm);
                }
                finally
                {
                    capturaMotivoForm.Aceptar -= eh;
                }
            }
        }

        private void btnRestaurante_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion != Habitacion.EstadosHabitacion.Ocupada
                && habitacionSeleccionada.HabitacionInterna.EstadoHabitacion != Habitacion.EstadosHabitacion.PendienteCobro)
                throw new SOTException(Textos.Errores.habitacion_no_ocupada_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);

            var controlador = new ControladorRentas();
            var controladorRS = new ControladorRoomServices();

            var rentaActual = controlador.ObtenerRentaActualPorHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion);


            if (controladorRS.ObtenerMaximoEstadoComandasPendientes(rentaActual.Id).HasValue)
            {
                var manejador = new ManejadorComandas(rentaActual.Id, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);

                Comandas.GestionComandasForm comandasF = new Comandas.GestionComandasForm(manejador);
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, comandasF);
            }
            else
            {
                var ccm = new ManejadorCreacionComandas(rentaActual.Id, "Habitación " + habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);

                Comandas.CreacionComandasForm comandasF = new Comandas.CreacionComandasForm(ccm);
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, comandasF);

            }
            habitacionSeleccionada.RecargarHabitacion();

        }

        private void btnSalida_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Ocupada)
            {
                if (MessageBox.Show(string.Format(Textos.Mensajes.finalizar_renta, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var controlador = new ControladorHabitaciones();
                    controlador.FinalizarOcupacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion, false);

                    habitacionSeleccionada.RecargarHabitacion();

                    var controladorReservaciones = new ControladorReservaciones();

                    int contador = controladorReservaciones.ObtenerCantidadReservacionesDia(habitacionSeleccionada.HabitacionInterna.IdTipoHabitacion);

                    if (contador > 0 && MessageBox.Show(string.Format(Textos.Mensajes.reservaciones_pendientes_enviar_limpieza, contador), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        Limpiar();

                        habitacionSeleccionada.RecargarHabitacion();
                    }
                }
            }
            else
                throw new SOTException(Textos.Errores.habitacion_no_ocupada_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);
        }

        private void ManejadorHabitaciones_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if(!new ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos{ VenderTarjetasLealtad = true}))
                    btnVentaTarjetas.Visibility = System.Windows.Visibility.Collapsed;

                //ActualizarHabitaciones();

                //timerRecarga = new System.Timers.Timer();
                //timerRecarga.Interval = TimeSpan.FromSeconds(5).TotalMilliseconds;
                //timerRecarga.Elapsed += TimerRecarga_Tick;

                //timerRecarga.Start();
            }
        }

        private void btnCambio_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Limpieza)
            {
                var selectorRecamareraF = new SeleccionadorRecamarerasForm(habitacionSeleccionada.HabitacionInterna);
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorRecamareraF);

                if ((selectorRecamareraF.ProcesoExitoso))
                {
                    habitacionSeleccionada.RecargarHabitacion();
                }

            }
            else if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Supervision)
            {
                var selectorSupervisor = new SeleccionadorSupervisorForm(habitacionSeleccionada.HabitacionInterna);


                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorSupervisor);

                if (selectorSupervisor.ProcesoExitoso)
                {
                    habitacionSeleccionada.RecargarHabitacion(null,1);
                }
            }
            else
            {
                throw new SOTException(Textos.Errores.habitacion_no_limpieza_o_mantenimiento_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);
            }
        }

        private void btnEstadisticaRec_Click(object sender, RoutedEventArgs e)
        {
            var recamarerasF = new ListaRecamarerasForm();
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, recamarerasF);
        }

        private void btnHorasExtra_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion != Habitacion.EstadosHabitacion.Ocupada)
                throw new SOTException(Textos.Errores.habitacion_no_ocupada_exception, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);

            var rentasForm = new RentasFormV2(habitacionSeleccionada.HabitacionInterna);//, RentasForm.Modos.Edicion);

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, rentasForm);

            habitacionSeleccionada.RecargarHabitacion();

        }

        private void btnFacturar_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            var controladorR = new ControladorRentas();
            var ultimaRenta = controladorR.ObtenerUltimaRentaPorHabitacionConDatosFiscales(habitacionSeleccionada.HabitacionInterna.IdHabitacion);

            if (ultimaRenta != null)
            {
                var edf = new EdicionDatosFacturacionForm(ultimaRenta.DatosFiscales);
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, edf);

                if (edf.ProcesoTerminado)
                    controladorR.VincularDatosFiscales(ultimaRenta.Id, edf.DatosFiscalesActuales == null ? default(int?) : edf.DatosFiscalesActuales.Id);
            }
        }

        private void btnReportarMatricula_Click(object sender, RoutedEventArgs e)
        {
            string matricula = null;

            if (habitacionSeleccionada != null)
            {
                var controlador = new ControladorRentas();

                matricula = controlador.ObtenerAutomovilesUltimaRenta(habitacionSeleccionada.HabitacionInterna.IdHabitacion).Select(m => m.Matricula).FirstOrDefault();
            }

            var reporteadorMatriculas = new ReportesMatriculaForm(matricula);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, reporteadorMatriculas);
        }

        private void btnEnviarSupervisor_Click(object sender, RoutedEventArgs e)
        {
            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Limpieza)
            {
                var selectorSupervisor = new SeleccionadorSupervisorForm(habitacionSeleccionada.HabitacionInterna);

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorSupervisor);

                if (selectorSupervisor.ProcesoExitoso)
                {
                    habitacionSeleccionada.RecargarHabitacion();
                }
            }
        }

        private void btnVPoints_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            var controladorRenta = new ControladorRentas();

            var rentaActual = controladorRenta.ObtenerRentaActualPorHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion);

            if (rentaActual == null || string.IsNullOrEmpty(rentaActual.NumeroTarjeta))
                throw new SOTException(Textos.Errores.renta_sin_tarjeta_v_exception);

            var cuponF = new ConsultaCupon();
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, cuponF);

            if (cuponF.DatosC != null && cuponF.DatosC.PorCanjear) 
            {
                //por lo que vi, cuando el número de tarjeta comienza con 0 en la respuesta de la consulta del estado del cupón omite ese cero

                if ((cuponF.DatosC.NumeroTarjeta.Length == rentaActual.NumeroTarjeta.Length && cuponF.DatosC.NumeroTarjeta.Equals(rentaActual.NumeroTarjeta)) ||
                    (cuponF.DatosC.NumeroTarjeta.Length < rentaActual.NumeroTarjeta.Length) && rentaActual.NumeroTarjeta.Equals(new string('0', rentaActual.NumeroTarjeta.Length - cuponF.DatosC.NumeroTarjeta.Length) + cuponF.DatosC.NumeroTarjeta))
                {
                    var controladorPremios = new ControladorPremios();
                    var premio = controladorPremios.ObtenerPremioCargadoNoNull(cuponF.DatosC.IdPremio);

                    var ccm = new ManejadorCreacionComandas(rentaActual.Id, cuponF.DatosC.Cupon, premio, "Habitación" + habitacionSeleccionada.HabitacionInterna.NumeroHabitacion);

                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new Comandas.CreacionComandasForm(ccm)); 
                }
                else
                {
                    throw new SOTException(Textos.Errores.cupon_diferente_tarjeta_v_exception);  
                }              
            }
        }

        //internal void LimpiarListaHabitaciones()
        //{
        //    foreach (UCHabitacion uc_habitacion in uc_habitaciones)
        //    {
        //        uc_habitacion.Click -= habitacion_Click;
        //        uc_habitacion.Recargar -= CargarHabitacion;

        //        try
        //        {
        //            //uc_habitacion.RecargaAutomatica = false;

        //            if (timerRecarga != null)
        //                timerRecarga.Stop();
        //            timerRecarga = null;
        //        }
        //        catch
        //        {
        //        }
        //    }

        //    uc_habitaciones.Clear();
        //}

        private void btnReservacion_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Libre)
            {
                var bloqueoF = new BloqueoHabitacionReservacionForm(new Dtos.DtoDatosHabitacionBasicos
                {
                    IdTipoHabitacion = habitacionSeleccionada.HabitacionInterna.IdTipoHabitacion,
                    NombreTipo = habitacionSeleccionada.HabitacionInterna.NombreTipo,
                    IdHabitacion = habitacionSeleccionada.HabitacionInterna.IdHabitacion,
                    NumeroHabitacion = habitacionSeleccionada.HabitacionInterna.NumeroHabitacion
                });

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, bloqueoF);

                habitacionSeleccionada.RecargarHabitacion();
            }
            else if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Reservada)
            {
                var reservacionesForm = new ReservacionesForm(habitacionSeleccionada.HabitacionInterna.IdTipoHabitacion, habitacionSeleccionada.HabitacionInterna.IdHabitacion);

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, reservacionesForm);

                habitacionSeleccionada.RecargarHabitacion();
            }
            else
                throw new SOTException(Textos.Errores.habitacion_no_libre_o_reservada_exception);
        }

        private void btnCancelarEntrada_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Preparada &&
                MessageBox.Show(string.Format(Textos.Mensajes.cancelar_preparacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorHabitaciones();

                controlador.CancelarPreparacionHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion);
            }
            else if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.Reservada &&
                MessageBox.Show(string.Format(Textos.Mensajes.cancelar_preparacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorHabitaciones();

                controlador.CancelarEstadoReservacionHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion);
            }

            habitacionSeleccionada.RecargarHabitacion();
        }

        private void btnCambiarHabitacion_Click(object sender, RoutedEventArgs e)
        {
            if (accionCambio == null)
            {
                accionCambio = CambiarHabitacion;
                btnCambiarHabitacion.Animar = true;
            }
            else
            {
                accionCambio = null;
                btnCambiarHabitacion.Animar = false;
            }
        }

        private void btnDesasignarReservacion_Click(object sender, RoutedEventArgs e)
        {
            ValidarSeleccion();

            if (habitacionSeleccionada.HabitacionInterna.EstadoHabitacion == Habitacion.EstadosHabitacion.PreparadaReservada &&
                MessageBox.Show(string.Format(Textos.Mensajes.desanclar_reservacion, habitacionSeleccionada.HabitacionInterna.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorReservaciones();

                controlador.DesasignarHabitacion(habitacionSeleccionada.HabitacionInterna.IdHabitacion);

                habitacionSeleccionada.RecargarHabitacion();
            }
        }
    }

}
