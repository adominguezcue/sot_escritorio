﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.Habitaciones
{
    /// <summary>
    /// Lógica de interacción para UCHabitacion.xaml
    /// </summary>
    public partial class UCHabitacion : UserControl
    {
        private LinearGradientBrush fb;
        private System.Windows.Media.Animation.Storyboard anim;

        private ImageSource ImagenE
        {
            get { return imagenEstado.Source; }
            set { imagenEstado.Source = value; }
        }

        bool _esVencida = false;
        public bool EsVencida
        {
            get { return _esVencida; }
            set
            {
                _esVencida = value;

                if (_esVencida)
                {
                    (anim ?? (anim = (Resources["sbVencimientoActivado"] as System.Windows.Media.Animation.Storyboard)))
                    .Begin(this, true);
                }
                else
                {
                    if (anim != null)
                        anim.Stop(this);
                }
            }
        }

        public UCHabitacion(int idHabitacion)
            : base()
        {
            InitializeComponent();

            CustomColor = Color.FromArgb(255, 139, 139, 139);
            btn.DataContext = this;

            fb = fondo.Background as LinearGradientBrush;

            CargarInformacion(idHabitacion);
        }

        public DtoResumenHabitacion HabitacionInterna { get; private set; }

        //public bool RecargaAutomatica
        //{
        //    get { return timerRecarga.IsEnabled; }
        //    set 
        //    {
        //        if (timerRecarga.IsEnabled == value)
        //            return;

        //        if (value)
        //            timerRecarga.Start();
        //        else
        //            timerRecarga.Stop();
        //    }
        //}

        public static readonly DependencyProperty CustomColorProperty =
             DependencyProperty.Register("CustomColor", typeof(Color),
             typeof(UCHabitacion), new FrameworkPropertyMetadata(Color.FromArgb(255, 139, 139, 139)));

        public static readonly DependencyProperty CustomColor2Property =
             DependencyProperty.Register("CustomColor2", typeof(Color),
             typeof(UCHabitacion), new FrameworkPropertyMetadata(Color.FromArgb(0, 139, 139, 139)));

        public Color CustomColor
        {
            get { return (Color)GetValue(CustomColorProperty); }
            set
            {
                SetValue(CustomColorProperty, value);
                CustomColor2 = Color.FromArgb(0, value.R, value.G, value.B);
            }
        }

        public Color CustomColor2
        {
            get { return (Color)GetValue(CustomColor2Property); }
            set { SetValue(CustomColor2Property, value); }
        }



        private void btn_MouseEnter(object sender, MouseEventArgs e)
        {
            CustomColor2 = CustomColor;
            Grid.SetRow(fondo, 0);
            Grid.SetRowSpan(fondo, 4);
            fondo.CornerRadius = new CornerRadius(5);
        }

        private void btn_MouseLeave(object sender, MouseEventArgs e)
        {
            CustomColor = CustomColor;
            Grid.SetRow(fondo, 1);
            Grid.SetRowSpan(fondo, 3);
            fondo.CornerRadius = new CornerRadius(0, 0, 5, 5);
        }


        public static readonly RoutedEvent ClickEvent = EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UCHabitacion));
        public static readonly RoutedEvent RecargarEvent = EventManager.RegisterRoutedEvent("Recargar", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UCHabitacion));
        public static readonly RoutedEvent VencimientoActivadoEvent = EventManager.RegisterRoutedEvent("VencimientoActivado", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UCHabitacion));
        public static readonly RoutedEvent VencimientoDesactivadoEvent = EventManager.RegisterRoutedEvent("VencimientoDesactivado", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UCHabitacion));

        public event RoutedEventHandler Click
        {
            add
            {
                AddHandler(ClickEvent, value);
            }

            remove
            {
                RemoveHandler(ClickEvent, value);
            }
        }

        public event RoutedEventHandler Recargar
        {
            add
            {
                AddHandler(RecargarEvent, value);
            }

            remove
            {
                RemoveHandler(RecargarEvent, value);
            }
        }

        public event RoutedEventHandler VencimientoActivado
        {
            add
            {
                AddHandler(VencimientoActivadoEvent, value);
            }

            remove
            {
                RemoveHandler(VencimientoActivadoEvent, value);
            }
        }

        public event RoutedEventHandler VencimientoDesactivado
        {
            add
            {
                AddHandler(VencimientoDesactivadoEvent, value);
            }

            remove
            {
                RemoveHandler(VencimientoDesactivadoEvent, value);
            }
        }

        private void CargarInformacion(DtoResumenHabitacion resumen)
        {
            txtNumero.Text = resumen.NumeroHabitacion;
            txtTipo.Text = resumen.DatoBase;
            txtDetalle.Text = resumen.Detalle;
            EsVencida = resumen.EsVencida;

            switch (resumen.EstadoHabitacion)
            {
                case Habitacion.EstadosHabitacion.EnCreacion:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_habilitada;
                        txtDetalle.Text = Habitacion.EstadosHabitacion.EnCreacion.Descripcion();
                    }
                    break;
                case Habitacion.EstadosHabitacion.Libre:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_habilitada;
                    }
                    break;
                case Habitacion.EstadosHabitacion.PendienteCobro:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_cobrar;
                    }
                    break;
                case Habitacion.EstadosHabitacion.Preparada:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_preparada;
                    }
                    break;
                case Habitacion.EstadosHabitacion.Sucia:
                    {
                        ImagenE = EsVencida ? Recursos.ImagenesEstados.estado_icono_vencida :
                                              Recursos.ImagenesEstados.estado_icono_sucia;
                    }
                    break;
                case Habitacion.EstadosHabitacion.Ocupada:
                    {
                        if (EsVencida)
                        {
                            ImagenE = Recursos.ImagenesEstados.estado_icono_vencida;
                        }
                        else
                        {
                            if (resumen.EstadoComanda.HasValue)
                            {
                                if (resumen.EstadoComanda.Value == Comanda.Estados.PorPagar || resumen.EstadoComanda.Value == Comanda.Estados.PorCobrar)
                                    ImagenE = Recursos.ImagenesEstados.estado_icono_comandacobrar;
                                else
                                    ImagenE = Recursos.ImagenesEstados.estado_icono_comanda;
                            }
                            else if (resumen.TieneTarjetaV)
                                ImagenE = Recursos.ImagenesEstados.estado_icono_tarjetaV;
                            else
                                ImagenE = Recursos.ImagenesEstados.estado_icono_ocupada;
                        }

                    }
                    break;
                case Habitacion.EstadosHabitacion.Reservada:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_reservada;
                    }
                    break;
                case Habitacion.EstadosHabitacion.PreparadaReservada:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_preparada;
                    }
                    break;
                case Habitacion.EstadosHabitacion.Supervision:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_revision;
                    }
                    break;
                case Habitacion.EstadosHabitacion.Limpieza:
                    {
                        ImagenE = EsVencida ? Recursos.ImagenesEstados.estado_icono_vencida :
                                              Recursos.ImagenesEstados.estado_icono_limpieza;
                    }
                    break;
                case Habitacion.EstadosHabitacion.Mantenimiento:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_mantenimiento;
                    }
                    break;
                case Habitacion.EstadosHabitacion.Bloqueada:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_bloqueada;
                    }
                    break;
            }

            CustomColor = SelectorColor.ObtenerColorPorEstado(resumen.EstadoHabitacion);

            HabitacionInterna = resumen;
        }

        private void CargarInformacion(int idHabitacion, int _EscambioSup = 0)
        {
            var resumen = new ControladorHabitaciones().SP_ObtenerResumenHabitacion(idHabitacion, _EscambioSup);

            CargarInformacion(resumen);
        }
        
        public void RecargarHabitacion(DtoResumenHabitacion resumen = null, int _EscambioSup =0)
        {
            if (HabitacionInterna != null)
            {
                try
                {
                    //var controlador = new ControladorHabitaciones();

                    //var h = controlador.ObtenerHabitacion(HabitacionInterna.IdHabitacion);
                    if (resumen != null)
                        CargarInformacion(resumen);
                    else
                        CargarInformacion(HabitacionInterna.IdHabitacion, _EscambioSup);

                    imagenEstado.Visibility = Visibility.Visible;
                    barraCarga.Visibility = Visibility.Collapsed;

                }
                catch(Exception e)
                {
//#if DEBUG
                    try
                    {
                        if (e.GetType() == typeof(DbEntityValidationException))
                        {
                            var excepcion = (DbEntityValidationException)e;

                            foreach (var er in excepcion.EntityValidationErrors)
                                Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                        }
                        else if (e.GetType() != typeof(SOTException))
                        {
                            var exCurrent = e;

                            for (int i = 0; i < 50 && exCurrent != null; i++, exCurrent = exCurrent.InnerException)
                            {
                                Transversal.Log.Logger.Fatal(exCurrent.Message);
                            }
                        }
                    }
                    catch { }
//#endif

                    imagenEstado.Visibility = Visibility.Collapsed;
                    barraCarga.Visibility = Visibility.Visible;
                    txtDetalle.Text = "";
                    txtTipo.Text = "RECARGANDO";
                    
                }
                RaiseEvent(new RoutedEventArgs(RecargarEvent));
            }
        }
        
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ClickEvent));

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            

            anim = null;
        }
    }
}