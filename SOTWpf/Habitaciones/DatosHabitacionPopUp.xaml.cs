﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Habitaciones
{
    /// <summary>
    /// Lógica de interacción para DatosHabitacionPopUp.xaml
    /// </summary>
    public partial class DatosHabitacionPopUp : UserControl
    {
        int idHabitacionActual = 0;

        public DatosHabitacionPopUp()
        {
            InitializeComponent();
        }

        public void ActualizarDatos(string numeroHabitacion)
        {
            var controlador = new SOTControladores.Controladores.ControladorHabitaciones();

            var habitacion = controlador.ObtenerDatosHabitacion(numeroHabitacion);

            gridDatos.DataContext = habitacion;

            imgEstadoLimpieza.Source = null;

            if (habitacion != null)
            {
                try
                {
                    switch (habitacion.Estado)
                    {
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Libre:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_habilitada;
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_sucia;

                            switch (habitacion.TipoLimpieza)
                            {
                                case Modelo.Entidades.TareaLimpieza.TiposLimpieza.Detallado:
                                    imgEstadoLimpieza.Source = Recursos.ImagenesEstados.estado_limpieza_detallado;
                                    break;
                                //Case Modelo.Entidades.TareaLimpieza.TiposLimpieza.Lavado
                                //    imgEstadoLimpieza.Image = Recursos.ImagenesEstados.estado_limpieza_lavado
                                case Modelo.Entidades.TareaLimpieza.TiposLimpieza.Normal:
                                    imgEstadoLimpieza.Source = Recursos.ImagenesEstados.estado_limpieza_normal;
                                    break;
                            }
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_ocupada;
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Reservada:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_reservada;
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_limpieza;
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Mantenimiento:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_mantenimiento;
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Supervision:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_revision;
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Bloqueada:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_bloqueada;
                            break;
                        case Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada:
                            pbEstado.Source = Recursos.ImagenesEstados.estado_icono_preparada;
                            break;
                        default:
                            pbEstado.Source = null;
                            break;
                    }

                    Visibility = System.Windows.Visibility.Visible;
                    idHabitacionActual = habitacion.Id;
                }
                catch
                {
                    Visibility = System.Windows.Visibility.Collapsed;
                    idHabitacionActual = 0;
                }
            }
            else
            {
                Visibility = System.Windows.Visibility.Collapsed;
                idHabitacionActual = 0;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
            e.Handled = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var detallesRenta = new SOTControladores.Controladores.ControladorRentas().ObtenerResumenVentasUltimaRenta(idHabitacionActual);

            if (detallesRenta != null)
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new HistorialClienteForm(detallesRenta));
        }
    }
}
