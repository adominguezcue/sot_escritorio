﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SOTWpf.Habitaciones
{
    internal class SelectorColor
    {
        internal static Color ObtenerColorPorEstado(Modelo.Entidades.Habitacion.EstadosHabitacion estado) 
        {
            switch (estado)
            {
                case Modelo.Entidades.Habitacion.EstadosHabitacion.EnCreacion:
                    return (Color)ColorConverter.ConvertFromString("#999999");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Libre:
                    return (Color)ColorConverter.ConvertFromString("#38d731");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.PendienteCobro:
                    return (Color)ColorConverter.ConvertFromString("#ea927e");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada:
                    return (Color)ColorConverter.ConvertFromString("#38d731");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia:
                    return (Color)ColorConverter.ConvertFromString("#f2d017");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada:
                    return (Color)ColorConverter.ConvertFromString("#ea927e");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Reservada:
                    return (Color)ColorConverter.ConvertFromString("#dabfff");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.PreparadaReservada:
                    return (Color)ColorConverter.ConvertFromString("#dabfff");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Supervision:
                    return (Color)ColorConverter.ConvertFromString("#94bffe");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza:
                    return (Color)ColorConverter.ConvertFromString("#94bffe");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Mantenimiento:
                    return (Color)ColorConverter.ConvertFromString("#cacaca");
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Bloqueada:
                    return (Color)ColorConverter.ConvertFromString("#dabfff");
                default: 
                    return Colors.Transparent;
            }
        }
    }
}
