﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Habitaciones
{
    /// <summary>
    /// Lógica de interacción para HistorialClienteForm.xaml
    /// </summary>
    public partial class HistorialClienteForm : Window
    {
        private Modelo.Dtos.DtoDetallesRenta detallesRenta
        {
            get { return DataContext as Modelo.Dtos.DtoDetallesRenta; }
            set { DataContext = value; }
        }

        public HistorialClienteForm(Modelo.Dtos.DtoDetallesRenta detallesRenta)
        {
            InitializeComponent();
            this.detallesRenta = detallesRenta;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = dgvComandas.SelectedItem as DtoResumenComanda;

            if (item != null)
                dgvArticulos.ItemsSource = item.Articulos;
            else
                dgvArticulos.ItemsSource = new List<DtoArticuloTicket>();
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as DtoResumenVentaRenta;

            if (item != null)
            {
                var controladorI = new SOTControladores.Controladores.ControladorConfiguracionesImpresoras();
                var controladorTickets = new SOTControladores.Controladores.ControladorTickets();
                controladorTickets.ImprimirTicketVentaRenta(item.Id, controladorI.ObtenerImpresoras().ImpresoraTickets, 1);


                MessageBox.Show("Impresión exitosa", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }
    }
}
