﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Extensiones;

namespace SOTWpf.Habitaciones
{
    public class PorcentajesEstadosRoutedEventArgs : RoutedEventArgs
    {
        public List<DtoEstadisticasEstadoHabitacionExtendida> Estadisticas { get; private set; }

        public PorcentajesEstadosRoutedEventArgs(List<DtoEstadisticasEstadoHabitacionExtendida> estadisticas) : base()
        {
            Estadisticas = estadisticas;
        }

        public PorcentajesEstadosRoutedEventArgs(List<DtoEstadisticasEstadoHabitacionExtendida> estadisticas, RoutedEvent routedEvent): base(routedEvent)
        {
            Estadisticas = estadisticas;
        }

        public PorcentajesEstadosRoutedEventArgs(List<DtoEstadisticasEstadoHabitacionExtendida> estadisticas, RoutedEvent routedEvent, object source): base(routedEvent, source)
        {
            Estadisticas = estadisticas;
        }
    }

    /// <summary>
    /// Lógica de interacción para PanelHabitaciones.xaml
    /// </summary>
    public partial class PanelHabitaciones : UserControl
    {
        public delegate void CalcularPorcentajesEstadosEventHandler(object sender, PorcentajesEstadosRoutedEventArgs e);

        //public static readonly RoutedEvent ClickEvent = EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PanelHabitaciones));
        public static readonly RoutedEvent PrevioRecargarEvent = EventManager.RegisterRoutedEvent("PrevioRecargar", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PanelHabitaciones));
        public static readonly RoutedEvent RecargarEvent = EventManager.RegisterRoutedEvent("Recargar", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PanelHabitaciones));
        public static readonly RoutedEvent CalcularPorcentajesEstadosEvent = EventManager.RegisterRoutedEvent("CalcularPorcentajesEstados", RoutingStrategy.Bubble, typeof(CalcularPorcentajesEstadosEventHandler), typeof(PanelHabitaciones));

        public event CalcularPorcentajesEstadosEventHandler CalcularPorcentajesEstados
        {
            add
            {
                AddHandler(CalcularPorcentajesEstadosEvent, value);
            }

            remove
            {
                RemoveHandler(CalcularPorcentajesEstadosEvent, value);
            }
        }

        //public event RoutedEventHandler Click
        //{
        //    add
        //    {
        //        AddHandler(ClickEvent, value);
        //    }

        //    remove
        //    {
        //        RemoveHandler(ClickEvent, value);
        //    }
        //}

        public event RoutedEventHandler Recargar
        {
            add
            {
                AddHandler(RecargarEvent, value);
            }

            remove
            {
                RemoveHandler(RecargarEvent, value);
            }
        }

        public event RoutedEventHandler PrevioRecargar
        {
            add
            {
                AddHandler(PrevioRecargarEvent, value);
            }

            remove
            {
                RemoveHandler(PrevioRecargarEvent, value);
            }
        }

        private readonly object bloqueador = new object();
        private bool recargando = false;
        List<UCHabitacion> uc_habitaciones;

        private System.Timers.Timer timerRecarga;
        private System.Timers.Timer CierraPopHabitacion = new System.Timers.Timer();
        public UCHabitacion HabitacionSeleccionada;

        internal bool TimerHabilitado
        {
            get { return timerRecarga?.Enabled ?? false; }
            set
            {
                if (timerRecarga != null && timerRecarga.Enabled != value)
                    timerRecarga.Enabled = value;
            }
        }

        public PanelHabitaciones()
        {
            InitializeComponent();

            uc_habitaciones = new List<UCHabitacion>();
        }


        public void ActualizarHabitaciones()
        {
            lock (bloqueador)
            {
                if (recargando)
                    return;

                recargando = true;
            }

            List<DtoResumenHabitacion> habitaciones;

            try
            {
                var controlador = new ControladorHabitaciones();

                habitaciones = controlador.SP_ObtenerResumenesHabitaciones();
            }
            catch (Exception ex)
            {
                lock (bloqueador)
                    recargando = false;

                Transversal.Log.Logger.Error(ex.Message);
                return;
            }

            Dispatcher.BeginInvoke((ThreadStart)delegate
            {

                try
                {
                    //datosHabitacionP.Visibility = Visibility.Collapsed;

                    panelHabitaciones.Children.Clear();
                    panelHabitaciones.RowDefinitions.Clear();
                    panelHabitaciones.ColumnDefinitions.Clear();

                    foreach (UCHabitacion uc_habitacion in uc_habitaciones.Where(m => !habitaciones.Any(a => a.IdHabitacion == m.HabitacionInterna.IdHabitacion)))
                    {
                        uc_habitacion.Click -= habitacion_Click;
                        uc_habitacion.Recargar -= CargarHabitacion;
                    }

                    uc_habitaciones.RemoveAll(m => !habitaciones.Any(a => a.IdHabitacion == m.HabitacionInterna.IdHabitacion));

                    if (!uc_habitaciones.Contains(HabitacionSeleccionada))
                        HabitacionSeleccionada = null;

                    if (habitaciones.Count > 0)
                    {
                        for (int i = 1; i <= habitaciones.Max(m => m.Posicion); i++)
                            panelHabitaciones.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(98) });

                        var pisoMaximo = habitaciones.Max(m => m.Piso);

                        for (int i = 1; i <= pisoMaximo; i++)
                            panelHabitaciones.RowDefinitions.Add(new RowDefinition { Height = new GridLength(76) });

                        foreach (var habitacion in habitaciones)
                        {
                            var existente = uc_habitaciones.FirstOrDefault(m => m.HabitacionInterna.IdHabitacion == habitacion.IdHabitacion);

                            var nh = existente ??
                                     new UCHabitacion(habitacion.IdHabitacion);

                            if (existente == null)
                            {
                                uc_habitaciones.Add(nh);
                                nh.Margin = new Thickness(0.5);


                                uc_habitaciones.Last().Click += habitacion_Click;
                                uc_habitaciones.Last().Recargar += CargarHabitacion;
                            }
                            else
                                nh.RecargarHabitacion(habitacion);

                            Grid.SetColumn(nh, habitacion.Posicion - 1);
                            Grid.SetRow(nh, pisoMaximo - habitacion.Piso);

                            panelHabitaciones.Children.Add(nh);
                        }
                    }

                    EjecutarCalculoPorcentajesEstados();
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Error(ex.Message);
                }
                finally
                {
                    lock (bloqueador)
                        recargando = false;
                }
            });
        }

        private void CargarHabitacion(object sender, RoutedEventArgs e)
        {
            if ((sender == null && HabitacionSeleccionada == null) || (sender != null && sender.Equals(HabitacionSeleccionada)))
            {
                RaiseEvent(new RoutedEventArgs(RecargarEvent));
            }
            EjecutarCalculoPorcentajesEstados();
        }
        /*
         
             */

        private void EjecutarCalculoPorcentajesEstados()
        {
            RaiseEvent(new PorcentajesEstadosRoutedEventArgs(EnumExtensions.ComoLista<Habitacion.EstadosHabitacion>().Select(m => new DtoEstadisticasEstadoHabitacionExtendida
            {
                Cantidad = uc_habitaciones.Count(h => h.HabitacionInterna.EstadoHabitacion == m),
                Estado = m
            }).ToList(), CalcularPorcentajesEstadosEvent));
        }

        private void habitacion_Click(object sender, RoutedEventArgs e)
        {

            RaiseEvent(new RoutedEventArgs(PrevioRecargarEvent));

            HabitacionSeleccionada = sender as UCHabitacion;
            CargarHabitacion(sender, e);
            //PoneEfecto 
            PoneEfectoHabitacionseleccionada(uc_habitaciones);
            //hablitar deshabilitar el pop

        }

        private void PoneEfectoHabitacionseleccionada(List<UCHabitacion> uc_habitaciones)
        {
            foreach (UCHabitacion uCHabitacion in uc_habitaciones)
            {
                uCHabitacion.Effect = null;
                uCHabitacion.BorderThickness = new Thickness();
                uCHabitacion.BorderBrush = new SolidColorBrush();
            }
            if (HabitacionSeleccionada != null)
            {
               /* HabitacionSeleccionada.Effect = new System.Windows.Media.Effects.DropShadowEffect()
                {
                    BlurRadius = 10,
                    ShadowDepth = 5
                };*/
                /* HabitacionSeleccionada.Effect = new System.Windows.Media.Effects.BlurEffect()
                 {
                     Radius = 10
                 };*/

                HabitacionSeleccionada.BorderThickness = new Thickness(3, 3,3,3);
                HabitacionSeleccionada.BorderBrush = new SolidColorBrush(Colors.Black);
                //System.Windows.Media.Effects.ShaderEffect
                /*HabitacionSeleccionada.Effect = new System.Windows.Media.Effects.BitmapEffectInput()
                {
                  AreaToApplyEffect
                };*/
                // System.Windows.Media.Effects.ShaderEffect

            }
        }

        //internal void LimpiarListaHabitaciones()
        //{
        //    foreach (UCHabitacion uc_habitacion in uc_habitaciones)
        //    {
        //        uc_habitacion.Click -= habitacion_Click;
        //        uc_habitacion.Recargar -= CargarHabitacion;

        //        try
        //        {
        //            //uc_habitacion.RecargaAutomatica = false;

        //            if (timerRecarga != null)
        //                timerRecarga.Stop();
        //            timerRecarga = null;
        //        }
        //        catch
        //        {
        //        }
        //    }

        //    uc_habitaciones.Clear();
        //}

        private void PanelHabitaciones_Loaded(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
                recargando = false;

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                ActualizarHabitaciones();

                try
                {
                    if (timerRecarga != null)
                    {
                        timerRecarga.Stop();
                        timerRecarga.Elapsed -= TimerRecarga_Tick;
                    }
                }
                catch
                {
                }

                timerRecarga = new System.Timers.Timer
                {
                    Interval = TimeSpan.FromSeconds(5).TotalMilliseconds
                };
                timerRecarga.Elapsed += TimerRecarga_Tick;

                timerRecarga.Start();
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
                recargando = true;

            //lock (bloqueador)
            //{
            //    while (recargando)
            //        Thread.Sleep(1000);

            //    recargando = true;
            //}

            try
            {
                if (timerRecarga != null)
                {
                    timerRecarga.Stop();
                    timerRecarga.Elapsed -= TimerRecarga_Tick;
                }

                foreach (UCHabitacion uc_habitacion in uc_habitaciones)
                {
                    uc_habitacion.Click -= habitacion_Click;
                    uc_habitacion.Recargar -= CargarHabitacion;
                }

                uc_habitaciones.Clear();
            }
            catch
            {
            }
        }

        private void TimerRecarga_Tick(object sender, EventArgs e)
        {
            ActualizarHabitaciones();
        }
    }
}
