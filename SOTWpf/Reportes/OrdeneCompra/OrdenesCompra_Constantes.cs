﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Reportes.OrdeneCompra
{
    public class OrdenesCompra_Constantes
    {
        public static readonly string TABLA_ALMACENES = "Almacenes";
        public static readonly string TABLA_DETALLES_ORDEN_COMPRA = "DetallesOrdenCompra";
        public static readonly string TABLA_ENCABEZADO_ORDEN_COMPRA = "EncabezadoOrdenCompra";
        public static readonly string TABLA_PROVEEDOR = "Proveedor";
        public static readonly string TABLA_PARAMETROS = "Parametros";
        public static readonly string TABLA_ENCABEZADO_REPORTE = "EncabezadoReporte";
    }
}
