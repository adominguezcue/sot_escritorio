﻿using SOTControladores.Controladores;
using SOTWpf.Impresiones;
using SOTWpf.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf
{
    /// <summary>
    /// Lógica de interacción para PrincipalRestaurante.xaml
    /// </summary>
    public partial class PrincipalRestaurante : Window
    {
        public PrincipalRestaurante()
        {
            InitializeComponent();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorRestaurante = new ControladorRestaurantes();

                controladorRestaurante.ValidarAcceso();
            }

            Title += " " + Assembly.GetExecutingAssembly().GetName().Version.ToString();

        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (System.Windows.MessageBox.Show(SOTWpf.Textos.Mensajes.salir_sistema, SOTWpf.Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                e.Cancel = true;

            if (!e.Cancel)
                try
                {
                    //contenedorM.LimpiarMesas();
                    //IniciarSesionForm.CerrarSesion();
                }
                finally
                {
                    //var principalF = new IniciarSesionForm();

                    //System.Windows.Application.Current.MainWindow = principalF;

                    //principalF.Show();
                }

            base.OnClosing(e);
        }

        private void tsmiConsumosInternos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConsumosInternos.GestionConsumosInternosForm());
        }

        private void tsmiRepararCancelaciones_Click(object sender, RoutedEventArgs e)
        {
            var controlador = new SOTControladores.Controladores.ControladorRestaurantes();

            var v = new Utilidades.CapturaCodigoSecretoForm();

            Utilidades.Dialogos.MostrarDialogos(this, v);

            if (!string.IsNullOrEmpty(v.Codigo))
            {
                var res = controlador.RepararCancelacionesOcupacion(v.Codigo);

                Utilidades.Dialogos.Aviso(res);
            }
            else
                Utilidades.Dialogos.Aviso("Operación cancelada");
        }

        private void tsmiImpresoras_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionImpresorasForm());
        }

        private void tsmiImpresorasLocales_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionImpresorasForm(true));
        }
    }
}
