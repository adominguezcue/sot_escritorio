﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SOTWpf.Comandas
{
    class ColoresComandas
    {
        public static readonly Color BUEN_TIEMPO1 = (System.Windows.Media.Color)ColorConverter.ConvertFromString("#FF2C6F9B");
        public static readonly Color BUEN_TIEMPO2 = (System.Windows.Media.Color)ColorConverter.ConvertFromString("#4DB6AC");

        public static readonly Color REGULAR_TIEMPO1 = (System.Windows.Media.Color)ColorConverter.ConvertFromString("#F0A67100");
        public static readonly Color REGULAR_TIEMPO2 = (System.Windows.Media.Color)ColorConverter.ConvertFromString("#FFCA28");

        public static readonly Color MAL_TIEMPO1 = (System.Windows.Media.Color)ColorConverter.ConvertFromString("#F0A62600");
        public static readonly Color MAL_TIEMPO2 = (System.Windows.Media.Color)ColorConverter.ConvertFromString("#E64A19");

        public static readonly Color COLOR_POR_PAGAR = (System.Windows.Media.Color)ColorConverter.ConvertFromString("#0055ff");
    }
}
