﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Comandas
{
    /// <summary>
    /// Lógica de interacción para ArticuloSeleccionadoUC.xaml
    /// </summary>
    public partial class ArticuloSeleccionadoUC : UserControl
    {
        public static readonly RoutedEvent EliminarEvent = EventManager.RegisterRoutedEvent("Eliminar", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ArticuloSeleccionadoUC));
        public event RoutedEventHandler Eliminar
        {
            add
            {
                this.AddHandler(EliminarEvent, value);
            }

            remove
            {
                this.RemoveHandler(EliminarEvent, value);
            }
        }
        ResumenArticulo resumenActual 
        {
            get { return DataContext as ResumenArticulo; }
            set { DataContext = value; }
        }

        public ArticuloSeleccionadoUC()
        {
            InitializeComponent();
            ctbCantidad.MaximumValue = int.MaxValue;
        }

        //protected override void OnPreviewMouseMove(MouseEventArgs e)
        //{
        //    base.OnPreviewMouseMove(e);
        //    if (e.LeftButton == MouseButtonState.Pressed)
        //    {
        //         //Package the data.
        //        DataObject data = new DataObject();
        //        data.SetData(typeof(ResumenArticulo).FullName, resumenActual);

        //         //Inititate the drag-and-drop operation.
        //        DragDrop.DoDragDrop(this, data, DragDropEffects.Move);

        //        e.Handled = true;
        //    }
        //}
        /*
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                //         Package the data.
                DataObject data = new DataObject();
                data.SetData(typeof(ResumenArticulo).FullName, resumenActual);

                //         Inititate the drag-and-drop operation.
                DragDrop.DoDragDrop(this, data, DragDropEffects.Move);
            }
        }

        protected override void OnGiveFeedback(GiveFeedbackEventArgs e)
        {
            base.OnGiveFeedback(e);
            // These Effects values are set in the drop target's
            // DragOver event handler.
            if (e.Effects.HasFlag(DragDropEffects.Move))
            {
                Mouse.SetCursor(Cursors.Hand);
            }
            else
            {
                Mouse.SetCursor(Cursors.No);
            }
            e.Handled = true;
        }*/

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(EliminarEvent));
        }

        private void btnReducir_Click(object sender, RoutedEventArgs e)
        {
            if (ctbCantidad.Number <= ctbCantidad.MinimumValue)
                ctbCantidad.Number = ctbCantidad.MinimumValue;
            else
                ctbCantidad.Number -= 1;
        }

        private void btnAumentar_Click(object sender, RoutedEventArgs e)
        {
            if (ctbCantidad.Number >= ctbCantidad.MaximumValue)
                ctbCantidad.Number = ctbCantidad.MaximumValue;
            else
                ctbCantidad.Number += 1;
        }

        private void ctbCantidad_LostFocus(object sender, RoutedEventArgs e)
        {
            if (ctbCantidad.Number <= ctbCantidad.MinimumValue)
                ctbCantidad.Number = ctbCantidad.MinimumValue;
            else if (ctbCantidad.Number >= ctbCantidad.MaximumValue)
                ctbCantidad.Number = ctbCantidad.MaximumValue;
        }



        //protected override void OnDrop(DragEventArgs e)
        //{
        //    base.OnDrop(e);

        //    // If the DataObject contains string data, extract it.
        //    if (e.Data.GetDataPresent(DataFormats.StringFormat))
        //    {
        //        string dataString = (string)e.Data.GetData(DataFormats.StringFormat);

        //        // If the string can be converted into a Brush, 
        //        // convert it and apply it to the ellipse.
        //        BrushConverter converter = new BrushConverter();
        //        if (converter.IsValid(dataString))
        //        {
        //            Brush newFill = (Brush)converter.ConvertFromString(dataString);
        //            circleUI.Fill = newFill;

        //            // Set Effects to notify the drag source what effect
        //            // the drag-and-drop operation had.
        //            // (Copy if CTRL is pressed; otherwise, move.)
        //            if (e.KeyStates.HasFlag(DragDropKeyStates.ControlKey))
        //            {
        //                e.Effects = DragDropEffects.Copy;
        //            }
        //            else
        //            {
        //                e.Effects = DragDropEffects.Move;
        //            }
        //        }
        //    }
        //    e.Handled = true;
        //}
        //protected override void OnDragOver(DragEventArgs e)
        //{
        //    base.OnDragOver(e);
        //    e.Effects = DragDropEffects.None;

        //    // If the DataObject contains string data, extract it.
        //    if (e.Data.GetDataPresent(DataFormats.StringFormat))
        //    {
        //        string dataString = (string)e.Data.GetData(DataFormats.StringFormat);

        //        // If the string can be converted into a Brush, allow copying or moving.
        //        BrushConverter converter = new BrushConverter();
        //        if (converter.IsValid(dataString))
        //        {
        //            // Set Effects to notify the drag source what effect
        //            // the drag-and-drop operation will have. These values are 
        //            // used by the drag source's GiveFeedback event handler.
        //            // (Copy if CTRL is pressed; otherwise, move.)
        //            if (e.KeyStates.HasFlag(DragDropKeyStates.ControlKey))
        //            {
        //                e.Effects = DragDropEffects.Copy;
        //            }
        //            else
        //            {
        //                e.Effects = DragDropEffects.Move;
        //            }
        //        }
        //    }
        //    e.Handled = true;
        //}

        //protected override void OnDragEnter(DragEventArgs e)
        //{
        //    base.OnDragEnter(e);
        //    // Save the current Fill brush so that you can revert back to this value in DragLeave.
        //    _previousFill = circleUI.Fill;

        //    // If the DataObject contains string data, extract it.
        //    if (e.Data.GetDataPresent(DataFormats.StringFormat))
        //    {
        //        string dataString = (string)e.Data.GetData(DataFormats.StringFormat);

        //        // If the string can be converted into a Brush, convert it.
        //        BrushConverter converter = new BrushConverter();
        //        if (converter.IsValid(dataString))
        //        {
        //            Brush newFill = (Brush)converter.ConvertFromString(dataString.ToString());
        //            circleUI.Fill = newFill;
        //        }
        //    }
        //}

        //protected override void OnDragLeave(DragEventArgs e)
        //{
        //    base.OnDragLeave(e);
        //    // Undo the preview that was applied in OnDragEnter.
        //    circleUI.Fill = _previousFill;
        //}
    }
}
