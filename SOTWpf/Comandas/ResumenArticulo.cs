﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Comandas
{
    public class ResumenArticulo: INotifyPropertyChanged
    {
        public ResumenArticulo(decimal porcentajeIVA) 
        {
            _porcentajeIVA = porcentajeIVA;
        }

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        //private int _idArticulo;
        //private string _nombre;
        //private string _tipo;
        private decimal _porcentajeIVA;
        private decimal _precio;
        private decimal _precioConIVA;
        private int _cantidad;
        private decimal _total, _totalSinIVA, _iva;
        private string _observaciones;
        private bool _esCortesia;

        public object Agrupador { get; set; }

        public int IdRelacional { get; set; }
        public string IdArticulo { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
        public decimal Precio
        {
            get
            {
                return _precio;
            }
            set
            {
                _precio = value;

                NotifyPropertyChanged();

                _precioConIVA = Math.Round(_precio * (1 + _porcentajeIVA), 2);

                TotalSinIVA = Precio * _cantidad;
                Total = _precioConIVA * _cantidad;
                IVA = TotalSinIVA * _porcentajeIVA;
            }
        }
        public int Cantidad
        {
            get { return _cantidad; }
            set
            {
                //if (value > 0)
                _cantidad = value;
                //else
                //    _cantidad = 1;

                TotalSinIVA = Precio * _cantidad;
                Total = _precioConIVA * _cantidad;
                IVA = TotalSinIVA * _porcentajeIVA;

                NotifyPropertyChanged();
            }
        }
        public decimal Total
        {
            get
            {
                return _total;//Precio * Cantidad;
            }
            private set
            {
                _total = value;
                NotifyPropertyChanged();
            }
        }
        public decimal TotalSinIVA
        {
            get
            {
                return _totalSinIVA;//Precio * Cantidad;
            }
            private set
            {
                _totalSinIVA = value;
                NotifyPropertyChanged();
            }
        }
        public decimal IVA
        {
            get
            {
                return _iva;//Precio * Cantidad;
            }
            private set
            {
                _iva = value;
                NotifyPropertyChanged();
            }
        }

        public string Observaciones
        {
            get { return _observaciones; }
            set
            {
                _observaciones = value;

                NotifyPropertyChanged();

                Alerta = "";
            }
        }

        public string Alerta
        {
            get { return (Observaciones != null && Observaciones.Length > 0) ? "!" : ""; }
            set
            {
                NotifyPropertyChanged();
            }
        }

        public string TextoEsCortesia
        {
            get { return EsCortesia ? "C" : ""; }
            set
            {
                NotifyPropertyChanged();
            }
        }

        public bool EsCortesia
        {
            get { return _esCortesia; }
            set
            {
                _esCortesia = value;

                NotifyPropertyChanged();

                TextoEsCortesia = "";
            }
        }

        internal bool EsCortesiaOriginal { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
