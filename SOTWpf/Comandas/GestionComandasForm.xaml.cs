﻿using SOTWpf.Comandas.Manejadores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
//using ZctSOT;

namespace SOTWpf.Comandas
{
    /// <summary>
    /// Lógica de interacción para GestionComandasForm.xaml
    /// </summary>
    public partial class GestionComandasForm : Window
    {
        public GestionComandasForm(IManejadorComandas gcfm)
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();

            DataContext = gcfm;
        }

        public void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
