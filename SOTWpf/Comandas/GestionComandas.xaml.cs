﻿using SOTWpf.Comandas.Manejadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SOTWpf.Comandas
{
    /// <summary>
    /// Lógica de interacción para GestionComandas.xaml
    /// </summary>
    public partial class GestionComandas : UserControl
    {
        private DispatcherTimer timerCargaComandas;

        IManejadorComandas _gcfm, tmp;

        bool seguirCargando = true;

        public GestionComandas()
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();
        }

        private void CargarComandas()
        {
            tmp = _gcfm;

            if (tmp == null)
                return;

            tmp.CargarComandas();
        }

        private void GestionComandas_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarComandas();

                timerCargaComandas = new DispatcherTimer();
                timerCargaComandas.Interval = TimeSpan.FromSeconds(15);
                timerCargaComandas.Tick += timerCargaComandas_Tick;

                timerCargaComandas.Start();
            }
        }

        private void timerCargaComandas_Tick(object sender, EventArgs e)
        {
            if (seguirCargando)
                CargarComandas();
        }

        private void GestionComandas_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                seguirCargando = false;

                if (timerCargaComandas != null)
                {
                    timerCargaComandas.Stop();
                    timerCargaComandas.Tick -= timerCargaComandas_Tick;
                }
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            tmp = _gcfm;

            if (tmp == null)
                return;

            timerCargaComandas.Stop();
            try
            {
                tmp.Agregar(Application.Current.MainWindow);
            }
            finally
            {
                timerCargaComandas.Start();
            }
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _gcfm = e.NewValue as IManejadorComandas;

            CargarComandas();
        }

        private void btnAgregar_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
                btnAgregar.Visibility = System.Windows.Visibility.Visible;
            else
                btnAgregar.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
