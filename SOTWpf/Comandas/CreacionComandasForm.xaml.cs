﻿using SOTControladores.Controladores;
using SOTWpf.Comandas.Manejadores;
using SOTWpf.ConceptosSistema;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Modelo.Entidades;

namespace SOTWpf.Comandas
{
    /// <summary>
    /// Lógica de interacción para CreacionComandasForm.xaml
    /// </summary>
    public partial class CreacionComandasForm : Window
    {
        static readonly string DataPresenter = typeof(ResumenArticulo).FullName; 
#if DEBUG

        int contadorClicksAceptar = 0;

#endif
        IManejadorCreacionComandas ccm;

        bool EsCambio = false;
        Modelo.Entidades.Comanda comandaactual = null;

        public bool ProcesoExitoso
        {
            get { return ccm.ProcesoExitoso; }
        }

        public CreacionComandasForm(IManejadorCreacionComandas creacionComandasM, bool?EsCambioDeArticulos=false, Modelo.Entidades.Comanda ComandaActual=null)
        {
            InitializeComponent();

            ccm = creacionComandasM;

            DataContext = ccm;

            comandaactual = ComandaActual;
            EsCambio = Convert.ToBoolean(EsCambioDeArticulos);
            //gridProductos.ItemsSource = ccm.Resumenes;

            //tcProductos.ItemsSource = ccm.Tipos;

            txtBusqueda.Focus();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {

            if(EsCambio)
            {
                var motivoF = new CapturaMotivo();
                motivoF.Motivo = "Cambio";
                motivoF.TextoAuxiliar = "Detalles";

                var aeh = new CapturaMotivo.AceptarEventHandler((s, ea) =>
                {
                    if (comandaactual != null)
                    {
                         var controlador = new ControladorRoomServices();
                         controlador.CrearMotivoDeCambio(comandaactual, motivoF.DescripcionMotivo);
#if DEBUG

                        Debug.Write(++contadorClicksAceptar);

#endif
                        ccm.Aceptar();

                        Close();
                    }

                });

                try
                {
                    motivoF.Aceptar += aeh;

                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, motivoF);
                }
                finally
                {
                    motivoF.Aceptar -= aeh;
                }

            }
            else
            {
#if DEBUG

                Debug.Write(++contadorClicksAceptar);

#endif
                ccm.Aceptar();

                Close();

            }
        }

        private void AgregarAComanda(object sender, RoutedEventArgs e)
        {
            //Button botonActual = (Button)sender;

            Modelo.Almacen.Entidades.Articulo articulo = ((Control)sender).DataContext as Modelo.Almacen.Entidades.Articulo;

            ccm.AgregarAComanda(articulo, tbActivarCortesia.IsChecked.Value || articulo.Prec_Art == 0);
            EnfocarTextoFiltro();
            //CollectionViewSource.GetDefaultView(gridProductos.ItemsSource).Refresh();
            //CollectionViewSource.GetDefaultView(gridCortesias.ItemsSource).Refresh();

            //gridProductos.ItemsSource = null;
            //gridProductos.ItemsSource = ccm.Resumenes;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //resumenes.Clear();
            //resumenes.CollectionChanged -= ResumenArticuloBindingSource_DataSourceChanged;
        }

        private void articulosCVS_filter(object sender, FilterEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtBusqueda.Text))
            {
                var item = e.Item as Modelo.Almacen.Entidades.Articulo;

                e.Accepted = item != null && item.Desc_Art.ToUpper().Contains(txtBusqueda.Text.ToUpper());
            }
            else
                e.Accepted = true;
        }

        private void txtBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(tcProductos.ItemsSource).Refresh();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var resumen = ((Control)sender).DataContext as ResumenArticulo;
            
            if (resumen != null)
            {
                ccm.EliminarDeComanda(resumen);

                EnfocarTextoFiltro();

                //CollectionViewSource.GetDefaultView(gridProductos.ItemsSource).Refresh();
                //CollectionViewSource.GetDefaultView(gridCortesias.ItemsSource).Refresh();
            }
        }

        //private void panel_DragOver(object sender, DragEventArgs e)
        //{
        //    if (e.Data.GetDataPresent(DataPresenter))
        //    {
        //        // These Effects values are used in the drag source's
        //        // GiveFeedback event handler to determine which cursor to display.
        //        if (e.KeyStates == DragDropKeyStates.ControlKey)
        //        {
        //            e.Effects = DragDropEffects.Copy;
        //        }
        //        else
        //        {
        //            e.Effects = DragDropEffects.Move;
        //        }
        //    }
        //}

        private void cortesia_Drop(object sender, DragEventArgs e)
        {
            if (tbActivarCortesia.IsChecked != true)
                return;

            // If an element in the panel has already handled the drop,
            // the panel should not also handle it.
            if (!e.Handled)
            {
                //ItemsControl _panel = (ItemsControl)sender;
                ResumenArticulo _element = (ResumenArticulo)e.Data.GetData(DataPresenter);

                if (/*_panel != null && */_element != null)
                {
                    // Get the panel that the element currently belongs to,
                    // then remove it from that panel and add it the Children of
                    // the panel that its been dropped on.
                    //ItemsControl _parent = (ItemsControl)VisualTreeHelper.GetParent(_element);

                    //if (_parent != null)
                    //{
                    if (e.AllowedEffects.HasFlag(DragDropEffects.Move))
                    {
                        _element.EsCortesia = true;

                        //_parent.Children.Remove(_element);
                        //_panel.Children.Add(_element);
                        // set the value to return to the DoDragDrop call
                        e.Effects = DragDropEffects.Move;

                        CollectionViewSource.GetDefaultView(gridProductos.ItemsSource).Refresh();
                        CollectionViewSource.GetDefaultView(gridCortesias.ItemsSource).Refresh();

                        EnfocarTextoFiltro();
                    }
                    //}
                }
            }
        }

        private void normal_Drop(object sender, DragEventArgs e)
        {
            if (!e.Handled)
            {
                //ItemsControl _panel = (ItemsControl)sender;
                ResumenArticulo _element = (ResumenArticulo)e.Data.GetData(DataPresenter);

                if (/*_panel != null && */_element != null)
                {
                    // Get the panel that the element currently belongs to,
                    // then remove it from that panel and add it the Children of
                    // the panel that its been dropped on.
                    //ItemsControl _parent = (ItemsControl)VisualTreeHelper.GetParent(_element);

                    //if (_parent != null)
                    //{
                    if (e.AllowedEffects.HasFlag(DragDropEffects.Move) && _element.Precio != 0)
                    {
                        _element.EsCortesia = false;

                        //_parent.Children.Remove(_element);
                        //_panel.Children.Add(_element);
                        // set the value to return to the DoDragDrop call
                        e.Effects = DragDropEffects.Move;

                        CollectionViewSource.GetDefaultView(gridProductos.ItemsSource).Refresh();
                        CollectionViewSource.GetDefaultView(gridCortesias.ItemsSource).Refresh();

                        EnfocarTextoFiltro();
                    }
                    //}
                }
            }
        }

        private void resumenesCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ResumenArticulo;

            e.Accepted = item != null && !item.EsCortesia;
        }

        private void resumenesCortesiaCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ResumenArticulo;

            e.Accepted = item != null && item.EsCortesia;
        }

        private void tbActivarCortesia_Checked(object sender, RoutedEventArgs e)
        {
            filaCortesias.Height = new GridLength(1, GridUnitType.Star);

            EnfocarTextoFiltro();
        }

        private void tbActivarCortesia_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!ccm.Resumenes.Any(m=> m.EsCortesia))
                filaCortesias.Height = new GridLength(1, GridUnitType.Auto);

            EnfocarTextoFiltro();
        }

        private void EnfocarTextoFiltro()
        {
            Dispatcher.BeginInvoke((ThreadStart)delegate
            {
                txtBusqueda.Focus();
            });
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void txtBusqueda_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBusqueda.Text))
                txtBusqueda.SelectAll();
        }
    }
}
