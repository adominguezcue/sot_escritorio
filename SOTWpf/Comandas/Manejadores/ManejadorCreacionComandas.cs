﻿using SOTControladores.Controladores;
using SOTWpf.ConceptosSistema;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Transversal.Excepciones;

namespace SOTWpf.Comandas.Manejadores
{
    public class ManejadorCreacionComandas : IManejadorCreacionComandas
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool _procesoExitoso = false;

        private int idRentaActual;

        private Modelo.Almacen.Entidades.ZctCatPar config = new Modelo.Almacen.Entidades.ZctCatPar();

        decimal _cortesia, _cortesiaConIVA, _descuento, _descuentoConIVA, _subtotal, _subtotalConIVA, _totalSinIVA, _iva, _totalConIVA;

        string _etiquetaDestino;

        public bool SoportaCortesias
        {
            get { return premio == null; }
        }
        public string EtiquetaDestino
        {
            get { return _etiquetaDestino; }
            private set
            {
                _etiquetaDestino = value;

                NotifyPropertyChanged();
            }
        }
        //public decimal Cortesia
        //{
        //    get { return _cortesia; }
        //    private set
        //    {
        //        _cortesia = value;

        //        NotifyPropertyChanged();

        //        CortesiaConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}
        //public decimal Descuento
        //{
        //    get { return _descuento; }
        //    private set
        //    {
        //        _descuento = value;
        //        NotifyPropertyChanged();

        //        DescuentoConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            private set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            private set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }

        //public decimal Subtotal
        //{
        //    get { return _subtotal; }
        //    private set
        //    {
        //        _subtotal = value;
        //        NotifyPropertyChanged();

        //        SubtotalConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            private set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public decimal TotalSinIVA
        //{
        //    get { return _totalSinIVA; }
        //    private set
        //    {
        //        _totalSinIVA = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        //public decimal IVA
        //{
        //    get { return _iva; }
        //    private set
        //    {
        //        _iva = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        public decimal TotalConIVA
        {
            get { return _totalConIVA; }
            private set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }

        ObservableCollection<ResumenArticulo> _resumenes;
        List<Modelo.Almacen.Entidades.Linea> _tipos;

        public ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            private set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public List<Modelo.Almacen.Entidades.Linea> Lineas
        {
            get { return _tipos; }
            private set
            {
                _tipos = value;
                NotifyPropertyChanged();
            }
        }

        private Modelo.Entidades.Comanda comandaActual;
        private Modelo.Entidades.Premio premio;
        private string cupon;

        public bool ProcesoExitoso
        {
            get { return _procesoExitoso; }
            private set { _procesoExitoso = value; }
        }

        public ManejadorCreacionComandas(int idRenta, string destino)
        {
            idRentaActual = idRenta;

            EtiquetaDestino = destino;

            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarArticulos();
        }

        public ManejadorCreacionComandas(Modelo.Entidades.Comanda comanda, string destino)
        {
            comandaActual = comanda;

            EtiquetaDestino = destino;

            if (comandaActual != null)
                idRentaActual = comandaActual.IdRenta;
            else
                idRentaActual = 0;

            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarArticulos();

            CargarDetallesComanda();
        }

        public ManejadorCreacionComandas(int idRenta, string cupon, Modelo.Entidades.Premio premio, string destino)
        {
            this.premio = premio;
            this.cupon = cupon;
            idRentaActual = idRenta;

            EtiquetaDestino = destino;

            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarArticulos();

            CargarDetallesPremio();

            
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (ResumenArticulo item in e.OldItems)
                {
                    //Removed items
                    item.PropertyChanged -= ResumenPropertyChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (ResumenArticulo item in e.NewItems)
                {
                    //Added items
                    item.PropertyChanged += ResumenPropertyChanged;
                }
            }


            RecalcularTotales();
        }

        private void ResumenPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RecalcularTotales();
        }

        private void RecalcularTotales()
        {
            if (Resumenes.Count == 0)
            {
                //Cortesia = 0;
                //Descuento = 0;
                //Subtotal = 0;
                //TotalSinIVA = 0;
                //IVA = 0;
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {

                //Cortesia = Resumenes.Where(m => m.EsCortesia).Sum(m => m.TotalSinIVA);

                //Subtotal = Resumenes.Sum(m => m.TotalSinIVA);
                
                //Descuento = premio != null ? Subtotal : 0;

                //TotalSinIVA = Subtotal - Cortesia - Descuento;
                //IVA = TotalSinIVA * config.Iva_CatPar;

                SubtotalConIVA = Resumenes.Sum(m => m.Total);
                CortesiaConIVA = Resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                DescuentoConIVA = premio != null ? SubtotalConIVA : 0;
                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public void CargarArticulos(string filtro = null)
        {
            config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            var controlador = new ControladorArticulos();

            var tipos = controlador.ObtenerLineasConArticulos(true, false, filtro);

            if (comandaActual != null)
            {
                foreach (var articulo in tipos.SelectMany(m => m.Articulos))
                {
                    var articuloComanda = comandaActual.ArticulosComanda.FirstOrDefault(m => m.Activo && m.IdArticulo == articulo.Cod_Art);

                    if (articuloComanda != null)
                        articulo.Prec_Art = articuloComanda.PrecioUnidad;
                }
            }

            Lineas = tipos;
        }

        public void Aceptar()
        {
            //decimal subtotal = default(decimal);
            //decimal cortesia = 0;
            //decimal descuento = 0;
            //if (Resumenes.Count > 0)
            //{
            //    subtotal = Resumenes.Sum(m => m.TotalSinIVA);

            //}
            //else
            //{
            //    subtotal = 0;
            //}

            RecalcularTotales();

            if (comandaActual == null)
            {
                try
                {
                    var config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

                    comandaActual = new Modelo.Entidades.Comanda();

                    comandaActual.Activo = true;
                    comandaActual.ValorSinIVA = SubtotalConIVA / (1 + config.Iva_CatPar);//subtotal - descuento;
                    comandaActual.ValorIVA = SubtotalConIVA - comandaActual.ValorSinIVA;//comandaActual.ValorSinIVA * config.Iva_CatPar;
                    comandaActual.ValorConIVA = SubtotalConIVA;//comandaActual.ValorSinIVA + comandaActual.ValorIVA;
                    comandaActual.IdRenta = idRentaActual;

                    if (cupon != null)
                        comandaActual.Cupon = cupon;

                    foreach (ResumenArticulo item in Resumenes)
                    {
                        var nuevoArticuloComanda = new Modelo.Entidades.ArticuloComanda();
                        nuevoArticuloComanda.Activo = true;
                        nuevoArticuloComanda.IdArticulo = item.IdArticulo;
                        nuevoArticuloComanda.Cantidad = item.Cantidad;
                        nuevoArticuloComanda.PrecioUnidad = item.Precio;
                        nuevoArticuloComanda.Observaciones = item.Observaciones;
                        nuevoArticuloComanda.EsCortesia = item.EsCortesia;

                        comandaActual.ArticulosComanda.Add(nuevoArticuloComanda);
                    }

                    var controlador = new ControladorRoomServices();

                    controlador.CrearComanda(comandaActual);
                }
                catch
                {
                    comandaActual = null;
                    throw;
                }

            }
            else
            {
                var articulos = new List<Modelo.Dtos.DtoArticuloPrepararGeneracion>();

                foreach (ResumenArticulo item in Resumenes)
                {
                    Modelo.Dtos.DtoArticuloPrepararGeneracion nuevoArticuloComanda = new Modelo.Dtos.DtoArticuloPrepararGeneracion
                    {
                        Activo = true,
                        IdArticulo = item.IdArticulo,
                        Cantidad = item.Cantidad,
                        PrecioConIVA = item.Precio,
                        Id = item.IdRelacional,
                        Observaciones = item.Observaciones,
                        EsCortesia = item.EsCortesia
                    };

                    articulos.Add(nuevoArticuloComanda);
                }

                foreach (var articuloComanda in comandaActual.ArticulosComanda.Where(m => m.Activo && !Resumenes.Any(r => r.IdRelacional == m.Id)))
                {
                    Modelo.Dtos.DtoArticuloPrepararGeneracion nuevoArticuloComanda = new Modelo.Dtos.DtoArticuloPrepararGeneracion
                    {
                        Activo = false,
                        IdArticulo = articuloComanda.IdArticulo,
                        Cantidad = articuloComanda.Cantidad,
                        PrecioConIVA = articuloComanda.PrecioUnidad,
                        Id = articuloComanda.Id
                    };

                    articulos.Add(nuevoArticuloComanda);
                }

                var controlador = new ControladorRoomServices();

                controlador.ModificarComanda(comandaActual.Id, articulos);
            }

            ProcesoExitoso = true;

        }

        public void AgregarAComanda(Modelo.Almacen.Entidades.Articulo articulo, bool esCortesia)
        {
            if (premio != null || (comandaActual != null && !string.IsNullOrEmpty(comandaActual.Cupon)))
                return;

            if (Resumenes.Any(m => m.IdArticulo == articulo.Cod_Art && m.EsCortesia == esCortesia))
            {
                Resumenes.First(m => m.IdArticulo == articulo.Cod_Art && m.EsCortesia == esCortesia).Cantidad += 1;
            }
            else
            {
                ResumenArticulo nuevoArticulo = new ResumenArticulo(config.Iva_CatPar);
                nuevoArticulo.IdArticulo = articulo.Cod_Art;
                nuevoArticulo.Nombre = articulo.Desc_Art;
                nuevoArticulo.Tipo = articulo.NombreLineaTmp;
                nuevoArticulo.Cantidad = 1;
                nuevoArticulo.Precio = articulo.Prec_Art;
                nuevoArticulo.EsCortesia = esCortesia;

                Resumenes.Add(nuevoArticulo);
            }
        }

        public void EliminarDeComanda(ResumenArticulo resumen)
        {
            if (premio != null || (comandaActual != null && !string.IsNullOrEmpty(comandaActual.Cupon)))
                return;

            if (Resumenes.Contains(resumen))
                Resumenes.Remove(resumen);
        }

        private void CargarDetallesComanda()
        {
            if (comandaActual != null)
            {
                foreach (var articuloComanda in comandaActual.ArticulosComanda)
                {
                    if (!articuloComanda.Activo)
                        continue;

                    ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                    nuevoResumen.IdRelacional = articuloComanda.Id;
                    nuevoResumen.IdArticulo = articuloComanda.IdArticulo;
                    nuevoResumen.Nombre = articuloComanda.ArticuloTmp.Desc_Art;
                    nuevoResumen.Tipo = articuloComanda.ArticuloTmp.NombreLineaTmp;
                    nuevoResumen.Cantidad = articuloComanda.Cantidad;
                    nuevoResumen.Precio = articuloComanda.PrecioUnidad;
                    nuevoResumen.Observaciones = articuloComanda.Observaciones;
                    nuevoResumen.EsCortesia = articuloComanda.EsCortesia;

                    Resumenes.Add(nuevoResumen);
                }
            }
        }

        private void CargarDetallesPremio()
        {
            if (premio != null)
            {
                foreach (var articuloPremio in premio.ArticulosPremio)
                {
                    if (!articuloPremio.Activo)
                        continue;

                    var art = Lineas.SelectMany(m => m.Articulos).FirstOrDefault(m => m.Cod_Art == articuloPremio.CodigoArticulo);

                    if (art == null)
                        throw new SOTException(Textos.Errores.articulo_inexistente_exception, articuloPremio.CodigoArticulo);

                    ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                    nuevoResumen.IdRelacional = articuloPremio.Id;
                    nuevoResumen.IdArticulo = articuloPremio.CodigoArticulo;
                    nuevoResumen.Nombre = art.Desc_Art;
                    nuevoResumen.Tipo = art.NombreLineaTmp;
                    nuevoResumen.Cantidad = articuloPremio.Cantidad;
                    nuevoResumen.Precio = art.Prec_Art;
                    nuevoResumen.EsCortesia = true;

                    Resumenes.Add(nuevoResumen);
                }
            }
        }

        //private void Window_Closing(object sender, CancelEventArgs e)
        //{
        //    Resumenes.Clear();
        //    Resumenes.CollectionChanged -= ResumenArticuloBindingSource_DataSourceChanged;
        //}


    }
}
