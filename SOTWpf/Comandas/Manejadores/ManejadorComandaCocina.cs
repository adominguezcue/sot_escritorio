﻿using SOTControladores.Controladores;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Transversal.Extensiones;

namespace SOTWpf.Comandas.Manejadores
{
    public class ManejadorComandaCocina: ManejadorComandaBase
    {
        int _numeroOrden;
        ObservableCollection<ResumenArticulo> _resumenes;

        public override ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            protected set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public override int NumeroOrden
        {
            get { return _numeroOrden; }
            protected set
            {
                _numeroOrden = value;
                NotifyPropertyChanged();
            }
        }

        string _estadoComanda, _tiposComanda, _informacionPertenencia;
        bool _enPreparacion;

        public override string EstadoComanda
        {
            get { return _estadoComanda; }
            protected set
            {
                _estadoComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string TiposComanda
        {
            get { return _tiposComanda; }
            protected set
            {
                _tiposComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string InformacionPertenencia
        {
            get { return _informacionPertenencia; }
            protected set
            {
                _informacionPertenencia = value;
                NotifyPropertyChanged();
            }
        }



        decimal _cortesia, _cortesiaConIVA, _descuento, _descuentoConIVA, _subtotal, _subtotalConIVA, _totalSinIVA, _iva, _totalConIVA;

        public override decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            protected set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public override decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            protected set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }

        public override decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            protected set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }

        public override decimal TotalConIVA
        {
            get { return _totalConIVA; }
            protected set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }

        public override bool EnPreparacion
        {
            get { return _enPreparacion; }
        }

        private Modelo.Dtos.DtoResumenComandaExtendido comandaActual;

        private TimeSpan _tiempo;

        public override TimeSpan Tiempo
        {
            get { return _tiempo; }
            protected set
            {
                _tiempo = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorComandaCocina(Modelo.Dtos.DtoResumenComandaExtendido detalles, bool enPreparacion)
        {
            comandaActual = detalles;
            _enPreparacion = enPreparacion;
            base.Initialize();
        }

        protected override void Inicializar()
        {
            base.Inicializar();

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarDetallesComanda();
        }

        private void CargarDetallesComanda()
        {
            //comandaActual = detalles;



            if (comandaActual != null)
            {
                Tiempo = DateTime.Now - comandaActual.FechaInicio;


                string cadenaTipos = string.Empty;

                if (comandaActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Equals("ALIMENTOS")))
                    cadenaTipos += "A";

                if (comandaActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Equals("BEBIDAS")))
                    cadenaTipos += "B";

                if (comandaActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Contains("SEX")))
                    cadenaTipos += "S";

                foreach (var articuloComanda in comandaActual.ArticulosComanda)
                {

                    

                    if (!articuloComanda.Activo)
                        continue;


                    if (articuloComanda.EsOtroDepto == true)
                        cadenaTipos = articuloComanda.OtosDeptos;

                    ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                    nuevoResumen.IdArticulo = articuloComanda.IdArticulo;
                    nuevoResumen.Nombre = articuloComanda.Nombre;
                    nuevoResumen.Tipo = articuloComanda.Tipo;
                    nuevoResumen.Cantidad = articuloComanda.Cantidad;
                    nuevoResumen.Precio = articuloComanda.PrecioUnidad;
                    nuevoResumen.Observaciones = articuloComanda.Observaciones;
                    nuevoResumen.EsCortesia = articuloComanda.EsCortesia;

                    Resumenes.Add(nuevoResumen);
                }


                ColorearBrush(comandaActual.FechaInicio, comandaActual.Estado);

                EstadoComanda = comandaActual.Estado.Descripcion();
                TiposComanda = cadenaTipos;
                InformacionPertenencia = "Habitación " + comandaActual.Destino;
                NumeroOrden = comandaActual.Orden;
            }
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Resumenes.Count == 0)
            {
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {
                SubtotalConIVA = Resumenes.Sum(m => m.Total);
                CortesiaConIVA = Resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                DescuentoConIVA = 0;
                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public override void Aceptar()
        {
            if (comandaActual != null)
            {
                var idsArticulosComandas = comandaActual.ArticulosComanda.Where(m => m.Activo && m.Estado == Modelo.Entidades.ArticuloComanda.Estados.EnProceso).Select(m => m.Id).ToList();

                if (idsArticulosComandas.Count > 0)
                {
                    var controladorRS = new ControladorRoomServices();
                    controladorRS.FinalizarElaboracionProductos(comandaActual.Id, idsArticulosComandas);
                }
                else
                {
                    idsArticulosComandas = comandaActual.ArticulosComanda.Where(m => m.Activo && m.Estado == Modelo.Entidades.ArticuloComanda.Estados.PorEntregar).Select(m => m.Id).ToList();

                    if (idsArticulosComandas.Count > 0)
                    {
                        var controladorRS = new ControladorRoomServices();

                        controladorRS.EntregarProductos(comandaActual.Id, idsArticulosComandas/*, seleccionadorMeseroF.MeseroSeleccionado.Id*/);
                    }
                    else
                        return;
                }

                OnPostCobro(EventArgs.Empty);
            }
        }

        

        public override void SolicitarCambio()
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void SolicitarCancelacion()
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void Sincronizar(object comandaUOrden)
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void Imprimir()
        {
            throw new NotSupportedException("Operación no soportada");
        }
    }
}
