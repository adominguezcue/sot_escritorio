﻿using System;
using System.ComponentModel;
namespace SOTWpf.Comandas.Manejadores
{
    public interface IManejadorComandas : INotifyPropertyChanged
    {
        string EtiquetaDestino { get; }
        bool AceptaMas { get; }
        void Agregar(System.Windows.Window padre);
        void CargarComandas();
        System.Collections.ObjectModel.ObservableCollection<ManejadorComandaBase> Manejadores { get; }
    }
}
