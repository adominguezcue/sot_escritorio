﻿using Modelo.Almacen.Entidades;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;

namespace SOTWpf.Comandas.Manejadores
{
    public interface IManejadorCreacionComandas: INotifyPropertyChanged
    {
        void Aceptar();
        void AgregarAComanda(Articulo articulo, bool esCortesia);
        void EliminarDeComanda(ResumenArticulo resumen);
        void CargarArticulos(string filtro);
        //decimal Cortesia { get; }
        //decimal Descuento { get; }
        decimal CortesiaConIVA { get; }
        decimal DescuentoConIVA { get; }
        bool ProcesoExitoso { get; }
        ObservableCollection<ResumenArticulo> Resumenes { get; }
        List<Linea> Lineas { get; }
        //decimal Subtotal { get; }
        decimal SubtotalConIVA { get; }
        //decimal TotalSinIVA { get; }
        //decimal IVA { get; }
        decimal TotalConIVA { get; }
        string EtiquetaDestino { get; }
        bool SoportaCortesias { get; }
    }
}
