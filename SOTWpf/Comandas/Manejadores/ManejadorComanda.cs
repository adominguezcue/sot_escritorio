﻿using Modelo.Entidades;
using Negocio.Seguridad.Permisos;
using SOTControladores.Controladores;
using SOTWpf.ConceptosSistema;
using SOTWpf.Empleados;
using SOTWpf.Pagos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.Comandas.Manejadores
{
    public class ManejadorComanda : ManejadorComandaBase 
    {
        int _numeroOrden;
        ObservableCollection<ResumenArticulo> _resumenes;

        public override string InformacionPertenencia { get; protected set; }

        public override ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            protected set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public override int NumeroOrden
        {
            get { return _numeroOrden; }
            protected set
            {
                _numeroOrden = value;
                NotifyPropertyChanged();
            }
        }

        string _estadoComanda, _tiposComanda;

        public override string EstadoComanda
        {
            get { return _estadoComanda; }
            protected set
            {
                _estadoComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string TiposComanda
        {
            get { return _tiposComanda; }
            protected set
            {
                _tiposComanda = value;
                NotifyPropertyChanged();
            }
        }

        decimal _cortesia, _cortesiaConIVA, _descuento, _descuentoConIVA, _subtotal, _subtotalConIVA, _totalSinIVA, _iva, _totalConIVA;

        //public override decimal Cortesia
        //{
        //    get { return _cortesia; }
        //    protected set
        //    {
        //        _cortesia = value;

        //        NotifyPropertyChanged();

        //        CortesiaConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}
        //public override decimal Descuento
        //{
        //    get { return _descuento; }
        //    protected set
        //    {
        //        _descuento = value;
        //        NotifyPropertyChanged();

        //        DescuentoConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            protected set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public override decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            protected set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal Subtotal
        //{
        //    get { return _subtotal; }
        //    protected set
        //    {
        //        _subtotal = value;
        //        NotifyPropertyChanged();

        //        SubtotalConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            protected set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal TotalSinIVA
        //{
        //    get { return _totalSinIVA; }
        //    protected set
        //    {
        //        _totalSinIVA = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        //public override decimal IVA
        //{
        //    get { return _iva; }
        //    protected set
        //    {
        //        _iva = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        public override decimal TotalConIVA
        {
            get { return _totalConIVA; }
            protected set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }


        public override bool EnPreparacion
        {
            get { return false; }
        }

        private Modelo.Entidades.Comanda comandaActual;
        string destino;

        private TimeSpan _tiempo;

        public override TimeSpan Tiempo
        {
            get { return _tiempo; }
            protected set
            {
                _tiempo = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorComanda(Modelo.Entidades.Comanda detalles, string destino)
        {
            comandaActual = detalles;
            this.destino = destino;
            base.Initialize();
        }

        protected override void Inicializar()
        {
            base.Inicializar();

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarDetallesComanda();
        }

        private void CargarDetallesComanda()
        {
            //comandaActual = detalles;

            if (comandaActual != null)
            {
                Tiempo = DateTime.Now - comandaActual.FechaCreacion;

                string cadenaTipos = string.Empty;

                if (comandaActual.ArticulosComanda.Any(m => m.Activo && m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Equals("ALIMENTOS")))
                    cadenaTipos += "A";

                if (comandaActual.ArticulosComanda.Any(m => m.Activo && m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Equals("BEBIDAS")))
                    cadenaTipos += "B";

                if (comandaActual.ArticulosComanda.Any(m => m.Activo && m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Contains("SEX")))
                    cadenaTipos += "S";

                foreach (Modelo.Entidades.ArticuloComanda articuloComanda in comandaActual.ArticulosComanda)
                {
                    if (!articuloComanda.Activo)
                        continue;

                    Resumenes.Add(new ResumenArticulo(config.Iva_CatPar)
                    {
                        IdArticulo = articuloComanda.IdArticulo,
                        Nombre = articuloComanda.ArticuloTmp.Desc_Art,
                        Tipo = articuloComanda.ArticuloTmp.NombreLineaTmp,
                        Cantidad = articuloComanda.Cantidad,
                        Precio = articuloComanda.PrecioUnidad,
                        Observaciones = articuloComanda.Observaciones,
                        EsCortesia = articuloComanda.EsCortesia
                    });
                }


                //if (comandaActual.Estado != Modelo.Entidades.Comanda.Estados.PorPagar && comandaActual.Estado != Modelo.Entidades.Comanda.Estados.Cobrada && comandaActual.Estado != Modelo.Entidades.Comanda.Estados.Cancelada)
                //    //(comandaActual.Estado == Modelo.Entidades.Comanda.Estados.Preparacion || comandaActual.Estado == Modelo.Entidades.Comanda.Estados.PorEntregar)
                //{
                //    var tiempo = System.DateTime.Now - comandaActual.FechaInicio;

                    ColorearBrush(comandaActual.FechaInicio, comandaActual.Estado);
                //}

                EstadoComanda = comandaActual.Estado.Descripcion();
                TiposComanda = cadenaTipos;
                NumeroOrden = comandaActual.Orden;
            }
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Resumenes.Count == 0)
            {
                //Cortesia = 0;
                //Descuento = 0;
                //Subtotal = 0;
                //TotalSinIVA = 0;
                //IVA = 0;
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {

                //Cortesia = Resumenes.Where(m => m.EsCortesia).Sum(m => m.TotalSinIVA);

                //Subtotal = Resumenes.Sum(m => m.TotalSinIVA);

                //Descuento = premio != null ? Subtotal : 0;

                //TotalSinIVA = Subtotal - Cortesia - Descuento;
                //IVA = TotalSinIVA * config.Iva_CatPar;

                SubtotalConIVA = Resumenes.Sum(m => m.Total);
                CortesiaConIVA = Resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                DescuentoConIVA = 0;
                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public override void Aceptar()
        {
            if (comandaActual != null)
            {
                var controladorRS = new ControladorRoomServices();

                /*if (comandaActual.Estado == Modelo.Entidades.Comanda.Estados.PorEntregar)
                {
                    var seleccionadorMeseroF = new SeleccionadorMeserosForm();

                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, seleccionadorMeseroF);

                    if (seleccionadorMeseroF.ProcesoExitoso)
                        controladorRent.EntregarComanda(comandaActual.Id, seleccionadorMeseroF.MeseroSeleccionado.Id);
                }
                else*/
                if (comandaActual.Estado == Modelo.Entidades.Comanda.Estados.PorCobrar)
                {
                    controladorRS.CobrarComanda(comandaActual.Id);
                }
                else if (comandaActual.Estado == Modelo.Entidades.Comanda.Estados.PorPagar)
                {
                    if (string.IsNullOrEmpty(comandaActual.Cupon))
                    {

                        SelectorFormaPagoForm cobroF = new SelectorFormaPagoForm(comandaActual.ValorConIVA, CortesiaConIVA, false, Resumenes.Any(m => m.EsCortesia) ?
                                SelectorFormaPagoForm.Justificaciones.Cortesia :
                                SelectorFormaPagoForm.Justificaciones.Ninguna);//, false, false);

                        Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, cobroF);

                        if (cobroF.ProcesoExitoso)
                            controladorRS.PagarComanda(comandaActual.Id, cobroF.FormasPagoSeleccionadas, cobroF.PropinaGenerada, cobroF.FormasPagoSeleccionadas.Count == 1 && cobroF.FormasPagoSeleccionadas.Any(m => m.TipoPago == Dominio.Nucleo.Entidades.TiposPago.Cortesia));
                    }
                    else
                    {
                        MessageBox.Show(Textos.Mensajes.comanda_pagada_cupon, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                        controladorRS.PagarComanda(comandaActual.Id, new List<Modelo.Dtos.DtoInformacionPago>(), null, false);
                    }
                }
                else
                    return;

                OnPostCobro(EventArgs.Empty);
            }
        }

        

        public override void SolicitarCambio()
        {
          /*  if (/*comanda.Estado != Comanda.Estados.PorCobrar &&*/
           /*     comandaActual.Estado != Comanda.Estados.PorEntregar &&
                comandaActual.Estado != Comanda.Estados.Preparacion)
                throw new SOTException(Textos.Errores.comanda_no_modificable_exception);*/
            /*Metemos credeiales codigo de arriba ya no va*/
            if (comandaActual.Estado == Comanda.Estados.PorPagar || comandaActual.Estado == Comanda.Estados.PorCobrar)
            {
                var controlador = new ControladorRoomServices();
                controlador.SolicitaCambioComanda();
            }
          
            var ccm = new ManejadorCreacionComandas(comandaActual, destino);

            var creacionComandaF = new CreacionComandasForm(ccm,true, comandaActual);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, creacionComandaF);

            OnPostCobro(EventArgs.Empty);
        }

        public override void SolicitarCancelacion()
        {
            var motivoF = new CapturaMotivo();
            motivoF.Motivo = "cancelación";
            motivoF.TextoAuxiliar = "Detalles";

            var aeh = new CapturaMotivo.AceptarEventHandler((s, ea) =>
            {
                if (comandaActual != null)
                {
                    var controlador = new ControladorRoomServices();
                    controlador.CancelarComanda(comandaActual.Id, motivoF.DescripcionMotivo);
                }

                OnPostCobro(EventArgs.Empty);
            });

            try
            {
                motivoF.Aceptar += aeh;

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, motivoF);
            }
            finally
            {
                motivoF.Aceptar -= aeh;
            }
        }

        public override void Sincronizar(object comandaUOrden)
        {
            var comanda = comandaUOrden as Modelo.Entidades.Comanda;

            if (comanda == null)
                throw new SOTException("El objeto a sincronizar es null o no es una comanda");

            comandaActual = comanda;
            base.Initialize();
        }

        public override void Imprimir()
        {
            if (comandaActual != null)
            {
                var controlador = new ControladorRoomServices();
                controlador.Imprimir(comandaActual.Id);

                MessageBox.Show("Impresión exitosa", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
