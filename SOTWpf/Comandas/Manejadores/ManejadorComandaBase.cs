﻿using Modelo.Entidades;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
namespace SOTWpf.Comandas.Manejadores
{
    public abstract class ManejadorComandaBase : INotifyPropertyChanged
    {
        protected Modelo.Almacen.Entidades.ZctCatPar config = new Modelo.Almacen.Entidades.ZctCatPar();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        System.Windows.Media.SolidColorBrush _gradiente;

        public event PostCobroEventHandler PostCobro;
        public delegate void PostCobroEventHandler(object sender, EventArgs e);

        //public abstract decimal Cortesia { get; protected set; }
        //public abstract decimal Descuento { get; protected set; }
        public abstract TimeSpan Tiempo { get; protected set; }
        public abstract decimal CortesiaConIVA { get; protected set; }
        public abstract decimal DescuentoConIVA { get; protected set; }
        public abstract string EstadoComanda { get; protected set; }
        public System.Windows.Media.SolidColorBrush Gradiente 
        {
            get { return _gradiente; }
            protected set
            {
                _gradiente = value;
                NotifyPropertyChanged();
            }
        }
        public bool Gestionable { get; protected set; }
        public abstract int NumeroOrden { get; protected set; }
        public abstract System.Collections.ObjectModel.ObservableCollection<SOTWpf.Comandas.ResumenArticulo> Resumenes { get; protected set; }
        //public abstract decimal Subtotal { get; protected set; }
        public abstract decimal SubtotalConIVA { get; protected set; }
        public abstract string TiposComanda { get; protected set; }
        public abstract string InformacionPertenencia { get; protected set; }
        //public abstract decimal TotalSinIVA { get; protected set; }
        //public abstract decimal IVA { get; protected set; }
        public abstract decimal TotalConIVA { get; protected set; }
        public abstract bool EnPreparacion { get; }

        public abstract void Aceptar();
        public abstract void SolicitarCambio();
        public abstract void SolicitarCancelacion();

        public abstract void Sincronizar(object comandaUOrden);

        protected void Initialize() 
        {
            Inicializar();
        }

        protected virtual void Inicializar() 
        {
            Gestionable = true;

            config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            Gradiente = new System.Windows.Media.SolidColorBrush
            {
                Color = Colors.Gray
                //StartPoint = new Point(0, 0.5),
                //EndPoint = new Point(1, 0.5)
            };

            //Gradiente.GradientStops.Add(new GradientStop { Offset = 0, Color = Colors.Gray });
            //Gradiente.GradientStops.Add(new GradientStop { Offset = 1, Color = Colors.DarkGray });
        }

        protected void OnPostCobro(EventArgs eventArgs)
        {
            var eh = PostCobro;

            if (eh != null)
                eh(this, eventArgs);
        }

        protected void ColorearBrush(DateTime fechaInicio, Comanda.Estados estadoComanda)
        {
            if (estadoComanda != Comanda.Estados.Preparacion && estadoComanda != Comanda.Estados.PorEntregar && estadoComanda != Comanda.Estados.PorCobrar/* && estadoComanda != Comanda.Estados.Cancelada*/)
            {
                if (estadoComanda == Comanda.Estados.PorPagar)
                    Gradiente.Color = ColoresComandas.COLOR_POR_PAGAR; 
            }
            else
            {
                var tiempo = System.DateTime.Now - fechaInicio;

                if (tiempo.TotalSeconds < 600)
                {
                    Gradiente.Color = ColoresComandas.BUEN_TIEMPO2;
                    //Gradiente.GradientStops[0].Color = ColoresComandas.BUEN_TIEMPO1;
                    //Gradiente.GradientStops[1].Color = ColoresComandas.BUEN_TIEMPO2;
                }
                else if (tiempo.TotalSeconds < 900)
                {
                    Gradiente.Color = ColoresComandas.REGULAR_TIEMPO2;
                    //Gradiente.GradientStops[0].Color = ColoresComandas.REGULAR_TIEMPO1;
                    //Gradiente.GradientStops[1].Color = ColoresComandas.REGULAR_TIEMPO2;
                }
                else
                {
                    Gradiente.Color = ColoresComandas.MAL_TIEMPO2;
                    //Gradiente.GradientStops[0].Color = ColoresComandas.MAL_TIEMPO1;
                    //Gradiente.GradientStops[1].Color = ColoresComandas.MAL_TIEMPO2;
                }
            }
        }

        public abstract void Imprimir();
    }
}
