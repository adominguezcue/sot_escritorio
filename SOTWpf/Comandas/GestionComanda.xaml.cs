﻿using SOTWpf.Comandas.Manejadores;
using SOTWpf.Pagos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Extensiones;

namespace SOTWpf.Comandas
{
    /// <summary>
    /// Lógica de interacción para GestionComanda.xaml
    /// </summary>
    public partial class GestionComanda : UserControl
    {
        ManejadorComandaBase gcmd;

        public GestionComanda()
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();
        }

        private void btnCobrar_Click(object sender, RoutedEventArgs e)
        {
            var tmp = gcmd;

            if (tmp != null)
                tmp.Aceptar();
        }

        private void btnSolicitarCambio_Click(object sender, RoutedEventArgs e)
        {
            var tmp = gcmd;

            if (tmp != null)
                tmp.SolicitarCambio();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            var tmp = gcmd;

            if (tmp != null)
                tmp.SolicitarCancelacion();
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            gcmd = e.NewValue as ManejadorComandaBase;

            RecargarVisibilidad();
        }

        private void RecargarVisibilidad()
        {
            if (gcmd == null || gcmd.EnPreparacion)
            {
                btnCancelar.Visibility = System.Windows.Visibility.Hidden;
                btnSolicitarCambio.Visibility = System.Windows.Visibility.Hidden;

                gridInformacion.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                btnCancelar.Visibility = System.Windows.Visibility.Visible;
                btnSolicitarCambio.Visibility = System.Windows.Visibility.Visible;

                gridInformacion.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void btnImprimirCuenta_Click(object sender, RoutedEventArgs e)
        {
            var tmp = gcmd;

            if (tmp != null)
                tmp.Imprimir();
        }
    }
}
