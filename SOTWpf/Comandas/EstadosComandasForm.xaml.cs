﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Extensiones;

namespace SOTWpf.Comandas
{
    /// <summary>
    /// Lógica de interacción para EstadosComandasForm.xaml
    /// </summary>
    public partial class EstadosComandasForm : Window
    {
        public EstadosComandasForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarDatosComandas();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarDatosComandas();
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var configuraciones = new ControladorConfiguracionGlobal().ObtenerConfiguracionesTurno();
                configuraciones.Insert(0, new Modelo.Entidades.ConfiguracionTurno { Nombre = "Todos" });
                cbTurno.ItemsSource = configuraciones;

                var tipos = EnumExtensions.ComoListaDto<Comanda.Tipos>();
                tipos.Insert(0, new Transversal.Dtos.DtoEnum { Nombre = "Todos" });
                cbTipos.ItemsSource = tipos;

                var estatus = EnumExtensions.ComoListaDto<Comanda.Estados>();
                estatus.Insert(0, new Transversal.Dtos.DtoEnum { Nombre = "Todos" });
                cbEstatus.ItemsSource = estatus;

                var lineas = new ControladorLineas().ObtenerLineasParaFiltro().Where(m => m.Comandable).ToList();
                lineas.Insert(0, new Modelo.Almacen.Entidades.Linea { Desc_Linea = "Todas" });
                cbLineas.ItemsSource = lineas;

                CargarDatosComandas();
            }
        }

        private void CargarDatosComandas()
        {
            if (IsLoaded) 
            {
                if (dpFechaInicial.SelectedDate.HasValue && dpFechaFinal.SelectedDate.HasValue)
                {
                    var orden = (int)cbTurno.SelectedValue;
                    var tipo = cbTipos.SelectedValue as Comanda.Tipos?;
                    var estado = cbEstatus.SelectedValue as Comanda.Estados?;
                    var linea = (int)(cbLineas.SelectedValue ?? 0);

                    dgvResumenes.ItemsSource = new ControladorRoomServices().ObtenerLogComandasPorFecha(dpFechaInicial.SelectedDate.Value, dpFechaFinal.SelectedDate.Value.AddDays(1).AddSeconds(-1),
                        orden == 0 ? null : (int?)orden, linea == 0 ? null : (int?)linea, tipo, estado);
                }
                else
                    dgvResumenes.ItemsSource = new List<Modelo.Dtos.DtoLogComanda>();
            }
        }

        private void cbTurno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarDatosComandas();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnReporte_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new SOTWpf.Reportes.Comandas.EstadoComandas();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = dpFechaInicial.SelectedDate.Value,
                FechaFin = dpFechaFinal.SelectedDate.Value,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            List<DtoFiltroReporte> filtros = new List<DtoFiltroReporte>();
            filtros.Add(new DtoFiltroReporte { Filtro = "Turno", Valor = cbTurno.Text ?? "" });
            filtros.Add(new DtoFiltroReporte { Filtro = "Tipo", Valor = cbTipos.Text ?? "" });
            //filtros.Add(new DtoFiltroReporte { Filtro = "Estatus", Valor = cbEstatus.Text ?? "" });
            filtros.Add(new DtoFiltroReporte { Filtro = "Subcategoría", Valor = cbLineas.Text ?? "" });


            var items = dgvResumenes.ItemsSource as List<Modelo.Dtos.DtoLogComanda>;

            documentoReporte.Subreports["Resumenes"].SetDataSource(items);
            documentoReporte.Subreports["FiltrosExtra"].SetDataSource(filtros);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }

        
    }

    
}
