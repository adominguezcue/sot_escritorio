﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Proveedores
{
    /// <summary>
    /// Lógica de interacción para BusquedaProveedor.xaml
    /// </summary>
    public partial class BusquedaProveedor : Window
    {

        private string textobusqueda = "";
        public Modelo.Almacen.Entidades.Proveedor ProveedorSeleccionado { get; set; }
        public BusquedaProveedor()
        {
            InitializeComponent();
            txtBusqueda.Focus();
            var proveedors = new SOTControladores.Controladores.ControladorProveedores().ObtenerProveedores();
            busquedaproveedor.ItemsSource = proveedors;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                Buscar();

        }

        private void Buscar()
        {
            var proveedors = new SOTControladores.Controladores.ControladorProveedores().ObtieneProveedoresPorFiltroNombre(textobusqueda);
            busquedaproveedor.ItemsSource = proveedors;
        }

        private void busquedaproveedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void busquedaproveedor_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ProveedorSeleccionado = busquedaproveedor.SelectedItem as Modelo.Almacen.Entidades.Proveedor;
            Close();
        }

        private void txtBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            textobusqueda = txtBusqueda.Text;
            Buscar();
        }
    }
}
