﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Comandas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.ConsumosInternos
{
    /// <summary>
    /// Lógica de interacción para EdicionConsumosInternosForm.xaml
    /// </summary>
    public partial class EdicionConsumosInternosForm : Window
    {
        private ConsumoInterno _consumoActual;


        SOTWpf.Comandas.Manejadores.ManejadorCreacionConsumosInternos ccm = new SOTWpf.Comandas.Manejadores.ManejadorCreacionConsumosInternos();

        ConsumoInterno ConsumoActual
        {
            get { return _consumoActual; }
            set 
            {
                _consumoActual = value;
                DataContext = _consumoActual;
            }
        }

        int idConsumoInterno;
        //bool gestionarInternamente;

        public EdicionConsumosInternosForm(int idConsumoInterno = 0/*, bool gestionarInternamente = false*/)
        {
            InitializeComponent();

            this.idConsumoInterno = idConsumoInterno;
            //this.gestionarInternamente = gestionarInternamente;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            ////if (gestionarInternamente)
            ////{
            //if (idConsumoInterno != 0)
            //{
            //    var controladorConsumosInternos = new ControladorConsumosInternos();
            //    var clon = ConsumoActual.ObtenerCopiaDePrimitivas();
            //    clon.Id = ConsumoActual.Id;

            //    controladorConsumosInternos.ModificarConsumoInterno(clon);

            //    MessageBox.Show(Textos.Mensajes.modificacion_consumo_interno_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            //}
            //else
            //{
            //    var controladorConsumosInternos = new ControladorConsumosInternos();
            //    controladorConsumosInternos.CrearConsumoInterno(ConsumoActual.ObtenerCopiaDePrimitivas());

            //    MessageBox.Show(Textos.Mensajes.creacion_consumo_interno_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            //}
            ////}
            //Close();


            ccm.RecalcularTotales();

            //decimal subtotal = default(decimal);
            //decimal cortesia = 0;
            //decimal descuento = 0;
            //if (ccm.Resumenes.Count > 0)
            //{
            //    subtotal = ccm.Resumenes.Sum(m => m.TotalSinIVA);

            //}
            //else
            //{
            //    subtotal = 0;
            //}

            if (ConsumoActual.Id == 0)
            {
                try
                {
                    var config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

                    //consumoActual = new Modelo.Entidades.ConsumoInterno();

                    var clon = ConsumoActual.ObtenerCopiaDePrimitivas();

                    clon.Activo = true;

                    clon.ValorConIVA = ccm.SubtotalConIVA;//clon.ValorSinIVA + clon.ValorIVA;
                    clon.ValorSinIVA = (ccm.SubtotalConIVA / (1 + config.Iva_CatPar));
                    clon.ValorIVA = clon.ValorConIVA - clon.ValorSinIVA;//clon.ValorSinIVA * config.Iva_CatPar;
                    //consumoActual.IdRenta = idRentaActual;

                    //if (cupon != null)
                    //    consumoActual.Cupon = cupon;

                    foreach (ResumenArticulo item in ccm.Resumenes)
                    {
                        var nuevoArticuloComanda = new Modelo.Entidades.ArticuloConsumoInterno();
                        nuevoArticuloComanda.Activo = true;
                        nuevoArticuloComanda.IdArticulo = item.IdArticulo;
                        nuevoArticuloComanda.Cantidad = item.Cantidad;
                        nuevoArticuloComanda.PrecioUnidad = item.Precio;
                        nuevoArticuloComanda.Observaciones = item.Observaciones;

                        clon.ArticulosConsumoInterno.Add(nuevoArticuloComanda);
                    }

                    var controladorConsumosInternos = new ControladorConsumosInternos();
                    controladorConsumosInternos.CrearConsumoInterno(clon);

                    MessageBox.Show(Textos.Mensajes.creacion_consumo_interno_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch
                {
                    //consumoActual = null;
                    throw;
                }

            }
            else
            {
                var articulos = new List<Modelo.Dtos.DtoArticuloPrepararGeneracion>();

                foreach (ResumenArticulo item in ccm.Resumenes)
                {
                    Modelo.Dtos.DtoArticuloPrepararGeneracion nuevoArticuloComanda = new Modelo.Dtos.DtoArticuloPrepararGeneracion
                    {
                        Activo = true,
                        IdArticulo = item.IdArticulo,
                        Cantidad = item.Cantidad,
                        PrecioConIVA = item.Precio,
                        Id = item.IdRelacional,
                        Observaciones = item.Observaciones
                    };

                    articulos.Add(nuevoArticuloComanda);
                }

                foreach (var articuloComanda in ConsumoActual.ArticulosConsumoInterno.Where(m => m.Activo && !ccm.Resumenes.Any(r => r.IdRelacional == m.Id)))
                {
                    Modelo.Dtos.DtoArticuloPrepararGeneracion nuevoArticuloComanda = new Modelo.Dtos.DtoArticuloPrepararGeneracion
                    {
                        Activo = false,
                        IdArticulo = articuloComanda.IdArticulo,
                        Cantidad = articuloComanda.Cantidad,
                        PrecioConIVA = articuloComanda.PrecioUnidad,
                        Id = articuloComanda.Id
                    };

                    articulos.Add(nuevoArticuloComanda);
                }

                var controladorConsumosInternos = new ControladorConsumosInternos();
                var clon = ConsumoActual.ObtenerCopiaDePrimitivas();
                clon.Id = ConsumoActual.Id;

                controladorConsumosInternos.ModificarConsumoInterno(clon, articulos);

                MessageBox.Show(Textos.Mensajes.modificacion_consumo_interno_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            //if (!gestionarInternamente)
                ConsumoActual = null;

            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    cbEmpleados.ItemsSource = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreasFILTRO();

                    if (idConsumoInterno != 0)
                    {
                        var controladorConsumosInternos = new ControladorConsumosInternos();
                        var consumo = controladorConsumosInternos.ObtenerDetallesConsumoInternoPendiente(idConsumoInterno);

                        if (consumo == null)
                            throw new SOTException(Textos.Errores.consumo_interno_nulo_eliminado_exception);


                        ConsumoActual = consumo;

                        //cbEmpleados.IsEnabled = false;
                        var config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();


                        foreach (var articuloComanda in ConsumoActual.ArticulosConsumoInterno)
                        {
                            if (!articuloComanda.Activo)
                                continue;

                            ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                            nuevoResumen.IdRelacional = articuloComanda.Id;
                            nuevoResumen.IdArticulo = articuloComanda.IdArticulo;
                            nuevoResumen.Nombre = articuloComanda.ArticuloTmp.Desc_Art;
                            nuevoResumen.Tipo = articuloComanda.ArticuloTmp.NombreLineaTmp;
                            nuevoResumen.Cantidad = articuloComanda.Cantidad;
                            nuevoResumen.Precio = articuloComanda.PrecioUnidad;
                            nuevoResumen.Observaciones = articuloComanda.Observaciones;

                            ccm.Resumenes.Add(nuevoResumen);
                        }

                    }
                    else
                    {
                        ConsumoActual = new Modelo.Entidades.ConsumoInterno
                        {
                            Activo = true,
                            FechaCreacion = DateTime.Now
                        };
                    }
                }
                catch
                {
                    Close();
                    throw;
                }
            }
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _consumoActual = e.NewValue as ConsumoInterno;
        }

        private void btnEditarDetalles_Click(object sender, RoutedEventArgs e)
        {

            Utilidades.Dialogos.MostrarDialogos(this, new Comandas.CreacionComandasForm(ccm));
        }
    }
}
