﻿using Modelo.Dtos;
using SOTControladores.Controladores;
using SOTWpf.Comandas;
using SOTWpf.Comandas.Manejadores;
using SOTWpf.Empleados;
using SOTWpf.Pagos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.ConsumosInternos.Manejadores
{
    public class ManejadorConsumoInternoCocina: ManejadorComandaBase
    {
        int _numeroOrden;
        ObservableCollection<ResumenArticulo> _resumenes;

        public override ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            protected set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public override int NumeroOrden
        {
            get { return _numeroOrden; }
            protected set
            {
                _numeroOrden = value;
                NotifyPropertyChanged();
            }
        }

        string _estadoComanda, _tiposComanda, _informacionPertenencia;
        bool _enPreparacion;

        public override string EstadoComanda
        {
            get { return _estadoComanda; }
            protected set
            {
                _estadoComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string TiposComanda
        {
            get { return _tiposComanda; }
            protected set
            {
                _tiposComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string InformacionPertenencia
        {
            get { return _informacionPertenencia; }
            protected set
            {
                _informacionPertenencia = value;
                NotifyPropertyChanged();
            }
        }



        decimal _cortesia, _cortesiaConIVA, _descuento, _descuentoConIVA, _subtotal, _subtotalConIVA, _totalSinIVA, _iva, _totalConIVA;

        //public override decimal Cortesia
        //{
        //    get { return _cortesia; }
        //    protected set
        //    {
        //        _cortesia = value;

        //        NotifyPropertyChanged();

        //        CortesiaConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}
        //public override decimal Descuento
        //{
        //    get { return _descuento; }
        //    protected set
        //    {
        //        _descuento = value;
        //        NotifyPropertyChanged();

        //        DescuentoConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            protected set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public override decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            protected set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal Subtotal
        //{
        //    get { return _subtotal; }
        //    protected set
        //    {
        //        _subtotal = value;
        //        NotifyPropertyChanged();

        //        SubtotalConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            protected set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal TotalSinIVA
        //{
        //    get { return _totalSinIVA; }
        //    protected set
        //    {
        //        _totalSinIVA = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        //public override decimal IVA
        //{
        //    get { return _iva; }
        //    protected set
        //    {
        //        _iva = value;
        //        NotifyPropertyChanged();
        //    }
        //}
        public override decimal TotalConIVA
        {
            get { return _totalConIVA; }
            protected set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }

        public override bool EnPreparacion
        {
            get { return _enPreparacion; }
        }

        private DtoResumenComandaExtendido consumoActual;

        //public ManejadorComandaCocina()
        //{
        //    comandaActual = null;
        //    NumeroOrden = 1;
        //    EstadoComanda = "Estado";
        //    TiposComanda = "ABS";
        //    InformacionPertenencia = "Habitación 000";

        //    base.Initialize();
        //}

        private TimeSpan _tiempo;

        public override TimeSpan Tiempo
        {
            get { return _tiempo; }
            protected set
            {
                _tiempo = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorConsumoInternoCocina(DtoResumenComandaExtendido detalles, bool enPreparacion)
        {
            consumoActual = detalles;
            _enPreparacion = enPreparacion;
            base.Initialize();
        }

        protected override void Inicializar()
        {
            base.Inicializar();

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarDetallesComanda();
        }

        private void CargarDetallesComanda()
        {
            //comandaActual = detalles;

            if (consumoActual != null)
            {

                Tiempo = DateTime.Now - consumoActual.FechaInicio;
                string cadenaTipos = string.Empty;

                if (consumoActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Equals("ALIMENTOS")))
                    cadenaTipos += "A";

                if (consumoActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Equals("BEBIDAS")))
                    cadenaTipos += "B";

                if (consumoActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Contains("SEX")))
                    cadenaTipos += "S";

                foreach (var articuloComanda in consumoActual.ArticulosComanda)
                {
                    if (!articuloComanda.Activo)
                        continue;


                    if (articuloComanda.EsOtroDepto == true)
                        cadenaTipos = articuloComanda.OtosDeptos;

                    ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                    nuevoResumen.IdArticulo = articuloComanda.IdArticulo;
                    nuevoResumen.Nombre = articuloComanda.Nombre;
                    nuevoResumen.Tipo = articuloComanda.Tipo;
                    nuevoResumen.Cantidad = articuloComanda.Cantidad;
#warning revisar esto, tenía el precio final
                    nuevoResumen.Precio = articuloComanda.PrecioUnidad;//.PrecioUnidadFinal;
                    nuevoResumen.Observaciones = articuloComanda.Observaciones;

                    Resumenes.Add(nuevoResumen);
                }


                //if (consumoActual.Estado != Modelo.Entidades.Comanda.Estados.Cobrada && consumoActual.Estado != Modelo.Entidades.Comanda.Estados.Cancelada)
                ////(comandaActual.Estado == Modelo.Entidades.Comanda.Estados.Preparacion || comandaActual.Estado == Modelo.Entidades.Comanda.Estados.PorEntregar)
                //{
                //    var tiempo = System.DateTime.Now - consumoActual.FechaInicio;

                    ColorearBrush(consumoActual.FechaInicio, consumoActual.Estado);
                //}

                EstadoComanda = consumoActual.Estado.Descripcion();
                TiposComanda = cadenaTipos;
                InformacionPertenencia = consumoActual.Destino;
                NumeroOrden = consumoActual.Orden;
            }
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Resumenes.Count == 0)
            {
                //Cortesia = 0;
                //Descuento = 0;
                //Subtotal = 0;
                //TotalSinIVA = 0;
                //IVA = 0;
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {

                //Cortesia = 0;

                //Subtotal = Resumenes.Sum(m => m.TotalSinIVA);

                //Descuento = Subtotal;//premio != null ? Subtotal : 0;

                //TotalSinIVA = Subtotal - Cortesia - Descuento;
                //IVA = TotalSinIVA * config.Iva_CatPar;
                CortesiaConIVA = 0;
                SubtotalConIVA = Resumenes.Sum(m => m.Total);
                DescuentoConIVA = SubtotalConIVA;

                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public override void Aceptar()
        {
            if (consumoActual != null)
            {
                var idsArticulosComandas = consumoActual.ArticulosComanda.Where(m => m.Activo && m.Estado == Modelo.Entidades.ArticuloComanda.Estados.EnProceso).Select(m => m.Id).ToList();

                if (idsArticulosComandas.Count > 0)
                {
                    var controladorRent = new ControladorConsumosInternos();
                    controladorRent.FinalizarElaboracionProductos(consumoActual.Id, idsArticulosComandas);
                }
                else
                {
                    idsArticulosComandas = consumoActual.ArticulosComanda.Where(m => m.Activo && m.Estado == Modelo.Entidades.ArticuloComanda.Estados.PorEntregar).Select(m => m.Id).ToList();

                    if (idsArticulosComandas.Count > 0)
                    {
                        var controladorRent = new ControladorConsumosInternos();

                        controladorRent.EntregarProductos(consumoActual.Id, idsArticulosComandas/*, seleccionadorMeseroF.MeseroSeleccionado.Id*/);
                    }
                    else
                        return;
                }

                OnPostCobro(EventArgs.Empty);
            }
        }

        

        public override void SolicitarCambio()
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void SolicitarCancelacion()
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void Sincronizar(object comandaUOrden)
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void Imprimir()
        {
            throw new NotSupportedException("Operación no soportada");
        }
    }
}
