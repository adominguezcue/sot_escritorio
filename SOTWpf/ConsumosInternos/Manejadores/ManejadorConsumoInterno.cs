﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Comandas;
using SOTWpf.Comandas.Manejadores;
using SOTWpf.ConceptosSistema;
using SOTWpf.Empleados;
using SOTWpf.Pagos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.ConsumosInternos.Manejadores
{
    public class ManejadorConsumoInterno : ManejadorComandaBase
    {
        int _numeroOrden;
        ObservableCollection<ResumenArticulo> _resumenes;

        public override string InformacionPertenencia { get; protected set; }

        public override ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            protected set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public override int NumeroOrden
        {
            get { return _numeroOrden; }
            protected set
            {
                _numeroOrden = value;
                NotifyPropertyChanged();
            }
        }

        string _estadoComanda, _tiposComanda;

        public override string EstadoComanda
        {
            get { return _estadoComanda; }
            protected set
            {
                _estadoComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string TiposComanda
        {
            get { return _tiposComanda; }
            protected set
            {
                _tiposComanda = value;
                NotifyPropertyChanged();
            }
        }

        decimal _cortesiaConIVA, _descuentoConIVA, _subtotalConIVA, _totalConIVA;



        public override decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            protected set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public override decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            protected set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }

        public override decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            protected set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }

        public override decimal TotalConIVA
        {
            get { return _totalConIVA; }
            protected set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }


        public override bool EnPreparacion
        {
            get { return false; }
        }

        private Modelo.Entidades.ConsumoInterno consumoActual;
        string destino;

        private TimeSpan _tiempo;

        public override TimeSpan Tiempo
        {
            get { return _tiempo; }
            protected set
            {
                _tiempo = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorConsumoInterno(Modelo.Entidades.ConsumoInterno detalles, string destino)
        {
            consumoActual = detalles;
            this.destino = destino;
            base.Initialize();
        }

        protected override void Inicializar()
        {
            base.Inicializar();

            Gestionable = false;

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarDetallesComanda();
        }

        private void CargarDetallesComanda()
        {
            //comandaActual = detalles;

            if (consumoActual != null)
            {
                Expression<Func<ArticuloConsumoInterno, bool>> filtro = m => m.Activo;

                if (consumoActual.Estado == Comanda.Estados.Cobrada)
                    filtro = m => m.Estado == ArticuloComanda.Estados.Entregado;
                else if (consumoActual.Estado == Comanda.Estados.Cancelada)
                    filtro = m => m.FechaEliminacion == consumoActual.FechaEliminacion;
                else
                    Tiempo = DateTime.Now - consumoActual.FechaCreacion;

                foreach (var articuloComanda in consumoActual.ArticulosConsumoInterno.Where(filtro.Compile()))
                {
                    Resumenes.Add(new ResumenArticulo(config.Iva_CatPar)
                    {
                        IdArticulo = articuloComanda.IdArticulo,
                        Nombre = articuloComanda.ArticuloTmp.Desc_Art,
                        Tipo = articuloComanda.ArticuloTmp.NombreLineaTmp,
                        Cantidad = articuloComanda.Cantidad,
                        Precio = articuloComanda.PrecioUnidad,
                        Observaciones = articuloComanda.Observaciones,
                        //EsCortesia = articuloComanda.EsCortesia
                    });
                }

                string cadenaTipos = string.Empty;

                if (consumoActual.ArticulosConsumoInterno.Where(filtro.Compile()).Any(m => m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Equals("ALIMENTOS")))
                    cadenaTipos += "A";

                if (consumoActual.ArticulosConsumoInterno.Where(filtro.Compile()).Any(m => m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Equals("BEBIDAS")))
                    cadenaTipos += "B";

                if (consumoActual.ArticulosConsumoInterno.Where(filtro.Compile()).Any(m => m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Contains("SEX")))
                    cadenaTipos += "S";

                


                //if (comandaActual.Estado != Modelo.Entidades.Comanda.Estados.PorPagar && comandaActual.Estado != Modelo.Entidades.Comanda.Estados.Cobrada && comandaActual.Estado != Modelo.Entidades.Comanda.Estados.Cancelada)
                //    //(comandaActual.Estado == Modelo.Entidades.Comanda.Estados.Preparacion || comandaActual.Estado == Modelo.Entidades.Comanda.Estados.PorEntregar)
                //{
                //    var tiempo = System.DateTime.Now - comandaActual.FechaInicio;

                    ColorearBrush(consumoActual.FechaInicio, consumoActual.Estado);
                //}

                EstadoComanda = consumoActual.Estado.Descripcion();
                TiposComanda = cadenaTipos;
                NumeroOrden = consumoActual.Orden;
            }
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Resumenes.Count == 0)
            {
                //Cortesia = 0;
                //Descuento = 0;
                //Subtotal = 0;
                //TotalSinIVA = 0;
                //IVA = 0;
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {

                //Cortesia = Resumenes.Where(m => m.EsCortesia).Sum(m => m.TotalSinIVA);

                //Subtotal = Resumenes.Sum(m => m.TotalSinIVA);

                //Descuento = premio != null ? Subtotal : 0;

                //TotalSinIVA = Subtotal - Cortesia - Descuento;
                //IVA = TotalSinIVA * config.Iva_CatPar;

                SubtotalConIVA = Resumenes.Sum(m => m.Total);
                CortesiaConIVA = Resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                DescuentoConIVA = 0;
                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public override void Aceptar()
        {
            //if (consumoActual != null)
            //{
            //    var controladorRS = new ControladorConsumosInternos();

            //    if (consumoActual.Estado == Modelo.Entidades.Comanda.Estados.PorCobrar)
            //    {
            //        controladorRS.CobrarComanda(consumoActual.Id);
            //    }
            //    else if (consumoActual.Estado == Modelo.Entidades.Comanda.Estados.PorPagar)
            //    {

            //        SelectorFormaPagoForm cobroF = new SelectorFormaPagoForm(consumoActual.ValorConIVA, CortesiaConIVA, false, Resumenes.Any(m => m.EsCortesia) ?
            //                SelectorFormaPagoForm.Justificaciones.Cortesia :
            //                SelectorFormaPagoForm.Justificaciones.Ninguna);//, false, false);

            //        Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, cobroF);

            //        if (cobroF.ProcesoExitoso)
            //            controladorRS.PagarComanda(consumoActual.Id, cobroF.FormasPagoSeleccionadas, cobroF.PropinaGenerada);

            //    }
            //    else
            //        return;

            //    OnPostCobro(EventArgs.Empty);
            //}
        }

        

        public override void SolicitarCambio()
        {
            //if (/*comanda.Estado != Comanda.Estados.PorCobrar &&*/
            //    consumoActual.Estado != Comanda.Estados.PorEntregar &&
            //    consumoActual.Estado != Comanda.Estados.Preparacion)
            //    throw new SOTException(Textos.Errores.comanda_no_modificable_exception);

            //var ccm = new ManejadorCreacionComandas(consumoActual, destino);

            //var creacionComandaF = new CreacionComandasForm(ccm);
            //Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, creacionComandaF);

            //OnPostCobro(EventArgs.Empty);
        }

        public override void SolicitarCancelacion()
        {
            //var motivoF = new CapturaMotivo();
            //motivoF.Motivo = "cancelación";
            //motivoF.TextoAuxiliar = "Detalles";

            //var aeh = new CapturaMotivo.AceptarEventHandler((s, ea) =>
            //{
            //    if (consumoActual != null)
            //    {
            //        var controlador = new ControladorRoomServices();
            //        controlador.CancelarComanda(consumoActual.Id, motivoF.DescripcionMotivo);
            //    }

            //    OnPostCobro(EventArgs.Empty);
            //});

            //try
            //{
            //    motivoF.Aceptar += aeh;

            //    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, motivoF);
            //}
            //finally
            //{
            //    motivoF.Aceptar -= aeh;
            //}
        }

        public override void Sincronizar(object comandaUOrden)
        {
            //var comanda = comandaUOrden as Modelo.Entidades.Comanda;

            //if (comanda == null)
            //    throw new SOTException("El objeto a sincronizar es null o no es una comanda");

            //consumoActual = comanda;
            //base.Initialize();
        }

        public override void Imprimir()
        {
            //if (consumoActual != null)
            //{
            //    var controlador = new ControladorRoomServices();
            //    controlador.Imprimir(consumoActual.Id);

            //    MessageBox.Show("Impresión exitosa", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            //}
        }
    }
}
