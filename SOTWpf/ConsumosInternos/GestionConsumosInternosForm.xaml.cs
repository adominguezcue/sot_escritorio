﻿using MaterialDesignThemes.Wpf;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.ConceptosSistema;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.ConsumosInternos
{
    /// <summary>
    /// Lógica de interacción para GestionConsumosInternosForm.xaml
    /// </summary>
    public partial class GestionConsumosInternosForm : Window
    {
        private DispatcherTimer timerCargarConsumosInternos;
        object bloqueador = new object();

        DataGridColumn sortedColumn = null;

        public GestionConsumosInternosForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarConsumosInternos();

                timerCargarConsumosInternos = new DispatcherTimer();
                timerCargarConsumosInternos.Interval = TimeSpan.FromSeconds(15);
                timerCargarConsumosInternos.Tick += timerCargaConsumosInternos_Tick;

                timerCargarConsumosInternos.Start();
            }
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    lock (bloqueador)
                        CargarConsumosInternos();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    lock (bloqueador)
                        CargarConsumosInternos();
                }
            }
        }

        private void CargarConsumosInternos()
        {
            if (this.IsLoaded)
            {
                var fechaI = !chbSoloEnCurso.IsChecked.Value && dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : default(DateTime?);
                var fechaF = !chbSoloEnCurso.IsChecked.Value && dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1) : default(DateTime?);

                var controladorConsumosInternos = new SOTControladores.Controladores.ControladorConsumosInternos();
                var consumosInternos = controladorConsumosInternos.ObtenerConsumosInternosFiltrados(txtNombre.Text, txtApellidoP.Text, txtApellidoM.Text,
                                                                                                    chbSoloEnCurso.IsChecked.Value, fechaI, fechaF);

                SortDescription sortD = new SortDescription();

                if (sortedColumn != null)
                {
                    sortD.Direction = sortedColumn.SortDirection ?? ListSortDirection.Ascending;
                    sortD.PropertyName = sortedColumn.SortMemberPath;
                }

                dgvConsumosInternos.ItemsSource = consumosInternos;

                txtEnCurso.Text = "Cantidad de consumos internos en curso : " + (chbSoloEnCurso.IsChecked.Value ? consumosInternos.Count.ToString() : controladorConsumosInternos.ObtenerCantidadEnCurso(txtNombre.Text, txtApellidoP.Text, txtApellidoM.Text).ToString());

                if (sortedColumn != null)
                    dgvConsumosInternos.Items.SortDescriptions.Add(sortD);

                var column = dgvConsumosInternos.Columns.FirstOrDefault(m => m.SortMemberPath == sortD.PropertyName);

                if (column != null)
                {
                    foreach (var col in dgvConsumosInternos.Columns)
                    {
                        col.SortDirection = null;
                    }

                    column.SortDirection = sortD.Direction;

                    dgvConsumosInternos.Items.Refresh();
                }
            }
        }

        private void txtNombre_LostFocus(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
                CargarConsumosInternos();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEliminarConsumoInterno_Click(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
            {
                var consumo = dgvConsumosInternos.SelectedItem as Modelo.Entidades.ConsumoInterno;

                if (consumo == null)
                    throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

                var motivoF = new CapturaMotivo();
                motivoF.Motivo = "cancelación";
                motivoF.TextoAuxiliar = "Detalles";

                var aeh = new CapturaMotivo.AceptarEventHandler((s, ea) =>
                {
                    var controladorConsumoInterno = new SOTControladores.Controladores.ControladorConsumosInternos();
                    controladorConsumoInterno.CancelarConsumoInterno(consumo.Id, motivoF.DescripcionMotivo);

                    CargarConsumosInternos();
                });

                try
                {
                    lock (bloqueador)
                    {
                        motivoF.Aceptar += aeh;
                        Utilidades.Dialogos.MostrarDialogos(this, motivoF);
                    }
                }
                finally
                {
                    motivoF.Aceptar -= aeh;
                }
            }
        }

        private void btnModificarConsumoInterno_Click(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
            {
                var consumo = dgvConsumosInternos.SelectedItem as Modelo.Entidades.ConsumoInterno;

                if (consumo == null)
                    throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

                //Antonio de jesus Domingues Cuevas 11/11/2019
                // if (/*consumoInterno.Estado != Comanda.Estados.PorEntregarCliente &&*/
                /* consumo.Estado != Comanda.Estados.PorEntregar &&
                 consumo.Estado != Comanda.Estados.Preparacion)
                     throw new SOTException(Textos.Errores.consumo_interno_no_modificable_exception);*/
                if (consumo.Estado == Comanda.Estados.PorPagar || consumo.Estado == Comanda.Estados.PorCobrar)
                {
                    var controlador = new ControladorRoomServices();
                    controlador.SolicitaCambioConsumoInterno();
                }
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionConsumosInternosForm(consumo.Id));

                CargarConsumosInternos();
            }
        }

        private void btnAltaConsumoInterno_Click(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
            {
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionConsumosInternosForm());

                CargarConsumosInternos();
            }
        }

        private void btnEntregarConsumoInterno_Click(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
            {
                var consumo = dgvConsumosInternos.SelectedItem as Modelo.Entidades.ConsumoInterno;

                if (consumo == null)
                    throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

                var controladorConsumos = new SOTControladores.Controladores.ControladorConsumosInternos();

                if (consumo.Estado == Modelo.Entidades.Comanda.Estados.PorCobrar)
                {
                    controladorConsumos.EntregarConsumoInterno(consumo.Id);
                    CargarConsumosInternos();
                }
                else
                    return;

            }
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            if (timerCargarConsumosInternos != null)
            {
                timerCargarConsumosInternos.Stop();
                timerCargarConsumosInternos.Tick -= timerCargaConsumosInternos_Tick;
            }
        }

        private void timerCargaConsumosInternos_Tick(object sender, EventArgs e)
        {
            lock (bloqueador)
                CargarConsumosInternos();
        }

        private void chbSoloEnCurso_CheckedChanged(object sender, RoutedEventArgs e)
        {
            if (chbSoloEnCurso.IsChecked == true)
            {
                dpFechaInicial.Visibility = System.Windows.Visibility.Collapsed;
                dpFechaFinal.Visibility = System.Windows.Visibility.Collapsed;
            }
            else 
            {
                dpFechaInicial.Visibility = System.Windows.Visibility.Visible;
                dpFechaFinal.Visibility = System.Windows.Visibility.Visible;
            }

            lock (bloqueador)
                CargarConsumosInternos();
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
            {
                var consumo = dgvConsumosInternos.SelectedItem as Modelo.Entidades.ConsumoInterno;

                if (consumo == null)
                    throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

                var controladorConsumos = new SOTControladores.Controladores.ControladorConsumosInternos();

                controladorConsumos.Imprimir(consumo.Id);

                MessageBox.Show("Impresión exitosa", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            }
        }

        private void btnDetalles_Click(object sender, RoutedEventArgs e)
        {

            MostrarDetalles();

        }

        private async void MostrarDetalles()
        {
            Comandas.GestionComanda editorForm;

            lock (bloqueador)
            {
                var consumo = dgvConsumosInternos.SelectedItem as Modelo.Entidades.ConsumoInterno;

                if (consumo == null)
                    throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

                var controladorConsumosInternos = new ControladorConsumosInternos();

                //var estado = consumo.Estado;
                
                //switch (estado)
                //{ 
                //    case Comanda.Estados.Preparacion:
                //    case Comanda.Estados.PorPagar:
                //    case Comanda.Estados.PorEntregar:
                //    case Comanda.Estados.PorCobrar:
                //        consumo = controladorConsumosInternos.ObtenerDetallesConsumoInternoPendiente(consumo.Id);
                //        break;
                //    case Comanda.Estados.Cobrada:
                //        consumo = controladorConsumosInternos.ObtenerDetallesConsumoInternoFinalizado(consumo.Id);
                //        break;
                //    case Comanda.Estados.Cancelada:
                //        consumo = controladorConsumosInternos.ObtenerDetallesConsumoInternoCancelado(consumo.Id);
                //        break;
                //}

                consumo = controladorConsumosInternos.ObtenerDetallesConsumoInternoIgnorandoEstado(consumo.Id);

                if (consumo == null)
                    throw new SOTException("No se encontraron detalles del consumo interno, posiblemente su estatus ha cambiado. Intente de nuevo en unos instantes.");

                editorForm = new Comandas.GestionComanda { Width = 350, DataContext = new Manejadores.ManejadorConsumoInterno(consumo, consumo.NombreConsumidorTmp) };
                //editorForm.EdicionRealizada += BotoneraBase_AceptarClick;
                //editorForm.EdicionCancelada += BotoneraBase_CancelarClick;

            }

            await DialogHost.Show(editorForm, "dialogHostPrincipal");

        }

        private void dgvConsumosInternos_Sorting(object sender, DataGridSortingEventArgs e)
        {
            lock (bloqueador)
            {
                sortedColumn = e.Column;

                ////var index = dgvConsumosInternos.Columns.IndexOf(e.Column);
                //sortD.Direction = e.Column.SortDirection ?? ListSortDirection.Ascending;
                //sortD.PropertyName = e.Column.SortMemberPath;
            }
        }
    }
}
