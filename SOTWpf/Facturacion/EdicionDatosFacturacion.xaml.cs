﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Facturacion
{
    /// <summary>
    /// Lógica de interacción para EdicionDatosFacturacion.xaml
    /// </summary>
    public partial class EdicionDatosFacturacion : UserControl
    {
        string rfc;
        public EdicionDatosFacturacion()
        {
            InitializeComponent();
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!txtRfc.Text.Trim().ToUpper().Equals(rfc))
            { 
                rfc = txtRfc.Text.Trim().ToUpper();

                DataContext = new ControladorFacturacion().ObtenerDatosPorRfc(rfc) ??
                              new Modelo.Entidades.DatosFiscales
                              {
                                  Activo = true,
                                  RFC = rfc
                              };
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var datos = e.NewValue as Modelo.Entidades.DatosFiscales;

            rfc = datos != null ? datos.RFC : "";
        }

        private void LetrasONumeros_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Transversal.Utilidades.UtilidadesRegex.LetrasONumeros(e.Text))
                e.Handled = true;
        }
        private void LetrasONumeros_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(string)))
            {
                var text = e.DataObject.GetData(typeof(string)) as string;
                if (!Transversal.Utilidades.UtilidadesRegex.LetrasONumeros(text))
                {
                    //replace and recopy
                    //var trimmed = Regex.Replace(text, "[^0-9a-zA-Z]+", string.Empty);
                    e.CancelCommand();
                    //Clipboard.SetData(DataFormats.Text, trimmed);
                    //ApplicationCommands.Paste.Execute(trimmed, e.Source as FrameworkElement);
                }
            }
        }

        private void Numeros_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Transversal.Utilidades.UtilidadesRegex.SoloDigitos(e.Text))
                e.Handled = true;
        }
        private void Numeros_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(string)))
            {
                var text = e.DataObject.GetData(typeof(string)) as string;
                if (!Transversal.Utilidades.UtilidadesRegex.SoloDigitos(text))
                {
                    //replace and recopy
                    //var trimmed = Regex.Replace(text, "[^0-9a-zA-Z]+", string.Empty);
                    e.CancelCommand();
                    //Clipboard.SetData(DataFormats.Text, trimmed);
                    //ApplicationCommands.Paste.Execute(trimmed, e.Source as FrameworkElement);
                }
            }
        }

        private void Letras_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Transversal.Utilidades.UtilidadesRegex.SoloLetras(e.Text))
                e.Handled = true;
        }
        private void Letras_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(string)))
            {
                var text = e.DataObject.GetData(typeof(string)) as string;
                if (!Transversal.Utilidades.UtilidadesRegex.SoloLetras(text))
                {
                    //replace and recopy
                    //var trimmed = Regex.Replace(text, "[^0-9a-zA-Z]+", string.Empty);
                    e.CancelCommand();
                    //Clipboard.SetData(DataFormats.Text, trimmed);
                    //ApplicationCommands.Paste.Execute(trimmed, e.Source as FrameworkElement);
                }
            }
        }
    }
}
