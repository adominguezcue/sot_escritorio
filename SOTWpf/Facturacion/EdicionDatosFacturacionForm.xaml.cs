﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Facturacion
{
    /// <summary>
    /// Lógica de interacción para EdicionDatosFacturacionForm.xaml
    /// </summary>
    public partial class EdicionDatosFacturacionForm : Window
    {
        bool esValida;

        public bool ProcesoTerminado 
        {
            get { return esValida; }
        }

        private Modelo.Entidades.DatosFiscales datosF = null;
        public Modelo.Entidades.DatosFiscales DatosFiscalesActuales 
        {
            get { return datosF; }
        }

        public EdicionDatosFacturacionForm(Modelo.Entidades.DatosFiscales datos = null)
        {
            InitializeComponent();

            datosF = datos;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            esValida = false;

            var controladorFact = new ControladorFacturacion();

            datosF = edf.DataContext as Modelo.Entidades.DatosFiscales;

            if (datosF != null)
            {
                if (datosF.Id == 0)
                    controladorFact.AgregarDatosFiscales(datosF);
                else
                    controladorFact.ActualizarDatosFiscales(datosF);

                esValida = true;
            }
            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            esValida = false;
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                edf.DataContext = datosF;
            }
        }

        private void btnDesvincular_Click(object sender, RoutedEventArgs e)
        {
            datosF = null;
            esValida = true;
            Close();
        }
    }
}
