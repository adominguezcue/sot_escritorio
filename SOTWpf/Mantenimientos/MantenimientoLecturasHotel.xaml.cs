﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Mantenimientos.EdicionCatalogos;

namespace SOTWpf.Mantenimientos
{
    /// <summary>
    /// Lógica de interacción para MantenimientoLecturasHotel.xaml
    /// </summary>
    public partial class MantenimientoLecturasHotel : Window
    {
        int idconfiguracionturno = 13;
        int idConfiguracionPeriocidad = 2;
        public MantenimientoLecturasHotel()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                //Carga las Configuraciones
                CargaDatosPresentacion();
                CargarPerioricidad();
                CargaPanelControl();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CargaPanelControl()
        {
            var controladorgloabl = new ControladorConfiguracionGlobal();
            var configuracionesturno = controladorgloabl.ObtenerConfiguracionesTurno(true);
            turnoconfiguracion.ItemsSource = configuracionesturno;
            var ControladorPerioricidad1 = new ControladorCatPeriodoDatos();
            perioricidadconfiguracion.ItemsSource= ControladorPerioricidad1.ObtieneDatosPeriodos();
        }

        private void CargarPerioricidad()
        {
            var ControladorPerioricidad = new ControladorCatPeriodoDatos();
            Perioricidad.ItemsSource = ControladorPerioricidad.ObtieneDatosPeriodos();
        }

        private void CargaDatosPresentacion()
        {
            var ControladorCatPresentacion = new ControladorLecturaHMantenimiento();
            DatosPresentacion.ItemsSource = ControladorCatPresentacion.ObtienePressentacion();
        }

        private void Agregarpresentacion_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionMedicionLecturas(false, null));
            CargaDatosPresentacion();

        }

        private void ModificaPresentacion_Click(object sender, RoutedEventArgs e)
        {
            if (DatosPresentacion.SelectedValue != null)
            {
                var Ctpresentacion = DatosPresentacion.SelectedValue as CatPresentacionMantenimiento;
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionMedicionLecturas(true, Ctpresentacion));
                CargaDatosPresentacion();
            }
        }

        private void EliminaPresentacion_Click(object sender, RoutedEventArgs e)
        {
            if (DatosPresentacion.SelectedValue != null)
            {
                var Ctpresentacion = DatosPresentacion.SelectedValue as CatPresentacionMantenimiento;
                var ControladorCatPresentacion = new ControladorLecturaHMantenimiento();
                ControladorCatPresentacion.EliminaPresentacion(Ctpresentacion);
                CargaDatosPresentacion();
            }
        }

        private void Agregaperiodo_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionPerioricidad(false, null));
            CargarPerioricidad();
        }

        private void ModificaPerioricidad_Click(object sender, RoutedEventArgs e)
        {
            if (Perioricidad.SelectedValue != null)
            {
                var periricidad = Perioricidad.SelectedValue as CatPeriodoDatos;
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionPerioricidad(true, periricidad));
                CargarPerioricidad();
            }

        }

        private void EliminaPerioricidad_Click(object sender, RoutedEventArgs e)
        {
            if (Perioricidad.SelectedValue != null)
            {
                var periricidad = Perioricidad.SelectedValue as CatPeriodoDatos;
                var ControladorPerioricidad = new ControladorCatPeriodoDatos();
                ControladorPerioricidad.EliminaPeriricidad(periricidad);
                CargarPerioricidad();
            }
        }

        private void turnoconfiguracion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ConfiguracionTurno)turnoconfiguracion.SelectedValue).Id != null)
                idconfiguracionturno = ((ConfiguracionTurno)turnoconfiguracion.SelectedValue).Id;
        }

        private void perioricidadconfiguracion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((CatPeriodoDatos)perioricidadconfiguracion.SelectedValue).Id != null)
                idConfiguracionPeriocidad = ((CatPeriodoDatos)perioricidadconfiguracion.SelectedValue).Id;
        }

        private void AceptarConfig_Click(object sender, RoutedEventArgs e)
        {
            var ControladorConfiguracionHMantenimiento = new ControladorConfiguracionesMantenimientosH();
            ControladorConfiguracionHMantenimiento.ModificaRegistro(idconfiguracionturno, idConfiguracionPeriocidad);
            Utilidades.Dialogos.Aviso("Se modificó la configuración Para las lecturas de medidores del Hotel");
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var ControladorConfiguracionHMantenimiento = new ControladorConfiguracionesMantenimientosH();
            ControladorConfiguracionHMantenimiento.ModificaRegistro(idconfiguracionturno, idConfiguracionPeriocidad);
            Utilidades.Dialogos.Aviso("Se modificó la configuración Para las lecturas de medidores del Hotel");
        }
    }
}
