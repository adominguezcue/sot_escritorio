﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Mantenimientos
{
    /// <summary>
    /// Lógica de interacción para HistorialMantenimientoForm.xaml
    /// </summary>
    public partial class HistorialMantenimientoForm : Window
    {
        List<Modelo.Dtos.DtoResumenMantenimiento> ListaMantenimiento = new List<Modelo.Dtos.DtoResumenMantenimiento>();																													   
        List<Modelo.Dtos.DtoMantenimientoResumenReporte> listareporte = new List<Modelo.Dtos.DtoMantenimientoResumenReporte>();																														   
        public HistorialMantenimientoForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarMantenimientos();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarMantenimientos();
                }
            }
        }
        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarMantenimientos();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                var habitaciones = controladorHabitaciones.ObtenerActivasConTipos();
                habitaciones.Insert(0, new Modelo.Entidades.Habitacion { NumeroHabitacion = "Todos" });																									   
                cbHabitaciones.ItemsSource = habitaciones;

                var controladoresConceptoMantenimiento = new ControladorConceptosMantenimiento();
                var conceptos = controladoresConceptoMantenimiento.ObtenerConceptos();
                conceptos.Insert(0, new Modelo.Entidades.ConceptoMantenimiento { Concepto = "Todos" });
                cbconcepto.ItemsSource = conceptos;
				
                CargarMantenimientos();
            }
        }

        private void CargarMantenimientos()
        {
            if (this.IsLoaded)
																								   

						 
            {
                int idH = cbHabitaciones.SelectedValue == null ? 0 : (int)cbHabitaciones.SelectedValue;
                int idconcep = cbconcepto.SelectedValue == null ? 0 : (int)cbconcepto.SelectedValue;

                var controlador = new ControladorMantenimientos();
                var mantenimientos = controlador.ObtenerMantenimientosHabitacion(idH, idconcep, dpFechaInicial.SelectedDate.Value.Date,
                                                                          dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1));

                var controladorTareas = new ControladorTareasMantenimiento();
                var items = controladorTareas.ObtenerMantenimientosPendientes(idH, dpFechaInicial.SelectedDate.Value.Date,
                                                                          dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1));

                var idsTareas = mantenimientos.Where(m => m.Estado.HasValue && m.Estado.Value == Modelo.Entidades.Mantenimiento.Estados.EnProceso).Select(m => m.IdTarea);

                items.RemoveAll(m => idsTareas.Contains(m.IdTarea));

                items.AddRange(mantenimientos);

                ListaMantenimiento = items;

                dgvMantenimiento.ItemsSource = ListaMantenimiento;

                CargaListaReporte(ListaMantenimiento);
            }
            
        }

        private void CargaListaReporte(List<Modelo.Dtos.DtoResumenMantenimiento> listamantenimiento)
        {
            listareporte = new List<Modelo.Dtos.DtoMantenimientoResumenReporte>();
            foreach (Modelo.Dtos.DtoResumenMantenimiento datosmantenimiento in listamantenimiento)
            {
                Modelo.Dtos.DtoMantenimientoResumenReporte dato1 = new Modelo.Dtos.DtoMantenimientoResumenReporte();
                dato1.habitacion = datosmantenimiento.NumeroHabitacio;
                dato1.NombreUsuario = datosmantenimiento.NombreUsuario;
                dato1.Fecha = datosmantenimiento.Fecha;
                dato1.Concepto = datosmantenimiento.Concepto;
                dato1.Descripcion = datosmantenimiento.Descripcion;
                listareporte.Add(dato1);
            }
			
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnReporte_Click(object sender, RoutedEventArgs e)
        {
            if(ListaMantenimiento.Count>0 && ListaMantenimiento != null)
            {
                var documentoReporte = new SOTWpf.Reportes.IncidenciasHabitacion.HistorialMantenimientoHabitaciones();
                documentoReporte.Load();
                var controladorGlobal = new ControladorConfiguracionGlobal();
                var config = controladorGlobal.ObtenerConfiguracionGlobal();
                System.DateTime localDate = System.DateTime.Now;
                documentoReporte.SetDataSource(new List<Dtos.DtoCabeceraReporte> {new Dtos.DtoCabeceraReporte
                    {
                        Direccion=config.Direccion,
                        FechaInicio = dpFechaInicial.SelectedDate.Value.Date,
                        FechaFin = dpFechaFinal.SelectedDate.Value.Date,
                        Hotel = config.Nombre,
                        Usuario = ControladorBase.UsuarioActual.NombreCompleto
                  } });
                documentoReporte.Subreports["DetalleMantenimiento"].SetDataSource(listareporte);
                var visorR = new VisorReportes();
                visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                visorR.UpdateLayout();
                Utilidades.Dialogos.MostrarDialogos(this, visorR);
            }
        }

        private void DgvMantenimiento_Sorting(object sender, DataGridSortingEventArgs e)
        {
            bool Orden = false;
            string _switch = "";
            _switch = e.Column.Header.ToString();

            if (e.Column.SortDirection == null || e.Column.SortDirection.Value.ToString() == "Descending")
                Orden = false;
            else if (e.Column.SortDirection.Value.ToString() == "Ascending")
                Orden = true;
            switch (_switch)
            {
                case "HABITACIÓN":
                    if (Orden == true)
                        listareporte = listareporte.OrderByDescending(o => o.habitacion).ToList();
                    else
                        listareporte = listareporte.OrderBy(o => o.habitacion).ToList();
                    break;
                case "FECHA":
                    if (Orden == true)
                        listareporte = listareporte.OrderByDescending(o => o.Fecha).ToList();
                    else
                        listareporte = listareporte.OrderBy(o => o.Fecha).ToList();
                    break;
                case "CONCEPTO":
                    if (Orden == true)
                        listareporte = listareporte.OrderByDescending(o => o.Concepto).ToList();
                    else
                        listareporte = listareporte.OrderBy(o => o.Concepto).ToList();
                    break;
                case "DESCRIPCIÓN":
                    if (Orden == true)
                        listareporte = listareporte.OrderByDescending(o => o.Descripcion).ToList();
                    else
                        listareporte = listareporte.OrderBy(o => o.Descripcion).ToList();
                    break;
                default:
                    break;
            }

        }

        private void Cbconcepto_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarMantenimientos();
		}			
    }
}
