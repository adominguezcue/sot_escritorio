﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SOTControladores.Controladores;
using Transversal.Excepciones;
using Modelo.Entidades;

namespace SOTWpf.Mantenimientos.EdicionCatalogos
{
    /// <summary>
    /// Lógica de interacción para EdicionMedicionLecturas.xaml
    /// </summary>
    public partial class EdicionMedicionLecturas : Window
    {
        bool _esmodificar = false;
        CatPresentacionMantenimiento grabapresentacionmantenimeinto = new CatPresentacionMantenimiento();
        public EdicionMedicionLecturas(bool esModificar, CatPresentacionMantenimiento catPresentacionMantenimiento )
        {
            InitializeComponent();
            _esmodificar = esModificar;
            if (_esmodificar)
            {
                grabapresentacionmantenimeinto = catPresentacionMantenimiento;
                DataContext = grabapresentacionmantenimeinto as CatPresentacionMantenimiento;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (_esmodificar)
            {
                ValidaCampos();
                var ControladorCatPresentacion = new ControladorLecturaHMantenimiento();
                grabapresentacionmantenimeinto.Descripcion = Descripcion.Text;
                grabapresentacionmantenimeinto.Presentacion = Unidad.Text;
                ControladorCatPresentacion.ModificaPresentacion(grabapresentacionmantenimeinto);
            }
            else
            {
                ValidaCampos();
                var ControladorCatPresentacion = new ControladorLecturaHMantenimiento();
                grabapresentacionmantenimeinto = new CatPresentacionMantenimiento();
                grabapresentacionmantenimeinto.Descripcion = Descripcion.Text;
                grabapresentacionmantenimeinto.Activa = true;
                grabapresentacionmantenimeinto.Presentacion = Unidad.Text;
                ControladorCatPresentacion.AgregaPresentacion(grabapresentacionmantenimeinto);
            }
            this.Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Activo_Checked(object sender, RoutedEventArgs e)
        {


        }

        private void ValidaCampos()
        {
            if(Descripcion.Text.Length<=0)
                throw new SOTException("Debes Poner Una descripción");
            if(Unidad.Text.Length<=0)
                throw new SOTException("Debes Poner Una Unidad");

        }
    }
}
