﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Modelo.Entidades;
using Transversal.Excepciones;
using SOTControladores.Controladores;
using Transversal.Utilidades;

namespace SOTWpf.Mantenimientos.EdicionCatalogos
{
    /// <summary>
    /// Lógica de interacción para EdicionPerioricidad.xaml
    /// </summary>
    public partial class EdicionPerioricidad : Window
    {
        bool _Esmodificar = false;
        CatPeriodoDatos datocontexto = new CatPeriodoDatos();

        public EdicionPerioricidad(bool esmodificar, CatPeriodoDatos catPeriodoDatos)
        {
            InitializeComponent();
            _Esmodificar = esmodificar;
            if (_Esmodificar)
            {
                datocontexto = catPeriodoDatos;
                DataContext = datocontexto as CatPeriodoDatos;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (_Esmodificar)
            {
                ValidaCampos();
                var ControladorCatPresentacion = new ControladorCatPeriodoDatos();
                datocontexto.Nombre = nombre.Text;
                datocontexto.ValorSegundo = Convert.ToInt32(Valor.Text);
                ControladorCatPresentacion.ModificaPerioricdad(datocontexto);
            }
            else
            {
                ValidaCampos();
                var Controladorperioricidad = new ControladorCatPeriodoDatos () ;
                datocontexto = new CatPeriodoDatos();
                datocontexto.Nombre = nombre.Text;
                datocontexto.Activa = true;
                datocontexto.ValorSegundo = Convert.ToInt32(Valor.Text);
                Controladorperioricidad.AgregaPerioricidad(datocontexto);
            }
            this.Close();
        }

        private void ValidaCampos()
        {
            if (nombre.Text.Length <= 0)
                throw new SOTException("Debes Poner Una descripción");
            if (Valor.Text.Length <= 0 && UtilidadesRegex.SoloDigitos(Valor.Text))
                throw new SOTException("Debes Poner Una Unidad");
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
