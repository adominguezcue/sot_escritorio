﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace SOTWpf.Impresiones
{
    public abstract class Impresora
    {
        internal static void Imprimir(ReportDocument documentoReporte, string nombreImpresora, int copias) 
        {
            documentoReporte.PrintOptions.NoPrinter = false;
            documentoReporte.PrintOptions.PrinterName = nombreImpresora;

            for (int i = 0; i < copias; i++)
                documentoReporte.PrintToPrinter(1, false, 0, 0);
        }

        internal static void Imprimir(UIElement elementoImprimir, string titulo) 
        {
            System.Windows.Controls.PrintDialog Printdlg = new System.Windows.Controls.PrintDialog();
            if ((bool)Printdlg.ShowDialog().GetValueOrDefault())
            {
                System.Windows.Size pageSize = new System.Windows.Size(Printdlg.PrintableAreaWidth, Printdlg.PrintableAreaHeight);
                // sizing of the element.
                elementoImprimir.Measure(pageSize);
                elementoImprimir.Arrange(new Rect(5, 5, pageSize.Width, pageSize.Height));
                Printdlg.PrintVisual(elementoImprimir, titulo);
            }
        }

        internal static void ImprimirTicket(FixedDocument documento, string nombreImpresora, int copias)
        {
            //using (ReportClass documentoReporte = resumen.EsComanda ? (ReportClass)new FormatoTicketComanda() : new FormatoTicketGeneral())
            //{

            try
            {
                for (int i = 0; i < copias; i++)
                {
                    using (var printFont = new Font("Arial", 9))
                    {
                        using (var pd = new PrintDocumentFD(documento))
                        {
                            try
                            {
                                pd.PrinterSettings.PrinterName = nombreImpresora;

                                //var psize = new PaperSize("CUSTOM", 280, 1000);

                                pd.PrintController = new StandardPrintController();
                                //pd.PrinterSettings.DefaultPageSettings.PaperSize = psize;
                                //pd.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                                //pd.DefaultPageSettings.PaperSize = psize;
                                //pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                                pd.PrinterSettings.Copies = 1;
                                pd.PrintPage += pd_PrintPageFD;
                                pd.Print();

                            }
                            finally
                            {
                                if (pd != null)
                                    pd.PrintPage -= pd_PrintPageFD;
                                //streamToPrint.Close();
                            }
                        }
                    }
                }
            }
            finally
            {
                //try
                //{
                //    documentoReporte.Close();
                //}
                //catch { }
            }
            //}
        }

        private static void pd_PrintPageFD(object sender, PrintPageEventArgs ev)
        {
            /*
            var pd = sender as PrintDocumentFD;

            if (pd == null)
            {
                ev.HasMorePages = false;
                return;
            }

            var XD = new StreamReader("");

            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

             //Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height /
               printFont.GetHeight(ev.Graphics);

            // (int)(ev.MarginBounds.Width / pd.Fuente.GetHeight(ev.Graphics));

            float startX = leftMargin;
            float startY = topMargin;
            float Offset = 0;

            int anchura = ev.MarginBounds.Width;

            SizeF layoutSize;// = new SizeF(anchura, tamF);
            RectangleF layout;// = new RectangleF(new PointF(startX, startY + Offset), layoutSize);

            //var tama = ev.Graphics.MeasureString("PRUEBA", pd.Fuente, pd.CaracteresPorLinea, new StringFormat(StringFormatFlags.NoClip));

            //pd.CaracteresPorLinea = 76 / ((int)tama.Width / ("PRUEBA").Length);
            //pd.CaracteresPorLinea = 76;
            // Print each line of the file.

            //yPos = topMargin + (count *
            //       pd.Fuente.GetHeight(ev.Graphics));

            //var linesPerPage = ev.MarginBounds.Height / pd.Fuente.GetHeight(ev.Graphics);

            if (!pd.Procesado)
                pd.Procesar();

            while (((line = XD.ReadLine()) != null))
            {
                count++;
            }

            //If more lines exist, print another page.
            if (pd.ObtenerTablaActual() != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;*/
        }
    }
}
