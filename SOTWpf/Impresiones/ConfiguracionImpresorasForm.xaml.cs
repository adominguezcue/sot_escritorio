﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Impresiones
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionImpresorasForm.xaml
    /// </summary>
    public partial class ConfiguracionImpresorasForm : Window
    {
        bool esLocal;

        public ConfiguracionImpresorasForm(bool esLocal = false)
        {
            InitializeComponent();

            this.esLocal = esLocal;

            if (esLocal)
                txtDatoHabitacion.Text += " locales";
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorConfiguracionesI = new SOTControladores.Controladores.ControladorConfiguracionesImpresoras();
            if(esLocal)
                controladorConfiguracionesI.ActualizarConfiguracionLocal(DataContext as Modelo.Entidades.ConfiguracionImpresoras);
            else
            controladorConfiguracionesI.ActualizarConfiguracion(DataContext as Modelo.Entidades.ConfiguracionImpresoras);

            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var impresoras = new List<string>();

                if (esLocal)
                    impresoras.Add("");

                foreach (string item in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                    impresoras.Add(item);

                cbImpresoras.ItemsSource = impresoras;

                var controladorConfiguracionesI = new SOTControladores.Controladores.ControladorConfiguracionesImpresoras();

                DataContext = esLocal ? controladorConfiguracionesI.ObtenerConfiguracionImpresorasLocal() :
                    controladorConfiguracionesI.ObtenerConfiguracionImpresoras();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
