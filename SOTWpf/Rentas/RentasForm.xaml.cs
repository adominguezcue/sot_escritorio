﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Matriculas;
using SOTWpf.Pagos;
using SOTWpf.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Dtos;
using Transversal.Excepciones;

namespace SOTWpf.Rentas
{
    /// <summary>
    /// Lógica de interacción para RentasForm.xaml
    /// </summary>
    public partial class RentasForm : Window
    {
        private Modelo.Entidades.Reservacion reservacion = null;

        private bool puedeCobrar;

        private Modelo.Entidades.ConfiguracionTipo configuracionTipo;
        private Modelo.Almacen.Entidades.ZctCatPar config;

        private Modelo.Entidades.Habitacion _habitacion;

        private Modelo.Entidades.Renta rentaActual;

        private AutomovilRenta AutoActual
        {
            get { return datosAuto.DataContext as AutomovilRenta; }
            set 
            { 
                datosAuto.DataContext = value;
            }
        }

        private ConsultaTarjetaVForm.DatosTarjeta datosT { get; set; }

        public RentasForm(Modelo.Entidades.Habitacion habitacion)
        {
            InitializeComponent();

            AutoActual = new AutomovilRenta { Activa = true };

            config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            var controladorPermisos = new ControladorPermisos();
            puedeCobrar = controladorPermisos.VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos
            {
                AsignarHabitaciones = true,
                CobrarHabitaciones = true
            });
            ActualizarHabitacion(habitacion);
        }

        private void MostrarParaCrear()
        {
            nudHabitaciones.IsEnabled = nudPaquetes.IsEnabled = txtTarjetaV.IsEnabled = false;
            nudPersonasExtra.IsEnabled = nudHospedajeExtra.IsEnabled = cbTarifa.IsEnabled = chbAPie.IsEnabled = true;

            btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Collapsed;
            btnVPoints.Visibility = puedeCobrar ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;

            ProcesarReservacion();
        }

        private void ProcesarReservacion()
        {
            if (reservacion != null) 
            {
                cbTarifa.IsEnabled = nudHabitaciones.IsEnabled = nudHospedajeExtra.IsEnabled = nudPersonasExtra.IsEnabled = nudPaquetes.IsEnabled = false;

                cbTarifa.SelectedItem = reservacion.ConfiguracionTipo.ConfiguracionTarifa.Tarifa;

                nudHabitaciones.Value = 1;
                nudHospedajeExtra.Value = reservacion.Noches - 1;
                nudPersonasExtra.Value = reservacion.PersonasExtra;
                nudPaquetes.Value = reservacion.PaquetesReservaciones.Where(m=>m.Activo && m.Precio > 0).Sum(m=> m.Cantidad);

                txtFolio.Text = reservacion.CodigoReserva;

                lblPaqueteTotal.Text = (reservacion.Paquetes * (1 + config.Iva_CatPar)).ToString("C");

                RecargarCantidades();
            }
        }

        private void MostrarParaEditar()
        {
            nudHabitaciones.IsEnabled = txtTarjetaV.IsEnabled = nudPaquetes.IsEnabled = cbTarifa.IsEnabled = false;
            nudPersonasExtra.IsEnabled = nudHospedajeExtra.IsEnabled = chbAPie.IsEnabled = true;

            btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Visible;
            btnVPoints.Visibility = puedeCobrar ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            btnVPoints.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void MostrarParaCobrar()
        {
            nudHabitaciones.IsEnabled = nudPersonasExtra.IsEnabled = nudHospedajeExtra.IsEnabled = false;
            nudPaquetes.IsEnabled = cbTarifa.IsEnabled = chbAPie.IsEnabled = txtTarjetaV.IsEnabled = false;
            gbDatosEntrada.IsEnabled = false;

            btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Visible;
        }

        private void ActualizarHabitacion(Modelo.Entidades.Habitacion habitacion)
        {
            if (habitacion == null)
                throw new SOTException(Textos.Errores.renta_habitacion_nula_exception);

            _habitacion = habitacion;

            lblDatosHabitacion.Text = string.Format(Textos.Etiquetas.RentasForm_InformacionHabitacion, habitacion.NumeroHabitacion, habitacion.NombreTipo);

            var controlador = new ControladorTiposHabitacion();
            var tarifas = controlador.ObtenerTarifasSoportadas(_habitacion.IdTipoHabitacion);

            cbTarifa.ItemsSource = tarifas;

            if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada)
            {
                lblDatosHospedaje.Text = string.Empty;

                MostrarParaCrear();
                nudHabitaciones.Value = 1;
            }
            else if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.PreparadaReservada)
            {
                var controladorReservaciones = new ControladorReservaciones();

                reservacion = controladorReservaciones.ObtenerReservacionActual(habitacion.Id);
                lblDatosHospedaje.Text = string.Empty;

                MostrarParaCrear();
            }
            else if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada)
            {
                var controladorR = new ControladorRentas();

                rentaActual = controladorR.ObtenerRentaActualPorHabitacion(habitacion.Id);
                txtTarjetaV.Text = rentaActual.NumeroTarjeta;


                cbTarifa.SelectedItem = rentaActual.TarifaTmp;

                var fechaFin = rentaActual.FechaFin;

                var tiemposE = rentaActual.VentasRenta.Where(m => m.Activo).SelectMany(m => m.TiemposExtra).Where(m => m.Activa).ToList();

                if (tiemposE.Count > 0)
                    fechaFin = fechaFin.AddMinutes(tiemposE.Sum(m => m.Minutos));

                TimeSpan tiempo = (fechaFin - rentaActual.FechaInicio);

                lblDatosHospedaje.Text = string.Format(Textos.Etiquetas.RentasForm_InformacionHospedaje, tiempo.ToString("hh\\:mm"));

                var automovil = rentaActual.RentaAutomoviles.FirstOrDefault(m => m.Activa);

                if (automovil != null)
                    AutoActual = automovil;
                else
                    AutoActual = new AutomovilRenta();

                MostrarParaEditar();
            }
            else if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.PendienteCobro)
            {
                var controladorR = new ControladorRentas();

                rentaActual = controladorR.ObtenerRentaActualPorHabitacion(habitacion.Id);

                cbTarifa.SelectedItem = rentaActual.TarifaTmp;

                var fechaFin = rentaActual.FechaFin;

                var tiemposE = rentaActual.VentasRenta.Where(m => m.Activo).SelectMany(m => m.TiemposExtra).Where(m => m.Activa).ToList();

                if (tiemposE.Count > 0)
                    fechaFin = fechaFin.AddMinutes(tiemposE.Sum(m => m.Minutos));

                TimeSpan tiempo = (fechaFin - rentaActual.FechaInicio);

                lblDatosHospedaje.Text = string.Format(Textos.Etiquetas.RentasForm_InformacionHospedaje, tiempo.ToString("hh\\:mm"));

                var ventaActual = rentaActual.VentasRenta.First(m => m.Activo);

                nudHabitaciones.Value = 1;

                if (ventaActual != null)
                {
                    nudPersonasExtra.Value = ventaActual.PersonasExtra.Count(m => m.Activa);
                    nudHospedajeExtra.Value = ventaActual.TiemposExtra.Count(m => m.Activa);
                    nudPaquetes.Value = ventaActual.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).Sum(m=> m.Cantidad);
                }

                RecargarCantidadesCobro();

                var automovil = rentaActual.RentaAutomoviles.FirstOrDefault(m => m.Activa);

                if (automovil != null)
                    AutoActual = automovil;
                else
                    AutoActual = new AutomovilRenta();

                MostrarParaCobrar();
            }
        }

        private void RentasForm_Load(object sender, RoutedEventArgs e)
        {
            if (rentaActual != null && rentaActual.Estado == Modelo.Entidades.Renta.Estados.PendienteCobro)
            { 
                RecargarCantidadesCobro();

                var auto = rentaActual.RentaAutomoviles.FirstOrDefault(m => m.Activa);

                if (auto != null)
                    AutoActual = rentaActual.RentaAutomoviles.First(m => m.Activa);
            }
            else
                RecargarCantidades();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (rentaActual == null)
                CrearRenta();
            else if (rentaActual.Estado == Modelo.Entidades.Renta.Estados.EnCurso)
                ModificarRenta();
            else if (rentaActual.Estado == Modelo.Entidades.Renta.Estados.PendienteCobro)
                CobrarRenta();
        }

        private void RecargarCantidades()
        {
            if (!IsLoaded)
                return;

            decimal precioHotel, precioPExtra, precioHExtra, precionPaquetes, descuento;

            if (reservacion != null)
            {
                precioHotel = reservacion.TotalHabitacion;
                precioPExtra = reservacion.PersonasExtra != 0 ? reservacion.TotalPersonasExtra / (reservacion.PersonasExtra * reservacion.Noches) : 0;
                precioHExtra = reservacion.Noches == 1 ? 0 :
                               reservacion.TotalHospedajeExtra / (reservacion.Noches - 1);

                precionPaquetes = reservacion.Paquetes;
                descuento = reservacion.Descuento;
            }
            else if (configuracionTipo != null)
            {
                precioHotel = configuracionTipo.Precio;
                precioPExtra = configuracionTipo.PrecioPersonaExtra;
                precioHExtra = configuracionTipo.PrecioHospedajeExtra;
                precionPaquetes = descuento = 0;
            }
            else
                precioHotel = precioPExtra = precioHExtra = precionPaquetes = descuento = 0;

            lblHabitacionPUnidad.Text = (precioHotel * (1 + config.Iva_CatPar)).ToString("C");
            decimal totalHab = (int)nudHabitaciones.Value * precioHotel;
            lblHabitacionTotal.Text = (totalHab * (1 + config.Iva_CatPar)).ToString("C");

            lblPersonasExtraPUnidad.Text = (precioPExtra * (1 + config.Iva_CatPar)).ToString("C");

            decimal totalPExtra = (int)nudPersonasExtra.Value * precioPExtra;
            
            if(reservacion != null)
                totalPExtra *= reservacion.Noches;

            lblPersonasExtraTotal.Text = (totalPExtra * (1 + config.Iva_CatPar)).ToString("C");

            lblHospedajeExtraPUnidad.Text = (precioHExtra * (1 + config.Iva_CatPar)).ToString("C");
            decimal totalHospExt = (int)nudHospedajeExtra.Value * precioHExtra;
            lblHospedajeExtraTotal.Text = (totalHospExt * (1 + config.Iva_CatPar)).ToString("C");

            lblPaqueteTotal.Text = (precionPaquetes * (1 + config.Iva_CatPar)).ToString("C");

            var totalPositivo = totalHab + totalPExtra + totalHospExt + precionPaquetes;

            if (reservacion == null)
            {
                descuento = datosT != null && datosT.Descontar ? (datosT.Saldo / (1 + config.Iva_CatPar)) : 0;
                descuento = (descuento >= totalPositivo ? totalPositivo : descuento);
            }

            lblDescuentosTotal.Text = (descuento * (1 + config.Iva_CatPar)).ToString("C");

            var totalFinal = totalPositivo - descuento;

            lblTotalSinIVA.Text = totalFinal.ToString("C");
            lblIVA.Text = (totalFinal * config.Iva_CatPar).ToString("C");
            lblTotalConIVA.Text = (totalFinal * (1 + config.Iva_CatPar)).ToString("C");
        }

        private void RecargarCantidadesCobro()
        {
            decimal precioHotel, precioPExtra, precioHExtra, precionPaquetes, descuento;

            var ventaActual = rentaActual.VentasRenta.First(m => m.Activo);

            if (ventaActual != null)
            {
                precioHotel = rentaActual.PrecioHabitacion;
                precioPExtra = ventaActual.PersonasExtra.Any(m => m.Activa) ? ventaActual.PersonasExtra.First(m => m.Activa).Precio : 0;
                precioHExtra = ventaActual.TiemposExtra.Any(m => m.Activa) ? ventaActual.TiemposExtra.First(m => m.Activa).Precio : 0;

                precionPaquetes = ventaActual.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).Sum(m => m.Precio * m.Cantidad);
                descuento = ventaActual.PaquetesRenta.Where(m => m.Activo && m.Descuento > 0).Sum(m => m.Descuento * m.Cantidad);
            }
            else if (configuracionTipo != null)
            {
                precioHotel = configuracionTipo.Precio;
                precioPExtra = configuracionTipo.PrecioPersonaExtra;
                precioHExtra = configuracionTipo.PrecioHospedajeExtra;
                precionPaquetes = descuento = 0;
            }
            else
                precioHotel = precioPExtra = precioHExtra = precionPaquetes = descuento = 0;

            lblHabitacionPUnidad.Text = (precioHotel * (1 + config.Iva_CatPar)).ToString("C");
            decimal totalHab = (int)nudHabitaciones.Value * precioHotel;
            lblHabitacionTotal.Text = (totalHab * (1 + config.Iva_CatPar)).ToString("C");

            lblPersonasExtraPUnidad.Text = (precioPExtra * (1 + config.Iva_CatPar)).ToString("C");

            decimal totalPExtra = (int)nudPersonasExtra.Value * precioPExtra;

            lblPersonasExtraTotal.Text = (totalPExtra * (1 + config.Iva_CatPar)).ToString("C");

            lblHospedajeExtraPUnidad.Text = (precioHExtra * (1 + config.Iva_CatPar)).ToString("C");
            decimal totalHospExt = (int)nudHospedajeExtra.Value * precioHExtra;
            lblHospedajeExtraTotal.Text = (totalHospExt * (1 + config.Iva_CatPar)).ToString("C");

            lblPaqueteTotal.Text = (precionPaquetes * (1 + config.Iva_CatPar)).ToString("C");

            var totalPositivo = totalHab + totalPExtra + totalHospExt + precionPaquetes;

            if (ventaActual == null)
            {
                descuento = datosT != null && datosT.Descontar ? (datosT.Saldo / (1 + config.Iva_CatPar)) : 0;
                descuento = (descuento >= totalPositivo ? totalPositivo : descuento);
            }

            lblDescuentosTotal.Text = (descuento * (1 + config.Iva_CatPar)).ToString("C");

            var totalFinal = totalPositivo - descuento;

            lblTotalSinIVA.Text = totalFinal.ToString("C");
            lblIVA.Text = (totalFinal * config.Iva_CatPar).ToString("C");
            lblTotalConIVA.Text = (totalFinal * (1 + config.Iva_CatPar)).ToString("C");
        }

        private void btnCancelarEntrada_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(string.Format(Textos.Mensajes.cancelar_preparacion, _habitacion.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorHabitaciones();
                controlador.CancelarPreparacionHabitacion(_habitacion.Id);
                Close();
            }
        }

        private void CrearRenta()
        {
            if (configuracionTipo == null)
                throw new SOTException(Textos.Errores.tarifa_no_seleccionada_exception);

            if (_habitacion == null || (_habitacion.EstadoHabitacion != Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada &&
                                        _habitacion.EstadoHabitacion != Modelo.Entidades.Habitacion.EstadosHabitacion.PreparadaReservada))
                return;

            ValidarDatosAutomovil();

            var controladorRM = new ControladorReportesMatriculas();

            if (AutoActual.Activa && controladorRM.PoseeReportes(AutoActual.Matricula))
            {
                var reporteadorMatriculas = new ReportesMatriculaForm(AutoActual.Matricula);
                Utilidades.Dialogos.MostrarDialogos(this, reporteadorMatriculas);

                if (MessageBox.Show(this, Textos.Mensajes.continuar_proceso_renta, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
            }

            decimal precioHotel, precioPExtra, precioHExtra;

            if (reservacion != null)
            {
                precioHotel = reservacion.TotalHabitacion;
                precioPExtra = reservacion.PersonasExtra != 0 ? reservacion.TotalPersonasExtra / reservacion.PersonasExtra : 0;
                precioHExtra = reservacion.Noches == 1 ? 0 :
                               reservacion.TotalHospedajeExtra / (reservacion.Noches - 1);
            }
            else if (configuracionTipo == null)
                precioHotel = precioPExtra = precioHExtra = 0;
            else
            {
                precioHotel = configuracionTipo.Precio;
                precioPExtra = configuracionTipo.PrecioPersonaExtra;
                precioHExtra = configuracionTipo.PrecioHospedajeExtra;
            }

            var rentaNueva = new Modelo.Entidades.Renta
            {
                Activa = true,
                IdConfiguracionTipo = configuracionTipo.Id,
            };

            if (datosT != null)
                rentaNueva.NumeroTarjeta = datosT.NumeroTarjeta;

            rentaNueva.IdHabitacion = _habitacion.Id;
            rentaNueva.PrecioHabitacion = precioHotel;

            if (reservacion != null)
                rentaNueva.IdDatosFiscales = reservacion.IdDatosFiscales;

            var ventaRentaNueva = new Modelo.Entidades.VentaRenta { Activo = true };
            rentaNueva.VentasRenta.Add(ventaRentaNueva);

            if (configuracionTipo.ConfiguracionTarifa.RangoFijo)
            {
                for (int i = 0; i < nudHospedajeExtra.Value; i++)
                {
                    ventaRentaNueva.Renovaciones.Add(new Modelo.Entidades.Renovacion
                    {
                        Activa = true,
                        Precio = precioHExtra,
                        Folio = Guid.NewGuid().ToString()
                    });

                    for (int j = 1; j <= nudPersonasExtra.Value; j++)
                    {
                        ventaRentaNueva.PersonasExtra.Add(new Modelo.Entidades.PersonaExtra
                        {
                            Activa = true,
                            Precio = precioPExtra,
                            FolioRenovacion = ventaRentaNueva.Renovaciones.Last().Folio
                        });
                    }
                }
            }
            else
            {
                var contador = 1;

                foreach (var configTiempo in (from t in configuracionTipo.TiemposHospedaje
                                              where t.Activo
                                              orderby t.Orden ascending
                                              select t))
                {

                    if (contador > nudHospedajeExtra.Value)
                        break;

                    ventaRentaNueva.TiemposExtra.Add(new Modelo.Entidades.TiempoExtra
                    {
                        Activa = true,
                        Precio = precioHExtra,
                        IdTiempoHospedaje = configTiempo.Id,
                        Minutos = configTiempo.Minutos
                    });

                    contador++;
                }
            }


            for (int i = 1; i <= nudPersonasExtra.Value; i++)
            {
                ventaRentaNueva.PersonasExtra.Add(new Modelo.Entidades.PersonaExtra
                {
                    Activa = true,
                    Precio = precioPExtra,
                    FolioRenovacion = "",
                });
            }

            if (reservacion != null)
            {
                foreach (var paquete in reservacion.PaquetesReservaciones.Where(m => m.Activo))
                    ventaRentaNueva.PaquetesRenta.Add(new Modelo.Entidades.PaqueteRenta
                    {
                        Activo = true,
                        Descuento = paquete.Descuento,
                        IdPaquete = paquete.IdPaquete,
                        Precio = paquete.Precio,
                        Cantidad = paquete.Cantidad,
                    });
            }

            decimal valorSinIva = rentaNueva.PrecioHabitacion;

            if (ventaRentaNueva.PersonasExtra.Any(m => m.Activa))
                valorSinIva += ventaRentaNueva.PersonasExtra.Where(m => m.Activa).Sum(m => m.Precio);

            if (ventaRentaNueva.TiemposExtra.Any(m => m.Activa))
                valorSinIva += ventaRentaNueva.TiemposExtra.Where(m => m.Activa).Sum(m => m.Precio);

            if (ventaRentaNueva.Renovaciones.Any(m => m.Activa))
                valorSinIva += ventaRentaNueva.Renovaciones.Where(m => m.Activa).Sum(m => m.Precio);

            if (ventaRentaNueva.PaquetesRenta.Any(m => m.Activo))
                valorSinIva += ventaRentaNueva.PaquetesRenta.Where(m => m.Activo).Sum(m => (m.Precio - m.Descuento) * m.Cantidad);

            decimal descuentoVConIVA = datosT != null && datosT.Descontar ? datosT.Saldo : 0;
            decimal descuentoV = descuentoVConIVA / (1 + config.Iva_CatPar);

            descuentoV = (descuentoV >= valorSinIva ? valorSinIva : descuentoV);

            ConsumoInternoHabitacion ci = null;

            if (puedeCobrar)
            {
                if (reservacion == null && descuentoV < valorSinIva)
                {
                    var vSinIVA = (valorSinIva - descuentoV);
                    var vIVA = vSinIVA * config.Iva_CatPar;
                    var vConIVA = vSinIVA + vIVA;

                    var selectorFormaPago = new SelectorFormaPagoForm(vSinIVA, vIVA, vConIVA, 0, true, true, true);

                    Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                    if (!selectorFormaPago.ProcesoExitoso)
                        return;

                    foreach (var pagoSeleccionado in selectorFormaPago.FormasPagoSeleccionadas)
                    {
                        ventaRentaNueva.Pagos.Add(new Modelo.Entidades.PagoRenta
                        {
                            Activo = true,
                            TipoPago = pagoSeleccionado.TipoPago,
                            Referencia = pagoSeleccionado.Referencia,
                            Valor = pagoSeleccionado.Valor,
                            TipoTarjeta = pagoSeleccionado.TipoTarjeta,
                            NumeroTarjeta = pagoSeleccionado.NumeroTarjeta,
                        });
                    }

                    ci = selectorFormaPago.DatosConsumo;
                }

                if (reservacion != null)
                    ventaRentaNueva.Pagos.Add(new Modelo.Entidades.PagoRenta
                    {
                        Activo = true,
                        TipoPago = TiposPago.Reservacion,
                        Referencia = reservacion.CodigoReserva,
                        Valor = valorSinIva * (1 + config.Iva_CatPar)
                    });
                else if (descuentoV > 0)
                    ventaRentaNueva.Pagos.Add(new Modelo.Entidades.PagoRenta
                    {
                        Activo = true,
                        TipoPago = TiposPago.VPoints,
                        Referencia = datosT.NumeroTarjeta.Trim(),
                        Valor = descuentoVConIVA
                    });
            }

            if (AutoActual.Activa)
                rentaNueva.RentaAutomoviles.Add(AutoActual);

            ventaRentaNueva.ValorSinIVA = valorSinIva;
            ventaRentaNueva.ValorIVA = ventaRentaNueva.ValorSinIVA * config.Iva_CatPar;
            ventaRentaNueva.ValorConIVA = ventaRentaNueva.ValorSinIVA + ventaRentaNueva.ValorIVA;

            var controlador = new ControladorRentas();

            if (puedeCobrar)
                controlador.CrearYPagarRenta(rentaNueva, nudPersonasExtra.Value > configuracionTipo.TipoHabitacion.MaximoPersonasExtra, ci);
            else
                controlador.CrearRenta(rentaNueva, nudPersonasExtra.Value > configuracionTipo.TipoHabitacion.MaximoPersonasExtra);

            MessageBox.Show(Textos.Mensajes.renta_agregada_con_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void ValidarDatosAutomovil()
        {
            if (AutoActual.Activa)
            {
                if (string.IsNullOrWhiteSpace(AutoActual.Matricula))
                    throw new SOTException(Textos.Errores.matricula_invalida_exception);

                if (string.IsNullOrWhiteSpace(AutoActual.Marca))
                    throw new SOTException(Textos.Errores.marca_invalida_exception);

                if (string.IsNullOrWhiteSpace(AutoActual.Modelo))
                    throw new SOTException(Textos.Errores.modelo_invalido_exception);

                if (string.IsNullOrWhiteSpace(AutoActual.Color))
                    throw new SOTException(Textos.Errores.color_invalido_exception);
            }
        }

        private void ModificarRenta()
        {
            if (configuracionTipo == null)
                throw new SOTException(Textos.Errores.tarifa_no_seleccionada_exception);

            ValidarDatosAutomovil();

            var controladorR = new ControladorRentas();

            var valor = (configuracionTipo.PrecioHospedajeExtra * (int)nudHospedajeExtra.Value) + 
                          (configuracionTipo.PrecioPersonaExtra * (int)nudPersonasExtra.Value);

            var pagos = new List<Modelo.Dtos.DtoInformacionPago>();
            ConsumoInternoHabitacion consumo = null;

            if (valor > 0)
            {
                var vIVA = valor * config.Iva_CatPar;
                var vConIVA = valor + vIVA;

                var selectorFormaPago = new SelectorFormaPagoForm(valor, vIVA, vConIVA, 0, true, true, true);

                Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                if (!selectorFormaPago.ProcesoExitoso)
                    return;

                pagos = selectorFormaPago.FormasPagoSeleccionadas;
                consumo = selectorFormaPago.DatosConsumo;
            }

            var autos = new List<Modelo.Dtos.DtoAutomovil>();

            if (AutoActual.Activa)
            {
                autos.Add(new Modelo.Dtos.DtoAutomovil
                {
                    Color = AutoActual.Color,
                    Marca = AutoActual.Marca,
                    Matricula = AutoActual.Matricula,
                    Modelo = AutoActual.Modelo
                });
            }

            controladorR.ActualizarDatosHabitacion(rentaActual.Id, (int)nudHospedajeExtra.Value, (int)nudPersonasExtra.Value, autos, pagos, consumo);

            MessageBox.Show(Textos.Mensajes.datos_renta_actualizados_con_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void CobrarRenta()
        {
            ValidarDatosAutomovil();

            var controladorR = new ControladorRentas();

            var valorSinIVA = rentaActual.VentasRenta.First(m => m.Activo).ValorSinIVA;
            var valorIVA = rentaActual.VentasRenta.First(m => m.Activo).ValorIVA;
            var valorConIVA = rentaActual.VentasRenta.First(m => m.Activo).ValorConIVA;

            var pagos = new List<Modelo.Dtos.DtoInformacionPago>();

            Propina propina = null;
            ConsumoInternoHabitacion consumo = null;

            if (valorSinIVA > 0)
            {
                var selectorFormaPago = new SelectorFormaPagoForm(valorSinIVA, valorIVA, valorConIVA, 0, false, true, true);

                Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                if (!selectorFormaPago.ProcesoExitoso)
                    return;

                pagos = selectorFormaPago.FormasPagoSeleccionadas;
                propina = selectorFormaPago.PropinaGenerada;
                consumo = selectorFormaPago.DatosConsumo;
            }

            controladorR.PagarRenta(rentaActual.Id, pagos, propina, consumo);

            MessageBox.Show(Textos.Mensajes.datos_renta_actualizados_con_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void btnSolicitarCancelacion_Click(object sender, RoutedEventArgs e)
        {
            if (rentaActual != null && MessageBox.Show(Textos.Mensajes.confirmar_cancelacion_renta, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorHabitaciones();
                controlador.FinalizarOcupacion(rentaActual.IdHabitacion, true);

                Close();

                MessageBox.Show(Textos.Mensajes.cancelacion_renta_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnVPoints_Click(object sender, RoutedEventArgs e)
        {
            var consultaTarjetaVF = new ConsultaTarjetaVForm();
            Utilidades.Dialogos.MostrarDialogos(this, consultaTarjetaVF);

            datosT = consultaTarjetaVF.DatosT;

            if (datosT != null)
            {
                datosT.Descontar = reservacion == null && MessageBox.Show(Textos.Acciones.UsarVPoint, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;

                if (datosT.Descontar)
                    lblDescuentos.Text = "Descuentos VPoints (-)";
                else
                    lblDescuentos.Text = "Descuentos (-)";

                txtTarjetaV.Text = datosT.NumeroTarjeta;
                txtVpoints.Text = datosT.Saldo.ToString("C");

                txtVpoints.Visibility = System.Windows.Visibility.Visible;
                lblVPoints.Visibility = System.Windows.Visibility.Visible;
            }
            else 
            {
                lblDescuentos.Text = "Descuentos (-)";
                txtTarjetaV.Clear();

                txtVpoints.Visibility = System.Windows.Visibility.Hidden;
                lblVPoints.Visibility = System.Windows.Visibility.Hidden;
            }

            RecargarCantidades();
        }

        private void cbTarifa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var controlador = new ControladorConfiguracionesTipo();

            if (cbTarifa.SelectedIndex >= 0)
            {
                Modelo.Entidades.ConfiguracionTarifa.Tarifas tarifaElegida = (Modelo.Entidades.ConfiguracionTarifa.Tarifas)cbTarifa.SelectedValue;

                configuracionTipo = controlador.ObtenerConfiguracionTipo(_habitacion.IdTipoHabitacion, tarifaElegida);
            }
            else
                configuracionTipo = null;

            nudHospedajeExtra.IsEnabled = reservacion == null;

            if (nudHospedajeExtra.IsEnabled && (configuracionTipo == null || configuracionTipo.ConfiguracionTarifa.RangoFijo))
                nudHospedajeExtra.Maximum = int.MaxValue;
            else if (nudHospedajeExtra.IsEnabled)
                nudHospedajeExtra.Maximum = configuracionTipo.TiemposHospedaje.Count(m => m.Activo);

            RecargarCantidades();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        } 

        private void nudHabitaciones_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            var nud = sender as MahApps.Metro.Controls.NumericUpDown;

            if (nud != null && !nud.Value.HasValue)
                nud.Value = 0;
            else
                RecargarCantidades();
        }
    }
}
