﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Matriculas;
using SOTWpf.Pagos;
using SOTWpf.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Rentas
{
    /// <summary>
    /// Lógica de interacción para RentasFormV2.xaml
    /// </summary>
    public partial class RentasFormV2 : Window
    {
        bool tarjetaCambiable = true;
        ResumenRenta ResumenActual 
        {
            get { return DataContext as ResumenRenta; }
            set { DataContext = value; }
        }


        private Modelo.Entidades.Reservacion reservacion = null;

        private bool puedeCobrar;

        private Modelo.Entidades.ConfiguracionTipo configuracionTipo;
        private Modelo.Almacen.Entidades.ZctCatPar config;

        private DtoResumenHabitacion _habitacion;

        private Modelo.Entidades.Renta rentaActual;

        private AutomovilRenta AutoActual
        {
            get { return datosAuto.DataContext as AutomovilRenta; }
            set 
            { 
                datosAuto.DataContext = value;
            }
        }

        private ConsultaTarjetaVForm.DatosTarjeta datosT { get; set; }

        public RentasFormV2(DtoResumenHabitacion habitacion)
        {
            InitializeComponent();

            btnVPoints.Flags = new Componentes.Buttons.HideableButton.FlagsList() { App.GestionarPuntosLealtad };

            ResumenActual = new ResumenRenta();
            ResumenActual.PropertyChanged += ResumenActual_PropertyChanged; 

            AutoActual = new AutomovilRenta { Activa = true };

            config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            var controladorPermisos = new ControladorPermisos();
            puedeCobrar = controladorPermisos.VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos
            {
                AsignarHabitaciones = true,
                CobrarHabitaciones = true
            });

            ActualizarHabitacion(habitacion);
        }

        private void ResumenActual_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var x = Transversal.Extensiones.ObjectExtensions.NombrePropiedad<ResumenRenta, decimal>(m => m.ValorDescuentosV);

            if (e.PropertyName != x)
            {
                if (!IsLoaded)
                    return;


                var ventaActual = rentaActual != null ? rentaActual.VentasRenta.FirstOrDefault(m => m.Activo && !m.Cobrada) : null;

                if (ventaActual != null)
                {
                    ResumenActual.ValorDescuentosV = (datosT != null && datosT.Descontar) ? Math.Min(datosT.Saldo, ResumenActual.SubTotal) : 0;
                }
                else if (reservacion != null)
                {
                    ResumenActual.ValorDescuentosV = 0;
                }
                else if (configuracionTipo != null)
                {
                    ResumenActual.ValorDescuentosV = (datosT != null && datosT.Descontar) ? Math.Min(datosT.Saldo, ResumenActual.SubTotal) : 0;
                }


                //else
                //{
                //    ResumenActual.TarifaHabitacion = 0;
                //    ResumenActual.TarifaPersonasExtra = 0;
                //    ResumenActual.TarifaHorasExtra = 0;
                //}
            }
        }

        private void MostrarParaCrear()
        {
            nudHabitaciones.IsEnabled = nudPaquetes.IsEnabled = txtTarjetaV.IsEnabled = false;
            nudPersonasExtra.IsEnabled = nudHoraseExtra.IsEnabled = nudNochesExtra.IsEnabled = cbTarifa.IsEnabled = chbAPie.IsEnabled = true;

            btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Collapsed;
            btnVPoints.Visibility = System.Windows.Visibility.Visible;//puedeCobrar ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            btnSolicitarCancelacionPendiente.Visibility = System.Windows.Visibility.Collapsed;

            ProcesarReservacion();
        }

        private void ProcesarReservacion()
        {
            if (reservacion != null)
            {
                ResumenActual.Noches = 1;
                ResumenActual.NochesExtra = reservacion.Noches - 1;
                ResumenActual.HorasExtra = 0;
                ResumenActual.PersonasExtra = reservacion.PersonasExtra; 
                ResumenActual.Paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Sum(m => m.Cantidad);
                ResumenActual.ValorPaquetes = reservacion.Paquetes * (1 + config.Iva_CatPar);
                ResumenActual.ValorDescuentos = reservacion.Descuento * (1 + config.Iva_CatPar);
                ResumenActual.ValorDescuentosV = 0;
                ResumenActual.Esreservacion = true;

                cbTarifa.IsEnabled = nudHabitaciones.IsEnabled = nudHoraseExtra.IsEnabled = nudNochesExtra.IsEnabled = nudPersonasExtra.IsEnabled = nudPaquetes.IsEnabled = false;

                cbTarifa.SelectedItem = reservacion.ConfiguracionTipo.ConfiguracionTarifa.Tarifa;

                txtFolio.Text = reservacion.CodigoReserva;

                RecargarCantidades();
            }
        }

        private void MostrarParaEditar()
        {
            nudHabitaciones.IsEnabled = txtTarjetaV.IsEnabled = nudPaquetes.IsEnabled = cbTarifa.IsEnabled = false;
            nudPersonasExtra.IsEnabled = nudHoraseExtra.IsEnabled = nudNochesExtra.IsEnabled = chbAPie.IsEnabled = true;

            btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Visible;
            btnVPoints.Visibility = System.Windows.Visibility.Collapsed;
            btnSolicitarCancelacionPendiente.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void MostrarParaCobrar()
        {
            nudHabitaciones.IsEnabled = nudPersonasExtra.IsEnabled = nudHoraseExtra.IsEnabled = nudNochesExtra.IsEnabled = false;
            nudPaquetes.IsEnabled = cbTarifa.IsEnabled = chbAPie.IsEnabled = txtTarjetaV.IsEnabled = false;

            txtTarjetaV.Text = rentaActual.NumeroTarjeta;
            gbDatosEntrada.IsEnabled = false;

            tarjetaCambiable = false;

            btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Visible;
            btnVPoints.Visibility = ResumenActual.Noches == 0 ?
                System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;

            if (rentaActual != null && rentaActual.VentasRenta.Any(m => m.Activo && !m.Cobrada))
                btnSolicitarCancelacionPendiente.Visibility = System.Windows.Visibility.Visible;
            else
                btnSolicitarCancelacionPendiente.Visibility = System.Windows.Visibility.Collapsed;

        }

        private void ActualizarHabitacion(DtoResumenHabitacion habitacion)
        {
            if (habitacion == null)
                throw new SOTException(Textos.Errores.renta_habitacion_nula_exception);

            _habitacion = habitacion;

            lblDatosHabitacion.Text = string.Format(Textos.Etiquetas.RentasForm_InformacionHabitacion, habitacion.NumeroHabitacion, habitacion.NombreTipo);

            var controlador = new ControladorTiposHabitacion();
            var tarifas = controlador.ObtenerTarifasSoportadas(_habitacion.IdTipoHabitacion);

            cbTarifa.ItemsSource = tarifas;

            if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada)
            {
                lblDatosHospedaje.Text = string.Empty;

                MostrarParaCrear();
                ResumenActual.Noches = 1;
            }
            else if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.PreparadaReservada)
            {
                var controladorReservaciones = new ControladorReservaciones();

                reservacion = controladorReservaciones.ObtenerReservacionActual(habitacion.IdHabitacion);
                lblDatosHospedaje.Text = string.Empty;

                MostrarParaCrear();
            }
            else if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada)
            {
                var controladorR = new ControladorRentas();

                rentaActual = controladorR.ObtenerRentaActualPorHabitacion(habitacion.IdHabitacion);
                txtTarjetaV.Text = rentaActual.NumeroTarjeta;


                cbTarifa.SelectedItem = rentaActual.TarifaTmp;

                var fechaFin = rentaActual.FechaFin;

                var tiemposE = rentaActual.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa).ToList();

                if (tiemposE.Count > 0)
                    fechaFin = tiemposE.Max(m=> m.FechaFin);

                TimeSpan tiempo = (fechaFin - rentaActual.FechaInicio);

                lblDatosHospedaje.Text = string.Format(Textos.Etiquetas.RentasForm_InformacionHospedaje, tiempo.ToString("hh\\:mm"));

                var automovil = rentaActual.RentaAutomoviles.FirstOrDefault(m => m.Activa);

                if (automovil != null)
                    AutoActual = automovil;
                else
                    AutoActual = new AutomovilRenta();

                MostrarParaEditar();
            }
            else if (habitacion.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.PendienteCobro)
            {
                var controladorR = new ControladorRentas();

                rentaActual = controladorR.ObtenerRentaActualPorHabitacion(habitacion.IdHabitacion);

                cbTarifa.SelectedItem = rentaActual.TarifaTmp;

                var fechaFin = rentaActual.FechaFin;

                var tiemposE = rentaActual.VentasRenta.Where(m => m.Activo && !m.Cobrada).SelectMany(m => m.Extensiones).Where(m => m.Activa).ToList();

                if (tiemposE.Count > 0)
                    fechaFin = tiemposE.Max(m => m.FechaFin);

                TimeSpan tiempo = (fechaFin - rentaActual.FechaInicio);

                lblDatosHospedaje.Text = string.Format(Textos.Etiquetas.RentasForm_InformacionHospedaje, tiempo.ToString("hh\\:mm"));

                var ventaActual = rentaActual.VentasRenta.First(m => m.Activo && !m.Cobrada);

                if (ventaActual != null && ventaActual.EsInicial)
                    ResumenActual.Noches = 1;

                if (ventaActual != null)
                {
                    ResumenActual.NochesExtra = ventaActual.Extensiones.Count(m => m.Activa && m.EsRenovacion);
                    ResumenActual.HorasExtra = ventaActual.Extensiones.Count(m => m.Activa && !m.EsRenovacion);
                    ResumenActual.Paquetes = ventaActual.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).Sum(m=> m.Cantidad);
                    ResumenActual.PersonasExtra = ventaActual.PersonasExtra.Count(m => m.Activa) / (ResumenActual.NochesExtra + Math.Max(ResumenActual.Noches, 1));
                }

                RecargarCantidades();

                var automovil = rentaActual.RentaAutomoviles.FirstOrDefault(m => m.Activa);

                if (automovil != null)
                    AutoActual = automovil;
                else
                    AutoActual = new AutomovilRenta();

                MostrarParaCobrar();
            }
        }

        private void RentasForm_Load(object sender, RoutedEventArgs e)
        {
            RecargarCantidades();

            if (rentaActual != null && rentaActual.Estado == Modelo.Entidades.Renta.Estados.PendienteCobro)
            {

                var auto = rentaActual.RentaAutomoviles.FirstOrDefault(m => m.Activa);

                if (auto != null)
                    AutoActual = rentaActual.RentaAutomoviles.First(m => m.Activa);
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (rentaActual == null)
                CrearRenta();
            else if (rentaActual.Estado == Modelo.Entidades.Renta.Estados.EnCurso)
                ModificarRenta();
            else if (rentaActual.Estado == Modelo.Entidades.Renta.Estados.PendienteCobro)
                CobrarRenta();
        }

        private void RecargarCantidades()
        {
            if (!IsLoaded)
                return;


            var ventaActual = rentaActual != null ? rentaActual.VentasRenta.FirstOrDefault(m => m.Activo && !m.Cobrada) : null;

            if (ventaActual != null)
            {
                ResumenActual.TarifaHabitacion = rentaActual.PrecioHabitacion * (1 + config.Iva_CatPar);
                ResumenActual.TarifaPersonasExtra = (ventaActual.PersonasExtra.Any(m => m.Activa) ? ventaActual.PersonasExtra.First(m => m.Activa).Precio : 0) * (1 + config.Iva_CatPar);
                ResumenActual.TarifaHorasExtra = (ventaActual.Extensiones.Any(m => m.Activa && !m.EsRenovacion) ? ventaActual.Extensiones.First(m => m.Activa && !m.EsRenovacion).Precio : 0) * (1 + config.Iva_CatPar);
                ResumenActual.Noches = ventaActual.EsInicial ? 1 : 0;
                ResumenActual.NochesExtra = ventaActual.Extensiones.Count(m => m.Activa && m.EsRenovacion);
                ResumenActual.HorasExtra = ventaActual.Extensiones.Count(m => m.Activa && !m.EsRenovacion);
                //ResumenActual.PersonasExtra = ventaActual.PersonasExtra.Count(m=> m.Activa);
                ResumenActual.Paquetes = ventaActual.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).Sum(m => m.Cantidad);
                ResumenActual.ValorPaquetes = (ventaActual.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).Sum(m => Math.Round(m.Precio * (1 + config.Iva_CatPar), 2) * m.Cantidad));


                //{
                    //ResumenActual.NochesExtra = ventaActual.Extensiones.Count(m => m.Activa && m.EsRenovacion);
                    //ResumenActual.HorasExtra = ventaActual.Extensiones.Count(m => m.Activa && !m.EsRenovacion);
                    //ResumenActual.Paquetes = ventaActual.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).Sum(m => m.Cantidad);
                    ResumenActual.PersonasExtra = ventaActual.PersonasExtra.Count(m => m.Activa) / (ResumenActual.NochesExtra + Math.Max(ResumenActual.Noches, 1));
                //}
                
                ResumenActual.ValorDescuentos = (ventaActual.PaquetesRenta.Where(m => m.Activo && m.Descuento > 0).Sum(m => Math.Round(m.Descuento * (1 + config.Iva_CatPar), 2) * m.Cantidad));
                ResumenActual.ValorDescuentosV = 0;

                ResumenActual.ValorDescuentosV = (datosT != null && datosT.Descontar) ? Math.Min(datosT.Saldo, ResumenActual.Total) : 0;
            }
            else if (reservacion != null)
            {
                var valorNoche = reservacion.TotalHabitacion;


                ResumenActual.TarifaHabitacion = valorNoche * (1 + config.Iva_CatPar);
                ResumenActual.TarifaPersonasExtra = (reservacion.PersonasExtra != 0 ? reservacion.TotalPersonasExtra / (reservacion.PersonasExtra * reservacion.Noches) : 0) * (1 + config.Iva_CatPar);
                ResumenActual.TarifaHorasExtra = 0;//precioHExtra * (1 + config.Iva_CatPar);
                ResumenActual.Noches = 1;
                ResumenActual.NochesExtra = reservacion.Noches - 1;
                ResumenActual.HorasExtra = 0;
                ResumenActual.PersonasExtra = reservacion.PersonasExtra;
                ResumenActual.Paquetes = reservacion.PaquetesReservaciones.Where(m => m.Activo && m.Precio > 0).Sum(m => m.Cantidad);
                ResumenActual.ValorPaquetes = reservacion.Paquetes * (1 + config.Iva_CatPar);
                ResumenActual.ValorDescuentos = reservacion.Descuento * (1 + config.Iva_CatPar);
                ResumenActual.ValorDescuentosV = 0;
            }
            else if (configuracionTipo != null)
            {
                ResumenActual.TarifaHabitacion = configuracionTipo.Precio * (1 + config.Iva_CatPar);
                ResumenActual.TarifaPersonasExtra = configuracionTipo.PrecioPersonaExtra * (1 + config.Iva_CatPar);
                ResumenActual.TarifaHorasExtra = configuracionTipo.PrecioHospedajeExtra * (1 + config.Iva_CatPar);
                ResumenActual.Noches = rentaActual != null && rentaActual.Estado == Renta.Estados.EnCurso ? 0 : 1;
                //ResumenActual.NochesExtra = 0;
                //ResumenActual.HorasExtra = 0;
                //ResumenActual.PersonasExtra = reservacion.PersonasExtra;
                ResumenActual.Paquetes = 0;
                ResumenActual.ValorPaquetes = 0;
                ResumenActual.ValorDescuentos = 0;
                ResumenActual.ValorDescuentosV = 0;

                ResumenActual.ValorDescuentosV = (datosT != null && datosT.Descontar) ? Math.Min(datosT.Saldo, ResumenActual.Total) : 0;
            }
            else
            {
                ResumenActual.TarifaHabitacion = 0;
                ResumenActual.TarifaPersonasExtra = 0;
                ResumenActual.TarifaHorasExtra = 0;
            }
        }

        private void btnCancelarEntrada_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(string.Format(Textos.Mensajes.cancelar_preparacion, _habitacion.NumeroHabitacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorHabitaciones();
                controlador.CancelarPreparacionHabitacion(_habitacion.IdHabitacion);
                Close();
            }
        }

        private void CrearRenta()
        {
            if (configuracionTipo == null)
                throw new SOTException(Textos.Errores.tarifa_no_seleccionada_exception);

            if (_habitacion == null || (_habitacion.EstadoHabitacion != Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada &&
                                        _habitacion.EstadoHabitacion != Modelo.Entidades.Habitacion.EstadosHabitacion.PreparadaReservada))
                return;

            ValidarDatosAutomovil();

            var controladorRM = new ControladorReportesMatriculas();

            if (AutoActual.Activa && controladorRM.PoseeReportes(AutoActual.Matricula))
            {
                var reporteadorMatriculas = new ReportesMatriculaForm(AutoActual.Matricula);
                Utilidades.Dialogos.MostrarDialogos(this, reporteadorMatriculas);

                if (MessageBox.Show(this, Textos.Mensajes.continuar_proceso_renta, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;
            }

            var rentaNueva = new Modelo.Entidades.Renta
            {
                Activa = true,
                IdConfiguracionTipo = configuracionTipo.Id,
            };

            if (datosT != null)
                rentaNueva.NumeroTarjeta = datosT.NumeroTarjeta;

            rentaNueva.IdHabitacion = _habitacion.IdHabitacion;
            rentaNueva.PrecioHabitacion = ResumenActual.TarifaHabitacion / (1 + config.Iva_CatPar);//precioHotel;

            if (reservacion != null)
                rentaNueva.IdDatosFiscales = reservacion.IdDatosFiscales;

            var ventaRentaNueva = new Modelo.Entidades.VentaRenta { Activo = true };
            rentaNueva.VentasRenta.Add(ventaRentaNueva);

            int orden = 1;

            for (int i = 1; i <= ResumenActual.PersonasExtra; i++)
            {
                ventaRentaNueva.PersonasExtra.Add(new Modelo.Entidades.PersonaExtra
                {
                    Activa = true,
                    Precio = ResumenActual.TarifaPersonasExtra / (1 + config.Iva_CatPar),
                    FolioRenovacion = "",
                });
            }

            for (int i = 0; i < ResumenActual.NochesExtra; i++)
            {
                ventaRentaNueva.Extensiones.Add(new Modelo.Entidades.Extension
                {
                    Activa = true,
                    EsRenovacion = true,
                    Precio = ResumenActual.TarifaHabitacion / (1 + config.Iva_CatPar),//precioHotel,
                    Folio = Guid.NewGuid().ToString(),
                    Orden = orden++
                });

                for (int j = 1; j <= ResumenActual.PersonasExtra; j++)
                {
                    ventaRentaNueva.PersonasExtra.Add(new Modelo.Entidades.PersonaExtra
                    {
                        Activa = true,
                        Precio = ResumenActual.TarifaPersonasExtra / (1 + config.Iva_CatPar),//precioPExtra,
                        FolioRenovacion = ventaRentaNueva.Extensiones.Last(m => m.EsRenovacion).Folio
                    });
                }
            }

            for (int i = 0; i < ResumenActual.HorasExtra; i++)
            {
                ventaRentaNueva.Extensiones.Add(new Modelo.Entidades.Extension
                {
                    Activa = true,
                    Precio = ResumenActual.TarifaHorasExtra / (1 + config.Iva_CatPar),
                    Orden = orden++
                });
            }

            if (reservacion != null)
            {
                foreach (var paquete in reservacion.PaquetesReservaciones.Where(m => m.Activo))
                    ventaRentaNueva.PaquetesRenta.Add(new Modelo.Entidades.PaqueteRenta
                    {
                        Activo = true,
                        Descuento = paquete.Descuento,
                        IdPaquete = paquete.IdPaquete,
                        Precio = paquete.Precio,
                        Cantidad = paquete.Cantidad,
                    });
            }

            ConsumoInternoHabitacion ci = null;
            Propina propina = null;
            decimal valorConIVAmnr = 0;

            if (puedeCobrar || reservacion != null)
            {
                if (reservacion == null && ResumenActual.Total > 0)//descuentoV < valorSinIva)
                {
                    //var vSinIVA = (valorSinIva - descuentoV);
                    //var vIVA = vSinIVA * config.Iva_CatPar;
                    var vConIVA = ResumenActual.Total;//vSinIVA + vIVA;

                    var selectorFormaPago = new SelectorFormaPagoForm(vConIVA, false, true, true);

                    Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                    if (!selectorFormaPago.ProcesoExitoso)
                        return;

                    foreach (var pagoSeleccionado in selectorFormaPago.FormasPagoSeleccionadas)
                    {
                        ventaRentaNueva.Pagos.Add(new Modelo.Entidades.PagoRenta
                        {
                            Activo = true,
                            TipoPago = pagoSeleccionado.TipoPago,
                            Referencia = pagoSeleccionado.Referencia,
                            Valor = pagoSeleccionado.Valor,
                            TipoTarjeta = pagoSeleccionado.TipoTarjeta,
                            NumeroTarjeta = pagoSeleccionado.NumeroTarjeta,
                        });
                    }

                    ci = selectorFormaPago.DatosConsumo;
                    propina = selectorFormaPago.PropinaGenerada;
                }

                if (reservacion != null)
                {
                    ventaRentaNueva.Pagos.Add(new Modelo.Entidades.PagoRenta
                    {
                        Activo = true,
                        TipoPago = TiposPago.Reservacion,
                        Referencia = reservacion.CodigoReserva,
                        Valor = reservacion.ValorConIVA + (reservacion.MontosNoReembolsables.Sum(m => m.ValorConIVA))//ResumenActual.Total//valorSinIva * (1 + config.Iva_CatPar)
                    });

                    if (reservacion.MontosNoReembolsables.Any(m => m.Activo))
                    {
                        valorConIVAmnr = reservacion.MontosNoReembolsables.Sum(m => m.ValorConIVA);
                        var valorSinIVAmnr = valorConIVAmnr / (1 + config.Iva_CatPar);

                        ventaRentaNueva.MontosNoReembolsablesRenta.Add(new MontoNoReembolsableRenta
                        {
                            Activo = true,
                            ValorConIVA = valorConIVAmnr,
                            ValorSinIVA = valorSinIVAmnr,
                            ValorIVA = valorConIVAmnr - valorSinIVAmnr
                        });
                    }
                }
                else if (ResumenActual.ValorDescuentosV > 0)//(descuentoV > 0)
                    ventaRentaNueva.Pagos.Add(new Modelo.Entidades.PagoRenta
                    {
                        Activo = true,
                        TipoPago = TiposPago.VPoints,
                        Referencia = datosT.NumeroTarjeta.Trim(),
                        Valor = ResumenActual.ValorDescuentosV//descuentoVConIVA
                    });
            }

            if (AutoActual.Activa)
                rentaNueva.RentaAutomoviles.Add(AutoActual);

            ventaRentaNueva.ValorConIVA = ResumenActual.SubTotal + valorConIVAmnr;//ventaRentaNueva.ValorSinIVA + ventaRentaNueva.ValorIVA;
            ventaRentaNueva.ValorSinIVA = ventaRentaNueva.ValorConIVA / (1 + config.Iva_CatPar);//valorSinIva;
            ventaRentaNueva.ValorIVA = ventaRentaNueva.ValorConIVA - ventaRentaNueva.ValorSinIVA;//ventaRentaNueva.ValorConIVA * config.Iva_CatPar;//ventaRentaNueva.ValorSinIVA * config.Iva_CatPar;

            var controlador = new ControladorRentas();

            if (puedeCobrar || reservacion != null)
                controlador.CrearYPagarRenta(rentaNueva, propina, ResumenActual.PersonasExtra > configuracionTipo.TipoHabitacion.MaximoPersonasExtra, ci);
            else
                controlador.CrearRenta(rentaNueva, ResumenActual.PersonasExtra > configuracionTipo.TipoHabitacion.MaximoPersonasExtra);

            MessageBox.Show(Textos.Mensajes.renta_agregada_con_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void ValidarDatosAutomovil()
        {
            if (AutoActual.Activa)
            {
                if (string.IsNullOrWhiteSpace(AutoActual.Matricula))
                    throw new SOTException(Textos.Errores.matricula_invalida_exception);

                if (string.IsNullOrWhiteSpace(AutoActual.Marca))
                    throw new SOTException(Textos.Errores.marca_invalida_exception);

                if (string.IsNullOrWhiteSpace(AutoActual.Modelo))
                    throw new SOTException(Textos.Errores.modelo_invalido_exception);

                if (string.IsNullOrWhiteSpace(AutoActual.Color))
                    throw new SOTException(Textos.Errores.color_invalido_exception);
            }
        }

        private void ModificarRenta()
        {
            if (configuracionTipo == null)
                throw new SOTException(Textos.Errores.tarifa_no_seleccionada_exception);

            ValidarDatosAutomovil();

            var controladorR = new ControladorRentas();

            /*
            var pagos = new List<Modelo.Dtos.DtoInformacionPago>();
            ConsumoInternoHabitacion consumo = null;

            if (ResumenActual.Total > 0)
            {
                //var vIVA = valor * config.Iva_CatPar;
                //var vConIVA = valor + vIVA;

                var selectorFormaPago = new SelectorFormaPagoForm(0, 0, ResumenActual.Total, 0, true, true, true);

                Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                if (!selectorFormaPago.ProcesoExitoso)
                    return;

                pagos = selectorFormaPago.FormasPagoSeleccionadas;
                consumo = selectorFormaPago.DatosConsumo;
            }*/

            var autos = new List<Modelo.Dtos.DtoAutomovil>();

            if (AutoActual.Activa)
            {
                autos.Add(new Modelo.Dtos.DtoAutomovil
                {
                    Color = AutoActual.Color,
                    Marca = AutoActual.Marca,
                    Matricula = AutoActual.Matricula,
                    Modelo = AutoActual.Modelo
                });
            }

            //controladorR.ActualizarDatosHabitacion(rentaActual.Id, ResumenActual.HorasExtra, ResumenActual.NochesExtra, 
            //    ResumenActual.PersonasExtra, autos, pagos, consumo, ResumenActual.PersonasExtra > configuracionTipo.TipoHabitacion.MaximoPersonasExtra ||
            //    ResumenActual.PersonasExtra + controladorR.ObtenerCantidadPersonasExtraUltimaRenovacion(rentaActual.Id) > configuracionTipo.TipoHabitacion.MaximoPersonasExtra);

            controladorR.ActualizarDatosHabitacionV2(rentaActual.Id, ResumenActual.HorasExtra, ResumenActual.NochesExtra,
                ResumenActual.PersonasExtra, autos, ResumenActual.PersonasExtra > configuracionTipo.TipoHabitacion.MaximoPersonasExtra ||
                ResumenActual.PersonasExtra + controladorR.ObtenerCantidadPersonasExtraUltimaRenovacion(rentaActual.Id) > configuracionTipo.TipoHabitacion.MaximoPersonasExtra);

            MessageBox.Show(Textos.Mensajes.datos_renta_actualizados_con_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void CobrarRenta()
        {
            ValidarDatosAutomovil();

            var controladorR = new ControladorRentas();

            var valorSinIVA = rentaActual.VentasRenta.First(m => m.Activo).ValorSinIVA;
            var valorIVA = rentaActual.VentasRenta.First(m => m.Activo).ValorIVA;
            var valorConIVA = rentaActual.VentasRenta.First(m => m.Activo).ValorConIVA;

            var pagos = new List<Modelo.Dtos.DtoInformacionPago>();

            Propina propina = null;
            ConsumoInternoHabitacion consumo = null;
            Empleado mesero = null;

            if (ResumenActual.Total > 0)
            {
                var selectorFormaPago = new SelectorFormaPagoForm(ResumenActual.Total, false, true, true);

                Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                if (!selectorFormaPago.ProcesoExitoso)
                    return;

                pagos = selectorFormaPago.FormasPagoSeleccionadas;
                propina = selectorFormaPago.PropinaGenerada;
                consumo = selectorFormaPago.DatosConsumo;

                if (propina != null)
                {
                    if (ResumenActual.Noches == 0)
                    {
                        var selectorMesero = new Empleados.SeleccionadorMeserosForm();

                        Utilidades.Dialogos.MostrarDialogos(this, selectorMesero);

                        if (!selectorMesero.ProcesoExitoso || (mesero = selectorMesero.MeseroSeleccionado) == null)
                            throw new SOTException("Operación cancelada");
                    }
                    //else
                    //{
                    //    var selectorMesero = new Empleados.SeleccionadorValetsForm();

                    //    Utilidades.Dialogos.MostrarDialogos(this, selectorMesero);

                    //    if (!selectorMesero.ProcesoExitoso || (mesero = selectorMesero.ValetSeleccionado) == null)
                    //        throw new SOTException("Operación cancelada");
                    //}
                }
            }

            if (ResumenActual.ValorDescuentosV > 0)//(descuentoV > 0)
                pagos.Add(new Modelo.Dtos.DtoInformacionPago
                {
                    TipoPago = TiposPago.VPoints,
                    Referencia = datosT.NumeroTarjeta.Trim(),
                    Valor = ResumenActual.ValorDescuentosV//descuentoVConIVA
                });

            //controladorR.PagarRenta(rentaActual.Id, pagos, propina, consumo);
            controladorR.PagarRentaV2(rentaActual.Id, pagos, consumo, propina, mesero != null ? (int?)mesero.Id : null);

            MessageBox.Show(Textos.Mensajes.renta_cobrada_con_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void btnSolicitarCancelacion_Click(object sender, RoutedEventArgs e)
        {
            if (rentaActual != null && MessageBox.Show(Textos.Mensajes.confirmar_cancelacion_renta, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorHabitaciones();
                controlador.FinalizarOcupacion(rentaActual.IdHabitacion, true);

                Close();

                MessageBox.Show(Textos.Mensajes.cancelacion_renta_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnVPoints_Click(object sender, RoutedEventArgs e)
        {
            var consultaTarjetaVF = new ConsultaTarjetaVForm(tarjetaCambiable, rentaActual != null ? rentaActual.NumeroTarjeta : null);
            Utilidades.Dialogos.MostrarDialogos(this, consultaTarjetaVF);

            datosT = consultaTarjetaVF.DatosT;

            if (datosT != null)
            {
                datosT.Descontar = reservacion == null && puedeCobrar && MessageBox.Show(Textos.Acciones.UsarVPoint, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;

                txtTarjetaV.Text = datosT.NumeroTarjeta;
                txtVpoints.Text = datosT.Saldo.ToString("C");

                txtVpoints.Visibility = System.Windows.Visibility.Visible;
                lblVPoints.Visibility = System.Windows.Visibility.Visible;
            }
            else 
            {
                txtTarjetaV.Text = string.Empty;

                txtVpoints.Visibility = System.Windows.Visibility.Hidden;
                lblVPoints.Visibility = System.Windows.Visibility.Hidden;
            }

            RecargarCantidades();
        }

        private void cbTarifa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var controlador = new ControladorConfiguracionesTipo();

            if (cbTarifa.SelectedIndex >= 0)
            {
                Modelo.Entidades.ConfiguracionTarifa.Tarifas tarifaElegida = (Modelo.Entidades.ConfiguracionTarifa.Tarifas)cbTarifa.SelectedValue;

                configuracionTipo = controlador.ObtenerConfiguracionTipo(_habitacion.IdTipoHabitacion, tarifaElegida);
            }
            else
                configuracionTipo = null;

            nudHoraseExtra.IsEnabled = nudNochesExtra.IsEnabled = reservacion == null;
            /*
            if (nudHospedajeExtra.IsEnabled && (configuracionTipo == null || configuracionTipo.ConfiguracionTarifa.RangoFijo))
                nudHospedajeExtra.Maximum = int.MaxValue;
            else if (nudHospedajeExtra.IsEnabled)
                nudHospedajeExtra.Maximum = configuracionTipo.TiemposHospedaje.Count(m => m.Activo);
            */
            RecargarCantidades();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSolicitarCancelacionPendiente_Click(object sender, RoutedEventArgs e)
        {
            var ventaActual = rentaActual != null ? rentaActual.VentasRenta.FirstOrDefault(m => m.Activo && !m.Cobrada) : null;

            if (ventaActual == null)
                throw new SOTException(Textos.Errores.no_venta_pendiente_exception);

            var controlador = new ControladorRentas();
            controlador.CancelarVentaPendiente(ventaActual.Id);

            Close();
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (IsLoaded && chbAPie.IsChecked == false && !string.IsNullOrWhiteSpace(AutoActual.Matricula))
            {
                var controladorR = new SOTControladores.Controladores.ControladorRentas();

                var auto = controladorR.ObtenerDatosAutomovil(AutoActual.Matricula);

                if (auto != null)
                {
                    AutoActual.Matricula = auto.Matricula;
                    AutoActual.Marca = auto.Marca;
                    AutoActual.Modelo = auto.Modelo;
                    AutoActual.Color = auto.Color;
                }
            }
        }

        private void nudNochesExtra_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!IsLoaded)
                return;

            ActualizaResumenActual();
        }

        private void nudPersonasExtra_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!IsLoaded)
                return;

            ActualizaResumenActual();
        }

        private void ActualizaResumenActual()
        {
            if (ResumenActual.NochesExtra > 0)
            {
                if (rentaActual == null)
                    ResumenActual.TotalPersonasExtra = ResumenActual.TarifaPersonasExtra * ResumenActual.PersonasExtra * (ResumenActual.NochesExtra + Math.Max(1, ResumenActual.Noches));

            }
        }

        //private void nudHabitaciones_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    var nud = sender as MahApps.Metro.Controls.NumericUpDown;

        //    if (nud != null && !nud.Value.HasValue)
        //        nud.Value = 0;
        //    else
        //        RecargarCantidades();
        //}
    }
}
