﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Rentas
{
    internal class ResumenRenta: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        internal void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        decimal _tarifaHabitacion, _tarifaHorasExtra, _tarifaPersonasExtra,
                _valorPaquetes, _valorDescuentos, _valorDescuentosV,
                _totalPersonasExtra, _totalHorasExtra, _totalNoches, _totalNochesExtra, _subTotal, _total;

        int _noches, _horasExtra, _personasExtra, _nochesExtra, _paquetes;

        bool _Reservacion;

        public bool Esreservacion
        {
            get
            {
                return _Reservacion;
            }
            set
            {
                _Reservacion = value;
                NotifyPropertyChanged();
                CalcularPE();
            }
        }

        public decimal TarifaHabitacion
        {
            get
            {
                return _tarifaHabitacion;
            }
            set
            {
                _tarifaHabitacion = Math.Round(value, 2);
                NotifyPropertyChanged();

                CalcularNE();
                CalcularN();
            }
        }

        public int Noches
        {
            get
            {
                return _noches;
            }
            set
            {
                _noches = value;
                NotifyPropertyChanged();

                CalcularN();
                CalcularPE();
            }
        }

        public decimal TotalNoches
        {
            get
            {
                return _totalNoches;
            }
            private set
            {
                _totalNoches = value;
                NotifyPropertyChanged();
                CalcularTotal();
            }
        }

        public int NochesExtra
        {
            get
            {
                return _nochesExtra;
            }
            set
            {
                _nochesExtra = value;
                NotifyPropertyChanged();

                CalcularNE(); 
                CalcularPE();
            }
        }

        public decimal TotalNochesExtra
        {
            get
            {
                return _totalNochesExtra;
            }
            private set
            {
                _totalNochesExtra = value;
                NotifyPropertyChanged();
                CalcularTotal();
            }
        }

        #region horas extra

        public decimal TarifaHorasExtra
        {
            get
            {
                return _tarifaHorasExtra;
            }
            set
            {
                _tarifaHorasExtra = Math.Round(value, 2);
                NotifyPropertyChanged();

                CalcularHE();
            }
        }

        public int HorasExtra
        {
            get
            {
                return _horasExtra;
            }
            set
            {
                _horasExtra = value;
                NotifyPropertyChanged();

                CalcularHE();
            }
        }

        public decimal TotalHorasExtra
        {
            get { return _totalHorasExtra; }
            private set
            {
                _totalHorasExtra = value;
                NotifyPropertyChanged();
                CalcularTotal();
            }
        }

        #endregion
        #region personas extra

        public decimal TarifaPersonasExtra
        {
            get
            {
                return _tarifaPersonasExtra;
            }
            set
            {
                _tarifaPersonasExtra = Math.Round(value, 2);

                NotifyPropertyChanged();

                CalcularPE();
            }
        }

        public int PersonasExtra
        {
            get
            {
                return _personasExtra;
            }
            set
            {
                _personasExtra = value;
                NotifyPropertyChanged();

                CalcularPE();
            }
        }

        public decimal TotalPersonasExtra 
        {
            get { return _totalPersonasExtra; }
             set
            {
                _totalPersonasExtra = value;
                NotifyPropertyChanged();
                CalcularTotal();
            }
        }

        #endregion
        #region paquetes y descuentos

        public int Paquetes
        {
            get
            {
                return _paquetes;
            }
            set
            {
                _paquetes = value;
                NotifyPropertyChanged();
            }
        }

        public decimal ValorPaquetes
        {
            get
            {
                return _valorPaquetes;
            }
            set
            {
                _valorPaquetes = Math.Round(value, 2);
                NotifyPropertyChanged();
                CalcularTotal();
            }
        }

        public decimal ValorDescuentos
        {
            get
            {
                return _valorDescuentos;
            }
            set
            {
                _valorDescuentos = Math.Round(value, 2);
                NotifyPropertyChanged();
                CalcularTotal();
            }
        }

        public decimal ValorDescuentosV
        {
            get
            {
                return _valorDescuentosV;
            }
            set
            {
                if (_valorDescuentosV != Math.Round(value, 2))
                {
                    _valorDescuentosV = Math.Round(value, 2);
                    NotifyPropertyChanged();
                    CalcularTotal();
                }
            }
        }

        #endregion

        public decimal Total
        {
            get
            {
                return _total;
            }
            private set
            {
                _total = value;
                NotifyPropertyChanged();
            }
        }
        public decimal SubTotal
        {
            get
            {
                return _subTotal;
            }
            private set
            {
                _subTotal = value;
                NotifyPropertyChanged();
            }
        }

        private void CalcularPE()
        {
            if(Esreservacion)
            TotalPersonasExtra = TarifaPersonasExtra * PersonasExtra * (NochesExtra + Math.Max(1, Noches));
            else
            TotalPersonasExtra = TarifaPersonasExtra * PersonasExtra;
        }
        private void CalcularHE()
        {
            TotalHorasExtra = TarifaHorasExtra * HorasExtra;
        }
        private void CalcularNE()
        {
            TotalNochesExtra = TarifaHabitacion * NochesExtra;
        }
        private void CalcularN()
        {
            TotalNoches = TarifaHabitacion * Noches;
        }

        private void CalcularTotal()
        {
            SubTotal = TotalNoches + TotalNochesExtra + TotalHorasExtra + TotalPersonasExtra
                     + ValorPaquetes - ValorDescuentos;

            Total = SubTotal - ValorDescuentosV;
        }
    }
}
