﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTControladores.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.ConceptosMantenimiento
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionConceptosMantenimientoForm.xaml
    /// </summary>
    public partial class ConfiguracionConceptosMantenimientoForm : Window
    {


        ObservableCollection<CheckableItemWrapper<ConceptoMantenimiento.Clasificaciones>> clasificaciones;

        ItemWrapper<ConceptoMantenimiento> cmWrapper;

        public ConfiguracionConceptosMantenimientoForm()
        {
            InitializeComponent();

            clasificaciones = new ObservableCollection<CheckableItemWrapper<ConceptoMantenimiento.Clasificaciones>>();

            cmWrapper = new ItemWrapper<ConceptoMantenimiento>();

            DataContext = cmWrapper;
        }

        private bool esNuevo;

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as CheckableItemWrapper<ConceptoMantenimiento.Clasificaciones>;

            foreach (var item in clasificaciones)
            {
                if (item == marcado)
                    continue;

                item.IsChecked = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
            CargarConceptos();
            CargarTipos();
        }

        private void CargarConceptos()
        {
            var controlador = new ControladorConceptosMantenimiento();

            var conceptos = controlador.ObtenerConceptos();

            dgvConceptos.ItemsSource = conceptos;
        }

        private void CargarTipos()
        {

            clasificaciones.Clear();

            var controladorConceptos = new ControladorConceptosMantenimiento();

            foreach (var clasificacion in controladorConceptos.ObtenerClasificaciones())
            {
                clasificaciones.Add(new CheckableItemWrapper<ConceptoMantenimiento.Clasificaciones> { Value = clasificacion });
            }

            listaMarcable.ItemsSource = clasificaciones;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;

            cmWrapper.Value = new ConceptoMantenimiento();

            foreach (var item in clasificaciones)
            {
                item.IsChecked = false;
            }

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if (dgvConceptos.SelectedItem == null)
                throw new SOTException(Textos.Errores.multiples_conceptos_seleccionados);


            var concepto = dgvConceptos.SelectedItem as Modelo.Entidades.ConceptoMantenimiento;

            cmWrapper.Value = concepto;

            var tipoActual = clasificaciones.FirstOrDefault(m => m.Value == concepto.Clasificacion);

            if (tipoActual != null)
                tipoActual.IsChecked = true;

            esNuevo = false;

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (dgvConceptos.SelectedItem == null)
                throw new SOTException(Textos.Errores.multiples_conceptos_seleccionados);

            var concepto = dgvConceptos.SelectedItem as Modelo.Entidades.ConceptoMantenimiento;

            if (MessageBox.Show(string.Format(Textos.Mensajes.eliminar_concepto, concepto.Concepto, concepto.Clasificacion.Descripcion()), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorConceptosMantenimiento();

                controlador.EliminarConcepto(concepto.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                Window_Loaded(null, null);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
            esNuevo = false;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;

            Window_Loaded(null, null);
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorConceptosMantenimiento();

            var tipoSeleccionado = clasificaciones.FirstOrDefault(m => m.IsChecked);

            if (tipoSeleccionado == null)
                throw new SOTException(Textos.Errores.clasificacion_concepto_mantenimiento_no_seleccionada_exception);

            cmWrapper.Value.Clasificacion = tipoSeleccionado.Value;

            if (esNuevo)
                controlador.CrearConcepto(cmWrapper.Value);

            else
                controlador.ModificarConcepto(cmWrapper.Value);


            Window_Loaded(null, null);

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;

            if (esNuevo)
                MessageBox.Show(Textos.Mensajes.creacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show(Textos.Mensajes.modificacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
