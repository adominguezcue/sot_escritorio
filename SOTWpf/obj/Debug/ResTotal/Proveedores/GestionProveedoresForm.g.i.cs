﻿#pragma checksum "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "813E88AD0DBD123C532767326D3A6AA71A5F1607"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Modelo.Seguridad.Dtos;
using SOTWpf.Componentes.Buttons;
using SOTWpf.Componentes.DataGrid;
using SOTWpf.Nucleo;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.ResTotal.Proveedores {
    
    
    /// <summary>
    /// GestionProveedoresForm
    /// </summary>
    public partial class GestionProveedoresForm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 23 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnEliminarProveedor;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnModificarProveedor;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnAltaProveedor;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnImprimir;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBusqueda;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvProveedores;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/restotal/proveedores/gestionproveedoresform.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 13 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
            ((SOTWpf.ResTotal.Proveedores.GestionProveedoresForm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnEliminarProveedor = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 3:
            this.btnModificarProveedor = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 4:
            this.btnAltaProveedor = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 5:
            this.btnImprimir = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 6:
            this.txtBusqueda = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            
            #line 97 "..\..\..\..\ResTotal\Proveedores\GestionProveedoresForm.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnBuscar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.dgvProveedores = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

