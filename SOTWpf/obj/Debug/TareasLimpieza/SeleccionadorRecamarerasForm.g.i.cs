﻿#pragma checksum "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "460F2FBCFD5873F67B4DE480DA3EDF5253C5A4F4"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using SOTWpf.Nucleo;
using SOTWpf.TareasLimpieza;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.TareasLimpieza {
    
    
    /// <summary>
    /// SeleccionadorRecamarerasForm
    /// </summary>
    public partial class SeleccionadorRecamarerasForm : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 43 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtDatoHabitacion;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox panelRecamarerasDisponibles;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl flpDisponibles;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox panelRecamarerasSeleccionadas;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ItemsControl flpSeleccionadas;
        
        #line default
        #line hidden
        
        
        #line 229 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton tglbTipo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/tareaslimpieza/seleccionadorrecamarerasform.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 17 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            ((SOTWpf.TareasLimpieza.SeleccionadorRecamarerasForm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.txtDatoHabitacion = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.panelRecamarerasDisponibles = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 4:
            this.flpDisponibles = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 6:
            this.panelRecamarerasSeleccionadas = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 7:
            this.flpSeleccionadas = ((System.Windows.Controls.ItemsControl)(target));
            return;
            case 9:
            
            #line 213 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 225 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 11:
            this.tglbTipo = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 238 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            this.tglbTipo.Checked += new System.Windows.RoutedEventHandler(this.tglbTipo_Checked);
            
            #line default
            #line hidden
            
            #line 239 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            this.tglbTipo.Unchecked += new System.Windows.RoutedEventHandler(this.tglbTipo_Unchecked);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 5:
            
            #line 83 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CambiarPadre);
            
            #line default
            #line hidden
            break;
            case 8:
            
            #line 154 "..\..\..\TareasLimpieza\SeleccionadorRecamarerasForm.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.CambiarPadre);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

