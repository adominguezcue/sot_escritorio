﻿#pragma checksum "..\..\..\..\Inventarios\KardexForm.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "57E31F7675ED65C7D35C0FD791F1FAC147F6175BAA2B853FA6F1E644AB2FB478"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using SOTWpf.Componentes.Buttons;
using SOTWpf.CustomValueConverters;
using SOTWpf.Nucleo;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.Inventarios {
    
    
    /// <summary>
    /// KardexForm
    /// </summary>
    public partial class KardexForm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 23 "..\..\..\..\Inventarios\KardexForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnReporteArticulosVendidos;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\Inventarios\KardexForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PanelControl;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\..\Inventarios\KardexForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFechaInicial;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\Inventarios\KardexForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFechaFinal;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\..\Inventarios\KardexForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtArticulo;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\Inventarios\KardexForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbAlmacenes;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/inventarios/kardexform.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Inventarios\KardexForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\..\..\Inventarios\KardexForm.xaml"
            ((SOTWpf.Inventarios.KardexForm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnReporteArticulosVendidos = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 3:
            this.PanelControl = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.dpFechaInicial = ((System.Windows.Controls.DatePicker)(target));
            
            #line 59 "..\..\..\..\Inventarios\KardexForm.xaml"
            this.dpFechaInicial.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dpFechaInicial_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.dpFechaFinal = ((System.Windows.Controls.DatePicker)(target));
            
            #line 68 "..\..\..\..\Inventarios\KardexForm.xaml"
            this.dpFechaFinal.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dpFechaFinal_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.txtArticulo = ((System.Windows.Controls.TextBox)(target));
            
            #line 77 "..\..\..\..\Inventarios\KardexForm.xaml"
            this.txtArticulo.KeyDown += new System.Windows.Input.KeyEventHandler(this.txtArticulo_KeyDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.cbAlmacenes = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

