﻿#pragma checksum "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "FDB21E3E06C668FAB6B6283BE5971C58C65DF8AD27EEC87018666F2AE315D2CC"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using CurrencyTextBoxControl;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using SOTWpf.CustomValidationRules;
using SOTWpf.Nucleo;
using SOTWpf.TareasLimpieza;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.TareasMantenimiento {
    
    
    /// <summary>
    /// EdicionTareaMantenimiento
    /// </summary>
    public partial class EdicionTareaMantenimiento : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 32 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbHabitaciones;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbConceptosMantenimiento;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbTiposMantenimiento;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFechaInicio;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chbRepetir;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbPeriodicidades;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtLapso;
        
        #line default
        #line hidden
        
        
        #line 153 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFechaFin;
        
        #line default
        #line hidden
        
        
        #line 201 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CurrencyTextBoxControl.CurrencyTextBox nudContadorTiempo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/tareasmantenimiento/ediciontareamantenimiento.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 13 "..\..\..\..\TareasMantenimiento\EdicionTareaMantenimiento.xaml"
            ((SOTWpf.TareasMantenimiento.EdicionTareaMantenimiento)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.cbHabitaciones = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.cbConceptosMantenimiento = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.cbTiposMantenimiento = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.dpFechaInicio = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 6:
            this.chbRepetir = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 7:
            this.cbPeriodicidades = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.txtLapso = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.dpFechaFin = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 10:
            this.nudContadorTiempo = ((CurrencyTextBoxControl.CurrencyTextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

