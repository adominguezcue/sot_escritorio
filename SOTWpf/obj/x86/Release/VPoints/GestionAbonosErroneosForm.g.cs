﻿#pragma checksum "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "605172C1D8670C2E1831D700F090AA5150D440A3E85EB29A603B9ECDAD6D6D4A"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Modelo.Entidades;
using SOTWpf.Componentes.Buttons;
using SOTWpf.Nucleo;
using SOTWpf.VPoints;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.VPoints {
    
    
    /// <summary>
    /// GestionAbonosErroneosForm
    /// </summary>
    public partial class GestionAbonosErroneosForm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 38 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnAbonar;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel PanelControl;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbHabitaciones;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFechaInicial;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFechaFinal;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvAbonos;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/vpoints/gestionabonoserroneosform.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 19 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
            ((SOTWpf.VPoints.GestionAbonosErroneosForm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnAbonar = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 3:
            this.PanelControl = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 4:
            this.cbHabitaciones = ((System.Windows.Controls.ComboBox)(target));
            
            #line 76 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
            this.cbHabitaciones.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cb_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.dpFechaInicial = ((System.Windows.Controls.DatePicker)(target));
            
            #line 86 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
            this.dpFechaInicial.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dpFechaInicial_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.dpFechaFinal = ((System.Windows.Controls.DatePicker)(target));
            
            #line 94 "..\..\..\..\VPoints\GestionAbonosErroneosForm.xaml"
            this.dpFechaFinal.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dpFechaFinal_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.dgvAbonos = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

