﻿#pragma checksum "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "39D7630874A9744621FC11A6D0CEAFCDA01C14EAA4E2AE1AA6540A127FB83F13"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using Modelo.Entidades;
using SOTWpf.Componentes.NumericFields;
using SOTWpf.CustomValueConverters;
using SOTWpf.Nucleo;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.SolicitudesPermisosFaltas {
    
    
    /// <summary>
    /// EdicionSolicitudFaltaForm
    /// </summary>
    public partial class EdicionSolicitudFaltaForm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 45 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbEmpleados;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtArea;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbSuplente;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFechaAplicacion;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbFormasPagos;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/solicitudespermisosfaltas/edicionsolicitudfaltaform.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 19 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
            ((SOTWpf.SolicitudesPermisosFaltas.EdicionSolicitudFaltaForm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.cbEmpleados = ((System.Windows.Controls.ComboBox)(target));
            
            #line 54 "..\..\..\..\SolicitudesPermisosFaltas\EdicionSolicitudFaltaForm.xaml"
            this.cbEmpleados.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.txtArea = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.cbSuplente = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.dpFechaAplicacion = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 6:
            this.cbFormasPagos = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

