﻿#pragma checksum "..\..\..\Pagos\SelectorFormaPagoForm.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "09BD126769D840ABE18B8D414BCB4B31B0D1672202CAF3E23ADB12CE08043317"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using SOTWpf.Nucleo;
using SOTWpf.Pagos.FormulariosTipos;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.Pagos {
    
    
    /// <summary>
    /// SelectorFormaPagoForm
    /// </summary>
    public partial class SelectorFormaPagoForm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblTotal;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtTotal;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbFormasPago;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Pagos.FormulariosTipos.TarjetaMixto formTarjetaMixto;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Pagos.FormulariosTipos.Cortesia formCortesia;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Pagos.FormulariosTipos.ConsumosInternos formConsumosInternos;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Pagos.FormulariosTipos.Transferencia formTransferencias;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/pagos/selectorformapagoform.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 11 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
            ((SOTWpf.Pagos.SelectorFormaPagoForm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.SelectorFormaPagoForm_Load);
            
            #line default
            #line hidden
            return;
            case 2:
            this.lblTotal = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.txtTotal = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.cbFormasPago = ((System.Windows.Controls.ComboBox)(target));
            
            #line 75 "..\..\..\Pagos\SelectorFormaPagoForm.xaml"
            this.cbFormasPago.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbFormasPago_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.formTarjetaMixto = ((SOTWpf.Pagos.FormulariosTipos.TarjetaMixto)(target));
            return;
            case 6:
            this.formCortesia = ((SOTWpf.Pagos.FormulariosTipos.Cortesia)(target));
            return;
            case 7:
            this.formConsumosInternos = ((SOTWpf.Pagos.FormulariosTipos.ConsumosInternos)(target));
            return;
            case 8:
            this.formTransferencias = ((SOTWpf.Pagos.FormulariosTipos.Transferencia)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

