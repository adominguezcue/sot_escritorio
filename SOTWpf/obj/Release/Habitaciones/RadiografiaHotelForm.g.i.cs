﻿#pragma checksum "..\..\..\Habitaciones\RadiografiaHotelForm.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "02FABC6D699219C804C0C35B4DE75FBF4E8B92D30567BA077D7F903D67CE83D2"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using SOTWpf.Componentes.Buttons;
using SOTWpf.CustomValueConverters;
using SOTWpf.Nucleo;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.Habitaciones {
    
    
    /// <summary>
    /// RadiografiaHotelForm
    /// </summary>
    public partial class RadiografiaHotelForm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 24 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnReporte;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PanelControl;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpFecha;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MaterialDesignThemes.Wpf.TimePicker tpHora;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvResumenes;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/habitaciones/radiografiahotelform.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
            ((SOTWpf.Habitaciones.RadiografiaHotelForm)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnReporte = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 3:
            this.PanelControl = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.dpFecha = ((System.Windows.Controls.DatePicker)(target));
            
            #line 59 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
            this.dpFecha.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dpFecha_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.tpHora = ((MaterialDesignThemes.Wpf.TimePicker)(target));
            
            #line 68 "..\..\..\Habitaciones\RadiografiaHotelForm.xaml"
            this.tpHora.DataContextChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.tpHora_DataContextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.dgvResumenes = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

