﻿#pragma checksum "..\..\..\Rentas\RentasFormV2.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "FD1304E2A67E70A7CDCED15A8AAEA9C8EC48CDC6670F0794C4357A3AB37B16FB"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using CurrencyTextBoxControl;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using SOTWpf.Componentes.Buttons;
using SOTWpf.CustomValueConverters;
using SOTWpf.Nucleo;
using SOTWpf.Rentas;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SOTWpf.Rentas {
    
    
    /// <summary>
    /// RentasFormV2
    /// </summary>
    public partial class RentasFormV2 : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 46 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblDatosHabitacion;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblDatosHospedaje;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CurrencyTextBoxControl.CurrencyTextBox nudHabitaciones;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CurrencyTextBoxControl.CurrencyTextBox nudPersonasExtra;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CurrencyTextBoxControl.CurrencyTextBox nudNochesExtra;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CurrencyTextBoxControl.CurrencyTextBox nudHoraseExtra;
        
        #line default
        #line hidden
        
        
        #line 254 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal CurrencyTextBoxControl.CurrencyTextBox nudPaquetes;
        
        #line default
        #line hidden
        
        
        #line 309 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock lblVPoints;
        
        #line default
        #line hidden
        
        
        #line 319 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtVpoints;
        
        #line default
        #line hidden
        
        
        #line 345 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gbDatosEntrada;
        
        #line default
        #line hidden
        
        
        #line 369 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbTarifa;
        
        #line default
        #line hidden
        
        
        #line 384 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtFolio;
        
        #line default
        #line hidden
        
        
        #line 398 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtTarjetaV;
        
        #line default
        #line hidden
        
        
        #line 405 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chbAPie;
        
        #line default
        #line hidden
        
        
        #line 412 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid datosAuto;
        
        #line default
        #line hidden
        
        
        #line 516 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.HideableFloatingIconButton btnVPoints;
        
        #line default
        #line hidden
        
        
        #line 534 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnSolicitarCancelacionPendiente;
        
        #line default
        #line hidden
        
        
        #line 543 "..\..\..\Rentas\RentasFormV2.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal SOTWpf.Componentes.Buttons.FloatingIconButton btnSolicitarCancelacion;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SOTWpf;component/rentas/rentasformv2.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Rentas\RentasFormV2.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 17 "..\..\..\Rentas\RentasFormV2.xaml"
            ((SOTWpf.Rentas.RentasFormV2)(target)).Loaded += new System.Windows.RoutedEventHandler(this.RentasForm_Load);
            
            #line default
            #line hidden
            return;
            case 2:
            this.lblDatosHabitacion = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.lblDatosHospedaje = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.nudHabitaciones = ((CurrencyTextBoxControl.CurrencyTextBox)(target));
            return;
            case 5:
            this.nudPersonasExtra = ((CurrencyTextBoxControl.CurrencyTextBox)(target));
            
            #line 135 "..\..\..\Rentas\RentasFormV2.xaml"
            this.nudPersonasExtra.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.nudPersonasExtra_TextChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.nudNochesExtra = ((CurrencyTextBoxControl.CurrencyTextBox)(target));
            
            #line 176 "..\..\..\Rentas\RentasFormV2.xaml"
            this.nudNochesExtra.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.nudNochesExtra_TextChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.nudHoraseExtra = ((CurrencyTextBoxControl.CurrencyTextBox)(target));
            return;
            case 8:
            this.nudPaquetes = ((CurrencyTextBoxControl.CurrencyTextBox)(target));
            return;
            case 9:
            this.lblVPoints = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.txtVpoints = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.gbDatosEntrada = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 12:
            this.cbTarifa = ((System.Windows.Controls.ComboBox)(target));
            
            #line 375 "..\..\..\Rentas\RentasFormV2.xaml"
            this.cbTarifa.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbTarifa_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.txtFolio = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.txtTarjetaV = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.chbAPie = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 16:
            this.datosAuto = ((System.Windows.Controls.Grid)(target));
            return;
            case 17:
            
            #line 438 "..\..\..\Rentas\RentasFormV2.xaml"
            ((System.Windows.Controls.TextBox)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnVPoints = ((SOTWpf.Componentes.Buttons.HideableFloatingIconButton)(target));
            return;
            case 19:
            this.btnSolicitarCancelacionPendiente = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            case 20:
            this.btnSolicitarCancelacion = ((SOTWpf.Componentes.Buttons.FloatingIconButton)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

