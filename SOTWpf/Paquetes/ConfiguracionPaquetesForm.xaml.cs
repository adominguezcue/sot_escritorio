﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTControladores.Utilidades;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Paquetes
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionPaquetesForm.xaml
    /// </summary>
    public partial class ConfiguracionPaquetesForm : Window
    {
        private bool esNuevo = false;

        private ItemWrapper<Paquete> paqueteActual;
        private ObservableCollection<CheckableItemWrapper<TipoHabitacion>> tipos;
        decimal IVA;

        public ConfiguracionPaquetesForm()
        {
            InitializeComponent();
            IVA = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar;

            paqueteActual = new ItemWrapper<Paquete>();
            DataContext = paqueteActual;
            dgvPaquetes.ItemsSource = null;

            tipos = new ObservableCollection<CheckableItemWrapper<TipoHabitacion>>();
            listaMarcable.ItemsSource = tipos;

            var meses = FindResource("mesesCVS") as CollectionViewSource;

            if (meses != null)
                meses.Source = new List<Dominio.Nucleo.Dtos.DtoIdValor>()
                {
                    new Dominio.Nucleo.Dtos.DtoIdValor(),
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 1, Valor = "Enero"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 2, Valor = "Febrero"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 3, Valor = "Marzo"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 4, Valor = "Abril"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 5, Valor = "Mayo"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 6, Valor = "Junio"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 7, Valor = "Julio"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 8, Valor = "Agosto"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 9, Valor = "Septiembre"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 10, Valor = "Octubre"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 11, Valor = "Noviembre"},
                    new Dominio.Nucleo.Dtos.DtoIdValor{Id = 12, Valor = "Diciembre"},
                };
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            paqueteActual.Value = new Paquete() { CobroUnico = false };

            foreach (var item in tipos)
            {
                item.IsChecked = false;
            }

            esNuevo = true;

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            Paquete paquete;

            if ((paquete = dgvPaquetes.SelectedItem as Paquete) == null)
                throw new SOTException(Textos.Errores.multiples_paquetes_seleccionados);

            if (!paquete.CobroUnico.HasValue)
                paquete.CobroUnico = false;

            paqueteActual.Value = paquete;

            var tipoActual = tipos.FirstOrDefault(m => m.Value.Id == paquete.IdTipoHabitacion);

            if (tipoActual != null)
                tipoActual.IsChecked = true;

            esNuevo = false;

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void CargarPaquetes()
        {
            var paquetes = new ControladorPaquetes().ObtenerPaquetesActivosConProgramas();

            foreach (var m in paquetes) 
            { 
                m.PrecioConIVATmp = m.Precio * (1 + IVA);
                m.DescuentoConIVATmp = m.Descuento * (1 + IVA);

                if (!m.CobroUnico.HasValue)
                    m.CobroUnico = false;
            }

            dgvPaquetes.ItemsSource = paquetes;
        }

        private void CargarTipos()
        {
            tipos.Clear();

            foreach (var tipo in new ControladorTiposHabitacion().ObtenerTiposActivos()) 
            {
                tipos.Add(new CheckableItemWrapper<TipoHabitacion> { Value = tipo });
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
            esNuevo = false;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;
            
            Window_Loaded(null, null);
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorPaquetes();

            var item = tipos.FirstOrDefault(m => m.IsChecked);

            if (item == null)
                throw new SOTException(Textos.Errores.tipo_habitacion_no_seleccionado_exception);

            paqueteActual.Value.IdTipoHabitacion = item.Value.Id;

            if (esNuevo)
                controlador.CrearPaquete(paqueteActual.Value);
            else
                controlador.ModificarPaquete(paqueteActual.Value);

            Window_Loaded(null, null);

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;

            if (esNuevo)
                MessageBox.Show(Textos.Mensajes.creacion_paquete_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show(Textos.Mensajes.modificacion_paquete_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var paquete = dgvPaquetes.SelectedItem as Paquete;

            if (paquete == null)
                throw new SOTException(Textos.Errores.multiples_paquetes_seleccionados);

            if (MessageBox.Show(string.Format(Textos.Mensajes.eliminar_paquete, paquete.Nombre, paquete.NombreTipo), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorPaquetes();

                controlador.EliminarPaquete(paquete.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_paquete_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                Window_Loaded(null, null);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            paqueteActual.Value = null;

            CargarPaquetes();
            CargarTipos();

            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as CheckableItemWrapper<TipoHabitacion>;

            foreach (var item in tipos)
            {
                if (item == marcado)
                    continue;

                item.IsChecked = false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void chbProgramar_Checked(object sender, RoutedEventArgs e)
        {
            dgvProgramas.Visibility = System.Windows.Visibility.Visible;
        }

        private void chbProgramar_Unchecked(object sender, RoutedEventArgs e)
        {
            dgvProgramas.Visibility = System.Windows.Visibility.Hidden;
        }

        //private void nudPrecio_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    if (paqueteActual.Value != null)
        //        paqueteActual.Value.Precio = (decimal)(e.NewValue ?? 0) / (1 + IVA);
        //}

        private void nudPrecio_ValueChanged(object sender, EventArgs e)
        {
            if (paqueteActual.Value != null)
                paqueteActual.Value.Precio = nudPrecio.Number / (1 + IVA);
        }

        //private void nudDescuento_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    if (paqueteActual.Value != null)
        //        paqueteActual.Value.Descuento = (decimal)(e.NewValue ?? 0) / (1 + IVA);
        //}

        private void nudDescuento_ValueChanged(object sender, EventArgs e)
        {
            if (paqueteActual.Value != null)
                paqueteActual.Value.Descuento = nudDescuento.Number / (1 + IVA);
        }

        private void btnEliminarPrograma_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ProgramaPaquete;

            if (item != null)
            {
                if (item.Id == 0)
                    paqueteActual.Value.ProgramasPaquete.Remove(item);
                else
                    item.Activo = false;
            }

            CollectionViewSource.GetDefaultView(dgvProgramas.ItemsSource).Refresh();
        }

        private void btnAgregarPrograma_Click(object sender, RoutedEventArgs e)
        {
            if (!paqueteActual.Value.Programado)
                return;

            paqueteActual.Value.ProgramasPaquete.Add(new ProgramaPaquete
            {
                Activo = true,
                Dia = 1
            });


            CollectionViewSource.GetDefaultView(dgvProgramas.ItemsSource).Refresh();
        }

        private void programasCVS_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ProgramaPaquete;

            e.Accepted = item != null && item.Activo;
        }
    }
}
