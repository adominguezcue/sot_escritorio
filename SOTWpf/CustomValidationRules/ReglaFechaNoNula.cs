﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SOTWpf.CustomValidationRules
{
    internal class ReglaFechaNoNula : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null || !(value is DateTime))
                return new ValidationResult(false, "El campo requiere una fecha");

            return new ValidationResult(true, null);
        }
    }
}
