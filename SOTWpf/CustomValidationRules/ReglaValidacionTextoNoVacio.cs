﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SOTWpf.CustomValidationRules
{
    internal class ReglaValidacionTextoNoVacio : ValidationRule
    {
        internal string MensajeError { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (string.IsNullOrWhiteSpace(value?.ToString()))
                return new ValidationResult(false, MensajeError);

            return new ValidationResult(true, null);
        }
    }
}
