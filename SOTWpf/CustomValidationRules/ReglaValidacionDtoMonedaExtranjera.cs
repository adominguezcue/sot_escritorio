﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using Transversal.Excepciones;

namespace SOTWpf.CustomValidationRules
{
    public class ReglaValidacionDtoMonedaExtranjera : ValidationRule
    {
        // Ensure that an item over $100 is available for at least 7 days.
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var bg = value as BindingGroup;

            // Get the source object.
            var item = bg.Items[0] as DtoConfiguracionMonedaExtranjera;

            if (item == null)
            {
                return new ValidationResult(false, "Configuración no encontrada");
            }

            object valorAbreviatura, valorNombre, valorValorCambio;

            // Get the proposed values for Price and OfferExpires.
            bool abreviaturaEncontrada = bg.TryGetValue(item, nameof(item.Abreviatura), out valorAbreviatura);
            bool nombreEncontrado = bg.TryGetValue(item, nameof(item.Nombre), out valorNombre);
            bool valorCambioEncontrado = bg.TryGetValue(item, nameof(item.ValorCambio), out valorValorCambio);

            if (!abreviaturaEncontrada || !nombreEncontrado || !valorCambioEncontrado)
            {
                return new ValidationResult(false, "Propiedades no encontradas");
            }

            if (string.IsNullOrWhiteSpace(valorAbreviatura?.ToString()))
                return new ValidationResult(false, "La abreviatura es obligatoria");

            if (string.IsNullOrWhiteSpace(valorNombre?.ToString()))
                return new ValidationResult(false, "El nombre es obligatorio");

            var valorCambio = (decimal)valorValorCambio;

            if (valorCambio <= 0)
                return new ValidationResult(false, "El valor de cambio debe ser mayor que cero");

            return ValidationResult.ValidResult;

        }
    }
}
