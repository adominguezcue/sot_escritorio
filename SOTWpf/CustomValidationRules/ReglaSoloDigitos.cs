﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SOTWpf.CustomValidationRules
{
    internal class ReglaSoloDigitos : ValidationRule
    {
        //static readonly System.Text.RegularExpressions.Regex REGEX_SOLO_DIGITOS
        //    = new System.Text.RegularExpressions.Regex("^[0-9]+$");

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value == null || !Transversal.Utilidades.UtilidadesRegex.SoloDigitos((string)value))
                return new ValidationResult(false, "El campo solamente acepta dígitos");

            return new ValidationResult(true, null);
        }
    }
}
