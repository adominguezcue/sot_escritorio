﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SOTWpf.CustomValidationRules
{
    internal class ReglaNoCeros: ValidationRule
    {
        internal string MensajeError { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            int val = 0;

            if (value == null || !int.TryParse((value ?? "").ToString(), out val) || val == 0)
                return new ValidationResult(false, MensajeError);

            return new ValidationResult(true, null);
        }
    }
}
