﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.ConceptosSistema;
using SOTWpf.Empleados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SOTWpf.Taxis
{
    /// <summary>
    /// Lógica de interacción para GestionTaxis.xaml
    /// </summary>
    public partial class GestionTaxis : UserControl
    {
        //public event RoutedEventHandler PostCobro;
        private DispatcherTimer timerRecarga;

        public GestionTaxis()
        {
            InitializeComponent();
        }

        private void CargarOrdenesTaxis()
        {
            

            var controlador = new ControladorConfiguracionGlobal();
            var controladorT = new ControladorTaxis();

            List<OrdenTaxi> ordenes = controladorT.ObtenerOrdenesPendientes();

            int i = 0;

            while (i < ordenes.Count)
            {
                ordenes[i].Orden = ++i;
            }

            var configuracionGlobal = controlador.ObtenerConfiguracionGlobal();

            ordenesTaxis.AlternationCount = ordenes.Count;
            ordenesTaxis.ItemsSource = ordenes;

            contadorTaxis.Text = ordenes.Count.ToString();

            if (ordenes.Count < configuracionGlobal.MaximoTaxisSimultaneos)
                btnNuevo.Visibility = Visibility.Visible;
            else
                btnNuevo.Visibility = Visibility.Collapsed;  
        }

        private void CargarCantidadOrdenesTaxis()
        {
            var controlador = new ControladorConfiguracionGlobal();
            var controladorT = new ControladorTaxis();

            var ordenes = controladorT.ObtenerCantidadOrdenesPendientes();

            //int i = 0;

            //while (i < ordenes.Count)
            //{
            //    ordenes[i].Orden = ++i;
            //}

            //var configuracionGlobal = controlador.ObtenerConfiguracionGlobal();

            //ordenesTaxis.AlternationCount = ordenes.Count;
            //ordenesTaxis.ItemsSource = ordenes;

            contadorTaxis.Text = ordenes.ToString();

            //if (ordenes.Count < configuracionGlobal.MaximoTaxisSimultaneos)
            //    btnNuevo.Visibility = Visibility.Visible;
            //else
            //    btnNuevo.Visibility = Visibility.Collapsed;
        }

        private void AgregarOrdenTaxi(object sender, RoutedEventArgs e)
        {
            CreacionOrdenTaxiForm taxisF = new CreacionOrdenTaxiForm();

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, taxisF);

            CargarCantidadOrdenesTaxis();
        }

        private void btnCobrar_Click(object sender, RoutedEventArgs e)
        {
            var orden = ((Control)sender).DataContext as OrdenTaxi;

            if (orden == null)
                return;

            var seleccionadorValetF = new SeleccionadorValetsForm();

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, seleccionadorValetF);

            if (seleccionadorValetF.ProcesoExitoso)
            {
                var controlador = new ControladorTaxis();

                controlador.CobrarOrden(orden.Id, seleccionadorValetF.ValetSeleccionado.Id);
            }


            //if (PostCobro != null)
            //{
            //    PostCobro(this, new RoutedEventArgs());
            //}

            CargarCantidadOrdenesTaxis();
        }

        private void btnModificarPrecio_Click(object sender, RoutedEventArgs e)
        {
            var orden = ((Control)sender).DataContext as OrdenTaxi;

            CreacionOrdenTaxiForm taxisF = new CreacionOrdenTaxiForm(orden);

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, taxisF);

            //if (PostCobro != null)
            //{
            //    PostCobro(this, new RoutedEventArgs());
            //}

            CargarCantidadOrdenesTaxis();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            var orden = ((Control)sender).DataContext as OrdenTaxi;

            CapturaMotivo motivoF = new CapturaMotivo();
            motivoF.Motivo = "cancelación";
            motivoF.TextoAuxiliar = "Detalles";

            var idOrden = 0;

            if (orden != null)
            {
                idOrden = orden.Id;
            }

            CapturaMotivo.AceptarEventHandler eh = new CapturaMotivo.AceptarEventHandler((s, ea) =>
            {
                var controlador = new ControladorTaxis();
                controlador.CancelarOrden(idOrden, motivoF.DescripcionMotivo);

                //if (PostCobro != null)
                //{
                //    PostCobro(this, new RoutedEventArgs());
                //}

                CargarCantidadOrdenesTaxis();
            });

            try
            {
                motivoF.Aceptar += eh;

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, motivoF);
            }
            finally
            {
                motivoF.Aceptar -= eh;
            }


        }

        private void GestionTaxisForm_Load(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (!new ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarTaxis = true }))
                {
                    Visibility = System.Windows.Visibility.Collapsed;
                    return;
                }

                timerRecarga = new DispatcherTimer();
                timerRecarga.Interval = TimeSpan.FromSeconds(15);
                timerRecarga.Tick += CargarOrdenesTaxisEH;
                CargarOrdenesTaxisEH(null, null);

                timerRecarga.Start();
            }
        }

        private void CargarOrdenesTaxisEH(object sender, EventArgs e)
        {
            try
            {
                CargarCantidadOrdenesTaxis();
            }
            catch { }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if ((e.NewValue as bool?) == true && !new ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarTaxis = true }))
                    Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (timerRecarga != null)
                {
                    timerRecarga.Stop();
                    timerRecarga.Tick -= CargarOrdenesTaxisEH;
                }
            }
        }

        private void PopupBox_Opened(object sender, RoutedEventArgs e)
        {
            try
            {
                CargarOrdenesTaxis();
            }
            catch { }
        }
    }
}
