﻿using SOTControladores.Controladores;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows;
using Transversal.Excepciones;

namespace SOTWpf.Taxis
{
    /// <summary>
    /// Lógica de interacción para CreacionOrdenTaxiForm.xaml
    /// </summary>
    public partial class CreacionOrdenTaxiForm : Window
    {
        private Modelo.Entidades.OrdenTaxi _ordenTaxi;

        public CreacionOrdenTaxiForm(Modelo.Entidades.OrdenTaxi ordenTaxi = null)
        {
            Loaded += CreacionOrdenTaxiForm_Load;
            // Llamada necesaria para el diseñador.
            InitializeComponent();

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().

            _ordenTaxi = ordenTaxi;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorTaxis();


            if (_ordenTaxi == null)
            {
                Modelo.Entidades.OrdenTaxi orden = new Modelo.Entidades.OrdenTaxi();
                orden.Activo = true;

                decimal precio;

                if (!decimal.TryParse(txtImporte.Text, out precio))
                    throw new SOTException(Textos.Errores.valor_campo_invalido_exception, "Importe");

                orden.Precio = precio;

                controlador.CrearOrden(orden);

            }
            else
            {
                decimal precio;

                if (!decimal.TryParse(txtImporte.Text, out precio))
                    throw new SOTException(Textos.Errores.valor_campo_invalido_exception, "Importe");

                controlador.ModificarPrecio(_ordenTaxi.Id, precio);
            }

            Close();
        }
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CreacionOrdenTaxiForm_Load(object sender, RoutedEventArgs e)
        {
            var controladorT = new ControladorTaxis();

            if ((_ordenTaxi == null))
            {
                var controlador = new ControladorConfiguracionGlobal();

                var configuracionGlobal = controlador.ObtenerConfiguracionGlobal();

                txtImporte.Text = configuracionGlobal.PrecioPorDefectoTaxi.ToString();
            }
            else
            {
                txtImporte.Text = _ordenTaxi.Precio.ToString();
            }



        }

        private void txtImporte_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {

        }
    }
}
