﻿using Modelo.Almacen.Entidades;
using SOTControladores.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ZctSOT;

namespace SOTWpf.OrdenesTrabajo
{
    /// <summary>
    /// Lógica de interacción para SelectorOrdenesTrabajoForm.xaml
    /// </summary>
    public partial class SelectorOrdenesTrabajoForm : Window
    {
        ObservableCollection<CheckableItemWrapper<ZctEncOT>> lista = new ObservableCollection<CheckableItemWrapper<ZctEncOT>>();

        public SelectorOrdenesTrabajoForm()
        {
            InitializeComponent();
        }

        internal IEnumerable<int> Encabezados 
        {
            get { return lista.Where(m => m.IsChecked).Select(m => m.Value.CodEnc_OT); }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                listaMarcable.ItemsSource = lista;
                CargarOrdenes();
            }
        }

        private void CargarOrdenes()
        {
            var controladorOT = new SOTControladores.Controladores.ControladorOrdenesTrabajo();

            lista.Clear();

            foreach (var item in controladorOT.ObtenerEncabezadosOrdenesAprobadasFILTRO())
                lista.Add(new CheckableItemWrapper<ZctEncOT> { Value = item });
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            lista.Clear();
            Close();
        }

        private void btnRecargar_Click(object sender, RoutedEventArgs e)
        {
            CargarOrdenes();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctOrdTrabajo());
        }
    }
}
