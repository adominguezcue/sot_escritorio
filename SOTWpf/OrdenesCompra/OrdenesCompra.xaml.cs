﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using ZctSOT.Datos;
using System.Data;
using SOTControladores.Fabricas;
using System.IO;
using System.Linq.Dynamic;

namespace SOTWpf.OrdenesCompra
{
    /// <summary>
    /// Lógica de interacción para OrdenesCompra.xaml
    /// </summary>
    public partial class OrdenesCompra : Window
    {
        Modelo.Seguridad.Dtos.DtoPermisos permisos = new Modelo.Seguridad.Dtos.DtoPermisos();
        SOTControladores.Controladores.ControladorPermisos controladorPermisos = new SOTControladores.Controladores.ControladorPermisos();
        List<Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC> listadearticulo = new List<Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC>();
        private TextBox textboxSurtido = new TextBox();
        private TextBox Texboxsolicitada = new TextBox();
        private zctFolios zctFolios = new zctFolios("OC");
        private ZctSOT.Datos.Clases.Sistema.ZctCatPar parametros = ZctSOT.ControladorParametros.ObtenerParametros();
        private DataTable td;
        private string sStatus = ""; //Estatus del Documento
        private int Folio_OC_Doc = 0;
        int indicearticuloseleccionado = 0;

        public OrdenesCompra(int Folio_OC)
        {
            InitializeComponent();
            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            fechafactura.Language = lang;
            datagridarticulos.Language = lang;
            ELiminaArticulo.IsEnabled = false;
            Imprimir.IsEnabled = false;
            Guardarcerrar.IsEnabled = false;
            CancelarOC.IsEnabled = false;
            if (Folio_OC != 0)
                Folio_OC_Doc = Folio_OC;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {

                permisos = controladorPermisos.ObtenerPermisosActuales();
                SOTControladores.Controladores.ControladorCuentasPorPagar controladorCuentasPorPagar = new SOTControladores.Controladores.ControladorCuentasPorPagar();
                if (permisos.CrearOrdenCompra || permisos.ModificarOrdenCompra || permisos.EliminarOrdenCompra)
                    Guardar.IsEnabled = true;
                if (permisos.CrearOrdenCompra) //permitir agregar un articulo validarlo despues de Obtener la lista
                    datagridarticulos.IsReadOnly = false;
                /*Validar el funicionamiento del datagrid despues de obtener la lista*/


                statusoc.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA;
                folios.Text = zctFolios.Consecutivo.ToString();
                fechafactura.SelectedDate = DateTime.Now;
                //horafactura.SelectedTime = DateTime.Now;
                fechaaplicaion.SelectedDate = DateTime.Now;
                //horaaplicacion.SelectedTime = DateTime.Now;
                sStatus = "A";

                if (Folio_OC_Doc != 0)
                {
                    folios.Text = Folio_OC_Doc.ToString();
                    CargaCompraV2();
                }

            }
            catch (Exception ex)
            {

            }
        }

        private void busquedproveedor_Click(object sender, RoutedEventArgs e)
        {
            var bp = new Proveedores.BusquedaProveedor();
            Utilidades.Dialogos.MostrarDialogos(this, bp);
            Proveedordesc.Text = bp.ProveedorSeleccionado != null ? bp.ProveedorSeleccionado.Nom_Prov.ToString() : "";
            numeroproveedores.Text = bp.ProveedorSeleccionado != null ? bp.ProveedorSeleccionado.Cod_Prov.ToString() : "";
        }

        private void numeroproveedores_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (numeroproveedores.Text != null && numeroproveedores.Text.Length > 0)
            {
                SOTControladores.Controladores.ControladorProveedores controladorProveedores = new SOTControladores.Controladores.ControladorProveedores();
                var proveedor1 = controladorProveedores.ObtenerProveedor(Convert.ToInt32(numeroproveedores.Text));
                Proveedordesc.Text = proveedor1 != null ? proveedor1.Nom_Prov.ToString() : "";
            }
            else
            {
                Proveedordesc.Text = "";
            }
        }

        private void numeroproveedores_LostFocus(object sender, RoutedEventArgs e)
        {
            if (numeroproveedores.Text != null && numeroproveedores.Text.Length > 0)
            {
                SOTControladores.Controladores.ControladorProveedores controladorProveedores = new SOTControladores.Controladores.ControladorProveedores();
                var proveedor1 = controladorProveedores.ObtenerProveedor(Convert.ToInt32(numeroproveedores.Text));
                Proveedordesc.Text = proveedor1 != null ? proveedor1.Nom_Prov.ToString() : "";
            }
            else
            {
                Proveedordesc.Text = "";
            }
        }

        private void folios_TextChanged(object sender, TextChangedEventArgs e)
        {
            //cambio de folios
        }

        private void folios_LostFocus(object sender, RoutedEventArgs e)
        {
            CargaCompraV2();
        }

        private void datagridarticulos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            indicearticuloseleccionado = ((System.Windows.Controls.Primitives.Selector)sender).SelectedIndex;
            if (indicearticuloseleccionado >= 0)
                ELiminaArticulo.IsEnabled = true;
            if (statusoc.Text.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA)
                || statusoc.Text.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                || statusoc.Text.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA))
            {
                ELiminaArticulo.IsEnabled = false;
            }
        }

        private void agregaarticulo_Click(object sender, RoutedEventArgs e)
        {
            var controlador = FabricaBuscadores.ObtenerBuscadorArticulos();
            controlador.SoloInventariables = true;
            controlador.Mostrar();
            Modelo.Almacen.Entidades.Dtos.DtoResultadoBusquedaArticuloSimple articuloseleccionado = new Modelo.Almacen.Entidades.Dtos.DtoResultadoBusquedaArticuloSimple();
            articuloseleccionado = controlador.ArticuloSeleccionado;
            if (articuloseleccionado != null)
            {
                Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC articuloseleccionado1 = new Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC();
                articuloseleccionado1.Cod_Art = articuloseleccionado.Codigo.ToString();
                articuloseleccionado1.Desc_Art = articuloseleccionado.Nombre.ToString();
                var articuloparabuscarlo = BuscaArticulo(articuloseleccionado);
                var almacenaseleccionar = new SOTControladores.Controladores.ControladorAlmacenes().ObtenerAlmacenes();
                articuloseleccionado1.Almacen = almacenaseleccionar;
                int index = 0;
                foreach (Modelo.Almacen.Entidades.Almacen item in articuloseleccionado1.Almacen)
                {
                    if (item.Cod_Alm == Convert.ToInt32(articuloparabuscarlo.cod_alm))
                    {
                        articuloseleccionado1.indicealmacenseleccionado = index;
                        articuloseleccionado1.almacenseleccionado = item.Cod_Alm;
                    }
                    index++;
                }

                CalculaExistencia(articuloseleccionado1);
                articuloseleccionado1.omitiriva = articuloparabuscarlo.OmitirIVA;
                articuloseleccionado1.Solicitada = 0;
                articuloseleccionado1.Surtido = 0;
                articuloseleccionado1.Elimina = false;
                articuloseleccionado1.CodDetOCIdentity = 0;
                articuloseleccionado1.costo = Convert.ToDecimal(articuloseleccionado.Costo);
                listadearticulo.Add(articuloseleccionado1);
                CargaTotal();
                RecargaArticulos();
            }
        }

        private void agregaarticulo_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void CargaTotal()
        {
            decimal subtotaltex = 0;
            decimal decimaliva = 0;
            if (listadearticulo != null && listadearticulo.Count > 0)
            {
                foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC arti in listadearticulo)
                {
                    decimal subtotalgrid = arti.subtotal;
                    subtotaltex = subtotaltex + subtotalgrid;
                    if (arti.omitiriva == false)
                    {
                        decimaliva = decimaliva + (subtotalgrid * Convert.ToDecimal(parametros.Iva_CatPar));
                    }
                }
            }
            subtotal.Text = subtotaltex.ToString("C");
            textoiva.Text = decimaliva.ToString("C");
            total.Text = (subtotaltex + decimaliva).ToString("C");
        }

        private void RecargaArticulos()
        {
            datagridarticulos.ItemsSource = null;
            datagridarticulos.ItemsSource = listadearticulo.Where(m => m.Elimina == false);
            ColoreaColumnaStatus(listadearticulo);
            indicearticuloseleccionado = listadearticulo.Count - 1;
            datagridarticulos.SelectedIndex = indicearticuloseleccionado;
            datagridarticulos.IsReadOnly = true;
            if (listadearticulo.Count <= 0)
                ELiminaArticulo.IsEnabled = false;
        }

        private void ColoreaColumnaStatus(List<Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC> datosarticulos)
        {
            foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC renglon in datosarticulos)
            {
                if (renglon.Surtido < renglon.Solicitada)
                {
                    renglon.statussolicitada = "PorSurtir";
                }
                if (renglon.Solicitada == renglon.Surtido)
                {
                    renglon.statussolicitada = "Surtido";
                }
                if (renglon.Surtido > renglon.Solicitada)
                {
                    renglon.statussolicitada = "Devolucion";
                }
            }
        }


        private void CalculaExistencia(Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC articuloseleccionado1)
        {
            try
            {
                var dt = new DataTable();
                td = ClassGen.GenGetData("SP_ZctArtXAlm", "@TpConsulta;int|Cod_Art;VarChar|Cod_Alm;Int|Exist_Art;Int|CostoProm_Art;Money"
                    , "1|" + articuloseleccionado1.Cod_Art.Trim().ToString() + "|" + articuloseleccionado1.almacenseleccionado + "|0|0");

                if (td != null && td.Rows.Count > 0)
                {
                    articuloseleccionado1.Existencias = Convert.ToDecimal(td.Rows[0]["Exist_Art"]);
                }
                else
                {
                    articuloseleccionado1.Existencias = 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Modelo.Almacen.Entidades.Dtos.DtoResultadoBusquedaArticulo BuscaArticulo(Modelo.Almacen.Entidades.Dtos.DtoResultadoBusquedaArticuloSimple articulobuscar)
        {
            var controlador = new SOTControladores.Controladores.ControladorArticulos();
            return controlador.SP_ZctCatArt_Busqueda(articulobuscar.Codigo.ToString(), true);

        }

        private void ELiminaArticulo_Click(object sender, RoutedEventArgs e)
        {
            if (listadearticulo != null && listadearticulo.Count > 0)
            {
                Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC
                 row = datagridarticulos.Items[indicearticuloseleccionado]
                 as Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC;
                foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC articuloaborrar in listadearticulo)
                {
                    if (row == articuloaborrar)
                    {

                        // listadearticulo.Remove(articuloaborrar);
                        articuloaborrar.Elimina = true;
                        GuardarV2();
                    }
                }

            }
        }

        private void comboboxalmacen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listadearticulo != null && listadearticulo.Count > 0)
            {
                foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC datomodifica in listadearticulo)
                {
                    Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC
                      rowseleccionado = datagridarticulos.Items[indicearticuloseleccionado] as Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC;
                    if (datomodifica == ((Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC)((System.Windows.FrameworkElement)sender).DataContext))
                    {
                        if (rowseleccionado == ((Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC)((System.Windows.FrameworkElement)sender).DataContext))
                        {
                            var comboboc = ((System.Windows.Controls.ComboBox)sender) as ComboBox;
                            datomodifica.almacenseleccionado = ((Modelo.Almacen.Entidades.Almacen)comboboc.SelectedItem).Cod_Alm;
                            datomodifica.indicealmacenseleccionado = comboboc.SelectedIndex;
                        }
                    }
                }
            }
        }

        private void solicitadasolicitada_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (listadearticulo != null && listadearticulo.Count > 0)
            {
                foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC datomodifica in listadearticulo)
                {
                    Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC
                      row = datagridarticulos.Items[indicearticuloseleccionado] as Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC;
                    if (datomodifica == ((Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC)((System.Windows.FrameworkElement)sender).DataContext))
                    {
                        if (row == ((Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC)((System.Windows.FrameworkElement)sender).DataContext))
                        {
                            Texboxsolicitada = ((System.Windows.Controls.TextBox)sender) as TextBox;
                            var valoinsertado = Texboxsolicitada.Text;

                            if (valoinsertado.Equals("0"))
                            {
                                if (!IsDecimalFormat(valoinsertado))
                                {
                                    Utilidades.Dialogos.Error("Debes poner numeros");
                                    return;
                                }
                                decimal valor = Convert.ToDecimal(valoinsertado);
                                datomodifica.Solicitada = valor;
                                CalculaSubtotalGrid();
                            }
                            else if (valoinsertado.Length > 0)
                            {
                                if (!IsDecimalFormat(valoinsertado))
                                {
                                    Utilidades.Dialogos.Error("Debes poner numeros");
                                    return;
                                }
                                decimal valor = Convert.ToDecimal(valoinsertado);
                                datomodifica.Solicitada = valor;
                                CalculaSubtotalGrid();
                            }
                        }
                    }
                }
            }

        }

        private void solicitadasolicitada_LostFocus(object sender, RoutedEventArgs e)
        {
            RecargaArticulos();
        }

        private void CalculaSubtotalGrid()
        {
            if (listadearticulo != null && listadearticulo.Count > 0)
            {
                foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC arti in listadearticulo)
                {
                    arti.subtotal = arti.Solicitada * arti.costo;
                }
                CargaTotal();
            }
        }

        private bool IsDecimalFormat(string input)
        {
            bool respuesta = false;
            decimal dummy;
            return respuesta = Decimal.TryParse(input, out dummy);
        }

        private void surtido_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (listadearticulo != null && listadearticulo.Count > 0)
            {
                foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC datomodifica in listadearticulo)
                {

                    Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC
                     row = datagridarticulos.Items[indicearticuloseleccionado] as Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC;
                    if (datomodifica == ((Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC)((System.Windows.FrameworkElement)sender).DataContext))
                    {
                        if (row == ((Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC)((System.Windows.FrameworkElement)sender).DataContext))
                        {
                            textboxSurtido = ((System.Windows.Controls.TextBox)sender) as TextBox;
                            var valoinsertado = textboxSurtido.Text;

                            if (valoinsertado.Equals("0"))
                            {
                                if (!IsDecimalFormat(valoinsertado))
                                {
                                    Utilidades.Dialogos.Error("Debes poner numeros");
                                    return;
                                }
                                decimal valor = Convert.ToDecimal(valoinsertado);
                                datomodifica.Surtido = valor;
                            }
                            else if (valoinsertado.Length > 0)
                            {
                                if (!IsDecimalFormat(valoinsertado))
                                {
                                    Utilidades.Dialogos.Error("Debes poner numeros");
                                }
                                decimal valor = Convert.ToDecimal(valoinsertado);
                                datomodifica.Surtido = valor;
                            }
                        }
                    }
                }
            }

        }

        private void surtido_LostFocus(object sender, RoutedEventArgs e)
        {
            RecargaArticulos();
        }

        private void Surtetodo_Click(object sender, RoutedEventArgs e)
        {
            if (listadearticulo != null && listadearticulo.Count > 0)
            {
                foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC item in listadearticulo)
                {
                    item.Surtido = item.Solicitada;
                }
                RecargaArticulos();
            }
        }

        private void CancelarEdicion_Click(object sender, RoutedEventArgs e)
        {

            if (sStatus.Equals("A"))
            {
                this.Close();
            }
            else
            {
                Limpia(true);
                sStatus = "A";
                Imprimir.IsEnabled = false;
            }
        }

        private void Limpia(bool limpiafolio)
        {
            if (limpiafolio)
            {
                //  folios.DataContext = null;
                folios.Text = "";
                folios.Text = zctFolios.Consecutivo.ToString();
                folios.IsEnabled = true;
                folios.Focus();
            }

            statusoc.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA;

            if (statusoc.Text.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA))
            {
                ELiminaArticulo.IsEnabled = false;
            }
            statusoc.Background = Brushes.Gold;
            // facturatext.DataContext = null;
            facturatext.Text = "";

            numeroproveedores.Text = "";
            Proveedordesc.Text = "";

            subtotal.Text = "0";
            textoiva.Text = "0";
            total.Text = "0";
            Surtetodo.IsEnabled = true;
            Guardar.IsEnabled = true;
            agregaarticulo.IsEnabled = true;
            CancelarOC.IsEnabled = false;
            listadearticulo = new List<Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC>();
            datagridarticulos.ItemsSource = null;
        }

        private void Guardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GuardarV2();
            }
            catch (Exception es)
            {
                throw es;
            }
        }

        private void GuardarV2()
        {
            if (statusoc.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA) || statusoc.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO))
            {
                Utilidades.Dialogos.Aviso("No puede modificar órdenes de trabajo ya gereradas o canceladas, si desea corregir cancele / cree una OC nueva");
                return;
            }
            else if (statusoc.Text.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA))
            {
                statusoc.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA;
            }
            else if (statusoc.Text.Equals(Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO))
            {
                statusoc.Text = Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO;
            }

            if (String.IsNullOrEmpty(numeroproveedores.Text) || numeroproveedores.Text.Equals("0"))
            {
                Utilidades.Dialogos.Aviso("El código del proveedor no puede estar vacío, verfique por favor.");
                if (numeroproveedores.IsEnabled)
                    numeroproveedores.Focus();
                return;
            }

            if (listadearticulo.Count < 0)
            {
                Utilidades.Dialogos.Aviso("La orden no puede ser grabada sin artículos, verifique por favor.");
                return;
            }
            Modelo.Almacen.Entidades.Dtos.DtoEncabezadoOC encabezado = new Modelo.Almacen.Entidades.Dtos.DtoEncabezadoOC();
            encabezado.Cod_OC = folios.Text == "" ? 0 : Convert.ToInt32(folios.Text);
            encabezado.Cod_Prov = numeroproveedores.Text == "" ? 0 : Convert.ToInt32(numeroproveedores.Text);
            encabezado.Fch_OC = DateTime.Now;
            encabezado.FchAplMov = fechaaplicaion.SelectedDate;
            encabezado.Fac_OC = facturatext.Text;
            encabezado.FchFac_OC = fechafactura.SelectedDate;
            encabezado.status = statusoc.Text;

            foreach (Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC item in listadearticulo)
            {
                Modelo.Almacen.Entidades.Dtos.DtoDetalleOC detalle = new Modelo.Almacen.Entidades.Dtos.DtoDetalleOC();
                detalle.Cod_OC = folios.Text == "" ? 0 : Convert.ToInt32(folios.Text);
                detalle.CodDet_Oc = item.CodDetOCIdentity;
                detalle.Cod_Art = item.Cod_Art;
                detalle.Ctd_Art = item.Solicitada;
                detalle.Cos_Art = item.costo;
                detalle.CtdStdDet_OC = item.Surtido;
                detalle.FchAplMov = fechaaplicaion.SelectedDate;
                detalle.Cat_Alm = item.almacenseleccionado;
                detalle.OmitirIVA = item.omitiriva;
                detalle.Eliminar = item.Elimina;
                encabezado.Detalles.Add(detalle);
            }

            var ControladorOrdenDeCompra = new SOTControladores.Controladores.ControladorOrdenesCompra();
            ControladorOrdenDeCompra.EditarOrdenCompra(encabezado, sStatus);
            var cclaseallgo = new ZctSOT.Datos.ClassGen();
            cclaseallgo.GrabaUsuario(SOTControladores.Controladores.ControladorBase.UsuarioActual.Id, "ZctOrdCompra", "A", folios.Text);

            Limpia(false);
            sStatus = "M";
            CargaCompraV2();

            if (sStatus.Equals("M"))
            {
                Imprimir.IsEnabled = true;
            }
            else
            {
                Imprimir.IsEnabled = false;
            }
        }


        private void CargaCompraV2()
        {
            folios.IsEnabled = false;
            var controladorordencompra = new SOTControladores.Controladores.ControladorOrdenesCompra();
            var drEncOT = controladorordencompra.ObtenerResumenOrdenCompra(folios.Text == "" ? 0 : Convert.ToInt32(folios.Text));
            listadearticulo = new List<Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC>();
            if (drEncOT != null && (!drEncOT.Servicios.HasValue || drEncOT.Servicios.Value == false))
            {
                folios.Text = drEncOT.Cod_OC.ToString();
                facturatext.Text = drEncOT.Fac_OC;
                fechaaplicaion.SelectedDate = drEncOT.FchAplMov;
                fechafactura.SelectedDate = drEncOT.FchFac_OC;
                numeroproveedores.Text = drEncOT.Cod_Prov.ToString();
                statusoc.Text = drEncOT.status;

                var almacenaseleccionar = new SOTControladores.Controladores.ControladorAlmacenes().ObtenerAlmacenes();
                foreach (Modelo.Almacen.Entidades.Dtos.DtoDetalleOC detalle in drEncOT.Detalles)
                {
                    Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC elemtno = new Modelo.Almacen.Entidades.Dtos.DtosArticulosWpfOC();
                    elemtno.Cod_Art = detalle.Cod_Art;
                    elemtno.Desc_Art = detalle.Desc_Art;
                    elemtno.Solicitada = Convert.ToDecimal(detalle.Ctd_Art);
                    elemtno.CodDetOCIdentity = detalle.CodDet_Oc;
                    elemtno.Surtido = Convert.ToDecimal(detalle.CtdStdDet_OC);
                    elemtno.costo = Convert.ToDecimal(detalle.Cos_Art);
                    elemtno.subtotal = elemtno.Solicitada * elemtno.costo;
                    elemtno.almacenseleccionado = Convert.ToInt32(detalle.Cat_Alm);
                    elemtno.Almacen = almacenaseleccionar;
                    int index = 0;
                    foreach (Modelo.Almacen.Entidades.Almacen item in almacenaseleccionar)
                    {
                        if (elemtno.almacenseleccionado == item.Cod_Alm)
                        {
                            elemtno.indicealmacenseleccionado = index;
                        }
                        index++;
                    }

                    elemtno.omitiriva = detalle.OmitirIVA;
                    CalculaExistencia(elemtno);
                    listadearticulo.Add(elemtno);
                }

                CargaTotal();

                sStatus = "M";
            }
            switch (statusoc.Text.ToUpper())
            {
                case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA:
                    Guardarcerrar.IsEnabled = false;
                    CancelarOC.IsEnabled = true;
                    Guardar.IsEnabled = false;
                    statusoc.Background = Brushes.LightBlue;
                    Imprimir.IsEnabled = true;
                    agregaarticulo.IsEnabled = false;
                    ELiminaArticulo.IsEnabled = false;
                    Surtetodo.IsEnabled = false;
                    break;
                case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO:
                    statusoc.Background = Brushes.OrangeRed;
                    Guardarcerrar.IsEnabled = false;
                    CancelarOC.IsEnabled = false;
                    Guardar.IsEnabled = false;
                    agregaarticulo.IsEnabled = false;
                    Surtetodo.IsEnabled = false;
                    ELiminaArticulo.IsEnabled = false;
                    break;
                case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.APLICADO:
                    statusoc.Background = Brushes.PaleGreen;
                    Guardarcerrar.IsEnabled = true;
                    CancelarOC.IsEnabled = true;
                    Guardar.IsEnabled = true;
                    agregaarticulo.IsEnabled = true;
                    ELiminaArticulo.IsEnabled = true;
                    break;
                case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA:
                    statusoc.Background = Brushes.Gold;
                    Guardarcerrar.IsEnabled = false;
                    CancelarOC.IsEnabled = false;
                    Guardar.IsEnabled = true;
                    agregaarticulo.IsEnabled = true;
                    ELiminaArticulo.IsEnabled = false;
                    break;
            }
            RecargaArticulos();
        }

        private void Guardarcerrar_Click(object sender, RoutedEventArgs e)
        {
            var respuesdialog = Utilidades.Dialogos.Pregunta("Si cierra la OC no podrá hacerle modificaciones ¿desea continuar?", true);
            if (respuesdialog.ToString().Equals("Yes"))
            {
                statusoc.Text = "CERRADA";
                Guardarcerrar.IsEnabled = false;
                statusoc.Background = Brushes.LightBlue;
                GuardarV2();
            }
        }

        private void CancelarOC_Click(object sender, RoutedEventArgs e)
        {
            var respuesdialog = Utilidades.Dialogos.Pregunta("Esta operación es irreversible ¿desea continuar?", true);
            if (respuesdialog.ToString().Equals("Yes"))
            {
                try
                {
                    statusoc.Text = "CANCELADO";
                    statusoc.Background = Brushes.OrangeRed;
                    GuardarV2();
                }
                catch (Exception ex)
                {
                    Limpia(false);
                    sStatus = "M";
                    CargaCompraV2();
                    if (sStatus.Equals("M"))
                    {
                        Imprimir.IsEnabled = true;
                    }
                    else
                    {
                        Imprimir.IsEnabled = false;
                    }
                    throw ex;
                }
            }
        }

        private void cmdIzqu_Click(object sender, RoutedEventArgs e)
        {
            if (!folios.Text.Equals("") && Convert.ToInt32(folios.Text) - 1 > 0)
            {
                var controladorordecompra = new SOTControladores.Controladores.ControladorOrdenesCompra();
                var ordencompraizq = controladorordecompra.ObtenerUltimoFolioMenorQue(Convert.ToInt32(folios.Text), false);
                if (!ordencompraizq.HasValue)
                    return;
                folios.Text = ordencompraizq.Value.ToString();
                Limpia(false);
                CargaCompraV2();

                if (sStatus.Equals("M") && Convert.ToInt32(folios.Text) < zctFolios.Consecutivo)
                {
                    Imprimir.IsEnabled = true;
                }
                else
                {
                    sStatus = "A";
                    Imprimir.IsEnabled = false;
                    fechaaplicaion.SelectedDate = null;
                    //horaaplicacion.SelectedTime = null;
                    fechafactura.SelectedDate = null;
                    //horafactura.SelectedTime = null;

                }
            }
        }

        private void cmdder_Click(object sender, RoutedEventArgs e)
        {
            if (folios.Text != null && !folios.Text.Equals("") && Convert.ToInt32(folios.Text) + 1 < zctFolios.Consecutivo)
            {
                var controladorordecompra = new SOTControladores.Controladores.ControladorOrdenesCompra();
                var ordencomprader = controladorordecompra.ObtenerPrimerFolioMayorQue(Convert.ToInt32(folios.Text), false);
                if (!ordencomprader.HasValue)
                    return;
                folios.Text = ordencomprader.Value.ToString();
                Limpia(false);
                CargaCompraV2();

                if (sStatus.Equals("M") && Convert.ToInt32(folios.Text) < zctFolios.Consecutivo)
                {
                    Imprimir.IsEnabled = true;
                }
                else
                {
                    sStatus = "A";
                    Imprimir.IsEnabled = false;
                    fechaaplicaion.SelectedDate = null;
                    // horaaplicacion.SelectedTime = null;
                    fechafactura.SelectedDate = null;
                    //horafactura.SelectedTime = null;

                }
            }
        }

        private void Imprimir_Click(object sender, RoutedEventArgs e)
        {
            var configuracioglobal = new SOTControladores.Controladores.ControladorConfiguracionGlobal();
            var ControladorVPoints = new SOTControladores.Controladores.ControladorVPoints();
            var controladorCLientes = new SOTControladores.Controladores.ControladorClientes();

            var config = configuracioglobal.ObtenerConfiguracionGlobal();
            var configpuntos = ControladorVPoints.ObtenerConfiguracion();
            var datosfiscales = controladorCLientes.ObtenerUltimoCliente();

            var documentoreporte = new SOTWpf.Reportes.OrdeneCompra.OrdenesCompra();
            documentoreporte.Load();

            var controladorordencompra = new SOTControladores.Controladores.ControladorOrdenesCompra();

            var drEncOT = controladorordencompra.ObtenerResumenOrdenCompra(folios.Text == "" ? 0 : Convert.ToInt32(folios.Text));
            if (drEncOT != null)
            {
                switch (drEncOT.status)
                {
                    case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.OC_NUEVA:
                        drEncOT.Fch_OC = drEncOT.Fch_OC;
                        break;
                    case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.APLICADO:
                        drEncOT.Fch_OC = drEncOT.FchAplMov;
                        break;
                    case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CANCELADO:
                        drEncOT.Fch_OC = drEncOT.FchAplMov != null ? drEncOT.Fch_OC : DateTime.Now;
                        break;
                    case Modelo.Almacen.Entidades.ZctEncOC.EstatusOrdenesCompra.CERRADA:
                        var controladorCXP = new SOTControladores.Controladores.ControladorCuentasPorPagar();
                        drEncOT.Fch_OC = controladorCXP.ObtenerFechaUltimaCuenta(drEncOT.Cod_OC);
                        break;
                }
            }

            var detallesordecompra = drEncOT.Detalles;
            List<Modelo.Almacen.Entidades.ZctCatPar> params1 = new List<Modelo.Almacen.Entidades.ZctCatPar>();
            params1.Add(config);
            var params2 = (from m in params1
                           select new Modelo.Almacen.Entidades.Dtos.DtoParametrosWpf
                           {
                               Iva_CatPar = m.Iva_CatPar,
                           }).ToList();

            List<Modelo.Almacen.Entidades.Dtos.DtoEncabezadoOC> dtoencabezadoot =
                new List<Modelo.Almacen.Entidades.Dtos.DtoEncabezadoOC>();
            dtoencabezadoot.Add(drEncOT);

            var encabezados = (from m in dtoencabezadoot
                               select new Modelo.Almacen.Entidades.Dtos.DtoEncabezadoOCWpf
                               {
                                   Cod_OC = drEncOT.Cod_OC,
                                   status = drEncOT.status,
                                   NN_Fch_OC = drEncOT.NN_Fch_OC
                               }).ToList();

            var detalles = (from m in detallesordecompra
                            select new
                            {
                                m.Cod_Art,
                                m.Cod_OC,
                                m.CodDet_Oc,
                                m.Desc_Art,
                                m.marca,
                                m.NN_Cat_Alm,
                                m.NN_Cos_Art,
                                m.NN_Ctd_Art,
                                m.NN_CtdStdDet_OC,
                                m.OmitirIVA
                            }).ToList();

            var controladoproveedores = new SOTControladores.Controladores.ControladorProveedores();
            var proveedores = controladoproveedores.ObtenerProveedor(Convert.ToInt32(drEncOT.Cod_Prov));
            var listaproveedores = new List<Modelo.Almacen.Entidades.Proveedor>();
            listaproveedores.Add(proveedores);
            var provx = (from m in listaproveedores
                         select new
                         {
                             m.NN_dias_credito,
                             m.DireccionAutomatica,
                             m.calle,
                             m.ciudad,
                             m.Cod_Prov,
                             m.colonia,
                             m.Contacto,
                             m.cp,
                             m.Dir_Prov,
                             m.email,
                             m.estado,
                             m.Nom_Prov,
                             m.NombreComercial,
                             m.numext,
                             m.numint,
                             m.RFC_Prov,
                             m.telefono
                         }).ToList();

            var rutaL = ReportesSOT.Parametros.Logos.RutaLogoGeneral;
            if (File.Exists(rutaL))
            {
                rutaL = System.IO.Path.GetTempFileName() + Guid.NewGuid().ToString();
                while (File.Exists(rutaL))
                {
                    rutaL = System.IO.Path.GetTempFileName() + Guid.NewGuid().ToString();
                }
                File.Copy(ReportesSOT.Parametros.Logos.RutaLogoGeneral, rutaL, true);
            }

            var encabezadosreporte = new List<SOTWpf.Dtos.DtoCabeceraReporte>();
            encabezadosreporte.Add(new Dtos.DtoCabeceraReporte
            {
                Direccion = config.Direccion,
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now,
                Hotel = config.Nombre,
                RazonSocial = datosfiscales.RazonSocial,
                Usuario = SOTControladores.Controladores.ControladorBase.UsuarioActual.NombreCompleto,
                RutaLogo = rutaL
            });

            documentoreporte.Database.Tables[Reportes.OrdeneCompra.OrdenesCompra_Constantes.TABLA_ENCABEZADO_REPORTE].SetDataSource(encabezadosreporte);
            documentoreporte.Database.Tables[ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_PROVEEDOR].SetDataSource(provx);
            documentoreporte.Database.Tables[ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_ENCABEZADO_ORDEN_COMPRA].SetDataSource(encabezados);
            documentoreporte.Database.Tables[ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_DETALLES_ORDEN_COMPRA].SetDataSource(detalles);
            documentoreporte.Database.Tables[ReportesSOT.OrdenesCompra.OrdenesCompra_Constantes.TABLA_PARAMETROS].SetDataSource(params2);
            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoreporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }
    }
}
