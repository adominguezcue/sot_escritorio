﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Asistencias
{
    /// <summary>
    /// Lógica de interacción para EdicionAsistenciaForm.xaml
    /// </summary>
    public partial class EdicionAsistenciaForm : Window
    {
        int idAsistencia;

        public EdicionAsistenciaForm(Modelo.Entidades.Asistencia asistencia = null)
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaEntrada.Language = lang;

            if (asistencia != null)
            {
                dpFechaEntrada.SelectedDate = asistencia.FechaEntrada;
                tpHoraEntrada.SelectedTime = asistencia.FechaEntrada;
                dpFechaSalida.SelectedDate = asistencia.FechaSalida;
                tpHoraSalida.SelectedTime = asistencia.FechaSalida ?? asistencia.FechaEntrada.AddDays(1).Date;
                cbEmpleados.SelectedValue = asistencia.IdEmpleado;
                cbEmpleados.IsEnabled = false;

                tbSalida.IsChecked = asistencia.FechaSalida.HasValue;

                idAsistencia = asistencia.Id;
            }
            else 
            {
                tpHoraSalida.SelectedTime = tpHoraEntrada.SelectedTime = DateTime.Now.Date;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var empleado = cbEmpleados.SelectedItem as Modelo.Entidades.Empleado;

            if (empleado == null)
                throw new SOTException(Textos.Errores.empleado_no_seleccionado_excepcion);

            if (!dpFechaEntrada.SelectedDate.HasValue)
                throw new SOTException(Textos.Errores.fecha_entrada_no_seleccionado_excepcion);

            if (!tpHoraEntrada.SelectedTime.HasValue)
                throw new SOTException(Textos.Errores.hora_entrada_no_seleccionado_excepcion);

            var fechaEntrada = new DateTime(dpFechaEntrada.SelectedDate.Value.Year, dpFechaEntrada.SelectedDate.Value.Month, dpFechaEntrada.SelectedDate.Value.Day,
                                            tpHoraEntrada.SelectedTime.Value.Hour, tpHoraEntrada.SelectedTime.Value.Minute, tpHoraEntrada.SelectedTime.Value.Second);

            DateTime? fechaSalida;

            if (tbSalida.IsChecked.Value)
            {
                if (!dpFechaSalida.SelectedDate.HasValue)
                    throw new SOTException(Textos.Errores.fecha_salida_no_seleccionado_excepcion);

                if (!tpHoraSalida.SelectedTime.HasValue)
                    throw new SOTException(Textos.Errores.hora_salida_no_seleccionado_excepcion);

                fechaSalida = new DateTime(dpFechaSalida.SelectedDate.Value.Year, dpFechaSalida.SelectedDate.Value.Month, dpFechaSalida.SelectedDate.Value.Day,
                                            tpHoraSalida.SelectedTime.Value.Hour, tpHoraSalida.SelectedTime.Value.Minute, tpHoraSalida.SelectedTime.Value.Second);
            }
            else
                fechaSalida = null;

            var controladorAsistencias = new ControladorAsistencias();

            if (idAsistencia == 0)
                controladorAsistencias.RegistrarAsistencia(empleado.Id, fechaEntrada, fechaSalida);
            else
                controladorAsistencias.ModificarAsistencia(idAsistencia, fechaEntrada, fechaSalida);

            MessageBox.Show(Textos.Mensajes.creacion_asistencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void CargarEmpleados()
        {
            if (!IsLoaded)
                return;

            var controladorEmpleados = new ControladorEmpleados();

            cbEmpleados.ItemsSource = controladorEmpleados.ObtenerEmpleadosFiltrados();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                CargarEmpleados();
        }
    }
}
