﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Asistencias
{
    /// <summary>
    /// Lógica de interacción para GestionAsistenciasForm.xaml
    /// </summary>
    public partial class GestionAsistenciasForm : Window
    {
        public GestionAsistenciasForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now.Date;

            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual.AddDays(1).AddSeconds(-1);
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEliminarAsistencia_Click(object sender, RoutedEventArgs e)
        {
            var asistencia = dgvAsistencias.SelectedItem as Modelo.Entidades.Asistencia;

            if (asistencia == null)
                throw new SOTException(Textos.Errores.asistencia_no_seleccionada_excepcion);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_asistencia, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorAsistencias = new ControladorAsistencias();
                controladorAsistencias.EliminarAsistencia(asistencia.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_asistencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarAsistencias();
            }
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarAsistencias();
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarAsistencias();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarAsistencias();
                }
            }
        }

        private void CargarAsistencias()
        {
            var controladorAsistencias = new ControladorAsistencias();

            dgvAsistencias.ItemsSource = controladorAsistencias.ObtenerAsistenciasFiltadas(txtNombre.Text, txtApellidoP.Text, txtApellidoM.Text, txtNumero.Text, dpFechaInicial.SelectedDate, dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.Date.AddDays(1).AddSeconds(-1) : dpFechaFinal.SelectedDate);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargarAsistencias();
            }
        }

        private void btnCrearAsistencia_Click(object sender, RoutedEventArgs e)
        {
            var edicionAsistenciaF = new EdicionAsistenciaForm();
            Utilidades.Dialogos.MostrarDialogos(this, edicionAsistenciaF);
            CargarAsistencias();
        }

        private void btnModificarAsistencia_Click(object sender, RoutedEventArgs e)
        {
            var asistencia = dgvAsistencias.SelectedItem as Modelo.Entidades.Asistencia;

            if (asistencia == null)
                throw new SOTException(Textos.Errores.asistencia_no_seleccionada_excepcion);

            var edicionAsistenciaF = new EdicionAsistenciaForm(asistencia);
            Utilidades.Dialogos.MostrarDialogos(this, edicionAsistenciaF);
            CargarAsistencias();
        }
    }
}
