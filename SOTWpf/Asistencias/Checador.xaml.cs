﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Asistencias
{
    /// <summary>
    /// Lógica de interacción para Checador.xaml
    /// </summary>
    public partial class Checador : Window
    {
        Modelo.Dtos.DtoResumenAsistencia ResumenActual 
        {
            get { return DataContext as Modelo.Dtos.DtoResumenAsistencia; }
            set 
            {
                if (value == null || !value.Entrada.HasValue || value.FinComida.HasValue)
                    tbModo.IsChecked = tbModo.IsEnabled = false;
                else if (DataContext != value)
                    tbModo.IsChecked = !(tbModo.IsEnabled = true);

                DataContext = value;
            }
        }


        public Checador()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Inicializar();
        }

        private void Inicializar(bool resetearTexto = true)
        {
            lector.Visibility = System.Windows.Visibility.Hidden;
            lector.Apagar();
            ResumenActual = null;

            if(resetearTexto)
                txtNumeroEmpleado.Text = "";
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            //var controladorEmpleado = new ControladorEmpleados();
            //DataContext = controladorEmpleado.ObtenerEmpleadoPorNumeroNoNull(txtNumeroEmpleado.Text);

            var controladorA = new ControladorAsistencias();
            ResumenActual = controladorA.ObtenerResumenUltimaAsistenciaAbierta(txtNumeroEmpleado.Text);


            lector.Visibility = System.Windows.Visibility.Visible;
            lector.Encender();
        }

        private void txtNumeroEmpleado_TextChanged(object sender, TextChangedEventArgs e)
        {
            Inicializar(false);
        }

        private void lector_MuestraCambiada(object sender, Seguridad.MuestraCambiadaEventArgs e)
        {
            if (ResumenActual == null)
                throw new SOTException(Textos.Errores.empleado_no_seleccionado_excepcion);

            var controlador = new ControladorAsistencias();

            Modelo.Entidades.Asistencia asistencia;

            if (tbModo.IsChecked == true && !ResumenActual.InicioComida.HasValue)
                asistencia = controlador.RegistrarSalidaAComer(ResumenActual.IdEmpleado, new Modelo.Seguridad.Dtos.DtoCredencial { TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella, ValorVerificarBin = e.Muestra });
            else if (tbModo.IsChecked == true && ResumenActual.InicioComida.HasValue && !ResumenActual.FinComida.HasValue)
                asistencia = controlador.RegistrarRegresoComer(ResumenActual.IdEmpleado, new Modelo.Seguridad.Dtos.DtoCredencial { TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella, ValorVerificarBin = e.Muestra });
            else if (tbModo.IsChecked == false)
                asistencia = controlador.RegistrarAsistencia(ResumenActual.IdEmpleado, new Modelo.Seguridad.Dtos.DtoCredencial { TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella, ValorVerificarBin = e.Muestra });
            else
                return;

            Inicializar();

            if (asistencia.FechaSalida.HasValue)
                MessageBox.Show(string.Format(Textos.Mensajes.salida_registrada, asistencia.FechaSalida.Value.ToString("dd/MM/yyyy hh:mm:ss tt")), Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else if (asistencia.FinComida.HasValue)
                MessageBox.Show(string.Format(Textos.Mensajes.regreso_comida_registrada, asistencia.FinComida.Value.ToString("dd/MM/yyyy hh:mm:ss tt")), Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else if (asistencia.InicioComida.HasValue)
                MessageBox.Show(string.Format(Textos.Mensajes.salida_comida_registrada, asistencia.InicioComida.Value.ToString("dd/MM/yyyy hh:mm:ss tt")), Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show(string.Format(Textos.Mensajes.entrada_registrada, asistencia.FechaEntrada.ToString("dd/MM/yyyy hh:mm:ss tt")), Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void lector_InicializacionFallida(object sender, RoutedEventArgs e)
        {
            throw new SOTException(Textos.Errores.error_inicializar_lector_huellas);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            lector.Apagar();
        }
    }
}
