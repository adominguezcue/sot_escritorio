﻿using Modelo.Entidades;
using SOTControladores.Utilidades;
using SOTControladores.Controladores;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.ConceptosGastos
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionConceptosForm.xaml
    /// </summary>
    public partial class ConfiguracionConceptosGastosForm : Window
    {

        private bool esNuevo;

        ObservableCollection<CheckableItemWrapper<Modelo.Entidades.CentroCostos>> tiposConceptos;

        ItemWrapper<ConceptoGasto> csWrapper;

        public ConfiguracionConceptosGastosForm()
        {
            InitializeComponent();

            tiposConceptos = new ObservableCollection<CheckableItemWrapper<Modelo.Entidades.CentroCostos>>();

            csWrapper = new ItemWrapper<ConceptoGasto>();

            DataContext = csWrapper;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as CheckableItemWrapper<Modelo.Entidades.CentroCostos>;

            foreach (var item in tiposConceptos)
            {
                if (item == marcado)
                    continue;

                item.IsChecked = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
            CargarConceptos();
            CargarTipos();
        }

        private void CargarConceptos()
        {
            var controlador = new ControladorConceptosGastos();

            var conceptos = controlador.ObtenerConceptosActivos();

            dgvConceptos.ItemsSource = conceptos;
        }

        private void CargarTipos()
        {

            tiposConceptos.Clear();

            var controladorCentroCostos = new ControladorCentrosCostos();

            foreach (var tipo in controladorCentroCostos.FILTRO(false))
                tiposConceptos.Add(new CheckableItemWrapper<Modelo.Entidades.CentroCostos> { Value = tipo });

            listaMarcable.ItemsSource = tiposConceptos;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;

            csWrapper.Value = new ConceptoGasto();

            foreach (var item in tiposConceptos)
            {
                item.IsChecked = false;
            }

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if (dgvConceptos.SelectedItem == null)
                throw new SOTException(Textos.Errores.multiples_conceptos_seleccionados);


            var concepto = dgvConceptos.SelectedItem as Modelo.Entidades.ConceptoGasto;

            csWrapper.Value = concepto;

            var tipoActual = tiposConceptos.FirstOrDefault(m => m.Value.Id == concepto.IdCentroCostos);

            if (tipoActual != null)
                tipoActual.IsChecked = true;

            esNuevo = false;

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (dgvConceptos.SelectedItem == null)
                throw new SOTException(Textos.Errores.multiples_conceptos_seleccionados);

            var concepto = dgvConceptos.SelectedItem as Modelo.Entidades.ConceptoGasto;

            if (MessageBox.Show(string.Format(Textos.Mensajes.eliminar_concepto_gastos, concepto.Concepto, concepto.NombreCentroCostosTmp), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorConceptosGastos();

                controlador.EliminarConcepto(concepto.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                Window_Loaded(null, null);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
            esNuevo = false;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;

            Window_Loaded(null, null);
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorConceptosGastos();

            var tipoSeleccionado = tiposConceptos.FirstOrDefault(m => m.IsChecked);

            if (tipoSeleccionado == null)
                throw new SOTException(Textos.Errores.tipo_concepto_no_seleccionado_exception);

            csWrapper.Value.IdCentroCostos = tipoSeleccionado.Value.Id;

            if (esNuevo)
                controlador.CrearConcepto(csWrapper.Value);

            else
                controlador.ModificarConcepto(csWrapper.Value);


            Window_Loaded(null, null);

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;

            if (esNuevo)
                MessageBox.Show(Textos.Mensajes.creacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show(Textos.Mensajes.modificacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
