﻿using Modelo.Almacen.Entidades;
using Modelo.Entidades;
using Modelo.Sistemas.Entidades.Dtos;
using Modelo.Sistemas.Entidades;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Transversal.Excepciones;
using Transversal.Excepciones.Seguridad;
using Transversal.Extensiones;

namespace SOTWpf
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal static ConfiguracionPuntosLealtad ConfiguracionPuntos { get; private set; }
        private static ParametrosSOT ConfiguracionActual { get; set; }

        internal static BitmapImage Imagotipo { get; private set; }

        internal static bool GestionarPuntosLealtad
        {
            get { return ConfiguracionPuntos != null; /*ConfiguracionActual == null || ConfiguracionActual.ManejaPuntosLealtad;*/ }
        }

        public static bool Reiniciar { get; set; }
        public static bool Actualizacion { get; internal set; }

        internal static void CargarConfiguracion()
        {
            ConfiguracionActual = null;
            ConfiguracionPuntos = null;

            ReportesSOT.Parametros.Logos.RutaLogoGeneral = null;

            try
            {
                var thisAssembly = Assembly.GetExecutingAssembly();
                //object[] attributes = thisAssembly.GetCustomAttributes(typeof(GuidAttribute), false);

                Guid idSistema = typeof(App).ObtenerGuidEnsamblado() ?? new Guid();

                //if (attributes.Length == 1)
                //{
                //    idSistema = Guid.Parse(((GuidAttribute)attributes[0]).Value);
                //}

                var controladorConfiguracionSistemas = new SOTControladores.Controladores.ControladorConfiguracionSistemas();

                var parametros = controladorConfiguracionSistemas.ObtenerParametros(idSistema);

                var config = new ParametrosSOT();

                foreach (var propiedad in typeof(ParametrosSOT).GetProperties())
                {
                    var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                    if (parametro != null)
                    {
                        propiedad.SetValue(config, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                    }
                }

                ConfiguracionActual = config;

                if (string.IsNullOrWhiteSpace(ConfiguracionActual?.ImagenReportes))
                    ReportesSOT.Parametros.Logos.RutaLogoGeneral = "";
                else
                {
                    try
                    {
                        var res = Current.Resources[ConfiguracionActual.ImagenReportes] as BitmapImage;
                        ReportesSOT.Parametros.Logos.RutaLogoGeneral = res?.UriSource.LocalPath ?? "";

                        if (res == null || res.UriSource.IsFile)
                        {
                            if (string.IsNullOrEmpty(ReportesSOT.Parametros.Logos.RutaLogoGeneral))
                                throw new SOTException("Imagen de reportes no encontrada: " + (ConfiguracionActual?.ImagenReportes ?? ""));

                            ReportesSOT.Parametros.Logos.RutaLogoGeneral = Path.Combine(Path.GetDirectoryName(thisAssembly.Location), ReportesSOT.Parametros.Logos.RutaLogoGeneral.StartsWith("/") ? ReportesSOT.Parametros.Logos.RutaLogoGeneral.Substring(1) : ReportesSOT.Parametros.Logos.RutaLogoGeneral);

                            if (!File.Exists(ReportesSOT.Parametros.Logos.RutaLogoGeneral))
                                ReportesSOT.Parametros.Logos.RutaLogoGeneral = "";
                        }
                        else
                        {
                            string pathImagen;

                            var controladorParametrosOperacion = new SOTControladores.Controladores.ControladorConfiguracionGlobal();
                            pathImagen = controladorParametrosOperacion.ObtenerConfiguracionGlobal()?.RutaReportes;

                            if(string.IsNullOrWhiteSpace(pathImagen))
                                pathImagen = Path.GetDirectoryName(thisAssembly.Location);

                            var encoder = new PngBitmapEncoder();
                            encoder.Frames.Add(BitmapFrame.Create(res));

                            var path = Path.Combine(pathImagen, "Resources\\Imagenes");

                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);

                            path = Path.Combine(path, idSistema.ToString());

                            using (var filestream = new FileStream(path, FileMode.Create))
                            {
                                encoder.Save(filestream);
                            }

                            ReportesSOT.Parametros.Logos.RutaLogoGeneral = path;
                        }
                    }
                    catch (Exception ex)
                    {
                        ProcesarError(ex, soloLog: true);

                        if (!File.Exists(ConfiguracionActual.ImagenReportes))
                            ReportesSOT.Parametros.Logos.RutaLogoGeneral = "";
                        else
                            ReportesSOT.Parametros.Logos.RutaLogoGeneral = ConfiguracionActual.ImagenReportes;
                    }
                }

                try
                {
                    Imagotipo = Current.Resources[ConfiguracionActual.Imagotipo] as BitmapImage;
                }
                catch (Exception ex)
                {
                    ProcesarError(ex, soloLog: true);
                }

                if (Imagotipo == null)
                    Imagotipo = new BitmapImage(new Uri(ConfiguracionActual.Imagotipo));
            }
            catch (Exception ex)
            {
                ProcesarError(ex, soloLog: true);
            }



            try
            {
                var controladorConfiguracionesPuntos = new SOTControladores.Controladores.ControladorVPoints();

                ConfiguracionPuntos = controladorConfiguracionesPuntos.ObtenerConfiguracion();
            }
            catch (Exception ex)
            {
                ProcesarError(ex, soloLog: true);
            }
        }

        /// <summary>
        /// Application Entry Point.
        /// </summary>
        [System.STAThreadAttribute()]
        //[System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        public static void Main(string[] args)
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            //System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            SOTWpf.App app = new SOTWpf.App();
            app.InitializeComponent();

            if (args.Length > 0 && args[0].Equals("RQ"))
                app.Run(new PrincipalRequisiciones());
            else
                app.Run(new Seguridad.IniciarSesionFormV2());

            if (Reiniciar && !Actualizacion)
                System.Windows.Forms.Application.Restart();
        }

        //protected override void OnExit(ExitEventArgs e)
        //{
        //    base.OnExit(e);
        //}

        public App()
        {
            Startup += new StartupEventHandler(App_Startup); // Can be called from XAML

            this.Exit += App_Exit;

            DispatcherUnhandledException += App_DispatcherUnhandledException; //Example 2

            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException; //Example 4

            System.Windows.Forms.Application.ThreadException += WinFormApplication_ThreadException; //Example 5


            Timeline.DesiredFrameRateProperty.OverrideMetadata(
                typeof(Timeline),
                new FrameworkPropertyMetadata { DefaultValue = 30 });
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            SOTWpf.Properties.ConexionSOT.Default.Save();
        }

        void App_Startup(object sender, StartupEventArgs e)
        {
            //Here if called from XAML, otherwise, this code can be in App()
            //AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException; // Example 1
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; // Example 3
            //AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        //private System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        //{
        //    return null;
        //}

        // Example 1
        void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            ProcesarError(e.Exception);
            //MessageBox.Show("1. CurrentDomain_FirstChanceException");
            //ProcessError(e.Exception);   - This could be used here to log ALL errors, even those caught by a Try/Catch block
        }

        // Example 2
        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ProcesarError(e.Exception);
            e.Handled = true;
        }

        private static void ProcesarError(Exception ex, bool procesoParalelo = false, bool soloLog = false)
        {
            //#if DEBUG
            try
            {
                if (ex.GetType() == typeof(DbEntityValidationException))
                {
                    var excepcion = (DbEntityValidationException)ex;

                    foreach (var er in excepcion.EntityValidationErrors)
                        Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                else if (ex.GetType() == typeof(DbUpdateException))
                {
                    //var excepcion = (DbUpdateException)ex;
                    //UpdateException inner = null;
                    Exception excepcion = null;

                    if (ex.InnerException != null && ex.InnerException.GetType() == typeof(UpdateException))
                    {
                        excepcion = ex.InnerException;

                        if (excepcion.InnerException != null)
                            excepcion = excepcion.InnerException;
                    }
                    else
                        excepcion = ex;

                    Transversal.Log.Logger.Fatal("[DbUpdateException] " + excepcion.Message);
                    //foreach (var er in excepcion.EntityValidationErrors)
                    //    Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                else if (ex.GetType() != typeof(SOTException))
                {
                    var exCurrent = ex;

                    for (int i = 0; i < 50 && exCurrent != null; i++, exCurrent = exCurrent.InnerException)
                    {
                        Transversal.Log.Logger.Fatal(exCurrent.Message);
                    }
                }
            }
            catch { }

            if (soloLog)
                return;

            //#endif
            ISnackContainer contenedora;
            try
            {
                contenedora = procesoParalelo ? null : Application.Current.Windows.OfType<ISnackContainer>().FirstOrDefault(x => x.IsActive);
            }
            catch
            {
                contenedora = null;
            }
            //var contenedora = MainWindow as ISnackContainer;

            if (contenedora != null)
            {
                contenedora.MostrarError(ex);
            }
            else if (ex.GetType() == typeof(SOTPermisosException))
            {
                var excepcion = (SOTPermisosException)ex;

                StringBuilder builder = new StringBuilder();
                builder.AppendLine(excepcion.Message);
                builder.AppendLine();

                foreach (var error in excepcion.Permisos)
                    builder.AppendLine(error);

                MessageBox.Show(builder.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (ex.GetType() == typeof(SOTAcumulativoException))
            {
                var excepcion = (SOTAcumulativoException)ex;

                StringBuilder builder = new StringBuilder();
                builder.AppendLine(excepcion.Message);
                builder.AppendLine();

                foreach (var error in excepcion.Iterar())
                    builder.AppendLine(error);

                MessageBox.Show(builder.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (ex.GetType() == typeof(DbUpdateException))
            {
                //var excepcion = (DbUpdateException)ex;
                //UpdateException inner = null;
                Exception excepcion = null;

                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(UpdateException))
                {
                    excepcion = ex.InnerException;

                    if (excepcion.InnerException != null)
                        excepcion = excepcion.InnerException;
                }
                else
                    excepcion = ex;

                MessageBox.Show(excepcion.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                //foreach (var er in excepcion.EntityValidationErrors)
                //    Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
            }
            else
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Example 3
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //MessageBox.Show("3. CurrentDomain_UnhandledException");
            var exception = e.ExceptionObject as Exception;
            ProcesarError(exception, true);
            //if (e.IsTerminating)
            //{
            //    //Now is a good time to write that critical error file!
            //    MessageBox.Show("Goodbye world!");
            //}
        }

        // Example 4
        void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            foreach (var ex in e.Exception.InnerExceptions)
                ProcesarError(ex, true);

            e.SetObserved();
        }

        // Example 5
        void WinFormApplication_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        
    }
}
