﻿using System;
using System.Collections.Generic;
using System.Linq;
using SOTWpf.Dtos;
using Modelo.Almacen.Entidades.Dtos;
using System.Windows;
using SOTControladores.Controladores;
using SOTControladores.Fabricas;

namespace SOTWpf.Inventarios
{
    /// <summary>
    /// Lógica de interacción para ReporteInventarioActual.xaml
    /// </summary>
    public partial class ReporteInventarioActual : Window
    {
        DtoResultadoBusquedaArticuloSimple ArticuloSeleccionado;

        string almacen = "";
        string articulo = "";
        string linea = "";
        string depto = "";
        List<Modelo.Entidades.Parciales.CatalogoAlmacen> reportealmacen = new List<Modelo.Entidades.Parciales.CatalogoAlmacen>();
        List<Modelo.Entidades.Parciales.CatalogoDepartamento> reportedepto = new List<Modelo.Entidades.Parciales.CatalogoDepartamento>();
        List<Modelo.Entidades.Parciales.CatalogoLinea> reportelinea = new List<Modelo.Entidades.Parciales.CatalogoLinea>();

        RoutedEventArgs eventarguments;
        public ReporteInventarioActual()
        {
            InitializeComponent();
        }
        private void BtnBotonReporteInventarioActual_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();
            System.DateTime localDate = System.DateTime.Now;
            var documentoReporte = new SOTWpf.Reportes.Inventarios.InventarioActual();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            {
                Direccion=config.Direccion,
                FechaInicio = localDate,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});
            var ArticulosInventarioActual = new ControladorReporteInventario().ObtieneArticulosInvActual(almacen, articulo, linea, depto);
            if (ArticulosInventarioActual != null && ArticulosInventarioActual.Count > 0)
            {

                LlenaAlmacenes(ArticulosInventarioActual);
                LlenaDeptos(ArticulosInventarioActual);
                LlenaLineas(ArticulosInventarioActual);
                documentoReporte.Subreports["DetallesInventarioActual"].Database.Tables[0].SetDataSource(reportealmacen);
                documentoReporte.Subreports["DetallesInventarioActual"].Database.Tables[1].SetDataSource(ArticulosInventarioActual);
                documentoReporte.Subreports["DetallesInventarioActual"].Database.Tables[2].SetDataSource(reportedepto);
                documentoReporte.Subreports["DetallesInventarioActual"].Database.Tables[3].SetDataSource(reportelinea);
            }
            else
            {
                Utilidades.Dialogos.Aviso("No hay productos favor de validar los filtros");
                return;
            }

            var visorR = new VisorReportes();
            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();
            Utilidades.Dialogos.MostrarDialogos(this, visorR);

        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void LlenaAlmacenes(List<Modelo.Entidades.Parciales.ArticulosInventarioActual> articulosInventarioActual)
        {
            reportealmacen = new List<Modelo.Entidades.Parciales.CatalogoAlmacen>();
            var almacenes = articulosInventarioActual.GroupBy(s => s.CodigoAlmacen);
            foreach (var group in almacenes)
            {
                bool bandera = true;
                foreach (Modelo.Entidades.Parciales.ArticulosInventarioActual item in group)
                {
                    if (bandera == true)
                    {
                        Modelo.Entidades.Parciales.CatalogoAlmacen catalogoAlmacen = new Modelo.Entidades.Parciales.CatalogoAlmacen();
                        catalogoAlmacen.idCodigo = item.CodigoAlmacen;
                        catalogoAlmacen.DescripcionAlmacen = item.Des_Almacen;
                        reportealmacen.Add(catalogoAlmacen);
                    }

                    bandera = false;
                }
            }
        }

        private void LlenaLineas(List<Modelo.Entidades.Parciales.ArticulosInventarioActual> articulosInventarioActual)
        {
            reportelinea = new List<Modelo.Entidades.Parciales.CatalogoLinea>();
            var lineas = articulosInventarioActual.GroupBy(s => s.CodigoLigea);
            foreach (var group in lineas)
            {
                bool bandera = true;
                foreach (Modelo.Entidades.Parciales.ArticulosInventarioActual item in group)
                {
                    if (bandera == true)
                    {
                        Modelo.Entidades.Parciales.CatalogoLinea catalogoAlmacen = new Modelo.Entidades.Parciales.CatalogoLinea();
                        catalogoAlmacen.codigolinea = item.CodigoLigea;
                        catalogoAlmacen.descripcionlinea = item.Des_Linea;
                        reportelinea.Add(catalogoAlmacen);
                    }

                    bandera = false;
                }
            }
        }

        private void LlenaDeptos(List<Modelo.Entidades.Parciales.ArticulosInventarioActual> articulosInventarioActual)
        {
            reportedepto = new List<Modelo.Entidades.Parciales.CatalogoDepartamento>();
            var deptos = articulosInventarioActual.GroupBy(s => s.CodigoDepto);
            foreach (var group in deptos)
            {
                bool bandera = true;
                foreach (Modelo.Entidades.Parciales.ArticulosInventarioActual item in group)
                {
                    if (bandera == true)
                    {
                        Modelo.Entidades.Parciales.CatalogoDepartamento catalogodepto = new Modelo.Entidades.Parciales.CatalogoDepartamento();
                        catalogodepto.codigoDepto = item.CodigoDepto;
                        catalogodepto.descripciondepto = item.Des_Depto;
                        reportedepto.Add(catalogodepto);
                    }

                    bandera = false;
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            eventarguments = e;
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var CatAlmacen = new ControladorReporteInventario().ObtieneCatalogoAlmacen(depto, linea);
                CatAlmacen.Insert(0, new Modelo.Entidades.Parciales.CatalogoAlmacen { DescripcionAlmacen = "Todos" });
                cbCatalogoAlmacen.ItemsSource = CatAlmacen;



                var CatDepto = new ControladorReporteInventario().ObtieneCatalogoDepto(depto);
                CatDepto.Insert(0, new Modelo.Entidades.Parciales.CatalogoDepartamento { descripciondepto = "Todos" });
                cbCatalogoDepartamento.ItemsSource = CatDepto;
                cbCatalogoDepartamento.IsEnabled = true;

                /*var CatLinea = new ControladorReporteInventario().ObtienecatalogoLinea(depto);
                CatLinea.Insert(0, new Modelo.Entidades.Parciales.CatalogoLinea { descripcionlinea = "Todos" });
                cbCatalogLinea.ItemsSource = CatLinea;*/
                cbCatalogLinea.IsEnabled = false;
                txtalmacen.IsEnabled = false;
                txtlinea.IsEnabled = false;
                txtalDepto.IsEnabled = false;
            }
        }

        private void CbCatalogoAlmacen_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

            CargaArticulos();
            cbCatalogoDepartamento.IsEnabled = true;
        }

        private void Txt_busquedarticulo_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            articulo = txt_busquedarticulo.Text;
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            var fBusqueda = FabricaBuscadores.ObtenerBuscadorArticulos();
            fBusqueda.SoloInventariables = true;
            fBusqueda.Mostrar();
            if (fBusqueda.ArticuloSeleccionado != null)
            {
                txt_busquedarticulo.Text = fBusqueda.ArticuloSeleccionado.Nombre;
                articulo = fBusqueda.ArticuloSeleccionado.Codigo;
            }
        }


        private void CargaArticulos()
        {
            /////Consulta el inventario 
            ///
            if (((Modelo.Entidades.Parciales.CatalogoAlmacen)cbCatalogoAlmacen.SelectedItem) != null)
            {
                if (((Modelo.Entidades.Parciales.CatalogoAlmacen)cbCatalogoAlmacen.SelectedItem).idCodigo.ToString().Equals("0"))
                {
                    almacen = "";
                    txtalmacen.Text = "Todos";
                }
                else
                {
                    almacen = ((Modelo.Entidades.Parciales.CatalogoAlmacen)cbCatalogoAlmacen.SelectedItem).idCodigo.ToString();
                    txtalmacen.Text = ((Modelo.Entidades.Parciales.CatalogoAlmacen)cbCatalogoAlmacen.SelectedItem).DescripcionAlmacen.ToString();
                }
            }

            if (((Modelo.Entidades.Parciales.CatalogoLinea)cbCatalogLinea.SelectedItem) != null)
            {
                if (((Modelo.Entidades.Parciales.CatalogoLinea)cbCatalogLinea.SelectedItem).codigolinea.ToString().Equals("0"))
                {
                    linea = "";
                    depto = "";
                    txtlinea.Text = "Todos";
                }
                else
                {
                    linea = ((Modelo.Entidades.Parciales.CatalogoLinea)cbCatalogLinea.SelectedItem).codigolinea.ToString();
                    depto = ((Modelo.Entidades.Parciales.CatalogoLinea)cbCatalogLinea.SelectedItem).codigodepto.ToString();
                    txtlinea.Text = ((Modelo.Entidades.Parciales.CatalogoLinea)cbCatalogLinea.SelectedItem).descripcionlinea.ToString();
                }
            }
            

            if (((Modelo.Entidades.Parciales.CatalogoDepartamento)cbCatalogoDepartamento.SelectedItem) != null)
            {
                if (((Modelo.Entidades.Parciales.CatalogoDepartamento)cbCatalogoDepartamento.SelectedItem).codigoDepto.ToString().Equals("0"))
                {
                    depto = "";
                    txtalDepto.Text = "Todos";
                }
                else
                {
                    depto = ((Modelo.Entidades.Parciales.CatalogoDepartamento)cbCatalogoDepartamento.SelectedItem).codigoDepto.ToString();
                    txtalDepto.Text= ((Modelo.Entidades.Parciales.CatalogoDepartamento)cbCatalogoDepartamento.SelectedItem).descripciondepto.ToString();
                }
            }

           // Window_Loaded(this, eventarguments);
        }

        private void CbCatalogLinea_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            CargaArticulos();
        }

        private void CbCatalogoDepartamento_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

            CargaArticulos();
            var CatLinea = new ControladorReporteInventario().ObtienecatalogoLinea(depto);
            CatLinea.Insert(0, new Modelo.Entidades.Parciales.CatalogoLinea { descripcionlinea = "Todos" });
            cbCatalogLinea.ItemsSource = CatLinea;
            this.cbCatalogLinea.SelectedIndex = 0;
            cbCatalogLinea.IsEnabled = true;
        }

    }
}
