﻿using SOTControladores.Controladores;
using SOTWpf.Articulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Inventarios
{
    /// <summary>
    /// Lógica de interacción para KardexForm.xaml
    /// </summary>
    public partial class KardexForm : Window
    {
        public KardexForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
            }
        }

        private void CargarKardex()
        {
            throw new NotImplementedException();
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
            }
        }

        private void txtArticulo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F3)
            {
                var bf = new BusquedaArticulosForm();

                Utilidades.Dialogos.MostrarDialogos(this, bf);

                txtArticulo.Text = bf.ArticuloSeleccionado != null ? bf.ArticuloSeleccionado.Codigo : "";
                this.Focus();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                cbAlmacenes.ItemsSource = new SOTControladores.Controladores.ControladorAlmacenes().ObtenerAlmacenes();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnReporteArticulosVendidos_Click(object sender, RoutedEventArgs e)
        {
            if (IsLoaded)
            {
                if (!dpFechaInicial.SelectedDate.HasValue)
                    throw new SOTException("Seleccione una fecha inicial");
                if (!dpFechaFinal.SelectedDate.HasValue)
                    throw new SOTException("Seleccione una fecha final");

                var items = new SOTControladores.Controladores.ControladorInventarios().SP_ZctKdxInv(txtArticulo.Text, (int)cbAlmacenes.SelectedValue, dpFechaInicial.SelectedDate.Value, dpFechaFinal.SelectedDate.Value.AddDays(1).AddSeconds(-1));

                if (items.Count == 0)
                {
                    MessageBox.Show("No hay información para mostrar", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

                    return;
                }

                var controladorGlobal = new ControladorConfiguracionGlobal();
                var config = controladorGlobal.ObtenerConfiguracionGlobal();

                var documentoReporte = new SOTWpf.Reportes.Inventarios.ZctRptKdxInvSP();
                documentoReporte.Load();

                var visorR = new VisorReportes();

                

                documentoReporte.SetDataSource(items);

                visorR.crCrystalReportViewer.ReportSource = documentoReporte;

                visorR.UpdateLayout();

                Utilidades.Dialogos.MostrarDialogos(this, visorR);
            }
        }

    }
}
