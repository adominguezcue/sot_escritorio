﻿using Modelo.Almacen.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Fichas
{
    /// <summary>
    /// Lógica de interacción para GestionFichasForm.xaml
    /// </summary>
    public partial class GestionFichasForm : Window
    {
        public GestionFichasForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaIngreso.Language = lang;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                dpFechaIngreso.SelectedDate = DateTime.Now;
                CargarFichas();
            }
        }

        private void CargarFichas()
        {
            var controladorFichas = new ControladorFichas();

            dgvFichas.ItemsSource = controladorFichas.ObtenerFichasDia(dpFechaIngreso.SelectedDate.Value.Date, txtNombre.Text);
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarFichas();
        }

        private void dpFechaIngreso_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarFichas();
        }

        private void btnAltaFicha_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionFichaForm());
            CargarFichas();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnModificarFicha_Click(object sender, RoutedEventArgs e)
        {
            var ficha = dgvFichas.SelectedItem as Ficha;

            if (ficha == null)
                throw new SOTException(Textos.Errores.ficha_no_seleccionada_exception);

            Utilidades.Dialogos.MostrarDialogos(this, new EdicionFichaForm(ficha.Id));
            CargarFichas();
        }

        private void btnEliminarFicha_Click(object sender, RoutedEventArgs e)
        {
            var ficha = dgvFichas.SelectedItem as Ficha;

            if (ficha == null)
                throw new SOTException(Textos.Errores.ficha_no_seleccionada_exception);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_ficha, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorFichas = new SOTControladores.Controladores.ControladorFichas();
                controladorFichas.EliminarFicha(ficha.Id);
                CargarFichas();

                MessageBox.Show(Textos.Mensajes.eliminacion_ficha_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void archivosCVS_filter(object sender, FilterEventArgs e)
        {

        }
    }
}
