﻿using Microsoft.Win32;
using Modelo.Almacen.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Fichas
{
    /// <summary>
    /// Lógica de interacción para EdicionFichaForm.xaml
    /// </summary>
    public partial class EdicionFichaForm : Window
    {
        int idFicha;
        Ficha FichaActual 
        {
            get { return DataContext as Ficha; }
            set { DataContext = value; }
        }
        public EdicionFichaForm(int idFicha = 0)
        {
            InitializeComponent();
            this.idFicha = idFicha;

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaIngreso.Language = lang;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorFichas = new ControladorFichas();


            var clon = FichaActual.ObtenerCopiaDePrimitivas();
            clon.Id = FichaActual.Id;

            if (FichaActual.Id == 0)
            {
                foreach (var archivo in FichaActual.ArchivosFicha)
                {
                    var clonArchivo = archivo.ObtenerCopiaDePrimitivas();
                    clonArchivo.Id = archivo.Id;
                    clonArchivo.IdFicha = archivo.IdFicha;
                    clon.ArchivosFicha.Add(clonArchivo);
                }

                controladorFichas.AgregarFicha(clon);
                MessageBox.Show(Textos.Mensajes.creacion_ficha_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var archivos = new List<Modelo.Almacen.Entidades.Dtos.DtoArchivoFicha>();

                foreach (var archivo in FichaActual.ArchivosFicha.Where(m => m.Activa))
                {
                    archivos.Add(new Modelo.Almacen.Entidades.Dtos.DtoArchivoFicha
                    {
                        Nombre = archivo.Nombre,
                        Valor = archivo.Valor
                    });
                }

                controladorFichas.ModificarFicha(clon, archivos);
                MessageBox.Show(Textos.Mensajes.modificacion_ficha_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorProveedores = new ControladorProveedores();

                cbProveedores.ItemsSource = controladorProveedores.ObtenerProveedores();

                var controladorActivosFijos = new ControladorArticulos();

                cbActivos.ItemsSource = controladorActivosFijos.ObtenerActivosFijos();

                if (idFicha == 0)
                    FichaActual = new Ficha 
                    { 
                        Activa = true,
                        FechaIngreso = DateTime.Now
                    };
                else 
                {
                    var controladorFichas = new ControladorFichas();
                    FichaActual = controladorFichas.ObtenerPorId(idFicha);
                }
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ArchivoFicha;

            if (item != null)
            {

                OpenFileDialog openFileDialog = ObtenerDialogo();

                if (openFileDialog.ShowDialog() == true)
                {
                    var bin = File.ReadAllBytes(openFileDialog.FileName);
                    var nom = System.IO.Path.GetFileName(openFileDialog.FileName);

                    item.Valor = bin;
                    item.Nombre = nom;
                }
            }

            CollectionViewSource.GetDefaultView(dgArchivos.ItemsSource).Refresh();
        }

        private OpenFileDialog ObtenerDialogo()
        {
            return new OpenFileDialog 
            {
                Filter = "Archivo jpg (*.jpg, *.jpeg)|*.JPG;*JPEG|Archivo png (*.png)|*PNG"
            };
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ArchivoFicha;

            if (item != null)
            {
                if (item.Id == 0)
                    FichaActual.ArchivosFicha.Remove(item);
                else
                    item.Activa = false;
            }

            CollectionViewSource.GetDefaultView(dgArchivos.ItemsSource).Refresh();
        }

        private void btnAgregarArchivo_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = ObtenerDialogo();

            if (openFileDialog.ShowDialog() == true)
            {
                var bin = File.ReadAllBytes(openFileDialog.FileName);
                var nom = System.IO.Path.GetFileName(openFileDialog.FileName);

                if (FichaActual.ArchivosFicha.Any(m => m.Nombre.ToUpper() == nom.ToUpper() && m.Activa))
                    throw new SOTException(Textos.Errores.archivo_ficha_repetido_excepcion, nom);

                FichaActual.ArchivosFicha.Add(new ArchivoFicha 
                { 
                    Activa = true,
                    Valor = bin,
                    Nombre = nom
                });
            }

            CollectionViewSource.GetDefaultView(dgArchivos.ItemsSource).Refresh();
        }

        private void archivosCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ArchivoFicha;

            e.Accepted = item != null && item.Activa;
        }

        private void btnDescargar_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ArchivoFicha;

            if (item != null)
            {
                SaveFileDialog sfd = new SaveFileDialog
                {
                    Filter = "Archivo jpg (*.jpg, *.jpeg)|*.JPG;*JPEG|Archivo png (*.png)|*PNG",
                    FileName = item.Nombre
                };

                if (sfd.ShowDialog() == true)
                {
                    using (System.IO.FileStream fs = (System.IO.FileStream)sfd.OpenFile())
                    {
                        fs.Write(item.Valor, 0, item.Valor.Length);

                        fs.Close();
                    }
                }

            }
        }
    }
}
