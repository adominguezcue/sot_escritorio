﻿using SOTControladores.Controladores;
using SOTWpf.Asistencias;
using SOTWpf.Properties;
using SOTWpf.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Timers;
using Newtonsoft.Json;
using SOTWpf.Actualizaciones;
using SOTWpf.Utilidades;
using System.Reflection;
using System.Diagnostics;
using Modelo.Seguridad;

namespace SOTWpf.Seguridad
{
    /// <summary>
    /// Lógica de interacción para IniciarSesionFormV2.xaml
    /// </summary>
    public partial class IniciarSesionFormV2 : Window
    {
        

        public IniciarSesionFormV2()
        {
            InitializeComponent();
            capturador.ColumnasIniciarSesion = 1;
            capturador.OcultarSalida = true;

            

            txtVersionEnsamblado.Text = "Versión " + GestorActualizaciones.Instancia.InstalledVersion.ToString();

            

//#if DEBUG

//            txtUsuario.Text = "SISTEMAS";
//            txtContrasena.Password = "Pass123";
//#endif

        }

        
        

        internal static void CerrarSesion()
        {
            var controladorS = new ControladorSesiones();

            controladorS.CerrarSesion();
        }

        private void btnBar_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalCocinaBar("Bar"));
        }

        private void btnCocina_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalCocinaBar("Cocina"));
        }

        private void btnConfigurarConexion_Click(object sender, RoutedEventArgs e)
        {
            MostarPanelConexion();
        }

        private void btnHotel_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalHotel());
        }

        private void btnRestaurante_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalRestaurante());
        }

        private void CambiarVentana(Window ventana, bool regresarAlCerrar = true)
        {
            Application.Current.MainWindow = ventana;

            App.Reiniciar = regresarAlCerrar;

            ventana.Show();
            Close();

            //if (regresarAlCerrar)
            //{
            //    Hide();
            //    if (ventana is PrincipalHotel)
            //    {
            //        ventana.ShowDialog();
            //        CerrarProcesos();
            //    }
            //    else
            //        ventana.ShowDialog();
            //    Application.Current.MainWindow = this;
            //    Show();
            //}
            //else
            //{
            //    ventana.Show();
            //    Close();
            //}
        }

        private void CerrarProcesos()
        {
            try
            {
                CerrarSesion();
                cambiador.SelectedIndex--;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error de incio", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void IniciarSesion_Click(object sender, RoutedEventArgs e)
        {
            //var controladorS = new ControladorSesiones();

            //var credencial = new Modelo.Seguridad.Dtos.DtoCredencial
            //{
            //    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
            //    ValorVerificarStr = txtContrasena.Password,
            //    IdentificadorUsuario = txtUsuario.Text
            //};

            //controladorS.IniciarSesion(credencial);

            //App.CargarConfiguracion();

            //cambiador.SelectedIndex++;
        }

        //private void InicaSesionHuella(Modelo.Seguridad.Dtos.DtoCredencial dtoCredencial)
        //{
        //    var controladorS = new ControladorSesiones();
        //    controladorS.IniciarSesion(dtoCredencial);
        //    App.CargarConfiguracion();

        //    cambiador.SelectedIndex++;
        //}


        private void IrChecador_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new Checador(), false);
        }

        private void MostarPanelConexion()
        {
            Utilidades.Dialogos.MostrarDialogos(this, new Sistema.EditorConexion());
        }

        private void regresar_Click(object sender, RoutedEventArgs e)
        {
            CerrarSesion();

            cambiador.SelectedIndex--;
        }

        private void Salir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CerrarSesion();
            }
            finally
            {
                Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //GestorActualizaciones.Instancia.Apagar();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                //if (!Properties.ConexionSOT.Default.EsConfiguradoSOT)
                //    MostarPanelConexion();

                EditorConexion ventana = null;

                try
                {


                    ventana = new EditorConexion();

                    ventana.Conexion = ConexionSOT.Default;

                    if (!ConexionSOT.Default.EsConfiguradoSOT)
                    {
                        Utilidades.Dialogos.Aviso("Se ha detectado que es la primera vez que se ejecuta la aplicación, se recomienda configurar las cadenas de conexión a la base de datos.");
                        MostarPanelConexion();
                    }
                    else
                    {
                        ventana.CambiarConexion(true);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error de incio", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    if (ventana != null)
                        ventana.Close();
                }

                GestorActualizaciones.Instancia.Cargar();
            }
        }

        private void CapturadorCredenciales_Salida(object sender, RoutedEventArgs e)
        {
            if (capturador.Valido)
            {
                var credencial = capturador.Credencial;

                var controladorS = new ControladorSesiones();

                controladorS.IniciarSesion(credencial);

                App.CargarConfiguracion();

                cambiador.SelectedIndex++;
            }
        }
    }
}
