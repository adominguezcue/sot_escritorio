﻿using SOTControladores.Controladores;
using SOTWpf.Asistencias;
using SOTWpf.Properties;
using SOTWpf.Sistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AutoUpdaterDotNET;
using System.Windows.Threading;
using System.Timers;
using Newtonsoft.Json;
using SOTWpf.Actualizaciones;
using SOTWpf.Utilidades;
using System.Reflection;
using System.Diagnostics;
using Modelo.Seguridad;					   

namespace SOTWpf.Seguridad
{
    /// <summary>
    /// Lógica de interacción para IniciarSesionForm.xaml
    /// </summary>
    public partial class IniciarSesionForm : Window
    {
        Timer timer;

        public IniciarSesionForm()
        {
            InitializeComponent();

            InstalledVersion = Assembly.GetExecutingAssembly().GetName().Version;

            txtVersionEnsamblado.Text = "Versión " + InstalledVersion.ToString();

            AutoUpdater.ParseUpdateInfoEvent += AutoUpdaterOnParseUpdateInfoEvent;
            AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;
            AutoUpdater.ReportErrors = false;

#if DEBUG

            txtUsuario.Text = "SISTEMAS";
            txtContrasena.Password = "Pass123";
#endif

        }

        private delegate void UpdateDelegate();
        private Version InstalledVersion { get; set; }

        internal static void CerrarSesion()
        {
            var controladorS = new ControladorSesiones();

            controladorS.CerrarSesion();
        }

        private void Actualizar()
        {
            string ruta = null;

            try
            {

                //ruta = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().RutaActualizacionesJSON;
#if DEBUG
                ruta = string.Empty;
#elif QA
                ruta = "http://www.gmsweb.com.mx/sot_test/ActualizacionSOT.json";
#else
                ruta = "http://www.gmsweb.com.mx/sot/ActualizacionSOT.json";
#endif
            }
            catch (Exception ex)
            {
                Transversal.Log.Logger.Error("[UPDATE]: " + ex.Message);

                if (ex.InnerException != null)
                    Transversal.Log.Logger.Error("[UPDATE]: " + ex.InnerException.Message);
            }

            Dispatcher.BeginInvoke(new UpdateDelegate(() =>
                {
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(ruta))
                            AutoUpdater.Start(ruta);
                    }
                    catch (Exception ex)
                    {
                        Transversal.Log.Logger.Error("[UPDATE]: " + ex.Message);

                        if (ex.InnerException != null)
                            Transversal.Log.Logger.Error("[UPDATE]: " + ex.InnerException.Message);
                    }
                }));
        }

        private void AutoUpdaterOnCheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            var extraArgs = args as UpdateInfoExtendedEventArgs;

            if (extraArgs != null && (extraArgs.IsUpdateAvailable || extraArgs.ResourceMode))
            {
                if (extraArgs.ResourceMode)
                {
                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new ActualizacionExtrasForm(extraArgs.ExtraUpdates.First()));
                }
                else if (extraArgs.IsUpdateAvailable)
                {
                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new ActualizacionForm(extraArgs));
                }
            }
        }

        private void AutoUpdaterOnParseUpdateInfoEvent(ParseUpdateInfoEventArgs args)
        {
            var ext = JsonConvert.DeserializeObject<UpdateInfoExtendedEventArgs>(args.RemoteData);
            args.UpdateInfo = ext;

            if (InstalledVersion >= args.UpdateInfo.CurrentVersion)
            {
                try
                {
                    if (ext.Extras != null && ext.Extras.Reportes != null)
                    {
                        var rutaReportes = "C:\\reportes\\";//new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().RutaReportes;

                        if (!string.IsNullOrWhiteSpace(rutaReportes))
                        {
                            rutaReportes = rutaReportes.Trim();

                            if (!rutaReportes.EndsWith("\\"))
                                rutaReportes += "\\";

                            var version = ext.Extras.Reportes.Version - 1;

                            if (System.IO.File.Exists(rutaReportes + Constantes.ARCHIVO_VERSION_REPORTE))
                            {
                                using (var file = System.IO.File.OpenText(rutaReportes + Constantes.ARCHIVO_VERSION_REPORTE))
                                {
                                    if (!int.TryParse(file.ReadToEnd(), out version))
                                        throw new Exception("Erro al leer la versión");
                                }
                            }

                            if (ext.Extras.Reportes.Version > version)
                            {
                                if (ext.ExtraUpdates == null)
                                    ext.ExtraUpdates = new List<ExtraUpdate>();

                                ext.Extras.Reportes.Name = "Reportes";
                                ext.Extras.Reportes.LocalPath = rutaReportes;
                                ext.Extras.Reportes.InstalledVersion = version;
                                ext.ExtraUpdates.Add(ext.Extras.Reportes);

                                args.UpdateInfo.CurrentVersion = new Version("0.0.0." + ext.Extras.Reportes.Version.ToString());
                                args.UpdateInfo.InstalledVersion = new Version("0.0.0." + ext.Extras.Reportes.InstalledVersion.ToString());
                                args.UpdateInfo.DownloadURL = ext.Extras.Reportes.DownloadURL;
                                args.UpdateInfo.InstallerArgs = "\"%source%\" \"" + ext.Extras.Reportes.LocalPath + "Launcher.exe\" \"" + Process.GetCurrentProcess().MainModule.FileName + "\"";

                                ext.ResourceMode = true;
                            }
                        }
                    }
                }
                catch
                {
                }
            }
        }

        private void btnBar_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalCocinaBar("Bar"));
        }

        private void btnCocina_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalCocinaBar("Cocina"));
        }

        private void btnConfigurarConexion_Click(object sender, RoutedEventArgs e)
        {
            MostarPanelConexion();
        }

        private void btnHotel_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalHotel());
        }

        private void btnRestaurante_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new PrincipalRestaurante());
        }

        private void CambiarVentana(Window ventana, bool regresarAlCerrar = true)
        {
            Application.Current.MainWindow = ventana;

            if (regresarAlCerrar)
            {
                Hide();
                if (ventana.Title.Contains("Hotel"))
                {													
					ventana.ShowDialog();
                    CerrarProcesos();
                }									 
                else
                    ventana.ShowDialog();				 			   
                Application.Current.MainWindow = this;
                Show();
            }
            else
            {
                ventana.Show();
                Close();
            }
        }

        private void CerrarProcesos()
        {

            try
            {
                CerrarSesion();
                cambiador.SelectedIndex--;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error de incio", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
		
		
        private void IniciarSesion_Click(object sender, RoutedEventArgs e)
        {
            var controladorS = new ControladorSesiones();

            var credencial = new Modelo.Seguridad.Dtos.DtoCredencial
            {
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                ValorVerificarStr = txtContrasena.Password,
                IdentificadorUsuario = txtUsuario.Text
            };

            controladorS.IniciarSesion(credencial);

            App.CargarConfiguracion();

            cambiador.SelectedIndex++;
        }

        private void InicaSesionHuella(Modelo.Seguridad.Dtos.DtoCredencial dtoCredencial)
        {
            var controladorS = new ControladorSesiones();
            controladorS.IniciarSesion(dtoCredencial);
            App.CargarConfiguracion();

            cambiador.SelectedIndex++;
        }
		
		
        private void IrChecador_Click(object sender, RoutedEventArgs e)
        {
            CambiarVentana(new Checador(), false);
        }

        private void MostarPanelConexion()
        {
            Utilidades.Dialogos.MostrarDialogos(this, new Sistema.EditorConexion());
        }

        private void regresar_Click(object sender, RoutedEventArgs e)
        {
            CerrarSesion();

            cambiador.SelectedIndex--;
        }

        private void Salir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CerrarSesion();
            }
            finally
            {
                Close();
            }
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            if (timer.Interval < 300000)
                timer.Interval = 300000;

            Actualizar();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Elapsed -= timer_Tick;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                //if (!Properties.ConexionSOT.Default.EsConfiguradoSOT)
                //    MostarPanelConexion();

                EditorConexion ventana = null;

                try
                {


                    ventana = new EditorConexion();

                    ventana.Conexion = ConexionSOT.Default;

                    if (!ConexionSOT.Default.EsConfiguradoSOT)
                    {
                        Utilidades.Dialogos.Aviso("Se ha detectado que es la primera vez que se ejecuta la aplicación, se recomienda configurar las cadenas de conexión a la base de datos.");
                        MostarPanelConexion();
                    }
                    else
                    {
                        ventana.CambiarConexion(true);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error de incio", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    if (ventana != null)
                        ventana.Close();
                }

                timer = new Timer { Interval = 1000 };
                timer.Elapsed += timer_Tick;
                timer.Start();

                try
                {
                    var controladorS = new ControladorSesiones();
                    var Datoscredencial = controladorS.HuellaInicioSesion();
                    if (Datoscredencial != null)
                    {
                        InicaSesionHuella(Datoscredencial);
                    }
                }
                catch(Exception ex)
                {
                    //CambiarVentana(new PrincipalHotel());
                }
                //txtUsuario.Text = Datoscredencial.
                //txtContrasena.Password = "Pass123";																	 
                //try
                //{
                //    Actualizar();
                //}
                //finally 
                //{ 
                //    timer.Start();
                //}
            }
        }
    }
}
