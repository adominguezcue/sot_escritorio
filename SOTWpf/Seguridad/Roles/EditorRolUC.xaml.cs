﻿using Modelo.Seguridad.Entidades;
using SOTWpf.Pagos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Dtos;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.Seguridad.Roles
{
    /// <summary>
    /// Lógica de interacción para EditorRolUC.xaml
    /// </summary>
    public partial class EditorRolUC : UserControl
    {
        public static readonly RoutedEvent GuardadoExitosoEvent = EventManager.RegisterRoutedEvent("GuardadoExitoso", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EditorRolUC));
        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EditorRolUC));

        public event RoutedEventHandler GuardadoExitoso
        {
            add
            {
                AddHandler(GuardadoExitosoEvent, value);
            }

            remove
            {
                RemoveHandler(GuardadoExitosoEvent, value);
            }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add
            {
                AddHandler(EdicionCanceladaEvent, value);
            }

            remove
            {
                RemoveHandler(EdicionCanceladaEvent, value);
            }
        }

        object bloqueador = new object();
        bool cargandoPropiedad = false;

        Rol RolActual
        {
            get { return DataContext as Rol; }
            set
            {

                cargandoPropiedad = true;
                try
                {
                    CargarDataContext(value);
                }
                finally
                {
                    cargandoPropiedad = false;
                }

            }
        }

        private void CargarDataContext(Rol value)
        {
            lock (bloqueador)
            {
                DataContext = value;

                if (value != null && value.Permisos != null)
                {
                    //icCategorias.ItemsSource = Newtonsoft.Json.JsonConvert.DeserializeObject<Modelo.Seguridad.Dtos.DtoPermisos>(value.Permisos).CategorizarPropiedades();
                    var collectionViewSource = this.FindResource("categoriasCVS") as CollectionViewSource;

                    var categorias = Newtonsoft.Json.JsonConvert.DeserializeObject<Modelo.Seguridad.Dtos.DtoPermisos>(value.Permisos).CategorizarPropiedades();

                    //var jsonXD = Newtonsoft.Json.JsonConvert.SerializeObject(categorias);

                    collectionViewSource.Source = new ObservableCollection<DtoCategoriaPropiedad>(categorias);
                }
            }
        }

        public EditorRolUC()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            //{
            //    if (idRol != 0)
            //    {
            //        var controladorRoles = new SOTControladores.Controladores.ControladorRoles();

            //        RolActual = controladorRoles.ObtenerRolConPermisos(idRol);
            //    }
            //    else
            //    {
            //        RolActual = new Rol { Activo = true, Permisos = "{}" };
            //    }
            //}
        }

        public void Guardar()
        {
            if (RolActual != null)
            {
                var clon = RolActual.ObtenerCopiaDePrimitivas();
                clon.Id = RolActual.Id;
                clon.Permisos = "{}";

                foreach (var pp in RolActual.PermisosPagos)
                {
                    var clonPp = pp.ObtenerCopiaDePrimitivas();
                    clonPp.Id = pp.Id;
                    clonPp.IdRol = pp.IdRol;
                    clon.PermisosPagos.Add(clonPp);
                }

                var collectionViewSource = this.FindResource("categoriasCVS") as CollectionViewSource;
                var categorias = collectionViewSource.Source as ObservableCollection<Transversal.Dtos.DtoCategoriaPropiedad>;

                //var categorias = icCategorias.ItemsSource as List<Transversal.Dtos.DtoCategoriaPropiedad>;
                var permiso = new Modelo.Seguridad.Dtos.DtoPermisos();

                foreach (var propiedad in categorias.SelectMany(m => m.Propiedades))
                    propiedad.Propiedad.SetValue(permiso, propiedad.Valor);

                clon.Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(permiso);

                var controladorRoles = new SOTControladores.Controladores.ControladorRoles();

                if (RolActual.Id == 0)
                {
                    controladorRoles.CrearRol(clon);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.creacion_elemento_exitosa);
                }
                else
                {
                    controladorRoles.ModificarRol(clon);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.modificacion_elemento_exitosa);
                }

                RaiseEvent(new RoutedEventArgs(GuardadoExitosoEvent));
            }
        }

        public void CancelarEdicion()
        {
            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        private void permisosPagosCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as PermisoPago;

            e.Accepted = item != null && item.Activo;
        }

        private void btnAgregarPermisoPago_Click(object sender, RoutedEventArgs e)
        {
            var selectorPagoSimple = new SelectorFormaPagoSimple();

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selectorPagoSimple);

            if (selectorPagoSimple.PagoSeleccionado.HasValue)
            {
                var ppExistente = RolActual.PermisosPagos.FirstOrDefault(m => m.FormaPago == selectorPagoSimple.PagoSeleccionado.Value);

                if (ppExistente != null && ppExistente.Activo)
                    throw new SOTException(Textos.Errores.forma_pago_ya_seleccionada_exception, selectorPagoSimple.PagoSeleccionado.Value.Descripcion());

                if (ppExistente == null)
                    RolActual.PermisosPagos.Add(new PermisoPago
                    {
                        Activo = true,
                        FormaPago = selectorPagoSimple.PagoSeleccionado.Value
                    });
                else
                {
                    ppExistente.Activo = true;
                    ppExistente.FechaEliminacion = null;
                    ppExistente.IdUsuarioElimino = null;
                }
            }

            CollectionViewSource.GetDefaultView(dgPermisosPagos.ItemsSource).Refresh();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as PermisoPago;

            if (item != null)
            {
                if (item.Id == 0)
                    RolActual.PermisosPagos.Remove(item);
                else
                    item.Activo = false;
            }

            CollectionViewSource.GetDefaultView(dgPermisosPagos.ItemsSource).Refresh();
        }

        //private void CheckBox_Checked(object sender, RoutedEventArgs e)
        //{

        //}

        //private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        //{

        //}

        private void txtBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(icCategorias.ItemsSource).Refresh();
        }

        private void propiedadesCVS_filter(object sender, FilterEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtBusqueda.Text))
            {
                var item = e.Item as DtoPropiedad;

                e.Accepted = item != null && item.Descripcion.ToUpper().Contains(txtBusqueda.Text.ToUpper());
            }
            else
                e.Accepted = true;
        }

        private void categoriasCVS_filter(object sender, FilterEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtBusqueda.Text))
            {
                var item = e.Item as DtoCategoriaPropiedad;

                e.Accepted = item != null && item.Propiedades.Any(m => m.Descripcion.ToUpper().Contains(txtBusqueda.Text.ToUpper()));
            }
            else
                e.Accepted = true;

            //if (e.Accepted)
            //{
            //    var c = sender as Control;

            //    var collectionViewSource = c.FindResource("propiedadesCVS") as CollectionViewSource;

            //    collectionViewSource.View.Refresh();
            //}
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!cargandoPropiedad)
                CargarDataContext(e.NewValue as Rol);
        }
    }
}
