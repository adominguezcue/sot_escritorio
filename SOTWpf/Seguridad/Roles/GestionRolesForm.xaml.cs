﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Seguridad.Roles
{
    /// <summary>
    /// Lógica de interacción para GestionRolesForm.xaml
    /// </summary>
    public partial class GestionRolesForm : Window
    {
        public GestionRolesForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarRoles();
            }
        }

        private void CargarRoles()
        {
            var controladorRoles = new SOTControladores.Controladores.ControladorRoles();

            tabla.ItemsSource = controladorRoles.ObtenerRoles();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var rol = (sender as Control).DataContext as Modelo.Seguridad.Entidades.Rol;

            if (rol != null)
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionRolForm(rol.Id));

            CargarRoles();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var rol = (sender as Control).DataContext as Modelo.Seguridad.Entidades.Rol;

            if (rol != null && MessageBox.Show(string.Format(Textos.Mensajes.confirmar_eliminacion_rol, rol.Nombre), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorRoles = new SOTControladores.Controladores.ControladorRoles();
                controladorRoles.EliminarRol(rol.Id);

                CargarRoles();
            }
        }

        private void btnCrearRol_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionRolForm());

            CargarRoles();
        }
    }
}
