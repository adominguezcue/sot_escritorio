﻿using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Seguridad.Roles
{
    /// <summary>
    /// Lógica de interacción para EdicionRolFormV2.xaml
    /// </summary>
    public partial class EdicionRolFormV2 : Window
    {
        int idRol;

        public EdicionRolFormV2(int idRol = 0)
        {
            InitializeComponent();

            this.idRol = idRol;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            editorRol.Guardar();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            editorRol.CancelarEdicion();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (idRol != 0)
                {
                    var controladorRoles = new SOTControladores.Controladores.ControladorRoles();

                    editorRol.DataContext = controladorRoles.ObtenerRolConPermisos(idRol);
                }
                else
                {
                    editorRol.DataContext = new Rol { Activo = true, Permisos = "{}" };
                }
            }
        }

        private void EditorRol_GuardadoExitoso(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void EditorRol_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
