﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Seguridad;

namespace SOTWpf.Seguridad
{
    public class InscriptorHuellaDigital : CapturadorHuellaDigital
    {
        private DPFP.Processing.Enrollment Enroller;
        //bool confirmar;
        //byte[] muestraBin = null;

        protected override void Init()
        {
            base.Init();

            Enroller = new DPFP.Processing.Enrollment();

            //confirmar = false;
        }

        protected override MuestraCambiadaEventArgs Process(DPFP.Sample Sample)
        {
            MuestraCambiadaEventArgs resultado = null;
            //if (confirmar)
            //{
            //    var confirmacion = new byte[Sample.Size];
            //    Sample.Serialize(ref confirmacion);

            //    SetPrompt("Proceso exitoso");

            //    Stop();

            //    var resultado = new MuestraCambiadaEventArgs(muestraBin, confirmacion);
            //    muestraBin = null;
            //    return resultado;
            //}
            //else
            //{
            DPFP.FeatureSet features = GestorHuella.ExtraerCaracteristicas(Sample, DPFP.Processing.DataPurpose.Enrollment);

            // Check quality of the sample and add to enroller if it's good
            if (features != null)
                try
                {
                    MakeReport("The fingerprint feature set was created.");
                    Enroller.AddFeatures(features);		// Add feature set to template.
                }
                finally
                {
                    UpdateStatus();
                    // Check if template has been created.
                    switch (Enroller.TemplateStatus)
                    {
                        case DPFP.Processing.Enrollment.Status.Ready:
                            var muestraBin = new byte[Sample.Size];
                            var plantilla = Enroller.Template;
                            plantilla.Serialize(ref muestraBin);

                            var confirmacion = new byte[Sample.Size];
                            Sample.Serialize(ref confirmacion);

                            resultado = new MuestraCambiadaEventArgs(muestraBin, confirmacion);

                            //Enroller.Clear();
                            SetPrompt("Proceso exitoso");
                            Enroller.Clear();
                            Stop();
                            break;
                        case DPFP.Processing.Enrollment.Status.Failed:	// report failure and restart capturing
                            Enroller.Clear();
                            resultado = null;
                            Stop();
                            //confirmar = false;
                            UpdateStatus();
                            Start();
                            break;
                        case DPFP.Processing.Enrollment.Status.Unknown:
                            Enroller.Clear();
                            resultado = null;
                            Stop();
                            //confirmar = false;
                            SetPrompt("Evento desconocido, comience de nuevo");
                            Start();
                            break;
                    }
                }
            //}

            return resultado;
        }

        private void UpdateStatus()
        {
            //if (confirmar)
            // Show number of samples needed.
            SetPrompt(String.Format("Muestras requeridas: {0}", Enroller.FeaturesNeeded));
            //else
            //    SetPrompt("Confirmar huella");
        }

    }
}
