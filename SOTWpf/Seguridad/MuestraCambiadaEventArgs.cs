﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SOTWpf.Seguridad
{
    public class MuestraCambiadaEventArgs : RoutedEventArgs
    {
        public byte[] Muestra { get; private set; }

        public byte[] Confirmacion { get; private set; }

        public MuestraCambiadaEventArgs(byte[] muestra, byte[] confirmacion = null)
        {
            Muestra = muestra;
            Confirmacion = confirmacion;
        }
    }
}
