﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Seguridad
{
    /// <summary>
    /// Lógica de interacción para CapturadorHuellaDigital.xaml
    /// </summary>
    public partial class CapturadorHuellaDigital : UserControl, DPFP.Capture.EventHandler
    {
        private DPFP.Capture.Capture Capturer;

        public delegate void MuestraCambiadaEventHandler(object sender, MuestraCambiadaEventArgs e);

        public static readonly RoutedEvent MuestraCambiadaEvent =
            EventManager.RegisterRoutedEvent(
                "MuestraCambiada",
                RoutingStrategy.Bubble,
                typeof(MuestraCambiadaEventHandler),
                typeof(CapturadorHuellaDigital));

        public static readonly RoutedEvent InicializacionFallidaEvent =
            EventManager.RegisterRoutedEvent(
                "InicializacionFallida",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(CapturadorHuellaDigital));

        public static readonly RoutedEvent DesconexionEvent =
            EventManager.RegisterRoutedEvent(
                "Desconexion",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(CapturadorHuellaDigital));

        public static readonly RoutedEvent ReconexionEvent =
            EventManager.RegisterRoutedEvent(
                "Reconexion",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(CapturadorHuellaDigital));

        public event MuestraCambiadaEventHandler MuestraCambiada
        {
            add { AddHandler(MuestraCambiadaEvent, value); }
            remove { RemoveHandler(MuestraCambiadaEvent, value); }
        }

        public event RoutedEventHandler InicializacionFallida
        {
            add { AddHandler(InicializacionFallidaEvent, value); }
            remove { RemoveHandler(InicializacionFallidaEvent, value); }
        }

        public event RoutedEventHandler Desconexion
        {
            add { AddHandler(DesconexionEvent, value); }
            remove { RemoveHandler(DesconexionEvent, value); }
        }

        public event RoutedEventHandler Reconexion
        {
            add { AddHandler(ReconexionEvent, value); }
            remove { RemoveHandler(ReconexionEvent, value); }
        }

        public CapturadorHuellaDigital()
        {
            InitializeComponent();

            //Encender();
        }

        public void OnComplete(object Capture, string ReaderSerialNumber, DPFP.Sample Sample)
        {
            MakeReport("The fingerprint sample was captured.");
            //SetPrompt("Scan the same fingerprint again.");
            var resultado = Process(Sample);

            if (resultado != null)
            {
                //var args = new MuestraCambiadaEventArgs(resultado);
                resultado.RoutedEvent = MuestraCambiadaEvent;
                //RaiseEvent(args);
                Dispatcher.Invoke(delegate()
                {
                    try
                    {
                        RaiseEvent(resultado);
                    }
                    catch (System.Reflection.TargetInvocationException ex)
                    {
                        SetPrompt(ex.InnerException.Message);
                    }
                    catch (Exception e)
                    {
                        SetPrompt(e.Message);
                    }
                });
            }
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {
            MakeReport("The finger was removed from the fingerprint reader.");
        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {
            MakeReport("The fingerprint reader was touched.");
        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {
            SetPrompt("El lector de huellas ha sido conectado");

            Dispatcher.Invoke(delegate()
            {
                try
                {
                    RaiseEvent(new RoutedEventArgs(ReconexionEvent));
                }
                catch (System.Reflection.TargetInvocationException ex)
                {
                    SetPrompt(ex.InnerException.Message);
                }
                catch (Exception e)
                {
                    SetPrompt(e.Message);
                }
            });
        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {
            SetPrompt("El lector de huellas ha sido desconectado");

            Dispatcher.Invoke(delegate()
            {
                try
                {
                    RaiseEvent(new RoutedEventArgs(DesconexionEvent));
                }
                catch (System.Reflection.TargetInvocationException ex)
                {
                    SetPrompt(ex.InnerException.Message);
                }
                catch (Exception e)
                {
                    SetPrompt(e.Message);
                }
            });
        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {
            if (CaptureFeedback == DPFP.Capture.CaptureFeedback.Good)
                MakeReport("The quality of the fingerprint sample is good.");
            else
                MakeReport("The quality of the fingerprint sample is poor.");
        }

        protected void MakeReport(string p)
        {
            
        }
        protected void SetPrompt(string p)
        {
           Dispatcher.Invoke(delegate()
           {
               estado.Text = p;
           });
        }
        protected virtual void Init()
        {
            try
            {
                Capturer = new DPFP.Capture.Capture();				// Create a capture operation.

                if (null != Capturer)
                    Capturer.EventHandler = this;					// Subscribe for capturing events.
                else
                    SetPrompt("No se puede inciar la captura");
            }
            catch
            {
                //MessageBox.Show("Can't initiate capture operation!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
               Dispatcher.Invoke(delegate()
               {
                   try 
                   {
                       RaiseEvent(new RoutedEventArgs(InicializacionFallidaEvent));
                   }
                   catch (System.Reflection.TargetInvocationException ex)
                   {
                       SetPrompt(ex.InnerException.Message);
                   }
                   catch(Exception e)
                   {
                       SetPrompt(e.Message);
                   }
               });
            }
        }
        protected void Start()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StartCapture();
                    SetPrompt("Coloque su dedo");
                }
                catch
                {
                    SetPrompt("No se puede inciar la captura");
                }
            }
        }
        protected void Stop()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StopCapture();
                    SetPrompt("");
                }
                catch
                {
                    SetPrompt("No se puede detener la captura");
                }
            }
        }
        protected virtual MuestraCambiadaEventArgs Process(DPFP.Sample Sample)
        {
            byte[] muestraBin = new byte[Sample.Size];
            Sample.Serialize(ref muestraBin);

            return new MuestraCambiadaEventArgs(muestraBin);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            //{
                //Init();
                //Start();
            //}
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                //Stop();
        }

        internal void Encender()
        {
            Init();
            Start();
        }

        internal void Apagar()
        {
            Stop();
        }
    }
}
