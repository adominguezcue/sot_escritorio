﻿using SOTControladores.Controladores;
using SOTControladores.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Seguridad
{
    /// <summary>
    /// Lógica de interacción para CapturaHuellaDigitalForm.xaml
    /// </summary>
    public partial class CapturaHuellaDigitalForm : Window, ICapturadorHuellaDigital
    {
        public CapturaHuellaDigitalForm()
        {
            InitializeComponent();
            //cu = new ControladorPermisos();
        }

        //private bool valido = false;
        //ControladorPermisos cu;

        public Modelo.Seguridad.Dtos.DtoCredencial Credencial
        {
            get;
            private set;
        }

        //private void lector_MuestraCambiada(object sender, MuestraCambiadaEventArgs e)
        //{
        //    valido = false;
        //    Credencial = new Modelo.Seguridad.Dtos.DtoCredencial
        //    {
        //        TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
        //        ValorVerificarBin = e.Muestra
        //    };

        //    cu.ObtenerUsuarioPorCredencial(Credencial);
        //    valido = true;
        //    Close();
        //}

        //private void lector_InicializacionFallida(object sender, RoutedEventArgs e)
        //{
        //    txtError.Text = Textos.Errores.error_inicializar_lector_huellas;
        //    cambiador.SelectedIndex = 1;
        //}

        //private void Aceptar_Click(object sender, RoutedEventArgs e)
        //{
        //    valido = false;
        //    Credencial = new Modelo.Seguridad.Dtos.DtoCredencial
        //    {
        //        TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
        //        ValorVerificarStr = txtContrasena.Password,
        //        IdentificadorUsuario = txtUsuario.Text
        //    };

        //    cu.ObtenerUsuarioPorCredencial(Credencial);
        //    valido = true;
        //    Close();
        //}

        //private void Salir_Click(object sender, RoutedEventArgs e)
        //{
        //    valido = false;
        //    Close();
        //}

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!capturador.Valido)
                Credencial = null;
            else
                Credencial = capturador.Credencial;

            //lector.Apagar();
        }

        //private void lector_Desconexion(object sender, RoutedEventArgs e)
        //{
        //    txtError.Text = Textos.Errores.lector_huellas_desconectado_exception;
        //    cambiador.SelectedIndex = 1;
        //    valido = false;
        //}

        //private void lector_Reconexion(object sender, RoutedEventArgs e)
        //{
        //    txtError.Text = Textos.Mensajes.lector_huellas_reconectado;
        //    cambiador.SelectedIndex = 0;
        //    valido = false;
        //}

        public void MostrarDialogo()
        {
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, this);
        }

        private void capturador_Salida(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //private void Window_Loaded(object sender, RoutedEventArgs e)
        //{
        //    lector.Encender();
        //}

        //private void Window_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if(e.Key == Key.Escape)
        //    {
        //        valido = false;
        //        Close();
        //    }
        //}
    }
}
