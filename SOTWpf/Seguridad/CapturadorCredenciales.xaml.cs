﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Seguridad
{
    /// <summary>
    /// Lógica de interacción para CapturadorCredenciales.xaml
    /// </summary>
    public partial class CapturadorCredenciales : UserControl
    {
        public static readonly RoutedEvent SalidaEvent = EventManager.RegisterRoutedEvent(nameof(Salida), RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CapturadorCredenciales));

        public event RoutedEventHandler Salida
        {
            add
            {
                AddHandler(SalidaEvent, value);
            }

            remove
            {
                RemoveHandler(SalidaEvent, value);
            }
        }

        public CapturadorCredenciales()
        {
            InitializeComponent();
            cu = new ControladorPermisos();
        }

        public bool Valido { get; private set; }

        public int ColumnasIniciarSesion
        {
            get { return gridContrasena.Columns; }
            set { gridContrasena.Columns = value; }
        }

        public bool OcultarSalida
        {
            get { return btnSalida.Visibility == Visibility.Collapsed; }
            set { btnSalida.Visibility = value ? Visibility.Collapsed : Visibility.Visible; }
        }

        readonly ControladorPermisos cu;

        public Modelo.Seguridad.Dtos.DtoCredencial Credencial
        {
            get;
            private set;
        }

        private void lector_MuestraCambiada(object sender, MuestraCambiadaEventArgs e)
        {
            Valido = false;
            Credencial = new Modelo.Seguridad.Dtos.DtoCredencial
            {
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                ValorVerificarBin = e.Muestra
            };

            ProcesarCredencial();
        }

        private void lector_InicializacionFallida(object sender, RoutedEventArgs e)
        {
            txtError.Text = Textos.Errores.error_inicializar_lector_huellas;
            cambiador.SelectedIndex = 1;
        }

        private void Aceptar_Click(object sender, RoutedEventArgs e)
        {
            Aceptar();
        }

        private void ProcesarCredencial()
        {
            cu.ObtenerUsuarioPorCredencial(Credencial);
            Valido = true;
            RaiseEvent(new RoutedEventArgs(SalidaEvent));//Close();
        }

        private void Salir_Click(object sender, RoutedEventArgs e)
        {
            Salir();
        }

        //private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    if (!valido)
        //        Credencial = null;

        //    lector.Apagar();
        //}

        private void lector_Desconexion(object sender, RoutedEventArgs e)
        {
            txtError.Text = Textos.Errores.lector_huellas_desconectado_exception;
            cambiador.SelectedIndex = 1;
            Valido = false;
        }

        private void lector_Reconexion(object sender, RoutedEventArgs e)
        {
            txtError.Text = Textos.Mensajes.lector_huellas_reconectado;
            cambiador.SelectedIndex = 0;
            Valido = false;
        }

        private void CapturadorCredenciales_Loaded(object sender, RoutedEventArgs e)
        {
            LimpiarDatos();
            lector.Encender();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Valido = false;
                RaiseEvent(new RoutedEventArgs(SalidaEvent)); //Close();
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            lector.Apagar();
        }

        private void Aceptar()
        {
            Valido = false;
            Credencial = new Modelo.Seguridad.Dtos.DtoCredencial
            {
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                ValorVerificarStr = txtContrasena.Password,
                IdentificadorUsuario = txtUsuario.Text
            };

            ProcesarCredencial();
        }

        private void Salir()
        {
            Valido = false;
            RaiseEvent(new RoutedEventArgs(SalidaEvent)); //Close();
        }

        internal void LimpiarDatos()
        {
            txtUsuario.Clear();
            txtContrasena.Clear();
            Valido = false;
            Credencial = null;
        }

        //private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    if ((bool)e.NewValue)
        //    {
        //        LimpiarDatos();
        //        lector.Encender();
        //    }
        //    else 
        //    {
        //        lector.Apagar();
        //    }
        //}
    }
}
