﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Empleados
{
    /// <summary>
    /// Lógica de interacción para SeleccionadorValetsForm.xaml
    /// </summary>
    public partial class SeleccionadorValetsForm : Window
    {
        private ObservableCollection<EmpleadoWrapper> empleados;

        public bool ProcesoExitoso { get; private set; }

        public Modelo.Entidades.Empleado ValetSeleccionado { get; private set; }

        public SeleccionadorValetsForm()
        {
            InitializeComponent();
            empleados = new ObservableCollection<EmpleadoWrapper>();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!empleados.Any(m => m.Seleccionado))
                throw new SOTException(Textos.Errores.valet_no_seleccionado_exception);

            ValetSeleccionado = empleados.First(m => m.Seleccionado).EmpleadoBase;

            ProcesoExitoso = true;

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            ValetSeleccionado = null;
            ProcesoExitoso = false;
            Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as EmpleadoWrapper;

            foreach (var item in empleados)
            {
                if (item == marcado)
                    continue;

                item.Seleccionado = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var controladorEmpleados = new ControladorEmpleados();

            empleados.Clear();

            foreach (var item in controladorEmpleados.ObtenerValetsFILTRO())
                empleados.Add(new EmpleadoWrapper(item));

            //listaMarcable.SelectionMode = SelectionMode.Single;

            listaMarcable.ItemsSource = empleados;

        }
    }
}
