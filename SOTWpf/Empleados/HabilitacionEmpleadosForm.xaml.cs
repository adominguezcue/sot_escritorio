﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Empleados
{
    /// <summary>
    /// Lógica de interacción para HabilitacionEmpleadosForm.xaml
    /// </summary>
    public partial class HabilitacionEmpleadosForm : Window
    {
        readonly int[] idsPuestos;
        //Puesto puesto;
        DtoPuesto dtoPuesto;

        public HabilitacionEmpleadosForm(params int[] idsPuestos)
        {
            InitializeComponent();

            this.idsPuestos = idsPuestos;

            //puesto = new ControladorPuestos().ObtenerPuestoPorId(idPuesto);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var controlador = new ControladorEmpleados();

                var corteActual = new ControladorCortesTurno().ObtenerUltimoCorte();

                dtoPuesto = new DtoPuesto
                {
                    Nombre = "",//puesto.Nombre,
                    Empleados = corteActual != null ? controlador.ObtenerEmpleadosActivosPorPuesto(idsPuestos) : new List<Empleado>()
                };

                //foreach (var item in controlador.ObtenerEmpleadosActivosPorPuesto(nombrePuesto))
                //{
                //    puesto.Empleados.Add(new Empleado(item) { Seleccionado = item.Habilitado });
                //}

                //dtoPuesto.Empleados.ForEach(m => m.NombrePuesto = puesto.Nombre);

                DataContext = dtoPuesto;
            }
            catch 
            {
                Close();
                throw;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //if (puesto.Nombre == "Valet")
            //    throw new Exception("Se está trabajando con los valets");

            dtoPuesto.Empleados.ForEach(m => m.Habilitado = true);

            ((CollectionViewSource)this.Resources["existentes"]).View.Refresh();
            ((CollectionViewSource)this.Resources["habilitadas"]).View.Refresh();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //if (puesto.Nombre == "Valet")
            //    throw new Exception("Se está trabajando con los valets");

            dtoPuesto.Empleados.ForEach(m => m.Habilitado = false);

            ((CollectionViewSource)this.Resources["existentes"]).View.Refresh();
            ((CollectionViewSource)this.Resources["habilitadas"]).View.Refresh();
        }

        //private void CambiarPadre(object sender, RoutedEventArgs e)
        //{
        //    var item = ((Control)sender).DataContext as Empleado;

        //    if (puesto.Nombre == "Valet")
        //        throw new Exception("Se está trabajando con los valets");

        //    item.Habilitado = !item.Habilitado;

        //    ((CollectionViewSource)this.Resources["existentes"]).View.Refresh();
        //    ((CollectionViewSource)this.Resources["habilitadas"]).View.Refresh();
        //}

        private void existentes_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as Empleado;

            e.Accepted = item != null && !item.Habilitado;
        }

        private void habilitadas_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as Empleado;

            e.Accepted = item != null && item.Habilitado;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        class DtoPuesto
        {
            public string Nombre { get; set; }
            public List<Empleado> Empleados { get; set; }
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorEmpleados();

            if (dtoPuesto.Empleados.Count > 0)
                controlador.HabilitarDeshabilitarEmpleados(dtoPuesto.Empleados.ToDictionary(m => m.Id, m => m.Habilitado));

            Close();
        }

        private void CambiarPadre(object sender, MouseButtonEventArgs e)
        {
            var item = ((Control)sender).DataContext as Empleado;

            if (item == null)
                return;

            //if (puesto.Nombre == "Valet")
            //    throw new Exception("Se está trabajando con los valets");

            item.Habilitado = !item.Habilitado;

            ((CollectionViewSource)this.Resources["existentes"]).View.Refresh();
            ((CollectionViewSource)this.Resources["habilitadas"]).View.Refresh();
        }
    }
}
