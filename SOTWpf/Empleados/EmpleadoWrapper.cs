﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Empleados
{
    internal class EmpleadoWrapper : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public readonly Empleado EmpleadoBase;

        private bool _seleccionado;

        public bool Seleccionado
        {
            get
            {
                return _seleccionado;//Precio * Cantidad;
            }
            set
            {
                _seleccionado = value;
                NotifyPropertyChanged();
            }
        }

        public string NombreCompleto { get { return EmpleadoBase.NombreCompleto; } }

        public EmpleadoWrapper(Empleado empleado)
        {
            EmpleadoBase = empleado;
        }
    }
}
