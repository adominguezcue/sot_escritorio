﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Empleados
{
    /// <summary>
    /// Lógica de interacción para GestionEmpleadosForm.xaml
    /// </summary>
    public partial class GestionEmpleadosForm : Window
    {
        public GestionEmpleadosForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaIngreso.Language = lang;
        }

        private void btnEliminarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            var empleadoSeleccionado = dgvEmpleados.SelectedItem as Modelo.Entidades.Empleado;

            if (empleadoSeleccionado == null)
                throw new SOTException(Textos.Errores.empleado_no_seleccionado_excepcion);

            if (MessageBox.Show(string.Format(Textos.Mensajes.confirmar_eliminacion_empleado, empleadoSeleccionado.NombreCompleto),
                               Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorEmpleados = new ControladorEmpleados();

                controladorEmpleados.EliminarEmpleado(empleadoSeleccionado.Id);

                CargarEmpleados();

                MessageBox.Show(Textos.Mensajes.eliminacion_empleado_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnModificarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            var empleadoSeleccionado = dgvEmpleados.SelectedItem as Modelo.Entidades.Empleado;

            if (empleadoSeleccionado == null)
                throw new SOTException(Textos.Errores.empleado_no_seleccionado_excepcion);

            var edicionEmpleadoF = new EdicionEmpleadoForm(empleadoSeleccionado);
            Utilidades.Dialogos.MostrarDialogos(this, edicionEmpleadoF);

            CargarEmpleados();
        }

        private void btnAltaEmpleado_Click(object sender, RoutedEventArgs e)
        {
            var edicionEmpleadoF = new EdicionEmpleadoForm();
            Utilidades.Dialogos.MostrarDialogos(this, edicionEmpleadoF);

            CargarEmpleados();
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarEmpleados();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargarEmpleados();
            }
        }

        private void CargarEmpleados()
        {
            if (!IsLoaded)
                return;

            var controladorEmpleados = new ControladorEmpleados();

            dgvEmpleados.ItemsSource = controladorEmpleados.ObtenerEmpleadosFiltrados(txtNombre.Text, txtApellidoP.Text, txtApellidoM.Text, txtNumero.Text, dpFechaIngreso.SelectedDate);
        }

        private void dpFechaIngreso_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarEmpleados();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnCredenciales_Click(object sender, RoutedEventArgs e)
        {
            var empleadoSeleccionado = dgvEmpleados.SelectedItem as Modelo.Entidades.Empleado;

            if (empleadoSeleccionado == null)
                throw new SOTException(Textos.Errores.empleado_no_seleccionado_excepcion);

            var edicionEmpleadoF = new EdicionCredencialesForm(empleadoSeleccionado.Id);
            Utilidades.Dialogos.MostrarDialogos(this, edicionEmpleadoF);

            CargarEmpleados();
        }
    }
}
