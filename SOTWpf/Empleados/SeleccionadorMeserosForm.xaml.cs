﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Empleados
{
    /// <summary>
    /// Lógica de interacción para SeleccionadorMeserosForm.xaml
    /// </summary>
    public partial class SeleccionadorMeserosForm : Window
    {
        private ObservableCollection<EmpleadoWrapper> empleados;

        public bool ProcesoExitoso { get; private set; }

        public Modelo.Entidades.Empleado MeseroSeleccionado { get; private set; }

        public SeleccionadorMeserosForm()
        {
            InitializeComponent();
            empleados = new ObservableCollection<EmpleadoWrapper>();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!empleados.Any(m => m.Seleccionado))
                throw new SOTException(Textos.Errores.mesero_no_seleccionado_exception);

            MeseroSeleccionado = empleados.First(m => m.Seleccionado).EmpleadoBase;

            ProcesoExitoso = true;

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            MeseroSeleccionado = null;
            ProcesoExitoso = false;
            Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as EmpleadoWrapper;

            foreach (var item in empleados)
            {
                if (item == marcado)
                    continue;

                item.Seleccionado = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var controladorEmpleados = new ControladorEmpleados();

            empleados.Clear();

            foreach (var item in controladorEmpleados.ObtenerMeserosFILTRO())
                empleados.Add(new EmpleadoWrapper(item));

            //listaMarcable.SelectionMode = SelectionMode.Single;

            //empleados = (from em in empleados
            //             orderby em.Seleccionado descending, em.NombreCompleto
            //             select em).ToList();

            listaMarcable.ItemsSource = empleados;

        }
    }
}
