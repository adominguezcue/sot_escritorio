﻿using Modelo.Seguridad.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Empleados
{
    /// <summary>
    /// Lógica de interacción para EdicionCredencialesForm.xaml
    /// </summary>
    public partial class EdicionCredencialesForm : Window
    {
        int idEmpleado;
        List<Credencial.TiposCredencial> tipos = new List<Credencial.TiposCredencial>();

        Modelo.Seguridad.Entidades.Usuario usuarioActual
        {
            get { return DataContext as Modelo.Seguridad.Entidades.Usuario; }
            set
            {
                DataContext = value;
                credenciales.Clear();
                comprobaciones.Clear();
            }
        }

        public EdicionCredencialesForm(int idEmpleado)
        {
            InitializeComponent();

            this.idEmpleado = idEmpleado;

            if (this.idEmpleado != 0)
            {
                var controladorUsuario = new ControladorUsuarios();

                usuarioActual = controladorUsuario.ObtenerUsuarioPorEmpleado(this.idEmpleado) ?? new Modelo.Seguridad.Entidades.Usuario { IdEmpleado = this.idEmpleado };

                if (usuarioActual != null)
                {
                    tipos = controladorUsuario.ObtenerTiposCredencialesExistentes(usuarioActual.Id);

                    if (tipos.Contains(Credencial.TiposCredencial.Contrasena))
                        chbContrasena.Foreground = new SolidColorBrush(Colors.Green);
                    if (tipos.Contains(Credencial.TiposCredencial.Huella))
                        chbHuella.Foreground = new SolidColorBrush(Colors.Green);
                }
            }
        }


        List<Modelo.Seguridad.Entidades.Credencial> credenciales = new List<Modelo.Seguridad.Entidades.Credencial>();
        List<Modelo.Seguridad.Dtos.DtoCredencial> comprobaciones = new List<Modelo.Seguridad.Dtos.DtoCredencial>();

        private void lector_MuestraCambiada(object sender, Seguridad.MuestraCambiadaEventArgs e)
        {
            var index = credenciales.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);

            if (index < 0)
                credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    Valor = e.Muestra
                });
            else
                credenciales[index] = new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    Valor = e.Muestra
                };

            index = comprobaciones.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);

            if (index < 0)
                comprobaciones.Add(new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    ValorVerificarBin = e.Confirmacion
                });
            else
                comprobaciones[index] = new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    ValorVerificarBin = e.Confirmacion
                };

            lector.Apagar();
        }

        private void btnGuardarContrasena_Click(object sender, RoutedEventArgs e)
        {
            AgregarContrasena();
        }

        private void AgregarContrasena()
        {
            var index = credenciales.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);

            if (index < 0)
                credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = Transversal.Seguridad.Encriptador.Encriptar(txtContrasena.Password)
                });
            else
                credenciales[index] = new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = Transversal.Seguridad.Encriptador.Encriptar(txtContrasena.Password)
                };

            index = comprobaciones.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);

            if (index < 0)
                comprobaciones.Add(new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    ValorVerificarStr = txtConfirmacionContrasena.Password
                });
            else
                comprobaciones[index] = new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    ValorVerificarStr = txtConfirmacionContrasena.Password
                };
        }

        private void lector_InicializacionFallida(object sender, RoutedEventArgs e)
        {

        }

        private void lector_Desconexion(object sender, RoutedEventArgs e)
        {

        }

        private void lector_Reconexion(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            lector.Apagar();
        }

        private void btnEncender_Click(object sender, RoutedEventArgs e)
        {
            lector.Encender();
        }

        private void btnApagar_Click(object sender, RoutedEventArgs e)
        {
            lector.Apagar();
        }

        private void btnGuardarUsuario_Click(object sender, RoutedEventArgs e)
        {
            if (idEmpleado != 0)
            {
                AgregarContrasena();


                if (usuarioActual.Id != 0 && !usuarioActual.Activo)
                {
                    var controladorUsuario = new ControladorUsuarios();
                    controladorUsuario.EliminarUsuario(usuarioActual.Id);

                    usuarioActual = controladorUsuario.ObtenerUsuarioPorEmpleado(idEmpleado) ?? new Modelo.Seguridad.Entidades.Usuario { IdEmpleado = idEmpleado };
                }
                else if (usuarioActual.Activo)
                {
                    var credencialesEliminar = new List<Modelo.Seguridad.Entidades.Credencial>();

                    if (chbHuella.IsChecked == true && chbEliminarHuella.IsChecked == true)
                    {
                        credencialesEliminar.AddRange(credenciales.Where(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella));

                        credenciales.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);
                        comprobaciones.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);
                    }
                    else if (chbHuella.IsChecked == false)
                    {
                        credenciales.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);
                        comprobaciones.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);
                    }

                    if (chbContrasena.IsChecked == true && chbEliminarContrasena.IsChecked == true)
                    {
                        credencialesEliminar.AddRange(credenciales.Where(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena));

                        credenciales.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);
                        comprobaciones.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);
                    }
                    else if (chbContrasena.IsChecked == false)
                    {
                        credenciales.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);
                        comprobaciones.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);
                    }

                    var controladorUsuario = new ControladorUsuarios();

                    if (usuarioActual.Id == 0)
                    {
                        var clon = usuarioActual.ObtenerCopiaDePrimitivas();
                        clon.Id = usuarioActual.Id;

                        foreach (var credencial in credenciales)
                            clon.Credenciales.Add(credencial);

                        controladorUsuario.CrearUsuario(clon, comprobaciones);
                    }
                    else
                    {
                        controladorUsuario.ModificarUsuario(usuarioActual.Id, usuarioActual.Alias, credenciales, credencialesEliminar, comprobaciones);
                    }

                    usuarioActual = controladorUsuario.ObtenerUsuarioPorEmpleado(idEmpleado) ?? new Modelo.Seguridad.Entidades.Usuario { IdEmpleado = idEmpleado };
                }
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
