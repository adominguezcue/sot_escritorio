﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Empleados
{
    /// <summary>
    /// Lógica de interacción para EdicionEmpleadoForm.xaml
    /// </summary>
    public partial class EdicionEmpleadoForm : Window
    {
        Modelo.Entidades.Empleado empleadoActual
        {
            get { return DataContext as Modelo.Entidades.Empleado; }
            set
            {
                DataContext = value;
                tiCredenciales.IsEnabled = value.Id != 0;
            }
        }

        Modelo.Seguridad.Entidades.Usuario usuarioActual 
        {
            get { return tiCredenciales.DataContext as Modelo.Seguridad.Entidades.Usuario; }
            set 
            { 
                tiCredenciales.DataContext = value;
                credenciales.Clear();
                comprobaciones.Clear();
            }
        }

        List<Modelo.Seguridad.Entidades.Credencial> credenciales = new List<Modelo.Seguridad.Entidades.Credencial>();
        List<Modelo.Seguridad.Dtos.DtoCredencial> comprobaciones = new List<Modelo.Seguridad.Dtos.DtoCredencial>();


        public EdicionEmpleadoForm(Modelo.Entidades.Empleado empleado = null)
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaIngreso.Language = lang;

            empleadoActual = empleado ?? new Modelo.Entidades.Empleado { Activo = true, FechaIngreso = DateTime.Now };

            if (empleadoActual.Id != 0)
            {
                var controladorUsuario = new ControladorUsuarios();

                usuarioActual = controladorUsuario.ObtenerUsuarioPorEmpleado(empleadoActual.Id) ?? new Modelo.Seguridad.Entidades.Usuario { IdEmpleado = empleadoActual.Id };
            }
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorEmpleados = new ControladorEmpleados();

            if (empleadoActual.Id == 0)
                try
                {
                    controladorEmpleados.CrearEmpleado(empleadoActual);

                    MessageBox.Show(Textos.Mensajes.creacion_empleado_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch
                {
                    empleadoActual.Id = 0;
                    throw;
                }
            else
            {
                controladorEmpleados.ModificarEmpleado(empleadoActual);

                MessageBox.Show(Textos.Mensajes.modificacion_empleado_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorEmpleados = new ControladorEmpleados();
                var controladorAreas = new ControladorAreas();

                var areas = controladorAreas.ObtenerAreasActivasConPuestos();
                cbAreas.ItemsSource = areas;

                if (empleadoActual.Id != 0) 
                {
                    var area = areas.FirstOrDefault(p => p.Puestos.Any(m => m.Activo && m.Id == empleadoActual.IdPuesto));

                    if (area != null) 
                    {
                        cbAreas.SelectedItem = area;
                        var puesto = area.Puestos.FirstOrDefault(m => m.Id == empleadoActual.IdPuesto);

                        if (puesto != null)
                            cbPuestos.SelectedItem = puesto;
                    }
                }

                //cbPuestos.ItemsSource = controladorEmpleados.ObtenerPuestosActivos();
                cbCategorias.ItemsSource = controladorEmpleados.ObtenerCategoriasEmpleados();

                var controladorConfiguracionGlobal = new ControladorConfiguracionGlobal();
                cbTurno.ItemsSource = controladorConfiguracionGlobal.ObtenerConfiguracionesTurno();

                //lector.Encender();
            }
        }

        private void cbAreas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = cbAreas.SelectedItem as Modelo.Entidades.Area;

            if (item != null)
            {
                cbPuestos.ItemsSource = item.Puestos;
            }
            else 
            {
                cbPuestos.ItemsSource = null;
            }

            cbPuestos.SelectedIndex = -1;
        }

        private void lector_MuestraCambiada(object sender, Seguridad.MuestraCambiadaEventArgs e)
        {
            var index = credenciales.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);

            if (index < 0)
                credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    Valor = e.Muestra
                });
            else
                credenciales[index] = new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    Valor = e.Muestra
                };

            index = comprobaciones.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);

            if (index < 0)
                comprobaciones.Add(new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    ValorVerificarBin = e.Confirmacion
                });
            else
                comprobaciones[index] = new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella,
                    ValorVerificarBin = e.Confirmacion
                };

            lector.Apagar();
        }

        private void btnGuardarContrasena_Click(object sender, RoutedEventArgs e)
        {
            var index = credenciales.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);

            if (index < 0)
                credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = Transversal.Seguridad.Encriptador.Encriptar(txtContrasena.Password)
                });
            else
                credenciales[index] = new Modelo.Seguridad.Entidades.Credencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = Transversal.Seguridad.Encriptador.Encriptar(txtContrasena.Password)
                };

            index = comprobaciones.FindIndex(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);

            if (index < 0)
                comprobaciones.Add(new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    ValorVerificarStr = txtConfirmacionContrasena.Password
                });
            else
                comprobaciones[index] = new Modelo.Seguridad.Dtos.DtoCredencial
                {
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    ValorVerificarStr = txtConfirmacionContrasena.Password
                };
        }

        private void lector_InicializacionFallida(object sender, RoutedEventArgs e)
        {

        }

        private void lector_Desconexion(object sender, RoutedEventArgs e)
        {

        }

        private void lector_Reconexion(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            lector.Apagar();
        }

        private void btnEncender_Click(object sender, RoutedEventArgs e)
        {
            lector.Encender();
        }

        private void btnApagar_Click(object sender, RoutedEventArgs e)
        {
            lector.Apagar();
        }

        private void btnGuardarUsuario_Click(object sender, RoutedEventArgs e)
        {
            if (empleadoActual.Id != 0)
            {
                if (usuarioActual.Id != 0 && !usuarioActual.Activo)
                {
                    var controladorUsuario = new ControladorUsuarios();
                    controladorUsuario.EliminarUsuario(usuarioActual.Id);

                    usuarioActual = controladorUsuario.ObtenerUsuarioPorEmpleado(empleadoActual.Id) ?? new Modelo.Seguridad.Entidades.Usuario { IdEmpleado = empleadoActual.Id };
                }
                else if (usuarioActual.Activo)
                {
                    var credencialesEliminar = new List<Modelo.Seguridad.Entidades.Credencial>();

                    if (chbHuella.IsChecked == false)
                    {
                        credencialesEliminar.AddRange(credenciales.Where(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella));

                        credenciales.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);
                        comprobaciones.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Huella);
                    }
                    if (chbContrasena.IsChecked == false)
                    {
                        credencialesEliminar.AddRange(credenciales.Where(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena));

                        credenciales.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);
                        comprobaciones.RemoveAll(m => m.TipoCredencial == Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena);
                    }

                    var controladorUsuario = new ControladorUsuarios();

                    if (usuarioActual.Id == 0)
                    {
                        var clon = usuarioActual.ObtenerCopiaDePrimitivas();
                        clon.Id = usuarioActual.Id;

                        foreach (var credencial in credenciales)
                            clon.Credenciales.Add(credencial);

                        controladorUsuario.CrearUsuario(clon, comprobaciones);
                    }
                    else
                    {
                        controladorUsuario.ModificarUsuario(usuarioActual.Id, usuarioActual.Alias, credenciales, credencialesEliminar, comprobaciones);
                    }

                    usuarioActual = controladorUsuario.ObtenerUsuarioPorEmpleado(empleadoActual.Id) ?? new Modelo.Seguridad.Entidades.Usuario { IdEmpleado = empleadoActual.Id };
                }
            }
        }
    }
}
