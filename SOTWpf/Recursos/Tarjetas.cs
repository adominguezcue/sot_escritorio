using System;
using System.Windows.Media.Imaging;

namespace SOTWpf.Recursos
{
    public class Tarjetas
    {
        public static readonly BitmapImage tarjeta_american_express = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Tarjetas/tarjeta_american_express.png"));
        public static readonly BitmapImage tarjeta_master_card = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Tarjetas/tarjeta_master_card.png"));
        public static readonly BitmapImage tarjeta_visa = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Tarjetas/tarjeta_visa.png"));
    }
}
