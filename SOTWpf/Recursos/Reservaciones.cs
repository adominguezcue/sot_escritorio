using System;
using System.Windows.Media.Imaging;

namespace SOTWpf.Recursos
{
    public class Reservaciones
    {
        public static readonly BitmapImage icono_reservacion = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Reservaciones/icono_reservacion.png"));
        public static readonly BitmapImage icono_reservacion_cancelar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Reservaciones/icono_reservacion_cancelar.png"));
        public static readonly BitmapImage icono_reservacion_confirmar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Reservaciones/icono_reservacion_confirmar.png"));
        public static readonly BitmapImage icono_reservacion_editar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Reservaciones/icono_reservacion_editar.png"));
    }
}
