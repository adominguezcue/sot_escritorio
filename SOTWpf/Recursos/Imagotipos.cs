using System;
using System.Windows.Media.Imaging;

namespace SOTWpf.Recursos
{
    public class Imagotipos
    {
        public static readonly BitmapImage Imagotipo_Default = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Imagotipos/Imagotipo_Default.png"));
        public static readonly BitmapImage Imagotipo_LeReve = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Imagotipos/Imagotipo_LeReve.png"));
        public static readonly BitmapImage Imagotipo_VBoutique = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Imagotipos/Imagotipo_VBoutique.png"));
    }
}
