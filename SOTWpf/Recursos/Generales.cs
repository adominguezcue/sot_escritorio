using System;
using System.Windows.Media.Imaging;

namespace SOTWpf.Recursos
{
    public class Generales
    {
        public static readonly BitmapImage default_imagen = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/broken.png"));
        public static readonly BitmapImage huella = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/huella.png"));
        public static readonly BitmapImage icono_cancelar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/icono_cancelar.png"));
        public static readonly BitmapImage icono_mal = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/icono_mal.png"));
        public static readonly BitmapImage icono_codigo_barras = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/icono_codigo_barras.png"));
        public static readonly BitmapImage icono_tarjetaV_activar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/icono_tarjetaV_activar.png"));
        public static readonly BitmapImage tarjetaV_sobre = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/tarjetaV_sobre.png"));
        public static readonly BitmapImage icono_usuario = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/icono_usuario.png"));
        public static readonly BitmapImage icono_taxi = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/icono_taxi.png"));
        public static readonly BitmapImage icono_imprimir = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/icono_imprimir.png"));
        public static readonly BitmapImage icono_qr = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/icono_qr.png"));
        public static readonly BitmapImage agregar_elemento = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/agregar_elemento.png"));
        public static readonly BitmapImage editar_elemento = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/editar_elemento.png"));
        public static readonly BitmapImage eliminar_elemento = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/eliminar_elemento.png"));
        public static readonly BitmapImage deshacer_cambios = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Generales/deshacer_cambios.png"));
    }
}
