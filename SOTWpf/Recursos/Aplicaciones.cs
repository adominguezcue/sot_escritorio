using System;
using System.Windows.Media.Imaging;

namespace SOTWpf.Recursos
{
    public class Aplicaciones
    {
        public static readonly BitmapImage icono_inicio_bar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Aplicaciones/icono_inicio_bar.png"));
        public static readonly BitmapImage cocina_icono_en_preparacion = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Aplicaciones/cocina_icono_en_preparacion.png"));
        public static readonly BitmapImage cocina_icono_por_entregar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Aplicaciones/cocina_icono_por_entregar.png"));
        public static readonly BitmapImage icono_inicio_cocina = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Aplicaciones/icono_inicio_cocina.png"));
        public static readonly BitmapImage icono_inicio_hotel = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Aplicaciones/icono_inicio_hotel.png"));
        public static readonly BitmapImage icono_inicio_restaurante = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Aplicaciones/icono_inicio_restaurante.png"));
    }
}
