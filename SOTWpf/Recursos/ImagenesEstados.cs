using System;
using System.Windows.Media.Imaging;

namespace SOTWpf.Recursos
{
    public class ImagenesEstados
    {
        public static readonly BitmapImage estado_icono_bloqueada = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_bloqueada.png"));
        public static readonly BitmapImage estado_icono_cobrar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_cobrar.png"));
        public static readonly BitmapImage estado_icono_comanda = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_comanda.png"));
        public static readonly BitmapImage estado_icono_habilitada = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_habilitada.png"));
        public static readonly BitmapImage estado_icono_limpia = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_limpia.png"));
        public static readonly BitmapImage estado_icono_limpieza = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_limpieza.png"));
        public static readonly BitmapImage estado_icono_mantenimiento = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_mantenimiento.png"));
        public static readonly BitmapImage estado_icono_ocupada = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_ocupada.png"));
        public static readonly BitmapImage estado_icono_vencida = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_ocupada_exceso.png"));
        public static readonly BitmapImage estado_icono_preparada = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_preparada.png"));
        public static readonly BitmapImage estado_icono_reservada = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_reservada.png"));
        public static readonly BitmapImage estado_icono_revision = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_revision.png"));
        public static readonly BitmapImage estado_icono_sucia = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_sucia.png"));
        public static readonly BitmapImage estado_icono_tarjetaV = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_tarjetaV.png"));
        public static readonly BitmapImage estado_limpieza_detallado = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_limpieza_detallado.png"));
        public static readonly BitmapImage estado_limpieza_lavado = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_limpieza_lavado.png"));
        public static readonly BitmapImage estado_limpieza_normal = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_limpieza_normal.png"));
        public static readonly BitmapImage estado_icono_comandacobrar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Estados/estado_icono_comandacobrar.png"));
    }
}
