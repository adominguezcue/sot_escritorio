using System;
using System.Windows.Media.Imaging;

namespace SOTWpf.Recursos
{
    public class ImagenesMenu
    {
        public static readonly BitmapImage barra_icono_cambio = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_cambio.png"));
        public static readonly BitmapImage barra_icono_cambio_reservacion = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_cambio_reservacion.png"));
        public static readonly BitmapImage barra_icono_cancelar_entrada = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_cancelar_entrada.png"));
        public static readonly BitmapImage barra_icono_corte_turno = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_corte_turno.png"));
        public static readonly BitmapImage barra_icono_deshabilitar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_deshabilitar.png"));
        public static readonly BitmapImage barra_icono_entrada = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_entrada.png"));
        public static readonly BitmapImage barra_icono_estadisticas_recamareras = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_estadisticas_recamareras.png"));
        public static readonly BitmapImage barra_icono_facturar = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_facturar.png"));
        public static readonly BitmapImage barra_icono_fajillas = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_fajillas.png"));
        public static readonly BitmapImage barra_icono_gastos = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_gastos.png"));
        public static readonly BitmapImage barra_icono_habilitar_meseros = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_habilitar_meseros.png"));
        public static readonly BitmapImage barra_icono_habilitar_valets = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_habilitar_valets.png"));
        public static readonly BitmapImage barra_icono_historial_mantenimiento = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_historial_mantenimiento.png"));
        public static readonly BitmapImage barra_icono_horas_extra = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_horas_extra.png"));
        public static readonly BitmapImage barra_icono_mantenimiento = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_mantenimiento.png"));
        public static readonly BitmapImage barra_icono_mantenimiento_terminado = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_mantenimiento_terminado.png"));
        public static readonly BitmapImage barra_icono_menu = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_menu.png"));
        public static readonly BitmapImage barra_icono_reportar_matricula = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_reportar_matricula.png"));
        public static readonly BitmapImage barra_icono_reservacion = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_reservacion.png"));
        public static readonly BitmapImage barra_icono_restaurante = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_restaurante.png"));
        public static readonly BitmapImage barra_icono_salida = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_icono_salida.png"));
        public static readonly BitmapImage barra_restaurante_icono_cambio = new BitmapImage(new Uri("pack://application:,,,/SOTWpf;component/Resources/Imagenes/Menus/barra_restaurante_icono_cambio.png"));
    }
}
