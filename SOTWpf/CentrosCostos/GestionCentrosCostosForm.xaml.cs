﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.CentrosCostos
{
    /// <summary>
    /// Lógica de interacción para GestionCentrosCostosForm.xaml
    /// </summary>
    public partial class GestionCentrosCostosForm : Window
    {
        public GestionCentrosCostosForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargarCentrosCostos(true);
            }
        }

        private void CargarCentrosCostos(bool recargarCategorias = false)
        {
            var controladorCentrosCostos = new ControladorCentrosCostos();

            if (recargarCategorias) 
            {
                var categorias = controladorCentrosCostos.ObtenerCategoriasCentrosCostos();
                categorias.Insert(0, new Modelo.Entidades.CategoriaCentroCostos { Nombre = "Todas" });

                cbCategorias.ItemsSource = categorias;
            }

            var categoriaSeleccionada = cbCategorias.SelectedItem as Modelo.Entidades.CategoriaCentroCostos;
            dgvCentrosCostos.ItemsSource = controladorCentrosCostos.ObtenerCentrosCostosFiltrados(txtNombre.Text, categoriaSeleccionada == null || categoriaSeleccionada.Id == 0 ? default(int?) : categoriaSeleccionada.Id);
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEliminarCentroCostos_Click(object sender, RoutedEventArgs e)
        {
            var centro = dgvCentrosCostos.SelectedItem as Modelo.Entidades.CentroCostos;

            if (centro == null)
                throw new SOTException(Textos.Errores.centro_costos_no_seleccionado_exception);

            if (MessageBox.Show(string.Format(Textos.Mensajes.confirmar_eliminacion_centro_costos, centro.Nombre), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorCentrosCostos = new ControladorCentrosCostos();

                controladorCentrosCostos.EliminarCentroCostos(centro.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_centro_costos_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarCentrosCostos();
            }
        }

        private void btnModificarCentroCostos_Click(object sender, RoutedEventArgs e)
        {
            var centro = dgvCentrosCostos.SelectedItem as Modelo.Entidades.CentroCostos;

            if (centro == null)
                throw new SOTException(Textos.Errores.centro_costos_no_seleccionado_exception);

            var edicionF = new EdicionCentroCostosForm(centro);
            Utilidades.Dialogos.MostrarDialogos(this, edicionF);

            CargarCentrosCostos();
        }

        private void btnAltaCentroCostos_Click(object sender, RoutedEventArgs e)
        {
            var edicionF = new EdicionCentroCostosForm();
            Utilidades.Dialogos.MostrarDialogos(this, edicionF);

            CargarCentrosCostos();
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarCentrosCostos();
        }

        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarCentrosCostos();
        }
    }
}
