﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.CentrosCostos
{
    /// <summary>
    /// Lógica de interacción para EdicionCentroCostos.xaml
    /// </summary>
    public partial class EdicionCentroCostosForm : Window
    {
        Modelo.Entidades.CentroCostos centroActual;

        public EdicionCentroCostosForm(Modelo.Entidades.CentroCostos centroCostos = null)
        {
            InitializeComponent();

            centroActual = centroCostos ?? new Modelo.Entidades.CentroCostos { Activo = true };

            DataContext = centroActual;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorCentrosCostos = new ControladorCentrosCostos();

            if(centroActual != null) 
            {
                if (centroActual.Id == 0)
                    controladorCentrosCostos.CrearCentroCostos(centroActual);
                else
                    controladorCentrosCostos.ModificarCentroCostos(centroActual);

                Close();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorCentrosCostos = new ControladorCentrosCostos();

                var categorias = controladorCentrosCostos.ObtenerCategoriasCentrosCostos();

                cbCategorias.ItemsSource = categorias;
            }
        }
    }
}
