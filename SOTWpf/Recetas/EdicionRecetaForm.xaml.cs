﻿using Modelo.Almacen.Entidades;
using SOTWpf.Articulos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Recetas
{
    /// <summary>
    /// Lógica de interacción para EdicionRecetaUC.xaml
    /// </summary>
    public partial class EdicionRecetaForm : Window
    {
        private ObservableCollection<ZctMixItems> ingredientes = new ObservableCollection<ZctMixItems>();
        private string codigoArticulo;

        public EdicionRecetaForm(string codigoArticulo, string nombreArticulo)
        {
            InitializeComponent();

            //var viewSource = FindResource("ingredientesCVS") as CollectionViewSource;

            dgvIngredientes.ItemsSource = ingredientes;

            this.codigoArticulo = codigoArticulo;
            cabecera.TextoExtra = "Receta para preparar " + nombreArticulo;
        }

        private void btnAgregarIngrediente_Click(object sender, RoutedEventArgs e)
        {
            var bf = new BusquedaArticulosForm();

            Utilidades.Dialogos.MostrarDialogos(this, bf);

            if (bf.ArticuloSeleccionado != null)
            {
                if (ingredientes.Any(m => m.Cod_Art_Mix == bf.ArticuloSeleccionado.Codigo))
                    throw new SOTException(Textos.Errores.ingrediente_repetido_exception, bf.ArticuloSeleccionado.Codigo);

                if (bf.ArticuloSeleccionado.Codigo == codigoArticulo)
                    throw new SOTException(Textos.Errores.articulo_elaborar_en_receta_exception);

                ingredientes.Add(new ZctMixItems 
                {
                    Cod_Art_Mix = bf.ArticuloSeleccionado.Codigo, 
                    DescArt = bf.ArticuloSeleccionado.Nombre,
                    cantidad = 0,
                    NombrePresentacionTMP = bf.ArticuloSeleccionado.NombreUnidad
                });

                //CollectionViewSource.GetDefaultView(dgvIngredientes.ItemsSource).Refresh();
            }
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorR = new SOTControladores.Controladores.ControladorMixItems();

            var ingredientesTMP = ingredientes.ToList();

            foreach (var ing in ingredientesTMP)
            {
                ing.@base = ctbCantidad.Number;
                ing.Cod_Art = codigoArticulo;
                ing.proporcion = null;
            }

            controladorR.GuardarIngredientes(ingredientesTMP);

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarReceta();
            }
        }

        private void CargarReceta()
        {
            try
            {
                var controladorR = new SOTControladores.Controladores.ControladorMixItems();

                ingredientes.Clear();
                controladorR.GetMixItems(codigoArticulo).ForEach(m => ingredientes.Add(m));

                if (ingredientes.Count > 0)
                    ctbCantidad.Number = ingredientes.First().@base ?? 0;
            }
            catch
            {
                Close();
                throw;
            }
        }

        private void ingredientesCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ZctMixItems;

            e.Accepted = item != null; // && item.Activo;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as ZctMixItems;

            if (item != null)
            {
                //if (item.uuid == null)
                    ingredientes.Remove(item);
                //else
                //    item.Activo = false;
            }

            //CollectionViewSource.GetDefaultView(dgvIngredientes.ItemsSource).Refresh();
        }

        private void btnDescartarCambios_Click(object sender, RoutedEventArgs e)
        {
            CargarReceta();
        }
    }
}