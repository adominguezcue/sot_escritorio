﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Almacenes
{
    /// <summary>
    /// Lógica de interacción para EdicionAlmacenesForm.xaml
    /// </summary>
    public partial class EdicionAlmacenesForm : Window
    {
        Modelo.Almacen.Entidades.Almacen AlmacenActual 
        {
            get { return DataContext as Modelo.Almacen.Entidades.Almacen; }
            set { DataContext = value; }
        }

        public EdicionAlmacenesForm(Modelo.Almacen.Entidades.Almacen almacen = null)
        {
            InitializeComponent();

            AlmacenActual = almacen ?? new Modelo.Almacen.Entidades.Almacen();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargaCentrosCostos();
            }
        }

        private void CargaCentrosCostos()
        {
            var controladorCC = new SOTControladores.Controladores.ControladorCentrosCostos();

            //cbCentroCostos.ItemsSource = controladorCC.FILTRO(false);
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorAlmacenes = new SOTControladores.Controladores.ControladorAlmacenes();

            if (AlmacenActual.IdCentroCostos.HasValue)
                controladorAlmacenes.VincularCentroCostos(AlmacenActual.Cod_Alm, AlmacenActual.IdCentroCostos.Value);
            else
                controladorAlmacenes.DesvincularCentroCostos(AlmacenActual.Cod_Alm);

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
