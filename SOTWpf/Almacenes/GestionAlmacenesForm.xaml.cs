﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Almacenes
{
    /// <summary>
    /// Lógica de interacción para GestionAlmacenesForm.xaml
    /// </summary>
    public partial class GestionAlmacenesForm : Window
    {
        public GestionAlmacenesForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargarAlmacenes();
            }
        }

        private void CargarAlmacenes()
        {
            var controladorAlmacenes = new SOTControladores.Controladores.ControladorAlmacenes();

            dgvAlmacenes.ItemsSource = controladorAlmacenes.ObtenerAlmacenes();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEliminarAlmacen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnModificarAlmacen_Click(object sender, RoutedEventArgs e)
        {
            var item = dgvAlmacenes.SelectedItem as Modelo.Almacen.Entidades.Almacen;

            if (item == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            Utilidades.Dialogos.MostrarDialogos(this, new EdicionAlmacenesForm(item));

            CargarAlmacenes();
        }

        private void btnAltaAlmacen_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionAlmacenesForm());
        }
    }
}
