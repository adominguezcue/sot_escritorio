﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Propinas
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionPropinasForm.xaml
    /// </summary>
    public partial class ConfiguracionPropinasForm : Window
    {
        private Modelo.Entidades.ConfiguracionPropinas cfp = null;

        public ConfiguracionPropinasForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var controladorP = new ControladorPropinas();

            cfp = controladorP.ObtenerConfiguracionPropinas();
            DataContext = cfp;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorP = new ControladorPropinas();

            controladorP.ActualizarConfiguracionPropinas(cfp);
            Window_Loaded(this, e);

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
