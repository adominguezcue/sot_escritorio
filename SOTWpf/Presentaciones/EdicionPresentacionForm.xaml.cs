﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Presentaciones
{
    /// <summary>
    /// Lógica de interacción para EdicionPresentacionForm.xaml
    /// </summary>
    public partial class EdicionPresentacionForm : Window
    {
        Presentacion PresentacionActual
        {
            get { return DataContext as Presentacion; }
            set
            {
                DataContext = value;

                if (value != null)
                {
                    foreach (var ce in PresentacionActual.ConversionesEntrada.Where(m => m.Activa))
                    {
                        conversiones.Add(new DtoConversion
                        {
                            Id = ce.Id,
                            Equivalencia = ce.Equivalencia,
                            IdPresentacionEntrada = ce.IdPrimeraPresentacion,
                            IdPresentacionSalida = ce.IdSegundaPresentacion,
                            Activa = true
                        });
                    }

                    foreach (var ce in PresentacionActual.ConversionesSalida.Where(m => m.Activa))
                    {
                        conversiones.Add(new DtoConversion
                        {
                            Id = ce.Id,
                            Equivalencia = ce.Equivalencia,
                            IdPresentacionEntrada = ce.IdSegundaPresentacion,
                            IdPresentacionSalida = ce.IdPrimeraPresentacion,
                            EsInversa = true,
                            Activa = true
                        });
                    }
                }
                else
                    conversiones.Clear();

                //CollectionViewSource.GetDefaultView(dgConversiones.ItemsSource).Refresh();
            }
        }

        ObservableCollection<DtoConversion> conversiones;
        int idPresentacion;

        public EdicionPresentacionForm(int idPresentacion = 0)
        {
            InitializeComponent();

            this.idPresentacion = idPresentacion;

            conversiones = new ObservableCollection<DtoConversion>();
            var collectionViewSource = this.FindResource("conversionesCVS") as CollectionViewSource;
            collectionViewSource.Source = conversiones;
        }

        private void conversionesCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as DtoConversion;

            e.Accepted = item != null && item.Activa;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (idPresentacion != 0)
                {
                    var controladorPresentaciones = new ControladorPresentaciones();

                    PresentacionActual = controladorPresentaciones.ObtenerPresentacion(idPresentacion);
                }
                else
                {
                    PresentacionActual = new Presentacion();
                }

                var collectionViewSource = this.FindResource("conversionesCVS") as CollectionViewSource;
                collectionViewSource.Source = conversiones;
            }
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorPresentacion = new ControladorPresentaciones();


            var clon = PresentacionActual.ObtenerCopiaDePrimitivas();
            clon.id = PresentacionActual.id;

            if (PresentacionActual.id == 0)
            {
                foreach (var conversion in conversiones.Where(m => m.Activa))
                {
                    clon.ConversionesEntrada.Add(new Conversiones 
                    { 
                        Activa = true,
                        Equivalencia = conversion.Equivalencia,
                        IdSegundaPresentacion =  conversion.IdPresentacionSalida
                    });
                }

                controladorPresentacion.CrearPresentacion(clon);
                MessageBox.Show(Textos.Mensajes.creacion_presentacion_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                //var archivos = new List<Modelo.Almacen.Entidades.Dtos.DtoArchivoFicha>();

                //foreach (var archivo in conversiones.Where(m => m.Activa && !m.EsInversa))
                //{
                //    archivos.Add(new Modelo.Almacen.Entidades.Dtos.DtoArchivoFicha
                //    {
                //        Nombre = archivo.Nombre,
                //        Valor = archivo.Valor
                //    });
                //}

                controladorPresentacion.ModificarPresentacion(clon, conversiones.Where(m => (m.Activa || m.Id != 0) && !m.EsInversa).ToList());
                MessageBox.Show(Textos.Mensajes.modificacion_presentacion_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAgregarConversion_Click(object sender, RoutedEventArgs e)
        {
            conversiones.Add(new DtoConversion
            {
                Equivalencia = 1,
                IdPresentacionEntrada = PresentacionActual.id,
                Activa = true
            });

            //CollectionViewSource.GetDefaultView(dgConversiones.ItemsSource).Refresh();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as DtoConversion;

            if (item != null)
            {
                if (item.Id == 0)
                    conversiones.Remove(item);
                else
                    item.Activa = false;
            }

            //CollectionViewSource.GetDefaultView(dgConversiones.ItemsSource).Refresh();
        }
    }
}
