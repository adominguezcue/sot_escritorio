﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Presentaciones
{
    /// <summary>
    /// Lógica de interacción para GestionPresentacionesForm.xaml
    /// </summary>
    public partial class GestionPresentacionesForm : Window
    {
        public GestionPresentacionesForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarPresentaciones();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnCrearPresentacion_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionPresentacionForm());

            CargarPresentaciones();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var presentacion = (sender as Control).DataContext as Modelo.Almacen.Entidades.Presentacion;

            if (presentacion != null)
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionPresentacionForm(presentacion.id));

            CargarPresentaciones();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var presentacion = (sender as Control).DataContext as Modelo.Almacen.Entidades.Presentacion;

            if (presentacion != null && MessageBox.Show(string.Format(Textos.Mensajes.confirmar_eliminacion_presentacion, presentacion.presentacion), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorPresentaciones = new SOTControladores.Controladores.ControladorPresentaciones();
                controladorPresentaciones.EliminarPresentacion(presentacion.id);

                MessageBox.Show(Textos.Mensajes.eliminacion_presentacion_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarPresentaciones();
            }
        }

        private void CargarPresentaciones()
        {
            var controladorRoles = new SOTControladores.Controladores.ControladorPresentaciones();

            tabla.ItemsSource = controladorRoles.ObtenerPresentaciones();
        }
    }
}
