﻿using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Ventas
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class DetallesVenta : Window
    {
        DtoResumenVenta ventaActual
        {
            get 
            {
                return DataContext as DtoResumenVenta;
            }
            set 
            {
                DataContext = value;
            }
        }

        VW_VentaCorrecta venta;

        public DetallesVenta(VW_VentaCorrecta venta)
        {
            InitializeComponent();

            this.venta = venta;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorVentas = new SOTControladores.Controladores.ControladorVentas();

                if (venta.Id.HasValue)
                    ventaActual = controladorVentas.ObtenerResumenVenta(venta.Id.Value) ?? new DtoResumenVenta();
                else if (venta.IdComanda.HasValue)
                {
                    ventaActual = controladorVentas.ObtenerResumenComandaCancelada(venta.IdComanda.Value) ?? new DtoResumenVenta();
                }
                else if (venta.IdConsumoInterno.HasValue)
                    ventaActual = controladorVentas.ObtenerResumenConsumoInternoCancelado(venta.IdConsumoInterno.Value) ?? new DtoResumenVenta();
                else if (venta.IdOcupacionMesa.HasValue)
                    ventaActual = controladorVentas.ObtenerResumenOcupacionMesaCancelada(venta.IdOcupacionMesa.Value) ?? new DtoResumenVenta();
                else if (venta.IdOrdenTaxi.HasValue)
                    ventaActual = controladorVentas.ObtenerResumenOrdenTaxi(venta.IdOrdenTaxi.Value) ?? new DtoResumenVenta();
                else if (venta.IdVentaRenta.HasValue)
                    ventaActual = controladorVentas.ObtenerResumenVentaRentaCancelada(venta.IdVentaRenta.Value) ?? new DtoResumenVenta();
                else
                    ventaActual = new DtoResumenVenta();
            }
        }

      /*  private void dgvDetalles_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            var ventacorrecta = row.DataContext as DtoResumenVenta;
            if (sender == dgvDetalles)
            {
                if (ventacorrecta != null)
                {
                    if (ventacorrecta.Total == 0)
                    {
                        row.Background = new SolidColorBrush(Colors.Red);
                    }
                }
            }
        }*/

    }

}
