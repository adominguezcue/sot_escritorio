﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTControladores.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Ventas
{
    /// <summary>
    /// Lógica de interacción para ConsultaVentasForm.xaml
    /// </summary>
    public partial class ConsultaVentasForm : Window
    {
        ObservableCollection<CheckableItemWrapper<VW_VentaCorrecta>> items;

        public ConsultaVentasForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;

            items = new ObservableCollection<CheckableItemWrapper<VW_VentaCorrecta>>();

            dgvVentas.ItemsSource = items;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var configuraciones = new ControladorConfiguracionGlobal().ObtenerConfiguracionesTurno();
                configuraciones.Insert(0, new Modelo.Entidades.ConfiguracionTurno { Nombre = "Todos" });
                cbTurno.ItemsSource = configuraciones;
            }
        }

        private void btnDetalles_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as CheckableItemWrapper<VW_VentaCorrecta>;

            if (item != null)
                Utilidades.Dialogos.MostrarDialogos(this, new DetallesVenta(item.Value));
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            if (!items.Any(m => m.IsChecked))
            {
                MessageBox.Show("Seleccione elementos a imprimir", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            StringBuilder errores = new StringBuilder();

            foreach (var item in items.Where(m => m.IsChecked))
            {
                try
                {
                    var controladorI = new SOTControladores.Controladores.ControladorConfiguracionesImpresoras();

                    var controlador = new SOTControladores.Controladores.ControladorTickets();

                    if (item.Value.Id.HasValue)
                        controlador.ImprimirTicketVenta(item.Value.Id.Value, controladorI.ObtenerImpresoras().ImpresoraTickets, 1);
                    else if (item.Value.IdComanda.HasValue)
                        controlador.ImprimirTicketComandaCancelada(item.Value.IdComanda.Value, controladorI.ObtenerImpresoras().ImpresoraTickets, 1);
                    else if (item.Value.IdConsumoInterno.HasValue)
                        controlador.ImprimirTicketConsumoInternoCancelado(item.Value.IdConsumoInterno.Value, controladorI.ObtenerImpresoras().ImpresoraTickets, 1);
                    else if (item.Value.IdOcupacionMesa.HasValue)
                        controlador.ImprimirTicketOcupacionMesaCancelada(item.Value.IdOcupacionMesa.Value, controladorI.ObtenerImpresoras().ImpresoraTickets, 1);
                    else if (item.Value.IdOrdenTaxi.HasValue)
                        controlador.ImprimirTicketOrdenTaxi(item.Value.IdOrdenTaxi.Value, controladorI.ObtenerImpresoras().ImpresoraTickets, 1);
                    else if (item.Value.IdVentaRenta.HasValue)
                        controlador.ImprimirTicketVentaRentaCancelada(item.Value.IdVentaRenta.Value, controladorI.ObtenerImpresoras().ImpresoraTickets, 1);
                    else
                    {
                        errores.AppendFormat("{0}: Sin información para imprimir", item.Value.Ticket);
                        errores.AppendLine();
                    }
                }
                catch (Exception ex)
                {
                    errores.AppendFormat("{0}: {1}", item.Value.Ticket, ex.Message);
                    errores.AppendLine();
                }
            }

            if (errores.Length == 0)
                MessageBox.Show("Impresión exitosa", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else
                Utilidades.Dialogos.Aviso("Las siguientes ventas no se pudieron imprimir: " + Environment.NewLine + errores.ToString());
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarVentas();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarVentas();
                }
            }
        }

        private void CargarVentas()
        {
            if (this.IsLoaded)
            {
                int? ordenTurno = cbTurno.SelectedValue == null || (int)cbTurno.SelectedValue == 0 ? null : cbTurno.SelectedValue as int?;
                items.Clear();

                cbHeader.IsChecked = false;

                foreach (var item in new ControladorVentas().ObtenerVentasCorrectasFiltradas(ordenTurno, dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate,
                                                                                                    dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1) : dpFechaFinal.SelectedDate))
                {
                    items.Add(new CheckableItemWrapper<VW_VentaCorrecta>() { Value = item });
                }
            }
        }

        private void cbTurno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarVentas();
        }

        private void chkSelectAll_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in items)
                item.IsChecked = true;
        }

        private void chkSelectAll_Unchecked(object sender, RoutedEventArgs e)
        {
            if (items.Any(m => !m.IsChecked))
                return;

            foreach (var item in items)
                item.IsChecked = false;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            cbHeader.IsChecked = false;
        }
    }
}
