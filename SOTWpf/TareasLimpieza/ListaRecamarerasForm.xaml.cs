﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.TareasLimpieza
{
    /// <summary>
    /// Lógica de interacción para ListaRecamarerasForm.xaml
    /// </summary>
    public partial class ListaRecamarerasForm : Window
    {
        public ListaRecamarerasForm()
        {
            InitializeComponent();
        }

        private void CargarRecamareras()
        {
            var controladorEmpleados = new ControladorEmpleados();
            var controladorTareasL = new ControladorTareasLimpieza();

            List<Modelo.Entidades.Empleado> empleados = controladorEmpleados.ObtenerRecamarerasTurnoFILTRO();
            List<Modelo.Entidades.LimpiezaEmpleado> empleadosTareas = controladorTareasL.ObtenerEmpleadosLimpiezaActivos();


            ActualizarLista(empleados, empleadosTareas);

        }

        private void ActualizarLista(List<Empleado> empleados, List<LimpiezaEmpleado> empleadosTareas)
        {
            foreach (var datosEmpleado in (from empleado in empleados
                                           join empleadoLimpieza in empleadosTareas on empleado.Id equals empleadoLimpieza.IdEmpleado into Group
                                           from empleadoLimpieza in Group.DefaultIfEmpty()
                                           select new
                                           {
                                               empleado,
                                               empleadoLimpieza
                                           }))
            {
                if (datosEmpleado.empleadoLimpieza != null)
                {
                    datosEmpleado.empleado.NumeroHabitacionActual = datosEmpleado.empleadoLimpieza.NumeroHabitacionActual;
                    datosEmpleado.empleado.Ocupado = true;
                }
            }

            lbRecamareras.ItemsSource = empleados;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                CargarRecamareras();
        }
    }
}
