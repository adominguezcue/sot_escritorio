﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Empleados;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.TareasLimpieza
{
    /// <summary>
    /// Lógica de interacción para SeleccionadorSupervisorForm.xaml
    /// </summary>
    public partial class SeleccionadorSupervisorForm : Window
    {
        private bool _procesoExitoso = false;
        private DtoResumenHabitacion HabitacionActual;
        private ObservableCollection<EmpleadoWrapper> empleados;

        public bool ProcesoExitoso
        {
            get { return _procesoExitoso; }
            private set { _procesoExitoso = value; }
        }

        public SeleccionadorSupervisorForm(DtoResumenHabitacion habitacion)
        {
            InitializeComponent();

            HabitacionActual = habitacion;
            empleados = new ObservableCollection<EmpleadoWrapper>();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (HabitacionActual != null)
            {
                if (!empleados.Any(m => m.Seleccionado))
                    throw new SOTException(Textos.Errores.supervisor_no_seleccionado_exception);

                var empleadoActual = empleados.First(m => m.Seleccionado);

                ControladorTareasLimpieza controlador = new ControladorTareasLimpieza();

                if (HabitacionActual.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza)
                    controlador.AsignarSupervisorATarea(HabitacionActual.IdHabitacion, empleadoActual.EmpleadoBase.Id);
                else if (HabitacionActual.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Supervision)
                    controlador.CambiarSupervisorTarea(HabitacionActual.IdHabitacion, empleadoActual.EmpleadoBase.Id);
                else
                    throw new SOTException(Textos.Errores.habitacion_no_limpieza_o_supervision_exception, HabitacionActual.NumeroHabitacion);

                ProcesoExitoso = true;
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (HabitacionActual != null)
                {
                    var controladorEmpleados = new ControladorEmpleados();
                    ControladorTareasLimpieza controladorTareasL = new ControladorTareasLimpieza();

                    empleados.Clear();

                    foreach (var item in controladorEmpleados.ObtenerSupervisoresFILTRO())
                        empleados.Add(new EmpleadoWrapper(item));

                    //listaMarcable.SelectionMode = SelectionMode.Single;

                    var tareaActual = controladorTareasL.ObtenerTareaActualConEmpleados(HabitacionActual.IdHabitacion);

                    if (tareaActual != null && tareaActual.IdEmpleadoSupervisa.HasValue)
                    {
                        foreach (var item in empleados.Where(m => m.EmpleadoBase.Id == tareaActual.IdEmpleadoSupervisa.Value))
                            item.Seleccionado = true;
                    }

                    //empleados = (from em in empleados
                    //             orderby em.Seleccionado descending, em.NombreCompleto
                    //             select em).ToList();

                    listaMarcable.ItemsSource = empleados;
                }
            }
            catch 
            {
                Close();
                throw;
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as EmpleadoWrapper;

            foreach (var item in empleados)
            {
                if (item == marcado)
                    continue;

                item.Seleccionado = false;
            }
        }
    }
}
