﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.TareasLimpieza
{
    /// <summary>
    /// Lógica de interacción para SeleccionadorRecamarerasForm.xaml
    /// </summary>
    public partial class SeleccionadorRecamarerasForm : Window
    {
        private bool _procesoExitoso = false;
        private DtoResumenHabitacion HabitacionActual;

        private TareaLimpieza.TiposLimpieza tipoLimpiezaSeleccionado;

        private List<Empleado> recamarerasDisponibles;
        private List<Empleado> recamarerasSeleccionadas;

        public bool ProcesoExitoso
        {
            get { return _procesoExitoso; }
            private set { _procesoExitoso = value; }
        }

        public SeleccionadorRecamarerasForm(DtoResumenHabitacion habitacion)
        {
            InitializeComponent();

            recamarerasDisponibles = new List<Empleado>();
            recamarerasSeleccionadas = new List<Empleado>();

            flpDisponibles.ItemsSource = recamarerasDisponibles;
            flpSeleccionadas.ItemsSource = recamarerasSeleccionadas;

            HabitacionActual = habitacion;

            txtDatoHabitacion.DataContext = HabitacionActual;

            tipoLimpiezaSeleccionado = TareaLimpieza.TiposLimpieza.Normal;
        }

        private void CargarRecamareras()
        {

            recamarerasDisponibles.Clear();
            recamarerasSeleccionadas.Clear();


            if (HabitacionActual != null)
            {
                var controladorEmpleados = new ControladorEmpleados();
                var controladorTareasL = new ControladorTareasLimpieza();

                recamarerasDisponibles.AddRange(controladorEmpleados.ObtenerRecamarerasTurnoFILTRO());

                Modelo.Entidades.TareaLimpieza tareaActual = controladorTareasL.ObtenerTareaActualConEmpleados(HabitacionActual.IdHabitacion);

                if (tareaActual != null)
                {
                    recamarerasSeleccionadas.AddRange(tareaActual.LimpiezaEmpleados.Where(m => m.Activo).Select(m => m.Empleado).ToList());

                    //CrearBotones(flpSeleccionadas, empleadosAsignados, new List<Modelo.Entidades.LimpiezaEmpleado>(), BotonesRecamarerasDiccionario, new EventHandler(CambiarPadre), COLOR_ASIGNADA);

                    recamarerasDisponibles.RemoveAll(m => recamarerasSeleccionadas.Any(e => e.Id == m.Id));

                    tglbTipo.IsChecked = tareaActual.Tipo != Modelo.Entidades.TareaLimpieza.TiposLimpieza.Normal;

                }
                else
                {
                    tglbTipo.IsChecked = false;
                }
                if (HabitacionActual.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia || HabitacionActual.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza)
                {
                    List<Modelo.Entidades.LimpiezaEmpleado> empleadosTareas = controladorTareasL.ObtenerEmpleadosLimpiezaActivos();

                    foreach (var datosEmpleado in (from empleado in recamarerasDisponibles
                                                   join empleadoLimpieza in empleadosTareas on empleado.Id equals empleadoLimpieza.IdEmpleado into e
                                                   from empleadoLimpieza in e.DefaultIfEmpty()
                                                   select new
                                                   {
                                                       empleado,
                                                       empleadoLimpieza
                                                   }))
                    {
                        if (datosEmpleado.empleadoLimpieza == null)
                        {
                            datosEmpleado.empleado.NumeroHabitacionActual = "";
                            datosEmpleado.empleado.Ocupado = false;
                        }
                        else
                        {
                            datosEmpleado.empleado.NumeroHabitacionActual = datosEmpleado.empleadoLimpieza.NumeroHabitacionActual;
                            datosEmpleado.empleado.Ocupado = true;
                        }
                    }

                    foreach (var datosEmpleado in (from empleado in recamarerasSeleccionadas
                                                   join empleadoLimpieza in empleadosTareas on empleado.Id equals empleadoLimpieza.IdEmpleado into e
                                                   from empleadoLimpieza in e.DefaultIfEmpty()
                                                   select new
                                                   {
                                                       empleado,
                                                       empleadoLimpieza
                                                   }))
                    {
                        if (datosEmpleado.empleadoLimpieza == null)
                        {
                            datosEmpleado.empleado.NumeroHabitacionActual = "";
                            datosEmpleado.empleado.Ocupado = false;
                        }
                        else
                        {
                            datosEmpleado.empleado.NumeroHabitacionActual = datosEmpleado.empleadoLimpieza.NumeroHabitacionActual;
                            datosEmpleado.empleado.Ocupado = true;
                        }
                    }

                    //CrearBotones(flpDisponibles, empleados, empleadosTareas, BotonesRecamarerasDiccionario, new EventHandler(CambiarPadre), COLOR_DISPONIBLE);
                }
                else
                {
                    throw new SOTException(Textos.Errores.habitacion_no_sucia_o_limpieza_exception, HabitacionActual.NumeroHabitacion);
                }
            }

            flpDisponibles.Items.Refresh();
            flpSeleccionadas.Items.Refresh();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {


            if (HabitacionActual != null)
            {

                var controlador = new ControladorTareasLimpieza();

                if (HabitacionActual.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia)
                {
                    controlador.AsignarEmpleadosATarea(HabitacionActual.IdHabitacion, recamarerasSeleccionadas.Select(m => m.Id).ToList(), tipoLimpiezaSeleccionado);
                }
                else if (HabitacionActual.EstadoHabitacion == Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza)
                {
                    controlador.CambiarEmpleadosTarea(HabitacionActual.IdHabitacion, recamarerasSeleccionadas.Select(m => m.Id).ToList(), tipoLimpiezaSeleccionado);
                }

                ProcesoExitoso = true;

            }

            Close();

        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void tglbTipo_Checked(object sender, RoutedEventArgs e)
        {
            tipoLimpiezaSeleccionado = TareaLimpieza.TiposLimpieza.Detallado;
        }

        private void tglbTipo_Unchecked(object sender, RoutedEventArgs e)
        {
            tipoLimpiezaSeleccionado = TareaLimpieza.TiposLimpieza.Normal;
        }

        private void CambiarPadre(object sender, RoutedEventArgs e)
        {
            var empleado = (sender as Control).DataContext as Empleado;

            if (recamarerasDisponibles.Contains(empleado))
            {
                recamarerasDisponibles.Remove(empleado);
                recamarerasSeleccionadas.Add(empleado);
            }
            else if (recamarerasSeleccionadas.Contains(empleado))
            {
                recamarerasSeleccionadas.Remove(empleado);
                recamarerasDisponibles.Add(empleado);
            }

            flpDisponibles.Items.Refresh();
            flpSeleccionadas.Items.Refresh();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CargarRecamareras();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (recamarerasDisponibles.Count == 0)
                return;

            recamarerasSeleccionadas.AddRange(recamarerasDisponibles);
            recamarerasDisponibles.Clear();

            flpDisponibles.Items.Refresh();
            flpSeleccionadas.Items.Refresh();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (recamarerasSeleccionadas.Count == 0)
                return;

            recamarerasDisponibles.AddRange(recamarerasSeleccionadas);
            recamarerasSeleccionadas.Clear();

            flpDisponibles.Items.Refresh();
            flpSeleccionadas.Items.Refresh();
        }
    }
}
