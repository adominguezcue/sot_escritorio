﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;
using SOTWpf.Reportes.Limpieza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.TareasLimpieza
{
    /// <summary>
    /// Lógica de interacción para GestionTareasLimpieza.xaml
    /// </summary>
    public partial class GestionTareasLimpieza : UserControl
    {
        bool filtrosCargados = false;
        public GestionTareasLimpieza()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        public void VerReporte(Window padre) 
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new LimpiezaPorFecha();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = dpFechaInicial.SelectedDate.Value,
                FechaFin = dpFechaFinal.SelectedDate.Value,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            var datos = dgvTareas.ItemsSource as List<Modelo.Dtos.DtoDetallesTareaImpuro>;

            var recamareras = datos.SelectMany(m=> m.NombresRecamareras).Distinct().OrderBy(m=> m).ToList();

            var items = new List<Modelo.Dtos.DtoDetallesTareaImpuroSTR>();
            var tmp = new List<Modelo.Dtos.DtoDetallesTareaImpuroSTR>();

            foreach(var r in recamareras)
            {
                tmp = datos.Where(m => m.NombresRecamareras.Contains(r)).Select(m => (Modelo.Dtos.DtoDetallesTareaImpuroSTR)m).ToList();
                tmp.ForEach(m => m.RecamaristaClave = r);

                items.AddRange(tmp);
            }

            documentoReporte.Subreports["Tareas"].SetDataSource(items);

            List<DtoFiltroReporte> filtros = new List<DtoFiltroReporte>();
            filtros.Add(new DtoFiltroReporte { Filtro = "Turno", Valor = cbTurno.Text ?? "" });
            filtros.Add(new DtoFiltroReporte { Filtro = "Empleados", Valor = cbEmpleados.SelectedValue != null ? cbEmpleados.Text : "" });
            filtros.Add(new DtoFiltroReporte { Filtro = "Tipo de habitación", Valor = cbTipos.SelectedValue != null ? cbTipos.Text : "" });

            documentoReporte.Subreports["FiltrosExtra"].SetDataSource(filtros);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(padre, visorR);
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarTareasL();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarTareasL();
                }
            }
        }

        private void CargarTareasL()
        {
            if (this.IsLoaded && filtrosCargados)
            {
                int? idH = cbTipos.SelectedValue == null || (int)cbTipos.SelectedValue == 0 ? null : cbTipos.SelectedValue as int?;
                int? idT = cbTurno.SelectedValue == null || (int)cbTurno.SelectedValue == 0 ? null : cbTurno.SelectedValue as int?;
                int? idE = cbEmpleados.SelectedValue == null || (int)cbEmpleados.SelectedValue == 0 ? null : cbEmpleados.SelectedValue as int?;

                var controlador = new ControladorTareasLimpieza();
                var incidencias = controlador.ObtenerDetallesTareasPorFecha(idT, idE, idH, dpFechaInicial.SelectedDate.Value.Date,
                                                                          dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1));

                dgvTareas.ItemsSource = incidencias;
            }
        }

        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarTareasL();
        }

        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                var tipos = new ControladorTiposHabitacion().ObtenerTiposActivos();
                tipos.Insert(0, new Modelo.Entidades.TipoHabitacion { Descripcion = "Todos" });
                cbTipos.ItemsSource = tipos;
                cbTipos.SelectedIndex = 0;

                var empleados = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreasFILTRO();
                empleados.Insert(0, new Modelo.Dtos.DtoEmpleadoAreaPuesto { NombreEmpleado = "Todos" });
                cbEmpleados.ItemsSource = empleados;
                cbEmpleados.SelectedIndex = 0;

                var configuraciones = new ControladorConfiguracionGlobal().ObtenerConfiguracionesTurno();
                configuraciones.Insert(0, new Modelo.Entidades.ConfiguracionTurno { Nombre = "Todos" });
                cbTurno.ItemsSource = configuraciones;
                cbTurno.SelectedIndex = 0;

                filtrosCargados = true;
                CargarTareasL();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarTareasL();
        }

        private void cbTurno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarTareasL();
        }

        private void cbTipos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarTareasL();
        }
    }
}
