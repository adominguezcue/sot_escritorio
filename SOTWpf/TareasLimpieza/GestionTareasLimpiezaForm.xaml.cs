﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.TareasLimpieza
{
    /// <summary>
    /// Lógica de interacción para GestionTareasLimpiezaForm.xaml
    /// </summary>
    public partial class GestionTareasLimpiezaForm : Window
    {
        public GestionTareasLimpiezaForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnReporteTareas_Click(object sender, RoutedEventArgs e)
        {
            gestionTareasComponente.VerReporte(this);
        }
    }
}
