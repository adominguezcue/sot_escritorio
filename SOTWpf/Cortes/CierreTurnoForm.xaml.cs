﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para CierreTurnoForm.xaml
    /// </summary>
    public partial class CierreTurnoForm : Window
    {
        public CierreTurnoForm()
        {
            InitializeComponent();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorCortes = new ControladorCortesTurno();
            controladorCortes.RealizarCorteTurno();

            MessageBox.Show(Textos.Mensajes.corte_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
