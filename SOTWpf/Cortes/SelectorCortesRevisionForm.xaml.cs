﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para SelectorCortesRevisionForm.xaml
    /// </summary>
    public partial class SelectorCortesRevisionForm : Window
    {
        public SelectorCortesRevisionForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarCortes();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var corte = (sender as Control).DataContext as Modelo.Dtos.DtoResumenCorteTurno;

            if (corte != null)
            {
                Utilidades.Dialogos.MostrarDialogos(this, new CorteTurnoForm(corte.Id));

                GC.Collect();
            }

            CargarCortes();
        }

        private void btnFinalizar_Click(object sender, RoutedEventArgs e)
        {
            var corte = (sender as Control).DataContext as Modelo.Dtos.DtoResumenCorteTurno;

            if (corte != null && MessageBox.Show(string.Format(Textos.Mensajes.confirmar_finalizar_revision_corte, corte.NumeroCorte), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                new SOTControladores.Controladores.ControladorCortesTurno().CerrarTurno(corte.Id);

                MessageBox.Show(Textos.Mensajes.finalizacion_revision_corte_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarCortes();
            }
        }

        private void CargarCortes() 
        {
            tabla.ItemsSource = new SOTControladores.Controladores.ControladorCortesTurno().ObtenerResumenesCortesEnRevision();
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            var corte = (sender as Control).DataContext as Modelo.Dtos.DtoResumenCorteTurno;

            if (corte != null)
                CorteTurnoForm.MostrarReporte(this, corte.NumeroCorte);
        }
    }
}
