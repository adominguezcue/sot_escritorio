﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para ConfiguradorTurnosOC.xaml
    /// </summary>
    public partial class ConfiguradorTurnosUC : UserControl
    {
        private ObservableCollection<ConfiguracionTurno> configuraciones;

        public ConfiguradorTurnosUC()
        {
            InitializeComponent();

            configuraciones = new ObservableCollection<ConfiguracionTurno>();
            dgConfiguracionesTurnos.ItemsSource = configuraciones;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarConfiguraciones();
            }
        }

        private void CargarConfiguraciones()
        {
            configuraciones.Clear();

            //foreach (var config in (from configuracion in new ControladorConfiguracionGlobal().ObtenerConfiguracionesTurno(false)
            //                        orderby new { configuracion.Orden, configuracion.Activa, configuracion.Id }
            //                        select configuracion).ToList())
            //{
            //    configuraciones.Add(config);
            //}

            foreach (var config in new ControladorConfiguracionGlobal().ObtenerConfiguracionesTurno(false).OrderBy(m => m.Orden).ThenBy(m => m.Activa).ThenBy(m => m.Id))
            {
                configuraciones.Add(config);
            }
        }

        public void Guardar()
        {
            int orden = 1;
            var listaConfiguraciones = configuraciones.ToList();
            foreach (var config in listaConfiguraciones)
            {
                if (config.Activa)
                    config.Orden = orden++;
            }

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_configuracion_turnos) != MessageBoxResult.Yes)
                return;

            new ControladorConfiguracionGlobal().ConfigurarTurnos(listaConfiguraciones);

            Utilidades.Dialogos.Aviso(Textos.Mensajes.configuracion_turnos_exitosa_excepcion);

            CargarConfiguraciones();
        }

        public void CancelarEdicion()
        {
            CargarConfiguraciones();
        }
    }
}
