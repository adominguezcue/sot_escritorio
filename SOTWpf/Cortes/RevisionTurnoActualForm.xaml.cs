﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para RevisionTurnoActualForm.xaml
    /// </summary>
    public partial class RevisionTurnoActualForm : Window
    {
        //Fajilla nuevaFajilla;
        //int idCorte;


        public RevisionTurnoActualForm()
        {
            InitializeComponent();
        }

        private void CargarDatosCorte()
        {
            var controladorCortes = new ControladorCortesTurno();

            var nuevoCorte = controladorCortes.ObtenerDatosUltimoCorteTurno();

            if (nuevoCorte == null)
                Close();

            DataContext = nuevoCorte;

            //this.desgloseF.IdCorte = idCorte;
            //this._comisionesMeserosF.IdCorte = idCorte;
            //this._comisionesValetsF.IdCorte = idCorte;
            //this._gastosF.IdCorte = idCorte;
            //this._incidenciasHabitacionesF.IdCorte = idCorte;
            //this._incidenciasTurnoF.IdCorte = idCorte;
            this._pagosHabitacionesF.IdCorte = nuevoCorte.Id;
            this._pagosRestaurantesF.IdCorte = nuevoCorte.Id;
            this._pagosRoomServiceF.IdCorte = nuevoCorte.Id;
            this.resumenCorteUC.IdCorte = nuevoCorte.Id;
        }

        private void nacionales_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as MontoFajilla;

            e.Accepted = item != null && !item.IdMonedaExtranjeraTmp.HasValue;
        }

        private void gridDesgloseResumido_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void Finalizar_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tPartes.Items.Count == tPartes.SelectedIndex + 1;
        }

        private void Finalizar_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //if (MessageBox.Show(Textos.Mensajes.confirmar_guardar_desglose_turno, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            //{
            //    var controladorFajillas = new ControladorFajillas();
            //    controladorFajillas.GuardarSobranteCaja((DataContext as CorteTurno).Id, desgloseF.SobranteCaja);

            //    MessageBox.Show(Textos.Mensajes.guardado_desglose_turno_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            //}

            //MostrarReporte(this, (DataContext as CorteTurno).NumeroCorte);

            Close();
        }

        //internal static void MostrarReporte(Window padre,int folioCorte)
        //{
        //    var controladorCortes = new ControladorCortesTurno();

        //    var reporte = controladorCortes.ObtenerReporteCierre(folioCorte);

        //    var documentoReporte = new Reportes.CierreTurno.ReporteCierreTurno();
        //    documentoReporte.Load();
        //    var items = new List<DtoReporteContable>() { reporte };

        //    documentoReporte.SetDataSource(items);
        //    documentoReporte.Subreports["Habitaciones"].SetDataSource(reporte.ResumenesHabitaciones);
        //    documentoReporte.Subreports["PersonasExtra"].SetDataSource(reporte.PersonasExtra);
        //    documentoReporte.Subreports["HospedajeExtra"].SetDataSource(reporte.HospedajeExtra);
        //    documentoReporte.Subreports["Paquetes"].SetDataSource(reporte.Paquetes);
        //    documentoReporte.Subreports["RoomService"].SetDataSource(reporte.RoomService);
        //    documentoReporte.Subreports["Restaurante"].SetDataSource(reporte.Restaurante);
        //    documentoReporte.Subreports["Descuentos"].SetDataSource(reporte.Descuentos);
        //    documentoReporte.Subreports["Taxis"].SetDataSource(reporte.ResumenesTaxis);
        //    documentoReporte.Subreports["FormasPago"].SetDataSource(reporte.FormasPago);
        //    documentoReporte.Subreports["Desglose"].SetDataSource(reporte.Desglose);

        //    var visorR = new VisorReportes();

        //    visorR.crCrystalReportViewer.ReportSource = documentoReporte;
        //    visorR.UpdateLayout();

        //    Utilidades.Dialogos.MostrarDialogos(padre, visorR);
        //}

        private void EdicionRealizada(object sender, RoutedEventArgs e)
        {
            CargarDatosCorte();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarDatosCorte();

                //var controlador = new ControladorFajillas();
                //var config = controlador.ObtenerConfiguracionFajillasCargada();

                //nuevaFajilla = new Fajilla
                //{
                //    Activa = true,
                //    ConfiguracionFajilla = config
                //};

                //desgloseF.DataContext = nuevaFajilla;
            }
        }
    }

    public static class ComandosPreCorte
    {
        public static readonly RoutedUICommand Finalizar = new RoutedUICommand
                (
                        "Finalizar",
                        "Finalizar",
                        typeof(ComandosPreCorte)
                );

        //Define more commands here, just like the one above
    }
}
