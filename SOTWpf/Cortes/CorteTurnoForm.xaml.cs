﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para CorteTurnoForm.xaml
    /// </summary>
    public partial class CorteTurnoForm : Window
    {
        Fajilla nuevaFajilla;
        int idCorte;


        public CorteTurnoForm(int idCorte)
        {
            InitializeComponent();

            this.idCorte = idCorte;
        }

        private void CargarDatosCorte()
        {
            var controladorCortes = new ControladorCortesTurno();

            var nuevoCorte = controladorCortes.RevisarCorteTurno(idCorte);

            if (nuevoCorte == null)
                Close();

            DataContext = nuevoCorte;

            this.desgloseF.IdCorte = idCorte;
            this._comisionesMeserosF.IdCorte = idCorte;
            this._comisionesValetsF.IdCorte = idCorte;
            this._gastosF.IdCorte = idCorte;
            //this._incidenciasHabitacionesF.IdCorte = idCorte;
            this._LecturaHMantenimiento.IdCorte = idCorte;
            this._incidenciasTurnoF.IdCorte = idCorte;
            this._pagosHabitacionesF.IdCorte = idCorte;
            this._pagosRestaurantesF.IdCorte = idCorte;
            this._pagosRoomServiceF.IdCorte = idCorte;
        }

        private void nacionales_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as MontoFajilla;

            e.Accepted = item != null && !item.IdMonedaExtranjeraTmp.HasValue;
        }

        private void gridDesgloseResumido_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void Finalizar_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = tPartes.Items.Count == tPartes.SelectedIndex + 1;
        }

        private void Finalizar_Executed(object sender, ExecutedRoutedEventArgs e)
        {

            //Valida Si tiene hay que poner lecturas en El Hotel
            var ControladorInsertaLecturasH = new ControladorConfiguracionesMantenimientosH();
            ControladorInsertaLecturasH.EsInsertaLecturas(idCorte);

            var controladorIncidenciasTurno = new ControladorIncidenciasTurno();
            controladorIncidenciasTurno.ValidarExistenIncidenciasArea(idCorte);

      

            if (MessageBox.Show(Textos.Mensajes.confirmar_guardar_desglose_turno, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorFajillas = new ControladorFajillas();
                controladorFajillas.GuardarSobranteCaja((DataContext as CorteTurno).Id, desgloseF.SobranteCaja);

                MessageBox.Show(Textos.Mensajes.guardado_desglose_turno_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            MostrarReporte(this, (DataContext as CorteTurno).NumeroCorte);

            Close();
        }

        internal static void MostrarReporte(Window padre,int folioCorte)
        {
            var controladorCortes = new ControladorCortesTurno();

            var reporte = controladorCortes.ObtenerReporteCierre(folioCorte);

            var documentoReporte = new Reportes.CierreTurno.ReporteCierreTurno();
            documentoReporte.Load();
            var items = new List<DtoReporteContable>() { reporte };

            documentoReporte.SetDataSource(items);
            documentoReporte.Subreports["Habitaciones"].SetDataSource(reporte.ResumenesHabitaciones);
            documentoReporte.Subreports["PersonasExtra"].SetDataSource(reporte.PersonasExtra);
            documentoReporte.Subreports["HospedajeExtra"].SetDataSource(reporte.HospedajeExtra);
            documentoReporte.Subreports["Paquetes"].SetDataSource(reporte.Paquetes);
            documentoReporte.Subreports["RoomService"].SetDataSource(reporte.RoomService);
            documentoReporte.Subreports["Restaurante"].SetDataSource(reporte.Restaurante);
            documentoReporte.Subreports["Descuentos"].SetDataSource(reporte.Descuentos);
            documentoReporte.Subreports["Taxis"].SetDataSource(reporte.ResumenesTaxis);
            documentoReporte.Subreports["FormasPago"].SetDataSource(reporte.FormasPago);
			
			
            //documentoReporte.Subreports["Desglose"].SetDataSource(reporte.Desglose);
            List<DtoMoneda> monedamx = new List<DtoMoneda>();
            monedamx = RegresaMonedaMx(reporte.Desglose);
            documentoReporte.Subreports["Desglose_Mx"].SetDataSource(monedamx);
            List<DtoMoneda> monedadolar = new List<DtoMoneda>();
            monedadolar = RegresaMonedaDolar(reporte.Desglose);
            documentoReporte.Subreports["Desglose_Eu"].SetDataSource(monedadolar);
            List<DtoMoneda> monedaeuro = new List<DtoMoneda>();
            monedaeuro = RegresaMonedaEuro(reporte.Desglose);
            documentoReporte.Subreports["Desglose_Euro"].SetDataSource(monedaeuro);


            var visorR = new VisorReportes();
            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(padre, visorR);
        }


        private static List<DtoMoneda> RegresaMonedaMx(List<DtoMoneda> moneda)
        {
            List<DtoMoneda> respuesta = new List<DtoMoneda>();
            foreach (DtoMoneda datos in moneda)
            {
                if (datos.Nombre.Equals("Peso") && datos.Monto>=20)
                    respuesta.Add(datos);
            }
           return respuesta;
        }

        private static List<DtoMoneda> RegresaMonedaDolar(List<DtoMoneda> moneda)
        {
            List<DtoMoneda> respuesta = new List<DtoMoneda>();
            foreach (DtoMoneda datos in moneda)
            {
                if (datos.Nombre.Equals("Peso") && datos.Monto<20)
                    respuesta.Add(datos);
            }
            return respuesta;
        }


        private static List<DtoMoneda> RegresaMonedaEuro(List<DtoMoneda> moneda)
        {
            List<DtoMoneda> respuesta = new List<DtoMoneda>();
            foreach (DtoMoneda datos in moneda)
            {
                if (!datos.Nombre.Equals("Peso"))
                    respuesta.Add(datos);
            }
            return respuesta;
        }
		
        private void EdicionRealizada(object sender, RoutedEventArgs e)
        {
            CargarDatosCorte();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarDatosCorte();

                //var controlador = new ControladorFajillas();
                //var config = controlador.ObtenerConfiguracionFajillasCargada();

                //nuevaFajilla = new Fajilla
                //{
                //    Activa = true,
                //    ConfiguracionFajilla = config
                //};

                //desgloseF.DataContext = nuevaFajilla;
            }
        }
    }

    public static class ComandosCorteTurno
    {
        public static readonly RoutedUICommand Finalizar = new RoutedUICommand
                (
                        "Finalizar",
                        "Finalizar",
                        typeof(ComandosCorteTurno)
                );

        //Define more commands here, just like the one above
    }
}