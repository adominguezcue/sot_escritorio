﻿using Modelo.Dtos;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Conversores;
using Transversal.Excepciones;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para SelectorReporteForm.xaml
    /// </summary>
    public partial class SelectorReporteForm : Window
    {
        public SelectorReporteForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            cbReportes.SelectedItem = TiposReportes.ReporteContable;
        }

        bool consultar;

        private void btnVerReporte_Click(object sender, RoutedEventArgs e)
        {
            //if (!dpFechaInicial.SelectedDate.HasValue || !dpFechaFinal.SelectedDate.HasValue)
            //    throw new SOTException("Seleccione una fecha inicial y una final");

            //int inicio, fin;

            //if (!int.TryParse(txtCorteInicial.Text, out inicio))
            //    throw new SOTException("Corte inicial inválido");

            //if (!int.TryParse(txtCorteFinal.Text, out fin))
            //    throw new SOTException("Corte final inválido");

            var filtro = (TiposReportes)cbReportes.SelectedItem;

            switch (filtro)
            {
                case TiposReportes.ReporteContable:
                    {
                        var controladorCortes = new ControladorCortesTurno();

                        var reporte = controladorCortes.ObtenerReporteContablePorRangoCortes((int)txtCorteInicial.Number, (int)txtCorteFinal.Number);

                        var documentoReporte = new SOTWpf.Reportes.CierreTurno.ReporteContable();
                        documentoReporte.Load();
                        var items = new List<DtoReporteContable>() { reporte };

                        documentoReporte.SetDataSource(items);
                        documentoReporte.Subreports["Habitaciones"].SetDataSource(reporte.ResumenesHabitaciones);
                        documentoReporte.Subreports["PersonasExtra"].SetDataSource(reporte.PersonasExtra);
                        documentoReporte.Subreports["HospedajeExtra"].SetDataSource(reporte.HospedajeExtra);
                        documentoReporte.Subreports["Paquetes"].SetDataSource(reporte.Paquetes);
                        documentoReporte.Subreports["RoomService"].SetDataSource(reporte.RoomService);
                        documentoReporte.Subreports["Restaurante"].SetDataSource(reporte.Restaurante);
                        documentoReporte.Subreports["Descuentos"].SetDataSource(reporte.Descuentos);
                        documentoReporte.Subreports["Taxis"].SetDataSource(reporte.ResumenesTaxis);
                        documentoReporte.Subreports["FormasPago"].SetDataSource(reporte.FormasPago);


                        var visorR = new VisorReportes();

                        visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                        visorR.UpdateLayout();

                        Utilidades.Dialogos.MostrarDialogos(this, visorR);
                    }
                    break;
                default:
                    MessageBox.Show("Reportes aun no visibles");
                    break;
            }
        }

        private void cbReportes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnVerReporte.IsEnabled = cbReportes.SelectedIndex >= 0;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    ActualizarCortes();
                }
            }
        }

        private void ActualizarCortes()
        {
            var controladorCortes = new ControladorCortesTurno();

            //var cortes = controladorCortes.ObtenerResumenesCortesFinalizadosPorRango(dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate, 
            //                                                     dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.Date.AddDays(1).AddSeconds(-1) : dpFechaFinal.SelectedDate);

            var cortes = controladorCortes.ObtenerResumenesCortesPorRango(dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate,
                                                                 dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.Date.AddDays(1).AddSeconds(-1) : dpFechaFinal.SelectedDate);

            dgvCortes.ItemsSource = cortes;

            if (cortes.Count == 0)
                txtCorteInicial.Number = txtCorteFinal.Number = 0;
            else 
            {
                txtCorteInicial.Number = cortes.Min(m => m.Corte);//.ToString();
                txtCorteFinal.Number = cortes.Max(m => m.Corte);//.ToString();
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    ActualizarCortes();
                }
            }
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void dgvCortes_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyType == typeof(decimal))
            {
                DataGridTextColumn dataGridTextColumn = e.Column as DataGridTextColumn;

                if (dataGridTextColumn != null)
                {
                    dataGridTextColumn.Binding.StringFormat = "C";
                }
            }
        }
    }

    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum TiposReportes
    {
        //[Description("Artículos vendidos")]
        //ArticulosVendidos = 1,
        //Cortes = 2,
        //[Description("Corte virtual")]
        //CorteVirtual = 3,
        //Detallado = 4,
        //[Description("Entradas de habitación")]
        //EntradasHabitacion = 5,
        //Gastos = 6,
        //General = 7,
        //Historial = 8,
        //Movimientos = 9,
        //Recamareras = 10,
        [Description("Reporte contable")]
        ReporteContable = 11,
        //Transferencias = 12,
        //Ventas = 13
    }
}
