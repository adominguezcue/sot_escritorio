﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SOTControladores.Controladores;
using Modelo.Dtos;
using SOTWpf.Dtos;
using SOTControladores.Utilidades;
using Modelo.Entidades;
using SOTWpf.Ventas;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para CortesCortesiasCancelaciones.xaml
    /// </summary>
    public partial class CortesCortesiasCancelaciones : Window
    {

        private string comboboxtext = "";
        private List<DtoCortesiasCancelaciones> Datosresporte = new List<DtoCortesiasCancelaciones>();
        public CortesCortesiasCancelaciones()
        {
            InitializeComponent();
            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpfechainicio.Language = lang;
            dpfechafin.Language = lang;
            var fechaActual = DateTime.Now;
            dpfechainicio.SelectedDate = fechaActual;
            dpfechafin.SelectedDate = fechaActual;
        }

        private void BtnBotonReporteClienteMatricula_Click(object sender, RoutedEventArgs e)
        {
            if(Datosresporte != null && Datosresporte.Count>0)
            {
                var controladorGlobal = new ControladorConfiguracionGlobal();
                var config = controladorGlobal.ObtenerConfiguracionGlobal();
                var documentoReporte = new Reportes.CortesiasCancelaciones.CortesiasCancelaciones();
                documentoReporte.Load();
                var visorR = new VisorReportes();
                documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
                    {
                    Direccion=config.Direccion,
                    FechaInicio =  dpfechainicio.SelectedDate.Value,
                    FechaFin = dpfechafin.SelectedDate.Value,
                    Hotel = config.Nombre,
                    Usuario = ControladorBase.UsuarioActual.NombreCompleto
                    } });

                visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                visorR.UpdateLayout();

                documentoReporte.Subreports["CancelacionYCortesias"].Database.Tables[0].SetDataSource(Datosresporte);

                Utilidades.Dialogos.MostrarDialogos(this, visorR);
            }
            else
            {
                Utilidades.Dialogos.Aviso("Verifica los filtros no hay datos que reportar");
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                //Llena el combo Box con un select DISTINCT  Concepto from  SotSchema.VW_ConsCancelacionesYCortesias
                ControladorCortesiasCancelaciones controladorCortesiasCancelaciones = new ControladorCortesiasCancelaciones();
                List<VW_ConceptoCancelacionesYCortesias> conceptos = new List<VW_ConceptoCancelacionesYCortesias>();
                conceptos = controladorCortesiasCancelaciones.ObtieneConceptos();
                conceptos.Insert(0, new VW_ConceptoCancelacionesYCortesias() { Concepto = "Todos" });
                cbxconceptos.ItemsSource = conceptos;
                CargaCancelaciones();

            }
        }

        private void CargaCancelaciones()
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            if (this.IsLoaded)
            {
               // Utilidades.Dialogos.Aviso("El proceso de Consulta puede demorar tiempo ó Acabarse el tiempo de consulta");
                ControladorCortesiasCancelaciones controladorCortesiasCancelaciones = new ControladorCortesiasCancelaciones();
                List<DtoCortesiasCancelaciones> dtoCortesiasCancelaciones = new List<DtoCortesiasCancelaciones>();
                dtoCortesiasCancelaciones = controladorCortesiasCancelaciones.ObtieneCortesiasCancelaciones(comboboxtext,
                dpfechainicio.SelectedDate.Value, dpfechafin.SelectedDate.Value.AddDays(1).AddSeconds(-1));
                dgridconceptoscancelacion.ItemsSource = dtoCortesiasCancelaciones;
                Datosresporte = dtoCortesiasCancelaciones;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Cbxconceptos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((System.Windows.Controls.Primitives.Selector)sender).SelectedItem != null)
            {
                if (((VW_ConceptoCancelacionesYCortesias)((System.Windows.Controls.Primitives.Selector)sender).SelectedItem).Concepto.Equals("Todos"))
                {
                    comboboxtext = "";
                    CargaCancelaciones();
                }
                else
                {
                    comboboxtext = ((VW_ConceptoCancelacionesYCortesias)((System.Windows.Controls.Primitives.Selector)sender).SelectedItem).Concepto.ToString();
                    CargaCancelaciones();
                }
            }
        }

        private void Dpfechainicio_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CargaCancelaciones();
        }

        private void Dpfechafin_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CargaCancelaciones();
        }

        private void Dgridconceptoscancelacion_Sorting(object sender, DataGridSortingEventArgs e)
        {
            bool Orden = false;
            string _switch = "";
            _switch = e.Column.Header.ToString();
            if (e.Column.SortDirection == null || e.Column.SortDirection.Value.ToString() == "Descending")
                Orden = false;
            else if (e.Column.SortDirection.Value.ToString() == "Ascending")
                Orden = true;

            switch (_switch)
            {
                case "Ticket":
                    if (Orden == true)
                    {
                        Datosresporte = Datosresporte.OrderByDescending(o => o.Ticket).ToList();
                    }
                    else
                    {
                        Datosresporte = Datosresporte.OrderBy(o => o.Ticket).ToList();
                    }
                    break;
                case "Fecha Modificación":
                    if (Orden == true)
                    {
                        Datosresporte = Datosresporte.OrderByDescending(o => o.FechaModificacion).ToList();
                    }
                    else
                    {
                        Datosresporte = Datosresporte.OrderBy(o => o.FechaModificacion).ToList();
                    }
                    break;
                case "Habitación":
                    if (Orden == true)
                    {
                        Datosresporte = Datosresporte.OrderByDescending(o => o.Habitacion).ToList();
                    }
                    else
                    {
                        Datosresporte = Datosresporte.OrderBy(o => o.Habitacion).ToList();
                    }
                    break;
                case "Concepto":
                    if (Orden == true)
                    {
                        Datosresporte = Datosresporte.OrderByDescending(o => o.Concepto).ToList();
                    }
                    else
                    {
                        Datosresporte = Datosresporte.OrderBy(o => o.Concepto).ToList();
                    }
                    break;
                case "Motivo Cancelación":
                    if (Orden == true)
                    {
                        Datosresporte = Datosresporte.OrderByDescending(o => o.MotivoCancelacion).ToList();
                    }
                    else
                    {
                        Datosresporte = Datosresporte.OrderBy(o => o.MotivoCancelacion).ToList();
                    }
                    break;
                case "Usuario Canceló":
                    if (Orden == true)
                    {
                        Datosresporte = Datosresporte.OrderByDescending(o => o.UsuarioCancelacion).ToList();
                    }
                    else
                    {
                        Datosresporte = Datosresporte.OrderBy(o => o.UsuarioCancelacion).ToList();
                    }
                    break;
                default:
                    break;
            }
            return;
        }

        private void BtnDetalles_Click(object sender, RoutedEventArgs e)
        {
            if ((System.Windows.FrameworkElement)sender != null)
                {
                if (((Modelo.Dtos.DtoCortesiasCancelaciones)((System.Windows.FrameworkElement)sender).DataContext).idVentaCorrecta >0)
                {
                    if (((Modelo.Dtos.DtoCortesiasCancelaciones)((System.Windows.FrameworkElement)sender).DataContext).Concepto != null)
                    {
                        if (!((Modelo.Dtos.DtoCortesiasCancelaciones)((System.Windows.FrameworkElement)sender).DataContext).Concepto.Equals("Reservación Cancelada"))
                        {
                            string concepto = ((Modelo.Dtos.DtoCortesiasCancelaciones)((System.Windows.FrameworkElement)sender).DataContext).Concepto.ToString();
                            int ticketseleccionado = Convert.ToInt32((((Modelo.Dtos.DtoCortesiasCancelaciones)((System.Windows.FrameworkElement)sender).DataContext).idVentaCorrecta));
                            if (ticketseleccionado > 0)
                            {
                                var item = new SOTControladores.Controladores.ControladorCortesiasCancelaciones().ObtieneVentaCorrecta(ticketseleccionado, concepto);
                                if (item != null && item.Count > 0)
                                    Utilidades.Dialogos.MostrarDialogos(this, new DetallesVenta(item.FirstOrDefault()));
                                else
                                {
                                    Utilidades.Dialogos.Aviso("Es una reservación ó no se encontró la venta correcta");
                                }

                            }
                        }
                        else
                        {
                            Utilidades.Dialogos.Aviso("Es una reservación ó no se encontró la venta correcta");
                        }
                    }
                }
            }

        }
    }
}
