﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SOTControladores.Controladores;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SOTWpf.Dtos;
using Modelo.Entidades;
using Modelo.Dtos;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para ConsultaConsumosInternosEmpleado.xaml
    /// </summary>
    public partial class ConsultaConsumosInternosEmpleado : Window
    {

        private bool _Esfechashabilitadas = false;
        private string Numeroempleado = "";
        private List<VW_ConsumoInternoEmpleado> vistaconsumointernoempleado = new List<VW_ConsumoInternoEmpleado>();
        private List<DtoEmpleadoConsumosInternos> reportencabezado = new List<DtoEmpleadoConsumosInternos>();
        private List<DtoConsumoInternosEmpleado> reporteconsumo = new List<DtoConsumoInternosEmpleado>();
        private bool _Eslistaconsumoordenado = false;
        private bool _OrdenConsumo = false;
        private string _ColumnaConsumoOrden = "";
        public ConsultaConsumosInternosEmpleado()
        {
            InitializeComponent();
            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpfechainicial.Language = lang;
            dpfechafinal.Language = lang;
            dpfechainicial.IsEnabled = false;
            dpfechafinal.IsEnabled = false;
            _Esfechashabilitadas = false;
            var fechaActual = DateTime.Now;
            dpfechainicial.SelectedDate = fechaActual;
            dpfechafinal.SelectedDate = fechaActual;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                SOTControladores.Controladores.ControladorConsumoInternoEmpleado controladorConsumoInternoEmpleado = new ControladorConsumoInternoEmpleado();
                vistaconsumointernoempleado = controladorConsumoInternoEmpleado.ObtieneEmpleados("");
                reportencabezado = new List<DtoEmpleadoConsumosInternos>();
                foreach (VW_ConsumoInternoEmpleado datoempleado in vistaconsumointernoempleado)
                {
                    DtoEmpleadoConsumosInternos datotoconsumointerno = new DtoEmpleadoConsumosInternos();
                    datotoconsumointerno.NumeroEmpleado = datoempleado.NumeroEmpleado;
                    datotoconsumointerno.NombreCompleado = datoempleado.NombreCompleto;
                    datotoconsumointerno.Totalenconsumos = Convert.ToDecimal(datoempleado.Total);
                    reportencabezado.Add(datotoconsumointerno);
                }
                reportencabezado.Insert(0, new DtoEmpleadoConsumosInternos { NombreCompleado = "Todos" });
                cbEmpleados.ItemsSource = reportencabezado;
                

                CargaConsumoInternoEmpleados("");
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BtnBotonReporteClienteMatricula_Click(object sender, RoutedEventArgs e)
        {
            if(reportencabezado != null && reportencabezado.Count>0)
            {
                reporteconsumo = ObtieneConsumosInternos(reportencabezado);

                if(reporteconsumo != null && reporteconsumo.Count>0)
                {
                    var controladorGlobal = new ControladorConfiguracionGlobal();
                    var config = controladorGlobal.ObtenerConfiguracionGlobal();
                    var documentoReporte = new Reportes.ConsumosInternos.ConsumosInternosEmpleado();
                    documentoReporte.Load();
                    var visorR = new VisorReportes();
                    if (_Esfechashabilitadas)
                    {
                        documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
                    {
                    Direccion=config.Direccion,
                    FechaInicio =  dpfechainicial.SelectedDate.Value,
                    FechaFin = dpfechafinal.SelectedDate.Value,
                    Hotel = config.Nombre,
                    Usuario = ControladorBase.UsuarioActual.NombreCompleto
                    } });
                        //Llena listas?? o visualiza reporte
                        
                        visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                        visorR.UpdateLayout();
                    }
                    else
                    {
                        documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
                    {
                    Direccion=config.Direccion,
                    FechaInicio =  DateTime.Now,
                    FechaFin = DateTime.Now,
                    Hotel = config.Nombre,
                    Usuario = ControladorBase.UsuarioActual.NombreCompleto
                    } });
                        //Llena listas?? o visualiza reporte

                        visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                        visorR.UpdateLayout();
                    }

                    documentoReporte.Subreports["ConsumosInternosPorEmpleado"].Database.Tables[0].SetDataSource(reportencabezado);
                    documentoReporte.Subreports["ConsumosInternosPorEmpleado"].Database.Tables[1].SetDataSource(reporteconsumo);

                    Utilidades.Dialogos.MostrarDialogos(this, visorR);
                }
                else
                {
                    Utilidades.Dialogos.Aviso("Los Empleados no tienen Consumos Internos Favor de Valida Filtros");
                }
            }
            else
            {
                Utilidades.Dialogos.Aviso("Debes Buscar Uno o mas Empleados que tengan Consumos Internos");
            }
        }

        private List<DtoConsumoInternosEmpleado> ObtieneConsumosInternos(List<DtoEmpleadoConsumosInternos> Empleadoslistaconusmo)
        {
            List<DtoConsumoInternosEmpleado> respuesta = new List<DtoConsumoInternosEmpleado>();
            foreach(DtoEmpleadoConsumosInternos datosempleado in Empleadoslistaconusmo)
            {
                List<DtoConsumoInternosEmpleado> CicloConsumoInterno = new List<DtoConsumoInternosEmpleado>();
                CicloConsumoInterno = CargaCicloConsumoInterno(datosempleado.NumeroEmpleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                foreach(DtoConsumoInternosEmpleado dato123 in CicloConsumoInterno)
                {
                    respuesta.Add(dato123);
                }

            }
            //Aquiva el sorting list
            if(_Eslistaconsumoordenado)
            {
                if(!_ColumnaConsumoOrden.Equals(""))
                {
                    switch(_ColumnaConsumoOrden)
                    {
                        case "Código Articulo":
                            if (_OrdenConsumo)
                                respuesta = respuesta.OrderByDescending(m => m.codigoArticulo).ToList();
                            else
                                respuesta = respuesta.OrderBy(m => m.codigoArticulo).ToList();
                            break;
                        case "Árticulo Descripción":
                            if(_OrdenConsumo)
                                respuesta = respuesta.OrderByDescending(m => m.Articulodescripcion).ToList();
                            else
                                respuesta = respuesta.OrderBy(m => m.Articulodescripcion).ToList();
                            break;
                        case "Cantidad Consumida":
                            if (_OrdenConsumo)
                                respuesta = respuesta.OrderByDescending(m => m.cantidad).ToList();
                            else
                                respuesta = respuesta.OrderBy(m => m.cantidad).ToList();
                            break;
                        case "Total":
                            if (_OrdenConsumo)
                                respuesta = respuesta.OrderByDescending(m => m.total).ToList();
                            else
                                respuesta = respuesta.OrderBy(m => m.total).ToList();
                            break;
                        default:
                            break;
                    }

                }
            }
            return respuesta;
        }

        private void CargaConsumoInternoEmpleados(string nombreempleado)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            if (this.IsLoaded)
            {
                SOTControladores.Controladores.ControladorConsumoInternoEmpleado controladorConsumoInternoEmpleado = new ControladorConsumoInternoEmpleado();
                vistaconsumointernoempleado= controladorConsumoInternoEmpleado.ObtieneEmpleados(nombreempleado);
                vistaconsumointernoempleado = ModificaTotales(vistaconsumointernoempleado);
                EmpleadoConsumoInterno.ItemsSource = vistaconsumointernoempleado;
                reportencabezado = new List<DtoEmpleadoConsumosInternos>();
                foreach (VW_ConsumoInternoEmpleado datoempleado in vistaconsumointernoempleado)
                {
                    DtoEmpleadoConsumosInternos datotoconsumointerno = new DtoEmpleadoConsumosInternos();
                    datotoconsumointerno.NumeroEmpleado = datoempleado.NumeroEmpleado;
                    datotoconsumointerno.NombreCompleado = datoempleado.NombreCompleto;
                    datotoconsumointerno.Totalenconsumos = Convert.ToDecimal(datoempleado.Total);
                    reportencabezado.Add(datotoconsumointerno);
                }
                if ((Modelo.Entidades.VW_ConsumoInternoEmpleado)EmpleadoConsumoInterno.SelectedValue != null)
                {
                    if (((Modelo.Entidades.VW_ConsumoInternoEmpleado)EmpleadoConsumoInterno.SelectedValue).NumeroEmpleado != null)
                    {
                        Numeroempleado = ((Modelo.Entidades.VW_ConsumoInternoEmpleado)EmpleadoConsumoInterno.SelectedValue).NumeroEmpleado.ToString();
                        CargaConsumosInternos(Numeroempleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                    }
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }

        private List<VW_ConsumoInternoEmpleado> ModificaTotales(List<VW_ConsumoInternoEmpleado> vW_ConsumoInternoEmpleados)
        {
            List<VW_ConsumoInternoEmpleado> respuesta = new List<VW_ConsumoInternoEmpleado>();
            decimal totalchido = 0;
            foreach (VW_ConsumoInternoEmpleado intem in vW_ConsumoInternoEmpleados)
            {
                List<DtoConsumoInternosEmpleado> listaparasuma= CargaCicloConsumoInterno(intem.NumeroEmpleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                intem.CantidadConsumos = ObtieneConsumosInternos(listaparasuma);
                intem.Total = OntieneTotalConusmosInternos(listaparasuma);
                totalchido = totalchido + Convert.ToDecimal(intem.Total);
                respuesta.Add(intem);
            }
            textTotalneto.Text = "Total Neto de Consumos: " + totalchido.ToString("C");
            return respuesta;
        }

        private decimal OntieneTotalConusmosInternos(List<DtoConsumoInternosEmpleado> listaparasuma)
        {
            decimal respuesta = 0;
            foreach(DtoConsumoInternosEmpleado item in listaparasuma)
            {
                respuesta = respuesta + item.total;
            }

            return respuesta;
        }

        private int ObtieneConsumosInternos(List<DtoConsumoInternosEmpleado> listaparasuma)
        {
            int respueta = 0;
            foreach(DtoConsumoInternosEmpleado item in listaparasuma)
            {
                respueta = respueta + item.cantidad;
            }

            return respueta;
        }

        private List<DtoConsumoInternosEmpleado> CargaCicloConsumoInterno (string Numeroempleado, DateTime fechainicial, DateTime fechafinal)
        {
            ControladorConsumoInternoEmpleado controladorConsumoInternoEmpleado2 = new ControladorConsumoInternoEmpleado();
            List<DtoConsumoInternosEmpleado> dtoConsumoInternosEmpleados = controladorConsumoInternoEmpleado2.ObtieneConsumoInternoempleado(Numeroempleado, fechainicial, fechafinal, _Esfechashabilitadas);
            return dtoConsumoInternosEmpleados;
        }

        private void CargaConsumosInternos(string Numeroempleado, DateTime fechainicial, DateTime fechafinal)
        {
            ControladorConsumoInternoEmpleado controladorConsumoInternoEmpleado1 = new ControladorConsumoInternoEmpleado();
            List<DtoConsumoInternosEmpleado> dtoConsumoInternosEmpleados = controladorConsumoInternoEmpleado1.ObtieneConsumoInternoempleado(Numeroempleado, fechainicial, fechafinal, _Esfechashabilitadas);
            dgconsumosInternos.ItemsSource = dtoConsumoInternosEmpleados;
        }

        /*private void TxtEmpleado_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargaConsumoInternoEmpleados(TxtEmpleado.Text);

        }*/

        private void Dpfechainicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                CargaConsumosInternos(Numeroempleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                ModificaTotales(vistaconsumointernoempleado);
            }
        }

        private void Dpfechafinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                CargaConsumosInternos(Numeroempleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                ModificaTotales(vistaconsumointernoempleado);
            }
        }

        private void Chckhabilitafechas_Checked(object sender, RoutedEventArgs e)
        {
            if (this.IsLoaded)
            {
                dpfechainicial.IsEnabled = true;
                dpfechafinal.IsEnabled = true;
                _Esfechashabilitadas = true;
                CargaConsumosInternos(Numeroempleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                ModificaTotales(vistaconsumointernoempleado);
            }

        }

        private void Chckhabilitafechas_Unchecked(object sender, RoutedEventArgs e)
        {
            if (this.IsLoaded)
            {
                dpfechainicial.IsEnabled = false;
                dpfechafinal.IsEnabled = false;
                _Esfechashabilitadas = false;
                CargaConsumosInternos(Numeroempleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                ModificaTotales(vistaconsumointernoempleado);
            }
        }

        private void EmpleadoConsumoInterno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Modelo.Entidades.VW_ConsumoInternoEmpleado)EmpleadoConsumoInterno.SelectedValue != null)
            {
                if (((Modelo.Entidades.VW_ConsumoInternoEmpleado)EmpleadoConsumoInterno.SelectedValue).NumeroEmpleado != null)
                {
                    txtbloempleado.Text= "Empleado: "+((Modelo.Entidades.VW_ConsumoInternoEmpleado)EmpleadoConsumoInterno.SelectedValue).NombreCompleto.ToString();
                    Numeroempleado = ((Modelo.Entidades.VW_ConsumoInternoEmpleado)EmpleadoConsumoInterno.SelectedValue).NumeroEmpleado.ToString();
                    CargaConsumosInternos(Numeroempleado, dpfechainicial.SelectedDate.Value, dpfechafinal.SelectedDate.Value);
                    ModificaTotales(vistaconsumointernoempleado);
                }
            }
            else
            {
                txtbloempleado.Text = "";
                dgconsumosInternos.ItemsSource = null;
            }
        }

        private void EmpleadoConsumoInterno_Sorting(object sender, DataGridSortingEventArgs e)
        {
            //reportencabezado
            bool Orden = false;
            string _switch = "";
            _switch = e.Column.Header.ToString();
            if (e.Column.SortDirection == null || e.Column.SortDirection.Value.ToString() == "Descending")
                Orden = false;
            else if (e.Column.SortDirection.Value.ToString() == "Ascending")
                Orden = true;

            switch (_switch)
            {
                case "Nombre Empleado":
                    if (Orden == true)
                    {
                        reportencabezado = reportencabezado.OrderByDescending(o => o.NombreCompleado).ToList();
                    }
                    else
                    {
                        reportencabezado = reportencabezado.OrderBy(o => o.NombreCompleado).ToList();
                    }
                    break;
                case "Total":
                    if (Orden == true)
                    {
                        reportencabezado = reportencabezado.OrderByDescending(o => o.Totalenconsumos).ToList();
                    }
                    else
                    {
                        reportencabezado = reportencabezado.OrderBy(o => o.Totalenconsumos).ToList();
                    }
                    break;
                case "Cantidad de Consumos":
                    if (Orden == true)
                    {
                        reportencabezado = reportencabezado.OrderByDescending(o => o.Totalenconsumos).ToList();
                    }
                    else
                    {
                        reportencabezado = reportencabezado.OrderBy(o => o.Totalenconsumos).ToList();
                    }
                    break;
                default:
                    break;
            } 
            return;
            
        }

        private void CbEmpleados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if ((Modelo.Dtos.DtoEmpleadoConsumosInternos)((System.Windows.Controls.Primitives.Selector)sender).SelectedItem != null)
            {
                if (((Modelo.Dtos.DtoEmpleadoConsumosInternos)((System.Windows.Controls.Primitives.Selector)sender).SelectedItem).NombreCompleado != null)
                {
                    if(((Modelo.Dtos.DtoEmpleadoConsumosInternos)((System.Windows.Controls.Primitives.Selector)sender).SelectedItem).NombreCompleado.Equals("Todos"))
                    {
                        CargaConsumoInternoEmpleados("");
                    }
                    else
                    {
                        CargaConsumoInternoEmpleados(((Modelo.Dtos.DtoEmpleadoConsumosInternos)((System.Windows.Controls.Primitives.Selector)sender).SelectedItem).NombreCompleado);
                    }
                    
                }
            }
        }

        private void DgconsumosInternos_Sorting(object sender, DataGridSortingEventArgs e)
        {
            _Eslistaconsumoordenado = true;

            bool Orden = false;
            string _switch = "";
            _switch = e.Column.Header.ToString();
            if (e.Column.SortDirection == null || e.Column.SortDirection.Value.ToString() == "Descending")
                Orden = false;
            else if (e.Column.SortDirection.Value.ToString() == "Ascending")
                Orden = true;

            switch (_switch)
            {
                case "Código Articulo":
                    if (Orden == true)
                    {
                        _OrdenConsumo = true;
                        _ColumnaConsumoOrden = "Código Articulo";
                    }
                    else
                    {
                        _OrdenConsumo = false;
                        _ColumnaConsumoOrden = "Código Articulo";
                    }
                    break;
                case "Árticulo Descripción":
                    if (Orden == true)
                    {
                        _OrdenConsumo = true;
                        _ColumnaConsumoOrden = "Árticulo Descripción";
                    }
                    else
                    {
                        _OrdenConsumo = false;
                        _ColumnaConsumoOrden = "Árticulo Descripción";
                    }
                    break;
                case "Cantidad Consumida":
                    if (Orden == true)
                    {
                        _OrdenConsumo = true;
                        _ColumnaConsumoOrden = "Cantidad Consumida";
                    }
                    else
                    {
                        _OrdenConsumo = false;
                        _ColumnaConsumoOrden = "Cantidad Consumida";
                    }
                    break;
                case "Total":
                    if (Orden == true)
                    {
                        _OrdenConsumo = true;
                        _ColumnaConsumoOrden = "Total";
                    }
                    else
                    {
                        _OrdenConsumo = false;
                        _ColumnaConsumoOrden = "Total";
                    }
                    break;
                default:
                    break;
            }
            return;
        }
    }
}
