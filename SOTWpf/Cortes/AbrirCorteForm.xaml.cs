﻿using MaterialDesignThemes.Wpf;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Cortes
{
    /// <summary>
    /// Lógica de interacción para AbrirCorteForm.xaml
    /// </summary>
    public partial class AbrirCorteForm : Window
    {
        private DateTime fechaActual;

        internal bool TurnoAbierto { get; private set; }

        public AbrirCorteForm()
        {
            InitializeComponent();
            fechaActual = DateTime.Now;

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            CombinedCalendar.Language = lang;
            CombinedClock.Language = lang;
        }


        public void CombinedDialogOpenedEventHandler(object sender, DialogOpenedEventArgs eventArgs)
        {
            CombinedCalendar.SelectedDate = fechaActual.Date;
            CombinedClock.Time = fechaActual;
        }

        public void CombinedDialogClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            if (Equals(eventArgs.Parameter, "1"))
            {
                fechaActual = CombinedCalendar.SelectedDate.Value.AddSeconds(CombinedClock.Time.TimeOfDay.TotalSeconds);
                CombinedClock.Time = fechaActual;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var configuraciones = new ControladorConfiguracionGlobal().ObtenerConfiguracionesTurno();
                configuraciones.Insert(0, null);

                cbTurnos.ItemsSource = configuraciones;
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {

            if (cbTurnos.SelectedItem == null)
                throw new SOTException(Textos.Errores.turno_no_seleccionado_excepcion);

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_apertura_primer_turno) == MessageBoxResult.Yes)
            {
                new ControladorCortesTurno().AbrirPrimerTurno(((ConfiguracionTurno)cbTurnos.SelectedItem).Id, chbFechaAnterior.IsChecked == true ? (DateTime?)fechaActual : null);

                TurnoAbierto = true;
                Utilidades.Dialogos.Aviso(Textos.Mensajes.primer_turno_abierto);

                Close();
            }
        }
    }
}
