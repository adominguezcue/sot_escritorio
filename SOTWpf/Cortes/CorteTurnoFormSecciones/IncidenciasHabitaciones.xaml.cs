﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para IncidenciasHabitaciones.xaml
    /// </summary>
    public partial class IncidenciasHabitaciones : UserControl
    {
        public IncidenciasHabitaciones()
        {
            InitializeComponent();
        }

        private void btnEliminarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.EliminarIncidencia();
        }

        private void btnCancelarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.CancelarIncidencia();
        }

        private void btnModificarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.ModificarIncidencia();
        }

        private void btnRegistrarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.RegistrarIncidencia();
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) && (e.NewValue as bool?) == true)
            {
                gestorIncidenciasComponente.CargarIncidencias(true, ((Modelo.Entidades.CorteTurno)((System.Windows.FrameworkElement)sender).DataContext).Id);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.GenerarReporte(System.Windows.Window.GetWindow(this));
        }																   	 
    }
}
