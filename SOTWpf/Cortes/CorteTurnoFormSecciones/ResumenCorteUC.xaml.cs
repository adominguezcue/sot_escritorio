﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para ResumenCorteUC.xaml
    /// </summary>
    public partial class ResumenCorteUC : UserControl
    {
        bool bloquearRecarga = true;

        
        #region propiedad IdCorte

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(ResumenCorteUC), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as ResumenCorteUC;

            pg.CargarInformacion();
        }

        #endregion

        public ResumenCorteUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                bloquearRecarga = false;

                CargarInformacion();
            }
        }

        private void CargarInformacion()
        {
            var controladorCortes = new ControladorCortesTurno();

            var datosCorte = controladorCortes.ObtenerDatosRevisionPorId(IdCorte);

            if (datosCorte == null)
            {
                this.Visibility = System.Windows.Visibility.Hidden;
                return;
            }

            DataContext = datosCorte;
        }
    }
}
