﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para ControlEdicionPagos.xaml
    /// </summary>
    public partial class ControlEdicionPagos : UserControl
    {
        TiposTarjeta? tipoTarjeta { get; set; }

        public string numeroTarjetaOriginal { get; set; }

        public string referenciaOriginal { get; set; }

        public decimal propinaOriginal { get; set; }

        public TiposTarjeta? tipoTarjetaOriginal { get; set; }

        public TiposPago formaPagoOriginal { get; set; }



        public int? idEmpleadoPropinaOriginal { get; set; }

        public int idPago { get; set; }

        public AreasPago Area
        {
            get
            {
                return (AreasPago)GetValue(AreaProperty);
            }
            set
            {
                SetValue(AreaProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AreaProperty =
            DependencyProperty.Register("Area", typeof(AreasPago), typeof(ControlEdicionPagos),
              new PropertyMetadata(AreasPago.Habitacion, new PropertyChangedCallback(AreaCambiada)));


        private static void AreaCambiada(
        DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            ControlEdicionPagos controlE = target as ControlEdicionPagos;
            controlE.CargarEmpleados();
        }


        private static readonly RoutedEvent CambiosRealizadosEvent =
            EventManager.RegisterRoutedEvent("CambiosRealizados", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(ControlEdicionPagos));

        /// <summary>The ValueChanged event is called when the TextBoxValue of the control changes.</summary>
        public event RoutedEventHandler CambiosRealizados
        {
            add { AddHandler(CambiosRealizadosEvent, value); }
            remove { RemoveHandler(CambiosRealizadosEvent, value); }
        }

        public bool SoloLectura
        {
            get { return (bool)GetValue(SoloLecturaProperty); }
            set { SetValue(SoloLecturaProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SoloLectura.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SoloLecturaProperty =
            DependencyProperty.Register("SoloLectura", typeof(bool), typeof(ControlEdicionPagos), new PropertyMetadata(false, SoloLecturaPropertyChanged));

        private static void SoloLecturaPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ControlEdicionPagos).CambiarModo((bool)e.NewValue);
        }

        private void CambiarModo(bool soloLectura)
        {
            IsEnabled = !soloLectura;
            //cbFormasPago.IsEditable = !soloLectura;
            //cbEmpleados.IsEditable = !soloLectura;
            //txtNumeroAprobacion.IsEnabled = !soloLectura;
            //txtNumeroAprobacion.IsEnabled = !soloLectura;
            //nudPropina.IsEnabled = !soloLectura;
            //btnVisa.IsEnabled = btnMasterCard.IsEnabled = btnAmericanExpress.IsEnabled = !soloLectura;
            //btnCancelar.Visibility = btnAceptar.Visibility = (soloLectura ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible);
        }



        public ControlEdicionPagos()
        {
            InitializeComponent();
        }
        private void cbFormasPago_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var seleccion = cbFormasPago.SelectedItem as TiposPago?;

            if (seleccion != null && seleccion == TiposPago.TarjetaCredito)
                cbEmpleados.Visibility = panelEdicionPagoTarjeta.Visibility = System.Windows.Visibility.Visible;
            else
                cbEmpleados.Visibility = panelEdicionPagoTarjeta.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnVisa_Click(object sender, RoutedEventArgs e)
        {
            SeleccionarBotonTarjeta(tipoTarjeta = TiposTarjeta.Visa);
        }

        private void btnMasterCard_Click(object sender, RoutedEventArgs e)
        {
            SeleccionarBotonTarjeta(tipoTarjeta = TiposTarjeta.MasterCard);
        }

        private void btnAmericanExpress_Click(object sender, RoutedEventArgs e)
        {
            SeleccionarBotonTarjeta(tipoTarjeta = TiposTarjeta.AmericanExpress);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var listaPagos = new List<TiposPago>();

            listaPagos.Add(TiposPago.Efectivo);
            listaPagos.Add(TiposPago.TarjetaCredito);

            cbFormasPago.ItemsSource = listaPagos;

            CargarEmpleados();

            CargarPago(idPago, idCorte, (TiposPago)0, 0, "");
        }

        private void CargarEmpleados()
        {
            List<Empleado> empleados = new List<Empleado>();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorEmpleados = new ControladorEmpleados();



                switch (Area)
                {
                    case AreasPago.Habitacion:
                        empleados = controladorEmpleados.ObtenerVendedoresFILTRO(idEmpleadoPropinaOriginal);
                        empleados.Insert(0, new Modelo.Entidades.Empleado());
                        break;
                    case AreasPago.Restaurante:
                        empleados = controladorEmpleados.ObtenerMeserosFILTRO(idEmpleadoPropinaOriginal);
                        break;
                    case AreasPago.RoomService:
                        empleados = controladorEmpleados.ObtenerMeserosFILTRO(idEmpleadoPropinaOriginal);
                        break;
                    //default:
                    //    empleados = new List<Empleado>();
                    //    break;
                }
            }

            cbEmpleados.ItemsSource = empleados;
        }

        internal void CargarPago(int idPago, int idCorte, TiposPago formaPago, decimal propina, string referencia, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null, int? idEmpleadoPropina = null)
        {
            this.idPago = idPago;
            this.idCorte = idCorte;
            this.idEmpleadoPropinaOriginal = idEmpleadoPropina;

            CargarEmpleados();

            if (formaPago == TiposPago.Efectivo || formaPago == TiposPago.TarjetaCredito)
            {
                this.formaPagoOriginal = formaPago;

                cbFormasPago.Visibility = System.Windows.Visibility.Visible;

                cbFormasPago.SelectedItem = formaPago;

                if (formaPago == TiposPago.TarjetaCredito)
                {
                    txtNumeroTarjeta.Text = this.numeroTarjetaOriginal = numeroTarjeta;
                    txtNumeroAprobacion.Text = this.referenciaOriginal = referencia;
                    nudPropina.Number = (this.propinaOriginal = propina);
                    if (this.idEmpleadoPropinaOriginal.HasValue)
                        cbEmpleados.SelectedValue = this.idEmpleadoPropinaOriginal.Value;
                    else
                        cbEmpleados.SelectedValue = 0;

                    cbEmpleados.Visibility = panelEdicionPagoTarjeta.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    txtNumeroTarjeta.Text = this.numeroTarjetaOriginal = "";
                    txtNumeroAprobacion.Text = this.referenciaOriginal = "";
                    nudPropina.Number = (this.propinaOriginal = 0);
                    if (this.idEmpleadoPropinaOriginal.HasValue)
                        cbEmpleados.SelectedValue = this.idEmpleadoPropinaOriginal.Value;
                    else
                        cbEmpleados.SelectedValue = 0;

                    cbEmpleados.Visibility = panelEdicionPagoTarjeta.Visibility = System.Windows.Visibility.Collapsed;
                }

                this.tipoTarjetaOriginal = this.tipoTarjeta = tipoTarjeta;

                btnAceptar.Visibility = btnCancelar.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.formaPagoOriginal = 0;

                cbFormasPago.Visibility = System.Windows.Visibility.Collapsed;
                cbEmpleados.Visibility = panelEdicionPagoTarjeta.Visibility = System.Windows.Visibility.Collapsed;

                this.tipoTarjetaOriginal = this.tipoTarjeta = null;

                btnAceptar.Visibility = btnCancelar.Visibility = System.Windows.Visibility.Collapsed;
            }

            SeleccionarBotonTarjeta(this.tipoTarjeta);
        }

        private void SeleccionarBotonTarjeta(TiposTarjeta? tipoTarjeta) 
        {
            if (tipoTarjeta.HasValue)
                switch (tipoTarjeta.Value)
                {
                    case TiposTarjeta.AmericanExpress:
                        btnVisa.Background = new SolidColorBrush(Colors.Transparent);
                        btnAmericanExpress.Background = new SolidColorBrush(Colors.Red);
                        btnMasterCard.Background = new SolidColorBrush(Colors.Transparent);
                        break;
                    case TiposTarjeta.MasterCard:
                        btnVisa.Background = new SolidColorBrush(Colors.Transparent);
                        btnAmericanExpress.Background = new SolidColorBrush(Colors.Transparent);
                        btnMasterCard.Background = new SolidColorBrush(Colors.Red);
                        break;
                    case TiposTarjeta.Visa:
                        btnVisa.Background = new SolidColorBrush(Colors.Red);
                        btnAmericanExpress.Background = new SolidColorBrush(Colors.Transparent);
                        btnMasterCard.Background = new SolidColorBrush(Colors.Transparent);
                        break;
                    default:
                        btnVisa.Background = new SolidColorBrush(Colors.Transparent);
                        btnAmericanExpress.Background = new SolidColorBrush(Colors.Transparent);
                        btnMasterCard.Background = new SolidColorBrush(Colors.Transparent);
                        break;
                }
            else
            {
                btnVisa.Background = new SolidColorBrush(Colors.Transparent);
                btnAmericanExpress.Background = new SolidColorBrush(Colors.Transparent);
                btnMasterCard.Background = new SolidColorBrush(Colors.Transparent);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            CargarPago(idPago, idCorte, formaPagoOriginal, propinaOriginal, referenciaOriginal, numeroTarjetaOriginal, tipoTarjetaOriginal, idEmpleadoPropinaOriginal);
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            switch (Area)
            {
                case AreasPago.Habitacion:
                    {
                        var empleado = cbEmpleados.SelectedItem as Empleado;

                        decimal propina;

                        if (empleado == null || empleado.Id == 0)
                            propina = 0;
                        else
                            propina = nudPropina.Number;//nudPropina.Value.HasValue ? (decimal)nudPropina.Value : 0;

                        var controladorRentas = new ControladorRentas();
                        var tipoPago = (TiposPago)cbFormasPago.SelectedItem;

                        if (tipoPago == TiposPago.TarjetaCredito)

                            controladorRentas.ActualizarPagoHabitacion(idPago, tipoPago,
                                                                       propina, txtNumeroAprobacion.Text,
                                                                       txtNumeroTarjeta.Text, tipoTarjeta, (empleado != null && empleado.Id != 0) ? empleado.Id : default(int?));
                        else if (tipoPago == TiposPago.Efectivo)
                            controladorRentas.ActualizarPagoHabitacion(idPago, tipoPago,
                                                                   0, txtNumeroAprobacion.Text,
                                                                   "", null, null);
                    }
                    break;
                case AreasPago.Restaurante:
                    {
                        var empleado = cbEmpleados.SelectedItem as Empleado;

                        decimal propina;

                        if (empleado == null || empleado.Id == 0)
                            propina = 0;
                        else
                            propina = nudPropina.Number;//nudPropina.Value.HasValue ? (decimal)nudPropina.Value : 0;

                        var controladorRestaurantes = new ControladorRestaurantes();
                        var tipoPago = (TiposPago)cbFormasPago.SelectedItem;

                        if (tipoPago == TiposPago.TarjetaCredito)

                            controladorRestaurantes.ActualizarPagoOcupacion(idPago, tipoPago,
                                                                       propina, txtNumeroAprobacion.Text, empleado != null ? empleado.Id : 0,
                                                                       txtNumeroTarjeta.Text, tipoTarjeta);
                        else if (tipoPago == TiposPago.Efectivo)
                            controladorRestaurantes.ActualizarPagoOcupacion(idPago, tipoPago,
                                                                   0, txtNumeroAprobacion.Text, empleado != null ? empleado.Id : 0,
                                                                   "", null);
                    }
                    break;
                case AreasPago.RoomService:
                    {
                        var empleado = cbEmpleados.SelectedItem as Empleado;

                        decimal propina;

                        if (empleado == null || empleado.Id == 0)
                            propina = 0;
                        else
                            propina = nudPropina.Number;//nudPropina.Value.HasValue ? (decimal)nudPropina.Value : 0;

                        var controladorRS = new ControladorRoomServices();
                        var tipoPago = (TiposPago)cbFormasPago.SelectedItem;

                        if (tipoPago == TiposPago.TarjetaCredito)

                            controladorRS.ActualizarPagoComanda(idPago, tipoPago,
                                                                       propina, txtNumeroAprobacion.Text, empleado != null ? empleado.Id : 0,
                                                                       txtNumeroTarjeta.Text, tipoTarjeta);
                        else if (tipoPago == TiposPago.Efectivo)
                            controladorRS.ActualizarPagoComanda(idPago, tipoPago,
                                                                   0, txtNumeroAprobacion.Text, empleado != null ? empleado.Id : 0,
                                                                   "", null);
                    }
                    break;
            }

            RaiseEvent(new RoutedEventArgs(CambiosRealizadosEvent));
        }

        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var empleado = cbEmpleados.SelectedItem as Empleado;

            if (empleado == null || empleado.Id == 0)
                nudPropina.Number = 0;
            else
                nudPropina.Number = propinaOriginal;
        }

        public int idCorte { get; set; }
    }
}
