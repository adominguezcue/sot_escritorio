﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Gastos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para GastosTurno.xaml
    /// </summary>
    public partial class GastosTurno : UserControl
    {
        private static readonly RoutedEvent EdicionRealizadaEvent =
            EventManager.RegisterRoutedEvent("EdicionRealizada", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(GastosTurno));

        /// <summary>The ValueChanged event is called when the TextBoxValue of the control changes.</summary>
        public event RoutedEventHandler EdicionRealizada
        {
            add { AddHandler(EdicionRealizadaEvent, value); }
            remove { RemoveHandler(EdicionRealizadaEvent, value); }
        }

        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(GastosTurno), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as GastosTurno;

            pg.CargarGastos();
        }

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        public GastosTurno()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarGastos();
            }
        }

        private void CargarGastos()
        {
            if (IsLoaded)
            {
                var controladorCortes = new ControladorCortesTurno();

                var ultimoCorte = controladorCortes.ObtenerCorteEnRevision(IdCorte);

                if (ultimoCorte == null)
                {
                    this.Visibility = System.Windows.Visibility.Hidden;
                    return;
                }

                var controlador = new ControladorGastos();

#warning pensarle bien aqui, puede que no sea necesario mostrar los gastos con clasificación
                var gastos = controlador.ObtenerGastosPorTurno(ultimoCorte.Id, false);
                List<Gasto> gastos1 = gastos;
                gastos1 = EliminaClasificacion(gastos1);													

                dgvGastos.ItemsSource = gastos1;

                RaiseEvent(new RoutedEventArgs(EdicionRealizadaEvent));
            }
        }

        private List<Gasto> EliminaClasificacion(List<Gasto>  gastos)
        {
            List<Gasto> respuetsa = new List<Gasto>();
            foreach(Gasto dato in gastos)
            {
                if (dato.Clasificacion  == null)
                    respuetsa.Add(dato);
            }
            return respuetsa;
        }
        private void btnCancelarGasto_Click(object sender, RoutedEventArgs e)
        {
            var gasto = dgvGastos.SelectedItem as Modelo.Entidades.Gasto;

            if (gasto != null && MessageBox.Show(string.Format(Textos.Mensajes.confirmar_cancelacion_gasto, gasto.ConceptoInstantaneo, gasto.Valor.ToString("C")), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorGastos();

                controlador.CancelarGasto(gasto.Id);

                MessageBox.Show(Textos.Mensajes.cancelacion_gasto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarGastos();
            }
        }

        private void btnRegistrarGasto_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new CreacionGastoForm(IdCorte));
            CargarGastos();
        }

        private void btnModificarGasto_Click(object sender, RoutedEventArgs e)
        {
            var gasto = dgvGastos.SelectedItem as Modelo.Entidades.Gasto;

            if (gasto != null)
            {
                var edicionF = new EdicionGastoForm(gasto);
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, edicionF);

                CargarGastos();
            }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) && (e.NewValue as bool?) == true)
            {
                CargarGastos();
            }
        }


    }
}
