﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para DesgloseEfectivo.xaml
    /// </summary>
    public partial class DesgloseEfectivo : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(DesgloseEfectivo), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as DesgloseEfectivo;

            pg.Procesar();
        }

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        //public Fajilla FajillaCondensada
        //{
        //    get
        //    {
        //        return (Fajilla)GetValue(FajillaCondensadaProperty);
        //    }
        //    private set
        //    {
        //        SetValue(FajillaCondensadaProperty, value);
        //    }
        //}

        //public static readonly DependencyProperty FajillaCondensadaProperty =
        //    DependencyProperty.Register("FajillaCondensada", typeof(Fajilla), typeof(DesgloseEfectivo),
        //      new PropertyMetadata(new Fajilla()));


        //private static void OnSomeValuePropertyChanged(
        //DependencyObject target, DependencyPropertyChangedEventArgs e)
        //{
        //    DesgloseEfectivo nb = target as DesgloseEfectivo;
        //    nb.Text = e.NewValue.ToString();
        //}

        Fajilla _fajillaCondensada = new Fajilla();
        Fajilla _sobranteCaja = new Fajilla();

        public Fajilla FajillaCondensada
        {
            get
            {
                return _fajillaCondensada;
            }
            private set
            {
                _fajillaCondensada = value;
                NotifyPropertyChanged();
            }
        }

        public Fajilla SobranteCaja
        {
            get
            {
                return _sobranteCaja;
            }
            private set
            {
                _sobranteCaja = value;
                NotifyPropertyChanged();
            }
        }


        //public Fajilla nuevaFajilla { get; private set; }
        ConfiguracionFajilla config { get; set; }

        Modelo.Dtos.DtoResumenConcepto resumenFajillasTurno;
        List<Fajilla> fajillasAutorizadas { get; set; }

        ControladorFajillas controladorFajillas;

        public DesgloseEfectivo()
        {
            InitializeComponent();
        }

        private void nacionales_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as MontoFajilla;

            e.Accepted = item != null && !item.IdMonedaExtranjeraTmp.HasValue;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Procesar();
        }

        private void Procesar()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                controladorFajillas = new ControladorFajillas();
                config = controladorFajillas.ObtenerConfiguracionFajillasCargadaFILTRO();

                SobranteCaja = controladorFajillas.ObtenerSobranteCajaCorteRevision(IdCorte) ??
                new Fajilla
                {
                    Activa = true,
                    //ConfiguracionFajilla = config
                };

                var controladorCortes = new ControladorCortesTurno();
                var ultimoCorte = controladorCortes.ObtenerCorteEnRevision(IdCorte);

                if (ultimoCorte == null)
                {
                    this.Visibility = System.Windows.Visibility.Hidden;
                    return;
                }

                controladorFajillas = new ControladorFajillas();
                fajillasAutorizadas = controladorFajillas.ObtenerFajillasAutorizadasConMontosPorCorte(ultimoCorte.Id);

                resumenFajillasTurno = new Modelo.Dtos.DtoResumenConcepto();

                if (fajillasAutorizadas.Count > 0)
                {
                    resumenFajillasTurno.Cantidad = fajillasAutorizadas.Count;
                    resumenFajillasTurno.Total = fajillasAutorizadas.Sum(m => m.Valor);
                }

                txtContadorFajillas.DataContext = resumenFajillasTurno;
                txtTotalFajillas.DataContext = resumenFajillasTurno;

                Total.Text = resumenFajillasTurno.Total.ToString("C");

                ObservableCollection<DtoMonedaExtranjera> monedasE = new ObservableCollection<DtoMonedaExtranjera>();

                foreach (var monto in SobranteCaja.MontosFajilla)
                {
                    if (!config.MontosConfiguracionFajilla.Any(m => m.Activa && m.Id == monto.IdMontoConfiguracionFajilla))
                        monto.Activa = false;
                }

                foreach (var montoC in config.MontosConfiguracionFajilla.Where(m => m.Activa))
                {
                    var monto = SobranteCaja.MontosFajilla.FirstOrDefault(m => m.Activa && m.IdMontoConfiguracionFajilla == montoC.Id);

                    if (monto == null)
                    {
                        monto = new MontoFajilla
                        {
                            Activa = true,
                            Cantidad = 0,
                            MontoTmp = montoC.Monto,
                            ValorCambioTmp = 1,
                            IdMonedaExtranjeraTmp = montoC.IdMonedaExtranjera,
                            IdMontoConfiguracionFajilla = montoC.Id,
                        };

                        SobranteCaja.MontosFajilla.Add(monto);
                    }
                    else
                    {
                        monto.MontoTmp = montoC.Monto;
                        monto.ValorCambioTmp = 1;
                        monto.IdMonedaExtranjeraTmp = montoC.IdMonedaExtranjera;
                    }
                    if (montoC.IdMonedaExtranjera.HasValue)
                    {
                        monto.ValorCambioTmp = montoC.MonedaExtranjera.ValorCambio;
                        monto.MonedaExtranjeraTmp = montoC.MonedaExtranjera.Nombre;

                        var monedaE = monedasE.FirstOrDefault(m => m.Id == montoC.IdMonedaExtranjera);
                        if (monedaE == null)
                        {
                            monedaE = new DtoMonedaExtranjera
                            {
                                Id = montoC.IdMonedaExtranjera.Value,
                                Abreviatura = montoC.MonedaExtranjera.Abreviatura,
                                Nombre = montoC.MonedaExtranjera.Nombre,
                                ValorCambio = montoC.MonedaExtranjera.ValorCambio,
                                MontosFajilla = new List<MontoFajilla>()
                            };

                            monedasE.Add(monedaE);
                        }

                        monedaE.MontosFajilla.Add(monto);
                    }

                    monto.PropertyChanged += CreacionFajillaForm_PropertyChanged;
                }

                SobranteCaja.MonedasExtranjeras = monedasE;

                DataContext = SobranteCaja;


                fajillasAutorizadas.Add(SobranteCaja);
                //FajillaCondensada = controladorFajillas.ObtenerCombinacionFajillas(config, fajillasAutorizadas);
                Recalcular();
            }
        }

        private void CreacionFajillaForm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            Recalcular();
        }

        private void Recalcular()
        {
            SobranteCaja.Valor = SobranteCaja.MontosFajilla.Where(m => m.Activa).Sum(m => m.MontoTmp * m.ValorCambioTmp * m.Cantidad);

            Total.Text = (SobranteCaja.Valor + resumenFajillasTurno.Total).ToString("C");

            var fj = controladorFajillas.ObtenerCombinacionFajillas(config, fajillasAutorizadas);

            ObservableCollection<DtoMonedaExtranjera> monedasE = new ObservableCollection<DtoMonedaExtranjera>();

            foreach (var montoC in config.MontosConfiguracionFajilla.Where(m => m.Activa))
            {
                var monto = fj.MontosFajilla.FirstOrDefault(m => m.IdMontoConfiguracionFajilla == montoC.Id);
                if (monto == null)
                    continue;

                monto.MontoTmp = montoC.Monto;
                monto.ValorCambioTmp = 1;
                monto.IdMonedaExtranjeraTmp = montoC.IdMonedaExtranjera;

                if (montoC.IdMonedaExtranjera.HasValue)
                {
                    monto.ValorCambioTmp = montoC.MonedaExtranjera.ValorCambio;
                    monto.MonedaExtranjeraTmp = montoC.MonedaExtranjera.Nombre;

                    var monedaE = monedasE.FirstOrDefault(m => m.Id == montoC.IdMonedaExtranjera);
                    if (monedaE == null)
                    {
                        monedaE = new DtoMonedaExtranjera
                        {
                            Id = montoC.IdMonedaExtranjera.Value,
                            Abreviatura = montoC.MonedaExtranjera.Abreviatura,
                            Nombre = montoC.MonedaExtranjera.Nombre,
                            ValorCambio = montoC.MonedaExtranjera.ValorCambio,
                            MontosFajilla = new List<MontoFajilla>()
                        };

                        monedasE.Add(monedaE);
                    }

                    //if (montoC.MonedaExtranjera.MontosFajilla == null)
                    //    montoC.MonedaExtranjera.MontosFajilla = new List<MontoFajilla>();

                    //montoC.MonedaExtranjera.MontosFajilla.Add(nuevaFajilla.MontosFajilla.Last());

                    monedaE.MontosFajilla.Add(monto);
                }

                //monto.PropertyChanged += CreacionFajillaForm_PropertyChanged;
            }

            fj.MonedasExtranjeras = monedasE;

            FajillaCondensada = fj;
        }
    }
}
