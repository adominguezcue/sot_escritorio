﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Conversores;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum FiltrosPagosHabitaciones
    {
        Todos = 1,
        [Description("Pagos con tarjeta")]
        PagosTarjeta = 2,
        [Description("Pagos en efectivo")]
        PagosEfectivo = 3,
        Reservaciones = 4,
        Propinas = 5
    }

    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum FiltrosPagosRestaurantes
    {
        Todos = 1,
        [Description("Pagos con tarjeta")]
        PagosTarjeta = 2,
        [Description("Pagos en efectivo")]
        PagosEfectivo = 3,
        [Description("Consumo interno")]
        ConsumoInterno = 4,
        Propinas = 5
    }

    public enum ModosRevision 
    {
        Completa = 0,
        Precorte = 1
    }
}
