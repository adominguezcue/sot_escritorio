﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Dtos;
using Transversal.Excepciones;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para PagosHabitaciones.xaml
    /// </summary>
    public partial class PagosHabitaciones : UserControl
    {
        bool bloquearRecarga = true;

        #region propiedad ModoRevision

        public ModosRevision ModoRevision
        {
            get { return (ModosRevision)GetValue(ModoRevisionProperty); }
            set { SetValue(ModoRevisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ModoRevision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ModoRevisionProperty =
            DependencyProperty.Register("ModoRevision", typeof(ModosRevision), typeof(PagosHabitaciones), new PropertyMetadata(ModosRevision.Completa, ModoRevisionPropertyChanged));

        private static void ModoRevisionPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pg = d as PagosHabitaciones;

            pg.controlEdicionP.SoloLectura = pg.ModoRevision == ModosRevision.Precorte;
            pg.Recargar();
        }

        #endregion
        #region propiedad IdCorte

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(PagosHabitaciones), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as PagosHabitaciones;

            pg.Recargar();
        }

        #endregion
        #region evento EdicionRealizada

        private static readonly RoutedEvent EdicionRealizadaEvent =
            EventManager.RegisterRoutedEvent("EdicionRealizada", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(PagosHabitaciones));

        public event RoutedEventHandler EdicionRealizada
        {
            add { AddHandler(EdicionRealizadaEvent, value); }
            remove { RemoveHandler(EdicionRealizadaEvent, value); }
        }

        #endregion

        public PagosHabitaciones()
        {
            InitializeComponent();
        }

        private void CargarInformacion(TiposPago? formaPago = null, bool soloConPropinas = false)
        {
            var controladorCortes = new ControladorCortesTurno();

            CorteTurno ultimoCorte = null;

            if (ModoRevision == ModosRevision.Completa)
                ultimoCorte = controladorCortes.ObtenerCorteEnRevision(IdCorte);
            else if (ModoRevision == ModosRevision.Precorte)
                ultimoCorte = controladorCortes.ObtenerUltimoCorte();

            if (ultimoCorte == null)
            {
                this.Visibility = System.Windows.Visibility.Hidden;
                return;
            }

            var resumenComisiones = controladorCortes.ObtenerMovimientosHabitaciones(ultimoCorte.FechaInicio, ultimoCorte.FechaCorte, formaPago, soloConPropinas);

            dgvMovimientos.ItemsSource = resumenComisiones;

            RaiseEvent(new RoutedEventArgs(EdicionRealizadaEvent));
        }

        private void Recargar()
        {
            if (bloquearRecarga)
                return;

            var filtro = (FiltrosPagosHabitaciones)cbFiltroPagos.SelectedItem;

            switch (filtro)
            {
                case FiltrosPagosHabitaciones.PagosEfectivo:
                    CargarInformacion(TiposPago.Efectivo);
                    break;
                case FiltrosPagosHabitaciones.PagosTarjeta:
                    CargarInformacion(TiposPago.TarjetaCredito);
                    break;
                case FiltrosPagosHabitaciones.Reservaciones:
                    CargarInformacion(TiposPago.Reservacion);
                    break;
                case FiltrosPagosHabitaciones.Propinas:
                    CargarInformacion(soloConPropinas: true);
                    break;
                case FiltrosPagosHabitaciones.Todos:
                    CargarInformacion();
                    break;
                default:
                    dgvMovimientos.ItemsSource = new List<Modelo.Dtos.DtoPagoHabitacionCorte>();
                    break;

            }
        }

        private void dgv_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            System.ComponentModel.PropertyDescriptor pd = e.PropertyDescriptor as System.ComponentModel.PropertyDescriptor;

            DisplayNameAttribute displayNameAttribute = null;
            DisplayFormatAttribute displayFormatAttribute = null;

            foreach (Attribute attr in pd.Attributes) 
            {
                if (displayNameAttribute == null)
                    displayNameAttribute = attr as DisplayNameAttribute;
                if (displayFormatAttribute == null)
                    displayFormatAttribute = attr as DisplayFormatAttribute;
            }

            if (displayNameAttribute != null)
                e.Column.Header = displayNameAttribute.DisplayName;

            if (displayFormatAttribute != null)
            {
                DataGridTextColumn dataGridTextColumn = e.Column as DataGridTextColumn;

                if (dataGridTextColumn != null)
                {
                    dataGridTextColumn.Binding.StringFormat = displayFormatAttribute.DataFormatString;
                }
            }
        }

        private void cbFiltroPagos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Recargar();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                bloquearRecarga = false;

                CargarInformacion();
            }
        }

        private void dgvMovimientos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var pagoSeleccionado = dgvMovimientos.SelectedItem as DtoPagoHabitacionCorte;

            if (pagoSeleccionado != null && (pagoSeleccionado.FormaPago == TiposPago.Efectivo || pagoSeleccionado.FormaPago == TiposPago.TarjetaCredito))
            {
                controlEdicionP.CargarPago(pagoSeleccionado.Id, IdCorte, pagoSeleccionado.FormaPago, pagoSeleccionado.Propina, pagoSeleccionado.Referencia, pagoSeleccionado.NumeroTarjeta, pagoSeleccionado.TipoTarjeta, pagoSeleccionado.IdValet);

                controlEdicionP.Visibility = System.Windows.Visibility.Visible;
            }
            else 
            {
                controlEdicionP.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void controlEdicionP_CambiosRealizados(object sender, RoutedEventArgs e)
        {
            Recargar();
        }
    }
}
