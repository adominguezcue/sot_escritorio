﻿using Microsoft.Win32;
using Modelo.Entidades;
using OfficeOpenXml;
using SOTControladores.Controladores;
using SOTWpf.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Transversal.Utilidades;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para ComisionesMeseros.xaml
    /// </summary>
    public partial class ComisionesMeseros : UserControl
    {
        private static readonly RoutedEvent EdicionRealizadaEvent =
            EventManager.RegisterRoutedEvent("EdicionRealizada", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(ComisionesMeseros));

        /// <summary>The ValueChanged event is called when the TextBoxValue of the control changes.</summary>
        public event RoutedEventHandler EdicionRealizada
        {
            add { AddHandler(EdicionRealizadaEvent, value); }
            remove { RemoveHandler(EdicionRealizadaEvent, value); }
        }

        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(ComisionesMeseros), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as ComisionesMeseros;

            pg.CargarInformacionPropinas();
        }

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        Modelo.Dtos.DtoComisionesMesero resumenComisiones;
        string folioCorte = null;

        public ComisionesMeseros()
        {
            InitializeComponent();
            //GenerarColumnas();
        }

        private void CargarInformacionPropinas()
        {
            var controladorCortes = new ControladorCortesTurno();

            var ultimoCorte = controladorCortes.ObtenerCorteEnRevision(IdCorte);

            if (ultimoCorte == null)
            {
                this.Visibility = System.Windows.Visibility.Hidden;
                return;
            }
            else
                folioCorte = ultimoCorte.NumeroCorte.ToString();

            resumenComisiones = controladorCortes.ObtenerComisionesMeseros(ultimoCorte.Id);

            var dpt = resumenComisiones.DatosPropinasTarjetas.Copy();
            var dpa = resumenComisiones.DatosPagosArticulos.Copy();
            var dr = resumenComisiones.DatosResumen.Copy();

            AgregarTotales(dpt);
            AgregarTotales(dpa);
            AgregarTotales(dr);

            dpt = EliminaCerosPropinas(dpt);
            dpa = EliminaCerosPropinas(dpa);
            dr = EliminaCerosPropinas(dr);										  
            dgvTarjetas.ItemsSource = dpt.DefaultView;
            dgvArticulos.ItemsSource = dpa.DefaultView;
            dgvPagos.ItemsSource = dr.DefaultView;

            RaiseEvent(new RoutedEventArgs(EdicionRealizadaEvent));
        }

        private System.Data.DataTable EliminaCerosPropinas(System.Data.DataTable datatable)
        {
            System.Data.DataTable respuesta = new System.Data.DataTable();
            respuesta = datatable.Clone();
            bool _TieneMayoracero = false;
            foreach (System.Data.DataRow row in datatable.Rows)
            {
                int rowcounter = row.ItemArray.Count();
                for (int i = 0; i < rowcounter - 1; i++)
                {
                    if (!row.ItemArray[i + 1].ToString().Equals("0"))
                    {
                        _TieneMayoracero = true;
                    }
                }

                if (_TieneMayoracero)
                {
                    respuesta.ImportRow(row);
                    _TieneMayoracero = false;
                }
            }
            return respuesta;
        }																						   	 
        private void AgregarTotales(System.Data.DataTable dataTable)
        {
            var datos = new object[dataTable.Columns.Count];
            datos[0] = "TOTALES";


            for (int i = 1; i < dataTable.Columns.Count; i++)
                datos[i] = dataTable.Rows.Count == 0 ?
                           0 :
                           dataTable.Compute("SUM([" + dataTable.Columns[i].ColumnName + "])", "");

            dataTable.Rows.Add(datos);
        }

        private void dgv_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyType == typeof(decimal))
            {
                DataGridTextColumn dataGridTextColumn = e.Column as DataGridTextColumn;

                if (dataGridTextColumn != null)
                {
                    dataGridTextColumn.Binding.StringFormat = "C";
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarInformacionPropinas();
            }
        }

        private void generarReporte_Click(object sender, RoutedEventArgs e)
        {
            if (resumenComisiones != null)
            {
                var controladorConfig = new ControladorConfiguracionGlobal();
                var config = controladorConfig.ObtenerConfiguracionGlobal();

                var controladorCortes = new ControladorCortesTurno();
                var ultimoCorte = controladorCortes.ObtenerCorteEnRevision(IdCorte);

                //var reporte = controladorCortes.ObtenerReporteCierre(folioCorte);

                var documentoReporte = new Reportes.CierreTurno.ComisionesMeseros();
                documentoReporte.Load();

                var articulos = resumenComisiones.DatosPagosArticulos.ToList<Dtos.DtoPuntosMesero>().Where(m => m.Alimentos != 0 || m.Bebidas != 0 || m.SexAndSpa != 0 || m.PuntosPagar != 0).ToList();
                var comisiones = resumenComisiones.DatosPropinasTarjetas.ToList<Dtos.DtoComisionesMesero>().Where(m => m.Visa != 0 || m.Mastercard != 0 || m.Amex != 0 || m.Comision != 0).ToList();
                var resumenes = resumenComisiones.DatosResumen.ToList<Dtos.DtoResumenMesero>().Where(m => m.Propinas != 0 || m.PuntosPagar != 0 || m.Comisiones != 0 || m.Totales != 0).ToList();


                documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
                { 
                    Direccion=config.Direccion,
                    FechaInicio = ultimoCorte.FechaInicio,
                    FechaFin = ultimoCorte.FechaCorte.Value,
                    Hotel = config.Nombre,
                    Usuario = ControladorBase.UsuarioActual.NombreCompleto
                }});

                documentoReporte.Subreports["Articulos"].SetDataSource(articulos);
                documentoReporte.Subreports["Comisiones"].SetDataSource(comisiones);
                documentoReporte.Subreports["Resumenes"].SetDataSource(resumenes);

                var visorR = new VisorReportes();

                visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                visorR.UpdateLayout();

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, visorR);

                //var listaPaginas = new List<ExcelData.DtoWorkSheet>();

                //var sheetPropinas = new ExcelData.DtoWorkSheet 
                //{ 
                //    SheetName = "Comisiones de meseros",
                //    FitToPage = true,
                //    PaperSize = ePaperSize.Letter,
                //    Orientation = eOrientation.Landscape
                //};//"Propinas por tarjeta" };
                //sheetPropinas.DtoSheetSections.Add(new ExcelData.DtoSheetSection());
                //sheetPropinas.DtoSheetSections.Last().AddTableAsSectionRow(new ExcelData.DtoDataTable
                //{
                //    Headers = new Dictionary<string, string>(),
                //    Data = (dgvTarjetas.ItemsSource as System.Data.DataView).Table
                //});

                //listaPaginas.Add(sheetPropinas);

                ///*var sheetArticulos = new ExcelData.DtoWorkSheet { SheetName = "Artículos" };
                //sheetArticulos*/
                //sheetPropinas.DtoSheetSections.Add(new ExcelData.DtoSheetSection());
                //sheetPropinas.DtoSheetSections.Last().AddTableAsSectionRow(new ExcelData.DtoDataTable
                //{
                //    Headers = new Dictionary<string, string>(),
                //    Data = (dgvArticulos.ItemsSource as System.Data.DataView).Table
                //});

                ////listaPaginas.Add(sheetArticulos);

                ///*var sheetResumen = new ExcelData.DtoWorkSheet { SheetName = "Resumen" };
                //sheetResumen*/
                //sheetPropinas.DtoSheetSections.Add(new ExcelData.DtoSheetSection());
                //sheetPropinas.DtoSheetSections.Last().AddTableAsSectionRow(new ExcelData.DtoDataTable
                //{
                //    Headers = new Dictionary<string, string>(),
                //    Data = (dgvPagos.ItemsSource as System.Data.DataView).Table
                //});

                ////listaPaginas.Add(sheetResumen);

                //var controladorReportes = new ControladorReportes();

                //var reporte = controladorReportes.GetReportFromSheets(listaPaginas, "Comisiones de meseros", folioCorte);

                //SaveFileDialog saveFileDialog = new SaveFileDialog
                //{
                //    FileName = reporte.ReportName,
                //    Filter = "Archivo de Excel (*.xlsx)|*.xlsx"
                //};

                //if (saveFileDialog.ShowDialog() == true)
                //{
                //    File.WriteAllBytes(saveFileDialog.FileName, reporte.Data);
                //}
            }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) && (e.NewValue as bool?) == true)
            {
                CargarInformacionPropinas();
            }
        }
    }
}
