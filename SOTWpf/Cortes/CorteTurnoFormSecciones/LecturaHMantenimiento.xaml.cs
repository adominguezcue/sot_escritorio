﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para LecturaHMantenimiento.xaml
    /// </summary>
    public partial class LecturaHMantenimiento : UserControl
    {
        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(LecturaHMantenimiento), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pg = d as LecturaHMantenimiento;
            pg.gestorLecturasComponente.IdCorte = (int)e.NewValue;
        }

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        public LecturaHMantenimiento()
        {
            InitializeComponent();

        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) && (e.NewValue as bool?) == true)
            {
                if (gestorLecturasComponente.Esregistros())
                {
                    RegistrarLecturas.IsEnabled = true;
                    ModificaLectura.IsEnabled = true;
                    //EliminaLectura.IsEnabled = true;
                }
                else
                {
                    RegistrarLecturas.IsEnabled = true;
                    ModificaLectura.IsEnabled = true;
                   // EliminaLectura.IsEnabled = false;
                }
            }
        }

        private void RegistrarLecturas_Click(object sender, RoutedEventArgs e)
        {
            gestorLecturasComponente.RegistrarLecturas();
            if (gestorLecturasComponente.Esregistros())
            {
                RegistrarLecturas.IsEnabled = true;
                ModificaLectura.IsEnabled = true ;
               // EliminaLectura.IsEnabled = true;
            }
            else
            {
                RegistrarLecturas.IsEnabled = true;
                ModificaLectura.IsEnabled = true;
               // EliminaLectura.IsEnabled = false;
            }
        }

        private void ModificaLectura_Click(object sender, RoutedEventArgs e)
        {
            gestorLecturasComponente.ModificarLecturas();
            if (gestorLecturasComponente.Esregistros())
            {
                RegistrarLecturas.IsEnabled = true;
                ModificaLectura.IsEnabled = true;
                //EliminaLectura.IsEnabled = true;
            }
            else
            {
                RegistrarLecturas.IsEnabled = true;
                ModificaLectura.IsEnabled = true;
                //EliminaLectura.IsEnabled = false;
            }
        }

        private void gestorLecturasComponente_Loaded(object sender, RoutedEventArgs e)
        {

        }

      /*  private void EliminaLectura_Click(object sender, RoutedEventArgs e)
        {
            gestorLecturasComponente.EliminaLecturas();
            if (gestorLecturasComponente.Esregistros())
            {
                RegistrarLecturas.IsEnabled = false;
                ModificaLectura.IsEnabled = true;
                EliminaLectura.IsEnabled = true;
            }
            else
            {
                RegistrarLecturas.IsEnabled = true;
                ModificaLectura.IsEnabled = false;
                EliminaLectura.IsEnabled = false;
            }
        }*/
    }
}
