﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Cortes.CorteTurnoFormSecciones
{
    /// <summary>
    /// Lógica de interacción para IncidenciasTurno.xaml
    /// </summary>
    public partial class IncidenciasTurno : UserControl
    {
        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(IncidenciasTurno), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as IncidenciasTurno;

            pg.gestorIncidenciasComponente.IdCorte = (int)e.NewValue;
            //pg.gestorIncidenciasComponente.CargarIncidencias();
        }

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        public IncidenciasTurno()
        {
            InitializeComponent();
        }

        private void btnEliminarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.EliminarIncidencia();
        }

        private void btnCancelarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.CancelarIncidencia();
        }

        private void btnModificarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.ModificarIncidencia();
        }

        private void btnRegistrarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.RegistrarIncidencia();
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) && (e.NewValue as bool?) == true)
            {
                gestorIncidenciasComponente.CargarIncidencias();
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.GeneraReport(System.Windows.Window.GetWindow(this));
        }																   	 
    }
}
