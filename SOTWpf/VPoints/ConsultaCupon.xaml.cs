﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.VPoints
{
    /// <summary>
    /// Lógica de interacción para ConsultaCupon.xaml
    /// </summary>
    public partial class ConsultaCupon : Window
    {
        internal Negocio.ServiciosExternos.DTOs.VPoints.DtoDatosCupon DatosC { get; private set; }
        private Negocio.ServiciosExternos.DTOs.VPoints.DtoDatosCupon _datosC;

        bool validado = false;

        public ConsultaCupon()
        {
            InitializeComponent();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (validado)
            {
                DatosC = _datosC;
                Close();
            }
            else
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(txtCupon.Password))
                    {
                        var controladorVPoints = new ControladorVPoints();

                        _datosC = controladorVPoints.ObtenerCupon(txtCupon.Password.Trim());

                        txtPuntos.Text = _datosC.EstatusCupon.ToUpper();

                        lblSaldo.Visibility = System.Windows.Visibility.Visible;
                        txtPuntos.Visibility = System.Windows.Visibility.Visible;

                        //if (_datosC == null)
                        //    _datosC = new DatosCupon();

                        //_datosC.NumeroTarjeta = txtCupon.Password.Trim();
                        //_datosC.Saldo = saldo;

                        validado = _datosC.PorCanjear;
                    }
                }
                finally
                {
                    if (!validado)
                    {
                        lblSaldo.Visibility = System.Windows.Visibility.Hidden;
                        txtPuntos.Visibility = System.Windows.Visibility.Hidden;

                        _datosC = null;
                        txtCupon.Password = "";
                    }
                }
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            DatosC = null;
            Close();
        }

        private void txtTarjeta_PasswordChanged(object sender, RoutedEventArgs e)
        {
            validado = false;

            lblSaldo.Visibility = System.Windows.Visibility.Hidden;
            txtPuntos.Visibility = System.Windows.Visibility.Hidden;

            DatosC = null;
        }
    }
}
