﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.VPoints
{
    /// <summary>
    /// Lógica de interacción para EdicionTarjeta.xaml
    /// </summary>
    public partial class EdicionTarjeta : Window
    {
        TarjetaPuntos TarjetaActual 
        {
            get { return DataContext as TarjetaPuntos; }
            set { DataContext = value; }
        }

        public EdicionTarjeta(TarjetaPuntos tarjeta = null)
        {
            InitializeComponent();

            TarjetaActual = tarjeta ?? new TarjetaPuntos { Activo = true };
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controlador = new SOTControladores.Controladores.ControladorTarjetasPuntos();

            if (TarjetaActual.Id == 0)
            {
                controlador.CrearTarjeta(TarjetaActual.ObtenerCopiaDePrimitivas());

                MessageBox.Show("Tarjeta de puntos creada con éxito", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var copia = TarjetaActual.ObtenerCopiaDePrimitivas();
                copia.Id = TarjetaActual.Id;

                controlador.ModificarTarjeta(copia);
                MessageBox.Show("Tarjeta de puntos modificada con éxito", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
