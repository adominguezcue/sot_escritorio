﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.VPoints
{
    /// <summary>
    /// Lógica de interacción para ConsultaTarjetaVForm.xaml
    /// </summary>
    public partial class ConsultaTarjetaVForm : Window
    {
        internal DatosTarjeta DatosT { get; private set; }
        private DatosTarjeta _datosT;

        bool validado = false;

        public ConsultaTarjetaVForm(bool cambiarTarjeta, string folio)
        {
            InitializeComponent();

            txtTarjeta.Text = folio;
            txtTarjeta.IsReadOnly = !cambiarTarjeta;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            Procesar();
        }

        private void Procesar()
        {
            if (validado)
            {
                DatosT = _datosT;
                Close();
            }
            else
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(txtTarjeta.Text))
                    {
                        var controladorVPoints = new ControladorVPoints();

                        var saldo = controladorVPoints.ObtenerSaldoTarjetaV(txtTarjeta.Text.Trim());

                        txtPuntos.Text = saldo + " VPoints";

                        lblSaldo.Visibility = System.Windows.Visibility.Visible;
                        txtPuntos.Visibility = System.Windows.Visibility.Visible;

                        if (_datosT == null)
                            _datosT = new DatosTarjeta();

                        _datosT.NumeroTarjeta = txtTarjeta.Text.Trim();
                        _datosT.Saldo = saldo;

                        validado = true;
                    }
                }
                finally
                {
                    if (!validado)
                    {
                        lblSaldo.Visibility = System.Windows.Visibility.Hidden;
                        txtPuntos.Visibility = System.Windows.Visibility.Hidden;

                        _datosT = null;
                    }
                }
            }
        }

        private void txtTarjeta_LostFocus(object sender, RoutedEventArgs e)
        {
            
        }

        internal class DatosTarjeta 
        {
            private string _numeroTarjeta;

            public string NumeroTarjeta
            {
                get { return _numeroTarjeta; }
                set
                {
                    if (!string.IsNullOrEmpty(value))
                        _numeroTarjeta = value.Trim();
                    else
                        _numeroTarjeta = value;
                }
            }
            public decimal Saldo { get; set; }

            public bool Descontar { get; set; }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            DatosT = null;
            Close();
        }

        //private void txtTarjeta_PasswordChanged(object sender, RoutedEventArgs e)
        //{
        //    validado = false;

        //    lblSaldo.Visibility = System.Windows.Visibility.Hidden;
        //    txtPuntos.Visibility = System.Windows.Visibility.Hidden;

        //    DatosT = null;
        //}

        private void txtTarjeta_TextChanged(object sender, TextChangedEventArgs e)
        {
            validado = false;

            lblSaldo.Visibility = System.Windows.Visibility.Hidden;
            txtPuntos.Visibility = System.Windows.Visibility.Hidden;

            DatosT = null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (txtTarjeta.IsReadOnly)
            {
                Procesar();
                Procesar();
            }
        }
    }
}
