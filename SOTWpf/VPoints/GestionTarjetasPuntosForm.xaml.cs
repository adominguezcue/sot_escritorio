﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.VPoints
{
    /// <summary>
    /// Lógica de interacción para GestionTarjetasPuntosForm.xaml
    /// </summary>
    public partial class GestionTarjetasPuntosForm : Window
    {
        public GestionTarjetasPuntosForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarTarjetas();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarTarjetas();
                }
            }
        }

        private void btnEliminarTarjeta_Click(object sender, RoutedEventArgs e)
        {
            var tarjetaSeleccionada = dgvTarjetas.SelectedItem as Modelo.Entidades.TarjetaPuntos;

            if (tarjetaSeleccionada == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_elemento,
                               Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorTarjetasPuntos();

                controlador.EliminarTarjeta(tarjetaSeleccionada.Id);

                CargarTarjetas();

                MessageBox.Show(Textos.Mensajes.eliminacion_elemento_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnModificarTarjeta_Click(object sender, RoutedEventArgs e)
        {
            var tarjetaSeleccionada = dgvTarjetas.SelectedItem as Modelo.Entidades.TarjetaPuntos;

            if (tarjetaSeleccionada == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            var edicionEmpleadoF = new EdicionTarjeta(tarjetaSeleccionada);
            Utilidades.Dialogos.MostrarDialogos(this, edicionEmpleadoF);

            CargarTarjetas();
        }

        private void btnAltaTarjeta_Click(object sender, RoutedEventArgs e)
        {
            var edicionEmpleadoF = new EdicionTarjeta();
            Utilidades.Dialogos.MostrarDialogos(this, edicionEmpleadoF);

            CargarTarjetas();
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarTarjetas();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var estados = Transversal.Extensiones.EnumExtensions.ComoListaDto<Modelo.Entidades.TarjetaPuntos.EstadosTarjeta>();
                estados.ForEach(m => m.Valor = (Modelo.Entidades.TarjetaPuntos.EstadosTarjeta?)m.Valor);//.ToList();
                estados.Insert(0, new Transversal.Dtos.DtoEnum { Nombre = "Todas" });
                cbEstados.ItemsSource = estados;

                CargarTarjetas();
            }
        }

        private void CargarTarjetas()
        {
            if (!IsLoaded)
                return;

            var controlador = new ControladorTarjetasPuntos();

            dgvTarjetas.ItemsSource = controlador.ObtenerTarjetas(dpFechaInicial.SelectedDate, dpFechaFinal.SelectedDate, cbEstados.SelectedValue as Modelo.Entidades.TarjetaPuntos.EstadosTarjeta?);
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cbTipos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarTarjetas();
        }
    }
}
