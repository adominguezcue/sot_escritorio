﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.VPoints
{
    /// <summary>
    /// Lógica de interacción para ActivacionTarjetaVFrom.xaml
    /// </summary>
    public partial class ActivacionTarjetaVFrom : Window
    {
        decimal valorSinIVA, valorIVA, valorConIVA;
        bool esCargada;
        public ActivacionTarjetaVFrom()
        {
            InitializeComponent();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!esCargada)
                return;

            if (chbSoloActivar.IsChecked != true)
            {
                var selectorPagos = new Pagos.SelectorFormaPagoForm(valorConIVA, true, false, false);

                Utilidades.Dialogos.MostrarDialogos(this, selectorPagos);

                if (selectorPagos.ProcesoExitoso)
                {
                    var controladorTarjetas = new ControladorTarjetasPuntos();
                    controladorTarjetas.VenderTarjeta(txtTarjeta.Text, selectorPagos.FormasPagoSeleccionadas);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.venta_exisota);
                    //MessageBox.Show(Textos.Mensajes.venta_exisota, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            else
            {
                if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_alta_tarjeta_puntos) == MessageBoxResult.Yes)
                {
                    var controladorTarjetas = new ControladorTarjetasPuntos();
                    controladorTarjetas.AltaWebTarjeta(txtTarjeta.Text);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.alta_tarjeta_puntos_exisota);
                    //MessageBox.Show(Textos.Mensajes.venta_exisota, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var configG = new ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();
                var configPunto = new ControladorVPoints().ObtenerConfiguracion();

                valorConIVA = Math.Round(configPunto.PrecioTarjetas * (1 + configG.Iva_CatPar), 2);
                valorSinIVA = valorConIVA / (1 + configG.Iva_CatPar);
                valorIVA = valorConIVA - valorSinIVA;

                //valorSinIVA = configG.PrecioTarjetasPuntos;
                //valorIVA = valorSinIVA * configG.Iva_CatPar;
                //valorConIVA = valorSinIVA + valorIVA;

                txtPrecio.Text = valorConIVA.ToString("C");
            }

            esCargada = true;
        }
    }
}
