﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.VPoints
{
    /// <summary>
    /// Lógica de interacción para GestionAbonosErroneosForm.xaml
    /// </summary>
    public partial class GestionAbonosErroneosForm : Window
    {
        public GestionAbonosErroneosForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarAbonosErroneos();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarAbonosErroneos();
                }
            }
        }

        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarAbonosErroneos();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                var habitaciones = controladorHabitaciones.ObtenerActivasConTipos();
                habitaciones.Insert(0, new Modelo.Entidades.Habitacion { NumeroHabitacion = "Todas" });
                cbHabitaciones.ItemsSource = habitaciones;

                CargarAbonosErroneos();
            }
        }

        private void CargarAbonosErroneos()
        {
            var controladorV = new ControladorVPoints();

            var h = cbHabitaciones.SelectedItem as Modelo.Entidades.Habitacion;

            int? idH = h == null || h.Id == 0 ? default(int?) : h.Id;
            DateTime? fechaI = dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate;
            DateTime? fechaF = dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.Date.AddDays(1).AddSeconds(-1) : dpFechaFinal.SelectedDate;

            dgvAbonos.ItemsSource = controladorV.ObtenerAbonosFallidos(idH, fechaI, fechaF);
        }

        private void btnAbonar_Click(object sender, RoutedEventArgs e)
        {
            var item = dgvAbonos.SelectedItem as Modelo.Entidades.AbonoPuntos;

            if (item == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            new ControladorVPoints().AbonarManualmente(item.Id);

            MessageBox.Show(Textos.Mensajes.abono_puntos_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            CargarAbonosErroneos();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
