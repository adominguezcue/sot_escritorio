﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Matriculas
{
    /// <summary>
    /// Lógica de interacción para ReportesMatriculaForm.xaml
    /// </summary>
    public partial class ReportesMatriculaForm : Window
    {
        public ReportesMatriculaForm(string matricula = null)
        {
            InitializeComponent();

            txtBusqueda.Text = matricula;
        }


        private void VerDetalles(object sender, MouseButtonEventArgs e)
        {
            var item = ((Control)sender).DataContext as ReporteMatricula;

            grupoDatos.DataContext = item;
        }

        private void RecargarReportes()
        {
            var controlador = new ControladorReportesMatriculas();

            grupoDatos.DataContext = null;

            listaReportes.ItemsSource = controlador.ObtenerReportesMatricula(txtBusqueda.Text);

            listaReportes.SelectedIndex = -1;

            txtDescripcion.Text = "";
            filtroActual.Text = txtBusqueda.Text;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ReportarMatricula_Click(object sender, RoutedEventArgs e)
        {
            var nuevoReporte = new Modelo.Entidades.ReporteMatricula();

            nuevoReporte.Matricula = filtroActual.Text;
            nuevoReporte.Detalles = txtDescripcion.Text;

            var controlador = new ControladorReportesMatriculas();

            controlador.CrearReporte(nuevoReporte);

            RecargarReportes();
        }


        private void btnInhabilitar_Click(object sender, RoutedEventArgs e)
        {
            ReporteMatricula reporteSeleccionado;

            if ((reporteSeleccionado = listaReportes.SelectedItem as ReporteMatricula) == null)
                throw new SOTException(Textos.Errores.reporte_no_seleccionado_deshabilitar_exception);

            if (!reporteSeleccionado.Activo)
                throw new SOTException(Textos.Errores.reporte_inactivo_no_desactivable_exception);

            var controlador = new ControladorReportesMatriculas();

            controlador.EliminarReporte(reporteSeleccionado.Id);
            
            RecargarReportes();
        }

        private void txtBusqueda_LostFocus(object sender, RoutedEventArgs e)
        {
            RecargarReportes();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                RecargarReportes();
            }
        }
    }
}
