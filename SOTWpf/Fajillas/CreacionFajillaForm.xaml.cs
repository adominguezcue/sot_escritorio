﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Impresiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Fajillas
{
    /// <summary>
    /// Lógica de interacción para CreacionFajillaForm.xaml
    /// </summary>
    public partial class CreacionFajillaForm : Window
    {
        Fajilla nuevaFajilla;

        public CreacionFajillaForm()
        {
            InitializeComponent();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorFajillas();
            var config = controlador.ObtenerConfiguracionFajillasCargadaFILTRO();

            var fajillaCrear = new Fajilla
            {
                Activa = nuevaFajilla.Activa,
                IdConfiguracionFajilla = nuevaFajilla.IdConfiguracionFajilla
            };

            foreach (var monto in nuevaFajilla.MontosFajilla.Where(m => m.Activa))
            {
                fajillaCrear.MontosFajilla.Add(new MontoFajilla
                {
                    Activa = monto.Activa,
                    Cantidad = monto.Cantidad,
                    IdMontoConfiguracionFajilla = monto.IdMontoConfiguracionFajilla
                });
            }

            controlador.CrearFajilla(fajillaCrear);

            ProcesoExitoso = true;

            QuitarHandlersYCerrar();



            AutorizacionFajillasForm.ImprimirFajilla(fajillaCrear.Id, 1);
            //var documentoReporte = new SOTWpf.Reportes.Fajillas.TicketFajilla();
            //documentoReporte.Load();

            //documentoReporte.SetDataSource(controlador.ObtenerDesgloseEfectivoPorId(fajillaCrear.Id));

            //var configImpresora = new ControladorConfiguracionesImpresoras().ObtenerImpresoras();

            //Impresora.Imprimir(documentoReporte, configImpresora.ImpresoraTickets, 1);

        }

        private void QuitarHandlersYCerrar()
        {
            if (nuevaFajilla != null)
                foreach (var monto in nuevaFajilla.MontosFajilla)
                    monto.PropertyChanged -= CreacionFajillaForm_PropertyChanged;

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            ProcesoExitoso = false;

            QuitarHandlersYCerrar();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            var controlador = new ControladorFajillas();
            var config = controlador.ObtenerConfiguracionFajillasCargadaFILTRO();


            nuevaFajilla = new Fajilla
            {
                Activa = true,
                ConfiguracionFajilla = config
            };

            foreach (var montoC in config.MontosConfiguracionFajilla.Where(m => m.Activa))
            {
                nuevaFajilla.MontosFajilla.Add(new MontoFajilla
                {
                    Activa = true,
                    Cantidad = 0,
                    MontoConfiguracionFajilla = montoC
                });

                if (montoC.IdMonedaExtranjera.HasValue)
                {
                    if (montoC.MonedaExtranjera.MontosFajilla == null)
                        montoC.MonedaExtranjera.MontosFajilla = new List<MontoFajilla>();

                    montoC.MonedaExtranjera.MontosFajilla.Add(nuevaFajilla.MontosFajilla.Last());
                }

                nuevaFajilla.MontosFajilla.Last().PropertyChanged += CreacionFajillaForm_PropertyChanged;
            }

            DataContext = nuevaFajilla;
        }

        private void CreacionFajillaForm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            nuevaFajilla.Valor = nuevaFajilla.MontosFajilla.Where(m => m.Activa).Sum(m => m.Total);
        }

        private void nacionales_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as MontoFajilla;

            e.Accepted = item != null && !item.MontoConfiguracionFajilla.IdMonedaExtranjera.HasValue;
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i);
        }

        public bool ProcesoExitoso { get; private set; }
    }
}
