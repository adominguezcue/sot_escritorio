﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Fajillas
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionFajillasFrorm.xaml
    /// </summary>
    public partial class ConfiguracionFajillasForm : Window
    {
        public ConfiguracionFajillasForm()
        {
            InitializeComponent();

            var controlador = new ControladorFajillas();
            DataContext = controlador.ObtenerConfiguracionFajillas();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            //if (Validation.GetHasError(txtValor))
            //    throw new SOTException(Textos.Errores.campo_con_errores_excepcion);

            var controlador = new ControladorFajillas();
            controlador.ActualizarMontoConfiguracion(((ConfiguracionFajilla)DataContext).Valor);

            MessageBox.Show(Textos.Mensajes.generico_cambios_guardados, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
