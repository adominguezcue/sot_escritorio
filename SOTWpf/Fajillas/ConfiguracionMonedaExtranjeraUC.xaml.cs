﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Fajillas
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionMonedaExtranjeraUC.xaml
    /// </summary>
    public partial class ConfiguracionMonedaExtranjeraUC : UserControl
    {
        public static readonly RoutedEvent EdicionAceptadaEvent = EventManager.RegisterRoutedEvent("EdicionAceptada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ConfiguracionMonedaExtranjeraUC));
        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ConfiguracionMonedaExtranjeraUC));

        public event RoutedEventHandler EdicionAceptada
        {
            add { AddHandler(EdicionAceptadaEvent, value); }
            remove { RemoveHandler(EdicionAceptadaEvent, value); }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add { AddHandler(EdicionCanceladaEvent, value); }
            remove { RemoveHandler(EdicionCanceladaEvent, value); }
        }

        internal DtoConfiguracionMonedaExtranjera ConfiguracionMonedaExtranjera
        {
            get { return DataContext as DtoConfiguracionMonedaExtranjera; }
            set { DataContext = value; }
        }

        public ConfiguracionMonedaExtranjeraUC()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            BindingGroup.CancelEdit();
            BindingGroup.BeginEdit();
            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            //var cActual = ConfiguracionMonedaExtranjera;

            //if (cActual != null)
            //{
            //    var controladorHabitaciones = new SOTControladores.Controladores.ControladorFajillas();

            //    if (cActual.Id == 0)
            //        controladorHabitaciones.CrearHabitacion(cActual);
            //    else
            //        controladorHabitaciones.ModificarHabitacion(cActual);
            //}
            //var XD = BindingGroup.ValidateWithoutUpdate();

            if (BindingGroup.CommitEdit())
            {
                BindingGroup.BeginEdit();

                RaiseEvent(new RoutedEventArgs(EdicionAceptadaEvent));
            }
            else if(BindingGroup.HasValidationError)
            {
                throw new SOTException(string.Join(System.Environment.NewLine, BindingGroup.ValidationErrors.Select(m => m.ErrorContent)));
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            BindingGroup.BeginEdit();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingGroup.CancelEdit();
        }
    }
}
