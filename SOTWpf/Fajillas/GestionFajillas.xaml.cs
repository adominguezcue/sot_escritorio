﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SOTWpf.Fajillas
{
    /// <summary>
    /// Lógica de interacción para GestionFajillas.xaml
    /// </summary>
    public partial class GestionFajillas : UserControl
    {
        private DispatcherTimer timerRecarga;

        public GestionFajillas()
        {
            InitializeComponent();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            var fajillaF = new CreacionFajillaForm();
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, fajillaF);

            CargarCantidadFajillas();

            if (fajillaF.ProcesoExitoso)
                MessageBox.Show(Textos.Mensajes.fajilla_creada_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }
        

        private void btnConsulta_Click(object sender, RoutedEventArgs e)
        {
            var autorizacionF = new AutorizacionFajillasForm();

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, autorizacionF);

            CargarCantidadFajillas();

            //if (autorizacionF.ProcesoExitoso)
            //    MessageBox.Show(Textos.Mensajes.fajillas_autorizadas_exito, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void CargarCantidadFajillas()
        {

            var controladorCortes = new ControladorCortesTurno();

            var corte = controladorCortes.ObtenerUltimoCorte();

            var controlador = new ControladorFajillas();

            contadorFajillas.Text = controlador.ObtenerCantidadFajillasPendientesPorTurno(corte.Id).ToString();
        }

        private void GestionFajillasForm_Load(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {

                if (!new ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarFajillas = true }))
                {
                    Visibility = System.Windows.Visibility.Collapsed;
                    return;
                }

                //CargarCantidadFajillas();

                timerRecarga = new DispatcherTimer();
                timerRecarga.Interval = TimeSpan.FromSeconds(15);
                timerRecarga.Tick += CargarCantidadFajillasEH;
                CargarCantidadFajillasEH(null, null);

                timerRecarga.Start();
            }
        }

        private void CargarCantidadFajillasEH(object sender, EventArgs e)
        {
            try
            {
                CargarCantidadFajillas();
            }
            catch
            {
                contadorFajillas.Text = "?";
            }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if ((e.NewValue as bool?) == true && !new ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarFajillas = true }))
                    Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (timerRecarga != null)
                {
                    timerRecarga.Stop();
                    timerRecarga.Tick -= CargarCantidadFajillasEH;
                }
            }
        }
    }
}
