﻿using Dominio.Nucleo.Dtos;
using MaterialDesignThemes.Wpf;
using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Fajillas
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionMonedasUC.xaml
    /// </summary>
    public partial class ConfiguracionMonedasUC : UserControl
    {
        private ConfiguracionMonedaExtranjeraUC editorForm;
        private Guid identificadorDialogo = Guid.NewGuid();

        private DtoConfiguracionFajilla ConfiguracionActual
        {
            get { return DataContext as DtoConfiguracionFajilla; }
            set { DataContext = value; }
        }
        public ConfiguracionMonedasUC()
        {
            InitializeComponent();

            dhMonedas.Identifier = identificadorDialogo;
        }

        public void CancelarEdicion()
        {
            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_cancelacion_edicion) == MessageBoxResult.Yes)
            {
                BindingGroup.CancelEdit();
                BindingGroup.BeginEdit();
                CargarConfiguracion();
            }
        }

        public void Guardar()
        {
            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_configuracion) == MessageBoxResult.Yes)
            {
                if (BindingGroup.CommitEdit())
                {
                    new ControladorFajillas().GuardarConfiguracion(ConfiguracionActual);

                    CargarConfiguracion();

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.configuracion_exitosa_excepcion);
                }
                else if (BindingGroup.HasValidationError)
                {
                    throw new SOTException(string.Join(System.Environment.NewLine, BindingGroup.ValidationErrors.Select(m => m.ErrorContent)));
                }
            }
        }

        //private void nacionales_Filter(object sender, FilterEventArgs e)
        //{
        //    var item = e.Item as MontoConfiguracionFajilla;

        //    e.Accepted = item != null && item.Activa && !item.IdMonedaExtranjera.HasValue;
        //}

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            BindingGroup.BeginEdit();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarConfiguracion();
            }
        }

        private void CargarConfiguracion()
        {
            var controlador = new ControladorFajillas();
            var config = controlador.ObtenerConfiguracionFajillasCargadaFILTRO();

            if (config != null)
            {
                ConfiguracionActual = new DtoConfiguracionFajilla
                {
                    Valor = config.Valor
                };

                foreach (var grupo in config.MontosConfiguracionFajilla.Where(m => m.Activa).GroupBy(m => m.IdMonedaExtranjera))
                {

                    if (grupo.Key.HasValue)
                    {
                        var monedaE = new DtoConfiguracionMonedaExtranjera
                        {
                            Abreviatura = grupo.First().MonedaExtranjera.Abreviatura,
                            Nombre = grupo.First().MonedaExtranjera.Nombre,
                            ValorCambio = grupo.First().MonedaExtranjera.ValorCambio
                        };

                        ConfiguracionActual.MonedasExtranjeras.Add(monedaE);

                        foreach (var montoC in grupo)
                        {
                            monedaE.Denominaciones.Add(new DtoValorGenerico<decimal> { Valor = montoC.Monto });
                        }
                    }
                    else
                    {
                        foreach (var montoC in grupo)
                        {
                            ConfiguracionActual.DenominacionesNacionales.Add(new DtoValorGenerico<decimal> { Valor = montoC.Monto });
                        }
                    }
                }
            }
            else
                ConfiguracionActual = new DtoConfiguracionFajilla();
        }

        private void btnEliminarDenominacion_Click(object sender, RoutedEventArgs e)
        {
            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                var control = sender as Control;

                var item = (control).DataContext as DtoValorGenerico<decimal>;

                if (!ConfiguracionActual.DenominacionesNacionales.Remove(item))
                {
                    foreach (var moneda in ConfiguracionActual.MonedasExtranjeras)
                    {
                        if (moneda.Denominaciones.Remove(item))
                            break;
                    }
                }


                //CollectionViewSource.GetDefaultView(listaMontos.ItemsSource).Refresh();
            }
        }

        private void btnAgregarDenominacion_Click(object sender, RoutedEventArgs e)
        {
            var control = sender as Control;

            var config = (control).DataContext as DtoConfiguracionFajilla;

            if (config != null)
            {
                config.DenominacionesNacionales.Add(new DtoValorGenerico<decimal>());
            }
            else
            {
                var moneda = (control).DataContext as DtoConfiguracionMonedaExtranjera;

                moneda.Denominaciones.Add(new DtoValorGenerico<decimal>());
            }
        }

        private void btnEditarMonedaExtranjera_Click(object sender, RoutedEventArgs e)
        {
            MostrarEditor((sender as Control).DataContext as DtoConfiguracionMonedaExtranjera);
        }

        private void btnEliminarMonedaExtranjera_Click(object sender, RoutedEventArgs e)
        {
            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                var item = (sender as Control).DataContext as DtoConfiguracionMonedaExtranjera;
                ConfiguracionActual.MonedasExtranjeras.Remove(item);

                //CollectionViewSource.GetDefaultView(listaMontos.ItemsSource).Refresh();
            }
        }

        private void btnAgregarMonedaExtranjera_Click(object sender, RoutedEventArgs e)
        {
            MostrarEditor();
        }

        private async Task MostrarEditor(DtoConfiguracionMonedaExtranjera monedaEditar = null)
        {
            editorForm = new ConfiguracionMonedaExtranjeraUC
            {
                DataContext = monedaEditar ?? new DtoConfiguracionMonedaExtranjera()
            };
            editorForm.EdicionAceptada += EditorForm_EdicionAceptada;
            editorForm.EdicionCancelada += EditorForm_EdicionCancelada;
            //editorForm.EdicionRealizada += BotoneraBase_AceptarClick;
            //editorForm.EdicionCancelada += BotoneraBase_CancelarClick;

            await DialogHost.Show(editorForm, identificadorDialogo);
        }

        private void EjecutarComando()
        {
            if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(null))
                ((ICommand)DialogHost.CloseDialogCommand).Execute(null);

            if (editorForm != null)
            {
                editorForm.EdicionAceptada -= EditorForm_EdicionAceptada;
                editorForm.EdicionCancelada -= EditorForm_EdicionCancelada;
            }
        }

        private void EditorForm_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            EjecutarComando();
        }

        private void EditorForm_EdicionAceptada(object sender, RoutedEventArgs e)
        {
            var moneda = editorForm.ConfiguracionMonedaExtranjera;

            if (moneda != null && !ConfiguracionActual.MonedasExtranjeras.Any(m => m.GetHashCode() == moneda.GetHashCode()))
                ConfiguracionActual.MonedasExtranjeras.Add(moneda);

            EjecutarComando();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            BindingGroup.CancelEdit();
        }
    }
}
