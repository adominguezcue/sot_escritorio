﻿using SOTControladores.Controladores;
using SOTWpf.Impresiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Fajillas
{
    /// <summary>
    /// Lógica de interacción para AutorizacionFajillas.xaml
    /// </summary>
    public partial class AutorizacionFajillasForm : Window
    {
        public AutorizacionFajillasForm()
        {
            InitializeComponent();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var fajillaConfirma = lbConfirmar.SelectedItem as Modelo.Entidades.Fajilla;

            if (fajillaConfirma == null)
                throw new SOTException(Textos.Errores.fajilla_no_seleccionada_exception);


            var controladorF = new ControladorFajillas();

            controladorF.AutorizarFajilla(fajillaConfirma.Id);

            CargarFajillas();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarFajillas();
            }
        }

        private void CargarFajillas()
        {
            var controladorCortes = new ControladorCortesTurno();

            var corte = controladorCortes.ObtenerUltimoCorte();

            var controladorF = new ControladorFajillas();

            var fajillas = controladorF.ObtenerFajillasPorTurno(corte.Id, false);

            lbConfirmar.ItemsSource = fajillas.Where(m => !m.Autorizada).ToList();
            lbRecibidas.ItemsSource = fajillas.Where(m => m.Autorizada).ToList();
        }

        private void btnImprimirCuenta_Click(object sender, RoutedEventArgs e)
        {
            var fajillaConfirma = lbConfirmar.SelectedItem as Modelo.Entidades.Fajilla;

            if (fajillaConfirma == null)
                throw new SOTException(Textos.Errores.fajilla_no_seleccionada_exception);

            ImprimirFajilla(fajillaConfirma.Id, 1);
        }

        internal static void ImprimirFajilla(int idFajilla, int copias)
        {
            var controlador = new ControladorFajillas();

            var documentoReporte = new SOTWpf.Reportes.Fajillas.TicketFajilla();
            documentoReporte.Load();

            var datosFajilla = controlador.ObtenerDesgloseEfectivoPorId(idFajilla);

            documentoReporte.Database.Tables["Modelo_Dtos_DtoCabeceraFajilla"].SetDataSource(new List<Modelo.Dtos.DtoCabeceraFajilla>() { datosFajilla });
            documentoReporte.Database.Tables["Modelo_Dtos_DtoMoneda"].SetDataSource(datosFajilla.Desglose);

            var configImpresora = new ControladorConfiguracionesImpresoras().ObtenerImpresoras();

            Impresora.Imprimir(documentoReporte, configImpresora.ImpresoraTickets, copias);
        }
    }
}
