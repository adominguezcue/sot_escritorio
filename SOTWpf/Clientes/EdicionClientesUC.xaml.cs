﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Clientes
{
    /// <summary>
    /// Lógica de interacción para EdicionClientesUC.xaml
    /// </summary>
    public partial class EdicionClientesUC : UserControl
    {
        public static readonly RoutedEvent GuardadoExitosoEvent = EventManager.RegisterRoutedEvent("GuardadoExitoso", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionClientesUC));
        public static readonly RoutedEvent EliminacionExitosaEvent = EventManager.RegisterRoutedEvent("EliminacionExitosa", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionClientesUC));
        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionClientesUC));

        public event RoutedEventHandler GuardadoExitoso
        {
            add
            {
                AddHandler(GuardadoExitosoEvent, value);
            }

            remove
            {
                RemoveHandler(GuardadoExitosoEvent, value);
            }
        }

        public event RoutedEventHandler EliminacionExitosa
        {
            add
            {
                AddHandler(EliminacionExitosaEvent, value);
            }

            remove
            {
                RemoveHandler(EliminacionExitosaEvent, value);
            }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add
            {
                AddHandler(EdicionCanceladaEvent, value);
            }

            remove
            {
                RemoveHandler(EdicionCanceladaEvent, value);
            }
        }

        private Cliente ClienteActual
        {
            get { return DataContext as Cliente; }
            set { DataContext = value; }
        }
        public EdicionClientesUC()
        {
            InitializeComponent();

            cbTiposPersonas.ItemsSource = Transversal.Extensiones.EnumExtensions.ComoListaDto<TiposPersonas>();
        }

        public void Guardar()
        {
            if (ClienteActual != null)
            {
                var controlador = new ControladorClientes();

                if (ClienteActual.Cod_Cte == 0)
                {
                    controlador.CrearCliente(ClienteActual);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.creacion_elemento_exitosa);
                }
                else
                {
                    controlador.ModificarCliente(ClienteActual);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.modificacion_elemento_exitosa);
                }

                ClienteActual = new Cliente();

                RaiseEvent(new RoutedEventArgs(GuardadoExitosoEvent));
            }
        }

        public void Eliminar()
        {
            if (ClienteActual != null && ClienteActual.Cod_Cte != 0)
            {
                var controlador = new ControladorClientes();

                if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) != MessageBoxResult.Yes)
                    return;

                controlador.EliminarCliente(ClienteActual.Cod_Cte);

                Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_elemento_exitosa);

                ClienteActual = new Cliente();

                RaiseEvent(new RoutedEventArgs(EliminacionExitosaEvent));
            }
        }

        public void Cancelar()
        {
            ClienteActual = new Cliente();

            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            CargarEstadosCiudades();
        }

        public void CargarEstadosCiudades()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorEstados = new ControladorEstados();
                var controladorCiudades = new ControladorCiudades();

                var estado = cbEstados.SelectedValue;
                var ciudad = cbCiudades.SelectedValue;

                cbEstados.ItemsSource = controladorEstados.ObtenerEstados();
                cbCiudades.ItemsSource = controladorCiudades.ObtenerCiudades();

                cbEstados.SelectedValue = estado;
                cbCiudades.SelectedValue = ciudad;
            }
        }

        private void CbEstados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var controladorCiudades = new ControladorCiudades();
            cbCiudades.ItemsSource = controladorCiudades.ObtenerCiudades((cbEstados.SelectedValue as int?) ?? 0);
        }
    }
}
