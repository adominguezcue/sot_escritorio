﻿using SOTControladores.Controladores;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using SOTWpf.Dtos;
using System.Windows.Controls;
using Modelo.Dtos;

namespace SOTWpf.Clientes
{
    /// <summary>
    /// Lógica de interacción para ClientePorMatricula.xaml
    /// </summary>
    public partial class ClientePorMatricula : Window
    {
        List<DtoHabitacionesxMatricula> habitacionesxmatricula = new List<DtoHabitacionesxMatricula>();
        List<DtTotalComandasxRenta>  ComandasParciales = new List<DtTotalComandasxRenta>();
        List<DtTotalComandasxRenta> ComandasTotales = new List<DtTotalComandasxRenta>();
        List<Modelo.Entidades.AutomovilRenta> Matriculas = new List<Modelo.Entidades.AutomovilRenta>();
        public ClientePorMatricula()
        {
            InitializeComponent();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CargaMatriculas()
        {
            if (this.IsLoaded)
            {
                ControladorClientesMatricula controladorClientesMatricula = new ControladorClientesMatricula();
                 Matriculas = new List<Modelo.Entidades.AutomovilRenta>();
                if (!txt_busquedamatricula.Equals(""))
                {
                    Matriculas = controladorClientesMatricula.ObtieneMatriculasAutos(txt_busquedamatricula.Text);
                    dgmatricula.ItemsSource = Matriculas;
                    CargaHabitaciones(Matriculas);
                }
            }
        }

        private void BtnBotonReporteClienteMatricula_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();
            System.DateTime localDate = System.DateTime.Now;
            var documentoReporte = new SOTWpf.Reportes.ClienteMatricula.ClienteMatricula();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            {
                Direccion=config.Direccion,
                FechaInicio = localDate,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            if (Matriculas != null && Matriculas.Count > 0)
            {
                List<DtoCabeceraMatricula> lista1 = new List<DtoCabeceraMatricula>();
                foreach (Modelo.Entidades.AutomovilRenta datos in Matriculas)
                {
                    DtoCabeceraMatricula dtoReporteMatricula = new DtoCabeceraMatricula();
                    dtoReporteMatricula.Modelo = datos.Modelo.ToString();
                    dtoReporteMatricula.Matricula = datos.Matricula.ToString();
                    dtoReporteMatricula.Marca = datos.Marca.ToString();
                    lista1.Add(dtoReporteMatricula);
                }
                documentoReporte.Subreports["DetalleMatricula"].SetDataSource(lista1);
                documentoReporte.Subreports["DetalleHabitacion"].Database.Tables[0].SetDataSource(habitacionesxmatricula);
                documentoReporte.Subreports["DetalleHabitacion"].Database.Tables[1].SetDataSource(ComandasParciales);
            }
            else
            {
                Utilidades.Dialogos.Aviso("Introduce una Placa de Vehiculo");
                return;
            }
            var visorR = new VisorReportes();
            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();
            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargaMatriculas();
            }
        }
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            CargaMatriculas();
        }
        private void Txt_busquedamatricula_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void CargaHabitaciones(List<Modelo.Entidades.AutomovilRenta> automovilRentas)
        {
            habitacionesxmatricula = new List<DtoHabitacionesxMatricula>();

            foreach (Modelo.Entidades.AutomovilRenta item in automovilRentas)
            {
                ControladorClientesMatricula controladorClientesMatricula = new ControladorClientesMatricula();
                habitacionesxmatricula = controladorClientesMatricula.ObtieneHabitacionesPorMatricula(item.Matricula);  
            }
            dghabitaciones.ItemsSource = habitacionesxmatricula;
            CargaComandas(habitacionesxmatricula);
            CargaTotales(habitacionesxmatricula, ComandasTotales);
        }

        private void CargaTotales(List<DtoHabitacionesxMatricula> habotacionprecios, List<DtTotalComandasxRenta> ComandasTotales)
        {
            decimal preciohabi = 0;
            decimal personasextra = 0;
            decimal nochesEstra = 0;
            decimal horasextra = 0;
            decimal paquetes = 0;
            decimal cortesias = 0;
            decimal descuentos = 0;
            foreach (DtoHabitacionesxMatricula datos in habotacionprecios)
            {
                preciohabi = preciohabi + datos.PrecioHabitacion;
                personasextra = personasextra + datos.PrecioPersonaExtra;
                nochesEstra = nochesEstra + datos.PrecioNochesExtra;
                horasextra = horasextra + datos.PrecioHorasExtras;
                paquetes = paquetes + datos.PrecioPaquetes;
                cortesias = cortesias + datos.PrecioCortesias;
                descuentos = descuentos + datos.PrecioDescuentos;
            }
            decimal preciototal = 0;
            foreach (DtTotalComandasxRenta datos in ComandasTotales)
            {
                preciototal = preciototal + datos.TotalPrecioComandas;
            }
            TotalPreciohabi.Text = "$" + preciohabi.ToString("n2");
            TotalPersonasExtr.Text= "$" + personasextra.ToString("n2");
            TotalNochesExtr.Text = "$" + nochesEstra.ToString("n2");
            TotalHorasExtr.Text = "$" + horasextra.ToString("n2");
            TotalPaquetes.Text = "$" + paquetes.ToString("n2");
            TotalCortesias.Text = "$" + cortesias.ToString("n2");
            TotalDescuentos.Text = "$" + descuentos.ToString("n2");
            decimal totalHabitaciones = 0;
            totalHabitaciones = (preciohabi + personasextra + nochesEstra + horasextra + paquetes) - (cortesias + descuentos);
            TotalNetoHabitaciones.Text = "$" + totalHabitaciones.ToString("n2");
            TotalNeto.Text = "$" + (totalHabitaciones+ preciototal).ToString("n2");
        }


        private void CargaComandas(List<DtoHabitacionesxMatricula> habotacionprecios)
        {
            ComandasParciales = new List<DtTotalComandasxRenta>();
            List<DtTotalComandasxRenta> comandas = new List<DtTotalComandasxRenta>();
            foreach (DtoHabitacionesxMatricula datos in habotacionprecios)
            {
                ControladorClientesMatricula controladorClientesMatricula = new ControladorClientesMatricula();
                comandas = controladorClientesMatricula.ObtieneComandasPorRenta(datos.Idrenta);
                if (comandas != null && comandas.Count > 0)
                {
                    foreach (DtTotalComandasxRenta datoscomandas1 in comandas)
                    {
                        DtTotalComandasxRenta datoscomandas = new DtTotalComandasxRenta();
                        datoscomandas.Idrenta = datoscomandas1.Idrenta;
                        datoscomandas.Totalcomandas = datoscomandas1.Totalcomandas;
                        datoscomandas.TotalPrecioComandas = datoscomandas1.TotalPrecioComandas;
                        ComandasParciales.Add(datoscomandas);
                    }
                }
                else
                {
                        DtTotalComandasxRenta datoscomandas = new DtTotalComandasxRenta();
                        datoscomandas.Idrenta = datos.Idrenta;
                        datoscomandas.Totalcomandas = 0;
                        datoscomandas.TotalPrecioComandas = 0;
                        ComandasParciales.Add(datoscomandas);
                }
            }
            ComandasTotales = new List<DtTotalComandasxRenta>();
            DtTotalComandasxRenta totales = new DtTotalComandasxRenta();
            int totalcomandas = 0;
            decimal preciototal = 0;
            foreach (DtTotalComandasxRenta datos in ComandasParciales)
            {

                totalcomandas = totalcomandas + datos.Totalcomandas;
                preciototal = preciototal + datos.TotalPrecioComandas;
            }
            totales.Idrenta = 1;
            totales.Totalcomandas = totalcomandas;
            totales.TotalPrecioComandas = preciototal;
            ComandasTotales.Add(totales);
            TotalComandas.Text = totalcomandas.ToString("n2");
            Totalpreciocomandas.Text = "$"+preciototal.ToString("n2");
            dgComandas.ItemsSource = ComandasTotales;
        }

        private void Dghabitaciones_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
