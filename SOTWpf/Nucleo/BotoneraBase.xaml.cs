﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Nucleo
{
    /// <summary>
    /// Lógica de interacción para BotoneraBase.xaml
    /// </summary>
    [ContentProperty("AdditionalContent")]
    public partial class BotoneraBase : UserControl
    {
        public event RoutedEventHandler AceptarClick;
        public event RoutedEventHandler CancelarClick;

        public object AdditionalContent
        {
            get { return (object)GetValue(AdditionalContentProperty); }
            set { SetValue(AdditionalContentProperty, value); }
        }
        public static readonly DependencyProperty AdditionalContentProperty =
            DependencyProperty.Register("AdditionalContent", typeof(object), typeof(BotoneraBase),
              new PropertyMetadata(null));

        public bool OcultarBotonAceptar
        {
            get { return btnAceptar.Visibility == System.Windows.Visibility.Collapsed; }
            set
            {
                if (value)
                    btnAceptar.Visibility = System.Windows.Visibility.Collapsed;
                else
                    btnAceptar.Visibility = System.Windows.Visibility.Visible;
            }
        }

        public BotoneraBase()
        {
            InitializeComponent();
            //var estilo = (Style)FindResource("BotoneraBaseEstilo");
            //Style = estilo;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventHandler handler = AceptarClick;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventHandler handler = CancelarClick;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            Border border = new Border();
            border.VerticalAlignment = VerticalAlignment.Stretch;
            border.HorizontalAlignment = HorizontalAlignment.Stretch;

            Grid grid = new Grid();

            ContentPresenter content = new ContentPresenter();
            content.Content = Content;

            grid.Children.Add(content);
            border.Child = grid;

            Content = border;
        }
    }
}
