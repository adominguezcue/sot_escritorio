﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Nucleo
{
    /// <summary>
    /// Lógica de interacción para BotoneraBase.xaml
    /// </summary>
    [ContentProperty("AdditionalContent")]
    public class BotoneraBase : ContentControl
    {
        ContentPresenter cp;

        public event RoutedEventHandler AceptarClick;
        public event RoutedEventHandler CancelarClick;
        Componentes.Buttons.FloatingIconButton btnAceptar, btnCancelar;

        protected void InitializeComponent()//override void OnInitialized(EventArgs e)
        {
            //base.OnInitialized(e);


            btnAceptar = new Componentes.Buttons.FloatingIconButton
            {
                Name = "btnAceptar",
                Width = 104,
                Height = 80,
                IconSize = new Size(48, 48),
                Margin = new Thickness(2),
                Text = "ACEPTAR",
                ImageSource = Application.Current.Resources["estado_icono_habilitada"] as ImageSource,//Recursos.ImagenesEstados.estado_icono_habilitada,
                VerticalAlignment = System.Windows.VerticalAlignment.Center
            };

            btnAceptar.Click += btnAceptar_Click;

            btnCancelar = new Componentes.Buttons.FloatingIconButton
            {
                Name = "btnCancelar",
                Width = 104,
                Height = 80,
                IconSize = new Size(48, 48),
                Margin = new Thickness(2),
                Text = "SALIR",
                ImageSource = Application.Current.Resources["icono_cancelar"] as ImageSource,//Recursos.Generales.icono_cancelar,
                VerticalAlignment = System.Windows.VerticalAlignment.Center
            };
            Grid.SetColumn(btnCancelar, 1);

            btnCancelar.Click += btnCancelar_Click;

            var cl = new MaterialDesignThemes.Wpf.ColorZone { Mode = MaterialDesignThemes.Wpf.ColorZoneMode.PrimaryMid };

            var gr = new Grid
            {
                //Orientation = System.Windows.Controls.Orientation.Horizontal,
                FlowDirection = System.Windows.FlowDirection.RightToLeft,
                VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                //Background = new SolidColorBrush(Colors.Gray)
            };

            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
            gr.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            cl.Content = gr;

            cp = new ContentPresenter
            {
                Name = "cp",
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };

            cp.SetBinding(ContentPresenter.ContentProperty, new Binding()
            {
                Path = new PropertyPath("AdditionalContent"),
                Source = this
            });
            Grid.SetColumn(cp, 2);

            gr.Children.Add(btnAceptar);
            gr.Children.Add(btnCancelar);
            gr.Children.Add(cp);

            //var sw = new ScrollViewer
            //{
            //    HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
            //    VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
            //    VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
            //    HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
            //    Content = wrp
            //};

            Content = cl;
        }

        public HorizontalAlignment HorizontalAdditionalContentAlignment
        {
            get { return (HorizontalAlignment)GetValue(HorizontalAdditionalContentAlignmentProperty); }
            set { SetValue(HorizontalAdditionalContentAlignmentProperty, value); }
        }
        public static readonly DependencyProperty HorizontalAdditionalContentAlignmentProperty =
            DependencyProperty.Register("HorizontalAdditionalContentAlignment", typeof(HorizontalAlignment), typeof(BotoneraBase),
              new PropertyMetadata(System.Windows.HorizontalAlignment.Left, new PropertyChangedCallback(HorizontalAdditionalContentAlignmentChanged)));

        private static void HorizontalAdditionalContentAlignmentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var bb = d as BotoneraBase;
            bb.cp.HorizontalAlignment = bb.HorizontalAdditionalContentAlignment;
        }

        public object AdditionalContent
        {
            get { return (object)GetValue(AdditionalContentProperty); }
            set { SetValue(AdditionalContentProperty, value); }
        }
        public static readonly DependencyProperty AdditionalContentProperty =
            DependencyProperty.Register("AdditionalContent", typeof(object), typeof(BotoneraBase),
              new PropertyMetadata(null));

        public bool OcultarBotonAceptar
        {
            get { return btnAceptar.Visibility == System.Windows.Visibility.Collapsed; }
            set
            {
                if (value)
                    btnAceptar.Visibility = System.Windows.Visibility.Collapsed;
                else
                    btnAceptar.Visibility = System.Windows.Visibility.Visible;
            }
        }

        public BotoneraBase()
        {
            InitializeComponent();
            //var estilo = (Style)FindResource("BotoneraBaseEstilo");
            //Style = estilo;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventHandler handler = AceptarClick;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventHandler handler = CancelarClick;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
