﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Nucleo
{
    /// <summary>
    /// Lógica de interacción para UserControl1.xaml
    /// </summary>
    [ContentProperty("AdditionalContent")]
    public partial class LogoBorder : ContentControl
    {
        public LogoBorder()
        {
            InitializeComponent();
            cp.DataContext = this;
        }

        public object AdditionalContent
        {
            get { return (object)GetValue(AdditionalContentProperty); }
            set { SetValue(AdditionalContentProperty, value); }
        }
        public static readonly DependencyProperty AdditionalContentProperty =
            DependencyProperty.Register("AdditionalContent", typeof(object), typeof(LogoBorder),
              new PropertyMetadata(null));
    }
}
