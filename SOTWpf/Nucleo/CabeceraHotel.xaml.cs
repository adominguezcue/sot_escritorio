﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Nucleo
{
    /// <summary>
    /// Lógica de interacción para CabeceraHotel.xaml
    /// </summary>
    public partial class CabeceraHotel : UserControl
    {
        public string TextoExtra
        {
            get
            {
                return (string)GetValue(TextoExtraProperty);
            }
            set
            {
                SetValue(TextoExtraProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextoExtraProperty =
            DependencyProperty.Register("TextoExtra", typeof(string), typeof(CabeceraHotel), new PropertyMetadata("", new PropertyChangedCallback(TextoExtra_Cambiado)));

        private static void TextoExtra_Cambiado(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CabeceraHotel nb = d as CabeceraHotel;
            nb.extra.Text = e.NewValue != null ? e.NewValue.ToString() : "";
        }

        public CabeceraHotel()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var configuracion = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

                textoLogotipo1.Content = configuracion?.Nombre ?? textoLogotipo1.Content;
                textoLogotipo2.Content = configuracion?.SubNombre ?? textoLogotipo2.Content;
            }
        }
    }
}
