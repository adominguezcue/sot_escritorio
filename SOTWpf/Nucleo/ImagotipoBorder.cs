﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;

namespace SOTWpf.Nucleo
{
    /*
     <Border>
                    <Border.Background>
                        <ImageBrush
                x:Name="logo"
                ImageSource="{StaticResource Logo_Default}"
                RenderOptions.BitmapScalingMode="HighQuality"
                Stretch="Uniform"/>
                    </Border.Background>
                    <ContentPresenter Name="cp" Content="{Binding AdditionalContent, ElementName=lb}" />
                </Border>
         */
    [ContentProperty("AdditionalContent")]
    public class ImagotipoBorder : ContentControl
    {
        ContentPresenter cp;

        protected void InitializeComponent()//override void OnInitialized(EventArgs e)
        {
            //base.OnInitialized(e);

            //var cl = new MaterialDesignThemes.Wpf.ColorZone { Mode = MaterialDesignThemes.Wpf.ColorZoneMode.PrimaryMid };

            var br = new Border
            {
                //FlowDirection = System.Windows.FlowDirection.RightToLeft,
                //VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
                //HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch
            };

            ImageSource imgs = null;

            try
            {
                if (App.Imagotipo != null)
                    imgs = App.Imagotipo;
            }
            catch
            {
            }

            var imb = new ImageBrush
            {
                ImageSource = imgs ?? Application.Current.Resources["Logo_Default"] as ImageSource,
                Stretch = Stretch.Uniform
            };

            RenderOptions.SetBitmapScalingMode(imb, BitmapScalingMode.HighQuality);

            br.Background = imb;

            //cl.Content = br;

            cp = new ContentPresenter
            {
                Name = "cp",
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left
            };

            cp.SetBinding(ContentPresenter.ContentProperty, new Binding()
            {
                Path = new PropertyPath("AdditionalContent"),
                Source = this
            });

            br.Child = cp;

            //var sw = new ScrollViewer
            //{
            //    HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
            //    VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
            //    VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
            //    HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
            //    Content = wrp
            //};

            Content = br;// cl;
            VerticalContentAlignment = VerticalAlignment.Stretch;
            HorizontalContentAlignment = HorizontalAlignment.Stretch;
        }

        public HorizontalAlignment HorizontalAdditionalContentAlignment
        {
            get { return (HorizontalAlignment)GetValue(HorizontalAdditionalContentAlignmentProperty); }
            set { SetValue(HorizontalAdditionalContentAlignmentProperty, value); }
        }
        public static readonly DependencyProperty HorizontalAdditionalContentAlignmentProperty =
            DependencyProperty.Register("HorizontalAdditionalContentAlignment", typeof(HorizontalAlignment), typeof(ImagotipoBorder),
              new PropertyMetadata(System.Windows.HorizontalAlignment.Left, new PropertyChangedCallback(HorizontalAdditionalContentAlignmentChanged)));

        private static void HorizontalAdditionalContentAlignmentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var bb = d as ImagotipoBorder;
            bb.cp.HorizontalAlignment = bb.HorizontalAdditionalContentAlignment;
        }

        public object AdditionalContent
        {
            get { return (object)GetValue(AdditionalContentProperty); }
            set { SetValue(AdditionalContentProperty, value); }
        }
        public static readonly DependencyProperty AdditionalContentProperty =
            DependencyProperty.Register("AdditionalContent", typeof(object), typeof(ImagotipoBorder),
              new PropertyMetadata(null));

        public ImagotipoBorder()
        {
            InitializeComponent();
            //var estilo = (Style)FindResource("LogoBorderEstilo");
            //Style = estilo;
        }
    }
}
