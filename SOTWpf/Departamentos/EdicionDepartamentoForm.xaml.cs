﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Departamentos
{
    /// <summary>
    /// Lógica de interacción para EdicionDepartamentoForm.xaml
    /// </summary>
    public partial class EdicionDepartamentoForm : Window
    {
        Departamento Depa
        {
            get { return DataContext as Departamento; }
            set { DataContext = value; }
        }

        public EdicionDepartamentoForm(Departamento departamento = null)
        {
            InitializeComponent();

            Depa = departamento ?? new Departamento();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (Depa != null)
            {
                if (Depa.Cod_Dpto == 0)
                {
                    var controlador = new SOTControladores.Controladores.ControladorDepartamentos();

                    controlador.CrearDepartamento(Depa.ObtenerCopiaDePrimitivas());

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.creacion_elemento_exitosa);
                }
                else
                {
                    var controlador = new SOTControladores.Controladores.ControladorDepartamentos();

                    var copia = Depa.ObtenerCopiaDePrimitivas();
                    copia.Cod_Dpto = Depa.Cod_Dpto;

                    controlador.ModificarDepartamento(copia);
                    
                    Utilidades.Dialogos.Aviso(Textos.Mensajes.modificacion_elemento_exitosa);
                }
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
