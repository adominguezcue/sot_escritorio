﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Departamentos
{
    /// <summary>
    /// Lógica de interacción para GestionDepartamentosForm.xaml
    /// </summary>
    public partial class GestionDepartamentosForm : Window
    {
        ObservableCollection<Departamento> items = new ObservableCollection<Departamento>();
        public GestionDepartamentosForm()
        {
            InitializeComponent();

            ((CollectionViewSource)this.Resources["departamentosCVS"]).Source = items;
        }

        private void btnEliminarCategoria_Click(object sender, RoutedEventArgs e)
        {
            var departamento = dgvDepartamentos.SelectedItem as Departamento;

            if (departamento == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (departamento.Editable != true)
                throw new SOTException(Textos.Errores.elemento_no_editable_excepcion);

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                var controlador = new SOTControladores.Controladores.ControladorDepartamentos();

                controlador.EliminarDepartamento(departamento.Cod_Dpto);

                Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_elemento_exitosa);

                Recargar();
            }
        }

        private void btnModificarCategoria_Click(object sender, RoutedEventArgs e)
        {
            var departamento = dgvDepartamentos.SelectedItem as Departamento;

            if (departamento == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (departamento.Editable != true)
                throw new SOTException(Textos.Errores.elemento_no_editable_excepcion);

            Utilidades.Dialogos.MostrarDialogos(this, new EdicionDepartamentoForm(departamento));

            Recargar();
        }

        private void btnAltaCategoria_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionDepartamentoForm());

            Recargar();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                Recargar();
            }
        }

        private void Recargar()
        {
            var controlador = new SOTControladores.Controladores.ControladorDepartamentos();

            items.Clear();
            
            foreach(var item in controlador.ObtenerDepartamentos())
                items.Add(item);
        }

        private void departamentosCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as Departamento;

            e.Accepted = item != null && item.Desc_Dpto.ToUpper().Contains(txtNombre.Text.Trim().ToUpper());
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(dgvDepartamentos.ItemsSource).Refresh();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
