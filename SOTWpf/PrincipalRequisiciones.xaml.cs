﻿using Datos.Nucleo.Contextos;
using Modelo.Sistemas.Constantes;
using Modelo.Sistemas.Entidades.Dtos;
using SOTControladores.Controladores;
using SOTWpf.Logs;
using SOTWpf.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Transversal.Excepciones;
using ZctSOT;

namespace SOTWpf
{
    /// <summary>
    /// Lógica de interacción para PrincipalRequisiciones.xaml
    /// </summary>
    public partial class PrincipalRequisiciones : Window
    {
        private DispatcherTimer timerRecarga;
        private ObservableCollection<string> listaLogs;
        private bool cargando = false;
        private static readonly object bloqueador = new object();

        public PrincipalRequisiciones()
        {
            InitializeComponent();

            listaLogs = new ObservableCollection<string>();
            logs.ItemsSource = listaLogs;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var conn = ConexionSOT.Default;

                if (conn != null)
                    ConexionHelper.CambiarConexion(false, conn.DbSOT, conn.OrigenSOT, conn.UsuarioSOT, conn.PassSOT, conn.SeguridadIntegradaSOT);

                LoggerRequisiciones.EscribirLog = new Action<string>((mensaje) =>
                {
                    // Simulate some work taking place 
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                (ThreadStart)delegate ()
                                {
                                    listaLogs.Add(mensaje);

                                    if (listaLogs.Count > 100)
                                        listaLogs.RemoveAt(0);
                                }
                                  );

                });

                var parametros = new SOTControladores.Controladores.ControladorConfiguracionSistemas().ObtenerParametros(IdentificadoresSistemas.CONFIGURADOR_SOT);

                var parametrosConfig = new ParametrosConfigurador();

                foreach (var propiedad in typeof(ParametrosConfigurador).GetProperties())
                {
                    var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                    if (parametro != null)
                    {
                        propiedad.SetValue(parametrosConfig, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                    }
                }

                if (parametrosConfig.ConfiguracionFinalizada)
                {
                    nudMinutos.Number = 5;
                    timerRecarga = new DispatcherTimer();
                    timerRecarga.Interval = TimeSpan.FromMinutes(5);
                    timerRecarga.Tick += timerRecarga_Tick;

                    timerRecarga.Start();
                }
                else
                    throw new SOTException("La configuración de la sucursal no ha finalizado");

                
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            LoggerRequisiciones.EscribirLog = null;

            if (timerRecarga != null)
            {
                timerRecarga.Stop();
                timerRecarga.Tick -= timerRecarga_Tick;
            }
        }

        private void timerRecarga_Tick(object sender, EventArgs e)
        {
            lock (bloqueador)
            {
                if (cargando)
                    return;

                cargando = true;
            }

            try
            {
                var suc = new ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().NombreSucursal;

                ControladorSincronizacionExterna.RealizarActualizacionInformacionExterna(suc);
            }
            finally
            {
                lock (bloqueador)
                {
                    cargando = false;
                }
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (System.Windows.MessageBox.Show(SOTWpf.Textos.Mensajes.salir_sistema, SOTWpf.Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                e.Cancel = true;

            base.OnClosing(e);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (timerRecarga != null)
            {
                timerRecarga.Interval = TimeSpan.FromMinutes((double)nudMinutos.Number);
            }
        }
    }
}
