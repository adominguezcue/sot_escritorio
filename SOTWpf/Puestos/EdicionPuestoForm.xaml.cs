﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Puestos
{
    /// <summary>
    /// Lógica de interacción para EdicionPuestoForm.xaml
    /// </summary>
    public partial class EdicionPuestoForm : Window
    {
        Puesto PuestoActual
        {
            get { return DataContext as Puesto; }
            set 
            {
                DataContext = value;
                txtNombre.IsEnabled = value != null && value.Id == 0;
            }
        }

        public EdicionPuestoForm(Puesto puesto = null)
        {
            InitializeComponent();

            PuestoActual = puesto ?? new Puesto();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
            //    if (idPuesto != 0)
            //    {
            //        var controladorPuestos = new SOTControladores.Controladores.ControladorPuestos();

            //        PuestoActual = controladorPuestos.ObtenerPuestoConPermisos(idPuesto);
            //    }
            //    else 
            //    {
            //        PuestoActual = new Puesto { Activo = true, Permisos = new Permisos() };
            //    }
                var controladorAreas = new SOTControladores.Controladores.ControladorAreas();

                cbAreas.ItemsSource = controladorAreas.ObtenerAreasActivas();

                var controladorRoles = new SOTControladores.Controladores.ControladorRoles();

                cbRoles.ItemsSource = controladorRoles.ObtenerRoles();
            }
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (PuestoActual != null)
            {
                var clon = PuestoActual.ObtenerCopiaDePrimitivas();
                clon.Id = PuestoActual.Id;

                var controladorPuestos = new SOTControladores.Controladores.ControladorPuestos();

                if (PuestoActual.Id == 0)
                {
                    controladorPuestos.CrearPuesto(clon);
                }
                else 
                {
                    controladorPuestos.ModificarPuesto(PuestoActual.Id, PuestoActual.IdRol, PuestoActual.IdArea);
                }
            }
            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
