﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Puestos
{
    /// <summary>
    /// Lógica de interacción para GestionPuestosForm.xaml
    /// </summary>
    public partial class GestionPuestosForm : Window
    {
        public GestionPuestosForm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarPuestos();
            }
        }

        private void CargarPuestos()
        {
            var controladorPuestos = new SOTControladores.Controladores.ControladorPuestos();

            tabla.ItemsSource = controladorPuestos.ObtenerPuestos();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var puesto = (sender as Control).DataContext as Modelo.Entidades.Puesto;

            if (puesto != null)
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionPuestoForm(puesto));

            CargarPuestos();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var puesto = (sender as Control).DataContext as Modelo.Entidades.Puesto;

            if (puesto != null && MessageBox.Show(string.Format(Textos.Mensajes.confirmar_eliminacion_puesto, puesto.Nombre), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorPuestos = new SOTControladores.Controladores.ControladorPuestos();
                controladorPuestos.EliminarPuesto(puesto.Id);

                CargarPuestos();
            }
        }

        private void btnCrearPuesto_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionPuestoForm());

            CargarPuestos();
        }
    }
}
