﻿using SOTWpf.Rentas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Reservaciones
{
    internal class ResumenReservacion : ResumenRenta
    {
        int _descuentos;

        public int Descuentos
        {
            get
            {
                return _descuentos;
            }
            set
            {
                _descuentos = value;
                NotifyPropertyChanged();
            }
        }
    }
}
