﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Reservaciones
{
    /// <summary>
    /// Lógica de interacción para BloqueoHabitacionReservacionForm.xaml
    /// </summary>
    public partial class BloqueoHabitacionReservacionForm : Window
    {
        bool cargada;
        public BloqueoHabitacionReservacionForm(Dtos.DtoDatosHabitacionBasicos datos)
        {
            InitializeComponent();
            DataContext = datos;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!cargada)
                return;

            var datos = DataContext as Dtos.DtoDatosHabitacionBasicos;

            if (datos == null)
                return;

            var controladorHabitaciones = new ControladorHabitaciones();

            controladorHabitaciones.MarcarComoReservada(datos.IdHabitacion);

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) && DataContext != null)
            {
                var controladorReservaciones = new ControladorReservaciones();

                int contador = controladorReservaciones.ObtenerCantidadReservacionesDia(((Dtos.DtoDatosHabitacionBasicos)DataContext).IdTipoHabitacion);

                txtContador.Text = contador.ToString();

                cargada = true;
            }
        }
    }
}
