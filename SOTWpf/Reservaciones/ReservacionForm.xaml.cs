﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTControladores.Utilidades;
using SOTWpf.Pagos;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Reservaciones
{
    /// <summary>
    /// Lógica de interacción para ReservacionForm.xaml
    /// </summary>
    public partial class ReservacionForm : Window
    {
        //int cantidadGlobalTMP = 0;


        int idReservacion;
        public bool ProcesoExitoso { get; private set; }
        bool esCargada;
        decimal IVA;
        List<CheckableItemWrapper<Paquete>> paquetes = new List<CheckableItemWrapper<Paquete>>();

        private Modelo.Almacen.Entidades.ZctCatPar configuracionGlobal;
        private Reservacion ReservacionActual 
        {
            get { return DataContext as Reservacion; }
            set { DataContext = value; }
        }

        private DatosFiscales DatosF 
        {
            get 
            { 
                return tbDatosF.IsChecked.Value ? edf.DataContext as DatosFiscales : null; 
            }
            set 
            { 
                edf.DataContext = value;

                //if (edf.DataContext as DatosFiscales != null)
                //    tbDatosF.IsChecked = true;
                //else
                //    tbDatosF.IsChecked = false;
            }
        }

        ConfiguracionTipo configuracionTipo;

        public ReservacionForm(int idReservacion = 0)
        {
            InitializeComponent();

            IVA = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar;

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            this.idReservacion = idReservacion;

            configuracionGlobal = new ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();
        }

        private void CargarTipos()
        {
            cbTipos.ItemsSource = new ControladorTiposHabitacion().ObtenerTiposActivosConConfiguracion(ConfiguracionTarifa.Tarifas.Hotel);
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!esCargada)
                throw new InvalidOperationException("La ventana no se cargó correctamente, ciérrela y ábrala de nuevo");

            try
            {
                var controladorReservaciones = new ControladorReservaciones();

                decimal precioPaquete, descuentoPaquete;

                if (ReservacionActual.Id == 0)
                {
                    var copia = ReservacionActual.ObtenerCopiaDePrimitivas();

                    foreach (var paquete in paquetes.Where(m => m.IsChecked))
                    {
                        precioPaquete = paquete.Value.Precio != 0 ? Math.Round(paquete.Value.Precio * (1 + IVA), 2) / (1 + IVA) : 0;
                        descuentoPaquete = paquete.Value.Descuento != 0 ? Math.Round(paquete.Value.Descuento * (1 + IVA), 2) / (1 + IVA) : 0;

                        copia.PaquetesReservaciones.Add(new PaqueteReservacion
                        {
                            Activo = true,
                            IdPaquete = paquete.Value.Id,
                            Precio = precioPaquete,
                            Descuento = descuentoPaquete,
                            Cantidad = CalcularDias(paquete.Value)
                        });
                    }

                    if (DatosF != null)
                        DatosF.Reservaciones.Clear();

                    //Propina propina = null;

                    List<DtoInformacionPago> pagos;

                    if (copia.ValorConIVA > 0)//descuentoV < valorSinIva)
                    {
                        //var vSinIVA = (valorSinIva - descuentoV);
                        //var vIVA = vSinIVA * config.Iva_CatPar;
                        var vConIVA = copia.ValorConIVA;//vSinIVA + vIVA;

                        var selectorFormaPago = new SelectorFormaPagoForm(vConIVA, true, false, false);

                        Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                        if (!selectorFormaPago.ProcesoExitoso)
                            return;

                        pagos = selectorFormaPago.FormasPagoSeleccionadas;

                        //ci = selectorFormaPago.DatosConsumo;
                        //propina = selectorFormaPago.PropinaGenerada;
                    }
                    else
                        pagos = new List<DtoInformacionPago>();

                    controladorReservaciones.CrearReservacion(copia, pagos, DatosF);
                }
                else
                {
                    var reservacionFinal = controladorReservaciones.ObtenerReservacionParaEditar(ReservacionActual.Id);

                    if (reservacionFinal == null)
                        throw new SOTException(Textos.Errores.reservacion_eliminada_otro_lado_exception);

                    var incrementoValor = ReservacionActual.ValorConIVA - reservacionFinal.PagosReservacion.Where(m => m.Activo).Sum(m => m.Valor);//reservacionFinal.ValorConIVA;

                    reservacionFinal.SincronizarPrimitivas(ReservacionActual);

                    var paquetesSeleccionables = paquetes.Where(m => m.IsChecked).Select(m => m.Value).ToList();

                    foreach (var paqueteReservacion in reservacionFinal.PaquetesReservaciones)
                    {
                        var paqueteExistente = paquetesSeleccionables.FirstOrDefault(m => m.Id == paqueteReservacion.IdPaquete);

                        if (paqueteExistente == null)
                            paqueteReservacion.Activo = false;
                        else
                        {
                            precioPaquete = paqueteExistente.Precio != 0 ? Math.Round(paqueteExistente.Precio * (1 + IVA), 2) / (1 + IVA) : 0;
                            descuentoPaquete = paqueteExistente.Descuento != 0 ? Math.Round(paqueteExistente.Descuento * (1 + IVA), 2) / (1 + IVA) : 0;

                            paqueteReservacion.Precio = precioPaquete;
                            paqueteReservacion.Descuento = descuentoPaquete;
                            paqueteReservacion.Cantidad = CalcularDias(paqueteExistente);
                        }
                    }

                    foreach (var paqueteSeleccionable in paquetesSeleccionables)
                    {
                        if (!reservacionFinal.PaquetesReservaciones.Any(m => m.IdPaquete == paqueteSeleccionable.Id))
                        {
                            precioPaquete = paqueteSeleccionable.Precio != 0 ? Math.Round(paqueteSeleccionable.Precio * (1 + IVA), 2) / (1 + IVA) : 0;
                            descuentoPaquete = paqueteSeleccionable.Descuento != 0 ? Math.Round(paqueteSeleccionable.Descuento * (1 + IVA), 2) / (1 + IVA) : 0;

                            reservacionFinal.PaquetesReservaciones.Add(new PaqueteReservacion
                            {
                                Activo = true,
                                IdPaquete = paqueteSeleccionable.Id,
                                Precio = precioPaquete,
                                Descuento = descuentoPaquete,
                                Cantidad = CalcularDias(paqueteSeleccionable)
                            });
                        }
                    }

                    if (DatosF != null)
                        DatosF.Reservaciones.Clear();

                    List<DtoInformacionPago> pagos;

                    if (reservacionFinal.ValorConIVA > 0 && /*(*/incrementoValor > 0/*||
                        MessageBox.Show("¿Desea ingresar de nuevo las formas de pago?", Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)*/)//descuentoV < valorSinIva)
                    {
                        //var vSinIVA = (valorSinIva - descuentoV);
                        //var vIVA = vSinIVA * config.Iva_CatPar;
                        var vConIVA = incrementoValor;//reservacionFinal.ValorConIVA;//vSinIVA + vIVA;

                        var selectorFormaPago = new SelectorFormaPagoForm(vConIVA, true, false, false);

                        Utilidades.Dialogos.MostrarDialogos(this, selectorFormaPago);

                        if (!selectorFormaPago.ProcesoExitoso)
                            return;

                        pagos = selectorFormaPago.FormasPagoSeleccionadas;

                        //ci = selectorFormaPago.DatosConsumo;
                        //propina = selectorFormaPago.PropinaGenerada;
                    }
                    else
                        pagos = new List<DtoInformacionPago>();
                        /*pagos = reservacionFinal.PagosReservacion.Select(m => new DtoInformacionPago
                        {
                            NumeroTarjeta = string.IsNullOrEmpty(m.NumeroTarjeta) ? null : m.NumeroTarjeta,
                            Referencia = m.Referencia,
                            TipoPago = m.TipoPago,
                            TipoTarjeta = m.TipoTarjeta,
                            Valor = m.Valor
                        }).ToList();*/


                    controladorReservaciones.ModificarReservacion(reservacionFinal, pagos, DatosF);
                }

                ProcesoExitoso = true;
                this.Close();
            }
            catch
            {
                (edf.DataContext as DatosFiscales).Reservaciones.Clear();
                ProcesoExitoso = false;
                throw;
            }
        }

        private int CalcularDias(Paquete paquete)
        {
            if (!IsLoaded || !dpFechaInicial.SelectedDate.HasValue || !dpFechaFinal.SelectedDate.HasValue)
                return 0;

            var fechaInicial = dpFechaInicial.SelectedDate.Value;
            var fechaFinal = dpFechaFinal.SelectedDate.Value;

            int dias = 0;

            for (DateTime fechaActual = fechaInicial.Date; fechaActual < fechaFinal.Date; fechaActual = fechaActual.AddDays(1))
            {


                var diaSemana = (int)fechaActual.DayOfWeek;
                if (diaSemana == 0)
                    diaSemana = 7;

                var mes = fechaActual.Month;
                var dia = fechaActual.Day;


                if (paquete.Programado &&
                    (paquete.ProgramasPaquete.Any(m => m.Activo && !m.Mes.HasValue && m.Dia == diaSemana) ||
                     paquete.ProgramasPaquete.Any(m => m.Activo && m.Mes.HasValue && m.Dia == dia && m.Mes == m.Mes)))
                    dias++;
                else if (!paquete.Programado)
                    dias++;
            }

            if (dias > 0 && paquete.CobroUnico == true)
                return 1;

            return dias;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            ProcesoExitoso = false;
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Reservacion reservacion = null;

            if (idReservacion > 0)
            {
                var controlador = new ControladorReservaciones();

                reservacion = controlador.ObtenerReservacionParaEditar(idReservacion);
            }

            if (reservacion != null)
            {
                var fechaEntrada = reservacion.FechaEntrada;
                var fechaSalida = reservacion.FechaSalida;

                ReservacionActual = reservacion;
                ReservacionActual.FechaSalida = fechaSalida;
                ReservacionActual.FechaEntrada = fechaEntrada;

                if (reservacion.IdDatosFiscales.HasValue)//(reservacion.DatosFiscales != null && reservacion.DatosFiscales.Activo)
                {
                    var controladorDatosF = new ControladorFacturacion();
                    DatosF = controladorDatosF.ObtenerDatosPorId(reservacion.IdDatosFiscales.Value) ??
                        new DatosFiscales();
                    //datosFiscales = reservacion.DatosFiscales; 
                }
                else
                    DatosF = new DatosFiscales();

                CargarTipos();

                var tipos = cbTipos.ItemsSource as List<TipoHabitacion>;

                if (tipos != null)
                    cbTipos.SelectedItem = tipos.FirstOrDefault(m => m.ConfiguracionesTipos.Any(c => c.Activa && c.Id == reservacion.IdConfiguracionTipoHabitacion));

                if (cbTipos.SelectedItem != null) 
                {
                    foreach (var paqueteR in reservacion.PaquetesReservaciones.Where(m => m.Activo)) 
                    {
                        if (paquetes.Any(m => m.Value.Id == paqueteR.IdPaquete))
                            paquetes.First(m => m.Value.Id == paqueteR.IdPaquete).IsChecked = true;
                    }
                }
            }
            else
            {
                var fechaActual = DateTime.Now;

                ReservacionActual = new Reservacion { Activo = true };

                ReservacionActual.FechaSalida = fechaActual.Date.AddDays(1);
                ReservacionActual.FechaEntrada = fechaActual.Date;

                DatosF = new DatosFiscales();

                CargarTipos();
            }

            esCargada = true;

            RecargarCantidades();
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!esCargada)
                return;

            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI >= dpFechaFinal.SelectedDate)
                {
                    dpFechaFinal.SelectedDate = fechaI.Value.AddDays(1);


                    //if (e.RemovedItems.Count > 0)
                    //    fechaI = e.RemovedItems[0] as DateTime?;
                    //else
                    //    fechaI = null;

                    //dpFechaInicial.SelectedDate = fechaI;

                    //throw new SOTException(Textos.Errores.fecha_entrada_mayor_igual_salida_exception);
                }
                /*else
                {*/
                    if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue)
                    {
                        MarcarPaquetes(fechaI.Value, dpFechaFinal.SelectedDate.Value);
                    }
                    RecargarCantidades();
                //}
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!esCargada)
                return;

            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF <= dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;

                    throw new SOTException(Textos.Errores.fecha_salida_menor_igual_entrada_exception);
                }
                else
                {
                    if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue)
                    {
                        MarcarPaquetes(dpFechaInicial.SelectedDate.Value, fechaF.Value);
                    }
                    RecargarCantidades();
                }
            }
        }

        private void MarcarPaquetes(DateTime fechaInicial, DateTime fechaFinal)
        {
            if (!IsLoaded)
                return;

            foreach (var p in paquetes)
            {
                for (DateTime fechaActual = fechaInicial.Date; fechaActual < fechaFinal.Date; fechaActual = fechaActual.AddDays(1))
                {
                    var diaSemana = (int)fechaActual.DayOfWeek;
                    if (diaSemana == 0)
                        diaSemana = 7;

                    var mes = fechaActual.Month;
                    var dia = fechaActual.Day;
                    if (p.Value.Programado &&
                        (p.Value.ProgramasPaquete.Any(m => m.Activo && !m.Mes.HasValue && m.Dia == diaSemana) ||
                         p.Value.ProgramasPaquete.Any(m => m.Activo && m.Mes.HasValue && m.Dia == dia && m.Mes == m.Mes)))
                    {
                        p.IsChecked = true;
                        p.Value.SeleccionableTmp = false;
                        break;
                    }
                    else if (p.Value.Programado)
                    {
                        p.IsChecked = false;
                        p.Value.SeleccionableTmp = false;
                    }
                    else
                        p.Value.SeleccionableTmp = true;
                }
            }
        }

        private void lbPaquetes_Checked(object sender, RoutedEventArgs e)
        {
            RecargarCantidades();
        }

        private void cbTipos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsLoaded)
                return;

            var tipoSeleccionado = cbTipos.SelectedItem as TipoHabitacion;

            if (tipoSeleccionado != null)
            {
                var controladorPaquetes = new ControladorPaquetes();

                configuracionTipo = tipoSeleccionado.ConfiguracionesTipos.FirstOrDefault(m => m.ConfiguracionTarifa.Tarifa == ConfiguracionTarifa.Tarifas.Hotel);//controladorCT.ObtenerConfiguracionTipo(tipoSeleccionado.Id, tarifaElegida);

                paquetes = new List<CheckableItemWrapper<Paquete>>();

                foreach (var p in controladorPaquetes.ObtenerPaquetesActivosConProgramasPorTipoHabitacion(tipoSeleccionado.Id))
                    paquetes.Add(new CheckableItemWrapper<Paquete> { Value = p });

                MarcarPaquetes(ReservacionActual.FechaEntrada, ReservacionActual.FechaSalida);
                

                lbPaquetes.ItemsSource = paquetes;

                ReservacionActual.IdConfiguracionTipoHabitacion = configuracionTipo.Id;
            }
            else //if (clbPaquetes.DataSource != null)
            {
                lbPaquetes.ItemsSource = null;
                configuracionTipo = null;
                //cbTarifa.DataSource = Nothing

                ReservacionActual.IdConfiguracionTipoHabitacion = 0;
            }

            RecargarCantidades();
        }

        private void RecargarCantidades()
        {
            if (!esCargada)
                return;

            ReservacionActual.Noches = (int)(ReservacionActual.FechaSalida - ReservacionActual.FechaEntrada).TotalDays;

            int contadorHabitaciones = (ReservacionActual.Noches > 0 ? 1 : 0);
            int contadorExtras = ReservacionActual.Noches - contadorHabitaciones;

            lblHabitacionContador.Text = contadorHabitaciones.ToString();
            lblHospExtraContador.Text = contadorExtras.ToString();

            decimal precioHotel, precioPExtra, precioHExtra;

            decimal precioHotelConIVA, precioPExtraConIVA, precioHExtraConIVA;

            if (configuracionTipo != null)
            {
                precioHotel = configuracionTipo.Precio;
                precioPExtra = configuracionTipo.PrecioPersonaExtra;
                precioHExtra = configuracionTipo.Precio;//PrecioHospedajeExtra;
            }
            else
                precioHotel = precioPExtra = precioHExtra = 0;

            precioHotelConIVA = Math.Round(precioHotel * (1 + IVA), 2);
            precioPExtraConIVA = Math.Round(precioPExtra * (1 + IVA), 2);
            precioHExtraConIVA = Math.Round(precioHExtra * (1 + IVA), 2);

            precioHotel = precioHotelConIVA / (1 + IVA);
            precioPExtra = precioPExtraConIVA / (1 + IVA);
            precioHExtra = precioHExtraConIVA / (1 + IVA);

            decimal valorPaquetesConIVA = paquetes.Where(m => m.IsChecked).Sum(m => Math.Round(m.Value.Precio * (1 + IVA), 2) * CalcularDias(m.Value));
            decimal valorDescuentosConIVA = paquetes.Where(m => m.IsChecked).Sum(m => Math.Round(m.Value.Descuento * (1 + IVA), 2) * CalcularDias(m.Value));

            decimal totalHabitacionConIVA = contadorHabitaciones * precioHotelConIVA;
            decimal totalPExtraConIVA = ReservacionActual.PersonasExtra * precioPExtraConIVA * ReservacionActual.Noches;
            decimal totalHExtraConIVA = precioHExtraConIVA * contadorExtras;

            lblHabitacionPUnidad.Text = precioHotelConIVA.ToString("C");
            ReservacionActual.TotalHabitacion = contadorHabitaciones * precioHotel;
            lblHabitacionTotal.Text = totalHabitacionConIVA.ToString("C");

            lblPExtraContador.Text = (ReservacionActual.PersonasExtra * ReservacionActual.Noches).ToString();
            lblPExtraPUnidad.Text = precioPExtraConIVA.ToString("C");
            //Importante, ahora las personas extra se multiplican por la cantidad de dias
            ReservacionActual.TotalPersonasExtra = ReservacionActual.PersonasExtra * precioPExtra * ReservacionActual.Noches;
            lblPExtraTotal.Text = totalPExtraConIVA.ToString("C");

            lblHospExtraPUnidad.Text = (precioHExtraConIVA).ToString("C");
            ReservacionActual.TotalHospedajeExtra = precioHExtra * contadorExtras;
            lblHospExtraTotal.Text = totalHExtraConIVA.ToString("C");

            ReservacionActual.Paquetes = valorPaquetesConIVA / (1 + IVA);
            int contadorPaquete = paquetes.Where(m => m.IsChecked && m.Value.Precio > 0).Sum(m => CalcularDias(m.Value));
            lblPaquetesContador.Text = contadorPaquete.ToString();

            ReservacionActual.Descuento = valorDescuentosConIVA / (1 + IVA);
            int contadorDescuentos = paquetes.Where(m => m.IsChecked && m.Value.Descuento > 0).Sum(m=> CalcularDias(m.Value));
            lblDescuentosContador.Text = contadorDescuentos.ToString();

            if (contadorDescuentos > 1)
                ReservacionActual.LeyendaDescuento = "Descuento acumulado";
            else if (contadorDescuentos == 1)
                ReservacionActual.LeyendaDescuento = paquetes.First(m => m.IsChecked && m.Value.Descuento > 0).Value.LeyendaDescuento;
            else
                ReservacionActual.LeyendaDescuento = "";

            ReservacionActual.ValorConIVA = totalHabitacionConIVA + totalPExtraConIVA + totalHExtraConIVA + valorPaquetesConIVA - valorDescuentosConIVA;
            ReservacionActual.ValorSinIVA = ReservacionActual.ValorConIVA / (1 + IVA);
            ReservacionActual.ValorIVA = ReservacionActual.ValorConIVA - ReservacionActual.ValorSinIVA;

            if (ReservacionActual.Id == 0)
            {
                ReservacionActual.ValorPagosActualesTMP = 0;
                ReservacionActual.ValorNoReembolsableTMP = 0;
            }
            else
            {
                ReservacionActual.ValorPagosActualesTMP = ReservacionActual.PagosReservacion.Where(m => m.Activo).Sum(m => m.Valor);
                ReservacionActual.ValorNoReembolsableTMP = Math.Max(ReservacionActual.ValorPagosActualesTMP - ReservacionActual.ValorConIVA, 0.0M);
            }

            txtPagosGuardados.Text = "Valor de los pagos actualmente registrados: " + ReservacionActual.ValorPagosActualesTMP.ToString("C");
            txtMontoNoReembolsable.Text = "Valor no reembolsable: " + ReservacionActual.ValorNoReembolsableTMP.ToString("C");


            //reservacionActual.ValorSinIVA = (reservacionActual.TotalHabitacion + reservacionActual.TotalPersonasExtra + reservacionActual.TotalHospedajeExtra + 
            //                           reservacionActual.Paquetes - reservacionActual.Descuento);

            //reservacionActual.ValorIVA = reservacionActual.ValorSinIVA * IVA;
            //reservacionActual.ValorConIVA = reservacionActual.ValorSinIVA + reservacionActual.ValorIVA;

            txtPaquetesConIVA.Text = valorPaquetesConIVA.ToString("C");
            txtDescuentoIVA.Text = valorDescuentosConIVA.ToString("C");
        }

        private void lbPaquetes_Unchecked(object sender, RoutedEventArgs e)
        {
            RecargarCantidades();
        }

        private void IntegerBox_ValueChanged(object sender, RoutedEventArgs e)
        {
            RecargarCantidades();
        }
    }
}
