﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;
using SOTWpf.Reportes.Reservaciones;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Modelo.Entidades.Parciales;								 

namespace SOTWpf.Reservaciones
{
    /// <summary>
    /// Lógica de interacción para ReservacionesForm.xaml
    /// </summary>
    public partial class ReservacionesForm : Window
    {

        List<DtoDetalleReservaciones> DetalleReporteReservaciones = new List<DtoDetalleReservaciones>();																									
        int idTipoFijo, idHabitacion;
        public ReservacionesForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
			
			
            cb_BuquedaFechacreacion.IsChecked = false;
            dpFechaCreacion.IsEnabled = false;
            dpFechaCreacion.SelectedDate = fechaActual;
            dpFechaCreacion.Language = lang;
            dpFechaCreacionFin.IsEnabled = false;
            dpFechaCreacionFin.SelectedDate = fechaActual;
            dpFechaCreacionFin.Language = lang;
            ActivaBusqueda.IsChecked = false;
            txtBusqueda.IsEnabled = false;

            CargaEstados();													  	 
        }


        public ReservacionesForm(int idTipoHabitacion, int idHabitacion) : this()
        {
            idTipoFijo = idTipoHabitacion;
            this.idHabitacion = idHabitacion;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarReservaciones();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarReservaciones();
                }
            }
        }

        private void cbTipos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarReservaciones();
        }

        private void CargarReservaciones()
        {
            Cursor = Cursors.Wait;		
            if (this.IsLoaded)
            {

                if (ActivaBusqueda.IsChecked == true)
                {

                    var reservaciones = new ControladorReservaciones().ObtenerReservaciones(txtBusqueda.Text);
                    dgvReservaciones.ItemsSource = reservaciones;
                    CargaListaRpt(reservaciones);
                }
                else if (cb_BuquedaFechacreacion.IsChecked == true)
                {
                      var reservaciones = idTipoFijo == 0 ? new ControladorReservaciones().ObtenerReservacionesPorFechaCreacion(dpFechaCreacion.SelectedDate.Value.Date,
                                                                                                              dpFechaCreacionFin.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1),
                                                                                                              cbEstado.SelectedValue.ToString(), (int)cbTipos.SelectedValue
                                                                                                              ):
                                                            new ControladorReservaciones().ObtenerReservacionesFiltradas(dpFechaCreacion.SelectedDate.Value.Date,
                                                                                                                         dpFechaCreacionFin.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1),
                                                                                                                         cbEstado.SelectedValue.ToString(), idTipoFijo);
                    dgvReservaciones.ItemsSource = reservaciones;
                    CargaListaRpt(reservaciones);
                }
                else
                {
                    var reservaciones = idTipoFijo == 0 ?
                        new ControladorReservaciones()
                                        .ObtenerReservacionesFiltradas(dpFechaInicial.SelectedDate.Value.Date,
                                        dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1),
                                       cbEstado.SelectedValue.ToString(), (int)cbTipos.SelectedValue) :
                        new ControladorReservaciones()
                                        .ObtenerReservacionesFiltradas(dpFechaInicial.SelectedDate.Value.Date,
                                        dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1),
                                        cbEstado.SelectedValue.ToString(), idTipoFijo);

                    dgvReservaciones.ItemsSource = reservaciones;
                    CargaListaRpt(reservaciones);
                }
            }
			Cursor = Cursors.Arrow;	 
        }

        private void CargarTipos() 
        {
            var tipos = new ControladorTiposHabitacion().ObtenerTiposActivos();

            tipos.Insert(0, new Modelo.Entidades.TipoHabitacion { Descripcion = "Todos" });

            cbTipos.ItemsSource = tipos;
        }
				
		
		private void CargaEstados()
        {
            List<string> Estados = new List<string>();
            Estados = ((Modelo.Entidades.Reservacion.EstadosReservacion[])Enum.GetValues(typeof(Modelo.Entidades.Reservacion.EstadosReservacion))).Select(c => c.ToString()).ToList();
            Estados.Insert(0, "Todos");
            cbEstado.ItemsSource = Estados;

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    if (idTipoFijo == 0)
                    {
                        CargarTipos();
						
                        btnConfirmarReservacion.Visibility = System.Windows.Visibility.Collapsed;
                    }
                    else
                    {
                        btnCancelarReservacion.Visibility = System.Windows.Visibility.Collapsed;
                        btnCrearReservacion.Visibility = System.Windows.Visibility.Collapsed;
                        btnModificarReservacion.Visibility = System.Windows.Visibility.Collapsed;
                        btnImprimir.Visibility = System.Windows.Visibility.Collapsed;
                        PanelControl.Visibility = System.Windows.Visibility.Hidden;
                    }

                    CargarReservaciones();
                }
                catch 
                {
                    Close();
                    throw;
                }
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnCancelarReservacion_Click(object sender, RoutedEventArgs e)
        {
            var reservacionSeleccionada = dgvReservaciones.SelectedItem as Modelo.Dtos.DtoReservacion;

            if (reservacionSeleccionada == null)
                throw new SOTException(Textos.Errores.multiples_reservaciones_seleccionadas);

            if (reservacionSeleccionada.Estado != Modelo.Entidades.Reservacion.EstadosReservacion.Pendiente)
                return;

            if (MessageBox.Show(string.Format(Textos.Mensajes.confirmar_cancelacion_reservacion, reservacionSeleccionada.Nombre), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorReservaciones = new ControladorReservaciones();

                controladorReservaciones.CancelarReservacion(reservacionSeleccionada.Id);

                CargarReservaciones();
            }
        }

        private void btnModificarReservacion_Click(object sender, RoutedEventArgs e)
        {
            var reservacionSeleccionada = dgvReservaciones.SelectedItem as Modelo.Dtos.DtoReservacion;

            if (reservacionSeleccionada == null)
                throw new SOTException(Textos.Errores.multiples_reservaciones_seleccionadas);

            if (reservacionSeleccionada.Estado != Modelo.Entidades.Reservacion.EstadosReservacion.Pendiente)
                return;

            var reservacionF = new ReservacionForm(reservacionSeleccionada.Id);
            Utilidades.Dialogos.MostrarDialogos(this, reservacionF);

            if (reservacionF.ProcesoExitoso)
                MessageBox.Show("Reservación modificada con éxito", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

            CargarReservaciones();
        }
        

        private void btnCrearReservacion_Click(object sender, RoutedEventArgs e)
        {
            var reservacionF = new ReservacionForm();
            Utilidades.Dialogos.MostrarDialogos(this, reservacionF);

            if (reservacionF.ProcesoExitoso)
                MessageBox.Show("Reservación agregada con éxito", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

            CargarReservaciones();
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            /*  var controladorGlobal = new ControladorConfiguracionGlobal();
              var config = controladorGlobal.ObtenerConfiguracionGlobal();


              var documentoReporte = new ReservacionesPorFecha();
              documentoReporte.Load();

              documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
              { 
                  Direccion=config.Direccion,
                  FechaInicio = dpFechaInicial.SelectedDate.Value,
                  FechaFin = dpFechaFinal.SelectedDate.Value,
                  Hotel = config.Nombre,
                  Usuario = ControladorBase.UsuarioActual.NombreCompleto
              }});

              documentoReporte.Subreports["Reservaciones.rpt"].SetDataSource(dgvReservaciones.ItemsSource as List<Modelo.Dtos.DtoReservacion>);

              var visorR = new VisorReportes();

              visorR.crCrystalReportViewer.ReportSource = documentoReporte;
              visorR.UpdateLayout();

              Utilidades.Dialogos.MostrarDialogos(this, visorR);*/

            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();
            var documentoReporte = new SOTWpf.Reportes.Reservaciones.Reservaciones1();
            documentoReporte.Load();
            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
              {
                  Direccion=config.Direccion,
                  FechaInicio = DateTime.Now,
                  FechaFin = DateTime.Now,
                  Hotel = config.Nombre,
                  Usuario = ControladorBase.UsuarioActual.NombreCompleto
              }});
            documentoReporte.Subreports["DetalleReservaciones"].SetDataSource(DetalleReporteReservaciones);
            documentoReporte.Subreports["DetallesAgrupados"].SetDataSource(DetalleReporteReservaciones);


            if (cb_BuquedaFechacreacion.IsChecked == true)
            {
                documentoReporte.ReportDefinition.Sections["DetailSection3"].SectionFormat.EnableSuppress = false;
                documentoReporte.ReportDefinition.Sections["Section3"].SectionFormat.EnableSuppress = true;
            }
            else
            {
                documentoReporte.ReportDefinition.Sections["DetailSection3"].SectionFormat.EnableSuppress = true;
                documentoReporte.ReportDefinition.Sections["Section3"].SectionFormat.EnableSuppress = false;
            }

            var visorR = new VisorReportes();
            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);

        }

        private void dgvReservaciones_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            datosRpopup.DataContext = dgvReservaciones.SelectedItem;

            if (dgvReservaciones.SelectedItem != null)
                datosRpopup.Visibility = System.Windows.Visibility.Visible;
            else
                datosRpopup.Visibility = System.Windows.Visibility.Collapsed;
        }
		
		
		private void CbEstado_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarReservaciones();
        }
		
        private void TxtBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarReservaciones();

        }
		
        private void ActivaBusqueda_Checked(object sender, RoutedEventArgs e)
        {
                txtBusqueda.IsEnabled = true;
                dpFechaInicial.IsEnabled = false;
                dpFechaFinal.IsEnabled = false;
                cbTipos.IsEnabled = false;
                cbEstado.IsEnabled = false;
                dpFechaCreacion.IsEnabled = false;
            dpFechaCreacionFin.IsEnabled = false;
            cb_BuquedaFechacreacion.IsEnabled = false;
            //CargarReservaciones();
        }
		
        private void ActivaBusqueda_Unchecked(object sender, RoutedEventArgs e)
        {
            dpFechaCreacion.IsEnabled = false;
            dpFechaCreacionFin.IsEnabled = false;
            cb_BuquedaFechacreacion.IsEnabled = true;
            dpFechaInicial.IsEnabled = true;
            dpFechaFinal.IsEnabled = true;
            cbTipos.IsEnabled = true;
            cbEstado.IsEnabled = true;
            txtBusqueda.Text = "";
            txtBusqueda.IsEnabled = false;
            CargarReservaciones();

        }																			   
		
		
        private void DpFechaCreacion_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarReservaciones();
        }																									
		
		
        private void Cb_BuquedaFechacreacion_Checked(object sender, RoutedEventArgs e)
        {
            dpFechaCreacion.IsEnabled = true;
            dpFechaCreacionFin.IsEnabled = true;
            dpFechaFinal.IsEnabled = false;
            dpFechaInicial.IsEnabled = false;
            txtBusqueda.IsEnabled = false;
            ActivaBusqueda.IsEnabled = false;
            CargarReservaciones();

        }																					  
		
        private void Cb_BuquedaFechacreacion_Unchecked(object sender, RoutedEventArgs e)
        {
            dpFechaCreacion.IsEnabled = false;
            dpFechaCreacionFin.IsEnabled = false;
            dpFechaFinal.IsEnabled = true;
            dpFechaInicial.IsEnabled = true;
            txtBusqueda.IsEnabled = true;
            ActivaBusqueda.IsEnabled = true;
            CargarReservaciones();
        }																						
		
        private void DpFechaCreacionFin_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarReservaciones();
        }																									   
		
        private void btnConfirmarReservacion_Click(object sender, RoutedEventArgs e)
        {
            if (idHabitacion != 0)
            {
                var reservacionSeleccionada = dgvReservaciones.SelectedItem as Modelo.Dtos.DtoReservacion;

                if (reservacionSeleccionada == null)
                    throw new SOTException(Textos.Errores.multiples_reservaciones_seleccionadas);


                var controladorReservaciones = new ControladorReservaciones();

                controladorReservaciones.AsignarHabitacion(reservacionSeleccionada.Id, idHabitacion);

                Close();
            }
		}
        private void CargaListaRpt(List<Modelo.Dtos.DtoReservacion> listareservacion)
        {
            DetalleReporteReservaciones = new List<DtoDetalleReservaciones>();

            foreach(Modelo.Dtos.DtoReservacion item in listareservacion)
            {
                DtoDetalleReservaciones dtalle = new DtoDetalleReservaciones();
                dtalle.CodigoReserva = item.CodigoReserva;
                dtalle.Nombre = item.Nombre;
                dtalle.FechaEntrada = item.FechaEntrada;
                dtalle.FechaSalida = item.FechaSalida;
                dtalle.Noches = item.Noches;
                dtalle.Estado = item.Estado.ToString();
                dtalle.PersonasExtra = item.PersonasExtra;
                dtalle.FechaCreacion = item.FechaCreacion;
                dtalle.Observaciones = item.Observaciones;
                dtalle.NombreTipo = item.NombreTipo;
                dtalle.CantidadPaquetes = item.CantidadPaquetes;
                dtalle.CantidadDescuentos = item.CantidadDescuentos;
                dtalle.Id = item.Id;
                dtalle.LeyendaDescuento = item.LeyendaDescuento;
                dtalle.NombreEstado = item.NombreEstado;
                dtalle.ValorIVA = item.ValorIVA;
                dtalle.ValorConIVA = item.ValorConIVA;
                dtalle.ValorSinIVA = item.ValorSinIVA;
                dtalle.Paquetes = item.Paquetes;
                dtalle.TurnoDia = item.TurnoDia;
                dtalle.FormaDePago = item.FormaDePago;
                DetalleReporteReservaciones.Add(dtalle);
            }

        }																					 
   private void DgvReservaciones_Sorting(object sender, DataGridSortingEventArgs e)
        {
            string _switch = "";
            bool Orden = false;
            _switch = e.Column.Header.ToString();
            if (e.Column.SortDirection == null || e.Column.SortDirection.Value.ToString() == "Descending")
                Orden = false;
            else if (e.Column.SortDirection.Value.ToString() == "Ascending")
                Orden = true;

            switch (_switch)
            {
                case "Reservación":
                    if (Orden == true)
                    {
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.CodigoReserva).ToList();
                    }
                    else
                    {
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.CodigoReserva).ToList();
                    }
                    break;
                case "Huésped":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.Nombre).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.Nombre).ToList();
                    break;
                case "Entrada":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.FechaEntrada).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.FechaEntrada).ToList();
                    break;
                case "Salida":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.FechaSalida).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.FechaSalida).ToList();
                    break;
                case "Noches":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.Noches).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.Noches).ToList();
                    break;
                case "Estado":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.Estado).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.Estado).ToList();
                    break;
                case "Tipo":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.NombreTipo).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.NombreTipo).ToList();
                    break;
                case "Personas Extra":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.PersonasExtra).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.PersonasExtra).ToList();
                    break;
                case "Paquetes":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.Paquetes).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.Paquetes).ToList();
                    break;
                case "Descuentos":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.CantidadDescuentos).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.CantidadDescuentos).ToList();
                    break;
                case "Fecha de reservación":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.FechaCreacion).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.FechaCreacion).ToList();
                    break;
                case "Observaciones":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.Observaciones).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.Observaciones).ToList();
                    break;
                case "Forma de pago":
                    if (Orden == true)
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderByDescending(o => o.FormaDePago).ToList();
                    else
                        DetalleReporteReservaciones = DetalleReporteReservaciones.OrderBy(o => o.FormaDePago).ToList();
                    break;
                default:
                    break;
            }
        }														
	}																												

}
