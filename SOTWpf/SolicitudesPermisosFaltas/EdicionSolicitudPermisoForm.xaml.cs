﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.SolicitudesPermisosFaltas
{
    /// <summary>
    /// Lógica de interacción para EdicionSolicitudPermisoForm.xaml
    /// </summary>
    public partial class EdicionSolicitudPermisoForm : Window
    {
        int idSolicitudP;

        public EdicionSolicitudPermisoForm(int idSolicitudP = 0)
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaAplicacion.Language = lang;
            tpHora.Language = lang;

            this.idSolicitudP = idSolicitudP;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    cbEmpleados.ItemsSource = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreas();
                    
                    if (idSolicitudP != 0)
                    {
                        var controladorSolicitudes = new ControladorSolicitudesPermisosFaltas();
                        var solicitud = controladorSolicitudes.ObtenerSolicitudPermiso(idSolicitudP);

                        if (solicitud == null)
                            throw new SOTException(Textos.Errores.solicitud_permiso_nula_eliminada_exception);

                        DataContext = solicitud;


                        tpHora.SelectedTime = solicitud.FechaAplicacion;

                        cbEmpleados.IsEnabled = false;
                        chbEsAnticipada.IsEnabled = false;
                        chbEsSalida.IsEnabled = false;
                    }
                    else
                    {
                        var sol = new Modelo.Entidades.SolicitudPermiso
                        {
                            Activa = true,
                            FechaAplicacion = DateTime.Now.Date
                        };

                        tpHora.SelectedTime = sol.FechaAplicacion;

                        DataContext = sol;
                    }
                }
                catch
                {
                    Close();
                    throw;
                }
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var emp = cbEmpleados.SelectedItem as Modelo.Dtos.DtoEmpleadoAreaPuesto;
            if (emp != null)
                txtArea.Text = emp.Area;
            else
                txtArea.Text = "";
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (idSolicitudP != 0)
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                var solicitud = DataContext as Modelo.Entidades.SolicitudPermiso;

                var fechaA = new DateTime(solicitud.FechaAplicacion.Year, solicitud.FechaAplicacion.Month, solicitud.FechaAplicacion.Day,
                                          tpHora.SelectedTime.Value.Hour, tpHora.SelectedTime.Value.Minute, tpHora.SelectedTime.Value.Second);

                if (solicitud.SolicitudAnticipada)
                    controladorSol.ActualizarSolicitudPermisoAnticipado(solicitud.Id, fechaA, solicitud.Motivo);
                else
                    controladorSol.ActualizarSolicitudPermisoMismoDia(solicitud.Id, fechaA, solicitud.Motivo);

                MessageBox.Show(Textos.Mensajes.modificacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                var solicitud = DataContext as Modelo.Entidades.SolicitudPermiso;

                if (solicitud.SolicitudAnticipada)
                    controladorSol.SolicitarPermisoAnticipado(solicitud);
                else
                    controladorSol.SolicitarPermisoMismoDia(solicitud);

                MessageBox.Show(Textos.Mensajes.creacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
