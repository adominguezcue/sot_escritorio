﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.SolicitudesPermisosFaltas
{
    /// <summary>
    /// Lógica de interacción para EdicionSolicitudFaltaForm.xaml
    /// </summary>
    public partial class EdicionSolicitudFaltaForm : Window
    {
        int idSolicitudF;
        public EdicionSolicitudFaltaForm(int idSolicitudF = 0)
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaAplicacion.Language = lang;

            this.idSolicitudF = idSolicitudF;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    cbEmpleados.ItemsSource = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreas();
                    cbSuplente.ItemsSource = cbEmpleados.ItemsSource;

                    cbFormasPagos.ItemsSource = EnumExtensions.ComoLista<Modelo.Entidades.SolicitudFalta.FormasPago>();

                    if (idSolicitudF != 0)
                    {
                        var controladorSolicitudes = new ControladorSolicitudesPermisosFaltas();
                        var solicitud = controladorSolicitudes.ObtenerSolicitudFalta(idSolicitudF);

                        if (solicitud == null)
                            throw new SOTException(Textos.Errores.solicitud_faltas_nula_eliminada_exception);

                        DataContext = solicitud;

                        cbEmpleados.IsEnabled = false;
                    }
                    else
                    {
                        DataContext = new Modelo.Entidades.SolicitudFalta
                        {
                            Activa = true,
                            FechaAplicacion = DateTime.Now.Date
                        };
                    }
                }
                catch
                {
                    Close();
                    throw;
                }
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var emp = cbEmpleados.SelectedItem as Modelo.Dtos.DtoEmpleadoAreaPuesto;
            if (emp != null)
                txtArea.Text = emp.Area;
            else
                txtArea.Text = "";
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (idSolicitudF != 0)
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                var sol = DataContext as Modelo.Entidades.SolicitudFalta;
                
                if (sol.IdEmpleadoSuplente == 0)
                    sol.IdEmpleadoSuplente = null;

                controladorSol.ActualizarSolicitudFalta(sol.Id, sol.IdEmpleadoSuplente, sol.FechaAplicacion, sol.Motivo);

                MessageBox.Show(Textos.Mensajes.modificacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                controladorSol.SolicitarFalta(DataContext as Modelo.Entidades.SolicitudFalta);

                MessageBox.Show(Textos.Mensajes.creacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
