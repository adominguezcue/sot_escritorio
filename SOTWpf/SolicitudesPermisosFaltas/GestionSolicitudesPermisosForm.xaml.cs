﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.SolicitudesPermisosFaltas
{
    /// <summary>
    /// Lógica de interacción para GestionSolicitudesPermisosForm.xaml
    /// </summary>
    public partial class GestionSolicitudesPermisosForm : Window
    {
        public GestionSolicitudesPermisosForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        private void btnCrearSolicitud_Click(object sender, RoutedEventArgs e)
        {
            var edicionF = new EdicionSolicitudPermisoForm();
            Utilidades.Dialogos.MostrarDialogos(this, edicionF);

            CargarSolicitudesPermisos();
        }

        private void btnModificarSolicitud_Click(object sender, RoutedEventArgs e)
        {
            var solicitud = dgvSolicitudes.SelectedItem as Modelo.Dtos.DtoSolicitudPermiso;

            if (solicitud == null)
                throw new SOTException(Textos.Errores.solicitud_no_seleccionada_excepcion);

            var edicionF = new EdicionSolicitudPermisoForm(solicitud.Id);
            Utilidades.Dialogos.MostrarDialogos(this, edicionF);

            CargarSolicitudesPermisos();
        }

        private void btnEliminarSolicitud_Click(object sender, RoutedEventArgs e)
        {
            var solicitud = dgvSolicitudes.SelectedItem as Modelo.Dtos.DtoSolicitudPermiso;

            if (solicitud == null)
                throw new SOTException(Textos.Errores.solicitud_no_seleccionada_excepcion);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_solicitud, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorS = new ControladorSolicitudesPermisosFaltas();
                controladorS.EliminarSolicitudPermiso(solicitud.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarSolicitudesPermisos();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CargarSolicitudesPermisos();
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarSolicitudesPermisos();
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarSolicitudesPermisos();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarSolicitudesPermisos();
                }
            }
        }

        private void CargarSolicitudesPermisos()
        {
            DateTime? fechaI = dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate;
            DateTime? fechaF = dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.Date.AddDays(1).AddSeconds(-1) : dpFechaFinal.SelectedDate;

            var controladorSolicitudes = new ControladorSolicitudesPermisosFaltas();
            dgvSolicitudes.ItemsSource = controladorSolicitudes.ObtenerSolicitudesPermiso(txtNombre.Text, txtApellidoP.Text, txtApellidoM.Text, fechaI, fechaF);
        }
    }
}
