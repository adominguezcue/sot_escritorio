﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.SolicitudesPermisosFaltas
{
    /// <summary>
    /// Lógica de interacción para EdicionVacacionesPermisosForm.xaml
    /// </summary>
    public partial class EdicionSolicitudVacacionesForm : Window
    {
        int idSolicitudV;
        public EdicionSolicitudVacacionesForm(int idSolicitudV = 0)
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            this.idSolicitudV = idSolicitudV;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CalcularDias();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CalcularDias();
                }
            }
        }

        private void CalcularDias()
        {
            if (dpFechaInicial.SelectedDate.HasValue && dpFechaFinal.SelectedDate.HasValue)
                txtDias.Text = "Días: " + (int)((dpFechaFinal.SelectedDate.Value.AddDays(1) - dpFechaInicial.SelectedDate.Value.Date).TotalDays);
            else
                txtDias.Text = "Días: 0";
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    cbEmpleados.ItemsSource = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreas();

                    if (idSolicitudV != 0)
                    {
                        var controladorSolicitudes = new ControladorSolicitudesPermisosFaltas();
                        var solicitud = controladorSolicitudes.ObtenerSolicitudVacaciones(idSolicitudV);

                        if (solicitud == null)
                            throw new SOTException(Textos.Errores.solicitud_vacaciones_nula_eliminada_exception);

                        DataContext = solicitud;

                        cbEmpleados.IsEnabled = false;
                    }
                    else
                    {
                        var fechaActual = DateTime.Now.Date;

                        DataContext = new Modelo.Entidades.SolicitudVacaciones
                        {
                            Activa = true,
                            FechaInicio = fechaActual.AddDays(1),
                            FechaFin = fechaActual.AddDays(2).AddSeconds(-1)
                        };
                    }
                }
                catch
                {
                    Close();
                    throw;
                }
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var emp = cbEmpleados.SelectedItem as Modelo.Dtos.DtoEmpleadoAreaPuesto;
            if (emp != null)
                txtArea.Text = emp.Area;
            else
                txtArea.Text = "";
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (idSolicitudV != 0)
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                var sol = DataContext as Modelo.Entidades.SolicitudVacaciones;

                controladorSol.ActualizarSolicitudVacaciones(sol.Id, sol.FechaInicio, sol.FechaFin);

                MessageBox.Show(Textos.Mensajes.modificacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                controladorSol.SolicitarVacaciones(DataContext as Modelo.Entidades.SolicitudVacaciones);

                MessageBox.Show(Textos.Mensajes.creacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
