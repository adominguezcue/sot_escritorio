﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.SolicitudesPermisosFaltas
{
    /// <summary>
    /// Lógica de interacción para EdicionSolicitudCambioDescansoForm.xaml
    /// </summary>
    public partial class EdicionSolicitudCambioDescansoForm : Window
    {
        int idSolicitudCD;

        public EdicionSolicitudCambioDescansoForm(int idSolicitudP = 0)
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaAplicacion.Language = lang;

            this.idSolicitudCD = idSolicitudP;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    cbEmpleados.ItemsSource = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreas();
                    //cbSuplentes.ItemsSource = cbEmpleados.ItemsSource;

                    if (idSolicitudCD != 0)
                    {
                        var controladorSolicitudes = new ControladorSolicitudesPermisosFaltas();
                        var solicitud = controladorSolicitudes.ObtenerSolicitudCambioDescanso(idSolicitudCD);

                        if (solicitud == null)
                            throw new SOTException(Textos.Errores.solicitud_permiso_nula_eliminada_exception);

                        DataContext = solicitud;

                        cbEmpleados.IsEnabled = false;
                    }
                    else
                    {
                        DataContext = new Modelo.Entidades.SolicitudCambioDescanso
                        {
                            Activa = true,
                            FechaAplicacion = DateTime.Now.Date
                        };
                    }
                }
                catch
                {
                    Close();
                    throw;
                }
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var emp = cbEmpleados.SelectedItem as Modelo.Dtos.DtoEmpleadoAreaPuesto;
            if (emp != null)
                txtArea.Text = emp.Area;
            else
                txtArea.Text = "";
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (idSolicitudCD != 0)
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                var solicitud = DataContext as Modelo.Entidades.SolicitudCambioDescanso;

                int? idSuplente = solicitud.IdEmpleadoSuplente == 0 ? null : solicitud.IdEmpleadoSuplente;

                controladorSol.ActualizarSolicitudCambioDescanso(solicitud.Id, idSuplente, solicitud.FechaAplicacion, solicitud.EsDescanso);

                MessageBox.Show(Textos.Mensajes.modificacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                var controladorSol = new ControladorSolicitudesPermisosFaltas();
                var solicitud = DataContext as Modelo.Entidades.SolicitudCambioDescanso;

                controladorSol.SolicitarCambioDescanso(solicitud);

                MessageBox.Show(Textos.Mensajes.creacion_solicitud_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
