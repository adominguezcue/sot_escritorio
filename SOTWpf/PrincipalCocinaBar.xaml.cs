﻿using SOTWpf.Cocina.Manejadores;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf
{
    /// <summary>
    /// Lógica de interacción para PrincipalCocina.xaml
    /// </summary>
    public partial class PrincipalCocinaBar : Window, ISnackContainer
    {
        string departamento;

        public PrincipalCocinaBar(string departamento) 
        {
            InitializeComponent();
            this.departamento = departamento;
            this.Title = departamento;

            Title += " " + Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            gsm.DataContext = new ManejadorComandasCocina(departamento);
            //gsmPreparadas.DataContext = new ManejadorComandasCocina("Cocina");

            gsm.txtBusqueda.Focus();
        }

        public void MostrarError(Exception exception)
        {
            if (exception != null)
                snackBarPrincipal.MessageQueue.Enqueue(exception.Message);
        }
    }
}
