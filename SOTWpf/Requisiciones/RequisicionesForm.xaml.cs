﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Requisiciones
{
    /// <summary>
    /// Lógica de interacción para RequisicionesForm.xaml
    /// </summary>
    public partial class RequisicionesForm : Window
    {
        public RequisicionesForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            //dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            //dpFechaFinal.SelectedDate = fechaActual;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargarEstados();
                CargarRequisiciones();
            }
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (e.AddedItems.Count > 0)
            //{
            //    var fechaI = e.AddedItems[0] as DateTime?;

            //    if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
            //    {
            //        if (e.RemovedItems.Count > 0)
            //            fechaI = e.RemovedItems[0] as DateTime?;
            //        else
            //            fechaI = null;

            //        dpFechaInicial.SelectedDate = fechaI;
            //    }
            //    else
            //    {
            //        CargarRequisiciones();
            //    }
            //}

            CargarRequisiciones();
        }

        private void CargarEstados() 
        { 
            var controladorReq = new SOTControladores.Controladores.ControladorRequisiciones();
            cbEstados.ItemsSource = controladorReq.ObtenerElementos().Select(m => m.estatus).Distinct().ToList();
        }

        private void CargarRequisiciones()
        {
            var anio = dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Year : default(int?);
            var mes = dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Month : default(int?);

            var controladorReq = new SOTControladores.Controladores.ControladorRequisiciones();
            dgvRequisiciones.ItemsSource = controladorReq.ObtenerElementos(cbEstados.SelectedItem != null ? cbEstados.SelectedItem.ToString() : null, anio, mes);
        }

        private void btnCrearRequisicion_Click(object sender, RoutedEventArgs e)
        {
            var requisicionF = new EdicionRequisicionesForm();
            Utilidades.Dialogos.MostrarDialogos(this, requisicionF);

            CargarRequisiciones();
        }

        private void cbEstados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarRequisiciones();
        }

        //private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (e.AddedItems.Count > 0)
        //    {
        //        var fechaF = e.AddedItems[0] as DateTime?;

        //        if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
        //        {
        //            if (e.RemovedItems.Count > 0)
        //                fechaF = e.RemovedItems[0] as DateTime?;
        //            else
        //                fechaF = null;

        //            dpFechaFinal.SelectedDate = fechaF;
        //        }
        //        else
        //        {
        //            CargarRequisiciones();
        //        }
        //    }
        //}
    }
}
