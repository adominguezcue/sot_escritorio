﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Requisiciones
{
    /// <summary>
    /// Lógica de interacción para EdicionRequisicionesForm.xaml
    /// </summary>
    public partial class EdicionRequisicionesForm : Window
    {
        Modelo.Almacen.Entidades.ZctRequisition requisicion;

        public EdicionRequisicionesForm()
        {
            InitializeComponent();

            requisicion = new Modelo.Almacen.Entidades.ZctRequisition();

            DataContext = requisicion;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorRequisiciones = new SOTControladores.Controladores.ControladorRequisiciones();
            //controladorRequisiciones.CrearRequisicion(requisicion);

            MessageBox.Show(Textos.Mensajes.creacion_requisicion_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        private void btnAgregarArticulo_Click(object sender, RoutedEventArgs e)
        {
            var articulo = dgvArticulos.SelectedItem as Modelo.Almacen.Entidades.Articulo;

            if (articulo == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (requisicion.ZctRequisitionDetail.Any(m => m.cod_art == articulo.Cod_Art))
            {
                requisicion.ZctRequisitionDetail.First(m => m.cod_art == articulo.Cod_Art).amount += 1;
            }
            else
            {
                var nuevoArticulo = new Modelo.Almacen.Entidades.ZctRequisitionDetail();
                nuevoArticulo.cod_art = articulo.Cod_Art;
                nuevoArticulo.ArticuloTmp = articulo.Desc_Art;
                nuevoArticulo.amount = 1;
                nuevoArticulo.price = articulo.Prec_Art;

                requisicion.ZctRequisitionDetail.Add(nuevoArticulo);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorConfig = new SOTControladores.Controladores.ControladorConfiguracionGlobal();
                var config = controladorConfig.ObtenerConfiguracionDepartamentosMaestros();

                var controladorArticulos = new SOTControladores.Controladores.ControladorArticulos();

                (this.Resources["articulosCVS"] as CollectionViewSource).Source = controladorArticulos.ObtenerArticulosPorDepartamento(config.Lavanderia);
            }
        }

        private void txtBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(dgvArticulos.ItemsSource).Refresh();
        }

        private void articulosCVS_filter(object sender, FilterEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtBusqueda.Text))
            {
                var item = e.Item as Modelo.Almacen.Entidades.Articulo;

                e.Accepted = item != null && (item.Cod_Art.ToUpper().Contains(txtBusqueda.Text.ToUpper()) || item.Desc_Art.ToUpper().Contains(txtBusqueda.Text.ToUpper()));
            }
            else
                e.Accepted = true;
        }
    }
}
