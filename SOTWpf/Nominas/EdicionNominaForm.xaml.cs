﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Nominas
{
    /// <summary>
    /// Lógica de interacción para EdicionNominaForm.xaml
    /// </summary>
    public partial class EdicionNominaForm : Window
    {
        private Modelo.Entidades.Nomina NominaDC
        {
            get { return DataContext as Modelo.Entidades.Nomina; }
            set { DataContext = value; }
        }
        DateTime fechaActual;

        public EdicionNominaForm(Modelo.Entidades.Nomina nomina = null)
        {
            InitializeComponent();

            NominaDC = nomina;

            fechaActual = DateTime.Now;

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            grupo.Language = lang;

            chbQuincena.IsChecked = (NominaDC != null && NominaDC.FechaInicio.Day == 16);

            //if (fechaActual.Day <= 15)
            //{
            //    //Primer día del mes
            //    fechaInicio = fechaActual.Date.AddDays(-fechaActual.Day + 1);
            //    //Último segundo del día 15 del mes actual
            //    fechaFin = fechaActual.Date.AddDays(-fechaActual.Day + 16).AddSeconds(-1);
            //}
            //else 
            //{
            //    //Día 16 del mes
            //    fechaInicio = fechaActual.Date.AddDays(-fechaActual.Day + 16);
            //    //Último segundo del último día del mes actual
            //    fechaFin = fechaActual.Date.AddDays(-fechaActual.Day + DateTime.DaysInMonth(fechaActual.Year, fechaActual.Month) + 1).AddSeconds(-1);
            //}
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            var controladorN = new ControladorNominas();

            if (NominaDC.Id == 0)
            {
                controladorN.RegistrarNomina(NominaDC);

                MessageBox.Show(Textos.Mensajes.creacion_nomina_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else 
            {
                controladorN.ModificarNomina(NominaDC);

                MessageBox.Show(Textos.Mensajes.modificacion_nomina_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }

            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                try
                {
                    cbEmpleados.ItemsSource = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreas();
                    
                    //if (idSolicitudF != 0)
                    //{
                    //    var controladorSolicitudes = new ControladorSolicitudesPermisosFaltas();
                    //    var solicitud = controladorSolicitudes.ObtenerSolicitudFalta(idSolicitudF);

                    //    if (solicitud == null)
                    //        throw new SOTException(Textos.Errores.solicitud_faltas_nula_eliminada_exception);

                    //    NominaDCt = solicitud;

                    //    cbEmpleados.IsEnabled = false;
                    //}
                    //else
                    //{
                        
                    //}

                    GenerarNominaTemporal();
                }
                catch
                {
                    Close();
                    throw;
                }
            }
        }

        private void GenerarNominaTemporal()
        {
            if (!IsLoaded)
                return;

            var emp = cbEmpleados.SelectedItem as Modelo.Dtos.DtoEmpleadoAreaPuesto;

            Modelo.Entidades.Nomina nominaRecalculada;

            if (emp != null)
                nominaRecalculada = new ControladorNominas().GenerarNominaNoPersistida(emp.IdEmpleado, fechaActual.Year, fechaActual.Month, !chbQuincena.IsChecked.Value, chbVacaciones.IsChecked.Value);
            else
                nominaRecalculada = new ControladorNominas().GenerarNominaNoPersistida(0, fechaActual.Year, fechaActual.Month, !chbQuincena.IsChecked.Value, chbVacaciones.IsChecked.Value);

            if (NominaDC == null || NominaDC.Id == 0)
                NominaDC = nominaRecalculada;
            else
            {
                NominaDC.IdEmpleado = nominaRecalculada.IdEmpleado;
                NominaDC.Faltas = nominaRecalculada.Faltas;
                NominaDC.Permisos = nominaRecalculada.Permisos;
                NominaDC.Retardos = nominaRecalculada.Retardos;
                NominaDC.FechaInicio = nominaRecalculada.FechaInicio;
                NominaDC.FechaFin = nominaRecalculada.FechaFin;
                NominaDC.Salario = nominaRecalculada.Salario;
                NominaDC.Bonos = nominaRecalculada.Bonos;
                NominaDC.FechaInicio = nominaRecalculada.FechaInicio;
                NominaDC.FechaFin = nominaRecalculada.FechaFin;

                //NominaDC.SincronizarPrimitivas(nominaRecalculada);
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var emp = cbEmpleados.SelectedItem as Modelo.Dtos.DtoEmpleadoAreaPuesto;
            if (emp != null)
            {
                txtArea.Text = emp.Area;
            }
            else
                txtArea.Text = "";

            GenerarNominaTemporal();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            GenerarNominaTemporal();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            GenerarNominaTemporal();
        }
    }
}
