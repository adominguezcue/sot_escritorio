﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Nominas
{
    /// <summary>
    /// Lógica de interacción para GestionNomina.xaml
    /// </summary>
    public partial class GestionNominas : Window
    {
        public GestionNominas()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargarNominas();
            }
        }

        private void CargarNominas()
        {
            if (!IsLoaded)
                return;

            DateTime? fechaI = dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate;
            DateTime? fechaF = dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.Date.AddDays(1).AddSeconds(-1) : dpFechaFinal.SelectedDate;

            var controladorNominas = new ControladorNominas();
            dgvNominas.ItemsSource = controladorNominas.ObtenerNominasFiltradas(txtNombre.Text, txtApellidoP.Text, txtApellidoM.Text, fechaI, fechaF);
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarNominas();
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarNominas();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarNominas();
                }
            }
        }

        private void btnRegistrarNomina_Click(object sender, RoutedEventArgs e)
        {
            var edicionN = new EdicionNominaForm();
            Utilidades.Dialogos.MostrarDialogos(this, edicionN);

            CargarNominas();
        }

        private void btnModificarNomina_Click(object sender, RoutedEventArgs e)
        {
            var item = dgvNominas.SelectedItem as Modelo.Entidades.Nomina;

            if (item == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            var edicionN = new EdicionNominaForm(item);
            Utilidades.Dialogos.MostrarDialogos(this, edicionN);

            CargarNominas();
        }

        private void btnEliminarNomina_Click(object sender, RoutedEventArgs e)
        {
            var item = dgvNominas.SelectedItem as Modelo.Entidades.Nomina;

            if (item == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_nomina, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorN = new ControladorNominas();
                controladorN.EliminarNomina(item.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_nomina_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarNominas();
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnReporte_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new Reportes.Nominas.Nominas();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = dpFechaInicial.SelectedDate.Value,
                FechaFin = dpFechaFinal.SelectedDate.Value,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            var items = (dgvNominas.ItemsSource as List<Modelo.Entidades.Nomina>).Select(m => new 
            {
                m.NombreEmpleadoTmp,
                m.AreaTmp,
                m.Salario,
                m.Bonos,
                m.EnVacaciones,
                m.Permisos,
                m.Retardos,
                m.Faltas, 
                m.FechaInicio, m.FechaFin
            });

            documentoReporte.Subreports["Nominas"].SetDataSource(items);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
            
        }
    }
}
