﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoResumenValet
    {
        [DisplayName("VALET")]
        public string Valet { get; set; }
        [DisplayName("PROPINAS")]
        public decimal Propinas { get; set; }
        [DisplayName("FONDOS A PAGAR")]
        public decimal FondosPagar { get; set; }
        [DisplayName("COMISIÓN TARJETA")]
        public decimal Comisiones { get; set; }
        [DisplayName("TOTALES")]
        public decimal Totales { get; set; }
    }
}
