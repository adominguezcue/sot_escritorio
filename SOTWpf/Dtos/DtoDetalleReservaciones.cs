﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoDetalleReservaciones
    {


        public string CodigoReserva { get; set; }

        public string Nombre { get; set; }

        public DateTime FechaEntrada { get; set; }

        public DateTime FechaSalida { get; set; }

        public int Noches { get; set; }

        public string Estado { get; set; }

        public int PersonasExtra { get; set; }

        public DateTime FechaCreacion { get; set; }

        public string Observaciones { get; set; }

        public string NombreTipo { get; set; }

        public int CantidadPaquetes { get; set; }

        public int CantidadDescuentos { get; set; }

        public int Id { get; set; }

        public string LeyendaDescuento { get; set; }

        public string NombreEstado { get; set; }

        public decimal ValorSinIVA { get; set; }

        public decimal ValorIVA { get; set; }

        public decimal ValorConIVA { get; set; }

        public List<string> Paquetes { get; set; }


        public string TurnoDia { get; set; }

        public string FormaDePago { get; set; }
    }
}
