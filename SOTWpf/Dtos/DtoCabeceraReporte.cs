﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoCabeceraReporte
    {
        public string Hotel { get; set; }
        public string Direccion { get; set; }
        public string Usuario { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string RutaLogo { get; set; }
        public string RazonSocial { get; set; }
    }
}
