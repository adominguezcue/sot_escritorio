﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoResumenMesero
    {
        [DisplayName("MESERO")]
        public string Mesero { get; set; }
        [DisplayName("PROPINAS")]
        public decimal Propinas { get; set; }
        [DisplayName("PUNTOS A PAGAR")]
        public decimal PuntosPagar { get; set; }
        [DisplayName("COMISIÓN TARJETA")]
        public decimal Comisiones { get; set; }
        [DisplayName("TOTALES")]
        public decimal Totales { get; set; }
    }
}
