﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoDatosHabitacionBasicos
    {
        public string NumeroHabitacion { get; set; }
        public string NombreTipo { get; set; }

        public int IdTipoHabitacion { get; set; }

        public int IdHabitacion { get; set; }
    }
}
