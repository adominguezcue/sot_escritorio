﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoFiltroReporte
    {
        public string Filtro { get; set; }
        public string Valor { get; set; }
    }
}
