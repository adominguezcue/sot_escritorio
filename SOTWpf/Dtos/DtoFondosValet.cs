﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoFondosValet
    {
        [DisplayName("VALETS")]
        public string Valet { get; set; }
        [DisplayName("FONDOS A PAGAR")]
        public decimal FondosPagar { get; set; }
    }
}
