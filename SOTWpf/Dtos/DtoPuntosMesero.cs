﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoPuntosMesero
    {
        [DisplayName("MESERO")]
        public string Mesero { get; set; }
        [DisplayName("ALIMENTOS")]
        public decimal Alimentos { get; set; }
        [DisplayName("BEBIDAS")]
        public decimal Bebidas { get; set; }
        [DisplayName("SPA & SEX")]
        public decimal SexAndSpa { get; set; }
        [DisplayName("PUNTOS A PAGAR")]
        public decimal PuntosPagar { get; set; }
    }
}
