﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoComisionesValet
    {
        [DisplayName("VALET")]
        public string Valet { get; set; }
        [DisplayName("VISA")]
        public decimal Visa { get; set; }
        [DisplayName("MASTERCARD")]
        public decimal Mastercard { get; set; }
        [DisplayName("AMERICAN EXPRESS")]
        public decimal Amex { get; set; }
        [DisplayName("COMISIÓN TARJETA")]
        public decimal Comision { get; set; }
    }
}
