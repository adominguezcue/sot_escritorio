﻿using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace SOTWpf.Dtos
{
    public class DtoEstadisticasEstadoHabitacionExtendida
    {
        public int Cantidad{ get; set; }

        Habitacion.EstadosHabitacion _estado;

        public Habitacion.EstadosHabitacion Estado 
        {
            get { return _estado; }
            set 
            { 
                Fondo = SOTWpf.Habitaciones.SelectorColor.ObtenerColorPorEstado(value);
                _estado = value;
            }
        }

        public Color Fondo
        {
            get;
            private set;
        }

        public static explicit operator DtoEstadisticasEstadoHabitacionExtendida(DtoEstadisticasHabitacion item) 
        {
            if (item == null)
                return null;

            return new DtoEstadisticasEstadoHabitacionExtendida
            {
                Cantidad = item.Cantidad,
                Estado = item.EstadoHabitacion
            };
        }
    }
}
