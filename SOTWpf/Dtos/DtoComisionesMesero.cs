﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Dtos
{
    public class DtoComisionesMesero
    {
        [DisplayName("MESERO")]
        public string Mesero { get; set; }
        [DisplayName("VISA")]
        public decimal Visa { get; set; }
        [DisplayName("MASTERCARD")]
        public decimal Mastercard { get; set; }
        [DisplayName("AMERICAN EXPRESS")]
        public decimal Amex { get; set; }
        [DisplayName("COMISIÓN TARJETA")]
        public decimal Comision { get; set; }
    }
}
