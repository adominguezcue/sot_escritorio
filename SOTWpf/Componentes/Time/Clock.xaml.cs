﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SOTWpf.Componentes.Time
{
    /// <summary>
    /// Lógica de interacción para Clock.xaml
    /// </summary>
    public partial class Clock : UserControl
    {
        private DispatcherTimer _dayTimer;

        public Clock()
        {
            InitializeComponent();

            this.Language = XmlLanguage.GetLanguage(
                        CultureInfo.CurrentCulture.IetfLanguageTag);

        }

        void Clock_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                // set the datacontext to be today's date
                DateTime now = DateTime.Now;
                DataContext = now;//.Day.ToString();

                // then set up a timer to fire at the start of tomorrow, so that we can update
                // the datacontext
                _dayTimer = new DispatcherTimer();
                _dayTimer.Interval = TimeSpan.FromSeconds(1);
                _dayTimer.Tick += OnDayChange;

                _dayTimer.Start();
            }
        }

        private void OnDayChange(object sender, EventArgs e)
        {
            // date has changed, update the datacontext to reflect today's date
            DateTime now = DateTime.Now;
            DataContext = now;
        }

        private void Clock_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_dayTimer != null)
            {
                _dayTimer.Stop();
                _dayTimer.Tick -= OnDayChange;
            }
        }
    }
}
