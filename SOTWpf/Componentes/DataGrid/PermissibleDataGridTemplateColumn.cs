﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SOTWpf.Componentes.DataGrid
{
    public class PermissibleDataGridTemplateColumn : DataGridTemplateColumn
    {
        public static readonly DependencyProperty PermisosProperty =
             DependencyProperty.Register("Permisos", typeof(Modelo.Seguridad.Dtos.DtoPermisos),
             typeof(PermissibleDataGridTemplateColumn), new FrameworkPropertyMetadata(new Modelo.Seguridad.Dtos.DtoPermisos()));

        public Modelo.Seguridad.Dtos.DtoPermisos Permisos
        {
            get { return (Modelo.Seguridad.Dtos.DtoPermisos)GetValue(PermisosProperty); }
            set
            {
                SetValue(PermisosProperty, value);
            }
        }

        public PermissibleDataGridTemplateColumn()
            : base()
        {
            //if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            //{
                if (!new SOTControladores.Controladores.ControladorPermisos().VerificarPermisos(Permisos))
                    Visibility = System.Windows.Visibility.Collapsed;
            //}

            //Loaded += PermissibleDataGridTemplateColumn_Loaded;
        }

        //private void PermissibleDataGridTemplateColumn_Loaded(object sender, System.Windows.RoutedEventArgs e)
        //{
        //    if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
        //    {
        //        if (!new SOTControladores.Controladores.ControladorPermisos().VerificarPermisos(Permisos))
        //            Visibility = System.Windows.Visibility.Collapsed;
        //    }
        //}
    }
}
