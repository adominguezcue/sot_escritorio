﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SOTWpf.Componentes.RegexFields
{
    public class RegexBox : TextBox
    {
        private Regex _regularExpression;

        /// <summary>Initializes a new instance of the NumericUpDownControlControlLib.NumericUpDownControl class.</summary>
        public RegexBox()
            : base()
        {
            _regularExpression = new Regex(@"^.$");


            PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(value_PreviewTextInput);
            TextChanged += new System.Windows.Controls.TextChangedEventHandler(value_TextChanged);
            Text = "";
        }

        private void ResetText(TextBox tb)
        {
            tb.Text = "";

            tb.SelectAll();
        }

        private void value_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var tb = (TextBox)sender;
            var text = tb.Text.Insert(tb.CaretIndex, e.Text);

            e.Handled = !_regularExpression.IsMatch(text);
        }

        private void value_TextChanged(object sender, TextChangedEventArgs e)
        {
            var tb = (TextBox)sender;
            if (!_regularExpression.IsMatch(tb.Text))
                ResetText(tb);
        }      

        public string RegularExpression
        {
            get
            {
                return (string)GetValue(RegularExpressionProperty);
            }
            set
            {
                SetValue(RegularExpressionProperty, value);
            }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RegularExpressionProperty =
            DependencyProperty.Register("RegularExpression", typeof(string), typeof(RegexBox),
              new PropertyMetadata("^.$", new PropertyChangedCallback(OnSomeValuePropertyChanged)));


        private static void OnSomeValuePropertyChanged(
        DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            RegexBox nb = target as RegexBox;

            nb._regularExpression = new Regex(e.NewValue.ToString());

            if (!nb._regularExpression.IsMatch(nb.Text))
                nb.ResetText(nb);
        }
    }
}
