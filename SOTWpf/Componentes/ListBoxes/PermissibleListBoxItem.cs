﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace SOTWpf.Componentes.ListBoxes
{
    [DefaultEvent("Selected")]
    public class PermissibleListBoxItem : ListBoxItem
    {
        public static readonly DependencyProperty PermisosProperty =
             DependencyProperty.Register("Permisos", typeof(Modelo.Seguridad.Dtos.DtoPermisos),
             typeof(PermissibleListBoxItem), new FrameworkPropertyMetadata(new Modelo.Seguridad.Dtos.DtoPermisos()));

        public Modelo.Seguridad.Dtos.DtoPermisos Permisos
        {
            get { return (Modelo.Seguridad.Dtos.DtoPermisos)GetValue(PermisosProperty); }
            set
            {
                SetValue(PermisosProperty, value);
            }
        }

        public PermissibleListBoxItem()
            : base()
        {
            Loaded += PermissibleListBox_Loaded;
        }

        private void PermissibleListBox_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                if (!new SOTControladores.Controladores.ControladorPermisos().VerificarPermisos(Permisos))
                    Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
