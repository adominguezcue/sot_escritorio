﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace SOTWpf.Componentes.Menus
{
    [DefaultEvent("Click")]
    [Localizability(LocalizationCategory.Menu)]
    [StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = typeof(MenuItem))]
    [TemplatePart(Name = "PART_Popup", Type = typeof(Popup))]
    public class PermissibleMenuItem : MenuItem
    {
        public static readonly DependencyProperty PermisosProperty =
             DependencyProperty.Register("Permisos", typeof(Modelo.Seguridad.Dtos.DtoPermisos),
             typeof(PermissibleMenuItem));//, new FrameworkPropertyMetadata(new Modelo.Seguridad.Dtos.DtoPermisos()));

        public Modelo.Seguridad.Dtos.DtoPermisos Permisos
        {
            get { return (Modelo.Seguridad.Dtos.DtoPermisos)GetValue(PermisosProperty); }
            set
            {
                SetValue(PermisosProperty, value);
            }
        }

        public PermissibleMenuItem()
            : base()
        {
            Loaded += PermissibleMenuItem_Loaded;
#if DEBUG
            IsVisibleChanged += PermissibleMenuItem_IsVisibleChanged;
#endif
        }

#if DEBUG
        void PermissibleMenuItem_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
 
            //var visible = e.NewValue as bool?;

            //if (visible == true)
            //{
            //    foreach (MenuItem item in Items)
            //    {
            //        if (item.Visibility == Visibility.Visible)
            //        {
            //            visible = true;
            //        }
            //    }

            //    if (visible == false)
            //    {
            //        //menuItem.IsEnabled = false;
            //    }
            //}
        }
#endif
        protected virtual void PermissibleMenuItem_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                if (Permisos != null && !new SOTControladores.Controladores.ControladorPermisos().VerificarPermisos(Permisos))
                    Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
