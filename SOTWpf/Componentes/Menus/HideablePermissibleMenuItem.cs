﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using SOTWpf.Componentes.Buttons;

namespace SOTWpf.Componentes.Menus
{
    [DefaultEvent("Click")]
    [Localizability(LocalizationCategory.Menu)]
    [StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = typeof(MenuItem))]
    [TemplatePart(Name = "PART_Popup", Type = typeof(Popup))]
    public class HideablePermissibleMenuItem: PermissibleMenuItem
    {
        public static readonly DependencyProperty FlagsProperty =
             DependencyProperty.Register("Flags", typeof(HideableButton.FlagsList),
             typeof(HideablePermissibleMenuItem));//, new FrameworkPropertyMetadata(new FlagsList()));

        public HideableButton.FlagsList Flags
        {
            get { return (HideableButton.FlagsList)GetValue(FlagsProperty); }
            set
            {
                SetValue(FlagsProperty, value);
            }
        }

        public static readonly DependencyProperty FlagModeProperty =
             DependencyProperty.Register("FlagMode", typeof(HideableButton.FlagsMode),
             typeof(HideablePermissibleMenuItem), new FrameworkPropertyMetadata(HideableButton.FlagsMode.AND));

        public HideableButton.FlagsMode FlagMode
        {
            get { return (HideableButton.FlagsMode)GetValue(FlagModeProperty); }
            set
            {
                SetValue(FlagModeProperty, value);
            }
        }

        public HideablePermissibleMenuItem() : base() { }

        protected override void PermissibleMenuItem_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if ((FlagMode == HideableButton.FlagsMode.AND && Flags.Any(m => !m)) || (FlagMode == HideableButton.FlagsMode.OR && Flags.All(m => !m)))
                    Visibility = System.Windows.Visibility.Collapsed;
                else
                    base.PermissibleMenuItem_Loaded(sender, e);

            }
        }
    }
}
