﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static SOTWpf.Componentes.Buttons.HideableButton;

namespace SOTWpf.Componentes.Buttons
{
    public class HideableFloatingIconButton: FloatingIconButton
    {
        public static readonly DependencyProperty FlagsProperty =
             DependencyProperty.Register("Flags", typeof(FlagsList),
             typeof(HideableFloatingIconButton));//, new FrameworkPropertyMetadata(new FlagsList()));

        public FlagsList Flags
        {
            get { return (FlagsList)GetValue(FlagsProperty); }
            set
            {
                SetValue(FlagsProperty, value);
            }
        }

        public static readonly DependencyProperty FlagModeProperty =
             DependencyProperty.Register("FlagMode", typeof(FlagsMode),
             typeof(HideableFloatingIconButton), new FrameworkPropertyMetadata(FlagsMode.AND));

        public FlagsMode FlagMode
        {
            get { return (FlagsMode)GetValue(FlagModeProperty); }
            set
            {
                SetValue(FlagModeProperty, value);
            }
        }

        public HideableFloatingIconButton():base()
        {
            Loaded += HideableFloatingIconButton_Loaded;
        }

        private void HideableFloatingIconButton_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (Flags != null && Flags.Count > 0)
                {
                    if ((FlagMode == FlagsMode.AND && Flags.Any(m => !m)) || (FlagMode == FlagsMode.OR && Flags.All(m => !m)))
                        Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }
    }
}
