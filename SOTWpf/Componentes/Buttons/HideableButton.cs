﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SOTWpf.Componentes.Buttons
{
    [DefaultEvent("Click")]
    [Localizability(LocalizationCategory.Button)]
    public class HideableButton: Button
    {
        public static readonly DependencyProperty FlagsProperty =
             DependencyProperty.Register("Flags", typeof(FlagsList),
             typeof(HideableButton));//, new FrameworkPropertyMetadata(new FlagsList()));

        public FlagsList Flags
        {
            get { return (FlagsList)GetValue(FlagsProperty); }
            set
            {
                SetValue(FlagsProperty, value);
            }
        }

        public static readonly DependencyProperty FlagModeProperty =
             DependencyProperty.Register("FlagMode", typeof(FlagsMode),
             typeof(HideableButton), new FrameworkPropertyMetadata(FlagsMode.AND));

        public FlagsMode FlagMode
        {
            get { return (FlagsMode)GetValue(FlagModeProperty); }
            set
            {
                SetValue(FlagModeProperty, value);
            }
        }

        public HideableButton()
    : base()
        {
            Loaded += HideableButton_Loaded;
#if DEBUG
            IsVisibleChanged += HideableButton_IsVisibleChanged;
#endif
        }

#if DEBUG
        void HideableButton_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

            //var visible = e.NewValue as bool?;

            //if (visible == true)
            //{
            //    foreach (MenuItem item in Items)
            //    {
            //        if (item.Visibility == Visibility.Visible)
            //        {
            //            visible = true;
            //        }
            //    }

            //    if (visible == false)
            //    {
            //        //menuItem.IsEnabled = false;
            //    }
            //}
        }
#endif
        private void HideableButton_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (Flags != null && Flags.Count > 0)
                {
                    if ((FlagMode == FlagsMode.AND && Flags.Any(m => !m)) || (FlagMode == FlagsMode.OR && Flags.All(m => !m)))
                        Visibility = System.Windows.Visibility.Collapsed;
                }
            }
        }

        public class FlagsList: List<bool>
        {

        }
        public enum FlagsMode
        {
            AND,
            OR
        }
    }
}
