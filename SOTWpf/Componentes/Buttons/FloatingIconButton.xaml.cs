﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Componentes.Buttons
{
    /// <summary>
    /// Lógica de interacción para FloatingIconButton.xaml
    /// </summary>
    public partial class FloatingIconButton : UserControl
    {
        object b = new object();
        bool _animar = false;
        private System.Windows.Media.Animation.Storyboard anim;

        public bool Animar
        {
            get { return _animar; }
            set
            {
                _animar = value;

                if (_animar)
                {
                    (anim ?? (anim = (this.Resources["sbVencimientoActivado"] as System.Windows.Media.Animation.Storyboard)))
                    .Begin(this, true);

                    //RoutedEventArgs newEventArgs = new RoutedEventArgs(VencimientoActivadoEvent);
                    //RaiseEvent(newEventArgs);
                }
                else
                {
                    if (anim != null)
                        anim.Stop(this);

                    //RoutedEventArgs newEventArgs = new RoutedEventArgs(VencimientoDesactivadoEvent);
                    //RaiseEvent(newEventArgs);
                }
            }
        }

        public string Text 
        {
            get { return ButtonText.Text; }
            set { ButtonText.Text = value; }
        }

        public Size IconSize
        {
            get { return ButtonImage.RenderSize; }
            set 
            {
                ButtonImage.Width = value.Width;
                ButtonImage.Height = value.Height;

                MainGrid.RowDefinitions[0].Height = new GridLength(value.Height / 2);
                MainGrid.RowDefinitions[1].Height = new GridLength(value.Height / 2);

                MainGrid.ColumnDefinitions[1].Width = new GridLength(value.Width / 2);
                MainGrid.ColumnDefinitions[2].Width = new GridLength(value.Width / 2);
            }
        }

        public static readonly DependencyProperty ImageSourceProperty =
            DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(FloatingIconButton),
              new PropertyMetadata(cambio));

        private static void cambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            FloatingIconButton fiButton = target as FloatingIconButton;
            fiButton.ButtonImage.Source = e.NewValue as ImageSource;
        }

        public ImageSource ImageSource 
        {
            //get { return ButtonImage.Source; }
            //set { ButtonImage.Source = value; }

            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        
        }

        public new FontFamily FontFamily 
        {
            get { return ButtonText.FontFamily; }
            set { ButtonText.FontFamily = value; }
        }

        public new double FontSize
        {
            get { return ButtonText.FontSize; }
            set { ButtonText.FontSize = value; }
        }

        public FloatingIconButton()
        {
            InitializeComponent();
            ButtonText.DataContext = this;
        }

        private static readonly RoutedEvent ClickEvent =
            EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(FloatingIconButton));

        /// <summary>The ValueChanged event is called when the TextBoxValue of the control changes.</summary>
        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

        //public event RoutedEventHandler Click;

        private void btn_Click(object sender, RoutedEventArgs e)
        {


            btn.IsEnabled = false;
            try
            {
                Task.WaitAll(Task.Delay(500));

                this.Focus();

                if (ClickEvent != null)
                    RaiseEvent(new RoutedEventArgs(ClickEvent));
            }
            finally
            {
                
                btn.IsEnabled = true;
            }

            //RoutedEventHandler handler = Click;
            //if (handler != null && !e.Handled)
            //{
            //    lock (b)
            //    {
            //        handler(this, e);

            //        e.Handled = true;
            //    }
            //}
        }

        //private void ButtonImage_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        //{
        //    this.btn.Focus();

        //    RoutedEventHandler handler = Click;
        //    if (handler != null && !e.Handled)
        //    {
        //        lock (b)
        //        {
        //            handler(this, e);

        //            e.Handled = true;
        //        }
        //    }
        //}

        private void padre_Unloaded(object sender, RoutedEventArgs e)
        {
            anim = null;
        }

        //private void btn_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    this.Focus();

        //    if (e.ClickCount > 2)
        //    {
        //        e.Handled = true;
        //        return;
        //    }

        //    if (ClickEvent != null)
        //        RaiseEvent(new RoutedEventArgs(ClickEvent));
        //}
    }
}
