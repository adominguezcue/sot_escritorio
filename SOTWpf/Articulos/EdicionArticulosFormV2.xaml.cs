﻿using MaterialDesignThemes.Wpf;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Articulos
{
    /// <summary>
    /// Lógica de interacción para EdicionArticulosFormV2.xaml
    /// </summary>
    public partial class EdicionArticulosFormV2 : Window
    {
        

        class ImagenArticulo
        {
            public string RutaOriginal;
            public string RutaImg;
        }
        //List<ImagenArticulo> ListaImagenes = new List<ImagenArticulo>();
        private ObservableCollection<DtoExistencias> Lista_Existencias = new ObservableCollection<DtoExistencias>();
        private ObservableCollection<DtoArticuloPresentacion> listaPresentaciones = new ObservableCollection<DtoArticuloPresentacion>();

        bool esNuevo = false;

        bool _EsInsumo = false;

        Articulo ArticuloActual
        {
            get { return DataContext as Articulo; }
            set 
            {
                if (value != null)
                {
                    value.PrecioConIVA = value.Prec_Art * (1 + IVA);
                    if (!value.Cos_Art.HasValue)
                        value.Cos_Art = 0;

                    if (value.OmitirIVA)
                        value.CostoFinal = value.Cos_Art.Value;
                    else
                        value.CostoFinal = value.Cos_Art.Value * (1 + IVA);

                    if (!value.NegarEntrada.HasValue)
                        value.NegarEntrada = false;

                    if (!value.NegarSalida.HasValue)
                        value.NegarSalida = false;
                }
                DataContext = value; 
            }
        }

        decimal IVA;

        public EdicionArticulosFormV2(string codigoArticulo = null, bool EsInsumo=false  )
        {
            InitializeComponent();
            _EsInsumo = EsInsumo;
            IVA = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar;

            txtArticulo.Text = codigoArticulo;
            ArticuloActual = new Articulo();
            GridExistencias.ItemsSource = Lista_Existencias;

            //var cvsPresentaciones = FindResource("cvsPresentaciones") as CollectionViewSource;

            //cvsPresentaciones.Source = listaPresentaciones;

            icPresentaciones.ItemsSource = listaPresentaciones;
        }

        private void CargarArticulo()
        {
            try
            {
                var _ControladorArt = new ControladorArticulos();

                ArticuloActual = _ControladorArt.TraerOGenerarArticuloPorCodigoOUpc(txtArticulo.Text);

                esNuevo = String.IsNullOrEmpty(ArticuloActual.Cod_Art);

                listaPresentaciones.Clear();

                foreach (var presentacion in ArticuloActual.PresentacionesArticulos)
                    listaPresentaciones.Add(presentacion);

                //CollectionViewSource.GetDefaultView(icPresentaciones.ItemsSource).Refresh();

                txtArticulo.IsEnabled = esNuevo;

                var controladorArticulosCentros = new ControladorArticulosConceptosGasto();
                var relaciones = controladorArticulosCentros.ObtenerVinculaciones(esNuevo ? txtArticulo.Text : ArticuloActual.Cod_Art);

                var vinculacion = relaciones.FirstOrDefault() ?? new ArticuloConceptoGasto
                {
                    CodigoArticulo = esNuevo ? txtArticulo.Text : ArticuloActual.Cod_Art
                };

                cbConceptosGastos.DataContext = vinculacion;

                if (esNuevo)
                {
                    var tmp = txtArticulo.Text;
                    Limpiar();
                    ArticuloActual.Cod_Art = tmp;
                    txtArticulo.Text = tmp;
                    chbDesactivar.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    txtArticulo.Text = ArticuloActual.Cod_Art;
                    chbDesactivar.Visibility = System.Windows.Visibility.Visible;
                }

                Lista_Existencias.Clear();
                bool errorCargarExistencias = false;

                try
                {
                    foreach (var x in (from existencia in _ControladorArt.SP_ConsultarExistencias(ArticuloActual.Cod_Art)
                                       orderby existencia.Existencias descending, existencia.CostoPromedio
                                       select existencia).ToList())
                    {
                        x.NombrePresentacion = ArticuloActual.Desc_Art;
                        Lista_Existencias.Add(x);
                    }

                    foreach (var presentacion in ArticuloActual.PresentacionesArticulos)
                    {
                        foreach (var x in (from existencia in _ControladorArt.SP_ConsultarExistencias(presentacion.CodigoSalida)
                                           orderby existencia.Existencias descending, existencia.CostoPromedio
                                           select existencia).ToList())
                        {
                            x.NombrePresentacion = presentacion.DescripcionSalida;
                            Lista_Existencias.Add(x);
                        }
                    }

                    foreach (var item in Lista_Existencias)
                        if (item.Existencias != 0)
                            item.CostoPromedio = item.CostoPromedio / item.Existencias;
                        else
                        {
                            item.CostoPromedio = _ControladorArt.ObtenerCosto(item.Codigo);
                        }
                }
                catch
                {
                    errorCargarExistencias = true;
                }

                if (Lista_Existencias.Count > 0 || errorCargarExistencias)
                {
                    GridExistencias.ItemsSource = Lista_Existencias;
                    cbTiposArticulos.IsEnabled = false;

                    var existenciaMaxima = Lista_Existencias.Max(m => m.Existencias);
                    var existenciaMinima = Lista_Existencias.Min(m => m.Existencias);

                    var existenciasMax = Lista_Existencias.Where(m => m.Existencias == existenciaMaxima).ToList();
                    var existenciasMin = Lista_Existencias.Where(m => m.Existencias == existenciaMinima).ToList();

                }
                else
                {
                    cbTiposArticulos.IsEnabled = true;
                }

                txtUltimoCosto.Text = "Último costo: " + new ControladorInventarios().ObtenerUltimoCostoMovimiento(ArticuloActual.Cod_Art).ToString("C");
            }
            catch (Exception ex)
            {
                Utilidades.Dialogos.Error(ex.Message);

                Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorMarcas = new ControladorMarcas();
                cbMarcas.ItemsSource = controladorMarcas.ObtenerMarcas();

                var controladorPresentaciones = new ControladorPresentaciones();
                cbPresentaciones.ItemsSource = controladorPresentaciones.ObtenerPresentacionesFILTRO();

                var controladorDepartamentos = new ControladorDepartamentos().ObtenerDepartamentosParaFiltro();
                List<Departamento> departamentos = new List<Departamento>();
                foreach(Departamento depto in controladorDepartamentos)
                {
                    Departamento typedepto = new Departamento();
                    typedepto = depto;
                    if (_EsInsumo == true)
                    {
                        if (depto.Desc_Dpto != "Bar" && depto.Desc_Dpto != "Cocina")
                        {
                            departamentos.Add(typedepto);
                        }
                    }
                    else
                    {
                        if (depto.Desc_Dpto == "Bar" || depto.Desc_Dpto == "Cocina")
                        {
                            departamentos.Add(typedepto);
                        }
                    }
                }
                cbDepartamentos.ItemsSource = departamentos;

                var controladorTiposArticulos = new ControladorTiposArticulos().ObtenerTipos();
                List<TipoArticulo> TipoArticulos = new List<TipoArticulo>();
                foreach (TipoArticulo Tipo in controladorTiposArticulos)
                {
                    TipoArticulo typeart = new TipoArticulo();
                    typeart = Tipo;
                    if (_EsInsumo == true)
                    {
                        if (Tipo.Desc_TpArt != "ARTÍCULOS COMPUESTOS")
                        {
                            TipoArticulos.Add(typeart);
                        }
                    }
                    else
                    {
                        if (Tipo.Desc_TpArt == "ARTÍCULOS COMPUESTOS")
                        {
                            typeart.Desc_TpArt = "ARTICULOS VENTA";
                            TipoArticulos.Add(typeart);
                        }
                    }
                }
                cbTiposArticulos.ItemsSource = TipoArticulos;

                var controladorAlmacenes = new ControladorAlmacenes();
                var almacenes = controladorAlmacenes.ObtenerAlmacenes();
                almacenes.Insert(0, null);
                List<Almacen> TipoAlmacen = new List<Almacen>();
                foreach (Almacen alma in almacenes)
                {
                    if (alma != null)
                    {
                        if (_EsInsumo)
                        {
                            if (alma.Desc_CatAlm != "RECEPCIÓN" && alma.Desc_CatAlm != "COCINA" && alma.Desc_CatAlm != "BARRA")
                            {
                                TipoAlmacen.Add(alma);
                            }
                        }
                        else
                        {
                            if (alma.Desc_CatAlm == "RECEPCIÓN" || alma.Desc_CatAlm == "COCINA" || alma.Desc_CatAlm == "BARRA")
                            {
                                TipoAlmacen.Add(alma);
                            }
                        }
                    }
                }
                cbAlmacenes.ItemsSource = TipoAlmacen;

                var controladorConceptosGastos = new ControladorConceptosGastos();
                var conceptos = controladorConceptosGastos.ObtenerConceptosActivos();
                conceptos.ForEach(m => m.Concepto = string.Join(" - ", m.NombreCentroCostosTmp, m.Concepto));
                conceptos = conceptos.OrderBy(m => m.Concepto).ToList();

                conceptos.Insert(0, new ConceptoGasto());
                if (_EsInsumo == true)
                {
                    cbConceptosGastos.IsEnabled = true;
                    ckcBoxIvareque.IsEnabled = true;
                    ctbMinimo.IsEnabled = true;
                    ctbMaximo.IsEnabled = true;
                    cbConceptosGastos.ItemsSource = conceptos;
                }
                else
                {
                    cbConceptosGastos.IsEnabled = false;
                    ckcBoxIvareque.IsEnabled = false;
                    ctbMinimo.IsEnabled = false;
                    ctbMaximo.IsEnabled = false;
                }

                CargarArticulo();
                //txtArticulo_LostFocus(null, null);
            }
        }

        private void NumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (ArticuloActual != null)
                ArticuloActual.Prec_Art = ctbPrecioConIVA.Number / (1 + IVA);
        }

        private void ctbCostoConIVA_NumberChanged(object sender, EventArgs e)
        {
            CalcularCosto();
        }

        public void CalcularCosto() 
        {
            if (ArticuloActual != null)
                ArticuloActual.Cos_Art = ArticuloActual.OmitirIVA ? ctbCostoConIVA.Number : ctbCostoConIVA.Number / (1 + IVA);
        }

        private void txtArticulo_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtArticulo.Text == "" || txtArticulo.Text == "0")
            {
                ArticuloActual = new Articulo { Cod_Art = txtArticulo.Text };

                return;
            }
            //Bnueva.Enabled = Pedita;
            CargarArticulo();
        }

        private void Limpiar()
        {
     
            txtArticulo.IsEnabled = true;

            txtArticulo.Text = "";
            Lista_Existencias.Clear();
            cbDepartamentos.SelectedIndex = -1;
            cbMarcas.SelectedIndex = -1;
            cbPresentaciones.SelectedIndex = -1;
        }

        private void cbDepartamentos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = cbDepartamentos.SelectedItem as Modelo.Almacen.Entidades.Departamento;

            var controladorLineas = new ControladorLineas();
            cbLineas.ItemsSource = controladorLineas.ObtenerLineasParaFiltro(item != null ? item.Cod_Dpto : -1);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Limpiar();
            ArticuloActual = new Articulo();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var _ControladorArt = new ControladorArticulos();
            var controladorVinculaciones = new ControladorArticulosConceptosGasto();
            var vinculacionActual = cbConceptosGastos.DataContext as ArticuloConceptoGasto;

            if (esNuevo)
            {
                ArticuloActual.Cod_Art = txtArticulo.Text.Trim();
#warning Valida ComboBox Cuando es un Insumo debe Poner un Concepto de Gastos   
                if (ArticuloActual.Cod_TpArt.Equals("I") && vinculacionActual != null)
                {
                    if (vinculacionActual.Id == 0 && vinculacionActual.IdConceptoGasto == 0)
                    {
                        Utilidades.Dialogos.Aviso("El producto es un Insumo debe poner un Concepto de Gastos");
                        return;

                    }
                }
                _ControladorArt.AgregaArticulo(ArticuloActual, listaPresentaciones.Where(m => m.Activo).ToList());
            }
            else
                if (ArticuloActual.Cod_TpArt.Equals("I") && vinculacionActual != null)
                {
                    if (vinculacionActual.Id == 0 && vinculacionActual.IdConceptoGasto == 0)
                    {
                     Utilidades.Dialogos.Aviso("El producto es un Insumo debe poner un Concepto de Gastos");
                     return;
                    }
                }
            _ControladorArt.ModificarArticulo(ArticuloActual, listaPresentaciones.Where(m => m.Activo || !string.IsNullOrEmpty(m.CodigoSalida)).ToList());

            try
            {
                //var itemSeleccionado = cbConceptosGastos.SelectedItem as ConceptoGasto;

                if (vinculacionActual != null)
                {
                    if (vinculacionActual.Id != 0 && vinculacionActual.IdConceptoGasto == 0)
                        controladorVinculaciones.EliminarVinculacion(vinculacionActual.Id);
                    else if (vinculacionActual.Id != 0)
                        controladorVinculaciones.ModificarVinculacion(vinculacionActual);
                    else if (vinculacionActual.IdConceptoGasto != 0)
                    {
                        var copia = vinculacionActual.ObtenerCopiaDePrimitivas();
                        copia.CodigoArticulo = ArticuloActual.Cod_Art;
                        controladorVinculaciones.CrearVinculacion(copia);
                    }
                }
            }
            catch(Exception ex)
            {
                throw new SOTException("Se guardaron los cambios en el artículo, pero ocurrió un error al almacenar la vinculación con el centro de costos:\n\n{0}", ex.Message);
            }
            Utilidades.Dialogos.Aviso("Guardado");

            Close();

            //ListaImagenes.Clear();
            //ArticuloActual = new Articulo();

            //cbConceptosGastos.DataContext = new ArticuloConceptoGasto
            //{
            //};

            //Limpiar();
            //txtArticulo.Focus();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            lblCostoSinIVA.Visibility = System.Windows.Visibility.Collapsed;
            CalcularCosto();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            lblCostoSinIVA.Visibility = System.Windows.Visibility.Visible;
            CalcularCosto();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAgregarPreExt_Click(object sender, RoutedEventArgs e)
        {
            var articuloNuevo = new DtoArticuloPresentacion
            {
                Activo = true,
                IVA = IVA,
                CodigoEntrada = ArticuloActual.Cod_Art,
                //DescripcionPrincipal = ArticuloActual.Desc_Art,
                //ArticuloIngrediente = articulo
            };

            miniEditor.DataContext = articuloNuevo;

            dh.IsOpen = true;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (sender as Control).DataContext as DtoArticuloPresentacion;

            if (selectedItem == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (selectedItem.Id == 0)
            {
                if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) != MessageBoxResult.Yes)
                    return;
            }
            else
            {
                if (Utilidades.Dialogos.Pregunta("¿Está seguro de eliminar el elemento seleccionado?" + Environment.NewLine + "La presentación se convertirá en un artículo independiente y no podrá volver a ser vinculada de nuevo.") != MessageBoxResult.Yes)
                    return;
            }

            selectedItem.Activo = false;

            //CollectionViewSource.GetDefaultView(icPresentaciones.ItemsSource).Refresh();
        }

        private void dh_DialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            var articulo = eventArgs.Parameter as DtoArticuloPresentacion;

            if (articulo == null)
                return;


            if (!listaPresentaciones.Contains(articulo))
            {
                listaPresentaciones.Add(articulo);

                //CollectionViewSource.GetDefaultView(icPresentaciones.Items).Refresh();
            }
        }

        private void miniCancelar_Click(object sender, RoutedEventArgs e)
        {
            EjecutarComando(null);
        }

        private void miniAceptar_Click(object sender, RoutedEventArgs e)
        {
            EjecutarComando(miniEditor.DataContext);
        }

        private void EjecutarComando(object parametro)
        {
            if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(parametro))
                ((ICommand)DialogHost.CloseDialogCommand).Execute(parametro);
        }

        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as DtoArticuloPresentacion;
            e.Accepted = item != null && item.Activo;
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Control).DataContext as DtoArticuloPresentacion;

            miniEditor.DataContext = item;

            dh.IsOpen = true;
        }

        private void btnReactivar_Click(object sender, RoutedEventArgs e)
        {
            var selectedItem = (sender as Control).DataContext as DtoArticuloPresentacion;

            if (selectedItem == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            selectedItem.Activo = true;
        }
    }
}
