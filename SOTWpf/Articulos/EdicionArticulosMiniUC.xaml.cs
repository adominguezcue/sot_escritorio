﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Articulos
{
    /// <summary>
    /// Lógica de interacción para EdicionArticulosMiniUC.xaml
    /// </summary>
    public partial class EdicionArticulosMiniUC : UserControl
    {
        DtoArticuloPresentacion ArticuloActual
        {
            get { return DataContext as DtoArticuloPresentacion; }
            set
            {
                //if (value != null)
                //{
                //    value.PrecioConIVA = value.Prec_Art * (1 + IVA);
                //    if (!value.Cos_Art.HasValue)
                //        value.Cos_Art = 0;

                //    if (value.OmitirIVA)
                //        value.CostoFinal = value.Cos_Art.Value;
                //    else
                //        value.CostoFinal = value.Cos_Art.Value * (1 + IVA);
                //}
                DataContext = value;
            }
        }

        decimal IVA;
        /*
        public EdicionArticulosMiniUC(Articulo articulo = null)
        {
            InitializeComponent();
            IVA = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar;

            ArticuloActual = articulo ?? new Articulo();
        }

        public EdicionArticulosMiniUC(string descripcion)
        {
            InitializeComponent();
            IVA = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar;

            ArticuloActual = new Articulo { Desc_Art = descripcion };
        }*/

        public EdicionArticulosMiniUC()
        {
            InitializeComponent();
            IVA = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar;

            //ArticuloActual = new Articulo { Desc_Art = descripcion };
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorPresentaciones = new ControladorPresentaciones();
                cbPresentaciones.ItemsSource = controladorPresentaciones.ObtenerPresentacionesFILTRO();

                var resp = ArticuloActual;
                ArticuloActual = null;
                ArticuloActual = resp;
                if (ArticuloActual != null)
                    ArticuloActual.UnidadSalida = cbPresentaciones.Text;
            }
        }

        //private void CheckBox_Checked(object sender, RoutedEventArgs e)
        //{
        //    lblCostoSinIVA.Visibility = System.Windows.Visibility.Collapsed;
        //    CalcularCosto();
        //}

        //private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        //{
        //    lblCostoSinIVA.Visibility = System.Windows.Visibility.Visible;
        //    CalcularCosto();
        //}

        //private void ctbCostoConIVA_NumberChanged(object sender, EventArgs e)
        //{
        //    CalcularCosto();
        //}

        //private void NumericUpDown_ValueChanged(object sender, EventArgs e)
        //{
        //    if (ArticuloActual != null)
        //        ArticuloActual.Prec_Art = ctbPrecioConIVA.Number / (1 + IVA);
        //}

        //public void CalcularCosto()
        //{
        //    if (ArticuloActual != null)
        //        ArticuloActual.Cos_Art = ArticuloActual.OmitirIVA ? ctbCostoConIVA.Number : ctbCostoConIVA.Number / (1 + IVA);
        //}
    }
}
