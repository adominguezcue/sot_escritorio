﻿using Modelo.Almacen.Entidades.Dtos;
using SOTControladores.Controladores;
using SOTWpf.Dtos;
using SOTWpf.Recetas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Articulos
{
    /// <summary>
    /// Lógica de interacción para GestionArticulosForm.xaml
    /// </summary>
    public partial class GestionArticulosForm : Window
    {
        bool Es_Insumos = false;
        bool _EsgestionArticulos = true;

        public GestionArticulosForm(bool Esinsumos)
        {
            Es_Insumos = Esinsumos;
            InitializeComponent();
            txtBusqueda.Focus();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            Buscar();
        }

        private void Buscar()
        {
            var controlador = new SOTControladores.Controladores.ControladorArticulos();
            _EsgestionArticulos = true;
            dgvArticulos.ItemsSource = controlador.ObtenerResumenesArticulosPorFiltro(txtBusqueda.Text, false,false, Es_Insumos, _EsgestionArticulos);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                Buscar();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEliminarArticulo_Click(object sender, RoutedEventArgs e)
        {
            var seleccionado = dgvArticulos.SelectedItem as DtoResultadoBusquedaArticuloSimple;

            if (seleccionado == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            var controlador = new SOTControladores.Controladores.ControladorArticulos();

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                controlador.EliminaArticulo(seleccionado.Codigo);

                Buscar();

                Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_elemento_exitosa);
            }
        }

        private void btnModificarArticulo_Click(object sender, RoutedEventArgs e)
        {
            var seleccionado = dgvArticulos.SelectedItem as DtoResultadoBusquedaArticuloSimple;

            if (seleccionado == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            Hide();
            try
            {
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionArticulosFormV2(seleccionado.Codigo, Es_Insumos));
            }
            finally
            {
                Show();
            }

            Buscar();
        }

        private void btnAltaArticulo_Click(object sender, RoutedEventArgs e)
        {
            //var seleccionado = dgvArticulos.SelectedItem as DtoResultadoBusquedaArticuloSimple;

            //if (seleccionado == null)
            //    throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);
            Hide();
            try
            {
                Utilidades.Dialogos.MostrarDialogos(this, new EdicionArticulosFormV2(null,Es_Insumos)); ; ;
            }
            finally
            {
                Show();
            }

            Buscar();
        }

        private void btnVerRecetas_Click(object sender, RoutedEventArgs e)
        {
            var seleccionado = dgvArticulos.SelectedItem as DtoResultadoBusquedaArticuloSimple;

            if (seleccionado != null)
            {
                Hide();
                try
                {
                    Utilidades.Dialogos.MostrarDialogos(this, new EdicionRecetaForm(seleccionado.Codigo, seleccionado.Nombre));
                }
                finally
                {
                    Show();
                }
            }
        }

        private void btnEliminarRecetas_Click(object sender, RoutedEventArgs e)
        {
            var seleccionado = dgvArticulos.SelectedItem as DtoResultadoBusquedaArticuloSimple;

            if (seleccionado == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_receta) != MessageBoxResult.Yes)
                return;

            var controlador = new SOTControladores.Controladores.ControladorMixItems();
            controlador.EliminarReceta(seleccionado.Codigo);

            Buscar();

            Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_receta_exitosa);
        }

        private void dgvArticulos_Loaded(object sender, RoutedEventArgs e)
        {

            if (!new SOTControladores.Controladores.ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarRecetas = true }))
                dgtcControlesRecetas.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();

            var controlador = new SOTControladores.Controladores.ControladorArticulos();
            var articulos = controlador.ObtenerResumenesArticulosPorFiltro("", false);

            var documentoReporte = new SOTWpf.Reportes.Articulos.Articulos();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            {
                Direccion=config.Direccion,
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            documentoReporte.Subreports["Articulos"].SetDataSource(articulos.OrderBy(m => m.Codigo).Select(m => new
            {
                m.Codigo,
                m.Nombre,
                m.Precio,
                m.Tipo,
                m.OmitirIVA,
                m.PorcentajeIVA,
                m.PrecioFinal,
                m.CostoFinal,
                m.NombreUnidad
            }).ToList());

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }

        private void TxtBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            Buscar();
        }
    }
}
