﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Dtos;
using Transversal.Excepciones;
//using ZctSOTRDN;

namespace SOTWpf.Articulos
{
    /// <summary>
    /// Lógica de interacción para EdicionArticulosForm.xaml
    /// </summary>
    public partial class EdicionArticulosForm : Window
    {
        //public EventHandler ImprimeEtiqueta(String Cod_Art);
        //public EventHandler ImprimeEtiquetaArticulo(String Cod_Art);

        private bool[] bloqueador = new bool[] { false };

        bool esNuevo = false;

        class ImagenArticulo
        {
            public string RutaOriginal;
            public string RutaImg;
        }
        List<ImagenArticulo> ListaImagenes = new List<ImagenArticulo>();
        //string PathImg = "";
        private ObservableCollection<DtoExistencias> Lista_Existencias = new ObservableCollection<DtoExistencias>();
        //private string sSPName = "SP_ZctCatArt";
        //private string sTabla = "ZctCatArt";
        //private string sSpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;INT|@Cod_Mar;INT|@Cod_Linea;INT|@Cod_Dpto;INT|@Cod_TpArt;VARCHAR|@Cod_TpRef;INT|@desactivar;BIT";
        //private string sSqlBusqueda = "SELECT [Cod_Cte] as Código ,isnull([Nom_Cte],space(0)) + space(1) + isnull([ApPat_Cte],space(0)) + isnull([ApMat_Cte],space(0)) FROM [ZctSOT].[dbo].[ZctCatCte]";
        //bool Pedita;
        public Modelo.Almacen.Entidades.ZctCatPar ParametrosSistema = new Modelo.Almacen.Entidades.ZctCatPar();

        Articulo ArticuloActual
        {
            get { return DataContext as Articulo; }
            set 
            {
                if (value != null)
                {
                    value.PrecioConIVA = value.Prec_Art * (1 + IVA);
                    if (!value.Cos_Art.HasValue)
                        value.Cos_Art = 0;
                }
                DataContext = value; 
            }
        }

        decimal IVA;

        public EdicionArticulosForm()
        {
            InitializeComponent();
            IVA = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar;

            ArticuloActual = new Articulo();
            GridExistencias.ItemsSource = Lista_Existencias;
        }

        private void CargarArticulo()
        {
            var _ControladorArt = new ControladorArticulos();

            ArticuloActual = _ControladorArt.TraerOGenerarArticuloPorCodigoOUpc(txtArticulo.Text);

            esNuevo = String.IsNullOrEmpty(ArticuloActual.Cod_Art);

            txtArticulo.IsEnabled = esNuevo;
            //Porque ://(
            //ArticuloActual.Cod_TpArt = "ART"

            var controladorArticulosCentros = new ControladorArticulosConceptosGasto();
            var relaciones = controladorArticulosCentros.ObtenerVinculaciones(esNuevo ? txtArticulo.Text : ArticuloActual.Cod_Art);

            var vinculacion = relaciones.FirstOrDefault() ?? new ArticuloConceptoGasto 
            {
                CodigoArticulo = esNuevo ? txtArticulo.Text : ArticuloActual.Cod_Art
            };

            cbConceptosGastos.DataContext = vinculacion;

            if (esNuevo)
            {
                var tmp = txtArticulo.Text;
                Limpiar();
                ArticuloActual.Cod_Art = tmp;
                txtArticulo.Text = tmp;
                btnEliminar.IsEnabled = false;
            }
            else
            {
                txtArticulo.Text = ArticuloActual.Cod_Art;
                btnEliminar.IsEnabled = true;// _Permisos.elimina;
            }
            //Enlazar();
            Lista_Existencias.Clear();

            foreach (var x in (from existencia in _ControladorArt.SP_ConsultarExistencias(ArticuloActual.Cod_Art)
                               orderby existencia.Existencias descending, existencia.CostoPromedio
                               select existencia).ToList())
                Lista_Existencias.Add(x);

            txtUltimoCosto.Text = "Último costo: " + new ControladorInventarios().ObtenerUltimoCostoMovimiento(ArticuloActual.Cod_Art).ToString("C");

            if (Lista_Existencias.Count > 0)
            {
                GridExistencias.ItemsSource = Lista_Existencias;
                cbTiposArticulos.IsEnabled = false;

                var existenciaMaxima = Lista_Existencias.Max(m => m.Existencias);
                var existenciaMinima = Lista_Existencias.Min(m => m.Existencias);

                var existenciasMax = Lista_Existencias.Where(m => m.Existencias == existenciaMaxima).ToList();
                var existenciasMin = Lista_Existencias.Where(m => m.Existencias == existenciaMinima).ToList();

                //txtMaximo.Text = string.Format("Existencias máximas: {0} ({1})", existenciaMaxima, string.Join(", ", existenciasMax.Select(m => m.Almacen)));
                //txtMinimo.Text = string.Format("Existencias mínimas: {0} ({1})", existenciaMinima, string.Join(", ", existenciasMin.Select(m => m.Almacen)));
            }
            else
            {
                cbTiposArticulos.IsEnabled = true;
                //txtMaximo.Text = "Existencias máximas: 0";
                //txtMinimo.Text = "Existencias mínimas: 0";
            }
            //if (DGVimagenes.RowCount > 0)
            //{
            //    btnAceptar.Enabled = Pedita;
            //    Belimina.Enabled = Pedita;
            //}
            //else
            //{
            //    particulo.Image = My.Resources.no_foto;
            //    btnAceptar.Enabled = Pedita;
            //    Belimina.Enabled = Pedita;
            //}

            //BimprimeEtiqueta.Enabled = Not esNuevo
            //cmdPreview.Enabled = Not esNuevo
            //cmdEtArticulo.Enabled = Not esNuevo
            //cmdPrevArt.Enabled = Not esNuevo
            //If Txtrefill.Text = "" Then
            //Txtrefill.Text = 0
            //End If
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorMarcas = new ControladorMarcas();
                cbMarcas.ItemsSource = controladorMarcas.ObtenerMarcas();

                var controladorPresentaciones = new ControladorPresentaciones();
                cbPresentaciones.ItemsSource = controladorPresentaciones.ObtenerPresentacionesFILTRO();

                var controladorDepartamentos = new ControladorDepartamentos();
                cbDepartamentos.ItemsSource = controladorDepartamentos.ObtenerDepartamentosParaFiltro();

                var controladorTiposArticulos = new ControladorTiposArticulos();
                cbTiposArticulos.ItemsSource = controladorTiposArticulos.ObtenerTipos();

                var controladorAlmacenes = new ControladorAlmacenes();
                var almacenes = controladorAlmacenes.ObtenerAlmacenes();
                almacenes.Insert(0, null);

                cbAlmacenes.ItemsSource = almacenes;

                var controladorConceptosGastos = new ControladorConceptosGastos();
                var conceptos = controladorConceptosGastos.ObtenerConceptosActivos();
                conceptos.ForEach(m => m.Concepto = string.Join(" - ", m.NombreCentroCostosTmp, m.Concepto));
                conceptos = conceptos.OrderBy(m => m.Concepto).ToList();

                conceptos.Insert(0, new ConceptoGasto());

                cbConceptosGastos.ItemsSource = conceptos;
            }
        }

        //private void NumericUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    if (ArticuloActual != null)
        //        ArticuloActual.Prec_Art = (decimal)(e.NewValue ?? 0) / (1 + IVA);

        //    //txtbPrecioSinIVA.Text = ArticuloActual.Prec_Art.ToString("C");
        //}

        private void NumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (ArticuloActual != null)
                ArticuloActual.Prec_Art = ctbPrecioConIVA.Number / (1 + IVA);
        }

        private void txtArticulo_LostFocus(object sender, RoutedEventArgs e)
        {
            if (txtArticulo.Text == "" || txtArticulo.Text == "0")
            {
                //Bnueva.Enabled = False;
                //BimprimeEtiqueta.Enabled = False;
                //cmdPreview.Enabled = False;
                //cmdEtArticulo.Enabled = False;
                //cmdPrevArt.Enabled = False;

                ArticuloActual = new Articulo { Cod_Art = txtArticulo.Text };

                return;
            }
            //Bnueva.Enabled = Pedita;
            CargarArticulo();
        }

        private void Limpiar()
        {
     
            txtArticulo.IsEnabled = true;
            //cboTipoArt.DataBindings.Clear();
            //cbDepartamentos.DataBindings.Clear();
            //chkDeshabilita.DataBindings.Clear();
            //txtDescArt.DataBindings.Clear();
            //chkDeshabilita.Checked = false;

            //Bedita.Enabled = Pedita;
            //Belimina.Enabled = Pedita;

            txtArticulo.Text = "";
            //txtArticulo.ZctSOTLabelDesc1.Text = ""
            //txtDescArt.Text = "";
            //txtCosto.DataBindings.Clear();
            //txtCosto.Text = 0;
            //lblPrecioIVA.DataBindings.Clear();
            //txtPrecio.Text = 0;
            //TxtPasillo.Text = "";
            //TxtSeccion.Text = "";
            //TxtRepisa.Text = "";
            //ImagenesArticuloBindingSource.DataSource = "";
            //particulo.Image = My.Resources.no_foto;
            Lista_Existencias.Clear();
            //cbDepartamentos.DataBindings.Clear();
            cbDepartamentos.SelectedIndex = -1;
            //CboLinea.DataBindings.Clear();
            //CboLinea.ZctSOTCombo1.SelectedIndex = -1;
            //cbMarcas.DataBindings.Clear();
            cbMarcas.SelectedIndex = -1;
            //DGVimagenes.Rows.Clear();

            //txtPais.DataBindings.Clear();
            //txtPais.Text = "";

            //cboPresentacion.DataBindings.Clear();
            cbPresentaciones.SelectedIndex = -1;

            //txtCaracteristicas.DataBindings.Clear();
            //txtCaracteristicas.Text = "";

            //TxtPasillo.DataBindings.Clear();
            //TxtSeccion.DataBindings.Clear();
            //TxtRepisa.DataBindings.Clear();
            //TxtDescuento.Text = "";

            //BimprimeEtiqueta.Enabled = ! esNuevo;
            //cmdPreview.Enabled = ! esNuevo;
            //cmdEtArticulo.Enabled = ! esNuevo;
            //cmdPrevArt.Enabled = !esNuevo;
            //Txtrefill.Text = ""
        }

        private void cbDepartamentos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = cbDepartamentos.SelectedItem as Modelo.Almacen.Entidades.Departamento;

            var controladorLineas = new ControladorLineas();
            cbLineas.ItemsSource = controladorLineas.ObtenerLineasParaFiltro(item != null ? item.Cod_Dpto : -1);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Limpiar();
            ArticuloActual = new Articulo();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            //if (esNuevo && _Permisos.agrega = false) {
            //    throw new Exception("No tiene permiso para agregar artículos nuevos")
            //}

            //if (String.IsNullOrWhiteSpace(cboTipoArt.ValueItemStr)) {
            //    throw new Exception(ZctSOT.Recursos.Articulos.tipo_no_seleccionado_exception)
            //}

            //decimal descuento;

            //if (!Decimal.TryParse(TxtDescuento.Text, descuento) 
            //{
            //    throw new Exception(ZctSOT.Recursos.Articulos.descuento_invalido_exception);
            //}

            //if CDbl(TxtDescuento.Text) > 1 {
            //    ArticuloActual.descuento = CDbl(TxtDescuento.Text) / 100
            //}
            //ArticulodBindingSource.EndEdit()
            //ImagenesArticuloBindingSource.EndEdit()
            
            //valida_datos();

            var _ControladorArt = new ControladorArticulos();
            var controladorVinculaciones = new ControladorArticulosConceptosGasto();

            if (esNuevo)
                _ControladorArt.AgregaArticulo(ArticuloActual, null);
            else
                _ControladorArt.ModificarArticulo(ArticuloActual, null);

            try
            {
                //var itemSeleccionado = cbConceptosGastos.SelectedItem as ConceptoGasto;
                var vinculacionActual = cbConceptosGastos.DataContext as ArticuloConceptoGasto;

                if (vinculacionActual != null)
                {
                    if (vinculacionActual.Id != 0 && vinculacionActual.IdConceptoGasto == 0)
                        controladorVinculaciones.EliminarVinculacion(vinculacionActual.Id);
                    else if (vinculacionActual.Id != 0)
                        controladorVinculaciones.ModificarVinculacion(vinculacionActual);
                    else if(vinculacionActual.IdConceptoGasto != 0)
                        controladorVinculaciones.CrearVinculacion(vinculacionActual.ObtenerCopiaDePrimitivas());
                }
            }
            catch(Exception ex)
            {
                throw new SOTException("Se guardaron los cambios en el artículo, pero ocurrió un error al almacenar la vinculación con el centro de costos:\n\n{0}", ex.Message);
            }

            //_ControladorArt.Graba()

            //var cClas = new Datos.ClassGen
            //cClas.GrabaUsuario(ControladorBase.UsuarioActual.Id, "ZctCatArt", "A", txtArticulo.Text)
            //_ControladorArt.Graba()

            /*
            For Each I As ImagenArticulo In ListaImagenes
                IO.File.Copy(I.RutaOriginal, I.RutaImg, true)
                Application.DoEvents()
            Next*/
            MessageBox.Show("Guardado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            ListaImagenes.Clear();
            ArticuloActual = new Articulo();

            cbConceptosGastos.DataContext = new ArticuloConceptoGasto
            {
            };

            Limpiar();
            txtArticulo.Focus();
        }

        //private bool valida_datos()
        //{
        //    string ErrorMsj = "";
        //    if (string.IsNullOrWhiteSpace(ArticuloActual.Cod_TpArt))
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar un tipo de artículo." + "\r\n"));
        //        // Return False
        //    }

        //    if (string.IsNullOrWhiteSpace(ArticuloActual.Cod_Art))
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar un código de artículo." + "\r\n"));
        //        // Return False
        //    }

        //    if (string.IsNullOrEmpty(ArticuloActual.Desc_Art))
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar una descripción de artículo." + "\r\n"));
        //        // Return False
        //    }

        //    //if (!ArticuloActual.Cos_Art.HasValue)
        //    //    ArticuloActual.Cos_Art = 0;
        //    if (!ArticuloActual.Cos_Art.HasValue || ArticuloActual.Cos_Art == 0)
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar un costo." + "\r\n"));
        //        // Return False
        //    }

        //    // If (TxtUpc.Text = "") Then
        //    //     ErrorMsj += "Debe especificar el campo UPC." & vbCrLf
        //    //     'Return False
        //    // End If

        //    /*if (ArticuloActual.Prec_Art == 0)
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar un precio con IVA" + "\r\n"));
        //        // Return False
        //    }*/

        //    if (ArticuloActual.id_presentacion == 0)
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar una presentación." + "\r\n"));
        //        // Return False
        //    }

        //    // If (CboMarca.Text = "") Then
        //    //     ErrorMsj += "Debe de asignar una marca." & vbCrLf
        //    //     'Return False
        //    // End If
        //    if (!ArticuloActual.Cod_Dpto.HasValue)
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar una categoría." + "\r\n"));
        //        // Return False
        //    }

        //    if (!ArticuloActual.Cod_Linea.HasValue)
        //    {
        //        ErrorMsj = (ErrorMsj + ("Debe de asignar una sub categoría." + "\r\n"));
        //        // Return False
        //    }

        //    if ((ErrorMsj.Length > 0))
        //    {
        //        ErrorMsj = ("Debe corregir los siguientes errores antes de guardar el artículo:" + ("\r\n" + ErrorMsj));
        //        throw new Exception(ErrorMsj);
        //    }

        //    return true;
        //}

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ArticuloActual.Cod_Art))
                throw new Exception(Textos.Errores.elemento_no_seleccionado_excepcion);

            if(MessageBox.Show("¿Desea eliminar el artículo " + ArticuloActual.Cod_Art + "? esta acción no se podrá deshacer", "Atención", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            return;

            /*For Each Row As DataGridViewRow In DGVimagenes.Rows
                IO.File.Delete(Row.Cells("ImagenDataGridViewTextBoxColumn").Value)
                Application.DoEvents()
            Next
                */
            var _ControladorArt = new ControladorArticulos();

            _ControladorArt.EliminaArticulo(ArticuloActual.Cod_Art);
            ListaImagenes.Clear();
            ArticuloActual = new Articulo();
            Limpiar();
            txtArticulo.Focus();

            MessageBox.Show("Artículo eliminado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            lblPrecioSinIVA.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            lblPrecioSinIVA.Visibility = System.Windows.Visibility.Visible;
        }

        private void txtArticulo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F3)
            {
                var bf = new BusquedaArticulosForm();

                Utilidades.Dialogos.MostrarDialogos(this, bf);

                txtArticulo.Text = bf.ArticuloSeleccionado != null ? bf.ArticuloSeleccionado.Codigo : "";
                this.Focus();
            }
        }
    }
}
