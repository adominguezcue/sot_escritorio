﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;
using System;
using System.ComponentModel;		
using System.Collections.Generic;
using System.Collections.ObjectModel;	 
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Articulos
{
    /// <summary>
    /// Lógica de interacción para GestionArticulosVendidosForm.xaml
    /// </summary>
    public partial class GestionArticulosVendidosForm : Window
    {
		List<Modelo.Entidades.VW_ArticuloVendido> listaordenar = new List<Modelo.Entidades.VW_ArticuloVendido>();
        private bool _esListaOrdenada = false;																										 
        public GestionArticulosVendidosForm()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;	
			
			
        }

        private void cbTurno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarArticulosVendidos();
        }

        private void cbDepartamentos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = cbDepartamentos.SelectedItem as Modelo.Almacen.Entidades.Departamento;

            var lineas = new ControladorLineas().ObtenerLineasParaFiltro(item != null ? item.Cod_Dpto : -1).Where(m => m.Comandable).ToList();
            lineas.Insert(0, new Modelo.Almacen.Entidades.Linea { Desc_Linea = "Todos" });
            cbLineas.ItemsSource = lineas;
            cbLineas.SelectedIndex = 0;

            CargarArticulosVendidos();
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarArticulosVendidos();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarArticulosVendidos();
                }
            }
        }

        private void CargarArticulosVendidos()
        {
            if (this.IsLoaded)
            {
				_esListaOrdenada = false;						 
                int? ordenTurno = cbTurno.SelectedValue == null || (int)cbTurno.SelectedValue == 0 ? null : cbTurno.SelectedValue as int?;

                dgvArticulosVendidos.ItemsSource = new ControladorVentas().ObtenerArticulosVendidos(ordenTurno, cbDepartamentos.SelectedValue != null && cbDepartamentos.SelectedValue != "Todos" ? cbDepartamentos.SelectedValue.ToString() : null,
                                                                                                    cbLineas.SelectedValue != null && cbLineas.SelectedValue != "Todos" ? cbLineas.SelectedValue.ToString() : null,
                                                                                                    dpFechaInicial.SelectedDate.Value.Date,
                                                                                                    dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1), Convert.ToBoolean(chcbox_agrupar.IsChecked));
            }
        }

        private void btnReporteArticulosVendidos_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new SOTWpf.Reportes.Articulos.ArticulosVendidos();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = dpFechaInicial.SelectedDate.Value,
                FechaFin = dpFechaFinal.SelectedDate.Value,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});
			
			
            List<DtoFiltroReporte> filtros = new List<DtoFiltroReporte>();
            filtros.Add(new DtoFiltroReporte { Filtro = "Turno", Valor = cbTurno.Text ?? "" });
            filtros.Add(new DtoFiltroReporte { Filtro = "Categoría", Valor = cbDepartamentos.SelectedValue != null ? cbDepartamentos.SelectedValue.ToString() : "" });
            filtros.Add(new DtoFiltroReporte { Filtro = "Subcategoría", Valor = cbLineas.SelectedValue != null ? cbLineas.SelectedValue.ToString() : "" });
			filtros.Add(new DtoFiltroReporte { Filtro = "Agrupar", Valor = chcbox_agrupar.IsChecked == true ? "SI" : "NO" });

            var items = dgvArticulosVendidos.ItemsSource as List<Modelo.Entidades.VW_ArticuloVendido>;
			if (_esListaOrdenada)
                items = listaordenar;

            if (chcbox_agrupar.IsChecked == true)
            {
                documentoReporte.ReportDefinition.Sections["DetailSection4"].SectionFormat.EnableSuppress = false;
                documentoReporte.ReportDefinition.Sections["DetailSection2"].SectionFormat.EnableSuppress = true;
            }
            else
            {
                documentoReporte.ReportDefinition.Sections["DetailSection2"].SectionFormat.EnableSuppress = false;
                documentoReporte.ReportDefinition.Sections["DetailSection4"].SectionFormat.EnableSuppress = true;
            }
            documentoReporte.Subreports["ArticulosAgrupados"].SetDataSource(items);					 
			documentoReporte.Subreports["Articulos"].SetDataSource(items);

            List<DtoDatosReportePp> datosReportePps = new List<DtoDatosReportePp>();
            datosReportePps.Add(ObtieneTotales(items));
            documentoReporte.Subreports["Totales"].SetDataSource(datosReportePps);			
            documentoReporte.Subreports["FiltrosExtra"].SetDataSource(filtros);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }

		
	    private DtoDatosReportePp ObtieneTotales(List<Modelo.Entidades.VW_ArticuloVendido> articulos)
        {
            DtoDatosReportePp respuesta = new DtoDatosReportePp();
            respuesta.Total = 0;
            respuesta.TotalConIva = 0;
            foreach (Modelo.Entidades.VW_ArticuloVendido ar in articulos)
            {
                respuesta.Total = respuesta.Total + Convert.ToDecimal(ar.Total);
                respuesta.TotalConIva = respuesta.TotalConIva + ar.PrecioUnidadConIVA;
            }
            return respuesta;
        }				
		
        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var configuraciones = new ControladorConfiguracionGlobal().ObtenerConfiguracionesTurno();
                configuraciones.Insert(0, new Modelo.Entidades.ConfiguracionTurno { Nombre = "Todos" });
                cbTurno.ItemsSource = configuraciones;

                var departamentos = new ControladorDepartamentos().ObtenerDepartamentosParaFiltro(true);
                departamentos.Insert(0, new Modelo.Almacen.Entidades.Departamento { Desc_Dpto = "Todos" });
                cbDepartamentos.ItemsSource = departamentos;

                CargarArticulosVendidos();
            }
		}
        private void cbOrderpor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void Chcbox_agrupar_Checked(object sender, RoutedEventArgs e)
        {
            CargarArticulosVendidos();
            dgvArticulosVendidos.Columns[0].Visibility = Visibility.Hidden;
            dgvArticulosVendidos.Columns[1].Visibility = Visibility.Hidden;
        }
        private void Chcbox_agrupar_Unchecked(object sender, RoutedEventArgs e)
        {
            CargarArticulosVendidos();
            dgvArticulosVendidos.Columns[0].Visibility = Visibility.Visible;
            dgvArticulosVendidos.Columns[1].Visibility = Visibility.Visible;
        }

        private void BtnReporteArticulosVendidos_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void DgvArticulosVendidos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void DgvArticulosVendidos_Sorting(object sender, DataGridSortingEventArgs e)
        {
            _esListaOrdenada = true;
            listaordenar = dgvArticulosVendidos.ItemsSource as List<Modelo.Entidades.VW_ArticuloVendido>;
            bool Orden = false;
            string _switch = "";
            _switch = e.Column.Header.ToString();

            if (e.Column.SortDirection == null || e.Column.SortDirection.Value.ToString() == "Descending")
                Orden = false;
            else if (e.Column.SortDirection.Value.ToString() == "Ascending")
                Orden = true;
            switch (_switch)
            {
                case "Cantidad":
                    if (Orden == true)
                    {
                        listaordenar = listaordenar.OrderByDescending(o => o.Cantidad).ToList();
                    }
                    else
                    {
                        listaordenar = listaordenar.OrderBy(o => o.Cantidad).ToList();
                    }
                    break;
                case "Turno":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.Turno).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.Turno).ToList();
                    break;
                case "Fecha":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.Fecha).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.Fecha).ToList();
                    break;
                case "Código":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.Codigo).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.Codigo).ToList();
                    break;
                case "Categoría":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.Categoria).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.Categoria).ToList();
                    break;
                case "Subcategoría":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.Subcategoria).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.Subcategoria).ToList();
                    break;
                case "Descripción":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.Descripcion).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.Descripcion).ToList();
                    break;
                case "Precio venta con IVA":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.PrecioUnidadConIVA).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.PrecioUnidadConIVA).ToList();
                    break;
                case "Total":
                    if (Orden == true)
                        listaordenar = listaordenar.OrderByDescending(o => o.Total).ToList();
                    else
                        listaordenar = listaordenar.OrderBy(o => o.Total).ToList();
                    break;
                default:
                    break;
            }
            return;

        }
    }
}
