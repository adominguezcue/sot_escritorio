﻿using Modelo.Almacen.Entidades.Dtos;
using SOTControladores.Buscadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Articulos
{
    /// <summary>
    /// Lógica de interacción para BusquedaArticulosForm.xaml
    /// </summary>
    public partial class BusquedaArticulosForm : Window, IBuscadorArticulos
    {
        SOTControladores.Controladores.ControladorArticulos controlador;
        bool _EsInsumo = false;
        public DtoResultadoBusquedaArticuloSimple ArticuloSeleccionado { get; private set; }

        public bool SoloInventariables { get; set; }

        public BusquedaArticulosForm()
        {
            InitializeComponent();
            txtBusqueda.Focus();
            controlador = new SOTControladores.Controladores.ControladorArticulos();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            Buscar();
        }

        private void Buscar()
        {
            dgvArticulos.ItemsSource = controlador.ObtenerResumenesArticulosPorFiltro(txtBusqueda.Text, soloInventariables: SoloInventariables, EsInsumo:true);
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            ArticuloSeleccionado = dgvArticulos.SelectedItem as DtoResultadoBusquedaArticuloSimple;

            Close();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                Buscar();
        }

        public void Mostrar()
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, this);
            else
                Utilidades.Dialogos.MostrarDialogos(this);
        }

        private void TxtBusqueda_TextChanged(object sender, TextChangedEventArgs e)
        {
            Buscar();
        }

        private void DgvArticulos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ArticuloSeleccionado = dgvArticulos.SelectedItem as DtoResultadoBusquedaArticuloSimple;

            Close();
        }	 
    }
}
