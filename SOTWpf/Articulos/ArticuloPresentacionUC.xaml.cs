﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Articulos
{
    /// <summary>
    /// Lógica de interacción para ArticuloPresentacionUC.xaml
    /// </summary>
    public partial class ArticuloPresentacionUC : UserControl
    {
        internal static readonly DependencyProperty NombreUnidadEntradaProperty =
        DependencyProperty.RegisterAttached("NombreUnidadEntrada", typeof(string), typeof(ArticuloPresentacionUC), new PropertyMetadata(null, new PropertyChangedCallback(NombreUnidadEntradaCambio)));

        private static void NombreUnidadEntradaCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as ArticuloPresentacionUC;

            /*pg.cpMain.ApplyTemplate();

            (pg.cpMain.ContentTemplate.FindName("txtDatosEntrada", pg.cpMain) as TextBlock)*/

            pg.txtDatosEntrada.Text = string.Format("{0} de {1}", e.NewValue, pg.DescripcionEntrada);
        }

        internal string NombreUnidadEntrada
        {
            get { return (string)GetValue(NombreUnidadEntradaProperty); }
            set { SetValue(NombreUnidadEntradaProperty, value); }
        }

        internal static readonly DependencyProperty DescripcionEntradaProperty =
        DependencyProperty.RegisterAttached("DescripcionEntrada", typeof(string), typeof(ArticuloPresentacionUC), new PropertyMetadata(null, new PropertyChangedCallback(DescripcionEntradaCambio)));

        private static void DescripcionEntradaCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as ArticuloPresentacionUC;

            /*pg.cpMain.ApplyTemplate();

            (pg.cpMain.ContentTemplate.FindName("txtDatosEntrada", pg.cpMain) as TextBlock)*/

            pg.txtDatosEntrada.Text = string.Format("{0} de {1}", pg.NombreUnidadEntrada, e.NewValue);
        }

        internal string DescripcionEntrada
        {
            get { return (string)GetValue(DescripcionEntradaProperty); }
            set { SetValue(DescripcionEntradaProperty, value); }
        }

        public ArticuloPresentacionUC()
        {
            InitializeComponent();
        }

        private void tglbDireccion_Checked(object sender, RoutedEventArgs e)
        {
            if (tglbDireccion.IsChecked == true)
            {
                Grid.SetColumn(ctbEntrada, 3);
                Grid.SetColumn(txtDatosEntrada, 4);

                Grid.SetColumn(ctbSalida, 0);
                Grid.SetColumn(txtDatosSalida, 1);
            }
            else
            {
                Grid.SetColumn(ctbEntrada, 0);
                Grid.SetColumn(txtDatosEntrada, 1);

                Grid.SetColumn(ctbSalida, 3);
                Grid.SetColumn(txtDatosSalida, 4);
            }
        }
    }
}
