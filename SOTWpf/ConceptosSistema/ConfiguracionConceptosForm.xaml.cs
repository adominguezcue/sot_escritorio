﻿using Modelo.Entidades;
using SOTControladores.Utilidades;
using SOTControladores.Controladores;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.ConceptosSistema
{
    /// <summary>
    /// Lógica de interacción para ConfiguracionConceptosForm.xaml
    /// </summary>
    public partial class ConfiguracionConceptosForm : Window
    {

        private bool esNuevo;

        ObservableCollection<CheckableItemWrapper<ConceptoSistema.TiposConceptos>> tiposConceptos;

        ItemWrapper<ConceptoSistema> csWrapper;

        public ConfiguracionConceptosForm()
        {
            InitializeComponent();

            tiposConceptos = new ObservableCollection<CheckableItemWrapper<ConceptoSistema.TiposConceptos>>();

            csWrapper = new ItemWrapper<ConceptoSistema>();

            DataContext = csWrapper;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var marcado = ((Control)sender).DataContext as CheckableItemWrapper<ConceptoSistema.TiposConceptos>;

            foreach (var item in tiposConceptos)
            {
                if (item == marcado)
                    continue;

                item.IsChecked = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
            CargarConceptos();
            CargarTipos();
        }

        private void CargarConceptos()
        {
            var controlador = new ControladorConceptosSistema();

            var conceptos = controlador.ObtenerConceptos();

            dgvConceptos.ItemsSource = conceptos;
        }

        private void CargarTipos()
        {

            tiposConceptos.Clear();

            var controladorConceptos = new ControladorConceptosSistema();

            foreach (var tipo in controladorConceptos.ObtenerTiposConcepto())
            {
                tiposConceptos.Add(new CheckableItemWrapper<ConceptoSistema.TiposConceptos> { Value = tipo });
            }

            listaMarcable.ItemsSource = tiposConceptos;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            esNuevo = true;

            csWrapper.Value = new ConceptoSistema();

            foreach (var item in tiposConceptos)
            {
                item.IsChecked = false;
            }

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if (dgvConceptos.SelectedItem == null)
                throw new SOTException(Textos.Errores.multiples_conceptos_seleccionados);


            var concepto = dgvConceptos.SelectedItem as Modelo.Entidades.ConceptoSistema;

            csWrapper.Value = concepto;

            var tipoActual = tiposConceptos.FirstOrDefault(m => m.Value == concepto.TipoConcepto);

            if (tipoActual != null)
                tipoActual.IsChecked = true;

            esNuevo = false;

            panelEdicion.Visibility = System.Windows.Visibility.Visible;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = false;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (dgvConceptos.SelectedItem == null)
                throw new SOTException(Textos.Errores.multiples_conceptos_seleccionados);

            var concepto = dgvConceptos.SelectedItem as Modelo.Entidades.ConceptoSistema;

            if (MessageBox.Show(string.Format(Textos.Mensajes.eliminar_concepto, concepto.Concepto, concepto.TipoConcepto.Descripcion()), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new ControladorConceptosSistema();

                controlador.EliminarConcepto(concepto.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                Window_Loaded(null, null);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            panelEdicion.Visibility = System.Windows.Visibility.Collapsed;
            esNuevo = false;

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;

            Window_Loaded(null, null);
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorConceptosSistema();

            var tipoSeleccionado = tiposConceptos.FirstOrDefault(m => m.IsChecked);

            if (tipoSeleccionado == null)
                throw new SOTException(Textos.Errores.tipo_concepto_no_seleccionado_exception);

            csWrapper.Value.TipoConcepto = tipoSeleccionado.Value;

            if (esNuevo)
                controlador.CrearConcepto(csWrapper.Value);

            else
                controlador.ModificarConcepto(csWrapper.Value);


            Window_Loaded(null, null);

            btnNuevo.IsEnabled = btnModificar.IsEnabled = btnEliminar.IsEnabled = true;

            if (esNuevo)
                MessageBox.Show(Textos.Mensajes.creacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show(Textos.Mensajes.modificacion_concepto_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
