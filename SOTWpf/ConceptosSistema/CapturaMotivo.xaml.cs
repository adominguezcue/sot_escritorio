﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.ConceptosSistema
{
    /// <summary>
    /// Lógica de interacción para CapturaMotivo.xaml
    /// </summary>
    public partial class CapturaMotivo : Window
    {
        public event AceptarEventHandler Aceptar;
        public delegate void AceptarEventHandler(object sender, EventArgs e);

        private string _motivo = "";

        private readonly string motivoOriginal;
        public string Motivo
        {
            get { return _motivo; }
            set
            {
                _motivo = value;
                if (!string.IsNullOrWhiteSpace(_motivo))
                {
                    grupoMotivo.Header = string.Format(Textos.TitulosComponentes.TituloMotivo, _motivo);
                }
                else
                {
                    grupoMotivo.Header = motivoOriginal;
                }
            }
        }

        public string DescripcionMotivo
        {
            get { return txtMotivo.Text; }
            set { txtMotivo.Text = value; }
        }

        public string TextoAuxiliar
        {
            get { return lblDetalleExtra.Text; }
            set { lblDetalleExtra.Text = value; }
        }

        public bool SoloLectura
        {
            get { return txtMotivo.IsReadOnly; }
            set { txtMotivo.IsReadOnly = value; }
        }

        public CapturaMotivo()
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            motivoOriginal = grupoMotivo.Header.ToString();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (Aceptar != null)
            {
                Aceptar(sender, e);
            }
            this.Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}