﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.ConceptosSistema
{
    /// <summary>
    /// Lógica de interacción para SelectorMotivoForm.xaml
    /// </summary>
    public partial class SelectorMotivoForm : Window
    {
        public event AceptarEventHandler Aceptar;
        public delegate void AceptarEventHandler(object sender, EventArgs e);

        private string _motivo = "";

        private readonly string motivoOriginal;
        public string Motivo
        {
            get { return _motivo; }
            set
            {
                _motivo = value;
                if (!string.IsNullOrWhiteSpace(_motivo))
                {
                    grupoMotivo.Header = string.Format(Textos.TitulosComponentes.TituloMotivo, _motivo);
                }
                else
                {
                    grupoMotivo.Header = motivoOriginal;
                }
            }
        }

        public int IdMotivo
        {
            get 
            {
                var concepto = cbMotivos.SelectedItem as ConceptoSistema;
                return concepto != null ? concepto.Id : 0;
            }
            //set { txtMotivo.Text = value; }
        }

        public string TextoAuxiliar
        {
            get { return lblDetalleExtra.Text; }
            set { lblDetalleExtra.Text = value; }
        }

        public bool SoloLectura
        {
            get { return cbMotivos.IsReadOnly; }
            set { cbMotivos.IsReadOnly = value; }
        }

        public string Observaciones
        {
            get { return txtObservaciones.Visibility == System.Windows.Visibility.Visible ? txtObservaciones.Text : null; }
            set
            {
                txtObservaciones.Text = value;
            }
        }

        public SelectorMotivoForm(ConceptoSistema.TiposConceptos filtro, bool capturarObservaciones)
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();

            var controlador = new SOTControladores.Controladores.ControladorConceptosSistema();
            cbMotivos.ItemsSource = controlador.ObtenerConceptosPorTipoFILTRO(filtro);

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            motivoOriginal = grupoMotivo.Header.ToString();

            txtObservaciones.Visibility = capturarObservaciones ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (Aceptar != null)
            {
                Aceptar(sender, e);
            }
            this.Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
