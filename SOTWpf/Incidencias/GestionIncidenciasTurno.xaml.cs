﻿using Modelo.Dtos;				  
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
#warning revisar combinaciones de EsRevisionCorte, EsRevisionCorte y MostrarCheckboxSoloCorteActual

    /// <summary>
    /// Lógica de interacción para GestionIncidenciasTurno.xaml
    /// </summary>
    public partial class GestionIncidenciasTurno : UserControl
    {
        public static readonly DependencyProperty EsRevisionCorteProperty =
        DependencyProperty.RegisterAttached("EsRevisionCorte", typeof(bool), typeof(GestionIncidenciasTurno), new PropertyMetadata(false, new PropertyChangedCallback(EsRevisionCorteCambio)));

        string area = "";						 
        internal bool EsRevisionCorte
        {
            get { return (bool)GetValue(EsRevisionCorteProperty); }
            set { SetValue(EsRevisionCorteProperty, value); }
        }
        private List<DtoIncidenciaTurno> _listadeIncidenciasTuno1;

        public List<Modelo.Dtos.DtoIncidenciaTurno> listadeIncidenciasTuno
        {
            get { return _listadeIncidenciasTuno1; }
            set{ _listadeIncidenciasTuno1 = value; }
        }

        private DateTime _fechainicio;
        public DateTime FechaInicio
        {
            get { return _fechainicio; }
            set { _fechainicio = value; }
        }
        private DateTime _fechafin;

        public DateTime FechaFin
        {
            get { return _fechafin; }
            set { _fechafin = value; }
        }																  	 

        private static void EsRevisionCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as GestionIncidenciasTurno;

            pg.CargarIncidencias();
        }

        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(GestionIncidenciasTurno), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));

        private static void IdCorteCambio(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            var pg = target as GestionIncidenciasTurno;

            pg.CargarIncidencias();
        }

        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        public static readonly DependencyProperty SoloCorteActualProperty =
            DependencyProperty.Register("SoloCorteActual", typeof(bool), typeof(GestionIncidenciasTurno),
              new PropertyMetadata(false, new PropertyChangedCallback(eventoSoloCorte)));

        private static void eventoSoloCorte(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            GestionIncidenciasTurno nb = target as GestionIncidenciasTurno;
            if ((bool)e.NewValue)
                nb.PanelControl.Visibility = Visibility.Collapsed;
            else
                nb.PanelControl.Visibility = Visibility.Visible;

            nb.CargarIncidencias();
        }

        public bool SoloCorteActual
        {
            get
            {
                return (bool)GetValue(SoloCorteActualProperty);
            }
            set
            {
                SetValue(SoloCorteActualProperty, value);
            }
        }

        public static readonly DependencyProperty MostrarCheckboxSoloCorteActualProperty =
            DependencyProperty.Register("MostrarCheckboxSoloCorteActual", typeof(bool), typeof(GestionIncidenciasTurno),
              new PropertyMetadata(false, new PropertyChangedCallback(EventoMostrarCheckboxSoloCorte)));

        private static void EventoMostrarCheckboxSoloCorte(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            GestionIncidenciasTurno nb = target as GestionIncidenciasTurno;
            if ((bool)e.NewValue)
                nb.chbSoloCorte.Visibility = Visibility.Visible;
            else
                nb.chbSoloCorte.Visibility = Visibility.Collapsed;
        }

        public bool MostrarCheckboxSoloCorteActual
        {
            get
            {
                return (bool)GetValue(MostrarCheckboxSoloCorteActualProperty);
            }
            set
            {
                SetValue(MostrarCheckboxSoloCorteActualProperty, value);
            }
        }

        public GestionIncidenciasTurno()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;

            chbSoloCorte.DataContext = this;
        }

        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarIncidencias();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarIncidencias();
                }
            }
        }


        internal void CargarIncidencias()
        {
            if (this.IsLoaded)
            {
                var controlador = new ControladorIncidenciasTurno();

                if (EsRevisionCorte)
                {
                    var controladorCortes = new ControladorCortesTurno();
                    var corteR = controladorCortes.ObtenerCorteEnRevision(IdCorte);

                    //if (corteR != null)
                    //{
                    //    dpFechaInicial.SelectedDate = null;// corteR.FechaInicio.Date;
                    //    dpFechaFinal.SelectedDate = null;// corteR.FechaCorte.Value.Date.AddDays(1).AddSeconds(-1);
                    //}


                    _listadeIncidenciasTuno1 = new List<DtoIncidenciaTurno>();																			  
                    dgvIncidencias.ItemsSource = controlador.ObtenerIncidenciasFiltradas(null, null, corteR?.Id ?? 0, area);
                    _listadeIncidenciasTuno1 = dgvIncidencias.ItemsSource as List<DtoIncidenciaTurno>;																									  
                }
                else if(SoloCorteActual)
                {
                    var controladorCortes = new ControladorCortesTurno();
                    var corteR = controladorCortes.ObtenerUltimoCorte();
                    _listadeIncidenciasTuno1 = new List<DtoIncidenciaTurno>();
                    dgvIncidencias.ItemsSource = controlador.ObtenerIncidenciasFiltradas(null, null, corteR?.Id ?? 0, area);
                    _listadeIncidenciasTuno1 = dgvIncidencias.ItemsSource as List<DtoIncidenciaTurno>;																									  
                }
                else 
                {
                    _listadeIncidenciasTuno1 = new List<DtoIncidenciaTurno>();																			  
                    dgvIncidencias.ItemsSource = controlador.ObtenerIncidenciasFiltradas(dpFechaInicial.SelectedDate.Value.Date, dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1),null, area);;
                    _fechainicio = dpFechaInicial.SelectedDate.Value.Date;
                    _fechafin = dpFechaFinal.SelectedDate.Value.Date;
                    _listadeIncidenciasTuno1 = dgvIncidencias.ItemsSource as List<DtoIncidenciaTurno>;																																		  
                }
                //var incidencias = SoloCorteActual ?
                //    controlador.ObtenerIncidenciasFiltradas(SoloCorteActual) :
                //    controlador.ObtenerIncidenciasFiltradas(SoloCorteActual, dpFechaInicial.SelectedDate.Value.Date,
                //                                                          dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1));

                //dgvIncidencias.ItemsSource = incidencias;
            }
        }

        public void RegistrarIncidencia()
        {
            var capturaIncidenciasF = new CapturaIncidenciasTurnoForm(idCorte: IdCorte == 0 ? null : (int?)IdCorte);

            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, capturaIncidenciasF);

            CargarIncidencias();
        }

        public void ModificarIncidencia()
        {
            var incidencia = dgvIncidencias.SelectedItem as Modelo.Dtos.DtoIncidenciaTurno;

            if (incidencia == null)
                throw new SOTException(Textos.Errores.incidencia_no_seleccionada_exception);

            if (incidencia.EstaCancelada)
                return;

            var capturaIncidenciasF = new CapturaIncidenciasTurnoForm(incidencia.Id, IdCorte == 0 ? null : (int?)IdCorte);

            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, capturaIncidenciasF);

            CargarIncidencias();
        }

        public void CancelarIncidencia()
        {
            var incidencia = dgvIncidencias.SelectedItem as Modelo.Dtos.DtoIncidenciaTurno;

            if (incidencia == null)
                throw new SOTException(Textos.Errores.incidencia_no_seleccionada_exception);

            if (incidencia.EstaCancelada)
                return;

            if (MessageBox.Show(Textos.Mensajes.confirmar_cancelacion_incidencia, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorI = new ControladorIncidenciasTurno();

                controladorI.CancelarIncidencia(incidencia.Id);

                MessageBox.Show(Textos.Mensajes.cancelacion_incidencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarIncidencias();
            }
        }

        public void EliminarIncidencia()
        {
            var incidencia = dgvIncidencias.SelectedItem as Modelo.Dtos.DtoIncidenciaTurno;

            if (incidencia == null)
                throw new SOTException(Textos.Errores.incidencia_no_seleccionada_exception);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_incidencia, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorI = new ControladorIncidenciasTurno();

                controladorI.EliminarIncidencia(incidencia.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_incidencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarIncidencias();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarIncidencias();

                //llenarcombobox
                var controladorCortes = new ControladorCortesTurno();
                var areas = controladorCortes.ObtieneAreas();
                areas.Insert(0, new Modelo.Dtos.DtoAreas { NombreArea = "Todos" });
                CmboxArea.ItemsSource = areas;										  
            }
        }

        private void CmboxArea_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (((Modelo.Dtos.DtoAreas)CmboxArea.SelectedItem) != null)
            {
                if (((Modelo.Dtos.DtoAreas)CmboxArea.SelectedItem).Id.ToString().Equals("0"))
                {
                    area = "";
                }
                else
                {
                    area = ((Modelo.Dtos.DtoAreas)CmboxArea.SelectedItem).NombreArea.ToString();
                }
            }
            CargarIncidencias();
        }


        public void GeneraReport (Window padre)
        {
            var listaaimprimir = listadeIncidenciasTuno;
            var fechaInicio = FechaInicio;
            var fechaFin = FechaFin;
            var EsCorteActual = SoloCorteActual;
            if (listaaimprimir != null && listaaimprimir.Count > 0)
            {
                var documentoReporte = new SOTWpf.Reportes.IncidenciasCorteTurno.InicidenciasCorteTurno();
                documentoReporte.Load();
                var controladorGlobal = new ControladorConfiguracionGlobal();
                var config = controladorGlobal.ObtenerConfiguracionGlobal();
                if (EsCorteActual)
                {

                    System.DateTime localDate = System.DateTime.Now;
                    documentoReporte.SetDataSource(new List<SOTWpf.Dtos.DtoCabeceraReporte> {new SOTWpf.Dtos.DtoCabeceraReporte
                    {
                        Direccion=config.Direccion,
                        FechaInicio = localDate,
                        FechaFin = localDate,
                        Hotel = config.Nombre,
                        Usuario = ControladorBase.UsuarioActual.NombreCompleto
                  } });
                }
                else
                {
                    documentoReporte.SetDataSource(new List<SOTWpf.Dtos.DtoCabeceraReporte> {new SOTWpf.Dtos.DtoCabeceraReporte
                    {
                        Direccion=config.Direccion,
                        FechaInicio = fechaInicio,
                        FechaFin =fechaFin,
                        Hotel = config.Nombre,
                        Usuario = ControladorBase.UsuarioActual.NombreCompleto
                  } });
                }
                documentoReporte.Subreports["DeatallesIncidencias"].Database.Tables[0].SetDataSource(listaaimprimir);
                var visorR = new VisorReportes();
                visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                visorR.UpdateLayout();
                Utilidades.Dialogos.MostrarDialogos(padre, visorR);
            }
            else
                Utilidades.Dialogos.Aviso("Debe de Exisitir al menos una incidencia");
        }	 
    }
}