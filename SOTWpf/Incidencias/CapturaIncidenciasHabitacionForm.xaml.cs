﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para CapturaIncidenciasForm.xaml
    /// </summary>
    public partial class CapturaIncidenciasHabitacionForm : Window
    {
        bool cargada;

        int idIncidencia;

        public CapturaIncidenciasHabitacionForm(int idIncidencia = 0)
        {
            InitializeComponent();

            this.idIncidencia = idIncidencia;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!cargada)
                return;

            var incidencia = DataContext as Modelo.Entidades.IncidenciaHabitacion;

            if (incidencia == null)
                return;

            var h = cbHabitaciones.SelectedItem as Modelo.Entidades.Habitacion;
            var em = cbEmpleados.SelectedItem as Modelo.Entidades.Empleado;

            if (h == null)
                throw new SOTException(Textos.Errores.habitacion_no_seleccionada_exception);

            if (em == null)
                throw new SOTException(Textos.Errores.empleado_no_seleccionado_excepcion);

            var controladorIncidencias = new ControladorIncidenciasHabitacion();

            if (idIncidencia == 0)
            {
                controladorIncidencias.RegistrarIncidencia(incidencia);
                MessageBox.Show(Textos.Mensajes.registro_incidencia_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else 
            {
                controladorIncidencias.ModificarIncidencia(incidencia);
                MessageBox.Show(Textos.Mensajes.modificacion_incidencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                cbHabitaciones.ItemsSource = controladorHabitaciones.ObtenerActivasConTipos();

                var controladorEmpleados = new ControladorEmpleados();

                cbEmpleados.ItemsSource = controladorEmpleados.ObtenerRecamarerasFILTRO(false);

                if (idIncidencia == 0)
                    DataContext = new Modelo.Entidades.IncidenciaHabitacion();
                else 
                {
                    var controladorIncidencia = new ControladorIncidenciasHabitacion();

                    var incidencia = controladorIncidencia.ObtenerIncidencia(idIncidencia);

                    if (incidencia == null)
                        try
                        {
                            throw new SOTException(Textos.Errores.incidencia_no_modificable_exception, idIncidencia.ToString());
                        }
                        finally 
                        {
                            Close();
                        }

                    DataContext = incidencia;
                }
            }

            cargada = true;
        }
    }
}
