﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para CapturaIncidenciasTurnoForm.xaml
    /// </summary>
    public partial class CapturaIncidenciasTurnoForm : Window
    {
        bool cargada;

        int idIncidencia;
        int? idCorte;

        public CapturaIncidenciasTurnoForm(int idIncidencia = 0, int? idCorte = null)
        {
            InitializeComponent();

            this.idCorte = idCorte;
            this.idIncidencia = idIncidencia;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!cargada)
                return;

            var incidencia = DataContext as Modelo.Entidades.IncidenciaTurno;

            if (incidencia == null)
                return;

            var controladorIncidencias = new ControladorIncidenciasTurno();

            if (idIncidencia == 0)
            {
                controladorIncidencias.RegistrarIncidencia(incidencia, idCorte ?? 0);
                MessageBox.Show(Textos.Mensajes.registro_incidencia_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                controladorIncidencias.ModificarIncidencia(incidencia);
                MessageBox.Show(Textos.Mensajes.modificacion_incidencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorAreas = new SOTControladores.Controladores.ControladorAreas();

                cbAreas.ItemsSource = controladorAreas.ObtenerAreasActivas();

                if (idIncidencia == 0)
                    DataContext = new Modelo.Entidades.IncidenciaTurno();
                else
                {
                    var controladorIncidencia = new ControladorIncidenciasTurno();

                    var incidencia = controladorIncidencia.ObtenerIncidencia(idIncidencia);

                    if (incidencia == null)
                        try
                        {
                            throw new SOTException(Textos.Errores.incidencia_no_modificable_exception, idIncidencia.ToString());
                        }
                        finally
                        {
                            Close();
                        }

                    DataContext = incidencia;
                }
            }

            cargada = true;
        }
    }
}
