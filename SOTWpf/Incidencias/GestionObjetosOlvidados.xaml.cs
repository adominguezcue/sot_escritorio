﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;
using SOTWpf.Reportes.ObjetosOlvidados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para GestionObjetosOlvidados.xaml
    /// </summary>
    public partial class GestionObjetosOlvidados : UserControl
    {
        public GestionObjetosOlvidados()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        public void RegistrarObjetoOlvidado()
        {
            var edicionObjetosOlvidadosF = new EdicionObjetoOlvidadoForm();

            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, edicionObjetosOlvidadosF);

            CargarObjetosOlvidados();
        }

        public void ModificarObjetosOlvidados()
        {
            var objetoOlvidado = dgvObjetoOlvidado.SelectedItem as Modelo.Dtos.DtoObjetoOlvidado;

            if (objetoOlvidado == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (objetoOlvidado.EsCancelado)
                return;

            var edicionObjetosOlvidadosF = new EdicionObjetoOlvidadoForm(objetoOlvidado.Id);

            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, edicionObjetosOlvidadosF);

            CargarObjetosOlvidados();
        }

        public void CancelarObjetosOlvidados()
        {
            var objetoOlvidado = dgvObjetoOlvidado.SelectedItem as Modelo.Dtos.DtoObjetoOlvidado;

            if (objetoOlvidado == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (objetoOlvidado.EsCancelado)
                return;

            if (MessageBox.Show(Textos.Mensajes.confirmar_entrega_objeto_olvidado, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorI = new ControladorObjetosOlvidados();

                controladorI.CancelarObjetoOlvidado(objetoOlvidado.Id);

                MessageBox.Show(Textos.Mensajes.entrega_objeto_olvidado_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarObjetosOlvidados();
            }
        }

        public void EliminarObjetoOlvidado()
        {
            return;

            var objetoOlvidado = dgvObjetoOlvidado.SelectedItem as Modelo.Dtos.DtoObjetoOlvidado;

            if (objetoOlvidado == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_objeto_olvidado, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorI = new ControladorObjetosOlvidados();

                controladorI.EliminarObjetoOlvidado(objetoOlvidado.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_objeto_olvidado_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarObjetosOlvidados();
            }
        }
        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarObjetosOlvidados();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarObjetosOlvidados();
                }
            }
        }

        private void CargarObjetosOlvidados()
        {
            if (this.IsLoaded)
            {
                int? idH = cbHabitaciones.SelectedValue == null || (int)cbHabitaciones.SelectedValue == 0 ? null : cbHabitaciones.SelectedValue as int?;
                int? idE = cbEmpleados.SelectedValue == null || (int)cbEmpleados.SelectedValue == 0 ? null : cbEmpleados.SelectedValue as int?;

                var controlador = new ControladorObjetosOlvidados();
                var objetosOlvidados = controlador.ObtenerObjetosOlvidadosFiltradas(dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate,
                    dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1) : dpFechaFinal.SelectedDate,
                                                                          idH, idE);

                dgvObjetoOlvidado.ItemsSource = objetosOlvidados;
            }
        }

        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarObjetosOlvidados();
        }

        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                var habitaciones = controladorHabitaciones.ObtenerActivasConTipos();
                habitaciones.Insert(0, new Modelo.Entidades.Habitacion { NumeroHabitacion = "Todas" });
                cbHabitaciones.ItemsSource = habitaciones;

                var controladorEmpleados = new ControladorEmpleados();

                var empleados = controladorEmpleados.ObtenerEmpleadosFILTRO(false);
                empleados.Insert(0, new Modelo.Entidades.Empleado { Nombre = "Todos" });
                cbEmpleados.ItemsSource = empleados;

                CargarObjetosOlvidados();
            }
        }

        public void GenerarReporte(Window padre)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new ObjetosOlvidados();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = dpFechaInicial.SelectedDate.Value,
                FechaFin = dpFechaFinal.SelectedDate.Value,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            documentoReporte.Subreports["ObjetosOlvidados"].SetDataSource(dgvObjetoOlvidado.ItemsSource as List<Modelo.Dtos.DtoObjetoOlvidado>);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(padre, visorR);
        }
    }
}
