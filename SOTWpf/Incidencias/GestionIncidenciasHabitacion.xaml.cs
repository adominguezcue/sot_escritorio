﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;
using SOTWpf.Reportes.IncidenciasHabitacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para GestionIncidenciasHabitacion.xaml
    /// </summary>
    public partial class GestionIncidenciasHabitacion : UserControl
    {
	    private bool  _EscorteTunro = false;									
        public GestionIncidenciasHabitacion()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaInicial.Language = lang;
            dpFechaFinal.Language = lang;

            var fechaActual = DateTime.Now;
            dpFechaInicial.SelectedDate = fechaActual;
            dpFechaFinal.SelectedDate = fechaActual;
        }

        public void RegistrarIncidencia()
        {
            var capturaIncidenciasF = new CapturaIncidenciasHabitacionForm();

            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, capturaIncidenciasF);

            CargarIncidencias();
        }

        public void ModificarIncidencia()
        {
            var incidencia = dgvIncidencias.SelectedItem as Modelo.Dtos.DtoIncidenciaHabitacion;

            if (incidencia == null)
                throw new SOTException(Textos.Errores.incidencia_no_seleccionada_exception);

            if (incidencia.EstaCancelada)
                return;

            var capturaIncidenciasF = new CapturaIncidenciasHabitacionForm(incidencia.Id);

            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, capturaIncidenciasF);

            CargarIncidencias();
        }

        public void CancelarIncidencia()
        {
            var incidencia = dgvIncidencias.SelectedItem as Modelo.Dtos.DtoIncidenciaHabitacion;

            if (incidencia == null)
                throw new SOTException(Textos.Errores.incidencia_no_seleccionada_exception);

            if (incidencia.EstaCancelada)
                return;

            if (MessageBox.Show(Textos.Mensajes.confirmar_cancelacion_incidencia, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorI = new ControladorIncidenciasHabitacion();

                controladorI.CancelarIncidencia(incidencia.Id);

                MessageBox.Show(Textos.Mensajes.cancelacion_incidencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarIncidencias();
            }
        }

        public void EliminarIncidencia()
        {
            var incidencia = dgvIncidencias.SelectedItem as Modelo.Dtos.DtoIncidenciaHabitacion;

            if (incidencia == null)
                throw new SOTException(Textos.Errores.incidencia_no_seleccionada_exception);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_incidencia, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controladorI = new ControladorIncidenciasHabitacion();

                controladorI.EliminarIncidencia(incidencia.Id);

                MessageBox.Show(Textos.Mensajes.eliminacion_incidencia_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);

                CargarIncidencias();
            }
        }
        private void dpFechaInicial_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaI = e.AddedItems[0] as DateTime?;

                if (fechaI.HasValue && dpFechaFinal.SelectedDate.HasValue && fechaI > dpFechaFinal.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaI = e.RemovedItems[0] as DateTime?;
                    else
                        fechaI = null;

                    dpFechaInicial.SelectedDate = fechaI;
                }
                else
                {
                    CargarIncidencias();
                }
            }
        }

        private void dpFechaFinal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var fechaF = e.AddedItems[0] as DateTime?;

                if (fechaF.HasValue && dpFechaInicial.SelectedDate.HasValue && fechaF < dpFechaInicial.SelectedDate)
                {
                    if (e.RemovedItems.Count > 0)
                        fechaF = e.RemovedItems[0] as DateTime?;
                    else
                        fechaF = null;

                    dpFechaFinal.SelectedDate = fechaF;
                }
                else
                {
                    CargarIncidencias();
                }
            }
        }

        public void CargarIncidencias(bool? _EsCorteTurno = null, int? idCorteTurno = null )
        {
            if (this.IsLoaded)
            {
                if (_EsCorteTurno != null && _EsCorteTurno == true && idCorteTurno != null)
                    EscorteTurno(idCorteTurno);																						   
											   
                int? idH = cbHabitaciones.SelectedValue == null || (int)cbHabitaciones.SelectedValue == 0 ? null : cbHabitaciones.SelectedValue as int?;
                int? idE = cbEmpleados.SelectedValue == null || (int)cbEmpleados.SelectedValue == 0 ? null : cbEmpleados.SelectedValue as int?;

                var controlador = new ControladorIncidenciasHabitacion();
                if (_EscorteTunro == true)
                {
																					

                    var incidencias = controlador.ObtenerIncidenciasFiltradas(dpFechaInicial.SelectedDate, dpFechaFinal.SelectedDate,idH, idE);
                    dgvIncidencias.ItemsSource = incidencias;

                }
                else
                {

                    var incidencias = controlador.ObtenerIncidenciasFiltradas(dpFechaInicial.SelectedDate.HasValue ? dpFechaInicial.SelectedDate.Value.Date : dpFechaInicial.SelectedDate,
                        dpFechaFinal.SelectedDate.HasValue ? dpFechaFinal.SelectedDate.Value.AddDays(1).Date.AddSeconds(-1) : dpFechaFinal.SelectedDate,
                                                                              idH, idE);

                    dgvIncidencias.ItemsSource = incidencias;
                }
            }
        }

        private void EscorteTurno (int? idcorteturno=null)
        {
            this._EscorteTunro = true;
            int idcorteturno1 = Convert.ToInt32(idcorteturno);
            var controlador = new ControladorCortesTurno();
            var CorteTurno = controlador.ObtenerCorteEnRevision(idcorteturno1);
            DateTime inicial = new DateTime();
            DateTime final1 = new DateTime();
            inicial = CorteTurno.FechaInicio;
            final1 = Convert.ToDateTime(CorteTurno.FechaCorte);
            dpFechaInicial.SelectedDate = inicial;
            dpFechaFinal.SelectedDate = final1;
            dpFechaInicial.IsEnabled = false;
            dpFechaFinal.IsEnabled = false;
            CargarIncidencias(true);
        }
        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarIncidencias();
        }

        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                var habitaciones = controladorHabitaciones.ObtenerActivasConTipos();
                habitaciones.Insert(0, new Modelo.Entidades.Habitacion { NumeroHabitacion = "Todas" });
                cbHabitaciones.ItemsSource = habitaciones;

                var controladorEmpleados = new ControladorEmpleados();

                var empleados = controladorEmpleados.ObtenerRecamarerasFILTRO(false);
                empleados.Insert(0, new Modelo.Entidades.Empleado { Nombre = "Todos" });
                cbEmpleados.ItemsSource = empleados;

                CargarIncidencias();
            }
        }

        public void GenerarReporte(Window padre)
        {
            List<Modelo.Dtos.DtoIncidenciaHabitacion>  listareporte  = dgvIncidencias.ItemsSource as List<Modelo.Dtos.DtoIncidenciaHabitacion>;
            if (listareporte.Count > 0)
            {													 
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new IncidenciasHabitacionPorFecha();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = dpFechaInicial.SelectedDate.Value,
                FechaFin = dpFechaFinal.SelectedDate.Value,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            documentoReporte.Subreports["IncidenciasH"].SetDataSource(dgvIncidencias.ItemsSource as List<Modelo.Dtos.DtoIncidenciaHabitacion>);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(padre, visorR);
            }
            else
            {
                Utilidades.Dialogos.Aviso("No hay registros de inicidencias revise los filtros ó corte");
            }			 	 
        }
    }
}
