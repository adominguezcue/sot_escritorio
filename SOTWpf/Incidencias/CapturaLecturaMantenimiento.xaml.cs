﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SOTControladores.Controladores;
using Modelo.Dtos;
using System.Data;
using System.Collections;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para CapturaLecturaMantenimiento.xaml
    /// </summary>
    public partial class CapturaLecturaMantenimiento : Window
    {
        List<Modelo.Dtos.DtoMedicionesMantenimiento> dtoMedicionesMantenimientos = new List<DtoMedicionesMantenimiento>();
        int? idCorte;
        bool EsModificar = false;
        public CapturaLecturaMantenimiento(bool Esmodicar, int? idCorte = null)
        {
            InitializeComponent();
            this.idCorte = idCorte;
            this.EsModificar = Esmodicar;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladormantenimientolectura = new ControladorLecturaHMantenimiento();
                var CatalogoDePresentacion = controladormantenimientolectura.ObtienePressentacion();
                var controladorLecturasAnterior = new ControladorMedicionesMantenimiento();
                var LecturaAnterior = controladorLecturasAnterior.ObtieneMedicionesActivas();
                if (CatalogoDePresentacion != null && CatalogoDePresentacion.Count > 0)
                {
                    foreach (Modelo.Entidades.CatPresentacionMantenimiento datos in CatalogoDePresentacion)
                    {
                        Modelo.Dtos.DtoMedicionesMantenimiento datopresentacion1 = new DtoMedicionesMantenimiento();
                        datopresentacion1.CorteActual = Convert.ToInt32(idCorte);
                        datopresentacion1.Presentacion = datos;
                        datopresentacion1.PresentacionUnidad = datos.Presentacion;
                        datopresentacion1.PresentacionDescripsion = datos.Descripcion;
                        foreach (Modelo.Entidades.MedicionMantenimiento valoresmediciones in LecturaAnterior)
                        {
                            if (valoresmediciones.CatPresentacionMantenimientoId == datopresentacion1.Presentacion.Id)
                            {
                                datopresentacion1.ValorAnterior = valoresmediciones.Valor;
                            }
                        }
                        dtoMedicionesMantenimientos.Add(datopresentacion1);
                    }
                }
                LecturasH.ItemsSource = dtoMedicionesMantenimientos;
                
            }
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (EsModificar)
            {
                var controladormedicionesmantenimiento = new ControladorMedicionesMantenimiento();
                controladormedicionesmantenimiento.ModificaRegistros(dtoMedicionesMantenimientos, idCorte);
            }
            else
            {
                var controladormedicionesmantenimiento = new ControladorMedicionesMantenimiento();
                controladormedicionesmantenimiento.AgregaRegistros(dtoMedicionesMantenimientos);
            }
            this.Close();
        }


        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Valor_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (dtoMedicionesMantenimientos != null && dtoMedicionesMantenimientos.Count > 0)
            {
                foreach (Modelo.Dtos.DtoMedicionesMantenimiento datoactualiza in dtoMedicionesMantenimientos)
                {
                    if (datoactualiza.Presentacion.Id == ((Modelo.Dtos.DtoMedicionesMantenimiento)((System.Windows.FrameworkElement)sender).DataContext).Presentacion.Id)
                    {
                        datoactualiza.ValorLectura = ((System.Windows.Controls.TextBox)sender).Text.ToString();
                    }
                }
            }
        }

        private void Valor_LostFocus(object sender, RoutedEventArgs e)
        {

        }
    }
}
