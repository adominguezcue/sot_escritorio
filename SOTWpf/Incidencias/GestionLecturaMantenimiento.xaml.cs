﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para GestionLecturaMantenimiento.xaml
    /// </summary>
    public partial class GestionLecturaMantenimiento : UserControl
    {
        internal static readonly DependencyProperty IdCorteProperty =
        DependencyProperty.RegisterAttached("IdCorte", typeof(int), typeof(GestionLecturaMantenimiento), new PropertyMetadata(0, new PropertyChangedCallback(IdCorteCambio)));
        internal int IdCorte
        {
            get { return (int)GetValue(IdCorteProperty); }
            set { SetValue(IdCorteProperty, value); }
        }

        private static void IdCorteCambio(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pg = d as GestionLecturaMantenimiento;
            pg.CargaLecturasManetnimiento();
        }

        public GestionLecturaMantenimiento()
        {
            InitializeComponent();

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public void RegistrarLecturas()
        {
            var CapturaMantenimientoLecturaH = new CapturaLecturaMantenimiento(false, idCorte: IdCorte == 0 ? null : (int?)IdCorte);
            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, CapturaMantenimientoLecturaH);
            CargaLecturasManetnimiento();
        }

        public void ModificarLecturas()
        {
            var CapturaMantenimientoLecturaH = new CapturaLecturaMantenimiento(true, idCorte: IdCorte == 0 ? null : (int?)IdCorte);
            Utilidades.Dialogos.MostrarDialogos(System.Windows.Application.Current.MainWindow, CapturaMantenimientoLecturaH);
            CargaLecturasManetnimiento();
        }

      /*  public void EliminaLecturas()
        {
            var ControladorMantenimientoLecturas = new SOTControladores.Controladores.ControladorMedicionesMantenimiento();
            ControladorMantenimientoLecturas.EliminaRegistrosLectura(idCorte: IdCorte == 0 ? null : (int?)IdCorte);
            CargaLecturasManetnimiento();
        }*/

        public bool Esregistros()
        {
            bool respuesta = false; 
            var ControladorMantenimientoLecturas = new SOTControladores.Controladores.ControladorMedicionesMantenimiento();
            var listachida = ControladorMantenimientoLecturas.ObtieneLecturasFiltradas(idCorte: IdCorte == 0 ? null : (int?)IdCorte);
            if (listachida != null && listachida.Count>0)
                respuesta = true;
                return respuesta;
        }

        public void CargaLecturasManetnimiento()
        {
            if (this.IsLoaded)
            {
                var ControladorMantenimientoLecturas = new SOTControladores.Controladores.ControladorMedicionesMantenimiento();
                var FechaInicio = DateTime.Today;
                //LecturasTurno.ItemsSource = ControladorMantenimientoLecturas.ObtieneLecturasFiltradas(idCorte: IdCorte == 0 ? null : (int?)IdCorte);
                LecturasTurno.ItemsSource = ControladorMantenimientoLecturas.ObtieneLecturasFiltradasPorFecha(FechaInicio);

            }
        }

    }
}
