﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para GestionIncidencias.xaml
    /// </summary>
    public partial class GestionIncidenciasHabitacionForm : Window
    {
        public GestionIncidenciasHabitacionForm()
        {
            InitializeComponent();
        }

        private void btnRegistrarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestionIncidenciasComponente.RegistrarIncidencia();
        }

        private void btnCancelarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestionIncidenciasComponente.CancelarIncidencia();
        }

        private void btnEliminarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestionIncidenciasComponente.EliminarIncidencia();
        }

        private void btnModificarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestionIncidenciasComponente.ModificarIncidencia();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnReporte_Click(object sender, RoutedEventArgs e)
        {
            gestionIncidenciasComponente.GenerarReporte(this);
        }
    }
}
