﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para EdicionObjetoOlvidadoForm.xaml
    /// </summary>
    public partial class EdicionObjetoOlvidadoForm : Window
    {
        bool cargada;

        int idObjetoOlvidado;

        public EdicionObjetoOlvidadoForm(int idObjetoOlvidado = 0)
        {
            InitializeComponent();

            this.idObjetoOlvidado = idObjetoOlvidado;
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (!cargada)
                return;

            var objetoOlvidado = DataContext as Modelo.Entidades.ObjetoOlvidado;

            if (objetoOlvidado == null)
                return;

            var h = cbHabitaciones.SelectedItem as Modelo.Entidades.Habitacion;
            var em = cbEmpleados.SelectedItem as Modelo.Entidades.Empleado;

            if (h == null)
                throw new SOTException(Textos.Errores.habitacion_no_seleccionada_exception);

            if (em == null)
                throw new SOTException(Textos.Errores.empleado_no_seleccionado_excepcion);

            var controladorObjetosOlvidados = new ControladorObjetosOlvidados();

            if (idObjetoOlvidado == 0)
            {
                controladorObjetosOlvidados.RegistrarObjetosOlvidados(objetoOlvidado);
                MessageBox.Show(Textos.Mensajes.registro_objeto_olvidado_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else 
            {
                controladorObjetosOlvidados.ModificarObjetoOlvidado(objetoOlvidado);
                MessageBox.Show(Textos.Mensajes.modificacion_objeto_olvidado_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                cbHabitaciones.ItemsSource = controladorHabitaciones.ObtenerActivasConTipos();

                var controladorEmpleados = new ControladorEmpleados();

                cbEmpleados.ItemsSource = controladorEmpleados.ObtenerEmpleadosFILTRO(false);

                if (idObjetoOlvidado == 0)
                    DataContext = new Modelo.Entidades.ObjetoOlvidado { FechaCreacion = DateTime.Now };
                else
                {
                    var controladorObjetosOlvidados = new ControladorObjetosOlvidados();

                    var objetoOlvidado = controladorObjetosOlvidados.ObtenerObjetoOlvidado(idObjetoOlvidado);

                    if (objetoOlvidado == null)
                        try
                        {
                            throw new SOTException(Textos.Errores.objeto_olvidado_no_modificable_exception, idObjetoOlvidado.ToString());
                        }
                        finally
                        {
                            Close();
                        }

                    DataContext = objetoOlvidado;
                }
            }

            cargada = true;
        }
    }
}
