﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para GestionObjetosOlvidadosForm.xaml
    /// </summary>
    public partial class GestionObjetosOlvidadosForm : Window
    {
        public GestionObjetosOlvidadosForm()
        {
            InitializeComponent();
        }

        private void btnRegistrarObjetoOlvidado_Click(object sender, RoutedEventArgs e)
        {
            gestionObjetosOlvidadosComponente.RegistrarObjetoOlvidado();
        }

        private void btnCancelarObjetoOlvidado_Click(object sender, RoutedEventArgs e)
        {
            gestionObjetosOlvidadosComponente.CancelarObjetosOlvidados();
        }

        private void btnEliminarObjetoOlvidado_Click(object sender, RoutedEventArgs e)
        {
            gestionObjetosOlvidadosComponente.EliminarObjetoOlvidado();
        }

        private void btnModificarObjetoOlvidado_Click(object sender, RoutedEventArgs e)
        {
            gestionObjetosOlvidadosComponente.ModificarObjetosOlvidados();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnReporte_Click(object sender, RoutedEventArgs e)
        {
            gestionObjetosOlvidadosComponente.GenerarReporte(this);
        }
    }
}
