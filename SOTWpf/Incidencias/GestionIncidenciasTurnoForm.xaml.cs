﻿using SOTControladores.Controladores;
using SOTWpf.Dtos;									 			  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Incidencias
{
    /// <summary>
    /// Lógica de interacción para GestionIncidenciasTurnoForm.xaml
    /// </summary>
    public partial class GestionIncidenciasTurnoForm : Window
    {
        public GestionIncidenciasTurnoForm()
        {
            InitializeComponent();
        }

        private void btnRegistrarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.RegistrarIncidencia();
        }

        private void btnCancelarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.CancelarIncidencia();
        }

        private void btnEliminarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.EliminarIncidencia();
        }

        private void btnModificarIncidencia_Click(object sender, RoutedEventArgs e)
        {
            gestorIncidenciasComponente.ModificarIncidencia();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
               var corte = new SOTControladores.Controladores.ControladorCortesTurno().ObtenerUltimoCorte();
               gestorIncidenciasComponente.IdCorte = corte.Id;
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void GestorIncidenciasComponente_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void BtnReporte_Click(object sender, RoutedEventArgs e)
        {

            gestorIncidenciasComponente.GeneraReport(this);
          /*  var listaaimprimir =  gestorIncidenciasComponente.listadeIncidenciasTuno;
            var fechaInicio = gestorIncidenciasComponente.FechaInicio;
            var fechaFin = gestorIncidenciasComponente.FechaFin;
            var EsCorteActual = gestorIncidenciasComponente.SoloCorteActual;
            if (listaaimprimir != null && listaaimprimir.Count > 0)
            {
                var documentoReporte = new SOTWpf.Reportes.IncidenciasCorteTurno.InicidenciasCorteTurno();
                documentoReporte.Load();
                var controladorGlobal = new ControladorConfiguracionGlobal();
                var config = controladorGlobal.ObtenerConfiguracionGlobal();
                if (EsCorteActual)
                {

                    System.DateTime localDate = System.DateTime.Now;
                    documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
                    {
                        Direccion=config.Direccion,
                        FechaInicio = localDate,
                        FechaFin = localDate,
                        Hotel = config.Nombre,
                        Usuario = ControladorBase.UsuarioActual.NombreCompleto
                  } });
                }
                else
                {
                    documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
                    {
                        Direccion=config.Direccion,
                        FechaInicio = fechaInicio,
                        FechaFin =fechaFin,
                        Hotel = config.Nombre,
                        Usuario = ControladorBase.UsuarioActual.NombreCompleto
                  } });
                }
                documentoReporte.Subreports["DeatallesIncidencias"].Database.Tables[0].SetDataSource(listaaimprimir);
                var visorR = new VisorReportes();
                visorR.crCrystalReportViewer.ReportSource = documentoReporte;
                visorR.UpdateLayout();
                Utilidades.Dialogos.MostrarDialogos(this, visorR);
            }
            else
                Utilidades.Dialogos.Aviso("Debe de Exisitir al menos una incidencia");*/

        } 
    }
}
