﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Modelo.Almacen.Entidades.Dtos;
using SOTControladores.Controladores;
using Transversal.Excepciones;
using SOTWpf.Dtos;

namespace SOTWpf.ResTotal.Proveedores
{
    /// <summary>
    /// Lógica de interacción para GestionProveedoresForm.xaml
    /// </summary>
    public partial class GestionProveedoresForm : Window
    {
        public GestionProveedoresForm()
        {
            InitializeComponent();
        }

        private void btnAltaProveedor_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new global::ResTotal.Vista.ZctCatProvV2(ZctSOT.Datos.DAO.Conexion.CadenaConexion(), ControladorBase.UsuarioActual.Id));

            CargarProveedores();
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new SOTWpf.Reportes.Proveedores.Proveedores();
            documentoReporte.Load();

            documentoReporte.SetDataSource(new List<DtoCabeceraReporte> {new DtoCabeceraReporte
            { 
                Direccion=config.Direccion,
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now,
                Hotel = config.Nombre,
                Usuario = ControladorBase.UsuarioActual.NombreCompleto
            }});

            List<DtoFiltroReporte> filtros = new List<DtoFiltroReporte>();
            if (!string.IsNullOrWhiteSpace(txtBusqueda.Text))
                filtros.Add(new DtoFiltroReporte { Filtro = "Filtro", Valor = txtBusqueda.Text });
            //filtros.Add(new DtoFiltroReporte { Filtro = "Categoría", Valor = cbDepartamentos.SelectedValue != null ? cbDepartamentos.SelectedValue.ToString() : "" });
            //filtros.Add(new DtoFiltroReporte { Filtro = "Subcategoría", Valor = cbLineas.SelectedValue != null ? cbLineas.SelectedValue.ToString() : "" });


            //var items = dgvProveedores.ItemsSource as List<Modelo.Entidades.VW_ArticuloVendido>;

            documentoReporte.Subreports["Proveedores"].SetDataSource(dgvProveedores.ItemsSource);
            documentoReporte.Subreports["FiltrosExtra"].SetDataSource(filtros);

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }

        private void btnModificarProveedor_Click(object sender, RoutedEventArgs e)
        {
            var codigoProveedor = dgvProveedores.SelectedValue as int?;

            if (!codigoProveedor.HasValue)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            Utilidades.Dialogos.MostrarDialogos(this, new global::ResTotal.Vista.ZctCatProvV2(ZctSOT.Datos.DAO.Conexion.CadenaConexion(), ControladorBase.UsuarioActual.Id, codigoProveedor.Value));

            CargarProveedores();
        }

        private void btnEliminarProveedor_Click(object sender, RoutedEventArgs e)
        {
            var codigoProveedor = dgvProveedores.SelectedValue as int?;

            if (!codigoProveedor.HasValue)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                var controladorProveedores = new ControladorProveedores();

                controladorProveedores.Elimina(codigoProveedor.Value);

                CargarProveedores();

                Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_elemento_exitosa);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarProveedores();
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            CargarProveedores();
        }

        private void CargarProveedores()
        {
            var controladorProveedores = new SOTControladores.Controladores.ControladorProveedores();
            dgvProveedores.ItemsSource = controladorProveedores.ObtenerResumenesProveedores(txtBusqueda.Text);
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
