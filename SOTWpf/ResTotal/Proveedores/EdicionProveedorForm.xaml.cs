﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.ResTotal.Proveedores
{
    /// <summary>
    /// Lógica de interacción para EdicionProveedorForm.xaml
    /// </summary>
    public partial class EdicionProveedorForm : Window
    {
        public delegate int DBuscaProveedor();
        int _pos = 0;
        bool _buscando = false;
        SOTControladores.Controladores.ControladorPermisos _ControladorPermisos;
        string sSqlBusqueda = "select Cod_Prov,Nom_Prov,RFC_Prov from ZctCatProv order by Cod_Prov";
        Proveedor _NuevoProveedor 
        {
            get { return DataContext as Proveedor; }
            set { DataContext = value; }
        }
        //SOTControladores.Controladores.ControladorProveedores ControladorProveedores;
        public EdicionProveedorForm()
        {
            InitializeComponent();
        }

        private void NuevoProveedor()
        {
            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

            cod_ProvTextBox.IsEnabled = true;
            //ZctCatProvBindingSource.AddNew();
            _pos = ControladorProveedores.ObtenerProveedores().Count;
            _NuevoProveedor = new Proveedor();
            cod_ProvTextBox.Text = ControladorProveedores.ObtenerSiguienteCodigoProveedor();
            nom_ProvTextBox.Focus();
        }

        public void CargaProveedor(int cod_prov)
        {
            var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

            cod_ProvTextBox.Text = cod_prov.ToString();
            _pos = ControladorProveedores.proveedorPos(cod_prov);
            ZctCatProvBindingSource.DataSource = ControladorProveedores.ObtenerProveedor(cod_prov);
            cod_ProvTextBox.Enabled = false;
        }

        private void cod_ProvTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void telefonoTextBox_TextChanged(object sender, EventArgs e)
        {
            var s = sender as TextBox;

            if (System.Text.RegularExpressions.Regex.IsMatch(s.Text, "  ^ [0-9]"))
            {
                s.Text = "";
            }
        }
        private void telefonoTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) /*&& (e.KeyChar != '.')*/)
            {
                e.Handled = true;
            }
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!(e.Key >= Key.D0 && e.Key <= Key.D9) && (e.Key != Key.Back))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                _ControladorPermisos = new SOTControladores.Controladores.ControladorPermisos();//Permisos.Controlador.Controlador(_cnn);
                //_permisos = _ControladorPermisos.ObtenerPermisosActuales();//.getpermisoventana(_user, this.Text);

                var controladorEstados = new SOTControladores.Controladores.ControladorEstados();
                var controladorCiudades = new SOTControladores.Controladores.ControladorCiudades();

                //bool Edita = (_permisos.CrearProveedor || _permisos.ModificarProveedor);
                //Bguardar.Enabled = Edita;

                if (string.IsNullOrEmpty(cod_ProvTextBox.Text))
                {
                    NuevoProveedor();
                }
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var ControladorProveedores = new SOTControladores.Controladores.ControladorProveedores();

                if (_NuevoProveedor != null)
                {
                    ControladorProveedores.CrearProveedor(_NuevoProveedor);

                }
                else
                {
                    var prov = ZctCatProvBindingSource.Current as Proveedor;
                    if (prov == null)
                        return;

                    ControladorProveedores.ModificarProveedor(prov);
                }

                MessageBox.Show("Proveedor guardado correctamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information);
                NuevoProveedor();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
