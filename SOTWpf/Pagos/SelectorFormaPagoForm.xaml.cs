﻿using Dominio.Nucleo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
//using ZctSOT.Interfaces;

namespace SOTWpf.Pagos
{
    /// <summary>
    /// Lógica de interacción para SelectorFormaPagoForm.xaml
    /// </summary>
    public partial class SelectorFormaPagoForm : Window//, ISelectorPago
    {
        private bool _procesoExitoso = false;
        private decimal _valorConIVA;
        private bool _incluirCortesia, _incluirConsumoInterno;

        private decimal valorJ;

        private bool _ocultarPropina;
        private List<Modelo.Dtos.DtoInformacionPago> _formasPagoSeleccionadas = new List<Modelo.Dtos.DtoInformacionPago>();

        private Modelo.Entidades.Propina _propinaGenerada;

        private Justificaciones justificacion;

        public decimal ValorConIVA
        {
            get { return _valorConIVA; }
        }

        public bool ProcesoExitoso
        {
            get { return _procesoExitoso; }
        }

        public Modelo.Entidades.Propina PropinaGenerada
        {
            get
            {
                if (_ocultarPropina || (_propinaGenerada == null) || _propinaGenerada.Valor <= 0)
                {
                    return null;
                }

                return _propinaGenerada;
            }
        }



        public Modelo.Entidades.ConsumoInternoHabitacion DatosConsumo { get; private set; }
        //public Modelo.Entidades.Cortesia DatosCortesia { get; private set; }

        public List<Modelo.Dtos.DtoInformacionPago> FormasPagoSeleccionadas
        {
            get { return _formasPagoSeleccionadas; }
        }


        public SelectorFormaPagoForm(decimal valorConIVA, decimal valorJustificado, bool ocultarPropina, Justificaciones justificacion)
        {
            InitializeComponent();

            _valorConIVA = valorConIVA - valorJustificado;
            _incluirCortesia = true;
            _incluirConsumoInterno = false;
            _ocultarPropina = ocultarPropina;

            this.valorJ = valorJustificado;
            this.justificacion = justificacion;

            DataContext = this;

            if (_valorConIVA <= 0)
            {
                lblTotal.Visibility = txtTotal.Visibility = cbFormasPago.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        public SelectorFormaPagoForm(decimal valorConIVA, bool ocultarPropina, bool incluirCortesia, bool incluirConsumoInterno)
        {
            InitializeComponent();

            _valorConIVA = valorConIVA;
            _incluirCortesia = incluirCortesia;
            _incluirConsumoInterno = incluirConsumoInterno;
            _ocultarPropina = ocultarPropina;

            this.valorJ = 0;
            justificacion = Justificaciones.Ninguna;
            DataContext = this;

            if (_valorConIVA <= 0)
            {
                lblTotal.Visibility = txtTotal.Visibility = cbFormasPago.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            _formasPagoSeleccionadas = new List<Modelo.Dtos.DtoInformacionPago>();

            if (_valorConIVA > 0)
            {
                if (cbFormasPago.SelectedIndex < 0)
                    throw new SOTException(Textos.Errores.forma_pago_no_seleccionada_exception);

                _formasPagoSeleccionadas.AddRange((Modelo.Dtos.DtoInformacionPago[])cbFormasPago.SelectedValue);

                if (_formasPagoSeleccionadas.Count > 1)
                {
                    var pagoTarjeta = _formasPagoSeleccionadas.FirstOrDefault(m => m.TipoPago == TiposPago.TarjetaCredito);
                    var pagoEfectivo = _formasPagoSeleccionadas.FirstOrDefault(m => m.TipoPago == TiposPago.Efectivo);


                    if (pagoTarjeta != null)
                    {
                        _propinaGenerada.TipoTarjeta = formTarjetaMixto.superP.TipoTarjeta;
                        _propinaGenerada.NumeroTarjeta = formTarjetaMixto.superP.NumeroTarjeta;

                        if (_propinaGenerada.IdTipoTarjeta <= 0)
                            throw new SOTException("Seleccione un tipo de tarjeta");

                        pagoTarjeta.Referencia = formTarjetaMixto.superP.NumeroAprobacion;
                        pagoTarjeta.Valor = formTarjetaMixto.superP.Tarjeta;
                        pagoTarjeta.TipoTarjeta = formTarjetaMixto.superP.TipoTarjeta;
                        pagoTarjeta.NumeroTarjeta = formTarjetaMixto.superP.NumeroTarjeta;
                        _propinaGenerada.Referencia = formTarjetaMixto.superP.NumeroAprobacion;
                    }

                    if (pagoEfectivo != null)
                    {
                        pagoEfectivo.Referencia = string.Empty;
                        pagoEfectivo.Valor = formTarjetaMixto.superP.Efectivo;
                    }

                    _propinaGenerada.Valor = formTarjetaMixto.superP.Propina;

                }
                else if (_formasPagoSeleccionadas.Count == 1)
                {
                    var primerPago = _formasPagoSeleccionadas[0];

                    primerPago.Valor = _valorConIVA;

                    switch (primerPago.TipoPago)
                    {
                        case TiposPago.Cortesia:
                            primerPago.Referencia = formCortesia.Observaciones;
                            break;
                        case TiposPago.Efectivo:
                            primerPago.Referencia = string.Empty;

                            break;
                        case TiposPago.Reservacion:
                            primerPago.Referencia = "Supongo que aqui va algo relacionado con la reservación";

                            break;
                        case TiposPago.TarjetaCredito:
                            _propinaGenerada.TipoTarjeta = formTarjetaMixto.superP.TipoTarjeta;
                            _propinaGenerada.NumeroTarjeta = formTarjetaMixto.superP.NumeroTarjeta;

                            if (_propinaGenerada.IdTipoTarjeta <= 0)
                                throw new SOTException("Seleccione un tipo de tarjeta");

                            primerPago.Referencia = formTarjetaMixto.superP.NumeroAprobacion;
                            primerPago.TipoTarjeta = formTarjetaMixto.superP.TipoTarjeta;
                            primerPago.NumeroTarjeta = formTarjetaMixto.superP.NumeroTarjeta;

                            _propinaGenerada.Referencia = formTarjetaMixto.superP.NumeroAprobacion;

                            _propinaGenerada.Valor = formTarjetaMixto.superP.Propina;

                            break;
                        case TiposPago.Consumo:
                            primerPago.Referencia = string.Empty;
                            DatosConsumo = formConsumosInternos.ConsumoActual;
                            //DatosConsumo = new Modelo.Entidades.ConsumoInterno
                            //{
                            //    Activo = true
                            //};
                            break;
                        case TiposPago.Transferencia:
                            primerPago.Referencia = formTransferencias.CodigoTransferencia;
                            break;
                    }

                }

            }

            switch (justificacion)
            {
                case Justificaciones.ConsumoInterno:
                    {
                        var pagoJustificado = _formasPagoSeleccionadas.FirstOrDefault(m => m.TipoPago == TiposPago.Consumo);

                        if (pagoJustificado == null)
                            _formasPagoSeleccionadas.Add(new Modelo.Dtos.DtoInformacionPago
                            {
                                NumeroTarjeta = "",
                                Referencia = string.Empty,
                                TipoPago = TiposPago.Consumo,
                                Valor = valorJ
                            });
                        else
                            pagoJustificado.Valor += valorJ;
                    }
                    break;
                case Justificaciones.Cortesia:
                    {
                        var pagoJustificado = _formasPagoSeleccionadas.FirstOrDefault(m => m.TipoPago == TiposPago.Cortesia);

                        if (pagoJustificado == null)
                            _formasPagoSeleccionadas.Add(new Modelo.Dtos.DtoInformacionPago
                            {
                                NumeroTarjeta = "",
                                Referencia = formCortesia.Observaciones,
                                TipoPago = TiposPago.Cortesia,
                                Valor = valorJ
                            });
                        else
                            pagoJustificado.Valor += valorJ;
                    }
                    break;
            }

            //if (_formasPagoSeleccionadas.)

            _procesoExitoso = true;
            Close();
        }

        private void OcultarTodo()
        {
            formTarjetaMixto.Visibility = System.Windows.Visibility.Collapsed;
            formTransferencias.Visibility = System.Windows.Visibility.Collapsed;
            formCortesia.Visibility = System.Windows.Visibility.Collapsed;
            formConsumosInternos.Visibility = System.Windows.Visibility.Collapsed;

            switch (justificacion)
            {
                case Justificaciones.Cortesia:
                    formCortesia.Visibility = System.Windows.Visibility.Visible;
                    break;
                case Justificaciones.ConsumoInterno:
                    formConsumosInternos.Visibility = System.Windows.Visibility.Visible;
                    break;
            }
        }

        private void SelectorFormaPagoForm_Load(object sender, RoutedEventArgs e)
        {
            OcultarTodo();

            var controladorPagos = new ControladorPagos();

            cbFormasPago.ItemsSource = controladorPagos.ObtenerTiposValidosConMixtos(_incluirCortesia, _incluirConsumoInterno);//_incluirReservacion);

            //cbFormasPago.SelectedIndex = -1;
        }

        private void cbFormasPago_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var elementoSelecionado = cbFormasPago.SelectedItem as Modelo.Dtos.DtoGrupoPago;

            OcultarTodo();

            if (elementoSelecionado == null || elementoSelecionado.Pagos.Length == 0)
            {
                OcultarTodo();
            }
            else if (elementoSelecionado.Pagos.Length > 1)
            {
                formTarjetaMixto.CargarPagos(elementoSelecionado.Pagos, _valorConIVA, _ocultarPropina);

                formTarjetaMixto.Visibility = System.Windows.Visibility.Visible;
                //formCortesia.Visibility = System.Windows.Visibility.Collapsed;

                _propinaGenerada = new Modelo.Entidades.Propina();
            }
            else
            {
                var primerPago = elementoSelecionado.Pagos[0];

                switch (primerPago.TipoPago)
                {
                    case TiposPago.Cortesia:
                        //formTarjetaMixto.Visibility = System.Windows.Visibility.Collapsed;
                        formCortesia.Visibility = System.Windows.Visibility.Visible;

                        _propinaGenerada = null;
                        
                        break;
                    case TiposPago.Cupon:

                        _propinaGenerada = null;

                        break;
                    case TiposPago.Efectivo:
                        _propinaGenerada = null;

                        break;
                    case TiposPago.Reservacion:
                        _propinaGenerada = null;

                        break;
                    case TiposPago.TarjetaCredito:
                        formTarjetaMixto.CargarPagos(elementoSelecionado.Pagos, _valorConIVA, _ocultarPropina);

                        formTarjetaMixto.Visibility = System.Windows.Visibility.Visible;

                        _propinaGenerada = new Modelo.Entidades.Propina();

                        break;
                    case TiposPago.VPoints:
                        _propinaGenerada = null;

                        break;
                    case TiposPago.Consumo:
                        formConsumosInternos.Visibility = System.Windows.Visibility.Visible;
                        _propinaGenerada = null;

                        break;
                    case TiposPago.Transferencia:
                        formTransferencias.Visibility = System.Windows.Visibility.Visible;
                        _propinaGenerada = null;

                        break;
                }

            }
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            _procesoExitoso = false;
            DatosConsumo = null;
            Close();
        }

        public enum Justificaciones
        {
            Ninguna = 0,
            Cortesia = 1,
            ConsumoInterno = 2
        }
    }
}