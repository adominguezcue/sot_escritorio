﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Pagos
{
    /// <summary>
    /// Lógica de interacción para SelectorFormaPagoSimple.xaml
    /// </summary>
    public partial class SelectorFormaPagoSimple : Window
    {
        internal TiposPago? PagoSeleccionado { get; private set; }

        public SelectorFormaPagoSimple()
        {
            InitializeComponent();
        }

        private void SelectorFormaPagoForm_Load(object sender, RoutedEventArgs e)
        {
            cbFormasPago.ItemsSource = Transversal.Extensiones.EnumExtensions.ComoLista<TiposPago>();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            PagoSeleccionado = cbFormasPago.SelectedItem as TiposPago?;

            if (PagoSeleccionado == null)
                throw new SOTException(Textos.Errores.forma_pago_no_seleccionada_exception);

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            PagoSeleccionado = null;
            Close();
        }
    }
}
