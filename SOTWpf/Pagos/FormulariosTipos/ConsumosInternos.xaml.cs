﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Pagos.FormulariosTipos
{
    /// <summary>
    /// Lógica de interacción para ConsumosInternos.xaml
    /// </summary>
    public partial class ConsumosInternos : UserControl
    {
        ConsumoInternoHabitacion _consumoActual;

        internal ConsumoInternoHabitacion ConsumoActual
        {
            get { return _consumoActual; }
            private set
            {
                _consumoActual = value;
                DataContext = _consumoActual;
            }
        }

        public ConsumosInternos()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                cbEmpleados.ItemsSource = new ControladorEmpleados().ObtenerResumenEmpleadosPuestosAreasFILTRO();


                ConsumoActual = new Modelo.Entidades.ConsumoInternoHabitacion
                {
                    Activo = true,
                    FechaCreacion = DateTime.Now
                };


            }
        }
    }
}
