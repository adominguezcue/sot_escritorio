﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Pagos.FormulariosTipos
{
    /// <summary>
    /// Lógica de interacción para TarjetaMixto.xaml
    /// </summary>
    public partial class TarjetaMixto : UserControl
    {
        private bool _ocultarPropina;

        public SuperPago superP { get; private set; }

        public TarjetaMixto()
        {
            InitializeComponent();

            superP = new SuperPago();

            DataContext = superP;
        }

        internal void CargarPagos(Modelo.Dtos.DtoInformacionPago[] informacionPagos, decimal valor, bool ocultarPropina) 
        {
            if (informacionPagos.Length == 1 && informacionPagos[0].TipoPago == TiposPago.TarjetaCredito)
            {
                lblPagoTarjeta.Visibility = System.Windows.Visibility.Collapsed;
                nudPagoTarjeta.Visibility = System.Windows.Visibility.Collapsed;
                lblPagoEfectivo.Visibility = System.Windows.Visibility.Collapsed;
                nudPagoEfectivo.Visibility = System.Windows.Visibility.Collapsed;
            }
            else if (informacionPagos.Length == 2 
                     && informacionPagos.Any(m => m.TipoPago == TiposPago.TarjetaCredito) 
                     && informacionPagos.Any(m => m.TipoPago == TiposPago.Efectivo))
            {

                lblPagoTarjeta.Visibility = System.Windows.Visibility.Visible;
                nudPagoTarjeta.Visibility = System.Windows.Visibility.Visible;
                lblPagoEfectivo.Visibility = System.Windows.Visibility.Visible;
                nudPagoEfectivo.Visibility = System.Windows.Visibility.Visible;
            }
            else 
            {
                Visibility = System.Windows.Visibility.Hidden;
                throw new InvalidOperationException(GetType() + " no funciona con formas de pago que no sean la combinación de efectivo y tarjeta de crédito o solamente la última");
            }

            _ocultarPropina = ocultarPropina;
            superP.Total = valor;

            superP.Tarjeta = superP.Total;
            ActualizarVisibilidadPropina();
        }

        private void ActualizarVisibilidadPropina()
        {
            if (_ocultarPropina)
            {
                lblPropinaTarjeta.Visibility = System.Windows.Visibility.Collapsed;
                nudPropina.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                lblPropinaTarjeta.Visibility = System.Windows.Visibility.Visible;
                nudPropina.Visibility = System.Windows.Visibility.Visible;
            }

            superP.Propina = 0;
        }

        //private void nudPagoTarjeta_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    nudPagoEfectivo.Value = _valorPago - nudPagoTarjeta.Value;
        //}

        private void btnVisa_Click(object sender, RoutedEventArgs e)
        {
            btnVisa.Background = new SolidColorBrush(Colors.Red);
            btnAmericanExpress.Background = new SolidColorBrush(Colors.Transparent);
            btnMasterCard.Background = new SolidColorBrush(Colors.Transparent);

            superP.TipoTarjeta = Modelo.Entidades.TiposTarjeta.Visa;
        }

        private void btnMasterCard_Click(object sender, RoutedEventArgs e)
        {
            btnVisa.Background = new SolidColorBrush(Colors.Transparent);
            btnAmericanExpress.Background = new SolidColorBrush(Colors.Transparent);
            btnMasterCard.Background = new SolidColorBrush(Colors.Red);

            superP.TipoTarjeta = Modelo.Entidades.TiposTarjeta.MasterCard;
        }

        private void btnAmericanExpress_Click(object sender, RoutedEventArgs e)
        {
            btnVisa.Background = new SolidColorBrush(Colors.Transparent);
            btnAmericanExpress.Background = new SolidColorBrush(Colors.Red);
            btnMasterCard.Background = new SolidColorBrush(Colors.Transparent);

            superP.TipoTarjeta = Modelo.Entidades.TiposTarjeta.AmericanExpress;
        }

        public class SuperPago : INotifyPropertyChanged
        {
            decimal _efectivo, _tarjeta, _total, _propina;
            string _numeroTarjeta, _numeroAprobacion;

            private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public decimal Efectivo
            {
                get
                {
                    return _efectivo;
                }
                private set
                {
                    _efectivo = value;
                    NotifyPropertyChanged();
                }
            }

            public decimal Tarjeta
            {
                get
                {
                    return _tarjeta;
                }
                set
                {
                    if (value > _total)
                        _tarjeta = _total;
                    else
                        _tarjeta = value;

                    Efectivo = Total - Tarjeta;

                    NotifyPropertyChanged();
                }
            }

            public decimal Total 
            {
                get { return _total; }
                set 
                {
                    _total = value;

                    if (Tarjeta > _total)
                        Tarjeta = _total;

                    NotifyPropertyChanged();
                }
            }

            public decimal Propina
            {
                get
                {
                    return _propina;
                }
                set
                {
                    _propina = value;
                    NotifyPropertyChanged();
                }
            }

            public string NumeroTarjeta
            {
                get
                {
                    return _numeroTarjeta;
                }
                set
                {
                    _numeroTarjeta = value;
                    NotifyPropertyChanged();
                }
            }

            public string NumeroAprobacion
            {
                get
                {
                    return _numeroAprobacion;
                }
                set
                {
                    _numeroAprobacion = value;
                    NotifyPropertyChanged();
                }
            }

            public Modelo.Entidades.TiposTarjeta TipoTarjeta { get; set; }
        }
    }
}
