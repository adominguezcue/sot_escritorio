﻿using SOTControladores.Controladores;
using SOTWpf.Reportes.Articulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.EtiquetasArticulosCompuestos
{
    /// <summary>
    /// Lógica de interacción para GestionEtiquetas.xaml
    /// </summary>
    public partial class GestionEtiquetas : Window
    {
        public GestionEtiquetas()
        {
            InitializeComponent();

            var lang = System.Windows.Markup.XmlLanguage.GetLanguage("es-MX");
            dpFechaElaboracion.Language = lang;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarEtiquetas();
            }
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CargarEtiquetas();
        }

        private void dpFechaElaboracion_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            CargarEtiquetas();
        }

        private void CargarEtiquetas()
        {
            if (!IsLoaded)
                return;

            var controladorEtiquetas = new ControladorEtiquetas();

            dgvEtiquetas.ItemsSource = controladorEtiquetas.ObtenerEtiquetasFiltradas(txtNombre.Text, dpFechaElaboracion.SelectedDate);
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            var item = dgvEtiquetas.SelectedItem as Modelo.Almacen.Entidades.VW_EtiquetasArticulosCompuestos;

            if (item == null)
                throw new SOTException(Textos.Errores.etiqueta_no_seleccionada_excepcion);

            var controladorGlobal = new ControladorConfiguracionGlobal();
            var config = controladorGlobal.ObtenerConfiguracionGlobal();


            var documentoReporte = new EtiquetaArticuloSimple();
            documentoReporte.Load();

            var itemsImprimir = new List<Modelo.Almacen.Entidades.VW_EtiquetasArticulosCompuestos>();

            for (int i = 0; i < item.Cantidad; i++) 
            {
                itemsImprimir.Add(new Modelo.Almacen.Entidades.VW_EtiquetasArticulosCompuestos
                {
                    CodigoArticulo = item.CodigoArticulo,
                    FechaElaboracion = item.FechaElaboracion,
                    NombreArticulo = item.NombreArticulo
                });
            }

            documentoReporte.SetDataSource(itemsImprimir.Select(m => new
            {
                CodigoArticulo = item.CodigoArticulo,
                FechaElaboracionNoNull = item.FechaElaboracionNoNull,
                NombreArticulo = item.NombreArticulo
            }));

            var visorR = new VisorReportes();

            visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            visorR.UpdateLayout();

            Utilidades.Dialogos.MostrarDialogos(this, visorR);
        }
    }
}
