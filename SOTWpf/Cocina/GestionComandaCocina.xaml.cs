﻿using SOTWpf.Comandas.Manejadores;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SOTWpf.Cocina
{
    /// <summary>
    /// Lógica de interacción para GestionComandaCocina.xaml
    /// </summary>
    public partial class GestionComandaCocina : UserControl
    {
        ManejadorComandaBase gcmd;


        //private static readonly RoutedEvent ClickEvent =
        //    EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
        //    typeof(RoutedEventHandler), typeof(GestionComandaCocina));

        ///// <summary>The ValueChanged event is called when the TextBoxValue of the control changes.</summary>
        //public event RoutedEventHandler Click
        //{
        //    add { AddHandler(ClickEvent, value); }
        //    remove { RemoveHandler(ClickEvent, value); }
        //}

        public GestionComandaCocina()
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();

            //this.Resources["plantillaArticuloSeleccionado1"] = this.Resources["plantillaArticuloSeleccionadosinObservaciones"];
            //this.Resources["plantillaArticuloSeleccionado1"] = this.Resources["plantillaArticuloSeleccionadocobserva"]; 
        }

        private void btnCobrar_Click(object sender, RoutedEventArgs e)
        {
            var tmp = gcmd;

            if (tmp != null)
                tmp.Aceptar();
        }

        private void btnSolicitarCambio_Click(object sender, RoutedEventArgs e)
        {
            var tmp = gcmd;

            if (tmp != null)
                tmp.SolicitarCambio();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            var tmp = gcmd;

            if (tmp != null)
                tmp.SolicitarCancelacion();
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            gcmd = e.NewValue as ManejadorComandaBase;
            ValidaObservaciones();

            //RecargarVisibilidad();
        }

        private void ValidaObservaciones()
        {
            bool _esPlantillaConObservaciobes = false;
            if (gcmd != null)
            {
                if (gcmd.Resumenes.Count > 0)
                {
                    foreach (SOTWpf.Comandas.ResumenArticulo resumenar in gcmd.Resumenes)
                    {
                        if (resumenar.Nombre.Length>21)
                            resumenar.Nombre = resumenar.Nombre.Substring(0, 21);
                        if (!resumenar.Observaciones.Equals(""))
                            _esPlantillaConObservaciobes = true;
                    }
                }
            }
            if (_esPlantillaConObservaciobes)
            {
                this.Resources["plantillaArticuloSeleccionado1"] = this.Resources["plantillaArticuloSeleccionadocobserva"];
                _esPlantillaConObservaciobes = false;
            }
            else
            {
                this.Resources["plantillaArticuloSeleccionado1"] = this.Resources["plantillaArticuloSeleccionadosinObservaciones"];
            }
        }

     
        private void RecargarVisibilidad()
        {
            //if (gcmd == null || gcmd.EnPreparacion)
            //{
            //    btnCancelar.Visibility = System.Windows.Visibility.Hidden;
            //    btnSolicitarCambio.Visibility = System.Windows.Visibility.Hidden;

            //    gridInformacion.Visibility = System.Windows.Visibility.Collapsed;
            //}
            //else
            //{
            //    btnCancelar.Visibility = System.Windows.Visibility.Visible;
            //    btnSolicitarCambio.Visibility = System.Windows.Visibility.Visible;

            //    gridInformacion.Visibility = System.Windows.Visibility.Visible;
            //}
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //RaiseEvent(new RoutedEventArgs(ClickEvent));
            var tmp = gcmd;

            if (tmp != null)
                tmp.Aceptar();
        }
    }
}
