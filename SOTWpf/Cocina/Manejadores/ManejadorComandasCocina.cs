﻿using SOTWpf.Comandas.Manejadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.Collections.Specialized;
using SOTWpf.OrdenesRestaurante.Manejadores;
using SOTControladores.Controladores;
using SOTWpf.ConsumosInternos.Manejadores;

namespace SOTWpf.Cocina.Manejadores
{
    public class ManejadorComandasCocina : IManejadorComandas
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool AceptaMas
        {
            get
            {
                return false;
            }
        }

        ObservableCollection<ManejadorComandaBase> _manejadores;

        public ObservableCollection<ManejadorComandaBase> Manejadores
        {
            get { return _manejadores; }
            private set
            {
                _manejadores = value;
                NotifyPropertyChanged();
            }
        }

        //private bool _paraEntregas;
        private string _departamento;

        private string _etiquetaDestino;
        public string EtiquetaDestino
        {
            get { return _etiquetaDestino; }
            private set
            {
                _etiquetaDestino = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorComandasCocina(string departamento/*, bool paraEntregas*/)
        {
            //_paraEntregas = paraEntregas;
            _departamento = departamento;

            Manejadores = new ObservableCollection<ManejadorComandaBase>();
            Manejadores.CollectionChanged += manejadoresBindingSource_DataSourceChanged;
        }

        private void manejadoresBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (ManejadorComandaBase item in e.OldItems)
                {
                    item.PostCobro -= CargarComandasEH;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (ManejadorComandaBase item in e.NewItems)
                {
                    item.PostCobro += CargarComandasEH;
                }
            }
        }

        private void CargarComandasEH(object sender, EventArgs e)
        {
            CargarComandas();
        }

        public void Agregar(Window padre)
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public void CargarComandas()
        {
            Manejadores.Clear();

            var controladorRS = new ControladorRoomServices();

            var comandas = controladorRS.ObtenerDetallesComandasArticulosPreparadosOEnPreparacion(_departamento);

            var controladorRestaurantes = new ControladorRestaurantes();

            var ordenes = controladorRestaurantes.ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(_departamento);

            var controladorConsumosInternos = new ControladorConsumosInternos();

            var consumosInternos = controladorConsumosInternos.ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion(_departamento);

            var estadoProceso = Modelo.Entidades.ArticuloComanda.Estados.EnProceso;

            foreach (var comanda in comandas)
            {
                var gComanda = new ManejadorComandaCocina(comanda, comanda.ArticulosComanda.Any(m => m.Activo && m.Estado == estadoProceso));

                Manejadores.Add(gComanda);
            }

            foreach (var comanda in ordenes)
            {
                var gComanda = new ManejadorOrdenCocina(comanda, comanda.ArticulosComanda.Any(m => m.Activo && m.Estado == estadoProceso));

                Manejadores.Add(gComanda);
            }

            foreach (var comanda in consumosInternos)
            {
                var gComanda = new ManejadorConsumoInternoCocina(comanda, comanda.ArticulosComanda.Any(m => m.Activo && m.Estado == estadoProceso));

                Manejadores.Add(gComanda);
            }
        }
    }
}
