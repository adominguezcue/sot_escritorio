﻿using SOTWpf.Comandas.Manejadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SOTWpf.Cocina
{
    /// <summary>
    /// Lógica de interacción para GestionComandasCocina.xaml
    /// </summary>
    public partial class GestionComandasCocina : UserControl
    {
        private DispatcherTimer timerCargaComandas;

        IManejadorComandas _gcfm, tmp;

        bool seguirCargando = true;

        public GestionComandasCocina()
        {
            // Llamada necesaria para el diseñador.
            InitializeComponent();

            txtBusqueda.Focus();
        }

        private void CargarComandas()
        {
            tmp = _gcfm;

            if (tmp == null)
                return;

            tmp.CargarComandas();
        }

        private void GestionComandas_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarComandas();

                timerCargaComandas = new DispatcherTimer();
                timerCargaComandas.Interval = TimeSpan.FromSeconds(15);
                timerCargaComandas.Tick += timerCargaComandas_Tick;

                timerCargaComandas.Start();
            }
        }

        private void timerCargaComandas_Tick(object sender, EventArgs e)
        {
            if (seguirCargando)
                CargarComandas();
        }

        private void GestionComandas_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                seguirCargando = false;
                if (timerCargaComandas != null)
                {
                    timerCargaComandas.Stop();
                    timerCargaComandas.Tick -= timerCargaComandas_Tick;
                }
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            tmp = _gcfm;

            if (tmp == null)
                return;

            timerCargaComandas.Stop();
            try
            {
                tmp.Agregar(Application.Current.MainWindow);
            }
            finally
            {
                timerCargaComandas.Start();
            }
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            _gcfm = e.NewValue as IManejadorComandas;

            CargarComandas();
        }

        private void manejadoresEnPreparacion_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ManejadorComandaBase;

            e.Accepted = item != null && item.EnPreparacion;// && (string.IsNullOrWhiteSpace(txtBusqueda.Text) || item.NumeroOrden.ToString() == txtBusqueda.Text);
        }

        private void manejadoresNoEnPreparacion_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ManejadorComandaBase;

            e.Accepted = item != null && !item.EnPreparacion;// && (string.IsNullOrWhiteSpace(txtBusqueda.Text) || item.NumeroOrden.ToString() == txtBusqueda.Text);
        }

        private void txtBusqueda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                tmp = _gcfm;

                if (tmp == null)
                    return;

                int orden;

                try
                {
                    if (int.TryParse(txtBusqueda.Text, out orden))
                    {
                        var manejadorC = tmp.Manejadores.FirstOrDefault(m => m.NumeroOrden == orden);

                        if (manejadorC != null)
                            manejadorC.Aceptar();
                    }
                }
                finally
                {
                    txtBusqueda.Text = "";
                }
            }
            //else if (e.Key == Key.Tab)
            //{
            //    txtBusqueda.Focus();
            //}
        }
    }
}
