﻿using Datos.Nucleo.Contextos;
using SOTWpf.Properties;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Sistema
{
    /// <summary>
    /// Lógica de interacción para EditorConexion.xaml
    /// </summary>
    public partial class EditorConexion : Window
    {
        internal ConexionSOT Conexion 
        {
            get { return DataContext as ConexionSOT; }
            set { DataContext = value; }
        }

        public EditorConexion()
        {
            InitializeComponent();

            Conexion = ConexionSOT.Default;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProbarConexion();

            MessageBox.Show("Conexión exitosa", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void ProbarConexion()
        {
            SqlConnectionStringBuilder ConexionBuilder = new SqlConnectionStringBuilder();

            if (!string.IsNullOrEmpty(Conexion.DbSOT))
                ConexionBuilder.InitialCatalog = Conexion.DbSOT;
            if (!string.IsNullOrEmpty(Conexion.OrigenSOT))
                ConexionBuilder.DataSource = Conexion.OrigenSOT;

            // set the integrated security status
            ConexionBuilder.IntegratedSecurity = Conexion.SeguridadIntegradaSOT;

            if (Conexion.SeguridadIntegradaSOT)
            {
                ConexionBuilder.UserID = ConexionBuilder.Password = "";
            }
            else
            {
                //if (!string.IsNullOrEmpty(Conexion.UsuarioSOT))
                    ConexionBuilder.UserID = Conexion.UsuarioSOT ?? "";
                //if (!string.IsNullOrEmpty(Conexion.PassSOT))
                    ConexionBuilder.Password = Conexion.PassSOT ?? "";
            }

            using (DbContext contexto = new DbContext(ConexionBuilder.ConnectionString))
            {
                try
                {
                    contexto.Database.Connection.Open();
                }
                finally
                {
                    try
                    {
                        if (contexto.Database.Connection.State == ConnectionState.Open)
                            contexto.Database.Connection.Close();
                    }
                    catch
                    {
                    }

                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                ProbarConexion();
            }
            catch (Exception ex)
            {
                if (MessageBox.Show(this, "Se produjeron errores al intentar probar la conexión ¿Desea guardar de todas formas?" +
                                          Environment.NewLine + Environment.NewLine +
                                          "Detalles:" + Environment.NewLine + ex.Message, "Advertencia", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
            }
            ConexionHelper.CambiarConexion(false, Conexion.DbSOT, Conexion.OrigenSOT, Conexion.UsuarioSOT, Conexion.PassSOT, Conexion.SeguridadIntegradaSOT);

            Conexion.EsConfiguradoSOT = true;

            Conexion.Save();

            MessageBox.Show("Cambios guardados", "Mensaje", MessageBoxButton.OK, MessageBoxImage.Information);

            Close();
        }

        internal void CambiarConexion(bool configurar)
        {
            try
            {
                ProbarConexion();
            }
            catch (Exception ex)
            {
                if (MessageBox.Show(this, "Se produjeron errores al intentar probar la conexión, se recomienda corregir la configuración" +
                                          Environment.NewLine + Environment.NewLine +
                                          "Detalles:" + Environment.NewLine + ex.Message, "Advertencia", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
            }
            ConexionHelper.CambiarConexion(false, Conexion.DbSOT, Conexion.OrigenSOT, Conexion.UsuarioSOT, Conexion.PassSOT, Conexion.SeguridadIntegradaSOT);

            if(configurar)
                Conexion.EsConfiguradoSOT = true;

            Conexion.Save();
        }
    }
}
