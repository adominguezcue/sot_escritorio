﻿using Modelo.Entidades;
using SOTWpf.CustomValueConverters;
using SOTWpf.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Sistema
{
    /// <summary>
    /// Lógica de interacción para EdicionParametrosPuntosLealtadUC.xaml
    /// </summary>
    public partial class EdicionParametrosPuntosLealtadUC : UserControl
    {
        public static readonly RoutedEvent GuardadoExitosoEvent = EventManager.RegisterRoutedEvent("GuardadoExitoso", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosPuntosLealtadUC));
        public static readonly RoutedEvent EliminacionExitosaEvent = EventManager.RegisterRoutedEvent("EliminacionExitosa", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosPuntosLealtadUC));
        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosPuntosLealtadUC));

        private ConfiguracionPuntosLealtad ConfiguracionActual
        {
            get { return DataContext as ConfiguracionPuntosLealtad; }
            set { DataContext = value; }
        }

        public void Cancelar()
        {
            CargarConfiguracion();
            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        public void Guardar()
        {
            if (chbRequiereConfiguracionPuntos.IsChecked == true)
            {
                if (ConfiguracionActual == null)
                    throw new SOTException(Textos.Errores.configuracion_puntos_requerida_exception);

                new SOTControladores.Controladores.ControladorVPoints().GuardarConfiguracion(ConfiguracionActual);

                Utilidades.Dialogos.Aviso(Textos.Mensajes.configuracion_puntos_exitosa_excepcion);

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new ConsultaTarjetaVForm(true, null));

                RaiseEvent(new RoutedEventArgs(GuardadoExitosoEvent));

            }
            else
            {
                if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
                {
                    new SOTControladores.Controladores.ControladorVPoints().EliminarConfiguracion();

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_elemento_exitosa);

                    RaiseEvent(new RoutedEventArgs(EliminacionExitosaEvent));
                }
            }
        }

        public event RoutedEventHandler GuardadoExitoso
        {
            add
            {
                AddHandler(GuardadoExitosoEvent, value);
            }

            remove
            {
                RemoveHandler(GuardadoExitosoEvent, value);
            }
        }

        public event RoutedEventHandler EliminacionExitosa
        {
            add
            {
                AddHandler(EliminacionExitosaEvent, value);
            }

            remove
            {
                RemoveHandler(EliminacionExitosaEvent, value);
            }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add
            {
                AddHandler(EdicionCanceladaEvent, value);
            }

            remove
            {
                RemoveHandler(EdicionCanceladaEvent, value);
            }
        }

        public EdicionParametrosPuntosLealtadUC()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                (Resources["MultiplicadorConverter"] as MultiplicadorConverter).Multiplicador = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().Iva_CatPar + 1;
                CargarConfiguracion();
            }
        }

        private void CargarConfiguracion()
        {
            ConfiguracionActual = new SOTControladores.Controladores.ControladorVPoints().ObtenerConfiguracion() ?? new ConfiguracionPuntosLealtad();

            chbRequiereConfiguracionPuntos.IsChecked = ConfiguracionActual != null;
        }

        private void GuardarYProbar_Click(object sender, RoutedEventArgs e)
        {
            Guardar();
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new ConsultaTarjetaVForm(true, null));
        }
    }
}
