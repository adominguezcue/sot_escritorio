﻿using Modelo.Almacen.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Sistema
{
    /// <summary>
    /// Lógica de interacción para EdicionParametrosSistemaForm.xaml
    /// </summary>
    public partial class EdicionParametrosOperacionUC : UserControl
    {
        public static readonly RoutedEvent GuardadoExitosoEvent = EventManager.RegisterRoutedEvent("GuardadoExitoso", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosOperacionUC));
        public static readonly RoutedEvent EliminacionExitosaEvent = EventManager.RegisterRoutedEvent("EliminacionExitosa", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosOperacionUC));
        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosOperacionUC));

        public event RoutedEventHandler GuardadoExitoso
        {
            add
            {
                AddHandler(GuardadoExitosoEvent, value);
            }

            remove
            {
                RemoveHandler(GuardadoExitosoEvent, value);
            }
        }

        public event RoutedEventHandler EliminacionExitosa
        {
            add
            {
                AddHandler(EliminacionExitosaEvent, value);
            }

            remove
            {
                RemoveHandler(EliminacionExitosaEvent, value);
            }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add
            {
                AddHandler(EdicionCanceladaEvent, value);
            }

            remove
            {
                RemoveHandler(EdicionCanceladaEvent, value);
            }
        }

        private ZctCatPar ParametrosActuales
        {
            get { return DataContext as ZctCatPar; }
            set { DataContext = value; }
        }

        public EdicionParametrosOperacionUC()
        {
            InitializeComponent();
        }

        public void Guardar()
        {
            if (ParametrosActuales != null)
            {
                var controlador = new ControladorConfiguracionGlobal();

                if (ParametrosActuales.id == 0)
                {
                    controlador.CrearParametros(ParametrosActuales);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.creacion_elemento_exitosa);
                }
                else
                {
                    controlador.ModificarParametros(ParametrosActuales);

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.modificacion_elemento_exitosa);
                }

                CargarParametros();

                RaiseEvent(new RoutedEventArgs(GuardadoExitosoEvent));
            }
        }

        public void Eliminar()
        {
            if (ParametrosActuales != null && ParametrosActuales.id != 0)
            {
                var controlador = new ControladorConfiguracionGlobal();

                if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) != MessageBoxResult.Yes)
                    return;

                controlador.EliminarParametros();

                Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_elemento_exitosa);

                CargarParametros();

                RaiseEvent(new RoutedEventArgs(EliminacionExitosaEvent));
            }
        }

        public void Cancelar()
        {
            CargarParametros();

            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CargarParametros();
        }

        private void CargarParametros()
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                ParametrosActuales = new ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal() ?? new ZctCatPar();
            }
        }
    }
}
