﻿using Microsoft.Win32;
using Modelo.Sistemas.Entidades;
using Modelo.Sistemas.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Transversal.Utilidades;

namespace SOTWpf.Sistema
{
    /// <summary>
    /// Lógica de interacción para EdicionParametrosSistemaUC.xaml
    /// </summary>
    public partial class EdicionParametrosSistemaUC : UserControl
    {

        public static readonly RoutedEvent GuardadoExitosoEvent = EventManager.RegisterRoutedEvent("GuardadoExitoso", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosSistemaUC));
        public static readonly RoutedEvent EliminacionExitosaEvent = EventManager.RegisterRoutedEvent("EliminacionExitosa", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosSistemaUC));


        ParametrosSOT ParametrosSOTConfig
        {
            get { return gbParametrosSOT.DataContext as ParametrosSOT; }
            set { gbParametrosSOT.DataContext = value; }
        }

        ParametrosSincronizacion ParametrosSincronizacionConfig
        {
            get { return gbParametrosSincronizacion.DataContext as ParametrosSincronizacion; }
            set { gbParametrosSincronizacion.DataContext = value; }
        }

        ParametrosGeneradorFolios ParametrosGeneradorFoliosConfig
        {
            get { return gbParametrosGeneradorFolios.DataContext as ParametrosGeneradorFolios; }
            set { gbParametrosGeneradorFolios.DataContext = value; }
        }

        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionParametrosSistemaUC));

        private Guid idSistemaSOT;
        private Guid idSistemaSincronizacion;
        private Guid idSistemaGeneradorFolios;

        public event RoutedEventHandler GuardadoExitoso
        {
            add
            {
                AddHandler(GuardadoExitosoEvent, value);
            }

            remove
            {
                RemoveHandler(GuardadoExitosoEvent, value);
            }
        }

        public event RoutedEventHandler EliminacionExitosa
        {
            add
            {
                AddHandler(EliminacionExitosaEvent, value);
            }

            remove
            {
                RemoveHandler(EliminacionExitosaEvent, value);
            }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add
            {
                AddHandler(EdicionCanceladaEvent, value);
            }

            remove
            {
                RemoveHandler(EdicionCanceladaEvent, value);
            }
        }

        public EdicionParametrosSistemaUC()
        {
            InitializeComponent();


            idSistemaSOT = typeof(SOTWpf.App).ObtenerGuidEnsamblado() ?? new Guid();

            idSistemaSincronizacion = typeof(SegadorPagosWindowsService.SegadorPagosWindowsService).ObtenerGuidEnsamblado() ?? new Guid();

            idSistemaGeneradorFolios = typeof(GeneracionFoliosWpf.App).ObtenerGuidEnsamblado() ?? new Guid();
        }
        private void cargarImagotipo_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Archivos de imagen(*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG";

            if (dialog.ShowDialog() == true)
            {
                ParametrosSOTConfig.Imagotipo = dialog.FileName;
            }
        }

        private void cargarImagenReportes_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Archivos de imagen(*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG";

            if (dialog.ShowDialog() == true)
            {
                ParametrosSOTConfig.ImagenReportes = dialog.FileName;
            }
        }
        public void Guardar()
        {
            ValidarParametros();

            var parametros = new List<ParametroSistema>();

            #region SOT

            foreach (var propiedad in typeof(ParametrosSOT).GetProperties())
            {
                var valor = propiedad.GetValue(ParametrosSOTConfig);

                if (valor == null)
                    continue;

                parametros.Add(new ParametroSistema
                {
                    GuidSistema = idSistemaSOT,
                    Nombre = propiedad.Name,
                    Valor = Newtonsoft.Json.JsonConvert.SerializeObject(valor),
                });

            }

            var controlador = new SOTControladores.Controladores.ControladorConfiguracionSistemas();
            controlador.GuardarParametrosSistema(parametros);

            #endregion
            #region Sincronización

            parametros.Clear();

            foreach (var propiedad in typeof(ParametrosSincronizacion).GetProperties())
            {
                var valor = propiedad.GetValue(ParametrosSincronizacionConfig);

                if (valor == null)
                    continue;

                parametros.Add(new ParametroSistema
                {
                    GuidSistema = idSistemaSincronizacion,
                    Nombre = propiedad.Name,
                    Valor = Newtonsoft.Json.JsonConvert.SerializeObject(valor)
                });
            }

            controlador.GuardarParametrosSistema(parametros);

            #endregion
            #region Generador de folios

            parametros.Clear();

            foreach (var propiedad in typeof(ParametrosGeneradorFolios).GetProperties())
            {
                var valor = propiedad.GetValue(ParametrosGeneradorFoliosConfig);

                if (valor == null)
                    continue;

                parametros.Add(new ParametroSistema
                {
                    GuidSistema = idSistemaGeneradorFolios,
                    Nombre = propiedad.Name,
                    Valor = Newtonsoft.Json.JsonConvert.SerializeObject(valor)
                });
            }

            controlador.GuardarParametrosSistema(parametros);

            #endregion

            Utilidades.Dialogos.Aviso("Parámetros guardados de forma exitosa");

            CargarParametros();

            RaiseEvent(new RoutedEventArgs(GuardadoExitosoEvent));
        }

        private void ValidarParametros()
        {
            #region SOT

            if (ParametrosSOTConfig == null)
                throw new SOTException(Textos.ErroresSistemas.parametros_sot_nulos_exception);

            if (!File.Exists(ParametrosSOTConfig.Imagotipo))
                throw new SOTException(Textos.Errores.archivo_inexistente_exception, ParametrosSOTConfig.Imagotipo);

            if (!File.Exists(ParametrosSOTConfig.ImagenReportes))
                throw new SOTException(Textos.Errores.archivo_inexistente_exception, ParametrosSOTConfig.ImagenReportes);

            #endregion
            #region Sincronización

            if (ParametrosSincronizacionConfig == null)
                throw new SOTException(Textos.ErroresSistemas.parametros_sincronizacion_nulos_exception);

            try
            {
                UtilidadesUrl.ValidarUrl(ParametrosSincronizacionConfig.UrlWebServiceRastrilleo, System.Net.WebRequestMethods.Http.Get);
            }
            catch (Exception ex)
            {
                throw new SOTException("Error al validar la url {0}: {1}", ParametrosSincronizacionConfig.UrlWebServiceCorte, ex.Message);
            }

            try
            {
                UtilidadesUrl.ValidarUrl(ParametrosSincronizacionConfig.UrlWebServiceCorte, System.Net.WebRequestMethods.Http.Get);
            }
            catch (Exception ex)
            {
                throw new SOTException("Error al validar la url {0}: {1}", ParametrosSincronizacionConfig.UrlWebServiceCorte, ex.Message);
            }

            if (string.IsNullOrWhiteSpace(ParametrosSincronizacionConfig.Servidor))
                throw new SOTException(Textos.ErroresSistemas.servidor_sincronizador_invalido_excepcion);

            if (ParametrosSincronizacionConfig.Puerto <= 0)
                throw new SOTException(Textos.ErroresSistemas.puerto_sincronizador_invalido_excepcion);

            #endregion
            #region Generador de folios

            if (ParametrosGeneradorFoliosConfig == null)
                throw new SOTException(Textos.ErroresSistemas.parametros_generador_folios_nulos_exception);

            if (string.IsNullOrWhiteSpace(ParametrosGeneradorFoliosConfig.Servidor))
                throw new SOTException(Textos.ErroresSistemas.servidor_generador_folios_invalido_excepcion);

            if (ParametrosGeneradorFoliosConfig.Puerto <= 0)
                throw new SOTException(Textos.ErroresSistemas.puerto_generador_folios_invalido_excepcion);

            #endregion
        }

        public void Cancelar()
        {
            CargarParametros();

            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        public void CargarParametros()
        {


            var controladorConfiguracionSistemas = new SOTControladores.Controladores.ControladorConfiguracionSistemas();

            #region SOT

            var parametros = controladorConfiguracionSistemas.ObtenerParametros(idSistemaSOT);

            var configSOT = new ParametrosSOT();

            foreach (var propiedad in typeof(ParametrosSOT).GetProperties())
            {
                var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                if (parametro != null)
                {
                    propiedad.SetValue(configSOT, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                }
            }

            ParametrosSOTConfig = configSOT;

            #endregion
            #region Sincronización

            parametros = controladorConfiguracionSistemas.ObtenerParametros(idSistemaSincronizacion);

            var configSincronizacion = new ParametrosSincronizacion();

            foreach (var propiedad in typeof(ParametrosSincronizacion).GetProperties())
            {
                var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                if (parametro != null)
                {
                    propiedad.SetValue(configSincronizacion, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                }
            }

            ParametrosSincronizacionConfig = configSincronizacion;

            #endregion
            #region Generador de folios

            parametros = controladorConfiguracionSistemas.ObtenerParametros(idSistemaGeneradorFolios);

            var configGeneradorFolios = new ParametrosGeneradorFolios();

            foreach (var propiedad in typeof(ParametrosGeneradorFolios).GetProperties())
            {
                var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                if (parametro != null)
                {
                    propiedad.SetValue(configGeneradorFolios, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                }
            }

            ParametrosGeneradorFoliosConfig = configGeneradorFolios;

            #endregion
        }

        

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarParametros();
            }
        }
    }
}
