﻿using Modelo.Almacen.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Lineas
{
    /// <summary>
    /// Lógica de interacción para EdicionLineaForm.xaml
    /// </summary>
    public partial class EdicionLineaForm : Window
    {
        Linea L
        {
            get { return DataContext as Linea; }
            set { DataContext = value; }
        }

        public EdicionLineaForm(Linea linea = null)
        {
            InitializeComponent();

            L = linea ?? new Linea();
        }

        private void BotoneraBase_AceptarClick(object sender, RoutedEventArgs e)
        {
            if (L != null)
            {
                if (L.Cod_Linea == 0)
                {
                    var controlador = new SOTControladores.Controladores.ControladorLineas();

                    controlador.CrearLinea(L.ObtenerCopiaDePrimitivas());

                    Utilidades.Dialogos.Aviso(Textos.Mensajes.creacion_elemento_exitosa);
                }
                else
                {
                    var controlador = new SOTControladores.Controladores.ControladorLineas();

                    var copia = L.ObtenerCopiaDePrimitivas();
                    copia.Cod_Linea = L.Cod_Linea;

                    controlador.ModificarLinea(copia);
                    
                    Utilidades.Dialogos.Aviso(Textos.Mensajes.modificacion_elemento_exitosa);
                }
            }

            Close();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorDepartamentos = new ControladorDepartamentos();
                cbDepartamentos.ItemsSource = controladorDepartamentos.ObtenerDepartamentosParaFiltro();
            }
        }
    }
}
