﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.Lineas
{
    /// <summary>
    /// Lógica de interacción para GestionLineasForm.xaml
    /// </summary>
    public partial class GestionLineasForm : Window
    {
        ObservableCollection<Linea> items = new ObservableCollection<Linea>();

        public GestionLineasForm()
        {
            InitializeComponent();

            ((CollectionViewSource)this.Resources["lineasCVS"]).Source = items;
        }

        private void btnEliminarSubcategoria_Click(object sender, RoutedEventArgs e)
        {
            var linea = dgvLineas.SelectedItem as Linea;

            if (linea == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (linea.Editable != true)
                throw new SOTException(Textos.Errores.elemento_no_editable_excepcion);

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                var controlador = new SOTControladores.Controladores.ControladorLineas();

                controlador.EliminarLinea(linea.Cod_Linea);

                Utilidades.Dialogos.Aviso(Textos.Mensajes.eliminacion_elemento_exitosa);

                Recargar();
            }
        }

        private void btnDesactivarSubcategoria_Click(object sender, RoutedEventArgs e)
        {
            var linea = dgvLineas.SelectedItem as Linea;

            if (linea == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (linea.Editable != true)
                throw new SOTException(Textos.Errores.elemento_no_editable_excepcion);

            if (Utilidades.Dialogos.Pregunta(Textos.Mensajes.confirmar_desactivacion_elemento) == MessageBoxResult.Yes)
            {
                var controlador = new SOTControladores.Controladores.ControladorLineas();

                controlador.DesactivarLinea(linea.Cod_Linea);

                Utilidades.Dialogos.Aviso(Textos.Mensajes.desactivacion_elemento_exitosa);

                Recargar();
            }
        }

        private void btnModificarSubcategoria_Click(object sender, RoutedEventArgs e)
        {
            var linea = dgvLineas.SelectedItem as Linea;

            if (linea == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (linea.Editable != true)
                throw new SOTException(Textos.Errores.elemento_no_editable_excepcion);

            Utilidades.Dialogos.MostrarDialogos(this, new EdicionLineaForm(linea));

            Recargar();
        }

        private void btnAltaSubcategoria_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EdicionLineaForm());

            Recargar();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                Recargar();
            }
        }

        private void Recargar()
        {
            var controlador = new SOTControladores.Controladores.ControladorLineas();

            items.Clear();

            foreach (var item in controlador.ObtenerLineas(true))
                items.Add(item);
        }

        private void lineasCVS_filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as Linea;

            e.Accepted = item != null && (chbSoloActivas.IsChecked != true || item.Activa) && item.Desc_Linea.ToUpper().Contains(txtNombre.Text.Trim().ToUpper());
        }

        private void txtNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(dgvLineas.ItemsSource).Refresh();
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(dgvLineas.ItemsSource).Refresh();
        }
    }
}
