﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SOTWpf.Utilidades
{
    public class Dialogos
    {
        internal static void MostrarDialogos(Window hijo) 
        {
            hijo.ShowDialog();
        }
        public static void MostrarDialogos(Window padre, Window hijo) 
        {
            hijo.Owner = padre;

#warning fix temporal

            MaterialDesignThemes.Wpf.ShadowAssist.SetCacheMode(hijo, null);

            //if (hijo.WindowStyle == WindowStyle.SingleBorderWindow)
            //    hijo.WindowState = WindowState.Maximized;

            hijo.ShowDialog();
        }

        public static void MostrarDialogos(Window padre, System.Windows.Forms.Form hijo)
        {
            //hijo.Owner = padre;
            hijo.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            if (hijo.Visible)
                hijo.Hide();
            var respuesta = hijo.ShowDialog();
        }

        public static MessageBoxResult Pregunta(string texto, bool aceptaCancelacion = false)
        {
            return MessageBox.Show(texto, Textos.TitulosVentanas.Aviso, aceptaCancelacion ? MessageBoxButton.YesNoCancel : MessageBoxButton.YesNo, MessageBoxImage.Question);
        }

        public static MessageBoxResult Aviso(string texto)
        {
            return MessageBox.Show(texto, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static MessageBoxResult Error(string texto)
        {
            return MessageBox.Show(texto, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
