﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Utilidades
{
    internal interface ISnackContainer
    {
        void MostrarError(Exception exception);
        bool IsActive { get; }
    }
}
