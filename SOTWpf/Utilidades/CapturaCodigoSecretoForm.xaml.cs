﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Utilidades
{
    /// <summary>
    /// Lógica de interacción para CapturaCodigoSecretoForm.xaml
    /// </summary>
    public partial class CapturaCodigoSecretoForm : Window
    {
        public CapturaCodigoSecretoForm()
        {
            InitializeComponent();
        }

        bool cerrar = false;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Codigo = psw.Password;
            cerrar = true;
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Codigo = null;
            cerrar = true;
            Close();
        }

        public string Codigo { get; private set; }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !cerrar;
            base.OnClosing(e);
        }
    }
}
