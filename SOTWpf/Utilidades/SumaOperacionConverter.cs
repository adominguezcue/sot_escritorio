﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SOTWpf.Utilidades
{
    public class SumaOperacionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (!values.Any())
                return 0;

            //if (!values.Any(m => m != null))
            //    return null;

            decimal value = 0;

            foreach (var item in values.Select(m => System.Convert.ToDecimal(m)))
                value = decimal.Add(value, item);

            return value;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
