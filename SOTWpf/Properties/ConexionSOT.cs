﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Properties
{
    public partial class ConexionSOT : global::System.Configuration.ApplicationSettingsBase
    {
        public ConexionSOT()
        {
            Upgrade();
        }

        public override void Upgrade()
        {
            try
            {
                if (!EsConfiguradoSOT)
                {
                    base.Upgrade();
                }
            }
            catch (Exception ex)
            {
                Transversal.Log.Logger.Error("[ConexionSOT]: " + ex.Message);
            }
        }
    }
}
