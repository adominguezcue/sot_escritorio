﻿using AutoUpdaterDotNET;
using Newtonsoft.Json;
using SOTWpf.Utilidades;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Threading;

namespace SOTWpf.Actualizaciones
{
    public class GestorActualizaciones
    {
        internal delegate void UpdateDelegate();

        public static GestorActualizaciones Instancia { get; } = new GestorActualizaciones();

        Timer timer;

        internal Version InstalledVersion { get; set; }

        private GestorActualizaciones() 
        {
            InstalledVersion = Assembly.GetExecutingAssembly().GetName().Version;

            AutoUpdater.ParseUpdateInfoEvent += AutoUpdaterOnParseUpdateInfoEvent;
            AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;
            AutoUpdater.ReportErrors = false;
        }

        private void AutoUpdaterOnCheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            var extraArgs = args as UpdateInfoExtendedEventArgs;

            if (extraArgs != null && (extraArgs.IsUpdateAvailable || extraArgs.ResourceMode))
            {
                if (extraArgs.ResourceMode)
                {
                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new ActualizacionExtrasForm(extraArgs.ExtraUpdates.First()));
                }
                else if (extraArgs.IsUpdateAvailable)
                {
                    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new ActualizacionForm(extraArgs));
                }
            }
        }

        private void AutoUpdaterOnParseUpdateInfoEvent(ParseUpdateInfoEventArgs args)
        {
            var ext = JsonConvert.DeserializeObject<UpdateInfoExtendedEventArgs>(args.RemoteData);
            args.UpdateInfo = ext;

            if (InstalledVersion >= args.UpdateInfo.CurrentVersion)
            {
                try
                {
                    if (ext.Extras != null && ext.Extras.Reportes != null)
                    {
                        var rutaReportes = "C:\\reportes\\";//new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().RutaReportes;

                        if (!string.IsNullOrWhiteSpace(rutaReportes))
                        {
                            rutaReportes = rutaReportes.Trim();

                            if (!rutaReportes.EndsWith("\\"))
                                rutaReportes += "\\";

                            var version = ext.Extras.Reportes.Version - 1;

                            if (System.IO.File.Exists(rutaReportes + Constantes.ARCHIVO_VERSION_REPORTE))
                            {
                                using (var file = System.IO.File.OpenText(rutaReportes + Constantes.ARCHIVO_VERSION_REPORTE))
                                {
                                    if (!int.TryParse(file.ReadToEnd(), out version))
                                        throw new Exception("Erro al leer la versión");
                                }
                            }

                            if (ext.Extras.Reportes.Version > version)
                            {
                                if (ext.ExtraUpdates == null)
                                    ext.ExtraUpdates = new List<ExtraUpdate>();

                                ext.Extras.Reportes.Name = "Reportes";
                                ext.Extras.Reportes.LocalPath = rutaReportes;
                                ext.Extras.Reportes.InstalledVersion = version;
                                ext.ExtraUpdates.Add(ext.Extras.Reportes);

                                args.UpdateInfo.CurrentVersion = new Version("0.0.0." + ext.Extras.Reportes.Version.ToString());
                                args.UpdateInfo.InstalledVersion = new Version("0.0.0." + ext.Extras.Reportes.InstalledVersion.ToString());
                                args.UpdateInfo.DownloadURL = ext.Extras.Reportes.DownloadURL;
                                args.UpdateInfo.InstallerArgs = "\"%source%\" \"" + ext.Extras.Reportes.LocalPath + "Launcher.exe\" \"" + Process.GetCurrentProcess().MainModule.FileName + "\"";

                                ext.ResourceMode = true;
                            }
                        }
                    }
                }
                catch
                {
                }
            }
        }

        internal void Cargar()
        {
            timer = new Timer { Interval = 1000 };
            timer.Elapsed += timer_Tick;
            timer.Start();
        }

        internal void Apagar()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Elapsed -= timer_Tick;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
#if DEBUG
#warning código de pruebas que reduce el tiempo entre avisos de nueva versión
            if (timer.Interval < 60000)
                timer.Interval = 60000;
#else
            if (timer.Interval < 300000)
                timer.Interval = 300000;
#endif
            Actualizar();
        }

        private void Actualizar()
        {
            string ruta = null;

            try
            {

                //ruta = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal().RutaActualizacionesJSON;
#if DEBUG
                ruta = "http://www.gmsweb.com.mx/sot_test/ActualizacionSOT.json";
#elif QA
                ruta = "http://www.gmsweb.com.mx/sot_test/ActualizacionSOT.json";
#else
                ruta = "http://www.gmsweb.com.mx/sot/ActualizacionSOT.json";
#endif
            }
            catch (Exception ex)
            {
                Transversal.Log.Logger.Error("[UPDATE]: " + ex.Message);

                if (ex.InnerException != null)
                    Transversal.Log.Logger.Error("[UPDATE]: " + ex.InnerException.Message);
            }
            Application.Current.Dispatcher.BeginInvoke(new UpdateDelegate(() =>
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(ruta))
                        AutoUpdater.Start(ruta);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Error("[UPDATE]: " + ex.Message);

                    if (ex.InnerException != null)
                        Transversal.Log.Logger.Error("[UPDATE]: " + ex.InnerException.Message);
                }
            }));
        }
    }
}
