﻿using AutoUpdaterDotNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Actualizaciones
{
    /// <summary>
    /// Lógica de interacción para ActualizacionUC.xaml
    /// </summary>
    public partial class ActualizacionUC : UserControl
    {
        private static readonly RoutedEvent FinalizarEvent =
            EventManager.RegisterRoutedEvent("Finalizar", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(ActualizacionUC));


        public event RoutedEventHandler Finalizar
        {
            add { AddHandler(FinalizarEvent, value); }
            remove { RemoveHandler(FinalizarEvent, value); }
        }

        private bool HideReleaseNotes { get; set; }

        UpdateInfoEventArgs _args = null;
        internal UpdateInfoEventArgs Args 
        {
            get { return _args; }
            set 
            {
                _args = value;

                if (_args != null)
                {
                    labelUpdate.Text = string.Format(labelUpdate.Text, AutoUpdater.AppTitle);
                    labelDescription.Text = string.Format(labelDescription.Text, AutoUpdater.AppTitle, Args.CurrentVersion, Args.InstalledVersion);
                    if (string.IsNullOrEmpty(Args.ChangelogURL))
                    {
                        HideReleaseNotes = true;
                        labelReleaseNotes.Visibility = System.Windows.Visibility.Collapsed;
                        wbDetallesCambios.Visibility = System.Windows.Visibility.Collapsed;
                    }

                    if (!HideReleaseNotes)
                    {
                        wbDetallesCambios.Navigate(Args.ChangelogURL);
                    }
                }
            }
        }



        public ActualizacionUC()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (AutoUpdater.OpenDownloadPage)
                {
                    //var processStartInfo = new ProcessStartInfo(uArgs.DownloadURL);

                    //Process.Start(processStartInfo);

                    //DialogResult = DialogResult.OK;

                    RaiseEvent(new RoutedEventArgs(FinalizarEvent));
                }
                else if (AutoUpdater.DownloadUpdate())
                {
                    Application.Current.Shutdown();
                    App.Actualizacion = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.GetType().ToString(), MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void BtnPosponer_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(FinalizarEvent));
        }
    }
}
