﻿using AutoUpdaterDotNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Actualizaciones
{
    public class UpdateInfoExtendedEventArgs : UpdateInfoEventArgs
    {
        public Extras Extras { get; set; }

        public List<ExtraUpdate> ExtraUpdates { get; set; }

        public bool ResourceMode { get; set; }
    }
}
