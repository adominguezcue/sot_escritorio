﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SOTWpf.Actualizaciones
{
    /// <summary>
    /// Lógica de interacción para ActualizacionExtrasForm.xaml
    /// </summary>
    public partial class ActualizacionExtrasForm : Window
    {
        public ActualizacionExtrasForm(ExtraUpdate args)
        {
            InitializeComponent();

            actUC.Args = args;
        }

        private void ActualizacionUC_Finalizar(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
