﻿using AutoUpdaterDotNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.Actualizaciones
{
    /// <summary>
    /// Lógica de interacción para ActualizacionExtrasUC.xaml
    /// </summary>
    public partial class ActualizacionExtrasUC : UserControl
    {
        private static readonly RoutedEvent FinalizarEvent =
            EventManager.RegisterRoutedEvent("Finalizar", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(ActualizacionExtrasUC));


        public event RoutedEventHandler Finalizar
        {
            add { AddHandler(FinalizarEvent, value); }
            remove { RemoveHandler(FinalizarEvent, value); }
        }

        ExtraUpdate _args = null;
        internal ExtraUpdate Args
        {
            get { return _args; }
            set
            {
                _args = value;

                if (_args != null)
                {
                    labelUpdate.Text = string.Format(labelUpdate.Text, Args.Name);
                    labelDescription.Text = string.Format(labelDescription.Text, Args.Name, Args.Version, Args.InstalledVersion);
                }
            }
        }

        public ActualizacionExtrasUC()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (AutoUpdater.OpenDownloadPage)
                {
                    RaiseEvent(new RoutedEventArgs(FinalizarEvent));
                }
                else if (AutoUpdater.DownloadUpdate())
                {
                    Application.Current.Shutdown();
                    App.Actualizacion = true;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, exception.GetType().ToString(), MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void BtnPosponer_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(FinalizarEvent));
        }
    }
}
