﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Actualizaciones
{
    public class ExtraUpdate
    {
        public int Version { get; set; }
        public string DownloadURL { get; set; }
        internal string LocalPath { get; set; }
        internal string Name { get; set; }

        public int InstalledVersion { get; set; }
    }
}