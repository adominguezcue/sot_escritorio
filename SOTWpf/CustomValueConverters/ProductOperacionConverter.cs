﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SOTWpf.CustomValueConverters
{
    public class ProductOperacionConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (!values.Any())
                return 0;

            //if (!values.Any(m => m != null))
            //    return null;

            decimal value = 1;

            foreach (var item in values.Select(m =>
            {
                try
                {
                    return System.Convert.ToDecimal(m);
                }
                catch 
                {
                    return 0;
                }
            }))
                value = decimal.Multiply(value, item);

            return value;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
