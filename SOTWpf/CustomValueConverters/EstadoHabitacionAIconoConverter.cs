﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SOTWpf.CustomValueConverters
{
    [ValueConversion(typeof(Modelo.Entidades.Habitacion.EstadosHabitacion), typeof(ImageSource))]
    internal class EstadoHabitacionAIconoConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(ImageSource))
                throw new InvalidOperationException("El tipo del destino debe ser ImageSource");

            return Procesar((Modelo.Entidades.Habitacion.EstadosHabitacion)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(ImageSource))
                throw new InvalidOperationException("El tipo del destino debe ser ImageSource");

            return Procesar((Modelo.Entidades.Habitacion.EstadosHabitacion)value);
        }

        private ImageSource Procesar(Modelo.Entidades.Habitacion.EstadosHabitacion estado)
        {
            switch (estado)
            {
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Libre:
                        return Recursos.ImagenesEstados.estado_icono_habilitada;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.PendienteCobro:
                        return Recursos.ImagenesEstados.estado_icono_cobrar;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Preparada:
                        return Recursos.ImagenesEstados.estado_icono_preparada;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Sucia:
                        return Recursos.ImagenesEstados.estado_icono_sucia;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Ocupada:
                        return Recursos.ImagenesEstados.estado_icono_ocupada;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Reservada:
                        return Recursos.ImagenesEstados.estado_icono_reservada;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.PreparadaReservada:
                        return Recursos.ImagenesEstados.estado_icono_preparada;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Supervision:
                        return Recursos.ImagenesEstados.estado_icono_revision;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Limpieza:
                        return Recursos.ImagenesEstados.estado_icono_limpieza;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Mantenimiento:
                        return Recursos.ImagenesEstados.estado_icono_mantenimiento;
                case Modelo.Entidades.Habitacion.EstadosHabitacion.Bloqueada:
                        return Recursos.ImagenesEstados.estado_icono_bloqueada;
                default:
                        return Recursos.Generales.default_imagen;
            }
        }
    }
}
