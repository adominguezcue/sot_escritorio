﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace SOTWpf.CustomValueConverters
{
    [ValueConversion(typeof(int?), typeof(int?))]
    public class DataGridRowIndexConverter : IValueConverter
    {
        public int IndiceInicial { get; set; }

        public object Convert(object value, Type TargetType, object parameter, CultureInfo culture)
        {
            DataGridRow item = (DataGridRow)value;
            //DataGrid listView = DataGridRow.contain(item) as DataGrid;
            int index = item.GetIndex();// listView.ItemContainerGenerator.IndexFromContainer(item);
            return IndiceInicial + index;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
