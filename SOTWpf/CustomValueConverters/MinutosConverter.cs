﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SOTWpf.CustomValueConverters
{
    [ValueConversion(typeof(int?), typeof(DateTime?))]
    public class MinutosConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var minutos = value as int?;

            if (!minutos.HasValue)
                return null;

            return DateTime.Now.Date.AddMinutes(minutos.Value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var fecha = value as DateTime?;

            if (!fecha.HasValue)
                return null;

            return (int)fecha.Value.TimeOfDay.TotalMinutes;
        }
    }
}
