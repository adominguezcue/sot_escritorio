﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SOTWpf.CustomValueConverters
{
    internal class IntegerWithEmptySupportValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int val = 0;

            if (int.TryParse((value ?? "").ToString(), out val))
                return val;

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int val = 0;

            if (int.TryParse((value ?? "").ToString(), out val))
                return val;

            return 0;
        }
    }
}
