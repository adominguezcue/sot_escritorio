﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SOTWpf.CustomValueConverters
{
    [ValueConversion(typeof(decimal?), typeof(decimal?))]
    public class MultiplicadorConverter : IValueConverter
    {
        public decimal Multiplicador { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valor = value as decimal?;

            if (valor == null)
                return null;


            return valor * Multiplicador;
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valor = value as decimal?;

            if (valor == null)
                return null;

            try
            {
                valor /= Multiplicador;
            }
            catch (DivideByZeroException)
            {
                throw new DivideByZeroException("Error en DivisorConverter, Divisor es cero");
            }

            return valor;
        }
    }
}
