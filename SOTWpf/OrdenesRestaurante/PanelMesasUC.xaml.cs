﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.OrdenesRestaurante
{
    /// <summary>
    /// Lógica de interacción para PanelMesasUC.xaml
    /// </summary>
    public partial class PanelMesasUC : UserControl
    {
        private readonly object bloqueador = new object();
        private bool recargando = false;
        private System.Timers.Timer timerRecarga;
        public UCMesa MesaSeleccionada;

        List<UCMesa> uc_mesas;

        public static readonly RoutedEvent PrevioRecargarEvent = EventManager.RegisterRoutedEvent("PrevioRecargar", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PanelMesasUC));
        public static readonly RoutedEvent RecargarEvent = EventManager.RegisterRoutedEvent("Recargar", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PanelMesasUC));

        public event RoutedEventHandler Recargar
        {
            add
            {
                AddHandler(RecargarEvent, value);
            }

            remove
            {
                RemoveHandler(RecargarEvent, value);
            }
        }

        public event RoutedEventHandler PrevioRecargar
        {
            add
            {
                AddHandler(PrevioRecargarEvent, value);
            }

            remove
            {
                RemoveHandler(PrevioRecargarEvent, value);
            }
        }

        internal bool TimerHabilitado
        {
            get { return timerRecarga?.Enabled ?? false; }
            set
            {
                if (timerRecarga != null && timerRecarga.Enabled != value)
                    timerRecarga.Enabled = value;
            }
        }

        public PanelMesasUC()
        {
            InitializeComponent();

            uc_mesas = new List<UCMesa>();
        }

        private void PanelMesas_Loaded(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
                recargando = false;

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                ActualizarMesas();

                try
                {
                    if (timerRecarga != null)
                    {
                        timerRecarga.Stop();
                        timerRecarga.Elapsed -= TimerRecarga_Tick;
                    }
                }
                catch
                {
                }

                timerRecarga = new System.Timers.Timer
                {
                    Interval = TimeSpan.FromSeconds(15).TotalMilliseconds
                };
                timerRecarga.Elapsed += TimerRecarga_Tick;

                timerRecarga.Start();
            }
        }

        private void TimerRecarga_Tick(object sender, EventArgs e)
        {
            ActualizarMesas();
        }

        internal void LimpiarMesas()
        {
            foreach (UCMesa uc_mesa in uc_mesas)
            {
                uc_mesa.Click -= mesa_Click;
                uc_mesa.Recargar -= CargarMesa;

                //try
                //{
                //    uc_mesa.RecargaAutomatica = false;
                //}
                //catch
                //{
                //}
            }

            uc_mesas.Clear();

        }

        public void ActualizarMesas()
        {
            lock (bloqueador)
            {
                if (recargando)
                    return;

                recargando = true;
            }

            List<Mesa> mesas;

            try
            {
                var controlador = new ControladorRestaurantes();

                mesas = controlador.ObtenerMesas();
            }
            catch (Exception ex)
            {
                lock (bloqueador)
                    recargando = false;

                Transversal.Log.Logger.Error(ex.Message);
                return;
            }
            //finally
            //{
                
            //}

            Dispatcher.BeginInvoke((ThreadStart)delegate
            {
                try
                {
                    panelMesas.Children.Clear();
                    panelMesas.RowDefinitions.Clear();
                    panelMesas.ColumnDefinitions.Clear();

                    foreach (UCMesa uc_mesa in uc_mesas.Where(m => !mesas.Any(a => a.Id == m.MesaInterna.Id)))
                    {
                        uc_mesa.Click -= mesa_Click;
                        uc_mesa.Recargar -= CargarMesa;
                        //uc_habitacion.Dispose();
                    }

                    uc_mesas.RemoveAll(m => !mesas.Any(a => a.Id == m.MesaInterna.Id));

                    if (!uc_mesas.Contains(MesaSeleccionada))
                        MesaSeleccionada = null;

                    if (mesas.Count > 0)
                    {
                        for (int i = 1; i <= mesas.Max(m => m.Columna); i++)
                        {
                            panelMesas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(98) });
                        }

                        var pisoMaximo = mesas.Max(m => m.Fila);

                        for (int i = 1; i <= pisoMaximo; i++)
                        {
                            panelMesas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(76) });
                        }

                        foreach (Mesa mesa in mesas)
                        {
                            var existente = uc_mesas.FirstOrDefault(m => m.MesaInterna.Id == mesa.Id);

                            var nh = existente ??
                                     new UCMesa(mesa);

                            if (existente == null)
                            {
                                uc_mesas.Add(nh);
                                nh.Margin = new Thickness(1, 2, 1, 2);

                                uc_mesas.Last().Click += mesa_Click;
                                uc_mesas.Last().Recargar += CargarMesa;
                            }
                            else
                                nh.RecargarMesa(mesa);

                            Grid.SetColumn(nh, mesa.Columna - 1);
                            Grid.SetRow(nh, pisoMaximo - mesa.Fila);


                            panelMesas.Children.Add(nh);//, habitacion.Columna - 1, pisoMaximo - habitacion.Fila);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Error(ex.Message);
                }
                finally
                {
                    lock (bloqueador)
                        recargando = false;
                }
            });
        }

        private void mesa_Click(object sender, EventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(PrevioRecargarEvent, sender as UCMesa));

            MesaSeleccionada = sender as UCMesa;

            CargarMesa(sender, e);
        }


        private void CargarMesa(object sender, EventArgs e)
        {

            if ((sender == null && MesaSeleccionada == null) || (sender != null && sender.Equals(MesaSeleccionada)))
            {
                RaiseEvent(new RoutedEventArgs(RecargarEvent));
            }
        }

        private void PanelMesas_Unloaded(object sender, RoutedEventArgs e)
        {
            lock (bloqueador)
                recargando = true;

            //lock (bloqueador)
            //{
            //    while (recargando)
            //        Thread.Sleep(1000);

            //    recargando = true;
            //}

            try
            {
                if (timerRecarga != null)
                {
                    timerRecarga.Stop();
                    timerRecarga.Elapsed -= TimerRecarga_Tick;
                }

                LimpiarMesas();
            }
            catch
            {
            }

            
        }
    }
}
