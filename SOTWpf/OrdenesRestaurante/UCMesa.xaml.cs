﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.OrdenesRestaurante
{
    /// <summary>
    /// Lógica de interacción para UCHabitacion.xaml
    /// </summary>
    public partial class UCMesa: UserControl
    {
        //private DispatcherTimer timerRecarga;
        private LinearGradientBrush fb;

        private ImageSource ImagenE
        {
            get { return imagenEstado.Source; }
            set { imagenEstado.Source = value; }
        }

        public UCMesa(Mesa m)
            : base()
        {
            InitializeComponent();

            //Style style = this.FindResource("UCHabitacionEstilo") as Style;
            //Style = style;

            CustomColor = Color.FromArgb(255, 139, 139, 139);
            btn.DataContext = this;

            fb = fondo.Background as LinearGradientBrush;

            CargarInformacion(m);
        }

        Mesa mesa;
        public event RecargarEventHandler Recargar;
        public delegate void RecargarEventHandler(object sender, EventArgs e);


        public Mesa MesaInterna
        {
            get { return mesa; }
        }

        //[Editor()]
        //public bool RecargaAutomatica
        //{
        //    get { return timerRecarga.IsEnabled; }
        //    set
        //    {
        //        if (timerRecarga.IsEnabled == value)
        //            return;

        //        if (value)
        //            timerRecarga.Start();
        //        else
        //            timerRecarga.Stop();
        //    }
        //}

        //[Editor()]
        //public TimeSpan IntervaloRecarga
        //{
        //    get { return timerRecarga.Interval; }
        //    set { timerRecarga.Interval = value; }
        //}

        public static readonly DependencyProperty CustomColorProperty =
             DependencyProperty.Register("CustomColor", typeof(Color),
             typeof(UCMesa), new FrameworkPropertyMetadata(Color.FromArgb(255, 139, 139, 139)));

        public static readonly DependencyProperty CustomColor2Property =
             DependencyProperty.Register("CustomColor2", typeof(Color),
             typeof(UCMesa), new FrameworkPropertyMetadata(Color.FromArgb(0, 139, 139, 139)));

        public Color CustomColor
        {
            get { return (Color)GetValue(CustomColorProperty); }
            set
            {
                SetValue(CustomColorProperty, value);
                CustomColor2 = Color.FromArgb(0, value.R, value.G, value.B);
            }
        }

        public Color CustomColor2
        {
            get { return (Color)GetValue(CustomColor2Property); }
            set { SetValue(CustomColor2Property, value); }
        }



        private void timerRecarga_Tick(object sender, EventArgs e)
        {
            RecargarMesa(null);
        }

        public static readonly RoutedEvent VencimientoActivadoEvent = EventManager.RegisterRoutedEvent("VencimientoActivado", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UCMesa));
        public static readonly RoutedEvent VencimientoDesactivadoEvent = EventManager.RegisterRoutedEvent("VencimientoDesactivado", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UCMesa));

        public event RoutedEventHandler VencimientoActivado
        {
            add
            {
                this.AddHandler(VencimientoActivadoEvent, value);
            }

            remove
            {
                this.RemoveHandler(VencimientoActivadoEvent, value);
            }
        }

        public event RoutedEventHandler VencimientoDesactivado
        {
            add
            {
                this.AddHandler(VencimientoDesactivadoEvent, value);
            }

            remove
            {
                this.RemoveHandler(VencimientoDesactivadoEvent, value);
            }
        }

        private void CargarInformacion(Mesa m)
        {
            txtNumero.Text = m.Clave;
            //txtTipo.Text = h.TipoHabitacion.Descripcion;
            txtDetalle.Text = string.Empty;

            switch (m.Estado)
            {
                case Mesa.EstadosMesa.EnCreacion:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_habilitada;
                        //CustomColor = (Color)ColorConverter.ConvertFromString("#38d731");

                        //TimeSpan tiempo = (DateTime.Now - m.FechaModificacion);
                        txtDetalle.Text = m.Estado.Descripcion();
                        //timerRecarga.Start();
                    }
                    break;
                case Mesa.EstadosMesa.Libre:
                    {
                        ImagenE = Recursos.ImagenesEstados.estado_icono_habilitada;
                        CustomColor = (Color)ColorConverter.ConvertFromString("#38d731");

                        //TimeSpan tiempo = (DateTime.Now - m.FechaModificacion);
                        txtDetalle.Text = m.Estado.Descripcion();
                        //timerRecarga.Start();
                    }
                    break;
                case Mesa.EstadosMesa.Ocupada:
                    {

                        CustomColor = (Color)ColorConverter.ConvertFromString("#ea927e");
                        ImagenE = Recursos.ImagenesEstados.estado_icono_comanda;

                        var controlador = new ControladorRestaurantes();


                        var ultimaOcupacion = controlador.ObtenerOcupacionActualPorMesa(m.Id);

                        if (ultimaOcupacion != null)
                            txtDetalle.Text = ultimaOcupacion.Mesero.NombreCompleto;
                        

                        //timerRecarga.Start();
                    }
                    break;
                case Mesa.EstadosMesa.Cobrar:
                    {
                        CustomColor = (Color)ColorConverter.ConvertFromString("#f2d017");
                        ImagenE = Recursos.ImagenesEstados.estado_icono_cobrar;

                        var controlador = new ControladorRestaurantes();


                        var ultimaOcupacion = controlador.ObtenerOcupacionActualPorMesa(m.Id);

                        if (ultimaOcupacion != null)
                            txtDetalle.Text = ultimaOcupacion.Mesero.NombreCompleto;
                        
                    }
                    break;
            }

            this.mesa = m;
        }


        public void RecargarMesa(Mesa mesaRecargar = null)
        {

            if (mesa != null)
            {
                try
                {
                    var controlador = new ControladorRestaurantes();

                    var m = mesaRecargar ?? controlador.ObtenerMesa(mesa.Id);
                    CargarInformacion(m);
                    imagenEstado.Visibility = System.Windows.Visibility.Visible;
                    barraCarga.Visibility = System.Windows.Visibility.Collapsed;

                }
                catch (Exception e)
                {
//#if DEBUG
                    try
                    {
                        if (e.GetType() == typeof(DbEntityValidationException))
                        {
                            var excepcion = (DbEntityValidationException)e;

                            foreach (var er in excepcion.EntityValidationErrors)
                                Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                        }
                        else if (e.GetType() != typeof(SOTException))
                        {
                            var exCurrent = e;

                            for (int i = 0; i < 50 && exCurrent != null; i++, exCurrent = exCurrent.InnerException)
                            {
                                Transversal.Log.Logger.Fatal(exCurrent.Message);
                            }
                        }
                    }
                    catch { }
//#endif
                    imagenEstado.Visibility = System.Windows.Visibility.Collapsed;
                    barraCarga.Visibility = System.Windows.Visibility.Visible;
                    txtDetalle.Text = "RECARGANDO";
                    //return;
                }
                if (Recargar != null)
                {
                    Recargar(this, EventArgs.Empty);
                }
            }
        }

        public event RoutedEventHandler Click;

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            RoutedEventHandler handler = Click;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void btn_MouseEnter(object sender, MouseEventArgs e)
        {
            CustomColor2 = CustomColor;
            Grid.SetRow(fondo, 0);
            Grid.SetRowSpan(fondo, 4);
            fondo.CornerRadius = new CornerRadius(5);
        }

        private void btn_MouseLeave(object sender, MouseEventArgs e)
        {
            CustomColor = CustomColor;
            Grid.SetRow(fondo, 1);
            Grid.SetRowSpan(fondo, 3);
            fondo.CornerRadius = new CornerRadius(0, 0, 5, 5);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            //{
            //    timerRecarga = new DispatcherTimer();
            //    timerRecarga.Interval = TimeSpan.FromSeconds(25);
            //    timerRecarga.Tick += timerRecarga_Tick;

            //    timerRecarga.Start();
            //}
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //if (timerRecarga != null)
            //{
            //    timerRecarga.Stop();
            //    timerRecarga.Tick -= timerRecarga_Tick;
            //}
        }
    }
}