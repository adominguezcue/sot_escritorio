﻿using Modelo.Dtos;
using Modelo.Entidades;
using SOTWpf.Comandas;
using SOTWpf.ConceptosSistema;
using SOTControladores.Controladores;
using SOTWpf.Empleados;
using SOTWpf.OrdenesRestaurante.Manejadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;
using SOTWpf.Facturacion;

namespace SOTWpf.OrdenesRestaurante
{
    /// <summary>
    /// Lógica de interacción para ManejadorMesas.xaml
    /// </summary>
    public partial class ManejadorMesas : UserControl
    {

        Dictionary<Mesa.EstadosMesa, List<Componentes.Buttons.FloatingIconButton>> diccionarioBotones;
        Action<UCMesa> accionCambio = null;

        //List<UCMesa> uc_mesas;

        UCMesa mesaSeleccionada
        {
            get { return panelM.MesaSeleccionada; }
            set { panelM.MesaSeleccionada = value; }
        }

        public ManejadorMesas()
        {
            InitializeComponent();

            //uc_mesas = new List<UCMesa>();

            datosMesaP.Visibility = Visibility.Collapsed;

            diccionarioBotones = new Dictionary<Mesa.EstadosMesa, List<Componentes.Buttons.FloatingIconButton>>();

            diccionarioBotones[Mesa.EstadosMesa.Libre] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Mesa.EstadosMesa.Libre].Add(btnAbrirMesa);

            diccionarioBotones[Mesa.EstadosMesa.Ocupada] = new List<Componentes.Buttons.FloatingIconButton>();
            diccionarioBotones[Mesa.EstadosMesa.Ocupada].Add(btnCancelar);
            diccionarioBotones[Mesa.EstadosMesa.Ocupada].Add(btnCambioMesa);
            diccionarioBotones[Mesa.EstadosMesa.Ocupada].Add(btnImprimirCuenta);
            diccionarioBotones[Mesa.EstadosMesa.Ocupada].Add(btnConsumo);

            diccionarioBotones[Mesa.EstadosMesa.Cobrar] = new List<Componentes.Buttons.FloatingIconButton>();
            //diccionarioBotones[Mesa.EstadosMesa.Cobrar].Add(btnFacturar);
            diccionarioBotones[Mesa.EstadosMesa.Cobrar].Add(btnRecibirPago);

            panelBotones.Children.Clear();
            panelBotones.Visibility = Visibility.Collapsed;
        }

        //public void ActualizarMesas()
        //{
        //    var controlador = new ControladorRestaurantes();

        //    List<Mesa> habitaciones = controlador.ObtenerMesas();

        //    datosMesaP.Visibility = System.Windows.Visibility.Collapsed;

        //    panelMesas.Children.Clear();
        //    panelMesas.RowDefinitions.Clear();
        //    panelMesas.ColumnDefinitions.Clear();

        //    foreach (UCMesa uc_mesa in uc_mesas)
        //    {
        //        uc_mesa.Click -= mesa_Click;
        //        uc_mesa.Recargar -= CargarMesa;
        //        //uc_habitacion.Dispose();
        //    }

        //    uc_mesas.Clear();

        //    if (habitaciones.Count > 0)
        //    {
        //        for (int i = 1; i <= habitaciones.Max(m => m.Columna); i++)
        //        {
        //            panelMesas.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(98) });
        //        }

        //        var pisoMaximo = habitaciones.Max(m => m.Fila);

        //        for (int i = 1; i <= pisoMaximo; i++)
        //        {
        //            panelMesas.RowDefinitions.Add(new RowDefinition { Height = new GridLength(76) });
        //        }

        //        foreach (Mesa mesa in habitaciones)
        //        {
        //            var nh = new UCMesa(mesa);

        //            uc_mesas.Add(nh);
        //            nh.Margin = new Thickness(1, 2, 1, 2);
        //            Grid.SetColumn(nh, mesa.Columna - 1);
        //            Grid.SetRow(nh, pisoMaximo - mesa.Fila);

        //            uc_mesas.Last().Click += mesa_Click;
        //            uc_mesas.Last().Recargar += CargarMesa;



        //            panelMesas.Children.Add(nh);//, habitacion.Columna - 1, pisoMaximo - habitacion.Fila);
        //        }
        //    }
        //}

        private void CambiarMesa(UCMesa mesaNueva)
        {
            try
            {
                if (mesaSeleccionada != null && mesaNueva != null)
                {
                    if (mesaSeleccionada == mesaNueva)
                        throw new SOTException(Textos.Errores.intercambio_misma_mesa_invalido_excepcion);

                    if (MessageBox.Show(string.Format(Textos.Mensajes.confirmar_intercambio_mesa, mesaSeleccionada.MesaInterna.Clave, mesaNueva.MesaInterna.Clave), Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        var controlador = new ControladorRestaurantes();

                        controlador.IntercambiarMesas(mesaSeleccionada.MesaInterna.Id, mesaNueva.MesaInterna.Id);

                        mesaSeleccionada.RecargarMesa();
                        mesaNueva.RecargarMesa();
                    }
                }
            }
            finally
            {
                accionCambio = null;
                btnCambioMesa.Animar = false;
            }
        }

        //private void mesa_Click(object sender, EventArgs e)
        //{
        //    if (accionCambio != null)
        //        accionCambio.Invoke(sender as UCMesa);

        //    mesaSeleccionada = sender as UCMesa;
        //    CargarMesa(sender, e);
        //}


        //private void CargarMesa(object sender, EventArgs e)
        //{

        //    if ((sender == null && mesaSeleccionada == null) || (sender != null && sender.Equals(mesaSeleccionada)))
        //    {
        //        MostrarBotonesPorEstado();

        //        if (mesaSeleccionada == null)
        //            return;

        //        datosMesaP.ActualizarDatos(mesaSeleccionada.MesaInterna.Id);
        //    }
        //}
        private void datos_VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (datosMesaP.Visibility == System.Windows.Visibility.Visible)
            {
                panelBotones.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                mesaSeleccionada = null;
                btnCambioMesa.Animar = false;
                accionCambio = null;
                panelBotones.Visibility = System.Windows.Visibility.Collapsed;
            }
        }


        private void MostrarBotonesPorEstado()
        {
            panelBotones.Children.Clear();

            if (mesaSeleccionada != null && diccionarioBotones.ContainsKey(mesaSeleccionada.MesaInterna.Estado))
            {
                foreach (Control c in diccionarioBotones[mesaSeleccionada.MesaInterna.Estado])
                {
                    panelBotones.Children.Add(c);
                }
            }

        }

        //private void ManejadorMesas_Loaded(object sender, RoutedEventArgs e)
        //{
        //    if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
        //    {
        //        ActualizarMesas();
        //    }
        //}

        private void btnRestaurante_Click(object sender, RoutedEventArgs e)
        {
            if (mesaSeleccionada == null)
                throw new SOTException(Textos.Errores.mesa_no_seleccionada_exception);

            var controlador = new ControladorRestaurantes();

            var ocupacionActual = controlador.ObtenerOcupacionActualPorMesa(mesaSeleccionada.MesaInterna.Id);


            if (ocupacionActual != null && controlador.VerificarTieneOrdenesPendientes(ocupacionActual.Id))
            {
                var manejador = new ManejadorOrdenesRestaurante(mesaSeleccionada.MesaInterna.Id, mesaSeleccionada.MesaInterna.Clave);

                var ordenesF = new GestionComandasForm(manejador);
                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, ordenesF);
            }
            else
            {
                var ccn = new ManejadorCreacionOrdenesRestaurante(mesaSeleccionada.MesaInterna.Id, "Mesa " + mesaSeleccionada.MesaInterna.Clave);

                var comandasF = new SOTWpf.Comandas.CreacionComandasForm(ccn);

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, comandasF);

            }
            mesaSeleccionada.RecargarMesa();

        }

        private void btnCobrar_Click(object sender, RoutedEventArgs e)
        {
            if (mesaSeleccionada == null)
                throw new SOTException(Textos.Errores.mesa_no_seleccionada_exception);

            var controlador = new ControladorRestaurantes();

            var ocupacion = controlador.ObtenerOcupacionBaseActualPorMesa(mesaSeleccionada.MesaInterna.Id);

            if (ocupacion == null)
                throw new SOTException("La mesa no tiene cuenta abierta.");

            var cuentaF = new CuentaForm(ocupacion.Id);

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, cuentaF);

            mesaSeleccionada.RecargarMesa();

        }

        private void btnHabilitarMeseros_Click(object sender, RoutedEventArgs e)
        {


            var habilitacionEmpleadosF = new HabilitacionEmpleadosForm(new ControladorConfiguracionGlobal().ObtenerConfiguracionPuestosMaestros().Mesero ?? 0);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, habilitacionEmpleadosF);
        }

        private void btnAbrirMesa_Click(object sender, RoutedEventArgs e)
        {
            if (mesaSeleccionada == null)
                throw new SOTException(Textos.Errores.mesa_no_seleccionada_exception);

            if (MessageBox.Show("Confirme la apertura de la mesa " + mesaSeleccionada.MesaInterna.Clave, "Apertura de mesas", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
            {
                var selector = new SeleccionadorMeserosForm();

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, selector);

                if (!selector.ProcesoExitoso)
                    return;

                var controlador = new ControladorRestaurantes();
                controlador.MarcarMesaComoOcupada(mesaSeleccionada.MesaInterna.Id, selector.MeseroSeleccionado.Id);



                mesaSeleccionada.RecargarMesa();
            }
        }

        private void PrevioRecargar(object sender, RoutedEventArgs e)
        {
            if (accionCambio != null)
                accionCambio.Invoke(/*sender*/e.OriginalSource as UCMesa);
        }

        private void CargarMesa(object sender, RoutedEventArgs e)
        {

            //if ((sender == null && mesaSeleccionada == null) || (sender != null && sender.Equals(mesaSeleccionada)))
            //{
            MostrarBotonesPorEstado();

            if (mesaSeleccionada == null)
                return;

            datosMesaP.ActualizarDatos(mesaSeleccionada.MesaInterna.Id);
            //}
        }

        private void btnCambioMesa_Click(object sender, RoutedEventArgs e)
        {
            if (accionCambio == null)
            {
                accionCambio = CambiarMesa;
                btnCambioMesa.Animar = true;
            }
            else
            {
                accionCambio = null;
                btnCambioMesa.Animar = false;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (mesaSeleccionada == null)
                throw new SOTException(Textos.Errores.mesa_no_seleccionada_exception);


            CapturaMotivo motivoF = new CapturaMotivo();
            motivoF.Motivo = "cancelación";
            motivoF.TextoAuxiliar = "Detalles";

            CapturaMotivo.AceptarEventHandler aeh = new CapturaMotivo.AceptarEventHandler((s, ea) =>
            {
                var controlador = new ControladorRestaurantes();

                var ocupacion = controlador.ObtenerOcupacionBaseActualPorMesa(mesaSeleccionada.MesaInterna.Id);

                controlador.CancelarOcupacion(ocupacion.Id, motivoF.DescripcionMotivo);
            });

            try
            {
                motivoF.Aceptar += aeh;

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, motivoF);
            }
            finally
            {
                motivoF.Aceptar -= aeh;
            }

            mesaSeleccionada.RecargarMesa();
        }

        private void btnCuenta_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnImprimirCuenta_Click(object sender, RoutedEventArgs e)
        {
            if (mesaSeleccionada == null)
                throw new SOTException(Textos.Errores.mesa_no_seleccionada_exception);

            var controlador = new ControladorRestaurantes();

            var ocupacion = controlador.ObtenerOcupacionBaseActualPorMesa(mesaSeleccionada.MesaInterna.Id);

            if (ocupacion == null)
                throw new SOTException("La mesa no tiene cuenta abierta.");

            controlador.ValidarNoTieneOrdenesPendientes(ocupacion.Id);

            if (controlador.ObtenerDetallesOrdenesEntregadas(ocupacion.Id).Count == 0)
                throw new SOTException("La mesa no tiene cuenta abierta.");

            var cuentaF = new CuentaForm(ocupacion.Id);


            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, cuentaF);



            mesaSeleccionada.RecargarMesa();
        }

        

        private void btnFacturar_Click(object sender, RoutedEventArgs e)
        {
            if (mesaSeleccionada == null)
                throw new SOTException(Textos.Errores.mesa_no_seleccionada_exception);

            var controlador = new ControladorRestaurantes();

            var ocupacion = controlador.ObtenerOcupacionBaseActualPorMesa(mesaSeleccionada.MesaInterna.Id);

            if (ocupacion == null)
                throw new SOTException("La mesa no tiene cuenta abierta.");

            var edf = new EdicionDatosFacturacionForm(ocupacion.DatosFiscales);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, edf);

            if (edf.ProcesoTerminado)
                controlador.VincularDatosFiscales(ocupacion.Id, edf.DatosFiscalesActuales == null ? default(int?) : edf.DatosFiscalesActuales.Id);
        }
    }
}
