﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Comandas;
using SOTWpf.Pagos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Extensiones;

namespace SOTWpf.OrdenesRestaurante
{
    /// <summary>
    /// Lógica de interacción para CuentaForm.xaml
    /// </summary>
    public partial class CuentaForm : Window
    {
        private int idOcupacion;
        private CollectionViewSource cvsProductos;
        private PropertyGroupDescription pdg;

        private Modelo.Almacen.Entidades.ZctCatPar config;

        ObservableCollection<ResumenArticulo> resumenes;

        decimal subtotal = 0;
        decimal cortesia = 0;
        decimal descuento = 0;
        private List<ArticuloOrdenRestaurante> articulos;

        public CuentaForm(int idOcupacion)
        {
            InitializeComponent();

            cvsProductos = ((CollectionViewSource)Resources["cvsProductos"]);
            pdg = new PropertyGroupDescription { PropertyName = ObjectExtensions.NombrePropiedad<ResumenArticulo, object>(m => m.Agrupador) };
            config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            this.idOcupacion = idOcupacion;

            var controlador = new ControladorRestaurantes();

            var mesa = controlador.ObtenerMesaPorOcupacion(idOcupacion);

            if (mesa != null)
            {
                if (mesa.Estado == Mesa.EstadosMesa.Cobrar)
                {
                    btnImprimirCuenta.Visibility = Visibility.Visible;
                    //tbAgrupar.Visibility = Visibility.Collapsed;
                }
                else
                {
                    btnImprimirCuenta.Visibility = Visibility.Collapsed;
                    cvsProductos.GroupDescriptions.Add(pdg);
                }
            }

            resumenes = new ObservableCollection<ResumenArticulo>();
            resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            cvsProductos.Source = resumenes;
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, EventArgs e)
        {
            RecalcularTotales();
        }

        private void RecalcularTotales()
        {
            if (resumenes.Count == 0)
            {
                subtotal = 0;
                cortesia = 0;
                descuento = 0;

                txtCortesia.Text = (0).ToString("C");
                //txtDescuento.Text = "$" + (0).ToString("C");
                txtSubtotal.Text = (0).ToString("C");
                //txtTotalSinIVA.Text = (0).ToString("C");
                //txtIVA.Text = (0).ToString("C");
                txtTotalConIVA.Text = (0).ToString("C");
            }
            else
            {
                var config = new ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

                cortesia = resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                descuento = 0;
                subtotal = resumenes.Sum(m => m.Total);


                txtCortesia.Text = cortesia.ToString("C");
                //txtDescuento.Text = "$" + descuento.ToString("C");
                txtSubtotal.Text = subtotal.ToString("C");
                var total = (subtotal - cortesia - descuento);

                txtTotalConIVA.Text = total.ToString("C");
                //txtTotalSinIVA.Text = total.ToString("C");
                //txtIVA.Text = (total * config.Iva_CatPar).ToString("C");

                //cortesia *= (1 + config.Iva_CatPar);
                //descuento *= (1 + config.Iva_CatPar);
                //subtotal *= (1 + config.Iva_CatPar);
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorRestaurantes();

            Modelo.Entidades.Mesa mesa = controlador.ObtenerMesaPorOcupacion(idOcupacion);

            if (mesa != null) 
            {
                switch (mesa.Estado)
                { 
                    case Modelo.Entidades.Mesa.EstadosMesa.Ocupada:

                        List<int> idsComandasCortesias = new List<int>();

                        foreach (var grupo in resumenes)
                        {
                            if (grupo.EsCortesia)
                                idsComandasCortesias.Add(grupo.IdRelacional);
                        }

                        controlador.MarcarMesaComoPorCobrar(mesa.Id, idsComandasCortesias);
                        break;
                    case Modelo.Entidades.Mesa.EstadosMesa.Cobrar:
                        {
                            //var valor = controlador.ObtenerValorCobrarMesa(mesa.Id);

                            var selectorPago = new SelectorFormaPagoForm(subtotal, cortesia, false, resumenes.Any(m => m.EsCortesia) ? 
                                SelectorFormaPagoForm.Justificaciones.Cortesia : 
                                SelectorFormaPagoForm.Justificaciones.Ninguna/*false, falsevalor.ValorSinIVA, valor.ValorIVA, valor.ValorConIVA*/);

                            Utilidades.Dialogos.MostrarDialogos(this, selectorPago);

                            if (!selectorPago.ProcesoExitoso)
                                return;

                            var selectorMesero = new Empleados.SeleccionadorMeserosForm();

                            Utilidades.Dialogos.MostrarDialogos(this, selectorMesero);

                            Empleado mesero;

                            if (!selectorMesero.ProcesoExitoso || (mesero = selectorMesero.MeseroSeleccionado) == null)
                                return;

                            controlador.CobrarMesa(mesa.Id, selectorPago.FormasPagoSeleccionadas, selectorPago.PropinaGenerada, mesero.Id, selectorPago.FormasPagoSeleccionadas.Count == 1 && selectorPago.FormasPagoSeleccionadas.Any(m => m.TipoPago == Dominio.Nucleo.Entidades.TiposPago.Cortesia));
                        }
                        break;
                }
            }

            Close();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSolicitarCambio_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSolicitarCancelacion_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var controlador = new ControladorRestaurantes();

            Modelo.Entidades.Mesa mesa = controlador.ObtenerMesaPorOcupacion(idOcupacion);

            if (mesa != null)
            {

                articulos = controlador.ObtenerDetallesOrdenesEntregadas(idOcupacion);


                switch (mesa.Estado)
                {
                    case Modelo.Entidades.Mesa.EstadosMesa.Ocupada:
                        //btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Hidden;
                        //btnSolicitarCambio.Visibility = System.Windows.Visibility.Hidden;

                        foreach (var articulo in articulos/*(from art in articulos
                                                       group art by new { art.OrdenTMP, art.PrecioUnidad, art.IdArticulo, art.EsCortesia } into ar
                                                       select ar)*/)
                        {

                            //var articulo = articuloOrden.First();

                            resumenes.Add(new ResumenArticulo(config.Iva_CatPar)
                            {
                                Agrupador = articulo.OrdenTMP,
                                IdArticulo = /*articuloOrden.Key*/articulo.IdArticulo,
                                Nombre = articulo.ArticuloTmp.Desc_Art,
                                Tipo = articulo.ArticuloTmp.NombreLineaTmp,
                                Cantidad = articulo.Cantidad,//articuloOrden.Sum(m => m.Cantidad),
                                Precio = /*articuloOrden.Key*/articulo.PrecioUnidad,
                                EsCortesia = /*articuloOrden.Key*/articulo.EsCortesia,
                                IdRelacional = articulo.Id
                            });
                        }

                        break;
                    case Modelo.Entidades.Mesa.EstadosMesa.Cobrar:
                        //btnSolicitarCancelacion.Visibility = System.Windows.Visibility.Visible;
                        //btnSolicitarCambio.Visibility = System.Windows.Visibility.Visible;

                        foreach (var articuloOrden in (from art in articulos
                                                       group art by new { art.PrecioUnidad, art.IdArticulo, art.EsCortesia } into ar
                                                       select ar))
                        {

                            var articulo = articuloOrden.First();

                            resumenes.Add(new ResumenArticulo(config.Iva_CatPar)
                            {
                                Agrupador = articulo.OrdenTMP,
                                IdArticulo = articuloOrden.Key.IdArticulo,
                                Nombre = articulo.ArticuloTmp.Desc_Art,
                                Tipo = articulo.ArticuloTmp.NombreLineaTmp,
                                Cantidad = articuloOrden.Sum(m => m.Cantidad),
                                Precio = articuloOrden.Key.PrecioUnidad,
                                EsCortesia = articuloOrden.Key.EsCortesia
                            });
                        }
                        break;
                }
            }

            
        }

        private void btnImprimirCuenta_Click(object sender, RoutedEventArgs e)
        {
            var controladorR = new SOTControladores.Controladores.ControladorRestaurantes();

            Modelo.Entidades.Mesa mesa = controladorR.ObtenerMesaPorOcupacion(idOcupacion);

            controladorR.ReimprimirTicket(mesa.Id);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var orden = ((sender as Control).DataContext as CollectionViewGroup).Name;

            foreach (var resumen in resumenes.Where(m => m.Agrupador.Equals(orden)))
            {
                resumen.EsCortesiaOriginal = resumen.EsCortesia;
                resumen.EsCortesia = true;
            }

            RecalcularTotales();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            var orden = ((sender as Control).DataContext as CollectionViewGroup).Name;

            foreach (var resumen in resumenes.Where(m => m.Agrupador.Equals(orden)))
            {
                resumen.EsCortesia = resumen.EsCortesiaOriginal;
            }

            RecalcularTotales();
        }

        //private void tbAgrupar_Unchecked(object sender, RoutedEventArgs e)
        //{
        //    if (cvsProductos != null)
        //        cvsProductos.GroupDescriptions.Clear();
        //}

        //private void tbAgrupar_Checked(object sender, RoutedEventArgs e)
        //{
        //    if (cvsProductos != null)
        //    {
        //        cvsProductos.GroupDescriptions.Clear();

        //        cvsProductos.GroupDescriptions.Add(pdg);
        //    }

        //}
    }
}
