﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.OrdenesRestaurante
{
    /// <summary>
    /// Lógica de interacción para EdicionMesasUC.xaml
    /// </summary>
    public partial class EdicionMesasUC : UserControl
    {
        public static readonly RoutedEvent EdicionAceptadaEvent = EventManager.RegisterRoutedEvent("EdicionAceptada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionMesasUC));
        public static readonly RoutedEvent EdicionCanceladaEvent = EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(EdicionMesasUC));

        public event RoutedEventHandler EdicionAceptada
        {
            add { AddHandler(EdicionAceptadaEvent, value); }
            remove { RemoveHandler(EdicionAceptadaEvent, value); }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add { AddHandler(EdicionCanceladaEvent, value); }
            remove { RemoveHandler(EdicionCanceladaEvent, value); }
        }

        private Mesa MesaActual
        {
            get { return DataContext as Mesa; }
            set { DataContext = value; }
        }

        public EdicionMesasUC()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var mActual = MesaActual;

            if (mActual != null)
            {
                var controladorMesas = new SOTControladores.Controladores.ControladorRestaurantes();

                if (mActual.Id == 0)
                    controladorMesas.CrearMesa(mActual);
                else
                    controladorMesas.ModificarMesa(mActual);
            }

            RaiseEvent(new RoutedEventArgs(EdicionAceptadaEvent));
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
