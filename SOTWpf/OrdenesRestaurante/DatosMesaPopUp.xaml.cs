﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SOTWpf.OrdenesRestaurante
{
    /// <summary>
    /// Lógica de interacción para DatosMesaPopUp.xaml
    /// </summary>
    public partial class DatosMesaPopUp : UserControl
    {
        public DatosMesaPopUp()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = System.Windows.Visibility.Collapsed;
        }

        internal void ActualizarDatos(int idMesa)
        {
            try
            {
                var controlador = new SOTControladores.Controladores.ControladorRestaurantes();
                var mesa = controlador.ObtenerDatosMesaActiva(idMesa);
                DataContext = mesa;
                if (mesa != null)
                    Visibility = System.Windows.Visibility.Visible;
                else
                    Visibility = System.Windows.Visibility.Collapsed;
            }
            catch 
            {
                Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
