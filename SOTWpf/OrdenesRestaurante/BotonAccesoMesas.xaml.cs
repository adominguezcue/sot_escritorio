﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SOTWpf.OrdenesRestaurante
{
    /// <summary>
    /// Lógica de interacción para BotonAccesoMesas.xaml
    /// </summary>
    public partial class BotonAccesoMesas : UserControl
    {
        private DispatcherTimer timerRecarga;

        public BotonAccesoMesas()
        {
            InitializeComponent();
        }

        private void btnMesas_Click(object sender, RoutedEventArgs e)
        {
            PrincipalRestaurante principalF = new PrincipalRestaurante();

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, principalF);

            CargarCantidadMesas(this, EventArgs.Empty);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (!new ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarMesas = true }))
                {
                    Visibility = System.Windows.Visibility.Collapsed;
                    return;
                }

                timerRecarga = new DispatcherTimer();
                timerRecarga.Interval = TimeSpan.FromSeconds(15);
                timerRecarga.Tick += CargarCantidadMesas;
                CargarCantidadMesas(null, null);

                timerRecarga.Start();
            }
        }

        private void CargarCantidadMesas(object sender, EventArgs e)
        { 
            var controladorRestaurante = new ControladorRestaurantes();

            try
            {
                contadorMesas.Text = controladorRestaurante.ObtenerCantidadMesasAbiertas().ToString();
            }
            catch
            {
                    
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if (timerRecarga != null)
                {
                    timerRecarga.Stop();
                    timerRecarga.Tick -= CargarCantidadMesas;
                }
            }
        }

        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                if ((e.NewValue as bool?) == true && !new ControladorPermisos().VerificarPermisos(new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarMesas = true }))
                    Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
