﻿using SOTControladores.Controladores;
using SOTWpf.Comandas;
using SOTWpf.Comandas.Manejadores;
using SOTWpf.Empleados;
using SOTWpf.Pagos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.OrdenesRestaurante.Manejadores
{
    public class ManejadorOrdenCocina: ManejadorComandaBase
    {
        int _numeroOrden;
        ObservableCollection<ResumenArticulo> _resumenes;

        public override ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            protected set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public override int NumeroOrden
        {
            get { return _numeroOrden; }
            protected set
            {
                _numeroOrden = value;
                NotifyPropertyChanged();
            }
        }

        string _estadoComanda, _tiposComanda, _informacionPertenencia;
        bool _enPreparacion;

        public override string EstadoComanda
        {
            get { return _estadoComanda; }
            protected set
            {
                _estadoComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string TiposComanda
        {
            get { return _tiposComanda; }
            protected set
            {
                _tiposComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string InformacionPertenencia
        {
            get { return _informacionPertenencia; }
            protected set
            {
                _informacionPertenencia = value;
                NotifyPropertyChanged();
            }
        }

        decimal _cortesia, _cortesiaConIVA, _descuento, _descuentoConIVA, _subtotal, _subtotalConIVA, _totalSinIVA, _iva, _totalConIVA;

        //public override decimal Cortesia
        //{
        //    get { return _cortesia; }
        //    protected set
        //    {
        //        _cortesia = value;

        //        NotifyPropertyChanged();

        //        CortesiaConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}
        //public override decimal Descuento
        //{
        //    get { return _descuento; }
        //    protected set
        //    {
        //        _descuento = value;
        //        NotifyPropertyChanged();

        //        DescuentoConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            protected set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public override decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            protected set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal Subtotal
        //{
        //    get { return _subtotal; }
        //    protected set
        //    {
        //        _subtotal = value;
        //        NotifyPropertyChanged();

        //        SubtotalConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            protected set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal TotalSinIVA
        //{
        //    get { return _totalSinIVA; }
        //    protected set
        //    {
        //        _totalSinIVA = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        //public override decimal IVA
        //{
        //    get { return _iva; }
        //    protected set
        //    {
        //        _iva = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        public override decimal TotalConIVA
        {
            get { return _totalConIVA; }
            protected set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }


        public override bool EnPreparacion
        {
            get { return _enPreparacion; }
        }

        private Modelo.Dtos.DtoResumenComandaExtendido ordenActual;

        private TimeSpan _tiempo;

        public override TimeSpan Tiempo
        {
            get { return _tiempo; }
            protected set
            {
                _tiempo = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorOrdenCocina(Modelo.Dtos.DtoResumenComandaExtendido detalles, bool enPreparacion)
        {
            ordenActual = detalles;
            _enPreparacion = enPreparacion;
            base.Initialize();
        }

        protected override void Inicializar()
        {
            base.Inicializar();

            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarDetallesOrden();
        }

        private void CargarDetallesOrden()
        {
            //ordenActual = detalles;

            if (ordenActual != null)
            {

                Tiempo = DateTime.Now - ordenActual.FechaInicio;


                string cadenaTipos = string.Empty;

                if (ordenActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Equals("ALIMENTOS")))
                    cadenaTipos += "A";

                if (ordenActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Equals("BEBIDAS")))
                    cadenaTipos += "B";

                if (ordenActual.ArticulosComanda.Any(m => m.Activo && m.Tipo.Trim().ToUpper().Contains("SEX")))
                    cadenaTipos += "S";

                foreach (var articuloOrden in ordenActual.ArticulosComanda)
                {
                    if (!articuloOrden.Activo)
                        continue;

                    if (articuloOrden.EsOtroDepto == true)
                        cadenaTipos = articuloOrden.OtosDeptos;

                    ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                    nuevoResumen.IdArticulo = articuloOrden.IdArticulo;
                    nuevoResumen.Nombre = articuloOrden.Nombre;
                    nuevoResumen.Tipo = articuloOrden.Tipo;
                    nuevoResumen.Cantidad = articuloOrden.Cantidad;
                    nuevoResumen.Precio = articuloOrden.PrecioUnidad;
                    nuevoResumen.Observaciones = articuloOrden.Observaciones;
                    nuevoResumen.EsCortesia = articuloOrden.EsCortesia;

                    Resumenes.Add(nuevoResumen);
                }


                //if (ordenActual.Estado != Modelo.Entidades.Comanda.Estados.PorPagar && ordenActual.Estado != Modelo.Entidades.Comanda.Estados.Cancelada)
                //    //(ordenActual.Estado == Modelo.Entidades.Comanda.Estados.Preparacion || ordenActual.Estado == Modelo.Entidades.Comanda.Estados.PorEntregar)
                //{
                //    var tiempo = System.DateTime.Now - ordenActual.FechaInicio;

                    ColorearBrush(ordenActual.FechaInicio, ordenActual.Estado);
                //}

                EstadoComanda = ordenActual.Estado.Descripcion();
                TiposComanda = cadenaTipos;
                InformacionPertenencia = "Mesa " + ordenActual.Destino;
                NumeroOrden = ordenActual.Orden;
            }
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Resumenes.Count == 0)
            {
                //Cortesia = 0;
                //Descuento = 0;
                //Subtotal = 0;
                //TotalSinIVA = 0;
                //IVA = 0;
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {
                //Cortesia = Resumenes.Where(m => m.EsCortesia).Sum(m => m.TotalSinIVA);
                //Descuento = 0;

                //Subtotal = Resumenes.Sum(m => m.TotalSinIVA);

                //TotalSinIVA = Subtotal - Cortesia - Descuento;
                //IVA = TotalSinIVA * config.Iva_CatPar;
                CortesiaConIVA = Resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                DescuentoConIVA = 0;
                SubtotalConIVA = Resumenes.Sum(m => m.Total);

                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public override void Aceptar()
        {
            if (ordenActual != null)
            {
                var idsArticulosComandas = ordenActual.ArticulosComanda.Where(m => m.Activo && m.Estado == Modelo.Entidades.ArticuloComanda.Estados.EnProceso).Select(m => m.Id).ToList();

                if (idsArticulosComandas.Count > 0)
                {
                    var controladorRest = new ControladorRestaurantes();
                    controladorRest.FinalizarElaboracionProductos(ordenActual.Id, idsArticulosComandas);
                }
                else
                {
                    idsArticulosComandas = ordenActual.ArticulosComanda.Where(m => m.Activo && m.Estado == Modelo.Entidades.ArticuloComanda.Estados.PorEntregar).Select(m => m.Id).ToList();

                    if (idsArticulosComandas.Count > 0)
                    {
                        var controladorRest = new ControladorRestaurantes();

                        //var seleccionadorMeseroF = new SeleccionadorMeserosForm();

                        //Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, seleccionadorMeseroF);

                        //if (seleccionadorMeseroF.ProcesoExitoso)
                            controladorRest.EntregarProductos(ordenActual.Id, idsArticulosComandas/*, seleccionadorMeseroF.MeseroSeleccionado.Id*/);
                    }
                    else
                        return;
                }

                OnPostCobro(EventArgs.Empty);
            }
        }

        

        public override void SolicitarCambio()
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void SolicitarCancelacion()
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void Sincronizar(object comandaUOrden)
        {
            throw new NotSupportedException("Operación no soportada");
        }

        public override void Imprimir()
        {
            throw new NotSupportedException("Operación no soportada");
        }
    }
}
