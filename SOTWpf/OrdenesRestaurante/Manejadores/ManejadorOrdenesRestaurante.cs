﻿using SOTControladores.Controladores;
using SOTWpf.Comandas;
using SOTWpf.Comandas.Manejadores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.OrdenesRestaurante.Manejadores
{
    public class ManejadorOrdenesRestaurante: IManejadorComandas
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private int idMesaActual = 0;

        ObservableCollection<ManejadorComandaBase> _manejadores;
        bool _aceptaMas;

        public ObservableCollection<ManejadorComandaBase> Manejadores
        {
            get { return _manejadores; }
            private set
            {
                _manejadores = value;
                NotifyPropertyChanged();
            }
        }

        public bool AceptaMas
        {
            get { return _aceptaMas; }
            private set
            {
                _aceptaMas = value;
                NotifyPropertyChanged();
            }
        }

        private string _etiquetaDestino;
        public string EtiquetaDestino
        {
            get { return _etiquetaDestino; }
            private set
            {
                _etiquetaDestino = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorOrdenesRestaurante(int idMesa, string mesaDestino)
        {
            idMesaActual = idMesa;

            EtiquetaDestino = "Mesa " + mesaDestino;

            Manejadores = new ObservableCollection<ManejadorComandaBase>();
            Manejadores.CollectionChanged += manejadoresBindingSource_DataSourceChanged;
        }

        private void manejadoresBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (ManejadorOrden item in e.OldItems)
                {
                    item.PostCobro -= CargarComandasEH;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (ManejadorOrden item in e.NewItems)
                {
                    item.PostCobro += CargarComandasEH;
                }
            }
        }

        private void CargarComandasEH(object sender, EventArgs e)
        {
            CargarComandas();
        }

        public void CargarComandas()
        {
            var controlador = new ControladorConfiguracionGlobal();

            var controladorR = new ControladorRestaurantes();

            var configuracionGlobal = controlador.ObtenerConfiguracionGlobal();
            List<Modelo.Entidades.OrdenRestaurante> ordenes = controladorR.ObtenerDetallesOrdenesPendientes(idMesaActual);

            //Manejadores.Clear();
            AceptaMas = false;

            var manejadoresR = Manejadores.Where(m => !ordenes.Any(c => c.Orden == m.NumeroOrden)).ToList();

            foreach (var manejador in manejadoresR)
                Manejadores.Remove(manejador);

            foreach (var par in (from comanda in ordenes
                                 join manejador in Manejadores on comanda.Orden equals manejador.NumeroOrden into m
                                 from manejador in m.DefaultIfEmpty()
                                 select new
                                 {
                                     comanda,
                                     manejador
                                 }).ToList())
            {
                if (par.manejador != null)
                    par.manejador.Sincronizar(par.comanda);
                else
                    Manejadores.Add(new ManejadorOrden(par.comanda, EtiquetaDestino));
            }

            AceptaMas = Manejadores.Count < configuracionGlobal.MaximoComandasSimultaneas;
            

            //for (int i = 0; i <= configuracionGlobal.MaximoComandasSimultaneas - 1; i++)
            //{
            //    if (i < ordenes.Count)
            //    {
            //        var gComanda = new ManejadorOrden(ordenes[i]);
            //        gComanda.NumeroOrden = i + 1;

            //        Manejadores.Add(gComanda);
            //    }
            //    else
            //        AceptaMas = true;
            //}
        }

        public void Agregar(System.Windows.Window padre)
        {
            try
            {
                var ccm = new ManejadorCreacionOrdenesRestaurante(idMesaActual, EtiquetaDestino);

                CreacionComandasForm comandasF = new CreacionComandasForm(ccm);
                Utilidades.Dialogos.MostrarDialogos(padre, comandasF);
            }
            finally
            {
                CargarComandas();
            }
        }
    }
}
