﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTWpf.Comandas;
using SOTWpf.Comandas.Manejadores;
using SOTWpf.ConceptosSistema;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.OrdenesRestaurante.Manejadores
{
    public class ManejadorOrden : ManejadorComandaBase
    {
        int _numeroOrden;
        ObservableCollection<ResumenArticulo> _resumenes;

        public override ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            protected set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public override int NumeroOrden
        {
            get { return _numeroOrden; }
            protected set
            {
                _numeroOrden = value;
                NotifyPropertyChanged();
            }
        }

        string _estadoComanda, _tiposComanda;

        public override string EstadoComanda
        {
            get { return _estadoComanda; }
            protected set
            {
                _estadoComanda = value;
                NotifyPropertyChanged();
            }
        }

        public override string TiposComanda
        {
            get { return _tiposComanda; }
            protected set
            {
                _tiposComanda = value;
                NotifyPropertyChanged();
            }
        }

        decimal _cortesia, _cortesiaConIVA, _descuento, _descuentoConIVA, _subtotal, _subtotalConIVA, _totalSinIVA, _iva, _totalConIVA;

        //public override decimal Cortesia
        //{
        //    get { return _cortesia; }
        //    protected set
        //    {
        //        _cortesia = value;

        //        NotifyPropertyChanged();

        //        CortesiaConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}
        //public override decimal Descuento
        //{
        //    get { return _descuento; }
        //    protected set
        //    {
        //        _descuento = value;
        //        NotifyPropertyChanged();

        //        DescuentoConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            protected set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public override decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            protected set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal Subtotal
        //{
        //    get { return _subtotal; }
        //    protected set
        //    {
        //        _subtotal = value;
        //        NotifyPropertyChanged();

        //        SubtotalConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public override decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            protected set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public override decimal TotalSinIVA
        //{
        //    get { return _totalSinIVA; }
        //    protected set
        //    {
        //        _totalSinIVA = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        //public override decimal IVA
        //{
        //    get { return _iva; }
        //    protected set
        //    {
        //        _iva = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        public override decimal TotalConIVA
        {
            get { return _totalConIVA; }
            protected set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public override bool EnPreparacion
        {
            get { return false; }
        }

        private Modelo.Entidades.OrdenRestaurante ordenActual;
        string destino;

        private TimeSpan _tiempo;

        public override TimeSpan Tiempo
        {
            get { return _tiempo; }
            protected set
            {
                _tiempo = value;
                NotifyPropertyChanged();
            }
        }

        public ManejadorOrden(Modelo.Entidades.OrdenRestaurante detalles, string destino)
        {
            ordenActual = detalles;
            this.destino = destino;
            base.Initialize();
        }

        protected override void Inicializar()
        {
            base.Inicializar();
            // Agregue cualquier inicialización después de la llamada a InitializeComponent().
            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarDetallesOrden();
        }

        private void CargarDetallesOrden()
        {
            //ordenActual = detalles;


            if (ordenActual != null)
            {
                Tiempo = DateTime.Now - ordenActual.FechaCreacion;

                string cadenaTipos = string.Empty;

                if (ordenActual.ArticulosOrdenRestaurante.Any(m => m.Activo && m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Equals("ALIMENTOS")))
                    cadenaTipos += "A";

                if (ordenActual.ArticulosOrdenRestaurante.Any(m => m.Activo && m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Equals("BEBIDAS")))
                    cadenaTipos += "B";

                if (ordenActual.ArticulosOrdenRestaurante.Any(m => m.Activo && m.ArticuloTmp.NombreLineaTmp.Trim().ToUpper().Contains("SEX")))
                    cadenaTipos += "S";

                foreach (var articuloComanda in ordenActual.ArticulosOrdenRestaurante)
                {
                    if (!articuloComanda.Activo)
                        continue;

                    ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                    nuevoResumen.IdArticulo = articuloComanda.IdArticulo;
                    nuevoResumen.Nombre = articuloComanda.ArticuloTmp.Desc_Art;
                    nuevoResumen.Tipo = articuloComanda.ArticuloTmp.NombreLineaTmp;
                    nuevoResumen.Cantidad = articuloComanda.Cantidad;
                    nuevoResumen.Precio = articuloComanda.PrecioUnidad;
                    nuevoResumen.Observaciones = articuloComanda.Observaciones;
                    nuevoResumen.EsCortesia = articuloComanda.EsCortesia;

                    Resumenes.Add(nuevoResumen);
                }


                //if (ordenActual.Estado != Modelo.Entidades.Comanda.Estados.PorPagar && ordenActual.Estado != Modelo.Entidades.Comanda.Estados.Cancelada)
                ////(ordenActual.Estado == Modelo.Entidades.Comanda.Estados.Preparacion || ordenActual.Estado == Modelo.Entidades.Comanda.Estados.PorEntregar)
                //{
                //    var tiempo = System.DateTime.Now - ordenActual.FechaInicio;

                    ColorearBrush(ordenActual.FechaInicio, ordenActual.Estado);
                //}

                EstadoComanda = ordenActual.Estado.Descripcion();
                TiposComanda = cadenaTipos;
                NumeroOrden = ordenActual.Orden;
            }
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Resumenes.Count == 0)
            {
                //Cortesia = 0;
                //Descuento = 0;
                //Subtotal = 0;
                //TotalSinIVA = 0;
                //IVA = 0;
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {
                //Cortesia = Resumenes.Where(m => m.EsCortesia).Sum(m => m.TotalSinIVA);
                //Descuento = 0;

                //Subtotal = Resumenes.Sum(m => m.TotalSinIVA);

                //TotalSinIVA = Subtotal - Cortesia - Descuento;
                //IVA = TotalSinIVA * config.Iva_CatPar;
                CortesiaConIVA = Resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                DescuentoConIVA = 0;
                SubtotalConIVA = Resumenes.Sum(m => m.Total);

                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public override void Aceptar()
        {
            if (ordenActual != null)
            {
                var controladorRest = new ControladorRestaurantes();

                if (ordenActual.Estado == Modelo.Entidades.Comanda.Estados.PorCobrar)
                {
                    controladorRest.EntregarOrdenACliente(ordenActual.Id);
                }

            }
            OnPostCobro(EventArgs.Empty);
        }

        public override void SolicitarCambio()
        {
            /* if (ordenActual.Estado != Comanda.Estados.Preparacion &&
                 ordenActual.Estado != Comanda.Estados.PorEntregar)
                 throw new SOTException(Textos.Errores.orden_restaurante_no_modificable_exception);*/
            if (ordenActual.Estado == Comanda.Estados.PorPagar || ordenActual.Estado == Comanda.Estados.PorCobrar)
            {
                var controlador = new ControladorRoomServices();
                controlador.SolicitaCambioRestaurante();
            }


            var ccn = new ManejadorCreacionOrdenesRestaurante(ordenActual, destino);

            var creacionOrdenF = new SOTWpf.Comandas.CreacionComandasForm(ccn);

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, creacionOrdenF);

            OnPostCobro(EventArgs.Empty);
        }

        public override void SolicitarCancelacion()
        {
            var motivoF = new CapturaMotivo();
            motivoF.Motivo = "cancelación";
            motivoF.TextoAuxiliar = "Detalles";

            var aeh = new CapturaMotivo.AceptarEventHandler((s, ea) =>
            {
                if (ordenActual != null)
                {
                    var controlador = new ControladorRestaurantes();
                    controlador.CancelarOrden(ordenActual.Id, motivoF.DescripcionMotivo);

                }

                OnPostCobro(EventArgs.Empty);
            });

            try
            {
                motivoF.Aceptar += aeh;

                Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, motivoF);
            }
            finally
            {
                motivoF.Aceptar -= aeh;
            }
        }

        public override string InformacionPertenencia { get; protected set; }

        public override void Sincronizar(object comandaUOrden)
        {
            var orden = comandaUOrden as Modelo.Entidades.OrdenRestaurante;

            if (orden == null)
                throw new SOTException("El objeto a sincronizar es null o no es una orden de restaurante");

            ordenActual = orden;
            base.Initialize();
        }

        public override void Imprimir()
        {
            if (ordenActual != null)
            {
                var controlador = new ControladorRestaurantes();
                controlador.Imprimir(ordenActual.Id);

                MessageBox.Show("Impresión exitosa", Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
