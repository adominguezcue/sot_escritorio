﻿using SOTControladores.Controladores;
using SOTWpf.Comandas;
using SOTWpf.Comandas.Manejadores;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Transversal.Excepciones;

namespace SOTWpf.OrdenesRestaurante.Manejadores
{
    public class ManejadorCreacionOrdenesRestaurante : IManejadorCreacionComandas
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool _procesoExitoso = false;

        private int idMesaActual = 0;
        private Modelo.Almacen.Entidades.ZctCatPar config = new Modelo.Almacen.Entidades.ZctCatPar();

        decimal _cortesia, _cortesiaConIVA, _descuento, _descuentoConIVA, _subtotal, _subtotalConIVA, _totalSinIVA, _iva, _totalConIVA;
        string _etiquetaDestino;

        public bool SoportaCortesias
        {
            get { return true; }
        }

        public string EtiquetaDestino
        {
            get { return _etiquetaDestino; }
            private set
            {
                _etiquetaDestino = value;

                NotifyPropertyChanged();
            }
        }
        //public decimal Cortesia
        //{
        //    get { return _cortesia; }
        //    private set
        //    {
        //        _cortesia = value;

        //        NotifyPropertyChanged();

        //        CortesiaConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}
        //public decimal Descuento
        //{
        //    get { return _descuento; }
        //    private set
        //    {
        //        _descuento = value;
        //        NotifyPropertyChanged();

        //        DescuentoConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public decimal CortesiaConIVA
        {
            get { return _cortesiaConIVA; }
            private set
            {
                _cortesiaConIVA = value;
                NotifyPropertyChanged();
            }
        }
        public decimal DescuentoConIVA
        {
            get { return _descuentoConIVA; }
            private set
            {
                _descuentoConIVA = value;
                NotifyPropertyChanged();
            }
        }

        //public decimal Subtotal
        //{
        //    get { return _subtotal; }
        //    private set
        //    {
        //        _subtotal = value;
        //        NotifyPropertyChanged();

        //        SubtotalConIVA = value * (1 + config.Iva_CatPar);
        //    }
        //}

        public decimal SubtotalConIVA
        {
            get { return _subtotalConIVA; }
            private set
            {
                _subtotalConIVA = value;
                NotifyPropertyChanged();
            }
        }
        //public decimal TotalSinIVA
        //{
        //    get { return _totalSinIVA; }
        //    private set
        //    {
        //        _totalSinIVA = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        //public decimal IVA
        //{
        //    get { return _iva; }
        //    private set
        //    {
        //        _iva = value;
        //        NotifyPropertyChanged();
        //    }
        //}

        public decimal TotalConIVA
        {
            get { return _totalConIVA; }
            private set
            {
                _totalConIVA = value;
                NotifyPropertyChanged();
            }
        }

        ObservableCollection<ResumenArticulo> _resumenes;
        List<Modelo.Almacen.Entidades.Linea> _tipos;

        public ObservableCollection<ResumenArticulo> Resumenes
        {
            get { return _resumenes; }
            private set
            {
                _resumenes = value;
                NotifyPropertyChanged();
            }
        }

        public List<Modelo.Almacen.Entidades.Linea> Lineas
        {
            get { return _tipos; }
            private set
            {
                _tipos = value;
                NotifyPropertyChanged();
            }
        }

        private Modelo.Entidades.OrdenRestaurante ordenActual;

        public bool ProcesoExitoso
        {
            get { return _procesoExitoso; }
            private set { _procesoExitoso = value; }
        }

        public ManejadorCreacionOrdenesRestaurante(int idMesa, string destino)
        {
            idMesaActual = idMesa;

            EtiquetaDestino = destino;

            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarArticulos();
        }

        public ManejadorCreacionOrdenesRestaurante(Modelo.Entidades.OrdenRestaurante ordenRestaurante, string destino)
        {
            ordenActual = ordenRestaurante;

            EtiquetaDestino = destino;

            if (ordenActual != null && ordenActual.OcupacionMesa != null)
                idMesaActual = ordenActual.OcupacionMesa.IdMesa;
            else
                idMesaActual = 0;

            Resumenes = new ObservableCollection<ResumenArticulo>();
            Resumenes.CollectionChanged += ResumenArticuloBindingSource_DataSourceChanged;

            CargarArticulos();

            CargarDetallesOrdenRestaurante();
        }

        private void ResumenArticuloBindingSource_DataSourceChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (ResumenArticulo item in e.OldItems)
                {
                    //Removed items
                    item.PropertyChanged -= ResumenPropertyChanged;
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (ResumenArticulo item in e.NewItems)
                {
                    //Added items
                    item.PropertyChanged += ResumenPropertyChanged;
                }
            }


            RecalcularTotales();
        }

        private void ResumenPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            RecalcularTotales();
        }

        private void RecalcularTotales()
        {
            if (Resumenes.Count == 0)
            {
                //Cortesia = 0;
                //Descuento = 0;
                //Subtotal = 0;
                //TotalSinIVA = 0;
                //IVA = 0;
                SubtotalConIVA = 0;
                CortesiaConIVA = 0;
                DescuentoConIVA = 0;
                TotalConIVA = 0;
            }
            else
            {
                //Cortesia = Resumenes.Where(m => m.EsCortesia).Sum(m => m.TotalSinIVA);
                //Descuento = 0;

                //Subtotal = Resumenes.Sum(m => m.TotalSinIVA);

                //TotalSinIVA = Subtotal - Cortesia - Descuento;
                //IVA = TotalSinIVA * config.Iva_CatPar;
                CortesiaConIVA = Resumenes.Where(m => m.EsCortesia).Sum(m => m.Total);
                DescuentoConIVA = 0;
                SubtotalConIVA = Resumenes.Sum(m => m.Total);

                TotalConIVA = SubtotalConIVA - CortesiaConIVA - DescuentoConIVA;
            }
        }

        public void CargarArticulos(string filtro = null)
        {
            config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            var controlador = new ControladorArticulos();

            var tipos = controlador.ObtenerLineasConArticulos(true, false, filtro);

            if (ordenActual != null)
            {
                foreach (var articulo in tipos.SelectMany(m => m.Articulos))
                {
                    var articuloOrdenRestaurante = ordenActual.ArticulosOrdenRestaurante.FirstOrDefault(m => m.Activo && m.IdArticulo == articulo.Cod_Art);

                    if (articuloOrdenRestaurante != null)
                        articulo.Prec_Art = articuloOrdenRestaurante.PrecioUnidad;
                }
            }

            Lineas = tipos;
        }

        public void Aceptar()
        {
            //decimal subtotal = default(decimal);
            //decimal cortesia = 0;
            //decimal descuento = 0;
            //if (Resumenes.Count > 0)
            //{
            //    subtotal = Resumenes.Sum(m => m.TotalSinIVA);

            //}
            //else
            //{
            //    subtotal = 0;
            //}

            RecalcularTotales();

            if (ordenActual == null)
            {
                if (idMesaActual <= 0)
                    throw new SOTException(Textos.Errores.mesa_no_seleccionada_exception);

                try
                {
                    var config = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

                    ordenActual = new Modelo.Entidades.OrdenRestaurante();
                    ordenActual.Activo = true;
                    ordenActual.ValorConIVA = SubtotalConIVA;//ordenActual.ValorSinIVA + ordenActual.ValorIVA;
                    ordenActual.ValorSinIVA = SubtotalConIVA / (1 + config.Iva_CatPar);//subtotal - descuento;
                    ordenActual.ValorIVA = ordenActual.ValorConIVA - ordenActual.ValorSinIVA;// *config.Iva_CatPar;

                    foreach (ResumenArticulo item in Resumenes)
                    {
                        var nuevoArticuloOrdenRestaurante = new Modelo.Entidades.ArticuloOrdenRestaurante();
                        nuevoArticuloOrdenRestaurante.Activo = true;
                        nuevoArticuloOrdenRestaurante.IdArticulo = item.IdArticulo;
                        nuevoArticuloOrdenRestaurante.Cantidad = item.Cantidad;
                        nuevoArticuloOrdenRestaurante.PrecioUnidad = item.Precio;
                        nuevoArticuloOrdenRestaurante.Observaciones = item.Observaciones;
                        nuevoArticuloOrdenRestaurante.EsCortesia = item.EsCortesia;

                        ordenActual.ArticulosOrdenRestaurante.Add(nuevoArticuloOrdenRestaurante);
                    }

                    var controlador = new ControladorRestaurantes();

                    controlador.CrearOrden(ordenActual, idMesaActual);
                }
                catch
                {
                    ordenActual = null;
                    throw;
                }

            }
            else
            {
                var articulos = new List<Modelo.Dtos.DtoArticuloPrepararGeneracion>();

                foreach (ResumenArticulo item in Resumenes)
                {
                    Modelo.Dtos.DtoArticuloPrepararGeneracion nuevoArticuloOrdenRestaurante = new Modelo.Dtos.DtoArticuloPrepararGeneracion
                    {
                        Activo = true,
                        IdArticulo = item.IdArticulo,
                        Cantidad = item.Cantidad,
                        PrecioConIVA = item.Precio,
                        Id = item.IdRelacional,
                        Observaciones = item.Observaciones,
                        EsCortesia = item.EsCortesia
                    };

                    articulos.Add(nuevoArticuloOrdenRestaurante);
                }

                foreach (var articuloOrdenRestaurante in ordenActual.ArticulosOrdenRestaurante.Where(m => m.Activo && !Resumenes.Any(r => r.IdRelacional == m.Id)))
                {
                    Modelo.Dtos.DtoArticuloPrepararGeneracion nuevoArticuloOrdenRestaurante = new Modelo.Dtos.DtoArticuloPrepararGeneracion
                    {
                        Activo = false,
                        IdArticulo = articuloOrdenRestaurante.IdArticulo,
                        Cantidad = articuloOrdenRestaurante.Cantidad,
                        PrecioConIVA = articuloOrdenRestaurante.PrecioUnidad,
                        Id = articuloOrdenRestaurante.Id
                    };

                    articulos.Add(nuevoArticuloOrdenRestaurante);
                }

                var controlador = new ControladorRestaurantes();

                controlador.ModificarOrden(ordenActual.Id, articulos);
            }

            ProcesoExitoso = true;
        }

        public void AgregarAComanda(Modelo.Almacen.Entidades.Articulo articulo, bool esCortesia)
        {
            if (Resumenes.Any(m => m.IdArticulo == articulo.Cod_Art && m.EsCortesia == esCortesia))
            {
                Resumenes.First(m => m.IdArticulo == articulo.Cod_Art && m.EsCortesia == esCortesia).Cantidad += 1;
            }
            else
            {
                ResumenArticulo nuevoArticulo = new ResumenArticulo(config.Iva_CatPar);
                nuevoArticulo.IdArticulo = articulo.Cod_Art;
                nuevoArticulo.Nombre = articulo.Desc_Art;
                nuevoArticulo.Tipo = articulo.NombreLineaTmp;
                nuevoArticulo.Cantidad = 1;
                nuevoArticulo.Precio = articulo.Prec_Art;
                nuevoArticulo.EsCortesia = esCortesia;

                Resumenes.Add(nuevoArticulo);
            }
        }

        public void EliminarDeComanda(ResumenArticulo resumen)
        {
            if (Resumenes.Contains(resumen))
                Resumenes.Remove(resumen);
        }

        private void CargarDetallesOrdenRestaurante()
        {
            if (ordenActual != null)
            {
                foreach (var articuloOrdenRestaurante in ordenActual.ArticulosOrdenRestaurante)
                {
                    if (!articuloOrdenRestaurante.Activo)
                    {
                        continue;
                    }

                    ResumenArticulo nuevoResumen = new ResumenArticulo(config.Iva_CatPar);
                    nuevoResumen.IdRelacional = articuloOrdenRestaurante.Id;
                    nuevoResumen.IdArticulo = articuloOrdenRestaurante.IdArticulo;
                    nuevoResumen.Nombre = articuloOrdenRestaurante.ArticuloTmp.Desc_Art;
                    nuevoResumen.Tipo = articuloOrdenRestaurante.ArticuloTmp.NombreLineaTmp;
                    nuevoResumen.Cantidad = articuloOrdenRestaurante.Cantidad;
                    nuevoResumen.Precio = articuloOrdenRestaurante.PrecioUnidad;
                    nuevoResumen.Observaciones = articuloOrdenRestaurante.Observaciones;
                    nuevoResumen.EsCortesia = articuloOrdenRestaurante.EsCortesia;

                    Resumenes.Add(nuevoResumen);
                }
            }
        }

        //private void Window_Closing(object sender, CancelEventArgs e)
        //{
        //    Resumenes.Clear();
        //    Resumenes.CollectionChanged -= ResumenArticuloBindingSource_DataSourceChanged;
        //}
    }
}
