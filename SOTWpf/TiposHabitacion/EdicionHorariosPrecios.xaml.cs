﻿using Modelo.Entidades;
using SOTControladores.Controladores;
using SOTControladores.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace SOTWpf.TiposHabitacion
{
    /// <summary>
    /// Lógica de interacción para EdicionHorariosPrecios.xaml
    /// </summary>
    public partial class EdicionHorariosPrecios : UserControl
    {
        int idTipoHabitacion;
        bool procesar = true;
        decimal IVA;

        List<CheckableItemWrapper<ConfiguracionTarifa>> configuracionesMarcables;

        TipoHabitacion tipoH
        {
            get { return DataContext as TipoHabitacion; }
            set { DataContext = value; }
        }

        private static readonly RoutedEvent EdicionRealizadaEvent =
            EventManager.RegisterRoutedEvent("EdicionRealizada", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(EdicionHorariosPrecios));

        private static readonly RoutedEvent EdicionCanceladaEvent =
            EventManager.RegisterRoutedEvent("EdicionCancelada", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(EdicionHorariosPrecios));

        public event RoutedEventHandler EdicionRealizada
        {
            add { AddHandler(EdicionRealizadaEvent, value); }
            remove { RemoveHandler(EdicionRealizadaEvent, value); }
        }

        public event RoutedEventHandler EdicionCancelada
        {
            add { AddHandler(EdicionCanceladaEvent, value); }
            remove { RemoveHandler(EdicionCanceladaEvent, value); }
        }

        public EdicionHorariosPrecios()
        {
            InitializeComponent();
            var parametros = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();

            if (parametros == null)
                throw new SOTException("Los parámetros de operación no han sido configurados");

            IVA = parametros.Iva_CatPar;

            configuracionesMarcables = new List<CheckableItemWrapper<ConfiguracionTarifa>>();
        }

        internal void Cargar(int idTipoHabitacion = 0) 
        {
            this.idTipoHabitacion = idTipoHabitacion;

            CargarTipo();
        }

        private void cbTipos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this)) 
            {
                CargarTarifas();

                CargarTipo();
            }
        }

        private void CargarTipo()
        {
            if (idTipoHabitacion == 0)
            {
                var nuevoTipoH = new TipoHabitacion { Activo = true };
                var tiposLimpieza = Transversal.Extensiones.EnumExtensions.ComoLista<TareaLimpieza.TiposLimpieza>();

                foreach (var tipoLimpieza in tiposLimpieza)
                {
                    nuevoTipoH.TiemposLimpieza.Add(new TiempoLimpieza
                    {
                        Activa = true,
                        TipoLimpieza = tipoLimpieza
                    });
                }

                tipoH = nuevoTipoH;

                configuracionesMarcables.ForEach(m => m.IsChecked = false);
            }
            else
            {
                var controladorTiposH = new ControladorTiposHabitacion();
                var tipoHEditar = controladorTiposH.ObtenerTipoActivoCargado(idTipoHabitacion);

                procesar = false;
                try
                {
                    configuracionesMarcables.ForEach(m => m.IsChecked = false);

                    foreach (var configT in tipoHEditar.ConfiguracionesTipos.Where(m=>m.Activa)) 
                    {
                        var item = configuracionesMarcables.FirstOrDefault(m => m.Value.Id == configT.IdConfiguracionNegocio);
                        if (item != null)
                            item.IsChecked = true;
                    }
                }
                finally
                {
                    procesar = true;
                }

                tipoH = tipoHEditar;
            }

            foreach (var ct in tipoH.ConfiguracionesTipos)
            {
                ct.PrecioConIVATmp = ct.Precio * (1 + IVA);
                ct.PrecioHospedajeExtraConIVATmp = ct.PrecioHospedajeExtra * (1 + IVA);
                ct.PrecioPersonaExtraConIVATmp = ct.PrecioPersonaExtra * (1 + IVA);
            }
        }

        private void CargarTarifas()
        {
            var controladorConfiguracionesFajillas = new ControladorConfiguracionesTarifas();
            var configuraciones = controladorConfiguracionesFajillas.ObtenerConfiguracionesActivas();

            foreach (var config in configuraciones) 
                configuracionesMarcables.Add(new CheckableItemWrapper<ConfiguracionTarifa> { Value = config });

            cbTarifas.ItemsSource = configuracionesMarcables;
        }

        private void CheckBoxTarifa_Checked(object sender, RoutedEventArgs e)
        {
            if (!procesar)
                return;

            var tipoActual = tipoH;

            if (tipoActual != null)
            {
                var marcado = ((Control)sender).DataContext as CheckableItemWrapper<ConfiguracionTarifa>;

                var configTipo = tipoActual.ConfiguracionesTipos.FirstOrDefault(m => !m.Activa && m.Id != 0 && !m.FechaEliminacion.HasValue && m.IdConfiguracionNegocio == marcado.Value.Id);

                if (configTipo != null)
                {
                    configTipo.Activa = true;
                }
                else
                {
                    //tipoActual.ConfiguracionesTipos.Add();

                    var configTipoN = new ConfiguracionTipo
                    {
                        Activa = true,
                        IdConfiguracionNegocio = marcado.Value.Id,
                        TarifaTmp = marcado.Value.Tarifa
                    };

                    if (marcado.Value.ExtensionesMaximas > 0) 
                    {
                        for (int i = 1; i <= marcado.Value.ExtensionesMaximas; i++) 
                        {
                            configTipoN.TiemposHospedaje.Add(new Modelo.Entidades.TiempoHospedaje
                            {
                                Orden = i,
                                Activo = true
                            });
                        }
                    }

                    tipoActual.ConfiguracionesTipos.Add(configTipoN);
                }
            }

            CollectionViewSource.GetDefaultView(configuracionesTarifa.ItemsSource).Refresh();
        }

        private void CheckBoxTarifa_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!procesar)
                return;

            var tipoActual = tipoH;

            if (tipoActual != null)
            {
                var marcado = ((Control)sender).DataContext as CheckableItemWrapper<ConfiguracionTarifa>;

                var configTipo = tipoActual.ConfiguracionesTipos.FirstOrDefault(m => m.Activa && m.IdConfiguracionNegocio == marcado.Value.Id);

                if (configTipo != null)
                {
                    if (configTipo.Id != 0)
                        configTipo.Activa = false;

                    else
                        tipoActual.ConfiguracionesTipos.Remove(configTipo);
                }
            }

            CollectionViewSource.GetDefaultView(configuracionesTarifa.ItemsSource).Refresh();
        }

        private void configuracionesActivas_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as ConfiguracionTipo;

            e.Accepted = item != null && item.Activa;
        }

        private void tiemposHospedajeActivos_Filter(object sender, FilterEventArgs e)
        {
            var item = e.Item as TiempoHospedaje;

            e.Accepted = item != null && item.Activo;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            procesar = false;
            try
            {
                configuracionesMarcables.ForEach(m => m.IsChecked = false);
            }
            finally
            {
                procesar = true;
            }

            tipoH = null;

            RaiseEvent(new RoutedEventArgs(EdicionCanceladaEvent));
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            var controladorTH = new ControladorTiposHabitacion();

            if (idTipoHabitacion == 0)
                controladorTH.CrearTipoHabitacion(tipoH);
            else
                controladorTH.ModificarTipoHabitacion(tipoH);

            RaiseEvent(new RoutedEventArgs(EdicionRealizadaEvent));
        }

        //private void nudPrecio_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    var configT = (sender as Control).DataContext as Modelo.Entidades.ConfiguracionTipo;

        //    if (configT != null) 
        //    {
        //        configT.Precio = (decimal)(e.NewValue ?? 0) / (1 + IVA);
        //    }
        //}

        private void nudPrecio_NumberChanged(object sender, EventArgs e)
        {
            var configT = (sender as Control).DataContext as Modelo.Entidades.ConfiguracionTipo;

            if (configT != null) 
            {
                configT.Precio = (sender as CurrencyTextBoxControl.CurrencyTextBox).Number / (1 + IVA);
            }
        }

        //private void nudPrecioHospedajeExtra_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    var configT = (sender as Control).DataContext as Modelo.Entidades.ConfiguracionTipo;

        //    if (configT != null)
        //    {
        //        configT.PrecioHospedajeExtra = (decimal)(e.NewValue ?? 0) / (1 + IVA);
        //    }
        //}

        private void nudPrecioHospedajeExtra_ValueChanged(object sender, EventArgs e)
        {
            var configT = (sender as Control).DataContext as Modelo.Entidades.ConfiguracionTipo;

            if (configT != null)
            {
                configT.PrecioHospedajeExtra = (sender as CurrencyTextBoxControl.CurrencyTextBox).Number / (1 + IVA);
            }
        }

        //private void nudPrecioPersonaExtra_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        //{
        //    var configT = (sender as Control).DataContext as Modelo.Entidades.ConfiguracionTipo;

        //    if (configT != null)
        //    {
        //        configT.PrecioPersonaExtra = (decimal)(e.NewValue ?? 0) / (1 + IVA);
        //    }
        //}

        private void nudPrecioPersonaExtra_ValueChanged(object sender, EventArgs e)
        {
            var configT = (sender as Control).DataContext as Modelo.Entidades.ConfiguracionTipo;

            if (configT != null)
            {
                configT.PrecioPersonaExtra = (sender as CurrencyTextBoxControl.CurrencyTextBox).Number / (1 + IVA);
            }
        }
    }
}
