﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace SOTWpf.TiposHabitacion
{
    /// <summary>
    /// Lógica de interacción para HorariosPreciosForm.xaml
    /// </summary>
    public partial class HorariosPreciosForm : Window
    {
        public HorariosPreciosForm()
        {
            InitializeComponent();
        }

        private void CargarTipos() 
        {
            var controladorTipos = new ControladorTiposHabitacion();
            var tipos = controladorTipos.ObtenerTiposActivosCargadosDataTable();

            var columnas = new Dictionary<string, Type>();
            columnas.Add("Id", typeof(int));
            columnas.Add("Descripción", typeof(string));
            columnas.Add("Descripción extendida", typeof(string));
            columnas.Add("Tarifas", typeof(string));

            var diccionarioColumnaTarifaOrden = new Dictionary<Tuple<Modelo.Entidades.ConfiguracionTarifa.Tarifas, int>, string>();
            
            foreach (var tarifas in tipos.SelectMany(m => m.ResumenesTarifas).GroupBy(m => m.Tarifa)) 
            {
                foreach (var tiempos in tarifas.SelectMany(m => m.ResumenesTiempo).GroupBy(m => m.Orden).OrderBy(m=>m.Key)) 
                {
                    columnas.Add(tarifas.Key.Descripcion() + ": hospedaje " + tiempos.Key, typeof(string));
                    diccionarioColumnaTarifaOrden.Add(new Tuple<Modelo.Entidades.ConfiguracionTarifa.Tarifas, int>(tarifas.Key, tiempos.Key), tarifas.Key.Descripcion() + ": hospedaje " + tiempos.Key);
                }
            }

            foreach (var tiempoL in tipos.SelectMany(m => m.ResumenesLimpiezaHabitacion).GroupBy(m => m.TipoLimpieza)) 
            {
                columnas.Add(tiempoL.Key.Descripcion(), typeof(string));
            }

            var encabezados = columnas.Keys.ToList();
            object[][] datos = new object[tipos.Count][];

            int it = 0;
            int ic = 0;

            foreach (var tipo in tipos) 
            {
                ic = 0;
                datos[it] = new object[columnas.Count];

                datos[it][ic++] = tipo.Id;
                datos[it][ic++] = tipo.Descripcion;
                datos[it][ic++] = tipo.DescripcionExtendida;
                datos[it][ic++] = string.Join(", ", tipo.ResumenesTarifas.Select(m => m.Tarifa.Descripcion()));

                foreach (var tarifa in tipo.ResumenesTarifas)
                {
                    foreach (var tiempo in tarifa.ResumenesTiempo)
                    {
                        var columnaStr = diccionarioColumnaTarifaOrden.First(m => m.Key.Item1 == tarifa.Tarifa && m.Key.Item2 == tiempo.Orden).Value;

                        ic = encabezados.IndexOf(columnaStr);
                        datos[it][ic] = tiempo.Minutos.ToString();
                    }
                }

                foreach (var tiempoL in tipo.ResumenesLimpiezaHabitacion)
                {
                    ic = encabezados.IndexOf(tiempoL.TipoLimpieza.Descripcion());
                    datos[it][ic] = tiempoL.Minutos.ToString();
                }

                it++;
            }

            dgvTipos.ItemsSource = Transversal.Extensiones.DataExtensions.GetDataTable(columnas, datos).DefaultView;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarTipos();
                panelE.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void btnEliminarTipoHabitacion_Click(object sender, RoutedEventArgs e)
        {
            var item = dgvTipos.SelectedItem as DataRowView;

            if (item == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            if (MessageBox.Show(Textos.Mensajes.confirmar_eliminacion_elemento, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                var controlador = new SOTControladores.Controladores.ControladorTiposHabitacion();
                controlador.EliminarTipoHabitacion((int)item.Row["Id"]);

                CargarTipos();
                panelE.Visibility = System.Windows.Visibility.Collapsed;

                MessageBox.Show(Textos.Mensajes.eliminacion_elemento_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnModificarTipoHabitacion_Click(object sender, RoutedEventArgs e)
        {
            var item = dgvTipos.SelectedItem as DataRowView;

            if (item == null)
                throw new SOTException(Textos.Errores.elemento_no_seleccionado_excepcion);

            editor.Cargar((int)item.Row["Id"]);
            panelE.Visibility = System.Windows.Visibility.Visible;
        }

        private void btnCrearTipoHabitacion_Click(object sender, RoutedEventArgs e)
        {
            editor.Cargar();
            panelE.Visibility = System.Windows.Visibility.Visible;
        }

        private void BotoneraBase_CancelarClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void editor_EdicionRealizada(object sender, RoutedEventArgs e)
        {
            CargarTipos();
            panelE.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void editor_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            panelE.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void dgvTipos_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column.Header == "Id")
                e.Column.Visibility = System.Windows.Visibility.Collapsed;
        }
    }
}
