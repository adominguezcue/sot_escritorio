﻿using System.Reflection;
using System.Windows;
using System.Windows.Input;
using MixItems.View;
using SOTControladores.Controladores;
using SOTWpf.Almacenes;
using SOTWpf.Articulos;
using SOTWpf.Asistencias;
using SOTWpf.CentrosCostos;
using SOTWpf.Comandas;
using SOTWpf.ConceptosGastos;
using SOTWpf.ConceptosMantenimiento;
using SOTWpf.ConceptosSistema;
using SOTWpf.Cortes;
using SOTWpf.Departamentos;
using SOTWpf.Empleados;
using SOTWpf.EtiquetasArticulosCompuestos;
using SOTWpf.Facturacion;
using SOTWpf.Fajillas;
using SOTWpf.Fichas;
using SOTWpf.Habitaciones;
using SOTWpf.Impresiones;
using SOTWpf.Incidencias;
using SOTWpf.Inventarios;
using SOTWpf.Lineas;
using SOTWpf.Matriculas;
using SOTWpf.Nominas;
using SOTWpf.Paquetes;
using SOTWpf.Presentaciones;
using SOTWpf.Propinas;
using SOTWpf.Puestos;
using SOTWpf.Reservaciones;
using SOTWpf.ResTotal.Proveedores;
using SOTWpf.Seguridad.Roles;
using SOTWpf.SolicitudesPermisosFaltas;
using SOTWpf.TareasLimpieza;
using SOTWpf.TareasMantenimiento;
using SOTWpf.TiposHabitacion;
using SOTWpf.Ventas;
using SOTWpf.VPoints;
using Transversal.Excepciones.Seguridad;
using System.Diagnostics;
using System.Threading;						 				   
using ZctSOT;

namespace SOTWpf
{
    /// <summary>
    /// Lógica de interacción para Principal.xaml
    /// </summary>
    public partial class PrincipalHotel : Window
    {
        private bool hayCortes;
        private bool cerrarSinPreguntar = false;

        public PrincipalHotel()
        {
            InitializeComponent();

            tsmiGestionarTarjetasV.Flags = new Componentes.Buttons.HideableButton.FlagsList() { App.GestionarPuntosLealtad };
            tsmiConsultaTarjeta.Flags = new Componentes.Buttons.HideableButton.FlagsList() { App.GestionarPuntosLealtad };
            tsmiConsultaCupon.Flags = new Componentes.Buttons.HideableButton.FlagsList() { App.GestionarPuntosLealtad };
            tsmiConsultarErroresAbonos.Flags = new Componentes.Buttons.HideableButton.FlagsList() { App.GestionarPuntosLealtad };
            tsmiConsultarErroresReembolsos.Flags = new Componentes.Buttons.HideableButton.FlagsList() { App.GestionarPuntosLealtad };
            //wfh.Child = new ZctSOT.ZctHabitaciones();

            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                var controladorHabitaciones = new ControladorHabitaciones();

                controladorHabitaciones.ValidarAcceso();

                try
                {
                    txtUsuarioActual.Text = ControladorBase.UsuarioActual.NombreCompleto;

                    var controladorCortes = new ControladorCortesTurno();

                    hayCortes = controladorCortes.VerificarExistenTurnos();
                }
                catch (SOTPermisosException)
                {
                    hayCortes = false;
                }

                if (!hayCortes)
                    txtTextoMenuCorteTurno.Text = "ABRIR PRIMER TURNO";
            }


            Title += " " + Assembly.GetExecutingAssembly().GetName().Version.ToString();

        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (!cerrarSinPreguntar && System.Windows.MessageBox.Show(SOTWpf.Textos.Mensajes.salir_sistema, SOTWpf.Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                e.Cancel = true;


            //if (!e.Cancel)
            //    contenedorH.LimpiarListaHabitaciones();

            base.OnClosing(e);
        }

        #region menú principal

        private void tsmiSalirSistema_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void tsmiPaquetes_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionPaquetesForm());
        }

        private void tsmiReservaciones_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ReservacionesForm());
        }

        private void tsmiHorariosPrecios_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new HorariosPreciosForm());
        }

        private void tsmiConfiguracionPropinas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionPropinasForm());
        }

        private void tsmiConceptos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionConceptosForm());
        }

        private void tsmiHabilitarRecamareras_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new HabilitacionEmpleadosForm(new ControladorConfiguracionGlobal().ObtenerConfiguracionPuestosMaestros().Recamarera ?? 0));
        }

        private void tsmiHabilitarMeseros_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new HabilitacionEmpleadosForm(new ControladorConfiguracionGlobal().ObtenerConfiguracionPuestosMaestros().Mesero ?? 0));
        }

        private void tsmiConfiguracionFajillas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionFajillasForm());
        }

        private void tsmiReportarInicidencia_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionIncidenciasHabitacionForm());
        }

        private void tsmiVacaciones_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionSolicitudesVacacionesForm());
        }

        private void tsmiCorteTurno_Click(object sender, RoutedEventArgs e)
        {
            //Utilidades.Dialogos.MostrarDialogos(this, new CorteTurnoForm());
        }

        private void tsmiReporteContable_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new SelectorReporteForm());

            //var controladorCortes = new ControladorCortesTurno();

            //var reporte = controladorCortes.ObtenerReporteContableTurnoActual();

            //var documentoReporte = new SOTWpf.Reportes.CierreTurno.ReporteContable();
            //documentoReporte.Load();
            //var items = new List<DtoReporteContable>() { reporte };

            //documentoReporte.SetDataSource(items);
            //documentoReporte.Subreports["Habitaciones"].SetDataSource(reporte.ResumenesHabitaciones);
            //documentoReporte.Subreports["PersonasExtra"].SetDataSource(reporte.PersonasExtra);
            //documentoReporte.Subreports["HospedajeExtra"].SetDataSource(reporte.HospedajeExtra);
            //documentoReporte.Subreports["Paquetes"].SetDataSource(reporte.Paquetes);
            //documentoReporte.Subreports["RoomService"].SetDataSource(reporte.RoomService);
            //documentoReporte.Subreports["Restaurante"].SetDataSource(reporte.Restaurante);
            //documentoReporte.Subreports["Descuentos"].SetDataSource(reporte.Descuentos);
            //documentoReporte.Subreports["Taxis"].SetDataSource(reporte.ResumenesTaxis);
            //documentoReporte.Subreports["FormasPago"].SetDataSource(reporte.FormasPago);


            //var visorR = new VisorReportes();

            //visorR.crCrystalReportViewer.ReportSource = documentoReporte;
            //visorR.UpdateLayout();

            //Utilidades.Dialogos.MostrarDialogos(this, visorR);

        }

        private void tsmiIncidenciasTurno_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionIncidenciasTurnoForm());
        }

        private void tsmiGestionEmpleados_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionEmpleadosForm());
        }

        private void tsmiConceptosGastos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionConceptosGastosForm());
        }

        private void tsmiCentrosCostos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionCentrosCostosForm());
        }

        private void tsmiHistorialEntradasSalidas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionAsistenciasForm());
        }

        private void tsmiLimpieza_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionTareasLimpiezaForm());
        }

        private void tsmiFaltas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionSolicitudesFaltasForm());
        }

        private void tsmiPermisos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionSolicitudesPermisosForm());
        }

        private void tsmiCambiosDescansos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionSolicitudesCambiosDescansosForm());
        }

        private void tsmiConsultaCupon_Click(object sender, RoutedEventArgs e)
        {
            var cuponF = new ConsultaCupon();
            Utilidades.Dialogos.MostrarDialogos(this, cuponF);
        }

        private void tsmiConsultaTarjeta_Click(object sender, RoutedEventArgs e)
        {
            var consultaTarjetaVF = new ConsultaTarjetaVForm(true, null);
            Utilidades.Dialogos.MostrarDialogos(this, consultaTarjetaVF);
        }

        private void tsmiConsultarErroresAbonos_Click(object sender, RoutedEventArgs e)
        {
            var gestionAbonosF = new GestionAbonosErroneosForm();
            Utilidades.Dialogos.MostrarDialogos(this, gestionAbonosF);
        }

        private void tsmiConsultarErroresReembolsos_Click(object sender, RoutedEventArgs e)
        {
            var gestionReembolsosF = new GestionReembolsosErroneosForm();
            Utilidades.Dialogos.MostrarDialogos(this, gestionReembolsosF);
        }

        private void tsmiRequisiciones_Click(object sender, RoutedEventArgs e)
        {
            //var requisicionesF = new RequisicionesForm();
            //Utilidades.Dialogos.MostrarDialogos(this, requisicionesF);

            var requisicionesF = new RequisicionesWeb.Vista.Requisicion(ZctSOT.Datos.DAO.Conexion.CadenaConexion());
            //frm_requisiciones.MdiParent = Me
            requisicionesF.lanza_salida += Crea_salida;
            requisicionesF.lanza_compra += Crea_compra;

            //frm_requisiciones.Show()
            Utilidades.Dialogos.MostrarDialogos(this, requisicionesF);
        }


        private int Crea_salida(int Cod_EncOT)
        {
            var OTS = new OrdenesTrabajoMantenimientoForm();
            OTS.Show();
            OTS.CargaOrden(Cod_EncOT);
            //Utilidades.Dialogos.MostrarDialogos(this, OTS);


            return 0;
        }

        private int Crea_compra(int Cod_OC)
        {
            ZctOrdCompra Ocs = new ZctOrdCompra();
            Ocs.Show();
            Ocs.CargaCompra(Cod_OC.ToString());

            //Utilidades.Dialogos.MostrarDialogos(this, Ocs);

            return 0;
        }

        private void tsmiNomina_Click(object sender, RoutedEventArgs e)
        {
            var nominasF = new GestionNominas();
            Utilidades.Dialogos.MostrarDialogos(this, nominasF);
        }

        #endregion
        #region menú lateral

        private void menuItemReportarMatricula_Click(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            string matricula = null;

            //if (habitacionSeleccionada != null)
            //{
            //    var controlador = new ControladorRentas();

            //    matricula = controlador.ObtenerAutomovilesUltimaRenta(habitacionSeleccionada.HabitacionInterna.Id).Select(m => m.Matricula).FirstOrDefault();
            //}

            var reporteadorMatriculas = new ReportesMatriculaForm(matricula);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, reporteadorMatriculas);
        }

        private void menuItemHabilitarRecamareras_Click(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            var habilitacionEmpleadosF = new HabilitacionEmpleadosForm(new ControladorConfiguracionGlobal().ObtenerConfiguracionPuestosMaestros().Recamarera ?? 0);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, habilitacionEmpleadosF);
        }

        private void menuItemHabilitarMeseros_Click(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            var habilitacionEmpleadosF = new HabilitacionEmpleadosForm(new ControladorConfiguracionGlobal().ObtenerConfiguracionPuestosMaestros().Mesero ?? 0);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, habilitacionEmpleadosF);
        }

        private void menuItemHabilitarValets_Click(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            var config = new ControladorConfiguracionGlobal().ObtenerConfiguracionPuestosMaestros();

            var habilitacionEmpleadosF = new HabilitacionEmpleadosForm(config.Valet ?? 0, config.Vendedor ?? 0);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, habilitacionEmpleadosF);
        }

        private void menuItemGastos_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            var gestionGastosF = new Gastos.GestionGastosForm();

            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, gestionGastosF);
        }

        private void menuItemFacturar_Click(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            //btnFacturar_Click(sender, e);
            Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new EdicionDatosFacturacionForm());
        }

        private void menuItemCorteTurno_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            if (hayCortes)
            {
                if (MessageBox.Show(Textos.Mensajes.confirmar_cerrar_turno, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    return;

                var controladorCortes = new ControladorCortesTurno();
                controladorCortes.RealizarCorteTurno();

                MessageBox.Show(Textos.Mensajes.corte_exitoso, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
                foreach (Process proceso in Process.GetProcesses())
                {
                    if (proceso.ProcessName == "SOTWpf")
                    {
                        proceso.Kill();
                    }
                }

                /*var controladorS = new ControladorSesiones();
                controladorS.CerrarSesion();
                cerrarSinPreguntar = true;
                this.Close();*/
                // System.Environment.Exit(0);
                //this.Close();
				
                //Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new CierreTurnoForm());
            }
            else
            {
                contenedorH.MenuAbierto = false;

                var abrirCorteForm = new AbrirCorteForm();

                Utilidades.Dialogos.MostrarDialogos(this, abrirCorteForm);

                if (abrirCorteForm.TurnoAbierto)
                {
                    cerrarSinPreguntar = true;
                    Close();
                }
            }
        }

        //private void menuItemPrecorte_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    contenedorH.MenuAbierto = false;

        //    Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new RevisionTurnoActualForm());
        //}

        private void menuItemRoles_Click(object sender, MouseButtonEventArgs e)
        {
            contenedorH.MenuAbierto = false;

            Utilidades.Dialogos.MostrarDialogos(this, new GestionRolesForm());
        }

        #endregion

        private void tsmiObjetosOlvidados_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionObjetosOlvidadosForm());
        }

        private void tsmiImpresoras_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionImpresorasForm());
        }

        private void tsmiImpresorasLocales_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionImpresorasForm(true));
        }

        private void tsmiCalendarioMantenimiento_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new CalendarioMantenimientoForm());
        }

        private void tsmiArticulos_Click(object sender, RoutedEventArgs e)
        {
            //Utilidades.Dialogos.MostrarDialogos(this, new EdicionArticulosForm());
            Utilidades.Dialogos.MostrarDialogos(this, new GestionArticulosForm(false));
        }

        private void tsmiConceptosMantenimientoClick(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConfiguracionConceptosMantenimientoForm());
        }

        private void tsmiRecetas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new MixItemsForm(ZctSOT.Datos.DAO.Conexion.CadenaConexion()));
        }

        private void tsmiEntradasSalidas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new MixMovInvForm(ZctSOT.Datos.DAO.Conexion.CadenaConexion()));
        }

        private void tsmiOrdenesTrabajo_Click(object sender, RoutedEventArgs e)
        {
            //Utilidades.Dialogos.MostrarDialogos(this, new ZctOrdTrabajo());
            Utilidades.Dialogos.MostrarDialogos(this, new OrdenesTrabajoMantenimientoForm());
        }

        private void tsmiConsultarInventarios_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctComparaInventarios());
        }

        private void tsmiIntercambioAlmacenes_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctIncAlmacenes());
        }

        private void tsmiOrdenesCompra_Click(object sender, RoutedEventArgs e)
        {
            // Utilidades.Dialogos.MostrarDialogos(this, new ZctOrdCompra());

            Utilidades.Dialogos.MostrarDialogos(this,new  OrdenesCompra.OrdenesCompra(0));
        }

        private void tsmiOrdenesServicio_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctOrdServicio());
        }

        private void tsmiProveedores_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionProveedoresForm());
            //var fProveedores = new ResTotal.Vista.ZctCatProv(ZctSOT.Datos.DAO.Conexion.CadenaConexion(), ControladorBase.UsuarioActual.Id);

            //fProveedores.BuscaProveedor += BuscaProveedor;
            //try
            //{
            //    Utilidades.Dialogos.MostrarDialogos(this, fProveedores);
            //}
            //finally
            //{
            //    fProveedores.BuscaProveedor -= BuscaProveedor;
            //}
        }

        //private int BuscaProveedor()
        //{
        //    var fBusqueda = new ZctBusqueda
        //    {
        //        sSPName = "SP_ZctCatProv",
        //        sSpVariables = "@Cod_Prov;INT|@Nom_Prov;VARCHAR|@RFC_Prov;VARCHAR|@Dir_Prov;VARCHAR",
        //    };

        //    Utilidades.Dialogos.MostrarDialogos(this, fBusqueda);

        //    return int.Parse(fBusqueda.iValor ?? "0");
        //}

        private void tsmiFichas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionFichasForm());
        }

        private void tsmiPuestos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionPuestosForm());
        }

        private void tsmiRespaldar_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctRespaldo());
        }

        private void tsmiFinalizarRevisionCorte_Click(object sender, RoutedEventArgs e)
        {/*
            var controladorCorteTurno = new ControladorCortesTurno();

            var corte = controladorCorteTurno.ObtenerCorteEnRevision();

            if (corte != null && MessageBox.Show(Textos.Mensajes.confirmar_finalizar_revision_corte, Textos.TitulosVentanas.Aviso, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                controladorCorteTurno.CerrarTurno();

                MessageBox.Show(Textos.Mensajes.finalizacion_revision_corte_exitosa, Textos.TitulosVentanas.Aviso, MessageBoxButton.OK, MessageBoxImage.Information);
            }*/
        }

        private void tsmiPagoProveedores_Click(object sender, RoutedEventArgs e)
        {
            var CxP = new global::ResTotal.Vista.CxP_principal(ZctSOT.Datos.DAO.Conexion.CadenaConexion(), ControladorBase.UsuarioActual.Id, "CJ");
            CxP.cambiaproveedor += ModificaProveedor;
            Utilidades.Dialogos.MostrarDialogos(this, CxP);
        }

        private void tsmiPagoProveedoresEstatus_Click(object sender, RoutedEventArgs e)
        {
            var CxP = new global::ResTotal.Vista.CxP_PorEstado(ZctSOT.Datos.DAO.Conexion.CadenaConexion(), ControladorBase.UsuarioActual.Id, "CJ");
            CxP.cambiaproveedor += ModificaProveedor;
            Utilidades.Dialogos.MostrarDialogos(this, CxP);
        }

        private void ModificaProveedor(int Cod_prov)
        {
            var frm = new global::ResTotal.Vista.ZctCatProv(ZctSOT.Datos.DAO.Conexion.CadenaConexion(), ControladorBase.UsuarioActual.Id);

            frm.CargaProveedor(Cod_prov);
            Utilidades.Dialogos.MostrarDialogos(this, frm);
        }

        private void tsmiInsumosFIFO_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionEtiquetas());
        }



        private void tsmiPresentaciones_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionPresentacionesForm());
        }

        private void tsmiHistorialMantenimientos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new Mantenimientos.HistorialMantenimientoForm());
        }

        private void tsmiAlmacenes_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionAlmacenesForm());
        }

        private void tsmiDatosFiscales_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctClientes());
        }

        private void tsmiOrdenesTrabajoMotos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctOrdTrabajoMot());
        }

        private void tsmiArticulosVendidos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionArticulosVendidosForm());
        }

        private void tsmiRadiografiaHotel_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new RadiografiaHotelForm());
        }

        private void tsmiComandas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new EstadosComandasForm());
        }

        private void tsmiKardex_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new KardexForm());
        }

        private void tsmiRevisionTurnos_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                contenedorH.TimerHabilitado = false;
                Utilidades.Dialogos.MostrarDialogos(this, new SelectorCortesRevisionForm());
            }
            finally
            {
                contenedorH.TimerHabilitado = true;
            }
        }

        private void tsmiGestionarTarjetas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionTarjetasPuntosForm());
        }

        //private void tsmiReimpresiones_Click(object sender, RoutedEventArgs e)
        //{
        //    Utilidades.Dialogos.MostrarDialogos(this, new Reimpresiones());
        //}

        private void tsmiVentas_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConsultaVentasForm());
        }

        private void tsmiCategorias_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionDepartamentosForm());
        }

        private void tsmiSubategorias_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionLineasForm());
        }

        private void tsmiPresupuestos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ZctMainPresupuesto());
        }

        private void DEBUG_tsmiRecargarPorGrupo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void tsmiReporteInventarioActual_Click(object sender, RoutedEventArgs e)
        {
          /*  ZctShowRpt fShowRpt = new ZctShowRpt();
            // 1 para inventarios

            fShowRpt.FijarReporte(1, 1);
            //fShowRpt.iCodCatego = 1;
            //fShowRpt.CboReportes.Enabled = false;
            //fShowRpt.CboReportes.SelectedIndex = 1;
            //fShowRpt.CargarParametros();
            fShowRpt.ShowDialog();*/
            Utilidades.Dialogos.MostrarDialogos(this, new ReporteInventarioActual());
        }

        private void tsmiClientes_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new SOTWpf.Clientes.ClientePorMatricula());
        }

        private void PermissibleMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new GestionArticulosForm(true));
        }

        private void CosumosInternos_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new ConsultaConsumosInternosEmpleado());
        }

        private void CortesiasyCancelaciones_Click(object sender, RoutedEventArgs e)
        {

            Utilidades.Dialogos.MostrarDialogos(this, new CortesCortesiasCancelaciones());
        }

        private void ConfiguracionMedidiores_Click(object sender, RoutedEventArgs e)
        {
            Utilidades.Dialogos.MostrarDialogos(this, new Mantenimientos.MantenimientoLecturasHotel());
        }
    }
}
