﻿using CrystalDecisions.CrystalReports.Engine;
using Modelo.Dtos;
using Negocio.Tickets;
using SOTWpf.Impresiones;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTWpf.Tickets
{
    public class ImpresoraTickets : Impresora, IImpresoraTickets
    {
        //public void ImprimirTicket(DtoResumenVenta resumen, string nombreImpresora, int copias)
        //{
        //    using (ReportClass documentoReporte = resumen.EsComanda ? (ReportClass)new FormatoTicketComanda() : new FormatoTicketGeneral())
        //    {
        //        documentoReporte.Load();

        //        documentoReporte.SetDataSource(new List<DtoResumenVenta> { resumen });

        //        documentoReporte.Subreports["Conceptos"].SetDataSource(resumen.Items);
        //        if (!resumen.EsComanda)
        //            documentoReporte.Subreports["Empleados"].SetDataSource(resumen.Empleados);

        //        try
        //        {
        //            Imprimir(documentoReporte, nombreImpresora, copias);
        //        }
        //        finally
        //        {
        //            try
        //            {
        //                documentoReporte.Close();
        //            }
        //            catch { }
        //        }
        //    }
        //}

        private static int c1 = 50;
        private static int c2 = 146;
        private static int c3 = 84;

        public void ImprimirTicket(DtoResumenVenta resumen, string nombreImpresora, int copias)
        {
            //using (ReportClass documentoReporte = resumen.EsComanda ? (ReportClass)new FormatoTicketComanda() : new FormatoTicketGeneral())
            //{

            try
            {
                for (int i = 0; i < copias; i++)
                {
                    using (var printFont = new Font("Arial", 9))
                    {
                        using (var pd = new PrintDocumentTicket(resumen, printFont))
                        {
                            try
                            {
                                pd.PrinterSettings.PrinterName = nombreImpresora;

                                var psize = new PaperSize("CUSTOM", 280, 1000);

                                pd.PrintController = new StandardPrintController();
                                pd.PrinterSettings.DefaultPageSettings.PaperSize = psize;
                                pd.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
                                pd.DefaultPageSettings.PaperSize = psize;
                                pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

                                pd.PrinterSettings.Copies = 1;
                                pd.PrintPage += this.pd_PrintPage;
                                pd.Print();

                            }
                            finally
                            {
                                if (pd != null)
                                    pd.PrintPage -= this.pd_PrintPage;
                                //streamToPrint.Close();
                            }
                        }
                    }
                }
            }
            finally
            {
                //try
                //{
                //    documentoReporte.Close();
                //}
                //catch { }
            }
            //}
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            var pd = sender as PrintDocumentTicket;

            if (pd == null)
            {
                ev.HasMorePages = false;
                return;
            }

            //float linesPerPage = 0;
            float yPos = 0;
            //int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            //string line = null;

            // Calculate the number of lines per page.
            //linesPerPage = ev.MarginBounds.Height /
            //   printFont.GetHeight(ev.Graphics);

            // (int)(ev.MarginBounds.Width / pd.Fuente.GetHeight(ev.Graphics));

            float startX = leftMargin;
            float startY = topMargin;
            float Offset = 0;

            int anchura = ev.MarginBounds.Width;

            //if ((int)ev.Graphics.MeasureString("m", pd.Fuente).Width > 0)
            //{

            //    pd.CaracteresPorLinea =
            //        (int)(anchura * 2 / ev.Graphics.MeasureString("m", pd.Fuente).Width);

            //    var prueba = (int)ev.Graphics.MeasureString(new string('m', pd.CaracteresPorLinea), pd.Fuente).Width;
            //}


            //var tamF = pd.Fuente.GetHeight();

            var tamF = pd.Fuente.GetHeight();

            SizeF layoutSize;// = new SizeF(anchura, tamF);
            RectangleF layout;// = new RectangleF(new PointF(startX, startY + Offset), layoutSize);

            //var tama = ev.Graphics.MeasureString("PRUEBA", pd.Fuente, pd.CaracteresPorLinea, new StringFormat(StringFormatFlags.NoClip));

            //pd.CaracteresPorLinea = 76 / ((int)tama.Width / ("PRUEBA").Length);
            //pd.CaracteresPorLinea = 76;
            // Print each line of the file.

            //yPos = topMargin + (count *
            //       pd.Fuente.GetHeight(ev.Graphics));

            //var linesPerPage = ev.MarginBounds.Height / pd.Fuente.GetHeight(ev.Graphics);

            if (!pd.Procesado)
                pd.ProcesarTabla(ev);

            Tabla tabla;
            bool continuar = true;

            while (continuar && (tabla = pd.ObtenerTablaActual()) != null)
            //while (((line = pd.ReadLine()) != null))
            {
                //tama = ev.Graphics.MeasureString(linea.Item1, pd.Fuente, pd.CaracteresPorLinea, linea.Item2);

                var maximoFilas = tabla.Columnas.Max(m => m.Celdas.Count);

                for (int cel = pd.CeldaActual; cel < maximoFilas; cel++)
                {
                    float tmpOffset = Offset;
                    float maxOffset = Offset;
                    startX = 0;

                    var maximoLineas = tabla.Columnas.Where(m=> m.Celdas.Count > cel).Select(m => m.Celdas[cel]).Max(m => m.Lineas.Count);

                    if ((Offset + (maximoLineas * tamF)) > ev.MarginBounds.Height)
                    {
                        pd.CeldaActual = cel;
                        continuar = false;
                        break;
                    }

                    for (int col = 0; col < tabla.Columnas.Count; col++)
                    {
                        tmpOffset = Offset;

                        if (tabla.Columnas[col].Celdas.Count > cel)
                        {

                            for (int l = 0; l < tabla.Columnas[col].Celdas[cel].Lineas.Count; l++)
                            {
                                layoutSize = new SizeF(tabla.Columnas[col].Anchura, tamF);
                                layout = new RectangleF(new PointF(startX, startY + tmpOffset), layoutSize);

                                ev.Graphics.DrawString(tabla.Columnas[col].Celdas[cel].Lineas[l].Item1, pd.Fuente, Brushes.Black, layout, tabla.Columnas[col].Celdas[cel].Lineas[l].Item2);

                                tmpOffset += tamF;
                            }
                        }

                        startX += tabla.Columnas[col].Anchura;
                        maxOffset = Math.Max(maxOffset, tmpOffset);
                    }

                    Offset = maxOffset;
                }

                //yPos += tama.Height;

                //count++;
                if (continuar)
                {
                    pd.CeldaActual = 0;
                    pd.Avanzar();
                }
            }

            //If more lines exist, print another page.
            if (pd.ObtenerTablaActual() != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false;
        }

        internal class PrintDocumentTicket : PrintDocument
        {
            internal DtoResumenVenta Resumen { get; private set; }
            internal Font Fuente { get; private set; }
            //internal int CaracteresPorLinea { get; set; }

            //List<Tuple<string, StringFormat>> lineas;

            //lista de arrays de listas de tuplas de strigs y sus formatos, la lista principal
            //sería el ticket, las sublistas las secciones, cada item del array una columna y cada item de una lista
            //en una posición x del array una fila de esa columna
            List<Tabla> tablas;
            int indexTabla = 0;
            internal bool Procesado { get; private set; }
            internal int CeldaActual { get; set; }

            StringFormat formatLeft;
            StringFormat formatCenter;
            StringFormat formatRight;

            internal PrintDocumentTicket(DtoResumenVenta resumen, Font fuente)
                : base()
            {
                Resumen = resumen;
                Fuente = fuente;
                //CaracteresPorLinea = caracteresPorLinea;

                formatLeft = new StringFormat(StringFormatFlags.NoClip);
                formatCenter = new StringFormat(formatLeft);
                formatRight = new StringFormat(formatLeft);

                formatCenter.Alignment = StringAlignment.Center;
                formatLeft.Alignment = StringAlignment.Near;
                formatRight.Alignment = StringAlignment.Far;
            }


            internal void ProcesarTabla(PrintPageEventArgs ev)
            {
                indexTabla = 0;

                Procesado = true;

                if (Resumen == null)
                {
                    return;
                    //return new List<Tabla>()
                    //{
                    //    new Tabla
                    //    {
                    //        Columnas = new List<Columna>()
                    //        { 
                    //            new Columna(0)
                    //            { 
                    //                Anchura = ev.MarginBounds.Width,
                    //                Celdas = new List<Celda>()
                    //                {
                    //                    new Celda
                    //                    {
                    //                         Lineas= new List<Tuple<string, StringFormat>>(){ new Tuple<string, StringFormat>("NADA QUE IMPRIMIR", formatCenter) }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //};
                }
                //var lineas = new List<Tuple<string, StringFormat>>();
                tablas = new List<Tabla>();
                //var sb = new StringBuilder();

                var caracteresFull = (int)(ev.MarginBounds.Width * 2 / ev.Graphics.MeasureString("m", Fuente).Width);

                #region Cabeceras
                Tabla tablaCabecera;

                if (!Resumen.EsComanda)
                {
                    tablaCabecera = new Tabla();
                    tablaCabecera.Columnas.Add(new Columna(caracteresFull) { Anchura = ev.MarginBounds.Width });
                    tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                    tablas.Add(tablaCabecera);

                    ProcesarCadena(Resumen.Hotel, tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Centro);
                    ProcesarCadena(Resumen.RazonSocial, tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Centro);
                    ProcesarCadena(Resumen.Direccion, tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Centro);
                    ProcesarCadena("RFC: " + Resumen.RFC, tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Centro);
                }

                #endregion
                #region Cabeceras de la venta

                //lineas = new List<Tuple<string, StringFormat>>();
                tablaCabecera = new Tabla();
                tablaCabecera.Columnas.Add(new Columna(caracteresFull) { Anchura = ev.MarginBounds.Width });
                tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                //tablaCabecera.Filas.Add(0, lineas);
                tablas.Add(tablaCabecera);

                ProcesarCadena("Ticket - " + Resumen.Ticket, tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Derecha);
                ProcesarCadena(Resumen.Procedencia, tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull);
                ProcesarCadena("Fecha: " + Resumen.FechaInicio.ToString("dd/MM/yyyy hh:mm:ss tt"), tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull);

                if (Resumen.Cancelada)
                    ProcesarCadena("CANCELADO", tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Centro);

                #endregion
                #region Cuerpo

                #region linea 1

                tablaCabecera = new Tabla();
                tablaCabecera.Columnas.Add(new Columna(caracteresFull) { Anchura = ev.MarginBounds.Width });
                tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                tablas.Add(tablaCabecera);

                tablaCabecera.Columnas[0].Celdas[0].Lineas.Add(new Tuple<string, StringFormat>(new string('*', tablaCabecera.Columnas[0].CaracteresPorLinea * 2), formatCenter));

                #endregion
                #region Títulos

                tablaCabecera = new Tabla();

                var anch = c1;
                var caracteres = (int)(anch * 2 / ev.Graphics.MeasureString("m", Fuente).Width);

                tablaCabecera.Columnas.Add(new Columna(caracteres) { Anchura = anch });

                anch = c2;
                caracteres = (int)(anch * 2 / ev.Graphics.MeasureString("m", Fuente).Width);

                tablaCabecera.Columnas.Add(new Columna(caracteres) { Anchura = anch });

                anch = c3;
                caracteres = (int)(anch * 2 / ev.Graphics.MeasureString("m", Fuente).Width);

                tablaCabecera.Columnas.Add(new Columna(caracteres) { Anchura = anch });
                //tablaCabecera.Filas.Add(0, lineas);
                tablas.Add(tablaCabecera);

                tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                tablaCabecera.Columnas[1].Celdas.Add(new Celda());
                tablaCabecera.Columnas[2].Celdas.Add(new Celda());

                ProcesarCadena("Cant.", tablaCabecera.Columnas[0].Celdas[0].Lineas, tablaCabecera.Columnas[0].CaracteresPorLinea);
                ProcesarCadena("Concepto", tablaCabecera.Columnas[1].Celdas[0].Lineas, tablaCabecera.Columnas[1].CaracteresPorLinea);
                ProcesarCadena("Precio", tablaCabecera.Columnas[2].Celdas[0].Lineas, tablaCabecera.Columnas[2].CaracteresPorLinea, Posiciones.Centro);

                #endregion
                #region linea 2

                tablaCabecera = new Tabla();
                tablaCabecera.Columnas.Add(new Columna(caracteresFull) { Anchura = ev.MarginBounds.Width });
                tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                tablas.Add(tablaCabecera);

                tablaCabecera.Columnas[0].Celdas[0].Lineas.Add(new Tuple<string, StringFormat>(new string('*', tablaCabecera.Columnas[0].CaracteresPorLinea * 2), formatCenter));
                #endregion

                tablaCabecera = new Tabla();

                anch = c1;
                caracteres = (int)(anch * 2 / ev.Graphics.MeasureString("m", Fuente).Width);

                tablaCabecera.Columnas.Add(new Columna(caracteres) { Anchura = anch });

                anch = c2;
                caracteres = (int)(anch * 2 / ev.Graphics.MeasureString("m", Fuente).Width);

                tablaCabecera.Columnas.Add(new Columna(caracteres) { Anchura = anch });

                anch = c3;
                caracteres = (int)(anch * 2 / ev.Graphics.MeasureString("m", Fuente).Width);

                tablaCabecera.Columnas.Add(new Columna(caracteres) { Anchura = anch });
                //tablaCabecera.Filas.Add(0, lineas);
                tablas.Add(tablaCabecera);

                int indexItem = 0;

                foreach (var articulo in Resumen.Items)
                {
                    tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                    tablaCabecera.Columnas[1].Celdas.Add(new Celda());
                    tablaCabecera.Columnas[2].Celdas.Add(new Celda());

                    ProcesarCadena(articulo.Cantidad.ToString(), tablaCabecera.Columnas[0].Celdas[indexItem].Lineas, tablaCabecera.Columnas[0].CaracteresPorLinea);
                    ProcesarCadena(articulo.Nombre, tablaCabecera.Columnas[1].Celdas[indexItem].Lineas, tablaCabecera.Columnas[1].CaracteresPorLinea);
                    ProcesarCadena(articulo.Total.ToString("C2"), tablaCabecera.Columnas[2].Celdas[indexItem].Lineas, tablaCabecera.Columnas[2].CaracteresPorLinea, Posiciones.Derecha);

                    indexItem++;
                }

                #endregion
                #region Totales

                if (!Resumen.EsComanda)
                {
                    tablaCabecera.Columnas[1].Celdas.Add(new Celda());
                    tablaCabecera.Columnas[2].Celdas.Add(new Celda());

                    ProcesarCadena("Subtotal +", tablaCabecera.Columnas[1].Celdas[indexItem].Lineas, tablaCabecera.Columnas[1].CaracteresPorLinea, Posiciones.Derecha);
                    ProcesarCadena(Resumen.SubTotal.ToString("C2"), tablaCabecera.Columnas[2].Celdas[indexItem].Lineas, tablaCabecera.Columnas[2].CaracteresPorLinea, Posiciones.Derecha);

                    indexItem++;

                    tablaCabecera.Columnas[1].Celdas.Add(new Celda());
                    tablaCabecera.Columnas[2].Celdas.Add(new Celda());

                    ProcesarCadena("Cortesía -", tablaCabecera.Columnas[1].Celdas[indexItem].Lineas, tablaCabecera.Columnas[1].CaracteresPorLinea, Posiciones.Derecha);
                    ProcesarCadena(Resumen.Cortesia.ToString("C2"), tablaCabecera.Columnas[2].Celdas[indexItem].Lineas, tablaCabecera.Columnas[2].CaracteresPorLinea, Posiciones.Derecha);

                    indexItem++;

                    tablaCabecera.Columnas[1].Celdas.Add(new Celda());
                    tablaCabecera.Columnas[2].Celdas.Add(new Celda());

                    ProcesarCadena("Descuento -", tablaCabecera.Columnas[1].Celdas[indexItem].Lineas, tablaCabecera.Columnas[1].CaracteresPorLinea, Posiciones.Derecha);
                    ProcesarCadena(Resumen.Descuentos.ToString("C2"), tablaCabecera.Columnas[2].Celdas[indexItem].Lineas, tablaCabecera.Columnas[2].CaracteresPorLinea, Posiciones.Derecha);

                    indexItem++;

                    tablaCabecera.Columnas[1].Celdas.Add(new Celda());
                    tablaCabecera.Columnas[2].Celdas.Add(new Celda());

                    ProcesarCadena("TOTAL", tablaCabecera.Columnas[1].Celdas[indexItem].Lineas, tablaCabecera.Columnas[1].CaracteresPorLinea, Posiciones.Derecha);
                    ProcesarCadena(Resumen.Total.ToString("C2"), tablaCabecera.Columnas[2].Celdas[indexItem].Lineas, tablaCabecera.Columnas[2].CaracteresPorLinea, Posiciones.Derecha);

                    #region linea 1

                    tablaCabecera = new Tabla();
                    tablaCabecera.Columnas.Add(new Columna(caracteresFull) { Anchura = ev.MarginBounds.Width });
                    tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                    tablas.Add(tablaCabecera);

                    tablaCabecera.Columnas[0].Celdas[0].Lineas.Add(new Tuple<string, StringFormat>(new string('*', tablaCabecera.Columnas[0].CaracteresPorLinea * 2), formatCenter));

                    #endregion
                }
                #endregion
                #region Pie

                if (!Resumen.EsComanda)
                {
                    //lineas = new List<Tuple<string, StringFormat>>();
                    tablaCabecera = new Tabla();
                    tablaCabecera.Columnas.Add(new Columna(caracteresFull) { Anchura = ev.MarginBounds.Width });
                    tablaCabecera.Columnas[0].Celdas.Add(new Celda());
                    //tablaCabecera.Filas.Add(0, lineas);
                    tablas.Add(tablaCabecera);

                    ProcesarCadena("Si requiere factura solicítela a recepción previo a su salida presentando su RFC", tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Centro);
                    if (Resumen.Empleados.Count > 1)
                        ProcesarCadena("Le atendieron:", tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull);
                    else if (Resumen.Empleados.Count == 1)
                        ProcesarCadena("Le atendió:", tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull);

                    foreach (var empleado in Resumen.Empleados)
                        ProcesarCadena(empleado.Puesto + " - " + empleado.NombreEmpleado, tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull);

                    ProcesarCadena("¡GRACIAS POR SU PREFERENCIA!", tablaCabecera.Columnas[0].Celdas[0].Lineas, caracteresFull, Posiciones.Centro);
                }

                #endregion

                //#region Cabeceras

                //lineas.Add(new Tuple<string, StringFormat>(Resumen.Hotel, formatCenter));
                //lineas.Add(new Tuple<string, StringFormat>(Resumen.RazonSocial, formatCenter));
                //lineas.Add(new Tuple<string, StringFormat>(Resumen.Direccion, formatCenter));
                //lineas.Add(new Tuple<string, StringFormat>("RFC: " + Resumen.RFC, formatCenter));

                //#endregion
                //#region Cabeceras de la venta

                //lineas.Add(new Tuple<string, StringFormat>("Ticket - " + Resumen.Ticket, formatRight));
                //lineas.Add(new Tuple<string, StringFormat>(Resumen.Procedencia, formatLeft));
                //lineas.Add(new Tuple<string, StringFormat>(Resumen.FechaInicio.ToString("dd/MM/yyyy hh:mm:ss tt"), formatLeft));

                //#endregion
                //#region Pie

                //lineas.Add(new Tuple<string, StringFormat>("Si requiere factura solicítela a recepción previo a su salida presentando su RFC", formatCenter));
                //if (Resumen.Empleados.Count > 1)
                //    lineas.Add(new Tuple<string, StringFormat>("Le atendieron:", formatLeft));
                //else if (Resumen.Empleados.Count == 1)
                //    lineas.Add(new Tuple<string, StringFormat>("Le atendió:", formatLeft));

                //foreach (var empleado in Resumen.Empleados)
                //    lineas.Add(new Tuple<string, StringFormat>(empleado.Puesto + " - " + empleado.NombreEmpleado, formatLeft));

                //lineas.Add(new Tuple<string, StringFormat>("¡GRACIAS POR SU PREFERENCIA!", formatLeft));

                //#endregion

                //return tablas;
            }

            internal void Avanzar() 
            {

                CeldaActual = 0;

                if (tablas == null || indexTabla >= tablas.Count)
                    return;

                indexTabla++;
            }

            internal Tabla ObtenerTablaActual()
            {
                if (tablas == null || indexTabla >= tablas.Count)
                    return null;

                return tablas[indexTabla];
            }

            private void ProcesarCadena(string cadena, List<Tuple<string, StringFormat>> lineas, int caracteres, Posiciones posicion = Posiciones.Izquierda)
            {
                if (!string.IsNullOrWhiteSpace(cadena))
                {
                    var matriz = cadena.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    int inicio = 0, fin, longitudActual;

                    while (inicio < matriz.Length)
                    {

                        fin = inicio;

                        longitudActual = matriz[inicio].Length;

                        for (int i = inicio + 1; i < matriz.Length; i++)
                        {
                            var longitudExtra = matriz[i].Length + 1;

                            if (longitudActual + longitudExtra > caracteres)
                            {
                                break;
                            }
                            else
                            {
                                longitudActual += longitudExtra;
                                fin = i;
                            }
                        }

                        AcomodarCadena(string.Join(" ", matriz.Skip(inicio).Take(fin - inicio + 1)), lineas, caracteres, posicion);
                        inicio = fin + 1;
                    }


                }
            }

            private void AcomodarCadena(string cadena, List<Tuple<string, StringFormat>> lineas, int caracteres, Posiciones posicion)
            {
                if (cadena.Length > caracteres)
                {
                    StringFormat formato = null;

                    switch (posicion)
                    {
                        case Posiciones.Centro:
                            formato = formatCenter;
                            break;
                        case Posiciones.Derecha:
                            formato = formatRight;
                            break;
                        case Posiciones.Izquierda:
                            formato = formatLeft;
                            break;
                    }

                    lineas.Add(new Tuple<string, StringFormat>(cadena.Substring(0, caracteres), formato ?? formatLeft));
                    AcomodarCadena(cadena.Substring(caracteres), lineas, caracteres, posicion);
                }
                else
                    switch (posicion)
                    {
                        case Posiciones.Izquierda:
                            lineas.Add(new Tuple<string, StringFormat>(cadena, formatLeft));
                            break;
                        case Posiciones.Centro:
                            {
                                lineas.Add(new Tuple<string, StringFormat>(cadena, formatCenter));
                                //if (cadena.Length == CaracteresPorLinea)
                                //    lineas.Add(new Tuple<string, StringFormat>(cadena, formatCenter));
                                //else
                                //{
                                //    int inicio = (CaracteresPorLinea - cadena.Length) / 2;
                                //    lineas.Add(new string(' ', inicio) + cadena);
                                //}
                            }
                            break;
                        case Posiciones.Derecha:
                            {
                                lineas.Add(new Tuple<string, StringFormat>(cadena, formatRight));
                                //if (cadena.Length == CaracteresPorLinea)
                                //    lineas.Add(cadena);
                                //else
                                //    lineas.Add(new string(' ', CaracteresPorLinea - cadena.Length) + cadena);
                            }
                            break;
                    }
            }

            private enum Posiciones
            {
                Izquierda = 0,
                Centro = 1,
                Derecha = 2
            }
        }

        internal class Tabla
        {
            internal Tabla()
            {
                Columnas = new List<Columna>();
            }

            internal List<Columna> Columnas { get; set; }
        }

        internal class Columna
        {
            internal Columna(int caracteresPorLinea)
            {
                Celdas = new List<Celda>();
                CaracteresPorLinea = caracteresPorLinea;
            }

            internal int Anchura { get; set; }
            internal int CaracteresPorLinea { get; private set; }

            internal List<Celda> Celdas { get; set; }
        }


        internal class Celda
        {
            internal Celda()
            {
                Lineas = new List<Tuple<string, StringFormat>>();
            }

            internal List<Tuple<string, StringFormat>> Lineas { get; set; }
        }
    }
}
