﻿Imports System.Configuration
Imports ZctSQL

Namespace DAO
    Public Class ZctDAOInvMotos
        Public Sub New()
            _Conn = DAO.Conexion.CadenaConexion 'String.Format(ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4")
        End Sub

        Private _Conn As String

        Public Sub SetInvMotos(ByVal Cod_Folio As String, ByVal Cod_Cte As String, ByVal Lista As SortedList(Of String, Entidades.zctInvMotosHist))
            Dim BD As New ZctDB
            Try
                BD.GetCadenaConexion = _Conn
                BD.Conectar()

                Dim ssql As String = ""

                BD.ComenzarTransaccion()
                Dim OcFolio As New zctFolios("IM")
                Cod_Folio = OcFolio.SetConsecutivo(BD)
                ssql = "EXECUTE SP_ZctInvEncHistMot '" & Cod_Folio & "', '" & Cod_Cte & "'"
                BD.CrearComando(ssql)
                BD.EjecutarComando()
                For Each Moto As Entidades.zctInvMotosHist In Lista.Values
                    '"EXECUTE SP_ZctInvMotosHist @Cod_Inv , @Cod_Moto, @Cod_Accion,  @Observaciones , @Cod_Usu "


                    Select Case Moto.Accion


                        Case Entidades.EnumAccionesMotHist.Agregar
                            '"EXECUTE SP_ZctInvDetMovMot @Cod_Inv, @Cod_Moto, @Tp_Mov, @Cod_Cte, @Valor_Mov"
                            BD.CrearComando(String.Format("EXECUTE SP_ZctCatMot 2, '{0}', '{1}', '{2}', {3}, {4}, '{5}', '{6}', '{7}', '{8}'", Moto.Cod_Mot, Moto.Vin_Mot, Moto.Cod_Cte, IIf(Moto.Cod_Mar = 0, "NULL", Moto.Cod_Mar), IIf(Moto.CodMod_Mot = 0, "NULL", Moto.CodMod_Mot), Moto.Placas_Mot, Moto.Motor_Mot, Moto.Anno_Mot, Moto.Cod_Ubicacion))
                            BD.EjecutarComando()
                            Moto.Cod_Mot = GetCodMoto(Moto.Vin_Mot, BD)
                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvMotosHist '{0}' , '{1}', '{2}',  '{3}' , '{4}', '{5}', '{6}' ", Cod_Folio, Moto.Cod_Mot, CInt(Moto.Accion), Moto.Observaciones, Moto.Cod_Usu, CInt(Moto.Estatus), Moto.Cod_Ubicacion))
                            BD.EjecutarComando()

                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvDetMovMot '{0}', '{1}', 'EN', '{2}', 1", Cod_Folio, Moto.Cod_Mot, Moto.Cod_Cte))
                            BD.EjecutarComando()
                            '"EXECUTE SP_ZctCatMot 2, @Cod_Mot, @Vin_Mot, @Cod_Cte, @Cod_Mar, @CodMod_Mot, @Placas_Mot, @Motor_Mot, @Anno_Mot"


                        Case Entidades.EnumAccionesMotHist.Cambiar_Cliente

                            BD.CrearComando(String.Format("EXECUTE SP_ZctCatMot 2, '{0}', '{1}', '{2}', {3}, {4}, '{5}', '{6}', '{7}', '{8}'", Moto.Cod_Mot, Moto.Vin_Mot, Moto.Cod_Cte, IIf(Moto.Cod_Mar = 0, "NULL", Moto.Cod_Mar), IIf(Moto.CodMod_Mot = 0, "NULL", Moto.CodMod_Mot), Moto.Placas_Mot, Moto.Motor_Mot, Moto.Anno_Mot, Moto.Cod_Ubicacion))
                            BD.EjecutarComando()

                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvMotosHist '{0}' , '{1}', '{2}',  '{3}' , '{4}', '{5}', '{6}' ", Cod_Folio, Moto.Cod_Mot, CInt(Moto.Accion), Moto.Observaciones, Moto.Cod_Usu, CInt(Moto.Estatus), Moto.Cod_Ubicacion))
                            BD.EjecutarComando()

                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvDetMovMot '{0}', '{1}', 'ST', '{2}', -1", Cod_Folio, Moto.Cod_Mot, Moto.CteCambio))
                            BD.EjecutarComando()
                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvDetMovMot '{0}', '{1}', 'ET', '{2}', 1", Cod_Folio, Moto.Cod_Mot, Moto.Cod_Cte))
                            BD.EjecutarComando()



                        Case Entidades.EnumAccionesMotHist.Eliminar

                            BD.CrearComando(String.Format("EXECUTE SP_ZctCatMot 2, '{0}', '{1}', {2}, {3}, {4}, '{5}', '{6}', '{7}', '{8}'", Moto.Cod_Mot, Moto.Vin_Mot, "NULL", IIf(Moto.Cod_Mar = 0, "NULL", Moto.Cod_Mar), IIf(Moto.CodMod_Mot = 0, "NULL", Moto.CodMod_Mot), Moto.Placas_Mot, Moto.Motor_Mot, Moto.Anno_Mot, Moto.Cod_Ubicacion))

                            BD.EjecutarComando()

                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvMotosHist '{0}' , '{1}', '{2}',  '{3}' , '{4}', '{5}', '{6}' ", Cod_Folio, Moto.Cod_Mot, CInt(Moto.Accion), Moto.Observaciones, Moto.Cod_Usu, CInt(Moto.Estatus), Moto.Cod_Ubicacion))
                            BD.EjecutarComando()

                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvDetMovMot '{0}', '{1}', 'SA', '{2}', -1", Cod_Folio, Moto.Cod_Mot, Moto.Cod_Cte))
                            BD.EjecutarComando()
                        Case Entidades.EnumAccionesMotHist.No_Accion
                            BD.CrearComando(String.Format("EXECUTE SP_ZctCatMot 2, '{0}', '{1}', '{2}', {3}, {4}, '{5}', '{6}', '{7}', '{8}'", Moto.Cod_Mot, Moto.Vin_Mot, Moto.Cod_Cte, IIf(Moto.Cod_Mar = 0, "NULL", Moto.Cod_Mar), IIf(Moto.CodMod_Mot = 0, "NULL", Moto.CodMod_Mot), Moto.Placas_Mot, Moto.Motor_Mot, Moto.Anno_Mot, Moto.Cod_Ubicacion))
                            BD.EjecutarComando()

                            BD.CrearComando(String.Format("EXECUTE SP_ZctInvMotosHist '{0}' , '{1}', '{2}',  '{3}' , '{4}', '{5}' ", Cod_Folio, Moto.Cod_Mot, CInt(Moto.Accion), Moto.Observaciones, Moto.Cod_Usu, CInt(Moto.Estatus)))
                            BD.EjecutarComando()

                    End Select

                Next


                BD.ConfirmarTransaccion()
            Catch ex As BaseDatosException
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Sub

        Public Function GetMotos(ByVal Cod_Folio As String, ByVal Cod_Cte As String) As SortedList(Of String, Entidades.zctInvMotosHist)
            'Dim ssql As String = String.Format("SELECT Cod_Mot, Vin_Mot, Cod_Cte FROM ZctCatMot WHERE (Cod_Cte = '{0}')", Cod_Cte)
            Dim ssql As String = String.Format("SELECT '" & Cod_Folio & "'  as Cod_Inv , ZctCatMot.Cod_Mot, ZctCatMot.Vin_Mot, ZctCatMot.Cod_Cte, ZctCatMot.Cod_Mar, ZctCatMot.CodMod_Mot, ZctCatMot.Placas_Mot, ZctCatMot.Motor_Mot, ZctCatMot.Anno_Mot, ZctCatMar.Desc_Mar, ZctCatMod.Desc_Mod, ZctCatMot.Cod_Ubicacion FROM ZctCatMot LEFT OUTER JOIN ZctCatMod ON ZctCatMot.CodMod_Mot = ZctCatMod.CodMod_Mot LEFT OUTER JOIN ZctCatMar ON ZctCatMot.Cod_Mar = ZctCatMar.Cod_Mar  WHERE (Cod_Cte = '{0}')", Cod_Cte)

            Dim BD As New ZctDB
            Dim rs As SqlClient.SqlDataReader = Nothing
            Dim Lista As New SortedList(Of String, Entidades.zctInvMotosHist)

            Try
                BD.GetCadenaConexion = _Conn
                BD.Conectar()
                BD.CrearComando(ssql)
                rs = BD.EjecutarConsulta()
                Dim dt As New DataTable
                dt.Load(rs)
                For Each Row As DataRow In dt.Rows
                    If Not Lista.ContainsKey(Row("Vin_Mot").ToString) Then
                        Dim Elemento As New Entidades.zctInvMotosHist
                        Elemento.llenaDato(Row)
                        Lista.Add(Row("Vin_Mot").ToString, Elemento)
                    End If
                Next
                'While rs.Read
                '    'Lista.Add(rs("Vin_Mot").ToString, New Entidades.zctInvMotosHist(GetGeneric(Of Integer)(rs("Cod_Inv")), GetGeneric(Of Integer)(rs("Cod_Cte")), GetGeneric(Of Integer)(rs("Cod_Mot")), GetGeneric(Of Integer)(rs("Vin_Mot"))))
                'End While
                Return Lista
            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Function

        ''' <summary>
        ''' Obtiene las motocicletas de solo lectura
        ''' </summary>
        ''' <param name="Cod_Folio"></param>
        ''' <param name="Cod_Cte"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetMotos(ByVal Cod_Folio As String, ByRef Cod_Cte As Integer) As SortedList(Of String, Entidades.zctInvMotosHist)
            'Dim ssql As String = String.Format("SELECT Cod_Mot, Vin_Mot, Cod_Cte FROM ZctCatMot WHERE (Cod_Cte = '{0}')", Cod_Cte)
            Dim ssql As String = String.Format("SELECT ZctInvMotosHist.Cod_Inv, ZctInvMotosHist.Cod_Accion, ZctInvMotosHist.Cod_Estatus , ZctCatMot.Cod_Mot, ZctCatMot.Vin_Mot, ZctCatMot.Cod_Cte, ZctCatMot.Cod_Mar, ZctCatMot.CodMod_Mot, ZctCatMot.Placas_Mot, ZctCatMot.Motor_Mot, ZctCatMot.Anno_Mot, ZctCatMar.Desc_Mar, ZctCatMod.Desc_Mod, ZctCatMot.Cod_Ubicacion FROM ZctCatMot INNER JOIN ZctInvMotosHist ON ZctCatMot.Cod_Mot = ZctInvMotosHist.Cod_Moto LEFT OUTER JOIN ZctCatMod ON ZctCatMot.CodMod_Mot = ZctCatMod.CodMod_Mot LEFT OUTER JOIN ZctCatMar ON ZctCatMot.Cod_Mar = ZctCatMar.Cod_Mar WHERE (ZctInvMotosHist.Cod_Inv = '" & Cod_Folio & "') ")

            Dim BD As New ZctDB
            Dim rs As SqlClient.SqlDataReader = Nothing
            Dim Lista As New SortedList(Of String, Entidades.zctInvMotosHist)

            Try
                BD.GetCadenaConexion = _Conn
                BD.Conectar()
                BD.CrearComando(ssql)
                rs = BD.EjecutarConsulta()
                Dim dt As New DataTable
                dt.Load(rs)
                Cod_Cte = 0
                For Each Row As DataRow In dt.Rows
                    If Not Lista.ContainsKey(Row("Vin_Mot").ToString) Then
                        If Cod_Cte = 0 Then Cod_Cte = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Cte"))
                        Dim Elemento As New Entidades.zctInvMotosHist
                        Elemento.llenaDato(Row)
                        Elemento.Estatus = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Estatus"))
                        Elemento.Accion = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Accion"))
                        Lista.Add(Row("Vin_Mot").ToString, Elemento)
                    End If
                Next
                'While rs.Read
                '    'Lista.Add(rs("Vin_Mot").ToString, New Entidades.zctInvMotosHist(GetGeneric(Of Integer)(rs("Cod_Inv")), GetGeneric(Of Integer)(rs("Cod_Cte")), GetGeneric(Of Integer)(rs("Cod_Mot")), GetGeneric(Of Integer)(rs("Vin_Mot"))))
                'End While
                Return Lista
            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Function

        Public Function GetCteMoto(ByVal Vin As String) As Integer

            Dim ssql As String = "SELECT  ZctCatMot.Cod_Cte FROM ZctCatMot WHERE (ZctCatMot.Vin_Mot  = '" & Vin & "')"
            'Dim ssql As String = String.Format("SELECT Cod_Mot, Vin_Mot, Cod_Cte FROM ZctCatMot WHERE (Cod_Cte = '{0}')", Cod_Cte)

            Dim BD As New ZctDB
            Dim rs As SqlClient.SqlDataReader = Nothing
            Try
                BD.GetCadenaConexion = _Conn
                BD.Conectar()
                BD.CrearComando(ssql)
                rs = BD.EjecutarConsulta()
                If rs.Read Then
                    Return GetGeneric(Of Integer)(rs("Cod_Cte"))
                Else
                    Return 0
                End If
            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Function


        Public Function GetCodMoto(ByVal Vin As String) As Integer

            Dim ssql As String = "SELECT  ZctCatMot.Cod_Mot FROM ZctCatMot WHERE (ZctCatMot.Vin_Mot  = '" & Vin & "')"
            'Dim ssql As String = String.Format("SELECT Cod_Mot, Vin_Mot, Cod_Cte FROM ZctCatMot WHERE (Cod_Cte = '{0}')", Cod_Cte)

            Dim BD As New ZctDB
            Dim rs As SqlClient.SqlDataReader = Nothing
            Try
                BD.GetCadenaConexion = _Conn
                BD.Conectar()
                BD.CrearComando(ssql)
                rs = BD.EjecutarConsulta()
                If rs.Read Then
                    Return GetGeneric(Of Integer)(rs("Cod_Mot"))
                Else
                    Return 0
                End If
            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Function

        Public Function GetCodMoto(ByVal Vin As String, ByVal BD As ZctDB) As Integer

            Dim ssql As String = "SELECT  ZctCatMot.Cod_Mot FROM ZctCatMot WHERE (ZctCatMot.Vin_Mot  = '" & Vin & "')"
            'Dim ssql As String = String.Format("SELECT Cod_Mot, Vin_Mot, Cod_Cte FROM ZctCatMot WHERE (Cod_Cte = '{0}')", Cod_Cte)


            Dim rs As SqlClient.SqlDataReader = Nothing
            Try
                BD.CrearComando(ssql)
                rs = BD.EjecutarConsulta()
                If rs.Read Then
                    Return GetGeneric(Of Integer)(rs("Cod_Mot"))
                Else
                    Return 0
                End If
            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Finally
                If rs IsNot Nothing Then rs.Close()
            End Try
        End Function
    End Class
End Namespace