﻿Imports ZctSOT.Datos.Clases.Catalogos
Namespace DAO
    Namespace Catalogos
        Public Class ZctDAOMeses
            ''' <summary>
            ''' Obtiene las regiones
            ''' </summary>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Shared Function GetMeses() As List(Of ZctMeses)
                Dim BD As New DAO.ZctDAOGeneral
                Dim rsadd As SqlClient.SqlDataReader = Nothing
                Dim meses As New List(Of ZctMeses)
                Try
                    Dim SQL As String = "SELECT Mes, MesNombre FROM ZctMeses"
                    'Obtiene los datos
                    rsadd = BD.GetData(SQL)
                    While rsadd.Read
                        Dim mes As New ZctMeses
                        mes.Mes = ZctFunciones.GetGeneric(Of Integer)(rsadd("Mes"))
                        mes.NombreMes = rsadd("MesNombre").ToString
                        meses.Add(mes)
                    End While
                    Return meses
                Finally
                    BD.Dispose()
                End Try
            End Function
        End Class
    End Namespace
End Namespace

