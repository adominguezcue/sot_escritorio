﻿Imports ZctSOT.Datos.Clases.Catalogos
Namespace DAO
    Namespace Catalogos
        Public Class ZctDAORegion
            ''' <summary>
            ''' Obtiene las regiones
            ''' </summary>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Shared Function GetRegiones() As List(Of ZctRegion)
                Dim BD As New DAO.ZctDAOGeneral
                Dim rsadd As SqlClient.SqlDataReader = Nothing
                Dim regiones As New List(Of ZctRegion)
                Try


                    Dim SQL As String = "SELECT Cod_Region, Desc_Region FROM ZctCatRegion"
                    'Obtiene los datos
                    rsadd = BD.GetData(SQL)
                    While rsadd.Read
                        Dim region As New ZctRegion
                        region.Codigo = rsadd("Cod_Region").ToString
                        region.Descripcion = rsadd("Desc_Region").ToString
                        regiones.Add(region)
                    End While
                    Return regiones

                Finally
                    BD.Dispose()
                End Try
            End Function



        End Class
    End Namespace
End Namespace

