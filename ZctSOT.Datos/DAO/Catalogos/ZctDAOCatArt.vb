﻿
Namespace DAO
    Namespace Catalogos
        Public Class ZctDAOCatArt
            ''' <summary>
            ''' Obtiene el artículo
            ''' </summary>
            ''' <param name="Cod_Art">Código de artículo</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Function GetArticulo(ByVal Cod_Art As String) As Clases.Catalogos.ZctCatArt
                Dim BD As New DAO.ZctDAOGeneral
                Dim rsadd As SqlClient.SqlDataReader = Nothing
                Dim Val As New Clases.Catalogos.ZctCatArt
                Try


                    Dim SQL As String = "SELECT Cod_Art, Desc_Art, Prec_Art, Cos_Art, Cod_Mar, Exist_Art, Cod_Linea, Cod_Dpto, Activo_Art, Cod_TpArt FROM ZctCatArt WHERE (Cod_Art = '" & Cod_Art & "')"
                    'Obtiene los datos
                    rsadd = BD.GetData(SQL)
                    If rsadd.Read Then
                        Val.Cod_Art = rsadd("Cod_Art").ToString
                        Val.Desc_Art = rsadd("Desc_Art").ToString
                        Val.Prec_Art = ZctFunciones.GetGeneric(Of Double)(rsadd("Prec_Art"))
                        Val.Cos_Art = ZctFunciones.GetGeneric(Of Double)(rsadd("Cos_Art"))
                        Val.Cod_Mar = ZctFunciones.GetGeneric(Of Integer)(rsadd("Cod_Mar"))
                        Val.Exist_Art = ZctFunciones.GetGeneric(Of Double)(rsadd("Exist_Art"))
                        Val.Cod_Linea = ZctFunciones.GetGeneric(Of Integer)(rsadd("Cod_Linea"))
                        Val.Cod_Dpto = ZctFunciones.GetGeneric(Of Integer)(rsadd("Cod_Dpto"))
                        Val.Activo_Art = ZctFunciones.GetGeneric(Of Boolean)(rsadd("Activo_Art"))
                        Val.Cod_TpArt = rsadd("Cod_TpArt").ToString
                    End If
                    Return Val

                Finally
                    BD.Dispose()


                End Try
            End Function

            ''' <summary>
            ''' Graba el artículo
            ''' </summary>
            ''' <param name="Articulo">Clase artículo</param>
            ''' <remarks></remarks>
            Public Sub SetArticulo(ByVal Articulo As Clases.Catalogos.ZctCatArt)
                Dim BD As New ZctDAOGeneral
                Try
                    BD.IniciaTransaccion()
                    Dim SQL As String = "EXECUTE SP_ZctCatArt 2, '" & Articulo.Cod_Art & "' , '" & Articulo.Desc_Art & "', " & Articulo.Prec_Art & ", " & Articulo.Cos_Art & ", " & Articulo.Cod_Mar & ", " & Articulo.Cod_Linea & ", " & Articulo.Cod_Dpto & ", '" & Articulo.Cod_TpArt & "', " & IIf(Articulo.Activo_Art, 1, 0) & ""
                    BD.SetDatos(SQL)
                    BD.FinalizaTransaccion()
                Catch ex As Exception
                    BD.CancelaTransaccion()
                    Throw ex
                Finally
                    BD.Dispose()
                End Try
            End Sub


        End Class
    End Namespace
End Namespace

