Imports ZctSOTRDN
Imports System.Configuration
Imports ZctSQL

Public Class ExistenciasDAO
    Public Sub New()
        _Conn = DAO.Conexion.CadenaConexion 'String.Format(ConfigurationManager.AppSettings.Get("CADENA_Conexion"), "L0k0m0t0r4")
    End Sub

    Private _Conn As String
    '''' <summary>
    '''' Conexión a la base de datos
    '''' </summary>
    '''' <value></value>
    '''' <returns></returns>
    '''' <remarks></remarks>
    'Public Property Conn() As String
    '    Get
    '        Return _Conn
    '    End Get
    '    Set(ByVal value As String)
    '        _Conn = value
    '    End Set
    'End Property

    Public Function ListaExistencia(ByVal Cod_Art As String) As List(Of Existencias)
        Dim ssql As String = String.Format("SELECT  ZctArtXAlm.Cod_Art, ZctArtXAlm.Cod_Alm, ZctArtXAlm.Exist_Art, ZctArtXAlm.CostoProm_Art, ZctCatAlm.Desc_CatAlm FROM ZctArtXAlm INNER JOIN ZctCatAlm ON ZctArtXAlm.Cod_Alm = ZctCatAlm.Cod_Alm WHERE (ZctArtXAlm.Cod_Art = '{0}') ORDER BY ZctArtXAlm.Cod_Art, ZctArtXAlm.Cod_Alm ", Cod_Art)
        Dim BD As New ZctDB
        Dim rs As SqlClient.SqlDataReader = Nothing
        Dim Lista As New List(Of Existencias)
        Try
            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.CrearComando(ssql)
            rs = BD.EjecutarConsulta()
            While rs.Read
                'TODO : Insertar los artículos para estas existencias.
                'TODO : Prueba de flujo de error.
                Lista.Add(New Existencias(rs("Cod_Art").ToString, rs("Cod_Alm").ToString, rs("Desc_CatAlm").ToString, GetGeneric(Of Double)(rs("Exist_Art")), GetGeneric(Of Double)(rs("CostoProm_Art"))))
            End While
            Return Lista
        Catch ex As BaseDatosException
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
        Finally
            BD.Desconectar()
            BD.Dispose()
        End Try

    End Function


    Public Function GetExistencia(ByVal Cod_Art As String, ByVal Cod_Alm As Integer) As Existencias
        Dim ssql As String = String.Format("SELECT  ZctArtXAlm.Cod_Art, ZctArtXAlm.Cod_Alm, ZctArtXAlm.Exist_Art, ZctArtXAlm.CostoProm_Art, ZctCatAlm.Desc_CatAlm FROM ZctArtXAlm INNER JOIN ZctCatAlm ON ZctArtXAlm.Cod_Alm = ZctCatAlm.Cod_Alm WHERE (ZctArtXAlm.Cod_Art = '{0}') AND (ZctArtXAlm.Cod_Alm = '{1}') ORDER BY ZctArtXAlm.Cod_Art, ZctArtXAlm.Cod_Alm ", Cod_Art, Cod_Alm)
        Dim BD As New ZctDB
        Dim rs As SqlClient.SqlDataReader = Nothing

        Try
            BD.GetCadenaConexion = _Conn
            BD.Conectar()
            BD.CrearComando(ssql)
            rs = BD.EjecutarConsulta()
            If rs.Read Then
                'TODO : Insertar los artículos para estas existencias.
                'TODO : Prueba de flujo de error.
                Return New Existencias(rs("Cod_Art").ToString, rs("Cod_Alm").ToString, rs("Desc_CatAlm").ToString, GetGeneric(Of Double)(rs("Exist_Art")), GetGeneric(Of Double)(rs("CostoProm_Art")))
            Else
                Return Nothing
            End If

        Catch ex As BaseDatosException
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
        Catch ex As Exception
            Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
        Finally
            BD.Desconectar()
            BD.Dispose()
        End Try

    End Function

End Class




