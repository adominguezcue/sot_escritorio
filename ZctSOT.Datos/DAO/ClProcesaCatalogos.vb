﻿Namespace DAO
    Public Class ClProcesaCatalogos
        Public Sub CargaDatos(ByRef IEnum As Interfaces.IEnumGenerico)
            Dim BD As New DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            IEnum.LimpiaElementos()
            Try
                'Obtiene los datos
                rsadd = BD.GetData(IEnum.SentenciaCargaDatos.ToUpper)
                IEnum.AgregaElemento(rsadd)
            Finally
                BD.Dispose()
            End Try
        End Sub

        Public Sub CargaDatos(ByRef IEnum As Interfaces.IEnumGenerico, ByVal args() As Object)
            Dim BD As New DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            IEnum.LimpiaElementos()
            Try
                'Obtiene los datos
                rsadd = BD.GetData(String.Format(IEnum.SentenciaCargaDatos.ToUpper, args))
                IEnum.AgregaElemento(rsadd)
            Finally
                BD.Dispose()
            End Try
        End Sub

        Public Function DevuelveDatos(ByRef IEnum As Interfaces.IEnumGenerico, ByVal args() As Object) As Interfaces.IEnumGenerico
            Dim BD As New DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            IEnum.LimpiaElementos()
            Try
                'Obtiene los datos
                rsadd = BD.GetData(String.Format(IEnum.SentenciaCargaDatos.ToUpper, args))
                IEnum.AgregaElemento(rsadd)
                Return IEnum
            Finally
                BD.Dispose()
            End Try
        End Function

        Public Sub ActualizarDatos(ByVal IEnum As IEnumerable(Of Interfaces.IGenerico))
            Dim BD As New DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            Try
                BD.IniciaTransaccion()
                For Each cc As Interfaces.IGenerico In IEnum
                    If cc.Actualiza Then BD.SetDatos(cc.SentenciaGraba.ToUpper)
                Next
                BD.FinalizaTransaccion()
            Catch ex As Exception
                BD.CancelaTransaccion()
                Throw ex
            Finally
                BD.Dispose()
            End Try

        End Sub

        Public Sub ActualizarDatos(ByVal IGen As Interfaces.IGenericoSingle)
            Dim BD As New DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            Try
                IGen.validar()
                BD.IniciaTransaccion()
                BD.SetDatos(IGen.SentenciaGraba.ToUpper)
                BD.FinalizaTransaccion()
            Catch ex As Exception
                BD.CancelaTransaccion()
                Throw ex
            Finally
                BD.Dispose()
            End Try

        End Sub




        Public Sub EliminaDatos(ByVal IGen As Interfaces.IGenerico)
            Dim BD As New DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            Try
                BD.IniciaTransaccion()
                BD.SetDatos(IGen.SentenciaElimina)
                BD.FinalizaTransaccion()
            Catch ex As Exception
                BD.CancelaTransaccion()
                Throw ex
            Finally
                BD.Dispose()
            End Try

        End Sub
    End Class
End Namespace