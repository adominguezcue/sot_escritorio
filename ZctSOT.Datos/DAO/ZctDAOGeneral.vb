﻿Imports System.Configuration
Imports System.Collections.Specialized
Imports ConfiguracionIni
Imports ZctSQL

Namespace DAO
    Class ZctDAOGeneral
        Implements IDisposable
        Private Shared _Conn As New Conexion
        Private _DAOBD As New ZctDB
        ''' <summary>
        ''' connección a la base de datos
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DAOBD() As ZctDB
            Get
                Return _DAOBD
            End Get
            Set(ByVal value As ZctDB)
                _DAOBD = value
            End Set
        End Property

        Public Sub IniciaTransaccion()
            _DAOBD.ComenzarTransaccion()
        End Sub

        Public Sub FinalizaTransaccion()
            _DAOBD.ConfirmarTransaccion()
        End Sub

        Public Sub CancelaTransaccion()
            _DAOBD.CancelarTransaccion()
        End Sub


        Public Sub New()
            'Conecta a la base de datos
            DAOBD.GetCadenaConexion = Conexion.CadenaConexion
            DAOBD.Conectar()
        End Sub
        ''' <summary>
        ''' Obtiene los datos de una consulta dada
        ''' </summary>
        ''' <param name="SQL">Consulta SQL</param>
        ''' <returns>DataReader</returns>
        ''' <remarks></remarks>
        Public Function GetData(ByVal SQL As String) As SqlClient.SqlDataReader
            Dim rs As SqlClient.SqlDataReader = Nothing
            Try

                'Crea el comando
                DAOBD.CrearComando(SQL)
                'Regresa los datos
                Return DAOBD.EjecutarConsulta()

            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            End Try
        End Function

        Public Sub SetDatos(ByVal SQL As String)
            Try

                'Crea el comando
                DAOBD.CrearComando(SQL)
                DAOBD.EjecutarComando()

            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            End Try
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free other state (managed objects).
                    DAOBD.Desconectar()
                    DAOBD.Dispose()
                End If

                ' TODO: free your own state (unmanaged objects).
                ' TODO: set large fields to null.
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

    Public Class Conexion
        Private Shared _RutaIni As String = "C:\reportes\SOT.ini"
        Public Shared Function CadenaConexion() As String
            Dim key As Microsoft.Win32.RegistryKey = Nothing
            'Try
            '    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software", True)
            'Catch ex As Security.SecurityException
            '    key = Nothing
            'End Try

            Dim sCadena As String = If(Global.Datos.Nucleo.Contextos.ConexionHelper.CadenaConexion, System.Configuration.ConfigurationManager.AppSettings("CADENA_Conexion"))

            'If key Is Nothing OrElse key.OpenSubKey("ZctSOT.Datos") Is Nothing Then
            '    _server = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "servidor")
            '    _user = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "user")
            '    _database = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "db")
            '    sCadena = String.Format(ConfigurationManager.AppSettings.Get("CADENA_Conexion"), _server, _database, _user, "l0k0m0t0r4")
            'Else
            '    Dim newkey As Microsoft.Win32.RegistryKey = key.OpenSubKey("ZctSOT.Datos")

            '    '//sCadena = String.Format("Data Source={0}; Initial Catalog={1};Connect Timeout=30;Persist Security Info=True ; User ID={2};Password={3}", _server, _database, _user, "l0k0m0t0r4")
            '    sCadena = String.Format("Server=tcp:{0},1433;Initial Catalog={1};Persist Security Info=False;User ID= User ID={2};Password={3};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;", _server, _database, _user, "l0k0m0t0r4")

            'End If
            Return sCadena

        End Function

        Private Shared _user As String
        Public Shared ReadOnly Property User() As String
            Get
                Return _user
            End Get
        End Property

        Private Shared _server As String
        Public Shared ReadOnly Property Server() As String
            Get
                Return _server
            End Get
        End Property
        Private Shared _database As String
        Public Shared ReadOnly Property Database() As String
            Get
                Return _database
            End Get
        End Property

        'Public Shared Sub SetActualServer(server As String)

        '    Dim key As Microsoft.Win32.RegistryKey = Nothing
        '    Try
        '        key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software", True)

        '    Catch ex As Security.SecurityException
        '        key = Nothing
        '    End Try


        '    If key Is Nothing OrElse key.OpenSubKey("ZctSOT.Datos") Is Nothing Then
        '        ConfigurationManager.AppSettings.Set("Server", server)
        '    Else
        '        key.SetValue("Server", server)
        '    End If
        '    _server = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "servidor")
        '    If _server <> server Then
        '        ConfiguracionIni.ConfiguracionIni.EscribirConfiguracion(_RutaIni, "configuracion", "servidor", server)
        '    End If
        'End Sub

        Public Shared Function GetActualServer() As String
            Dim key As Microsoft.Win32.RegistryKey = Nothing
            'Try
            '    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software", True)
            'Catch ex As Security.SecurityException
            '    key = Nothing
            'End Try

            If key Is Nothing OrElse key.OpenSubKey("ZctSOT.Datos") Is Nothing Then
                _server = ConfigurationManager.AppSettings.Get("Server")
            Else
                Dim newkey As Microsoft.Win32.RegistryKey = key.OpenSubKey("ZctSOT.Datos")
                _server = newkey.GetValue("Server").ToString
            End If


            If IO.File.Exists(_RutaIni) = True Then
                _server = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "servidor")
                _user = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "user")
                _database = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "db")
            Else
                '_server = "./SQLEXPRESS"
                ConfiguracionIni.ConfiguracionIni.EscribirConfiguracion(_RutaIni, "configuracion", "user", ConfigurationManager.AppSettings.Get("user"))
                ConfiguracionIni.ConfiguracionIni.EscribirConfiguracion(_RutaIni, "configuracion", "db", ConfigurationManager.AppSettings.Get("Database"))
                ConfiguracionIni.ConfiguracionIni.EscribirConfiguracion(_RutaIni, "configuracion", "servidor", ConfigurationManager.AppSettings.Get("server"))
            End If

            _server = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "servidor")

            Return _server
        End Function

        'Public Shared Sub SetActualBD(BD As String)

        '    Dim key As Microsoft.Win32.RegistryKey = Nothing
        '    Try
        '        key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software", True)

        '    Catch ex As Security.SecurityException
        '        key = Nothing
        '    End Try


        '    If key Is Nothing OrElse key.OpenSubKey("ZctSOT.Datos") Is Nothing Then
        '        ConfigurationManager.AppSettings.Set("db", BD)
        '    Else
        '        key.SetValue("db", Server)
        '    End If
        '    _database = ConfiguracionIni.ConfiguracionIni.LeerConfiguracion(_RutaIni, "configuracion", "db")
        '    If _database <> BD Then
        '        ConfiguracionIni.ConfiguracionIni.EscribirConfiguracion(_RutaIni, "configuracion", "db", BD)
        '    End If
        '    _database = BD
        'End Sub
    End Class
End Namespace