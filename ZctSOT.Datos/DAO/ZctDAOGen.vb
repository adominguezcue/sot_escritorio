﻿Namespace DAO
    Public Class ZctDAOCatalogos(Of T As IZctElemento)

        Private _Elemento As IZctElemento
        Public Property Elemento() As IZctElemento
            Get
                Return _Elemento
            End Get
            Set(ByVal value As IZctElemento)
                _Elemento = value
            End Set
        End Property

        Sub Grabar(ByVal list As List(Of T))

            Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            Try
                BD.IniciaTransaccion()

                For Each item As IZctElemento In list
                    BD.SetDatos(item.SetSQL)
                    Debug.Print(item.SetSQL)
                Next
                BD.FinalizaTransaccion()
            Catch ex As Exception
                BD.CancelaTransaccion()
                Throw ex

            Finally
                BD.Dispose()
            End Try

        End Sub

        Sub Eliminar(ByVal Elemento As IZctElemento)
            Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
            Try
                BD.IniciaTransaccion()

                BD.SetDatos(Elemento.DelSQL)

                BD.FinalizaTransaccion()
            Catch ex As Exception
                BD.CancelaTransaccion()
                Throw ex
            Finally
                BD.Dispose()
            End Try
        End Sub

        Function GetDatos() As List(Of T)
            Dim lista As New List(Of T)
            Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            Try
                Dim dt As New DataTable()
                dt.Load(BD.GetData(_Elemento.GetSQL))
                For Each row As DataRow In dt.Rows
                    Dim NewElemento As T = _Elemento.Clone
                    NewElemento.SetDatos(row)
                    lista.Add(NewElemento)
                Next
                Return lista
            Finally
                BD.Dispose()
            End Try
        End Function

        Function GetDatos(ByVal args() As Object) As List(Of T)
            Dim lista As New List(Of T)
            Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            If _Elemento Is Nothing Then Throw New Exception("El elemento no puede estar vacio")
            Try
                Dim dt As New DataTable()
                dt.Load(BD.GetData(String.Format(_Elemento.GetSQL, args)))
                For Each row As DataRow In dt.Rows
                    Dim NewElemento As T = _Elemento.Clone
                    NewElemento.SetDatos(row)
                    lista.Add(NewElemento)
                Next
                Return lista
            Finally
                BD.Dispose()
            End Try
        End Function
    End Class

    Public Class ZctDAOCatalogosVerifica

        Public Shared Function HasElement(ByVal Elemento As IZctElementoVerifica) As Boolean
            Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            Try
                Return BD.GetData(Elemento.GetSQLVerifica).HasRows
            Finally
                BD.Dispose()
            End Try
        End Function
    End Class

    Public Class ZctDAOCatalogoSingle(Of T As IZctElementoSingle)


        Sub Grabrar(ByVal list As List(Of IZctElementoSingle))

        End Sub

        Sub Eliminar(ByVal Elemento As IZctElementoSingle)

        End Sub

        Sub GetDatos(ByRef elemento As T)
            Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
            Dim rsadd As SqlClient.SqlDataReader = Nothing
            Try
                elemento.SetDatos(BD.GetData(elemento.GetElemento))
            Finally
                BD.Dispose()
            End Try
        End Sub

    End Class

End Namespace