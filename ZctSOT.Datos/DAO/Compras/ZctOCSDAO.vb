﻿Imports System.Collections.Generic

Namespace DAO
    Namespace Compras

        Public Class ZctOCSDAO

            Public Function GetLista(ByVal FchCalc As Date, ByVal Cod_alm As Integer, ByVal OC As Double, ByVal LT As Double, ByVal SS As Double, ByVal DiasHabiles As Double, ByVal Meses As Integer, ByVal Cod_Linea As Integer, ByVal Cod_Dpto As Integer) As List(Of Clases.Compras.ZctArtDet)
                Dim lista As New List(Of Clases.Compras.ZctArtDet)
                Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
                Dim rsadd As SqlClient.SqlDataReader = Nothing
                Try


                    Dim SQL As String = "SELECT ZctEncMovInv.Cod_Art, abs(sum(ZctEncMovInv.CtdMov_Inv)) AS Ctd,  ZctEncMovInv.Cod_Alm, YEAR(ZctEncMovInv.FchMov_Inv) AS Year, MONTH(ZctEncMovInv.FchMov_Inv) AS Month, ZctEncMovInv.TpOs_Inv, ZctCatArt.Desc_Art,ZctCatArt.Cos_Art , ZctCatArt.Cod_Linea, ZctCatArt.Cod_Dpto FROM ZctEncMovInv INNER JOIN ZctCatArt ON ZctEncMovInv.Cod_Art = ZctCatArt.Cod_Art WHERE  ( ZctCatArt.desactivar is null OR ZctCatArt.desactivar = 0 ) and FchMov_Inv >= convert(smalldatetime, '" & DateAdd(DateInterval.Month, -11, DateValue(FchCalc)) & "',103) AND FchMov_Inv <= convert(smalldatetime, '" & DateValue(FchCalc) & "',103) GROUP BY ZctEncMovInv.Cod_Art,  ZctEncMovInv.Cod_Alm, YEAR(ZctEncMovInv.FchMov_Inv), MONTH(ZctEncMovInv.FchMov_Inv), ZctEncMovInv.TpOs_Inv, ZctCatArt.Desc_Art, ZctCatArt.Cod_Linea, ZctCatArt.Cod_Dpto,ZctCatArt.Cos_Art HAVING (TpOs_Inv = 'OT') AND ZctEncMovInv.Cod_Alm = '" & Cod_alm & "'"

                    If Cod_Dpto > 0 Then
                        SQL &= " AND ZctCatArt.Cod_Dpto = " & Cod_Dpto.ToString
                    End If

                    If Cod_Linea > 0 Then
                        SQL &= " AND ZctCatArt.Cod_Linea = " & Cod_Dpto.ToString
                    End If
                    rsadd = BD.GetData(SQL)
                    While rsadd.Read
                        _cod_art = rsadd("Cod_art")
                        Dim elemento As Clases.Compras.ZctArtDet
                        elemento = lista.Find(New Predicate(Of ZctSOT.Datos.Clases.Compras.ZctArtDet)(AddressOf _GetElemento))

                        '= New ZctSOT.Datos.DAO.Catalogos.ZctDAOCatArt().GetArticulo(rsadd("Cod_Art").ToString)
                        If elemento Is Nothing Then
                            elemento = New Clases.Compras.ZctArtDet
                            elemento.Cod_Alm = Cod_alm
                            elemento.Cod_Art = rsadd("Cod_Art").ToString
                            elemento.Desc_Art = rsadd("Desc_Art").ToString
                            If rsadd("Cod_Linea").ToString <> "" Then
                                elemento.Cod_Linea = rsadd("Cod_Linea").ToString
                            Else
                                elemento.Cod_Linea = 0
                            End If
                            elemento.Cod_Dpto = rsadd("Cod_Dpto").ToString
                            'elemento.Cod_Art = 
                            elemento.Cos_Art = ZctFunciones.GetGeneric(Of Double)(rsadd("Cos_Art"))
                            elemento.Exist_Art = New ZctSOT.Datos.ExistenciasDAO().GetExistencia(rsadd("Cod_Art").ToString, Cod_alm).Existencias
                            elemento.DiasHabilies = DiasHabiles
                            elemento.OC = OC
                            elemento.LT = LT
                            elemento.SS = SS
                            lista.Add(elemento)
                        End If
                        Dim dateFin As Date = DateSerial(ZctFunciones.GetGeneric(Of Integer)(rsadd("year")), ZctFunciones.GetGeneric(Of Integer)(rsadd("Month")), 1)
                        Dim datecalc As Date = DateSerial(ZctFunciones.GetGeneric(Of Integer)(FchCalc.Year), ZctFunciones.GetGeneric(Of Integer)(FchCalc.Month), 1)
                        elemento.ArrayMeses(DateDiff(DateInterval.Month, dateFin, datecalc) + 1) = rsadd("Ctd")
                        'Select Case DateDiff(DateInterval.Month, dateFin, FchCalc) + 1
                        '    Case 1
                        '        elemento.Mes1 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 2
                        '        elemento.Mes2 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 3
                        '        elemento.Mes3 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 4
                        '        elemento.Mes4 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 5
                        '        elemento.Mes5 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 6
                        '        elemento.Mes6 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 7
                        '        elemento.Mes7 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 8
                        '        elemento.Mes8 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 9
                        '        elemento.Mes9 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 10
                        '        elemento.Mes10 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 11
                        '        elemento.Mes11 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        '    Case 12
                        '        elemento.Mes12 = ZctFunciones.GetGeneric(Of Double)(rsadd("Ctd"))
                        'End Select
                        elemento.MesesDis = Meses
                        elemento.Calcula()


                    End While

                    Return lista
                Finally
                    BD.Dispose()
                End Try
            End Function

            Private _cod_art As String

            Private Function _GetElemento(ByVal elemento As Clases.Compras.ZctArtDet) As Boolean
                Return (elemento.Cod_Art = _cod_art)
            End Function

            Public Function GetConfig(ByVal Cod_Alm As Integer) As Clases.Compras.ZctConfOCS
                Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
                Dim rsadd As SqlClient.SqlDataReader = Nothing
                Dim elemento As New Clases.Compras.ZctConfOCS
                Try
                    elemento.Cod_Alm = Cod_Alm

                    ' Dim sql As String = "SP_ZctOCCnf 1, Cod_Alm, OC, LT ,SS ,DH "
                    Dim sql As String = String.Format("SP_ZctOCCnf 1, {0}, {1}, {2}, {3}, {4}, {5}", elemento.Cod_Alm, elemento.OC, elemento.LT, elemento.SS, elemento.DiasHabiles, elemento.Meses)

                    rsadd = BD.GetData(sql)
                    If rsadd.Read Then
                        elemento.Cod_Alm = ZctFunciones.GetGeneric(Of Integer)(rsadd("Cod_Alm"))
                        elemento.OC = ZctFunciones.GetGeneric(Of Integer)(rsadd("OC"))
                        elemento.LT = ZctFunciones.GetGeneric(Of Integer)(rsadd("LT"))
                        elemento.SS = ZctFunciones.GetGeneric(Of Integer)(rsadd("SS"))
                        elemento.DiasHabiles = ZctFunciones.GetGeneric(Of Integer)(rsadd("DH"))
                        elemento.Meses = ZctFunciones.GetGeneric(Of Integer)(rsadd("Mes"))
                    End If
                    Return elemento
                Finally
                    BD.Dispose()
                End Try


            End Function

            Public Sub SetConfig(ByVal elemento As Clases.Compras.ZctConfOCS)
                Dim BD As New ZctSOT.Datos.DAO.ZctDAOGeneral
                Dim rsadd As SqlClient.SqlDataReader = Nothing

                Try
                    ' Dim sql As String = "SP_ZctOCCnf 1, Cod_Alm, OC, LT ,SS ,DH "
                    Dim sql As String = String.Format("SP_ZctOCCnf 2, {0}, {1}, {2}, {3}, {4}, {5}", elemento.Cod_Alm, elemento.OC, elemento.LT, elemento.SS, elemento.DiasHabiles, elemento.Meses)
                    BD.SetDatos(sql)

                Finally
                    BD.Dispose()
                End Try

            End Sub

        End Class

    End Namespace
End Namespace
