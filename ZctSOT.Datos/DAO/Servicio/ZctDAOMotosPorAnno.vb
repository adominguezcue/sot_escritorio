﻿Imports ZctSOT.Datos.Clases.Catalogos
Imports ZctSOT.Datos.Clases.Servicio
Namespace DAO
    Namespace Servicio
        Public Class ZctDAOMotosPorAnno
            ''' <summary>
            ''' Obtiene las regiones
            ''' </summary>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Shared Function GetMotosPorMes(ByVal Anno As Integer, ByVal Mes As Integer, ByVal Cod_Region As String) As Integer
                ValidaMotosPorMes(Anno, Mes, Cod_Region)
                Dim BD As New DAO.ZctDAOGeneral
                Dim rsadd As SqlClient.SqlDataReader = Nothing
                Dim meses As New List(Of ZctMeses)
                Try
                    Dim SQL As String = String.Format("Select NoMotos FROM ZctNoMotosXAnno where  Anno = {0} AND Mes = {1} AND Cod_Region = '{2}'", Anno, Mes, Cod_Region)
                    'Obtiene los datos
                    rsadd = BD.GetData(SQL)
                    If rsadd.Read Then
                        Return ZctFunciones.GetGeneric(Of Integer)(rsadd("NoMotos"))
                    Else
                        Return 0
                    End If

                Finally
                    BD.Dispose()
                End Try
            End Function

            Private Shared Sub ValidaMotosPorMes(ByVal Anno As Integer, ByVal Mes As Integer, ByVal Cod_Region As String)
                'Recordar en el año 3000  cambiar esto :)
                If Anno <= 1980 OrElse Anno >= 3000 Then Throw New ArgumentException("El valor del año no es valido")
                If Mes <= 0 OrElse Mes > 12 Then Throw New ArgumentException("El valor del mes no es valido")
                If Cod_Region = String.Empty Then Throw New ArgumentException("Ingrese el código de región")
            End Sub

            Private Shared Sub ValidaMotosPorMes(ByVal motocicleta As ZctMotosPorAnno)
                ValidaMotosPorMes(motocicleta.Anno, motocicleta.Mes, motocicleta.Cod_Region)
                If motocicleta.NoMotos < 0 Then Throw New ArgumentException("El valor de las motocicletas no puede ser menor a cero")
            End Sub
            Public Shared Sub GrabaMotocicletas(ByVal Motocicletas As List(Of ZctMotosPorAnno))
                Dim BD As New ZctDAOGeneral
                Try
                    BD.IniciaTransaccion()
                    For Each motocicleta As ZctMotosPorAnno In Motocicletas
                        ValidaMotosPorMes(motocicleta)
                        Dim SQL As String = String.Format("EXECUTE SP_ZctNoMotosXAnno {0}, {1}, '{2}', {3}", motocicleta.Anno, motocicleta.Mes, motocicleta.Cod_Region, motocicleta.NoMotos)
                        BD.SetDatos(SQL)
                    Next
                    
                    BD.FinalizaTransaccion()
                Catch ex As Exception
                    BD.CancelaTransaccion()
                    Throw ex
                Finally
                    BD.Dispose()
                End Try
            End Sub
        End Class
    End Namespace


End Namespace

