Imports System.Management
Public Class ZctDatos
    Dim objMOS As ManagementObjectSearcher
    Dim objMOC As Management.ManagementObjectCollection
    Dim objMO As Management.ManagementObject


    Public Function GetMac() As String
        Try

            Dim Mac As String = ""

            objMOS = New ManagementObjectSearcher("Select * From Win32_NetworkAdapter")

            objMOC = objMOS.Get

            For Each objMO In objMOC
                Mac = objMO("MACAddress")
            Next
            Return Mac
        Catch ex As Exception
            MsgBox("Ha ocurrido un error al buscar el servidor, comuniquese con sistemas.")
            Return ""
        Finally
            objMOS.Dispose()
            objMOS = Nothing
            objMO.Dispose()
            objMO = Nothing
        End Try
        

    End Function


End Class
