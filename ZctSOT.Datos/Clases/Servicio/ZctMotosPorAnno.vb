﻿Namespace Clases
    Namespace Servicio

        Public Class ZctMotosPorAnno

            Private _Anno As Integer
            Public Property Anno() As Integer
                Get
                    Return _Anno
                End Get
                Set(ByVal value As Integer)
                    _Anno = value
                End Set
            End Property

            Private _Mes As Integer
            Public Property Mes() As Integer
                Get
                    Return _Mes
                End Get
                Set(ByVal value As Integer)
                    _Mes = value
                End Set
            End Property

            Private _Cod_Region As String
            Public Property Cod_Region() As String
                Get
                    Return _Cod_Region
                End Get
                Set(ByVal value As String)
                    _Cod_Region = value
                End Set
            End Property

            Private _Desc_Region As String
            Public Property Desc_Region() As String
                Get
                    Return _Desc_Region
                End Get
                Set(ByVal value As String)
                    _Desc_Region = value
                End Set
            End Property

            Private _NoMotos As Integer
            Public Property NoMotos() As Integer
                Get
                    Return _NoMotos
                End Get
                Set(ByVal value As Integer)
                    _NoMotos = value
                End Set
            End Property
        End Class

    End Namespace
End Namespace

