﻿Namespace Clases
    Namespace Catalogos
        Public Class ZctCatCliente
            Private _Cod_Cte As Integer = 0
            ''' <summary>
            ''' Código de cliente
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Cte() As Integer
                Get
                    Return _Cod_Cte
                End Get
                Set(ByVal value As Integer)
                    _Cod_Cte = value
                End Set
            End Property


            Private _Nom_Cte As String

            public property Nom_Cte as STRING
                Get
                    return _Nom_Cte
                End Get
                Set(ByVal value As String)
                    _Nom_Cte = Value
                End Set
            End Property

            Private _ApPat_Cte As String

            public property ApPat_Cte as STRING
                Get
                    return _ApPat_Cte
                End Get
                Set(ByVal value As String)
                    _ApPat_Cte = Value
                End Set
            End Property

            Private _ApMat_Cte As String

            public property ApMat_Cte as STRING
                Get
                    return _ApMat_Cte
                End Get
                Set(ByVal value As String)
                    _ApMat_Cte = Value
                End Set
            End Property

            Public ReadOnly Property Nombre() As String
                Get
                    Return _Nom_Cte & " " & " " & _ApPat_Cte & " " & _ApMat_Cte
                End Get
            End Property

Private _Dir_Cte	 as STRING
            public property Dir_Cte as STRING
                Get
                    return _Dir_Cte
                End Get
                Set(ByVal value As String)
                    _Dir_Cte = Value
                End Set
            End Property

Private _RFC_Cte	 as STRING
            public property RFC_Cte as STRING
                Get
                    return _RFC_Cte
                End Get
                Set(ByVal value As String)
                    _RFC_Cte = Value
                End Set
            End Property

Private _Cod_Ciu	 as integer
            public property Cod_Ciu as integer
                Get
                    return _Cod_Ciu
                End Get
                Set(ByVal value As Integer)
                    _Cod_Ciu = Value
                End Set
            End Property

Private _Cod_Edo	 as integer
            public property Cod_Edo as integer
                Get
                    return _Cod_Edo
                End Get
                Set(ByVal value As Integer)
                    _Cod_Edo = Value
                End Set
            End Property

Private _Ref_Cte	 as STRING
            public property Ref_Cte as STRING
                Get
                    return _Ref_Cte
                End Get
                Set(ByVal value As String)
                    _Ref_Cte = Value
                End Set
            End Property

Private _Cod_Gte	 as integer
            public property Cod_Gte as integer
                Get
                    return _Cod_Gte
                End Get
                Set(ByVal value As Integer)
                    _Cod_Gte = Value
                End Set
            End Property

            Private _Cod_Region As String
            public property Cod_Region as STRING
                Get
                    return _Cod_Region
                End Get
                Set(ByVal value As String)
                    _Cod_Region = Value
                End Set
            End Property

            Private _Cte_Old As Boolean
            public property Cte_Old as BOOLEAN
                Get
                    return _Cte_Old
                End Get
                Set(ByVal value As Boolean)
                    _Cte_Old = Value
                End Set
            End Property
        End Class

        Public Class ZctCatClienteVerifica
            Inherits ZctCatCliente
            Implements IZctElementoVerifica

            Public Sub SetDatos(ByVal row As SqlClient.SqlDataReader) Implements IZctElementoSingle.SetDatos
                If row.Read Then
                    Me.Cod_Cte = ZctFunciones.GetGeneric(Of Integer)(row("Cod_Cte"))
                    Me.Nom_Cte = ZctFunciones.GetGeneric(row("Nom_Cte"))
                    Me.ApPat_Cte = ZctFunciones.GetGeneric(row("ApPat_Cte"))
                    Me.ApMat_Cte = ZctFunciones.GetGeneric(row("ApMat_Cte"))
                    Me.Dir_Cte = ZctFunciones.GetGeneric(IIf(IsDBNull(row("Dir_Cte")), Nothing, row("Dir_Cte")))
                    Me.RFC_Cte = ZctFunciones.GetGeneric(row("RFC_Cte"))
                    Me.Cod_Ciu = ZctFunciones.GetGeneric(Of Integer)(row("Cod_Ciu"))
                    Me.Cod_Edo = ZctFunciones.GetGeneric(Of Integer)(row("Cod_Edo"))
                    Me.Ref_Cte = ZctFunciones.GetGeneric(IIf(IsDBNull(row("Ref_Cte")), Nothing, row("Ref_Cte")))
                    Me.Cod_Gte = ZctFunciones.GetGeneric(Of Integer)(IIf(IsDBNull(row("Cod_Gte")), Nothing, row("Cod_Gte")))
                    Me.Cod_Region = ZctFunciones.GetGeneric(IIf(IsDBNull(row("Cod_Region")), Nothing, row("Cod_Region")))
                    Me.Cte_Old = ZctFunciones.GetGeneric(Of Boolean)(IIf(IsDBNull(row("Cte_Old")), Nothing, row("Cte_Old")))
                End If
            End Sub

            Private ReadOnly Property GetSQLVerifica() As String Implements IZctElementoVerifica.GetSQLVerifica
                Get
                    Return "SELECT Cod_Cte FROM ZctCatCte WHERE (Cod_Cte = '" & Me.Cod_Cte & "')"
                End Get
            End Property

            Public Function HasElement(ByVal indice As String) As Boolean Implements IZctElementoVerifica.HasElement
                If indice = String.Empty Then indice = 0
                If Not IsNumeric(indice) Then Throw New ZctReglaNegocioEx("Se esperaba un valor numérico")
                Me.Cod_Cte = CInt(indice)
                Return DAO.ZctDAOCatalogosVerifica.HasElement(Me)

            End Function


            Public ReadOnly Property DelElemento() As String Implements IZctElementoSingle.DelElemento
                Get
                    Return ""
                End Get
            End Property


            Public ReadOnly Property SetElemento() As String Implements IZctElementoSingle.SetElemento
                Get
                    Return ""
                End Get
            End Property

            Public ReadOnly Property GetElemento() As String Implements IZctElementoSingle.GetElemento
                Get
                    Return "SELECT     Cod_Cte, Nom_Cte, ApPat_Cte, ApMat_Cte, Dir_Cte, RFC_Cte, Cod_Ciu, Cod_Edo, Ref_Cte, Cod_Gte, Cod_Region, Cte_Old FROM ZctCatCte where Cod_Cte = '" & Me.Cod_Cte & "'"
                End Get
            End Property

        End Class
    End Namespace
End Namespace