﻿Namespace Clases
    Namespace Catalogos


        ''' <summary>
        ''' Código de artículo
        ''' </summary>
        ''' <remarks></remarks>
        Public Class ZctCatArt
            Private _Cod_Art As String
            ''' <summary>
            ''' Código de Artículo
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Art() As String
                Get
                    Return _Cod_Art
                End Get
                Set(ByVal value As String)
                    _Cod_Art = value
                End Set
            End Property

            Private _Desc_Art As String
            ''' <summary>
            ''' Descripción de Artículo
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Desc_Art() As String
                Get
                    Return _Desc_Art
                End Get
                Set(ByVal value As String)
                    _Desc_Art = value
                End Set
            End Property

            Private _Prec_Art As Double
            ''' <summary>
            ''' Precio Artículo
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Prec_Art() As Double
                Get
                    Return _Prec_Art
                End Get
                Set(ByVal value As Double)
                    _Prec_Art = value
                End Set
            End Property

            Private _Cos_Art As Double
            ''' <summary>
            ''' Costo del artículo
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cos_Art() As Double
                Get
                    Return _Cos_Art
                End Get
                Set(ByVal value As Double)
                    _Cos_Art = value
                End Set
            End Property

            Private _Cod_Mar As Integer
            ''' <summary>
            ''' Código de Marca
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Mar() As Integer
                Get
                    Return _Cod_Mar
                End Get
                Set(ByVal value As Integer)
                    _Cod_Mar = value
                End Set
            End Property

            Private _Exist_Art As Integer
            ''' <summary>
            ''' Existencias
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Exist_Art() As Integer
                Get
                    Return _Exist_Art
                End Get
                Set(ByVal value As Integer)
                    _Exist_Art = value
                End Set
            End Property

            Private _Cod_Linea As Integer
            ''' <summary>
            ''' Código de Linea
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Linea() As Integer
                Get
                    Return _Cod_Linea
                End Get
                Set(ByVal value As Integer)
                    _Cod_Linea = value
                End Set
            End Property

            Private _Cod_Dpto As Integer
            ''' <summary>
            ''' Código de departamento
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Dpto() As Integer
                Get
                    Return _Cod_Dpto
                End Get
                Set(ByVal value As Integer)
                    _Cod_Dpto = value
                End Set
            End Property

            Private _Activo_Art As Boolean
            ''' <summary>
            ''' Artículo Activo
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Activo_Art() As Boolean
                Get
                    Return _Activo_Art
                End Get
                Set(ByVal value As Boolean)
                    _Activo_Art = value
                End Set
            End Property

            Private _Cod_TpArt As String
            ''' <summary>
            ''' Tipo de Artículo
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_TpArt() As String
                Get
                    Return _Cod_TpArt
                End Get
                Set(ByVal value As String)
                    _Cod_TpArt = value
                End Set
            End Property

            Private _Cod_TpRef As String
            ''' <summary>
            ''' Tipo de Refacción
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_TpRef() As String
                Get
                    Return _Cod_TpRef
                End Get
                Set(ByVal value As String)
                    _Cod_TpRef = value
                End Set
            End Property


        End Class
    End Namespace
End Namespace