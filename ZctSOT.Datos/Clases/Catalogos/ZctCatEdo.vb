﻿Namespace Clases
    Namespace Catalogos

        Public Class ZctCatEdo
            Private _Cod_Edo As Integer
            Public Property Cod_Edo() As Integer
                Get
                    Return _Cod_Edo
                End Get
                Set(ByVal value As Integer)
                    value = _Cod_Edo
                End Set
            End Property

            Private _Desc_Edo As String
            Public Property Desc_Edo() As String
                Get
                    Return _Desc_Edo
                End Get
                Set(ByVal value As String)
                    value = _Desc_Edo
                End Set
            End Property

        End Class
    End Namespace
End Namespace

