﻿Namespace Clases
    Namespace Catalogos

        Public Class ZctRegion

            Private _Codigo As String
            Public Property Codigo() As String
                Get
                    Return _Codigo
                End Get
                Set(ByVal value As String)
                    _Codigo = value
                End Set
            End Property

            Private _Descripcion As String
            Public Property Descripcion() As String
                Get
                    Return _Descripcion
                End Get
                Set(ByVal value As String)
                    _Descripcion = value
                End Set
            End Property
        End Class
    End Namespace
End Namespace
