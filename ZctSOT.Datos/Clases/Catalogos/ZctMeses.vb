﻿Namespace Clases
    Namespace Catalogos

        Public Class ZctMeses

            Private _Mes As String
            Public Property Mes() As String
                Get
                    Return _Mes
                End Get
                Set(ByVal value As String)
                    _Mes = value
                End Set
            End Property

            Private _NombreMes As String
            Public Property NombreMes() As String
                Get
                    Return _NombreMes
                End Get
                Set(ByVal value As String)
                    _NombreMes = value
                End Set
            End Property
        End Class
    End Namespace
End Namespace
