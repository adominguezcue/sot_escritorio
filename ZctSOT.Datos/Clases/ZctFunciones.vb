Public Class ZctFunciones

    Public Enum ZctTipos
        Zct_String
        Zct_Int
        Zct_Date
    End Enum

    'Valida los valores por default
    Public Shared Function GetDefault(ByVal Valor As String, ByVal Tipo As ZctTipos) As Object
        If Valor = "NULL" Then Return DBNull.Value
        Select Case Tipo
            Case ZctTipos.Zct_Int
                If Valor = "" Then Return 0 Else Return Valor
            Case ZctTipos.Zct_Date
                If Valor = "" Then Return Date.Now Else Return Valor
            Case ZctTipos.Zct_String
                If Valor = "" Then Return "" Else Return Valor
            Case Else
                If Valor = "" Then Return "" Else Return Valor
        End Select
    End Function

    Public Shared Function GetDefaultV2(ByVal Valor As Object, ByVal Tipo As ZctTipos) As Object
        If Valor Is "NULL" Or Valor = Nothing Then Return DBNull.Value
        Select Case Tipo
            Case ZctTipos.Zct_Int
                If Valor Is "" Then Return 0 Else Return Valor
            Case ZctTipos.Zct_Date
                If Valor Is "" Then Return Date.Now Else Return Valor
            Case ZctTipos.Zct_String
                If Valor Is "" Then Return "" Else Return Valor
            Case Else
                If Valor Is "" Then Return "" Else Return Valor
        End Select
    End Function

    'Valida los valores por default
    Public Shared Function GetDefault(ByVal Valor As DBNull, ByVal Tipo As ZctTipos) As Object
        Select Case Tipo
            Case ZctTipos.Zct_Int
                Return 0
            Case ZctTipos.Zct_Date
                Return (Date.Now)
            Case ZctTipos.Zct_String
                Return ""
            Case Else
                Return ""
        End Select
    End Function

    Public Shared Function GetGeneric(Of T As Structure)(ByVal Valor As Nullable(Of T)) As T
        Return Valor.GetValueOrDefault
    End Function

    
    Public Shared Function GetGeneric(ByVal Valor As String) As String
        If Valor Is DBNull.Value Then
            Return ""
        Else
            Return Valor
        End If

    End Function

    
End Class
