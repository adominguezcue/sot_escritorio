Imports ZctSQL

Public Class zctFolios

    Private _CodFolio As String
    ''' <summary>
    ''' C�digo del folio
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodFolio() As String
        Get
            Return _CodFolio
        End Get
        Set(ByVal value As String)
            _CodFolio = value
        End Set
    End Property

    Public Sub New(ByVal CodFolio As String)
        Me._CodFolio = CodFolio
    End Sub

    Public ReadOnly Property Consecutivo() As Integer
        Get
            Dim Cons As Integer = 0
            Dim dt As DataTable = Nothing
            Try
                If _CodFolio = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar el identificador del folio", Nothing)
                dt = ClassGen.GenGetData("SP_ZctCatFolios", "@TpConsulta;int|@Cod_Folio;varchar", "1|" & _CodFolio)
                Cons = ZctFunciones.GetGeneric(Of Integer)(dt.Rows(0)("Cons"))
            Catch ex As Exception
                Throw ex
            Finally
                dt.Dispose()
            End Try
            Return Cons
        End Get
    End Property

    Public ReadOnly Property SetConsecutivo() As Integer
        Get
            Dim Cons As Integer = 0
            Dim dt As DataTable = Nothing
            Try
                If _CodFolio = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar el identificador del folio", Nothing)
                dt = ClassGen.GenGetData("SP_ZctCatFolios", "@TpConsulta;int|@Cod_Folio;varchar", "2|" & _CodFolio)
                Cons = ZctFunciones.GetGeneric(Of Integer)(dt.Rows(0)("Cons"))
            Catch ex As Exception
                Throw ex
            Finally
                dt.Dispose()
            End Try
            Return Cons

        End Get
    End Property

    Public ReadOnly Property SetConsecutivo(ByVal PkBusCon As ZctDataBase) As Integer
        Get
            Dim Cons As Integer = 0
            Dim dt As DataTable = Nothing
            Try
                If _CodFolio = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar el identificador del folio", Nothing)
                dt = ClassGen.GenGetData("SP_ZctCatFolios", "@TpConsulta;int|@Cod_Folio;varchar", "2|" & _CodFolio, PkBusCon)
                Cons = ZctFunciones.GetGeneric(Of Integer)(dt.Rows(0)("Cons"))
                Return Cons
            Catch ex As Exception
                Throw ex
            Finally
                dt.Dispose()
            End Try


        End Get
    End Property

    Public ReadOnly Property SetConsecutivo(ByVal BD As ZctDB) As Integer
        Get
            Dim Cons As Integer = 0
            Dim dt As DataTable = Nothing
            Dim rs As SqlClient.SqlDataReader = Nothing
            Try
                If _CodFolio = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar el identificador del folio", Nothing)

                BD.CrearComando("EXECUTE SP_ZctCatFolios @Tipo, @Folio")
                BD.AsignarParametroEntero("@Tipo", 2)
                BD.AsignarParametroCadena("@Folio", _CodFolio)

                'BD.EjecutarComando()
                'BD.CrearComando("EXECUTE SP_ZctCatFolios 2, '" & _CodFolio & "'")
                rs = BD.EjecutarConsulta
                'dt.Load()
                'dt = ClassGen.GenGetData("SP_ZctCatFolios", "@TpConsulta;int|@Cod_Folio;varchar", "2|" & _CodFolio, PkBusCon)
                If rs.Read Then
                    Cons = ZctFunciones.GetGeneric(Of Integer)(rs("Cons"))
                Else
                    Cons = 1
                End If
            Catch ex As Exception
                Throw ex
            Finally
                rs.Dispose()
            End Try
            Return Cons

        End Get
    End Property



End Class
