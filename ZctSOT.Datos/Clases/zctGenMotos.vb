Public Class zctCatMotos
    Implements IElementoGen


    Private _Cod_Mot As Integer
    ''' <summary>
    ''' C�digo de la moto
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Cod_Mot() As Integer
        Get
            Return _Cod_Mot
        End Get
        Set(ByVal value As Integer)
            _Cod_Mot = value
        End Set
    End Property

    Private _Vin_Mot As String
    ''' <summary>
    ''' Vin de la moto
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Vin_Mot() As String
        Get
            Return _Vin_Mot
        End Get
        Set(ByVal value As String)
            _Vin_Mot = value
        End Set
    End Property

    Private _Cod_Cte As Integer
    ''' <summary>
    ''' C�digo del cliente
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Cod_Cte() As Integer
        Get
            Return _Cod_Cte
        End Get
        Set(ByVal value As Integer)
            _Cod_Cte = value
        End Set
    End Property

    Private _Cod_Mar As Integer
    ''' <summary>
    ''' C�digo de la marca
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Cod_Mar() As Integer
        Get
            Return _Cod_Mar
        End Get
        Set(ByVal value As Integer)
            _Cod_Mar = value
        End Set
    End Property

    Private _CodMod_Mot As Integer
    ''' <summary>
    ''' C�digo del modelo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodMod_Mot() As Integer
        Get
            Return _CodMod_Mot
        End Get
        Set(ByVal value As Integer)
            _CodMod_Mot = value
        End Set
    End Property

    Private _Placas_Mot As String
    ''' <summary>
    ''' Placas de la Moto
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Placas_Mot() As String
        Get
            Return _Placas_Mot
        End Get
        Set(ByVal value As String)
            _Placas_Mot = value
        End Set
    End Property

    Private _Motor_Mot As String
    ''' <summary>
    ''' C�digo del Motor
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Motor_Mot() As String
        Get
            Return _Motor_Mot
        End Get
        Set(ByVal value As String)
            _Motor_Mot = value
        End Set
    End Property

    Private _Anno_Mot As Integer
    ''' <summary>
    ''' A�o de la moto
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Anno_Mot() As Integer
        Get
            Return _Anno_Mot
        End Get
        Set(ByVal value As Integer)
            _Anno_Mot = value
        End Set
    End Property

    Private _Cod_Ubicacion As String
    Public Property Cod_Ubicacion() As String
        Get
            Return _Cod_Ubicacion
        End Get
        Set(ByVal value As String)
            _Cod_Ubicacion = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Overridable Sub llenaDato(ByVal Midato As System.Data.DataRow) Implements IElementoGen.llenaDato
        _Cod_Mot = ZctFunciones.GetGeneric(Of Integer)(Midato("Cod_Mot"))
        _Vin_Mot = Midato("Vin_Mot")
        _Cod_Cte = ZctFunciones.GetGeneric(Of Integer)(Midato("Cod_Cte"))
        _Cod_Mar = ZctFunciones.GetGeneric(Of Integer)(Midato("Cod_Mar"))
        _CodMod_Mot = ZctFunciones.GetGeneric(Of Integer)(Midato("CodMod_Mot"))
        _Placas_Mot = Midato("Placas_Mot")
        _Motor_Mot = Midato("Motor_Mot")
        _Anno_Mot = ZctFunciones.GetGeneric(Of Integer)(Midato("Anno_Mot"))
        If Midato("Cod_Ubicacion").ToString = "" Then _Cod_Ubicacion = "USO" Else _Cod_Ubicacion = Midato("Cod_Ubicacion").ToString


    End Sub
End Class


