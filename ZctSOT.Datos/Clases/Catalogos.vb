﻿Namespace Clases
    Namespace Catalogos

        Public Class CatAlmacen

            Private _CodAlm As Integer
            Public Property CodAlm() As Integer
                Get
                    Return _CodAlm
                End Get
                Set(ByVal value As Integer)
                    _CodAlm = value
                End Set
            End Property

            Private _DescAlm As String
            Public Property DescAlm() As String
                Get
                    Return _DescAlm
                End Get
                Set(ByVal value As String)
                    _DescAlm = value
                End Set
            End Property

            Public Sub New(ByVal CodAlm As Integer, ByVal DescAlm As String)
                _CodAlm = CodAlm
                _DescAlm = DescAlm
            End Sub
        End Class

#Region "Departamento"
        Public Class ZctClCatDpto
            Implements ZctSOT.Datos.Interfaces.IGenerico

            Private _Actualiza As Boolean = True
            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return _Actualiza
                End Get
                Set(ByVal value As Boolean)
                    _Actualiza = value
                End Set
            End Property


            Private _Cod_Dpto As Integer = 0
            Public Property Codigo() As Integer
                Get
                    Return _Cod_Dpto
                End Get
                Set(ByVal value As Integer)
                    If _Cod_Dpto = 0 Then
                        _Cod_Dpto = value
                    End If
                End Set
            End Property

            Private _Desc_Dpto As String
            Public Property Departamento() As String
                Get
                    Return _Desc_Dpto
                End Get
                Set(ByVal value As String)
                    _Actualiza = True
                    _Desc_Dpto = value
                End Set
            End Property

            Public Sub New()

            End Sub

            Public Sub New(ByVal Codigo As Integer, ByVal Desc_Dpto As String)
                _Cod_Dpto = Codigo
                _Desc_Dpto = Desc_Dpto
            End Sub


            Public ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina
                Get
                    Return "EXECUTE SP_ZctCatDpto 3, '" & Me._Cod_Dpto & "', '" & Me.Departamento & "', '1'"
                End Get
            End Property

            Public ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
                Get
                    Return "EXECUTE SP_ZctCatDpto 2, '" & Me._Cod_Dpto & "', '" & Me.Departamento & "', '1'"
                End Get
            End Property

        End Class

        Public Class listaDepartamentos
            Inherits List(Of ZctClCatDpto)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum

            Public Sub AgregaElemento(ByRef Row As SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                While Row.Read
                    Dim iElemento As New ZctClCatDpto(ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Dpto")), Row("Desc_Dpto").ToString)
                    '            iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Dpto"))
                    '           iElemento.Departamento = Row("Desc_Dpto").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While
            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Dpto, Desc_Dpto FROM ZctCatDpto"
                End Get
            End Property

            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function
        End Class

#End Region
#Region "Generico"
        Public MustInherit Class ZctClCatGen
            Implements ZctSOT.Datos.Interfaces.IGenerico
            Private _Codigo As Integer

            Public Property Codigo() As Integer
                Get
                    Return _Codigo
                End Get
                Set(ByVal value As Integer)
                    If _Codigo <> 0 Then Exit Property
                    _Codigo = value
                End Set
            End Property

            Private _Actualiza As Boolean
            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return _Actualiza
                End Get
                Set(ByVal value As Boolean)
                    _Actualiza = value
                End Set
            End Property

            Public MustOverride ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina

            Public MustOverride ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
        End Class

        Public MustInherit Class ZctClCatGenString
            Implements ZctSOT.Datos.Interfaces.IGenerico
            Private _Codigo As String

            Public Property Codigo() As String
                Get
                    Return _Codigo
                End Get
                Set(ByVal value As String)
                    _Codigo = value
                End Set
            End Property

            Private _Actualiza As Boolean
            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return _Actualiza
                End Get
                Set(ByVal value As Boolean)
                    _Actualiza = value
                End Set
            End Property

            Public MustOverride ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina

            Public MustOverride ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
        End Class

#End Region


#Region "Técnicos"
        Public Class zctClCatTecnico
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Nombre As String = ""

            Public Property Nombre() As String
                Get
                    Return _Nombre
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Nombre = value
                End Set
            End Property

            Private _ApPaterno As String

            Public Property ApPaterno() As String
                Get
                    Return _ApPaterno
                End Get
                Set(ByVal value As String)
                    _ApPaterno = value
                    Me.Actualiza = True
                End Set
            End Property

            Private _ApMaterno As String
            Public Property ApMaterno() As String
                Get
                    Return _ApMaterno
                End Get
                Set(ByVal value As String)
                    _ApMaterno = value
                    Me.Actualiza = True
                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "EXECUTE SP_ZctCatTec 3, '" & Me.Codigo & "', '" & _Nombre & "', '" & _ApPaterno & "', '" & _ApMaterno & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "EXECUTE SP_ZctCatTec 2, '" & Me.Codigo & "', '" & _Nombre & "', '" & _ApPaterno & "', '" & _ApMaterno & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaTecnicos
            Inherits List(Of zctClCatTecnico)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatTecnico

                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Tec"))
                    iElemento.Nombre = Row("Nom_Tec").ToString
                    iElemento.ApPaterno = Row("ApPat_Tec").ToString
                    iElemento.ApMaterno = Row("ApMat_Tec").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT     Cod_Tec, Nom_Tec, ApPat_Tec, ApMat_Tec  FROM ZctCatTec"
                End Get
            End Property
        End Class
#End Region

#Region "Regiones"
        Public Class zctClCatRegion
            Inherits ZctClCatGenString


            Public Shadows Property Codigo() As String
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As String)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Region As String

            Public Property Region() As String
                Get
                    Return _Region
                End Get
                Set(ByVal value As String)
                    _Region = value
                    Me.Actualiza = True
                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "EXECUTE SP_ZctCatRegion 3, '" & Me.Codigo & "', '" & _Region & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "EXECUTE SP_ZctCatRegion 2, '" & Me.Codigo & "', '" & _Region & "', 1"
                End Get
            End Property
        End Class


        Public Class ListaRegion
            Inherits List(Of zctClCatRegion)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatRegion
                    iElemento.Codigo = Row("Cod_Region").ToString
                    iElemento.Region = Row("Desc_Region").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Region, Desc_Region FROM ZctCatRegion"
                End Get
            End Property
        End Class
#End Region

#Region "Proveedores"
        Public Class zctClCatProv
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Nombre As String
            Public Property Nombre() As String
                Get
                    Return _Nombre
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Nombre = value
                End Set
            End Property


            Private _RFC As String
            Public Property RFC() As String
                Get
                    Return _RFC
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _RFC = value
                End Set
            End Property


            Private _Dir As String
            Public Property Direccion() As String
                Get
                    Return _Dir
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Dir = value
                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "EXECUTE SP_ZctCatProv 3, '" & Me.Codigo & "', '" & _Nombre & "', '" & _RFC & "', '" & _Dir & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "EXECUTE SP_ZctCatProv 2, '" & Me.Codigo & "', '" & _Nombre & "', '" & _RFC & "', '" & _Dir & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaProveedores
            Inherits List(Of zctClCatProv)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatProv
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Prov"))
                    iElemento.Nombre = Row("Nom_Prov").ToString
                    iElemento.RFC = Row("RFC_Prov").ToString
                    iElemento.Direccion = Row("Dir_Prov").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Prov, Nom_Prov, RFC_Prov, Dir_Prov FROM ZctCatProv"
                End Get
            End Property
        End Class

#End Region

#Region "Marcas"
        Public Class zctClCatMar
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Marca As String
            Public Property Marca() As String
                Get
                    Return _Marca
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Marca = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "EXECUTE SP_ZctCatMar 3, '" & Me.Codigo & "', '" & _Marca & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatMar 2, '" & Me.Codigo & "', '" & _Marca & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaMarcas
            Inherits List(Of zctClCatMar)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatMar
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Mar"))
                    iElemento.Marca = Row("Desc_Mar").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Mar, Desc_Mar FROM ZctCatMar"
                End Get
            End Property
        End Class
#End Region

#Region "Ubicaciones"
        Public Class zctClCatUbic
            Inherits ZctClCatGenString

            Public Shadows Property Codigo() As String
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As String)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Ubicacion As String
            Public Property Ubicacion() As String
                Get
                    Return _Ubicacion
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Ubicacion = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "EXECUTE SP_ZctCatUbicacion 3, '" & Me.Codigo & "', '" & _Ubicacion & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatUbicacion 2, '" & Me.Codigo & "', '" & _Ubicacion & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaUbicaciones
            Inherits List(Of zctClCatUbic)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatUbic
                    iElemento.Codigo = Row("Cod_Ubicacion").ToString
                    iElemento.Ubicacion = Row("Desc_Ubicacion").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Ubicacion, Desc_Ubicacion FROM ZctCatUbicacion"
                End Get
            End Property
        End Class
#End Region


#Region "Gerentes"


        Public Class zctClCatGte
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Gerente As String
            Public Property Gerente() As String
                Get
                    Return _Gerente
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Gerente = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "EXECUTE SP_ZctCatGte 3, '" & Me.Codigo & "', '" & _Gerente & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatGte 2, '" & Me.Codigo & "', '" & _Gerente & "', 1"
                End Get
            End Property
        End Class



        Public Class ListaGerentes
            Inherits List(Of zctClCatGte)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatGte
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Gte"))
                    iElemento.Gerente = Row("Nom_Gte").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Gte, Nom_Gte FROM ZctCatGte"
                End Get
            End Property
        End Class
#End Region


#Region "Responsables"



        Public Class zctClCatRep
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Nombre As String
            Public Property Nombre() As String
                Get
                    Return _Nombre
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Nombre = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "EXECUTE SP_ZctCatRep 3, '" & Me.Codigo & "', '" & Me.Nombre & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatRep 2, '" & Me.Codigo & "', '" & Me.Nombre & "', 1"
                End Get
            End Property
        End Class



        Public Class ListaResponsables
            Inherits List(Of zctClCatRep)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatRep
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Rep"))
                    iElemento.Nombre = Row("Nom_Rep").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Rep, Nom_Rep FROM ZctCatRep"
                End Get
            End Property
        End Class




#End Region

#Region "Almacenes"
        Public Class zctClCatAlmacen
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Almacen As String
            Public Property Almacen() As String
                Get
                    Return _Almacen
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Almacen = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "Execute SP_ZctCatAlm 3, '" & Me.Codigo & "', '" & _Almacen & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatAlm 2, '" & Me.Codigo & "', '" & _Almacen & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaAlmacenes
            Inherits List(Of zctClCatAlmacen)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatAlmacen
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Alm"))
                    iElemento.Almacen = Row("Desc_CatAlm").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Alm, Desc_CatAlm FROM ZctCatAlm"
                End Get
            End Property
        End Class
#End Region


#Region "Lineas"
        Public Class ZctClCatLinea
            Implements ZctSOT.Datos.Interfaces.IGenerico
            Private _Actualiza As Boolean = False
            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return _Actualiza
                End Get
                Set(ByVal value As Boolean)
                    _Actualiza = value
                End Set
            End Property

            Private _Cod_Linea As Integer = 0
            Public Property Codigo() As Integer
                Get
                    Return _Cod_Linea
                End Get
                Set(ByVal value As Integer)
                    If _Cod_Linea <> 0 Then Exit Property
                    _Cod_Linea = value
                End Set
            End Property

            Private _Desc_Linea As String
            Public Property Linea() As String
                Get
                    Return _Desc_Linea
                End Get
                Set(ByVal value As String)
                    _Actualiza = True
                    _Desc_Linea = value

                End Set
            End Property

            Private _Cod_Dpto As Integer = 0

            Public Property Cod_Dpto() As Integer
                Get
                    Return _Cod_Dpto
                End Get
                Set(ByVal value As Integer)
                    _Actualiza = True
                    _Cod_Dpto = value
                End Set
            End Property



            Public ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina
                Get
                    Return "SP_ZctCatLinea 3, '" & _Cod_Linea & "', '" & _Desc_Linea & "', '" & _Cod_Dpto & "', 1"
                End Get
            End Property

            Public ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
                Get
                    Return "SP_ZctCatLinea 2, '" & _Cod_Linea & "', '" & _Desc_Linea & "', '" & _Cod_Dpto & "', 1"
                End Get
            End Property


        End Class

        Public Class listaLineas
            Inherits List(Of ZctClCatLinea)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                While Row.Read
                    Dim iElemento As New ZctClCatLinea
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Linea"))
                    iElemento.Linea = Row("Desc_Linea").ToString
                    iElemento.Cod_Dpto = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Dpto"))
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Linea, Desc_Linea, Cod_Dpto FROM ZctCatLinea"
                End Get
            End Property

            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function
        End Class

#End Region

#Region "Modelos"
        Public Class ZctClCatModelo
            Implements ZctSOT.Datos.Interfaces.IGenerico
            Private _Actualiza As Boolean = False
            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return _Actualiza
                End Get
                Set(ByVal value As Boolean)
                    _Actualiza = value
                End Set
            End Property

            Private _Cod_Modelo As Integer = 0
            Public Property Codigo() As Integer
                Get
                    Return _Cod_Modelo
                End Get
                Set(ByVal value As Integer)
                    If _Cod_Modelo <> 0 Then Exit Property
                    _Cod_Modelo = value
                End Set
            End Property

            Private _Desc_Modelo As String
            Public Property Modelo() As String
                Get
                    Return _Desc_Modelo
                End Get
                Set(ByVal value As String)
                    _Actualiza = True
                    _Desc_Modelo = value

                End Set
            End Property

            Private _Cod_Mar As Integer = 0

            Public Property Cod_Mar() As Integer
                Get
                    Return _Cod_Mar
                End Get
                Set(ByVal value As Integer)
                    _Actualiza = True
                    _Cod_Mar = value
                End Set
            End Property



            Public ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina
                Get
                    Return "SP_ZctCatMod 3, '" & _Cod_Modelo & "', '" & _Desc_Modelo & "', '" & _Cod_Mar & "',1"
                End Get
            End Property

            Public ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
                Get
                    Return "SP_ZctCatMod 2, '" & _Cod_Modelo & "', '" & _Desc_Modelo & "', '" & _Cod_Mar & "', 1"
                End Get
            End Property


        End Class

        Public Class listaModelos
            Inherits List(Of ZctClCatModelo)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                While Row.Read
                    Dim iElemento As New ZctClCatModelo
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("CodMod_Mot"))
                    iElemento.Modelo = Row("Desc_Mod").ToString
                    iElemento.Cod_Mar = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Mar"))
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT     CodMod_Mot, Cod_Mar, Desc_Mod FROM ZctCatMod"
                End Get
            End Property

            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function
        End Class

#End Region


#Region "Estados"
        Public Class zctClCatEdo
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Estado As String
            Public Property Estado() As String
                Get
                    Return _Estado
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Estado = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "Execute SP_ZctCatEdo 3, '" & Me.Codigo & "', '" & _Estado & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatEdo 2, '" & Me.Codigo & "', '" & _Estado & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaEstados
            Inherits List(Of zctClCatEdo)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatEdo
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Edo"))
                    iElemento.Estado = Row("Desc_Edo").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Edo, Desc_Edo FROM  ZctCatEdo"
                End Get
            End Property
        End Class
#End Region


#Region "MarcaTiendas"
        Public Class zctClCatMarcaTienda
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _MarcaTienda As String
            Public Property MarcaTienda() As String
                Get
                    Return _MarcaTienda
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _MarcaTienda = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "Execute SP_ZctCatMarcaTienda 3, '" & Me.Codigo & "', '" & _MarcaTienda & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatMarcaTienda 2, '" & Me.Codigo & "', '" & _MarcaTienda & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaMarcaTiendas
            Inherits List(Of zctClCatMarcaTienda)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatMarcaTienda
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_MarcaTienda"))
                    iElemento.MarcaTienda = Row("Desc_MarcaTienda").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_MarcaTienda, Desc_MarcaTienda FROM  ZctCatMarcaTienda"
                End Get
            End Property
        End Class
#End Region


#Region "Ciudades"
        Public Class ZctClCatCiudades
            Implements ZctSOT.Datos.Interfaces.IGenerico
            Private _Actualiza As Boolean = False
            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return _Actualiza
                End Get
                Set(ByVal value As Boolean)
                    _Actualiza = value
                End Set
            End Property

            Private _Cod_Ciu As Integer = 0
            Public Property Codigo() As Integer
                Get
                    Return _Cod_Ciu
                End Get
                Set(ByVal value As Integer)
                    If _Cod_Ciu <> 0 Then Exit Property
                    _Cod_Ciu = value
                End Set
            End Property

            Private _Desc_Ciu As String
            Public Property Ciudad() As String
                Get
                    Return _Desc_Ciu
                End Get
                Set(ByVal value As String)
                    _Actualiza = True
                    _Desc_Ciu = value

                End Set
            End Property

            Private _Cod_Edo As Integer = 0

            Public Property Cod_Edo() As Integer
                Get
                    Return _Cod_Edo
                End Get
                Set(ByVal value As Integer)
                    _Actualiza = True
                    _Cod_Edo = value
                End Set
            End Property



            Public ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina
                Get
                    Return "SP_ZctCatCiu 3, '" & _Cod_Ciu & "', '" & _Desc_Ciu & "', '" & _Cod_Edo & "', 1"
                End Get
            End Property

            Public ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
                Get
                    Return "SP_ZctCatCiu 2, '" & _Cod_Ciu & "', '" & _Desc_Ciu & "', '" & _Cod_Edo & "', 1"
                End Get
            End Property


        End Class

        Public Class listaCiudades
            Inherits List(Of ZctClCatCiudades)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                While Row.Read
                    Dim iElemento As New ZctClCatCiudades
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Ciu"))
                    iElemento.Ciudad = Row("Desc_Ciu").ToString
                    iElemento.Cod_Edo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Edo"))
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Ciu, Cod_Edo, Desc_Ciu  FROM ZctCatCiu"
                End Get
            End Property

            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function
        End Class

#End Region


#Region "Tipo de Ordenes"
        Public Class zctClCatTpOT
            Inherits ZctClCatGen

            Public Shadows Property Codigo() As Integer
                Get
                    Return MyBase.Codigo
                End Get
                Set(ByVal value As Integer)
                    MyBase.Codigo = value
                End Set
            End Property

            Private _Tipo_Orden As String
            Public Property Tipo_Orden() As String
                Get
                    Return _Tipo_Orden
                End Get
                Set(ByVal value As String)
                    Me.Actualiza = True
                    _Tipo_Orden = value

                End Set
            End Property

            Public Overrides ReadOnly Property SentenciaElimina() As String
                Get
                    Return "Execute SP_ZctCatTpOT 3, '" & Me.Codigo & "', '" & _Tipo_Orden & "', 1"
                End Get
            End Property

            Public Overrides ReadOnly Property SentenciaGraba() As String
                Get
                    Return "Execute SP_ZctCatTpOT 2, '" & Me.Codigo & "', '" & _Tipo_Orden & "', 1"
                End Get
            End Property
        End Class

        Public Class ListaTipo_Orden
            Inherits List(Of zctClCatTpOT)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento

                While Row.Read
                    Dim iElemento As New zctClCatTpOT
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_TpOT"))
                    iElemento.Tipo_Orden = Row("Desc_TpOT").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_TpOT, Desc_TpOT FROM ZctCatTpOT"
                End Get
            End Property
        End Class
#End Region




#Region "Zonas"
        Public Class ZctClCatZonas
            Implements ZctSOT.Datos.Interfaces.IGenerico
            Private _Actualiza As Boolean = False
            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return _Actualiza
                End Get
                Set(ByVal value As Boolean)
                    _Actualiza = value
                End Set
            End Property

            Private _Cod_Zona As Integer = 0
            Public Property Codigo() As Integer
                Get
                    Return _Cod_Zona
                End Get
                Set(ByVal value As Integer)
                    If _Cod_Zona <> 0 Then Exit Property
                    _Cod_Zona = value
                End Set
            End Property

            Private _Desc_Zona As String
            Public Property Zona() As String
                Get
                    Return _Desc_Zona
                End Get
                Set(ByVal value As String)
                    _Actualiza = True
                    _Desc_Zona = value

                End Set
            End Property

            Private _Cod_Region As String = ""

            Public Property Cod_Region() As String
                Get
                    Return _Cod_Region
                End Get
                Set(ByVal value As String)
                    _Actualiza = True
                    _Cod_Region = value
                End Set
            End Property



            Public ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina
                Get
                    Return "SP_ZctCatZona 3, '" & _Cod_Zona & "', '" & _Desc_Zona & "', '" & _Cod_Region & "', 1"
                End Get
            End Property

            Public ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
                Get
                    Return "SP_ZctCatZona 2, '" & _Cod_Zona & "', '" & _Desc_Zona & "', '" & _Cod_Region & "', 1"
                End Get
            End Property


        End Class

        Public Class listaZonas
            Inherits List(Of ZctClCatZonas)
            Implements ZctSOT.Datos.Interfaces.IEnumGenericoEnum


            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                While Row.Read
                    Dim iElemento As New ZctClCatZonas
                    iElemento.Codigo = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Zona"))
                    iElemento.Zona = Row("Desc_Zona").ToString
                    iElemento.Cod_Region = Row("Cod_Region").ToString
                    iElemento.Actualiza = False
                    Me.Add(iElemento)
                End While

            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Zona, Cod_Region, Desc_Zona  FROM ZctCatZona"
                End Get
            End Property

            Public Function GetEnumerator1() As System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico) Implements System.Collections.Generic.IEnumerable(Of Interfaces.IGenerico).GetEnumerator
                Return New Interfaces.EnumIGenerico(Me.ToArray)
            End Function
        End Class

#End Region


    End Namespace
End Namespace



