'Importa la clase de la base de datos
Imports ZctSOT.Datos.ZctDataBase
Imports ZctSOT.Datos.ZctFunciones
Imports ZctSOT.Datos.Clases.Catalogos

Public Class ClassGen
    Public Enum Tipo_Dato
        NUMERICO
        ALFANUMERICO
    End Enum

    Public Sub GrabaUsuario(ByVal Codigo As Integer, ByVal Formulario As String, ByVal Accion As String, ByVal Desc As String)
        Dim sVariables As String
        Dim sParametros As String
        Dim DtUsu As DataTable
        'verifica que exista el usuario
        sVariables = "@Cod_Usu;INT|@NomFrm_Seg;VARCHAR|@ActFrm_Seg;VARCHAR|@DescMod;VARCHAR"
        sParametros = Codigo & "|" & Formulario & "|" & Accion & "|" & Desc
        DtUsu = GenGetData(2, "SP_ZctSegFrm", sVariables, sParametros)
    End Sub

    Public Shared Function GenGetData(ByVal TpConsulta As Integer, ByVal sSpNAME As String, ByVal sSpVariables As String, ByVal sSpValores As String) As DataTable
        Try
            Dim PkBusCon As New ZctDataBase                         'Objeto de la base de datos
            Dim iSp As Integer                                      'Indice del procedimiento almacenado
            Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
            Dim sValores() As String = Split(sSpValores, "|")
            'Inicia el procedimieto almacenado
            PkBusCon.IniciaProcedimiento(sSpNAME)
            'Agrega el parametro que define que se va a realizar
            PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
            'Obtiene el total de los procedimientos
            Dim iTot As Integer = UBound(sVector)
            'Recorre las variables del procedimiento almacenado
            For iSp = 0 To iTot
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1).ToUpper
                    Case "INT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Integer), SqlDbType.Int)
                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Date), SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Decimal), SqlDbType.Decimal)
                    Case "TEXT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)
                End Select
            Next

            Select Case TpConsulta
                'SELECT
                Case 1
                    PkBusCon.InciaDataAdapter()
                    Return PkBusCon.GetReaderSP.Tables("DATOS")
                Case 2
                    'Ejecuta el escalar
                    PkBusCon.GetScalarSP()
                    Return Nothing

                Case 3
                    'Delete
                    PkBusCon.GetScalarSP()
                    Return Nothing
                Case Else
                    Return Nothing
            End Select

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return Nothing
        End Try
    End Function

    Public Shared Function GenGetData(ByVal sSpNAME As String, ByVal sSpVariables As String, ByVal sSpValores As String) As DataTable
        Try
            Dim PkBusCon As New ZctDataBase                         'Objeto de la base de datos
            Dim iSp As Integer                                      'Indice del procedimiento almacenado
            Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
            Dim sValores() As String = Split(sSpValores, "|")
            'Inicia el procedimieto almacenado
            PkBusCon.IniciaProcedimiento(sSpNAME)
            'Agrega el parametro que define que se va a realizar
            'PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
            'Obtiene el total de los procedimientos
            Dim iTot As Integer = UBound(sVector)
            'Recorre las variables del procedimiento almacenado
            For iSp = 0 To iTot
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1).ToUpper
                    Case "INT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Integer), SqlDbType.Int)
                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Date), SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Decimal), SqlDbType.Decimal)
                    Case "TEXT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)
                    Case "MONEY"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Integer), SqlDbType.Money)
                End Select
            Next
            PkBusCon.InciaDataAdapter()
            Return PkBusCon.GetReaderSP.Tables("DATOS")
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return Nothing
        End Try
    End Function
    Public Shared Function GenGetData(ByVal sSpNAME As String, ByVal sSpVariables As String, ByVal sSpValores As String, ByVal PkBusCon As ZctDataBase) As DataTable
        Try
            'Dim PkBusCon As New ZctDataBase                         'Objeto de la base de datos
            Dim iSp As Integer                                      'Indice del procedimiento almacenado
            Dim sVector() As String = Split(sSpVariables, "|")      'Obtiene los parametros del procedimiento
            Dim sValores() As String = Split(sSpValores, "|")
            'Inicia el procedimieto almacenado
            PkBusCon.IniciaProcedimiento(sSpNAME)
            'Agrega el parametro que define que se va a realizar
            'PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int)
            'Obtiene el total de los procedimientos
            Dim iTot As Integer = UBound(sVector)
            'Recorre las variables del procedimiento almacenado
            For iSp = 0 To iTot
                Select Case Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1).ToUpper
                    Case "INT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Integer), SqlDbType.Int)
                    Case "VARCHAR"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.VarChar)
                    Case "SMALLDATETIME"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Date), SqlDbType.SmallDateTime)
                    Case "DECIMAL"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), Decimal), SqlDbType.Decimal)
                    Case "TEXT"
                        PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)
                End Select
            Next
            PkBusCon.InciaDataAdapter()
            Return PkBusCon.GetReaderSPTrans.Tables("DATOS")
            PkBusCon.GetReaderSP()

        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Return Nothing
        End Try
    End Function



End Class

Public Class ListAlmacenes
    Inherits List(Of CatAlmacen)

    Private _SPName As String
    ''' <summary>
    ''' Nombre del procedimiento almacenado en donde se obtendra los datos
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPName() As String
        Get
            Return _SPName
        End Get
        Set(ByVal value As String)
            _SPName = value
        End Set
    End Property
    Private _SPParam As String
    ''' <summary>
    ''' Parametros del procedimiento almacenado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPParam() As String
        Get
            Return _SPParam
        End Get
        Set(ByVal value As String)
            _SPParam = value
        End Set
    End Property

    Private _SPValores As String
    ''' <summary>
    ''' Valores de los procedimientos almacenados
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPValores() As String
        Get
            Return _SPValores
        End Get
        Set(ByVal value As String)
            _SPValores = value
        End Set
    End Property


    Public Sub New(ByVal SPName As String, ByVal SPParam As String, ByVal SPValores As String)
        _SPName = SPName
        _SPParam = SPParam
        _SPValores = SPValores
        LLenaLista()
    End Sub

    'Llena la lista de los elementos
    Public Sub LLenaLista()
        If Not Me Is Nothing Then
            Me.Clear()
        End If
        If _SPName = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar el nombre del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        If _SPParam = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar los parametros del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        If _SPValores = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar los valores del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        Dim Dt As DataTable = Nothing
        Try
            Dt = ClassGen.GenGetData(1, _SPName, _SPParam, _SPValores)
            'Llena la lista
            For Each iRow As DataRow In Dt.Rows
                Me.Add(New CatAlmacen(GetGeneric(Of Integer)(iRow("Codigo")), GetGeneric(iRow("Descripcion"))))
            Next
        Catch ex As Exception
            Throw ex
        Finally
            Dt.Dispose()
        End Try
    End Sub


End Class


Public Interface IElementoGen
    Sub llenaDato(ByVal Midato As DataRow)
End Interface

Public Interface IListaElementoGen
    Property SPName() As String
    Property SPParam() As String
    Property SPValores() As String
    Sub llenaLista()
End Interface

Public Class ListaGen(Of T As {New, IElementoGen})
    Inherits List(Of T)
    Implements IListaElementoGen

    Public Sub New(ByVal SPName As String, ByVal SPParam As String, ByVal SPValores As String)
        _SPName = SPName
        _SPParam = SPParam
        _SPValores = SPValores
        llenaLista()
    End Sub
    ''' <summary>
    ''' Llena la lista de los elementos
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub llenaLista() Implements IListaElementoGen.llenaLista
        If Not Me Is Nothing Then
            Me.Clear()
        End If
        If _SPName = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar el nombre del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        If _SPParam = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar los parametros del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        If _SPValores = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar los valores del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        Dim Dt As DataTable = Nothing
        Try
            Dt = ClassGen.GenGetData(1, _SPName, _SPParam, _SPValores)
            'Llena la lista
            For Each iRow As DataRow In Dt.Rows
                Dim Elemento As New T
                Elemento.llenaDato(iRow)
                Me.Add(Elemento)

                'IElementoGen().llenaDato(iRow))

            Next
        Catch ex As Exception
            Throw ex
        Finally
            Dt.Dispose()
        End Try

    End Sub

    Private _SPName As String
    ''' <summary>
    ''' Nombre del procedimiento almacenado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPName() As String Implements IListaElementoGen.SPName
        Get
            Return _SPName
        End Get
        Set(ByVal value As String)
            _SPName = value
        End Set
    End Property

    Private _SPParam As String
    ''' <summary>
    ''' Parametros del Procedimiento
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPParam() As String Implements IListaElementoGen.SPParam
        Get
            Return _SPParam
        End Get
        Set(ByVal value As String)
            _SPParam = value
        End Set
    End Property

    Private _SPValores As String
    ''' <summary>
    ''' Valores del procedimiento Almacenado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPValores() As String Implements IListaElementoGen.SPValores
        Get
            Return _SPValores
        End Get
        Set(ByVal value As String)
            _SPValores = value
        End Set
    End Property
End Class


Public Interface IGenericTipo

End Interface

Public Interface IGeneric
    Property Codigo() As Integer
    Property Descripcion() As String
    Sub Carga(ByVal Codigo As Integer, ByVal Descripcion As String)
End Interface

Public Class Generic_Dpto
    Implements IGeneric
    Private _Cod_Dpto As Integer
    Private _Desc_Dpto As String

    Public Sub Carga(ByVal Codigo As Integer, ByVal Descripcion As String) Implements IGeneric.Carga
        _Cod_Dpto = Codigo
        _Desc_Dpto = Descripcion
    End Sub

    Public Property Cod_Dpto() As Integer Implements IGeneric.Codigo
        Get
            Return _Cod_Dpto
        End Get
        Set(ByVal value As Integer)
            _Cod_Dpto = value
        End Set
    End Property

    Public Property Descripcion() As String Implements IGeneric.Descripcion
        Get
            Return _Desc_Dpto
        End Get
        Set(ByVal value As String)
            _Desc_Dpto = value
        End Set
    End Property
End Class



Public Class ListGeneric
    Inherits List(Of IGeneric)

    Private _SPName As String
    ''' <summary>
    ''' Nombre del procedimiento almacenado en donde se obtendra los datos
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPName() As String
        Get
            Return _SPName
        End Get
        Set(ByVal value As String)
            _SPName = value
        End Set
    End Property
    Private _SPParam As String
    ''' <summary>
    ''' Parametros del procedimiento almacenado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPParam() As String
        Get
            Return _SPParam
        End Get
        Set(ByVal value As String)
            _SPParam = value
        End Set
    End Property

    Private _SPValores As String
    ''' <summary>
    ''' Valores de los procedimientos almacenados
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SPValores() As String
        Get
            Return _SPValores
        End Get
        Set(ByVal value As String)
            _SPValores = value
        End Set
    End Property


    Public Sub New(ByVal SPName As String, ByVal SPParam As String, ByVal SPValores As String)
        _SPName = SPName
        _SPParam = SPParam
        _SPValores = SPValores
        LLenaLista()
    End Sub

    'Llena la lista de los elementos
    Public Sub LLenaLista()
        If Not Me Is Nothing Then
            Me.Clear()
        End If
        If _SPName = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar el nombre del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        If _SPParam = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar los parametros del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        If _SPValores = "" Then Throw New ZctReglaNegocioEx("Debe proporcionar los valores del Procedimiento Almacenado", Nothing, "ListAlmacenes", "") : Exit Sub
        Dim Dt As DataTable = Nothing
        Try
            Dt = ClassGen.GenGetData(1, _SPName, _SPParam, _SPValores)
            'Llena la lista
            For Each iRow As DataRow In Dt.Rows
                Dim t As  IGeneric
                t.Carga(GetGeneric(Of Integer)(iRow(0)), iRow(1).ToString)
                Me.Add(t)
            Next
        Catch ex As Exception
            Throw ex
        Finally
            Dt.Dispose()
        End Try
    End Sub


End Class

