﻿Namespace Entidades
    <DebuggerNonUserCode()> _
    Public Class zctInvMotosHist
        Inherits zctCatMotos

        Private _Cod_Inv As String
        ''' <summary>
        ''' Códgio de inventario
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Cod_Inv() As String
            Get
                Return _Cod_Inv
            End Get
            Set(ByVal value As String)
                _Cod_Inv = value
            End Set
        End Property

        'Private _Cod_Moto As Integer
        '''' <summary>
        '''' Código de Moto
        '''' </summary>
        '''' <value></value>
        '''' <returns></returns>
        '''' <remarks></remarks>
        'Public Property Cod_Moto() As Integer
        '    Get
        '        Return _Cod_Moto
        '    End Get
        '    Set(ByVal value As Integer)
        '        _Cod_Moto = value
        '    End Set
        'End Property

        Private _Cod_Accion As Integer
        ''' <summary>
        ''' Código de Acción
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Cod_Accion() As Integer
            Get
                Return _Cod_Accion
            End Get
            Set(ByVal value As Integer)
                _Cod_Accion = value
            End Set
        End Property

        Private _Observaciones As String
        ''' <summary>
        ''' Código de Observaciones
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Observaciones() As String
            Get
                Return _Observaciones
            End Get
            Set(ByVal value As String)
                _Observaciones = value
            End Set
        End Property

        Private _Cod_Usu As Integer
        ''' <summary>
        ''' Código de usuario
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Cod_Usu() As Integer
            Get
                Return _Cod_Usu
            End Get
            Set(ByVal value As Integer)
                _Cod_Usu = value
            End Set
        End Property

        Private _desc_Mar As String

        Public Property Desc_Mar() As String
            Get
                Return _desc_Mar
            End Get
            Set(ByVal value As String)
                _desc_Mar = value
            End Set
        End Property

        Private _Desc_Mod As String
        Public Property Desc_Mod() As String
            Get
                Return _Desc_Mod
            End Get
            Set(ByVal value As String)
                _Desc_Mod = value
            End Set
        End Property

        Private _Estatus As EnumEstatusMotHist = EnumEstatusMotHist.No_Encontrado_Archivo
        Public Property Estatus() As EnumEstatusMotHist
            Get
                Return _Estatus
            End Get
            Set(ByVal value As EnumEstatusMotHist)
                _Estatus = value
            End Set
        End Property

        Public ReadOnly Property Estatus_Text() As String
            Get
                Select Case _Estatus
                    Case 0
                        Return "No Encontrado"
                    Case 1
                        Return "Encontrado"
                    Case 2
                        Return "Encontrado con otro cliente"
                    Case 3
                        Return "No Encontrado Archivo"
                    Case Else
                        Return ""
                End Select
            End Get
        End Property


        Private _Accion As EnumAccionesMotHist = EnumAccionesMotHist.Eliminar
        Public Property Accion() As EnumAccionesMotHist
            Get
                Return _Accion
            End Get
            Set(ByVal value As EnumAccionesMotHist)
                'Valida el estatus seleccionado
                Select Case _Estatus
                    Case EnumEstatusMotHist.Encontrado
                        If value = EnumAccionesMotHist.No_Accion Then
                            _Accion = value
                        End If
                    Case EnumEstatusMotHist.Encontrado_Otro_Cliente
                        If value = EnumAccionesMotHist.Cambiar_Cliente OrElse value = EnumAccionesMotHist.No_Accion Then
                            _Accion = value
                        End If
                    Case EnumEstatusMotHist.No_Encontrado
                        If value = EnumAccionesMotHist.Agregar OrElse value = EnumAccionesMotHist.No_Accion Then
                            _Accion = value
                        End If
                    Case EnumEstatusMotHist.No_Encontrado_Archivo
                        If value = EnumAccionesMotHist.Eliminar OrElse value = EnumAccionesMotHist.No_Accion Then
                            _Accion = value
                        End If

                End Select

            End Set
        End Property

        Private _CteCambio As Integer = 0
        Public Property CteCambio() As Integer
            Get
                Return _CteCambio
            End Get
            Set(ByVal value As Integer)
                _CteCambio = value
            End Set
        End Property

        Public Sub New(ByVal Cod_Inv As String, ByVal Cod_Cte As Integer, ByVal Cod_Mot As Integer, ByVal Vin As String)
            Me.Cod_Cte = Cod_Cte
            Me.Cod_Inv = Cod_Inv
            Me.Cod_Mot = Cod_Mot
            Me.Vin_Mot = Vin

        End Sub

        Public Sub New()
        End Sub

        Public Overrides Sub llenaDato(ByVal Midato As System.Data.DataRow)
            MyBase.llenaDato(Midato)

            Me.Cod_Cte = ZctFunciones.GetGeneric(Of Integer)(Midato("Cod_Cte"))
            Me.Cod_Inv = Midato("Cod_Inv").ToString
            Me.Cod_Mot = ZctFunciones.GetGeneric(Of Integer)(Midato("Cod_Mot"))
            _Desc_Mod = Midato("Desc_Mod").ToString
            _desc_Mar = Midato("Desc_Mar").ToString
            Me.Vin_Mot = Midato("Vin_Mot").ToString
        End Sub
    End Class

    Public Enum EnumEstatusMotHist
        No_Encontrado
        Encontrado
        Encontrado_Otro_Cliente
        No_Encontrado_Archivo
    End Enum

    Public Enum EnumAccionesMotHist
        No_Accion
        Agregar
        Eliminar
        Cambiar_Cliente
    End Enum






End Namespace