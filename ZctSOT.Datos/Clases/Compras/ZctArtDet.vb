﻿Namespace Clases
    Namespace Compras
        Public Class ZctArtDet
            Inherits Catalogos.ZctCatArt

            Private _Cod_Alm As Integer
            Public Property Cod_Alm() As Integer
                Get
                    Return _Cod_Alm
                End Get
                Set(ByVal value As Integer)
                    _Cod_Alm = value
                End Set
            End Property

            Private _OC As Double
            Public Property OC() As Double
                Get
                    Return _OC
                End Get
                Set(ByVal value As Double)
                    _OC = value
                End Set
            End Property

            Private _LT As Double
            Public Property LT() As Double
                Get
                    Return _LT
                End Get
                Set(ByVal value As Double)
                    _LT = value
                End Set
            End Property

            Private _SST As Double

            Public Property SST() As Double
                Get
                    Return _SST
                End Get
                Set(ByVal value As Double)
                    _SST = value
                End Set
            End Property

            Private _SS As Double

            Public Property SS() As Double
                Get
                    Return _SS
                End Get
                Set(ByVal value As Double)
                    _SS = value
                End Set
            End Property

            Private _MovProm As Double
            Public Property MovProm() As Double
                Get
                    Return _MovProm
                End Get
                Set(ByVal value As Double)
                    _MovProm = value
                End Set
            End Property

            Private _Transito As Double
            Public Property Transito() As Double
                Get
                    Return _Transito
                End Get
                Set(ByVal value As Double)
                    _Transito = value
                End Set
            End Property

            Private _BO As Double
            Public Property BO() As Double
                Get
                    Return _BO
                End Get
                Set(ByVal value As Double)
                    _BO = value
                End Set
            End Property

            Private _SOQ As Double
            ''' <summary>
            ''' Compra sugerida
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property SOQ() As Double
                Get
                    Return _SOQ
                End Get
                Set(ByVal value As Double)
                    _SOQ = value
                End Set
            End Property

private _DiasHabilies As Double
            Public Property DiasHabilies() As Double
                Get
                    return _DiasHabilies
                End Get
                Set(ByVal value As Double)
                    _DiasHabilies = value
                End Set
            End Property

            'Esto me recuerda a mi epoca de nob pero funciona ..
            'Si mis empleados se burlan de esto y me entero, los corro!!
            Private _array(12) As Double
            ''' <summary>
            ''' Almacena el array de los meses.
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property ArrayMeses() As Double()
                Get
                    Return _array
                End Get
                Set(ByVal value As Double())
                    _array = value
                End Set
            End Property

            Public ReadOnly Property Mes1() As Double
                Get
                    Return _array(1)
                End Get
            End Property

            public readonly property Mes2 as double
                Get
                    return _array(2)
                End Get
            End Property

            public readonly property Mes3 as double
                Get
                    return _array(3)
                End Get
            End Property

            public readonly property Mes4 as double
                Get
                    return _array(4)
                End Get
            End Property

            public readonly property Mes5 as double
                Get
                    return _array(5)
                End Get
            End Property

            public readonly property Mes6 as double
                Get
                    return _array(6)
                End Get
            End Property

            public readonly property Mes7 as double
                Get
                    return _array(7)
                End Get
            End Property

            public readonly property Mes8 as double
                Get
                    return _array(8)
                End Get
            End Property

            public readonly property Mes9 as double
                Get
                    return _array(9)
                End Get
            End Property

            public readonly property Mes10 as double
                Get
                    return _array(10)
                End Get
            End Property

            public readonly property Mes11 as double
                Get
                    return _array(11)
                End Get
            End Property

            public readonly property Mes12 as double
                Get
                    return _array(12)
                End Get
            End Property




            Private _MIP As Double
            Private _MAD As Double

            Private _MesesDis As Integer = 1

            Public Property MesesDis() As Integer
                Get
                    Return _MesesDis
                End Get
                Set(ByVal value As Integer)
                    If value < 0 Or value > 12 Then value = 1
                    _MesesDis = value
                End Set
            End Property

            Private _AComprar As Double = 0

            Public Property AComprar() As Double
                Get
                    return _AComprar
                End Get
                Set(ByVal value As Double)
                    If value < 0 Then value = 0
                    _AComprar = value
                End Set
            End Property

            Private _Estatus As Estatus_OCS
            Public ReadOnly Property Estatus() As Estatus_OCS
                Get
                    Return _Estatus
                End Get
            End Property


            Public Sub Calcula()
                '_MovProm = (_Mes1 + _Mes2 + _Mes3 + _Mes4 + _Mes5 + _Mes6 + _Mes7 + _Mes8 + _Mes9 + _Mes10 + _Mes11 + _Mes12) / 12
                _MovProm = 0
                For i As Integer = 1 To _MesesDis
                    _MovProm = _MovProm + _array(i)
                Next

                _MovProm = _MovProm / _MesesDis
                _MIP = _MovProm * ((_OC / _DiasHabilies) + (_LT / _DiasHabilies) + (_SS / _DiasHabilies))
                _SOQ = Math.Round(_MIP - Me.Exist_Art, 0)

                If _SOQ = 0 Then
                    _Estatus = Estatus_OCS.En_Equilibrio
                    _AComprar = _SOQ
                ElseIf _SOQ > 0 Then
                    _Estatus = Estatus_OCS.Faltante
                    _AComprar = _SOQ
                Else
                    _Estatus = Estatus_OCS.Sobrante
                    _AComprar = 0
                End If



            End Sub
        End Class

        Public Enum Estatus_OCS
            En_Equilibrio
            Sobrante
            Faltante
        End Enum



    End Namespace
End Namespace

