﻿Namespace Clases
    Namespace Compras

        Public Class ZctConfOCS
            Private _Cod_Alm As Integer
            ''' <summary>
            ''' Código de Almacén
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Alm() As Integer
                Get
                    Return _Cod_Alm

                End Get
                Set(ByVal value As Integer)
                    _Cod_Alm = value
                End Set
            End Property

            Private _OC As Double
            ''' <summary>
            ''' Ciclo de Compra
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property OC() As Double
                Get
                    Return _OC
                End Get
                Set(ByVal value As Double)
                    _OC = value
                End Set
            End Property

            Private _LT As Double
            ''' <summary>
            ''' Tiempo de entrega
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property LT() As Double
                Get
                    Return _LT
                End Get
                Set(ByVal value As Double)
                    _LT = value
                End Set
            End Property

            Private _SS As Double
            ''' <summary>
            ''' Stock de seguridad 
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property SS() As Double
                Get
                    Return _SS
                End Get
                Set(ByVal value As Double)
                    _SS = value
                End Set
            End Property

            Private _DiasHabiles As Double
            ''' <summary>
            ''' Días Habilies
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property DiasHabiles() As Double
                Get
                    Return _DiasHabiles
                End Get
                Set(ByVal value As Double)
                    _DiasHabiles = value
                End Set
            End Property

            Private _Meses As Integer
            ''' <summary>
            ''' Meses a conciderar
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Meses() As Integer
                Get
                    return _Meses
                End Get
                Set(ByVal value As Integer)
                    If value < 0 Or value > 12 Then value = 1
                    _Meses = value
                End Set
            End Property

        End Class
    End Namespace
End Namespace
