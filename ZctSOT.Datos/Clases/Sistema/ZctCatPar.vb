﻿Namespace Clases
    Namespace Sistema

    
        Public Class ZctCatPar
            Implements Interfaces.IEnumGenerico
            Implements Interfaces.IGenericoSingle

            Private _Iva_CatPar As Double
            ''' <summary>
            ''' Iva declarado
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Iva_CatPar() As Double
                Get
                    Return _Iva_CatPar
                End Get
                Set(ByVal value As Double)
                    _Iva_CatPar = value
                End Set
            End Property
            Private _MaximoCaja As Double
            ''' <summary>
            ''' Maximo en caja pra realizar un retiro 
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property MaximoCaja
                Set(value)
                    _MaximoCaja = value
                End Set
                Get
                    Return _MaximoCaja
                End Get
            End Property
            Private _MinimoCaja As Double
            ''' <summary>
            ''' Minimo en caja despues del corte 
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property MinimoCaja As Double
                Set(value As Double)
                    _MinimoCaja = value
                End Set
                Get
                    Return _MinimoCaja
                End Get
            End Property
            Private _RutaImg As String
            ''' <summary>
            ''' Ruta de guardado para imagenes del catalogo de articulos
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property RutaImagenesArticulos As String
                Set(value As String)
                    _RutaImg = value
                End Set
                Get
                    Return _RutaImg
                End Get
            End Property

            Public Property Reporteador_web As String
            Public Property Taller As String
            Public Property cod_alm_entrada As Integer
            Public Property cod_alm_salida As Integer
            Public Property cod_cte_default As Integer

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                If Row.Read Then
                    _Iva_CatPar = ZctFunciones.GetGeneric(Of Decimal)(Row("Iva_CatPar"))
                    _MaximoCaja = ZctFunciones.GetGeneric(Of Decimal)(Row("maximo_caja"))
                    _MinimoCaja = ZctFunciones.GetGeneric(Of Decimal)(Row("minimo_caja"))
                    cod_alm_entrada = ZctFunciones.GetGeneric(Of Integer)(Row("cod_alm_entrada"))
                    cod_alm_salida = ZctFunciones.GetGeneric(Of Integer)(Row("cod_alm_salida"))
                    cod_cte_default = ZctFunciones.GetGeneric(Of Integer)(Row("cod_cte_default"))

                    If IsDBNull(Row("ruta_imagenes_articulos")) = False Then
                        _RutaImg = Row("ruta_imagenes_articulos")
                    Else
                        _RutaImg = "C:\Reportes\Imagenes Articulos"
                    End If
                    Reporteador_web = Row("Reporteador_web").ToString
                    Taller = Row("taller").ToString
                End If
            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                If _Iva_CatPar < 0 Then Throw New ZctReglaNegocioEx("El valor del IVA no puede ser negativo.")
                If _MaximoCaja < 0 Then Throw New ZctReglaNegocioEx("El valor del Maximo en caja no puede ser negativo.")
                If _MinimoCaja < 0 Then Throw New ZctReglaNegocioEx("El valor del MINIMO en caja no puede ser negativo.")
                If _MinimoCaja < 0 Then Throw New ZctReglaNegocioEx("Debe especificar una ruta para las imagenes de articulos.")
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT  Iva_CatPar,minimo_caja,maximo_caja, Reporteador_web,ruta_imagenes_articulos, taller, cod_alm_entrada, cod_alm_salida, cod_cte_default FROM ZctCatPar"
                End Get
            End Property

            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return True
                End Get
                Set(ByVal value As Boolean)

                End Set
            End Property

            Public ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina
                Get
                    Return ""
                End Get
            End Property

            Public ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
                Get
                    Return "EXECUTE SP_ZctCatPar " & _Iva_CatPar & "," & _MinimoCaja & "," & _MaximoCaja & ",'" & _RutaImg & "'"
                End Get
            End Property

            Public Sub validar() Implements Interfaces.IGenericoSingle.validar

            End Sub
        End Class

    End Namespace
End Namespace