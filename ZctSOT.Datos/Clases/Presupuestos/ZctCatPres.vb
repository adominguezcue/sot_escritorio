﻿Imports ZctSQL

Public Class ZctPresupuesto

    Private _Cod_Cte As Integer
    ''' <summary>
    ''' Código de cliente
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Cod_Cte() As Integer
        Get
            Return _Cod_Cte
        End Get
        Set(ByVal value As Integer)
            _Cod_Cte = value
        End Set
    End Property

    Private _NombreCte As String
    Public Property NombreCte() As String
        Get
            Return _NombreCte
        End Get
        Set(ByVal value As String)
            _NombreCte = value
        End Set
    End Property

    Private _Anno_Pres As Integer
    ''' <summary>
    ''' Año del presupuesto
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Anno_Pres() As Integer
        Get
            Return _Anno_Pres
        End Get
        Set(ByVal value As Integer)
            _Anno_Pres = value
        End Set
    End Property

    Private _enero As Double
    Public Property Enero() As Double
        Get
            Return _enero
        End Get
        Set(ByVal value As Double)
            _enero = value
        End Set
    End Property

    Private _febrero As Double
    Public Property Febrero() As Double
        Get
            Return _febrero
        End Get
        Set(ByVal value As Double)
            _febrero = value
        End Set
    End Property

    Private _marzo As Double
    Public Property Marzo() As Double
        Get
            Return _marzo
        End Get
        Set(ByVal value As Double)
            _marzo = value
        End Set
    End Property

    Private _abril As Double
    Public Property Abril() As Double
        Get
            Return _abril
        End Get
        Set(ByVal value As Double)
            _abril = value
        End Set
    End Property

    Private _mayo As Double
    Public Property Mayo() As Double
        Get
            Return _mayo
        End Get
        Set(ByVal value As Double)
            _mayo = value
        End Set
    End Property

    Private _junio As Double
    Public Property Junio() As Double
        Get
            Return _junio
        End Get
        Set(ByVal value As Double)
            _junio = value
        End Set
    End Property

    Private _julio As Double
    Public Property Julio() As Double
        Get
            Return _julio
        End Get
        Set(ByVal value As Double)
            _julio = value
        End Set
    End Property

    Private _agosto As Double
    Public Property Agosto() As Double
        Get
            Return _agosto
        End Get
        Set(ByVal value As Double)
            _agosto = value
        End Set
    End Property

    Private _septiembre As Double
    Public Property Septiembre() As Double
        Get
            Return _septiembre
        End Get
        Set(ByVal value As Double)
            _septiembre = value
        End Set
    End Property

    Private _octubre As Double
    Public Property Octubre() As Double
        Get
            Return _octubre
        End Get
        Set(ByVal value As Double)
            _octubre = value
        End Set
    End Property

    Private _noviembre As Double
    Public Property Noviembre() As Double
        Get
            Return _noviembre
        End Get
        Set(ByVal value As Double)
            _noviembre = value
        End Set
    End Property

    Private _diciembre As Double
    Public Property Diciembre() As Double
        Get
            Return _diciembre
        End Get
        Set(ByVal value As Double)
            _diciembre = value
        End Set
    End Property
    Private _almacen As Double
    Public Property CodAlmacen() As Double
        Get
            Return _almacen
        End Get
        Set(ByVal value As Double)
            _almacen = value
        End Set
    End Property

End Class

Public Class ZctPresupuesto_Excel
    Inherits ZctPresupuesto_Elemento

    <ZctDBAtributoOrigen("A")> _
    Public Shadows Property Cod_Cte() As Integer
        Get
            Return MyBase.Cod_Cte
        End Get
        Set(ByVal value As Integer)
            MyBase.Cod_Cte = Math.Round(value)
        End Set
    End Property

    Public Shadows Property Anno_Pres() As Integer
        Get
            Return MyBase.Anno_Pres
        End Get
        Set(ByVal value As Integer)
            MyBase.Anno_Pres = Math.Round(value)
        End Set
    End Property


    <ZctDBAtributoOrigen("B")> _
    Public Shadows Property Enero() As Double
        Get
            Return MyBase.Enero
        End Get
        Set(ByVal value As Double)
            MyBase.Enero = Math.Round(value)
        End Set
    End Property


    <ZctDBAtributoOrigen("C")> _
    Public Shadows Property Febrero() As Double
        Get
            Return MyBase.Febrero
        End Get
        Set(ByVal value As Double)
            MyBase.Febrero = Math.Round(value)
        End Set
    End Property


    <ZctDBAtributoOrigen("D")> _
    Public Shadows Property Marzo() As Double
        Get
            Return MyBase.Marzo
        End Get
        Set(ByVal value As Double)
            MyBase.Marzo = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("E")> _
    Public Shadows Property Abril() As Double
        Get
            Return MyBase.Abril
        End Get
        Set(ByVal value As Double)
            MyBase.Abril = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("F")> _
    Public Shadows Property Mayo() As Double
        Get
            Return MyBase.Mayo
        End Get
        Set(ByVal value As Double)
            MyBase.Mayo = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("G")> _
    Public Shadows Property Junio() As Double
        Get
            Return MyBase.Junio
        End Get
        Set(ByVal value As Double)
            MyBase.Junio = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("H")> _
    Public Shadows Property Julio() As Double
        Get
            Return MyBase.Julio
        End Get
        Set(ByVal value As Double)
            MyBase.Julio = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("I")> _
    Public Shadows Property Agosto() As Double
        Get
            Return MyBase.Agosto
        End Get
        Set(ByVal value As Double)
            MyBase.Agosto = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("J")> _
    Public Shadows Property Septiembre() As Double
        Get
            Return MyBase.Septiembre
        End Get
        Set(ByVal value As Double)
            MyBase.Septiembre = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("K")> _
    Public Shadows Property Octubre() As Double
        Get
            Return MyBase.Octubre
        End Get
        Set(ByVal value As Double)
            MyBase.Octubre = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("L")> _
    Public Shadows Property Noviembre() As Double
        Get
            Return MyBase.Noviembre
        End Get
        Set(ByVal value As Double)
            MyBase.Noviembre = Math.Round(value)
        End Set
    End Property

    <ZctDBAtributoOrigen("M")> _
    Public Shadows Property Diciembre() As Double
        Get
            Return MyBase.Diciembre
        End Get
        Set(ByVal value As Double)
            MyBase.Diciembre = Math.Round(value)
        End Set
    End Property

End Class

Public Class ZctPresupuesto_Elemento
    Inherits ZctPresupuesto
    Implements IZctElemento


    Public ReadOnly Property Clone() As IZctElemento Implements IZctElemento.Clone
        Get

            Dim elemento As New ZctPresupuesto_Elemento
            elemento.Anno_Pres = Me.Anno_Pres
            elemento.CodAlmacen = Me.CodAlmacen
            'elemento.Desc_Edo = Me.Desc_Edo
            Return elemento
        End Get
    End Property

    Public ReadOnly Property DelSQL() As String Implements IZctElemento.DelSQL
        Get
            Return "DELETE FROM ZctPresupuestos WHERE Cod_Cte = '" & Me.Cod_Cte.ToString & "'"
        End Get
    End Property

    Public ReadOnly Property GetSQL() As String Implements IZctElemento.GetSQL
        Get
            'If _Anno = 0 Then Throw New ZctReglaNegocioEx("Debe de proporcionar el año")

            Return "SELECT ZctPresupuestos.Cod_Cte, ISNULL(ZctCatCte.Nom_Cte, SPACE(0)) + SPACE(1) + ISNULL(ZctCatCte.ApPat_Cte, SPACE(0)) + ISNULL(ZctCatCte.ApMat_Cte, SPACE(0)) AS cliente, ZctPresupuestos.Anno_Pres, ZctPresupuestos.Enero_Pres, ZctPresupuestos.Febrero_Pres, ZctPresupuestos.Marzo_Pres, ZctPresupuestos.Abril_Pres, ZctPresupuestos.Mayo_Pres, ZctPresupuestos.Junio_Pres, ZctPresupuestos.Julio_Pres, ZctPresupuestos.Agosto_Pres, ZctPresupuestos.Septiembre_Pres, ZctPresupuestos.Octubre_Pres, ZctPresupuestos.Noviembre_Pres, ZctPresupuestos.Diciembre_Pres,cod_alm FROM ZctPresupuestos INNER JOIN ZctCatCte ON ZctPresupuestos.Cod_Cte = ZctCatCte.Cod_Cte WHERE  ZctPresupuestos.Anno_Pres = '{0}' and ZctPresupuestos.cod_alm = '{1}'"
        End Get
    End Property

    Private _Modificado As Boolean = True
    Public Property Modificado() As Boolean Implements IZctElemento.Modificado
        Get
            Return _Modificado
        End Get
        Set(ByVal value As Boolean)
            _Modificado = value
        End Set
    End Property

    Public Sub SetDatos(ByVal row As System.Data.DataRow) Implements IZctElemento.SetDatos

        Me.Cod_Cte = ZctFunciones.GetGeneric(Of Integer)(row("Cod_Cte"))
        Me.Anno_Pres = ZctFunciones.GetGeneric(Of Integer)(row("Anno_Pres"))
        Me.Enero = ZctFunciones.GetGeneric(Of Decimal)(row("Enero_Pres"))
        Me.Febrero = ZctFunciones.GetGeneric(Of Decimal)(row("Febrero_Pres"))
        Me.Marzo = ZctFunciones.GetGeneric(Of Decimal)(row("Marzo_Pres"))
        Me.Abril = ZctFunciones.GetGeneric(Of Decimal)(row("Abril_Pres"))
        Me.Mayo = ZctFunciones.GetGeneric(Of Decimal)(row("Mayo_Pres"))
        Me.Junio = ZctFunciones.GetGeneric(Of Decimal)(row("Junio_Pres"))
        Me.Julio = ZctFunciones.GetGeneric(Of Decimal)(row("Julio_Pres"))
        Me.Agosto = ZctFunciones.GetGeneric(Of Decimal)(row("Agosto_Pres"))
        Me.Septiembre = ZctFunciones.GetGeneric(Of Decimal)(row("Septiembre_Pres"))
        Me.Octubre = ZctFunciones.GetGeneric(Of Decimal)(row("Octubre_Pres"))
        Me.Noviembre = ZctFunciones.GetGeneric(Of Decimal)(row("Noviembre_Pres"))
        Me.Diciembre = ZctFunciones.GetGeneric(Of Decimal)(row("Diciembre_Pres"))
        Me.NombreCte = row("cliente").ToString
        Me.CodAlmacen = ZctFunciones.GetGeneric(Of Integer)(row("cod_alm"))
        Me.Modificado = False
    End Sub

    Public ReadOnly Property SetSQL() As String Implements IZctElemento.SetSQL
        Get
            Return "SP_ZctPresupuestos '" & Me.Cod_Cte & "', '" & Me.Anno_Pres & "', '" & Me.Enero & "', '" & Me.Febrero & "', '" & Me.Marzo & "', '" & Me.Abril & "', '" & Me.Mayo & "', '" & Me.Junio & "', '" & Me.Julio & "', '" & Me.Agosto & "', '" & Me.Septiembre & "', '" & Me.Octubre & "', '" & Me.Noviembre & "', '" & Me.Diciembre & "'," & Me.CodAlmacen
        End Get
    End Property
End Class
