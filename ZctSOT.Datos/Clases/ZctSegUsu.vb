﻿Namespace Clases
    Namespace Seguridad
        Public Class ZctSegUsu
            Implements ZctSOT.Datos.Interfaces.IGenericoSingle
            Implements ZctSOT.Datos.Interfaces.IEnumGenerico

            Private _Cod_Usu As Integer = 0
            ''' <summary>
            ''' Código de usuario
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Usu() As Integer
                Get
                    Return _Cod_Usu
                End Get
                Set(ByVal value As Integer)
                    _Cod_Usu = value
                End Set
            End Property

            Private _Nom_Usu As String = Nothing
            ''' <summary>
            ''' Nombre de usuario
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Nom_Usu() As String
                Get
                    Return _Nom_Usu
                End Get
                Set(ByVal value As String)
                    _Nom_Usu = value
                End Set
            End Property

            Private _Pass_Usu As String
            ''' <summary>
            ''' Password del usuario
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Pass_Usu() As String
                Get
                    Return _Pass_Usu
                End Get
                Set(ByVal value As String)
                    _Pass_Usu = value
                End Set
            End Property

            Private _Pass_Usu2 As String
            ''' <summary>
            ''' Password del usuario
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Pass_Usu2() As String
                Get
                    Return _Pass_Usu2
                End Get
                Set(ByVal value As String)
                    _Pass_Usu2 = value
                End Set
            End Property
            Private _Vendedor As Boolean
            Public Property Vendedor As Boolean
                Set(value As Boolean)
                    _Vendedor = value
                End Set
                Get
                    Return _Vendedor
                End Get
            End Property

            Public Property Actualiza() As Boolean Implements Interfaces.IGenerico.Actualiza
                Get
                    Return True
                End Get
                Set(ByVal value As Boolean)
                    value = True
                End Set
            End Property

            Public ReadOnly Property SentenciaElimina() As String Implements Interfaces.IGenerico.SentenciaElimina
                Get
                    Return "EXECUTE SP_ZctSegUsu 3, '" & _Cod_Usu & "', '" & _Nom_Usu & "', '" & _Pass_Usu & "', 0, " & _Vendedor
                End Get
            End Property

            Public ReadOnly Property SentenciaGraba() As String Implements Interfaces.IGenerico.SentenciaGraba
                Get
                    Return "EXECUTE SP_ZctSegUsu 2, '" & _Cod_Usu & "', '" & _Nom_Usu & "', '" & _Pass_Usu & "', 0," & _Vendedor
                End Get
            End Property

            Public Overridable Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                If Row.Read Then
                    _Cod_Usu = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Usu"))
                    _Nom_Usu = Row("Nom_Usu").ToString
                    _Pass_Usu = Row("Pass_Usu").ToString
                    _Vendedor = Row("Vendedor").ToString()
                End If
            End Sub

            Public Overridable Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos

            End Sub

            Public Overridable ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Cod_Usu, Nom_Usu, Pass_Usu,Vendedor FROM ZctSegUsu WHERE Cod_Usu = '{0}'"
                End Get
            End Property

            Public Sub validar() Implements Interfaces.IGenericoSingle.validar
                If _Nom_Usu.Trim = String.Empty Then Throw New ZctReglaNegocioEx("El nombre no puede estar vacio, verifique por favor.")
                If _Pass_Usu Is Nothing OrElse _Pass_Usu2 Is Nothing OrElse _Pass_Usu.Trim = String.Empty OrElse _Pass_Usu2.Trim = String.Empty Then Throw New ZctReglaNegocioEx("El password no puede estar vacio, verifique por favor.")
                If _Pass_Usu.Trim <> _Pass_Usu2.Trim Then Throw New ZctReglaNegocioEx("Los passwords no coniciden, verifique por favor.")
            End Sub
        End Class


        Public Class ZctSegUsuOperativo
            Inherits ZctSegUsu

        End Class



        Public Class ZctSegAut
            Private _Id_SegAut As Integer
            ''' <summary>
            ''' Identificador de seguridad
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Id_SegAut As Integer
                Get
                    Return _Id_SegAut
                End Get
                Set(ByVal value As Integer)
                    _Id_SegAut = value
                End Set
            End Property

            Private _Cod_Aut As Integer
            ''' <summary>
            ''' Código de autorización
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Aut As Integer
                Get
                    Return _Cod_Aut
                End Get
                Set(ByVal value As Integer)
                    _Cod_Aut = value
                End Set
            End Property

            Private _Cod_Usu As Integer
            ''' <summary>
            ''' Código de usuario
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_Usu As Integer
                Get
                    Return _Cod_Usu
                End Get
                Set(ByVal value As Integer)
                    _Cod_Usu = value
                End Set
            End Property

            Private _Desc_SegAut As String
            Public Property Desc_SegAut() As String
                Get
                    Return _Desc_SegAut
                End Get
                Set(ByVal value As String)
                    _Desc_SegAut = value
                End Set
            End Property
            Private _Vendedor As Boolean
            Public Property Vendedor As Boolean
                Set(value As Boolean)
                    _Vendedor = value
                End Set
                Get
                    Return _Vendedor
                End Get
            End Property
        End Class

        Public Class ListaAutorizacionesByUser
            Inherits List(Of ZctSegAut)
            Implements Interfaces.IEnumGenerico

            Public Sub AgregaElemento(ByRef Row As System.Data.SqlClient.SqlDataReader) Implements Interfaces.IEnumGenerico.AgregaElemento
                While (Row.Read)
                    Dim elemento As New ZctSegAut
                    elemento.Id_SegAut = ZctFunciones.GetGeneric(Of Integer)(Row("Id_SegAut"))
                    elemento.Cod_Aut = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Aut"))
                    elemento.Cod_Usu = ZctFunciones.GetGeneric(Of Integer)(Row("Cod_Usu"))
                    elemento.Desc_SegAut = ZctFunciones.GetGeneric(Row("Desc_SegAut"))
                    elemento.Vendedor = ZctFunciones.GetGeneric(Row("Vendedor"))
                    Me.Add(elemento)
                End While
            End Sub

            Public Sub LimpiaElementos() Implements Interfaces.IEnumGenerico.LimpiaElementos
                Me.Clear()
            End Sub

            Public ReadOnly Property SentenciaCargaDatos() As String Implements Interfaces.IEnumGenerico.SentenciaCargaDatos
                Get
                    Return "SELECT Id_SegAut, Cod_Aut, Cod_Usu, Desc_SegAut,Vendedor FROM ZctSegAut WHERE (Cod_Usu = {0})"
                End Get
            End Property
        End Class


    End Namespace
End Namespace