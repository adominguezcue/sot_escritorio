﻿Namespace Interfaces
    Public Interface IGenerico
        ReadOnly Property SentenciaGraba() As String
        ReadOnly Property SentenciaElimina() As String
        Property Actualiza() As Boolean
    End Interface

    Public Interface IGenericoSingle
        Inherits IGenerico
        Sub validar()
    End Interface

    Public Interface IEnumGenerico
        ReadOnly Property SentenciaCargaDatos() As String
        Sub AgregaElemento(ByRef Row As SqlClient.SqlDataReader)
        Sub LimpiaElementos()
    End Interface


    Public Interface IEnumGenericoEnum
        Inherits IEnumGenerico
        Inherits IEnumerable(Of IGenerico)

    End Interface


    Public Class EnumIGenerico
        Implements IEnumerator(Of Interfaces.IGenerico)

        Private _Curr As Integer = -1
        Private _lista() As Interfaces.IGenerico
        Public Sub New(ByVal lista() As Interfaces.IGenerico)
            _lista = lista
        End Sub


        Public ReadOnly Property Current() As Interfaces.IGenerico Implements System.Collections.Generic.IEnumerator(Of Interfaces.IGenerico).Current
            Get
                If ((_Curr < 0) Or (_Curr = _lista.Length)) Then Throw New InvalidOperationException()
                Return _lista(_Curr)

            End Get
        End Property

        Public ReadOnly Property Current1() As Object Implements System.Collections.IEnumerator.Current
            Get
                If ((_Curr < 0) Or (_Curr = _lista.Length)) Then Throw New InvalidOperationException()
                Return _lista(_Curr)
            End Get
        End Property

        Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
            If (_Curr < _lista.Length) Then _Curr = _Curr + 1
            Return Not (_Curr = _lista.Length)

        End Function

        Public Sub Reset() Implements System.Collections.IEnumerator.Reset
            _Curr = -1
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free other state (managed objects).
                End If

                ' TODO: free your own state (unmanaged objects).
                ' TODO: set large fields to null.
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class

End Namespace
