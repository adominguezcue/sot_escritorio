﻿Imports ZctSQL

Namespace RDN
    Public Class ZctRDNInvMotos

        Private _InvMotosHist As New SortedDictionary(Of String, Entidades.zctInvMotosHist)
        Private _DAO As New DAO.ZctDAOInvMotos
        Private InvFolios As New zctFolios("IM")

        Public ReadOnly Property Folio() As String
            Get
                Return InvFolios.Consecutivo
            End Get
        End Property

        Private _Status As String = "A"
        Public Property Status() As String
            Get
                Return _Status
            End Get
            Set(ByVal value As String)
                _Status = value
            End Set
        End Property

        Private _datos As New SortedList(Of String, Entidades.zctInvMotosHist)
        Public Property Datos() As SortedList(Of String, Entidades.zctInvMotosHist)
            Get
                Return _datos
            End Get
            Set(ByVal value As SortedList(Of String, Entidades.zctInvMotosHist))
                _datos = value
            End Set
        End Property



        Private _Cod_Cte As Integer = 0
        ''' <summary>
        ''' Código de cliente
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Cod_Cte() As Integer
            Get
                Return _Cod_Cte
            End Get
            Set(ByVal value As Integer)
                _Cod_Cte = value
            End Set
        End Property


        Public Sub CargaDatos(ByVal Cod_Folio As String, ByVal Cod_Cte As String)
            If Cod_Cte Is Nothing OrElse Cod_Cte = "" Then Throw New ZctReglaNegocioEx("Proporcione el código de cliente.")
            _Cod_Cte = Cod_Cte
            _datos.Clear()
            _Status = "A"
            _datos = _DAO.GetMotos(Cod_Folio, Cod_Cte)

        End Sub

        Public Sub CargaDatos(ByVal Cod_Folio As String)
            _Status = "M"
            _datos.Clear()
            _datos = _DAO.GetMotos(Cod_Folio, Cod_Cte)
        End Sub


        Public Sub Guardar(ByVal Cod_Folio As String, ByVal Cod_Cte As String)
            _DAO.SetInvMotos(Cod_Folio, Cod_Cte, _datos)
        End Sub

        Public Sub CargaExcel(ByVal Path As String, ByVal Cod_Fol As String)
            If IO.File.Exists(Path) Then
                Dim exDao As New ZctObjDatos(Of ZctDatosExcel)

                For Each vin As ZctDatosExcel In exDao.GetDatosExcel(Path)
                    'Si se encuentra el vin del archivo de excel, no hace nada
                    If _datos.ContainsKey(vin.Vin) Then
                        _datos.Item(vin.Vin).Estatus = Entidades.EnumEstatusMotHist.Encontrado
                        _datos.Item(vin.Vin).Accion = Entidades.EnumAccionesMotHist.No_Accion
                    Else
                        'Buscar la moto
                        Dim cteMoto As Integer = _DAO.GetCteMoto(vin.Vin)
                        If cteMoto > 0 Then
                            Dim Nueva_moto As New Entidades.zctInvMotosHist(Cod_Fol, _Cod_Cte, _DAO.GetCodMoto(vin.Vin), vin.Vin)
                            Nueva_moto.Estatus = Entidades.EnumEstatusMotHist.Encontrado_Otro_Cliente
                            Nueva_moto.CteCambio = cteMoto
                            Nueva_moto.Accion = Entidades.EnumAccionesMotHist.Cambiar_Cliente
                            _datos.Add(vin.Vin, Nueva_moto)
                        Else
                            Dim Nueva_moto As New Entidades.zctInvMotosHist(Cod_Fol, _Cod_Cte, 0, vin.Vin)
                            Nueva_moto.Estatus = Entidades.EnumEstatusMotHist.No_Encontrado
                            Nueva_moto.Accion = Entidades.EnumAccionesMotHist.Agregar
                            _datos.Add(vin.Vin, Nueva_moto)
                        End If


                    End If
                Next
            Else
                Throw New ZctReglaNegocioEx("El archivo especificado no existe.")
            End If
        End Sub

        Private _DiccEstatus As New EnumDiccEstatus
        Public ReadOnly Property DiccEstatus() As EnumDiccEstatus
            Get
                Return _DiccEstatus
            End Get
        End Property

        Private _DiccAcciones As New EnumDiccAcciones
        Public ReadOnly Property DiccAcciones() As EnumDiccAcciones
            Get
                Return _DiccAcciones
            End Get
        End Property
#Region "Clases"
        Public Class EnumDiccEstatus
            Inherits SortedDictionary(Of Entidades.EnumEstatusMotHist, String)
            Public Sub New()
                Me.Add(Entidades.EnumEstatusMotHist.No_Encontrado, "No Encontrado")
                Me.Add(Entidades.EnumEstatusMotHist.Encontrado, "Encontrado")
                Me.Add(Entidades.EnumEstatusMotHist.Encontrado_Otro_Cliente, "Encontrado con otro cliente")
                Me.Add(Entidades.EnumEstatusMotHist.No_Encontrado_Archivo, "No Encontrado Archivo")
            End Sub
        End Class

        Public Class EnumDiccAcciones
            Inherits SortedDictionary(Of Entidades.EnumAccionesMotHist, String)
            Public Sub New()
                Me.Add(Entidades.EnumAccionesMotHist.No_Accion, "No Acción")
                Me.Add(Entidades.EnumAccionesMotHist.Agregar, "Agregar")
                Me.Add(Entidades.EnumAccionesMotHist.Eliminar, "Eliminar")
                Me.Add(Entidades.EnumAccionesMotHist.Cambiar_Cliente, "Cambiar de cliente")
            End Sub
        End Class

        Private Class ZctDatosExcel
            Public _Vin As String

            <ZctDBAtributoOrigen("A")> _
            Public Property Vin() As String
                Get
                    Return _Vin
                End Get
                Set(ByVal value As String)
                    _Vin = value
                End Set
            End Property

        End Class
#End Region
    End Class
End Namespace