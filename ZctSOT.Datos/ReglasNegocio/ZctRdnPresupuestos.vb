﻿Imports ZctSQL

Namespace RDN

    Public Class ZctRdnPresupuestos

        Public Lista As New List(Of ZctPresupuesto_Elemento)

        Private _Path_Exl As String
        ''' <summary>
        ''' Ruta de excel
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Path_Exl() As String
            Get
                Return _Path_Exl
            End Get
            Set(ByVal value As String)
                _Path_Exl = value
            End Set
        End Property

        Private _Anno As String
        Private _CodAlmacen As Integer
        ''' <summary>
        ''' Año del presupuesto
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Anno() As String
            Get
                Return _Anno
            End Get
            Set(ByVal value As String)
                _Anno = value
            End Set
        End Property
        ''' <summary>
        ''' Almacen del presupuesto
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property CodAlmacen() As String
            Get
                Return _CodAlmacen
            End Get
            Set(ByVal value As String)
                _CodAlmacen = value
            End Set
        End Property
        Private _dao As New DAO.ZctDAOCatalogos(Of ZctPresupuesto_Elemento)

        Public Sub Grabar()
            _dao.Grabar(Lista)
        End Sub

        Public Sub GetData()
            _dao.Elemento = New ZctPresupuesto_Elemento
            Lista = _dao.GetDatos(New Object() {_Anno, _CodAlmacen})
        End Sub

        Public Sub DelData(ByVal Elemento As ZctPresupuesto_Elemento)
            If Elemento Is Nothing Then Throw New ZctReglaNegocioEx("Debe de especificar un elemento.")
            _dao.Eliminar(Elemento)
        End Sub

        Private _cod_cte As Integer
        Public Function GetElemento(ByVal Cod_Cte As Integer) As ZctPresupuesto_Elemento
            _cod_cte = Cod_Cte
            Return Lista.Find(New Predicate(Of ZctPresupuesto_Elemento)(AddressOf _GetElemento))
        End Function

        Public Function _GetElemento(ByVal elemento As ZctPresupuesto_Elemento) As Boolean
            Return (elemento.Cod_Cte = _cod_cte)
        End Function


        Public Sub GetExcel()
            If IO.File.Exists(Path_Exl) Then
                Dim exDao As New ZctObjDatos(Of ZctPresupuesto_Excel)
                Dim cte As New Clases.Catalogos.ZctCatClienteVerifica


                For Each ctePres As ZctPresupuesto_Excel In exDao.GetDatosExcel(Path_Exl)
                    'validar la existencia del usuario



                    cte = New Clases.Catalogos.ZctCatClienteVerifica
                    cte.Cod_Cte = ctePres.Cod_Cte
                    Dim dao As New DAO.ZctDAOCatalogoSingle(Of Clases.Catalogos.ZctCatClienteVerifica)
                    dao.GetDatos(cte)

                    If cte.Nom_Cte IsNot Nothing Then
                        ctePres.Anno_Pres = _Anno
                        ctePres.NombreCte = cte.Nombre
                        ctePres.Modificado = False
                        ctePres.CodAlmacen = _CodAlmacen
                        'Dim ctep As ZctPresupuesto = ctePres
                        Lista.Add(ctePres)
                    End If
                Next
            Else
                Throw New ZctReglaNegocioEx("El archivo especificado no existe.")
            End If
        End Sub

        Public Function GetCliente(ByVal Cod_Cte As Integer) As Clases.Catalogos.ZctCatCliente
            Dim cte = New Clases.Catalogos.ZctCatClienteVerifica
            cte.Cod_Cte = Cod_Cte
            Dim dao As New DAO.ZctDAOCatalogoSingle(Of Clases.Catalogos.ZctCatClienteVerifica)
            dao.GetDatos(cte)
            Return cte
        End Function
    End Class
End Namespace