﻿Namespace RDN
    Namespace Compras
        Public Class ZctOCSRDN
            Private _dao As New DAO.Compras.ZctOCSDAO
            Private _lista As New List(Of Clases.Compras.ZctArtDet)

            Public Property Lista() As List(Of Clases.Compras.ZctArtDet)
                Get
                    Return _lista
                End Get
                Set(ByVal value As List(Of Clases.Compras.ZctArtDet))
                    _lista = value
                End Set
            End Property

            Private _FechaIni As Date
            ''' <summary>
            ''' Fecha inicial
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property FechaIni() As Date
                Get
                    Return _FechaIni
                End Get
                Set(ByVal value As Date)
                    _FechaIni = value
                End Set
            End Property

            Private _FechaFin As Date

            ''' <summary>
            ''' Fecha Fin
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property FechaFin() As Date
                Get
                    Return _FechaFin
                End Get
                Set(ByVal value As Date)
                    _FechaFin = value
                End Set
            End Property

            Private _Cod_alm As Integer
            ''' <summary>
            ''' Código de almacén
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Cod_alm() As Integer
                Get
                    Return _Cod_alm
                End Get
                Set(ByVal value As Integer)
                    _Cod_alm = value
                End Set
            End Property
            Private _Cod_Linea As Integer = 0
            Public Property Cod_Linea() As Integer
                Get
                    return _Cod_Linea
                End Get
                Set(ByVal value As Integer)
                    _Cod_Linea = value
                End Set
            End Property

            Private _Cod_Dpto As Integer = 0
            Public Property Cod_Dpto() As Integer
                Get
                    return _Cod_Dpto
                End Get
                Set(ByVal value As Integer)
                    _Cod_Dpto = value
                End Set
            End Property



            Private _cfg As New Clases.Compras.ZctConfOCS

            Public Sub GetLista()
                _cfg = _dao.GetConfig(_Cod_alm)
                _lista = _dao.GetLista(_FechaIni, Cod_alm, _cfg.OC, _cfg.LT, _cfg.SS, _cfg.DiasHabiles, _cfg.Meses, _Cod_Linea, _Cod_Dpto)
            End Sub

            Private _cod_art As String
            Public Function GetElemento(ByVal Cod_art As String) As Clases.Compras.ZctArtDet
                _cod_art = Cod_art
                Return _lista.Find(New Predicate(Of ZctSOT.Datos.Clases.Compras.ZctArtDet)(AddressOf _GetElemento))
            End Function

            Private Function _GetElemento(ByVal elemento As Clases.Compras.ZctArtDet) As Boolean
                Return (elemento.Cod_Art = _cod_art)
            End Function

            Public Function GetConfig(ByVal Cod_Alm As Integer) As Clases.Compras.ZctConfOCS
                Return _dao.GetConfig(Cod_Alm)

            End Function

            ''' <summary>
            ''' Configuración de la compra sugerida
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public ReadOnly Property Cfg() As Clases.Compras.ZctConfOCS
                Get
                    Return _cfg
                End Get
            End Property

            Public Sub SetConfig(ByVal Cfg As Clases.Compras.ZctConfOCS)
                _dao.SetConfig(Cfg)
            End Sub

            Public Sub LoadCfg()
                _cfg = _dao.GetConfig(_Cod_alm)
            End Sub
        End Class
    End Namespace
End Namespace
