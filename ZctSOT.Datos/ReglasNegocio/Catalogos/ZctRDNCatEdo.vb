﻿Namespace RDN
    Namespace Catalogos

        Public Class ZctRDNCatEdo
            Implements IZctRDN(Of RDN.Catalogos.ZctCatEdoGen)


            Private _DAO As New DAO.ZctDAOCatalogos(Of ZctCatEdoGen)

            Private _Lista As New List(Of ZctCatEdoGen)
            Public Property Lista() As List(Of ZctCatEdoGen)
                Get
                    Return _Lista
                End Get
                Set(ByVal value As List(Of ZctCatEdoGen))
                    _Lista = value
                End Set
            End Property


            Public Sub New()
                _DAO.Elemento = New ZctCatEdoGen


            End Sub


            Public Sub Eliminar(ByVal Elemento As ZctCatEdoGen) Implements IZctRDN(Of ZctCatEdoGen).Eliminar

            End Sub

            Public Function GetDatos() As System.Collections.Generic.List(Of ZctCatEdoGen) Implements IZctRDN(Of ZctCatEdoGen).GetDatos
                _Lista = _DAO.GetDatos()
                Return _Lista
            End Function

            Public Function GetElemento(ByVal Codigo As Object) As ZctCatEdoGen Implements IZctRDN(Of ZctCatEdoGen).GetElemento

            End Function

            Public Function GetPropiedades() As System.Collections.Generic.List(Of ZctPropiedades) Implements IZctRDN(Of ZctCatEdoGen).GetPropiedades

            End Function

            Public Sub Grabrar() Implements IZctRDN(Of ZctCatEdoGen).Grabrar

            End Sub
        End Class

        Public Class ZctCatEdoGen
            Inherits Clases.Catalogos.ZctCatEdo
            Implements IZctElemento


            Public ReadOnly Property DelSQL() As String Implements IZctElemento.DelSQL
                Get

                End Get

            End Property

            Private _Modificado As Boolean = False
            Public Property Modificado() As Boolean Implements IZctElemento.Modificado
                Get
                    Return _Modificado
                End Get
                Set(ByVal value As Boolean)
                    _Modificado = value
                End Set
            End Property

            Public Sub SetDatos(ByVal row As System.Data.DataRow) Implements IZctElemento.SetDatos
                Me.Cod_Edo = ZctFunciones.GetGeneric(Of Integer)(row("Cod_Edo"))
                Me.Desc_Edo = row("Desc_Edo").ToString
            End Sub

            Public ReadOnly Property GetSQL() As String Implements IZctElemento.GetSQL
                Get
                    Return "SELECT * FROM ZctCatEdo"
                End Get
            End Property

            Public ReadOnly Property SetSQL() As String Implements IZctElemento.SetSQL
                Get

                End Get
            End Property

            Public ReadOnly Property Clone() As IZctElemento Implements IZctElemento.Clone
                Get
                    Dim elemento As New ZctCatEdoGen
                    elemento.Cod_Edo = Me.Cod_Edo
                    elemento.Desc_Edo = Me.Desc_Edo
                    Return elemento
                End Get

            End Property
        End Class
    End Namespace
End Namespace