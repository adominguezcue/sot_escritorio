﻿Namespace RDN
    Public Class ZctRDNCatArt

        Private _Articulo As New Clases.Catalogos.ZctCatArt
        Private _DAO As New DAO.Catalogos.ZctDAOCatArt

        Public Property Articulo() As Clases.Catalogos.ZctCatArt
            Get
                Return _Articulo
            End Get
            Set(ByVal value As Clases.Catalogos.ZctCatArt)
                _Articulo = value
            End Set
        End Property

        Public Sub GetArticulo(ByVal Cod_Art As String)
            If String.IsNullOrEmpty(Cod_Art) Then Exit Sub
            _Articulo = _DAO.GetArticulo(Cod_Art)

        End Sub

        Public Sub SetArticulo()
            If _Articulo.Cod_Art = "" Then Throw New ZctReglaNegocioEx("Debe de proporcionar el código de artículo.")
            If _Articulo.Cod_Mar = 0 Then Throw New ZctReglaNegocioEx("Debe de proporcionar la marca.")
            If _Articulo.Cod_Dpto = 0 Then Throw New ZctReglaNegocioEx("Debe de proporcionar el departamento.")
            If _Articulo.Cod_Linea = 0 Then Throw New ZctReglaNegocioEx("Debe de proporcionar la linea.")

            _DAO.SetArticulo(_Articulo)
        End Sub

    End Class


End Namespace
