﻿Public Interface IZctRDN(Of IZctElemento)
    Sub Grabrar()
    Sub Eliminar(ByVal Elemento As IZctElemento)
    Function GetDatos() As List(Of IZctElemento)
    Function GetPropiedades() As List(Of ZctPropiedades)
    Function GetElemento(ByVal Codigo As Object) As IZctElemento
End Interface

Public Interface IZctElemento
    ReadOnly Property GetSQL() As String
    ReadOnly Property SetSQL() As String
    ReadOnly Property DelSQL() As String
    ReadOnly Property Clone() As IZctElemento
    Property Modificado() As Boolean
    Sub SetDatos(ByVal row As DataRow)

End Interface

Public Interface IZctElementoSingle
    ReadOnly Property GetElemento() As String
    ReadOnly Property SetElemento() As String
    ReadOnly Property DelElemento() As String
    Sub SetDatos(ByVal rs As SqlClient.SqlDataReader)
End Interface

Public Interface IZctElementoVerifica
    Inherits IZctElementoSingle
    ReadOnly Property GetSQLVerifica() As String
    Function HasElement(ByVal Indice As String) As Boolean
End Interface

