Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO


Public Class ZctDataBase
    Implements IDisposable

    Private _ConnStr As New SqlConnection
    Private _ComSQL As New SqlCommand
    Private _DTAdapter As New SqlDataAdapter
    Private _DTTable As New DataTable
    Private _SQL As String
    Private _SqlComm As New SqlCommand
    Private _sqlTran As SqlTransaction = Nothing
    Private Password As String
    ''' <summary>
    ''' Crea una instancia del acceso a la base de datos.
    ''' </summary>
    Public Sub New()
        Password = "L0k0m0t0r4"
        Configurar()
    End Sub

    ''' <summary>
    ''' Configura el acceso a la base de datos para su utilización.
    ''' </summary>
    ''' <exception cref="BaseDatosException">Si existe un error al cargar la configuración.</exception>
    Private Sub Configurar()
        Try
            ''Dim proveedor As String = ConfigurationManager.AppSettings.Get("PROVEEDOR_ADONET")
            _ConnStr.ConnectionString = DAO.Conexion.CadenaConexion ' String.Format(ConfigurationManager.AppSettings.Get("CADENA_Conexion"), Password.ToString)
        Catch ex As ConfigurationErrorsException
            Throw New BaseDatosException("Error al cargar la configuración del acceso a datos.", ex)
        End Try
    End Sub


    'Public Sub Open()
    '    Me._ConnStr.Open()
    'End Sub

    'Public Sub Close()
    '    Me._ConnStr.Close()
    'End Sub

    Public Sub Inicia_Transaccion()
        'Inicia la transacción si no se ha iniciado aun
        'If Not Me._sqlTran Is Nothing Then
        _sqlTran = Me._ConnStr.BeginTransaction
        'End If
    End Sub

    Public Sub Termina_Transaccion()
        'Inicia la transacción si no se ha iniciado aun
        If Not Me._sqlTran Is Nothing Then
            Me._sqlTran.Commit()
        End If
    End Sub


    Public Sub Cancela_Transaccion()
        'Inicia la transacción si no se ha iniciado aun
        If Not Me._sqlTran Is Nothing Then
            Me._sqlTran.Rollback()
        End If
    End Sub

    Public Property SQL() As String
        Get
            Return _SQL
        End Get
        Set(ByVal value As String)
            _SQL = value
        End Set
    End Property

    Public Function GetDataTable() As DataTable
        Try

            If _SQL = "" Then Throw New Exception("Debe el valor de la sentencia SQL esta vacío, verifique por favor.")
            Dim taComun As New SqlDataAdapter(_SQL, _ConnStr)
            Dim DtComun As New DataTable

            taComun.Fill(DtComun)
            GetDataTable = DtComun
            DtComun.Dispose()
            taComun.Dispose()
        Catch ex As ConfigurationErrorsException
            Throw New BaseDatosException("Error al obtener el DataTable.", ex)
        End Try
    End Function

#Region "Procedimientos"
    Public Sub InciaDataAdapter()
        _DTAdapter.SelectCommand = _SqlComm
    End Sub
    Public Sub IniciaProcedimiento(ByVal Nombre_Procedimiento As String)
        Try
            'Limpia el comando

            _SqlComm.CommandText = Nombre_Procedimiento
            _SqlComm.CommandType = CommandType.StoredProcedure
            _SqlComm.Connection = _ConnStr
            _SqlComm.Parameters.Clear()

            If Not Me._sqlTran Is Nothing Then
                Me._SqlComm.Transaction = Me._sqlTran
            End If

        Catch ex As ConfigurationErrorsException
            Throw New BaseDatosException("Error al iniciar el procedimiento.", ex)
        End Try
    End Sub

    Public Sub AddParameterSP(ByVal Parametro As String, ByVal Valor As Object, ByVal Tipo As SqlDbType)
        Try
            'Valida que el procedimiento ya se haya asignado
            If _SqlComm.CommandText = "" Then Throw New Exception("Error al obtener el DataTable.")

            'Crea el nuevo parametro
            Dim ParamFill As New SqlClient.SqlParameter(Parametro, Tipo)
            Debug.Print("Parametro = {0}, Tipo = {1}", Parametro, Tipo)

            'Parametro de Dirección
            ParamFill.Direction = ParameterDirection.Input
            'Agrega el parametro al comando
            _SqlComm.Parameters.Add(ParamFill)

            'Agrega el valor
            _SqlComm.Parameters(Parametro).Value = Valor

        Catch ex As ConfigurationErrorsException
            Throw New BaseDatosException("Error al obtener el DataTable.", ex)
        End Try

    End Sub

    Public Sub AddParameterSPOuput(ByVal Parametro As String, ByVal Valor As Object, ByVal Tipo As SqlDbType)
        Try
            'Valida que el procedimiento ya se haya asignado
            If _SqlComm.CommandText = "" Then Throw New Exception("Error al obtener el DataTable.")

            'Crea el nuevo parametro
            Dim ParamFill As New SqlClient.SqlParameter(Parametro, Tipo)

            'Parametro de Dirección
            ParamFill.Direction = ParameterDirection.Output
            'Agrega el parametro al comando
            _SqlComm.Parameters.Add(ParamFill)

        Catch ex As ConfigurationErrorsException
            Throw New BaseDatosException("Error al obtener el DataTable.", ex)
        End Try

    End Sub

    Public Function GetScalarSP() As Object

        Try


            _ConnStr.Open()
            Return _SqlComm.ExecuteScalar()
            _ConnStr.Close()

        Catch ex As Exception
            'Throw New BaseDatosException("Error al obtener el Scalar.", ex)
            MsgBox(ex.Message)

            Return Nothing
        End Try
    End Function

    Public Sub OpenConTran()
        _ConnStr.Open()
    End Sub

    Public Sub CloseConTran()
        _ConnStr.Close()
    End Sub
    Public Function GetScalarSPTran() As Object

        Try
            Return _SqlComm.ExecuteScalar()
        Catch ex As Exception
            Debug.Print(_SqlComm.CommandText)
            Debug.Print(ex.Message)

            Throw New BaseDatosException("Error al obtener el Scalar.", ex)
            'MsgBox(ex.Message)

            'Return Nothing
        End Try
    End Function

    Public Function GetValueParameters(ByVal Parameter As String) As Object
        If _SqlComm.CommandText = "" Then Return Nothing : Exit Function
        Return _SqlComm.Parameters(Parameter).Value

    End Function
    Public Function GetReaderSP() As DataSet
        Try
            Dim DsDatos As New DataSet
            _ConnStr.Open()
            _DTAdapter.SelectCommand = _SqlComm

            _DTAdapter.Fill(DsDatos, "DATOS")
            _ConnStr.Close()
            Return DsDatos
            'Return _SqlComm.ExecuteReader()


        Catch ex As ConfigurationErrorsException
            Throw New BaseDatosException("Error al obtener el Scalar.", ex)
        End Try
    End Function

    Public Function GetReaderSPTrans() As DataSet
        Try
            Dim DsDatos As New DataSet
            '_ConnStr.Open()
            _DTAdapter.SelectCommand = _SqlComm

            _DTAdapter.Fill(DsDatos, "DATOS")
            '_ConnStr.Close()
            Return DsDatos
            'Return _SqlComm.ExecuteReader()


        Catch ex As ConfigurationErrorsException
            Throw New BaseDatosException("Error al obtener el Scalar.", ex)
        End Try
    End Function

#End Region


#Region "Seguridad"
    Public Function VerificaArch() As Boolean
        Try

            VerificaArch = False

            If Dir("c:\Windows\system32\zc001.dll", FileAttribute.Archive) = "" Then Exit Function
            If Dir("c:\Windows\system32\zc001.dll", FileAttribute.Hidden) = "" Then Exit Function
            'verifico que exista el archivo

            Dim fs As New FileStream("c:\Windows\system32\zc001.dll", FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            Dim sr As New StreamReader(fs)

            'Leo toda la informacion del archivo
            Dim sContent As String = sr.ReadToEnd()


            'If sContent = getmac() Then
            '    VerificaArch = True
            'ElseIf sContent = "" Then
            '    VerificaArch = True
            'End If

            'cierro los objetos
            fs.Close()
            sr.Close()

        Catch ex As Exception
            VerificaArch = False
            Exit Function

        End Try

    End Function

#End Region

    'Public Function getmac() As String
    '    Try


    '        Dim iMac As System.Net.NetworkInformation.PhysicalAddress
    '        Dim iBytes() As Byte = iMac.GetAddressBytes
    '        Dim iB As Integer
    '        Dim sMac As String
    '        For iB = 0 To iBytes.Length
    '            sMac = iBytes(iB).ToString
    '        Next

    '        Return sMac
    '    Catch ex As Exception
    '        Return "JAHZEEL"
    '    End Try


    'End Function

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If
            _ConnStr.Close()
            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
