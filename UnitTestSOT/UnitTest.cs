﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using Modelo;

namespace UnitTestSOT
{
    [TestClass]
    public class TestCXC
    {
        [TestMethod]
        public void TestCXCInstancia()
        {
            Modelo.CXC cxc = new Modelo.CXC();
            Assert.IsInstanceOfType(cxc, typeof(Modelo.cxc));

        }
    }
}
