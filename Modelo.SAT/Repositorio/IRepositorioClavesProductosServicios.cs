﻿using Dominio.Nucleo.Repositorios;
using Modelo.SAT.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.SAT.Repositorio
{
    public interface IRepositorioClavesProductosServicios : IRepositorio<ClaveProductoServicio>
    {
    }
}
