﻿using Modelo.Sistemas.Constantes;
using Modelo.Sistemas.Entidades;
using Modelo.Sistemas.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;
using Transversal.Excepciones;
using Transversal.Excepciones.Seguridad;
using Transversal.Extensiones;

namespace ConfiguradorSOT
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal static ConfiguracionSistema ConfiguracionActual { get; private set; }

        public App()
        {
            Startup += new StartupEventHandler(App_Startup); // Can be called from XAML

            DispatcherUnhandledException += App_DispatcherUnhandledException; //Example 2

            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException; //Example 4

            System.Windows.Forms.Application.ThreadException += WinFormApplication_ThreadException;

            Timeline.DesiredFrameRateProperty.OverrideMetadata(
                typeof(Timeline),
                new FrameworkPropertyMetadata { DefaultValue = 30 }
);
        }

        void App_Startup(object sender, StartupEventArgs e)
        {
            //Here if called from XAML, otherwise, this code can be in App()
            //AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException; // Example 1
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; // Example 3
            //AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        internal static void CargarConfiguracion()
        {
            var parametros = new SOTControladores.Controladores.ControladorConfiguracionSistemas().ObtenerParametros(IdentificadoresSistemas.CONFIGURADOR_SOT);//typeof(App).ObtenerGuidEnsamblado() ?? new Guid());

            var parametrosConfig = new ParametrosConfigurador();

            foreach (var propiedad in typeof(ParametrosConfigurador).GetProperties())
            {
                var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                if (parametro != null)
                {
                    propiedad.SetValue(parametrosConfig, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                }
            }

            if (parametrosConfig.ConfiguracionFinalizada)
                throw new SOTException("No se puede realizar la configuración en esta base de datos");
        }

        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ProcesarError(e.Exception);
            e.Handled = true;
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //ManejadorDialogos.Error("3. CurrentDomain_UnhandledException");
            var exception = e.ExceptionObject as Exception;
            ProcesarError(exception, true);
            //if (e.IsTerminating)
            //{
            //    //Now is a good time to write that critical error file!
            //    ManejadorDialogos.Error("Goodbye world!");
            //}
        }

        // Example 4
        void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            foreach (var ex in e.Exception.InnerExceptions)
                ProcesarError(ex, true);

            e.SetObserved();
        }

        private static void ProcesarError(Exception ex, bool procesoParalelo = false, bool soloLog = false)
        {
            //#if DEBUG
            try
            {
                if (ex.GetType() == typeof(TargetInvocationException) || ex.GetType() == typeof(System.Windows.Markup.XamlParseException))
                {
                    ProcesarError(ex.InnerException, procesoParalelo, soloLog);
                    return;
                }
                if (ex.GetType() == typeof(DbEntityValidationException))
                {
                    var excepcion = (DbEntityValidationException)ex;

                    foreach (var er in excepcion.EntityValidationErrors)
                        Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                else if (ex.GetType() == typeof(DbUpdateException))
                {
                    //var excepcion = (DbUpdateException)ex;
                    //UpdateException inner = null;
                    Exception excepcion = null;

                    if (ex.InnerException != null && ex.InnerException.GetType() == typeof(UpdateException))
                    {
                        excepcion = ex.InnerException;

                        if (excepcion.InnerException != null)
                            excepcion = excepcion.InnerException;
                    }
                    else
                        excepcion = ex;

                    Transversal.Log.Logger.Fatal("[DbUpdateException] " + excepcion.Message);
                    //foreach (var er in excepcion.EntityValidationErrors)
                    //    Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                else if (ex.GetType() != typeof(SOTException))
                {
                    var exCurrent = ex;

                    for (int i = 0; i < 50 && exCurrent != null; i++, exCurrent = exCurrent.InnerException)
                    {
                        Transversal.Log.Logger.Fatal(exCurrent.Message);
                    }
                }
            }
            catch { }

            if (soloLog)
                return;

            //#endif
            /*ISnackContainer contenedora;
            try
            {
                contenedora = procesoParalelo ? null : Application.Current.Windows.OfType<ISnackContainer>().FirstOrDefault(x => x.IsActive);
            }
            catch
            {
                contenedora = null;
            }*/
            //var contenedora = MainWindow as ISnackContainer;

            /*if (contenedora != null)
            {
                contenedora.MostrarError(ex);
            }
            else */if (ex.GetType() == typeof(SOTPermisosException))
            {
                var excepcion = (SOTPermisosException)ex;

                StringBuilder builder = new StringBuilder();
                builder.AppendLine(excepcion.Message);
                builder.AppendLine();

                foreach (var error in excepcion.Permisos)
                    builder.AppendLine(error);

                SOTWpf.Utilidades.Dialogos.Error(builder.ToString());
            }
            else if (ex.GetType() == typeof(SOTAcumulativoException))
            {
                var excepcion = (SOTAcumulativoException)ex;

                StringBuilder builder = new StringBuilder();
                builder.AppendLine(excepcion.Message);
                builder.AppendLine();

                foreach (var error in excepcion.Iterar())
                    builder.AppendLine(error);

                SOTWpf.Utilidades.Dialogos.Error(builder.ToString());
            }
            else if (ex.GetType() == typeof(DbUpdateException))
            {
                //var excepcion = (DbUpdateException)ex;
                //UpdateException inner = null;
                Exception excepcion = null;

                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(UpdateException))
                {
                    excepcion = ex.InnerException;

                    if (excepcion.InnerException != null)
                        excepcion = excepcion.InnerException;
                }
                else
                    excepcion = ex;

                SOTWpf.Utilidades.Dialogos.Error(excepcion.Message);
                //foreach (var er in excepcion.EntityValidationErrors)
                //    Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
            }
            else
            {
                SOTWpf.Utilidades.Dialogos.Error(ex.Message);
            }
        }

        void WinFormApplication_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
