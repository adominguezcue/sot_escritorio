﻿using ConfiguradorSOT.Pasos;
using Datos.Nucleo.Contextos;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Modelo.Sistemas.Constantes;
using Modelo.Sistemas.Entidades;
using Modelo.Sistemas.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Extensiones;
using Transversal.Transacciones;

namespace ConfiguradorSOT
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private CargaSemillaUC cargadorSemilla;
        ////private IniciarSesionUC iniciadorSesion;
        //private ConfigurarHabitacionesUC configuradorHabitacionesUC;
        //private ConfigurarMesasUC configuradorMesasUC;
        //private ConfigurarDatosFiscalesUC configuradorDatosFiscalesUC;
        //private ConfigurarParametrosOperacionUC configuradorParametrosOperacion;
        private static readonly string CADENA_RESETEO = @"USE {0}
                                                               GO
                                                               UPDATE [dbo].[ZctArtXAlm] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctPresupuestosXmes] SET uid = NEWID(), update_web = NULL;UPDATE [SotSchema].[BloqueadoresFolios] SET UUID = NEWID();/*, update_web = NULL;*/UPDATE [dbo].[ZctEncOT] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctDetOT] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctDetOC] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctCatAlm] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctMixItems] SET uuid = NEWID();/*, update_web = NULL;*/UPDATE [dbo].[ZctInvCorteFis] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctEncCuentasPagar] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctRequisitionDetail] SET uid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctReporteGeneral] SET uid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctServiciosOrdenCompra] SET Uuid = NEWID();/*, update_web = NULL;*/UPDATE [dbo].[ZctRequisition] SET uid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctCatArt] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctCatRegion] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctCatMot] SET uuid = NEWID(), update_web = NULL;UPDATE [dbo].[ZctCatCte] SET uuid = NEWID(), update_web = NULL;";

        private Dictionary<ListBoxItem, UserControl> contenidos = new Dictionary<ListBoxItem, UserControl>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            contenidos.Add(lbiConfigurarDB, new CargaSemillaUC());
            contenidos.Add(lbiEditarHabitaciones, new ConfigurarHabitacionesUC());
            contenidos.Add(lbiEditarMesas, new ConfigurarMesasUC());
            contenidos.Add(lbiEditarDatosFiscales, new ConfigurarDatosFiscalesUC());
            contenidos.Add(lbiConfigurarParametrosOperacion, new ConfigurarParametrosOperacionUC());
            contenidos.Add(lbiConfigurarParametrosSistema, new ConfigurarParametrosSistemaUC());
            contenidos.Add(lbiConfigurarPuntosLealtad, new ConfigurarParametrosPuntosLealtadUC());
            contenidos.Add(lbiConfigurarTurnos, new ConfigurarTurnosUC());
            contenidos.Add(lbiConfigurarRolActual, new ConfigurarRolActualUC());
            contenidos.Add(lbiEditarFajillas, new ConfigurarFajillasUC());




            contenedor.Content = contenidos[lbiConfigurarDB];
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void AvanzarCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var index = listBoxMenuPrincipal.SelectedIndex + 1;

            if (index < listBoxMenuPrincipal.Items.Count)
                CargarContenido(index);
            else
            {
                if (SOTWpf.Utilidades.Dialogos.Pregunta("Si continúa se finalizará la configuración y no podrá seguir utilizando la base de datos actual con el configurador ¿desea continuar?") == MessageBoxResult.Yes)
                {
                    CambiarUUIDS();

                    var parametros = new List<ParametroSistema>
                    {
                        new ParametroSistema
                        {
                            GuidSistema = IdentificadoresSistemas.CONFIGURADOR_SOT,//typeof(App).ObtenerGuidEnsamblado() ?? new Guid(),
                            Nombre = nameof(ParametrosConfigurador.ConfiguracionFinalizada),
                            Valor = Newtonsoft.Json.JsonConvert.SerializeObject(true),
                        }
                    };

                    var controladorSis = new SOTControladores.Controladores.ControladorConfiguracionSistemas();
                    var controladorHab = new SOTControladores.Controladores.ControladorHabitaciones();
                    var controladorRest = new SOTControladores.Controladores.ControladorRestaurantes();

                    using (var scope = new SotTransactionScope())
                    {
                        controladorSis.GuardarParametrosSistema(parametros);
                        controladorHab.FinalizarConfiguracionHabitaciones();
                        controladorRest.FinalizarConfiguracionMesas();

                        scope.Complete();
                    }
                    SOTWpf.Utilidades.Dialogos.Aviso("Configuración finalizada");
                    Close();
                }
            }
        }

        private void CambiarUUIDS()
        {
            var cadena = ConexionHelper.CadenaConexion;

         
                using (SqlConnection connection = new SqlConnection(cadena))
                {
                    try
                    {

                        Server server = new Server(new ServerConnection(connection));
                        server.ConnectionContext.ExecuteNonQuery(string.Format(CADENA_RESETEO, connection.Database));
                    }
                    finally
                    {
                        try
                        {
                            if (connection.State == ConnectionState.Open)
                                connection.Close();
                        }
                        catch
                        {
                        }

                    }
                }
        }

        private void RetrocederCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var index = listBoxMenuPrincipal.SelectedIndex - 1;

            if (index < listBoxMenuPrincipal.Items.Count && index >= 0)
                CargarContenido(index);
        }

        private void lbi_Click(object sender, MouseButtonEventArgs e)
        {
            CargarContenido(listBoxMenuPrincipal.Items.IndexOf(sender));
        }

        private void CargarContenido(int index)
        {
            //var contenido = contenedor.Content;
            var indexActual = listBoxMenuPrincipal.SelectedIndex;

            try
            {
                listBoxMenuPrincipal.SelectedIndex = index;
                (listBoxMenuPrincipal.SelectedItem as ListBoxItem).IsEnabled = true;
                contenedor.Content = contenidos[listBoxMenuPrincipal.SelectedItem as ListBoxItem];
            }
            catch
            {
                //contenedor.Content = contenido;
                listBoxMenuPrincipal.SelectedIndex = indexActual;
                contenedor.Content = contenidos[listBoxMenuPrincipal.SelectedItem as ListBoxItem];
                throw;
            }

            if (listBoxMenuPrincipal.SelectedIndex <= 2)
                foreach (ListBoxItem item in listBoxMenuPrincipal.Items)
                {
                    if (listBoxMenuPrincipal.Items.IndexOf(item) <= listBoxMenuPrincipal.SelectedIndex)
                        continue;

                    item.IsEnabled = false;
                }
        }

        /*private void lbiConfigurarDB_Click(object sender, MouseButtonEventArgs e)
        {
            CargarConfiguradorSemilla();
        }

        //private void lbiIniciarSesion_Click(object sender, MouseButtonEventArgs e)
        //{
        //    CargarInicioSesion();
        //}

        private void lbiEditarHabitaciones_Click(object sender, MouseButtonEventArgs e)
        {
            CargarConfiguradorHabitaciones();
        }

        private void lbiEditarMesas_Click(object sender, MouseButtonEventArgs e)
        {
            CargarConfiguradorMesas();
        }

        private void lbiEditarDatosFiscales_Click(object sender, MouseButtonEventArgs e)
        {
            CargarConfiguradorDatosFiscales();
        }

        private void lbiConfigurarParametrosOperacion_Click(object sender, MouseButtonEventArgs e)
        {
            CargarConfiguradorParametrosOperacion();
        }

        private void lbiConfigurarParametrosSistema_Click(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void FinalizarCargaSemillaCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //CargarInicioSesion();
            CargarConfiguradorHabitaciones();
        }

        private void FinalizarConfiguracionDatosFiscalesCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CargarConfiguradorParametrosOperacion();
        }

        private void FinalizarConfiguracionHabitacionesCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CargarConfiguradorMesas();
        }

        private void FinalizarConfiguracionMesasCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            CargarConfiguradorDatosFiscales();
        }

        //private void IniciarSesionCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        //{
        //    CargarConfiguradorHabitaciones();
        //}

        private void FinalizarConfiguracionParametrosOperacionCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void FinalizarConfiguracionParametrosSistemaCommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
        }*/

        /*private void CargarConfiguradorSemilla()
        {
            var contenido = contenedor.Content;
            var index = listBoxMenuPrincipal.SelectedIndex;

            try
            {
                contenedor.Content = cargadorSemilla;
                listBoxMenuPrincipal.SelectedItem = lbiConfigurarDB;
            }
            catch
            {
                contenedor.Content = contenido;
                listBoxMenuPrincipal.SelectedIndex = index;
                throw;
            }

            foreach (ListBoxItem item in listBoxMenuPrincipal.Items)
            {
                if (item == lbiConfigurarDB)
                    continue;

                item.IsEnabled = false;
            }
        }

        //private void CargarInicioSesion()
        //{
        //    var contenido = contenedor.Content;
        //    var index = listBoxMenuPrincipal.SelectedIndex;

        //    try
        //    {
        //        contenedor.Content = iniciadorSesion;
        //        lbiIniciarSesion.IsEnabled = true;
        //        listBoxMenuPrincipal.SelectedItem = lbiIniciarSesion;
        //    }
        //    catch
        //    {
        //        contenedor.Content = contenido;
        //        listBoxMenuPrincipal.SelectedIndex = index;
        //        throw;
        //    }
        //}

        private void CargarConfiguradorHabitaciones()
        {
            var contenido = contenedor.Content;
            var index = listBoxMenuPrincipal.SelectedIndex;

            try
            {
                contenedor.Content = configuradorHabitacionesUC;
                lbiEditarHabitaciones.IsEnabled = true;
                listBoxMenuPrincipal.SelectedItem = lbiEditarHabitaciones;
            }
            catch
            {
                contenedor.Content = contenido;
                listBoxMenuPrincipal.SelectedIndex = index;
                throw;
            }
        }

        private void CargarConfiguradorMesas()
        {
            var contenido = contenedor.Content;
            var index = listBoxMenuPrincipal.SelectedIndex;

            try
            {
                contenedor.Content = configuradorMesasUC;
                lbiEditarMesas.IsEnabled = true;
                listBoxMenuPrincipal.SelectedItem = lbiEditarMesas;
            }
            catch
            {
                contenedor.Content = contenido;
                listBoxMenuPrincipal.SelectedIndex = index;
                throw;
            }
        }

        private void CargarConfiguradorDatosFiscales()
        {
            var contenido = contenedor.Content;
            var index = listBoxMenuPrincipal.SelectedIndex;

            try
            {
                contenedor.Content = configuradorDatosFiscalesUC;
                lbiEditarDatosFiscales.IsEnabled = true;
                listBoxMenuPrincipal.SelectedItem = lbiEditarDatosFiscales;
            }
            catch
            {
                contenedor.Content = contenido;
                listBoxMenuPrincipal.SelectedIndex = index;
                throw;
            }
        }

        private void CargarConfiguradorParametrosOperacion()
        {
            var contenido = contenedor.Content;
            var index = listBoxMenuPrincipal.SelectedIndex;

            try
            {
                contenedor.Content = configuradorParametrosOperacion;
                lbiConfigurarParametrosOperacion.IsEnabled = true;
                listBoxMenuPrincipal.SelectedItem = lbiConfigurarParametrosOperacion;
            }
            catch
            {
                contenedor.Content = contenido;
                listBoxMenuPrincipal.SelectedIndex = index;
                throw;
            }
        }*/
    }
}
