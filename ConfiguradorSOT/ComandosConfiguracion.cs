﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ConfiguradorSOT
{
    public static class ComandosConfiguracion
    {
        public static readonly RoutedUICommand AvanzarCommand = new RoutedUICommand("Avanzar", "AvanzarCommand", typeof(ComandosConfiguracion));
        public static readonly RoutedUICommand RetrocederCommand = new RoutedUICommand("Retroceder", "RetrocederCommand", typeof(ComandosConfiguracion));

        //public static readonly RoutedUICommand FinalizarCargaSemillaCommand = new RoutedUICommand("Finalizar carga de semilla", "FinalizarCargaSemillaCommand", typeof(ComandosConfiguracion));
        ////public static readonly RoutedUICommand IniciarSesionCommand = new RoutedUICommand("Iniciar sesión", "IniciarSesionCommand", typeof(ComandosConfiguracion));
        //public static readonly RoutedUICommand FinalizarConfiguracionHabitacionesCommand = new RoutedUICommand("Finalizar configuración de habitaciones", "FinalizarConfiguracionHabitacionesCommand", typeof(ComandosConfiguracion));
        //public static readonly RoutedUICommand FinalizarConfiguracionMesasCommand = new RoutedUICommand("Finalizar configuración de mesas", "FinalizarConfiguracionMesasCommand", typeof(ComandosConfiguracion));
        //public static readonly RoutedUICommand FinalizarConfiguracionDatosFiscalesCommand = new RoutedUICommand("Finalizar configuración de datos fiscales", "FinalizarConfiguracionDatosFiscalesCommand", typeof(ComandosConfiguracion));
        //public static readonly RoutedUICommand FinalizarConfiguracionParametrosOperacionCommand = new RoutedUICommand("Finalizar configuración de parámetros de operación", "FinalizarConfiguracionParametrosOperacionCommand", typeof(ComandosConfiguracion));
        //public static readonly RoutedUICommand FinalizarConfiguracionParametrosSistemaCommand = new RoutedUICommand("Finalizar configuración de parámetros del sistema", "FinalizarConfiguracionParametrosSistemaCommand", typeof(ComandosConfiguracion));


        //Define more commands here, just like the one above
    }
}
