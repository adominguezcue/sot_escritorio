﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarRolActualUC.xaml
    /// </summary>
    public partial class ConfigurarRolActualUC : UserControl
    {
        public ConfigurarRolActualUC()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            editorRoles.CancelarEdicion();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            editorRoles.Guardar();
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            if (SOTWpf.Utilidades.Dialogos.Pregunta("¿Desea continuar? Cualquier cambio será descartado") != MessageBoxResult.Yes)
                return;

            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                CargarRol();
            }
        }

        private void CargarRol()
        {
            var controladorRoles = new ControladorRoles();

            editorRoles.DataContext = controladorRoles.ObtenerRolConPermisos(ControladorBase.UsuarioActual.IdRol);
        }

        private void EditorRoles_GuardadoExitoso(object sender, RoutedEventArgs e)
        {
            try
            {
                new ControladorSesiones().CerrarSesion();
            }
            catch
            {

            }

            ComandosConfiguracion.RetrocederCommand.Execute(null, null);
        }

        private void EditorRoles_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            CargarRol();
        }
    }
}
