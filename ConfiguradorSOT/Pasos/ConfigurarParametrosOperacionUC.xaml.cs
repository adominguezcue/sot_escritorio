﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarParametrosSistemaUC.xaml
    /// </summary>
    public partial class ConfigurarParametrosOperacionUC : UserControl
    {
        public ConfigurarParametrosOperacionUC()
        {
            InitializeComponent();
        }

        private void EditorClientes_GuardadoExitoso(object sender, RoutedEventArgs e)
        {
            editorParametrosOperacion.DataContext = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();
        }

        private void EditorClientes_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            editorParametrosOperacion.DataContext = new SOTControladores.Controladores.ControladorConfiguracionGlobal().ObtenerConfiguracionGlobal();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosOperacion.Cancelar();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosOperacion.Guardar();
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosOperacion.Guardar();
            //new SOTControladores.Controladores.ControladorConfiguracionGlobal().ValidarConfiguracionGlobalEsCorrecta();

            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }
    }
}
