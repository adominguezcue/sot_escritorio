﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarTurnosUC.xaml
    /// </summary>
    public partial class ConfigurarTurnosUC : UserControl
    {
        public ConfigurarTurnosUC()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            editorTurnos.Guardar();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            editorTurnos.CancelarEdicion();
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            editorTurnos.Guardar();

            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }
    }
}
