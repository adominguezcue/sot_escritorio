﻿using MaterialDesignThemes.Wpf;
using Modelo.Entidades;
using SOTWpf.OrdenesRestaurante;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarMesasUC.xaml
    /// </summary>
    public partial class ConfigurarMesasUC : UserControl
    {
        private EdicionMesasUC editorForm;
        private Guid identificadorDialogo = Guid.NewGuid();

        public ConfigurarMesasUC()
        {
            InitializeComponent();
            dhMesas.Identifier = identificadorDialogo;
        }

        private void dialogHostPrincipal_DialogClosing(object sender, MaterialDesignThemes.Wpf.DialogClosingEventArgs eventArgs)
        {

        }

        private void btnAgregarMesa_Click(object sender, RoutedEventArgs e)
        {
            MostrarEditor();
        }

        private void btnEditarMesa_Click(object sender, RoutedEventArgs e)
        {
            var mesa = panelMesa.MesaSeleccionada?.MesaInterna;

            if (mesa == null)
                throw new SOTException(SOTWpf.Textos.Errores.mesa_no_seleccionada_exception);

            MostrarEditor(mesa);
        }

        private void btnEliminarMesa_Click(object sender, RoutedEventArgs e)
        {
            var mesa = panelMesa.MesaSeleccionada?.MesaInterna;

            if (mesa == null)
                throw new SOTException(SOTWpf.Textos.Errores.mesa_no_seleccionada_exception);

            var controlador = new SOTControladores.Controladores.ControladorRestaurantes();

            if (SOTWpf.Utilidades.Dialogos.Pregunta(SOTWpf.Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                controlador.EliminarMesa(mesa.Id);

                SOTWpf.Utilidades.Dialogos.Aviso(SOTWpf.Textos.Mensajes.eliminacion_elemento_exitosa);

                panelMesa.ActualizarMesas();
            }
        }

        private async Task MostrarEditor(Mesa mesaEditar = null)
        {
            editorForm = new EdicionMesasUC
            {
                DataContext = mesaEditar ?? new Mesa() { Activa = true }
            };
            editorForm.EdicionAceptada += EditorForm_EdicionAceptada;
            editorForm.EdicionCancelada += EditorForm_EdicionCancelada;
            //editorForm.EdicionRealizada += BotoneraBase_AceptarClick;
            //editorForm.EdicionCancelada += BotoneraBase_CancelarClick;

            await DialogHost.Show(editorForm, identificadorDialogo);
        }

        private void EditorForm_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            EjecutarComando();
        }

        private void EditorForm_EdicionAceptada(object sender, RoutedEventArgs e)
        {
            panelMesa.ActualizarMesas();

            //var h = panelMesa.MesaSeleccionada;

            //if (h != null)
            //    h.RecargarMesa();

            EjecutarComando();
        }

        private void EjecutarComando()
        {
            if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(null))
                ((ICommand)DialogHost.CloseDialogCommand).Execute(null);

            if (editorForm != null)
            {
                editorForm.EdicionAceptada -= EditorForm_EdicionAceptada;
                editorForm.EdicionCancelada -= EditorForm_EdicionCancelada;
            }
        }

        private void RecargarMesas(object sender, RoutedEventArgs e)
        {
            txtMesaSeleccionada.Text = panelMesa.MesaSeleccionada?.MesaInterna?.Clave;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            scvMesas.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            scvMesas.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            scvMesas.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            scvMesas.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            scvMesas.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            scvMesas.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;

            chbMostrarPanelTamanoReal.IsChecked = false;
        }
    }
}
