﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarParametrosSistemaUC.xaml
    /// </summary>
    public partial class ConfigurarParametrosSistemaUC : UserControl
    {
        public ConfigurarParametrosSistemaUC()
        {
            InitializeComponent();
        }

        private void EditorClientes_GuardadoExitoso(object sender, RoutedEventArgs e)
        {
            editorParametrosSistema.CargarParametros();
        }

        private void EditorClientes_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            editorParametrosSistema.CargarParametros();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosSistema.Cancelar();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosSistema.Guardar();
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosSistema.Guardar();
            //editorParametrosSistema.ValidarParametros();
            //new SOTControladores.Controladores.ControladorConfiguracionSistemas().ValidarConfiguracionCorrecta();

            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }
    }
}
