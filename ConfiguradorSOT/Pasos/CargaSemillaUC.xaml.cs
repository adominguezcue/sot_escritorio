﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Datos.Nucleo.Contextos;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.Win32;
using SOTControladores.Controladores;
using SOTWpf.Properties;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para CargaSemillaUC.xaml
    /// </summary>
    public partial class CargaSemillaUC : UserControl
    {
        private static readonly string CADENA_RESTAURACION = @"RESTORE DATABASE {0} FROM DISK='{1}'
                                                               WITH MOVE 'ZctSOT' TO '{2}.mdf',
                                                               MOVE 'ZctSOT_Log' TO '{3}.ldf';
                                                               GO"; 
        

        private ConexionSOT Conexion
        {
            get
            {
                return DataContext as ConexionSOT;
            }
            set
            {
                DataContext = value;
            }
        }

        public CargaSemillaUC()
        {
            InitializeComponent();

            Conexion = ConexionSOT.Default;
        }

        private void cargarSemilla_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Respaldos de bases de datos|*.bak";

            if (dialog.ShowDialog() == true)
            {
                txtRuta.Text = dialog.FileName;
            }
        }
        
        /*private void CargarListaInstancias()
        {
            var instance = SqlDataSourceEnumerator.Instance;
            DataTable table = instance.GetDataSources();

            foreach (DataRow row in table.Rows)
            {
                string name = row["ServerName"].ToString();
                if (!string.IsNullOrWhiteSpace(row["InstanceName"]?.ToString()))
                    name += string.Format(@"{0}", row["InstanceName"]);
                cbInstancias.Items.Add(name);
            }
        }

        private void cargarSemilla_Loaded(object sender, RoutedEventArgs e)
        {
            CargarListaInstancias();
        }*/

        private void probarConexion_Click(object sender, RoutedEventArgs e)
        {
            ProbarConexion(true);

            SOTWpf.Utilidades.Dialogos.Aviso("Conexión exitosa con " + Conexion.OrigenSOT);
        }

        private void restaurarDb_Click(object sender, RoutedEventArgs e)
        {
            var comando = string.Format(CADENA_RESTAURACION /*+ System.Environment.NewLine + CADENA_RESETEO*/, Conexion.DbSOT, txtRuta.Text, System.IO.Path.Combine(txtCarpetaDestino.Text, Conexion.DbSOT), System.IO.Path.Combine(txtCarpetaDestino.Text, Conexion.DbSOT));

            ProbarConexion(true, comando);

            SOTWpf.Utilidades.Dialogos.Aviso("Respaldo restaurado con éxito");
        }

        private void ProbarConexion(bool usarMaster, string strComando = null)
        {
            SqlConnectionStringBuilder ConexionBuilder = new SqlConnectionStringBuilder();

            if (chbDbExiste.IsChecked == true || !usarMaster)
                ConexionBuilder.InitialCatalog = Conexion.DbSOT;
            else
                ConexionBuilder.InitialCatalog = "master";

            if (!string.IsNullOrEmpty(Conexion.OrigenSOT))
                ConexionBuilder.DataSource = Conexion.OrigenSOT;


            ConexionBuilder.IntegratedSecurity = Conexion.SeguridadIntegradaSOT;

            if (Conexion.SeguridadIntegradaSOT)
            {
                ConexionBuilder.UserID = ConexionBuilder.Password = "";
            }
            else
            {
                //if (!string.IsNullOrEmpty(Conexion.UsuarioSOT))
                ConexionBuilder.UserID = Conexion.UsuarioSOT ?? "";
                //if (!string.IsNullOrEmpty(Conexion.PassSOT))
                ConexionBuilder.Password = Conexion.PassSOT ?? "";
            }
            if (!string.IsNullOrWhiteSpace(strComando))
                using (SqlConnection connection = new SqlConnection(ConexionBuilder.ConnectionString))
                {
                    try
                    {
                       
                            Server server = new Server(new ServerConnection(connection));
                            server.ConnectionContext.ExecuteNonQuery(strComando);
                        

                        //connection.Open();

                        //using (var comando = connection.CreateCommand())
                        //{
                        //    comando.CommandText = strComando;
                        //    comando.ExecuteNonQuery();
                        //}

                        //connection.Close();
                    }
                    finally
                    {
                        try
                        {
                            if (connection.State == ConnectionState.Open)
                                connection.Close();
                        }
                        catch
                        {
                        }

                    }
                }
            else
                using (DbContext contexto = new DbContext(ConexionBuilder.ConnectionString))
                {

                    try
                    {
                        contexto.Database.Connection.Open();
                    }
                    finally
                    {
                        try
                        {
                            if (contexto.Database.Connection.State == ConnectionState.Open)
                                contexto.Database.Connection.Close();
                        }
                        catch
                        {
                        }

                    }


                }
        }

        private void buscarCarpeta_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtCarpetaDestino.Text = dialog.SelectedPath;
            }
        }

        private void guardarConexionClick(object sender, RoutedEventArgs e)
        {
            //var comando = string.Format(CADENA_RESETEO, Conexion.DbSOT);

            ProbarConexion(false/*, comando*/);
            ConexionHelper.CambiarConexion(true, Conexion.DbSOT, Conexion.OrigenSOT, Conexion.UsuarioSOT, Conexion.PassSOT, Conexion.SeguridadIntegradaSOT);

            Conexion.EsConfiguradoSOT = true;

            Conexion.Save();

            var controladorS = new ControladorSesiones();

            var credencial = new Modelo.Seguridad.Dtos.DtoCredencial
            {
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                ValorVerificarStr = "Pass123",
                IdentificadorUsuario = "sistemas"
            };

            controladorS.IniciarSesion(credencial);

            App.CargarConfiguracion();

            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }
    }
}
