﻿using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para IniciarSesionUC.xaml
    /// </summary>
    public partial class IniciarSesionUC : UserControl
    {
        public IniciarSesionUC()
        {
            InitializeComponent();
        }

        private void IniciarSesion_Click(object sender, RoutedEventArgs e)
        {
            var controladorS = new ControladorSesiones();

            var credencial = new Modelo.Seguridad.Dtos.DtoCredencial
            {
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                ValorVerificarStr = txtContrasena.Password,
                IdentificadorUsuario = txtUsuario.Text
            };

            controladorS.IniciarSesion(credencial);

            App.CargarConfiguracion();

            //ComandosConfiguracion.IniciarSesionCommand.Execute(null, null);
        }
    }
}
