﻿using Modelo.Sistemas.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Extensiones;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarFajillas.xaml
    /// </summary>
    public partial class ConfigurarFajillasUC : UserControl
    {
        public ConfigurarFajillasUC()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            configuradorFajillas.CancelarEdicion();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            configuradorFajillas.Guardar();
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            configuradorFajillas.Guardar();

            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }
    }
}
