﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para EditarDatosFiscalesUC.xaml
    /// </summary>
    public partial class ConfigurarDatosFiscalesUC : UserControl
    {
        public ConfigurarDatosFiscalesUC()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            editorClientes.Cancelar();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            editorClientes.Guardar();
        }

        private void EditorClientes_GuardadoExitoso(object sender, RoutedEventArgs e)
        {
            CargarDatosFiscales();
        }

        private void EditorClientes_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            CargarDatosFiscales();
        }

        private void CargarDatosFiscales()
        {
            editorClientes.CargarEstadosCiudades();
            editorClientes.DataContext = new SOTControladores.Controladores.ControladorClientes().ObtenerUltimoCliente() ?? new Modelo.Almacen.Entidades.Cliente();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                CargarDatosFiscales();
            }
            catch { }
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            editorClientes.Guardar();

            if (new SOTControladores.Controladores.ControladorClientes().VerificarNoExisteCliente())
                throw new SOTException(ConfiguradorSOT.Resources.Errores.datos_fiscales_clientes_no_capturados_excepcion);

            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }
    }
}
