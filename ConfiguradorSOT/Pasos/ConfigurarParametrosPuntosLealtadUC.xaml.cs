﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarPuntosLealtadUC.xaml
    /// </summary>
    public partial class ConfigurarParametrosPuntosLealtadUC : UserControl
    {
        public ConfigurarParametrosPuntosLealtadUC()
        {
            InitializeComponent();
        }

        private void EditorClientes_GuardadoExitoso(object sender, RoutedEventArgs e)
        {

        }

        private void EditorClientes_EdicionCancelada(object sender, RoutedEventArgs e)
        {

        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosPuntosLealtad.Guardar();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosPuntosLealtad.Cancelar();
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            editorParametrosPuntosLealtad.Guardar();
            ComandosConfiguracion.AvanzarCommand.Execute(null, null);
        }
    }
}
