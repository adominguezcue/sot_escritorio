﻿using MaterialDesignThemes.Wpf;
using Modelo.Entidades;
using SOTWpf.Habitaciones;
using SOTWpf.TiposHabitacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transversal.Excepciones;

namespace ConfiguradorSOT.Pasos
{
    /// <summary>
    /// Lógica de interacción para ConfigurarHabitacionesUC.xaml
    /// </summary>
    public partial class ConfigurarHabitacionesUC : UserControl
    {
        private EdicionHabitacionesUC editorForm;
        private Guid identificadorDialogo = Guid.NewGuid();

        public ConfigurarHabitacionesUC()
        {
            InitializeComponent();

            dhHabitaciones.Identifier = identificadorDialogo;
        }

        private void btnHorariosPrecios_Click(object sender, RoutedEventArgs e)
        {
            SOTWpf.Utilidades.Dialogos.MostrarDialogos(Application.Current.MainWindow, new HorariosPreciosForm());
        }

        private void btnAgregarHabitacion_Click(object sender, RoutedEventArgs e)
        {
            MostrarEditor();
        }

        private void btnEditarHabitacion_Click(object sender, RoutedEventArgs e)
        {
            var habS = panelHabitacion.HabitacionSeleccionada;

            if (habS == null)
                throw new SOTException(SOTWpf.Textos.Errores.habitacion_no_seleccionada_exception);

            var hab = new SOTControladores.Controladores.ControladorHabitaciones().ObtenerHabitacion(habS.HabitacionInterna?.IdHabitacion ?? 0);

            if (hab == null)
                throw new SOTException(SOTWpf.Textos.Errores.habitacion_no_seleccionada_exception);

            MostrarEditor(hab);
        }

        private void btnEliminarHabitacion_Click(object sender, RoutedEventArgs e)
        {
            var habS = panelHabitacion.HabitacionSeleccionada?.HabitacionInterna;

            if (habS == null)
                throw new SOTException(SOTWpf.Textos.Errores.habitacion_no_seleccionada_exception);

            var controlador = new SOTControladores.Controladores.ControladorHabitaciones();

            if (SOTWpf.Utilidades.Dialogos.Pregunta(SOTWpf.Textos.Mensajes.confirmar_eliminacion_elemento) == MessageBoxResult.Yes)
            {
                controlador.EliminarHabitacion(habS.IdHabitacion);

                SOTWpf.Utilidades.Dialogos.Aviso(SOTWpf.Textos.Mensajes.eliminacion_elemento_exitosa);

                panelHabitacion.ActualizarHabitaciones();
            }
        }

        private async Task MostrarEditor(Habitacion habitacionEditar = null)
        {
            editorForm = new EdicionHabitacionesUC
            {
                DataContext = habitacionEditar ?? new Habitacion() { Activo = true }
            };
            editorForm.EdicionAceptada += EditorForm_EdicionAceptada;
            editorForm.EdicionCancelada += EditorForm_EdicionCancelada;
            //editorForm.EdicionRealizada += BotoneraBase_AceptarClick;
            //editorForm.EdicionCancelada += BotoneraBase_CancelarClick;

            await DialogHost.Show(editorForm, identificadorDialogo);
        }

        private void EditorForm_EdicionCancelada(object sender, RoutedEventArgs e)
        {
            EjecutarComando();
        }

        private void EditorForm_EdicionAceptada(object sender, RoutedEventArgs e)
        {
            panelHabitacion.ActualizarHabitaciones();

            //var h = panelHabitacion.HabitacionSeleccionada;

            //if (h != null)
            //    h.RecargarHabitacion();

            EjecutarComando();
        }

        private void EjecutarComando()
        {
            if (((ICommand)DialogHost.CloseDialogCommand).CanExecute(null))
                ((ICommand)DialogHost.CloseDialogCommand).Execute(null);

            if (editorForm != null)
            {
                editorForm.EdicionAceptada -= EditorForm_EdicionAceptada;
                editorForm.EdicionCancelada -= EditorForm_EdicionCancelada;
            }
        }

        private void dialogHostPrincipal_DialogClosing(object sender, MaterialDesignThemes.Wpf.DialogClosingEventArgs eventArgs)
        {
            
        }

        private void RecargarHabitacion(object sender, RoutedEventArgs e)
        {
            txtHabitacionSeleccionada.Text = panelHabitacion.HabitacionSeleccionada?.HabitacionInterna?.NumeroHabitacion;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            scvHabitaciones.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            scvHabitaciones.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            scvHabitaciones.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            scvHabitaciones.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            scvHabitaciones.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            scvHabitaciones.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;

            chbMostrarPanelTamanoReal.IsChecked = false;
        }
    }
}
