﻿#pragma checksum "..\..\Principal.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "6AACAF55D8F4D56C65809CC2D03448F773F138154CF1D718EBFF7C6E3B64F976"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using ConfiguradorSOT;
using ConfiguradorSOT.Pasos;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ConfiguradorSOT {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 77 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox listBoxMenuPrincipal;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiConfigurarDB;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiConfigurarRolActual;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiConfigurarParametrosSistema;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiConfigurarParametrosOperacion;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiConfigurarPuntosLealtad;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiConfigurarTurnos;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiEditarHabitaciones;
        
        #line default
        #line hidden
        
        
        #line 194 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiEditarMesas;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiEditarDatosFiscales;
        
        #line default
        #line hidden
        
        
        #line 226 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem lbiEditarFajillas;
        
        #line default
        #line hidden
        
        
        #line 243 "..\..\Principal.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ContentControl contenedor;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ConfiguradorSOT;component/principal.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Principal.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 18 "..\..\Principal.xaml"
            ((ConfiguradorSOT.MainWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 22 "..\..\Principal.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.CommandBinding_CanExecute);
            
            #line default
            #line hidden
            
            #line 23 "..\..\Principal.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.AvanzarCommandBinding_Executed);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 26 "..\..\Principal.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.CommandBinding_CanExecute);
            
            #line default
            #line hidden
            
            #line 27 "..\..\Principal.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.RetrocederCommandBinding_Executed);
            
            #line default
            #line hidden
            return;
            case 4:
            this.listBoxMenuPrincipal = ((System.Windows.Controls.ListBox)(target));
            return;
            case 5:
            this.lbiConfigurarDB = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 84 "..\..\Principal.xaml"
            this.lbiConfigurarDB.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.lbiConfigurarRolActual = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 100 "..\..\Principal.xaml"
            this.lbiConfigurarRolActual.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.lbiConfigurarParametrosSistema = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 116 "..\..\Principal.xaml"
            this.lbiConfigurarParametrosSistema.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lbiConfigurarParametrosOperacion = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 132 "..\..\Principal.xaml"
            this.lbiConfigurarParametrosOperacion.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.lbiConfigurarPuntosLealtad = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 148 "..\..\Principal.xaml"
            this.lbiConfigurarPuntosLealtad.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.lbiConfigurarTurnos = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 164 "..\..\Principal.xaml"
            this.lbiConfigurarTurnos.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.lbiEditarHabitaciones = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 180 "..\..\Principal.xaml"
            this.lbiEditarHabitaciones.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.lbiEditarMesas = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 196 "..\..\Principal.xaml"
            this.lbiEditarMesas.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.lbiEditarDatosFiscales = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 212 "..\..\Principal.xaml"
            this.lbiEditarDatosFiscales.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.lbiEditarFajillas = ((System.Windows.Controls.ListBoxItem)(target));
            
            #line 228 "..\..\Principal.xaml"
            this.lbiEditarFajillas.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.lbi_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.contenedor = ((System.Windows.Controls.ContentControl)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

