﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenVentaConTipos
    {
        public int? Id { get; set; }
        public int? IdVentaRenta { get; set; }
        public int? IdComanda { get; set; }
        public int? IdConsumoInterno { get; set; }
        public int? IdOcupacionMesa { get; set; }
        public int? IdOrdenTaxi { get; set; }
        public int NumeroCorte { get; set; }
        public int FolioTicket { get; set; }
        public string SerieTicket { get; set; }
        public bool Cancelada { get; set; }
        public bool Cobrada { get; set; }
        public int IdClasificacionVenta { get; set; }
        public decimal ValorConIVA { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}