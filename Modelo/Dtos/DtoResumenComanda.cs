﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenComanda
    {
        public int Id { get; set; }

        public bool Contabilizable 
        { 
            get 
            {
                return Estado == Entidades.Comanda.Estados.Cobrada || Estado == Entidades.Comanda.Estados.PorPagar;
            } 
        }

        public Entidades.Comanda.Estados Estado { get; set; }

        public DateTime Fecha { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Cortesias { get; set; }

        public decimal Descuentos { get; set; }

        public List<DtoArticuloTicket> Articulos { get; set; }

        public decimal Total
        {
            get { return SubTotal - Descuentos - Cortesias; }
        }
    }
}
