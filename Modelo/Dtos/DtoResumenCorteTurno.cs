﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenCorteTurno
    {
        public int Id { get; set; }
        public int NumeroCorte { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaCorte { get; set; }
    }
}
