﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoEstadisticasHabitacion
    {
        public int Cantidad { get; set; }

        public Habitacion.EstadosHabitacion EstadoHabitacion
        {
            get { return (Habitacion.EstadosHabitacion)IdEstadoHabitacion; }
            set { IdEstadoHabitacion = (int)value; }
        }

        public int IdEstadoHabitacion { get; set; }
    }
}
