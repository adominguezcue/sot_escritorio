﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoItemCobrable// : DtoItemCobrableBase
    {
        public DtoItemCobrable() : base() { }

        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnidad { get; set; }
        public decimal Total 
        { 
            get 
            { 
                return PrecioUnidad * (decimal)Cantidad; 
            }
            //protected set 
            //{
            //    base.Total = value;
            //}
        }
    }
}
