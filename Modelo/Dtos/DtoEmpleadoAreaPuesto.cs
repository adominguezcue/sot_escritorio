﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoEmpleadoAreaPuesto : DtoEmpleadoPuesto
    {
        public string Area { get; set; }
    }
}
