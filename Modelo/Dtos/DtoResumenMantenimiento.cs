﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Modelo.Dtos
{
    public class DtoResumenMantenimiento
    {
        public DateTime Fecha { get; set; }
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Concepto { get; set; }

        public string Descripcion { get; set; }
        public Mantenimiento.Estados? Estado { get; set; }
        public string EstadoStr { get { return Estado.HasValue ? Estado.Descripcion() : "Pendiente"; } }

        public int IdTarea { get; set; }
		
        public int IdHabitacion { get; set; }
		
        public string  NumeroHabitacio { get; set; }
		
    }
}
