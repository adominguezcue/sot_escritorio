﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoDetallesRenta
    {
        public string NumeroServicio { get; set; }

        public List<DtoResumenVentaRenta> ResumenesVentasHabitacion { get; set; }

        public List<DtoResumenComanda> ResumenesComandas { get; set; }

        public decimal Habitacion
        {
            get { return ResumenesVentasHabitacion != null ? ResumenesVentasHabitacion.Where(m=> m.Cobrada).Sum(m=> m.Subtotal) : 0; }
        }

        public decimal Restaurante
        {
            get { return ResumenesComandas != null ? ResumenesComandas.Where(m => m.Contabilizable).Sum(m => m.SubTotal) : 0; }
        }

        public decimal Cortesias
        {
            get
            {
                return (ResumenesComandas != null ? ResumenesComandas.Where(m => m.Contabilizable).Sum(m => m.Cortesias) : 0) +
                       (ResumenesVentasHabitacion != null ? ResumenesVentasHabitacion.Where(m => m.Cobrada).Sum(m => m.Cortesias) : 0);
            }
        }

        public decimal Descuentos
        {
            get
            {
                return (ResumenesComandas != null ? ResumenesComandas.Where(m => m.Contabilizable).Sum(m => m.Descuentos) : 0) +
                       (ResumenesVentasHabitacion != null ? ResumenesVentasHabitacion.Where(m => m.Cobrada).Sum(m => m.Descuentos) : 0);
            }
        }

        public string Reservacion { get; set; }

        public decimal Total
        {
            get { return Habitacion + Restaurante; }
        }
    }
}
