﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenHabitacion
    {
        public int IdHabitacion { get; set; }
        public string NumeroHabitacion { get; set; }
        public string DatoBase { get; set; }
        public string Detalle { get; set; }
        public int IdEstado { get; set; }
        public int? IdEstadoComanda { get; set; }
        public bool EsVencida { get; set; }
        public bool TieneTarjetaV { get; set; }


        public Habitacion.EstadosHabitacion EstadoHabitacion
        {
            get { return (Habitacion.EstadosHabitacion)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public Comanda.Estados? EstadoComanda
        {
            get { return (Comanda.Estados?)IdEstadoComanda; }
            set { IdEstado = (int)value; }
        }

        public int IdTipoHabitacion { get; set; }
        public string NombreTipo { get; set; }
        public int Posicion { get; set; }
        public int Piso { get; set; }
    }
}
