﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoLogComanda
    {
        public int Turno { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime FechaCambio { get; set; }

        public string Ticket { get; set; }

        public decimal Monto { get; set; }

        public string FormasPago { get; set; }

        public string Mesero { get; set; }

        public string Estatus { get; set; }

        public string TiempoPreparacion { get; set; }

        public string TiempoPorEntregar { get; set; }

        public string TiempoPorCobrar { get; set; }

        public string TiempoPorPagar { get; set; }

        public string TiempoTotal { get; set; }

        public double SegundosPreparacion { get; set; }

        public double SegundosPorEntregar { get; set; }

        public double SegundosPorCobrar { get; set; }

        public double SegundosPorPagar { get; set; }

        public double SegundosTotal { get; set; }
    }
}
