﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoArticuloPrepararConsulta
    {
        public int Id { get; set; }
        public string IdArticulo { get; set; }

        public string NombreTipo { get; set; }

        public string Nombre { get; set; }

        public decimal Precio { get; set; }

        public int Cantidad { get; set; }

        public string Resumen { get { return NombreTipo + " - " + Nombre + " (" + Cantidad + ")"; } }

        public string Observaciones { get; set; }

        public Clasificaciones Clasificacion { get; set; }

        public enum Clasificaciones
        {
            Comanda = 1,
            OrdenRestaurante = 2
        }
    }
}
