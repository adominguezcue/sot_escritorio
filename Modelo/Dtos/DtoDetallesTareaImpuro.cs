﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Modelo.Dtos
{
    public class DtoDetallesTareaImpuro
    {
        public string Habitacion { get; set; }

        public TareaLimpieza.Estados Estado { get; set; }

        public TareaLimpieza.TiposLimpieza Tipo { get; set; }

        public int IdHabitacion { get; set; }

        public DateTime? FechaInicioLimpieza { get; set; }

        public DateTime? FechaFinLimpieza { get; set; }

        public DateTime? FechaInicioSupervision { get; set; }

        public DateTime? FechaFinSupervision { get; set; }

        public List<string> NombresRecamareras { get; set; }

        public string NombreSupervisor { get; set; }

        public int FolioCorte { get; set; }

        public string MotivoLiberacion { get; set; }

        public DateTime FechaCreacion { get; set; }

        public string TipoHabitacion { get; set; }

        public static explicit operator DtoDetallesTareaImpuroSTR(DtoDetallesTareaImpuro item)
        {
            if (item == null)
                return null;

            var fechaActual = DateTime.Now;

            return new DtoDetallesTareaImpuroSTR
            {
                TipoHabitacion = item.TipoHabitacion,
                FolioCorte = item.FolioCorte,
                Habitacion = item.Habitacion,
                Estado = item.Estado.Descripcion(),
                Tipo = item.Tipo.Descripcion(),
                FechaInicioLimpieza = item.FechaInicioLimpieza.HasValue ? item.FechaInicioLimpieza.Value.ToString("dd/MM/yyyy hh:mm:ss tt") : "",
                FechaFinLimpieza = item.FechaFinLimpieza.HasValue ? item.FechaFinLimpieza.Value.ToString("dd/MM/yyyy hh:mm:ss tt") : "",
                TiempoLimpieza = item.FechaInicioLimpieza.HasValue ? ((item.FechaFinLimpieza ?? fechaActual) - item.FechaInicioLimpieza.Value).ToString("dd\\.hh\\:mm\\:ss") : "",
                SegundosLimpieza = item.FechaInicioLimpieza.HasValue ? ((item.FechaFinLimpieza ?? fechaActual) - item.FechaInicioLimpieza.Value).TotalSeconds : 0,
                FechaInicioSupervision = item.FechaInicioSupervision.HasValue ? item.FechaInicioSupervision.Value.ToString("dd/MM/yyyy hh:mm:ss tt") : "",
                FechaFinSupervision = item.FechaFinSupervision.HasValue ? item.FechaFinSupervision.Value.ToString("dd/MM/yyyy hh:mm:ss tt") : "",
                TiempoSupervision = item.FechaInicioSupervision.HasValue ? ((item.FechaFinSupervision ?? fechaActual) - item.FechaInicioSupervision.Value).ToString("dd\\.hh\\:mm\\:ss") : "",
                SegundosSupervision = item.FechaInicioSupervision.HasValue ? ((item.FechaFinSupervision ?? fechaActual) - item.FechaInicioSupervision.Value).TotalSeconds : 0,
                NombresRecamareras = string.Join(", ", item.NombresRecamareras),
                NombreSupervisor = item.NombreSupervisor,
                MotivoLiberacion = item.MotivoLiberacion
            };
        }
    }

    public class DtoDetallesTareaImpuroSTR 
    {
        public string RecamaristaClave { get; set; }
        public string TipoHabitacion { get; set; }

        public int FolioCorte { get; set; }

        public string Habitacion { get; set; }

        public string Estado { get; set; }

        public string Tipo { get; set; }

        public string FechaInicioLimpieza { get; set; }

        public string FechaFinLimpieza { get; set; }

        public string FechaInicioSupervision { get; set; }

        public string FechaFinSupervision { get; set; }

        public string NombresRecamareras { get; set; }

        public string NombreSupervisor { get; set; }

        public string MotivoLiberacion { get; set; }

        public string TiempoLimpieza { get; set; }

        public string TiempoSupervision { get; set; }

        public double SegundosLimpieza { get; set; }

        public double SegundosSupervision { get; set; }
    }
}
