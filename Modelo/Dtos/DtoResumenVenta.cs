﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenVenta
    {
        public DtoResumenVenta() 
        {
            Items = new List<DtoItemCobrable>();
            Empleados = new List<DtoEmpleadoPuesto>();
        }

        public bool EsComanda { get; set; }

        public List<DtoItemCobrable> Items { get; set; }
        public List<DtoEmpleadoPuesto> Empleados { get; set; }

        public string Hotel { get; set; }
        public string Direccion { get; set; }
        //public string Usuario { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string RazonSocial { get; set; }
        public string RFC { get; set; }
        public string Procedencia { get; set; }
        public string Tipo { get; set; }
        public string Ticket { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Cortesia { get; set; }

        public decimal Descuentos { get; set; }

        public decimal Total { get; set; }

        public bool Cancelada { get; set; }
    }
}
