﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenPropina
    {
        public TiposTarjeta TipoTarjeta { get; set; }
        public Modelo.Entidades.Propina.TiposPropina TipoPropina { get; set; }
        public decimal Valor { get; set; }
        public string Referencia { get; set; }
        public string NumeroTarjeta { get; set; }
        public string Transaccion { get; set; }
    }
}
