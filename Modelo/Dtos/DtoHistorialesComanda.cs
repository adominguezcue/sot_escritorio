﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoHistorialesComanda
    {
        public int IdComanda { get; set; }
        public int IdTipo { get; set; }

        public int Turno { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime Fecha { get; set; }

        public int FolioTicket { get; set; }

        public string SerieTicket { get; set; }

        public string Ticket { get { return SerieTicket + FolioTicket.ToString(); } }

        public decimal Monto { get; set; }

        public string Mesero { get; set; }

        public int IdEstado { get; set; }

        public int? IdEstadoHistorico { get; set; }

        public DateTime? FechaHistorica { get; set; }

        public bool Modificada { get; set; }

        public DateTime FechaModificacion { get; set; }

        public string FormasPago { get; set; }

        public Modelo.Entidades.Comanda.Estados Estado
        {
            get { return (Modelo.Entidades.Comanda.Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public Modelo.Entidades.Comanda.Tipos Tipo
        {
            get { return (Modelo.Entidades.Comanda.Tipos)IdTipo; }
            set { IdTipo = (int)value; }
        }

        public Modelo.Entidades.Comanda.Estados EstadoHistorico
        {
            get { return (Modelo.Entidades.Comanda.Estados)IdEstadoHistorico; }
            set { IdEstadoHistorico = (int)value; }
        }

        public string Transaccion { get; set; }
    }
}
