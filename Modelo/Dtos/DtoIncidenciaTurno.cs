﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoIncidenciaTurno
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Area { get; set; }

        public DateTime FechaCreacion { get; set; }

        public string NombreEmpleado { get; set; }

        public int NumeroCorte { get; set; }

        public bool EstaCancelada { get; set; }

        public int IdUsuario { get; set; }
    }
}
