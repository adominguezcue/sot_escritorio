﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoEmpleadoPuesto
    {
        public int IdEmpleado { get; set; }
        public string NombreEmpleado { get; set; }
        public string Puesto { get; set; }
    }
}
