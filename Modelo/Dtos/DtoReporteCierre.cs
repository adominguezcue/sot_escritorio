﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoReporteCierre : DtoReporteContable
    {
        public decimal VentaDocumental { get; set; }

        public List<DtoMoneda> Desglose { get; set; }

        public int CantidadFajillas { get; set; }

        public decimal TotalDesglose { get { return Desglose != null && Desglose.Count > 0 ? Desglose.Sum(m => m.Total) : 0; } }

        public decimal TotalEfectivoCierre { get { return TotalDesglose - PagoNetoPropinas; } }

        public decimal PagoNetoPropinas { get; set; }

        public decimal PropinasPagadas { get; set; }

        public decimal Diferencia { get; set; }

        public string EstatusDiferencia { get; set; }

        public decimal AnticipoReservas { get { return VTRE_Reservaciones; } }

        public int CancelacionesHabitacion { get; set; }

        public int CancelacionesRoomRest { get; set; }

        public decimal TotalCancelaciones { get; set; }
		
        public string AliasUsuarioCerro { get; set; }
		
        public string SiglaTurno { get; set; }
		
    }
}
