﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoArticuloPrepararGeneracion
    {
        public int Id { get; set; }
        public bool Activo { get; set; }
        public string IdArticulo { get; set; }
        public int Cantidad { get; set; }
        [Obsolete("Por los enlaces en el front no le cambio el nombre, pero es precio por unidad sin IVA")]
        public decimal PrecioConIVA { get; set; }
        public string Observaciones { get; set; }
        public bool EsCortesia { get; set; }
    }
}
