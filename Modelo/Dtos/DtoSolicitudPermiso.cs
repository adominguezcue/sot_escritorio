﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoSolicitudPermiso
    {
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string Area { get; set; }
        public DateTime FechaAplicacion { get; set; }
        public string Motivo { get; set; }
        public bool EsSalida { get; set; }
        public bool SolicitudAnticipada { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
