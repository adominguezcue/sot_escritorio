﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo.Dtos
{
    public class DtoResumenArticuloComanda
    {
        public int Id { get; set; }

        public bool Activo { get; set; }

        public string IdArticulo { get; set; }

        public string Nombre { get; set; }

        public bool EsOtroDepto { get; set; }

        public string OtosDeptos { get; set; }
        public string Tipo { get; set; }

        public int Cantidad { get; set; }

        public decimal PrecioUnidad { get; set; }

        public string Observaciones { get; set; }

        public bool EsCortesia { get; set; }

        public Entidades.ArticuloComanda.Estados Estado { get; set; }
    }
}
