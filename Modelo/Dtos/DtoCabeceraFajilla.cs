﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoCabeceraFajilla
    {
        public DtoCabeceraFajilla() 
        {
            Desglose = new List<DtoMoneda>();
        }
        public string NombreCrea { get; set; }
        public string PuestoCrea { get; set; }
        public string NombreRecibe { get; set; }
        public string PuestoRecibe { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaAutorizacion { get; set; }
        public int FolioCorte { get; set; }
        public string Turno { get; set; }

        public List<DtoMoneda> Desglose { get; set; }
    }
}
