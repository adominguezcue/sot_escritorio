﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
   public class DtoCortesiasCancelaciones
    {
        public int idVentaCorrecta { get; set; }
        public string Ticket { get; set; }
        public DateTime FechaModificacion { get; set; }
        public string  Habitacion { get; set; }
        public decimal Precio { get; set; }
        public string Concepto { get; set; }
        public string MotivoCancelacion { get; set; }
        public string UsuarioCancelacion { get; set; }
		 public string Turno { get; set; }								 
    }
}
