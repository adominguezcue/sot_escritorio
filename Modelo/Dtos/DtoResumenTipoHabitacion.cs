﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenTipoHabitacion
    {
        public DtoResumenTipoHabitacion()
        {
            ResumenesLimpiezaHabitacion = new List<DtoResumenLimpiezaHabitacion>();
            ResumenesTarifas = new List<DtoResumenTarifa>();
        }
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public string DescripcionExtendida { get; set; }

        public List<DtoResumenLimpiezaHabitacion> ResumenesLimpiezaHabitacion { get; set; }

        public List<DtoResumenTarifa> ResumenesTarifas { get; set; }

        public class DtoResumenLimpiezaHabitacion 
        {

            public Entidades.TareaLimpieza.TiposLimpieza TipoLimpieza { get; set; }

            public int Minutos { get; set; }
        }

        public class DtoResumenTarifa 
        {
            public DtoResumenTarifa() 
            {
                ResumenesTiempo = new List<DtoResumenTiempo>();
            }

            public Entidades.ConfiguracionTarifa.Tarifas Tarifa { get; set; }

            public decimal Precio { get; set; }

            public List<DtoResumenTiempo> ResumenesTiempo { get; set; }
        }

        public class DtoResumenTiempo 
        {

            public int Minutos { get; set; }

            public int Orden { get; set; }
        }
    }
}
