﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenTareaMantenimiento
    {
        public DateTime Fecha { get; set; }
        public string Motivo { get; set; }
        public string Detalles { get; set; }
        public int IdTipoMantenimiento { get; set; }
        public TareaMantenimiento.TiposMantenimiento TipoMantenimiento 
        {
            get { return (TareaMantenimiento.TiposMantenimiento)IdTipoMantenimiento; }
            set { IdTipoMantenimiento = (int)value; }
        }
    }
}
