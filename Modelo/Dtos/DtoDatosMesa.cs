﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoDatosMesa
    {
        public string NumeroMesa { get; set; }
        public string NumeroServicio { get; set; }
        public DateTime? FechaEntrada { get; set; }
        public Mesa.EstadosMesa Estado { get; set; }
        public string Mesero { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Cortesias { get; set; }
        //public decimal TotalSinIVA { get; set; }
        //public decimal IVA { get; set; }
        public decimal TotalConIVA { get; set; }

    }
}
