﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoComisionesValets
    {
        public System.Data.DataTable DatosFondosPagar { get; set; }

        public System.Data.DataTable DatosPropinasTarjetas { get; set; }

        public System.Data.DataTable DatosResumen { get; set; }
    }
}
