﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoAreas
    {
        public int Id { get; set; }

        public string NombreArea { get; set; }
    }
}
