﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoPagoHabitacionCorte 
    {
        [DisplayFormat(DataFormatString = "dd/MM/yyyy hh:mm:ss tt")]
        public DateTime Fecha { get; set; }
        public string Ticket { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Total 
        {
            get 
            { 
                return Habitacion + HospedajeExtra + PersonasExtra + Paquetes - 
                       Cortesias - Descuento; 
            }
        }
        public string Cajero { get; set; }
        public string Valet { get; set; }
        [DisplayName("Forma de pago")]
        public TiposPago FormaPago { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Propina { get; set; }
        public string Movimiento { get; set; }
        [DisplayName("Operación")]
        public string Operacion { get; set; }
        [DisplayFormat(DataFormatString="{0:C}")]
        [DisplayName("Habitación")]
        public decimal Habitacion { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("P. Extra")]
        public decimal PersonasExtra { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("H. Extra")]
        public decimal HospedajeExtra { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Paquetes { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Cortesías")]
        public decimal Cortesias { get; set; }
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Descuento { get; set; }

        public int Id { get; set; }

        public string Referencia { get; set; }

        public string NumeroTarjeta { get; set; }

        public TiposTarjeta? TipoTarjeta { get; set; }

        public int? IdValet { get; set; }

        public decimal Consumos { get; set; }
    }
}
