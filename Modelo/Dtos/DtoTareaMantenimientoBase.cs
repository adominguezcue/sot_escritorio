﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoTareaMantenimientoBase
    {
        public int Id { get; set; }
        public DateTime FechaSiguiente { get; set; }
        public string Concepto { get; set; }
    }
}
