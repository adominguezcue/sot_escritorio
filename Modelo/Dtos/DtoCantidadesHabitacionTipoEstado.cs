﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoCantidadesHabitacionTipoEstado
    {
        public string TipoHabitacion { get; set; }
        public int IdTipoHabitacion { get; set; }
        public int IdEstadoHabitacion { get; set; }
        public int Cantidad { get; set; }
        public Entidades.Habitacion.EstadosHabitacion EstadoHabitacion
        {
            get { return (Entidades.Habitacion.EstadosHabitacion)IdEstadoHabitacion; }
            set { IdEstadoHabitacion = (int)value; }
        }
    }
}
