﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoSolicitudCambioDescanso
    {
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string Area { get; set; }
        public DateTime FechaAplicacion { get; set; }
        public string NombreSuplente { get; set; }
        public bool EsDescanso { get; set; }
    }
}
