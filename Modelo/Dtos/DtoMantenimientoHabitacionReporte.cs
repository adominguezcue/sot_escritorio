﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoMantenimientoHabitacionReporte
    {
        public int Habitacion { get; set; }

        public string TipoHabitacion { get; set; }
    }
}
