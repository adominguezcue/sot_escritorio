﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoDatosPersonaExtrasRepCorteTurno
    {
        public int Cantidad { get; set; }

        public decimal Precio { get; set; }
    }
}
