﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoMonedaExtranjera
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Abreviatura { get; set; }
        public decimal ValorCambio { get; set; }
        public List<MontoFajilla> MontosFajilla { get; set; }
    }
}
