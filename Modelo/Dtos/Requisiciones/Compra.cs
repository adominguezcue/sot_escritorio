﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("CompraCollection")]
    public class ComprasCollection {
        [System.Xml.Serialization.XmlArray("compras")]
        [System.Xml.Serialization.XmlArrayItem("compra", typeof(compra))]
        public compra[] compras { get; set; }
    }

    [Serializable()]
    public class compra 
    {
        
        [System.Xml.Serialization.XmlElement("uid")]
        public Guid? uid_web { get; set; }
       

        public bool ShouldSerializeid_web() {
            return uid_web.HasValue;
        }

        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
        
        [System.Xml.Serialization.XmlElement("cod_oc")]
        public int Cod_OC {get; set;}

        [System.Xml.Serialization.XmlElement("proveedor")]
        public string Nom_Prov {get;set;}

        [System.Xml.Serialization.XmlElement("fecha")]
        public DateTime Fch_OC {get; set;}

        [System.Xml.Serialization.XmlElement("status")]
        public string status {get; set;}
        [System.Xml.Serialization.XmlElement("cod_art")]
        public string Cod_Art {get; set;}
        [System.Xml.Serialization.XmlElement("costo")]
        public decimal Cos_Art {get; set;}
        [System.Xml.Serialization.XmlElement("surtido")]
        public int CtdStdDet_OC {get; set;}
        [System.Xml.Serialization.XmlElement("cat_alm")]
        public int Cat_Alm {get; set;}
         [System.Xml.Serialization.XmlElement("anio")]
        public int anio {get; set;}
         [System.Xml.Serialization.XmlElement("mes")]
        public int mes {get; set;}


    }
}
