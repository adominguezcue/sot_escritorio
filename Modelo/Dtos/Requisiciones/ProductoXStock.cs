﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
namespace Modelo
{
    
        [Serializable()]
        [System.Xml.Serialization.XmlRoot("productxstockCollection")]
        public class productxstockCollection
        {
            [System.Xml.Serialization.XmlArray("productxstock")]
            [System.Xml.Serialization.XmlArrayItem("productxstock", typeof(productxstock))]
            public productxstock[] productxstock { get; set; }

        }

        [Serializable()]
        public class productxstock
        {
            private Guid _uid = Guid.NewGuid();
            [System.Xml.Serialization.XmlElement("uid")]
            public Guid? uid { get { return _uid; } set{_uid = value.GetValueOrDefault();} }
            [System.Xml.Serialization.XmlElement("taller")]
            public string taller { get; set; }
            [System.Xml.Serialization.XmlElement("cod_alm")]
            public int cod_alm { get; set; }

            [System.Xml.Serialization.XmlElement("cod_art")]
            public string  cod_art { get; set; }

            [System.Xml.Serialization.XmlElement("costo_promedio")]
            public decimal costo_promedio { get; set; }

            [System.Xml.Serialization.XmlElement("existencia")]
            public decimal existencia { get; set; }

            //public bool ShouldSerializeid_web()
            //{
            //    return id_web.HasValue;
            //}
            [System.Xml.Serialization.XmlIgnore()]
            public bool actualizado { get; set; }
           
            
            //public Guid person_uid_sitio { get { return person.uid; } set { person.uid = value; } }
            //private Person _person = new Person();
            //[System.Xml.Serialization.XmlElement("person_attributes")]
            //public Person person { get { return _person; } set { _person = value; } }
            //[System.Xml.Serialization.XmlElement("credit_days")]
            //public int credit_days { get; set; }
        }
    
}
