﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    
        [Serializable()]
        [System.Xml.Serialization.XmlRoot("customerCollection")]
        public class CustomerCollection
        {
            [System.Xml.Serialization.XmlArray("customers")]
            [System.Xml.Serialization.XmlArrayItem("customer", typeof(customer))]
            public customer[] customer { get; set; }

        }

        [Serializable()]
        public class customer
        {
            private Guid _uid = Guid.NewGuid();
            [System.Xml.Serialization.XmlElement("uid")]
            public Guid? uid { get { return _uid; } set{_uid = value.GetValueOrDefault();} }
            [System.Xml.Serialization.XmlElement("name")]
            public string name { get; set; }
            [System.Xml.Serialization.XmlElement("taller")]
            public string taller { get; set; }
            [System.Xml.Serialization.XmlElement("cod_cte")]
            public int Cod_Cte { get; set; }

            [System.Xml.Serialization.XmlElement("cod_cte_base")]
            public int cod_cte_base { get; set; }

            [System.Xml.Serialization.XmlElement("lugar_envio")]
            public string lugar_envio { get; set; }

            [System.Xml.Serialization.XmlElement("ApPat_Cte")]
            public string ApPat_Cte  { get; set; }

            [System.Xml.Serialization.XmlElement("ApMat_Cte")]
            public string ApMat_Cte { get; set; }

            [System.Xml.Serialization.XmlElement("telefono")]
            public string telefono { get; set; }

            [System.Xml.Serialization.XmlElement("email")]
            public string email { get; set; }

            //public bool ShouldSerializeid_web()
            //{
            //    return id_web.HasValue;
            //}
            [System.Xml.Serialization.XmlIgnore()]
            public bool actualizado { get; set; }
           
            
            //public Guid person_uid_sitio { get { return person.uid; } set { person.uid = value; } }
            //private Person _person = new Person();
            //[System.Xml.Serialization.XmlElement("person_attributes")]
            //public Person person { get { return _person; } set { _person = value; } }
            //[System.Xml.Serialization.XmlElement("credit_days")]
            //public int credit_days { get; set; }
        }
    
}
