﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("saleCollection")]
    public class saleCollection
    {
        [System.Xml.Serialization.XmlArray("sales")]
        [System.Xml.Serialization.XmlArrayItem("sale", typeof(sale))]
        public sale[] sale { get; set; }

    }



    //[Serializable()]
    //[System.Xml.Serialization.XmlType(IncludeInSchema = true, TypeName = "array")]
    //public class inventory_movements_attributes
    //{
    //    [System.Xml.Serialization.XmlAttribute("type")]
    //    public string type = "array";

    //    private List<InventoryMovement> _Detalle = new List<InventoryMovement>();
    //    [System.Xml.Serialization.XmlElement("inventory_movement")]
    //    public List<InventoryMovement> inventory_movement
    //    {
    //        get { return _Detalle; }
    //        set { _Detalle = value; }
    //    }
    //}
    

    [Serializable()]
    public class type_payments_attributes
    {
        [System.Xml.Serialization.XmlAttribute("type")]
        public string type = "array";

        private List<TypePayment> _Detalle = new List<TypePayment>();
        [System.Xml.Serialization.XmlElement("type_payment")]
        public List<TypePayment> type_payments
        {
            get { return _Detalle; }
            set { _Detalle = value; }
        }
    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("sale", Namespace="")]
    public class sale
    {

        [System.Xml.Serialization.XmlElement("id", typeof(int))]
        public int? id_web { get; set; }
        public bool ShouldSerializeid_web()
        {
            return id_web.HasValue;
        }
        [System.Xml.Serialization.XmlElement("folio")]
        public string folio { get; set; }
        [System.Xml.Serialization.XmlElement("bill_folio")]
        public string factura { get; set; }

        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }

        [System.Xml.Serialization.XmlElement("bill_date")]
        public DateTime? fecha { get; set; }

        [System.Xml.Serialization.XmlElement("cash_register_id", typeof(int))]
        public int cash_register_id { get; set; }

        private customer _customer = new customer();
        [System.Xml.Serialization.XmlIgnore()]
        public customer customer {get{return _customer;}  set{_customer = value;}}

        //[System.Xml.Serialization.XmlElement("customer_id", typeof(int))]
        //public int customer_web_id { get { return _customer.id_web.Value; } set { _customer.id_web = value; } }
        //public bool ShouldSerializecustomer_web_id()
        //{
        //    return _customer.id_web.HasValue;
        //}
        //[System.Xml.Serialization.XmlElement("inventory_movements_attributes")]
           //[System.Xml.Serialization.XmlArrayItem("inventory_movement")]
           //[System.Xml.Serialization.XmlArray("inventory_movements_attributes")]
           //[System.Xml.Serialization.XmlType("array")]
           //public InventoryMovement[] inventory_movements_attributes
           //{
           //    get { return _Detalle.ToArray(); }
           //    set
           //    {
           //        if (value != null && value.Length > 0)
           //            _Detalle.AddRange(value);
           //    }

           //}
           //[System.Xml.Serialization.XmlElement("inventory_movements")]
           //inventory_movements_attributes _inventory_movements_attributes = new inventory_movements_attributes();

           ////[System.Xml.Serialization.XmlElement("inventory_movements_attributes")]
           //public inventory_movements_attributes inventory_movements_attributes
           //{
           //    get { return _inventory_movements_attributes; }
           //    set
           //    {
           //        _inventory_movements_attributes = value;
           //    }
           //}
        //private List<InventoryMovement> _Detalle = new List<InventoryMovement>();
        //[System.Xml.Serialization.XmlIgnore()]
        //public List<InventoryMovement> Detalle 
        //{
        //    get {

        //        //return _Detalle;//
        //        return _inventory_movements_attributes.inventory_movement ;
        //    }
        //    set {

        //        //_Detalle = value; //
        //        _inventory_movements_attributes.inventory_movement = value;
        //    }
        //}

        private type_payments_attributes _Pagos = new type_payments_attributes();
        [System.Xml.Serialization.XmlElement("type_payments_attributes")]
        public type_payments_attributes type_payments_attributes
        {
            get { return _Pagos; }
            set { _Pagos = value; }
        }

        [System.Xml.Serialization.XmlIgnore()]
        public List<TypePayment> Pagos
        {
            get { return _Pagos.type_payments; }
            set { _Pagos.type_payments = value; }
        }

        //[System.Xml.Serialization.XmlElement("folio_cash_register")]
        //public string folio_cash_register { get; set; }

        [System.Xml.Serialization.XmlElement("seller_id")]
        public int? seller_id { get; set; }
        public bool ShouldSerializeseller_id()
        {
            return this.seller_id.HasValue;
        }
    }
}
