﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("stateCollection")]
    public class stateCollection
    {
        [System.Xml.Serialization.XmlArray("states")]
        [System.Xml.Serialization.XmlArrayItem("state", typeof(State))]
        public State[] state { get; set; }

    }


    public class State
    {
        public int id { get; set; }
        public string description { get; set; }
        public int country_id { get; set; }
    }
}
