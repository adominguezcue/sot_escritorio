﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class TypePaymentCollection{
    
    }
    public class TypePayment
    {
        [System.Xml.Serialization.XmlElement("id")]
        public int? id_web { get; set; }
        
        
        private MethodPayment _method_payment = new MethodPayment();
        [System.Xml.Serialization.XmlIgnore()]
        public MethodPayment method_payment {
            get { return _method_payment; }
            set { _method_payment = value; }
        }
        public int method_payment_id {
            get { return _method_payment.id; }
            set { _method_payment.id = value; }
        }
        public string reference { get; set; }
        public decimal total { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public string folio { get; set; }
        
        public bool ShouldSerializeid_web()
        {
            return id_web.HasValue;
        }
        [System.Xml.Serialization.XmlElement("id_sitio")]
        public int id { get; set; }
    }

}
