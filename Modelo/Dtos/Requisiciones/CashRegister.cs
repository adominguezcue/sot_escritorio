﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    
        [Serializable()]
        [System.Xml.Serialization.XmlRoot("CashRegisterCollection")]
        public class CashRegisterCollection
        {
            [System.Xml.Serialization.XmlArray("cash_registers")]
            [System.Xml.Serialization.XmlArrayItem("cash_register", typeof(CashRegister))]
            public CashRegister[] cash_register { get; set; }

        }

        [Serializable()]
        public class CashRegister
        {
            [System.Xml.Serialization.XmlElement("id")]
            public int id{ get; set; }
            [System.Xml.Serialization.XmlElement("description")]
            public string description { get; set; }

            [System.Xml.Serialization.XmlElement("folio")]
            public string folio { get; set; }
        }
    
}
