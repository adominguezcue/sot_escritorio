﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("countryCollection")]
    public class countryCollection
    {
        [System.Xml.Serialization.XmlArray("countries")]
        [System.Xml.Serialization.XmlArrayItem("country", typeof(Country))]
        public Country[] country { get; set; }

    }


    public class Country
    {
        public int id { get; set; }
        public string description { get; set; }

    }
}
