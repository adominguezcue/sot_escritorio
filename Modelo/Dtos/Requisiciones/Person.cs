﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("personCollection")]
    public class PersonCollection
    {
        [System.Xml.Serialization.XmlArray("people")]
        [System.Xml.Serialization.XmlArrayItem("person", typeof(Person))]
        public Person[] person { get; set; }

    }

    [Serializable()]
    public class Person
    {
        private Guid _uid = Guid.NewGuid();
        [System.Xml.Serialization.XmlElement("uid_sitio")]
        public Guid uid { get { return _uid; } set { _uid = value; } }
        [System.Xml.Serialization.XmlElement("id")]
        public int? id_web { get; set; }
        public bool ShouldSerializeid_web()
        {
            return id_web.HasValue;
        }
        [System.Xml.Serialization.XmlElement("RFC")]
        public string RFC {get; set;}
        [System.Xml.Serialization.XmlElement("address")]
        public string address {get; set;}
        [System.Xml.Serialization.XmlElement("celphone")]
        public string celphone {get; set;}
        [System.Xml.Serialization.XmlElement("city")]
        public string city {get; set;}
        [System.Xml.Serialization.XmlElement("cp")]
        public string cp {get; set;}
        [System.Xml.Serialization.XmlElement("email")]
        public string email {get; set;}
        [System.Xml.Serialization.XmlElement("email_confirmation")]
        public string email_confirmation { get { return this.email; } set { } }
        [System.Xml.Serialization.XmlElement("ext")]
        public string ext {get; set;}
        [System.Xml.Serialization.XmlElement("last_name")]
        public string last_name {get; set;}
        [System.Xml.Serialization.XmlElement("name")]
        public string name {get; set;}
        [System.Xml.Serialization.XmlElement("phone")]
        public string phone {get; set;}
        [System.Xml.Serialization.XmlElement("state_id")]
        public int state_id {get; set;}
        //public string email_confirmation {get;set;}
        [System.Xml.Serialization.XmlElement("country_id")]
        public int country_id { get; set; }
    }
}
