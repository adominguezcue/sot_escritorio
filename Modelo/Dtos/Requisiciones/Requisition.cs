﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [System.Xml.Serialization.XmlRoot("requisitionCollection")]
    public class RequisitionsCollection
    {
        [System.Xml.Serialization.XmlArray("requisitions")]
        [System.Xml.Serialization.XmlArrayItem("requisition", typeof(Requisition))]
        public Requisition[] requisition { get; set; }

    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("requisition", Namespace = "")]
    public class Requisition
    {
        [System.Xml.Serialization.XmlElement("cod_cte")]
        public int cod_cte { get; set; }
        [System.Xml.Serialization.XmlElement("estatus")]
        public string estatus { get; set; }
        [System.Xml.Serialization.XmlElement("uid")]
        public Guid? uid_web { get; set; }
        [System.Xml.Serialization.XmlElement("taller")]
        public string taller { get; set; }
        [System.Xml.Serialization.XmlElement("cod_alm")]
        public int cod_alm { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
        [System.Xml.Serialization.XmlElement("year")]
        public int anio { get; set; }
        [System.Xml.Serialization.XmlElement("month")]
        public int mes { get; set; }
        [System.Xml.Serialization.XmlElement("observaciones", typeof(string))]
        public string observaciones { get; set; }


        detail_requisitions_attributes _detail_requisitions_attributes = new detail_requisitions_attributes();

        [System.Xml.Serialization.XmlElement("detail_requisitions_attributes")]
        public detail_requisitions_attributes detail_requisitions_attributes
        {

            get { return _detail_requisitions_attributes; }
            set
            {
                _detail_requisitions_attributes = value;
            }
        }

    }



    [Serializable()]
    public class detail_requisitions_attributes
    {
        [System.Xml.Serialization.XmlAttribute("type")]
        public string type = "array";

        private List<DetailRequisition> _Detalle = new List<DetailRequisition>();
        [System.Xml.Serialization.XmlElement("detail_requisition")]
        public List<DetailRequisition> detail_requisitions
        {
            get { return _Detalle; }
            set { _Detalle = value; }
        }
    }



    [Serializable()]
    [System.Xml.Serialization.XmlRoot("detail_requisition", Namespace = "")]
    public class DetailRequisition
    {
        [System.Xml.Serialization.XmlElement("folio_requisition")]
        public int folio_requisition { get; set; }

        [System.Xml.Serialization.XmlElement("amount")]
        public int amount { get; set; }


        [System.Xml.Serialization.XmlElement("price")]
        public decimal price { get; set; }

        [System.Xml.Serialization.XmlElement("cod_art")]
        public string cod_art { get; set; }

        [System.Xml.Serialization.XmlElement("cod_mot")]
        public int cod_mot { get; set; }


        [System.Xml.Serialization.XmlElement("uid")]
        public Guid? uid_web { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
        [System.Xml.Serialization.XmlElement("omitir_iva")]
        public bool omitir_iva { get; set; }
    }

}
