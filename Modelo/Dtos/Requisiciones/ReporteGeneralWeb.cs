﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("ReporteGeneralCollection")]
    public class ReporteGeneralCollection
    {
        [System.Xml.Serialization.XmlArray("reporte_generals")]
        [System.Xml.Serialization.XmlArrayItem("reporte_general", typeof(ReporteGeneral))]
        public ReporteGeneral[] reporte_general { get; set; }

    }

    [Serializable()]
    public class ReporteGeneral
    {
        [System.Xml.Serialization.XmlElement("uid")]
        public Guid uid { get; set; }

        [System.Xml.Serialization.XmlElement("total")]
        public double total { get; set; }

        [System.Xml.Serialization.XmlElement("almacen")]
        public string almacen { get; set; }
        
        [System.Xml.Serialization.XmlElement("tipo")]
        public string tipo { get; set; }

        [System.Xml.Serialization.XmlElement("year")]
        public int year { get; set; }

        [System.Xml.Serialization.XmlElement("month")]
        public int month { get; set; }

        [System.Xml.Serialization.XmlElement("taller", typeof(string))]
        public string taller { get; set; }

    }

}
