﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("sellerCollection")]
    public class sellerCollection
    {
        [System.Xml.Serialization.XmlArray("sellers")]
        [System.Xml.Serialization.XmlArrayItem("seller", typeof(Seller))]
        public Seller[] seller { get; set; }

    }
    
    public class Seller
    {
        public int id { get; set; }
        public string name { get; set; }
        public string key { get; set; }

    }
}
