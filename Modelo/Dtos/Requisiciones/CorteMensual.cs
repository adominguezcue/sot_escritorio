﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("CorteMensualCollection")]
    public class CorteMensualesCollection {
        [System.Xml.Serialization.XmlArray("corte_mensuales")]
        [System.Xml.Serialization.XmlArrayItem("corte_mensual", typeof(corte_mensual))]
        public corte_mensual[] corte_mensuales { get; set; }
    }

    [Serializable()]
    public class corte_mensual 
    {
        
        [System.Xml.Serialization.XmlElement("UID")]
        public Guid? uid_web { get; set; }
       
        public bool ShouldSerializeid_web() {
            return uid_web.HasValue;
        }

        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
        
        [System.Xml.Serialization.XmlElement("region", typeof(string))]
        public string region { get; set; }

      [System.Xml.Serialization.XmlElement("distrital", typeof(string))]
      public string distrital {get; set;}

      [System.Xml.Serialization.XmlElement("CECO", typeof(int))]
      public int CECO {get; set;}

      [System.Xml.Serialization.XmlElement("tienda", typeof(string))]
      public string tienda {get; set;}

      [System.Xml.Serialization.XmlElement("folio", typeof(int))]
      public int folio {get; set;}

      [System.Xml.Serialization.XmlElement("fecha_folio", typeof(DateTime))]
      public DateTime fecha_folio {get; set;}

      [System.Xml.Serialization.XmlElement("articulo", typeof(string))]
      public string articulo {get; set;}

      [System.Xml.Serialization.XmlElement("nombre", typeof(string))]
      public string nombre {get; set;}

      [System.Xml.Serialization.XmlElement("cantidad", typeof(int))]
      public int cantidad {get; set;}

      [System.Xml.Serialization.XmlElement("precio", typeof(decimal))]
      public decimal Precio {get; set;}

      [System.Xml.Serialization.XmlElement("sub_total", typeof(decimal))]
      public decimal sub_total { get; set; }

      [System.Xml.Serialization.XmlElement("total", typeof(decimal))]
      public decimal total { get; set; }

      [System.Xml.Serialization.XmlElement("presupuesto", typeof(decimal))]
      public decimal presupuesto { get; set; }

      [System.Xml.Serialization.XmlElement("almacen", typeof(string))]
      public string almacen { get; set; }

      //[System.Xml.Serialization.XmlElement("cod_alm", typeof(int))]
      //public int cod_alm {get; set;}

      //[System.Xml.Serialization.XmlElement("cod_mot", typeof(int))]
      //public string cod_mot {get; set;}

      [System.Xml.Serialization.XmlElement("vin", typeof(string))]
      public string vin {get; set;}

      [System.Xml.Serialization.XmlElement("motor", typeof(string))]
      public string motor { get; set;}

      [System.Xml.Serialization.XmlElement("taller", typeof(string))]
      public string taller { get; set; }

      [System.Xml.Serialization.XmlElement("mes", typeof(int))]
      public int mes { get; set; }

      [System.Xml.Serialization.XmlElement("anio", typeof(int))]
      public int anio { get; set; }

       [System.Xml.Serialization.XmlElement("status", typeof(string))]
      public string status { get; set; }
    }
}
