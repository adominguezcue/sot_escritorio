﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
     [Serializable()]
     [System.Xml.Serialization.XmlRoot("BudgetsCollection")]
    public class BudgetsCollection
    {
        [System.Xml.Serialization.XmlArray("budgets")]
        [System.Xml.Serialization.XmlArrayItem("budget", typeof(budget))]
         public budget[] budgets { get; set; }
    }

    [Serializable()]
    public class budget
    {
        [System.Xml.Serialization.XmlElement("anno")]
        public  int  Anno_Pres {get; set;}
        [System.Xml.Serialization.XmlElement("month")]
        public int month_Pres	{get; set;}
        [System.Xml.Serialization.XmlElement("cod_cte")]
        public int Cod_Cte	{get; set;}
        [System.Xml.Serialization.XmlElement("monto")]
        public decimal monto_Pres	{get; set;}
        [System.Xml.Serialization.XmlElement("uid")]
        public Guid? uid	{get; set;}
        [System.Xml.Serialization.XmlElement("taller")]
        public string taller { get; set; }
        [System.Xml.Serialization.XmlElement("cod_alm")]
        public int cod_alm { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
    }
}