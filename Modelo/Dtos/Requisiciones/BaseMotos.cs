﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("BaseMotosCollection")]
    public class BaseMotosCollection {
        [System.Xml.Serialization.XmlArray("base_motos")]
        [System.Xml.Serialization.XmlArrayItem("base_moto", typeof(base_moto))]
        public base_moto[] base_motos { get; set; }
    }

    [Serializable()]
    public class base_moto 
    {
        [System.Xml.Serialization.XmlElement("marca")]
        public string marca { get; set; }

        [System.Xml.Serialization.XmlElement("UID")]
        public Guid? uid_web { get; set; }
       
        public bool ShouldSerializeid_web() {
            return uid_web.HasValue;
        }

        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
        
        [System.Xml.Serialization.XmlElement("region", typeof(string))]
        public string region { get; set; }

        [System.Xml.Serialization.XmlElement("ceco", typeof(int))]
        public int ceco { get; set; }

        [System.Xml.Serialization.XmlElement("tienda", typeof(string))]
        public string tienda { get; set; }

        [System.Xml.Serialization.XmlElement("codigo", typeof(int))]
        public int codigo { get; set; }

        [System.Xml.Serialization.XmlElement("motor", typeof(string))]
        public string motor { get; set; }

        [System.Xml.Serialization.XmlElement("anno_moto", typeof(int))]
        public int anno_motor { get; set; }

        [System.Xml.Serialization.XmlElement("serie", typeof(string))]
        public string serie { get; set; }

        [System.Xml.Serialization.XmlElement("eco", typeof(string))]
        public string eco { get; set; }

        [System.Xml.Serialization.XmlElement("tipo", typeof(string))]
        public string tipo { get; set; }

        [System.Xml.Serialization.XmlElement("estatus", typeof(string))]
        public string estatus { get; set; }

        [System.Xml.Serialization.XmlElement("fecha_alta")]
        public DateTime fecha_alta { get; set; }

        [System.Xml.Serialization.XmlElement("fecha_baja")]
        public DateTime fecha_baja { get; set; }
        
        [System.Xml.Serialization.XmlElement("asegurada", typeof(string))]
        public string asegurada { get; set; }
        
        [System.Xml.Serialization.XmlElement("responsoble", typeof(string))]
        public string responsoble { get; set; }
        
        [System.Xml.Serialization.XmlElement("placas", typeof(string))]
        public string placas { get; set; }
        
        [System.Xml.Serialization.XmlElement("zona", typeof(string))]
        public string zona { get; set; }
        
        [System.Xml.Serialization.XmlElement("no_taller", typeof(string))]
        public string no_taller { get; set; }

        [System.Xml.Serialization.XmlElement("taller", typeof(string))]
        public string taller { get; set; }
    }
}
