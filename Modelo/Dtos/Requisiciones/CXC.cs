﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    public class CXC
    {
        public Double amount { get; set; }
        public DateTime create_date { get; set; }
        public int customer_id { get; set; }
        public DateTime due_date { get; set; }
        public int order_id { get; set; }
        public Double paid { get; set; }
        public int type_of_payment_id { get; set; }
    }
}
