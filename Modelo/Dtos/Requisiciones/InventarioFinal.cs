﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("InventarioCollection")]
    public class InventarioFinalesCollection {
        [System.Xml.Serialization.XmlArray("inventario")]
        [System.Xml.Serialization.XmlArrayItem("inventario", typeof(inventario))]
        public inventario[] inventarios { get; set; }
    }

    [Serializable()]
    public class inventario
    {
        
        [System.Xml.Serialization.XmlElement("UID")]
        public Guid? uid_web { get; set; }
       
        public bool ShouldSerializeid_web() {
            return uid_web.HasValue;
        }

        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
        
        [System.Xml.Serialization.XmlElement("taller", typeof(string))]
        public string taller { get; set; }

        
        [System.Xml.Serialization.XmlElement("codigo_articulo", typeof(string ))]
        public string codigo_articulo { get; set; }
        [System.Xml.Serialization.XmlElement("conteo_3", typeof(int))]
        public int conteo_3 { get; set; }

        [System.Xml.Serialization.XmlElement("costo_promedio", typeof(decimal))]
        public decimal costo_promedio { get; set; }

        [System.Xml.Serialization.XmlElement("articulo", typeof(string))]
        public string articulo { get; set; }

        [System.Xml.Serialization.XmlElement("almacen", typeof(string))]
        public string almacen { get; set; }

        [System.Xml.Serialization.XmlElement("diferencia", typeof(int))]
        public int diferencia { get; set; }

        [System.Xml.Serialization.XmlElement("existencia", typeof(int))]
        public int existencia { get; set; }

        [System.Xml.Serialization.XmlElement("anio", typeof(int))]
        public int anio { get; set; }

        [System.Xml.Serialization.XmlElement("mes", typeof(int))]
        public int mes { get; set; }

        [System.Xml.Serialization.XmlElement("folio", typeof(string))]
        public string folio { get; set; }

        [System.Xml.Serialization.XmlElement("termino", typeof(bool))]
        public bool termino { get; set; }
    }
}
