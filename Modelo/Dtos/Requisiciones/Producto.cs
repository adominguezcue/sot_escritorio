﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("productCollection")]
    public class ProductCollection {
        [System.Xml.Serialization.XmlArray("products")]
        [System.Xml.Serialization.XmlArrayItem("product", typeof(Product))]
        public Product[] product { get; set; }

    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("product")]
    public class Product
    {
        [System.Xml.Serialization.XmlElement("code")]
        public string codigo { get; set; }
        [System.Xml.Serialization.XmlElement("name")]
        public string nombre { get; set; }
        [System.Xml.Serialization.XmlElement("price")]
        public decimal precio { get; set; }
        [System.Xml.Serialization.XmlElement("costo")]
        public decimal costo { get; set; }
        [System.Xml.Serialization.XmlElement("uid")]
        public Guid? uid_web { get; set; }
        [System.Xml.Serialization.XmlElement("desactivar")]
        public bool? desactivar { get; set; }
        [System.Xml.Serialization.XmlElement("taller", typeof(string))]
        public string taller { get; set; }
        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }

    }
}
