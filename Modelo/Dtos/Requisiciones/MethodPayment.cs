﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("MethodPaymentCollection")]
    public class MethodPaymentCollection
    {
        [System.Xml.Serialization.XmlArray("method_payments")]
        [System.Xml.Serialization.XmlArrayItem("method_payment", typeof(MethodPayment))]
        public MethodPayment[] method_payment { get; set; }

    }

    [Serializable()]
    public class MethodPayment
    {
        [System.Xml.Serialization.XmlElement("id")]
        public int id { get; set; }
        [System.Xml.Serialization.XmlElement("description")]
        public string description { get; set;  }
    }
}
