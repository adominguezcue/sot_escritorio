﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    public class user_session
    {
         [System.Xml.Serialization.XmlElement("username")]
        public string username { get; set; }

         [System.Xml.Serialization.XmlElement("password")]
        public string password { get; set; }

        [System.Xml.Serialization.XmlElement("single_access_token")]
         public string single_access_token { get; set; }
    }

}
