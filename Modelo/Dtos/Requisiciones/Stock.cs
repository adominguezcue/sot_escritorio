﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 
namespace Modelo
{
    
        [Serializable()]
        [System.Xml.Serialization.XmlRoot("stockCollection")]
        public class stockCollection
        {
            [System.Xml.Serialization.XmlArray("stocks")]
            [System.Xml.Serialization.XmlArrayItem("stock", typeof(stock))]
            public stock[] stock { get; set; }

        }

        [Serializable()]
        public class stock
        {
            private Guid _uid = Guid.NewGuid();
            [System.Xml.Serialization.XmlElement("uid")]
            public Guid? uid { get { return _uid; } set{_uid = value.GetValueOrDefault();} }
            [System.Xml.Serialization.XmlElement("name")]
            public string name { get; set; }
            [System.Xml.Serialization.XmlElement("taller")]
            public string taller { get; set; }
            [System.Xml.Serialization.XmlElement("cod_alm")]
            public int cod_alm { get; set ; }

            //public bool ShouldSerializeid_web()
            //{
            //    return id_web.HasValue;
            //}
            [System.Xml.Serialization.XmlIgnore()]
            public bool actualizado { get; set; }

            [System.Xml.Serialization.XmlElement("centro_costos")]
            public string centro_costos { get; set; }
           
            
            //public Guid person_uid_sitio { get { return person.uid; } set { person.uid = value; } }
            //private Person _person = new Person();
            //[System.Xml.Serialization.XmlElement("person_attributes")]
            //public Person person { get { return _person; } set { _person = value; } }
            //[System.Xml.Serialization.XmlElement("credit_days")]
            //public int credit_days { get; set; }
        }
    
}
