﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("regionCollection")]
    public class RegionesCollection {
        [System.Xml.Serialization.XmlArray("regiones")]
        [System.Xml.Serialization.XmlArrayItem("region", typeof(region))]
        public region[] location { get; set; }

    }

    [Serializable()]
    public class region
    {
        [System.Xml.Serialization.XmlElement("UID")]
        public Guid? uid_web { get; set; }

        [System.Xml.Serialization.XmlElement("name")]
        public string name { get; set; }
        
        [System.Xml.Serialization.XmlIgnore()]
        public bool actualizado { get; set; }
        
    }
}
