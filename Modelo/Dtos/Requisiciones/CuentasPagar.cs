﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Modelo
{

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("cxpCollection")]
    public class cxpCollection
    {
        [System.Xml.Serialization.XmlArray("cxps")]
        [System.Xml.Serialization.XmlArrayItem("cxp", typeof(Country))]
        public CXP[] cxp { get; set; }

    }


    public class CXP
    {
        [System.Xml.Serialization.XmlElement("cod_cuenta_pago")]
        public int cod_cuenta_pago { get; set; }

        [System.Xml.Serialization.XmlElement("cod_oc")]
        public int cod_oc { get; set; }

        [System.Xml.Serialization.XmlElement("monto")]
        public decimal monto { get; set; }

        [System.Xml.Serialization.XmlElement("iva")]
        public decimal iva { get; set; }

        [System.Xml.Serialization.XmlElement("subtotal")]
        public decimal subtotal { get; set; }

        [System.Xml.Serialization.XmlElement("total")]
        public decimal total { get; set; }

        [System.Xml.Serialization.XmlElement("fecha_creacion")]
        public DateTime fecha_creacion { get; set; }

        [System.Xml.Serialization.XmlElement("fecha_vencido")]
        public DateTime fecha_vencido { get; set; }

        [System.Xml.Serialization.XmlElement("estado")]
        public string estado { get; set; }

        private Guid _uid = Guid.NewGuid();
        [System.Xml.Serialization.XmlElement("uid")]
        public Guid? uid { get { return _uid; } set{_uid = value.GetValueOrDefault();} }

        [System.Xml.Serialization.XmlElement("nom_prov")]
        public string nom_prov { get; set; }

        
    }
}
