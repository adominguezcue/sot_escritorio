﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
   public class DtoMantenimientoResumenReporte
    {
        public DateTime Fecha { get; set; }
        public string NombreUsuario { get; set; }

        public string Descripcion { get; set; }

        public string Concepto { get; set; }

        public string habitacion { get; set; }

    }
}
