﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoMedicionMantenimientoGestion
    {
        public int NumeroCorte { get; set; }

        public DateTime FechaRegistro { get; set; }

        public string DescripcionCatPresentacion { get; set; }

        public decimal Valorregistrado { get; set; }

        public string Presentacion {get;set;}

        public string NombreEmpleado { get; set; }
    }
}
