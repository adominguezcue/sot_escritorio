﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos.Reportes
{
    public class DtoReporte
    {
        public string ReportName { get; set; }

        public byte[] Data { get; set; }

        public int FileSize { get; set; }
    }
}
