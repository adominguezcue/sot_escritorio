﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoCorteTurno
    {
        public int Corte { get; set; }
        public CorteTurno.Estados EstadoCorte { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime? FechaCorte { get; set; }
        public string Recepcionista { get; set; }
        public decimal Habitaciones { get; set; }
        public decimal RoomService { get; set; }
        public decimal Restaurante { get; set; }
        public decimal Cortesias { get; set; }
        public decimal Gastos { get; set; }
        public decimal Total { get; set; }

        public int? IdUsuarioCerro { get; set; }
    }
}
