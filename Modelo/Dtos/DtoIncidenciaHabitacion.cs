﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoIncidenciaHabitacion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public string Zonas { get; set; }

        public DateTime FechaCreacion { get; set; }

        public string NombreEmpleado { get; set; }

        public string EtiquetaHabitacion { get; set; }

        public bool EstaCancelada { get; set; }
    }
}
