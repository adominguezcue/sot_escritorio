﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenAsistencia
    {
        public string Empleado { get; set; }

        public DateTime? Entrada { get; set; }

        public DateTime? Salida { get; set; }

        public DateTime? InicioComida { get; set; }

        public DateTime? FinComida { get; set; }

        public int IdEmpleado { get; set; }
    }
}
