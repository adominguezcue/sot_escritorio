﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoPagoRestauranteCorte 
    {
        [DisplayFormat(DataFormatString = "dd/MM/yyyy hh:mm:ss tt")]
        public DateTime Fecha { get; set; }
        
        public string Ticket { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Total
        {
            get
            {
                return Subtotal - Cortesias - Consumos - Descuento;
            }
        }
        
        public string Cajero { get; set; }
        
        public string Mesero { get; set; }
        
        [DisplayName("Forma de pago")]
        public TiposPago FormaPago { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Propina { get; set; }
        
        [DisplayName("Operación")]
        public string Operacion { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Subtotal { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Cortesías")]
        public decimal Cortesias { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Consumos internos")]
        public decimal Consumos { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Descuento { get; set; }

        public int Id { get; set; }

        public string Referencia { get; set; }

        public string NumeroTarjeta { get; set; }

        public TiposTarjeta? TipoTarjeta { get; set; }

        public int? IdMesero { get; set; }
    }
}
