﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoEmpleadoLineaArticuloValor
    {
        public int IdEmpleado { get; set; }
        public string Nombre { get; set; }
        public int IdLineaArticulo { get; set; }
        public decimal ValorConIVA { get; set; }
    }
}
