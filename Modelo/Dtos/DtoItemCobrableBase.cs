﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoItemCobrableBase
    {
        public DtoItemCobrableBase() { }

        public string Nombre { get; set; }
        public decimal Total { get; set; }
    }
}
