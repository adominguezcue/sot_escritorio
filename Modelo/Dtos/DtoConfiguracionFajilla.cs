﻿using Dominio.Nucleo.Dtos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoConfiguracionFajilla
    {
        public decimal Valor { get; set; }

        public ObservableCollection<DtoValorGenerico<decimal>> DenominacionesNacionales { get; set; }

        public ObservableCollection<DtoConfiguracionMonedaExtranjera> MonedasExtranjeras { get; set; }

        public DtoConfiguracionFajilla()
        {
            DenominacionesNacionales = new ObservableCollection<DtoValorGenerico<decimal>>();
            MonedasExtranjeras = new ObservableCollection<DtoConfiguracionMonedaExtranjera>();
        }
    }
}
