﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoMoneda
    {
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public decimal Monto { get; set; }
        public decimal ValorCambio { get; set; }
        public decimal Total
        {
            get
            {
                return Monto * ValorCambio * (decimal)Cantidad;
            }
            //protected set 
            //{
            //    base.Total = value;
            //}
        }
    }
}
