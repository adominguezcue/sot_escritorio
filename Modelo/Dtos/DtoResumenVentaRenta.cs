﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenVentaRenta
    {
        public bool Cobrada { get; set; }
        public int Id { get; set; }

        public string Servicio { get; set; }

        public string NumeroHabitacion { get; set; }

        public DateTime Fecha { get; set; }

        public decimal Habitacion { get; set; }

        public decimal PersonasExtra { get; set; }

        public decimal Renovaciones { get; set; }

        public decimal HorasExtra { get; set; }

        public decimal Paquetes { get; set; }

        public decimal DescuentosPorPaquete { get; set; }
        /// <summary>
        /// Ya contiene el valor de los descuentos por paquetes
        /// </summary>
        public decimal Descuentos { get; set; }

        public decimal Cortesias { get; set; }

        public decimal Subtotal
        {
            get { return Habitacion + PersonasExtra + Renovaciones + HorasExtra + Paquetes; }
        }

        public decimal Total
        {
            get { return Subtotal - Descuentos - Cortesias; }
        }
    }
}
