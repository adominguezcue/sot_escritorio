﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Entidades;

namespace Modelo.Dtos
{
    public class DtoInformacionPago
    {
        public decimal Valor { get; set; }
        public TiposPago TipoPago { get; set; }
        public string Referencia { get; set; }
        public Entidades.TiposTarjeta? TipoTarjeta { get; set; }

        public string NumeroTarjeta { get; set; }
    }
}
