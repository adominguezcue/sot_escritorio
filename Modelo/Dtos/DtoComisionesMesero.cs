﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoComisionesMesero
    {
        public System.Data.DataTable DatosPropinasTarjetas { get; set; }

        public System.Data.DataTable DatosPagosArticulos { get; set; }

        public System.Data.DataTable DatosResumen { get; set; }
    }
}
