﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoSolicitudVacaciones
    {
        public string NombreEmpleado { get; set; }
        public string Area { get; set; }
        public string Turno { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public double Dias { get { return (FechaFin.Date.AddDays(1) - FechaInicio.Date).TotalDays; } }

        public int Id { get; set; }
    }
}
