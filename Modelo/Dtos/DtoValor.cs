﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoValor
    {
        public decimal ValorSinIVA { get; set; }
        public decimal ValorIVA { get; set; }
        public decimal ValorConIVA { get; set; }
    }
}
