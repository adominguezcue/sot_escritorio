﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Entidades;

namespace Modelo.Dtos
{
    public class DtoGrupoPago
    {
        public string Nombre { get; private set; }

        public DtoInformacionPago[] Pagos { get; private set; }

        public DtoGrupoPago(string nombre, params TiposPago[] tiposPagos) 
        {
            Nombre = nombre;

            Pagos = new DtoInformacionPago[tiposPagos.Distinct().Count()];

            int i = 0;

            foreach (var tipo in tiposPagos.Distinct())
            {
                Pagos[i++] = new DtoInformacionPago
                {
                    TipoPago = tipo
                };
            }
        }
    }
}
