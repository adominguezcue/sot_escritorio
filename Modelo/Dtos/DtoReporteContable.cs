﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoReporteContable
    {
        public DtoReporteContable() { }

        public string Folio { get; set; }
        public DateTime InicioReporte { get; set; }
        public DateTime FinReporte { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreNegocio { get; set; }
        public string Direccion { get; set; }
        public List<DtoItemCobrable> ResumenesHabitaciones { get; set; }

        //public decimal PrecioPersonasExtra { get; set; }

        //public decimal PrecioHospedajeExtra { get; set; }

        //public int CantidadPersonasExtra { get; set; }
        //public int CantidadHospedajeExtra { get; set; }

        //public decimal TotalPersonasExtra { get; set; }

        //public decimal TotalHospedajeExtra { get; set; }

        public List<DtoItemCobrable> PersonasExtra { get; set; }
        public List<DtoItemCobrable> HospedajeExtra { get; set; }
        public List<DtoItemCobrable> Paquetes { get; set; }
        public List<DtoItemCobrableBase> RoomService { get; set; }
        public List<DtoItemCobrableBase> Restaurante { get; set; }
        public List<DtoItemCobrable> Descuentos { get; set; }

        public List<DtoItemCobrable> ResumenesTaxis { get; set; }
        
        #region Venta total habitaciones

        public decimal VTH_Habitaciones { get; set; }
        public decimal VTH_PersonasExtra { get; set; }
        public decimal VTH_HospedajeExtra { get; set; }
        public decimal VTH_Paquetes { get; set; }
        public decimal VTH_NoReembolsables { get; set; }
        public decimal VTH_Cortesias { get; set; }
        public decimal VTH_ConsumosInternos { get; set; }
        public decimal VTH_Descuentos { get; set; }

        #endregion
        #region Venta total reservaciones

        public decimal VTRE_Reservaciones { get; set; }
        public decimal VTRE_Cortesias { get; set; }
        public decimal VTRE_ConsumosInternos { get; set; }
        public decimal VTRE_Descuentos { get; set; }

        #endregion
        #region Venta total room & rest

        public decimal VTRR_Restaurante { get; set; }
        public decimal VTRR_Cortesias { get; set; }
        public decimal VTRR_Consumos { get; set; }
        public decimal VTRR_Descuentos { get; set; }

        #endregion

        public decimal Habitaciones
        {
            get { return VTH_Habitaciones + VTH_PersonasExtra + VTH_HospedajeExtra +
                         VTH_Paquetes + VTH_NoReembolsables - VTH_Cortesias - VTH_ConsumosInternos - VTH_Descuentos;
            }
        }
        public decimal Reservaciones
        {
            get
            {
                return VTRE_Reservaciones - VTRE_Cortesias - VTRE_ConsumosInternos - VTRE_Descuentos;
            }
        }
        public decimal RoomYRest
        {
            get { return VTRR_Restaurante - VTRR_Cortesias - VTRR_Consumos - VTRR_Descuentos; }
        }
        public decimal Taxis { get { return ResumenesTaxis.Count > 0 ? ResumenesTaxis.Sum(m => m.PrecioUnidad * m.Cantidad) : 0; } }

        public decimal TarjetasV { get; set; }
        public decimal Propinas { get; set; }

        public decimal Efectivo { get; set; }
        public decimal Transferencias { get; set; }
        public List<DtoItemCobrableBase> FormasPago { get; set; }

        public decimal Total 
        {
            get { return Habitaciones + Reservaciones + RoomYRest + Taxis + Propinas; }
        }
    }
}
