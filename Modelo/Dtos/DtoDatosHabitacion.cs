﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoDatosHabitacion
    {
        [Description("Habitación")]
        public string Habitacion { get; set; }
        [Description("Habitación")]
        public decimal PrecioHabitacion { get; set; }
        public string Tipo { get; set; }
        [Description("Tarjeta V")]
        public string TarjetaV { get; set; }
        [Description("Fecha de entrada")]
        public DateTime? FechaEntrada { get; set; }
        [Description("Fecha de salida")]
        public DateTime? FechaSalida { get; set; }
        [Description("Matrícula")]
        public string Matricula { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        [Description("Línea")]
        public string Linea { get; set; }
        public string Color { get; set; }
        [Description("Servicio No")]
        public string NumeroServicio { get; set; }
        [Description("Personas extra")]
        public decimal PersonasExtra { get; set; }
        [Description("Hospedaje extra")]
        public decimal HospedajeExtra { get; set; }
        [Description("Cortesia Hab")]
        public decimal CortesiaHabitacion { get; set; }
        [Description("Paquetes")]
        public decimal Paquetes { get; set; }
        [Description("Ser. Restaurante")]
        public decimal ServicioRestaurante { get; set; }
        [Description("Reservacion")]
        public string Reservacion { get; set; }
        public TareaLimpieza.TiposLimpieza TipoLimpieza { get; set; }
        public Habitacion.EstadosHabitacion Estado { get; set; }

        public bool PoseeComanda { get; set; }

        public int Id { get; set; }
    }
}
