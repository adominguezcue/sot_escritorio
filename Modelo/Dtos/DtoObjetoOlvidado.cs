﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoObjetoOlvidado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public string Descripcion { get; set; }

        public bool EsObsoleto { get; set; }

        public DateTime FechaCreacion { get; set; }

        public string NombreEmpleado { get; set; }

        public string EtiquetaHabitacion { get; set; }

        public bool EsCancelado { get; set; }
    }
}
