﻿using Dominio.Nucleo.Dtos;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Modelo.Dtos
{
    public class DtoConfiguracionMonedaExtranjera
    {
        public string Nombre { get; set; }
        public string Abreviatura { get; set; }
        public decimal ValorCambio { get; set; }

        public ObservableCollection<DtoValorGenerico<decimal>> Denominaciones { get; set; }

        public DtoConfiguracionMonedaExtranjera()
        {
            Denominaciones = new ObservableCollection<DtoValorGenerico<decimal>>();
        }
    }
}