﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;

namespace Modelo.Dtos
{
    public class DtoDetallesTarea
    {
        public string EstadoStr { get; set; }

        public TareaLimpieza.Estados Estado { get; set; }

        public string Detalle { get; set; }

        public bool EsVencida { get; set; }

        public TimeSpan Tiempo { get; set; }

        public string NumeroHabitacion { get; set; }
    }
}
