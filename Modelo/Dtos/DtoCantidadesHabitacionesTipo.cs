﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoCantidadesHabitacionesTipo
    {
        public string TipoHabitacion { get; set; }

        public int CantidadLibre { get; set; }

        public int CantidadPendienteCobro { get; set; }

        public int CantidadPreparada { get; set; }

        public int CantidadReservada { get; set; }

        public int CantidadPreparadaReservada { get; set; }

        public int CantidadBloqueada { get; set; }

        public int CantidadOcupada { get; set; }

        public int CantidadSucia { get; set; }

        public int CantidadLimpieza { get; set; }

        public int CantidadSupervision { get; set; }

        public int CantidadMantenimiento { get; set; }
    }
}
