﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtTotalComandasxRenta
    {
        public int Idrenta { get; set; }

        public int  Totalcomandas { get; set; }

        public decimal  TotalPrecioComandas { get; set; }
    }
}
