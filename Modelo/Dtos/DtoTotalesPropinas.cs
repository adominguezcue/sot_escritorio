﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoTotalesPropinas
    {
        public decimal TotalPositivo { get; set; }

        public decimal TotalNegativo { get; set; }
    }

}
