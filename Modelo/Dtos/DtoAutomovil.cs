﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoAutomovil
    {
        private string _matricula;
        public string Matricula 
        {
            get { return _matricula; }
            set 
            {
                if (value != null)
                    _matricula = value.Trim().ToUpper();
                else
                    _matricula = value;
            }
        }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Color { get; set; }
    }
}
