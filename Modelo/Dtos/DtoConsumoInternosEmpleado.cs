﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoConsumoInternosEmpleado
    {
        public string codigoArticulo { get; set; }

        public string Articulodescripcion {get;set;}

        public int cantidad { get; set; }

        public decimal total { get; set; }

        public string NumeroEmpleado { get; set; }
    }
}
