﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoHabitacionesxMatricula
    {
        public int Idrenta { get; set; }
        public int Habitacion { get; set; }

        public DateTime fechaRegistro { get; set; }

        public decimal PrecioHabitacion { get; set; }
        public decimal PrecioPersonaExtra { get; set; }
        public decimal PrecioNochesExtra { get; set; }
        public decimal PrecioHorasExtras { get; set; }
        public decimal PrecioPaquetes { get; set; }
        public decimal PrecioCortesias { get; set; }
        public decimal PrecioDescuentos { get; set; }
        public bool EsReservacion { get; set; }

    }
}
