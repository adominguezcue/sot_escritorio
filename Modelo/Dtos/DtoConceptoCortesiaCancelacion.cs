﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoConceptoCortesiaCancelacion
    {
        public string Concepto { get; set; }
    }
}
