﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoMedicionesMantenimiento
    {
        public int CorteActual { get; set; }

        public string ValorLectura { get; set; }

        public Modelo.Entidades.CatPresentacionMantenimiento Presentacion {get; set;}

        public string PresentacionUnidad { get; set; }

        public string PresentacionDescripsion { get; set; }

        public decimal ValorAnterior { get; set; }
    }
}
