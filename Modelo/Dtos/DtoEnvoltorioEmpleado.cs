﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoEnvoltorioEmpleado
    {
        public int IdEntidadPadre { get; set; }
        public string NombreEmpleado { get; set; }
        public int? IdEmpleado { get; set; }
    }
}
