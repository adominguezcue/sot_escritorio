﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenCancelaciones
    {
        public int IdClasificacionVenta { get; set; }
        public decimal Total { get; set; }
        public int Cantidad { get; set; }

        public Venta.ClasificacionesVenta ClasificacionVenta
        {
            get { return (Venta.ClasificacionesVenta)IdClasificacionVenta; }
            set { IdClasificacionVenta = (int)value; }
        }
    }
}
