﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoResumenComandaExtendido
    {
        public int Id { get; set; }

        public DateTime FechaInicio { get; set; }

        public string Destino { get; set; }

        public int Orden { get; set; }

        public List<DtoResumenArticuloComanda> ArticulosComanda { get; set; }

        public Entidades.Comanda.Estados Estado { get; set; }
    }
}
