﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Dtos
{
    public class DtoArticuloTicket : DtoItemCobrable
    {
        public int? IdLineaBase { get; set; }
        public bool EsCortesia { get; set; }
        public string Codigo { get; set; }
    }
}
