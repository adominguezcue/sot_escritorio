//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Empleado))]
    [KnownType(typeof(Habitacion))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class IncidenciaHabitacion: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "IncidenciasHabitacion";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<IncidenciaHabitacion, bool>> Comparador()
        //{
            //return m => m.Id == Id;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(EmpleadoReporta != null)
                propiedades.Add(EmpleadoReporta);
            if(Habitacion != null)
                propiedades.Add(Habitacion);
    
            if(EmpleadoReporta != null)
                EmpleadoReporta.ObtenerPropiedadesNavegacion(propiedades);
            if(Habitacion != null)
                Habitacion.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(EmpleadoReporta != null && !propiedadesT.Contains(EmpleadoReporta))
                propiedades.Add(EmpleadoReporta);
            if(Habitacion != null && !propiedadesT.Contains(Habitacion))
                propiedades.Add(Habitacion);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public IncidenciaHabitacion()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public IncidenciaHabitacion ObtenerCopiaDePrimitivas()
        {
            return new IncidenciaHabitacion
            {
                Activa = this.Activa,
                Cancelada = this.Cancelada,
                IdHabitacion = this.IdHabitacion,
                IdEmpleadoReporta = this.IdEmpleadoReporta,
                Nombre = this.Nombre,
                Descripcion = this.Descripcion,
                Zonas = this.Zonas,
                FechaCreacion = this.FechaCreacion,
                FechaModificacion = this.FechaModificacion,
                FechaEliminacion = this.FechaEliminacion,
                FechaCancelacion = this.FechaCancelacion,
                IdUsuarioCreo = this.IdUsuarioCreo,
                IdUsuarioModifico = this.IdUsuarioModifico,
                IdUsuarioElimino = this.IdUsuarioElimino,
                IdUsuarioCancelo = this.IdUsuarioCancelo,
            };
        }
    
        public void SincronizarPrimitivas(IncidenciaHabitacion origen)
        {
            this.Activa = origen.Activa;
            this.Cancelada = origen.Cancelada;
            this.IdHabitacion = origen.IdHabitacion;
            this.IdEmpleadoReporta = origen.IdEmpleadoReporta;
            this.Nombre = origen.Nombre;
            this.Descripcion = origen.Descripcion;
            this.Zonas = origen.Zonas;
            this.FechaCreacion = origen.FechaCreacion;
            this.FechaModificacion = origen.FechaModificacion;
            this.FechaEliminacion = origen.FechaEliminacion;
            this.FechaCancelacion = origen.FechaCancelacion;
            this.IdUsuarioCreo = origen.IdUsuarioCreo;
            this.IdUsuarioModifico = origen.IdUsuarioModifico;
            this.IdUsuarioElimino = origen.IdUsuarioElimino;
            this.IdUsuarioCancelo = origen.IdUsuarioCancelo;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }
        private int _id;
        [DataMember]
        public bool Activa
        {
            get { return _activa; }
            set
            {
                if (_activa != value)
                {
                    _activa = value;
                    OnPropertyChanged("Activa");
                }
            }
        }
        private bool _activa;
        [DataMember]
        public bool Cancelada
        {
            get { return _cancelada; }
            set
            {
                if (_cancelada != value)
                {
                    _cancelada = value;
                    OnPropertyChanged("Cancelada");
                }
            }
        }
        private bool _cancelada;
        [DataMember]
        public int IdHabitacion
        {
            get { return _idHabitacion; }
            set
            {
                if (_idHabitacion != value)
                {
                    if (!IsDeserializing)
                    {
                        if (Habitacion != null && Habitacion.Id != value)
                        {
                            Habitacion = null;
                        }
                    }
                    _idHabitacion = value;
                    OnPropertyChanged("IdHabitacion");
                }
            }
        }
        private int _idHabitacion;
        [DataMember]
        public int IdEmpleadoReporta
        {
            get { return _idEmpleadoReporta; }
            set
            {
                if (_idEmpleadoReporta != value)
                {
                    if (!IsDeserializing)
                    {
                        if (EmpleadoReporta != null && EmpleadoReporta.Id != value)
                        {
                            EmpleadoReporta = null;
                        }
                    }
                    _idEmpleadoReporta = value;
                    OnPropertyChanged("IdEmpleadoReporta");
                }
            }
        }
        private int _idEmpleadoReporta;
        [DataMember]
        public string Nombre
        {
            get { return _nombre; }
            set
            {
                if ((value == null && _nombre != value) || (value != null && _nombre != value.Trim()))
                {
                    _nombre = (value == null ? value : value.Trim());
                    OnPropertyChanged("Nombre");
                }
            }
        }
        private string _nombre;
        [DataMember]
        public string Descripcion
        {
            get { return _descripcion; }
            set
            {
                if ((value == null && _descripcion != value) || (value != null && _descripcion != value.Trim()))
                {
                    _descripcion = (value == null ? value : value.Trim());
                    OnPropertyChanged("Descripcion");
                }
            }
        }
        private string _descripcion;
        [DataMember]
        public string Zonas
        {
            get { return _zonas; }
            set
            {
                if ((value == null && _zonas != value) || (value != null && _zonas != value.Trim()))
                {
                    _zonas = (value == null ? value : value.Trim());
                    OnPropertyChanged("Zonas");
                }
            }
        }
        private string _zonas;
        [DataMember]
        public System.DateTime FechaCreacion
        {
            get { return _fechaCreacion; }
            set
            {
                if (_fechaCreacion != value)
                {
                    _fechaCreacion = value;
                    OnPropertyChanged("FechaCreacion");
                }
            }
        }
        private System.DateTime _fechaCreacion;
        [DataMember]
        public System.DateTime FechaModificacion
        {
            get { return _fechaModificacion; }
            set
            {
                if (_fechaModificacion != value)
                {
                    _fechaModificacion = value;
                    OnPropertyChanged("FechaModificacion");
                }
            }
        }
        private System.DateTime _fechaModificacion;
        [DataMember]
        public Nullable<System.DateTime> FechaEliminacion
        {
            get { return _fechaEliminacion; }
            set
            {
                if (_fechaEliminacion != value)
                {
                    _fechaEliminacion = value;
                    OnPropertyChanged("FechaEliminacion");
                }
            }
        }
        private Nullable<System.DateTime> _fechaEliminacion;
        [DataMember]
        public Nullable<System.DateTime> FechaCancelacion
        {
            get { return _fechaCancelacion; }
            set
            {
                if (_fechaCancelacion != value)
                {
                    _fechaCancelacion = value;
                    OnPropertyChanged("FechaCancelacion");
                }
            }
        }
        private Nullable<System.DateTime> _fechaCancelacion;
        [DataMember]
        public int IdUsuarioCreo
        {
            get { return _idUsuarioCreo; }
            set
            {
                if (_idUsuarioCreo != value)
                {
                    _idUsuarioCreo = value;
                    OnPropertyChanged("IdUsuarioCreo");
                }
            }
        }
        private int _idUsuarioCreo;
        [DataMember]
        public int IdUsuarioModifico
        {
            get { return _idUsuarioModifico; }
            set
            {
                if (_idUsuarioModifico != value)
                {
                    _idUsuarioModifico = value;
                    OnPropertyChanged("IdUsuarioModifico");
                }
            }
        }
        private int _idUsuarioModifico;
        [DataMember]
        public Nullable<int> IdUsuarioElimino
        {
            get { return _idUsuarioElimino; }
            set
            {
                if (_idUsuarioElimino != value)
                {
                    _idUsuarioElimino = value;
                    OnPropertyChanged("IdUsuarioElimino");
                }
            }
        }
        private Nullable<int> _idUsuarioElimino;
        [DataMember]
        public Nullable<int> IdUsuarioCancelo
        {
            get { return _idUsuarioCancelo; }
            set
            {
                if (_idUsuarioCancelo != value)
                {
                    _idUsuarioCancelo = value;
                    OnPropertyChanged("IdUsuarioCancelo");
                }
            }
        }
        private Nullable<int> _idUsuarioCancelo;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public Empleado EmpleadoReporta
        {
            get { return _empleadoReporta; }
            set
            {
                if (!ReferenceEquals(_empleadoReporta, value))
                {
                    var previousValue = _empleadoReporta;
                    _empleadoReporta = value;
                    FixupEmpleadoReporta(previousValue);
                    OnNavigationPropertyChanged("EmpleadoReporta");
                }
            }
        }
        private Empleado _empleadoReporta;
    
        [DataMember]
        public Habitacion Habitacion
        {
            get { return _habitacion; }
            set
            {
                if (!ReferenceEquals(_habitacion, value))
                {
                    var previousValue = _habitacion;
                    _habitacion = value;
                    FixupHabitacion(previousValue);
                    OnNavigationPropertyChanged("Habitacion");
                }
            }
        }
        private Habitacion _habitacion;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            EmpleadoReporta = null;
            Habitacion = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupEmpleadoReporta(Empleado previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.IncidenciasHabitacionReportadas.Contains(this))
            {
                previousValue.IncidenciasHabitacionReportadas.Remove(this);
            }
    
            if (EmpleadoReporta != null)
            {
                if (!EmpleadoReporta.IncidenciasHabitacionReportadas.Contains(this))
                {
                    EmpleadoReporta.IncidenciasHabitacionReportadas.Add(this);
                }
    
                IdEmpleadoReporta = EmpleadoReporta.Id;
            }
        }
    
        private void FixupHabitacion(Habitacion previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.Incidencias.Contains(this))
            {
                previousValue.Incidencias.Remove(this);
            }
    
            if (Habitacion != null)
            {
                if (!Habitacion.Incidencias.Contains(this))
                {
                    Habitacion.Incidencias.Add(this);
                }
    
                IdHabitacion = Habitacion.Id;
            }
        }

        #endregion

    }
}
