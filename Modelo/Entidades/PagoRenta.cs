//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(VentaRenta))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class PagoRenta: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "PagosRenta";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<PagoRenta, bool>> Comparador()
        //{
            //return m => m.Id == Id;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(VentaRenta != null)
                propiedades.Add(VentaRenta);
    
            if(VentaRenta != null)
                VentaRenta.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(VentaRenta != null && !propiedadesT.Contains(VentaRenta))
                propiedades.Add(VentaRenta);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public PagoRenta()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public PagoRenta ObtenerCopiaDePrimitivas()
        {
            return new PagoRenta
            {
                Valor = this.Valor,
                IdVentaRenta = this.IdVentaRenta,
                Referencia = this.Referencia,
                NumeroTarjeta = this.NumeroTarjeta,
                IdTipoPago = this.IdTipoPago,
                Activo = this.Activo,
                Transaccion = this.Transaccion,
                IdTipoTarjeta = this.IdTipoTarjeta,
                FechaCreacion = this.FechaCreacion,
                FechaModificacion = this.FechaModificacion,
                FechaEliminacion = this.FechaEliminacion,
                IdUsuarioModifico = this.IdUsuarioModifico,
                IdUsuarioCreo = this.IdUsuarioCreo,
                IdUsuarioElimino = this.IdUsuarioElimino,
            };
        }
    
        public void SincronizarPrimitivas(PagoRenta origen)
        {
            this.Valor = origen.Valor;
            this.IdVentaRenta = origen.IdVentaRenta;
            this.Referencia = origen.Referencia;
            this.NumeroTarjeta = origen.NumeroTarjeta;
            this.IdTipoPago = origen.IdTipoPago;
            this.Activo = origen.Activo;
            this.Transaccion = origen.Transaccion;
            this.IdTipoTarjeta = origen.IdTipoTarjeta;
            this.FechaCreacion = origen.FechaCreacion;
            this.FechaModificacion = origen.FechaModificacion;
            this.FechaEliminacion = origen.FechaEliminacion;
            this.IdUsuarioModifico = origen.IdUsuarioModifico;
            this.IdUsuarioCreo = origen.IdUsuarioCreo;
            this.IdUsuarioElimino = origen.IdUsuarioElimino;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }
        private int _id;
        [DataMember]
        public decimal Valor
        {
            get { return _valor; }
            set
            {
                if (_valor != value)
                {
                    _valor = value;
                    OnPropertyChanged("Valor");
                }
            }
        }
        private decimal _valor;
        [DataMember]
        public int IdVentaRenta
        {
            get { return _idVentaRenta; }
            set
            {
                if (_idVentaRenta != value)
                {
                    if (!IsDeserializing)
                    {
                        if (VentaRenta != null && VentaRenta.Id != value)
                        {
                            VentaRenta = null;
                        }
                    }
                    _idVentaRenta = value;
                    OnPropertyChanged("IdVentaRenta");
                }
            }
        }
        private int _idVentaRenta;
        [DataMember]
        public string Referencia
        {
            get { return _referencia; }
            set
            {
                if ((value == null && _referencia != value) || (value != null && _referencia != value.Trim()))
                {
                    _referencia = (value == null ? value : value.Trim());
                    OnPropertyChanged("Referencia");
                }
            }
        }
        private string _referencia;
        [DataMember]
        public string NumeroTarjeta
        {
            get { return _numeroTarjeta; }
            set
            {
                if ((value == null && _numeroTarjeta != value) || (value != null && _numeroTarjeta != value.Trim()))
                {
                    _numeroTarjeta = (value == null ? value : value.Trim());
                    OnPropertyChanged("NumeroTarjeta");
                }
            }
        }
        private string _numeroTarjeta;
        [DataMember]
        public int IdTipoPago
        {
            get { return _idTipoPago; }
            set
            {
                if (_idTipoPago != value)
                {
                    _idTipoPago = value;
                    OnPropertyChanged("IdTipoPago");
                }
            }
        }
        private int _idTipoPago;
        [DataMember]
        public bool Activo
        {
            get { return _activo; }
            set
            {
                if (_activo != value)
                {
                    _activo = value;
                    OnPropertyChanged("Activo");
                }
            }
        }
        private bool _activo;
        [DataMember]
        public string Transaccion
        {
            get { return _transaccion; }
            set
            {
                if ((value == null && _transaccion != value) || (value != null && _transaccion != value.Trim()))
                {
                    _transaccion = (value == null ? value : value.Trim());
                    OnPropertyChanged("Transaccion");
                }
            }
        }
        private string _transaccion;
        [DataMember]
        public Nullable<int> IdTipoTarjeta
        {
            get { return _idTipoTarjeta; }
            set
            {
                if (_idTipoTarjeta != value)
                {
                    _idTipoTarjeta = value;
                    OnPropertyChanged("IdTipoTarjeta");
                }
            }
        }
        private Nullable<int> _idTipoTarjeta;
        [DataMember]
        public System.DateTime FechaCreacion
        {
            get { return _fechaCreacion; }
            set
            {
                if (_fechaCreacion != value)
                {
                    _fechaCreacion = value;
                    OnPropertyChanged("FechaCreacion");
                }
            }
        }
        private System.DateTime _fechaCreacion;
        [DataMember]
        public System.DateTime FechaModificacion
        {
            get { return _fechaModificacion; }
            set
            {
                if (_fechaModificacion != value)
                {
                    _fechaModificacion = value;
                    OnPropertyChanged("FechaModificacion");
                }
            }
        }
        private System.DateTime _fechaModificacion;
        [DataMember]
        public Nullable<System.DateTime> FechaEliminacion
        {
            get { return _fechaEliminacion; }
            set
            {
                if (_fechaEliminacion != value)
                {
                    _fechaEliminacion = value;
                    OnPropertyChanged("FechaEliminacion");
                }
            }
        }
        private Nullable<System.DateTime> _fechaEliminacion;
        [DataMember]
        public int IdUsuarioModifico
        {
            get { return _idUsuarioModifico; }
            set
            {
                if (_idUsuarioModifico != value)
                {
                    _idUsuarioModifico = value;
                    OnPropertyChanged("IdUsuarioModifico");
                }
            }
        }
        private int _idUsuarioModifico;
        [DataMember]
        public int IdUsuarioCreo
        {
            get { return _idUsuarioCreo; }
            set
            {
                if (_idUsuarioCreo != value)
                {
                    _idUsuarioCreo = value;
                    OnPropertyChanged("IdUsuarioCreo");
                }
            }
        }
        private int _idUsuarioCreo;
        [DataMember]
        public Nullable<int> IdUsuarioElimino
        {
            get { return _idUsuarioElimino; }
            set
            {
                if (_idUsuarioElimino != value)
                {
                    _idUsuarioElimino = value;
                    OnPropertyChanged("IdUsuarioElimino");
                }
            }
        }
        private Nullable<int> _idUsuarioElimino;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public VentaRenta VentaRenta
        {
            get { return _ventaRenta; }
            set
            {
                if (!ReferenceEquals(_ventaRenta, value))
                {
                    var previousValue = _ventaRenta;
                    _ventaRenta = value;
                    FixupVentaRenta(previousValue);
                    OnNavigationPropertyChanged("VentaRenta");
                }
            }
        }
        private VentaRenta _ventaRenta;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            VentaRenta = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupVentaRenta(VentaRenta previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.Pagos.Contains(this))
            {
                previousValue.Pagos.Remove(this);
            }
    
            if (VentaRenta != null)
            {
                if (!VentaRenta.Pagos.Contains(this))
                {
                    VentaRenta.Pagos.Add(this);
                }
    
                IdVentaRenta = VentaRenta.Id;
            }
        }

        #endregion

    }
}
