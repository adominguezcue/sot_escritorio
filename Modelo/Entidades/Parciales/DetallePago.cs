﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class DetallePago
    {
        public ConceptosPago ConceptoPago 
        {
            get { return (ConceptosPago)IdConceptoPago; }
            set { IdConceptoPago = (int)value; }
        }

        public enum ConceptosPago 
        { 
            [Description("Habitación")]
            Habitacion = 1,
            [Description("Renovación")]
            Renovacion = 2,
            [Description("Horas extra")]
            HorasExtra = 3,
            [Description("Personas extra")]
            PersonasExtra = 4,
            [Description("Paquetes")]
            Paquetes = 5,
            [Description("Descuentos")]
            Descuentos = 6,
            [Description("Ajuste de reservación")]
            AjusteReservacion = 7
        }
    }
}
