﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Conversores;

namespace Modelo.Entidades
{
    public partial class OrdenRestaurante
    {
        //public int IdMesero { get; set; }

        public Modelo.Entidades.Comanda.Estados Estado
        {
            get { return (Modelo.Entidades.Comanda.Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }
        /*
        [TypeConverter(typeof(EnumDescriptionTypeConverter))]
        public enum Estados
        {
            [Description("Preparación")]
            Preparacion = 1,
            [Description("Por entregar")]
            PorEntregar = 2,
            [Description("Entregada al mesero")]
            PorCobrar = 3,
            [Description("Entregada al cliente")]
            PorPagar = 4,
            Cobrada = 5,
            Cancelada = 6
        }*/

        public string NumeroMesaTmp { get; set; }
    }
}
