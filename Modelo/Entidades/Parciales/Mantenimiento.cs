﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Mantenimiento
    {
        public string ConceptoStrTmp { get; set; }

        public Estados Estado 
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public TareaMantenimiento.TiposMantenimiento TipoMantenimiento
        {
            get { return (TareaMantenimiento.TiposMantenimiento)IdTipoMantenimiento; }
            set { IdTipoMantenimiento = (int)value; }
        }

        public string NombreUsuario { get; set; }

        public enum Estados
        {
            [Description("En proceso")]
            EnProceso = 1,
            [Description("Finalizado")]
            Finalizado = 2,
            [Description("Cancelado")]
            Cancelado = 3
        }
    }
}
