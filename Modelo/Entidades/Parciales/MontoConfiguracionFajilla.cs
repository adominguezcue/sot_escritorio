﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class MontoConfiguracionFajilla
    {
        public decimal MontoNacional
        {
            get
            {
                if (IdMonedaExtranjera.HasValue && MonedaExtranjera != null)
                    return Monto * MonedaExtranjera.ValorCambio;
                else if (!IdMonedaExtranjera.HasValue)
                    return Monto;

                return 0;
            }
        }
    }
}
