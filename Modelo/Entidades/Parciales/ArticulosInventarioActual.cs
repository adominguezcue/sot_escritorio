﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades.Parciales
{
    public class ArticulosInventarioActual
    {
        public string CodigoArticulo { get; set; }
        public string Des_Articulo { get; set; }
        public int CodigoAlmacen { get; set; }
        public string Des_Almacen { get; set; }

        public int CodigoLigea { get; set; }
        public string Des_Linea { get; set; }

        public int CodigoDepto {get;set;}
        public string Des_Depto { get; set;}

        public int Existencia { get; set; }
            
        public decimal CostoUnitario { get; set; }
        public decimal CostoPromedio { get; set; }
    }
}
