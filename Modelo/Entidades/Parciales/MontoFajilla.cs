﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class MontoFajilla
    {
        public decimal Total
        {
            get
            {
                if (MontoConfiguracionFajilla != null && (!MontoConfiguracionFajilla.IdMonedaExtranjera.HasValue || MontoConfiguracionFajilla.MonedaExtranjera != null))
                    return MontoConfiguracionFajilla.Monto * Cantidad * (MontoConfiguracionFajilla.IdMonedaExtranjera.HasValue ? MontoConfiguracionFajilla.MonedaExtranjera.ValorCambio : 1);

                

                return 0;
            }
        }

        //public decimal MontoValor
        //{
        //    get { return MontoTmp * ValorCambioTmp; }
        //}

        public decimal MontoTmp { get; set; }

        public string MonedaExtranjeraTmp { get; set; }

        public decimal ValorCambioTmp { get; set; }

        public int? IdMonedaExtranjeraTmp { get; set; }
    }
}
