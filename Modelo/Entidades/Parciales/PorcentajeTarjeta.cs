﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class PorcentajeTarjeta
    {
        public int PorcentajeEntero
        {
            get { return (int)(Porcentaje * 100); }
            set { Porcentaje = (decimal)value / 100; }
        }

        public TiposTarjeta TipoTarjeta
        {
            get { return (TiposTarjeta)IdTipoTarjeta; }
            set { IdTipoTarjeta = (int)value; }
        }
    }
}
