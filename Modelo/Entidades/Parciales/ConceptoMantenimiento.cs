﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class ConceptoMantenimiento
    {
        public Clasificaciones Clasificacion 
        {
            get { return (Clasificaciones)IdClasificacion; }
            set { IdClasificacion = (int)value; }
        }

        [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
        public enum Clasificaciones 
        {
            Aparatos = 1,
            [Description("Baños")]
            Banios = 2,
            [Description("Habitación")]
            Habitacion = 3,
        }
    }
}
