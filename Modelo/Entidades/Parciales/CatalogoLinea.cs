﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades.Parciales
{
    public class CatalogoLinea
    {
        public int codigolinea { get; set; }

        public int codigodepto { get; set; }
        public string descripcionlinea { get; set; }

    }
}
