﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Empleado
    {

        public string NombreCompleto 
        {
            get { return string.Join(" ", Nombre, ApellidoPaterno, ApellidoMaterno); }
        }

        public int CantidadTareas { get; set; }

        public string NumeroHabitacionActual { get; set; }

        public bool Ocupado { get; set; }

        public string NombrePuesto { get; set; }
        public string NombreAreaTmp { get; set; }
    }
}
