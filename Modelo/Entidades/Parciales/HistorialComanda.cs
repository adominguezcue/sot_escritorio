﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Conversores;

namespace Modelo.Entidades
{
    public partial class HistorialComanda
    {
        public Modelo.Entidades.Comanda.Estados Estado 
        {
            get { return (Modelo.Entidades.Comanda.Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }
    }
}
