﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Mesa
    {
        public EstadosMesa Estado
        {
            get { return (EstadosMesa)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public enum EstadosMesa
        {
            [Description("En creación")]
            EnCreacion = 0,
            Libre = 1,
            Ocupada = 2,
            Cobrar = 3
        }
    }
}
