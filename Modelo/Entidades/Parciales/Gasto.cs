﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Gasto
    {
        public string ConceptoTmp { get; set; }

        public string CentroCostosTmp { get; set; }

        public ClasificacionesGastos? Clasificacion
        {
            get { return IdClasificacion.HasValue ? (ClasificacionesGastos?)IdClasificacion : null; }
            set { IdClasificacion = (int?)value; }
        }
    }
}
