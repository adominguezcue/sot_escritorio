﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class ConceptoGasto
    {
        public string NombreCentroCostosTmp { get; set; }

        public decimal PresupuestoRestanteTmp { get; set; }
    }
}
