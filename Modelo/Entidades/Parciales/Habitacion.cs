﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Modelo.Entidades
{
    public partial class Habitacion
    {
        public string NombreTipo
        {
            get { return TipoHabitacion != null ? TipoHabitacion.Descripcion : ""; }
        }

        public string NombreMasTipo
        {
            get { return TipoHabitacion != null ? string.Join(" - ", NumeroHabitacion, TipoHabitacion.Descripcion) : NumeroHabitacion; }
        }

        [Description("Estado")]
        public EstadosHabitacion EstadoHabitacion
        {
            get { return (EstadosHabitacion)IdEstadoHabitacion; }
            set { IdEstadoHabitacion = (int)value; }
        }

        [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
        public enum EstadosHabitacion
        {
            [Description("En creación")]
            EnCreacion = 0,
            Libre = 1,
            [Description("Por cobrar")]
            PendienteCobro = 2,
            Preparada = 3,
            Reservada = 4,
            [Description("Preparada y reservada")]
            PreparadaReservada = 5,
            Bloqueada = 6,
            Ocupada = 7,
            Sucia = 8,
            Limpieza = 9,
            //Ocupada2
            [Description("Supervisión")]
            Supervision = 10,

            Mantenimiento = 11,
            //Comanda = 10,
            //[Description("Con V Points")]
            //ConVPoint = 9
        }
    }
}
