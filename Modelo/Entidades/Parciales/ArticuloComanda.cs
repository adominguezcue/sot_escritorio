﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class ArticuloComanda
    {
        public Estados Estado
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public enum Estados
        {
            EnProceso = 1,
            PorEntregar = 2,
            Entregado = 3,
            Cancelado = 4,
        }

        public Modelo.Almacen.Entidades.Articulo ArticuloTmp { get; set; }
    }
}
