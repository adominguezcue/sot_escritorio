﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class ConfiguracionTarifa
    {
        public Tarifas Tarifa 
        {
            get { return (Tarifas)IdTarifa; }
            set { IdTarifa = (int)value; }
        }

        public enum Tarifas 
        { 
            Hotel = 1,
            Motel = 2
        }
    }
}
