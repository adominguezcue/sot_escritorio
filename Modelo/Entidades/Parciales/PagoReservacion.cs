﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class PagoReservacion : IPago
    {
        public TiposPago TipoPago 
        {
            get { return (TiposPago)IdTipoPago; }
            set { IdTipoPago = (int)value; }
        }

        public TiposTarjeta? TipoTarjeta
        {
            get 
            {
                if (IdTipoTarjeta.HasValue)
                    return (TiposTarjeta)IdTipoTarjeta;
                return null;
            }
            set 
            {
                if (value.HasValue)
                    IdTipoTarjeta = (int)value;
                else
                    IdTipoTarjeta = null;
            }
        }
    }
}
