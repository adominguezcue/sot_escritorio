﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class TareaMantenimiento
    {
        public string ConceptoStrTmp { get; set; }

        public TiposMantenimiento TipoMantenimiento
        {
            get { return (TiposMantenimiento)IdTipoMantenimiento; }
            set { IdTipoMantenimiento = (int)value; }
        }

        public Periodicidades? Periodicidad 
        {
            get { return IdPeriodicidad.HasValue ? (Periodicidades?)IdPeriodicidad.Value : null; }
            set { IdPeriodicidad = value.HasValue ? (int?)value : null; }
        }

        public string NombreUsuario { get; set; }

        public enum TiposMantenimiento 
        { 
            Preventivo = 1,
            Correctivo = 2
        }
    }
}
