﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class SolicitudFalta
    {
        public FormasPago TipoPago 
        {
            get { return (FormasPago)IdFormaPago; }
            set { IdFormaPago = (int)value; }
        }

        [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
        public enum FormasPago 
        {
            //“Sueldo por nómina” o “Pagos
            [Description("Sueldo por nómina")]
            SueldoNomina = 1,
            Pagos = 2
        }

        public string AreaTmp { get; set; }
    }
}
