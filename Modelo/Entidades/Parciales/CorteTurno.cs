﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Conversores;

namespace Modelo.Entidades
{
    public partial class CorteTurno
    {
        public Estados Estado
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        [TypeConverter(typeof(EnumDescriptionTypeConverter))]
        public enum Estados 
        { 
            Abierto = 1,
            [Description("En revisión")]
            EnRevision = 2,
            Cerrado = 3
        }
    }
}
