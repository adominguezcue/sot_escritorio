﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Modelo.Entidades
{
    public partial class VW_VentaCorrecta
    {
        public Modelo.Entidades.Venta.ClasificacionesVenta ClasificacionVenta 
        {
            get { return (Modelo.Entidades.Venta.ClasificacionesVenta)IdClasificacionVenta; }
            set { IdClasificacionVenta = (int)value; }
        }

        public string Ticket { get { return SerieTicket + FolioTicket.ToString(); } }

        public string[] ArrayPagos 
        {
            get 
            {
                if (ClasificacionVenta == Venta.ClasificacionesVenta.Taxi)
                    return new string[] { TiposPago.Efectivo.Descripcion() };

                try
                {
                    return Pagos.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(m => ((TiposPago)int.Parse(m)).Descripcion()).ToArray();
                }
                catch 
                {
                    return new string[] { "Error al leer la información de los pagos" };
                }
            }
        }
    }
}
