﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Propina
    {
        public TiposTarjeta TipoTarjeta
        {
            get { return (TiposTarjeta)IdTipoTarjeta; }
            set { IdTipoTarjeta = (int)value; }
        }

        public TiposPropina TipoPropina
        {
            get { return (TiposPropina)IdTipoPropina; }
            set { IdTipoPropina = (int)value; }
        }

        public enum TiposPropina
        {
            Comanda = 1,//Room service
            Restaurante = 2,
            Valet = 3,//Habitación
        }
    }
}
