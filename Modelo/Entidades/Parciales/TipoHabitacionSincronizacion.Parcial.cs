﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class TipoHabitacionSincronizacion
    {
        public TiposMovimientos TipoMovimiento 
        {
            get { return (TiposMovimientos)IdMovimiento; }
            set { IdMovimiento = (int)value; }
        }

        public enum TiposMovimientos 
        {
            Creacion = 1,
            Modificacion = 2,
            Eliminacion = 3
        }
    }
}
