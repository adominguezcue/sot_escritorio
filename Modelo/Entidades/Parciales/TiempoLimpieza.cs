﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class TiempoLimpieza
    {
        [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
        public TareaLimpieza.TiposLimpieza TipoLimpieza 
        {
            get { return (TareaLimpieza.TiposLimpieza)IdTipoLimpieza; }
            set { IdTipoLimpieza = (int)value; }
        }
    }
}
