﻿
namespace Modelo.Entidades.Parciales
{
    public class CatalogoAlmacen
    {
        public int idCodigo { get; set; }

        public string DescripcionAlmacen { get; set; }
    }
}
