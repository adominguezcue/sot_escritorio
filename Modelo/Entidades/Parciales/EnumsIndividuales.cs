﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public enum AreasPago
    {
        Habitacion = 1,
        RoomService = 2,
        Restaurante = 3,
        TarjetasV = 4,
        Taxis = 5
    }

    public enum TiposTarjeta
    {
        Visa = 1,
        MasterCard = 2,
        [Description("American Express")]
        AmericanExpress = 3,
    }


    public enum Turnos
    {
        Matutino = 1,
        Vespertino = 2,
        Nocturno = 3
    }

    public enum Periodicidades 
    { 
        Diariamente = 1,
        Semanalmente = 2,
        Mensualmente = 3,
        Anualmente = 4
    }
}
