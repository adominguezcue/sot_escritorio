﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Modelo.Entidades
{
    public partial class Reservacion
    {
        public string NombreTipo { get; set; }

        public string NombreEstado { get { return Estado.Descripcion(); } set { } }

        public EstadosReservacion Estado
        {
            get { return (EstadosReservacion)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public decimal ValorPagosActualesTMP { get; set; }
        public decimal ValorNoReembolsableTMP { get; set; }

        public enum EstadosReservacion
        {
            Pendiente = 1,
            Confirmada = 2,
            Consumida = 3,
            Cancelada = 4
        }

        //public TiposPago TipoPago 
        //{
        //    get { return (TiposPago)IdTipoPago; }
        //    set { IdTipoPago = (int)value; }
        //}

        //public int CantidadPaquetes { get; set; }

        //public int CantidadDescuentos { get; set; }
    }
}
