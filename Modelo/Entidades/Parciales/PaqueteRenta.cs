﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class PaqueteRenta
    {
        public string NombrePaquete { get; set; }

        public decimal ValorFinal { get { return Precio - Descuento; } }
    }
}
