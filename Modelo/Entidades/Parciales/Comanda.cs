﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Conversores;

namespace Modelo.Entidades
{
    public partial class Comanda
    {
        public Estados Estado 
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        [TypeConverter(typeof(EnumDescriptionTypeConverter))]
        public enum Estados
        {
            [Description("Preparación")]
            Preparacion = 1,
            [Description("Por entregar")]
            PorEntregar = 2,
            [Description("Por cobrar")]
            PorCobrar = 3,
            [Description("Por pagar")]
            PorPagar = 4,
            Cobrada = 5,
            Cancelada = 6
        }

        [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
        public enum Tipos
        {
            [Description("Room service")]
            RoomService = 1,
            Restaurante = 2,
            [Description("Consumo interno")]
            ConsumoInterno = 3
        };

        public string NumeroHabitacionTmp { get; set; }
    }
}
