﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class FondoDia
    {
        public string NombreDia 
        {
            get 
            {
                switch (Dia)
                {
                    case 1:
                        return "Lunes";
                    case 2:
                        return "Martes";
                    case 3:
                        return "Miércoles";
                    case 4:
                        return "Jueves";
                    case 5:
                        return "Viernes";
                    case 6:
                        return "Sábado";
                    case 7:
                        return "Domingo";
                    default:
                        return "";
                }
            }
        }
    }
}
