﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Conversores;

namespace Modelo.Entidades
{
    public partial class ConceptoSistema
    {
        public TiposConceptos TipoConcepto 
        {
            get { return (TiposConceptos)IdTipoConcepto; }
            set { IdTipoConcepto = (int)value; }
        }

        [TypeConverter(typeof(EnumDescriptionTypeConverter))]
        public enum TiposConceptos 
        {
            [Description("Consumos y cortesías")]
            Cortesia = 1,
            //[Description("Concepto de gasto")]
            //Gasto = 2,
            [Description("Cancelación de habitación")]
            CancelacionHabitacion = 2,
            [Description("Liberación de habitación")]
            LiberacionHabitacion = 3,
            [Description("Cancelación de venta")]
            CancelacionVenta = 4
        }

        /*
         
         var conceptoGastos = new TipoConcepto
                    {
                        Activo = true,
                        Nombre = "",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                    };

                    _repositorioTiposConceptos.Agregar(conceptoGastos);

                    var conceptoCancelacionH = new TipoConcepto
                    {
                        Activo = true,
                        Nombre = "",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                    };

                    _repositorioTiposConceptos.Agregar(conceptoCancelacionH);

                    var conceptoCancelacionV = new TipoConcepto
                    {
                        Activo = true,
                        Nombre = "",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                    };

                    _repositorioTiposConceptos.Agregar(conceptoCancelacionV);

                    var conceptoConsumos = new TipoConcepto
                    {
                        Activo = true,
                        Nombre = "",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                    };
         
         
         */
    }
}
