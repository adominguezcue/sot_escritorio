﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Fajilla
    {

        public string UsuarioAutorizo { get; set; }

        //public Estados Estado
        //{
        //    get { return (Estados)IdEstado; }
        //    set { IdEstado = (int)value; }
        //}

        //public enum Estados
        //{
        //    Creada = 1,
        //    Autorizada = 2,
        //}

        public ObservableCollection<Dtos.DtoMonedaExtranjera> MonedasExtranjeras { get; set; }
    }
}
