﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class ProgramaPaquete
    {
        public bool EsMensual { get { return Mes.HasValue; } }
    }
}
