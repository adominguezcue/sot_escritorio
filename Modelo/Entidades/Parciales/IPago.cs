﻿using Dominio.Nucleo.Entidades;
using System;
namespace Modelo.Entidades
{
    public interface IPago
    {
        bool Activo { get; set; }
        int Id { get; set; }
        int IdTipoPago { get; set; }
        int? IdTipoTarjeta { get; set; }
        string NumeroTarjeta { get; set; }
        string Referencia { get; set; }
        TiposPago TipoPago { get; set; }
        TiposTarjeta? TipoTarjeta { get; set; }
        string Transaccion { get; set; }
        decimal Valor { get; set; }
        DateTime FechaCreacion { get; set; }
    }
}
