﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class ConfiguracionTipo
    {
        public Entidades.ConfiguracionTarifa.Tarifas TarifaTmp { get; set; }

        public decimal PrecioConIVATmp { get; set; }

        public decimal PrecioHospedajeExtraConIVATmp { get; set; }

        public decimal PrecioPersonaExtraConIVATmp { get; set; }
    }
}
