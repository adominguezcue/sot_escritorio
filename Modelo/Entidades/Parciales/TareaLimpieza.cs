﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class TareaLimpieza
    {
        public Estados Estado 
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public TiposLimpieza Tipo 
        {
            get { return (TiposLimpieza)IdTipoLimpieza; }
            set { IdTipoLimpieza = (int)value; }
        }
        public enum TiposLimpieza 
        {
            Detallado = 1,
            Normal = 2,
            MinutosVenta = 3
        }

        [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
        public enum Estados
        { 
            Creada = 1,
            [Description("En proceso")]
            EnProceso = 2,
            [Description("En supervisión")]
            EnSupervision = 3,
            Finalizada = 4,
            Cancelada = 5
        }
    }
}
