﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Preventa
    {
        public Venta.ClasificacionesVenta ClasificacionVenta
        {
            get { return (Venta.ClasificacionesVenta)IdClasificacionVenta; }
            set { IdClasificacionVenta = (int)value; }
        }

        public string Ticket { get { return SerieTicket + FolioTicket.ToString(); } }
    }
}
