﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Venta
    {
        public ClasificacionesVenta ClasificacionVenta 
        {
            get { return (ClasificacionesVenta)IdClasificacionVenta; }
            set { IdClasificacionVenta = (int)value; }
        }

        public string Ticket { get { return SerieTicket + FolioTicket.ToString(); } }

        [DataContract]
        public enum ClasificacionesVenta 
        { 
            [EnumMember]
            Habitacion = 1,
            [EnumMember]
            RoomService = 2,
            [EnumMember]
            Restaurante = 3,
            [EnumMember]
            TarjetaPuntos = 4,
            [EnumMember]
            Taxi = 5,
            [EnumMember]
            Reservacion = 6
        }

        public int FolioCorteTmp { get; set; }
    }
}
