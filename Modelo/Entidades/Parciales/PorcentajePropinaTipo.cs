﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class PorcentajePropinaTipo
    {
        public int PorcentajeEntero
        {
            get { return (int)(Porcentaje * 100); }
            set { Porcentaje = (decimal)value / 100; }
        }

        public Almacen.Entidades.Linea LineaTmp { get; set; }
    }
}
