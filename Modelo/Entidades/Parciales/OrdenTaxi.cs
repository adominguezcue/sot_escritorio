﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class OrdenTaxi
    {
        public int Orden { get; set; }

        public Estados Estado
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public enum Estados
        {
            Pendiente = 1,
            Cobrada = 2,
            Cancelada = 3
        }
    }
}
