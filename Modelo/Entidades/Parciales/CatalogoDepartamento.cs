﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades.Parciales
{
    public class CatalogoDepartamento
    {
        public int codigoDepto { get; set; }
        public string descripciondepto { get; set; }
    }
}
