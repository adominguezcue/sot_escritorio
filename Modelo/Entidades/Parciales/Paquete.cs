﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Paquete
    {
        //public decimal PrecioPositivo 
        //{
        //    get { return Precio > 0 ? Precio : 0; }
        //}

        //public decimal PrecioNegativo
        //{
        //    get { return Precio < 0 ? -Precio : 0; }
        //}

        public string NombreTipo 
        {
            get { return TipoHabitacion != null ? TipoHabitacion.Descripcion : ""; }
        }


        bool _seleccionableTmp;
        public bool SeleccionableTmp
        {
            get { return _seleccionableTmp; }
            set
            {
                if (_seleccionableTmp != value)
                {
                    _seleccionableTmp = value;
                    OnPropertyChanged("SeleccionableTmp");
                }
            }
        }

        public decimal PrecioConIVATmp { get; set; }

        public decimal DescuentoConIVATmp { get; set; }
    }
}
