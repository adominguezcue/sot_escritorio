﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class TipoArticulo
    {
        public AreasPreparacion? AreaPreparacion
        {
            get { return IdAreaPreparacion.HasValue ? (AreasPreparacion)IdAreaPreparacion.Value : default(AreasPreparacion?); }
            set { IdAreaPreparacion = value.HasValue ? (int)value.Value : default(int); }
        }

        public enum AreasPreparacion
        {
            Cocina = 1,
            Bar = 2
        }
    }
}
