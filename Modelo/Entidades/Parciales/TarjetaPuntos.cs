﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class TarjetaPuntos
    {
        public EstadosTarjeta EstadoTarjeta 
        {
            get { return (EstadosTarjeta)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public enum EstadosTarjeta 
        { 
            Creada = 1,
            Vendida = 2,
            Invalida = 3,
            /// <summary>
            /// Estado agregado para contemplar la posibilidad de que una tarjeta se pueda dar de alta en web si recibir un cobro directo en el hotel
            /// </summary>
            Alta = 4
            //Eliminada = 4
        }
    }
}
