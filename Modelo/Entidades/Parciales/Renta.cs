﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class Renta
    {
        public Estados Estado
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public enum Estados 
        { 
            PendienteCobro = 1,
            EnCurso = 2,
            Finalizada = 3,
            Cancelada = 4
        }

        public ConfiguracionTarifa.Tarifas TarifaTmp { get; set; }
    }
}
