﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class VentaRenta
    {
        public decimal PrecioHotelTmp { get; set; }

        public string ClaveTipoHabitacionTmp { get; set; }

        public string ClaveSATTipoHabitacionTmp { get; set; }

        public string TipoHabitacionTmp { get; set; }
    }
}
