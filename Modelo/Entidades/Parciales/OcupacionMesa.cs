﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades
{
    public partial class OcupacionMesa
    {
        public Estados Estado 
        {
            get { return (Estados)IdEstado; }
            set { IdEstado = (int)value; }
        }

        public enum Estados 
        { 
            EnProceso = 1,
            Cobrada = 2,
            Cancelada = 3
        }
    }
}
