﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Entidades.Parciales
{
    public class DtoConfiguracionImpresoras
    {
        public string ImpresoraTickets { get; set; }

        public string ImpresoraCocina { get; set; }

        public string ImpresoraBar { get; set; }

        public string ImpresoraCortes { get; set; }

        public string ImpresoraVendedores { get; set; }
    }
}
