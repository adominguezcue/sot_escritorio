//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Habitacion))]
    [KnownType(typeof(ConceptoMantenimiento))]
    [KnownType(typeof(Mantenimiento))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class TareaMantenimiento: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "TareasMantenimiento";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<TareaMantenimiento, bool>> Comparador()
        //{
            //return m => m.Id == Id;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(Habitacion != null)
                propiedades.Add(Habitacion);
            if(ConceptoMantenimiento != null)
                propiedades.Add(ConceptoMantenimiento);
            foreach(var item in Mantenimientos)
                if(item != null)
                    propiedades.Add(item);
    
            if(Habitacion != null)
                Habitacion.ObtenerPropiedadesNavegacion(propiedades);
            if(ConceptoMantenimiento != null)
                ConceptoMantenimiento.ObtenerPropiedadesNavegacion(propiedades);
            foreach(var item in Mantenimientos)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(Habitacion != null && !propiedadesT.Contains(Habitacion))
                propiedades.Add(Habitacion);
            if(ConceptoMantenimiento != null && !propiedadesT.Contains(ConceptoMantenimiento))
                propiedades.Add(ConceptoMantenimiento);
            foreach(var item in Mantenimientos)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public TareaMantenimiento()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public TareaMantenimiento ObtenerCopiaDePrimitivas()
        {
            return new TareaMantenimiento
            {
                Descripcion = this.Descripcion,
                FechaCreacion = this.FechaCreacion,
                FechaModificacion = this.FechaModificacion,
                FechaEliminacion = this.FechaEliminacion,
                IdUsuarioModifico = this.IdUsuarioModifico,
                FechaInicio = this.FechaInicio,
                FechaSiguiente = this.FechaSiguiente,
                FechaFin = this.FechaFin,
                IdUsuarioCreo = this.IdUsuarioCreo,
                IdUsuarioElimino = this.IdUsuarioElimino,
                Activa = this.Activa,
                IdHabitacion = this.IdHabitacion,
                IdConceptoMantenimiento = this.IdConceptoMantenimiento,
                IdTipoMantenimiento = this.IdTipoMantenimiento,
                IdPeriodicidad = this.IdPeriodicidad,
                DiasAnticipacion = this.DiasAnticipacion,
                Lapso = this.Lapso,
            };
        }
    
        public void SincronizarPrimitivas(TareaMantenimiento origen)
        {
            this.Descripcion = origen.Descripcion;
            this.FechaCreacion = origen.FechaCreacion;
            this.FechaModificacion = origen.FechaModificacion;
            this.FechaEliminacion = origen.FechaEliminacion;
            this.IdUsuarioModifico = origen.IdUsuarioModifico;
            this.FechaInicio = origen.FechaInicio;
            this.FechaSiguiente = origen.FechaSiguiente;
            this.FechaFin = origen.FechaFin;
            this.IdUsuarioCreo = origen.IdUsuarioCreo;
            this.IdUsuarioElimino = origen.IdUsuarioElimino;
            this.Activa = origen.Activa;
            this.IdHabitacion = origen.IdHabitacion;
            this.IdConceptoMantenimiento = origen.IdConceptoMantenimiento;
            this.IdTipoMantenimiento = origen.IdTipoMantenimiento;
            this.IdPeriodicidad = origen.IdPeriodicidad;
            this.DiasAnticipacion = origen.DiasAnticipacion;
            this.Lapso = origen.Lapso;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }
        private int _id;
        [DataMember]
        public string Descripcion
        {
            get { return _descripcion; }
            set
            {
                if ((value == null && _descripcion != value) || (value != null && _descripcion != value.Trim()))
                {
                    _descripcion = (value == null ? value : value.Trim());
                    OnPropertyChanged("Descripcion");
                }
            }
        }
        private string _descripcion;
        [DataMember]
        public System.DateTime FechaCreacion
        {
            get { return _fechaCreacion; }
            set
            {
                if (_fechaCreacion != value)
                {
                    _fechaCreacion = value;
                    OnPropertyChanged("FechaCreacion");
                }
            }
        }
        private System.DateTime _fechaCreacion;
        [DataMember]
        public System.DateTime FechaModificacion
        {
            get { return _fechaModificacion; }
            set
            {
                if (_fechaModificacion != value)
                {
                    _fechaModificacion = value;
                    OnPropertyChanged("FechaModificacion");
                }
            }
        }
        private System.DateTime _fechaModificacion;
        [DataMember]
        public Nullable<System.DateTime> FechaEliminacion
        {
            get { return _fechaEliminacion; }
            set
            {
                if (_fechaEliminacion != value)
                {
                    _fechaEliminacion = value;
                    OnPropertyChanged("FechaEliminacion");
                }
            }
        }
        private Nullable<System.DateTime> _fechaEliminacion;
        [DataMember]
        public int IdUsuarioModifico
        {
            get { return _idUsuarioModifico; }
            set
            {
                if (_idUsuarioModifico != value)
                {
                    _idUsuarioModifico = value;
                    OnPropertyChanged("IdUsuarioModifico");
                }
            }
        }
        private int _idUsuarioModifico;
        [DataMember]
        public System.DateTime FechaInicio
        {
            get { return _fechaInicio; }
            set
            {
                if (_fechaInicio != value)
                {
                    _fechaInicio = value;
                    OnPropertyChanged("FechaInicio");
                }
            }
        }
        private System.DateTime _fechaInicio;
        [DataMember]
        public System.DateTime FechaSiguiente
        {
            get { return _fechaSiguiente; }
            set
            {
                if (_fechaSiguiente != value)
                {
                    _fechaSiguiente = value;
                    OnPropertyChanged("FechaSiguiente");
                }
            }
        }
        private System.DateTime _fechaSiguiente;
        [DataMember]
        public Nullable<System.DateTime> FechaFin
        {
            get { return _fechaFin; }
            set
            {
                if (_fechaFin != value)
                {
                    _fechaFin = value;
                    OnPropertyChanged("FechaFin");
                }
            }
        }
        private Nullable<System.DateTime> _fechaFin;
        [DataMember]
        public int IdUsuarioCreo
        {
            get { return _idUsuarioCreo; }
            set
            {
                if (_idUsuarioCreo != value)
                {
                    _idUsuarioCreo = value;
                    OnPropertyChanged("IdUsuarioCreo");
                }
            }
        }
        private int _idUsuarioCreo;
        [DataMember]
        public Nullable<int> IdUsuarioElimino
        {
            get { return _idUsuarioElimino; }
            set
            {
                if (_idUsuarioElimino != value)
                {
                    _idUsuarioElimino = value;
                    OnPropertyChanged("IdUsuarioElimino");
                }
            }
        }
        private Nullable<int> _idUsuarioElimino;
        [DataMember]
        public bool Activa
        {
            get { return _activa; }
            set
            {
                if (_activa != value)
                {
                    _activa = value;
                    OnPropertyChanged("Activa");
                }
            }
        }
        private bool _activa;
        [DataMember]
        public int IdHabitacion
        {
            get { return _idHabitacion; }
            set
            {
                if (_idHabitacion != value)
                {
                    if (!IsDeserializing)
                    {
                        if (Habitacion != null && Habitacion.Id != value)
                        {
                            Habitacion = null;
                        }
                    }
                    _idHabitacion = value;
                    OnPropertyChanged("IdHabitacion");
                }
            }
        }
        private int _idHabitacion;
        [DataMember]
        public int IdConceptoMantenimiento
        {
            get { return _idConceptoMantenimiento; }
            set
            {
                if (_idConceptoMantenimiento != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ConceptoMantenimiento != null && ConceptoMantenimiento.Id != value)
                        {
                            ConceptoMantenimiento = null;
                        }
                    }
                    _idConceptoMantenimiento = value;
                    OnPropertyChanged("IdConceptoMantenimiento");
                }
            }
        }
        private int _idConceptoMantenimiento;
        [DataMember]
        public int IdTipoMantenimiento
        {
            get { return _idTipoMantenimiento; }
            set
            {
                if (_idTipoMantenimiento != value)
                {
                    _idTipoMantenimiento = value;
                    OnPropertyChanged("IdTipoMantenimiento");
                }
            }
        }
        private int _idTipoMantenimiento;
        [DataMember]
        public Nullable<int> IdPeriodicidad
        {
            get { return _idPeriodicidad; }
            set
            {
                if (_idPeriodicidad != value)
                {
                    _idPeriodicidad = value;
                    OnPropertyChanged("IdPeriodicidad");
                }
            }
        }
        private Nullable<int> _idPeriodicidad;
        [DataMember]
        public int DiasAnticipacion
        {
            get { return _diasAnticipacion; }
            set
            {
                if (_diasAnticipacion != value)
                {
                    _diasAnticipacion = value;
                    OnPropertyChanged("DiasAnticipacion");
                }
            }
        }
        private int _diasAnticipacion;
        [DataMember]
        public int Lapso
        {
            get { return _lapso; }
            set
            {
                if (_lapso != value)
                {
                    _lapso = value;
                    OnPropertyChanged("Lapso");
                }
            }
        }
        private int _lapso;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public Habitacion Habitacion
        {
            get { return _habitacion; }
            set
            {
                if (!ReferenceEquals(_habitacion, value))
                {
                    var previousValue = _habitacion;
                    _habitacion = value;
                    FixupHabitacion(previousValue);
                    OnNavigationPropertyChanged("Habitacion");
                }
            }
        }
        private Habitacion _habitacion;
    
        [DataMember]
        public ConceptoMantenimiento ConceptoMantenimiento
        {
            get { return _conceptoMantenimiento; }
            set
            {
                if (!ReferenceEquals(_conceptoMantenimiento, value))
                {
                    var previousValue = _conceptoMantenimiento;
                    _conceptoMantenimiento = value;
                    FixupConceptoMantenimiento(previousValue);
                    OnNavigationPropertyChanged("ConceptoMantenimiento");
                }
            }
        }
        private ConceptoMantenimiento _conceptoMantenimiento;
    
        [DataMember]
        public SotCollection<Mantenimiento> Mantenimientos
        {
            get
            {
                if (_mantenimientos == null)
                {
                    _mantenimientos = new SotCollection<Mantenimiento>();
                    _mantenimientos.CollectionChanged += FixupMantenimientos;
                }
                return _mantenimientos;
            }
            set
            {
                if (!ReferenceEquals(_mantenimientos, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_mantenimientos != null)
                    {
                        _mantenimientos.CollectionChanged -= FixupMantenimientos;
                    }
                    _mantenimientos = value;
                    if (_mantenimientos != null)
                    {
                        _mantenimientos.CollectionChanged += FixupMantenimientos;
                    }
                    OnNavigationPropertyChanged("Mantenimientos");
                }
            }
        }
        private SotCollection<Mantenimiento> _mantenimientos;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            Habitacion = null;
            ConceptoMantenimiento = null;
            Mantenimientos.Clear();
        }

        #endregion

        #region Association Fixup
    
        private void FixupHabitacion(Habitacion previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.TareasMantenimiento.Contains(this))
            {
                previousValue.TareasMantenimiento.Remove(this);
            }
    
            if (Habitacion != null)
            {
                if (!Habitacion.TareasMantenimiento.Contains(this))
                {
                    Habitacion.TareasMantenimiento.Add(this);
                }
    
                IdHabitacion = Habitacion.Id;
            }
        }
    
        private void FixupConceptoMantenimiento(ConceptoMantenimiento previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.TareasMantenimiento.Contains(this))
            {
                previousValue.TareasMantenimiento.Remove(this);
            }
    
            if (ConceptoMantenimiento != null)
            {
                if (!ConceptoMantenimiento.TareasMantenimiento.Contains(this))
                {
                    ConceptoMantenimiento.TareasMantenimiento.Add(this);
                }
    
                IdConceptoMantenimiento = ConceptoMantenimiento.Id;
            }
        }
    
        private void FixupMantenimientos(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (Mantenimiento item in e.NewItems)
                {
                    item.TareaMantenimiento = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Mantenimiento item in e.OldItems)
                {
                    if (ReferenceEquals(item.TareaMantenimiento, this))
                    {
                        item.TareaMantenimiento = null;
                    }
                }
            }
        }

        #endregion

    }
}
