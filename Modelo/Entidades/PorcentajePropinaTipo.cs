//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(ConfiguracionPropinas))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class PorcentajePropinaTipo: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "PorcentajesPropinaTipo";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<PorcentajePropinaTipo, bool>> Comparador()
        //{
            //return m => m.Id == Id;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(ConfiguracionPropinas != null)
                propiedades.Add(ConfiguracionPropinas);
    
            if(ConfiguracionPropinas != null)
                ConfiguracionPropinas.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(ConfiguracionPropinas != null && !propiedadesT.Contains(ConfiguracionPropinas))
                propiedades.Add(ConfiguracionPropinas);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public PorcentajePropinaTipo()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public PorcentajePropinaTipo ObtenerCopiaDePrimitivas()
        {
            return new PorcentajePropinaTipo
            {
                IdConfiguracionPropina = this.IdConfiguracionPropina,
                IdLineaArticulo = this.IdLineaArticulo,
                Porcentaje = this.Porcentaje,
                Activo = this.Activo,
            };
        }
    
        public void SincronizarPrimitivas(PorcentajePropinaTipo origen)
        {
            this.IdConfiguracionPropina = origen.IdConfiguracionPropina;
            this.IdLineaArticulo = origen.IdLineaArticulo;
            this.Porcentaje = origen.Porcentaje;
            this.Activo = origen.Activo;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }
        private int _id;
        [DataMember]
        public int IdConfiguracionPropina
        {
            get { return _idConfiguracionPropina; }
            set
            {
                if (_idConfiguracionPropina != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ConfiguracionPropinas != null && ConfiguracionPropinas.Id != value)
                        {
                            ConfiguracionPropinas = null;
                        }
                    }
                    _idConfiguracionPropina = value;
                    OnPropertyChanged("IdConfiguracionPropina");
                }
            }
        }
        private int _idConfiguracionPropina;
        [DataMember]
        public int IdLineaArticulo
        {
            get { return _idLineaArticulo; }
            set
            {
                if (_idLineaArticulo != value)
                {
                    _idLineaArticulo = value;
                    OnPropertyChanged("IdLineaArticulo");
                }
            }
        }
        private int _idLineaArticulo;
        [DataMember]
        public decimal Porcentaje
        {
            get { return _porcentaje; }
            set
            {
                if (_porcentaje != value)
                {
                    _porcentaje = value;
                    OnPropertyChanged("Porcentaje");
                }
            }
        }
        private decimal _porcentaje;
        [DataMember]
        public bool Activo
        {
            get { return _activo; }
            set
            {
                if (_activo != value)
                {
                    _activo = value;
                    OnPropertyChanged("Activo");
                }
            }
        }
        private bool _activo;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public ConfiguracionPropinas ConfiguracionPropinas
        {
            get { return _configuracionPropinas; }
            set
            {
                if (!ReferenceEquals(_configuracionPropinas, value))
                {
                    var previousValue = _configuracionPropinas;
                    _configuracionPropinas = value;
                    FixupConfiguracionPropinas(previousValue);
                    OnNavigationPropertyChanged("ConfiguracionPropinas");
                }
            }
        }
        private ConfiguracionPropinas _configuracionPropinas;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ConfiguracionPropinas = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupConfiguracionPropinas(ConfiguracionPropinas previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.PorcentajesPropinaTipo.Contains(this))
            {
                previousValue.PorcentajesPropinaTipo.Remove(this);
            }
    
            if (ConfiguracionPropinas != null)
            {
                if (!ConfiguracionPropinas.PorcentajesPropinaTipo.Contains(this))
                {
                    ConfiguracionPropinas.PorcentajesPropinaTipo.Add(this);
                }
    
                IdConfiguracionPropina = ConfiguracionPropinas.Id;
            }
        }

        #endregion

    }
}
