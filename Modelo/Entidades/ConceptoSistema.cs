//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(TareaLimpieza))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class ConceptoSistema: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ConceptosSistema";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<ConceptoSistema, bool>> Comparador()
        //{
            //return m => m.Id == Id;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            foreach(var item in TareasLimpieza)
                if(item != null)
                    propiedades.Add(item);
    
            foreach(var item in TareasLimpieza)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            foreach(var item in TareasLimpieza)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public ConceptoSistema()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public ConceptoSistema ObtenerCopiaDePrimitivas()
        {
            return new ConceptoSistema
            {
                Concepto = this.Concepto,
                IdTipoConcepto = this.IdTipoConcepto,
                Activo = this.Activo,
                FechaCreacion = this.FechaCreacion,
                FechaModificacion = this.FechaModificacion,
                FechaEliminacion = this.FechaEliminacion,
                IdUsuarioModifico = this.IdUsuarioModifico,
                IdUsuarioCreo = this.IdUsuarioCreo,
                IdUsuarioElimino = this.IdUsuarioElimino,
            };
        }
    
        public void SincronizarPrimitivas(ConceptoSistema origen)
        {
            this.Concepto = origen.Concepto;
            this.IdTipoConcepto = origen.IdTipoConcepto;
            this.Activo = origen.Activo;
            this.FechaCreacion = origen.FechaCreacion;
            this.FechaModificacion = origen.FechaModificacion;
            this.FechaEliminacion = origen.FechaEliminacion;
            this.IdUsuarioModifico = origen.IdUsuarioModifico;
            this.IdUsuarioCreo = origen.IdUsuarioCreo;
            this.IdUsuarioElimino = origen.IdUsuarioElimino;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }
        private int _id;
        [DataMember]
        public string Concepto
        {
            get { return _concepto; }
            set
            {
                if ((value == null && _concepto != value) || (value != null && _concepto != value.Trim()))
                {
                    _concepto = (value == null ? value : value.Trim());
                    OnPropertyChanged("Concepto");
                }
            }
        }
        private string _concepto;
        [DataMember]
        public int IdTipoConcepto
        {
            get { return _idTipoConcepto; }
            set
            {
                if (_idTipoConcepto != value)
                {
                    _idTipoConcepto = value;
                    OnPropertyChanged("IdTipoConcepto");
                }
            }
        }
        private int _idTipoConcepto;
        [DataMember]
        public bool Activo
        {
            get { return _activo; }
            set
            {
                if (_activo != value)
                {
                    _activo = value;
                    OnPropertyChanged("Activo");
                }
            }
        }
        private bool _activo;
        [DataMember]
        public System.DateTime FechaCreacion
        {
            get { return _fechaCreacion; }
            set
            {
                if (_fechaCreacion != value)
                {
                    _fechaCreacion = value;
                    OnPropertyChanged("FechaCreacion");
                }
            }
        }
        private System.DateTime _fechaCreacion;
        [DataMember]
        public System.DateTime FechaModificacion
        {
            get { return _fechaModificacion; }
            set
            {
                if (_fechaModificacion != value)
                {
                    _fechaModificacion = value;
                    OnPropertyChanged("FechaModificacion");
                }
            }
        }
        private System.DateTime _fechaModificacion;
        [DataMember]
        public Nullable<System.DateTime> FechaEliminacion
        {
            get { return _fechaEliminacion; }
            set
            {
                if (_fechaEliminacion != value)
                {
                    _fechaEliminacion = value;
                    OnPropertyChanged("FechaEliminacion");
                }
            }
        }
        private Nullable<System.DateTime> _fechaEliminacion;
        [DataMember]
        public int IdUsuarioModifico
        {
            get { return _idUsuarioModifico; }
            set
            {
                if (_idUsuarioModifico != value)
                {
                    _idUsuarioModifico = value;
                    OnPropertyChanged("IdUsuarioModifico");
                }
            }
        }
        private int _idUsuarioModifico;
        [DataMember]
        public int IdUsuarioCreo
        {
            get { return _idUsuarioCreo; }
            set
            {
                if (_idUsuarioCreo != value)
                {
                    _idUsuarioCreo = value;
                    OnPropertyChanged("IdUsuarioCreo");
                }
            }
        }
        private int _idUsuarioCreo;
        [DataMember]
        public Nullable<int> IdUsuarioElimino
        {
            get { return _idUsuarioElimino; }
            set
            {
                if (_idUsuarioElimino != value)
                {
                    _idUsuarioElimino = value;
                    OnPropertyChanged("IdUsuarioElimino");
                }
            }
        }
        private Nullable<int> _idUsuarioElimino;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public SotCollection<TareaLimpieza> TareasLimpieza
        {
            get
            {
                if (_tareasLimpieza == null)
                {
                    _tareasLimpieza = new SotCollection<TareaLimpieza>();
                    _tareasLimpieza.CollectionChanged += FixupTareasLimpieza;
                }
                return _tareasLimpieza;
            }
            set
            {
                if (!ReferenceEquals(_tareasLimpieza, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_tareasLimpieza != null)
                    {
                        _tareasLimpieza.CollectionChanged -= FixupTareasLimpieza;
                    }
                    _tareasLimpieza = value;
                    if (_tareasLimpieza != null)
                    {
                        _tareasLimpieza.CollectionChanged += FixupTareasLimpieza;
                    }
                    OnNavigationPropertyChanged("TareasLimpieza");
                }
            }
        }
        private SotCollection<TareaLimpieza> _tareasLimpieza;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            TareasLimpieza.Clear();
        }

        #endregion

        #region Association Fixup
    
        private void FixupTareasLimpieza(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (TareaLimpieza item in e.NewItems)
                {
                    item.Motivo = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (TareaLimpieza item in e.OldItems)
                {
                    if (ReferenceEquals(item.Motivo, this))
                    {
                        item.Motivo = null;
                    }
                }
            }
        }

        #endregion

    }
}
