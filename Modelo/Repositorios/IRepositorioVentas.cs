﻿using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioVentas : IRepositorio<Venta>
    {
        //int? ObtenerUltimoTicket();
        List<Venta> ObtenerVentasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);
        List<VW_VentaCorrecta> ObtenerVentasCorrectasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);

        List<DtoResumenCancelaciones> SP_ObtenerCancelacionesTurno(int idCorteTurno);
    }
}
