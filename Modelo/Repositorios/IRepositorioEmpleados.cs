﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;

namespace Modelo.Repositorios
{
    public interface IRepositorioEmpleados: IRepositorio<Empleado>
    {
        //List<Empleado> ObtenerPorPuesto(string puesto, bool soloHabilitados);
        List<Empleado> ObtenerPorPuesto(bool soloHabilitados, /*int? idTurno, */params int[] idsaPuestos);
        List<Empleado> ObtenerConNombrePuesto(bool soloHabilitados);
        Empleado ObtenerConCantidadTareas(int idEmpleado);

        List<Empleado> ObtenerEmpleadosFiltrados(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaRegistro = null);
        List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreas();

        List<DtoEmpleadoPuesto> ObtenerNombreEmpleadoPuestoPorIdsConInactivos(params int[] idsEmpleados);

        DtoEmpleadoPuesto ObtenerResumenEmpleadoPuestoPorId(int idEmpleado);
    }
}
