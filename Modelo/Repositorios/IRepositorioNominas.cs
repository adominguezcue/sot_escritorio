﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioNominas : IRepositorio<Nomina>
    {
        List<Nomina> ObtenerNominasFiltradas(string nombre, string apellidoPaterno, string apellidoMaterno, DateTime? fechaInicial, DateTime? fechaFinal);
    }
}
