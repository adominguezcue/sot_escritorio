﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioPagosRenta : IRepositorio<PagoRenta>
    {
        List<PagoRenta> ObtenerPagosPorTransaccion(string transaccion,bool incluirCancelada);// DateTime? fechaFin);

        VentaRenta ObtenerVentaConConceptos(string transaccion, bool incluirCancelada);

        VentaRenta ObtenerVentaConConceptos(int idVentaRenta);

        VentaRenta ObtenerVentaCanceladaConConceptos(int idVentaRenta);
    }
}
