﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioOcupacionesMesa : IRepositorio<OcupacionMesa>
    {
        OcupacionMesa ObtenerOcupacionActualPorMesa(int idMesa);
        OcupacionMesa ObtenerParaCancelar(int idOcupacion);
        OcupacionMesa ObtenerParaCobrar(int idMesa);

        OcupacionMesa ObtenerOcupacionFinalizadaConDetalles(int idOcupacion);

        DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion);

        string ObtenerNumeroMesaPorTransaccion(string numeroTransaccion);
    }
}
