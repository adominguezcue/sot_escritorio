﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioReembolsosPuntos : IRepositorio<ReembolsoPuntos>
    {
        List<ReembolsoPuntos> ObtenerReembolsosFallidos(int? idHabitacion, DateTime? fechaInicial, DateTime? fechaFinal);
    }
}
