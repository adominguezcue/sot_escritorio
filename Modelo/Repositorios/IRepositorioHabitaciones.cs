﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;

namespace Modelo.Repositorios
{
    public interface IRepositorioHabitaciones : IRepositorio<Habitacion>
    {
        List<Habitacion> ObtenerActivasConTipos();

        Habitacion ObtenerConMotivoBloqueo(int idHabitacion);

        Habitacion ObtenerConReservacionConfirmada(int idHabitacion);

        List<Habitacion> ObtenerPreparadasOHabilitadasConTipos();

        DtoResumenHabitacion SP_ObtenerResumenHabitacion(int idHabitacion, int? cambiosup);

        List<DtoResumenHabitacion> SP_ObtenerResumenesHabitaciones();

        List<DtoEstadisticasHabitacion> ObtenerEstadisticasHabitaciones();
    }
}
