﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using Dominio.Nucleo.Entidades;

namespace Modelo.Repositorios
{
    public interface IRepositorioOrdenesRestaurantes : IRepositorio<OrdenRestaurante>
    {
        OrdenRestaurante ObtenerParaCobrar(int idOrdenRestaurante);
        OrdenRestaurante ObtenerParaCancelar(int idOrdenRestaurante);

        OrdenRestaurante ObtenerParaEntregarCliente(int idOrdenRestaurante);

        List<OrdenRestaurante> ObtenerOrdenesPendientesCargadas(int idMesa);

        OrdenRestaurante ObtenerPorArticulos(int idOrden, List<int> idsArticulosOrden);

        //List<DtoArticuloPrepararConsulta> ObtenerArticulosPorPreparar();

        decimal ObtenerPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null);

        Dictionary<TiposPago, decimal> ObtenerPagosMesaPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin);

        List<DtoEmpleadoLineaArticuloValor> ObtenerArticulosPorEmpleado(List<DtoIdArticuloIdLinea> articulos, List<int> idsCortes);

        List<OcupacionMesa> ObtenerOcupacionesCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);

        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosMesaTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);

        List<ArticuloOrdenRestaurante> ObtenerDetallesOrdenesEntregadasCliente(int idOcupacion);

        //List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosEnPreparacionArea(List<string> idsArticulos);

        //List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosPreparadosArea(List<string> idsArticulos);

        List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(List<string> idsArticulos);

        List<DtoResumenComandaExtendido> ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion();

        List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketOrdenRestaurante(int idOrdenRestaurante);

        List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketOrdenRestauranteCancelada(int idOrdenRestaurante);
        
        int ObtenerCantidadArticulosOrdenesRestaurantePorCodigoArticulo(string codigoArticulo);
    }
}
