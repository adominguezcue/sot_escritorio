﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioTareasLimpieza : IRepositorio<TareaLimpieza>
    {
        TareaLimpieza ObtenerConEmpleados(int idHabitacion);

        List<LimpiezaEmpleado> ObtenerEmpleadosLimpiezaActivos();

        List<Dtos.DtoDetallesTareaImpuro> ObtenerDetallesImpuros(int? ordenTurno = null, int? idEmpleado = null, int? idTipoHabitacion = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);
    }
}
