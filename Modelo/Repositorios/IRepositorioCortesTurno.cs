﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioCortesTurno : IRepositorio<CorteTurno>
    {
        //int ObtenerUltimoNumeroCorte();

        //DateTime? ObtenerFechaUltimoCorte();

        CorteTurno ObtenerUltimoCorte();

        CorteTurno ObtenerCorteEnRevision(int idCorteTurno);

        //List<Dtos.DtoCorteTurno> ObtenerResumenesCortesFinalizadosPorRango(DateTime? fechaInicial, DateTime? fechaFinal);

        //CorteTurno ObtenerUltimoCorteCerrado();
        string ObtieneSiglaTurno(int idconfigTurno);													
        string UsuarioCerroReporte(int idUsuariocerro);													   

        List<Dtos.DtoResumenCorteTurno> ObtenerResumenesCortesEnRevision();
        List<Dtos.DtoAreas> ObtieneAreas();										   

        void SP_MoverComandas(int idCorte);

        void Sp_PateaComandaOrdeResConcumoInte(int idcoret1, int idcorte2);

        List<Dtos.DtoCorteTurno> ObtenerResumenesCortesPorRango(DateTime? fechaInicial, DateTime? fechaFinal);
    }
}
