﻿using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using Dominio.Nucleo.Entidades;

namespace Modelo.Repositorios
{
    public interface IRepositorioComandas: IRepositorio<Comanda>
    {
        Comanda ObtenerParaPagar(int idComanda);

        Comanda ObtenerPorArticulos(int idComanda, List<int> idsArticulosComandas);

        List<Comanda> ObtenerComandasPendientesCargadas(int idRenta);

        //List<DtoArticuloPrepararConsulta> ObtenerArticulosPorPreparar();

        List<Comanda> ObtenerComandasCobradasCargadas(int idRenta);

        //List<DtoEmpleadoArticuloValor> ObtenerValorArticulosComandasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);

        decimal ObtenerPagosComandasPorFecha(DateTime? fechaInicio, DateTime? fechaFin);

        Dictionary<TiposPago, decimal> ObtenerPagosComandasPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin);
        //List<Comanda> ObtenerDetallesComandasArticulosEnPreparacion(List<string> idsArticulos);
        //List<Comanda> ObtenerDetallesComandasArticulosPreparados(List<string> idsArticulos);
        List<Comanda> ObtenerDetallesComandasArticulosPreparadosOEnPreparacion(List<string> idsArticulos);
        List<DtoResumenComandaExtendido> ObtenerDetallesComandasArticulosPreparadosOEnPreparacion();
        List<Comanda> ObtenerComandasCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);

        Comanda ObtenerComandaPagadaConDetalles(int idComanda);
        List<DtoResumenComanda> ObtenerResumenesComandasPorRenta(int idRenta);

        List<DtoEmpleadoLineaArticuloValor> ObtenerArticulosPorEmpleado(List<DtoIdArticuloIdLinea> articulos, List<int> idsCortes);

        List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketComanda(int idComanda);
        string ObtenerNumeroHabitacionPorTransaccion(string numeroTransaccion);

        List<DtoHistorialesComanda> SP_ObtenerHistorialesComandas(DateTime fechaInicio, DateTime fechaFin, int? ordenTurno = null, int? idLinea = null, Comanda.Tipos? tipo = null, Comanda.Estados? estado = null);

        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosComandasTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);


        List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketComandaCancelada(int idComanda , bool?Escambio=false);

        int ObtenerCantidadArticulosComandasPorCodigoArticulo(string codigoArticulo);
    }
}
