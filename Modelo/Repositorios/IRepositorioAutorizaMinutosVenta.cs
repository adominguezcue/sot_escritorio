﻿using System;
using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioAutorizaMinutosVenta: IRepositorio<AutorizaMinutosVenta>
    {
    }
}
