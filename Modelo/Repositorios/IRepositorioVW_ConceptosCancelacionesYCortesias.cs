﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Dtos;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioVW_ConceptosCancelacionesYCortesias: IRepositorio<VW_ConceptoCancelacionesYCortesias>
    {
    }
}
