﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioFajillas : IRepositorio<Fajilla>
    {
        List<Fajilla> ObtenerFajillasPorTurno(int idCorte, bool soloAutorizadas);

        int ObtenerUltimoNumeroFajilla(int idCorte);

        List<Fajilla> ObtenerFajillasAutorizadasConMontosPorCorte(int idCorte);

        Fajilla ObtenerFajillaCargada(int idFajilla);

        Fajilla ObtenerSobrantePorTurno(int idCorte);

        List<Fajilla> ObtenerFajillasYSobranteCargadosNoSincronizados();
    }
}
