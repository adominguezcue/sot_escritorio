﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioVentasRentas : IRepositorio<VentaRenta>
    {
        string ObtenerNumeroHabitacionPorTransaccion(string numeroTransaccion);
        string ObtenerNumeroHabitacionPorIdComanda(int idComanda);
        string ObtenerNumeroHabitacionPorIdRenta(int idRenta);
        VentaRenta ObtenerVentaRentaPendienteParaCancelar(int idVentaRenta);
        List<DtoDatosPersonaExtrasRepCorteTurno> ObtienePersonasExtrasPorCorte(int folioCorte);
    }
}
