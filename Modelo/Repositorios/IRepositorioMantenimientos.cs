﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioMantenimientos : IRepositorio<Mantenimiento> 
    {
        List<Dtos.DtoResumenMantenimiento> ObtenerMantenimientosHabitacion(int idHabitacion, int? idconcepto = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0);
    }
}
