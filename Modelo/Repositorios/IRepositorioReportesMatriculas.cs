﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioReportesMatriculas : IRepositorio<ReporteMatricula>
    {
        List<ReporteMatricula> ObtenerReportesMatricula(string matricula);
    }
}
