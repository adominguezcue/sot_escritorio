﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Dominio.Nucleo.Entidades;

namespace Modelo.Repositorios
{
    public interface IRepositorioReservaciones : IRepositorio<Reservacion>
    {
        Reservacion ObtenerReservacionParaEditar(int idReservacion);

        //Reservacion ObtenerReservacionParaCancelar(int idReservacion);
        /*
        DtoValor ObtenerValorReservacionesPendientesPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);
        */
        List<DtoReservacion> ObtenerReservacionesFiltradas(DateTime fechaInicio, DateTime fechaFin,string idEstado, int idTipoHabitacion = 0);
        List<DtoReservacion> ObtenerReservacionesPorFechaCreacion(DateTime fechaInicio, DateTime fechaFin, string idEstado, int idTipoHabitacion = 0);

        List<DtoReservacion> ObtenerReservaciones(string reservacion);																																					  
		
        Reservacion ObtenerReservacionConfimada(int idHabitacion);

        int SP_ObtenerCantidadCruzadas(DateTime fechaEntrada, DateTime fechaSalida, int idReservacion, int idConfiguracionTipoHabitacion);

        Dictionary<TiposPago, decimal> ObtenerPagosPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin);
        Dictionary<TiposPago, decimal> ObtenerPagosDeConsumidasPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin);
        Dictionary<TiposTarjeta, decimal> ObtenerPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        Dictionary<TiposTarjeta, decimal> ObtenerPagosTarjetaDeConsumidasPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        Reservacion ObtenerPorCodigoConMontosNoReembolsables(string codigoReservacion);
    }
}
