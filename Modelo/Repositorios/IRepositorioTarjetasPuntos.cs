﻿using Dominio.Nucleo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioTarjetasPuntos : IRepositorio<TarjetaPuntos>
    {
        Dictionary<TiposPago, decimal> ObtenerDetallesPagosPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin);

        List<TarjetaPuntos> ObtenerTarjetas(DateTime? fechaInicio = null, DateTime? fechaFin = null, TarjetaPuntos.EstadosTarjeta? estado = null);
        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);

    }
}
