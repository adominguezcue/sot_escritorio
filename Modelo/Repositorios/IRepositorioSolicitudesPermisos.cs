﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioSolicitudesPermisos : IRepositorio<SolicitudPermiso>
    {
        List<Dtos.DtoSolicitudPermiso> ObtenerSolicitudesPermiso(string nombre, string apellidoPaterno, string apellidoMaterno, DateTime? fechaInicial, DateTime? fechaFinal);
    }
}
