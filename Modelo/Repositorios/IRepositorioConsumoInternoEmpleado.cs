﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Dtos;

namespace Modelo.Repositorios
{
    public interface IRepositorioConsumoInternoEmpleado
    {
        List<VW_ConsumoInternoEmpleado> ObtieneEmpleados(string nombreempleado);

        List<DtoConsumoInternosEmpleado> ObtieneConsumoInternoempleado(string numeroempleado, DateTime fechainicial, DateTime fechafinal, bool Esfechashablitadas);
    }
}
