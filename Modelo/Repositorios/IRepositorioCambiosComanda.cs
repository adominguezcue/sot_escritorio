﻿using System;
using System.Collections.Generic;
using Modelo.Entidades;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioCambiosComanda : IRepositorio<CambiosComandas>
    {
    }
}
