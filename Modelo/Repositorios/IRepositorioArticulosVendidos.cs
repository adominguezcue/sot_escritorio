﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioArticulosVendidos: IRepositorio<VW_ArticuloVendido>
    {
        List<VW_ArticuloVendido> ObtenerArticulosVendidos(int? ordenTurno = null, string categoria = null, string subCategoria = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, bool? agrupar = null);
    }
}
