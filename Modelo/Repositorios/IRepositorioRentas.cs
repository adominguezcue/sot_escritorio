﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Dominio.Nucleo.Entidades;

namespace Modelo.Repositorios
{
    public interface IRepositorioRentas: IRepositorio<Renta>
    {
        Renta ObtenerRentaActualPorHabitacion(int idHabitacion);
        Renta ObtenerCargada(int idRenta);
        List<AutomovilRenta> ObtenerAutomovilesUltimaRenta(int idHabitacion);
        Renta ObtenerUltimaRentaPorHabitacionConDatosFiscales(int idHabitacion);
        Renta ObtenerUltimaRentaPorHabitacion(int idHabitacion);
        Renta ObtenerUltimaRentaPorHabitacionCargada(int idHabitacion);
        List<DtoResumenVentaRenta> ObtenerResumenVentas(int idRenta, decimal porcentajeIVA);
        List<DetallePago> ObtenerDetallesPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null);
        Dictionary<string, List<DetallePago>> ObtenerDetallesPagosPorFechaYTipo(DateTime? fechaInicio = null, DateTime? fechaFin = null);

        Dictionary<TiposPago, decimal> ObtenerPagosHabitacionPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin);
        decimal ObtenerMontoNoReembolsablePorFecha(DateTime? fechaInicio, DateTime? fechaFin);

        List<PagoRenta> ObtenerPagosRentasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);

        //List<PersonaExtra> ObtenerPersonasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);

        //List<Extension> ObtenerHorasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);

        List<PaqueteRenta> ObtenerPaquetesRentaPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);

        Dictionary<TiposTarjeta, decimal> ObtenerPagosHabitacionTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);

        List<DetallePago> ObtenerDetallesPorTransaccion(string transaccion, DateTime? fechaFin);

        List<DtoEnvoltorioEmpleado> ObtenerInformacionValetsPorIdVentaRenta(List<int> idsVentasRenta);

        DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion);

        Renta ObtenerConExtensiones(int idRenta);

        Renta ObtenerRentaParaCancelar(int idHabitacion);

        DtoAutomovil ObtenerDatosAutomovil(string matricula);

        string ObtieneCodigoReservacion(DateTime fecharegistro, int idHabitacion);
    }
}
