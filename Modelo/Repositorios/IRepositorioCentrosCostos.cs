﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioCentrosCostos : IRepositorio<CentroCostos>
    {
        List<CentroCostos> ObtenerCentrosCostosConNombreCategoria();
        List<CentroCostos> ObtenerCentrosCostosFiltrados(string nombreCentro, int? idCategoriaCentro);

        //decimal ObtenerPresupuestoRestante(int idCentroCostos, int? idConceptoIgnorar);

        //decimal ObtenerPresupuestoGastos(int idCentroCostos);
    }
}
