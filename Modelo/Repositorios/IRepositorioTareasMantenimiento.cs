﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;

namespace Modelo.Repositorios
{
    public interface IRepositorioTareasMantenimiento : IRepositorio<TareaMantenimiento>
    {
        List<TareaMantenimiento> ObtenerTareasPorMes(DateTime fecha, int idHabitacion = 0);

        int SP_ObtenerCantidadTareasPendientes(int idHabitacion = 0);

        List<DtoTareaMantenimientoBase> SP_ObtenerTareasMes(int mes, int anio, int idHabitacion = 0);

        List<DtoResumenMantenimiento> ObtenerMantenimientosPendientes(int idHabitacion, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0);
    }
}
