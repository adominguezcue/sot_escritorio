﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;

namespace Modelo.Repositorios
{
    public interface IRepositorioMedicionesMantenimiento : IRepositorio<MedicionMantenimiento>
    {
        List<MedicionMantenimiento> ObtieneLecturasActivas();

        List<MedicionMantenimiento> ObtieneLecturasAnteriores();

        List<MedicionMantenimiento> ObtieneApuntadoresAnteriores();

        List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradas(int? idCorte);

        List<MedicionMantenimiento> ObtieneLecturasPorCorte(int? idCorte);

        List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradasPorFecha(DateTime? fechaInicio);

    }
}
