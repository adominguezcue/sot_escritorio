﻿using Dominio.Nucleo.Entidades;
using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioConsumosInternos : IRepositorio<ConsumoInterno>
    {
        Dictionary<TiposPago, decimal> ObtenerPagosConsumoInternoPorFecha(List<int> idsTiposPago, DateTime? fechaInicio, DateTime? fechaFin);

        decimal ObtenerPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null);

        List<ConsumoInterno> ObtenerConsumosInternosCargadosPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);

        ConsumoInterno ObtenerParaPagar(int idConsumoInterno);

        ConsumoInterno ObtenerPorArticulos(int idConsumoInterno, List<int> idsArticulosConsumosInternos);

        List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketConsumoInterno(int idConsumoInterno);

        ConsumoInterno ObtenerConsumoInternoEntregadoConDetalles(int idConsumoInterno);

        int ObtenerCantidadEnCurso(string nombre, string apellidoPaterno, string apellidoMaterno);

        List<ConsumoInterno> ObtenerConsumosInternosFiltrados(string nombre, string apellidoPaterno, string apellidoMaterno, bool soloEnCurso, DateTime? fechaInicial = null, DateTime? fechaFinal = null);

        ConsumoInterno ObtenerConsumoPendienteCargado(int idConsumoInterno);
        ConsumoInterno ObtenerConsumoFinalizadoCargado(int idConsumoInterno);
        ConsumoInterno ObtenerConsumoCanceladoCargado(int idConsumoInterno);

        bool ExisteDeVerdadConsumosInternos(List<int> Estados);

        List<ConsumoInterno> ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion(List<string> idsArticulos);

        List<DtoResumenComandaExtendido> ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion();

        List<DtoEmpleadoLineaArticuloValor> ObtenerArticulosPorEmpleado(List<DtoIdArticuloIdLinea> articulos, List<int> idsCortes);

        List<DtoArticuloTicket> SP_ObtenerResumenesArticulosTicketConsumoInternoCancelado(int idConsumoInterno);
        
        int ObtenerCantidadArticulosConsumosInternosPorCodigoArticulo(string codigoArticulo);
    }
}
