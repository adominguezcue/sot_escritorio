﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioCatPresentacionMantenimiento : IRepositorio<CatPresentacionMantenimiento>
    {
        List<CatPresentacionMantenimiento> ObtienePresentacionMantenimiento();
    }
}
