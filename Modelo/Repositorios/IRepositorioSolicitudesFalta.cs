﻿using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioSolicitudesFalta : IRepositorio<SolicitudFalta>
    {
        List<DtoSolicitudFalta> ObtenerSolicitudesFaltas(string nombre, string apellidoPaterno, string apellidoMaterno, DateTime? fechaInicial, DateTime? fechaFinal);

        List<DtoSolicitudFalta> ObtenerSolicitudesFaltaSuplenciaPorFecha(int idEmpleado, DateTime? fechaInicial, DateTime? fechaFinal);
    }
}
