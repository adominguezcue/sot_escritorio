﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;
using System.Data;

namespace Modelo.Repositorios
{
    public interface IRepositorioTiposHabitacion : IRepositorio<TipoHabitacion>
    {
        List<int> ObtenerTarifasSoportadas(int idTipoHabitacion);

        TipoHabitacion ObtenerTipoConTiemposLimpiezaPorHabitacion(int idHabitacion);

        List<TipoHabitacion> ObtenerTiposActivosConConfiguracion(ConfiguracionTarifa.Tarifas tarifa);

        List<TipoHabitacion> ObtenerTiposActivosConConfiguraciones();

        List<DtoResumenTipoHabitacion> ObtenerTiposActivosCargadosDataTable();

        TipoHabitacion ObtenerTipoActivoCargado(int idTipoHabitacion);

        List<DtoCantidadesHabitacionTipoEstado> SP_ObtenerCantidadHabitacionesPorEstado(DateTime fecha);
    }
}
