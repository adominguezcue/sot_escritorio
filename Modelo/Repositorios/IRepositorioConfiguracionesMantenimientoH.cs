﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;

namespace Modelo.Repositorios
{
    public interface IRepositorioConfiguracionesMantenimientoH : IRepositorio<ConfiguracionHMantenimiento>
    {
        bool EsInsertaRegistroPorCorteTurno(int idcorte);
    }
}
