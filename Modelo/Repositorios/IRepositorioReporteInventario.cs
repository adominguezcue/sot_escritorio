﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades.Parciales;

namespace Modelo.Repositorios
{
    public interface IRepositorioReporteInventario
    {

        List<CatalogoAlmacen> ObtieneCatalogoAlmacen(string depto, string linea);


        List<CatalogoLinea> ObtienecatalogoLinea(string deptos);

        List<CatalogoDepartamento> ObtieneCatalogoDepto(string deptos);

        List<ArticulosInventarioActual> ObtieneArticulosInvActual(string almacen, string articulos, string linea, string depto);
    }
}
