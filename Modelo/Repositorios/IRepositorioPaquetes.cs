﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioPaquetes : IRepositorio<Paquete>
    {

        List<Paquete> ObtenerPaquetesActivosConProgramas();

        List<Paquete> ObtenerPaquetesActivosConProgramasPorTipoHabitacion(int idTipoHabitacion);

        Paquete ObtenerPaqueteActivoConProgramas(int p);
    }
}
