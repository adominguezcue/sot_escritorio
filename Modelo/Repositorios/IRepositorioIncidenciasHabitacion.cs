﻿using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioIncidenciasHabitacion : IRepositorio<IncidenciaHabitacion>
    {
        List<DtoIncidenciaHabitacion> ObtenerIncidenciasFiltradas(DateTime? fechaInicial, DateTime? fechaFinal, int? idHabitacion, int? idEmpleado);
    }
}
