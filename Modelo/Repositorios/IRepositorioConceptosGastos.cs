﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioConceptosGastos : IRepositorio<ConceptoGasto>
    {
        List<ConceptoGasto> ObtenerConceptosPagablesEnCaja();
        List<ConceptoGasto> ObtenerConceptosActivos();

        //decimal ObtenerTotalTopesCentro(int idCentroCostos);

        ConceptoGasto ObtenerConceptoConRestante(int idConceptoGasto, int? idGasto);
    }
}
