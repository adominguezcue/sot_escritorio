﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioConfiguracionesTurno : IRepositorio<ConfiguracionTurno>
    {
        List<ConfiguracionTurno> ObtenerConfiguracionesTurno(bool soloActivas = true);

        string ObtenerNombreTurnoPorCorte(int idCorteTurno);
    }
}
