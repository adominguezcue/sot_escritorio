﻿using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioIncidenciasTurno : IRepositorio<IncidenciaTurno>
    {
        List<DtoIncidenciaTurno> ObtenerIncidenciasFiltradas(DateTime? fechaInicial, DateTime? fechaFinal, int? idCorte = null, string area = null);
    }
}
