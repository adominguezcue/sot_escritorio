﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioPropinas : IRepositorio<Propina>
    {
        Dictionary<Propina.TiposPropina, decimal> ObtenerTotalPropinasPorTipo(DateTime? fechaInicio, DateTime? fechaFin);
        Dictionary<TiposTarjeta, decimal> ObtenerTotalPropinasPorTarjeta(DateTime? fechaInicio, DateTime? fechaFin);

        List<Propina> ObtenerPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, params Propina.TiposPropina[] tipos);

        List<Dtos.DtoResumenPropina> ObtenerResumenesPorTransaccion(string transaccion);
    }
}
