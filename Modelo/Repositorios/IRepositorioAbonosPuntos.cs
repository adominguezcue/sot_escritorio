﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioAbonosPuntos : IRepositorio<AbonoPuntos>
    {
        List<AbonoPuntos> ObtenerAbonosFallidos(int? idHabitacion, DateTime? fechaInicial, DateTime? fechaFinal);
    }
}
