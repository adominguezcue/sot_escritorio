﻿using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioSolicitudesVacaciones : IRepositorio<SolicitudVacaciones>
    {
        List<DtoSolicitudVacaciones> ObtenerSolicitudesVacaciones(string nombre, string apellidoPaterno, string apellidoMaterno, DateTime? fechaInicial, DateTime? fechaFinal);
    }
}
