﻿using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioPagosReservaciones : IRepositorio<PagoReservacion>
    {
        List<PagoReservacion> ObtenerPagosPorTransaccion(string transaccion, bool incluirCancelada);// DateTime? fechaFin);

        List<PagoReservacion> ObtenerPagosPorCodigosReserva(List<string> codigosReservaciones);
    }
}
