﻿using Dominio.Nucleo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Repositorios
{
    public interface IRepositorioAsistencias : IRepositorio<Asistencia>
    {
        List<Asistencia> ObtenerAsistenciasFiltadas(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);

        Asistencia ObtenerUltimaSinSalida(int idEmpleado);
        DtoResumenAsistencia ObtenerResumenUltimaAsistenciaAbierta(string numeroEmpleado);
    }
}
