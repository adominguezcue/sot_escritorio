﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Dtos;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Repositorios
{
    public interface IRepositorioCortesiaCancelaciones : IRepositorio<VW_ConsCancelacionesYCortesias>
    {
        List<DtoConceptoCortesiaCancelacion> ObtieneConceptos();

        List<DtoCortesiasCancelaciones> ObtieneCortesiasCancelaciones(string concepto, DateTime fechainicio, DateTime fechafin);

        List<VW_VentaCorrecta> ObtieneVentaCorrecta(int ticketseleccionado, string tipoid);
    }
}
