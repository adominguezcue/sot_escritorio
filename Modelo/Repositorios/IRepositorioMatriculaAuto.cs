﻿using System.Collections.Generic;
using Dominio.Nucleo.Repositorios;
using Modelo.Entidades;
using Modelo.Dtos;

namespace Modelo.Repositorios
{
    public interface IRepositorioMatriculaAuto : IRepositorio<AutomovilRenta>
    {
         List<AutomovilRenta> ObtieneMatriculasAutos(string matricula = null);

        List<DtoHabitacionesxMatricula> ObtieneHabitacionesPorMatricula(string matricula);

        List<DtTotalComandasxRenta> ObtieneComandasPorRenta(int idrenta);
    }
}
