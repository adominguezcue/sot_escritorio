﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    [DataContract]
    public partial class TypeMovMix
    {
        public string IdType { get; set; }
        public string TypeDesc { get; set; }
    }
}
