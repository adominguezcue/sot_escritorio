﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoArticuloPresentacion : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string _codigoSalida,
                       _unidadSalida,
                       _descripcionSalida,
                       _codigoEntrada;
                       //_unidadSalida,
                       //_descripcionSalida;

        private int _id, _idUnidad;

        private bool _activo, _esEntrada, _omitirIVA, _negarEntrada, _negarSalida;

        private decimal _cantidadEntrada, _cantidadSalida, _precio, _costo, _precioConIVA, _costoFinal, _iva;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                NotifyPropertyChanged();
            }
        }

        public string CodigoSalida
        {
            get
            {
                return _codigoSalida;
            }
            set
            {
                _codigoSalida = value;
                NotifyPropertyChanged();
            }
        }
        public string UnidadSalida
        {
            get
            {
                return _unidadSalida;
            }
            set
            {
                _unidadSalida = value;
                NotifyPropertyChanged();
            }
        }
        public string DescripcionSalida
        {
            get
            {
                return _descripcionSalida;
            }
            set
            {
                _descripcionSalida = value;
                NotifyPropertyChanged();
            }
        }
        public decimal CantidadSalida
        {
            get
            {
                return _cantidadSalida;
            }
            set
            {
                _cantidadSalida = value;
                NotifyPropertyChanged();
            }
        }

        public string CodigoEntrada
        {
            get
            {
                return _codigoEntrada;
            }
            set
            {
                _codigoEntrada = value;
                NotifyPropertyChanged();
            }
        }
        public decimal CantidadEntrada
        {
            get
            {
                return _cantidadEntrada;
            }
            set
            {
                _cantidadEntrada = value;
                NotifyPropertyChanged();
            }
        }

        public int IdUnidad
        {
            get
            {
                return _idUnidad;
            }
            set
            {
                _idUnidad = value;
                NotifyPropertyChanged();
            }
        }

        public bool Activo
        {
            get
            {
                return _activo;
            }
            set
            {
                _activo = value;
                NotifyPropertyChanged();
            }
        }

        public bool EsEntrada
        {
            get
            {
                return _esEntrada;
            }
            set
            {
                _esEntrada = value;
                NotifyPropertyChanged();
            }
        }

        public bool OmitirIVA
        {
            get
            {
                return _omitirIVA;
            }
            set
            {
                _omitirIVA = value;
                RecalcularCosto();
                NotifyPropertyChanged();
            }
        }

        public decimal Precio
        {
            get
            {
                return _precio;
            }
            private set
            {
                _precio = value;
                NotifyPropertyChanged();
            }
        }

        public decimal Costo
        {
            get
            {
                return _costo;
            }
            private set
            {
                _costo = value;
                NotifyPropertyChanged();
            }
        }

        public decimal PrecioConIVA
        {
            get
            {
                return _precioConIVA;
            }
            set
            {
                _precioConIVA = value;
                RecalcularPrecio();
                NotifyPropertyChanged();
            }
        }

        public decimal CostoFinal
        {
            get
            {
                return _costoFinal;
            }
            set
            {
                _costoFinal = value;
                RecalcularCosto();
                NotifyPropertyChanged();
            }
        }

        public decimal IVA
        {
            get
            {
                return _iva;
            }
            set
            {
                _iva = value;
                RecalcularCosto();
                RecalcularPrecio();
                NotifyPropertyChanged();
            }
        }

        private void RecalcularPrecio() 
        {
            Precio = PrecioConIVA / (1 + IVA);
        }

        private void RecalcularCosto()
        {
            Costo = OmitirIVA ? CostoFinal : (CostoFinal / (1 + IVA));
        }

        public bool NegarEntrada
        {
            get
            {
                return _negarEntrada;
            }
            set
            {
                _negarEntrada = value;
                NotifyPropertyChanged();
            }
        }

        public bool NegarSalida
        {
            get
            {
                return _negarSalida;
            }
            set
            {
                _negarSalida = value;
                NotifyPropertyChanged();
            }
        }
    }
}
