﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoExistencias
    {
        //public DtoExistencias(string Cod_Art, string Cod_Alm, string Desc_Alm, int Exist_Art, decimal CostoProm_Art)
        //{
        //    Codigo = Cod_Art;
        //    CodigoAlmacen = Cod_Alm;
        //    Almacen = Desc_Alm;
        //    Existencias = Exist_Art;
        //    CostoPromedio = CostoProm_Art;
        //}

        public string Codigo { get; set; }

        public int CodigoAlmacen { get; set; }

        public string Almacen { get; set; }

        public decimal Existencias { get; set; }

        public decimal CostoPromedio { get; set; }

        public string NombrePresentacion { get; set; }
    }
}
