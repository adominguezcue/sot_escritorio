﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoItemKardex
    {
        public string Cod_Art { get; set; }
        public decimal CtdMov_Inv { get; set; }
        public DateTime FchMov_Inv { get; set; }
        public string TpMov_Inv { get; set; }
        public decimal CosMov_Inv { get; set; }
        public string TpOs_Inv { get; set; }
        public string FolOS_Inv { get; set; }
        public int Cod_Alm { get; set; }
        public int CtdIni { get; set; }
        public decimal CostoIni { get; set; }
        public string Desc_CatAlm { get; set; }
    }
}
