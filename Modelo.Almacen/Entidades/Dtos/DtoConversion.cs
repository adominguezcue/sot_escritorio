﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoConversion
    {
        public int Id { get; set; }
        public bool EsInversa { get; set; }
        public decimal Equivalencia { get; set; }
        public int IdPresentacionEntrada { get; set; }
        public int IdPresentacionSalida { get; set; }
        public bool Activa { get; set; }
    }
}
