﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoEncabezadoOCWpf
    {
        public int Cod_OC { get; set; }
        public string status { get; set; }

        public DateTime NN_Fch_OC { get; set; }
    }
}
