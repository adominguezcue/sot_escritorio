﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoResultadoBusquedaArticulo
    {
        public string Cod_Art { get; set; }
        public string Desc_Art { get; set; }
        public int? ColExist { get; set; }
        public int ColSurt { get; set; }
        public decimal Prec_Art { get; set; }
        public decimal Cos_Art { get; set; }
        public string Cod_TpArt { get; set; }
        public int? cod_alm { get; set; }
        public string marca { get; set; }
        public bool OmitirIVA { get; set; }
    }
}
