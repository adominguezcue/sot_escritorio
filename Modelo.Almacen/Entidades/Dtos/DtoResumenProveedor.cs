﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoResumenProveedor
    {
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public string RFC { get; set; }
        public string NombreComercial { get; set; }
    }
}
