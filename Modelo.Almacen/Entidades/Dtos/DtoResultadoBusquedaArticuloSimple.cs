﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoResultadoBusquedaArticuloSimple
    {
        public string Codigo { get; set; }

        public string Nombre { get; set; }

        public decimal Precio { get; set; }

        public decimal? Costo { get; set; }

        public string Tipo { get; set; }

        public bool OmitirIVA { get; set; }

        public decimal PorcentajeIVA { get; set; }

        public decimal PrecioFinal { get { return Math.Round(Precio * (1 + PorcentajeIVA), 2); } }

        public decimal CostoFinal { get { return Costo.HasValue ? (OmitirIVA ? Math.Round(Costo.Value, 2) : Math.Round(Costo.Value * (1 + PorcentajeIVA), 2)) : 0; } }

        public string NombreUnidad { get; set; }
    }
}
