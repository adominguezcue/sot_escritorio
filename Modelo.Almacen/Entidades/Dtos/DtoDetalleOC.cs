﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoDetalleOC
    {
        public int Cod_OC { get; set; }
        public int CodDet_Oc { get; set; }
        public string Cod_Art { get; set; }
        public string upc { get; set; }
        public decimal? Ctd_Art { get; set; }
        public decimal? Cos_Art { get; set; }
        public decimal? CtdStdDet_OC { get; set; }
        public string Desc_Art { get; set; }
        public int? Exist_Art { get; set; }
        public int? Cat_Alm { get; set; }
        public string marca { get; set; }
        public bool OmitirIVA { get; set; }

        public DateTime? FchAplMov { get; set; }

        public decimal NN_Ctd_Art
        {
            get { return Ctd_Art ?? 0; }
        }

        public decimal NN_Cos_Art
        {
            get { return Cos_Art ?? 0; }
        }

        public decimal NN_CtdStdDet_OC
        {
            get { return CtdStdDet_OC ?? 0; }
        }

        public int NN_Cat_Alm
        {
            get { return Cat_Alm ?? 0; }
        }

        public bool Eliminar { get; set; }
    }
}
