﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtosArticulosWpfOC
    {
        public string Cod_Art { get; set; }
        public string Desc_Art { get; set; }
        public decimal Existencias { get; set; }
        public List<Modelo.Almacen.Entidades.Almacen> Almacen { get; set; } //este hay que buscarlo 
        public int indicealmacenseleccionado { get; set; }
        public int almacenseleccionado { get; set; }
        public string statussolicitada { get; set; }
        public decimal Solicitada { get; set; }
        public int CodDetOCIdentity { get; set; }
        public decimal Surtido { get; set; }
        public decimal costo { get; set; }
        public decimal subtotal { get; set; }
        public bool Elimina { get; set; }
        public bool omitiriva { get; set; }


    }
}
