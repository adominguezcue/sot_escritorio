﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoEncabezadoIntercambio
    {
        public DtoEncabezadoIntercambio()
        {
            Detalles = new List<DtoDetalleIntercambio>();
        }

        public int Folio { get; set; }
        public string AlmacenSalida { get; set; }
        public string AlmacenEntrada { get; set; }
        public DateTime FechaMovimiento { get; set; }
        public List<DtoDetalleIntercambio> Detalles { get; set; }
    }
}
