﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoEncabezadoOC
    {
        public int Cod_OC { get; set; }
        public int? Cod_Prov { get; set; }
        public DateTime? Fch_OC { get; set; }
        public DateTime? FchAplMov { get; set; }
        public string Fac_OC { get; set; }
        public string status { get; set; }

        public bool? Servicios { get; set; }

        private List<DtoDetalleOC> _detalles;

        public List<DtoDetalleOC> Detalles 
        {
            get { return _detalles ?? (_detalles = new List<DtoDetalleOC>()); }
            set { _detalles = value; }
        }

        private List<DtoServicioOC> _servicios;

        public List<DtoServicioOC> ServiciosOC 
        {
            get { return _servicios ?? (_servicios = new List<DtoServicioOC>()); }
            set { _servicios = value; }
        }

        public DateTime? FchFac_OC { get; set; }

        public DateTime NN_FchFac_OC { get { return FchFac_OC ?? DateTime.MinValue; } }
        public DateTime NN_FchAplMov { get { return FchAplMov ?? DateTime.MinValue; } }
        public DateTime NN_Fch_OC { get { return Fch_OC ?? DateTime.MinValue; } }

        public int? IdConceptoGasto { get; set; }
    }
}
