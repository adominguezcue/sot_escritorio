﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoDetalleIntercambio
    {
        public string CodigoArticulo { get; set; }
        public string DescripcionArticulo { get; set; }
        public DateTime FechaMovimiento { get; set; }
        public decimal CantidadIntercambiada { get; set; }
        public decimal CostoUnitario { get; set; }
        public decimal CostoMovimiento { get; set; }
    }
}
