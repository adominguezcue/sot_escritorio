﻿namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoServicioOC
    {
        public int Id { get; set; }
        public int CodigoOrdenCompra { get; set; }
        public string Descripcion { get; set; }
        public decimal Costo { get; set; }
        public bool Eliminar { get; set; }
    }
}