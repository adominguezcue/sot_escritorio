﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoArchivoFicha
    {
        public string Nombre { get; set; }
        public byte[] Valor { get; set; }
    }
}
