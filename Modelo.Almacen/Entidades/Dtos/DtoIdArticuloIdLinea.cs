﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades.Dtos
{
    public class DtoIdArticuloIdLinea
    {
        public string IdArticulo { get; set; }
        public int IdLinea { get; set; }
    }
}
