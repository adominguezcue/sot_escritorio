//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(ZctDetOC))]
    [KnownType(typeof(ZctEncCuentasPagar))]
    [KnownType(typeof(ZctRequisition))]
    [KnownType(typeof(Proveedor))]
    [KnownType(typeof(ServicioOrdenCompra))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class ZctEncOC: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctEncOC";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<ZctEncOC, bool>> Comparador()
        //{
            //return m => m.Cod_OC == Cod_OC;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            foreach(var item in ZctDetOC)
                if(item != null)
                    propiedades.Add(item);
            foreach(var item in ZctEncCuentasPagar)
                if(item != null)
                    propiedades.Add(item);
            foreach(var item in ZctRequisition)
                if(item != null)
                    propiedades.Add(item);
            if(ZctCatProv != null)
                propiedades.Add(ZctCatProv);
            foreach(var item in ServiciosOrdenCompra)
                if(item != null)
                    propiedades.Add(item);
    
            foreach(var item in ZctDetOC)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
            foreach(var item in ZctEncCuentasPagar)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
            foreach(var item in ZctRequisition)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
            if(ZctCatProv != null)
                ZctCatProv.ObtenerPropiedadesNavegacion(propiedades);
            foreach(var item in ServiciosOrdenCompra)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            foreach(var item in ZctDetOC)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
            foreach(var item in ZctEncCuentasPagar)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
            foreach(var item in ZctRequisition)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
            if(ZctCatProv != null && !propiedadesT.Contains(ZctCatProv))
                propiedades.Add(ZctCatProv);
            foreach(var item in ServiciosOrdenCompra)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public ZctEncOC()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public ZctEncOC ObtenerCopiaDePrimitivas()
        {
            return new ZctEncOC
            {
                Cod_Prov = this.Cod_Prov,
                Fch_OC = this.Fch_OC,
                FchAplMov = this.FchAplMov,
                Fac_OC = this.Fac_OC,
                FchFac_OC = this.FchFac_OC,
                status = this.status,
                Servicios = this.Servicios,
                IdConceptoGasto = this.IdConceptoGasto,
            };
        }
    
        public void SincronizarPrimitivas(ZctEncOC origen)
        {
            this.Cod_Prov = origen.Cod_Prov;
            this.Fch_OC = origen.Fch_OC;
            this.FchAplMov = origen.FchAplMov;
            this.Fac_OC = origen.Fac_OC;
            this.FchFac_OC = origen.FchFac_OC;
            this.status = origen.status;
            this.Servicios = origen.Servicios;
            this.IdConceptoGasto = origen.IdConceptoGasto;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Cod_OC
        {
            get { return _cod_OC; }
            set
            {
                if (_cod_OC != value)
                {
                    _cod_OC = value;
                    OnPropertyChanged("Cod_OC");
                }
            }
        }
        private int _cod_OC;
        [DataMember]
        public Nullable<int> Cod_Prov
        {
            get { return _cod_Prov; }
            set
            {
                if (_cod_Prov != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ZctCatProv != null && ZctCatProv.Cod_Prov != value)
                        {
                            ZctCatProv = null;
                        }
                    }
                    _cod_Prov = value;
                    OnPropertyChanged("Cod_Prov");
                }
            }
        }
        private Nullable<int> _cod_Prov;
        [DataMember]
        public Nullable<System.DateTime> Fch_OC
        {
            get { return _fch_OC; }
            set
            {
                if (_fch_OC != value)
                {
                    _fch_OC = value;
                    OnPropertyChanged("Fch_OC");
                }
            }
        }
        private Nullable<System.DateTime> _fch_OC;
        [DataMember]
        public Nullable<System.DateTime> FchAplMov
        {
            get { return _fchAplMov; }
            set
            {
                if (_fchAplMov != value)
                {
                    _fchAplMov = value;
                    OnPropertyChanged("FchAplMov");
                }
            }
        }
        private Nullable<System.DateTime> _fchAplMov;
        [DataMember]
        public string Fac_OC
        {
            get { return _fac_OC; }
            set
            {
                if ((value == null && _fac_OC != value) || (value != null && _fac_OC != value.Trim()))
                {
                    _fac_OC = (value == null ? value : value.Trim());
                    OnPropertyChanged("Fac_OC");
                }
            }
        }
        private string _fac_OC;
        [DataMember]
        public Nullable<System.DateTime> FchFac_OC
        {
            get { return _fchFac_OC; }
            set
            {
                if (_fchFac_OC != value)
                {
                    _fchFac_OC = value;
                    OnPropertyChanged("FchFac_OC");
                }
            }
        }
        private Nullable<System.DateTime> _fchFac_OC;
        [DataMember]
        public string status
        {
            get { return _status; }
            set
            {
                if ((value == null && _status != value) || (value != null && _status != value.Trim()))
                {
                    _status = (value == null ? value : value.Trim());
                    OnPropertyChanged("status");
                }
            }
        }
        private string _status;
        [DataMember]
        public Nullable<bool> Servicios
        {
            get { return _servicios; }
            set
            {
                if (_servicios != value)
                {
                    _servicios = value;
                    OnPropertyChanged("Servicios");
                }
            }
        }
        private Nullable<bool> _servicios;
        [DataMember]
        public Nullable<int> IdConceptoGasto
        {
            get { return _idConceptoGasto; }
            set
            {
                if (_idConceptoGasto != value)
                {
                    _idConceptoGasto = value;
                    OnPropertyChanged("IdConceptoGasto");
                }
            }
        }
        private Nullable<int> _idConceptoGasto;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public SotCollection<ZctDetOC> ZctDetOC
        {
            get
            {
                if (_zctDetOC == null)
                {
                    _zctDetOC = new SotCollection<ZctDetOC>();
                    _zctDetOC.CollectionChanged += FixupZctDetOC;
                }
                return _zctDetOC;
            }
            set
            {
                if (!ReferenceEquals(_zctDetOC, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_zctDetOC != null)
                    {
                        _zctDetOC.CollectionChanged -= FixupZctDetOC;
                        // This is the principal end in an association that performs cascade deletes.
                        // Remove the cascade delete event handler for any entities in the current collection.
                        foreach (ZctDetOC item in _zctDetOC)
                        {
                            //ChangeTracker.ObjectStateChanging -= item.HandleCascadeDelete;
                        }
                    }
                    _zctDetOC = value;
                    if (_zctDetOC != null)
                    {
                        _zctDetOC.CollectionChanged += FixupZctDetOC;
                        // This is the principal end in an association that performs cascade deletes.
                        // Add the cascade delete event handler for any entities that are already in the new collection.
                        foreach (ZctDetOC item in _zctDetOC)
                        {
                            //ChangeTracker.ObjectStateChanging += item.HandleCascadeDelete;
                        }
                    }
                    OnNavigationPropertyChanged("ZctDetOC");
                }
            }
        }
        private SotCollection<ZctDetOC> _zctDetOC;
    
        [DataMember]
        public SotCollection<ZctEncCuentasPagar> ZctEncCuentasPagar
        {
            get
            {
                if (_zctEncCuentasPagar == null)
                {
                    _zctEncCuentasPagar = new SotCollection<ZctEncCuentasPagar>();
                    _zctEncCuentasPagar.CollectionChanged += FixupZctEncCuentasPagar;
                }
                return _zctEncCuentasPagar;
            }
            set
            {
                if (!ReferenceEquals(_zctEncCuentasPagar, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_zctEncCuentasPagar != null)
                    {
                        _zctEncCuentasPagar.CollectionChanged -= FixupZctEncCuentasPagar;
                    }
                    _zctEncCuentasPagar = value;
                    if (_zctEncCuentasPagar != null)
                    {
                        _zctEncCuentasPagar.CollectionChanged += FixupZctEncCuentasPagar;
                    }
                    OnNavigationPropertyChanged("ZctEncCuentasPagar");
                }
            }
        }
        private SotCollection<ZctEncCuentasPagar> _zctEncCuentasPagar;
    
        [DataMember]
        public SotCollection<ZctRequisition> ZctRequisition
        {
            get
            {
                if (_zctRequisition == null)
                {
                    _zctRequisition = new SotCollection<ZctRequisition>();
                    _zctRequisition.CollectionChanged += FixupZctRequisition;
                }
                return _zctRequisition;
            }
            set
            {
                if (!ReferenceEquals(_zctRequisition, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_zctRequisition != null)
                    {
                        _zctRequisition.CollectionChanged -= FixupZctRequisition;
                    }
                    _zctRequisition = value;
                    if (_zctRequisition != null)
                    {
                        _zctRequisition.CollectionChanged += FixupZctRequisition;
                    }
                    OnNavigationPropertyChanged("ZctRequisition");
                }
            }
        }
        private SotCollection<ZctRequisition> _zctRequisition;
    
        [DataMember]
        public Proveedor ZctCatProv
        {
            get { return _zctCatProv; }
            set
            {
                if (!ReferenceEquals(_zctCatProv, value))
                {
                    var previousValue = _zctCatProv;
                    _zctCatProv = value;
                    FixupZctCatProv(previousValue);
                    OnNavigationPropertyChanged("ZctCatProv");
                }
            }
        }
        private Proveedor _zctCatProv;
    
        [DataMember]
        public SotCollection<ServicioOrdenCompra> ServiciosOrdenCompra
        {
            get
            {
                if (_serviciosOrdenCompra == null)
                {
                    _serviciosOrdenCompra = new SotCollection<ServicioOrdenCompra>();
                    _serviciosOrdenCompra.CollectionChanged += FixupServiciosOrdenCompra;
                }
                return _serviciosOrdenCompra;
            }
            set
            {
                if (!ReferenceEquals(_serviciosOrdenCompra, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_serviciosOrdenCompra != null)
                    {
                        _serviciosOrdenCompra.CollectionChanged -= FixupServiciosOrdenCompra;
                        // This is the principal end in an association that performs cascade deletes.
                        // Remove the cascade delete event handler for any entities in the current collection.
                        foreach (ServicioOrdenCompra item in _serviciosOrdenCompra)
                        {
                            //ChangeTracker.ObjectStateChanging -= item.HandleCascadeDelete;
                        }
                    }
                    _serviciosOrdenCompra = value;
                    if (_serviciosOrdenCompra != null)
                    {
                        _serviciosOrdenCompra.CollectionChanged += FixupServiciosOrdenCompra;
                        // This is the principal end in an association that performs cascade deletes.
                        // Add the cascade delete event handler for any entities that are already in the new collection.
                        foreach (ServicioOrdenCompra item in _serviciosOrdenCompra)
                        {
                            //ChangeTracker.ObjectStateChanging += item.HandleCascadeDelete;
                        }
                    }
                    OnNavigationPropertyChanged("ServiciosOrdenCompra");
                }
            }
        }
        private SotCollection<ServicioOrdenCompra> _serviciosOrdenCompra;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ZctDetOC.Clear();
            ZctEncCuentasPagar.Clear();
            ZctRequisition.Clear();
            ZctCatProv = null;
            ServiciosOrdenCompra.Clear();
        }

        #endregion

        #region Association Fixup
    
        private void FixupZctCatProv(Proveedor previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.ZctEncOC.Contains(this))
            {
                previousValue.ZctEncOC.Remove(this);
            }
    
            if (ZctCatProv != null)
            {
                if (!ZctCatProv.ZctEncOC.Contains(this))
                {
                    ZctCatProv.ZctEncOC.Add(this);
                }
    
                Cod_Prov = ZctCatProv.Cod_Prov;
            }
            else if (!skipKeys)
            {
                Cod_Prov = null;
            }
    
        }
    
        private void FixupZctDetOC(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (ZctDetOC item in e.NewItems)
                {
                    item.ZctEncOC = this;
                    // This is the principal end in an association that performs cascade deletes.
                    // Update the event listener to refer to the new dependent.
                    //ChangeTracker.ObjectStateChanging += item.HandleCascadeDelete;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ZctDetOC item in e.OldItems)
                {
                    if (ReferenceEquals(item.ZctEncOC, this))
                    {
                        item.ZctEncOC = null;
                    }
                    // This is the principal end in an association that performs cascade deletes.
                    // Remove the previous dependent from the event listener.
                    //ChangeTracker.ObjectStateChanging -= item.HandleCascadeDelete;
                }
            }
        }
    
        private void FixupZctEncCuentasPagar(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (ZctEncCuentasPagar item in e.NewItems)
                {
                    item.ZctEncOC = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ZctEncCuentasPagar item in e.OldItems)
                {
                    if (ReferenceEquals(item.ZctEncOC, this))
                    {
                        item.ZctEncOC = null;
                    }
                }
            }
        }
    
        private void FixupZctRequisition(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (ZctRequisition item in e.NewItems)
                {
                    item.ZctEncOC = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ZctRequisition item in e.OldItems)
                {
                    if (ReferenceEquals(item.ZctEncOC, this))
                    {
                        item.ZctEncOC = null;
                    }
                }
            }
        }
    
        private void FixupServiciosOrdenCompra(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (ServicioOrdenCompra item in e.NewItems)
                {
                    item.Encabezado = this;
                    // This is the principal end in an association that performs cascade deletes.
                    // Update the event listener to refer to the new dependent.
                    //ChangeTracker.ObjectStateChanging += item.HandleCascadeDelete;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ServicioOrdenCompra item in e.OldItems)
                {
                    if (ReferenceEquals(item.Encabezado, this))
                    {
                        item.Encabezado = null;
                    }
                    // This is the principal end in an association that performs cascade deletes.
                    // Remove the previous dependent from the event listener.
                    //ChangeTracker.ObjectStateChanging -= item.HandleCascadeDelete;
                }
            }
        }

        #endregion

    }
}
