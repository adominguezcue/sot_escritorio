//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(ZctCatAut))]
    [KnownType(typeof(Usuario))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class ZctSegAut: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctSegAut";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<ZctSegAut, bool>> Comparador()
        //{
            //return m => m.Id_SegAut == Id_SegAut;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(ZctCatAut != null)
                propiedades.Add(ZctCatAut);
            if(ZctSegUsu != null)
                propiedades.Add(ZctSegUsu);
    
            if(ZctCatAut != null)
                ZctCatAut.ObtenerPropiedadesNavegacion(propiedades);
            if(ZctSegUsu != null)
                ZctSegUsu.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(ZctCatAut != null && !propiedadesT.Contains(ZctCatAut))
                propiedades.Add(ZctCatAut);
            if(ZctSegUsu != null && !propiedadesT.Contains(ZctSegUsu))
                propiedades.Add(ZctSegUsu);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public ZctSegAut()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public ZctSegAut ObtenerCopiaDePrimitivas()
        {
            return new ZctSegAut
            {
                Cod_Aut = this.Cod_Aut,
                Cod_Usu = this.Cod_Usu,
                Desc_SegAut = this.Desc_SegAut,
            };
        }
    
        public void SincronizarPrimitivas(ZctSegAut origen)
        {
            this.Cod_Aut = origen.Cod_Aut;
            this.Cod_Usu = origen.Cod_Usu;
            this.Desc_SegAut = origen.Desc_SegAut;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id_SegAut
        {
            get { return _id_SegAut; }
            set
            {
                if (_id_SegAut != value)
                {
                    _id_SegAut = value;
                    OnPropertyChanged("Id_SegAut");
                }
            }
        }
        private int _id_SegAut;
        [DataMember]
        public int Cod_Aut
        {
            get { return _cod_Aut; }
            set
            {
                if (_cod_Aut != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ZctCatAut != null && ZctCatAut.Cod_Aut != value)
                        {
                            ZctCatAut = null;
                        }
                    }
                    _cod_Aut = value;
                    OnPropertyChanged("Cod_Aut");
                }
            }
        }
        private int _cod_Aut;
        [DataMember]
        public int Cod_Usu
        {
            get { return _cod_Usu; }
            set
            {
                if (_cod_Usu != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ZctSegUsu != null && ZctSegUsu.Cod_Usu != value)
                        {
                            ZctSegUsu = null;
                        }
                    }
                    _cod_Usu = value;
                    OnPropertyChanged("Cod_Usu");
                }
            }
        }
        private int _cod_Usu;
        [DataMember]
        public string Desc_SegAut
        {
            get { return _desc_SegAut; }
            set
            {
                if ((value == null && _desc_SegAut != value) || (value != null && _desc_SegAut != value.Trim()))
                {
                    _desc_SegAut = (value == null ? value : value.Trim());
                    OnPropertyChanged("Desc_SegAut");
                }
            }
        }
        private string _desc_SegAut;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public ZctCatAut ZctCatAut
        {
            get { return _zctCatAut; }
            set
            {
                if (!ReferenceEquals(_zctCatAut, value))
                {
                    var previousValue = _zctCatAut;
                    _zctCatAut = value;
                    FixupZctCatAut(previousValue);
                    OnNavigationPropertyChanged("ZctCatAut");
                }
            }
        }
        private ZctCatAut _zctCatAut;
    
        [DataMember]
        public Usuario ZctSegUsu
        {
            get { return _zctSegUsu; }
            set
            {
                if (!ReferenceEquals(_zctSegUsu, value))
                {
                    var previousValue = _zctSegUsu;
                    _zctSegUsu = value;
                    FixupZctSegUsu(previousValue);
                    OnNavigationPropertyChanged("ZctSegUsu");
                }
            }
        }
        private Usuario _zctSegUsu;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ZctCatAut = null;
            ZctSegUsu = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupZctCatAut(ZctCatAut previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.ZctSegAut.Contains(this))
            {
                previousValue.ZctSegAut.Remove(this);
            }
    
            if (ZctCatAut != null)
            {
                if (!ZctCatAut.ZctSegAut.Contains(this))
                {
                    ZctCatAut.ZctSegAut.Add(this);
                }
    
                Cod_Aut = ZctCatAut.Cod_Aut;
            }
        }
    
        private void FixupZctSegUsu(Usuario previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.ZctSegAut.Contains(this))
            {
                previousValue.ZctSegAut.Remove(this);
            }
    
            if (ZctSegUsu != null)
            {
                if (!ZctSegUsu.ZctSegAut.Contains(this))
                {
                    ZctSegUsu.ZctSegAut.Add(this);
                }
    
                Cod_Usu = ZctSegUsu.Cod_Usu;
            }
        }

        #endregion

    }
}
