//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(LugarEnvio))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class LugarEnvio: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctCatLugarEnvio";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<LugarEnvio, bool>> Comparador()
        //{
            //return m => m.cod_lugar_envio == cod_lugar_envio;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(ZctCatLugarEnvio1 != null)
                propiedades.Add(ZctCatLugarEnvio1);
            if(ZctCatLugarEnvio2 != null)
                propiedades.Add(ZctCatLugarEnvio2);
    
            if(ZctCatLugarEnvio1 != null)
                ZctCatLugarEnvio1.ObtenerPropiedadesNavegacion(propiedades);
            if(ZctCatLugarEnvio2 != null)
                ZctCatLugarEnvio2.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(ZctCatLugarEnvio1 != null && !propiedadesT.Contains(ZctCatLugarEnvio1))
                propiedades.Add(ZctCatLugarEnvio1);
            if(ZctCatLugarEnvio2 != null && !propiedadesT.Contains(ZctCatLugarEnvio2))
                propiedades.Add(ZctCatLugarEnvio2);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public LugarEnvio()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public LugarEnvio ObtenerCopiaDePrimitivas()
        {
            return new LugarEnvio
            {
                desc_lugar_envio = this.desc_lugar_envio,
            };
        }
    
        public void SincronizarPrimitivas(LugarEnvio origen)
        {
            this.desc_lugar_envio = origen.desc_lugar_envio;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public string cod_lugar_envio
        {
            get { return _cod_lugar_envio; }
            set
            {
                if ((value == null && _cod_lugar_envio != value) || (value != null && _cod_lugar_envio != value.Trim()))
                {
                    if (!IsDeserializing)
                    {
                        if (ZctCatLugarEnvio2 != null && ZctCatLugarEnvio2.cod_lugar_envio != (value == null ? value : value.Trim()))
                        {
                            ZctCatLugarEnvio2 = null;
                        }
                    }
                    _cod_lugar_envio = (value == null ? value : value.Trim());
                    OnPropertyChanged("cod_lugar_envio");
                }
            }
        }
        private string _cod_lugar_envio;
        [DataMember]
        public string desc_lugar_envio
        {
            get { return _desc_lugar_envio; }
            set
            {
                if ((value == null && _desc_lugar_envio != value) || (value != null && _desc_lugar_envio != value.Trim()))
                {
                    _desc_lugar_envio = (value == null ? value : value.Trim());
                    OnPropertyChanged("desc_lugar_envio");
                }
            }
        }
        private string _desc_lugar_envio;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public LugarEnvio ZctCatLugarEnvio1
        {
            get { return _zctCatLugarEnvio1; }
            set
            {
                if (!ReferenceEquals(_zctCatLugarEnvio1, value))
                {
                    var previousValue = _zctCatLugarEnvio1;
                    _zctCatLugarEnvio1 = value;
                    FixupZctCatLugarEnvio1(previousValue);
                    OnNavigationPropertyChanged("ZctCatLugarEnvio1");
                }
            }
        }
        private LugarEnvio _zctCatLugarEnvio1;
    
        [DataMember]
        public LugarEnvio ZctCatLugarEnvio2
        {
            get { return _zctCatLugarEnvio2; }
            set
            {
                if (!ReferenceEquals(_zctCatLugarEnvio2, value))
                {
                    /*if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added && value != null)
                    {
                        // This the dependent end of an identifying relationship, so the principal end cannot be changed if it is already set,
                        // otherwise it can only be set to an entity with a primary key that is the same value as the dependent's foreign key.
                        if (cod_lugar_envio != value.cod_lugar_envio)
                        {
                            throw new InvalidOperationException("The principal end of an identifying relationship can only be changed when the dependent end is in the Added state.");
                        }
                    }*/
                    var previousValue = _zctCatLugarEnvio2;
                    _zctCatLugarEnvio2 = value;
                    FixupZctCatLugarEnvio2(previousValue);
                    OnNavigationPropertyChanged("ZctCatLugarEnvio2");
                }
            }
        }
        private LugarEnvio _zctCatLugarEnvio2;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        // This entity type is the dependent end in at least one association that performs cascade deletes.
        // This event handler will process notifications that occur when the principal end is deleted.
        /*internal void HandleCascadeDelete(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                this.MarkAsDeleted();
            }
        }*/
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ZctCatLugarEnvio1 = null;
            ZctCatLugarEnvio2 = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupZctCatLugarEnvio1(LugarEnvio previousValue)
        {
            // This is the principal end in an association that performs cascade deletes.
            // Update the event listener to refer to the new dependent.
            if (previousValue != null)
            {
                //ChangeTracker.ObjectStateChanging -= previousValue.HandleCascadeDelete;
            }
    
            if (ZctCatLugarEnvio1 != null)
            {
                //ChangeTracker.ObjectStateChanging += ZctCatLugarEnvio1.HandleCascadeDelete;
            }
    
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && ReferenceEquals(previousValue.ZctCatLugarEnvio2, this))
            {
                previousValue.ZctCatLugarEnvio2 = null;
            }
    
            if (ZctCatLugarEnvio1 != null)
            {
                ZctCatLugarEnvio1.ZctCatLugarEnvio2 = this;
            }
    
        }
    
        private void FixupZctCatLugarEnvio2(LugarEnvio previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && ReferenceEquals(previousValue.ZctCatLugarEnvio1, this))
            {
                previousValue.ZctCatLugarEnvio1 = null;
            }
    
            if (ZctCatLugarEnvio2 != null)
            {
                ZctCatLugarEnvio2.ZctCatLugarEnvio1 = this;
                cod_lugar_envio = ZctCatLugarEnvio2.cod_lugar_envio;
            }
    
        }

        #endregion

    }
}
