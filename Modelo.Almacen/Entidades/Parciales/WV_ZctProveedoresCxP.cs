﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class WV_ZctProveedoresCxP
    {
        public decimal SubtotalNN
        {
            get { return subtotal ?? 0; }
            set { subtotal = value; }
        }
        public decimal IVANN
        {
            get { return iva ?? 0; }
            set { iva = value; }
        }
        public decimal TotalNN
        {
            get { return total ?? 0; }
            set { total = value; }
        }
    }
}
