﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctDetOC
    {

        public string ArticuloNombre
        {
            get
            {
                //try
                //{
                return this.ZctCatArt.Desc_Art;
                //}
                //catch (Exception ex)
                //{
                //    return "";
                //}
            }
        }
        public Decimal Importe
        {
            get
            {
                return Decimal.Round((decimal)this.Cos_Art * (decimal)this.Ctd_Art, 2);
            }
        }
        public string ArticuloMarca
        {
            get
            {
                //try
                //{
                return this.ZctCatArt.ZctCatMar.Desc_Mar;
                //}
                //catch (Exception ex)
                //{
                //    return "";
                //}
            }
        }

        public decimal NN_Ctd_Art
        {
            get { return Ctd_Art ?? 0; }
        }

        public decimal NN_Cos_Art
        {
            get { return Cos_Art ?? 0; }
        }

        public decimal NN_CtdStdDet_OC
        {
            get { return CtdStdDet_OC ?? 0; }
        }

        public int NN_Cat_Alm
        {
            get { return Cat_Alm ?? 0; }
        }
    }
}
