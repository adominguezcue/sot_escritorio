﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class Linea
    {
        public LineasBase? LineaBase 
        {
            get { return IdLineaBase.HasValue ? (LineasBase)IdLineaBase : default(LineasBase?); }
            set { IdLineaBase = value.HasValue ? (int)value : default(int?); }
        }

        [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
        public enum LineasBase
        {
            Alimentos = 1,
            Bebidas = 2,
            [Description("Spa & Sex")]
            SpaYSex = 3
        }

        public string NombreDepartamentoTmp { get; set; }
    }
}
