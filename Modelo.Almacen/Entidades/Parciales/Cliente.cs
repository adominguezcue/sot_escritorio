﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class Cliente
    {
        public TiposPersonas? TipoPersona
        {
            get { return (TiposPersonas?)tipo_persona; }
            set { tipo_persona = (int?)value; }
        }

        public string RazonSocial
        {
            get { return Nom_Cte; }
        }
    }
}
