﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class Ficha
    {
        public string ActivoTmp { get; set; }
        public string RFCProveedorTmp { get; set; }
        public string NombreProveedorTmp { get; set; }
    }
}
