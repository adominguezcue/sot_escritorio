﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class VW_ZctInventarioFinal
    {
        public int Exist_ArtNN
        {
            get { return Exist_Art ?? 0; }
        }
        public decimal CostoProm_ArtNN
        {
            get { return CostoProm_Art ?? 0; }
        }
        public int Conteo3_ArtNN
        {
            get { return Conteo3_Art ?? 0; }
        }
        public int Diferencia_ArtNN
        {
            get { return Diferencia_Art ?? 0; }
        }
        public DateTime FchyHrRegNN
        {
            get { return FchyHrReg ?? DateTime.MinValue; }
        }
    }
}
