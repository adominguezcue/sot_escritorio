﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctEncCuentasPagar
    {
        public Estados Estado
        {
            get { return (Estados)idEstado; }
            set { idEstado = (int)value; }
        }

        public enum Estados
        {
            PENDIENTE = 1,
            PAGADO = 2,
            CANCELADO = 3
        }

        public bool EsVencida { get { return Estado == Estados.PENDIENTE && fecha_vencido < DateTime.Now; } }

        public string FacturaTMP { get; set; }
        public string NombreProveedorTMP { get; set; }
    }
}
