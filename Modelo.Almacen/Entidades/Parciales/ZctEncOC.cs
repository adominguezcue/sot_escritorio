﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctEncOC
    {

        public static class EstatusOrdenesCompra
        {
            public const string CANCELADO = "CANCELADO";
            public const string CERRADA = "CERRADA";
            public const string APLICADO = "APLICADO";
            public const string OC_NUEVA = "OC NUEVA";
        }
    }
}
