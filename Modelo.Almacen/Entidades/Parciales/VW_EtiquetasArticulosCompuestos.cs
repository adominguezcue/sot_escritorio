﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class VW_EtiquetasArticulosCompuestos
    {
        public DateTime FechaElaboracionNoNull 
        {
            get { return FechaElaboracion.Value; }
            set { FechaElaboracion = value; }
        }
    }
}
