﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctCatTipoPago
    {
        public ClasificacionesGastos? ClasificacionGasto
        {
            get { return IdClasificacionGasto.HasValue ? (ClasificacionesGastos?)IdClasificacionGasto : null; }
            set { IdClasificacionGasto = (int?)value; }
        }

    }
}
