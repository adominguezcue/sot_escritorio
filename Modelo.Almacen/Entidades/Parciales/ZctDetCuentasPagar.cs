﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctDetCuentasPagar
    {
        public ZctEncCuentasPagar.Estados Estado 
        {
            get { return (ZctEncCuentasPagar.Estados)idEstado; }
            set { idEstado = (int)value; }
        }
    }
}
