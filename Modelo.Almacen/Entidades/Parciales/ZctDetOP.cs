﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctDetOP
    {
        [DataMember]
        public string DescArt { get; set; }

        public decimal SubTotal { get; set; }
    }
}
