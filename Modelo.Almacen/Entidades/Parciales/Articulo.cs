﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class Articulo
    {
        [DataMember]
        public string NombreLineaTmp
        {
            get { return ZctCatLinea != null ? ZctCatLinea.Desc_Linea : ""; }
        }
        [DataMember]
        public decimal PrecioConIVA { get; set; }
        [DataMember]
        public decimal CostoFinal { get; set; }
        [DataMember]
        public string NombreTipoTmp { get; set; }
        [DataMember]
        public string NombreMarcaTmp { get; set; }
        [DataMember]
        public string NombreDepartamentoTmp { get; set; }
        [DataMember]
        public string NombreRefaccionTmp { get; set; }

        public List<Dtos.DtoArticuloPresentacion> PresentacionesArticulos { get; set; }

        public decimal ExistenciasTMP { get; set; }

        public decimal CostoAcumuladoTMP { get; set; }
    }
}
