﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class VW_ZctEncCuentasPagar
    {
        public Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados Estado
        {
            get { return (Modelo.Almacen.Entidades.ZctEncCuentasPagar.Estados)idEstado; }
            set { idEstado = (int)value; }
        }
    }
}
