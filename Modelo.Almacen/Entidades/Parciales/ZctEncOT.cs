﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctEncOT
    {
        public static class Estados
        {
            public const string Aprobada = "A";
            public const string Cancelada = "C";
            public const string Pendiente = "N";
        }
    }
}