﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class Proveedor
    {
        public string DireccionAutomatica
        {
            get { return string.Format("{0} No {1} Col. {2}, CP {3}, {4}, {5}", calle, numext, colonia, cp, estado, ciudad); }
        }

        public int NN_dias_credito
        {
            get { return dias_credito ?? 0; }
        }
    }
}
