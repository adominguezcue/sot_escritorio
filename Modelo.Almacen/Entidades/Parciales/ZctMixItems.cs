﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctMixItems
    {
        [DataMember]
        public string DescArt { get; set; }
        [DataMember]
        public string DescArtPadreTMP { get; set; }

        public string NombreUnidadPadreTMP { get; set; }
        public string NombreUnidadTMP { get; set; }
        public string NombrePresentacionTMP { get; set; }
        //[DataMember]
        //public string Porcion { get; set; }
 
        [DataMember]
        [Description("Costo promedio")]
        public decimal Cost { get; set; }
        public decimal Exist { get; set; }

        public decimal CostoArt { get; set; }
    }
}
