﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Entidades
{
    public partial class ZctDetalleUbicacionValores
    {
        string _DescripcionUbicacion;
        public string DescripcionUbicacion
        {
            get
            {
                if (this.ZctCatUbicacionValores == null)
                {
                    return _DescripcionUbicacion;
                }
                return this.ZctCatUbicacionValores.descripcion;
            }
            set
            {
                _DescripcionUbicacion = value;
            }
        }
    }
}
