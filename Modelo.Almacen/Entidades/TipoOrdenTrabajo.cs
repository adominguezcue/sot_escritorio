//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(TipoOrdenTrabajo))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class TipoOrdenTrabajo: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctCatTpOT";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<TipoOrdenTrabajo, bool>> Comparador()
        //{
            //return m => m.Cod_TpOT == Cod_TpOT;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(ZctCatTpOT1 != null)
                propiedades.Add(ZctCatTpOT1);
            if(ZctCatTpOT2 != null)
                propiedades.Add(ZctCatTpOT2);
    
            if(ZctCatTpOT1 != null)
                ZctCatTpOT1.ObtenerPropiedadesNavegacion(propiedades);
            if(ZctCatTpOT2 != null)
                ZctCatTpOT2.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(ZctCatTpOT1 != null && !propiedadesT.Contains(ZctCatTpOT1))
                propiedades.Add(ZctCatTpOT1);
            if(ZctCatTpOT2 != null && !propiedadesT.Contains(ZctCatTpOT2))
                propiedades.Add(ZctCatTpOT2);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public TipoOrdenTrabajo()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public TipoOrdenTrabajo ObtenerCopiaDePrimitivas()
        {
            return new TipoOrdenTrabajo
            {
                Desc_TpOT = this.Desc_TpOT,
            };
        }
    
        public void SincronizarPrimitivas(TipoOrdenTrabajo origen)
        {
            this.Desc_TpOT = origen.Desc_TpOT;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Cod_TpOT
        {
            get { return _cod_TpOT; }
            set
            {
                if (_cod_TpOT != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ZctCatTpOT2 != null && ZctCatTpOT2.Cod_TpOT != value)
                        {
                            ZctCatTpOT2 = null;
                        }
                    }
                    _cod_TpOT = value;
                    OnPropertyChanged("Cod_TpOT");
                }
            }
        }
        private int _cod_TpOT;
        [DataMember]
        public string Desc_TpOT
        {
            get { return _desc_TpOT; }
            set
            {
                if ((value == null && _desc_TpOT != value) || (value != null && _desc_TpOT != value.Trim()))
                {
                    _desc_TpOT = (value == null ? value : value.Trim());
                    OnPropertyChanged("Desc_TpOT");
                }
            }
        }
        private string _desc_TpOT;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public TipoOrdenTrabajo ZctCatTpOT1
        {
            get { return _zctCatTpOT1; }
            set
            {
                if (!ReferenceEquals(_zctCatTpOT1, value))
                {
                    var previousValue = _zctCatTpOT1;
                    _zctCatTpOT1 = value;
                    FixupZctCatTpOT1(previousValue);
                    OnNavigationPropertyChanged("ZctCatTpOT1");
                }
            }
        }
        private TipoOrdenTrabajo _zctCatTpOT1;
    
        [DataMember]
        public TipoOrdenTrabajo ZctCatTpOT2
        {
            get { return _zctCatTpOT2; }
            set
            {
                if (!ReferenceEquals(_zctCatTpOT2, value))
                {
                    /*if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added && value != null)
                    {
                        // This the dependent end of an identifying relationship, so the principal end cannot be changed if it is already set,
                        // otherwise it can only be set to an entity with a primary key that is the same value as the dependent's foreign key.
                        if (Cod_TpOT != value.Cod_TpOT)
                        {
                            throw new InvalidOperationException("The principal end of an identifying relationship can only be changed when the dependent end is in the Added state.");
                        }
                    }*/
                    var previousValue = _zctCatTpOT2;
                    _zctCatTpOT2 = value;
                    FixupZctCatTpOT2(previousValue);
                    OnNavigationPropertyChanged("ZctCatTpOT2");
                }
            }
        }
        private TipoOrdenTrabajo _zctCatTpOT2;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        // This entity type is the dependent end in at least one association that performs cascade deletes.
        // This event handler will process notifications that occur when the principal end is deleted.
        /*internal void HandleCascadeDelete(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                this.MarkAsDeleted();
            }
        }*/
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ZctCatTpOT1 = null;
            ZctCatTpOT2 = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupZctCatTpOT1(TipoOrdenTrabajo previousValue)
        {
            // This is the principal end in an association that performs cascade deletes.
            // Update the event listener to refer to the new dependent.
            if (previousValue != null)
            {
                //ChangeTracker.ObjectStateChanging -= previousValue.HandleCascadeDelete;
            }
    
            if (ZctCatTpOT1 != null)
            {
                //ChangeTracker.ObjectStateChanging += ZctCatTpOT1.HandleCascadeDelete;
            }
    
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && ReferenceEquals(previousValue.ZctCatTpOT2, this))
            {
                previousValue.ZctCatTpOT2 = null;
            }
    
            if (ZctCatTpOT1 != null)
            {
                ZctCatTpOT1.ZctCatTpOT2 = this;
            }
    
        }
    
        private void FixupZctCatTpOT2(TipoOrdenTrabajo previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && ReferenceEquals(previousValue.ZctCatTpOT1, this))
            {
                previousValue.ZctCatTpOT1 = null;
            }
    
            if (ZctCatTpOT2 != null)
            {
                ZctCatTpOT2.ZctCatTpOT1 = this;
                Cod_TpOT = ZctCatTpOT2.Cod_TpOT;
            }
    
        }

        #endregion

    }
}
