//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(ZctDetOP))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class ZctEncOP: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctEncOP";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<ZctEncOP, bool>> Comparador()
        //{
            //return m => m.Cod_OP == Cod_OP;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            foreach(var item in ZctDetOP)
                if(item != null)
                    propiedades.Add(item);
    
            foreach(var item in ZctDetOP)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            foreach(var item in ZctDetOP)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public ZctEncOP()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public ZctEncOP ObtenerCopiaDePrimitivas()
        {
            return new ZctEncOP
            {
                Fch_OP = this.Fch_OP,
                Type_OP = this.Type_OP,
                Notes = this.Notes,
            };
        }
    
        public void SincronizarPrimitivas(ZctEncOP origen)
        {
            this.Fch_OP = origen.Fch_OP;
            this.Type_OP = origen.Type_OP;
            this.Notes = origen.Notes;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Cod_OP
        {
            get { return _cod_OP; }
            set
            {
                if (_cod_OP != value)
                {
                    _cod_OP = value;
                    OnPropertyChanged("Cod_OP");
                }
            }
        }
        private int _cod_OP;
        [DataMember]
        public System.DateTime Fch_OP
        {
            get { return _fch_OP; }
            set
            {
                if (_fch_OP != value)
                {
                    _fch_OP = value;
                    OnPropertyChanged("Fch_OP");
                }
            }
        }
        private System.DateTime _fch_OP;
        [DataMember]
        public string Type_OP
        {
            get { return _type_OP; }
            set
            {
                if ((value == null && _type_OP != value) || (value != null && _type_OP != value.Trim()))
                {
                    _type_OP = (value == null ? value : value.Trim());
                    OnPropertyChanged("Type_OP");
                }
            }
        }
        private string _type_OP;
        [DataMember]
        public string Notes
        {
            get { return _notes; }
            set
            {
                if ((value == null && _notes != value) || (value != null && _notes != value.Trim()))
                {
                    _notes = (value == null ? value : value.Trim());
                    OnPropertyChanged("Notes");
                }
            }
        }
        private string _notes;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public SotCollection<ZctDetOP> ZctDetOP
        {
            get
            {
                if (_zctDetOP == null)
                {
                    _zctDetOP = new SotCollection<ZctDetOP>();
                    _zctDetOP.CollectionChanged += FixupZctDetOP;
                }
                return _zctDetOP;
            }
            set
            {
                if (!ReferenceEquals(_zctDetOP, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_zctDetOP != null)
                    {
                        _zctDetOP.CollectionChanged -= FixupZctDetOP;
                    }
                    _zctDetOP = value;
                    if (_zctDetOP != null)
                    {
                        _zctDetOP.CollectionChanged += FixupZctDetOP;
                    }
                    OnNavigationPropertyChanged("ZctDetOP");
                }
            }
        }
        private SotCollection<ZctDetOP> _zctDetOP;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ZctDetOP.Clear();
        }

        #endregion

        #region Association Fixup
    
        private void FixupZctDetOP(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (ZctDetOP item in e.NewItems)
                {
                    item.ZctEncOP = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (ZctDetOP item in e.OldItems)
                {
                    if (ReferenceEquals(item.ZctEncOP, this))
                    {
                        item.ZctEncOP = null;
                    }
                }
            }
        }

        #endregion

    }
}
