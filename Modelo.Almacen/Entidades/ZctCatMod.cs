//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class ZctCatMod: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctCatMod";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<ZctCatMod, bool>> Comparador()
        //{
            //return m => m.CodMod_Mot == CodMod_Mot;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
    
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public ZctCatMod()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public ZctCatMod ObtenerCopiaDePrimitivas()
        {
            return new ZctCatMod
            {
                Cod_Mar = this.Cod_Mar,
                Desc_Mod = this.Desc_Mod,
            };
        }
    
        public void SincronizarPrimitivas(ZctCatMod origen)
        {
            this.Cod_Mar = origen.Cod_Mar;
            this.Desc_Mod = origen.Desc_Mod;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int CodMod_Mot
        {
            get { return _codMod_Mot; }
            set
            {
                if (_codMod_Mot != value)
                {
                    _codMod_Mot = value;
                    OnPropertyChanged("CodMod_Mot");
                }
            }
        }
        private int _codMod_Mot;
        [DataMember]
        public int Cod_Mar
        {
            get { return _cod_Mar; }
            set
            {
                if (_cod_Mar != value)
                {
                    _cod_Mar = value;
                    OnPropertyChanged("Cod_Mar");
                }
            }
        }
        private int _cod_Mar;
        [DataMember]
        public string Desc_Mod
        {
            get { return _desc_Mod; }
            set
            {
                if ((value == null && _desc_Mod != value) || (value != null && _desc_Mod != value.Trim()))
                {
                    _desc_Mod = (value == null ? value : value.Trim());
                    OnPropertyChanged("Desc_Mod");
                }
            }
        }
        private string _desc_Mod;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
        }

        #endregion

    }
}
