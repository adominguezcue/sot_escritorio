//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Ciudad))]
    [KnownType(typeof(Estado))]
    [KnownType(typeof(Folio))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class Emisor: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctCatEmisor";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<Emisor, bool>> Comparador()
        //{
            //return m => m.IdEmisor == IdEmisor;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(ZctCatCiu != null)
                propiedades.Add(ZctCatCiu);
            if(ZctCatEdo != null)
                propiedades.Add(ZctCatEdo);
            foreach(var item in ZctCatfolios)
                if(item != null)
                    propiedades.Add(item);
    
            if(ZctCatCiu != null)
                ZctCatCiu.ObtenerPropiedadesNavegacion(propiedades);
            if(ZctCatEdo != null)
                ZctCatEdo.ObtenerPropiedadesNavegacion(propiedades);
            foreach(var item in ZctCatfolios)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(ZctCatCiu != null && !propiedadesT.Contains(ZctCatCiu))
                propiedades.Add(ZctCatCiu);
            if(ZctCatEdo != null && !propiedadesT.Contains(ZctCatEdo))
                propiedades.Add(ZctCatEdo);
            foreach(var item in ZctCatfolios)
                if(item != null && !propiedadesT.Contains(item))
                    propiedades.Add(item);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public Emisor()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public Emisor ObtenerCopiaDePrimitivas()
        {
            return new Emisor
            {
                Nombre = this.Nombre,
                RFC = this.RFC,
                Calle = this.Calle,
                CP = this.CP,
                Colonia = this.Colonia,
                Cod_Edo = this.Cod_Edo,
                Cod_Ciu = this.Cod_Ciu,
                NoExterior = this.NoExterior,
                NoInterior = this.NoInterior,
                localidad = this.localidad,
                Pais = this.Pais,
                Emisor_default = this.Emisor_default,
                regimen = this.regimen,
            };
        }
    
        public void SincronizarPrimitivas(Emisor origen)
        {
            this.Nombre = origen.Nombre;
            this.RFC = origen.RFC;
            this.Calle = origen.Calle;
            this.CP = origen.CP;
            this.Colonia = origen.Colonia;
            this.Cod_Edo = origen.Cod_Edo;
            this.Cod_Ciu = origen.Cod_Ciu;
            this.NoExterior = origen.NoExterior;
            this.NoInterior = origen.NoInterior;
            this.localidad = origen.localidad;
            this.Pais = origen.Pais;
            this.Emisor_default = origen.Emisor_default;
            this.regimen = origen.regimen;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int IdEmisor
        {
            get { return _idEmisor; }
            set
            {
                if (_idEmisor != value)
                {
                    _idEmisor = value;
                    OnPropertyChanged("IdEmisor");
                }
            }
        }
        private int _idEmisor;
        [DataMember]
        public string Nombre
        {
            get { return _nombre; }
            set
            {
                if ((value == null && _nombre != value) || (value != null && _nombre != value.Trim()))
                {
                    _nombre = (value == null ? value : value.Trim());
                    OnPropertyChanged("Nombre");
                }
            }
        }
        private string _nombre;
        [DataMember]
        public string RFC
        {
            get { return _rFC; }
            set
            {
                if ((value == null && _rFC != value) || (value != null && _rFC != value.Trim()))
                {
                    _rFC = (value == null ? value : value.Trim());
                    OnPropertyChanged("RFC");
                }
            }
        }
        private string _rFC;
        [DataMember]
        public string Calle
        {
            get { return _calle; }
            set
            {
                if ((value == null && _calle != value) || (value != null && _calle != value.Trim()))
                {
                    _calle = (value == null ? value : value.Trim());
                    OnPropertyChanged("Calle");
                }
            }
        }
        private string _calle;
        [DataMember]
        public Nullable<int> CP
        {
            get { return _cP; }
            set
            {
                if (_cP != value)
                {
                    _cP = value;
                    OnPropertyChanged("CP");
                }
            }
        }
        private Nullable<int> _cP;
        [DataMember]
        public string Colonia
        {
            get { return _colonia; }
            set
            {
                if ((value == null && _colonia != value) || (value != null && _colonia != value.Trim()))
                {
                    _colonia = (value == null ? value : value.Trim());
                    OnPropertyChanged("Colonia");
                }
            }
        }
        private string _colonia;
        [DataMember]
        public Nullable<int> Cod_Edo
        {
            get { return _cod_Edo; }
            set
            {
                if (_cod_Edo != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ZctCatEdo != null && ZctCatEdo.Cod_Edo != value)
                        {
                            ZctCatEdo = null;
                        }
                    }
                    _cod_Edo = value;
                    OnPropertyChanged("Cod_Edo");
                }
            }
        }
        private Nullable<int> _cod_Edo;
        [DataMember]
        public Nullable<int> Cod_Ciu
        {
            get { return _cod_Ciu; }
            set
            {
                if (_cod_Ciu != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ZctCatCiu != null && ZctCatCiu.Cod_Ciu != value)
                        {
                            ZctCatCiu = null;
                        }
                    }
                    _cod_Ciu = value;
                    OnPropertyChanged("Cod_Ciu");
                }
            }
        }
        private Nullable<int> _cod_Ciu;
        [DataMember]
        public string NoExterior
        {
            get { return _noExterior; }
            set
            {
                if ((value == null && _noExterior != value) || (value != null && _noExterior != value.Trim()))
                {
                    _noExterior = (value == null ? value : value.Trim());
                    OnPropertyChanged("NoExterior");
                }
            }
        }
        private string _noExterior;
        [DataMember]
        public string NoInterior
        {
            get { return _noInterior; }
            set
            {
                if ((value == null && _noInterior != value) || (value != null && _noInterior != value.Trim()))
                {
                    _noInterior = (value == null ? value : value.Trim());
                    OnPropertyChanged("NoInterior");
                }
            }
        }
        private string _noInterior;
        [DataMember]
        public string localidad
        {
            get { return _localidad; }
            set
            {
                if ((value == null && _localidad != value) || (value != null && _localidad != value.Trim()))
                {
                    _localidad = (value == null ? value : value.Trim());
                    OnPropertyChanged("localidad");
                }
            }
        }
        private string _localidad;
        [DataMember]
        public string Pais
        {
            get { return _pais; }
            set
            {
                if ((value == null && _pais != value) || (value != null && _pais != value.Trim()))
                {
                    _pais = (value == null ? value : value.Trim());
                    OnPropertyChanged("Pais");
                }
            }
        }
        private string _pais;
        [DataMember]
        public Nullable<bool> Emisor_default
        {
            get { return _emisor_default; }
            set
            {
                if (_emisor_default != value)
                {
                    _emisor_default = value;
                    OnPropertyChanged("Emisor_default");
                }
            }
        }
        private Nullable<bool> _emisor_default;
        [DataMember]
        public string regimen
        {
            get { return _regimen; }
            set
            {
                if ((value == null && _regimen != value) || (value != null && _regimen != value.Trim()))
                {
                    _regimen = (value == null ? value : value.Trim());
                    OnPropertyChanged("regimen");
                }
            }
        }
        private string _regimen;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public Ciudad ZctCatCiu
        {
            get { return _zctCatCiu; }
            set
            {
                if (!ReferenceEquals(_zctCatCiu, value))
                {
                    var previousValue = _zctCatCiu;
                    _zctCatCiu = value;
                    FixupZctCatCiu(previousValue);
                    OnNavigationPropertyChanged("ZctCatCiu");
                }
            }
        }
        private Ciudad _zctCatCiu;
    
        [DataMember]
        public Estado ZctCatEdo
        {
            get { return _zctCatEdo; }
            set
            {
                if (!ReferenceEquals(_zctCatEdo, value))
                {
                    var previousValue = _zctCatEdo;
                    _zctCatEdo = value;
                    FixupZctCatEdo(previousValue);
                    OnNavigationPropertyChanged("ZctCatEdo");
                }
            }
        }
        private Estado _zctCatEdo;
    
        [DataMember]
        public SotCollection<Folio> ZctCatfolios
        {
            get
            {
                if (_zctCatfolios == null)
                {
                    _zctCatfolios = new SotCollection<Folio>();
                    _zctCatfolios.CollectionChanged += FixupZctCatfolios;
                }
                return _zctCatfolios;
            }
            set
            {
                if (!ReferenceEquals(_zctCatfolios, value))
                {
                    /*
    				if (ChangeTracker.ChangeTrackingEnabled && value != null)
                    {
                        throw new InvalidOperationException("Cannot set the FixupChangeTrackingCollection when ChangeTracking is enabled");
                    }
    				*/
                    if (_zctCatfolios != null)
                    {
                        _zctCatfolios.CollectionChanged -= FixupZctCatfolios;
                    }
                    _zctCatfolios = value;
                    if (_zctCatfolios != null)
                    {
                        _zctCatfolios.CollectionChanged += FixupZctCatfolios;
                    }
                    OnNavigationPropertyChanged("ZctCatfolios");
                }
            }
        }
        private SotCollection<Folio> _zctCatfolios;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ZctCatCiu = null;
            ZctCatEdo = null;
            ZctCatfolios.Clear();
        }

        #endregion

        #region Association Fixup
    
        private void FixupZctCatCiu(Ciudad previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.ZctCatEmisor.Contains(this))
            {
                previousValue.ZctCatEmisor.Remove(this);
            }
    
            if (ZctCatCiu != null)
            {
                if (!ZctCatCiu.ZctCatEmisor.Contains(this))
                {
                    ZctCatCiu.ZctCatEmisor.Add(this);
                }
    
                Cod_Ciu = ZctCatCiu.Cod_Ciu;
            }
            else if (!skipKeys)
            {
                Cod_Ciu = null;
            }
    
        }
    
        private void FixupZctCatEdo(Estado previousValue, bool skipKeys = false)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.ZctCatEmisor.Contains(this))
            {
                previousValue.ZctCatEmisor.Remove(this);
            }
    
            if (ZctCatEdo != null)
            {
                if (!ZctCatEdo.ZctCatEmisor.Contains(this))
                {
                    ZctCatEdo.ZctCatEmisor.Add(this);
                }
    
                Cod_Edo = ZctCatEdo.Cod_Edo;
            }
            else if (!skipKeys)
            {
                Cod_Edo = null;
            }
    
        }
    
        private void FixupZctCatfolios(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (e.NewItems != null)
            {
                foreach (Folio item in e.NewItems)
                {
                    item.ZctCatEmisor = this;
                }
            }
    
            if (e.OldItems != null)
            {
                foreach (Folio item in e.OldItems)
                {
                    if (ReferenceEquals(item.ZctCatEmisor, this))
                    {
                        item.ZctCatEmisor = null;
                    }
                }
            }
        }

        #endregion

    }
}
