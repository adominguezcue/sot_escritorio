//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(ZctFacturaEnc))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class ZctFacturaComprobante: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctFacturaComprobante";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<ZctFacturaComprobante, bool>> Comparador()
        //{
            //return m => m.folio_factura == folio_factura;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(ZctFacturaEnc != null)
                propiedades.Add(ZctFacturaEnc);
    
            if(ZctFacturaEnc != null)
                ZctFacturaEnc.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(ZctFacturaEnc != null && !propiedadesT.Contains(ZctFacturaEnc))
                propiedades.Add(ZctFacturaEnc);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public ZctFacturaComprobante()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public ZctFacturaComprobante ObtenerCopiaDePrimitivas()
        {
            return new ZctFacturaComprobante
            {
                cod_folio = this.cod_folio,
                fecha_factura = this.fecha_factura,
                cadena_factura = this.cadena_factura,
                sello_factura = this.sello_factura,
                status_factura = this.status_factura,
                fyh_mod = this.fyh_mod,
                folio_fiscal_factura = this.folio_fiscal_factura,
                fyh_fiscal = this.fyh_fiscal,
                sello_digital_emisor = this.sello_digital_emisor,
                sello_digital_sat = this.sello_digital_sat,
                QR = this.QR,
            };
        }
    
        public void SincronizarPrimitivas(ZctFacturaComprobante origen)
        {
            this.cod_folio = origen.cod_folio;
            this.fecha_factura = origen.fecha_factura;
            this.cadena_factura = origen.cadena_factura;
            this.sello_factura = origen.sello_factura;
            this.status_factura = origen.status_factura;
            this.fyh_mod = origen.fyh_mod;
            this.folio_fiscal_factura = origen.folio_fiscal_factura;
            this.fyh_fiscal = origen.fyh_fiscal;
            this.sello_digital_emisor = origen.sello_digital_emisor;
            this.sello_digital_sat = origen.sello_digital_sat;
            this.QR = origen.QR;
        }

        #endregion

        #region Primitive Properties
        [DataMember]
        public string cod_folio
        {
            get { return _cod_folio; }
            set
            {
                if ((value == null && _cod_folio != value) || (value != null && _cod_folio != value.Trim()))
                {
                    if (!IsDeserializing)
                    {
                        if (ZctFacturaEnc != null && ZctFacturaEnc.cod_folio != (value == null ? value : value.Trim()))
                        {
                            ZctFacturaEnc = null;
                        }
                    }
                    _cod_folio = (value == null ? value : value.Trim());
                    OnPropertyChanged("cod_folio");
                }
            }
        }
        private string _cod_folio;
    	[Key]
        [DataMember]
        public int folio_factura
        {
            get { return _folio_factura; }
            set
            {
                if (_folio_factura != value)
                {
                    if (!IsDeserializing)
                    {
                        if (ZctFacturaEnc != null && ZctFacturaEnc.folio_factura != value)
                        {
                            ZctFacturaEnc = null;
                        }
                    }
                    _folio_factura = value;
                    OnPropertyChanged("folio_factura");
                }
            }
        }
        private int _folio_factura;
        [DataMember]
        public System.DateTime fecha_factura
        {
            get { return _fecha_factura; }
            set
            {
                if (_fecha_factura != value)
                {
                    _fecha_factura = value;
                    OnPropertyChanged("fecha_factura");
                }
            }
        }
        private System.DateTime _fecha_factura;
        [DataMember]
        public string cadena_factura
        {
            get { return _cadena_factura; }
            set
            {
                if ((value == null && _cadena_factura != value) || (value != null && _cadena_factura != value.Trim()))
                {
                    _cadena_factura = (value == null ? value : value.Trim());
                    OnPropertyChanged("cadena_factura");
                }
            }
        }
        private string _cadena_factura;
        [DataMember]
        public string sello_factura
        {
            get { return _sello_factura; }
            set
            {
                if ((value == null && _sello_factura != value) || (value != null && _sello_factura != value.Trim()))
                {
                    _sello_factura = (value == null ? value : value.Trim());
                    OnPropertyChanged("sello_factura");
                }
            }
        }
        private string _sello_factura;
        [DataMember]
        public string status_factura
        {
            get { return _status_factura; }
            set
            {
                if ((value == null && _status_factura != value) || (value != null && _status_factura != value.Trim()))
                {
                    _status_factura = (value == null ? value : value.Trim());
                    OnPropertyChanged("status_factura");
                }
            }
        }
        private string _status_factura;
        [DataMember]
        public System.DateTime fyh_mod
        {
            get { return _fyh_mod; }
            set
            {
                if (_fyh_mod != value)
                {
                    _fyh_mod = value;
                    OnPropertyChanged("fyh_mod");
                }
            }
        }
        private System.DateTime _fyh_mod;
        [DataMember]
        public System.Guid folio_fiscal_factura
        {
            get { return _folio_fiscal_factura; }
            set
            {
                if (_folio_fiscal_factura != value)
                {
                    _folio_fiscal_factura = value;
                    OnPropertyChanged("folio_fiscal_factura");
                }
            }
        }
        private System.Guid _folio_fiscal_factura;
        [DataMember]
        public System.DateTime fyh_fiscal
        {
            get { return _fyh_fiscal; }
            set
            {
                if (_fyh_fiscal != value)
                {
                    _fyh_fiscal = value;
                    OnPropertyChanged("fyh_fiscal");
                }
            }
        }
        private System.DateTime _fyh_fiscal;
        [DataMember]
        public string sello_digital_emisor
        {
            get { return _sello_digital_emisor; }
            set
            {
                if ((value == null && _sello_digital_emisor != value) || (value != null && _sello_digital_emisor != value.Trim()))
                {
                    _sello_digital_emisor = (value == null ? value : value.Trim());
                    OnPropertyChanged("sello_digital_emisor");
                }
            }
        }
        private string _sello_digital_emisor;
        [DataMember]
        public string sello_digital_sat
        {
            get { return _sello_digital_sat; }
            set
            {
                if ((value == null && _sello_digital_sat != value) || (value != null && _sello_digital_sat != value.Trim()))
                {
                    _sello_digital_sat = (value == null ? value : value.Trim());
                    OnPropertyChanged("sello_digital_sat");
                }
            }
        }
        private string _sello_digital_sat;
        [DataMember]
        public byte[] QR
        {
            get { return _qR; }
            set
            {
                if (_qR != value)
                {
                    _qR = value;
                    OnPropertyChanged("QR");
                }
            }
        }
        private byte[] _qR;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public ZctFacturaEnc ZctFacturaEnc
        {
            get { return _zctFacturaEnc; }
            set
            {
                if (!ReferenceEquals(_zctFacturaEnc, value))
                {
                    /*if (ChangeTracker.ChangeTrackingEnabled && ChangeTracker.State != ObjectState.Added && value != null)
                    {
                        // This the dependent end of an identifying relationship, so the principal end cannot be changed if it is already set,
                        // otherwise it can only be set to an entity with a primary key that is the same value as the dependent's foreign key.
                        if (cod_folio != value.cod_folio || folio_factura != value.folio_factura)
                        {
                            throw new InvalidOperationException("The principal end of an identifying relationship can only be changed when the dependent end is in the Added state.");
                        }
                    }*/
                    var previousValue = _zctFacturaEnc;
                    _zctFacturaEnc = value;
                    FixupZctFacturaEnc(previousValue);
                    OnNavigationPropertyChanged("ZctFacturaEnc");
                }
            }
        }
        private ZctFacturaEnc _zctFacturaEnc;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        // This entity type is the dependent end in at least one association that performs cascade deletes.
        // This event handler will process notifications that occur when the principal end is deleted.
        /*internal void HandleCascadeDelete(object sender, ObjectStateChangingEventArgs e)
        {
            if (e.NewState == ObjectState.Deleted)
            {
                this.MarkAsDeleted();
            }
        }*/
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            ZctFacturaEnc = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupZctFacturaEnc(ZctFacturaEnc previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && ReferenceEquals(previousValue.ZctFacturaComprobante, this))
            {
                previousValue.ZctFacturaComprobante = null;
            }
    
            if (ZctFacturaEnc != null)
            {
                ZctFacturaEnc.ZctFacturaComprobante = this;
                cod_folio = ZctFacturaEnc.cod_folio;
                folio_factura = ZctFacturaEnc.folio_factura;
            }
    
        }

        #endregion

    }
}
