//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Almacen))]
    [KnownType(typeof(Articulo))]
    [KnownType(typeof(EncabezadoOrdenConversion))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class DetalleOrdenConversion: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "ZctDetallesOrdenesConversion";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<DetalleOrdenConversion, bool>> Comparador()
        //{
            //return m => m.Id == Id;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(Almacen != null)
                propiedades.Add(Almacen);
            if(Articulo != null)
                propiedades.Add(Articulo);
            if(Encabeado != null)
                propiedades.Add(Encabeado);
    
            if(Almacen != null)
                Almacen.ObtenerPropiedadesNavegacion(propiedades);
            if(Articulo != null)
                Articulo.ObtenerPropiedadesNavegacion(propiedades);
            if(Encabeado != null)
                Encabeado.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(Almacen != null && !propiedadesT.Contains(Almacen))
                propiedades.Add(Almacen);
            if(Articulo != null && !propiedadesT.Contains(Articulo))
                propiedades.Add(Articulo);
            if(Encabeado != null && !propiedadesT.Contains(Encabeado))
                propiedades.Add(Encabeado);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public DetalleOrdenConversion()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public DetalleOrdenConversion ObtenerCopiaDePrimitivas()
        {
            return new DetalleOrdenConversion
            {
                IdEncabezadoOrdenConversion = this.IdEncabezadoOrdenConversion,
                Cantidad = this.Cantidad,
                Costo = this.Costo,
                CodigoArticulo = this.CodigoArticulo,
                IdAlmacen = this.IdAlmacen,
                AnioFiscal = this.AnioFiscal,
                MesFiscal = this.MesFiscal,
            };
        }
    
        public void SincronizarPrimitivas(DetalleOrdenConversion origen)
        {
            this.IdEncabezadoOrdenConversion = origen.IdEncabezadoOrdenConversion;
            this.Cantidad = origen.Cantidad;
            this.Costo = origen.Costo;
            this.CodigoArticulo = origen.CodigoArticulo;
            this.IdAlmacen = origen.IdAlmacen;
            this.AnioFiscal = origen.AnioFiscal;
            this.MesFiscal = origen.MesFiscal;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }
        private int _id;
        [DataMember]
        public int IdEncabezadoOrdenConversion
        {
            get { return _idEncabezadoOrdenConversion; }
            set
            {
                if (_idEncabezadoOrdenConversion != value)
                {
                    if (!IsDeserializing)
                    {
                        if (Encabeado != null && Encabeado.Id != value)
                        {
                            Encabeado = null;
                        }
                    }
                    _idEncabezadoOrdenConversion = value;
                    OnPropertyChanged("IdEncabezadoOrdenConversion");
                }
            }
        }
        private int _idEncabezadoOrdenConversion;
        [DataMember]
        public decimal Cantidad
        {
            get { return _cantidad; }
            set
            {
                if (_cantidad != value)
                {
                    _cantidad = value;
                    OnPropertyChanged("Cantidad");
                }
            }
        }
        private decimal _cantidad;
        [DataMember]
        public decimal Costo
        {
            get { return _costo; }
            set
            {
                if (_costo != value)
                {
                    _costo = value;
                    OnPropertyChanged("Costo");
                }
            }
        }
        private decimal _costo;
        [DataMember]
        public string CodigoArticulo
        {
            get { return _codigoArticulo; }
            set
            {
                if ((value == null && _codigoArticulo != value) || (value != null && _codigoArticulo != value.Trim()))
                {
                    if (!IsDeserializing)
                    {
                        if (Articulo != null && Articulo.Cod_Art != (value == null ? value : value.Trim()))
                        {
                            Articulo = null;
                        }
                    }
                    _codigoArticulo = (value == null ? value : value.Trim());
                    OnPropertyChanged("CodigoArticulo");
                }
            }
        }
        private string _codigoArticulo;
        [DataMember]
        public int IdAlmacen
        {
            get { return _idAlmacen; }
            set
            {
                if (_idAlmacen != value)
                {
                    if (!IsDeserializing)
                    {
                        if (Almacen != null && Almacen.Cod_Alm != value)
                        {
                            Almacen = null;
                        }
                    }
                    _idAlmacen = value;
                    OnPropertyChanged("IdAlmacen");
                }
            }
        }
        private int _idAlmacen;
        [DataMember]
        public Nullable<int> AnioFiscal
        {
            get { return _anioFiscal; }
            set
            {
                if (_anioFiscal != value)
                {
                    _anioFiscal = value;
                    OnPropertyChanged("AnioFiscal");
                }
            }
        }
        private Nullable<int> _anioFiscal;
        [DataMember]
        public Nullable<int> MesFiscal
        {
            get { return _mesFiscal; }
            set
            {
                if (_mesFiscal != value)
                {
                    _mesFiscal = value;
                    OnPropertyChanged("MesFiscal");
                }
            }
        }
        private Nullable<int> _mesFiscal;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public Almacen Almacen
        {
            get { return _almacen; }
            set
            {
                if (!ReferenceEquals(_almacen, value))
                {
                    var previousValue = _almacen;
                    _almacen = value;
                    FixupAlmacen(previousValue);
                    OnNavigationPropertyChanged("Almacen");
                }
            }
        }
        private Almacen _almacen;
    
        [DataMember]
        public Articulo Articulo
        {
            get { return _articulo; }
            set
            {
                if (!ReferenceEquals(_articulo, value))
                {
                    var previousValue = _articulo;
                    _articulo = value;
                    FixupArticulo(previousValue);
                    OnNavigationPropertyChanged("Articulo");
                }
            }
        }
        private Articulo _articulo;
    
        [DataMember]
        public EncabezadoOrdenConversion Encabeado
        {
            get { return _encabeado; }
            set
            {
                if (!ReferenceEquals(_encabeado, value))
                {
                    var previousValue = _encabeado;
                    _encabeado = value;
                    FixupEncabeado(previousValue);
                    OnNavigationPropertyChanged("Encabeado");
                }
            }
        }
        private EncabezadoOrdenConversion _encabeado;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            Almacen = null;
            Articulo = null;
            Encabeado = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupAlmacen(Almacen previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.ZctDetallesOrdenesConversion.Contains(this))
            {
                previousValue.ZctDetallesOrdenesConversion.Remove(this);
            }
    
            if (Almacen != null)
            {
                if (!Almacen.ZctDetallesOrdenesConversion.Contains(this))
                {
                    Almacen.ZctDetallesOrdenesConversion.Add(this);
                }
    
                IdAlmacen = Almacen.Cod_Alm;
            }
        }
    
        private void FixupArticulo(Articulo previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.ZctDetallesOrdenesConversion.Contains(this))
            {
                previousValue.ZctDetallesOrdenesConversion.Remove(this);
            }
    
            if (Articulo != null)
            {
                if (!Articulo.ZctDetallesOrdenesConversion.Contains(this))
                {
                    Articulo.ZctDetallesOrdenesConversion.Add(this);
                }
    
                CodigoArticulo = Articulo.Cod_Art;
            }
        }
    
        private void FixupEncabeado(EncabezadoOrdenConversion previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.Detalles.Contains(this))
            {
                previousValue.Detalles.Remove(this);
            }
    
            if (Encabeado != null)
            {
                if (!Encabeado.Detalles.Contains(this))
                {
                    Encabeado.Detalles.Add(this);
                }
    
                IdEncabezadoOrdenConversion = Encabeado.Id;
            }
        }

        #endregion

    }
}
