//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Almacen.Entidades
{
    [DataContract(IsReference = true)]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class VW_FacturacionEReceptor: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "VW_FacturacionEReceptor";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<VW_FacturacionEReceptor, bool>> Comparador()
        //{
            //return m => m.Cod_Cte == Cod_Cte;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
    
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public VW_FacturacionEReceptor()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public VW_FacturacionEReceptor ObtenerCopiaDePrimitivas()
        {
            return new VW_FacturacionEReceptor
            {
                nombre = this.nombre,
                rfc = this.rfc,
                calle = this.calle,
                colonia = this.colonia,
                Desc_Ciu = this.Desc_Ciu,
                Desc_Edo = this.Desc_Edo,
                pais = this.pais,
                numext = this.numext,
                numint = this.numint,
                cp = this.cp,
                email = this.email,
            };
        }
    
        public void SincronizarPrimitivas(VW_FacturacionEReceptor origen)
        {
            this.nombre = origen.nombre;
            this.rfc = origen.rfc;
            this.calle = origen.calle;
            this.colonia = origen.colonia;
            this.Desc_Ciu = origen.Desc_Ciu;
            this.Desc_Edo = origen.Desc_Edo;
            this.pais = origen.pais;
            this.numext = origen.numext;
            this.numint = origen.numint;
            this.cp = origen.cp;
            this.email = origen.email;
        }

        #endregion

        #region Primitive Properties
        [DataMember]
        public string nombre
        {
            get { return _nombre; }
            set
            {
                if ((value == null && _nombre != value) || (value != null && _nombre != value.Trim()))
                {
                    _nombre = (value == null ? value : value.Trim());
                    OnPropertyChanged("nombre");
                }
            }
        }
        private string _nombre;
        [DataMember]
        public string rfc
        {
            get { return _rfc; }
            set
            {
                if ((value == null && _rfc != value) || (value != null && _rfc != value.Trim()))
                {
                    _rfc = (value == null ? value : value.Trim());
                    OnPropertyChanged("rfc");
                }
            }
        }
        private string _rfc;
        [DataMember]
        public string calle
        {
            get { return _calle; }
            set
            {
                if ((value == null && _calle != value) || (value != null && _calle != value.Trim()))
                {
                    _calle = (value == null ? value : value.Trim());
                    OnPropertyChanged("calle");
                }
            }
        }
        private string _calle;
        [DataMember]
        public string colonia
        {
            get { return _colonia; }
            set
            {
                if ((value == null && _colonia != value) || (value != null && _colonia != value.Trim()))
                {
                    _colonia = (value == null ? value : value.Trim());
                    OnPropertyChanged("colonia");
                }
            }
        }
        private string _colonia;
        [DataMember]
        public string Desc_Ciu
        {
            get { return _desc_Ciu; }
            set
            {
                if ((value == null && _desc_Ciu != value) || (value != null && _desc_Ciu != value.Trim()))
                {
                    _desc_Ciu = (value == null ? value : value.Trim());
                    OnPropertyChanged("Desc_Ciu");
                }
            }
        }
        private string _desc_Ciu;
        [DataMember]
        public string Desc_Edo
        {
            get { return _desc_Edo; }
            set
            {
                if ((value == null && _desc_Edo != value) || (value != null && _desc_Edo != value.Trim()))
                {
                    _desc_Edo = (value == null ? value : value.Trim());
                    OnPropertyChanged("Desc_Edo");
                }
            }
        }
        private string _desc_Edo;
        [DataMember]
        public string pais
        {
            get { return _pais; }
            set
            {
                if ((value == null && _pais != value) || (value != null && _pais != value.Trim()))
                {
                    _pais = (value == null ? value : value.Trim());
                    OnPropertyChanged("pais");
                }
            }
        }
        private string _pais;
        [DataMember]
        public string numext
        {
            get { return _numext; }
            set
            {
                if ((value == null && _numext != value) || (value != null && _numext != value.Trim()))
                {
                    _numext = (value == null ? value : value.Trim());
                    OnPropertyChanged("numext");
                }
            }
        }
        private string _numext;
        [DataMember]
        public string numint
        {
            get { return _numint; }
            set
            {
                if ((value == null && _numint != value) || (value != null && _numint != value.Trim()))
                {
                    _numint = (value == null ? value : value.Trim());
                    OnPropertyChanged("numint");
                }
            }
        }
        private string _numint;
    	[Key]
        [DataMember]
        public int Cod_Cte
        {
            get { return _cod_Cte; }
            set
            {
                if (_cod_Cte != value)
                {
                    _cod_Cte = value;
                    OnPropertyChanged("Cod_Cte");
                }
            }
        }
        private int _cod_Cte;
        [DataMember]
        public Nullable<int> cp
        {
            get { return _cp; }
            set
            {
                if (_cp != value)
                {
                    _cp = value;
                    OnPropertyChanged("cp");
                }
            }
        }
        private Nullable<int> _cp;
        [DataMember]
        public string email
        {
            get { return _email; }
            set
            {
                if ((value == null && _email != value) || (value != null && _email != value.Trim()))
                {
                    _email = (value == null ? value : value.Trim());
                    OnPropertyChanged("email");
                }
            }
        }
        private string _email;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
        }

        #endregion

    }
}
