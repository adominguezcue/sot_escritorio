﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Constantes
{
    public static class ClavesFolios
    {
        /// <summary>
        /// CAJA
        /// </summary>
        [Description("CAJA")]
        public static readonly string CJ = "CJ";
        /// <summary>
        /// FACTURACIÓN
        /// </summary>
        [Description("FACTURACIÓN")]
        public static readonly string FC = "FC";
        /// <summary>
        /// INTERCAMBIO DE FOLIOS
        /// </summary>
        [Description("INTERCAMBIO DE FOLIOS")]
        public static readonly string IA = "IA";
        /// <summary>
        /// INVENTARIOS FÍSICOS
        /// </summary>
        [Description("INVENTARIOS FÍSICOS")]
        public static readonly string IF = "IF";
        /// <summary>
        /// INVENTARIO DE MOTOCICLETAS
        /// </summary>
        [Description("INVENTARIO DE MOTOCICLETAS")]
        public static readonly string IM = "IM";
        /// <summary>
        /// INVENTARIO DE PRODUCCIÓN
        /// </summary>
        [Description("INVENTARIO DE PRODUCCIÓN")]
        public static readonly string IP = "IP";
        /// <summary>
        /// NOTAS DE CRÉDITO
        /// </summary>
        [Description("NOTAS DE CRÉDITO")]
        public static readonly string NT = "NT";
        /// <summary>
        /// ÓRDENES DE COMPRA
        /// </summary>
        [Description("ÓRDENES DE COMPRA")]
        public static readonly string OC = "OC";
        /// <summary>
        /// ÓRDENES DE PRODUCCIÓN
        /// </summary>
        [Description("ÓRDENES DE PRODUCCIÓN")]
        public static readonly string OP = "OP";
        /// <summary>
        /// MERMA
        /// </summary>
        [Description("MERMA")]
        public static readonly string OS = "OS";
        /// <summary>
        /// ÓRDENES DE LAMINADO
        /// </summary>
        [Description("ÓRDENES DE LAMINADO")]
        public static readonly string OL = "OL";
        /// <summary>
        /// ÓRDENES DE TRABAJO
        /// </summary>
        [Description("ÓRDENES DE TRABAJO")]
        public static readonly string OT = "OT";
        /// <summary>
        /// REQUISICIONES WEB
        /// </summary>
        [Description("REQUISICIONES WEB")]
        public static readonly string RW = "RW";
        ///// <summary>
        ///// COMANDAS
        ///// </summary>
        //[Description("COMANDAS")]
        //public static readonly string CO = "CO";
        /// <summary>
        /// CONVERSIONES
        /// </summary>
        [Description("CONVERSIONES")]
        public static readonly string CONV = "CONV";
        /// <summary>
        /// DEVOLUCIÓN DE CONVERSIÓN
        /// </summary>
        [Description("DEVOLUCIÓN DE CONVERSIÓN")]
        public static readonly string DEV = "DEV";
        /// <summary>
        /// INVENTARIO DE CONVERSIÓN
        /// </summary>
        [Description("INVENTARIO DE CONVERSIÓN")]
        public static readonly string IC = "IC";
        /// <summary>
        /// COMANDAS
        /// </summary>
        [Description("COMANDAS")]
        public static readonly string RS = "RS";
        /// <summary>
        /// CONSUMOS INTERNOS
        /// </summary>
        [Description("CONSUMOS INTERNOS")]
        public static readonly string CI = "CI";
        /// <summary>
        /// ÓRDENES DE RESTAURANTE
        /// </summary>
        [Description("ÓRDENES DE RESTAURANTE")]
        public static readonly string R = "R";
    }
}
