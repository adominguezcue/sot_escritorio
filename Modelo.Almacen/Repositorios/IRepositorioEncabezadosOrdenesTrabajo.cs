﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioEncabezadosOrdenesTrabajo : IRepositorio<ZctEncOT>
    {
        void SP_ZctEncOT_Edicion(int CodEnc_OT, string Cod_folio, DateTime FchDoc_OT, int Cod_Cte, int Cod_Contacto, int Cod_Mot, int Cod_Tec, DateTime FchEnt, DateTime FchSal,
                       DateTime FchEntRmp, DateTime FchSalRmp, string ObsOT, DateTime FchAplMov, int UltKm_OT, int Cod_TpOT, string Cod_Estatus = "N", string HistEstatus_OT = null);
    }
}
