﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioDetallesOrdenesTrabajo : IRepositorio<ZctDetOC>
    {
        void SP_ZctDetOT_EDICION(int Cod_EncOT, int Cod_DetOT, string Cod_Art, int Ctd_Art, decimal CosArt_DetOT, decimal PreArt_DetOT, int CtdStd_DetOT,
                       DateTime Fch_DetOT, DateTime FchAplMov, int Cat_Alm, int Cod_Mot = 0, int tiempo_vida = 0, int cod_usu = 0);
    }
}
