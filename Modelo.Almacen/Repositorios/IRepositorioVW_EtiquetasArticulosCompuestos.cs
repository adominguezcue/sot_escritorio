﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioVW_EtiquetasArticulosCompuestos : IRepositorio<VW_EtiquetasArticulosCompuestos>
    {
        List<VW_EtiquetasArticulosCompuestos> ObtenerEtiquetasFiltradas(string nombreArticulo = "", DateTime? diaIngreso = null);
    }
}
