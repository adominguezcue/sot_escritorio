﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioZctRequisition : IRepositorio<ZctRequisition>
    {
        List<ZctRequisition> ObtenerRequisiciones(string status, int? anio, int? mes);

        int ObtenerUltimoFolio();
    }
}
