﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioAlmacenes : IRepositorio<Modelo.Almacen.Entidades.Almacen>
    {
        DtoEncabezadoIntercambio SP_ObtenerResumenIntercambio(int idFolio);
    }
}
