﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioArticulos : IRepositorio<Articulo>
    {
        List<Articulo> ObtenerArticulosPorTipo(string idTipo);

        List<Articulo> ObtenerArticulosPorDepartamento(int idDepartamento);

        List<Articulo> ObtenerArticulosConLineaYPresentacionPorId(List<string> idsArticulos);

        List<DtoExistencias> SP_ConsultarExistencias(string codigoArticulo);

        DtoResultadoBusquedaArticulo SP_ZctCatArt_Busqueda(string codigo, bool soloInventariable);

        List<DtoResultadoBusquedaArticuloSimple> ObtenerResumenesArticulosPorFiltro(string filtro, bool incluirArticulosPresentacion, bool soloInventariables, bool? EsInsumo, bool? _EsgestionArticulos);

        Articulo TraerOGenerarArticuloPorCodigoOUpc(string Cod_art, bool cargarTemporales);

        Articulo ObtenerConCostoAcumulado(string codigoArticulo/*, int idAlmacen*/);
    }
}
