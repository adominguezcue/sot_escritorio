﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioDetallesOrdenesCompra : IRepositorio<ZctDetOC>
    {
        //List<DtoDetalleOC> SP_ZctDetOC_CONSULTA(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, decimal cantidadArticulo, decimal costoArticulo, decimal cantidadEnStock, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA);

        List<DtoDetalleOC> ObtenerDetallesOrdenCompra(int codigoOrdenCompra);

        void SP_ZctDetOC_EDICION(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, decimal cantidadArticulo, decimal costoArticulo, decimal cantidadSurtida, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA, bool _EsCancelado);

        void SP_ZctDetOC_ELIMINACION(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, decimal cantidadArticulo, decimal costoArticulo, decimal cantidadSurtida, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA);
    }
}
