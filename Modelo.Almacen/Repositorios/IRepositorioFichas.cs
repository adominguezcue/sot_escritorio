﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioFichas : IRepositorio<Ficha>
    {
        List<Ficha> ObtenerFichasDia(DateTime dia, string nombreArticulo = "");

        Ficha ObtenerPorIdConArchivos(int idFicha);
    }
}
