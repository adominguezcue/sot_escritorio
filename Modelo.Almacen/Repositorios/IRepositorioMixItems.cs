﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioMixItems : IRepositorio<ZctMixItems>
    {
        List<ZctMixItems> GetMixItems(string codArt);

        List<ZctMixItems> GetMixItemsCost(string codArt, int codAlm);

        List<ZctDetOP> GetDetOP(int codOP);

        ZctEncOP GetEncOP(int codOP);
    }
}
