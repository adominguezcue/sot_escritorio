﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioEncabezadosCuentasPorPagar : IRepositorio<ZctEncCuentasPagar>
    {
        int ObtenerFolioMaximo();

        void SP_EstadoCxP(int OC, ZctEncCuentasPagar.Estados estados);

        ZctEncCuentasPagar ObtenerCargada(int cod_cuenta_pago);
        List<ZctEncCuentasPagar> TraeCuentasPorEstado(string filtroProveedor, ZctEncCuentasPagar.Estados? estado, DateTime? fechacreacionini, DateTime? fechacreacionfin, DateTime? fechavencimientoini, DateTime? fechavencimientofin, bool? esfechacreacion, bool? esfechafin);
        List<ZctEncCuentasPagar> TraeCuentasProveedores(int cod_prov, ZctEncCuentasPagar.Estados? estado);
    }
}
