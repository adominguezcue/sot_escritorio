﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioProveedores : IRepositorio<Proveedor>
    {
        int ObtenerUltimoCodigo();
        List<DtoResumenProveedor> ObtenerResumenesProveedores(string filtro = null);

        List<Proveedor> ObtieneProveedoresPorFiltroNombre(string text);
    }
}
