﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioDepartamentos: IRepositorio<Departamento>
    {
    }
}
