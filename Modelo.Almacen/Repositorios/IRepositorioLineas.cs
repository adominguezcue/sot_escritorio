﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Almacen.Entidades;
using Dominio.Nucleo.Repositorios;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioLineas: IRepositorio<Linea>
    {
        List<Linea> ObtenerLineasConArticulos(bool soloComandables = true, bool incluirSalidaNegada = false, string filtro = null);

        List<Linea> ObtenerLineas();

        List<Linea> ObtenerConNombreDepartamento(bool incluirInactivas, int cod_dpto);
    }
}
