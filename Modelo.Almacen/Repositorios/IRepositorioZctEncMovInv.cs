﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioZctEncMovInv : IRepositorio<ZctEncMovInv>
    {
        void SP_ZctEncMovInv_EDICION(int CodMov_Inv, string TpMov_Inv, decimal CosMov_Inv, string FolOS_Inv, string Cod_Art, decimal CtdMov_Inv, string TpOs_Inv, DateTime FchMov_Inv, int Cod_Alm);

        decimal ObtenerUltimoCostoMovimiento(string codigoArticulo);

        List<DtoItemKardex> SP_ZctKdxInv(string cod_Art, int cod_Alm, DateTime fchIni, DateTime fchFin);
    }
}
