﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioWV_ZctProveedoresCxP : IRepositorio<WV_ZctProveedoresCxP>
    {
        List<WV_ZctProveedoresCxP> SP_ZctProveedoresCxP(int idEstado);
        List<WV_ZctProveedoresCxP> SP_TotalesCXCProveedor(int? idEstado);
    }
}
