﻿using Dominio.Nucleo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioParametros : IRepositorio<Modelo.Almacen.Entidades.ZctCatPar>
    {
    }
}
