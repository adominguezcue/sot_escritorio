﻿using Dominio.Nucleo.Repositorios;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Almacen.Repositorios
{
    public interface IRepositorioEncabezadosOrdenesCompra : IRepositorio<ZctEncOC>
    {
        List<DtoEncabezadoOC> ObtenerResumenesEncabezadosOC();

        DtoEncabezadoOC ObtenerResumenEncabezadoOC(int codigoOrdenCompra);
        //List<DtoEncabezadoOC> SP_ZctEncOC_CONSULTA(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra, DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion);
        void SP_ZctEncOC_EDICION(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra, DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion, string estado, bool? servicio);
    }
}
