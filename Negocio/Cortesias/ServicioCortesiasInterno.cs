﻿//using Modelo.Entidades;
//using Modelo.Repositorios;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Transactions;
//using Transversal.Dependencias;
//using Transversal.Excepciones;

//namespace Negocio.Cortesias
//{
//    public class ServicioCortesiasInterno : IServicioCortesiasInterno
//    {
//        IRepositorioCortesias RepositorioCortesias
//        {
//            get { return _repositorioCortesias.Value; }
//        }

//        Lazy<IRepositorioCortesias> _repositorioCortesias = new Lazy<IRepositorioCortesias>(() => FabricaDependencias.Instancia.Resolver<IRepositorioCortesias>());

//        #region métodos internal

//        void IServicioCortesiasInterno.CrearCortesia(Cortesia cortesia, int idUsuario)
//        {
//            if (Transaction.Current == null)
//                throw new SOTException("Se requiere una transacción abierta para realizar la operación");

//            if (cortesia == null)
//                throw new SOTException(Recursos.Cortesias.nula_excepcion);

//            cortesia.Activa = true;
//            cortesia.IdUsuarioCreo = idUsuario;
//            cortesia.IdUsuarioModifico = idUsuario;
//            cortesia.FechaCreacion = cortesia.FechaModificacion = DateTime.Now;

//            RepositorioCortesias.Agregar(cortesia);
//            RepositorioCortesias.GuardarCambios();
//        }

//        #endregion
//    }
//}
