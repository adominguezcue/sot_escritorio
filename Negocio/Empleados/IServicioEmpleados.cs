﻿using Modelo.Entidades;
using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo.Seguridad.Dtos;
using Modelo.Dtos;
using Negocio.Compartido.Empleados;

namespace Negocio.Empleados
{
    public interface IServicioEmpleados : IServicioEmpleadosCompartido
    {
        /// <summary>
        /// Permite registrar un nuevo empleado en el sistema
        /// </summary>
        /// <param name="empleado"></param>
        /// <param name="usuario"></param>
        void CrearEmpleado(Empleado empleado, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar un nuevo empleado
        /// </summary>
        /// <param name="empleado"></param>
        /// <param name="usuario"></param>
        void ModificarEmpleado(Empleado empleado, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar un empleado de la base de datos
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="huellaDigital"></param>
        void EliminarEmpleado(int idEmpleado, DtoCredencial credencial);
        /// <summary>
        /// Realiza una búsqueda filtrada de los empleados activos en el sistema, los filtros son opcionales
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="telefono"></param>
        /// <param name="fechaRegistro"></param>
        /// <returns></returns>
        List<Empleado> ObtenerEmpleadosFiltrados(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaRegistro = null);
        /// <summary>
        /// Retorna una lista de empleados cuyo puesto es "Recamarera" que se encuentren habilitados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloHabilitadas"></param>
        /// <returns></returns>
        List<Empleado> ObtenerRecamarerasFILTRO(bool soloHabilitadas = true);
        /// <summary>
        /// Retorna una lista de empleados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloHabilitadas"></param>
        /// <returns></returns>
        List<Empleado> ObtenerEmpleadosFILTRO(bool soloHabilitadas = true);
        ///// <summary>
        ///// Retorna los empleados 
        ///// </summary>
        ///// <param name="nombrePuesto"></param>
        ///// <param name="usuario"></param>
        ///// <returns></returns>
        //List<Empleado> ObtenerEmpleadosActivosPorPuesto(string nombrePuesto, DtoUsuario usuario);
        /// <summary>
        /// Retorna los empleados 
        /// </summary>
        /// <param name="idPuesto"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Empleado> ObtenerEmpleadosActivosPorPuesto(DtoUsuario usuario, /*int? idTurno,*/ params int[] idsPuestos);

        /// <summary>
        /// Retorna una lista de empleados cuyo puesto es "Supervisor" que se encuentren habilitados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloHabilitados"></param>
        /// <returns></returns>
        List<Empleado> ObtenerSupervisoresFILTRO(bool soloHabilitados = true);
        /// <summary>
        /// Permite modificar el valor de la propiedad Habilitado de los empleados activos con ids
        /// iguales a las llaves del diccionario proporcionado
        /// </summary>
        /// <param name="idsEmpleadosHabilitacion"></param>
        /// <param name="usuario"></param>
        void HabilitarDeshabilitarEmpleados(Dictionary<int, bool> idsEmpleadosHabilitacion, DtoUsuario usuario);

        /// <summary>
        /// Retorna una lista de empleados cuyo puesto es "Mesero" que se encuentren habilitados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloHabilitados"></param>
        /// <returns></returns>
        List<Empleado> ObtenerMeserosFILTRO(bool soloHabilitados);
        /// <summary>
        /// Retorna una lista de empleados cuyo puesto es "Valet" que se encuentren habilitados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloHabilitados"></param>
        /// <returns></returns>
        List<Empleado> ObtenerValetsFILTRO(bool soloHabilitados = true);
        /// <summary>
        /// Valida que los ids proporcionados correspondan a recamareras activas y habilitadas
        /// </summary>
        /// <param name="idsEmpleados"></param>
        void ValidarRecamarerasActivas(List<int> idsEmpleados);
        /// <summary>
        /// Valida que el id proporcionado corresponda a un supervisor activo y habilitado
        /// </summary>
        /// <param name="idEmpleado"></param>
        void ValidarSupervisorActivoHabilitado(int idEmpleado);
        /// <summary>
        /// Valida que el empleado porporcionado no tenga tareas asignadas
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="idTareaLimpieza"></param>
        void ValidarEmpleadoLibre(int idEmpleado);
        ///// <summary>
        ///// Valida que el id proporcionado correspondan a un mesero activo y habilitado
        ///// </summary>
        ///// <param name="idEmpleado"></param>
        //void ValidarMeseroActivoHabilitado(int idEmpleado);
        ///// <summary>
        ///// Valida que el id proporcionado correspondan a un Valet activo y habilitado
        ///// </summary>
        ///// <param name="idEmpleado"></param>
        //void ValidarValetActivoHabilitado(int idEmpleado);
        /// <summary>
        /// Valida que el id proporcionado correspondan a una Recamarera activa y habilitada
        /// </summary>
        /// <param name="idEmpleado"></param>
        void ValidarRecamareraActiva(int idEmpleado);
        ///// <summary>
        ///// Retorna la lista de puestos activos en el sistema
        ///// </summary>
        ///// <param name="usuario"></param>
        ///// <returns></returns>
        //List<Puesto> ObtenerPuestosActivos(DtoUsuario usuario);
        Empleado ObtenerEmpleadoPorNumeroNoNull(string numeroEmpleado);
        ///// <summary>
        ///// Valida que el id proporcionado correspondan a un cocinero activo y habilitado
        ///// </summary>
        ///// <param name="idEmpleado"></param>
        //void ValidarCocineroActivoHabilitado(int idEmpleado);
        /// <summary>
        /// Retorna un resumen de los empleados activos, incluye el id, nombre completo, área y puesto
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreas(DtoUsuario usuario);
        /// <summary>
        /// Retorna un resumen de los empleados activos, incluye el id, nombre completo, área y puesto
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreasFILTRO();
        /// <summary>
        /// Retorna la lista de categorías de empleados.
        /// </summary>
        /// <returns></returns>
        List<CategoriaEmpleado> ObtenerCategoriasEmpleados();

        /// <summary>
        /// Indica si un puesto está vinculado a uno o más empleados activos
        /// </summary>
        /// <param name="idPuesto"></param>
        /// <returns></returns>
        bool VerificarPuestoEnUso(int idPuesto);

        List<Empleado> ObtenerVendedoresFILTRO(/*int idCorteTurno,*/ int? idEmpleadoIncluir);
        List<Empleado> ObtenerMeserosFILTRO(/*int idCorteTurno,*/ int? idEmpleadoIncluir);
    }
}
