﻿using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Empleados
{
    internal interface IServicioEmpleadosInterno : IServicioEmpleados
    {

        /// <summary>
        /// Retorna el empleado activo que posea el id proporcionado
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <returns></returns>
        Empleado ObtenerEmpleadoActivoConPuestoPorId(int idEmpleado);
        /// <summary>
        /// Retorna el empleado activo en el sistema con el id porporcionado, en caso de que no
        /// existan coinciencias lanzará una excepción
        /// </summary>
        /// <param name="idEmpleado"></param>
        Empleado ObtenerEmpleadoActivoNoNullConTurno(int idEmpleado);
        /// <summary>
        /// Verifica que el empleado esté activo en el sistema, retorna false en caso contrario
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <returns></returns>
        bool VerificarEmpleadoActivo(int idEmpleado);
        /// <summary>
        /// Retorna el empleado activo que posea el id proporcionado con su categoría cargada
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="usuario"></param>
        Empleado ObtenerEmpleadoActivoPorIdConCategoria(int idEmpleado);
        /// <summary>
        /// Retorna una lista de empleados cuyo puesto es "Valet" que se encuentren habilitados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloHabilitados"></param>
        /// <returns></returns>
        List<Empleado> ObtenerValets(/*int idTurno*/);
        /// <summary>
        /// Retorna una lista de empleados cuyo puesto es "Mesero" que se encuentren habilitados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloHabilitados"></param>
        /// <returns></returns>
        List<Empleado> ObtenerMeseros(/*int idTurno*/);
        /// <summary>
        /// Valida que el empleado tenga un puesto que posea los permisos requeridos
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="permisos"></param>
        void ValidarPermisos(int idEmpleado, Modelo.Seguridad.Dtos.DtoPermisos permisos);
        /// <summary>
        /// Retorna el nombre completo del empleado con el id proporcionado y el puesto que ocupa
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <returns></returns>
        DtoEmpleadoPuesto ObtenerResumenEmpleadoPuestoPorId(int idEmpleado);
    }
}
