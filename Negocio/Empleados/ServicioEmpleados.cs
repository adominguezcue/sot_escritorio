﻿using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.ConfiguracionesGlobales;
using Negocio.CortesTurno;
using Negocio.Seguridad.Permisos;
using Negocio.Seguridad.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.Empleados
{
    public class ServicioEmpleados : IServicioEmpleadosInterno
    {
        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IRepositorioEmpleados RepositorioEmpleados
        {
            get { return _repositorioEmpleados.Value; }
        }

        Lazy<IRepositorioEmpleados> _repositorioEmpleados = new Lazy<IRepositorioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioEmpleados>(); });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });

        IRepositorioCategoriasEmpleados RepositorioCategoriasEmpleados
        {
            get { return _repositorioCategoriasEmpleados.Value; }
        }

        Lazy<IRepositorioCategoriasEmpleados> _repositorioCategoriasEmpleados = new Lazy<IRepositorioCategoriasEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCategoriasEmpleados>(); });

        IRepositorioPuestos RepositorioPuestos
        {
            get { return _repositorioPuestos.Value; }
        }

        Lazy<IRepositorioPuestos> _repositorioPuestos = new Lazy<IRepositorioPuestos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPuestos>(); });

        public void CrearEmpleado(Empleado empleado, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearEmpleado = true });

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_excepcion);

            if (RepositorioEmpleados.Alguno(m => m.NumeroEmpleado == empleado.NumeroEmpleado))
                throw new SOTException(Recursos.Empleados.numero_empleado_en_uso_excepcion, empleado.NumeroEmpleado.ToString());

            if (RepositorioEmpleados.Alguno(m => m.NSS == empleado.NSS))
                throw new SOTException(Recursos.Empleados.numero_seguro_social_en_uso_excepcion, empleado.NSS.ToString());

            ValidarCamposObligatorios(empleado);

            DejarValidosCamposOpcionales(empleado);

            var fechaActual = DateTime.Now;

            empleado.Activo = true;
            empleado.FechaCreacion = fechaActual;
            empleado.FechaModificacion = fechaActual;
            empleado.Habilitado = true;
            empleado.IdUsuarioCreo = usuario.Id;
            empleado.IdUsuarioModifico = usuario.Id;

            RepositorioEmpleados.Agregar(empleado);
            RepositorioEmpleados.GuardarCambios();
        }

        public void ModificarEmpleado(Empleado empleado, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarEmpleado = true });

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_excepcion);

            if (RepositorioEmpleados.Alguno(m => m.NumeroEmpleado == empleado.NumeroEmpleado && m.Id != empleado.Id))
                throw new SOTException(Recursos.Empleados.numero_empleado_en_uso_excepcion, empleado.NumeroEmpleado.ToString());

            if (RepositorioEmpleados.Alguno(m => m.NSS == empleado.NSS && m.Id != empleado.Id))
                throw new SOTException(Recursos.Empleados.numero_seguro_social_en_uso_excepcion, empleado.NSS.ToString());

            ValidarActivo(empleado);
            ValidarCamposObligatorios(empleado);

            DejarValidosCamposOpcionales(empleado);

            var fechaActual = DateTime.Now;


            empleado.FechaModificacion = fechaActual;
            empleado.IdUsuarioModifico = usuario.Id;

            RepositorioEmpleados.Modificar(empleado);
            RepositorioEmpleados.GuardarCambios();
        }

        public void EliminarEmpleado(int idEmpleado, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarEmpleado = true });

            var empleado = RepositorioEmpleados.Obtener(m => m.Activo && m.Id == idEmpleado);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            empleado.Activo = false;
            empleado.Habilitado = false;
            empleado.FechaModificacion = fechaActual;
            empleado.FechaEliminacion = fechaActual;
            empleado.IdUsuarioModifico = usuario.Id;
            empleado.IdUsuarioElimino = usuario.Id;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioEmpleados.Modificar(empleado);
                RepositorioEmpleados.GuardarCambios();

                ServicioUsuarios.EliminarUsuarioPorEmpleado(empleado.Id, usuario);

                scope.Complete();
            }
        }

        public List<Empleado> ObtenerEmpleadosFiltrados(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaRegistro = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarEmpleado = true });

            return RepositorioEmpleados.ObtenerEmpleadosFiltrados(nombre, apellidoPaterno, apellidoMaterno, telefono, fechaRegistro);
        }

        private void ValidarActivo(Empleado empleado)
        {
            if (!RepositorioEmpleados.Alguno(m => m.Activo && m.Id == empleado.Id))
                throw new SOTException(Recursos.Empleados.empleado_no_activo_excepcion);
            if (!empleado.Activo)
                throw new SOTException(Recursos.Empleados.empleado_desactivado_excepcion);
        }

        private void DejarValidosCamposOpcionales(Empleado empleado)
        {
            if (!string.IsNullOrEmpty(empleado.Telefono))
            {
                if (!UtilidadesRegex.SoloDigitos(empleado.Telefono))
                    throw new SOTException(Recursos.Empleados.telefono_invalido_excepcion);
            }
            else
                empleado.Telefono = "";

            if (!string.IsNullOrEmpty(empleado.Celular))
            {
                if (!UtilidadesRegex.SoloDigitos(empleado.Celular))
                    throw new SOTException(Recursos.Empleados.celular_invalido_excepcion);
            }
            else
                empleado.Celular = "";

            if (string.IsNullOrWhiteSpace(empleado.Domicilio))
                empleado.Domicilio = "";
        }

        private void ValidarCamposObligatorios(Empleado empleado)
        {
            if (!UtilidadesRegex.SoloDigitos(empleado.NumeroEmpleado))
                throw new SOTException(Recursos.Empleados.numero_empleado_invalido_excepcion);

            if (!UtilidadesRegex.SoloLetras(empleado.Nombre))
                throw new SOTException(Recursos.Empleados.nombre_invalido_excepcion);

            if (!UtilidadesRegex.SoloLetras(empleado.ApellidoPaterno))
                throw new SOTException(Recursos.Empleados.apellido_paterno_invalido_excepcion);

            if (!UtilidadesRegex.SoloLetras(empleado.ApellidoMaterno))
                throw new SOTException(Recursos.Empleados.apellido_materno_invalido_excepcion);

            if (empleado.IdPuesto <= 0)
                throw new SOTException(Recursos.Empleados.area_no_seleccionada_excepcion);

            if (empleado.IdTurno <= 0)
                throw new SOTException(Recursos.Empleados.turno_no_seleccionado_excepcion);

            if (empleado.FechaIngreso == DateTime.MinValue)
                throw new SOTException(Recursos.Empleados.fecha_ingreso_invalida_excepcion);

            if (empleado.Salario <= 0)
                throw new SOTException(Recursos.Empleados.salario_invalido_excepcion, empleado.Salario.ToString("C"));

            if (!UtilidadesRegex.SoloDigitos(empleado.NSS))
                throw new SOTException(Recursos.Empleados.numero_seguro_social_invalido_excepcion);

            if (empleado.IdCategoria <= 0)
                throw new SOTException(Recursos.Empleados.categoria_no_seleccionada_excepcion);
        }

        public List<Empleado> ObtenerRecamarerasFILTRO(bool soloHabilitadas = true)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarEmpleado = true });

            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            //int? idTurno;

            //if (soloTurnoActual)
            //    idTurno = ServicioCortesTurno.ObtenerUltimoCorte().Id;
            //else
                //idTurno = null;

            return RepositorioEmpleados.ObtenerPorPuesto(soloHabilitadas, /*idTurno,*/ config.Recamarera ?? 0);
            //return RepositorioEmpleados.ObtenerPorPuesto("Recamarera", soloHabilitadas);
        }

        public List<Empleado> ObtenerEmpleadosFILTRO(bool soloHabilitadas = true)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarEmpleado = true });

            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            //int? idTurno;

            //if (soloTurnoActual)
            //    idTurno = ServicioCortesTurno.ObtenerUltimoCorte().Id;
            //else
            //idTurno = null;

            return RepositorioEmpleados.ObtenerConNombrePuesto(soloHabilitadas);
            //return RepositorioEmpleados.ObtenerPorPuesto("Recamarera", soloHabilitadas);
        }

        public List<Empleado> ObtenerSupervisoresFILTRO(bool soloHabilitados = true)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarEmpleado = true });

            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            int? idTurno;

            //if (soloTurnoActual)
            //    idTurno = ServicioCortesTurno.ObtenerUltimoCorte().Id;
            //else
            //    idTurno = null;

            return RepositorioEmpleados.ObtenerPorPuesto(soloHabilitados, /*idTurno,*/ config.Supervisor ?? 0);
            //return RepositorioEmpleados.ObtenerPorPuesto("Supervisor", soloHabilitados);
        }

        public List<Empleado> ObtenerMeserosFILTRO(bool soloHabilitados)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarEmpleado = true});

            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            int? idTurno;

            //if (soloTurnoActual)
            //    idTurno = ServicioCortesTurno.ObtenerUltimoCorte().Id;
            //else
            //    idTurno = null;

            return RepositorioEmpleados.ObtenerPorPuesto(soloHabilitados, /*idTurno,*/ config.Mesero ?? 0);
            //return RepositorioEmpleados.ObtenerPorPuesto("Mesero", soloHabilitados);
        }

        public List<Empleado> ObtenerValetsFILTRO(bool soloHabilitados = true)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarEmpleado = true });

            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            int? idTurno;

            //if (soloTurnoActual)
            //    idTurno = ServicioCortesTurno.ObtenerUltimoCorte().Id;
            //else
            //    idTurno = null;

            return RepositorioEmpleados.ObtenerPorPuesto(soloHabilitados, /*idTurno,*/ config.Valet ?? 0);
            //return RepositorioEmpleados.ObtenerPorPuesto("Valet", soloHabilitados);
        }

        public void ValidarRecamarerasActivas(List<int> idsEmpleados) 
        {
            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            var idsExistentes =RepositorioEmpleados.ObtenerElementos(m => m.Activo && m.Habilitado 
                                                                     && idsEmpleados.Contains(m.Id) 
                                                                     && m.IdPuesto == config.Recamarera)//.Puesto.Nombre == "Recamarera")
                                                                     .Select(m => m.Id).ToList();

            if (idsEmpleados.Except(idsExistentes).Count() > 0)
                throw new SOTException(Recursos.TareasLimpieza.empleados_no_recamareras_exception);
        }

        public void ValidarSupervisorActivoHabilitado(int idEmpleado)
        {
            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            if (!RepositorioEmpleados.Alguno(m => m.Activo && m.Habilitado
                                                 && m.Id == idEmpleado
                                                 && m.IdPuesto == config.Supervisor))//m.Puesto.Nombre == "Supervisor"))
                throw new SOTException(Recursos.Empleados.empleado_no_supervisor_exception);
        }

        public void ValidarEmpleadoLibre(int idEmpleado) 
        {
            var empleado = RepositorioEmpleados.ObtenerConCantidadTareas(idEmpleado);

            if (empleado == null)
                throw new SOTException(Recursos.TareasLimpieza.id_invalido_excepcion);

            if (empleado.CantidadTareas > 0)
                throw new SOTException(Recursos.TareasLimpieza.recamarera_ocupada_excepcion, empleado.NombreCompleto);
        }


        //public void ValidarMeseroActivoHabilitado(int idEmpleado)
        //{
        //    if (!RepositorioEmpleados.Alguno(m => m.Activo && m.Habilitado
        //                                         && m.Id == idEmpleado
        //                                         && m.Puesto.Nombre == "Mesero"))
        //        throw new SOTException(Recursos.Empleados.empleado_no_mesero_exception);
        //}

        //public void ValidarValetActivoHabilitado(int idEmpleado)
        //{
        //    if (!RepositorioEmpleados.Alguno(m => m.Activo && m.Habilitado
        //                                         && m.Id == idEmpleado
        //                                         && m.Puesto.Nombre == "Valet"))
        //        throw new SOTException(Recursos.Empleados.empleado_no_valet_exception);
        //}

        public void HabilitarDeshabilitarEmpleados(List<int> idsEmpleadosHabilitar, List<int> idsEmpleadosDeshabilitar, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarEmpleado = true });

            if (idsEmpleadosHabilitar.Except(idsEmpleadosDeshabilitar).Count() != idsEmpleadosHabilitar.Count)
                throw new SOTException(Recursos.Empleados.habilitar_deshabilitar_mismo_empleado_exception);

            var fechaActual = DateTime.Now;

            var empleadosDeshabilitar = RepositorioEmpleados.ObtenerElementos(m => idsEmpleadosDeshabilitar.Contains(m.Id) && m.Activo);

            foreach (var tupla in (from idEmpleado in idsEmpleadosDeshabilitar
                                   join empleado in empleadosDeshabilitar on idEmpleado equals empleado.Id
                                     into e
                                   from empleado in e.DefaultIfEmpty()
                                   select new
                                   {
                                       idEmpleado,
                                       empleado
                                   }))
            {
                if (tupla.empleado == null)
                    throw new SOTException(Recursos.Empleados.id_invalido_excepcion, tupla.idEmpleado.ToString());

                if (tupla.empleado.Habilitado)
                {
                    tupla.empleado.Habilitado = false;
                    tupla.empleado.FechaModificacion = fechaActual;
                    tupla.empleado.IdUsuarioModifico = usuario.Id;
                }
            }

            var empleadosHabilitar = RepositorioEmpleados.ObtenerElementos(m => idsEmpleadosHabilitar.Contains(m.Id) && m.Activo);

            foreach (var tupla in (from idEmpleado in idsEmpleadosHabilitar
                                   join empleado in empleadosHabilitar on idEmpleado equals empleado.Id
                                     into e
                                   from empleado in e.DefaultIfEmpty()
                                   select new
                                   {
                                       idEmpleado,
                                       empleado
                                   }))
            {
                if (tupla.empleado == null)
                    throw new SOTException(Recursos.Empleados.id_invalido_excepcion, tupla.idEmpleado.ToString());

                if (!tupla.empleado.Habilitado)
                {
                    tupla.empleado.Habilitado = true;
                    tupla.empleado.FechaModificacion = fechaActual;
                    tupla.empleado.IdUsuarioModifico = usuario.Id;
                }
            }

            foreach (var e in empleadosDeshabilitar)
                RepositorioEmpleados.Modificar(e);

            foreach (var e in empleadosHabilitar)
                RepositorioEmpleados.Modificar(e);

            RepositorioEmpleados.GuardarCambios();
        }

        //public List<Empleado> ObtenerEmpleadosActivosPorPuesto(string nombrePuesto, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarEmpleado = true });

        //    return RepositorioEmpleados.ObtenerPorPuesto(nombrePuesto, false);
        //}

        public List<Empleado> ObtenerEmpleadosActivosPorPuesto(DtoUsuario usuario, /*int? idTurno,*/ params int[] idsPuestos)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarEmpleado = true });

            return RepositorioEmpleados.ObtenerPorPuesto(false, /*idTurno,*/ idsPuestos);
        }

        public void HabilitarDeshabilitarEmpleados(Dictionary<int, bool> idsEmpleadosHabilitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarEmpleado = true });

            if (idsEmpleadosHabilitacion.Count == 0)
                return;

            var keys = idsEmpleadosHabilitacion.Keys.ToList();

            var empleados = RepositorioEmpleados.ObtenerElementos(m => m.Activo && keys.Contains(m.Id)).ToList();

            if (empleados.Count != idsEmpleadosHabilitacion.Count)
                throw new SOTException(Recursos.Empleados.habilitacion_deshabilitacion_eliminados_excepcion);

            var fechaActual = DateTime.Now;

            foreach (var empleado in empleados)
            {
                empleado.Habilitado = idsEmpleadosHabilitacion[empleado.Id];
                empleado.FechaModificacion = fechaActual;
                empleado.IdUsuarioModifico = usuario.Id;

                RepositorioEmpleados.Modificar(empleado);
            }

            RepositorioEmpleados.GuardarCambios();
        }


        public void ValidarRecamareraActiva(int idEmpleado)
        {
            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            if (!RepositorioEmpleados.Alguno(m => m.Activo && m.Habilitado
                                                                     && m.Id == idEmpleado
                                                                     && m.IdPuesto == config.Recamarera))//m.Puesto.Nombre == "Recamarera"))
                throw new SOTException(Recursos.Empleados.empleado_no_recamarera_exception);
        }


        //public List<Puesto> ObtenerPuestosActivos(DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos();

        //    return RepositorioPuestos.ObtenerElementos(m => m.Activo).ToList();
        //}


        


        public Empleado ObtenerEmpleadoPorNumeroNoNull(string numeroEmpleado)
        {
            var empleado = RepositorioEmpleados.Obtener(m => m.NumeroEmpleado.Equals(numeroEmpleado) && m.Activo);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.numero_empleado_busqueda_invalido_excepcion, numeroEmpleado);

            return empleado;
        }


        //public void ValidarCocineroActivoHabilitado(int idEmpleado)
        //{
        //    if (!RepositorioEmpleados.Alguno(m => m.Activo && m.Habilitado
        //                                         && m.Id == idEmpleado
        //                                         && m.Puesto.Nombre == "Cocinero"))
        //        throw new SOTException(Recursos.Empleados.empleado_no_cocinero_exception);
        //}

        public List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreas(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarEmpleado = true });

            return RepositorioEmpleados.ObtenerResumenEmpleadosPuestosAreas();
        }

        public List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreasFILTRO()
        {
            return RepositorioEmpleados.ObtenerResumenEmpleadosPuestosAreas();
        }

        public List<CategoriaEmpleado> ObtenerCategoriasEmpleados() 
        {
            return RepositorioCategoriasEmpleados.ObtenerElementos(m => m.Activa).ToList();
        }


        

        public string ObtenerNombreCompletoEmpleado(int idEmpleado)
        {
            var empleado = RepositorioEmpleados.Obtener(m => m.Id == idEmpleado);// && m.Activo);

            if (empleado == null)
                return "";

            return empleado.NombreCompleto;
        }

        public int ObtenerIdRol(int idEmpleado)
        {
            var empleado = RepositorioEmpleados.Obtener(m => m.Id == idEmpleado && m.Activo, m=>m.Puesto);

            if (empleado == null)
                return 0;

            return empleado.Puesto.IdRol;
        }

        #region métodos internal

        DtoEmpleadoPuesto IServicioEmpleadosInterno.ObtenerResumenEmpleadoPuestoPorId(int idEmpleado) 
        {
            return RepositorioEmpleados.ObtenerResumenEmpleadoPuestoPorId(idEmpleado);
        }

        void IServicioEmpleadosInterno.ValidarPermisos(int idEmpleado, Modelo.Seguridad.Dtos.DtoPermisos permisos) 
        {
            var idRol = RepositorioEmpleados.ObtenerElementos(m => m.Id == idEmpleado, m => m.Puesto).Select(m => m.Puesto.IdRol).FirstOrDefault();

            ServicioPermisos.ValidarPermisos(idRol, permisos);
        }

        List<Empleado> IServicioEmpleadosInterno.ObtenerValets(/*int idTurno*/)
        {
            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            return RepositorioEmpleados.ObtenerPorPuesto(false, /*idTurno,*/ config.Valet ?? 0);
            //return RepositorioEmpleados.ObtenerPorPuesto("Valet", soloHabilitados);
        }

        List<Empleado> IServicioEmpleadosInterno.ObtenerMeseros(/*int idTurno*/)
        {
            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            return RepositorioEmpleados.ObtenerPorPuesto(false, /*idTurno,*/ config.Mesero ?? 0);
            //return RepositorioEmpleados.ObtenerPorPuesto("Valet", soloHabilitados);
        }

        Empleado IServicioEmpleadosInterno.ObtenerEmpleadoActivoNoNullConTurno(int idEmpleado)
        {
            var empleado = RepositorioEmpleados.Obtener(m => m.Id == idEmpleado && m.Activo, m => m.Turno);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.id_invalido_excepcion, idEmpleado.ToString());

            return empleado;
        }


        Empleado IServicioEmpleadosInterno.ObtenerEmpleadoActivoConPuestoPorId(int idEmpleado)
        {
            return RepositorioEmpleados.Obtener(m => m.Id == idEmpleado && m.Activo, m => m.Puesto);
        }

        bool IServicioEmpleadosInterno.VerificarEmpleadoActivo(int idEmpleado)
        {
            return RepositorioEmpleados.Alguno(m => m.Activo && m.Id == idEmpleado);
        }

        Empleado IServicioEmpleadosInterno.ObtenerEmpleadoActivoPorIdConCategoria(int idEmpleado)
        {
            return RepositorioEmpleados.Obtener(m => m.Id == idEmpleado && m.Activo, m => m.CategoriaEmpleado);
        }

        #endregion


        public bool VerificarRolEnUso(int idRol)
        {
            return RepositorioPuestos.Alguno(m => m.Activo && m.IdRol == idRol);
        }


        public bool VerificarPuestoEnUso(int idPuesto)
        {
            return RepositorioEmpleados.Alguno(m => m.Activo && m.IdPuesto == idPuesto);
        }


        public List<Empleado> ObtenerVendedoresFILTRO(/*int idCorteTurno,*/ int? idEmpleadoIncluir)
        {
            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            var empleados = RepositorioEmpleados.ObtenerPorPuesto(false, /*idCorteTurno,*/ config.Vendedor ?? 0);

            if (idEmpleadoIncluir.HasValue && !empleados.Any(m => m.Id == idEmpleadoIncluir))
            {
                var empleadoObligatorio = RepositorioEmpleados.Obtener(m => m.Id == idEmpleadoIncluir);

                if (empleadoObligatorio != null)
                    empleados.Add(empleadoObligatorio);
            }

            return empleados;
        }
        public List<Empleado> ObtenerMeserosFILTRO(/*int idCorteTurno, */int? idEmpleadoIncluir)
        {
            var config = ServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();

            var empleados = RepositorioEmpleados.ObtenerPorPuesto(false, /*idCorteTurno,*/ config.Mesero ?? 0);

            if (idEmpleadoIncluir.HasValue && !empleados.Any(m => m.Id == idEmpleadoIncluir))
            {
                var empleadoObligatorio = RepositorioEmpleados.Obtener(m => m.Id == idEmpleadoIncluir);

                if (empleadoObligatorio != null)
                    empleados.Add(empleadoObligatorio);
            }

            return empleados;
        }
    }
}
