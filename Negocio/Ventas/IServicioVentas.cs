﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Ventas
{
    public interface IServicioVentas
    {
        List<Venta> ObtenerVentasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);
        List<VW_VentaCorrecta> ObtenerVentasCorrectasFiltradas(DtoUsuario usuario, int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);
        DtoResumenVenta ObtenerResumenVenta(int idVenta);
        DtoResumenVenta ObtenerResumenVenta(string transaccion);
        DtoResumenVenta ObtenerResumenVentaRentaCancelada(int idVentaRenta);
        DtoResumenVenta ObtenerResumenComandaCancelada(int idComanda);
        DtoResumenVenta ObtenerResumenConsumoInternoCancelado(int idConsumoInterno);
        DtoResumenVenta ObtenerResumenOcupacionMesaCancelada(int idOcupacionMesa);
        DtoResumenVenta ObtenerResumenOrdenTaxi(int idOrdenTaxi);
    }
}
