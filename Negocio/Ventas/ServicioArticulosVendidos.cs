﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Ventas
{
    public class ServicioArticulosVendidos : IServicioArticulosVendidos
    {
        private IRepositorioArticulosVendidos RepositorioArticulosVendidos
        {
            get { return _repositorioArticulosVendidos.Value; }
        }

        private Lazy<IRepositorioArticulosVendidos> _repositorioArticulosVendidos = new Lazy<IRepositorioArticulosVendidos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioArticulosVendidos>();
        });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });


        public List<VW_ArticuloVendido> ObtenerArticulosVendidos(DtoUsuario usuario, int? ordenTurno = null, string categoria = null, string subCategoria = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, bool? agrupar =null) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarArticulos = true });

            return RepositorioArticulosVendidos.ObtenerArticulosVendidos(ordenTurno, categoria, subCategoria, fechaInicio, fechaFin, agrupar);
        }
    }
}
