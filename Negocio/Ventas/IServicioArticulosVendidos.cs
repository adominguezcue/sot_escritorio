﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Ventas
{
    public interface IServicioArticulosVendidos
    {
        List<VW_ArticuloVendido> ObtenerArticulosVendidos(DtoUsuario usuario, int? ordenTurno = null, string categoria = null, string subCategoria = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, bool? agrupar = null);
    }
}
