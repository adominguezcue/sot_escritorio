﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Ventas
{
    internal interface IServicioVentasInterno: IServicioVentas
    {
        void CancelarVentasPorTransacciones(List<string> transacciones, string transaccionCancelacion, int idUsuario, DateTime fechaCancelacion, string motivoCancelacion);

        List<DtoResumenCancelaciones> SP_ObtenerCancelacionesTurno(int idCorteTurno);
    }
}
