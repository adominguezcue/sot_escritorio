﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Clientes;
using Negocio.Almacen.Parametros;
using Negocio.Pagos;
using Negocio.Preventas;
using Negocio.Rentas;
using Negocio.Restaurantes;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Ventas
{
    public class ServicioVentas : IServicioVentasInterno
    {
        IRepositorioOrdenesRestaurantes RepositorioOrdenesRestaurantes
        {
            get { return _repostorioOrdenesRestaurantes.Value; }
        }

        Lazy<IRepositorioOrdenesRestaurantes> _repostorioOrdenesRestaurantes = new Lazy<IRepositorioOrdenesRestaurantes>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOrdenesRestaurantes>(); });

        IRepositorioOrdenesTaxi RepositorioOrdenesTaxi
        {
            get { return _repostorioOrdenesTaxi.Value; }
        }

        Lazy<IRepositorioOrdenesTaxi> _repostorioOrdenesTaxi = new Lazy<IRepositorioOrdenesTaxi>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOrdenesTaxi>(); });

        IRepositorioVentasRentas RepositorioVentasRenta
        {
            get { return _repostorioVentasRenta.Value; }
        }

        Lazy<IRepositorioVentasRentas> _repostorioVentasRenta = new Lazy<IRepositorioVentasRentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentasRentas>(); });

        IRepositorioTarjetasPuntos RepositorioTarjetasPuntos
        {
            get { return _repostorioTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioTarjetasPuntos> _repostorioTarjetasPuntos = new Lazy<IRepositorioTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTarjetasPuntos>(); });


        IRepositorioPagosTarjetasPuntos RepositorioPagosTarjetasPuntos
        {
            get { return _repostorioPagosTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioPagosTarjetasPuntos> _repostorioPagosTarjetasPuntos = new Lazy<IRepositorioPagosTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosTarjetasPuntos>(); });

        IRepositorioComandas RepositorioComandas
        {
            get { return _repostorioComandas.Value; }
        }

        Lazy<IRepositorioComandas> _repostorioComandas = new Lazy<IRepositorioComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioComandas>(); });

        IRepositorioCambiosComanda RepositorioCambiosComanda
        {
            get { return _repostorioCambiosComandas.Value; }
        }

        Lazy<IRepositorioCambiosComanda> _repostorioCambiosComandas = new Lazy<IRepositorioCambiosComanda>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCambiosComanda>(); });

        IRepositorioConsumosInternos RepositorioConsumosInternos
        {
            get { return _repostorioConsumosInternos.Value; }
        }

        Lazy<IRepositorioConsumosInternos> _repostorioConsumosInternos = new Lazy<IRepositorioConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConsumosInternos>(); });

        IRepositorioVentas RepositorioVentas
        {
            get { return _RepositorioVentas.Value; }
        }

        Lazy<IRepositorioVentas> _RepositorioVentas = new Lazy<IRepositorioVentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentas>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioClientes ServicioClientes
        {
            get { return _servicioClientes.Value; }
        }

        Lazy<IServicioClientes> _servicioClientes = new Lazy<IServicioClientes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioClientes>(); });

        IServicioPagosInterno ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagosInterno> _servicioPagos = new Lazy<IServicioPagosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagosInterno>(); });

        IRepositorioPagosRenta RepositorioPagosRenta
        {
            get { return _repostorioPagosRenta.Value; }
        }

        Lazy<IRepositorioPagosRenta> _repostorioPagosRenta = new Lazy<IRepositorioPagosRenta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosRenta>(); });

        IRepositorioPagosReservaciones RepositorioPagosReservaciones
        {
            get { return _repostorioPagosReservaciones.Value; }
        }

        Lazy<IRepositorioPagosReservaciones> _repostorioPagosReservaciones = new Lazy<IRepositorioPagosReservaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosReservaciones>(); });

        IServicioRentasInterno ServicioRentas
        {
            get { return _servicioRentas.Value; }
        }

        Lazy<IServicioRentasInterno> _servicioRentas = new Lazy<IServicioRentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRentasInterno>(); });

        IRepositorioEmpleados RepositorioEmpleados
        {
            get { return _repostorioEmpleados.Value; }
        }

        Lazy<IRepositorioEmpleados> _repostorioEmpleados = new Lazy<IRepositorioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioEmpleados>(); });

        IRepositorioPagosOcupacionesMesa RepositorioPagosOcupacionesMesa
        {
            get { return _repostorioPagosOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioPagosOcupacionesMesa> _repostorioPagosOcupacionesMesa = new Lazy<IRepositorioPagosOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosOcupacionesMesa>(); });

        IRepositorioPagosComandas RepositorioPagosComandas
        {
            get { return _repostorioPagosComandas.Value; }
        }

        Lazy<IRepositorioPagosComandas> _repostorioPagosComandas = new Lazy<IRepositorioPagosComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosComandas>(); });

        IRepositorioPagosConsumosInternos RepositorioPagosConsumosInternos
        {
            get { return _repostorioPagosConsumosInternos.Value; }
        }

        Lazy<IRepositorioPagosConsumosInternos> _repostorioPagosConsumosInternos = new Lazy<IRepositorioPagosConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosConsumosInternos>(); });

        IServicioRestaurantesInterno ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantesInterno> _servicioRestaurantes = new Lazy<IServicioRestaurantesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantesInterno>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IRepositorioOcupacionesMesa RepositorioOcupacionesMesa
        {
            get { return _repostorioOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioOcupacionesMesa> _repostorioOcupacionesMesa = new Lazy<IRepositorioOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOcupacionesMesa>(); });

        IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });

        IServicioPermisos AplicacionServicioPermisos
        {
            get { return _aplicacionServicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _aplicacionServicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        public List<Venta> ObtenerVentasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            return RepositorioVentas.ObtenerVentasFiltradas(ordenTurno, fechaInicio, fechaFin);
        }

        public List<VW_VentaCorrecta> ObtenerVentasCorrectasFiltradas(DtoUsuario usuario, int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            AplicacionServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarVentas = true });

            return RepositorioVentas.ObtenerVentasCorrectasFiltradas(ordenTurno, fechaInicio, fechaFin);
        }

        public DtoResumenVenta ObtenerResumenVenta(int idVenta)
        {
            var configG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var venta = RepositorioVentas.Obtener(m => m.Id == idVenta);

            var resumen = new DtoResumenVenta
            {
                Direccion = configG.Direccion,
                FechaInicio = venta.FechaModificacion,
                FechaFin = venta.FechaModificacion,
                Hotel = configG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = venta.Ticket.ToString(),
                Cancelada = venta.Cancelada
            };


            IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
            //Modelo.Entidades.DatosFiscales datosF;

            var porcentajeIVA = venta.IVAEnCurso;

            decimal descuentoP = 0;

            switch (venta.ClasificacionVenta)
            {
                case Venta.ClasificacionesVenta.Habitacion:
                    {
                        pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion, venta.Cancelada);//.FechaCancelacion);
                        resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccion(venta.Transaccion);

                        #region detalles de habitación

                        var ventaRenta = RepositorioPagosRenta.ObtenerVentaConConceptos(venta.Transaccion, venta.Cancelada);

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ventaRenta.IdValet ?? 0, ventaRenta.IdUsuarioModifico);

                        if (ventaRenta.EsInicial)
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = 1,
                                Nombre = ventaRenta.TipoHabitacionTmp,
                                PrecioUnidad = Math.Round(ventaRenta.PrecioHotelTmp * (1 + porcentajeIVA), 2)
                            });
                        }
                        foreach (var grupoHE in ventaRenta.Extensiones.Where(m => (m.Activa || venta.Cancelada) && !m.EsRenovacion).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoHE.Count(),
                                Nombre = "Tiempo extra",
                                PrecioUnidad = Math.Round(grupoHE.Key * (1 + porcentajeIVA), 2)
                            });
                        }

                        foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => (m.Activa || venta.Cancelada)).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPE.Count(),
                                Nombre = "Persona extra",
                                PrecioUnidad = Math.Round(grupoPE.Key * (1 + porcentajeIVA), 2)
                            });
                        }

                        foreach (var grupoRen in ventaRenta.Extensiones.Where(m => (m.Activa || venta.Cancelada) && m.EsRenovacion).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoRen.Count(),
                                Nombre = "Renovación",
                                PrecioUnidad = Math.Round(grupoRen.Key * (1 + porcentajeIVA), 2)
                            });
                        }

                        foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || venta.Cancelada) && m.Precio > 0).GroupBy(m => new { m.IdPaquete, m.Precio }))
                        {
                            var muestra = grupoPAQ.First();

                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                                Nombre = muestra.Paquete.Nombre + " (+)",
                                PrecioUnidad = Math.Round(grupoPAQ.Key.Precio * (1 + porcentajeIVA), 2)
                            });
                        }

                        descuentoP = ventaRenta.PaquetesRenta.Where(m => (m.Activo || venta.Cancelada)).Sum(m => Math.Round(m.Descuento * (1 + porcentajeIVA), 2) * m.Cantidad);

                        foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || venta.Cancelada) && m.Descuento > 0).GroupBy(m => new { m.IdPaquete, m.Descuento }))
                        {
                            var muestra = grupoPAQ.First();

                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                                Nombre = muestra.Paquete.Nombre + " (-)",
                                PrecioUnidad = Math.Round(grupoPAQ.Key.Descuento * (1 + porcentajeIVA), 2)
                            });
                        }



                        if(ventaRenta.MontosNoReembolsablesRenta.Any(m => m.Activo || venta.Cancelada))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = 1,//grupoRen.Count(),
                                Nombre = "Ajuste reservación",
                                PrecioUnidad = ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo || venta.Cancelada).Sum(m=> m.ValorConIVA)//Math.Round(grupoRen.Key * (1 + porcentajeIVA), 2)
                            });
                        }

                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.Restaurante:
                    {
                        pagos = RepositorioPagosOcupacionesMesa.ObtenerPagosPorTransaccion(venta.Transaccion);
                        bool esMesa;

                        if (!(esMesa = pagos.Any()))
                        {
                            pagos = RepositorioPagosConsumosInternos.ObtenerPagosPorTransaccion(venta.Transaccion);
                            resumen.Procedencia = "";
                        }
                        else
                        {
                            resumen.Procedencia = "Mesa " + ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(venta.Transaccion);
                        }

                        #region detalles de restaurante

                        if (esMesa)
                        {
                            var ocupacion = RepositorioOcupacionesMesa.ObtenerOcupacionFinalizadaConDetalles(((PagoOcupacionMesa)pagos.First()).IdOcupacionMesa);

                            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ocupacion.IdMesero, ocupacion.IdUsuarioModifico);

                            var idsArticulos = ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

                            var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                            foreach (var artCom in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante))
                            {
                                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                            }

                            foreach (var grupoArticulo in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).GroupBy(m => m.IdArticulo))
                            {
                                var muestra = grupoArticulo.First();

                                resumen.Items.Add(new DtoItemCobrable
                                {
                                    Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                    Nombre = muestra.ArticuloTmp.Desc_Art,
                                    PrecioUnidad = muestra.PrecioUnidadFinal// * (1 + configuracionG.Iva_CatPar),
                                });
                            }
                        }
                        else
                        {
                            var consumoInterno = RepositorioConsumosInternos.ObtenerConsumoInternoEntregadoConDetalles(((PagoConsumoInterno)pagos.First()).IdConsumoInterno);

                            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(consumoInterno.IdMesero ?? 0, consumoInterno.IdUsuarioModifico);

                            var idsArticulos = consumoInterno.ArticulosConsumoInterno.Select(m => m.IdArticulo).ToList();

                            var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                            foreach (var artCom in consumoInterno.ArticulosConsumoInterno)
                            {
                                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                            }

                            foreach (var grupoArticulo in consumoInterno.ArticulosConsumoInterno.GroupBy(m => m.IdArticulo))
                            {
                                var muestra = grupoArticulo.First();

                                resumen.Items.Add(new DtoItemCobrable
                                {
                                    Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                    Nombre = muestra.ArticuloTmp.Desc_Art,
                                    PrecioUnidad = muestra.PrecioUnidadFinal //* (1 + configuracionG.Iva_CatPar),
                                });
                            }
                        }

                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.RoomService:
                    {
                        pagos = RepositorioPagosComandas.ObtenerPagosPorTransaccion(venta.Transaccion);
                        resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(venta.Transaccion);

                        #region detalles de comanda

                        var comanda = RepositorioComandas.ObtenerComandaPagadaConDetalles(((PagoComanda)pagos.First()).IdComanda);

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

                        var idsArticulos = comanda.ArticulosComanda.Select(m => m.IdArticulo).ToList();

                        var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

                        foreach (var artCom in comanda.ArticulosComanda)
                        {
                            artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                        }

                        foreach (var articuloComanda in comanda.ArticulosComanda)
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = articuloComanda.Cantidad,
                                Nombre = articuloComanda.ArticuloTmp.Desc_Art,
                                PrecioUnidad = articuloComanda.PrecioUnidadFinal,
                            });
                        }

                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.TarjetaPuntos:
                    {
                        pagos = RepositorioPagosTarjetasPuntos.ObtenerPagosPorTransaccion(venta.Transaccion);
                        resumen.Procedencia = "";

                        #region detalles de tarjeta de puntos

                        var idTarjeta = ((PagoTarjetaPuntos)pagos.First()).IdTarjetaPuntos;

                        var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Id == idTarjeta);

                        resumen.Items.Add(new DtoItemCobrable
                        {
                            Cantidad = 1,
                            Nombre = "Tarjeta de puntos",
                            PrecioUnidad = pagos.Sum(m => m.Valor)//tarjeta.Precio * (1 + configuracionG.Iva_CatPar),
                        });

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(tarjeta.IdUsuarioModifico);


                        #endregion
                    }
                    break;
                default:
                    pagos = new List<IPago>();
                    resumen.Procedencia = "";
                    //datosF = null;
                    break;
            }

            #region pagos

            var sumaPagos = pagos.Where(m => (m.Activo || venta.Cancelada)).Sum(m => m.Valor) + descuentoP;

            decimal valor = 0, descuento = descuentoP, cortesia = 0, valorV = 0;

            foreach (var pago in pagos)
            {
                switch (pago.TipoPago)
                {
                    case TiposPago.Consumo:
                        descuento += pago.Valor;
                        break;
                    case TiposPago.Cortesia:
                        cortesia += pago.Valor;
                        break;
                    case TiposPago.Cupon:
                        cortesia += pago.Valor;
                        break;
                    case TiposPago.Efectivo:
                        valor += pago.Valor;
                        break;
                    case TiposPago.Reservacion:
                        valor += pago.Valor;
                        break;
                    case TiposPago.TarjetaCredito:
                        valor += pago.Valor;
                        break;
                    case TiposPago.VPoints:
                        descuento += pago.Valor;
                        valorV += pago.Valor;
                        break;
                    case TiposPago.Transferencia:
                        valor += pago.Valor;
                        break;
                }
            }

            if (valorV != 0)
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = 1,
                    Nombre = "VPOINTS (-)",
                    PrecioUnidad = valorV,
                });

            resumen.SubTotal = sumaPagos;
            resumen.Cortesia = cortesia;
            resumen.Descuentos = descuento;
            resumen.Total = valor;

            #endregion

            return resumen;
        }

        public DtoResumenVenta ObtenerResumenVenta(string transaccion)
        {
            var configG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var venta = RepositorioVentas.Obtener(m => m.Transaccion == transaccion);

            var resumen = new DtoResumenVenta
            {
                Direccion = configG.Direccion,
                FechaInicio = venta.FechaModificacion,
                FechaFin = venta.FechaModificacion,
                Hotel = configG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = venta.Ticket.ToString(),
                Cancelada = venta.Cancelada
            };


            IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
            //Modelo.Entidades.DatosFiscales datosF;

            var porcentajeIVA = venta.IVAEnCurso;

            decimal descuentoP = 0;

            switch (venta.ClasificacionVenta)
            {
                case Venta.ClasificacionesVenta.Habitacion:
                    {
                        pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion, venta.Cancelada);//.FechaCancelacion);
                        resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccion(venta.Transaccion);

                        #region detalles de habitación

                        var ventaRenta = RepositorioPagosRenta.ObtenerVentaConConceptos(venta.Transaccion, venta.Cancelada);

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ventaRenta.IdValet ?? 0, ventaRenta.IdUsuarioModifico);

                        if (ventaRenta.EsInicial)
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = 1,
                                Nombre = ventaRenta.TipoHabitacionTmp,
                                PrecioUnidad = Math.Round(ventaRenta.PrecioHotelTmp * (1 + porcentajeIVA), 2)
                            });
                        }
                        foreach (var grupoHE in ventaRenta.Extensiones.Where(m => (m.Activa || venta.Cancelada) && !m.EsRenovacion).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoHE.Count(),
                                Nombre = "Tiempo extra",
                                PrecioUnidad = Math.Round(grupoHE.Key * (1 + porcentajeIVA), 2)
                            });
                        }

                        foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => (m.Activa || venta.Cancelada)).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPE.Count(),
                                Nombre = "Persona extra",
                                PrecioUnidad = Math.Round(grupoPE.Key * (1 + porcentajeIVA), 2)
                            });
                        }

                        foreach (var grupoRen in ventaRenta.Extensiones.Where(m => (m.Activa || venta.Cancelada) && m.EsRenovacion).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoRen.Count(),
                                Nombre = "Renovación",
                                PrecioUnidad = Math.Round(grupoRen.Key * (1 + porcentajeIVA), 2)
                            });
                        }

                        foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || venta.Cancelada) && m.Precio > 0).GroupBy(m => new { m.IdPaquete, m.Precio }))
                        {
                            var muestra = grupoPAQ.First();

                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                                Nombre = muestra.Paquete.Nombre + " (+)",
                                PrecioUnidad = Math.Round(grupoPAQ.Key.Precio * (1 + porcentajeIVA), 2)
                            });
                        }

                        descuentoP = ventaRenta.PaquetesRenta.Where(m => (m.Activo || venta.Cancelada)).Sum(m => Math.Round(m.Descuento * (1 + porcentajeIVA), 2) * m.Cantidad);

                        foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || venta.Cancelada) && m.Descuento > 0).GroupBy(m => new { m.IdPaquete, m.Descuento }))
                        {
                            var muestra = grupoPAQ.First();

                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                                Nombre = muestra.Paquete.Nombre + " (-)",
                                PrecioUnidad = Math.Round(grupoPAQ.Key.Descuento * (1 + porcentajeIVA), 2)
                            });
                        }


                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.Restaurante:
                    {
                        pagos = RepositorioPagosOcupacionesMesa.ObtenerPagosPorTransaccion(venta.Transaccion);
                        bool esMesa;

                        if (!(esMesa = pagos.Any()))
                        {
                            pagos = RepositorioPagosConsumosInternos.ObtenerPagosPorTransaccion(venta.Transaccion);
                            resumen.Procedencia = "";
                        }
                        else
                        {
                            resumen.Procedencia = "Mesa " + ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(venta.Transaccion);
                        }

                        #region detalles de restaurante

                        if (esMesa)
                        {
                            var ocupacion = RepositorioOcupacionesMesa.ObtenerOcupacionFinalizadaConDetalles(((PagoOcupacionMesa)pagos.First()).IdOcupacionMesa);

                            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ocupacion.IdMesero, ocupacion.IdUsuarioModifico);

                            var idsArticulos = ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

                            var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                            foreach (var artCom in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante))
                            {
                                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                            }

                            foreach (var grupoArticulo in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).GroupBy(m => m.IdArticulo))
                            {
                                var muestra = grupoArticulo.First();

                                resumen.Items.Add(new DtoItemCobrable
                                {
                                    Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                    Nombre = muestra.ArticuloTmp.Desc_Art,
                                    PrecioUnidad = muestra.PrecioUnidadFinal// * (1 + configuracionG.Iva_CatPar),
                                });
                            }
                        }
                        else
                        {
                            var consumoInterno = RepositorioConsumosInternos.ObtenerConsumoInternoEntregadoConDetalles(((PagoConsumoInterno)pagos.First()).IdConsumoInterno);

                            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(consumoInterno.IdMesero ?? 0, consumoInterno.IdUsuarioModifico);

                            var idsArticulos = consumoInterno.ArticulosConsumoInterno.Select(m => m.IdArticulo).ToList();

                            var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                            foreach (var artCom in consumoInterno.ArticulosConsumoInterno)
                            {
                                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                            }

                            foreach (var grupoArticulo in consumoInterno.ArticulosConsumoInterno.GroupBy(m => m.IdArticulo))
                            {
                                var muestra = grupoArticulo.First();

                                resumen.Items.Add(new DtoItemCobrable
                                {
                                    Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                    Nombre = muestra.ArticuloTmp.Desc_Art,
                                    PrecioUnidad = muestra.PrecioUnidadFinal //* (1 + configuracionG.Iva_CatPar),
                                });
                            }
                        }

                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.RoomService:
                    {
                        pagos = RepositorioPagosComandas.ObtenerPagosPorTransaccion(venta.Transaccion);
                        resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(venta.Transaccion);

                        #region detalles de comanda

                        var comanda = RepositorioComandas.ObtenerComandaPagadaConDetalles(((PagoComanda)pagos.First()).IdComanda);

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

                        var idsArticulos = comanda.ArticulosComanda.Select(m => m.IdArticulo).ToList();

                        var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

                        foreach (var artCom in comanda.ArticulosComanda)
                        {
                            artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                        }

                        foreach (var articuloComanda in comanda.ArticulosComanda)
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = articuloComanda.Cantidad,
                                Nombre = articuloComanda.ArticuloTmp.Desc_Art,
                                PrecioUnidad = articuloComanda.PrecioUnidadFinal,
                            });
                        }

                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.TarjetaPuntos:
                    {
                        pagos = RepositorioPagosTarjetasPuntos.ObtenerPagosPorTransaccion(venta.Transaccion);
                        resumen.Procedencia = "";

                        #region detalles de tarjeta de puntos

                        var idTarjeta = ((PagoTarjetaPuntos)pagos.First()).IdTarjetaPuntos;

                        var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Id == idTarjeta);

                        resumen.Items.Add(new DtoItemCobrable
                        {
                            Cantidad = 1,
                            Nombre = "Tarjeta de puntos",
                            PrecioUnidad = pagos.Sum(m => m.Valor)//tarjeta.Precio * (1 + configuracionG.Iva_CatPar),
                        });

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(tarjeta.IdUsuarioModifico);


                        #endregion
                    }
                    break;
                default:
                    pagos = new List<IPago>();
                    resumen.Procedencia = "";
                    //datosF = null;
                    break;
            }

            #region pagos

            var sumaPagos = pagos.Where(m => (m.Activo || venta.Cancelada)).Sum(m => m.Valor) + descuentoP;

            decimal valor = 0, descuento = descuentoP, cortesia = 0, valorV = 0;

            foreach (var pago in pagos)
            {
                switch (pago.TipoPago)
                {
                    case TiposPago.Consumo:
                        descuento += pago.Valor;
                        break;
                    case TiposPago.Cortesia:
                        cortesia += pago.Valor;
                        break;
                    case TiposPago.Cupon:
                        cortesia += pago.Valor;
                        break;
                    case TiposPago.Efectivo:
                        valor += pago.Valor;
                        break;
                    case TiposPago.Reservacion:
                        valor += pago.Valor;
                        break;
                    case TiposPago.TarjetaCredito:
                        valor += pago.Valor;
                        break;
                    case TiposPago.VPoints:
                        descuento += pago.Valor;
                        valorV += pago.Valor;
                        break;
                    case TiposPago.Transferencia:
                        valor += pago.Valor;
                        break;
                }
            }

            if (valorV != 0)
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = 1,
                    Nombre = "VPOINTS (-)",
                    PrecioUnidad = valorV,
                });

            resumen.SubTotal = sumaPagos;
            resumen.Cortesia = cortesia;
            resumen.Descuentos = descuento;
            resumen.Total = valor;

            #endregion

            return resumen;
        }

        public DtoResumenVenta ObtenerResumenVentaRentaCancelada(int idVentaRenta)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var ventaRenta = RepositorioPagosRenta.ObtenerVentaCanceladaConConceptos(idVentaRenta);
            Venta venta = null;

            if (ventaRenta == null || (venta = RepositorioVentas.Obtener(m => m.Transaccion == ventaRenta.Transaccion)) != null)
            {
                ventaRenta = RepositorioVentasRenta.Obtener(m => m.Id == idVentaRenta);

                if (ventaRenta != null && !string.IsNullOrEmpty(ventaRenta.Transaccion))
                {
                    venta = RepositorioVentas.Obtener(m => m.Transaccion == ventaRenta.Transaccion);

                    if (venta != null)
                        return ObtenerResumenVenta(venta.Id);
                }
            }

            var preventa = ServicioPreventas.ObtenerPreventaPorId(ventaRenta.IdPreventa ?? 0);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = ventaRenta.FechaModificacion,
                FechaFin = ventaRenta.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa != null ? preventa.Ticket : "",//venta.Ticket.ToString(),
                Cancelada = !ventaRenta.Activo
            };


            //IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
            //Modelo.Entidades.DatosFiscales datosF;

            //pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
            resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorIdRenta(ventaRenta.IdRenta);

            #region detalles de habitación



            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ventaRenta.IdValet ?? 0, ventaRenta.IdUsuarioModifico);

            if (ventaRenta.EsInicial)
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = 1,
                    Nombre = ventaRenta.TipoHabitacionTmp,
                    PrecioUnidad = Math.Round(ventaRenta.PrecioHotelTmp * (1 + configuracionG.Iva_CatPar), 2),
                });
            }
            foreach (var grupoHE in ventaRenta.Extensiones.Where(m => (m.Activa || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion)) && !m.EsRenovacion).GroupBy(m => m.Precio))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoHE.Count(),
                    Nombre = "Tiempo extra",
                    PrecioUnidad = Math.Round(grupoHE.Key * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => (m.Activa || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion))).GroupBy(m => m.Precio))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoPE.Count(),
                    Nombre = "Persona extra",
                    PrecioUnidad = Math.Round(grupoPE.Key * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            foreach (var grupoRen in ventaRenta.Extensiones.Where(m => (m.Activa || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion)) && m.EsRenovacion).GroupBy(m => m.Precio))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoRen.Count(),
                    Nombre = "Renovación",
                    PrecioUnidad = Math.Round(grupoRen.Key * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion)) && m.Precio > 0).GroupBy(m => new { m.IdPaquete, m.Precio }))
            {
                var muestra = grupoPAQ.First();

                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                    Nombre = muestra.Paquete.Nombre + " (+)",
                    PrecioUnidad = Math.Round(grupoPAQ.Key.Precio * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            var descuentos = ventaRenta.PaquetesRenta.Where(m => (m.Activo || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion))).Sum(m => Math.Round(m.Descuento * (1 + configuracionG.Iva_CatPar), 2) * m.Cantidad);

            foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion)) && m.Descuento > 0).GroupBy(m => new { m.IdPaquete, m.Descuento }))
            {
                var muestra = grupoPAQ.First();

                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                    Nombre = muestra.Paquete.Nombre + " (-)",
                    PrecioUnidad = Math.Round(grupoPAQ.Key.Descuento * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            if (ventaRenta.MontosNoReembolsablesRenta.Any(m => m.Activo || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion)))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = 1,//grupoRen.Count(),
                    Nombre = "Ajuste reservación",
                    PrecioUnidad = ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo || (!ventaRenta.Activo && ventaRenta.FechaEliminacion <= m.FechaEliminacion)).Sum(m => m.ValorConIVA)//Math.Round(grupoRen.Key * (1 + porcentajeIVA), 2)
                });
            }


            #endregion


            #region pagos



            resumen.SubTotal = ventaRenta.ValorConIVA + descuentos;//sumaPagos;
            resumen.Cortesia = 0;// cortesia;
            resumen.Descuentos = descuentos;// descuento;
            resumen.Total = ventaRenta.ValorConIVA;// valor;

            #endregion

            return resumen;
        }

        public DtoResumenVenta ObtenerResumenComandaCancelada(int idComanda)
        {
            bool _EsCambio = false;
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var comanda = RepositorioComandas.Obtener(m => m.Id == idComanda);

            var Cambioscomandas = RepositorioCambiosComanda.Obtener(m => m.IdComanda == idComanda);

            if(Cambioscomandas != null)
            {
                _EsCambio = true;
            }

            string procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorIdComanda(comanda.Id);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(comanda.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = comanda.FechaCreacion,
                FechaFin = comanda.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia,
                Cancelada = comanda.Estado == Comanda.Estados.Cancelada
            };

            var articulos = RepositorioComandas.SP_ObtenerResumenesArticulosTicketComandaCancelada(comanda.Id, _EsCambio);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = 0;
            resumen.Cortesia = articulos.Where(m => m.EsCortesia).Sum(m => m.Total);
            resumen.Total = resumen.SubTotal - resumen.Cortesia - resumen.Descuentos;

            return resumen;
        }

        public DtoResumenVenta ObtenerResumenConsumoInternoCancelado(int idConsumoInterno)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            string procedencia = "";//"Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(consumo.Transaccion);

            Venta venta = null;

            var consumo = RepositorioConsumosInternos.Obtener(m => m.Id == idConsumoInterno);

            if (consumo == null || (venta = RepositorioVentas.Obtener(m => m.Transaccion == consumo.Transaccion)) != null)
            {
                consumo = RepositorioConsumosInternos.Obtener(m => m.Id == idConsumoInterno);

                if (consumo != null && !string.IsNullOrEmpty(consumo.Transaccion))
                {
                    venta = RepositorioVentas.Obtener(m => m.Transaccion == consumo.Transaccion);

                    if (venta != null)
                        return ObtenerResumenVenta(venta.Id);
                }
            }


            var preventa = ServicioPreventas.ObtenerPreventaPorId(consumo.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = consumo.FechaCreacion,
                FechaFin = consumo.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia,
                Cancelada = consumo.Estado == Comanda.Estados.Cancelada
            };

            var articulos = RepositorioConsumosInternos.SP_ObtenerResumenesArticulosTicketConsumoInternoCancelado(consumo.Id);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(consumo.IdMesero ?? 0, consumo.IdUsuarioModifico);

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = resumen.SubTotal;
            resumen.Total = 0;

            return resumen;
        }

        public DtoResumenVenta ObtenerResumenOcupacionMesaCancelada(int idOcupacionMesa)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Id == idOcupacionMesa, m=> m.Mesa);

            var procedencia = "Mesa " + ocupacion.Mesa.Clave;//ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(ocupacion.Transaccion);

            ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Id == idOcupacionMesa, m => m.OrdenesRestaurante);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(ocupacion.IdPreventa);

            var ordenes = ocupacion.Estado == OcupacionMesa.Estados.Cancelada ?
                ocupacion.OrdenesRestaurante.Where(m => m.FechaEliminacion >= ocupacion.FechaEliminacion) :
                ocupacion.OrdenesRestaurante.Where(m => m.Estado == Comanda.Estados.PorPagar);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = ordenes.Select(m => m.FechaCreacion).OrderBy(m => m).FirstOrDefault(),
                FechaFin = ordenes.Select(m => m.FechaCreacion).OrderByDescending(m => m).FirstOrDefault(),
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia,
                Cancelada = ocupacion.Estado == OcupacionMesa.Estados.Cancelada
            };

            List<DtoArticuloTicket> articulos = new List<DtoArticuloTicket>();

            foreach (var orden in ordenes)
                articulos.AddRange(RepositorioOrdenesRestaurantes.SP_ObtenerResumenesArticulosTicketOrdenRestauranteCancelada(orden.Id));

            articulos = (from art in articulos
                         group art by new { PrecioUnidad = art.PrecioUnidad, art.Codigo, art.EsCortesia } into ar
                         select ar).ToList().Select(m => new DtoArticuloTicket
                         {
                             Cantidad = m.Sum(x => x.Cantidad),
                             Codigo = m.Key.Codigo,
                             EsCortesia = m.Key.EsCortesia,
                             IdLineaBase = m.First().IdLineaBase,
                             Nombre = m.First().Nombre,
                             PrecioUnidad = m.Key.PrecioUnidad
                         }).ToList();

            List<int> idsEmpleados = ordenes.Select(m => m.IdUsuarioModifico).ToList();
            idsEmpleados.Add(ocupacion.IdMesero);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(idsEmpleados.ToArray());

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = 0;
            resumen.Cortesia = articulos.Where(m => m.EsCortesia).Sum(m => m.Total);
            resumen.Total = resumen.SubTotal - resumen.Cortesia - resumen.Descuentos;

            return resumen;
        }

        public DtoResumenVenta ObtenerResumenOrdenTaxi(int idOrdenTaxi)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var orden = RepositorioOrdenesTaxi.Obtener(m => m.Id == idOrdenTaxi);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(orden.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = orden.FechaCreacion,
                FechaFin = orden.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket//venta.Ticket.ToString(),
            };

            resumen.SubTotal = orden.Precio;
            resumen.Descuentos = 0;
            resumen.Total = orden.Precio;

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(orden.IdEmpleadoCobro ?? 0, orden.IdUsuarioModifico);

            resumen.Items.Add(new DtoItemCobrable
            {
                Cantidad = 1,
                Nombre = "Taxi",
                PrecioUnidad = orden.Precio,
            });

            return resumen;
        }

        #region métodos internal

        void IServicioVentasInterno.CancelarVentasPorTransacciones(List<string> transacciones, string transaccionCancelacion, int idUsuario, DateTime fechaCancelacion, string motivoCancelacion)
        {
            var ventas = RepositorioVentas.ObtenerElementos(m => m.Activa && transacciones.Contains(m.Transaccion)).ToList();

            //var fechaActual = DateTime.Now;

            foreach (var venta in ventas) 
            {
                IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
                //Modelo.Entidades.DatosFiscales datosF;

                switch (venta.ClasificacionVenta)
                {
                    case Venta.ClasificacionesVenta.Habitacion:
                        {
                            pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion, !venta.Activa);//venta.FechaEliminacion);
                        }
                        break;
                    //case Venta.ClasificacionesVenta.Restaurante:
                    //    {
                    //        pagos = RepositorioPagosOcupacionesMesa.ObtenerPagosPorTransaccion(venta.Transaccion);
                    //        bool esMesa;

                    //        if (!(esMesa = pagos.Any()))
                    //            pagos = RepositorioPagosConsumosInternos.ObtenerPagosPorTransaccion(venta.Transaccion);
                    //    }
                    //    break;
                    //case Venta.ClasificacionesVenta.RoomService:
                    //    {
                    //        pagos = RepositorioPagosComandas.ObtenerPagosPorTransaccion(venta.Transaccion);
                    //    }
                    //    break;
                    //case Venta.ClasificacionesVenta.TarjetaPuntos:
                    //    {
                    //        pagos = RepositorioPagosTarjetasPuntos.ObtenerPagosPorTransaccion(venta.Transaccion);
                    //    }
                    //    break;
                    case Venta.ClasificacionesVenta.Reservacion:
                        {
                            pagos = RepositorioPagosReservaciones.ObtenerPagosPorTransaccion(venta.Transaccion, !venta.Activa);//.FechaEliminacion);
                        }
                        break;
                    default:
                        throw new NotSupportedException("De momento solamente se pueden cancelar pagos de habitación y de reservaciones");
                        //pagos = new List<IPago>();
                        //break;
                }

                foreach (var pago in pagos)
                {
                    ServicioPagos.CancelarPago(pago, transaccionCancelacion, idUsuario);

                    //var pagoR = pago as PagoRenta;

                    //pagoR.Activo = false;
                    //pagoR.FechaEliminacion = fechaActual;
                    //pagoR.FechaModificacion = fechaActual;
                    //pagoR.IdUsuarioElimino = idUsuario;
                    //pagoR.IdUsuarioModifico = idUsuario;
                }

                venta.Activa = false;
                venta.Cancelada = true;
                venta.FechaCancelacion = fechaCancelacion;
                venta.FechaEliminacion = fechaCancelacion;
                venta.FechaModificacion = fechaCancelacion;
                venta.IdUsuarioCancelo = idUsuario;
                venta.IdUsuarioElimino = idUsuario;
                venta.IdUsuarioModifico = idUsuario;
                venta.MotivoCancelacion = motivoCancelacion;

                RepositorioVentas.Modificar(venta);
            }

            RepositorioVentas.GuardarCambios();
        }

        List<DtoResumenCancelaciones> IServicioVentasInterno.SP_ObtenerCancelacionesTurno(int idCorteTurno) 
        {
            return RepositorioVentas.SP_ObtenerCancelacionesTurno(idCorteTurno);
        }
        #endregion
    }
}
