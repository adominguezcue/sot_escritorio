﻿using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.ConfiguracionesAsistencia
{
    public class ServicioConfiguracionesAsistencias : Negocio.ConfiguracionesAsistencia.IServicioConfiguracionesAsistencias
    {
        IRepositorioConfiguracionesAsistencias RepositorioConfiguracionesAsistencias
        {
            get { return _repostorioConfiguracionesAsistencias.Value; }
        }

        Lazy<IRepositorioConfiguracionesAsistencias> _repostorioConfiguracionesAsistencias = new Lazy<IRepositorioConfiguracionesAsistencias>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesAsistencias>(); });

        public ConfiguracionAsistencias ObtenerConfiguracionAsistencias() 
        {
            return RepositorioConfiguracionesAsistencias.ObtenerTodo().OrderBy(m => m.Id).FirstOrDefault();
        }

        public void ActualizarConfiguracionAsistencias() 
        {
            throw new NotImplementedException("No se han definido las reglas de negocio para validar esta modificación");
        }
    }
}
