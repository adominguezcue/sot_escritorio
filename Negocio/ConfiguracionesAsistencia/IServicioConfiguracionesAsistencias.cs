﻿using Modelo.Entidades;
using System;

namespace Negocio.ConfiguracionesAsistencia
{
    public interface IServicioConfiguracionesAsistencias
    {
        void ActualizarConfiguracionAsistencias();
        ConfiguracionAsistencias ObtenerConfiguracionAsistencias();
    }
}
