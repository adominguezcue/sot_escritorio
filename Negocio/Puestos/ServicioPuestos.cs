﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Areas;
using Negocio.Empleados;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Puestos
{
    public class ServicioPuestos : IServicioPuestos
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioAreasInterno ServicioAreas
        {
            get { return _servicioAreas.Value; }
        }

        Lazy<IServicioAreasInterno> _servicioAreas = new Lazy<IServicioAreasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioAreasInterno>(); });


        IServicioEmpleados ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleados> _servicioEmpleados = new Lazy<IServicioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>(); });


        IRepositorioPuestos RepositorioPuestos
        {
            get { return _repositorioPuestos.Value; }
        }

        Lazy<IRepositorioPuestos> _repositorioPuestos = new Lazy<IRepositorioPuestos>(() => FabricaDependencias.Instancia.Resolver<IRepositorioPuestos>());


        public List<Puesto> ObtenerPuestos(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarPuestos = true });

            return RepositorioPuestos.ObtenerElementos(m => m.Activo).ToList();
        }


        public void EliminarPuesto(int idPuesto, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarPuestos = true });

            var puesto = RepositorioPuestos.Obtener(m => m.Activo && m.Id == idPuesto);

            if (puesto == null)
                throw new SOTException(Recursos.Puestos.nulo_eliminado_excepcion);

            if (ServicioEmpleados.VerificarPuestoEnUso(idPuesto))
                throw new SOTException(Recursos.Puestos.puesto_en_uso_excepcion);

            var fechaActual = DateTime.Now;

            puesto.Activo = false;
            puesto.FechaEliminacion = fechaActual;
            puesto.FechaModificacion = fechaActual;
            puesto.IdUsuarioElimino = usuario.Id;
            puesto.IdUsuarioModifico = usuario.Id;

            RepositorioPuestos.Modificar(puesto);
            RepositorioPuestos.GuardarCambios();
        }


        public void CrearPuesto(Puesto puesto, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearPuestos = true });

            if (puesto == null)
                throw new SOTException(Recursos.Puestos.nulo_eliminado_excepcion);

            ValidarDatos(puesto);

            var fechaActual = DateTime.Now;

            puesto.Activo = true;
            puesto.FechaCreacion = fechaActual;
            puesto.FechaModificacion = fechaActual;
            puesto.IdUsuarioCreo = usuario.Id;
            puesto.IdUsuarioModifico = usuario.Id;

            RepositorioPuestos.Agregar(puesto);
            RepositorioPuestos.GuardarCambios();
        }

        public void ModificarPuesto(int idPuesto, int idRol, int idArea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarPuestos = true });

            var puesto = RepositorioPuestos.Obtener(m => m.Activo && m.Id == idPuesto);

            if (puesto == null)
                throw new SOTException(Recursos.Puestos.nulo_eliminado_excepcion);

            if (puesto.IdRol == idRol && puesto.IdArea == idArea)
                return;

            ValidarDatos(puesto);

            var fechaActual = DateTime.Now;

            puesto.IdRol = idRol;
            puesto.IdArea = idArea;
            puesto.FechaModificacion = fechaActual;
            puesto.IdUsuarioModifico = usuario.Id;

            RepositorioPuestos.Modificar(puesto);
            RepositorioPuestos.GuardarCambios();
        }

        private void ValidarDatos(Puesto puesto)
        {
            if (!Transversal.Utilidades.UtilidadesRegex.SoloLetras(puesto.Nombre))
                throw new SOTException(Recursos.Puestos.nombre_invalido_excepcion);

            if (!ServicioAreas.VerificarExistencia(puesto.IdArea))
                throw new SOTException(Recursos.Puestos.area_invalida_excepcion);
        }


        public Puesto ObtenerPuestoPorId(int idPuesto)
        {
            return RepositorioPuestos.Obtener(m => m.Activo && m.Id == idPuesto);
        }
    }
}
