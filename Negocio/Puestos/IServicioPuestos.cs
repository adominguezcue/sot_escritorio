﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Puestos
{
    public interface IServicioPuestos
    {
        /// <summary>
        /// Retorna los puestos activos del sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Puesto> ObtenerPuestos(DtoUsuario usuario);
        /// <summary>
        /// Marca un puesto como eliminado, siempre y cuando no esté relacionado a empleados activos
        /// </summary>
        /// <param name="idPuesto"></param>
        /// <param name="usuario"></param>
        void EliminarPuesto(int idPuesto, DtoUsuario usuario);
        /// <summary>
        /// Permite agregar un nuevo puesto al sistema
        /// </summary>
        /// <param name="puesto"></param>
        void CrearPuesto(Puesto puesto, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar el rol del puesto seleccionado
        /// </summary>
        /// <param name="idPuesto"></param>
        /// <param name="idRol"></param>
        void ModificarPuesto(int idPuesto, int idRol, int idArea, DtoUsuario usuario);

        Puesto ObtenerPuestoPorId(int idPuesto);
    }
}
