﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Taxis
{
    public interface IServicioTaxis
    {
        /// <summary>
        /// Retorna las órdenes de taxis que aun no han sido cobrados
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<OrdenTaxi> ObtenerOrdenesPendientes(DtoUsuario usuario);
        /// <summary>
        /// Retorna la cantidad de órdenes de taxis que aun no han sido cobrados
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        int ObtenerCantidadOrdenesPendientes(DtoUsuario usuario);
        /// <summary>
        /// Permite crear una nueva orden de taxi
        /// </summary>
        /// <param name="orden"></param>
        /// <param name="usuario"></param>
        void CrearOrden(OrdenTaxi orden, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar el precio de un taxi
        /// </summary>
        /// <param name="idOrdenTaxi"></param>
        /// <param name="nuevoPrecio"></param>
        /// <param name="usuario"></param>
        void ModificarPrecioTaxi(int idOrdenTaxi, decimal nuevoPrecio, DtoUsuario usuario);
        /// <summary>
        /// Permite cancelar una orden de taxi, siempre que la huella digital pertenezca a un usuario
        /// válido y con los permisos necesarios
        /// </summary>
        /// <param name="idOrden"></param>
        /// <param name="motivo"></param>
        /// <param name="huellaDigital">Array de bytes que representan la huella digital</param>
        void CancelarOrden(int idOrden, string motivo, DtoCredencial credencial);
        /// <summary>
        /// Permite cobrar una orden de taxi, siempre que aún esté pendiente de pago y el usuario
        /// posea los permisos necesarios
        /// </summary>
        /// <param name="idOrden"></param>
        /// <param name="idEmpleado"></param>
        /// <param name="usuario"></param>
        void CobrarOrden(int idOrden, int idEmpleado, DtoUsuario usuario);
        /// <summary>
        /// Valida que no existan taxis pendientes por pagar
        /// </summary>
        /// <param name="usuario"></param>
        void ValidarNoExistenPagosPendientes(DtoUsuario usuario);
    }
}
