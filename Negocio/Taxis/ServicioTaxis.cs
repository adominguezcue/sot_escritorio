﻿using Modelo;
using Modelo.Repositorios;
using Negocio.Empleados;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Tickets;
using Negocio.ConfiguracionesImpresoras;
using System.Transactions;
using Negocio.Preventas;

namespace Negocio.Taxis
{
    public class ServicioTaxis : IServicioTaxisInterno
    {
        IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });

        IServicioConfiguracionesImpresorasInterno ServicioConfiguracionesImpresoras
        {
            get { return _servicioConfiguracionesImpresoras.Value; }
        }

        Lazy<IServicioConfiguracionesImpresorasInterno> _servicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresorasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresorasInterno>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioTicketsInterno ServicioTickets
        {
            get { return _servicioTickets.Value; }
        }

        Lazy<IServicioTicketsInterno> _servicioTickets = new Lazy<IServicioTicketsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTicketsInterno>(); });


        IRepositorioOrdenesTaxi RepositorioOrdenesTaxi
        {
            get { return _repositorioOrdenesTaxi.Value; }
        }

        Lazy<IRepositorioOrdenesTaxi> _repositorioOrdenesTaxi = new Lazy<IRepositorioOrdenesTaxi>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOrdenesTaxi>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });


        public List<OrdenTaxi> ObtenerOrdenesPendientes(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarTaxis = true });

            return RepositorioOrdenesTaxi.ObtenerOrdenesPendientes();
        }

        public int ObtenerCantidadOrdenesPendientes(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarTaxis = true });

            return RepositorioOrdenesTaxi.ObtenerCantidadOrdenesPendientes();
        }

        public void CrearOrden(OrdenTaxi orden, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearTaxis = true });

            if (orden == null)
                throw new SOTException(Recursos.OrdenesTaxi.taxi_nulo_excepcion);

            if (orden.Precio <= 0)
                throw new SOTException(Recursos.OrdenesTaxi.precio_invalido_excepcion);

            var fechaActual = DateTime.Now;

            orden.Estado = OrdenTaxi.Estados.Pendiente;
            orden.FechaCreacion = fechaActual;
            orden.FechaModificacion = fechaActual;
            orden.IdUsuarioCreo = usuario.Id;
            orden.IdUsuarioModifico = usuario.Id;
            orden.MotivoCancelacion = string.Empty;

            orden.IdPreventa = ServicioPreventas.GrenerarPreventa(Venta.ClasificacionesVenta.Taxi);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioOrdenesTaxi.Agregar(orden);
                RepositorioOrdenesTaxi.GuardarCambios();

                scope.Complete();
            }
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            ServicioTickets.ImprimirTicket(orden, configuracionImpresoras.ImpresoraTickets, 1);
            ServicioTickets.ImprimirTicket(orden, configuracionImpresoras.ImpresoraVendedores, 1);
        }

        public void ModificarPrecioTaxi(int idOrdenTaxi, decimal nuevoPrecio, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarTaxis = true });

            var orden = RepositorioOrdenesTaxi.Obtener(m => m.Activo && m.Id == idOrdenTaxi);

            if (orden == null)
                throw new SOTException(Recursos.OrdenesTaxi.taxi_nulo_excepcion);

            if (orden.Estado != OrdenTaxi.Estados.Pendiente)
                throw new SOTException(Recursos.OrdenesTaxi.taxi_no_por_cobrar_excepcion);

            if (nuevoPrecio <= 0)
                throw new SOTException(Recursos.OrdenesTaxi.precio_invalido_excepcion);

            if (orden.Precio == nuevoPrecio)
                return;

            var fechaActual = DateTime.Now;

            orden.FechaModificacion = fechaActual;
            orden.IdUsuarioModifico = usuario.Id;
            orden.Precio = nuevoPrecio;

            RepositorioOrdenesTaxi.Modificar(orden);
            RepositorioOrdenesTaxi.GuardarCambios();

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            ServicioTickets.ImprimirTicket(orden, configuracionImpresoras.ImpresoraTickets, 1);
            ServicioTickets.ImprimirTicket(orden, configuracionImpresoras.ImpresoraVendedores, 1);
        }

        public void CancelarOrden(int idOrden, string motivo, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            var ordenTaxi = RepositorioOrdenesTaxi.Obtener(m => m.Id == idOrden);

            if (ordenTaxi == null)
                throw new SOTException(Recursos.OrdenesTaxi.taxi_nulo_excepcion);

            if (ordenTaxi.Estado != OrdenTaxi.Estados.Pendiente)
                throw new SOTException(Recursos.OrdenesTaxi.taxi_no_por_cobrar_excepcion);

            if (string.IsNullOrWhiteSpace(motivo))
                throw new SOTException(Recursos.OrdenesTaxi.motivo_cancelacion_invalido_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarTaxis = true });


            var fechaActual = DateTime.Now;

            ordenTaxi.Activo = false;
            ordenTaxi.FechaModificacion = fechaActual;
            ordenTaxi.FechaEliminacion = fechaActual;
            ordenTaxi.IdUsuarioCreo = usuario.Id;
            ordenTaxi.IdUsuarioElimino = usuario.Id;
            ordenTaxi.Estado = OrdenTaxi.Estados.Cancelada;
            ordenTaxi.MotivoCancelacion = motivo;

            RepositorioOrdenesTaxi.Modificar(ordenTaxi);
            RepositorioOrdenesTaxi.GuardarCambios();
        }

        public void CobrarOrden(int idOrden, int idEmpleado, DtoUsuario usuario)
        {
            var ordenTaxi = RepositorioOrdenesTaxi.Obtener(m => m.Id == idOrden);

            if (ordenTaxi == null)
                throw new SOTException(Recursos.OrdenesTaxi.taxi_nulo_excepcion);

            if (ordenTaxi.Estado != OrdenTaxi.Estados.Pendiente)
                throw new SOTException(Recursos.OrdenesTaxi.taxi_no_por_cobrar_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CobrarTaxis = true });

            ServicioEmpleados.ValidarPermisos(idEmpleado, new DtoPermisos { Aparcar = true });
            //ServicioEmpleados.ValidarValetActivoHabilitado(idEmpleado);


            var fechaActual = DateTime.Now;

            ordenTaxi.Activo = false;
            ordenTaxi.FechaModificacion = fechaActual;
            ordenTaxi.FechaEliminacion = fechaActual;
            ordenTaxi.IdUsuarioCreo = usuario.Id;
            ordenTaxi.IdUsuarioElimino = usuario.Id;
            ordenTaxi.Estado = OrdenTaxi.Estados.Cobrada;
            ordenTaxi.IdEmpleadoCobro = idEmpleado;

            RepositorioOrdenesTaxi.Modificar(ordenTaxi);
            RepositorioOrdenesTaxi.GuardarCambios();

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            ServicioTickets.ImprimirTicket(ordenTaxi, configuracionImpresoras.ImpresoraTickets, 1);
        }


        List<OrdenTaxi> IServicioTaxisInterno.ObtenerOrdenesCobradasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarTaxis = true });

            return RepositorioOrdenesTaxi.ObtenerOrdenesCobradasPorPeriodo(fechaInicio, fechaFin);
        }


        public void ValidarNoExistenPagosPendientes(DtoUsuario usuario)
        {
            var idEstadoPendiente = (int)OrdenTaxi.Estados.Pendiente;

            if(RepositorioOrdenesTaxi.Alguno(m=>m.IdEstado == idEstadoPendiente))
                throw new SOTException(Recursos.CortesTurno.taxis_pendientes_excepcion);
        }
    }
}
