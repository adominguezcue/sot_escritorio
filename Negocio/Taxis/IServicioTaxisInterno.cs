﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Taxis
{
    internal interface IServicioTaxisInterno : IServicioTaxis
    {
        /// <summary>
        /// Retorna los taxis que ya fueron cobrados dentro del período especificado
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<OrdenTaxi> ObtenerOrdenesCobradasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);
    }
}
