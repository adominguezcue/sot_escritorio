﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using Transversal.Excepciones;
using Modelo.Entidades.Parciales;

namespace Negocio.ConfiguracionesImpresoras
{
    public class ServicioConfiguracionesImpresoras : IServicioConfiguracionesImpresorasInterno
    {
        IRepositorioConfiguracionesImpresoras RepositorioConfiguracionesImpresoras
        {
            get { return _repostorioConfiguracionesImpresoras.Value; }
        }

        Lazy<IRepositorioConfiguracionesImpresoras> _repostorioConfiguracionesImpresoras = new Lazy<IRepositorioConfiguracionesImpresoras>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesImpresoras>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });


        public ConfiguracionImpresoras ObtenerConfiguracionImpresoras(DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConfiguracionImpresoras = true });

            return RepositorioConfiguracionesImpresoras.Obtener(m => m.Activa) ?? new ConfiguracionImpresoras { Activa = true };
        }

        public ConfiguracionImpresoras ObtenerConfiguracionImpresorasLocal(DtoUsuario usuario)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConfiguracionImpresoras = true });

            return Newtonsoft.Json.JsonConvert.DeserializeObject<ConfiguracionImpresoras>(Impresora.Default.Configuracion) ?? new ConfiguracionImpresoras { Activa = true };//Impresora.Default.Configuracion ?? new ConfiguracionImpresoras();
        }


        public void ActualizarConfiguracion(ConfiguracionImpresoras configuracionImpresoras, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarConfiguracionImpresoras = true });

            var configuracionActual = RepositorioConfiguracionesImpresoras.Obtener(m => m.Activa);

            bool esNueva;

            if ((esNueva = configuracionActual == null))
                configuracionActual = configuracionImpresoras;
            else
                configuracionActual.SincronizarPrimitivas(configuracionImpresoras);

            if (string.IsNullOrWhiteSpace(configuracionActual.ImpresoraTickets))
                throw new SOTException(Recursos.Impresoras.impresora_recepcion_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(configuracionActual.ImpresoraCocina))
                throw new SOTException(Recursos.Impresoras.impresora_cocina_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(configuracionActual.ImpresoraBar))
                throw new SOTException(Recursos.Impresoras.impresora_bar_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(configuracionActual.ImpresoraCortes))
                throw new SOTException(Recursos.Impresoras.impresora_cortes_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(configuracionActual.ImpresoraVendedores))
                throw new SOTException(Recursos.Impresoras.impresora_vendedores_invalida_excepcion);

            if (esNueva)
                RepositorioConfiguracionesImpresoras.Agregar(configuracionActual);
            else
                RepositorioConfiguracionesImpresoras.Modificar(configuracionActual);

            RepositorioConfiguracionesImpresoras.GuardarCambios();
        }

        public void ActualizarConfiguracionLocal(ConfiguracionImpresoras configuracionLocal, DtoUsuario usuario)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarConfiguracionImpresoras = true });

            //var configuracionActual = RepositorioConfiguracionesImpresoras.Obtener(m => m.Activa);

            if (configuracionLocal != null)
            {
                if (string.IsNullOrWhiteSpace(configuracionLocal.ImpresoraTickets))
                    configuracionLocal.ImpresoraTickets = null;

                if (string.IsNullOrWhiteSpace(configuracionLocal.ImpresoraCocina))
                    configuracionLocal.ImpresoraCocina = null;

                if (string.IsNullOrWhiteSpace(configuracionLocal.ImpresoraBar))
                    configuracionLocal.ImpresoraBar = null;

                if (string.IsNullOrWhiteSpace(configuracionLocal.ImpresoraCortes))
                    configuracionLocal.ImpresoraCortes = null;

                if (string.IsNullOrWhiteSpace(configuracionLocal.ImpresoraVendedores))
                    configuracionLocal.ImpresoraVendedores = null;

            }

            Impresora.Default.Configuracion = Newtonsoft.Json.JsonConvert.SerializeObject(configuracionLocal ?? new ConfiguracionImpresoras());//.SincronizarPrimitivas(configuracionLocal ?? new ConfiguracionImpresoras());
            Impresora.Default.Save();
        }
        public DtoConfiguracionImpresoras ObtenerConfiguracionImpresoras()
        {
            ConfiguracionImpresoras impresoraLocal;

            try
            {
                impresoraLocal = Newtonsoft.Json.JsonConvert.DeserializeObject<ConfiguracionImpresoras>(Impresora.Default.Configuracion);
            }
            catch
            {
                impresoraLocal = null;
            }


            var impresoraGlobal = RepositorioConfiguracionesImpresoras.Obtener(m => m.Activa);

            if (impresoraLocal != null && impresoraGlobal != null)
            {
                return new DtoConfiguracionImpresoras
                {
                    //Activa = true,
                    ImpresoraBar = impresoraLocal.ImpresoraBar ?? impresoraGlobal.ImpresoraBar,
                    ImpresoraCocina = impresoraLocal.ImpresoraCocina ?? impresoraGlobal.ImpresoraCocina,
                    ImpresoraCortes = impresoraLocal.ImpresoraCortes ?? impresoraGlobal.ImpresoraCortes,
                    ImpresoraTickets = impresoraLocal.ImpresoraTickets ?? impresoraGlobal.ImpresoraTickets,
                    ImpresoraVendedores = impresoraLocal.ImpresoraVendedores ?? impresoraGlobal.ImpresoraVendedores,
                };
            }

            return new DtoConfiguracionImpresoras
            {
                //Activa = true,
                ImpresoraBar = impresoraGlobal?.ImpresoraBar,
                ImpresoraCocina = impresoraGlobal?.ImpresoraCocina,
                ImpresoraCortes = impresoraGlobal?.ImpresoraCortes,
                ImpresoraTickets = impresoraGlobal?.ImpresoraTickets,
                ImpresoraVendedores = impresoraGlobal?.ImpresoraVendedores,
            };
        }

        #region métodos internal

        
        #endregion
    }
}