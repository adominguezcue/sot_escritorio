﻿using Modelo.Entidades;
using Modelo.Entidades.Parciales;
using Modelo.Seguridad.Dtos;
using System;
namespace Negocio.ConfiguracionesImpresoras
{
    public interface IServicioConfiguracionesImpresoras
    {
        /// <summary>
        /// Permite obtener la configuración de las impresoras en el sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        ConfiguracionImpresoras ObtenerConfiguracionImpresoras(DtoUsuario usuario);
        /// <summary>
        /// Permite obtener la configuración de las impresoras en el sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        ConfiguracionImpresoras ObtenerConfiguracionImpresorasLocal(DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar la configuración de las impresoras del sistema
        /// </summary>
        /// <param name="configuracionImpresoras"></param>
        /// <param name="usuario"></param>
        void ActualizarConfiguracion(ConfiguracionImpresoras configuracionImpresoras, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar la configuración de las impresoras del sistema
        /// </summary>
        /// <param name="configuracionImpresoras"></param>
        /// <param name="usuario"></param>
        void ActualizarConfiguracionLocal(ConfiguracionImpresoras configuracionLocal, DtoUsuario usuario);
        /// <summary>
        /// Permite obtener la configuración de las impresoras en el sistema
        /// </summary>
        /// <returns></returns>
        DtoConfiguracionImpresoras ObtenerConfiguracionImpresoras();
    }
}
