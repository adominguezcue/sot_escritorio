﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConfiguracionesImpresoras
{
    internal sealed partial class Impresora : global::System.Configuration.ApplicationSettingsBase 
    {
        public Impresora() 
        {
            Upgrade();
        }

        public override void Upgrade()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.Configuracion) || this.Configuracion.Equals("{}"))
                {
                    base.Upgrade();

                    //var pv = GetPreviousVersion("Configuracion");

                    //this.Configuracion = pv != null ? pv.ToString() : this.Configuracion;
                    //Save();
                }
            }
            catch(Exception ex)
            {
                Transversal.Log.Logger.Error("[ConfigImpresoras]: " + ex.Message);
            }
        }
    }
}
