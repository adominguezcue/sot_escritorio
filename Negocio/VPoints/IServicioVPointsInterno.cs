﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using Negocio.ServiciosExternos.DTOs.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.VPoints
{
    internal interface IServicioVPointsInterno : IServicioVPoints
    {
        /// <summary>
        /// Retorna el saldo en V points de la tarjeta, o lanza una excepción en caso de que la
        /// tarjeta sea inválida
        /// </summary>
        /// <param name="numeroTarjeta"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        decimal ObtenerSaldoTarjetaV(string numeroTarjeta);
        /// <summary>
        /// Descuenta los puntos indicados de la tarjeta proporcionada
        /// </summary>
        /// <param name="numeroTarjeta"></param>
        /// <param name="ticket"></param>
        /// <param name="puntos"></param>
        void DescontarPuntos(string numeroTarjeta, string ticket, decimal puntos);
        /// <summary>
        /// Obtiene la información del cupón en base al id proporcionado
        /// </summary>
        /// <param name="idCupon"></param>
        /// <returns></returns>
        DtoDatosCupon ObtenerCupon(string idCupon);

        Dictionary<TiposPago, decimal> ObtenerDiccionarioPagosPorFecha(DateTime fechaInicio, DateTime fechaFin, params TiposPago[] tiposPagos);
        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Reembolsa los puntos indicados de la tarjeta proporcionada
        /// </summary>
        /// <param name="numeroTarjeta"></param>
        /// <param name="idRenta"></param>
        /// <param name="numeroServicio"></param>
        /// <param name="puntos"></param>
        /// <param name="idUsuario"></param>
        void ReembolsarPuntos(string numeroTarjeta, string numeroServicio, decimal puntos, int idUsuario);
        /// <summary>
        /// Cancela un registro de abonos de puntos
        /// </summary>
        /// <param name="idRenta"></param>
        /// <param name="numeroDeTarjeta"></param>
        /// <param name="idUsuario"></param>
        void CancelarAbonosPuntos(int idRenta, string numeroDeTarjeta, int idUsuario);
    }
}
