﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.ServiciosExternos.Agentes.VPoints;
using Negocio.Seguridad.Permisos;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo;
using Negocio.ServiciosExternos.DTOs.VPoints;
using Negocio.ConfiguracionesGlobales;
using Modelo.Seguridad.Dtos;
using Modelo.Repositorios;
using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Pagos;
using System.Transactions;
using Negocio.ServiciosExternos.Excepciones;
using Dominio.Nucleo.Entidades;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesImpresoras;
using Negocio.Tickets;

namespace Negocio.VPoints
{
    public class ServicioVPoints : IServicioVPointsInterno
    {
        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioConfiguracionesPuntosLealtad ServicioConfiguracionesPuntosLealtad
        {
            get { return _servicioConfiguracionesPuntosLealtad.Value; }
        }

        Lazy<IServicioConfiguracionesPuntosLealtad> _servicioConfiguracionesPuntosLealtad = new Lazy<IServicioConfiguracionesPuntosLealtad>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesPuntosLealtad>(); });

        IServicioConfiguracionesImpresorasInterno ServicioConfiguracionesImpresoras
        {
            get { return _servicioConfiguracionesImpresoras.Value; }
        }

        Lazy<IServicioConfiguracionesImpresorasInterno> _servicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresorasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresorasInterno>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioTicketsInterno ServicioTickets
        {
            get { return _servicioTickets.Value; }
        }

        Lazy<IServicioTicketsInterno> _servicioTickets = new Lazy<IServicioTicketsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTicketsInterno>(); });
        
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioPagos ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagos> _servicioPagos = new Lazy<IServicioPagos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagos>(); });

        IRepositorioTarjetasPuntos RepositorioTarjetasPuntos
        {
            get { return _repositorioTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioTarjetasPuntos> _repositorioTarjetasPuntos = new Lazy<IRepositorioTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTarjetasPuntos>(); });

        IRepositorioAbonosPuntos RepositorioAbonosPuntos
        {
            get { return _repositorioAbonosPuntos.Value; }
        }

        Lazy<IRepositorioAbonosPuntos> _repositorioAbonosPuntos = new Lazy<IRepositorioAbonosPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioAbonosPuntos>(); });

        IRepositorioReembolsosPuntos RepositorioReembolsosPuntos
        {
            get { return _repositorioReembolsosPuntos.Value; }
        }

        Lazy<IRepositorioReembolsosPuntos> _repositorioReembolsosPuntos = new Lazy<IRepositorioReembolsosPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioReembolsosPuntos>(); });

        IRepositorioErroresCupon RepositorioErroresCupon
        {
            get { return _repositorioErroresCupon.Value; }
        }

        Lazy<IRepositorioErroresCupon> _repositorioErroresCupon = new Lazy<IRepositorioErroresCupon>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioErroresCupon>(); });

        public List<TarjetaPuntos> ObtenerTarjetas(DtoUsuario usuario, DateTime? fechaInicio = null, DateTime? fechaFin = null, TarjetaPuntos.EstadosTarjeta? estado = null) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTarjetasLealtad = true });

            return RepositorioTarjetasPuntos.ObtenerTarjetas(fechaInicio, fechaFin, estado);
        }

        public void CrearTarjeta(TarjetaPuntos tarjeta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearTarjetasLealtad = true });

            ValidarDatos(tarjeta);

            var fechaActual = DateTime.Now;

            tarjeta.Activo = true;
            tarjeta.FechaCreacion = fechaActual;
            tarjeta.FechaModificacion = fechaActual;
            tarjeta.IdUsuarioCreo = usuario.Id;
            tarjeta.IdUsuarioModifico = usuario.Id;
            tarjeta.EstadoTarjeta = TarjetaPuntos.EstadosTarjeta.Creada;

            RepositorioTarjetasPuntos.Agregar(tarjeta);
            RepositorioTarjetasPuntos.GuardarCambios();
        }

        public void ModificarTarjeta(TarjetaPuntos tarjeta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarTarjetasLealtad = true });

            ValidarDatos(tarjeta);

            var estadoActual = RepositorioTarjetasPuntos.Obtener(m => m.Id == tarjeta.Id).EstadoTarjeta;

            if (estadoActual == TarjetaPuntos.EstadosTarjeta.Vendida)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_registrada_excepcion);

            if (estadoActual == TarjetaPuntos.EstadosTarjeta.Alta)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_alta_excepcion);

            if (tarjeta.EstadoTarjeta != estadoActual)
                throw new SOTException(Recursos.TarjetasPuntos.estado_no_modificable_excepcion);

            var fechaActual = DateTime.Now;

            tarjeta.Activo = true;
            tarjeta.FechaModificacion = fechaActual;
            tarjeta.IdUsuarioModifico = usuario.Id;

            RepositorioTarjetasPuntos.Modificar(tarjeta);
            RepositorioTarjetasPuntos.GuardarCambios();
        }

        public void EliminarTarjeta(int idTarjeta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarTarjetasLealtad = true });

            var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Id == idTarjeta && m.Activo);

            if (tarjeta == null)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_nula_eliminada_excepcion);

            if (tarjeta.EstadoTarjeta == TarjetaPuntos.EstadosTarjeta.Vendida)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_registrada_excepcion);

            if (tarjeta.EstadoTarjeta == TarjetaPuntos.EstadosTarjeta.Alta)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_alta_excepcion);

            var fechaActual = DateTime.Now;

            tarjeta.Activo = false;
            //tarjeta.EstadoTarjeta = TarjetaPuntos.EstadosTarjeta.Eliminada;
            tarjeta.FechaEliminacion = fechaActual;
            tarjeta.FechaModificacion = fechaActual;
            tarjeta.IdUsuarioElimino = usuario.Id;
            tarjeta.IdUsuarioModifico = usuario.Id;

            RepositorioTarjetasPuntos.Modificar(tarjeta);
            RepositorioTarjetasPuntos.GuardarCambios();
        }

        private void ValidarDatos(TarjetaPuntos tarjeta)
        {
            if (tarjeta == null)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_nula_excepcion);

            if (RepositorioTarjetasPuntos.Alguno(m => m.Activo && m.Id != tarjeta.Id && (m.IdCliente.Trim().ToUpper().Equals(tarjeta.IdCliente.Trim().ToUpper()) || m.NumeroDeTarjeta.Trim().ToUpper().Equals(tarjeta.NumeroDeTarjeta.Trim().ToUpper()))))
                throw new SOTException(Recursos.TarjetasPuntos.datos_duplicados_excepcion);

            //if (tarjeta.Precio <= 0)
            //    throw new SOTException(Recursos.TarjetasPuntos.precio_invalido_excepcion);
        }

        public void VenderTarjeta(string idCliente, List<DtoInformacionPago> pagos, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { VenderTarjetasLealtad = true });

            var config = ServicioParametros.ObtenerParametros();
            var configuracionV = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Activo && m.IdCliente == idCliente);

            if (tarjeta == null)
                throw new SOTException(Recursos.TarjetasPuntos.id_cliente_invalido_excepcion);

            if (tarjeta.EstadoTarjeta != TarjetaPuntos.EstadosTarjeta.Creada)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_no_estado_creado_excepcion);

            if (pagos.Any(m => m.TipoPago != TiposPago.Efectivo && m.TipoPago != TiposPago.TarjetaCredito))
                throw new SOTException(Recursos.TarjetasPuntos.tipos_pago_no_soportados_excepcion);

            if (pagos == null || pagos.Count == 0)
                throw new SOTException(Recursos.TarjetasPuntos.forma_pago_no_seleccionada_excepcion);

            var precioFinal = Math.Round(configuracionV.PrecioTarjetas * (1 + config.Iva_CatPar), 2);

            if (pagos.Sum(m => m.Valor) != precioFinal) //tarjeta.Precio)
                throw new SOTException(Recursos.TarjetasPuntos.valor_pagos_diferente_valor_tarjeta_excepcion, precioFinal.ToString("C"), pagos.Sum(m => m.Valor).ToString("C"));

            var agenteV = new AgenteWsVPoints();

            var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
            var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();
            var sucursalServicio = ObtenerSucursalServicios();

            var fechaActual = DateTime.Now;
            var transaccion = Guid.NewGuid().ToString();

            tarjeta.FechaModificacion = fechaActual;

            tarjeta.FechaModificacion = fechaActual;
            tarjeta.IdUsuarioModifico = usuario.Id;
            tarjeta.EstadoTarjeta = TarjetaPuntos.EstadosTarjeta.Vendida;

            foreach (var pago in pagos)
            {
                tarjeta.PagosTarjetaPuntos.Add(new PagoTarjetaPuntos
                {
                    Activo = true,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    TipoPago = pago.TipoPago,
                    TipoTarjeta = pago.TipoTarjeta,
                    NumeroTarjeta = pago.NumeroTarjeta ?? "",
                    Referencia = pago.Referencia,
                    Valor = pago.Valor,
                    Transaccion = transaccion
                });
            }
            try
            {
                var venta = ServicioPagos.GenerarVentaNoCompletada(tarjeta.PagosTarjetaPuntos.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.TarjetaPuntos, null);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    ServicioPagos.Pagar(venta.Id, tarjeta.PagosTarjetaPuntos.Where(m => m.Activo), usuario);

                    RepositorioTarjetasPuntos.Modificar(tarjeta);
                    RepositorioTarjetasPuntos.GuardarCambios();


                    agenteV.RegistrarTarjeta(usuarioServicio, contrasenaServicio, tarjeta.NumeroDeTarjeta, idCliente, sucursalServicio);


                    scope.Complete();
                }

                var configI = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

                ServicioTickets.ImprimirTicket(venta, configI.ImpresoraTickets, 2);
            }
            catch (VPointsException)
            {
                RepositorioTarjetasPuntos.DescartarCambios();

                var tarjetaN = RepositorioTarjetasPuntos.Obtener(m => m.Id == tarjeta.Id);
                tarjetaN.EstadoTarjeta = TarjetaPuntos.EstadosTarjeta.Invalida;
                tarjetaN.FechaModificacion = fechaActual;

                tarjetaN.FechaModificacion = fechaActual;
                tarjetaN.IdUsuarioModifico = usuario.Id;

                RepositorioTarjetasPuntos.Modificar(tarjetaN);
                RepositorioTarjetasPuntos.GuardarCambios();

                throw;
            }
        }

        public void AltaWebTarjeta(string idCliente, /*List<DtoInformacionPago> pagos, */DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { VenderTarjetasLealtad = true });

            var config = ServicioParametros.ObtenerParametros();
            var configuracionV = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Activo && m.IdCliente == idCliente);

            if (tarjeta == null)
                throw new SOTException(Recursos.TarjetasPuntos.id_cliente_invalido_excepcion);

            if (tarjeta.EstadoTarjeta != TarjetaPuntos.EstadosTarjeta.Creada)
                throw new SOTException(Recursos.TarjetasPuntos.tarjeta_no_estado_creado_excepcion);

            /*if (pagos.Any(m => m.TipoPago != TiposPago.Efectivo && m.TipoPago != TiposPago.TarjetaCredito))
                throw new SOTException(Recursos.TarjetasPuntos.tipos_pago_no_soportados_excepcion);

            if (pagos == null || pagos.Count == 0)
                throw new SOTException(Recursos.TarjetasPuntos.forma_pago_no_seleccionada_excepcion);*/

            var precioFinal = Math.Round(configuracionV.PrecioTarjetas * (1 + config.Iva_CatPar), 2);

            /*if (pagos.Sum(m => m.Valor) != precioFinal) //tarjeta.Precio)
                throw new SOTException(Recursos.TarjetasPuntos.valor_pagos_diferente_valor_tarjeta_excepcion, precioFinal.ToString("C"), pagos.Sum(m => m.Valor).ToString("C"));*/

            var agenteV = new AgenteWsVPoints();

            var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
            var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();
            var sucursalServicio = ObtenerSucursalServicios();

            var fechaActual = DateTime.Now;
            var transaccion = Guid.NewGuid().ToString();

            tarjeta.FechaModificacion = fechaActual;

            tarjeta.FechaModificacion = fechaActual;
            tarjeta.IdUsuarioModifico = usuario.Id;
            tarjeta.EstadoTarjeta = TarjetaPuntos.EstadosTarjeta.Alta;

            /*foreach (var pago in pagos)
            {
                tarjeta.PagosTarjetaPuntos.Add(new PagoTarjetaPuntos
                {
                    Activo = true,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    TipoPago = pago.TipoPago,
                    TipoTarjeta = pago.TipoTarjeta,
                    NumeroTarjeta = pago.NumeroTarjeta ?? "",
                    Referencia = pago.Referencia,
                    Valor = pago.Valor,
                    Transaccion = transaccion
                });
            }*/
            try
            {
                //var venta = ServicioPagos.GenerarVentaNoCompletada(tarjeta.PagosTarjetaPuntos.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.TarjetaPuntos, null);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    //ServicioPagos.Pagar(venta.Id, tarjeta.PagosTarjetaPuntos.Where(m => m.Activo), usuario);

                    RepositorioTarjetasPuntos.Modificar(tarjeta);
                    RepositorioTarjetasPuntos.GuardarCambios();


                    agenteV.RegistrarTarjeta(usuarioServicio, contrasenaServicio, tarjeta.NumeroDeTarjeta, idCliente, sucursalServicio);


                    scope.Complete();
                }

                var configI = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

                //ServicioTickets.ImprimirTicket(venta, configI.ImpresoraTickets, 2);
            }
            catch (VPointsException)
            {
                RepositorioTarjetasPuntos.DescartarCambios();

                var tarjetaN = RepositorioTarjetasPuntos.Obtener(m => m.Id == tarjeta.Id);
                tarjetaN.EstadoTarjeta = TarjetaPuntos.EstadosTarjeta.Invalida;
                tarjetaN.FechaModificacion = fechaActual;

                tarjetaN.FechaModificacion = fechaActual;
                tarjetaN.IdUsuarioModifico = usuario.Id;

                RepositorioTarjetasPuntos.Modificar(tarjetaN);
                RepositorioTarjetasPuntos.GuardarCambios();

                throw;
            }
        }

        public decimal ObtenerSaldoTarjetaV(string numeroTarjeta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarSaldoTarjetasLealtad = true });

            return ((IServicioVPointsInterno)this).ObtenerSaldoTarjetaV(numeroTarjeta);
        }

        public DtoDatosCupon ObtenerCupon(string idCupon, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarCupones = true });

            return ((IServicioVPointsInterno)this).ObtenerCupon(idCupon);
        }

        public void CanjearCupon(string idCupon, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CanjearCupones = true });

            var agenteV = new AgenteWsVPoints();

            var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
            var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();
            var sucursal = ObtenerSucursalServicios();

            agenteV.CanjearCupon(usuarioServicio, contrasenaServicio, idCupon, sucursal);
        }

        private void AcumularPuntosDetalle(string numeroDeTarjeta, string ticket, decimal montoConsumoTotal, decimal montoHabitacion,
                                          decimal montoPersonasExtra, decimal montoAlimentos, decimal montoBebidas, decimal montoSexSpa/*, 
                                          DtoUsuario usuario*/)

        {
            //if (usuario == null)
            //    throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

            //ServicioPermisos.ValidarPermisos();

            var agenteV = new AgenteWsVPoints();

            var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
            var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();
            var sucursalServicio = ObtenerSucursalServicios();

            agenteV.AcumularPuntosDetalle(usuarioServicio, contrasenaServicio, numeroDeTarjeta, ticket, montoConsumoTotal, montoHabitacion,
                                          montoPersonasExtra, montoAlimentos, montoBebidas, montoSexSpa, sucursalServicio);
        }

        private string ObtenerSucursalServicios()
        {
            var cg = ServicioParametros.ObtenerParametros();

#warning cambiar por NombreSucurtal
            return cg.Direccion;
        }

        //private string ObtenerContrasenaServicios()
        //{
        //    return System.Configuration.ConfigurationManager.AppSettings["contrasenaV"] ?? "";
        //}

        //private string ObtenerUsuarioServicios()
        //{
        //    return System.Configuration.ConfigurationManager.AppSettings["usuarioV"] ?? "";
        //}


        public void CancelarErroresCupon(string idCupon)
        {
            var errores = RepositorioErroresCupon.ObtenerElementos(m=> m.Cupon == idCupon && !m.EsCancelado).ToList();

            foreach (var error in errores)
            {
                error.EsCancelado = true;

                RepositorioErroresCupon.Modificar(error);
            }

            RepositorioErroresCupon.GuardarCambios();
        }

        public void RegistrarErrorCupon(string idCupon, string error)
        {
            RepositorioErroresCupon.Agregar(new ErrorCupon
            {
                Cupon = idCupon,
                Error = error
            });

            RepositorioErroresCupon.GuardarCambios();
        }

        public void AbrirAbonoPuntos(int idRenta, string numeroDeTarjeta, string ticket, decimal montoHabitacion, decimal montoPersonasExtra, decimal montoAlimentos, decimal montoBebidas, decimal montoSexSpa, DtoUsuario usuario) 
        { 
            var fechaActual = DateTime.Now;

            //FIX
            montoPersonasExtra = 0;

            RepositorioAbonosPuntos.Agregar(new AbonoPuntos
            {
                IdRenta = idRenta,
                Activo = true,
                Abierto = true,
                ConsumoAlimentos = montoAlimentos,
                ConsumoBebidas = montoBebidas,
                ConsumoSexAndSpa = montoSexSpa,
                ConsumoHabitacion = montoHabitacion,
                ConsumoPersonasExtra = montoPersonasExtra,
                Tarjeta = numeroDeTarjeta,
                TicketInicial = ticket,
                TicketFinal = ticket,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                IdUsuarioModifico = usuario.Id,
                ConsumoTotal = montoAlimentos + montoBebidas + montoSexSpa + montoHabitacion + montoPersonasExtra
            });

            RepositorioAbonosPuntos.GuardarCambios();
        }

        public void RegistrarAbonosPuntos(int idRenta, string numeroDeTarjeta, string ticket, decimal montoHabitacion, decimal montoPersonasExtra, decimal montoAlimentos, decimal montoBebidas, decimal montoSexSpa, DtoUsuario usuario) 
        {
            var abonoAbierto = RepositorioAbonosPuntos.Obtener(m => m.Activo && m.Abierto && m.IdRenta == idRenta && m.Tarjeta == numeroDeTarjeta);

            //FIX
            montoPersonasExtra = 0;

            if (abonoAbierto != null)
            {
                abonoAbierto.ConsumoAlimentos += montoAlimentos;
                abonoAbierto.ConsumoBebidas += montoBebidas;
                abonoAbierto.ConsumoSexAndSpa += montoSexSpa;
                abonoAbierto.ConsumoHabitacion += montoHabitacion;
                abonoAbierto.ConsumoPersonasExtra += montoPersonasExtra;
                abonoAbierto.TicketFinal = ticket;
                abonoAbierto.FechaModificacion = DateTime.Now;
                abonoAbierto.IdUsuarioModifico = usuario.Id;
                abonoAbierto.ConsumoTotal += (montoAlimentos + montoBebidas + montoSexSpa + montoHabitacion + montoPersonasExtra);

                RepositorioAbonosPuntos.Modificar(abonoAbierto);
                RepositorioAbonosPuntos.GuardarCambios();
            }
        }

        public void CerrarAbonosPuntos(int idRenta, string numeroDeTarjeta, DtoUsuario usuario)
        {
            var abonoAbierto = RepositorioAbonosPuntos.Obtener(m => m.Activo && m.Abierto && m.IdRenta == idRenta && m.Tarjeta == numeroDeTarjeta);

            if (abonoAbierto != null)
            {
                abonoAbierto.Abierto = false;

                Procesar(abonoAbierto, usuario);

                RepositorioAbonosPuntos.Modificar(abonoAbierto);
                RepositorioAbonosPuntos.GuardarCambios();
            }
        }

        

        public void AbonarManualmente(int idAbono, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { GestionarPuntosLealtad = true });

            var abonoErroneo = RepositorioAbonosPuntos.Obtener(m => m.Activo && !m.Abonado && !m.Abierto && m.Id == idAbono, m => m.Renta);

            if (abonoErroneo == null)
                throw new SOTException(Recursos.TarjetasPuntos.abono_realizado_cancelado_excepcion);

            if (abonoErroneo.Renta.Estado != Renta.Estados.Finalizada)
                throw new SOTException(Recursos.Rentas.renta_no_finalizada_excepcion);

            var ex = Procesar(abonoErroneo, usuario);

            if (ex != null)
                throw ex;

            RepositorioAbonosPuntos.Modificar(abonoErroneo);
            RepositorioAbonosPuntos.GuardarCambios();
        }

        public void ReembolsarManualmente(int idReembolso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { GestionarPuntosLealtad = true });

            var reembolso = RepositorioReembolsosPuntos.Obtener(m => m.Activo && !m.Abonado && m.Id == idReembolso);

            if (reembolso == null)
                throw new SOTException(Recursos.TarjetasPuntos.reembolso_realizado_cancelado_excepcion);

            var ex = ProcesarReembolso(reembolso, usuario.Id);

            if (ex != null)
                throw ex;

            RepositorioReembolsosPuntos.Modificar(reembolso);
            RepositorioReembolsosPuntos.GuardarCambios();
        }

        private Exception Procesar(AbonoPuntos abono, DtoUsuario usuario)
        {
            abono.ErrorControlado = false;
            abono.MotivoError = null;
            abono.FechaModificacion = DateTime.Now;
            abono.IdUsuarioModifico = usuario.Id;

            try
            {
                this.AcumularPuntosDetalle(abono.Tarjeta, abono.TicketFinal, abono.ConsumoTotal, abono.ConsumoHabitacion,
                                           abono.ConsumoPersonasExtra, abono.ConsumoAlimentos, abono.ConsumoBebidas, abono.ConsumoSexAndSpa);
                abono.Abonado = true;
            }
            catch (Negocio.ServiciosExternos.Excepciones.VPointsException e)
            {
                abono.Abonado = false;
                abono.ErrorControlado = true;
                abono.MotivoError = e.Message;

                return e;
            }
            catch (SOTException)
            {
                throw;
            }
            catch (Exception e)
            {
                abono.Abonado = false;
                abono.ErrorControlado = false;
                abono.MotivoError = e.Message;

                return e;
            }

            return null;
        }


        public List<AbonoPuntos> ObtenerAbonosFallidos(DtoUsuario usuario, int? idHabitacion = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { GestionarPuntosLealtad = true });

            return RepositorioAbonosPuntos.ObtenerAbonosFallidos(idHabitacion, fechaInicial, fechaFinal);
        }

        public List<ReembolsoPuntos> ObtenerReembolsosFallidos(DtoUsuario usuario, int? idHabitacion = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { GestionarPuntosLealtad = true });

            return RepositorioReembolsosPuntos.ObtenerReembolsosFallidos(idHabitacion, fechaInicial, fechaFinal);
        }

        #region métodos internal

        void IServicioVPointsInterno.CancelarAbonosPuntos(int idRenta, string numeroDeTarjeta, int idUsuario)
        {
            var abonoAbierto = RepositorioAbonosPuntos.Obtener(m => m.Activo && m.Abierto && m.IdRenta == idRenta && m.Tarjeta == numeroDeTarjeta);

            if (abonoAbierto != null)
            {
                var fechaActual = DateTime.Now;

                abonoAbierto.Activo = false;
                abonoAbierto.Abierto = false;
                abonoAbierto.ErrorControlado = false;
                abonoAbierto.MotivoError = null;
                abonoAbierto.FechaModificacion = fechaActual;
                abonoAbierto.IdUsuarioModifico = idUsuario;
                abonoAbierto.FechaEliminacion = fechaActual;
                abonoAbierto.IdUsuarioElimino = idUsuario;

                RepositorioAbonosPuntos.Modificar(abonoAbierto);
                RepositorioAbonosPuntos.GuardarCambios();
            }
        }

        Dictionary<TiposTarjeta, decimal> IServicioVPointsInterno.ObtenerDiccionarioPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioTarjetasPuntos.ObtenerDiccionarioPagosTarjetaPorFecha(fechaInicio, fechaFin);


            foreach (var item in Enum.GetValues(typeof(TiposTarjeta)).Cast<TiposTarjeta>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }

        decimal IServicioVPointsInterno.ObtenerSaldoTarjetaV(string numeroTarjeta)
        {
            var agenteV = new AgenteWsVPoints();

            var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
            var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();

            return agenteV.ObtenerSaldo(usuarioServicio, contrasenaServicio, numeroTarjeta);
        }

        void IServicioVPointsInterno.DescontarPuntos(string numeroTarjeta, string ticket, decimal puntos)
        {
            //ServicioPermisos.ValidarPermisos();

            var agenteV = new AgenteWsVPoints();

            var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
            var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();
            var sucursalServicio = ObtenerSucursalServicios();

            agenteV.DescontarPuntos(usuarioServicio, contrasenaServicio, numeroTarjeta, ticket, puntos, sucursalServicio);
        }

        void IServicioVPointsInterno.ReembolsarPuntos(string numeroTarjeta, string numeroServicio, decimal puntos, int idUsuario)
        {
            //ServicioPermisos.ValidarPermisos();

            DateTime fechaActual = DateTime.Now;

            var reembolso = new ReembolsoPuntos
            {
                Activo = true, 
                Abonado = false,
                ErrorControlado = false,
                MotivoError = null,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = idUsuario,
                IdUsuarioModifico = idUsuario,
                MontoReembolsar = puntos,
                Tarjeta = numeroTarjeta,
                NumeroServicio = numeroServicio
            };

            if (ProcesarReembolso(reembolso, idUsuario) != null)
            {
                RepositorioReembolsosPuntos.Agregar(reembolso);
                RepositorioReembolsosPuntos.GuardarCambios();
            }
        }

        

        DtoDatosCupon IServicioVPointsInterno.ObtenerCupon(string idCupon)
        {

            var agenteV = new AgenteWsVPoints();

            var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

            var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
            var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();

            return agenteV.ObtenerCupon(usuarioServicio, contrasenaServicio, idCupon);
        }

        Dictionary<TiposPago, decimal> IServicioVPointsInterno.ObtenerDiccionarioPagosPorFecha(DateTime fechaInicio, DateTime fechaFin, params TiposPago[] tiposPagos)
        {
            var diccionario = RepositorioTarjetasPuntos.ObtenerDetallesPagosPorFecha(tiposPagos.Select(m => (int)m).ToList(), fechaInicio, fechaFin);

            if (tiposPagos.Length == 0)
                foreach (var item in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }
            else
                foreach (var item in tiposPagos)
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }

            return diccionario;
        }

        #endregion

        private Exception ProcesarReembolso(ReembolsoPuntos reembolso, int idUsuario)
        {
            reembolso.ErrorControlado = false;
            reembolso.MotivoError = null;
            reembolso.FechaModificacion = DateTime.Now;
            reembolso.IdUsuarioModifico = idUsuario;

            try
            {
                var agenteV = new AgenteWsVPoints();

                var configuracionPuntos = ServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();

                var usuarioServicio = configuracionPuntos?.Usuario;//ObtenerUsuarioServicios();
                var contrasenaServicio = configuracionPuntos?.Contrasena;//ObtenerContrasenaServicios();
                var sucursalServicio = ObtenerSucursalServicios();

                agenteV.DescontarPuntos(usuarioServicio, contrasenaServicio, reembolso.Tarjeta, reembolso.NumeroServicio, -reembolso.MontoReembolsar, sucursalServicio);

                reembolso.Abonado = true;

            }
            catch (Negocio.ServiciosExternos.Excepciones.VPointsException e)
            {
                reembolso.Abonado = false;
                reembolso.ErrorControlado = true;
                reembolso.MotivoError = e.Message;

                return e;
            }
            catch (SOTException)
            {
                throw;
            }
            catch (Exception e)
            {
                reembolso.Abonado = false;
                reembolso.ErrorControlado = false;
                reembolso.MotivoError = e.Message;

                return e;
            }

            return null;
        }
    }
}
