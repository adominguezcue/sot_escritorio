﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.VPoints
{
    public interface IServicioConfiguracionesPuntosLealtad
    {
        ConfiguracionPuntosLealtad ObtenerConfiguracionPuntosLealtad();
        void GuardarConfiguracion(ConfiguracionPuntosLealtad configuracion, DtoUsuario usuario);
        void EliminarConfiguracion(DtoUsuario usuario);
    }
}
