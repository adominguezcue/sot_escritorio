﻿using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.ServiciosExternos.DTOs.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.VPoints
{
    public interface IServicioVPoints
    {
        /// <summary>
        /// Retorna la lista de tarjetas de puntos en base a los filtros porporcionados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        List<TarjetaPuntos> ObtenerTarjetas(DtoUsuario usuario, DateTime? fechaInicio = null, DateTime? fechaFin = null, TarjetaPuntos.EstadosTarjeta? estado = null);
        /// <summary>
        /// Permite registrar una nueva tarjeta de puntos en el sistema
        /// </summary>
        /// <param name="tarjeta"></param>
        /// <param name="usuario"></param>
        void CrearTarjeta(TarjetaPuntos tarjeta, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar los datos de una tarjeta existente
        /// </summary>
        /// <param name="tarjeta"></param>
        /// <param name="usuario"></param>
        void ModificarTarjeta(TarjetaPuntos tarjeta, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar una tarjeta del sistema, siempre y cuando no haya sido vendida
        /// </summary>
        /// <param name="idTarjeta"></param>
        /// <param name="usuario"></param>
        void EliminarTarjeta(int idTarjeta, DtoUsuario usuario);
        /// <summary>
        /// Permite dar de alta una nueva tarjeta V en web, pero sin activación. También almacena las formas de pago de manera local.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="pagos"></param>
        /// <param name="usuario"></param>
        void VenderTarjeta(string idCliente, List<DtoInformacionPago> pagos, DtoUsuario usuario);
        /// <summary>
        /// Permite dar de alta una nueva tarjeta V en web, pero sin activación. No requiere de formas de pago.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="usuario"></param>
        void AltaWebTarjeta(string idCliente, /*List<DtoInformacionPago> pagos, */DtoUsuario usuario);
        /// <summary>
        /// Retorna el saldo en V points de la tarjeta, o lanza una excepción en caso de que la
        /// tarjeta sea inválida
        /// </summary>
        /// <param name="numeroTarjeta"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        decimal ObtenerSaldoTarjetaV(string numeroTarjeta, DtoUsuario usuario);
        /// <summary>
        /// Obtiene la información del cupón en base al id proporcionado
        /// </summary>
        /// <param name="idCupon"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        DtoDatosCupon ObtenerCupon(string idCupon, DtoUsuario usuario);
        /// <summary>
        /// Permite canjear un cupón
        /// </summary>
        /// <param name="idCupon"></param>
        /// <param name="usuario"></param>
        void CanjearCupon(string idCupon, DtoUsuario usuario);
        ///// <summary>
        ///// Permite acumular puntos al momento de la salida de un cliente en base a su consumo
        ///// </summary>
        ///// <param name="numeroDeTarjeta"></param>
        ///// <param name="ticket"></param>
        ///// <param name="montoConsumoTotal"></param>
        ///// <param name="montoHabitacion"></param>
        ///// <param name="montoPersonasExtra"></param>
        ///// <param name="montoAlimentos"></param>
        ///// <param name="montoBebidas"></param>
        ///// <param name="montoSexSpa"></param>
        ///// <param name="usuario"></param>
        //void AcumularPuntosDetalle(string numeroDeTarjeta, string ticket, decimal montoConsumoTotal, decimal montoHabitacion,
        //                                  decimal montoPersonasExtra, decimal montoAlimentos, decimal montoBebidas, decimal montoSexSpa,
        //                                  DtoUsuario usuario);
        /// <summary>
        /// Cancela los registros de errores para el cupón proporcionado
        /// </summary>
        /// <param name="idCupon"></param>
        void CancelarErroresCupon(string idCupon);
        /// <summary>
        /// Registra el error para el cupón proporcionado
        /// </summary>
        /// <param name="idCupon"></param>
        /// <param name="error"></param>
        void RegistrarErrorCupon(string idCupon, string error);
        /// <summary>
        /// Abre un registro de todo lo que se vaya acumulando durante una estancia, con el fin de abonarlo a una tarjeta de puntos
        /// </summary>
        /// <param name="idRenta"></param>
        /// <param name="numeroDeTarjeta"></param>
        /// <param name="ticket"></param>
        /// <param name="montoHabitacion"></param>
        /// <param name="montoPersonasExtra"></param>
        /// <param name="montoAlimentos"></param>
        /// <param name="montoBebidas"></param>
        /// <param name="montoSexSpa"></param>
        /// <param name="usuario"></param>
        void AbrirAbonoPuntos(int idRenta, string numeroDeTarjeta, string ticket, decimal montoHabitacion, decimal montoPersonasExtra, decimal montoAlimentos, decimal montoBebidas, decimal montoSexSpa, DtoUsuario usuario);
        /// <summary>
        /// Actualiza los montos de consumo de un registro de abonos
        /// </summary>
        /// <param name="idRenta"></param>
        /// <param name="numeroDeTarjeta"></param>
        /// <param name="ticket"></param>
        /// <param name="montoHabitacion"></param>
        /// <param name="montoPersonasExtra"></param>
        /// <param name="montoAlimentos"></param>
        /// <param name="montoBebidas"></param>
        /// <param name="montoSexSpa"></param>
        /// <param name="usuario"></param>
        void RegistrarAbonosPuntos(int idRenta, string numeroDeTarjeta, string ticket, decimal montoHabitacion, decimal montoPersonasExtra, decimal montoAlimentos, decimal montoBebidas, decimal montoSexSpa, DtoUsuario usuario);
        /// <summary>
        /// Cierra un registro de abonos e intenta abonar los montos. Si ocurre un error no controlado se guardará el motivo para poder solicitar que se abonen después
        /// </summary>
        /// <param name="idRenta"></param>
        /// <param name="numeroDeTarjeta"></param>
        /// <param name="usuario"></param>
        void CerrarAbonosPuntos(int idRenta, string numeroDeTarjeta, DtoUsuario usuario);
        /// <summary>
        /// Permite realizar el abono de los vpoints de manera manual. Esto es útil en los casos en
        /// que un error inesperado evita que se realice de forma correcta el abono de puntos, un
        /// ejemplo sería un error de red
        /// </summary>
        /// <param name="idAbono"></param>
        /// <param name="usuario"></param>
        void AbonarManualmente(int idAbono, DtoUsuario usuario);
        /// <summary>
        /// Retorna los datos de los puntos que no pudieron ser abonados por algun error externo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="idHabitacion"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<AbonoPuntos> ObtenerAbonosFallidos(DtoUsuario usuario, int? idHabitacion = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null);
        /// <summary>
        /// Retorna los datos de los puntos que no pudieron ser reembolsados por algun error externo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="idHabitacion"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<ReembolsoPuntos> ObtenerReembolsosFallidos(DtoUsuario UsuarioActual, int? idHabitacion, DateTime? fechaInicial, DateTime? fechaFinal);

        void ReembolsarManualmente(int idReembolso, DtoUsuario UsuarioActual);
    }
}
