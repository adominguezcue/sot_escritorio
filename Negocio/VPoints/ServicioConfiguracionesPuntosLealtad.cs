﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using Negocio.ServiciosExternos.ProveedoresConfiguraciones;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.VPoints
{
    public class ServicioConfiguracionesPuntosLealtad : IServicioConfiguracionesPuntosLealtad, IProveedorConfiguracionVPoints
    {
        IServicioPermisos AplicacionServicioPermisos
        {
            get { return _aplicacionServicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _aplicacionServicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IRepositorioConfiguracionesPuntosLealtad DominioRepositorioConfiguracionPuntosLealtad
        {
            get { return _dominioRepositorioConfiguracionPuntosLealtad.Value; }
        }

        Lazy<IRepositorioConfiguracionesPuntosLealtad> _dominioRepositorioConfiguracionPuntosLealtad = new Lazy<IRepositorioConfiguracionesPuntosLealtad>(() => FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesPuntosLealtad>());

        public ConfiguracionPuntosLealtad ObtenerConfiguracionPuntosLealtad()
        {
#warning validar permisos por sistema

            return DominioRepositorioConfiguracionPuntosLealtad.Obtener(m => true);
        }

        public void GuardarConfiguracion(ConfiguracionPuntosLealtad configuracion, DtoUsuario usuario)
        {
            AplicacionServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EditarConfiguracionPuntosLealtad = true });

            if (configuracion == null)
                throw new SOTException(Recursos.TarjetasPuntos.configuracion_nula_excepcion);

            var configuracionActual = DominioRepositorioConfiguracionPuntosLealtad.Obtener(m => true);

            if (configuracionActual == null)
            {
                ValidarDatos(configuracion);

                DominioRepositorioConfiguracionPuntosLealtad.Agregar(configuracion);
            }
            else
            {
                configuracionActual.SincronizarPrimitivas(configuracion);

                ValidarDatos(configuracionActual);

                DominioRepositorioConfiguracionPuntosLealtad.Modificar(configuracionActual);
            }

            DominioRepositorioConfiguracionPuntosLealtad.GuardarCambios();
        }

        private void ValidarDatos(ConfiguracionPuntosLealtad configuracion)
        {
            UtilidadesUrl.ValidarUrl(configuracion.UrlServicio, "GET");

            if (string.IsNullOrWhiteSpace(configuracion.Usuario))
                throw new SOTException(Recursos.TarjetasPuntos.usuario_servicio_invalido_excepcion);

            if (string.IsNullOrWhiteSpace(configuracion.Contrasena))
                throw new SOTException(Recursos.TarjetasPuntos.contrasena_servicio_invalido_excepcion);

            if (configuracion.PrecioTarjetas <= 0)
                throw new SOTException(Recursos.TarjetasPuntos.precio_invalido_excepcion);
        }

        public void EliminarConfiguracion(DtoUsuario usuario)
        {
            AplicacionServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarConfiguracionPuntosLealtad = true });

            var configuracionActual = DominioRepositorioConfiguracionPuntosLealtad.Obtener(m => true);

            if (configuracionActual == null)
                return;

            DominioRepositorioConfiguracionPuntosLealtad.Eliminar(configuracionActual);
            DominioRepositorioConfiguracionPuntosLealtad.GuardarCambios();
        }

        #region Servicios externos

        string IProveedorConfiguracionVPoints.ObtenerUrl()
        {
            var configuracionActual = DominioRepositorioConfiguracionPuntosLealtad.Obtener(m => true);

            return configuracionActual?.UrlServicio;
        }

        #endregion
    }
}
