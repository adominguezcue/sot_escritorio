﻿using Modelo;
using Modelo.Repositorios;
using Negocio.Habitaciones;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Usuarios;
using Transversal.Extensiones;
using Modelo.Dtos;
using Negocio.ConceptosMantenimiento;
using Negocio.Mantenimientos;
using System.Transactions;

namespace Negocio.TareasMantenimiento
{
    public class ServicioTareasMantenimiento : IServicioTareasMantenimientoInterno
    {
        IServicioConceptosMantenimientoInterno ServicioConceptosMantenimiento
        {
            get { return _servicioConceptosMantenimiento.Value; }
        }

        Lazy<IServicioConceptosMantenimientoInterno> _servicioConceptosMantenimiento = new Lazy<IServicioConceptosMantenimientoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosMantenimientoInterno>(); });

        IServicioMantenimientosInterno ServicioMantenimientos
        {
            get { return _servicioMantenimientos.Value; }
        }

        Lazy<IServicioMantenimientosInterno> _servicioMantenimientos = new Lazy<IServicioMantenimientosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioMantenimientosInterno>(); });


        IRepositorioTareasMantenimiento RepositorioTareasMantenimiento
        {
            get { return _repostorioTareasMantenimientoes.Value; }
        }

        Lazy<IRepositorioTareasMantenimiento> _repostorioTareasMantenimientoes = new Lazy<IRepositorioTareasMantenimiento>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTareasMantenimiento>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });


        //public void CrearTarea(int idHabitacion, string descripcion, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos();

        //    if (RepositorioTareasMantenimiento.Alguno(m => m.Activa && m.IdHabitacion == idHabitacion))
        //        throw new SOTException(Recursos.TareasMantenimiento.mantenimiento_en_curso_excepcion);

        //    var fechaActual = DateTime.Now;

        //    var tarea = new TareaMantenimiento
        //    {
        //        Activa = true,
        //        Descripcion = descripcion,
        //        FechaCreacion = fechaActual,
        //        FechaModificacion = fechaActual,
        //        IdUsuarioCreo = usuario.Id,
        //        IdUsuarioModifico = usuario.Id,
        //        IdHabitacion = idHabitacion,
        //        Estado = TareaMantenimiento.Estados.EnProceso
        //    };

        //    RepositorioTareasMantenimiento.Agregar(tarea);
        //    RepositorioTareasMantenimiento.GuardarCambios();
        //}

        public void CrearTareaInstantanea(int idHabitacion, int idConceptoMantenimiento, string descripcion, DtoUsuario usuario)
        {
            var tarea = new TareaMantenimiento
            {
                Activa = true,
                Descripcion = descripcion,
                FechaInicio = DateTime.Now,
                IdHabitacion = idHabitacion,
                IdConceptoMantenimiento = idConceptoMantenimiento,
                TipoMantenimiento = TareaMantenimiento.TiposMantenimiento.Correctivo
            };

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                CrearTareaPrivado(tarea, usuario);
                ServicioHabitaciones.PonerEnMantenimiento(idHabitacion, tarea.Id, usuario);

                scope.Complete();
            }
        }

        public void CrearTarea(TareaMantenimiento tarea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearTareaMantenimiento = true });

            CrearTareaPrivado(tarea, usuario);
        }

        private void CrearTareaPrivado(TareaMantenimiento tarea, DtoUsuario usuario)
        {
            //if (RepositorioTareasMantenimiento.Alguno(m => m.Activa && m.IdHabitacion == idHabitacion))
            //    throw new SOTException(Recursos.TareasMantenimiento.mantenimiento_en_curso_excepcion);

            ServicioHabitaciones.ValidarEsActiva(tarea.IdHabitacion);

            ValidarDatos(tarea);

            var fechaActual = DateTime.Now;

            tarea.Activa = true;
            tarea.FechaCreacion = fechaActual;
            tarea.FechaModificacion = fechaActual;
            tarea.IdUsuarioCreo = usuario.Id;
            tarea.IdUsuarioModifico = usuario.Id;
            tarea.FechaSiguiente = tarea.FechaInicio;
            //tarea.Estado = TareaMantenimiento.Estados.Creada;

            RepositorioTareasMantenimiento.Agregar(tarea);
            RepositorioTareasMantenimiento.GuardarCambios();
        }

        public void ModificarTarea(TareaMantenimiento tarea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarTareaMantenimiento = true });

            if (!tarea.Activa)
                throw new SOTException(Recursos.TareasMantenimiento.eliminacion_no_permitida_excepcion);

            //if (RepositorioTareasMantenimiento.Alguno(m => m.Activa && m.IdHabitacion == idHabitacion))
            //    throw new SOTException(Recursos.TareasMantenimiento.mantenimiento_en_curso_excepcion);

            ServicioHabitaciones.ValidarEsActiva(tarea.IdHabitacion);

            ValidarDatos(tarea);
            DateTime? ultimaFecha;

            tarea.FechaSiguiente = tarea.FechaInicio;

            if (tarea.Periodicidad.HasValue && (ultimaFecha = ServicioMantenimientos.ObtenerFechaUltimoMantenimiento(tarea.Id)).HasValue)
            {
                ultimaFecha = new DateTime(Math.Max(ultimaFecha.Value.Ticks, tarea.FechaSiguiente.Ticks));

                switch (tarea.Periodicidad.Value)
                {
                    case Periodicidades.Anualmente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddYears(tarea.Lapso);
                        break;
                    case Periodicidades.Mensualmente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddMonths(tarea.Lapso);
                        break;
                    case Periodicidades.Semanalmente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddDays(tarea.Lapso * 7);
                        break;
                    case Periodicidades.Diariamente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddDays(tarea.Lapso);
                        break;
                }
            }

            var fechaActual = DateTime.Now;

            tarea.FechaModificacion = fechaActual;
            tarea.IdUsuarioModifico = usuario.Id;
            //tarea.Estado = TareaMantenimiento.Estados.Creada;


            RepositorioTareasMantenimiento.Modificar(tarea);
            RepositorioTareasMantenimiento.GuardarCambios();
        }

        public void EliminarTarea(int idTarea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarTareaMantenimiento = true });

            var tarea = RepositorioTareasMantenimiento.Obtener(m => m.Id == idTarea);

            if (tarea == null)
                throw new SOTException(Recursos.TareasMantenimiento.tarea_no_encontrada_excepcion);

            var fechaActual = DateTime.Now;

            tarea.Activa = false;
            tarea.FechaModificacion = fechaActual;
            tarea.IdUsuarioModifico = usuario.Id;
            tarea.FechaEliminacion = fechaActual;
            tarea.IdUsuarioElimino = usuario.Id;
            //tarea.Estado = TareaMantenimiento.Estados.Creada;


            RepositorioTareasMantenimiento.Modificar(tarea);
            RepositorioTareasMantenimiento.GuardarCambios();
        }

        private void ValidarDatos(TareaMantenimiento tarea)
        {
            if (!ServicioConceptosMantenimiento.VerificarExiste(tarea.IdConceptoMantenimiento, tarea.Id != 0))
                throw new SOTException(Recursos.TareasMantenimiento.concepto_no_seleccionado_excepcion);

            if (string.IsNullOrWhiteSpace(tarea.Descripcion))
                throw new SOTException(Recursos.TareasMantenimiento.descripcion_invalida_excepcion);

            if (!EnumExtensions.ComoLista<TareaMantenimiento.TiposMantenimiento>().Contains(tarea.TipoMantenimiento))
                throw new SOTException(Recursos.TareasMantenimiento.tipo_mantenimiento_invalido_excepcion);

            if (tarea.TipoMantenimiento == TareaMantenimiento.TiposMantenimiento.Correctivo && tarea.Periodicidad.HasValue)
                throw new SOTException(Recursos.TareasMantenimiento.mantenimiento_correctivo_no_repetible_excepcion);

            if (tarea.Periodicidad.HasValue && !EnumExtensions.ComoLista<Periodicidades>().Contains(tarea.Periodicidad.Value))
                throw new SOTException(Recursos.TareasMantenimiento.periodicidad_invalida_excepcion);

            if (!tarea.Periodicidad.HasValue && tarea.FechaFin.HasValue)
                throw new SOTException(Recursos.TareasMantenimiento.fecha_fin_no_permitida_excepcion);

            if (tarea.FechaFin.HasValue && tarea.FechaFin.Value < tarea.FechaInicio)
                throw new SOTException(Recursos.TareasMantenimiento.fecha_fin_menor_fecha_inicio_excepcion);

            if (tarea.Periodicidad.HasValue && tarea.Lapso <= 0)
                throw new SOTException(Recursos.TareasMantenimiento.lapso_invalido_excepcion);

        }

        //public void FinalizarTarea(int idHabitacion, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos();

        //    var tarea = RepositorioTareasMantenimiento.Obtener(m => m.Activa && m.IdHabitacion == idHabitacion);

        //    if (tarea == null)
        //        throw new SOTException(Recursos.TareasMantenimiento.tarea_nula_excepcion);

        //    var fechaActual = DateTime.Now;


        //    tarea.Activa = false;
        //    tarea.FechaEliminacion = fechaActual;
        //    tarea.FechaModificacion = fechaActual;
        //    tarea.IdUsuarioElimino = usuario.Id;
        //    tarea.IdUsuarioModifico = usuario.Id;
        //    tarea.Estado = TareaMantenimiento.Estados.Finalizada;

        //    RepositorioTareasMantenimiento.Modificar(tarea);
        //    RepositorioTareasMantenimiento.GuardarCambios();
        //}

        //public List<TareaMantenimiento> ObtenerTareasHabitacion(int idHabitacion, DtoUsuario usuario, int pagina = 0, int elementos = 0) 
        //{
        //    ServicioPermisos.ValidarPermisos();


        //    List<TareaMantenimiento> retorno;

        //    if (pagina == 0 && elementos == 0)
        //        retorno = RepositorioTareasMantenimiento.ObtenerElementos(m => m.IdHabitacion == idHabitacion).ToList();
        //    else
        //        retorno = RepositorioTareasMantenimiento.ObtenerElementos(m => m.IdHabitacion == idHabitacion, m => m.FechaCreacion, pagina, elementos).ToList();

        //    foreach (var grupo in retorno.GroupBy(m=>m.IdUsuarioCreo)) 
        //    {
        //        var alias = ServicioUsuarios.ObtenerAlias(grupo.Key);

        //        foreach (var item in grupo) 
        //        {
        //            item.NombreUsuario = alias;
        //        }
        //    }

        //    return retorno;
        //}

        //public TareaMantenimiento ObtenerTareaActual(int idHabitacion, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos();

        //    ServicioHabitaciones.ValidarEstaMantenimiento(idHabitacion);

        //    var mantenimiento = RepositorioTareasMantenimiento.Obtener(m => m.Activa && m.IdHabitacion == idHabitacion);

        //    if (mantenimiento != null)
        //        return mantenimiento;

        //    return mantenimiento;
        //}

        public int ObtenerCantidadTareasPendientes(DtoUsuario usuario, int idHabitacion = 0)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTareaMantenimiento = true });

            return RepositorioTareasMantenimiento.SP_ObtenerCantidadTareasPendientes(idHabitacion);
        }

        public List<DtoTareaMantenimientoBase> ObtenerTareasPorMes(int mes, int anio, DtoUsuario usuario, int idHabitacion = 0)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTareaMantenimiento = true });

            return RepositorioTareasMantenimiento.SP_ObtenerTareasMes(mes, anio, idHabitacion);
        }


        public TareaMantenimiento ObtenerTareaPorId(int idTarea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTareaMantenimiento = true });

            return (this as IServicioTareasMantenimientoInterno).ObtenerTareaPorId(idTarea);
        }

        public List<DtoResumenMantenimiento> ObtenerMantenimientosPendientes(int idHabitacion, DtoUsuario usuario, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTareaMantenimiento = true });


            return RepositorioTareasMantenimiento.ObtenerMantenimientosPendientes(idHabitacion, fechaInicio, fechaFin, pagina, elementos);

            //if (pagina == 0 && elementos == 0)
            //    retorno = RepositorioMantenimientos.ObtenerElementos(m => m.IdHabitacion == idHabitacion).ToList();
            //else
            //    retorno = RepositorioMantenimientos.ObtenerElementos(m => m.IdHabitacion == idHabitacion, m => m.FechaCreacion, pagina, elementos).ToList();

            //foreach (var grupo in retorno.GroupBy(m => m.IdUsuario))
            //{
            //    var alias = ServicioUsuarios.ObtenerAlias(grupo.Key);

            //    foreach (var item in grupo)
            //    {
            //        item.NombreUsuario = alias;
            //    }
            //}

            //return retorno;
        }

        #region métodos internal

        TareaMantenimiento IServicioTareasMantenimientoInterno.ObtenerTareaPorId(int idTarea)
        {
            return RepositorioTareasMantenimiento.Obtener(m => m.Activa && m.Id == idTarea);
        }

        void IServicioTareasMantenimientoInterno.CalcularSiguienteFecha(int idTarea)
        {
            var tarea = RepositorioTareasMantenimiento.Obtener(m => m.Id == idTarea && m.Activa);

            if (tarea == null)
                return;

            //if (!tarea.Activa)
            //    throw new SOTException(Recursos.TareasMantenimiento.eliminacion_no_permitida_excepcion);

            //if (RepositorioTareasMantenimiento.Alguno(m => m.Activa && m.IdHabitacion == idHabitacion))
            //    throw new SOTException(Recursos.TareasMantenimiento.mantenimiento_en_curso_excepcion);

            //ServicioHabitaciones.ValidarEsActiva(tarea.IdHabitacion);

            //ValidarDatos(tarea);
            DateTime? ultimaFecha;


            tarea.FechaSiguiente = tarea.FechaInicio;

            if (tarea.Periodicidad.HasValue && (ultimaFecha = ServicioMantenimientos.ObtenerFechaUltimoMantenimiento(tarea.Id)).HasValue)
            {
                switch (tarea.Periodicidad.Value)
                {
                    case Periodicidades.Anualmente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddYears(tarea.Lapso);
                        break;
                    case Periodicidades.Mensualmente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddMonths(tarea.Lapso);
                        break;
                    case Periodicidades.Semanalmente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddDays(tarea.Lapso * 7);
                        break;
                    case Periodicidades.Diariamente:
                        while (tarea.FechaSiguiente < ultimaFecha)
                            tarea.FechaSiguiente = tarea.FechaInicio.AddDays(tarea.Lapso);
                        break;
                }

                if (tarea.FechaFin.HasValue)
                    tarea.FechaSiguiente = new DateTime(Math.Min(tarea.FechaSiguiente.Ticks, tarea.FechaFin.Value.Ticks));

                RepositorioTareasMantenimiento.Modificar(tarea);
                RepositorioTareasMantenimiento.GuardarCambios();
            }
        }

        void IServicioTareasMantenimientoInterno.FinalizarTareaMantenimiento(int idTarea, int idUsuario)
        {
            var tarea = RepositorioTareasMantenimiento.Obtener(m => m.Id == idTarea && m.Activa);

            if (tarea == null)
                return;

            //if (!tarea.Activa)
            //    throw new SOTException(Recursos.TareasMantenimiento.eliminacion_no_permitida_excepcion);

            //if (RepositorioTareasMantenimiento.Alguno(m => m.Activa && m.IdHabitacion == idHabitacion))
            //    throw new SOTException(Recursos.TareasMantenimiento.mantenimiento_en_curso_excepcion);

            //ServicioHabitaciones.ValidarEsActiva(tarea.IdHabitacion);

            //ValidarDatos(tarea);
            var fechaActual = DateTime.Now;

            DateTime? ultimaFecha = ServicioMantenimientos.ObtenerFechaUltimoMantenimiento(tarea.Id);


            //tarea.FechaSiguiente = tarea.FechaInicio;

            if (ultimaFecha.HasValue && tarea.FechaSiguiente <= ultimaFecha)
            {
                tarea.Activa = false;
                tarea.FechaModificacion = fechaActual;
                tarea.FechaEliminacion = fechaActual;
                tarea.IdUsuarioModifico = idUsuario;
                tarea.IdUsuarioElimino = idUsuario;

                RepositorioTareasMantenimiento.Modificar(tarea);
                RepositorioTareasMantenimiento.GuardarCambios();
            }
        }

        #endregion
    }
}
