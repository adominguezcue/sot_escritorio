﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.TareasMantenimiento
{
    internal interface IServicioTareasMantenimientoInterno : IServicioTareasMantenimiento
    {
        /// <summary>
        /// Retorna la tarea de mantenimiento activa con el id proporcionado
        /// </summary>
        /// <param name="idTarea"></param>
        /// <returns></returns>
        TareaMantenimiento ObtenerTareaPorId(int idTarea);
        /// <summary>
        /// Calcula la siguiente ejecución de la tarea en base a su periodicidad
        /// </summary>
        /// <param name="idTarea"></param>
        void CalcularSiguienteFecha(int idTarea);

        void FinalizarTareaMantenimiento(int idTarea, int idUsuario);
    }
}
