﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Modelo.Dtos;

namespace Negocio.TareasMantenimiento
{
    public interface IServicioTareasMantenimiento
    {
        ///// <summary>
        ///// Permite registrar una tarea de mantenimiento
        ///// </summary>
        ///// <param name="idHabitacion"></param>
        ///// <param name="descripcion"></param>
        ///// <param name="usuario"></param>
        //void CrearTarea(int idHabitacion, string descripcion, DtoUsuario usuario);
        /// <summary>
        /// Permite crear una tarea de mantenimiento preventivo y aplicarla dentro de una misma transacción
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idConceptoMantenimiento"></param>
        /// <param name="descripcion"></param>
        /// <param name="usuario"></param>
        void CrearTareaInstantanea(int idHabitacion, int idConceptoMantenimiento, string descripcion, DtoUsuario usuario);
        /// <summary>
        /// Permite registrar una tarea de mantenimiento
        /// </summary>
        /// <param name="tarea"></param>
        /// <param name="usuario"></param>
        void CrearTarea(TareaMantenimiento tarea, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar una tarea de mantenimiento
        /// </summary>
        /// <param name="tarea"></param>
        /// <param name="usuario"></param>
        void ModificarTarea(TareaMantenimiento tarea, DtoUsuario usuario);
        ///// <summary>
        ///// Permite finalizar el manteinimiento en curso de una habitacion
        ///// </summary>
        ///// <param name="idHabitacion"></param>
        ///// <param name="usuario"></param>
        //void FinalizarTarea(int idHabitacion, DtoUsuario usuario);

        ///// <summary>
        ///// Retorna las tareas de mantenimiento de una habiación desde la última hasta la primera
        ///// </summary>
        ///// <param name="idHabitacion"></param>
        ///// <param name="usuario"></param>
        ///// <param name="pagina"></param>
        ///// <param name="elementos"></param>
        ///// <returns></returns>
        //List<TareaMantenimiento> ObtenerTareasHabitacion(int idHabitacion, DtoUsuario usuario, int pagina = 0, int elementos = 0);
        ///// <summary>
        ///// Retorna la tarea de mantenimiento actual de la habitación con el id proporcionado,
        ///// siempre y cuando se encuentre en estado "Mantenimiento"
        ///// </summary>
        ///// <param name="idHabitacion"></param>
        ///// <param name="usuario"></param>
        ///// <returns></returns>
        //TareaMantenimiento ObtenerTareaActual(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Retorna la cantidad de tareas pendientes por realizar hasta la fecha actual, tomando en cuenta los recordatorios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        int ObtenerCantidadTareasPendientes(DtoUsuario usuario, int idHabitacion = 0);
        /// <summary>
        /// Retorna las tareas a realizarse el mes especificado, si se proporciona un id de
        /// habitación diferente de cero, se filtrarán las tareas por habitación
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="usuario"></param>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        List<DtoTareaMantenimientoBase> ObtenerTareasPorMes(int mes, int anio, DtoUsuario usuario, int idHabitacion = 0);
        /// <summary>
        /// Retorna la tarea de mantenimiento activa con el id proporcionado
        /// </summary>
        /// <param name="idTarea"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        TareaMantenimiento ObtenerTareaPorId(int idTarea, DtoUsuario usuario);
        /// <summary>
        /// Elimina una tarea del sistema
        /// </summary>
        /// <param name="idTarea"></param>
        /// <param name="usuario"></param>
        void EliminarTarea(int idTarea, DtoUsuario usuario);

        List<DtoResumenMantenimiento> ObtenerMantenimientosPendientes(int idHabitacion, DtoUsuario usuario, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0);
    }
}
