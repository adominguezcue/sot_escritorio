﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Incidencias
{
    public interface IServicioObjetosOlvidados
    {
        /// <summary>
        /// Permite registrar un nuevo objeto olvidado en el sistema
        /// </summary>
        /// <param name="objetoOlvidado"></param>
        /// <param name="usuario"></param>
        void RegistrarObjetosOlvidados(ObjetoOlvidado objetoOlvidado, DtoUsuario usuario);
        /// <summary>
        /// Permite obtener los objetos olvidados en el sistema, los filtros son opcionales.
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <param name="idHabitacion"></param>
        /// <param name="idEmpleado"></param>
        /// <returns></returns>
        List<DtoObjetoOlvidado> ObtenerObjetosOlvidadosFiltradas(DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idHabitacion = null, int? idEmpleado = null);
        /// <summary>
        /// Permite marcar objeto olvidado como cancelado
        /// </summary>
        /// <param name="idObjetoOlvidado"></param>
        /// <param name="credencial"></param>
        void CancelarObjetoOlvidado(int idObjetoOlvidado, DtoCredencial credencial);
        /// <summary>
        /// Permite marcar un objeto olvidado como eliminada
        /// </summary>
        /// <param name="idObjetoOlvidado"></param>
        /// <param name="credencial"></param>
        void EliminarObjetoOlvidado(int idObjetoOlvidado, DtoCredencial credencial);
        /// <summary>
        /// Retorna el objeto olvidado que posea el id proporcionado o null en caso de no existir coincidencias
        /// </summary>
        /// <param name="idObjetoOlvidado"></param>
        /// <param name="UsuarioActual"></param>
        /// <returns></returns>
        ObjetoOlvidado ObtenerObjetoOlvidado(int idObjetoOlvidado, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar el objeto olvidado
        /// </summary>
        /// <param name="objetoOlvidado"></param>
        /// <param name="huellaDigital"></param>
        void ModificarObjetoOlvidado(ObjetoOlvidado objetoOlvidado, DtoCredencial credencial);
    }
}
