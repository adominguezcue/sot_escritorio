﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Areas;
using Negocio.CortesTurno;
using Negocio.Empleados;
using Negocio.Seguridad.Permisos;
using Negocio.Seguridad.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.Incidencias
{
    public class ServicioIncidenciasTurno : IServicioIncidenciasTurnoInterno
    {
        private IRepositorioIncidenciasTurno RepositorioIncidencias
        {
            get { return _repositorioIncidencias.Value; }
        }

        private Lazy<IRepositorioIncidenciasTurno> _repositorioIncidencias = new Lazy<IRepositorioIncidenciasTurno>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioIncidenciasTurno>();
        });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });
        
        IServicioEmpleados ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleados> _servicioEmpleados = new Lazy<IServicioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>(); });

        IServicioAreasInterno ServicioAreas
        {
            get { return _servicioAreas.Value; }
        }

        Lazy<IServicioAreasInterno> _servicioAreas = new Lazy<IServicioAreasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioAreasInterno>(); });

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });


        public void RegistrarIncidencia(IncidenciaTurno incidencia, int idCorteTurno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearIncidenciaTurno = true });

            if(incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            //ServicioEmpleados.ValidarRecamareraActiva(incidencia.IdEmpleadoReporta);

            ValidarCamposObligatorios(incidencia);
            //if (incidencia.Zonas == null)
            //    incidencia.Zonas = "";

            var fechaActual = DateTime.Now;

            //if (RepositorioIncidencias.Alguno(m => m.Activa && !m.Cancelada
            //                                     && m.Nombre.Trim().ToUpper().Equals(incidencia.Nombre.Trim().ToUpper())
            //                                     && m.FechaCreacion.Year == fechaActual.Year
            //                                     && m.FechaCreacion.Month == fechaActual.Month
            //                                     && m.FechaCreacion.Day == fechaActual.Day
            //                                     && m.FechaCreacion.Hour == fechaActual.Hour))
            //    throw new SOTException(Recursos.Incidencias.incidencia_duplicada_excepcion);

            var corte = ServicioCortesTurno.ObtenerUltimoCorte();

            if (corte.Id != idCorteTurno)
                corte = ServicioCortesTurno.ObtenerCorteEnRevision(idCorteTurno);

            if (corte == null)
                throw new SOTException(Recursos.CortesTurno.corte_no_en_revision_o_actual_excepcion);

            incidencia.IdCorteTurno = corte.Id;
            incidencia.Activa = true;
            incidencia.FechaCreacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioCreo = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Agregar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }

        private void ValidarCamposObligatorios(Modelo.Entidades.IncidenciaTurno incidencia)
        {
            if (!UtilidadesRegex.LetrasONumeros(incidencia.Nombre))
                throw new SOTException(Recursos.Incidencias.nombre_invalido_excepcion);

            if (!ServicioAreas.VerificarExistencia(incidencia.IdArea))
                throw new SOTException(Recursos.Incidencias.area_invalida_excepcion);

            //if (!string.IsNullOrEmpty(incidencia.Zonas) && !UtilidadesRegex.TextoConPuntuacion(incidencia.Zonas))
            //    throw new SOTException(Recursos.Incidencias.zonas_invalidas_excepcion);

            if (!UtilidadesRegex.TextoONumerosConPuntuacion(incidencia.Descripcion))
                throw new SOTException(Recursos.Incidencias.descripcion_invalida_excepcion);
        }


        public List<DtoIncidenciaTurno> ObtenerIncidenciasFiltradas(DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idCorte = null, string area=null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarIncidenciaTurno = true });

            //int? idTurno = null;

            //if(soloTurnoActual)
            //    idTurno = ServicioCortesTurno.ObtenerUltimoCorte().Id;

            var incidencias = RepositorioIncidencias.ObtenerIncidenciasFiltradas(fechaInicial, fechaFinal, idCorte, area);

            foreach (var grupo in incidencias.GroupBy(m => m.IdUsuario)) 
            {
                var alias = ServicioEmpleados.ObtenerNombreCompletoEmpleado(grupo.Key) ?? "";

                foreach (var incidencia in grupo)
                    incidencia.NombreEmpleado = alias;
            }

            return incidencias;
        }

        public void CancelarIncidencia(int idIncidencia, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CancelarIncidenciaTurno = true });

            var incidencia = RepositorioIncidencias.Obtener(m => m.Activa && m.Id == idIncidencia);

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            if (incidencia.Cancelada)
                throw new SOTException(Recursos.Incidencias.incidencia_previamente_cancelada_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Cancelada = true;
            incidencia.FechaCancelacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioCancelo = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Modificar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }

        public void EliminarIncidencia(int idIncidencia, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarIncidenciaTurno = true });

            var incidencia = RepositorioIncidencias.Obtener(m => m.Activa && m.Id == idIncidencia);

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Activa = false;
            incidencia.FechaEliminacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioElimino = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Modificar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }

        public IncidenciaTurno ObtenerIncidencia(int idIncidencia, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarIncidenciaTurno = true });

            return RepositorioIncidencias.Obtener(m => m.Activa && m.Id == idIncidencia);
        }

        public void ModificarIncidencia(IncidenciaTurno incidencia, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarIncidenciaTurno = true });

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            ValidarActivaNoCancelada(incidencia.Id);

            var corte = ServicioCortesTurno.ObtenerUltimoCorte();

            if (corte.Id != incidencia.IdCorteTurno)
                corte = ServicioCortesTurno.ObtenerCorteEnRevision(incidencia.IdCorteTurno);

            if (corte == null)
                throw new SOTException(Recursos.CortesTurno.corte_no_en_revision_o_actual_excepcion);

            ValidarCamposObligatorios(incidencia);
            //if (incidencia.Zonas == null)
            //    incidencia.Zonas = "";

            //if (RepositorioIncidencias.Alguno(m => m.Activa && !m.Cancelada
            //                                     && m.Nombre.Trim().ToUpper().Equals(incidencia.Nombre.Trim().ToUpper())
            //                                     && m.FechaCreacion.Year == incidencia.FechaCreacion.Year
            //                                     && m.FechaCreacion.Month == incidencia.FechaCreacion.Month
            //                                     && m.FechaCreacion.Day == incidencia.FechaCreacion.Day
            //                                     && m.FechaCreacion.Hour == incidencia.FechaCreacion.Hour
            //                                     && m.Id != incidencia.Id))
            //    throw new SOTException(Recursos.Incidencias.incidencia_duplicada_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Activa = true;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Modificar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }

        public void ValidarExistenIncidenciasArea(int idCorteTurno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { RevisarCorteTurno = true });

            ((IServicioIncidenciasTurnoInterno)this).ValidarExistenIncidencias(idCorteTurno);
        }

        #region métodos internal

        void IServicioIncidenciasTurnoInterno.ValidarExistenIncidencias(int idCorteTurno)
        {
            var areas = ServicioAreas.ObtenerAreasActivas();

            StringBuilder error = new StringBuilder();

            foreach (var area in areas)
                if (!RepositorioIncidencias.Alguno(m => m.IdArea == area.Id && m.Activa && !m.Cancelada && m.IdCorteTurno == idCorteTurno))
                {
                    error.AppendLine(area.Nombre);
                    //throw new SOTException(Recursos.Incidencias.no_incidencias_turno_excepcion);
                }

            if (error.Length > 0)
            {
                error.Insert(0, "Las siguientes áreas no tienen incidencias registradas:" + System.Environment.NewLine);
                throw new SOTException(error.ToString());
            }
        }

        #endregion

        private void ValidarActivaNoCancelada(int idIncidencia)
        {
            if (!RepositorioIncidencias.Alguno(m => m.Activa && !m.Cancelada && m.Id == idIncidencia))
                throw new SOTException(Recursos.Incidencias.incidencia_no_modificable_excepcion);
        }
    }
}
