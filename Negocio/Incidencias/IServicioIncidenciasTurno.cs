﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Incidencias
{
    public interface IServicioIncidenciasTurno
    {
        /// <summary>
        /// Permite registrar una nueva incidencia en el sistema
        /// </summary>
        /// <param name="incidencia"></param>
        /// <param name="usuario"></param>
        void RegistrarIncidencia(IncidenciaTurno incidencia, int idCorteTurno, DtoUsuario usuario);
        /// <summary>
        /// Permite obtener las incidencias en el sistema, los filtros son opcionales.
        /// </summary>
        /// <param name="soloTurnoActual"></param>
        /// <param name="usuario"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<DtoIncidenciaTurno> ObtenerIncidenciasFiltradas(DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idCorte = null,string area=null);
        /// <summary>
        /// Permite marcar una incidencia como cancelada
        /// </summary>
        /// <param name="idIncidencia"></param>
        /// <param name="credencial"></param>
        void CancelarIncidencia(int idIncidencia, DtoCredencial credencial);
        /// <summary>
        /// Permite marcar una incidencia como eliminada
        /// </summary>
        /// <param name="idIncidencia"></param>
        /// <param name="credencial"></param>
        void EliminarIncidencia(int idIncidencia, DtoCredencial credencial);
        /// <summary>
        /// Retorna la incidencia que posea el id proporcionado o null en caso de no existir coincidencias
        /// </summary>
        /// <param name="idIncidencia"></param>
        /// <param name="UsuarioActual"></param>
        /// <returns></returns>
        IncidenciaTurno ObtenerIncidencia(int idIncidencia, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar la incidencia
        /// </summary>
        /// <param name="incidencia"></param>
        /// <param name="huellaDigital"></param>
        void ModificarIncidencia(IncidenciaTurno incidencia, DtoCredencial credencial);
        /// <summary>
        /// Valida que exista una incidencia de turno activa en el turno especificado para cada una de las áreas del hotel.
        /// </summary>
        /// <param name="idCorteTurno"></param>
        /// <param name="usuario"></param>
        void ValidarExistenIncidenciasArea(int idCorteTurno, DtoUsuario usuario);
    }
}
