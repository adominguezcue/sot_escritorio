﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Empleados;
using Negocio.Habitaciones;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.Incidencias
{
    public class ServicioIncidenciasHabitacion : IServicioIncidenciasHabitacion
    {
        private IRepositorioIncidenciasHabitacion RepositorioIncidencias
        {
            get { return _repositorioIncidencias.Value; }
        }

        private Lazy<IRepositorioIncidenciasHabitacion> _repositorioIncidencias = new Lazy<IRepositorioIncidenciasHabitacion>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioIncidenciasHabitacion>();
        });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioEmpleados ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleados> _servicioEmpleados = new Lazy<IServicioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>(); });

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });


        public void RegistrarIncidencia(IncidenciaHabitacion incidencia, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearIncidenciaHabitacion = true });

            if(incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            //ServicioEmpleados.ValidarRecamareraActiva(incidencia.IdEmpleadoReporta);
            ServicioHabitaciones.ValidarEsActiva(incidencia.IdHabitacion);

            ValidarCamposObligatorios(incidencia);

            var fechaActual = DateTime.Now;
            if (incidencia.Zonas == null)
                incidencia.Zonas = "";

            //if (RepositorioIncidencias.Alguno(m => m.Activa && !m.Cancelada
            //                                     && m.Nombre.Trim().ToUpper().Equals(incidencia.Nombre.Trim().ToUpper())
            //                                     && m.IdHabitacion == incidencia.IdHabitacion
            //                                     && m.IdEmpleadoReporta == m.IdEmpleadoReporta
            //                                     && m.FechaCreacion.Year == fechaActual.Year
            //                                     && m.FechaCreacion.Month == fechaActual.Month
            //                                     && m.FechaCreacion.Day == fechaActual.Day
            //                                     && m.FechaCreacion.Hour == fechaActual.Hour))
            //    throw new SOTException(Recursos.Incidencias.incidencia_duplicada_excepcion);

            incidencia.Activa = true;
            incidencia.FechaCreacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioCreo = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Agregar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }


        public List<DtoIncidenciaHabitacion> ObtenerIncidenciasFiltradas(DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idHabitacion = null, int? idEmpleado = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarIncidenciaHabitacion = true });

            return RepositorioIncidencias.ObtenerIncidenciasFiltradas(fechaInicial, fechaFinal, idHabitacion, idEmpleado);
        }

        public void CancelarIncidencia(int idIncidencia, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CancelarIncidenciaHabitacion = true });

            var incidencia = RepositorioIncidencias.Obtener(m => m.Activa && m.Id == idIncidencia);

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            if (incidencia.Cancelada)
                throw new SOTException(Recursos.Incidencias.incidencia_previamente_cancelada_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Cancelada = true;
            incidencia.FechaCancelacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioCancelo = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Modificar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }

        public void EliminarIncidencia(int idIncidencia, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarIncidenciaHabitacion = true });

            var incidencia = RepositorioIncidencias.Obtener(m => m.Activa && m.Id == idIncidencia);

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Activa = false;
            incidencia.FechaEliminacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioElimino = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Modificar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }


        public IncidenciaHabitacion ObtenerIncidencia(int idIncidencia, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarIncidenciaHabitacion = true });

            return RepositorioIncidencias.Obtener(m => m.Activa && m.Id == idIncidencia);
        }

        public void ModificarIncidencia(IncidenciaHabitacion incidencia, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarIncidenciaHabitacion = true });

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            ValidarActivaNoCancelada(incidencia.Id);

            ServicioEmpleados.ValidarRecamareraActiva(incidencia.IdEmpleadoReporta);
            ServicioHabitaciones.ValidarEsActiva(incidencia.IdHabitacion);


            ValidarCamposObligatorios(incidencia);
            if (incidencia.Zonas == null)
                incidencia.Zonas = "";

            //if (RepositorioIncidencias.Alguno(m => m.Activa && !m.Cancelada
            //                                     && m.Nombre.Trim().ToUpper().Equals(incidencia.Nombre.Trim().ToUpper())
            //                                     && m.IdHabitacion == incidencia.IdHabitacion
            //                                     && m.IdEmpleadoReporta == m.IdEmpleadoReporta
            //                                     && m.FechaCreacion.Year == incidencia.FechaCreacion.Year
            //                                     && m.FechaCreacion.Month == incidencia.FechaCreacion.Month
            //                                     && m.FechaCreacion.Day == incidencia.FechaCreacion.Day
            //                                     && m.FechaCreacion.Hour == incidencia.FechaCreacion.Hour
            //                                     && m.Id != incidencia.Id))
            //    throw new SOTException(Recursos.Incidencias.incidencia_duplicada_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Activa = true;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioIncidencias.Modificar(incidencia);
            RepositorioIncidencias.GuardarCambios();
        }

        private void ValidarActivaNoCancelada(int idIncidencia)
        {
            if (!RepositorioIncidencias.Alguno(m => m.Activa && !m.Cancelada && m.Id == idIncidencia))
                throw new SOTException(Recursos.Incidencias.incidencia_no_modificable_excepcion);
        }

        private void ValidarCamposObligatorios(Modelo.Entidades.IncidenciaHabitacion incidencia)
        {
            if (!UtilidadesRegex.LetrasONumeros(incidencia.Nombre))
                throw new SOTException(Recursos.Incidencias.nombre_invalido_excepcion);

            if (!string.IsNullOrEmpty(incidencia.Zonas) && !UtilidadesRegex.TextoConPuntuacion(incidencia.Zonas))
                throw new SOTException(Recursos.Incidencias.zonas_invalidas_excepcion);

            if (!UtilidadesRegex.TextoONumerosConPuntuacion(incidencia.Descripcion))
                throw new SOTException(Recursos.Incidencias.descripcion_invalida_excepcion);
        }
    }
}
