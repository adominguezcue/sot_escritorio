﻿using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Empleados;
using Negocio.Habitaciones;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.Incidencias
{
    public class ServicioObjetosOlvidados : IServicioObjetosOlvidados
    {
#warning meter a la config global

        static int MESES_VIGENCIA = 3;

        private IRepositorioObjetosOlvidados RepositorioObjetosOlvidados
        {
            get { return _repositorioObjetosOlvidados.Value; }
        }

        private Lazy<IRepositorioObjetosOlvidados> _repositorioObjetosOlvidados = new Lazy<IRepositorioObjetosOlvidados>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioObjetosOlvidados>();
        });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioEmpleados ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleados> _servicioEmpleados = new Lazy<IServicioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>(); });

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });

        public void RegistrarObjetosOlvidados(Modelo.Entidades.ObjetoOlvidado objetoOlvidado, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearObjetoOlvidado = true });

            if (objetoOlvidado == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            //ServicioEmpleados.ValidarRecamareraActiva(objetoOlvidado.IdEmpleadoReporta);
            ServicioHabitaciones.ValidarEsActiva(objetoOlvidado.IdHabitacion);

            ValidarCamposObligatorios(objetoOlvidado);

            var fechaActual = DateTime.Now;

            if (RepositorioObjetosOlvidados.Alguno(m => m.Activo && !m.Cancelado
                                                 && m.Nombre.Trim().ToUpper().Equals(objetoOlvidado.Nombre.Trim().ToUpper())
                                                 && m.IdHabitacion == objetoOlvidado.IdHabitacion
                                                 && m.IdEmpleadoReporta == m.IdEmpleadoReporta
                                                 && m.FechaCreacion.Year == fechaActual.Year
                                                 && m.FechaCreacion.Month == fechaActual.Month
                                                 && m.FechaCreacion.Day == fechaActual.Day
                                                 && m.FechaCreacion.Hour == fechaActual.Hour))
                throw new SOTException(Recursos.Incidencias.incidencia_duplicada_excepcion);

            objetoOlvidado.Activo = true;
            objetoOlvidado.FechaCreacion = fechaActual;
            objetoOlvidado.FechaModificacion = fechaActual;
            objetoOlvidado.IdUsuarioCreo = usuario.Id;
            objetoOlvidado.IdUsuarioModifico = usuario.Id;

            RepositorioObjetosOlvidados.Agregar(objetoOlvidado);
            RepositorioObjetosOlvidados.GuardarCambios();
        }

        public List<Modelo.Dtos.DtoObjetoOlvidado> ObtenerObjetosOlvidadosFiltradas(DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idHabitacion = null, int? idEmpleado = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarObjetoOlvidado = true });

            var objetos = RepositorioObjetosOlvidados.ObtenerObjetosOlvidadosFiltrados(fechaInicial, fechaFinal, idHabitacion, idEmpleado);

            if (objetos.Count > 0)
            {
                var fechaActual = DateTime.Now.AddMonths(-MESES_VIGENCIA);

                foreach (var objeto in objetos) 
                {
                    if (objeto.FechaCreacion <= fechaActual)
                        objeto.EsObsoleto = true;
                }
            }

            return objetos;
        }

        public void CancelarObjetoOlvidado(int idObjetoOlvidado, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CancelarObjetoOlvidado = true });

            var incidencia = RepositorioObjetosOlvidados.Obtener(m => m.Activo && m.Id == idObjetoOlvidado);

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            if (incidencia.Cancelado)
                throw new SOTException(Recursos.Incidencias.incidencia_previamente_cancelada_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Cancelado = true;
            incidencia.FechaCancelacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioCancelo = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioObjetosOlvidados.Modificar(incidencia);
            RepositorioObjetosOlvidados.GuardarCambios();
        }

        public void EliminarObjetoOlvidado(int idObjetoOlvidado, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarObjetoOlvidado = true });

            var incidencia = RepositorioObjetosOlvidados.Obtener(m => m.Activo && m.Id == idObjetoOlvidado);

            if (incidencia == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            var fechaActual = DateTime.Now;

            incidencia.Activo = false;
            incidencia.FechaEliminacion = fechaActual;
            incidencia.FechaModificacion = fechaActual;
            incidencia.IdUsuarioElimino = usuario.Id;
            incidencia.IdUsuarioModifico = usuario.Id;

            RepositorioObjetosOlvidados.Modificar(incidencia);
            RepositorioObjetosOlvidados.GuardarCambios();
        }

        public Modelo.Entidades.ObjetoOlvidado ObtenerObjetoOlvidado(int idObjetoOlvidado, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarObjetoOlvidado = true });

            return RepositorioObjetosOlvidados.Obtener(m => m.Activo && m.Id == idObjetoOlvidado);
        }

        public void ModificarObjetoOlvidado(Modelo.Entidades.ObjetoOlvidado objetoOlvidado, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarObjetoOlvidado = true });

            if (objetoOlvidado == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            var objetoOriginal = RepositorioObjetosOlvidados.Obtener(m => m.Id == objetoOlvidado.Id);

            if (objetoOriginal == null)
                throw new SOTException(Recursos.Incidencias.incidencia_nula_excepcion);

            ValidarActivaNoCancelada(objetoOriginal.Id);

            //ServicioEmpleados.ValidarRecamareraActiva(objetoOriginal.IdEmpleadoReporta);
            ServicioHabitaciones.ValidarEsActiva(objetoOriginal.IdHabitacion);


            objetoOriginal.SincronizarPrimitivas(objetoOlvidado);

            ValidarCamposObligatorios(objetoOriginal);

            if (RepositorioObjetosOlvidados.Alguno(m => m.Activo && !m.Cancelado
                                                 && m.Nombre.Trim().ToUpper().Equals(objetoOriginal.Nombre.Trim().ToUpper())
                                                 && m.IdHabitacion == objetoOriginal.IdHabitacion
                                                 && m.IdEmpleadoReporta == m.IdEmpleadoReporta
                                                 && m.FechaCreacion.Year == objetoOriginal.FechaCreacion.Year
                                                 && m.FechaCreacion.Month == objetoOriginal.FechaCreacion.Month
                                                 && m.FechaCreacion.Day == objetoOriginal.FechaCreacion.Day
                                                 && m.FechaCreacion.Hour == objetoOriginal.FechaCreacion.Hour
                                                 && m.Id != objetoOriginal.Id))
                throw new SOTException(Recursos.Incidencias.incidencia_duplicada_excepcion);

            var fechaActual = DateTime.Now;

            objetoOriginal.Activo = true;
            objetoOriginal.FechaModificacion = fechaActual;
            objetoOriginal.IdUsuarioModifico = usuario.Id;

            RepositorioObjetosOlvidados.Modificar(objetoOriginal);
            RepositorioObjetosOlvidados.GuardarCambios();
        }

        private void ValidarActivaNoCancelada(int idObjeto)
        {
            if (!RepositorioObjetosOlvidados.Alguno(m => m.Activo && !m.Cancelado && m.Id == idObjeto))
                throw new SOTException(Recursos.Incidencias.incidencia_no_modificable_excepcion);
        }

        private void ValidarCamposObligatorios(Modelo.Entidades.ObjetoOlvidado objetoOlvidado)
        {
            if (!UtilidadesRegex.SoloLetras(objetoOlvidado.Nombre))
                throw new SOTException(Recursos.Incidencias.nombre_invalido_excepcion);

            if (!UtilidadesRegex.TextoONumerosConPuntuacion(objetoOlvidado.Descripcion))
                throw new SOTException(Recursos.Incidencias.descripcion_invalida_excepcion);
        }
    }
}
