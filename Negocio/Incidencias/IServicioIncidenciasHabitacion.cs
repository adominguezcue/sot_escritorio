﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Incidencias
{
    public interface IServicioIncidenciasHabitacion
    {
        /// <summary>
        /// Permite registrar una nueva incidencia en el sistema
        /// </summary>
        /// <param name="incidencia"></param>
        /// <param name="usuario"></param>
        void RegistrarIncidencia(IncidenciaHabitacion incidencia, DtoUsuario usuario);
        /// <summary>
        /// Permite obtener las incidencias en el sistema, los filtros son opcionales.
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <param name="idHabitacion"></param>
        /// <param name="idEmpleado"></param>
        /// <returns></returns>
        List<DtoIncidenciaHabitacion> ObtenerIncidenciasFiltradas(DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idHabitacion = null, int? idEmpleado = null);
        /// <summary>
        /// Permite marcar una incidencia como cancelada
        /// </summary>
        /// <param name="idIncidencia"></param>
        /// <param name="credencial"></param>
        void CancelarIncidencia(int idIncidencia, DtoCredencial credencial);
        /// <summary>
        /// Permite marcar una incidencia como eliminada
        /// </summary>
        /// <param name="idIncidencia"></param>
        /// <param name="credencial"></param>
        void EliminarIncidencia(int idIncidencia, DtoCredencial credencial);
        /// <summary>
        /// Retorna la incidencia que posea el id proporcionado o null en caso de no existir coincidencias
        /// </summary>
        /// <param name="idIncidencia"></param>
        /// <param name="UsuarioActual"></param>
        /// <returns></returns>
        IncidenciaHabitacion ObtenerIncidencia(int idIncidencia, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar la incidencia
        /// </summary>
        /// <param name="incidencia"></param>
        /// <param name="huellaDigital"></param>
        void ModificarIncidencia(IncidenciaHabitacion incidencia, DtoCredencial credencial);
    }
}
