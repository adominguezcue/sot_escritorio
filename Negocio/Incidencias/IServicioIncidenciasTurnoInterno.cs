﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Incidencias
{
    internal interface IServicioIncidenciasTurnoInterno : IServicioIncidenciasTurno
    {
        /// <summary>
        /// Valida que existan registradas incidencias en el turno actual
        /// </summary>
        /// <param name="idTurno"></param>
        void ValidarExistenIncidencias(int idTurno);
    }
}
