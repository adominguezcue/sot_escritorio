﻿using Modelo.Entidades;

namespace Negocio.Preventas
{
    internal interface IServicioPreventasInterno : IServicioPreventas
    {
        int GrenerarPreventa(Venta.ClasificacionesVenta clasificacion);
        //Preventa GrenerarPreventaVolatil(Venta.ClasificacionesVenta clasificacionVenta);
        /// <summary>
        /// Retorna la preventa que coincide con el id proporcionado sin importar si está activa o no
        /// </summary>
        /// <param name="idPreventa"></param>
        /// <returns></returns>
        Preventa ObtenerPreventaPorId(int idPreventa);
    }
}