﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.Preventas
{
    public interface IServicioPreventas
    {
        int GrenerarPreventaExt(Venta.ClasificacionesVenta clasificacion);
    }
}
