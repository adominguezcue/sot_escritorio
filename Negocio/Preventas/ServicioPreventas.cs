﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Modelo.Entidades;
using Modelo.Repositorios;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Negocio.ServiciosLocales.Agentes.Preventas;

namespace Negocio.Preventas
{
    public class ServicioPreventas : IServicioPreventasInterno
    {
        static object bloqueadorObj = new object();

        //IRepositorioFoliosClasificacionVenta
        IRepositorioFoliosClasificacionVenta RepositorioFoliosClasificacionVenta
        {
            get { return _repositorioFoliosClasificacionVenta.Value; }
        }

        Lazy<IRepositorioFoliosClasificacionVenta> _repositorioFoliosClasificacionVenta = new Lazy<IRepositorioFoliosClasificacionVenta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioFoliosClasificacionVenta>(); });


        IRepositorioBloqueadoresFolios RepositorioBloqueadoresFolios
        {
            get { return _repositorioBloqueadoresFolios.Value; }
        }

        Lazy<IRepositorioBloqueadoresFolios> _repositorioBloqueadoresFolios = new Lazy<IRepositorioBloqueadoresFolios>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioBloqueadoresFolios>(); });

        IRepositorioPreventas RepositorioPreventas
        {
            get { return _repositorioPreventas.Value; }
        }

        Lazy<IRepositorioPreventas> _repositorioPreventas = new Lazy<IRepositorioPreventas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPreventas>(); });

        public int GrenerarPreventaExt(Venta.ClasificacionesVenta clasificacion)
        {
            lock (bloqueadorObj)
            {
                if (Transaction.Current == null)
                    throw new SOTException("La generación de preventa debe realizarse dentro de una transacción de base de datos");

                var nuevoGuid = Guid.NewGuid();

                var bloqueo = new BloqueadorFolios
                {
                    IdClasificacionVenta = (int)clasificacion,
                    UUID = nuevoGuid,
                    Fecha = DateTime.Now
                };

                RepositorioBloqueadoresFolios.Agregar(bloqueo);

                //intento bloquear 
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        RepositorioBloqueadoresFolios.GuardarCambios();
                        break;
                    }
                    catch (DbUpdateException dbUpdateEx)
                    {
                        if (dbUpdateEx.InnerException != null
                                && dbUpdateEx.InnerException.InnerException != null)
                        {
                            if (dbUpdateEx.InnerException.InnerException is SqlException)
                            {
                                var sqlException = dbUpdateEx.InnerException.InnerException as SqlException;

                                switch (sqlException.Number)
                                {
                                    case 2627:  // Unique constraint error
                                    case 547:   // Constraint check violation
                                    case 2601:  // Duplicated key row error
                                        // Constraint violation exception
                                        // A custom exception of yours for concurrency issues
                                        Thread.Sleep(3000);
                                        break;
                                    default:
                                        // A custom exception of yours for other DB issues
                                        throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
                                }
                            }

                            throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
                        }
                        else
                            throw dbUpdateEx;
                    }
                }

                var idClasificacionVenta = (int)clasificacion;

                var ultimoFolio = RepositorioFoliosClasificacionVenta.Obtener(m => m.IdClasificacionVenta == idClasificacionVenta);

                if (ultimoFolio == null)
                    throw new SOTException("No se pudo encontrar el último folio para el tipo de ventas " + clasificacion.Descripcion() + ", póngase en contacto con el administrador de sistemas");

                var nuevaPreventa = new Preventa
                {
                    Activa = true,
                    ClasificacionVenta = clasificacion,
                    FolioTicket = ++ultimoFolio.FolioActual,
                    SerieTicket = ultimoFolio.Serie
                };

                try
                {
                    RepositorioPreventas.Agregar(nuevaPreventa);
                    RepositorioPreventas.GuardarCambios();

                    RepositorioFoliosClasificacionVenta.Modificar(ultimoFolio);
                    RepositorioFoliosClasificacionVenta.GuardarCambios();
                }
                finally
                {
                    var bloqueador = RepositorioBloqueadoresFolios.Obtener(m => m.UUID == nuevoGuid && m.IdClasificacionVenta == idClasificacionVenta);

                    if (bloqueador != null)
                    {
                        RepositorioBloqueadoresFolios.Eliminar(bloqueador);
                        for (int i = 0; i < 5; i++)
                        {
                            try
                            {
                                RepositorioBloqueadoresFolios.GuardarCambios();
                                break;
                            }
                            catch (DbUpdateException dbUpdateEx)
                            {
                                if (dbUpdateEx.InnerException != null
                                        && dbUpdateEx.InnerException.InnerException != null)
                                {
                                    if (dbUpdateEx.InnerException.InnerException is SqlException)
                                    {
                                        var sqlException = dbUpdateEx.InnerException.InnerException as SqlException;

                                        switch (sqlException.Number)
                                        {
                                            case 2627:  // Unique constraint error
                                            case 547:   // Constraint check violation
                                            case 2601:  // Duplicated key row error
                                                // Constraint violation exception
                                                // A custom exception of yours for concurrency issues
                                                Thread.Sleep(3000);
                                                break;
                                            default:
                                                // A custom exception of yours for other DB issues
                                                throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
                                        }
                                    }

                                    throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
                                }
                                else
                                    throw dbUpdateEx;
                            }
                        }
                    }
                }

                Transversal.Log.Logger.Info("Generada la preventa con el folio " + nuevaPreventa.Ticket);

                return nuevaPreventa.Id;
            }
        }

        //Preventa IServicioPreventasInterno.GrenerarPreventaVolatil(Venta.ClasificacionesVenta clasificacionVenta)
        //{
        //    if (Transaction.Current == null)
        //        throw new SOTException("El incremento de folio debe realizarse dentro de una transacción de base de datos");

        //    var nuevoGuid = Guid.NewGuid();

        //    var bloqueo = new BloqueadorFolios
        //    {
        //        IdClasificacionVenta = (int)clasificacionVenta,
        //        UUID = nuevoGuid,
        //        Fecha = DateTime.Now
        //    };
        //    RepositorioBloqueadoresFolios.Agregar(bloqueo);

        //    //intento bloquear 
        //    for (int i = 0; i < 5; i++)
        //    {
        //        try
        //        {
        //            RepositorioBloqueadoresFolios.GuardarCambios();
        //        }
        //        catch (DbUpdateException dbUpdateEx)
        //        {
        //            if (dbUpdateEx.InnerException != null
        //                    && dbUpdateEx.InnerException.InnerException != null)
        //            {
        //                if (dbUpdateEx.InnerException.InnerException is SqlException)
        //                {

        //                    var sqlException = dbUpdateEx.InnerException.InnerException as SqlException;

        //                    switch (sqlException.Number)
        //                    {
        //                        case 2627:  // Unique constraint error
        //                        case 547:   // Constraint check violation
        //                        case 2601:  // Duplicated key row error
        //                                    // Constraint violation exception
        //                                    // A custom exception of yours for concurrency issues
        //                            Thread.Sleep(3000);
        //                            break;
        //                        default:
        //                            // A custom exception of yours for other DB issues
        //                            throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
        //                    }
        //                }

        //                throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
        //            }
        //            else
        //                throw dbUpdateEx;
        //        }
        //    }

        //    var idClasificacionVenta = (int)clasificacionVenta;

        //    var ultimoFolio = RepositorioFoliosClasificacionVenta.Obtener(m => m.IdClasificacionVenta == idClasificacionVenta);

        //    if (ultimoFolio == null)
        //        throw new SOTException("No se pudo encontrar el último folio para el tipo de ventas " + clasificacionVenta.Descripcion() + ", póngase en contacto con el administrador de sistemas");

        //    var nuevaPreventa = new Preventa
        //    {
        //        Activa = true,
        //        ClasificacionVenta = clasificacionVenta,
        //        FolioTicket = ++ultimoFolio.FolioActual,
        //        SerieTicket = ultimoFolio.Serie
        //    };

        //    try
        //    {
        //        //RepositorioPreventas.Agregar(nuevaPreventa);
        //        //RepositorioPreventas.GuardarCambios();

        //        RepositorioFoliosClasificacionVenta.Modificar(ultimoFolio);
        //        RepositorioFoliosClasificacionVenta.GuardarCambios();
        //    }
        //    finally
        //    {
        //        var bloqueador = RepositorioBloqueadoresFolios.Obtener(m => m.UUID == nuevoGuid && m.IdClasificacionVenta == idClasificacionVenta);

        //        if (bloqueador != null)
        //        {
        //            RepositorioBloqueadoresFolios.Eliminar(bloqueador);
        //            for (int i = 0; i < 5; i++)
        //            {
        //                try
        //                {
        //                    RepositorioBloqueadoresFolios.GuardarCambios();
        //                }
        //                catch (DbUpdateException dbUpdateEx)
        //                {
        //                    if (dbUpdateEx.InnerException != null
        //                            && dbUpdateEx.InnerException.InnerException != null)
        //                    {
        //                        if (dbUpdateEx.InnerException.InnerException is SqlException)
        //                        {
        //                            var sqlException = dbUpdateEx.InnerException.InnerException as SqlException;

        //                            switch (sqlException.Number)
        //                            {
        //                                case 2627:  // Unique constraint error
        //                                case 547:   // Constraint check violation
        //                                case 2601:  // Duplicated key row error
        //                                            // Constraint violation exception
        //                                            // A custom exception of yours for concurrency issues
        //                                    Thread.Sleep(3000);
        //                                    break;
        //                                default:
        //                                    // A custom exception of yours for other DB issues
        //                                    throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
        //                            }
        //                        }

        //                        throw new SOTException(dbUpdateEx.Message, dbUpdateEx.InnerException);
        //                    }
        //                    else
        //                        throw dbUpdateEx;
        //                }
        //            }
        //        }
        //    }
        //    return nuevaPreventa;
        //}

        #region métodos internal

        Preventa IServicioPreventasInterno.ObtenerPreventaPorId(int idPreventa)
        {
            return RepositorioPreventas.Obtener(m => /*m.Activa && */m.Id == idPreventa);
        }

        int IServicioPreventasInterno.GrenerarPreventa(Venta.ClasificacionesVenta clasificacionVentas) 
        {
            var agente = new AgenteLsPreventas();
            return agente.SubirInformacionAsync((ServiciosLocales.lsPreventas.VentaClasificacionesVenta)clasificacionVentas);
        }

        #endregion
    }
}
