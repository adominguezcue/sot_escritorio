﻿using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System.Collections.Generic;

namespace Negocio.Fajillas
{
    public interface IServicioFajillas
    {
        /// <summary>
        /// Retorna la configuración de fajillas activa, sin información de los montos soportados
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        ConfiguracionFajilla ObtenerConfiguracionFajillas(DtoUsuario usuario);
        /// <summary>
        /// Retorna la configuración de fajillas activa, con información de los montos soportados
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        ConfiguracionFajilla ObtenerConfiguracionFajillasCargadaFILTRO();
        /// <summary>
        /// Permite agregar una nueva fajilla al sistema
        /// </summary>
        /// <param name="fajillaNueva"></param>
        /// <param name="usuario"></param>
        void CrearFajilla(Fajilla fajillaNueva, DtoUsuario usuario);
        /// <summary>
        /// Permite realizar una combinación de fajillas sin persistencia en la base de datos
        /// </summary>
        /// <param name="configuracion"></param>
        /// <param name="fajillas"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Fajilla ObtenerCombinacionFajillas(ConfiguracionFajilla configuracion, IEnumerable<Fajilla> fajillas);
        /// <summary>
        /// Permite actualizar el valor configurado para las plantillas
        /// </summary>
        /// <param name="valor"></param>
        /// <param name="usuario"></param>
        void ActualizarMontoConfiguracion(decimal valor, DtoUsuario usuario);
        /// <summary>
        /// Retorna las fajillas actuales que no han sido asignadas a un corte de turno
        /// </summary>
        /// <param name="Usuario"></param>
        int ObtenerCantidadFajillasPendientesPorTurno(int idCorte, DtoUsuario Usuario);
        void GuardarConfiguracion(DtoConfiguracionFajilla configuracion, DtoUsuario usuario);

        /// <summary>
        /// Retorna las fajillas que aun no han sido vinculadas con un corte de turno
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="soloAutorizadas"></param>
        /// <returns></returns>
        List<Fajilla> ObtenerFajillasPorTurno(int idCorte, bool soloAutorizadas, DtoUsuario usuario);
        /// <summary>
        /// Retorna fajillas aprobadas que corresponden a turno solicitado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="idCorte"></param>
        /// <returns></returns>
        List<Fajilla> ObtenerFajillasAutorizadasConMontosPorCorte(DtoUsuario usuario, int idCorte);
        ///// <summary>
        ///// Asigna el corte de turno a las fajillas autorizadas
        ///// </summary>
        ///// <param name="usuario"></param>
        ///// <param name="idCorte"></param>
        //void AsignarCorteAFajillasAutorizadas(DtoUsuario usuario, int idCorte);
        /// <summary>
        /// Permite autorizar la fajilla seleccionada
        /// </summary>
        /// <param name="idFajilla"></param>
        /// <param name="huella"></param>
        void AutorizarFajilla(int idFajilla, DtoCredencial credencial);
        /// <summary>
        /// Permite guardar los montos del sobrante en caja utilizando la misma estructura de una fajilla
        /// </summary>
        /// <param name="idCorte"></param>
        /// <param name="sobranteCaja"></param>
        /// <param name="usuario"></param>
        void GuardarSobranteCaja(int idCorte, Fajilla sobranteCaja, DtoUsuario usuario);
        /// <summary>
        /// Retorna el sobrante de caja
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Fajilla ObtenerSobranteCajaTurnoRevision(int idCorteTurno, DtoUsuario usuario);
        DtoCabeceraFajilla ObtenerDesgloseEfectivoPorId(int idFajilla);
    }
}