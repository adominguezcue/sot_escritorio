﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Fajillas
{
    internal interface IServicioFajillasInterno : IServicioFajillas
    {

        void ValidarNoExistenFajillasPendientes();
        List<DtoMoneda> ObtenerDesgloseEfectivo(int idCorteTurno);

        int ObtenerCantidadFajillasAutorizadas(int idCorteTurno);
    }
}
