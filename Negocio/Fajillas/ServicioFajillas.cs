﻿using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.CortesTurno;
using Negocio.Empleados;
using Negocio.Puestos;
using Negocio.Seguridad.Permisos;
using Negocio.Seguridad.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Fajillas
{
    public class ServicioFajillas : IServicioFajillasInterno
    {
        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });

        IServicioCortesTurnoInterno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurnoInterno> _servicioCortesTurno = new Lazy<IServicioCortesTurnoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurnoInterno>(); });


        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IRepositorioConfiguracionesFajillas RepositorioConfiguracionesFajillas
        {
            get { return _repositorioConfiguracionesFajillas.Value; }
        }

        Lazy<IRepositorioConfiguracionesFajillas> _repositorioConfiguracionesFajillas = new Lazy<IRepositorioConfiguracionesFajillas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesFajillas>(); });

        IRepositorioFajillas RepositorioFajillas
        {
            get { return _repositorioFajillas.Value; }
        }

        Lazy<IRepositorioFajillas> _repositorioFajillas = new Lazy<IRepositorioFajillas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioFajillas>(); });

        public ConfiguracionFajilla ObtenerConfiguracionFajillas(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarConfiguracionFajillas = true });

            return RepositorioConfiguracionesFajillas.Obtener(m => m.Activa);
        }

        public ConfiguracionFajilla ObtenerConfiguracionFajillasCargadaFILTRO()
        {
            //ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarConfiguracionFajillas = true });

            return RepositorioConfiguracionesFajillas.ObtenerConfiguracionFajillasCargada();
        }

        public void CrearFajilla(Fajilla fajillaNueva, DtoUsuario usuario)
        {
            if (fajillaNueva == null)
                throw new SOTException(Recursos.Fajillas.fajilla_nula_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearFajillas = true });

            var corte = ServicioCortesTurno.ObtenerUltimoCorte();

            var configuracion = ObtenerConfiguracionFajillasCargadaFILTRO();

            if(configuracion == null)
                throw new SOTException(Recursos.Fajillas.configuracion_fajillas_nula_excepcion);

            if (fajillaNueva.MontosFajilla.Count(m => m.Activa) != fajillaNueva.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().Count())
                throw new SOTException(Recursos.Fajillas.montos_duplicados_excepcion);

            if (!fajillaNueva.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().OrderBy(m => m)
                .SequenceEqual(configuracion.MontosConfiguracionFajilla.Where(m => m.Activa).Select(m => m.Id).Distinct().OrderBy(m => m)))
                throw new SOTException(Recursos.Fajillas.montos_fajilla_diferentes_configuracion_excepcion);

            decimal valor = 0;

            foreach (var par in (from monto in fajillaNueva.MontosFajilla
                                 join configuracionMonto in configuracion.MontosConfiguracionFajilla on monto.IdMontoConfiguracionFajilla equals configuracionMonto.Id
                                 where monto.Activa
                                 && configuracion.Activa
                                 select new
                                 {
                                     monto,
                                     configuracionMonto
                                 }))
            {
                if (par.monto.Cantidad < 0)
                    throw new SOTException(Recursos.Fajillas.cantidad_monto_invalida_excepcion, par.configuracionMonto.Monto.ToString("C"));

                if (par.configuracionMonto.IdMonedaExtranjera.HasValue)
                    valor += par.monto.Cantidad * par.configuracionMonto.Monto * par.configuracionMonto.MonedaExtranjera.ValorCambio;
                else
                    valor += par.monto.Cantidad * par.configuracionMonto.Monto;
            }

            if (configuracion.Valor != valor)
                throw new SOTException(Recursos.Fajillas.valor_fajilla_invalido_excepcion, valor.ToString("C"), configuracion.Valor.ToString("C"));

            fajillaNueva.Valor = valor;

            var fechaActual = DateTime.Now;

            fajillaNueva.Activa = true;
            fajillaNueva.FechaCreacion = fechaActual;
            fajillaNueva.FechaModificacion = fechaActual;
            fajillaNueva.IdUsuarioCreo = usuario.Id;
            fajillaNueva.IdUsuarioModifico = usuario.Id;
            fajillaNueva.Numero = ObtenerSiguienteNumeroFajilla(corte.Id);
            fajillaNueva.IdCorteTurno = corte.Id;

            fajillaNueva.ConfiguracionFajilla = null;
            fajillaNueva.IdConfiguracionFajilla = configuracion.Id;

            foreach (var monto in fajillaNueva.MontosFajilla) 
            {
                var idConfigM = monto.IdMontoConfiguracionFajilla;
                monto.MontoConfiguracionFajilla = null;
                monto.IdMontoConfiguracionFajilla = idConfigM;
            }

            //fajillaNueva.Estado = Fajilla.Estados.Creada;

            RepositorioFajillas.Agregar(fajillaNueva);
            RepositorioFajillas.GuardarCambios();
        }

        public Fajilla ObtenerCombinacionFajillas(ConfiguracionFajilla configuracion, IEnumerable<Fajilla> fajillas)
        {
            //var configuracion = ObtenerConfiguracionFajillasCargada(usuario);

            if (configuracion == null)
                throw new SOTException(Recursos.Fajillas.configuracion_fajillas_nula_excepcion);

            var nuevaFajilla = new Fajilla { Activa = true };

            foreach (var montoC in configuracion.MontosConfiguracionFajilla.Where(m => m.Activa))
            {
                nuevaFajilla.MontosFajilla.Add(new MontoFajilla
                {
                    Activa = true,
                    Cantidad = 0,
                    IdMontoConfiguracionFajilla = montoC.Id
                });
            }

            foreach (var fajilla in fajillas)
            {
                if (fajilla.MontosFajilla.Count(m => m.Activa) != fajilla.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().Count())
                    throw new SOTException(Recursos.Fajillas.montos_duplicados_excepcion);

                var secuenciaMontos = fajilla.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().OrderBy(m => m).ToList();
                var secuenciaConfiguracion = configuracion.MontosConfiguracionFajilla.Where(m => m.Activa).Select(m => m.Id).Distinct().OrderBy(m => m).ToList();

                if (!secuenciaMontos.SequenceEqual(secuenciaConfiguracion))
                    throw new SOTException(Recursos.Fajillas.montos_fajilla_diferentes_configuracion_excepcion);

                decimal valor = 0;

                foreach (var par in (from monto in fajilla.MontosFajilla
                                     join configuracionMonto in configuracion.MontosConfiguracionFajilla on monto.IdMontoConfiguracionFajilla equals configuracionMonto.Id
                                     where monto.Activa
                                     && configuracion.Activa
                                     select new
                                     {
                                         monto,
                                         configuracionMonto
                                     }))
                {
                    if (par.monto.Cantidad < 0)
                        throw new SOTException(Recursos.Fajillas.cantidad_monto_invalida_excepcion, par.configuracionMonto.Monto.ToString("C"));

                    if (par.configuracionMonto.IdMonedaExtranjera.HasValue)
                        valor += par.monto.Cantidad * par.configuracionMonto.Monto * par.configuracionMonto.MonedaExtranjera.ValorCambio;
                    else
                        valor += par.monto.Cantidad * par.configuracionMonto.Monto;

                    var montoN = nuevaFajilla.MontosFajilla.FirstOrDefault(m => m.IdMontoConfiguracionFajilla == par.configuracionMonto.Id);

                    if (montoN != null)
                    {
                        montoN.Cantidad += par.monto.Cantidad;
                        montoN.MontoTmp = par.configuracionMonto.Monto;
                        if (par.configuracionMonto.IdMonedaExtranjera.HasValue)
                        {
                            montoN.MonedaExtranjeraTmp = par.configuracionMonto.MonedaExtranjera.Abreviatura;
                            montoN.ValorCambioTmp = par.configuracionMonto.MonedaExtranjera.ValorCambio;
                        }
                    }
                }

                if (fajilla.Valor != valor)
                    throw new SOTException(Recursos.Fajillas.valor_fajilla_invalido_excepcion, fajilla.Valor.ToString("C"), valor.ToString("C"));

                nuevaFajilla.Valor += fajilla.Valor;
            }

            return nuevaFajilla;
        }

        private int ObtenerSiguienteNumeroFajilla(int idCorte)
        {
            return RepositorioFajillas.ObtenerUltimoNumeroFajilla(idCorte) + 1;
        }

        public void ActualizarMontoConfiguracion(decimal valor, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarConfiguracionFajillas = true });

            var configuracion = ObtenerConfiguracionFajillas(usuario);

            if (configuracion == null)
                throw new SOTException(Recursos.Fajillas.configuracion_fajillas_nula_excepcion);

            configuracion.Valor = valor;
            configuracion.FechaModificacion = DateTime.Now;
            configuracion.IdUsuarioModifico = usuario.Id;

            RepositorioConfiguracionesFajillas.Modificar(configuracion);
            RepositorioConfiguracionesFajillas.GuardarCambios();
        }


        public int ObtenerCantidadFajillasPendientesPorTurno(int idCorte, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarFajillas = true });

            return RepositorioFajillas.Contador(m => m.Activa && !m.Autorizada && m.IdCorteTurno == idCorte && !m.EsSobrante);
        }

        public List<Fajilla> ObtenerFajillasPorTurno(int idCorte, bool soloAutorizadas, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarFajillas = true });

            var fajillas = RepositorioFajillas.ObtenerFajillasPorTurno(idCorte, soloAutorizadas);

            foreach (var grupoF in fajillas.Where(m => m.Autorizada).GroupBy(m => m.IdUsuarioAutorizo))
            {
                var aliasUsuario = ServicioEmpleados.ObtenerNombreCompletoEmpleado(grupoF.Key.Value);

                foreach (var fajilla in grupoF) 
                {
                    fajilla.UsuarioAutorizo = aliasUsuario;
                }
            }

            return fajillas;
        }


        public List<Fajilla> ObtenerFajillasAutorizadasConMontosPorCorte(DtoUsuario usuario, int idCorte)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarFajillas = true });

            return RepositorioFajillas.ObtenerFajillasAutorizadasConMontosPorCorte(idCorte);

            //if (valores.Count == 0)
            //    return new DtoResumenConcepto { Concepto = "Fajillas" };

            //return new DtoResumenConcepto
            //{
            //    Concepto = "Fajillas",
            //    Cantidad = valores.Count,
            //    Total = valores.Sum(m => m)
            //};
        }


        //public void AsignarCorteAFajillasAutorizadas(DtoUsuario usuario, int idCorte)
        //{
        //    var fajillas = ObtenerFajillasPorTurno(usuario, true);

        //    if (fajillas.Count == 0)
        //        return;

        //    foreach (var fajilla in fajillas)
        //    {
        //        fajilla.IdCorteTurno = idCorte;
        //        RepositorioFajillas.Modificar(fajilla);
        //    }

        //    RepositorioFajillas.GuardarCambios();
        //}


        public void AutorizarFajilla(int idFajilla, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AutorizarFajillas = true });

            var fajilla = RepositorioFajillas.Obtener(m => m.Activa && !m.Autorizada && !m.EsSobrante && m.Id == idFajilla);

            if (fajilla == null)
                throw new SOTException(Recursos.Fajillas.fajilla_nula_excepcion);

            var fechaActual = DateTime.Now;

            fajilla.Autorizada = true;
            fajilla.FechaAutorizacion = fechaActual;
            fajilla.IdUsuarioAutorizo = usuario.Id;

            RepositorioFajillas.Modificar(fajilla);
            RepositorioFajillas.GuardarCambios();
        }


        public void GuardarSobranteCaja(int idCorte, Fajilla sobranteCaja, DtoUsuario usuario)
        {
            if (sobranteCaja == null)
                throw new SOTException(Recursos.Fajillas.fajilla_nula_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { RevisarCorteTurno = true });

            var corte = ServicioCortesTurno.ObtenerCorteEnRevision(idCorte);

            if (corte == null)
                throw new SOTException(Recursos.CortesTurno.corte_seleccionado_no_en_revision_exception);

            var configuracion = ObtenerConfiguracionFajillasCargadaFILTRO();

            if (configuracion == null)
                throw new SOTException(Recursos.Fajillas.configuracion_fajillas_nula_excepcion);

            if (sobranteCaja.MontosFajilla.Count(m => m.Activa) != sobranteCaja.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().Count())
                throw new SOTException(Recursos.Fajillas.montos_duplicados_excepcion);

            if (!sobranteCaja.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().OrderBy(m => m)
                .SequenceEqual(configuracion.MontosConfiguracionFajilla.Where(m => m.Activa).Select(m => m.Id).Distinct().OrderBy(m => m)))
                throw new SOTException(Recursos.Fajillas.montos_fajilla_diferentes_configuracion_excepcion);

            decimal valor = 0;

            foreach (var par in (from monto in sobranteCaja.MontosFajilla
                                 join configuracionMonto in configuracion.MontosConfiguracionFajilla on monto.IdMontoConfiguracionFajilla equals configuracionMonto.Id
                                 where monto.Activa
                                 && configuracion.Activa
                                 select new
                                 {
                                     monto,
                                     configuracionMonto
                                 }))
            {
                if (par.monto.Cantidad < 0)
                    throw new SOTException(Recursos.Fajillas.cantidad_monto_invalida_excepcion, par.configuracionMonto.Monto.ToString("C"));

                if (par.configuracionMonto.IdMonedaExtranjera.HasValue)
                    valor += par.monto.Cantidad * par.configuracionMonto.Monto * par.configuracionMonto.MonedaExtranjera.ValorCambio;
                else
                    valor += par.monto.Cantidad * par.configuracionMonto.Monto;
            }

            if (sobranteCaja.Valor != valor)
                throw new SOTException(Recursos.Fajillas.valor_fajilla_invalido_excepcion, valor.ToString("C"), sobranteCaja.Valor.ToString("C"));

            var fechaActual = DateTime.Now;

            if (sobranteCaja.Id == 0) 
            {
                sobranteCaja.FechaCreacion = fechaActual;
                sobranteCaja.IdUsuarioCreo = usuario.Id;
            }

            sobranteCaja.Activa = true;
            sobranteCaja.EsSobrante = true;
            sobranteCaja.FechaModificacion = fechaActual;
            sobranteCaja.IdUsuarioModifico = usuario.Id;
            //sobranteCaja.Numero = ObtenerSiguienteNumeroFajilla(corte.Id);
            sobranteCaja.IdCorteTurno = corte.Id;

            sobranteCaja.ConfiguracionFajilla = null;
            sobranteCaja.IdConfiguracionFajilla = configuracion.Id;

            foreach (var monto in sobranteCaja.MontosFajilla)
            {
                var idConfigM = monto.IdMontoConfiguracionFajilla;
                monto.MontoConfiguracionFajilla = null;
                monto.IdMontoConfiguracionFajilla = idConfigM;

                if (sobranteCaja.Id != 0)
                    monto.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;
            }

            //fajillaNueva.Estado = Fajilla.Estados.Creada;
            if (sobranteCaja.Id != 0)
                RepositorioFajillas.Modificar(sobranteCaja);
            else
                RepositorioFajillas.Agregar(sobranteCaja);
            RepositorioFajillas.GuardarCambios();
        }

        public Fajilla ObtenerSobranteCajaTurnoRevision(int idCorteTurno, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDatosCorte = true });

            var corte = ServicioCortesTurno.ObtenerCorteEnRevision(idCorteTurno);

            if (corte == null)
                return null;

            return RepositorioFajillas.ObtenerSobrantePorTurno(corte.Id);
        }

        public DtoCabeceraFajilla ObtenerDesgloseEfectivoPorId(int idFajilla)
        {
            var fajilla = RepositorioFajillas.ObtenerFajillaCargada(idFajilla);

            if (fajilla == null)
                throw new SOTException(Recursos.Fajillas.fajilla_nula_excepcion);

            var configuracion = RepositorioConfiguracionesFajillas.ObtenerConfiguracionFajillasCargada();

            if (configuracion == null)
                throw new SOTException(Recursos.Fajillas.configuracion_fajillas_nula_excepcion);

            var montos = new Dictionary<int, DtoMoneda>();

            foreach (var montoC in configuracion.MontosConfiguracionFajilla.Where(m => m.Activa))
            {
                var montoN = new DtoMoneda();
                montos.Add(montoC.Id, montoN);

                montoN.Monto = montoC.Monto;
                if (montoC.IdMonedaExtranjera.HasValue)
                {
                    montoN.Nombre = montoC.MonedaExtranjera.Nombre;
                    montoN.ValorCambio = montoC.MonedaExtranjera.ValorCambio;
                }
                else
                {
                    montoN.Nombre = "Peso";
                    montoN.ValorCambio = 1;
                }
            }


            if (fajilla.MontosFajilla.Count(m => m.Activa) != fajilla.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().Count())
                throw new SOTException(Recursos.Fajillas.montos_duplicados_excepcion);

            var secuenciaMontos = fajilla.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().OrderBy(m => m).ToList();
            var secuenciaConfiguracion = configuracion.MontosConfiguracionFajilla.Where(m => m.Activa).Select(m => m.Id).Distinct().OrderBy(m => m).ToList();

            if (!secuenciaMontos.SequenceEqual(secuenciaConfiguracion))
                throw new SOTException(Recursos.Fajillas.montos_fajilla_diferentes_configuracion_excepcion);

            decimal valor = 0;

            foreach (var par in (from monto in fajilla.MontosFajilla
                                 join configuracionMonto in configuracion.MontosConfiguracionFajilla on monto.IdMontoConfiguracionFajilla equals configuracionMonto.Id
                                 where monto.Activa
                                 && configuracion.Activa
                                 select new
                                 {
                                     monto,
                                     configuracionMonto
                                 }))
            {
                if (par.monto.Cantidad < 0)
                    throw new SOTException(Recursos.Fajillas.cantidad_monto_invalida_excepcion, par.configuracionMonto.Monto.ToString("C"));

                if (par.configuracionMonto.IdMonedaExtranjera.HasValue)
                    valor += par.monto.Cantidad * par.configuracionMonto.Monto * par.configuracionMonto.MonedaExtranjera.ValorCambio;
                else
                    valor += par.monto.Cantidad * par.configuracionMonto.Monto;

                var montoN = montos[par.configuracionMonto.Id];

                montoN.Cantidad += par.monto.Cantidad;
            }

            if (fajilla.Valor != valor)
                throw new SOTException(Recursos.Fajillas.valor_fajilla_invalido_excepcion, fajilla.Valor.ToString("C"), valor.ToString("C"));

            var resumenEmpleado = ServicioEmpleados.ObtenerResumenEmpleadoPuestoPorId(fajilla.IdUsuarioCreo);

            return new DtoCabeceraFajilla
            {
                FechaAutorizacion = fajilla.FechaAutorizacion ?? fajilla.FechaCreacion,
                FechaCreacion = fajilla.FechaCreacion,
                FolioCorte = ServicioCortesTurno.ObtenerFolioCortePorId(fajilla.IdCorteTurno),
                NombreCrea = resumenEmpleado?.NombreEmpleado ?? "",
                NombreRecibe = "",
                PuestoCrea = resumenEmpleado?.Puesto ?? "",
                PuestoRecibe = "",
                Turno = ServicioCortesTurno.ObtenerNombreTurno(fajilla.IdCorteTurno),
                Desglose = montos.Values.ToList()
            };

        }

        #region métodos internal

        void IServicioFajillasInterno.ValidarNoExistenFajillasPendientes()
        {
            var corte = ServicioCortesTurno.ObtenerUltimoCorte();

            if (RepositorioFajillas.Alguno(m => m.Activa && !m.EsSobrante && !m.Autorizada && m.IdCorteTurno == corte.Id))
                throw new SOTException(Recursos.Fajillas.fajillas_pendientes_excepcion);
        }

        List<DtoMoneda> IServicioFajillasInterno.ObtenerDesgloseEfectivo(int idCorteTurno) 
        {
            var fajillas = RepositorioFajillas.ObtenerFajillasAutorizadasConMontosPorCorte(idCorteTurno);
            var sobrante = RepositorioFajillas.ObtenerSobrantePorTurno(idCorteTurno);

            if (sobrante != null)
                fajillas.Add(sobrante);

            var configuracion = RepositorioConfiguracionesFajillas.ObtenerConfiguracionFajillasCargada();

            if (configuracion == null)
                throw new SOTException(Recursos.Fajillas.configuracion_fajillas_nula_excepcion);

            var montos = new Dictionary<int, DtoMoneda>();

            foreach (var montoC in configuracion.MontosConfiguracionFajilla.Where(m => m.Activa))
            { 
                var montoN = new DtoMoneda();
                montos.Add(montoC.Id, montoN);

                montoN.Monto = montoC.Monto;
                if (montoC.IdMonedaExtranjera.HasValue)
                {
                    montoN.Nombre = montoC.MonedaExtranjera.Nombre;
                    montoN.ValorCambio = montoC.MonedaExtranjera.ValorCambio;
                }
                else
                {
                    montoN.Nombre = "Peso";
                    montoN.ValorCambio = 1;
                }
            }
            
            foreach (var fajilla in fajillas)
            {
                if (fajilla.MontosFajilla.Count(m => m.Activa) != fajilla.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().Count())
                    throw new SOTException(Recursos.Fajillas.montos_duplicados_excepcion);

                var secuenciaMontos = fajilla.MontosFajilla.Where(m => m.Activa).Select(m => m.IdMontoConfiguracionFajilla).Distinct().OrderBy(m => m).ToList();
                var secuenciaConfiguracion = configuracion.MontosConfiguracionFajilla.Where(m => m.Activa).Select(m => m.Id).Distinct().OrderBy(m => m).ToList();

                if (!secuenciaMontos.SequenceEqual(secuenciaConfiguracion))
                    throw new SOTException(Recursos.Fajillas.montos_fajilla_diferentes_configuracion_excepcion);

                decimal valor = 0;

                foreach (var par in (from monto in fajilla.MontosFajilla
                                     join configuracionMonto in configuracion.MontosConfiguracionFajilla on monto.IdMontoConfiguracionFajilla equals configuracionMonto.Id
                                     where monto.Activa
                                     && configuracion.Activa
                                     select new
                                     {
                                         monto,
                                         configuracionMonto
                                     }))
                {
                    if (par.monto.Cantidad < 0)
                        throw new SOTException(Recursos.Fajillas.cantidad_monto_invalida_excepcion, par.configuracionMonto.Monto.ToString("C"));

                    if (par.configuracionMonto.IdMonedaExtranjera.HasValue)
                        valor += par.monto.Cantidad * par.configuracionMonto.Monto * par.configuracionMonto.MonedaExtranjera.ValorCambio;
                    else
                        valor += par.monto.Cantidad * par.configuracionMonto.Monto;

                    var montoN = montos[par.configuracionMonto.Id];

                    montoN.Cantidad += par.monto.Cantidad;


                }

                if (fajilla.Valor != valor)
                    throw new SOTException(Recursos.Fajillas.valor_fajilla_invalido_excepcion, fajilla.Valor.ToString("C"), valor.ToString("C"));

            }

            return montos.Values.ToList();
        }

        int IServicioFajillasInterno.ObtenerCantidadFajillasAutorizadas(int idCorteTurno) 
        {
            return RepositorioFajillas.Contador(m => m.IdCorteTurno == idCorteTurno && m.Activa && !m.EsSobrante && m.Autorizada);
        }

        public void GuardarConfiguracion(DtoConfiguracionFajilla dtoConfiguracion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarConfiguracionFajillas = true });
            var configuracionActual = RepositorioConfiguracionesFajillas.ObtenerConfiguracionFajillasCargada();

            var fechaActual = DateTime.Now;

            var configuracion = new ConfiguracionFajilla
            {
                Activa = true,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                Valor = dtoConfiguracion.Valor,
            };

            if (dtoConfiguracion.Valor <= 0)
                throw new SOTException(Recursos.Fajillas.valor_configuracion_fajillas_invalido_excepcion);

            if (dtoConfiguracion.DenominacionesNacionales.Count == 0)
                throw new SOTException(Recursos.Fajillas.denominaciones_inexistentes_excepcion);

            if (dtoConfiguracion.DenominacionesNacionales.Count != dtoConfiguracion.DenominacionesNacionales.GroupBy(m=> m.Valor).Count())
                throw new SOTException(Recursos.Fajillas.denominaciones_repetidas_excepcion);

            if (dtoConfiguracion.DenominacionesNacionales.Any(m => m.Valor <= 0))
                throw new SOTException(Recursos.Fajillas.denominaciones_valor_invalido_excepcion);

            if (dtoConfiguracion.MonedasExtranjeras.Any(m => m.ValorCambio <= 0))
                throw new SOTException(Recursos.Fajillas.monedas_extranjeras_valor_cambio_invalido_excepcion);

            if (dtoConfiguracion.MonedasExtranjeras.Any(m => string.IsNullOrWhiteSpace(m.Abreviatura)))
                throw new SOTException(Recursos.Fajillas.abreviatura_moneda_invalida_excepcion);

            if (dtoConfiguracion.MonedasExtranjeras.Any(m => string.IsNullOrWhiteSpace(m.Nombre)))
                throw new SOTException(Recursos.Fajillas.nombre_moneda_invalido_excepcion);

            if (dtoConfiguracion.MonedasExtranjeras.Any(m => m.Abreviatura.Trim().ToUpper().Equals("MXN")))
                throw new SOTException(Recursos.Fajillas.abreviaturas_monedas_prohibida_excepcion, "MXN");

            if (dtoConfiguracion.MonedasExtranjeras.Count != dtoConfiguracion.MonedasExtranjeras.GroupBy(m => m.Abreviatura.Trim().ToUpper()).Count())
                throw new SOTException(Recursos.Fajillas.abreviaturas_monedas_repetidas_excepcion);

            if (dtoConfiguracion.MonedasExtranjeras.Count != dtoConfiguracion.MonedasExtranjeras.GroupBy(m => m.Nombre.Trim().ToUpper()).Count())
                throw new SOTException(Recursos.Fajillas.nombres_monedas_repetidos_excepcion);

            foreach (var denominacion in dtoConfiguracion.DenominacionesNacionales)
            {
                configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                {
                    Activa = true,
                    Monto = denominacion.Valor
                });
            }

            foreach (var moneda in dtoConfiguracion.MonedasExtranjeras)
            {
                if (moneda.Denominaciones.Count == 0)
                    throw new SOTException(Recursos.Fajillas.denominaciones_extranjeras_inexistentes_excepcion, moneda.Abreviatura, moneda.Nombre);

                if (moneda.Denominaciones.Count != moneda.Denominaciones.GroupBy(m => m.Valor).Count())
                    throw new SOTException(Recursos.Fajillas.denominaciones_extranjeras_repetidas_excepcion, moneda.Abreviatura, moneda.Nombre);

                if (moneda.Denominaciones.Any(m => m.Valor <= 0))
                    throw new SOTException(Recursos.Fajillas.denominaciones_extranjeras_valor_invalido_excepcion, moneda.Abreviatura, moneda.Nombre);

                var monedaNueva = new MonedaExtranjera
                {
                    Activa = true,
                    Abreviatura = moneda.Abreviatura,
                    Nombre = moneda.Nombre,
                    ValorCambio = moneda.ValorCambio
                };

                configuracion.MonedasExtranjeras.Add(monedaNueva);

                foreach (var denominacion in moneda.Denominaciones)
                {
                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = denominacion.Valor,
                        MonedaExtranjera = monedaNueva
                    });
                }
            }

            RepositorioConfiguracionesFajillas.Agregar(configuracion);

            if (configuracionActual != null)
            {
                configuracionActual.Activa = false;
                configuracionActual.FechaEliminacion = fechaActual;
                configuracionActual.FechaEliminacion = fechaActual;
                configuracionActual.IdUsuarioElimino = usuario.Id;
                configuracionActual.IdUsuarioModifico = usuario.Id;

                foreach (var moneda in configuracionActual.MonedasExtranjeras)
                {
                    moneda.Activa = false;
                    moneda.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;
                }

                foreach (var monto in configuracionActual.MontosConfiguracionFajilla)
                {
                    monto.Activa = false;
                    monto.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;
                }

                RepositorioConfiguracionesFajillas.Modificar(configuracionActual);
            }

            RepositorioConfiguracionesFajillas.GuardarCambios();
        }

        #endregion
    }
}
