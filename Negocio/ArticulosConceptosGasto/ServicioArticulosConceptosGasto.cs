﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Negocio.Almacen.Articulos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.ArticulosConceptosGasto
{
    public class ServicioArticulosConceptosGasto : IServicioArticulosConceptosGastoInterno
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });


        private IRepositorioArticulosConceptosGasto RepositorioArticulosConceptosGasto
        {
            get { return _repositorioArticulosConceptosGasto.Value; }
        }

        private Lazy<IRepositorioArticulosConceptosGasto> _repositorioArticulosConceptosGasto = new Lazy<IRepositorioArticulosConceptosGasto>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioArticulosConceptosGasto>();
        });

        public List<Modelo.Entidades.ArticuloConceptoGasto> ObtenerVinculaciones(string codigoArticulo, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual)
        {
            return RepositorioArticulosConceptosGasto.ObtenerElementos(m => m.CodigoArticulo.Equals(codigoArticulo)).ToList();
        }

        public void EliminarVinculacion(int idArticuloConcepto, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual)
        {
            var vinculacion = RepositorioArticulosConceptosGasto.Obtener(m => m.Id == idArticuloConcepto);

            if (vinculacion == null)
                throw new SOTException("La vinculación que desea eliminar no existe");

            if (ServicioArticulos.VerificarEsPresentacion(vinculacion.CodigoArticulo))
                throw new SOTException("No se puede asignar una vinculación con un gasto directamente a una presentación, tiene que ser por medio del artículo principal");

            List<string> presentaciones = ServicioArticulos.ObtenerClavesPresentaciones(vinculacion.CodigoArticulo);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ArticuloConceptoGasto vinculacionPres;

                foreach (var clavePresentacion in presentaciones)
                {
                    vinculacionPres = RepositorioArticulosConceptosGasto.Obtener(m => m.CodigoArticulo == clavePresentacion && m.IdConceptoGasto == vinculacion.IdConceptoGasto);

                    if (vinculacionPres != null)
                    {
                        RepositorioArticulosConceptosGasto.Eliminar(vinculacionPres);
                    }
                }

                RepositorioArticulosConceptosGasto.Eliminar(vinculacion);
                RepositorioArticulosConceptosGasto.GuardarCambios();

                scope.Complete();
            }
        }

        public void ModificarVinculacion(ArticuloConceptoGasto articuloConcepto, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual)
        {
            if (string.IsNullOrWhiteSpace(articuloConcepto.CodigoArticulo))
                throw new SOTException("El código de artículo a vincular no puede ser una cadena vacía");

            if (ServicioArticulos.VerificarEsPresentacion(articuloConcepto.CodigoArticulo))
                throw new SOTException("No se puede asignar una vinculación con un gasto directamente a una presentación, tiene que ser por medio del artículo principal");

            var vinculacion = RepositorioArticulosConceptosGasto.Obtener(m => m.Id == articuloConcepto.Id);

            if (vinculacion == null)
                throw new SOTException("La vinculación que desea actualizar no existe");

            if (articuloConcepto.CodigoArticulo != vinculacion.CodigoArticulo)
                throw new SOTException("No se puede cambiar el artículo de la vinculación");

            var idConceptoOriginal = vinculacion.IdConceptoGasto;

            vinculacion.SincronizarPrimitivas(articuloConcepto);

            if (RepositorioArticulosConceptosGasto.Alguno(m => m.Id != vinculacion.Id && m.CodigoArticulo.Equals(vinculacion.CodigoArticulo) && m.IdConceptoGasto == vinculacion.IdConceptoGasto))
                throw new SOTException("Ya existe una vinculación con el concepto seleccionado");

            List<string> presentaciones = ServicioArticulos.ObtenerClavesPresentaciones(vinculacion.CodigoArticulo);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ArticuloConceptoGasto vinculacionPres;

                foreach (var clavePresentacion in presentaciones)
                {
                    vinculacionPres = RepositorioArticulosConceptosGasto.Obtener(m => m.CodigoArticulo == clavePresentacion && m.IdConceptoGasto == idConceptoOriginal);

                    if (vinculacionPres == null)
                    {
                        RepositorioArticulosConceptosGasto.Agregar(new ArticuloConceptoGasto
                        {
                            CodigoArticulo = clavePresentacion,
                            IdConceptoGasto = vinculacion.IdConceptoGasto
                        });
                    }
                    else
                    {
                        vinculacionPres.IdConceptoGasto = vinculacion.IdConceptoGasto;

                        if (RepositorioArticulosConceptosGasto.Alguno(m => m.Id != vinculacionPres.Id && m.CodigoArticulo.Equals(vinculacionPres.CodigoArticulo) && m.IdConceptoGasto == vinculacionPres.IdConceptoGasto))
                            throw new SOTException("Ya existe una vinculación con el concepto seleccionado");

                        RepositorioArticulosConceptosGasto.Modificar(vinculacionPres);
                    }
                }

                RepositorioArticulosConceptosGasto.Modificar(vinculacion);
                RepositorioArticulosConceptosGasto.GuardarCambios();

                scope.Complete();
            }
        }

        public void CrearVinculacion(Modelo.Entidades.ArticuloConceptoGasto articuloConcepto, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual)
        {
            if (string.IsNullOrWhiteSpace(articuloConcepto.CodigoArticulo))
                throw new SOTException("El código de artículo a vincular no puede ser una cadena vacía");

            if (ServicioArticulos.VerificarEsPresentacion(articuloConcepto.CodigoArticulo))
                throw new SOTException("No se puede asignar una vinculación con un gasto directamente a una presentación, tiene que ser por medio del artículo principal");

            if (RepositorioArticulosConceptosGasto.Alguno(m => m.Id != articuloConcepto.Id && m.CodigoArticulo.Equals(articuloConcepto.CodigoArticulo) && m.IdConceptoGasto == articuloConcepto.IdConceptoGasto))
                throw new SOTException("Ya existe una vinculación con el concepto seleccionado");


            List<string> presentaciones = ServicioArticulos.ObtenerClavesPresentaciones(articuloConcepto.CodigoArticulo);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ArticuloConceptoGasto vinculacionPres;

                foreach (var clavePresentacion in presentaciones)
                {
                    vinculacionPres = RepositorioArticulosConceptosGasto.Obtener(m => m.CodigoArticulo == clavePresentacion && m.IdConceptoGasto == articuloConcepto.IdConceptoGasto);

                    if (vinculacionPres == null)
                    {
                        RepositorioArticulosConceptosGasto.Agregar(new ArticuloConceptoGasto
                        {
                            CodigoArticulo = clavePresentacion,
                            IdConceptoGasto = articuloConcepto.IdConceptoGasto
                        });
                    }
                }

                RepositorioArticulosConceptosGasto.Agregar(articuloConcepto);
                RepositorioArticulosConceptosGasto.GuardarCambios();

                scope.Complete();
            }
        }

        List<Modelo.Entidades.ArticuloConceptoGasto> IServicioArticulosConceptosGastoInterno.ObtenerVinculaciones(string codigoArticulo)
        {
            return RepositorioArticulosConceptosGasto.ObtenerElementos(m => m.CodigoArticulo.Equals(codigoArticulo)).ToList();
        }
    }
}
