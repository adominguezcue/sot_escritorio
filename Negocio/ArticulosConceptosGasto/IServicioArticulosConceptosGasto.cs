﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ArticulosConceptosGasto
{
    public interface IServicioArticulosConceptosGasto
    {
        List<Modelo.Entidades.ArticuloConceptoGasto> ObtenerVinculaciones(string codigoArticulo, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual);

        void EliminarVinculacion(int idArticuloConcepto, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual);

        void ModificarVinculacion(Modelo.Entidades.ArticuloConceptoGasto articuloConcepto, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual);

        void CrearVinculacion(Modelo.Entidades.ArticuloConceptoGasto articuloConcepto, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual);
    }
}
