﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ArticulosConceptosGasto
{
    internal interface IServicioArticulosConceptosGastoInterno : IServicioArticulosConceptosGasto
    {
        List<Modelo.Entidades.ArticuloConceptoGasto> ObtenerVinculaciones(string codigoArticulo);
    }
}
