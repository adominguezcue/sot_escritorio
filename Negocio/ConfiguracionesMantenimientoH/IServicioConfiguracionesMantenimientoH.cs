﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConfiguracionesMantenimientoH
{
    public interface IServicioConfiguracionesMantenimientoH 
    {
        void ValidaSiRegistraLectura(int idcorte);

        void ModificaRegistro(int configturno, int periodo);
    }
}
