﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Modelo.Entidades;
using Modelo.Repositorios;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.ConfiguracionesMantenimientoH
{
    public class ServicioConfiguracionesMantenimeintoH : IServicioConfiguracionesMantenimientoH
    {
        IRepositorioConfiguracionesMantenimientoH RepositpositorioConfiguracionesMantenimientoH
        {
            get { return _repositorioconfiguracionmantenimientoh.Value; }
        }

        Lazy<IRepositorioConfiguracionesMantenimientoH> _repositorioconfiguracionmantenimientoh = new Lazy<IRepositorioConfiguracionesMantenimientoH>(
            () => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesMantenimientoH>(); });

        IRepositorioCatPeriodo RepositorioCatalogoPeriodo
        {
            get { return _reporistoriocatalogoperiodo.Value; }
        }
        Lazy<IRepositorioCatPeriodo> _reporistoriocatalogoperiodo = new Lazy<IRepositorioCatPeriodo>(
            () => { return FabricaDependencias.Instancia.Resolver<IRepositorioCatPeriodo>(); });


        public void ValidaSiRegistraLectura(int idcorte)
        {
            if (RepositpositorioConfiguracionesMantenimientoH.EsInsertaRegistroPorCorteTurno(idcorte))
                throw new SOTException("Inserta Lecturas de Medidores en el Hotel");
        }

        public void ModificaRegistro(int configturno, int periodo)
        {
            if(configturno != null && configturno >0)
            {
                if(periodo != null && periodo >0)
                {
                    var listamodificar = RepositpositorioConfiguracionesMantenimientoH.ObtenerTodo().ToList();
                    foreach (ConfiguracionHMantenimiento datomodifica in listamodificar)
                    {
                        datomodifica.Activa = false;
                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                        {
                            RepositpositorioConfiguracionesMantenimientoH.Modificar(datomodifica);
                            RepositpositorioConfiguracionesMantenimientoH.GuardarCambios();
                            scope.Complete();
                        }
                    }
                    if (periodo == 1 || periodo == 2)
                    {
                        if (periodo == 2)
                        { //////////cyuando es diario
                            var registromodifica = RepositpositorioConfiguracionesMantenimientoH.Obtener(m => m.IdConfiguracionTurno == configturno);
                            registromodifica.Activa = true;
                            registromodifica.IdCatPeriodoDato = periodo;
                            registromodifica.IdConfiguracionTurno = configturno;
                            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                            {
                                RepositpositorioConfiguracionesMantenimientoH.Modificar(registromodifica);
                                RepositpositorioConfiguracionesMantenimientoH.GuardarCambios();
                                scope.Complete();
                            }
                        }
                        else
                        {
                             //////////cuando es por turno
                            var listamodificar1 = RepositpositorioConfiguracionesMantenimientoH.ObtenerTodo().ToList();
                            foreach (ConfiguracionHMantenimiento datomodifica1 in listamodificar1)
                            {
                                datomodifica1.Activa = true;
                                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                                {
                                    RepositpositorioConfiguracionesMantenimientoH.Modificar(datomodifica1);
                                    RepositpositorioConfiguracionesMantenimientoH.GuardarCambios();
                                    scope.Complete();
                                }
                            }

                        }
                    }
                    else
                    {
                        var datomodifica = RepositpositorioConfiguracionesMantenimientoH.Obtener(m => m.IdConfiguracionTurno == configturno);
                       datomodifica.Activa = true;
                       datomodifica.IdCatPeriodoDato = periodo;
                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                        {
                            RepositpositorioConfiguracionesMantenimientoH.Modificar(datomodifica);
                            RepositpositorioConfiguracionesMantenimientoH.GuardarCambios();
                            scope.Complete();
                        }
                        
                    }
                }
            }
        }
    }
}
