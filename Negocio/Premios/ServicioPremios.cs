﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Premios
{
    public class ServicioPremios : IServicioPremios
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IRepositorioPremios RepositorioPremios
        {
            get { return _repositorioPremios.Value; }
        }

        Lazy<IRepositorioPremios> _repositorioPremios = new Lazy<IRepositorioPremios>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPremios>(); });

        public Premio ObtenerPremioCargadoNoNull(string numeroPremio, DtoUsuario usuario)
        {
            var premio = RepositorioPremios.ObtenerPorNumeroCargado(numeroPremio);

            if (premio == null)
                throw new SOTException(Recursos.Premios.numero_premio_invalido_excepcion, numeroPremio);

            return premio;
        }
    }
}
