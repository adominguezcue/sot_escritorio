﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Premios
{
    public interface IServicioPremios
    {
        Premio ObtenerPremioCargadoNoNull(string numeroPremio, DtoUsuario usuario);
    }
}
