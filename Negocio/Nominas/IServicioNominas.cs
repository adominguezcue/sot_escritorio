﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;

namespace Negocio.Nominas
{
    public interface IServicioNominas
    {
        /// <summary>
        /// Genera una nómina con la información proporcionada pero no la almacena en base de datos
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="anio"></param>
        /// <param name="mes"></param>
        /// <param name="primeraMitad"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Nomina GenerarNominaNoPersistida(int idEmpleado, int anio, int mes, bool primeraMitad, bool enVacaciones, DtoUsuario usuario);
        /// <summary>
        /// Permite registrar una nueva nómina
        /// </summary>
        /// <param name="nuevaNomina"></param>
        /// <param name="usuario"></param>
        void RegistrarNomina(Nomina nuevaNomina, DtoUsuario usuario);
        /// <summary>
        /// Retorna las nóminas que coincidan con los filtros proporcionados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<Nomina> ObtenerNominasFiltradas(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null);
        /// <summary>
        /// Permite actualizar los datos de la nómina
        /// </summary>
        /// <param name="nomina"></param>
        /// <param name="usuario"></param>
        void ModificarNomina(Nomina nomina, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar la nómina con el id proporcionado
        /// </summary>
        /// <param name="idNomina"></param>
        /// <param name="usuario"></param>
        void EliminarNomina(int idNomina, DtoUsuario usuario);
    }
}
