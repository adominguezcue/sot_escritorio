﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Asistencias;
using Negocio.ConfiguracionesAsistencia;
using Negocio.Empleados;
using Negocio.Seguridad.Permisos;
using Negocio.SolicitudesPermisosFaltas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Nominas
{
    public class ServicioNominas : IServicioNominas
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioSolicitudesPermisosFaltas ServicioSolicitudesPermisosFaltas
        {
            get { return _servicioSolicitudesPermisosFaltas.Value; }
        }

        Lazy<IServicioSolicitudesPermisosFaltas> _servicioSolicitudesPermisosFaltas = new Lazy<IServicioSolicitudesPermisosFaltas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioSolicitudesPermisosFaltas>(); });
        
        IServicioAsistenciasInterno ServicioAsistencias
        {
            get { return _servicioAsistencias.Value; }
        }

        Lazy<IServicioAsistenciasInterno> _servicioAsistencias = new Lazy<IServicioAsistenciasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioAsistenciasInterno>(); });

        IServicioConfiguracionesAsistencias ServicioConfiguracionesAsistencias
        {
            get { return _servicioConfiguracionesAsistencias.Value; }
        }

        Lazy<IServicioConfiguracionesAsistencias> _servicioConfiguracionesAsistencias = new Lazy<IServicioConfiguracionesAsistencias>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesAsistencias>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IRepositorioNominas RepositorioNominas
        {
            get { return _repositorioNominas.Value; }
        }

        Lazy<IRepositorioNominas> _repositorioNominas = new Lazy<IRepositorioNominas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioNominas>(); });

        public void RegistrarNomina(Nomina nuevaNomina, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearNomina = true });

            if (nuevaNomina == null)
                throw new SOTException(Recursos.Nominas.nomina_nula_excepcion);

            if(!ServicioEmpleados.VerificarEmpleadoActivo(nuevaNomina.IdEmpleado))
                throw new SOTException(Recursos.Nominas.empleado_eliminado_excepcion); 

            if (RepositorioNominas.Alguno(m => m.Activa && m.IdEmpleado == nuevaNomina.IdEmpleado &&
                                            m.FechaInicio == nuevaNomina.FechaInicio && m.FechaFin == nuevaNomina.FechaFin))
                throw new SOTException(Recursos.Nominas.nomina_duplicada_excepcion);

            var nominaValidacion = GenerarNominaNoPersistida(nuevaNomina.IdEmpleado, nuevaNomina.FechaInicio.Year, nuevaNomina.FechaInicio.Month, nuevaNomina.FechaInicio.Day == 1, nuevaNomina.EnVacaciones, usuario);

            if (nominaValidacion.Faltas != nuevaNomina.Faltas ||
                nominaValidacion.Permisos != nuevaNomina.Permisos ||
                nominaValidacion.Retardos != nuevaNomina.Retardos ||
                nominaValidacion.Salario != nuevaNomina.Salario ||
                nominaValidacion.Bonos != nuevaNomina.Bonos)
                throw new SOTException(Recursos.Nominas.nomina_invalida_excepcion);

            var fechaActual = DateTime.Now;

            nuevaNomina.Activa = true;
            nuevaNomina.FechaCreacion = fechaActual;
            nuevaNomina.FechaModificacion = fechaActual;
            nuevaNomina.IdUsuarioCreo = usuario.Id;
            nuevaNomina.IdUsuarioModifico = usuario.Id;

            RepositorioNominas.Agregar(nuevaNomina);
            RepositorioNominas.GuardarCambios();
        }

        public Nomina GenerarNominaNoPersistida(int idEmpleado, int anio, int mes, bool primeraMitad, bool enVacaciones, DtoUsuario usuario) 
        {
            //var fechaActual = new DateTime(anio, mes, 1);

            DateTime fechaInicio, fechaFin;

            try
            {
                if (primeraMitad)
                {
                    //Primer día del mes
                    fechaInicio = new DateTime(anio, mes, 1);
                    //Último segundo del día 15 del mes solicitado
                    fechaFin = fechaInicio.AddDays(-fechaInicio.Day + 16).AddSeconds(-1);
                }
                else
                {
                    //Día 16 del mes
                    fechaInicio = new DateTime(anio, mes, 16);
                    //Último segundo del último día del mes solicitado
                    fechaFin = fechaInicio.Date.AddDays(-fechaInicio.Day + DateTime.DaysInMonth(anio, mes) + 1).AddSeconds(-1);
                }
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                throw new SOTException(Recursos.Nominas.anio_o_mes_invalidos_excepcion, e);
            }

            var empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorIdConCategoria(idEmpleado);
           
            if(empleado == null)
                return new Nomina
                {
                    Activa = true,
                    IdEmpleado = idEmpleado,
                    FechaInicio = fechaInicio,
                    FechaFin = fechaFin
                };

                //throw new SOTException(Recursos.Nominas.empleado_eliminado_excepcion);

            var configuracion = ServicioConfiguracionesAsistencias.ObtenerConfiguracionAsistencias();

            var diasTrabajados = (decimal)Math.Round((fechaFin.AddDays(1).Date - fechaInicio).TotalDays);
            decimal salarioDiario = empleado.Salario / diasTrabajados;

            var asistencias = ServicioAsistencias.ObtenerAsistenciasEmpleado(idEmpleado, fechaInicio, fechaFin);

            var plusasistenciasValidas = asistencias.Count(m => !m.TieneFalta && m.EsDobleTurno);
            var plusfaltasJustificadas = asistencias.Count(m => m.TieneFalta && m.Justificado && m.EsDobleTurno);
            //var plusretardosJustificados = asistencias.Count(m => m.TieneRetardo && m.Justificado && m.EsDobleTurno);
           // var plusretardos = asistencias.Count(m => m.TieneRetardo && !m.Justificado && m.EsDobleTurno);
            //var plusfaltas = asistencias.Count(m => m.TieneFalta && !m.Justificado && m.EsDobleTurno);
            //var pluspermisos = plusfaltasJustificadas + plusretardosJustificados;
            //Por si hay algun día sin registro
            //plusfaltas += asistencias.Count - pluspermisos - plusasistenciasNormales - plusretardos - plusfaltas;

            var multiplicadorDoblesTurnos = plusasistenciasValidas * configuracion.DiasDobleTurno +
                       plusfaltasJustificadas * (configuracion.DiasDobleTurno - configuracion.DiasDescuentoJustificado);

            var asistenciasNormales = asistencias.Count(m => !m.TieneRetardo && !m.TieneFalta && !m.EsDobleTurno);
            var faltasJustificadas = asistencias.Count(m => m.TieneFalta && m.Justificado && !m.EsDobleTurno);
            var retardosJustificados = asistencias.Count(m => m.TieneRetardo && m.Justificado && !m.EsDobleTurno);
            var retardos = asistencias.Count(m => m.TieneRetardo && !m.Justificado && !m.EsDobleTurno);
            var faltas = asistencias.Count(m => m.TieneFalta && !m.Justificado && !m.EsDobleTurno);
            var permisos = faltasJustificadas + retardosJustificados;
            //Por si hay algun día sin registro
            faltas += (int)diasTrabajados - permisos - asistenciasNormales - retardos - faltas;

            decimal descuentoFaltasJustificadas, descuentoFaltas, dobleTurno;
            descuentoFaltasJustificadas = descuentoFaltas = dobleTurno = 0;

            if (!enVacaciones)
            {
                descuentoFaltasJustificadas = salarioDiario * faltasJustificadas * configuracion.DiasDescuentoJustificado;
                descuentoFaltas = salarioDiario * faltas * configuracion.DiasDescuento;
                dobleTurno = salarioDiario * multiplicadorDoblesTurnos;
            }

            var pagoFinal = empleado.Salario - descuentoFaltas - descuentoFaltasJustificadas + dobleTurno;

            var bono = pagoFinal * empleado.CategoriaEmpleado.PorcentajeBonificacion;

            return new Nomina
            {
                Activa = true,
                IdEmpleado = idEmpleado,
                Faltas = faltas,
                Permisos = permisos,
                Retardos = retardos,
                FechaInicio = fechaInicio,
                FechaFin = fechaFin,
                Salario = Math.Max(pagoFinal, 0),
                Bonos = Math.Max(bono, 0),
                EnVacaciones = enVacaciones
            };
        }

        public List<Nomina> ObtenerNominasFiltradas(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarNomina = true });

            return RepositorioNominas.ObtenerNominasFiltradas(nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }


        public void ModificarNomina(Nomina nomina, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarNomina = true });

            if (nomina == null)
                throw new SOTException(Recursos.Nominas.nomina_nula_excepcion);

            if (!RepositorioNominas.Alguno(m => m.Activa && m.Id == nomina.Id))
                throw new SOTException(Recursos.Nominas.nomina_nula_eliminada_excepcion);

            if (!nomina.Activa)
                throw new SOTException(Recursos.Nominas.nomina_no_eliminable_excepcion);

            if (!ServicioEmpleados.VerificarEmpleadoActivo(nomina.IdEmpleado))
                throw new SOTException(Recursos.Nominas.empleado_eliminado_excepcion); 

            if (RepositorioNominas.Alguno(m => m.Id != nomina.Id && m.Activa && m.IdEmpleado == nomina.IdEmpleado &&
                                            m.FechaInicio == nomina.FechaInicio && m.FechaFin == nomina.FechaFin))
                throw new SOTException(Recursos.Nominas.nomina_duplicada_excepcion);

            var nominaValidacion = GenerarNominaNoPersistida(nomina.IdEmpleado, nomina.FechaInicio.Year, nomina.FechaInicio.Month, nomina.FechaInicio.Day == 1, nomina.EnVacaciones, usuario);

            if (nominaValidacion.Faltas != nomina.Faltas ||
                nominaValidacion.Permisos != nomina.Permisos ||
                nominaValidacion.Retardos != nomina.Retardos ||
                nominaValidacion.Salario != nomina.Salario ||
                nominaValidacion.Bonos != nomina.Bonos)
                throw new SOTException(Recursos.Nominas.nomina_invalida_excepcion);

            var fechaActual = DateTime.Now;

            nomina.FechaModificacion = fechaActual;
            nomina.IdUsuarioModifico = usuario.Id;

            RepositorioNominas.Modificar(nomina);
            RepositorioNominas.GuardarCambios();
        }

        public void EliminarNomina(int idNomina, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarNomina = true });

            var nomina = RepositorioNominas.Obtener(m => m.Activa && m.Id == idNomina);

            if (nomina == null)
                throw new SOTException(Recursos.Nominas.nomina_nula_eliminada_excepcion);

            //if (RepositorioNominas.Alguno(m => m.Id != nomina.Id && m.Activa && m.IdEmpleado == nomina.IdEmpleado &&
            //                                m.FechaInicio == nomina.FechaInicio && m.FechaFin == nomina.FechaFin))
            //    throw new SOTException(Recursos.Nominas.nomina_duplicada_excepcion);

            //var nominaValidacion = GenerarNominaNoPersistida(nomina.IdEmpleado, nomina.FechaInicio.Year, nomina.FechaInicio.Month, nomina.FechaInicio.Day == 1, usuario);

            //if (nominaValidacion.Faltas != nomina.Faltas ||
            //    nominaValidacion.Permisos != nomina.Permisos ||
            //    nominaValidacion.Retardos != nomina.Retardos ||
            //    nominaValidacion.Salario != nomina.Salario ||
            //    nominaValidacion.Bonos != nomina.Bonos)
            //    throw new SOTException(Recursos.Nominas.nomina_invalida_excepcion);

            var fechaActual = DateTime.Now;

            nomina.Activa = false;
            nomina.FechaModificacion = fechaActual;
            nomina.IdUsuarioModifico = usuario.Id;
            nomina.FechaEliminacion= fechaActual;
            nomina.IdUsuarioElimino = usuario.Id;

            RepositorioNominas.Modificar(nomina);
            RepositorioNominas.GuardarCambios();
        }
    }
}
