﻿using System;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Modelo.Dtos;
using Negocio.Seguridad.Permisos;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.MatriculaClientes
{
    public class ServicioMatriculaCliente : IServicioMatriculaCliente
    {

        private IRepositorioMatriculaAuto RepositorioMatriculasAuto
        {
            get { return _repositorioMatriculasAuto.Value; }
        }

        private Lazy<IRepositorioMatriculaAuto> _repositorioMatriculasAuto = new Lazy<IRepositorioMatriculaAuto>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioMatriculaAuto>();
        });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }
        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        public List<AutomovilRenta> ObtieneMatriculasAutos(DtoUsuario usuario, string matricula = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultaClientesMatricula = true });

            return RepositorioMatriculasAuto.ObtieneMatriculasAutos(matricula);
        }

        public List<DtoHabitacionesxMatricula> ObtieneHabitacionesPorMatricula(DtoUsuario UsuarioActual, string matricula)
        {
            ServicioPermisos.ValidarPermisos(UsuarioActual, new DtoPermisos { ConsultaClientesMatricula = true });

            return RepositorioMatriculasAuto.ObtieneHabitacionesPorMatricula(matricula);
        }


        public List<DtTotalComandasxRenta> ObtieneComandasPorRenta(DtoUsuario UsuarioActual, int idrenta)
        {
            ServicioPermisos.ValidarPermisos(UsuarioActual, new DtoPermisos { ConsultaClientesMatricula = true });

            return RepositorioMatriculasAuto.ObtieneComandasPorRenta(idrenta);
        }

    }
}
