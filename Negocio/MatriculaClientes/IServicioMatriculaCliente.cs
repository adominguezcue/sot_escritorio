﻿using Modelo.Entidades;
using Modelo.Dtos;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.MatriculaClientes
{
    public interface IServicioMatriculaCliente
    {
         List<AutomovilRenta> ObtieneMatriculasAutos(DtoUsuario usuario, string matricula = null);


        List<DtoHabitacionesxMatricula> ObtieneHabitacionesPorMatricula(DtoUsuario UsuarioActual, string matricula);

        List<DtTotalComandasxRenta> ObtieneComandasPorRenta(DtoUsuario UsuarioActual,int idrenta);
    }
}
