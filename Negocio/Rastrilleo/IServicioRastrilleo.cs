﻿using Negocio.ServiciosExternos.DTOs.Rastrilleo;
using Negocio.ServiciosExternos.wsRastrilleo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Rastrilleo
{
    public interface IServicioRastrilleo
    {
        List<DtoEstatusTransaccion> EnviarOrden(string folioCaja, MastPedido[] pedidos);
        List<DtoEstatusTransaccion> MarcarOrden(string folioCaja, MastPedido[] pedidos);
        System.Data.DataTable ObtenerRastrilleadas(string folioCaja);
        void Sincronizar();
    }
}
