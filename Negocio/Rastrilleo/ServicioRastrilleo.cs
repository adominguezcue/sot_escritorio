﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesGlobales;
using Negocio.CortesTurno;
using Negocio.Rentas;
using Negocio.Restaurantes;
using Negocio.Seguridad.Permisos;
using Negocio.ServiciosExternos.Agentes.Rastrilleo;
using Negocio.ServiciosExternos.DTOs.Rastrilleo;
using Negocio.ServiciosExternos.wsRastrilleo;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using serviciosE = Negocio.ServiciosExternos;

namespace Negocio.Rastrilleo
{
    public class ServicioRastrilleo : IServicioRastrilleo
    {
        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioRentasInterno ServicioRentas
        {
            get { return _servicioRentas.Value; }
        }

        Lazy<IServicioRentasInterno> _servicioRentas = new Lazy<IServicioRentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRentasInterno>(); });

        IServicioRestaurantesInterno ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantesInterno> _servicioRestaurantes = new Lazy<IServicioRestaurantesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantesInterno>(); });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IRepositorioCortesTurno RepositorioCortesTurno
        {
            get { return _repostorioCortesTurno.Value; }
        }

        Lazy<IRepositorioCortesTurno> _repostorioCortesTurno = new Lazy<IRepositorioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCortesTurno>(); });

        IRepositorioVentas RepositorioVentas
        {
            get { return _repostorioVentas.Value; }
        }

        Lazy<IRepositorioVentas> _repostorioVentas = new Lazy<IRepositorioVentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentas>(); });

        IRepositorioEmpleados RepositorioEmpleados
        {
            get { return _repostorioEmpleados.Value; }
        }

        Lazy<IRepositorioEmpleados> _repostorioEmpleados = new Lazy<IRepositorioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioEmpleados>(); });
        
        IRepositorioVentasRentas RepositorioVentasRentas
        {
            get { return _repostorioVentasRentas.Value; }
        }

        Lazy<IRepositorioVentasRentas> _repostorioVentasRentas = new Lazy<IRepositorioVentasRentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentasRentas>(); });


        IRepositorioPagosComandas RepositorioPagosComandas
        {
            get { return _repostorioPagosComandas.Value; }
        }

        Lazy<IRepositorioPagosComandas> _repostorioPagosComandas = new Lazy<IRepositorioPagosComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosComandas>(); });

        IRepositorioPagosConsumosInternos RepositorioPagosConsumosInternos
        {
            get { return _repostorioPagosConsumosInternos.Value; }
        }

        Lazy<IRepositorioPagosConsumosInternos> _repostorioPagosConsumosInternos = new Lazy<IRepositorioPagosConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosConsumosInternos>(); });

        IRepositorioPagosOcupacionesMesa RepositorioPagosOcupacionesMesa
        {
            get { return _repostorioPagosOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioPagosOcupacionesMesa> _repostorioPagosOcupacionesMesa = new Lazy<IRepositorioPagosOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosOcupacionesMesa>(); });

        IRepositorioOcupacionesMesa RepositorioOcupacionesMesa
        {
            get { return _repostorioOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioOcupacionesMesa> _repostorioOcupacionesMesa = new Lazy<IRepositorioOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOcupacionesMesa>(); });

        IRepositorioComandas RepositorioComandas
        {
            get { return _repostorioComandas.Value; }
        }

        Lazy<IRepositorioComandas> _repostorioComandas = new Lazy<IRepositorioComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioComandas>(); });


        IRepositorioPagosRenta RepositorioPagosRenta
        {
            get { return _repostorioPagosRenta.Value; }
        }

        Lazy<IRepositorioPagosRenta> _repostorioPagosRenta = new Lazy<IRepositorioPagosRenta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosRenta>(); });

        IRepositorioPagosReservaciones RepositorioPagosReservaciones
        {
            get { return _repostorioPagosReservaciones.Value; }
        }

        Lazy<IRepositorioPagosReservaciones> _repostorioPagosReservaciones = new Lazy<IRepositorioPagosReservaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosReservaciones>(); });


        IRepositorioPagosTarjetasPuntos RepositorioPagosTarjetasPuntos
        {
            get { return _repostorioPagosTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioPagosTarjetasPuntos> _repostorioPagosTarjetasPuntos = new Lazy<IRepositorioPagosTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosTarjetasPuntos>(); });

        IRepositorioTarjetasPuntos RepositorioTarjetasPuntos
        {
            get { return _repostorioTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioTarjetasPuntos> _repostorioTarjetasPuntos = new Lazy<IRepositorioTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTarjetasPuntos>(); });

        IRepositorioConsumosInternos RepositorioConsumosInternos
        {
            get { return _repostorioConsumosInternos.Value; }
        }

        Lazy<IRepositorioConsumosInternos> _repostorioConsumosInternos = new Lazy<IRepositorioConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConsumosInternos>(); });

        public List<DtoEstatusTransaccion> EnviarOrden(string folioCaja, MastPedido[] pedidos)
        {
            var agenteR = new AgenteWsRastrilleo();

            //var usuarioServicio = ObtenerUsuarioServicios();
            //var contrasenaServicio = ObtenerContrasenaServicios();

            return agenteR.EnviarOrden(folioCaja, pedidos);
        }

        public List<DtoEstatusTransaccion> MarcarOrden(string folioCaja, MastPedido[] pedidos)
        {
            var agenteR = new AgenteWsRastrilleo();

            //var usuarioServicio = ObtenerUsuarioServicios();
            //var contrasenaServicio = ObtenerContrasenaServicios();

            return agenteR.MarcarOrden(folioCaja, pedidos);
        }

        public System.Data.DataTable ObtenerRastrilleadas(string folioCaja)
        {
            var agenteR = new AgenteWsRastrilleo();

            //var usuarioServicio = ObtenerUsuarioServicios();
            //var contrasenaServicio = ObtenerContrasenaServicios();

            return agenteR.ObtenerRastrilleadas(folioCaja, "");
        }

        public void Sincronizar()
        {
            //var ultimoCorteCerrado = ServicioCortesTurno.ObtenerUltimoCorteCerrado();

            var ultimoCorte = ServicioCortesTurno.ObtenerUltimoCorte();

            

            var ventas = RepositorioVentas.ObtenerElementos(m => (m.Activa || m.FechaCancelacion > m.CorteTurno.FechaCorte) && m.Correcta && !m.Subida && m.IdCorteTurno != ultimoCorte.Id).ToList();// && m.IdCorteTurno == ultimoCorteCerrado);

            var nombreSucursal = ServicioParametros.ObtenerParametros().NombreSucursal;


            foreach (var grupoVenta in ventas.GroupBy(m => m.IdCorteTurno))
            {
                var idEstadoCerrado = (int)CorteTurno.Estados.Cerrado;

                var corte = RepositorioCortesTurno.Obtener(m => m.Id == grupoVenta.Key && m.IdEstado == idEstadoCerrado, m => m.ConfiguracionTurno);

                if (corte == null)
                    continue;

                //var folioCorte = ServicioCortesTurno.ObtenerFolioCortePorId(grupoVenta.Key);

                var pedidos = new List<serviciosE.wsRastrilleo.MastPedido>();
                int indexPedido = 0;
                int indexPago = 0;
                try
                {
                    foreach (var venta in grupoVenta)
                    {
                        decimal porcentajeIVA = venta.IVAEnCurso;

                        indexPago = 0;

                        IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
                        Modelo.Entidades.DatosFiscales datosF;
                        string procedencia;
                        bool esMesa = false;

                        switch (venta.ClasificacionVenta)
                        {
                            case Venta.ClasificacionesVenta.Habitacion:
                                pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion, !venta.Activa);//corte.FechaCorte);
                                datosF = ServicioRentas.ObtenerDatosFiscalesPorTransaccion(venta.Transaccion);
                                procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccion(venta.Transaccion);
                                break;
                            case Venta.ClasificacionesVenta.Restaurante:
                                pagos = RepositorioPagosOcupacionesMesa.ObtenerPagosPorTransaccion(venta.Transaccion);

                                //No tiene caso que revise los consumos internos ya que van a ser descartados... Me retracto

                                if (!(esMesa = pagos.Any()))
                                {
                                    pagos = RepositorioPagosConsumosInternos.ObtenerPagosPorTransaccion(venta.Transaccion);
                                    procedencia = "";
                                }
                                else
                                {
                                    procedencia = "Mesa " + ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(venta.Transaccion);
                                }

                                datosF = ServicioRestaurantes.ObtenerDatosFiscalesPorTransaccion(venta.Transaccion);
                                break;
                            case Venta.ClasificacionesVenta.RoomService:
                                pagos = RepositorioPagosComandas.ObtenerPagosPorTransaccion(venta.Transaccion);
                                datosF = ServicioRentas.ObtenerDatosFiscalesPorTransaccion(venta.Transaccion);
                                procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(venta.Transaccion);
                                break;
                            case Venta.ClasificacionesVenta.TarjetaPuntos:
                                pagos = RepositorioPagosTarjetasPuntos.ObtenerPagosPorTransaccion(venta.Transaccion);
                                datosF = null;
                                procedencia = "";
                                break;
                            default:
                                pagos = new List<IPago>();
                                datosF = null;
                                procedencia = "";
                                break;
                        }

                        /*
                            1 Efectivo
                            2 reservaciones//vpoints
                            3 Credito
                            4 Debito
                         */

                        if (pagos.Count() == 0)
                            continue;

                        var pagosAdecuados = pagos.Where(m => m.TipoPago != TiposPago.Reservacion).Select(m=> new Tuple<bool, IPago>(false, m)).ToList();// .Where(m => m.TipoPago == TiposPago.Efectivo || m.TipoPago == TiposPago.TarjetaCredito || m.TipoPago == TiposPago.Reservacion).ToList();

                        var pagosTipoReservacion = pagos.Where(m => m.TipoPago == TiposPago.Reservacion).ToList();

                        if (pagosTipoReservacion.Count > 0)
                            pagosAdecuados.AddRange(RepositorioPagosReservaciones.ObtenerPagosPorCodigosReserva(pagosTipoReservacion.Select(m => m.Referencia).ToList()).Select(m => new Tuple<bool, IPago>(true, m)));

                        var nuevoPedido = new serviciosE.wsRastrilleo.MastPedido
                        {
                            Folio = venta.FolioTicket.ToString(),
                            Sucursal = nombreSucursal,//configuracionG.NombreSucursal,
                            NoFactura = "",
                            Fecha = venta.FechaCreacion,
                            HabitacionServicio = procedencia,
                            IdClasificacionVenta = (short)venta.IdClasificacionVenta
                        };

                        pedidos.Add(nuevoPedido);

                        if (datosF != null)
                            nuevoPedido.Cliente = new serviciosE.wsRastrilleo.Cliente
                            {
                                Calle = datosF.Calle,
                                Ciudad = datosF.Ciudad,
                                CodigoPostal = datosF.CP,
                                Colonia = datosF.Colonia,
                                Estado = datosF.Estado,
                                NoExterior = datosF.NumeroExterior,
                                NoInterior = datosF.NumeroInterior,
                                Nombre = datosF.NombreRazonSocial,
                                Pais = "",
                                RFC = datosF.RFC,
                                TipoPersona = datosF.TipoPersona == TiposPersonas.Fisica ? "F" : "M",
                                Mail = datosF.Email,
                            };

                        nuevoPedido.Pago = new serviciosE.wsRastrilleo.Pago[pagosAdecuados.Count];

                        decimal valor = 0, descuento = 0, cortesia = 0;

                        foreach (var tupla in pagosAdecuados)
                        {
                            var pago = tupla.Item2;

                            int idTipoPago;
                            string numeroCuenta;
                            int idTipoTarjeta;

                            switch (pago.TipoPago)
                            {
                                case TiposPago.Efectivo:
                                    valor += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Efectivo;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Reservacion:
                                    valor += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Reservacion;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.TarjetaCredito:
                                    valor += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Tarjeta;
                                    numeroCuenta = pago.NumeroTarjeta;
                                    idTipoTarjeta = pago.IdTipoTarjeta.Value;
                                    break;
                                case TiposPago.Consumo:
                                    descuento += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Consumo;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Cortesia:
                                    cortesia += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Cortesia;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Cupon:
                                    cortesia += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Cupon;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.VPoints:
                                    descuento += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.VPoints;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Transferencia:
                                    valor += pago.Valor;
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Paypal;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                                default:
                                    idTipoPago = 0;
                                    numeroCuenta = "";
                                    idTipoTarjeta = 0;
                                    break;
                            }

#warning cambiar
                            nuevoPedido.Pago[indexPago] = new serviciosE.wsRastrilleo.Pago
                            {
                                Total = pago.Valor,
                                NoDeTransaccion = pago.Transaccion,
                                TipoPago = idTipoPago,
                                TipoTarjeta = idTipoTarjeta,
                                EsReserva = tupla.Item1,
                                NoDeCuenta = numeroCuenta
                            };

                            indexPago++;
                        }

                        nuevoPedido.Descuento = descuento;
                        nuevoPedido.Cortesia = cortesia;

                        var detallesPedido = new List<serviciosE.wsRastrilleo.DetPedido>();
                        var empleados = new List<DtoEmpleadoPuesto>();

                        switch (venta.ClasificacionVenta)
                        {
                            case Venta.ClasificacionesVenta.Habitacion:
                                {
                                    #region detalles de habitación

                                    var ventaRenta = RepositorioPagosRenta.ObtenerVentaConConceptos(venta.Transaccion, venta.Cancelada);
                                    empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ventaRenta.IdValet ?? 0, ventaRenta.IdUsuarioModifico);

#warning oojo aqui, se le quita un segundo a la fecha porque el filtro valida que los detalles tengan fecha de cancelación mayor que la fecha proporcionada, lo que es bueno cuando la fecha corresponde con el cierre de un turno, pero es malo cuando es la fecha de cancelación de la venta
                                    var detalles = ServicioRentas.ObtenerDetallesPorTransaccion(venta.Transaccion, venta.FechaCancelacion.HasValue ? venta.FechaCancelacion.Value.AddSeconds(-1) : venta.FechaCancelacion);

                                    nuevoPedido.ValorNoReembolsable = ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo || (venta.Cancelada && m.FechaEliminacion >= ventaRenta.FechaEliminacion)).Sum(m=> m.ValorConIVA);

                                    if (ventaRenta.EsInicial)
                                    {
                                        var detalleH = detalles.FirstOrDefault(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion);

                                        var precio = Math.Round(detalleH != null ? (detalleH.ValorSinIVA / detalleH.Cantidad) : ventaRenta.PrecioHotelTmp, 2);
                                        var iva = Math.Round(detalleH != null ? (detalleH.ValorIVA / detalleH.Cantidad) : (ventaRenta.PrecioHotelTmp * porcentajeIVA), 2);
                                        var total = Math.Round(detalleH != null ? (detalleH.ValorConIVA / detalleH.Cantidad) : ventaRenta.PrecioHotelTmp * (1 + porcentajeIVA), 2);

                                        detallesPedido.Add(new serviciosE.wsRastrilleo.DetPedido
                                        {
                                            Cantidad = 1,
                                            Codigo = ventaRenta.ClaveTipoHabitacionTmp,
                                            Descripcion = "Habitación",
                                            Precio = precio,
                                            IVA = iva,
                                            Total = total,
                                            ClaveProdServ = ventaRenta.ClaveSATTipoHabitacionTmp,
                                            ClaveUnidad = "",
                                            Unidad = "",
                                            Servicio = ventaRenta.ClaveTipoHabitacionTmp
                                        });
                                    }
                                    foreach (var grupoHE in ventaRenta.Extensiones.Where(m => (m.Activa || (venta.Cancelada  && m.FechaEliminacion >= ventaRenta.FechaEliminacion)) && !m.EsRenovacion).GroupBy(m => m.Precio))
                                    {
                                        detallesPedido.Add(new serviciosE.wsRastrilleo.DetPedido
                                        {
                                            Cantidad = grupoHE.Count(),
                                            Codigo = "HE",//"HrsE" + grupoHE.Key,
                                            Descripcion = "Tiempo extra",
                                            Precio = grupoHE.Key,
                                            IVA = grupoHE.Key * porcentajeIVA,
                                            Total = Math.Round(grupoHE.Key * (1 + porcentajeIVA), 2) * grupoHE.Count(),
                                            ClaveUnidad = "",
                                            Unidad = "",
                                            Servicio = "HE",
                                            ClaveProdServ = ventaRenta.ClaveSATTipoHabitacionTmp,
                                        });
                                    }

                                    foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => (m.Activa || (venta.Cancelada && m.FechaEliminacion >= ventaRenta.FechaEliminacion))).GroupBy(m => m.Precio))
                                    {
                                        detallesPedido.Add(new serviciosE.wsRastrilleo.DetPedido
                                        {
                                            Cantidad = grupoPE.Count(),
                                            Codigo = "PE",//"PE" + grupoPE.Key,
                                            Descripcion = "Persona extra",
                                            Precio = grupoPE.Key,
                                            IVA = grupoPE.Key * porcentajeIVA,
                                            Total = Math.Round(grupoPE.Key * (1 + porcentajeIVA), 2) * grupoPE.Count(),
                                            ClaveUnidad = "",
                                            Unidad = "",
                                            Servicio = "PE",
                                            ClaveProdServ = ventaRenta.ClaveSATTipoHabitacionTmp,
                                        });
                                    }

                                    foreach (var grupoRen in ventaRenta.Extensiones.Where(m => (m.Activa || (venta.Cancelada && m.FechaEliminacion >= ventaRenta.FechaEliminacion)) && m.EsRenovacion).GroupBy(m => m.Precio))
                                    {
                                        detallesPedido.Add(new serviciosE.wsRastrilleo.DetPedido
                                        {
                                            Cantidad = grupoRen.Count(),
                                            Codigo = "HE",//"REN" + grupoRen.Key,
                                            Descripcion = "Renovación",
                                            Precio = grupoRen.Key,
                                            IVA = grupoRen.Key * porcentajeIVA,
                                            Total = Math.Round(grupoRen.Key * (1 + porcentajeIVA), 2) * grupoRen.Count(),
                                            ClaveUnidad = "",
                                            Unidad = "",
                                            Servicio = "HE",
                                            ClaveProdServ = ventaRenta.ClaveSATTipoHabitacionTmp,
                                        });
                                    }

                                    foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || (venta.Cancelada && m.FechaEliminacion >= ventaRenta.FechaEliminacion)) && m.Precio > 0).GroupBy(m => new { m.IdPaquete, m.Precio }))
                                    {
                                        var muestra = grupoPAQ.First();

                                        detallesPedido.Add(new serviciosE.wsRastrilleo.DetPedido
                                        {
                                            Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                                            ClaveProdServ = muestra.Paquete.ClaveSAT,
                                            Codigo = muestra.Paquete.Clave,
                                            Descripcion = muestra.Paquete.Nombre,
                                            Precio = grupoPAQ.Key.Precio,
                                            IVA = grupoPAQ.Key.Precio * porcentajeIVA,
                                            Total = Math.Round(grupoPAQ.Key.Precio * (1 + porcentajeIVA), 2) * grupoPAQ.Sum(m => m.Cantidad),
                                            ClaveUnidad = "",
                                            Unidad = "",
                                            Servicio = muestra.Paquete.Clave,
                                            ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasHabitacion.PAQUETES
                                        });
                                    }

                                    foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => (m.Activo || (venta.Cancelada && m.FechaEliminacion >= ventaRenta.FechaEliminacion)) && m.Descuento > 0).GroupBy(m => new { m.IdPaquete, m.Descuento }))
                                    {
                                        var muestra = grupoPAQ.First();

                                        detallesPedido.Add(new serviciosE.wsRastrilleo.DetPedido
                                        {
                                            Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                                            ClaveProdServ = muestra.Paquete.ClaveSAT,
                                            Codigo = muestra.Paquete.Clave,
                                            Descripcion = muestra.Paquete.Nombre,
                                            Precio = -grupoPAQ.Key.Descuento,
                                            IVA = -grupoPAQ.Key.Descuento * porcentajeIVA,
                                            Total = Math.Round(-grupoPAQ.Key.Descuento * (1 + porcentajeIVA), 2) * grupoPAQ.Sum(m => m.Cantidad),
                                            ClaveUnidad = "",
                                            Unidad = "",
                                            Servicio = muestra.Paquete.Clave,
                                            ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasHabitacion.DESCUENTOS
                                        });
                                    }

                                    //foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => m.Activo && m.Descuento > 0).GroupBy(m => m.IdPaquete))
                                    //{
                                    //    var muestra = grupoPAQ.First();
                                    //    nuevoPedido.Descuento += Math.Round(muestra.Paquete.Descuento * (1 + porcentajeIVA), 2) * grupoPAQ.Sum(m => m.Cantidad);
                                    //}


                                    #endregion
                                }
                                break;
                            case Venta.ClasificacionesVenta.Restaurante:
                                {
                                    #region detalles de restaurante

                                    if (esMesa)
                                    {
                                        var ocupacion = RepositorioOcupacionesMesa.ObtenerOcupacionFinalizadaConDetalles(((PagoOcupacionMesa)pagosAdecuados.First().Item2).IdOcupacionMesa);

                                        empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ocupacion.IdMesero, ocupacion.IdUsuarioModifico);

                                        var idsArticulos = ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

                                        var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                                        foreach (var artCom in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante))
                                        {
                                            artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                                        }

                                        foreach (var grupoArticulo in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).GroupBy(m => new { m.IdArticulo, m.PrecioUnidadFinal }))
                                        {
                                            var muestra = grupoArticulo.First();

                                            var det = new serviciosE.wsRastrilleo.DetPedido
                                            {
                                                Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                                Codigo = muestra.ArticuloTmp.Cod_Art,
                                                Descripcion = muestra.ArticuloTmp.Desc_Art,
                                                Precio = muestra.PrecioUnidad,
                                                IVA = muestra.PrecioUnidadFinal - muestra.PrecioUnidad,//muestra.PrecioSinIVA * configuracionG.Iva_CatPar,
                                                Total = muestra.PrecioUnidadFinal * grupoArticulo.Sum(m => m.Cantidad),//muestra.PrecioSinIVA * grupoArticulo.Sum(m => m.Cantidad) * (1 + configuracionG.Iva_CatPar),
                                                ClaveProdServ = muestra.ArticuloTmp.CodigoSAT ?? "",
                                                Unidad = muestra.ArticuloTmp.ZctCatPresentacion != null ?
                                                         muestra.ArticuloTmp.ZctCatPresentacion.presentacion : "",
                                                ClaveUnidad = muestra.ArticuloTmp.ZctCatPresentacion != null ?
                                                              (muestra.ArticuloTmp.ZctCatPresentacion.CodigoSAT ?? "") : "",
                                                Servicio = "R",
                                            };



                                            if (muestra.ArticuloTmp.ZctCatLinea.LineaBase.HasValue)
                                            {
                                                switch (muestra.ArticuloTmp.ZctCatLinea.LineaBase.Value)
                                                {
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos:
                                                        det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.ALIMENTOS;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas:
                                                        det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.BEBIDAS;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex:
                                                        det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.SEX_AND_SPA;
                                                        break;
                                                }
                                            }

                                            detallesPedido.Add(det);
                                        }
                                    }
                                    else 
                                    {
                                        var consumoInterno = RepositorioConsumosInternos.ObtenerConsumoInternoEntregadoConDetalles(((PagoConsumoInterno)pagos.First()).IdConsumoInterno);

                                        empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(consumoInterno.IdMesero ?? 0, consumoInterno.IdUsuarioModifico);

                                        var idsArticulos = consumoInterno.ArticulosConsumoInterno.Select(m => m.IdArticulo).ToList();

                                        var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                                        foreach (var artCom in consumoInterno.ArticulosConsumoInterno)
                                        {
                                            artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                                        }

                                        foreach (var grupoArticulo in consumoInterno.ArticulosConsumoInterno.GroupBy(m => m.IdArticulo))
                                        {
                                            var muestra = grupoArticulo.First();

                                            var det = new serviciosE.wsRastrilleo.DetPedido
                                            {
                                                Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                                Codigo = muestra.ArticuloTmp.Cod_Art,
                                                Descripcion = muestra.ArticuloTmp.Desc_Art,
                                                Precio = muestra.PrecioUnidad,
                                                IVA = muestra.PrecioUnidadFinal - muestra.PrecioUnidad,//muestra.PrecioSinIVA * configuracionG.Iva_CatPar,
                                                Total = muestra.PrecioUnidadFinal * grupoArticulo.Sum(m => m.Cantidad),//muestra.PrecioSinIVA * grupoArticulo.Sum(m => m.Cantidad) * (1 + configuracionG.Iva_CatPar),
                                                ClaveProdServ = muestra.ArticuloTmp.CodigoSAT ?? "",
                                                Unidad = muestra.ArticuloTmp.ZctCatPresentacion != null ?
                                                         muestra.ArticuloTmp.ZctCatPresentacion.presentacion : "",
                                                ClaveUnidad = muestra.ArticuloTmp.ZctCatPresentacion != null ?
                                                              (muestra.ArticuloTmp.ZctCatPresentacion.CodigoSAT ?? "") : "",
                                                Servicio = "R",
                                            };

                                            if (muestra.ArticuloTmp.ZctCatLinea.LineaBase.HasValue)
                                            {
                                                switch (muestra.ArticuloTmp.ZctCatLinea.LineaBase.Value)
                                                {
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos:
                                                        det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.ALIMENTOS;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas:
                                                        det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.BEBIDAS;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex:
                                                        det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.SEX_AND_SPA;
                                                        break;
                                                }
                                            }

                                            detallesPedido.Add(det);
                                        }
                                    }
                                    #endregion
                                }
                                break;
                            case Venta.ClasificacionesVenta.RoomService:
                                {
                                    #region detalles de comanda

                                    var comanda = RepositorioComandas.ObtenerComandaPagadaConDetalles(((PagoComanda)pagosAdecuados.First().Item2).IdComanda);

                                    empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

                                    var idsArticulos = comanda.ArticulosComanda.Select(m => m.IdArticulo).ToList();

                                    var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

                                    foreach (var artCom in comanda.ArticulosComanda)
                                    {
                                        artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                                    }

                                    foreach (var grupoArticulo in comanda.ArticulosComanda.GroupBy(m => new { m.IdArticulo, m.PrecioUnidadFinal }))
                                    {
                                        var muestra = grupoArticulo.First();

                                        var det = new serviciosE.wsRastrilleo.DetPedido
                                        {
                                            Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                            Codigo = muestra.ArticuloTmp.Cod_Art,
                                            Descripcion = muestra.ArticuloTmp.Desc_Art,
                                            Precio = muestra.PrecioUnidad,
                                            IVA = muestra.PrecioUnidadFinal - muestra.PrecioUnidad,//articuloComanda.PrecioSinIVA * configuracionG.Iva_CatPar,
                                            Total = muestra.PrecioUnidadFinal * grupoArticulo.Sum(m => m.Cantidad),// * (1 + configuracionG.Iva_CatPar),
                                            ClaveProdServ = muestra.ArticuloTmp.CodigoSAT ?? "",
                                            Unidad = muestra.ArticuloTmp.ZctCatPresentacion != null ?
                                                     muestra.ArticuloTmp.ZctCatPresentacion.presentacion : "",
                                            ClaveUnidad = muestra.ArticuloTmp.ZctCatPresentacion != null ?
                                                          (muestra.ArticuloTmp.ZctCatPresentacion.CodigoSAT ?? "") : "",
                                            Servicio = "RS"
                                        };

                                        if (muestra.ArticuloTmp.ZctCatLinea.LineaBase.HasValue)
                                        {
                                            switch (muestra.ArticuloTmp.ZctCatLinea.LineaBase.Value)
                                            {
                                                case Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos:
                                                    det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.ALIMENTOS;
                                                    break;
                                                case Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas:
                                                    det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.BEBIDAS;
                                                    break;
                                                case Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex:
                                                    det.ClaveSubCategoria = serviciosE.Constantes.Rastrilleo.ConstantesSubcategoriasArticulos.SEX_AND_SPA;
                                                    break;
                                            }
                                        }

                                        detallesPedido.Add(det);
                                    }

                                    #endregion
                                }
                                break;
                            case Venta.ClasificacionesVenta.TarjetaPuntos:
                                {
                                    #region detalles de tarjeta de puntos

                                    var idTarjeta = ((PagoTarjetaPuntos)pagosAdecuados.First().Item2).IdTarjetaPuntos;

                                    var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Id == idTarjeta);

                                    detallesPedido.Add(new serviciosE.wsRastrilleo.DetPedido
                                    {
                                        Cantidad = 1,
                                        Codigo = "",
                                        Descripcion = "Tarjeta de puntos",
                                        Precio = pagosAdecuados.Sum(m=> m.Item2.Valor) / (1 + porcentajeIVA), //tarjeta.Precio,
                                        IVA = (pagosAdecuados.Sum(m=> m.Item2.Valor) / (1 + porcentajeIVA)) * porcentajeIVA,//tarjeta.Precio * porcentajeIVA,
                                        Total = pagosAdecuados.Sum(m=> m.Item2.Valor),// Math.Round(tarjeta.Precio * (1 + porcentajeIVA), 2)
                                        ClaveUnidad = "",
                                        Unidad = "",
                                        ClaveProdServ = ""
                                    });

                                    empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(tarjeta.IdUsuarioModifico);

                                    #endregion
                                }
                                break;
                            default:
                                {
                                }
                                break;
                        }

                        nuevoPedido.DetPedido = detallesPedido.ToArray();
                        nuevoPedido.Involucrado = new Involucrados[empleados.Count];

                        for (int i = 0; i < empleados.Count; i++)
                        {
                            nuevoPedido.Involucrado[i] = new Involucrados
                            {
                                Involucrado = string.Join(" - ", empleados[i].Puesto, empleados[i].NombreEmpleado)
                            };
                        }

                        indexPedido++;
                    }

                    //string json = JsonConvert.SerializeObject(pedidos, Formatting.Indented);

                    var resultado = EnviarOrden(corte.NumeroCorte.ToString(), pedidos.ToArray());

                    var itemGeneral = resultado.FirstOrDefault(m => m.Folio.ToUpper().Contains("GENERAL"));

                    foreach (var venta in grupoVenta)
                    {
                        var itemResultado = resultado.FirstOrDefault(m => m.IdClasificacionVenta == venta.IdClasificacionVenta && m.Folio == venta.FolioTicket.ToString());

                        if (itemResultado == null && pedidos.Any(m => m.IdClasificacionVenta == venta.IdClasificacionVenta && m.Folio == venta.FolioTicket.ToString()))
                        {
                            if (itemGeneral != null)
                            {
                                venta.Subida = false;
                                venta.ErrorUltimoIntento = itemGeneral.Respuesta;
                                venta.EsErrorSubida = false;
                            }
                            else
                            {
                                venta.Subida = false;
                                venta.ErrorUltimoIntento = "DESCONOCIDO";
                                venta.EsErrorSubida = false;
                            }
                        }
                        else 
                        {
                            if (!pedidos.Any(m => m.IdClasificacionVenta == venta.IdClasificacionVenta && m.Folio == venta.FolioTicket.ToString()) || itemResultado.Respuesta.ToUpper().Contains("EXITO") || itemResultado.Respuesta.ToUpper().Contains("ÉXITO"))
                            {
                                venta.Subida = true;
                                venta.ErrorUltimoIntento = null;
                                venta.EsErrorSubida = false;
                            }
                            else
                            {
                                venta.Subida = false;
                                venta.ErrorUltimoIntento = itemResultado.Respuesta;
                                venta.EsErrorSubida = false;
                            }
                        }

                        RepositorioVentas.Modificar(venta);
                    }

                    RepositorioVentas.GuardarCambios();
                }
                catch (Exception ex)
                {
                    foreach(var venta in grupoVenta)
                    {
                        venta.Subida = false;
                        venta.ErrorUltimoIntento = ex.Message;
                        venta.EsErrorSubida = true;

                        RepositorioVentas.Modificar(venta);
                    }

                    RepositorioVentas.GuardarCambios();
                }
            }
        }
    }
}
