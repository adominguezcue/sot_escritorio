﻿using Modelo;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System.Collections.Generic;

namespace Negocio.ConceptosSistema
{
    public interface IServicioConceptosSistema
    {
        /// <summary>
        /// Retorna los conceptos activos en el sistema cargados con sus tipos
        /// </summary>
        /// <returns></returns>
        List<ConceptoSistema> ObtenerConceptos(DtoUsuario usuario);
        /// <summary>
        /// Retorna los tipos de conceptos activos en el sistema
        /// </summary>
        /// <returns></returns>
        List<ConceptoSistema.TiposConceptos> ObtenerTiposConcepto();
        /// <summary>
        /// Permite crear un nuevo concepto
        /// </summary>
        /// <param name="conceptoSistema"></param>
        /// <param name="usuario"></param>
        void CrearConcepto(ConceptoSistema conceptoSistema, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar un concepto
        /// </summary>
        /// <param name="conceptoSistema"></param>
        /// <param name="usuario"></param>
        void ModificarConcepto(ConceptoSistema conceptoSistema, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar un contexto
        /// </summary>
        /// <param name="idConceptoSistema"></param>
        /// <param name="usuario"></param>
        void EliminarConcepto(int idConceptoSistema, DtoUsuario usuario);
        /// <summary>
        /// Retorna los conceptos de sistema que posean el tipo especificado y que se encuentren activos
        /// </summary>
        /// <param name="tipoConcepto"></param>
        /// <returns></returns>
        List<ConceptoSistema> ObtenerConceptosPorTipoFILTRO(ConceptoSistema.TiposConceptos tipoConcepto);
        ///// <summary>
        ///// Retorna el concepto activo que coincide con el id proporcionado y el tipo, o null en caso
        ///// de no existir coincidencias
        ///// </summary>
        ///// <param name="idConcepto"></param>
        ///// <param name="tipoConcepto"></param>
        ///// <returns></returns>
        //ConceptoSistema ObtenerConcepto(int idConcepto, ConceptoSistema.TiposConceptos tipoConcepto);
        /// <summary>
        /// Verifica que exista un concepto con el id proporcionado y pertenezca al tipo espscificado
        /// </summary>
        /// <param name="idConcepto"></param>
        /// <param name="tipoConcepto"></param>
        /// <returns></returns>
        bool VerificarConceptoPorTipo(int idConcepto, ConceptoSistema.TiposConceptos tipoConcepto);
    }
}