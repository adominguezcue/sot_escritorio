﻿using Modelo.Entidades;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Modelo;
using Transversal.Excepciones;
using Modelo.Seguridad.Dtos;
using Transversal.Extensiones;
using Negocio.Seguridad.Permisos;

namespace Negocio.ConceptosSistema
{
    public class ServicioConceptosSistema : IServicioConceptosSistema
    {
        IRepositorioConceptosSistema RepositorioConceptosSistema
        {
            get { return _repositorioConceptosSistema.Value; }
        }

        Lazy<IRepositorioConceptosSistema> _repositorioConceptosSistema = new Lazy<IRepositorioConceptosSistema>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConceptosSistema>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        //IRepositorioTiposConcepto RepositorioTiposConcepto
        //{
        //    get { return _repositorioTiposConcepto.Value; }
        //}

        //Lazy<IRepositorioTiposConcepto> _repositorioTiposConcepto = new Lazy<IRepositorioTiposConcepto>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTiposConcepto>(); });


        public List<ConceptoSistema> ObtenerConceptos(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarConceptosSistema = true });

            return RepositorioConceptosSistema.ObtenerElementos(m => m.Activo).ToList();
        }

        public List<ConceptoSistema.TiposConceptos> ObtenerTiposConcepto()
        {
            return EnumExtensions.ComoLista<ConceptoSistema.TiposConceptos>();
        }

        public void CrearConcepto(ConceptoSistema conceptoSistema, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearConceptosSistema = true });

            ValidarConcepto(conceptoSistema);

            var fechaActual = DateTime.Now;

            conceptoSistema.Activo = true;
            conceptoSistema.FechaCreacion = fechaActual;
            conceptoSistema.FechaModificacion = fechaActual;
            conceptoSistema.IdUsuarioCreo = usuario.Id;
            conceptoSistema.IdUsuarioModifico = usuario.Id;

            RepositorioConceptosSistema.Agregar(conceptoSistema);
            RepositorioConceptosSistema.GuardarCambios();
        }

        public void ModificarConcepto(ConceptoSistema conceptoSistema, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarConceptosSistema = true });

            ValidarConcepto(conceptoSistema);
            ValidarRelaciones(conceptoSistema);

            var fechaActual = DateTime.Now;

            conceptoSistema.FechaModificacion = fechaActual;
            conceptoSistema.IdUsuarioModifico = usuario.Id;

            RepositorioConceptosSistema.Modificar(conceptoSistema);
            RepositorioConceptosSistema.GuardarCambios();
        }

        public void EliminarConcepto(int idConceptoSistema, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarConceptosSistema = true });

            var conceptoSistema = RepositorioConceptosSistema.Obtener(m => m.Activo && m.Id == idConceptoSistema);

            if (conceptoSistema == null)
                throw new SOTException(Recursos.ConceptosSistema.concepto_nulo_excepcion);

            ValidarRelaciones(conceptoSistema);

            var fechaActual = DateTime.Now;

            conceptoSistema.Activo = false;
            conceptoSistema.FechaModificacion = fechaActual;
            conceptoSistema.FechaEliminacion = fechaActual;
            conceptoSistema.IdUsuarioModifico = usuario.Id;
            conceptoSistema.IdUsuarioElimino = usuario.Id;

            RepositorioConceptosSistema.Modificar(conceptoSistema);
            RepositorioConceptosSistema.GuardarCambios();
        }

        private void ValidarRelaciones(ConceptoSistema conceptoSistema)
        {
#warning de momento no hace nada
        }

        private void ValidarConcepto(ConceptoSistema conceptoSistema)
        {
            if (conceptoSistema == null)
                throw new SOTException(Recursos.ConceptosSistema.concepto_nulo_excepcion);

            if (string.IsNullOrWhiteSpace(conceptoSistema.Concepto))
                throw new SOTException(Recursos.ConceptosSistema.concepto_vacio_excepcion);

            if (RepositorioConceptosSistema.Alguno(m => m.Activo && 
                                                        m.Concepto.Trim().ToUpper().Equals(conceptoSistema.Concepto.Trim().ToUpper()) && 
                                                        m.Id != conceptoSistema.Id))
                throw new SOTException(Recursos.ConceptosSistema.concepto_repetido_excepcion, conceptoSistema.Concepto);

            if (!ObtenerTiposConcepto().Any(m => m == conceptoSistema.TipoConcepto))
                throw new SOTException(Recursos.ConceptosSistema.tipo_invalido_excepcion);
        }


        public List<ConceptoSistema> ObtenerConceptosPorTipoFILTRO(ConceptoSistema.TiposConceptos tipoConcepto)
        {
            int idTipoC = (int)tipoConcepto;

            return RepositorioConceptosSistema.ObtenerElementos(m => m.Activo && m.IdTipoConcepto == idTipoC).ToList();
        }


        //public ConceptoSistema ObtenerConcepto(int idConcepto, ConceptoSistema.TiposConceptos tipoConcepto)
        //{
        //    int idTipoC = (int)tipoConcepto;

        //    return RepositorioConceptosSistema.Obtener(m => m.Id == idConcepto && m.IdTipoConcepto == idTipoC);
        //}

        public bool VerificarConceptoPorTipo(int idConcepto, ConceptoSistema.TiposConceptos tipoConcepto)
        {
            int idTipoC = (int)tipoConcepto;

            return RepositorioConceptosSistema.Alguno(m => m.Activo && m.IdTipoConcepto == idTipoC && m.Id == idConcepto);
        }
    }
}
