﻿using Dominio.Nucleo.Entidades;
using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.CortesTurno
{
    public interface IServicioCortesTurno
    {
        //int ObtenerSiguienteNumeroCorte();

        //DateTime? ObtenerFechaUltimoCorte();

        /// <summary>
        /// permite realizar el corte de turno
        /// </summary>
        /// <param name="usuario"></param>
        void RealizarCorteTurno(DtoUsuario usuario);
        CorteTurno RevisarCorteTurno(int idCorteTurno, DtoUsuario usuario);
        CorteTurno ObtenerDatosUltimoCorteTurno(DtoUsuario usuario);
        void CerrarTurno(int idCorteTurno, DtoUsuario usuario);
        /// <summary>
        /// Retorna la información del último corte de turno realizado
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        CorteTurno ObtenerUltimoCorte();
        /// <summary>
        /// Retorna un resumen con la información de las propinas de los meseros dentro del lapso especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        DtoComisionesMesero ObtenerComisionesMeseros(DtoUsuario usuario, int idCorte);
        /// <summary>
        /// Retorna un resumen con la información de las propinas de los valets dentro del lapso especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        DtoComisionesValets ObtenerComisionesValets(DtoUsuario usuario, int idCorte);
        /// <summary>
        /// Retorna un resumen de los movimientos realizados en las habitaciones dentro del período especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="formasPago"></param>
        /// <param name="soloConPropina"></param>
        /// <returns></returns>
        List<DtoPagoHabitacionCorte> ObtenerMovimientosHabitaciones(DtoUsuario usuario, DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago = null, bool soloConPropina = false);
        /// <summary>
        /// Retorna un resumen de los movimientos realizados en los restaurantes dentro del período especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<DtoPagoRestauranteCorte> ObtenerMovimientosRestaurante(DtoUsuario usuario, DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago = null, bool soloConPropina = false);
        bool VerificarExistenTurnos(DtoUsuario usuario);

        /// <summary>
        /// Retorna un resumen de los movimientos de room service dentro del período especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="formasPago"></param>
        /// <param name="soloConPropina"></param>
        /// <returns></returns>
        List<DtoPagoRestauranteCorte> ObtenerMovimientosRoomService(DtoUsuario usuario, DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago = null, bool soloConPropina = false);
        /// <summary>
        /// Obtiene el reporte contable con los datos dentro de las fechas de los cortes especificados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="corteInicio"></param>
        /// <param name="corteFin"></param>
        /// <returns></returns>
        DtoReporteContable ObtenerReporteContablePorRangoCortes(DtoUsuario usuario, int corteInicio, int corteFin);
        DtoReporteCierre ObtenerReporteCierre(DtoUsuario usuario, int folioCorte);
        //DtoReporteCierre ObtenerDatosRevisionPorFolio(DtoUsuario usuario, int folioCorte);
        //DtoReporteCierre ObtenerDatosRevisionPorId(DtoUsuario usuario, int idCorte);
        ///// <summary>
        ///// Retorna un conjunto de resúmenes de los cortes realizados dentro del rango de fechas especificado
        ///// </summary>
        ///// <param name="fechaInicial"></param>
        ///// <param name="fechaFinal"></param>
        ///// <param name="usuario"></param>
        ///// <returns></returns>
        //List<DtoCorteTurno> ObtenerResumenesCortesFinalizadosPorRango(DateTime? fechaInicial, DateTime? fechaFinal, DtoUsuario usuario);
        /// <summary>
        /// Retorna el número del corte que posee el id porporcionado
        /// </summary>
        /// <param name="idCorte"></param>
        /// <returns></returns>
        /// 
        string ObtieneSiglaTurno(int idconfigTurno);
        string UsuarioCerroReporte(int idUsuariocerro);													   
        int ObtenerFolioCortePorId(int idCorte);
        /// <summary>
        /// Retorna la configuración del turno con el orden superior inmediato al proporcionado, o el
        /// de orden menor en caso de que no exista ninguno mayor
        /// </summary>
        /// <param name="ordenBase"></param>
        /// <returns></returns>
        ConfiguracionTurno ObtenerSiguienteConfiguracionTurno(int ordenBase);
        /// <summary>
        /// Permite abrir el primer turno del sistema
        /// </summary>
        /// <param name="idConfiguracionTurno"></param>
        /// <param name="usuarioActual"></param>
        /// <param name="fechaInicial"></param>
        void AbrirPrimerTurno(int idConfiguracionTurno, DtoUsuario usuario, DateTime? fechaInicial);

        /// <summary>
        /// Retorna los cortes de turnos cerrados que no han sido sincronizados con el servidor
        /// </summary>
        /// <returns></returns>
        List<CorteTurno> ObtenerCortesNoSincronizados();

        CorteTurno ObtenerCorteEnRevision(int idCorteTurno);
        /// <summary>
        /// Retorna un resumen de los cortes en revisión, los datos que se incluyen son el id, el número de corte y el rango de fechas en las que el corte estuvo vigente
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<DtoResumenCorteTurno> ObtenerResumenesCortesEnRevision(DtoUsuario usuario);

        //DtoReporteContable ObtenerReporteContableTurnoActual(DtoUsuario UsuarioActual);

        List<DtoAreas> ObtieneAreas(DtoUsuario usuario);														
        List<DtoCorteTurno> ObtenerResumenesCortesPorRango(DateTime? fechaInicial, DateTime? fechaFinal, DtoUsuario usuario);
    }
}
