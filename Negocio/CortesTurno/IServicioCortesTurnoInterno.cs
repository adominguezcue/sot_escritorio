﻿using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.CortesTurno
{
    internal interface IServicioCortesTurnoInterno : IServicioCortesTurno
    {
        decimal ObtenerPropinasPagadasMeseros(CorteTurno corte);

        decimal ObtenerPropinasPagadasValets(CorteTurno corte);


        DtoTotalesPropinas ObtenerPagoNetoPropinasMeseros(CorteTurno corte);

        DtoTotalesPropinas ObtenerPagoNetoPropinasValets(CorteTurno corte);

        string ObtenerNombreTurno(int idCorteTurno);
        /// <summary>
        /// Retorna el corte de turno cuyo rango incluya la fecha proporcionada
        /// </summary>
        /// <param name="fechaFiltroTurno"></param>
        /// <returns></returns>
        CorteTurno ObtenerCorteIncluyeFecha(DateTime fechaFiltroTurno);
        bool VerificarExistenTurnos();
    }
}
