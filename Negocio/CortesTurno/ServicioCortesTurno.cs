﻿using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.ConfiguracionesGlobales;
using Negocio.ConfiguracionesPropinas;
using Negocio.Empleados;
using Negocio.Seguridad.Permisos;
using Negocio.Propinas;
using Negocio.Rentas;
using Negocio.Reservaciones;
using Negocio.Restaurantes;
using Negocio.Taxis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Negocio.Fajillas;
using System.Transactions;
using Negocio.Seguridad.Usuarios;
using Negocio.Gastos;
using Negocio.Almacen.Lineas;
using Negocio.Pagos;
using Negocio.Incidencias;
using Dominio.Nucleo.Entidades;
using Negocio.ConsumosInternos;
using Negocio.Almacen.Parametros;
using Negocio.VPoints;
using Negocio.RoomServices;
using Negocio.Ventas;
using Negocio.Sistemas.ConfiguracionesSistemas;
using Modelo.Sistemas.Constantes;
using Modelo.Sistemas.Entidades.Dtos;

namespace Negocio.CortesTurno
{
    public class ServicioCortesTurno : IServicioCortesTurnoInterno
    {
        #region dependencias
        private IServicioConfiguracionesSistemas AplicacionServicioConfiguracionesSistemas
        {
            get { return _aplicacionServicioConfiguracionesSistemas.Value; }
        }

        private Lazy<IServicioConfiguracionesSistemas> _aplicacionServicioConfiguracionesSistemas = new Lazy<IServicioConfiguracionesSistemas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesSistemas>();
        });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioConfiguracionesPropinasInterno ServicioConfiguracionesPropinas
        {
            get { return _servicioConfiguracionesPropinas.Value; }
        }

        Lazy<IServicioConfiguracionesPropinasInterno> _servicioConfiguracionesPropinas = new Lazy<IServicioConfiguracionesPropinasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesPropinasInterno>(); });

        IServicioPropinasInterno ServicioPropinas
        {
            get { return _servicioPropinas.Value; }
        }

        Lazy<IServicioPropinasInterno> _servicioPropinas = new Lazy<IServicioPropinasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPropinasInterno>(); });

        IServicioPagos ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagos> _servicioPagos = new Lazy<IServicioPagos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagos>(); });

        IServicioGastos ServicioGastos
        {
            get { return _servicioGastos.Value; }
        }

        Lazy<IServicioGastos> _servicioGastos = new Lazy<IServicioGastos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioGastos>(); });

        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });

        IServicioFajillasInterno ServicioFajillas
        {
            get { return _servicioFajillas.Value; }
        }

        Lazy<IServicioFajillasInterno> _servicioFajillas = new Lazy<IServicioFajillasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioFajillasInterno>(); });

        IServicioVentasInterno ServicioVentas
        {
            get { return _servicioVentas.Value; }
        }

        Lazy<IServicioVentasInterno> _servicioVentas = new Lazy<IServicioVentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVentasInterno>(); });


        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioLineas ServicioLineas
        {
            get { return _servicioLineas.Value; }
        }

        Lazy<IServicioLineas> _servicioLineas = new Lazy<IServicioLineas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioLineas>(); });


        IServicioReservacionesInterno ServicioReservaciones
        {
            get { return _servicioReservaciones.Value; }
        }

        Lazy<IServicioReservacionesInterno> _servicioReservaciones = new Lazy<IServicioReservacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioReservacionesInterno>(); });

        IServicioTaxisInterno ServicioTaxis
        {
            get { return _servicioTaxis.Value; }
        }

        Lazy<IServicioTaxisInterno> _servicioTaxis = new Lazy<IServicioTaxisInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTaxisInterno>(); });

        IServicioVPointsInterno ServicioVPoints
        {
            get { return _servicioVPoints.Value; }
        }

        Lazy<IServicioVPointsInterno> _servicioVPoints = new Lazy<IServicioVPointsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVPointsInterno>(); });

        IServicioIncidenciasTurnoInterno ServicioIncidenciasTurno
        {
            get { return _servicioIncidenciasTurno.Value; }
        }

        Lazy<IServicioIncidenciasTurnoInterno> _servicioIncidenciasTurno = new Lazy<IServicioIncidenciasTurnoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioIncidenciasTurnoInterno>(); });

        IServicioRentasInterno ServicioRentas
        {
            get { return _servicioRentas.Value; }
        }

        Lazy<IServicioRentasInterno> _servicioRentas = new Lazy<IServicioRentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRentasInterno>(); });

        IServicioRoomServicesInterno ServicioRoomServices
        {
            get { return _servicioRoomServices.Value; }
        }

        Lazy<IServicioRoomServicesInterno> _servicioRoomServices = new Lazy<IServicioRoomServicesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRoomServicesInterno>(); });

        IServicioConsumosInternosInterno ServicioConsumosInternos
        {
            get { return _servicioConsumosInternos.Value; }
        }

        Lazy<IServicioConsumosInternosInterno> _servicioConsumosInternos = new Lazy<IServicioConsumosInternosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternosInterno>(); });


        IServicioRestaurantesInterno ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantesInterno> _servicioRestaurantes = new Lazy<IServicioRestaurantesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantesInterno>(); });


        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });


        IRepositorioCortesTurno RepositorioCortesTurno
        {
            get { return _repostorioCortesTurno.Value; }
        }

        Lazy<IRepositorioCortesTurno> _repostorioCortesTurno = new Lazy<IRepositorioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCortesTurno>(); });
        
        IRepositorioConfiguracionesTurno RepositorioConfiguracionesTurno
        {
            get { return _repostorioConfiguracionesTurno.Value; }
        }

        Lazy<IRepositorioConfiguracionesTurno> _repostorioConfiguracionesTurno = new Lazy<IRepositorioConfiguracionesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesTurno>(); });

        #endregion

        private void ProcesarDatosCorte(CorteTurno corteActual, DateTime fechaFin, DtoUsuario usuario)
        {
            var valoresPropinas = ServicioPropinas.ObtenerTotalPropinasPorTipo(corteActual.FechaInicio, fechaFin);

            var gastos = ServicioGastos.ObtenerGastosPorTurno(corteActual.Id, true, usuario);

            #region habitación

            var pagosHabitaciones = ServicioRentas.ObtenerDetallesPagosPorFecha(corteActual.FechaInicio, fechaFin);
            var pagosRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(corteActual.FechaInicio, fechaFin);


            corteActual.Habitaciones = pagosHabitaciones.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion /*|| m.ConceptoPago == DetallePago.ConceptosPago.Renovacion*/).Sum(m => m.ValorConIVA);
            corteActual.PersonasExtra = pagosHabitaciones.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA);
            corteActual.HospedajeExtra = pagosHabitaciones.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA);
                                        //pagosHabitaciones.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA);
            corteActual.Paquetes = pagosHabitaciones.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes).Sum(m => m.ValorConIVA);

            corteActual.PropinasHabitacion = valoresPropinas[Propina.TiposPropina.Valet];



            corteActual.CortesiasHabitacion = pagosRentaDic[TiposPago.Cortesia] + pagosRentaDic[TiposPago.Cupon];
            corteActual.ConsumoHabitacion = pagosRentaDic[TiposPago.Consumo];


            corteActual.DescuentosHabitacion = pagosRentaDic[TiposPago.VPoints] + pagosHabitaciones.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Descuentos).Sum(m => m.ValorConIVA); ;


            corteActual.TotalHabitacion = corteActual.Habitaciones + corteActual.PersonasExtra + corteActual.HospedajeExtra +
                                         corteActual.PropinasHabitacion + corteActual.Paquetes - corteActual.CortesiasHabitacion -
                                         corteActual.DescuentosHabitacion - corteActual.ConsumoHabitacion;

            #endregion
            #region room service

            var pagosComandas = ServicioRoomServices.ObtenerValorPagosComandasPorFecha(corteActual.FechaInicio, fechaFin);

            corteActual.RoomService = pagosComandas;
            corteActual.PropinasRoomService = valoresPropinas[Propina.TiposPropina.Comanda];

            var valoresPagosComandas = ServicioRentas.ObtenerPagosComandaPorFecha(corteActual.FechaInicio, fechaFin);

            corteActual.CortesiasRoomService = valoresPagosComandas[TiposPago.Cortesia] + valoresPagosComandas[TiposPago.Cupon];


            corteActual.DescuentosRoomService = valoresPagosComandas[TiposPago.VPoints];

            corteActual.TotalRoomService = corteActual.RoomService + corteActual.PropinasRoomService -
                                          corteActual.CortesiasRoomService - corteActual.DescuentosRoomService;

            #endregion
            #region restaurante

            var valoresPagosRestaurante = ServicioRestaurantes.ObtenerDiccionarioPagosMesaPorFecha(corteActual.FechaInicio, fechaFin);
            var valoresPagosConsumoInterno = ServicioConsumosInternos.ObtenerDiccionarioPagosConsumoInternoPorFecha(corteActual.FechaInicio, fechaFin);

            var pagosRestaurante = ServicioRestaurantes.ObtenerPagosPorFecha(corteActual.FechaInicio, fechaFin) + valoresPagosConsumoInterno[TiposPago.Consumo];

            corteActual.ServicioRestaurante = pagosRestaurante;
            corteActual.PropinasRestaurante = valoresPropinas[Propina.TiposPropina.Restaurante];

            corteActual.CortesiasRestautante = valoresPagosRestaurante[TiposPago.Cortesia] + valoresPagosRestaurante[TiposPago.Cupon];
            corteActual.ConsumoRestaurante = valoresPagosRestaurante[TiposPago.Consumo] + valoresPagosConsumoInterno[TiposPago.Consumo];

            corteActual.DescuentosRestaurante = valoresPagosRestaurante[TiposPago.VPoints];

            corteActual.TotalRestaurante = corteActual.ServicioRestaurante + corteActual.PropinasRestaurante -
                                          corteActual.CortesiasRestautante - corteActual.ConsumoRestaurante - corteActual.DescuentosRestaurante;

            #endregion
            #region taxis

            var ordenesTaxi = ServicioTaxis.ObtenerOrdenesCobradasPorPeriodo(corteActual.FechaInicio, fechaFin);

            var resumenesTaxis = new List<DtoItemCobrable>();

            foreach (var grupo in (from orden in ordenesTaxi
                                   group orden by orden.Precio into o
                                   select o).OrderBy(m => m.Key))

                resumenesTaxis.Add(new DtoItemCobrable
                {
                    Cantidad = grupo.Count(),
                    PrecioUnidad = grupo.Key
                });

            corteActual.Taxis = (ordenesTaxi.Count > 0 ? ordenesTaxi.Sum(m => m.Precio) : 0);

            #endregion
            #region tarjetas de puntos

            var pagosTarjetasVDic = ServicioVPoints.ObtenerDiccionarioPagosPorFecha(corteActual.FechaInicio, fechaFin);

            corteActual.TarjetasV = pagosTarjetasVDic.Sum(m => m.Value);

            #endregion
            #region reservación

            var pagosReservacionDic = ServicioReservaciones.ObtenerDiccionarioPagosPorFecha(corteActual.FechaInicio, fechaFin);

            var valorReservacionesPendientes = /*corteActual.Estado == CorteTurno.Estados.Abierto ?*/
                pagosReservacionDic.Sum(m => m.Value);//:
            ////ServicioReservaciones.ObtenerValorReservacionesPendientesPorPeriodo(usuario, corteActual.FechaInicio, fechaFin).ValorConIVA :
            //corteActual.AnticiposReservas;

            var cortesiasReservacion = pagosReservacionDic[TiposPago.Cortesia] + pagosReservacionDic[TiposPago.Cupon];
            var consumosReservacion = pagosReservacionDic[TiposPago.Consumo];


            var descuentosReservacion = pagosReservacionDic[TiposPago.VPoints];


            var totalReservacionesP = valorReservacionesPendientes - cortesiasReservacion - descuentosReservacion - consumosReservacion;

            #endregion


            if (gastos.Count(m => !m.IdClasificacion.HasValue) > 0)
                corteActual.Gastos = gastos.Where(m => !m.IdClasificacion.HasValue).Sum(m => m.Valor);
            else
                corteActual.Gastos = 0;


            //if (corteActual.Estado == CorteTurno.Estados.Abierto)
            corteActual.AnticiposReservas = valorReservacionesPendientes;
            corteActual.MontosNoReembolsables = ServicioRentas.ObtenerMontoNoReembolsablePorFecha(corteActual.FechaInicio, fechaFin);


            

            corteActual.PagosTarjeta = pagosRentaDic[TiposPago.TarjetaCredito] + valoresPagosComandas[TiposPago.TarjetaCredito] + valoresPagosRestaurante[TiposPago.TarjetaCredito] + pagosTarjetasVDic[TiposPago.TarjetaCredito] + pagosReservacionDic[TiposPago.TarjetaCredito] +
                                       valoresPropinas.Sum(m => m.Value);
            corteActual.PagosEfectivo = pagosRentaDic[TiposPago.Efectivo] + valoresPagosComandas[TiposPago.Efectivo] + valoresPagosRestaurante[TiposPago.Efectivo] + pagosTarjetasVDic[TiposPago.Efectivo] + pagosReservacionDic[TiposPago.Efectivo] +
                                       (ordenesTaxi.Count > 0 ? ordenesTaxi.Sum(m => m.Precio) : 0);
            corteActual.ReservasValidas = pagosRentaDic[TiposPago.Reservacion];

            corteActual.Total = corteActual.TotalHabitacion + corteActual.MontosNoReembolsables.Value + corteActual.TotalRoomService + corteActual.TotalRestaurante + corteActual.Taxis + corteActual.TarjetasV + totalReservacionesP - corteActual.Gastos - corteActual.ReservasValidas;

            var propinasPagadasMeseros = ((IServicioCortesTurnoInterno)this).ObtenerPropinasPagadasMeseros(corteActual);
            var propinasPagadasValets = ((IServicioCortesTurnoInterno)this).ObtenerPropinasPagadasValets(corteActual);


            var pagoNetoMeseros = ((IServicioCortesTurnoInterno)this).ObtenerPagoNetoPropinasMeseros(corteActual);
            var pagoNetoValets = ((IServicioCortesTurnoInterno)this).ObtenerPagoNetoPropinasValets(corteActual);

            corteActual.PagoNetoPropinas = propinasPagadasMeseros - (pagoNetoMeseros.TotalPositivo + pagoNetoValets.TotalPositivo);

            corteActual.PropinasPagadas = propinasPagadasMeseros + propinasPagadasValets;
        }

        public void RealizarCorteTurno(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { RealizarCorteTurno = true }); ;

            CorteTurno corteActual = ObtenerUltimoCorte();

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                var fechaFin = DateTime.Now;

                var nuevoCorte = new CorteTurno
                {
                    FechaInicio = fechaFin.AddSeconds(1),
                    NumeroCorte = corteActual.NumeroCorte + 1,
                    //Estado = CorteTurno.Estados.Abierto
                };

                var configuracionTurnoActual = ObtenerSiguienteConfiguracionTurnoValidada(corteActual.IdConfiguracionTurno, nuevoCorte.FechaInicio);

                nuevoCorte.IdConfiguracionTurno = configuracionTurnoActual.Id;
                RepositorioCortesTurno.Agregar(nuevoCorte);
                RepositorioCortesTurno.GuardarCambios();

                RepositorioCortesTurno.SP_MoverComandas(corteActual.Id);

                ServicioRentas.ValidarNoExistenPagosPendientes(null);
                ServicioRestaurantes.ValidarNoExistenPagosPendientes(null);
                ServicioConsumosInternos.ValidarNoExistenPagosPendientes(null);
                //ServicioTaxis.ValidarNoExistenPagosPendientes(usuario);
                ServicioFajillas.ValidarNoExistenFajillasPendientes();

                ProcesarDatosCorte(corteActual, fechaFin, usuario);

                corteActual.FechaCorte = fechaFin;
                corteActual.IdEstado = (int)CorteTurno.Estados.EnRevision;
                corteActual.IdUsuarioCerro = usuario.Id;



                ServicioRentas.ValidarNoExistenPagosPendientes(corteActual.Id);
                ServicioRestaurantes.ValidarNoExistenPagosPendientes(corteActual.Id);
                ServicioConsumosInternos.ValidarNoExistenPagosPendientes(corteActual.Id);
                //ServicioTaxis.ValidarNoExistenPagosPendientes(usuario);
                ServicioFajillas.ValidarNoExistenFajillasPendientes();


                nuevoCorte = RepositorioCortesTurno.Obtener(m => m.Id == nuevoCorte.Id);
                nuevoCorte.Estado = CorteTurno.Estados.Abierto;

                RepositorioCortesTurno.Modificar(nuevoCorte);
                RepositorioCortesTurno.Modificar(corteActual);
                RepositorioCortesTurno.GuardarCambios();

                //ServicioIncidenciasTurno.ValidarExistenIncidencias(corteActual.Id);

                scope.Complete();
            }

            //Thread.Sleep(100);
            CorteTurno cortesiguiente = ObtenerUltimoCorte();
            RepositorioCortesTurno.Sp_PateaComandaOrdeResConcumoInte(corteActual.Id, cortesiguiente.Id);
        }

        public CorteTurno RevisarCorteTurno(int idCorteTurno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { RevisarCorteTurno = true }); ;

            CorteTurno corteActual = RepositorioCortesTurno.ObtenerCorteEnRevision(idCorteTurno);

            if (corteActual == null)
                throw new SOTException(Recursos.CortesTurno.corte_seleccionado_no_en_revision_exception);

            ProcesarDatosCorte(corteActual, corteActual.FechaCorte.Value, usuario);

            return corteActual;
        }

        public CorteTurno ObtenerDatosUltimoCorteTurno(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { RevisarCorteTurno = true }); ;

            var corteActual = RepositorioCortesTurno.ObtenerUltimoCorte();

            if (corteActual == null)
                throw new SOTException(Recursos.CortesTurno.corte_seleccionado_no_actual_exception);

            var fechaFin = DateTime.Now;//.AddYears(1000);

            ProcesarDatosCorte(corteActual, fechaFin, usuario);

            return corteActual;
        }

        public void CerrarTurno(int idCorteTurno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { FinalizarCorteTurno = true }); ;

            CorteTurno corteActual = RepositorioCortesTurno.ObtenerCorteEnRevision(idCorteTurno);

            if (corteActual == null)
                throw new SOTException(Recursos.CortesTurno.corte_seleccionado_no_en_revision_exception);

            var fechaFin = corteActual.FechaCorte.Value;

            ProcesarDatosCorte(corteActual, fechaFin, usuario);

            corteActual.Estado = CorteTurno.Estados.Cerrado;

            ServicioIncidenciasTurno.ValidarExistenIncidencias(corteActual.Id);

            RepositorioCortesTurno.Modificar(corteActual);
            RepositorioCortesTurno.GuardarCambios();

            OrdenarSincronizacionConServidor();
        }
        
        private async Task OrdenarSincronizacionConServidor()
        {
            try
            {
                var agente = new Negocio.ServiciosLocales.Agentes.Sincronizacion.AgenteLsSincronizacion();

                await agente.SubirInformacionAsync();
            }
            catch (Exception e) 
            {
                Transversal.Log.Logger.Error(e.Message);
            }
        }

        /// <summary>
        /// Retorna la configuración aplicable al siguiente turno, siempre y cuando no exista otro similar en la misma fecha
        /// </summary>
        /// <param name="idCorteActual"></param>
        /// <param name="fechaInicioSiguiente"></param>
        /// <returns></returns>
        ConfiguracionTurno ObtenerSiguienteConfiguracionTurnoValidada(int idConfiguracionTurno, DateTime fechaInicioSiguiente) 
        {
            var ordenConfiguracionActual = RepositorioConfiguracionesTurno.ObtenerElementos(m => m.Activa && m.Id == idConfiguracionTurno).Select(m => m.Orden).FirstOrDefault();

            ConfiguracionTurno siguienteConfig;

            if (!RepositorioConfiguracionesTurno.Alguno(m => m.Activa && m.Orden > ordenConfiguracionActual))
                siguienteConfig = RepositorioConfiguracionesTurno.Obtener(m => m.Activa && m.Orden == 1);
            else
                siguienteConfig = RepositorioConfiguracionesTurno.ObtenerElementos(m => m.Activa && m.Orden > ordenConfiguracionActual).OrderBy(m => m.Orden).FirstOrDefault();

            if (siguienteConfig != null)
            {
                if (RepositorioCortesTurno.Alguno(m => m.IdConfiguracionTurno == siguienteConfig.Id && m.FechaInicio.Day == fechaInicioSiguiente.Day &&
                                                      m.FechaInicio.Month == fechaInicioSiguiente.Month && m.FechaInicio.Year == fechaInicioSiguiente.Year))
                    throw new SOTException(Recursos.CortesTurno.siguiente_turno_repetido_excepcion, siguienteConfig.Nombre, fechaInicioSiguiente.ToString("dd/MM/yyyy"));
            }

            return siguienteConfig;
        }

        public CorteTurno ObtenerUltimoCorte()
        {
            var corte = RepositorioCortesTurno.ObtenerUltimoCorte();

            if (corte == null)
                throw new SOTException(Recursos.CortesTurno.corte_actual_nulo_excepcion);

            return corte;
        }

        public CorteTurno ObtenerCorteEnRevision(int idCorteTurno)
        {
            return RepositorioCortesTurno.ObtenerCorteEnRevision(idCorteTurno);
        }
        
        public DtoComisionesMesero ObtenerComisionesMeseros(DtoUsuario usuario, int idCorte)
        {
            var corte = RepositorioCortesTurno.Obtener(m => m.Id == idCorte);

            if (corte == null)
                return null;

            var fechaInicio = corte.FechaInicio;
            var fechaFin = corte.FechaCorte;

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            var configuracion = ServicioConfiguracionesPropinas.ObtenerConfiguracionPropinas(usuario);
            var configuracionGlobal = ServicioParametros.ObtenerParametros();

            var resumenComisiones = new DtoComisionesMesero();

            var idsLineas = configuracion.PorcentajesPropinaTipo.Select(m => m.IdLineaArticulo).Distinct().ToList();

#warning revisar si se deben incluir los consumos internos
            var pagosArticulosPorTipo = ServicioRoomServices.ObtenerValorArticulosComandasPorPeriodoYLineas(idsLineas, new List<int>() { idCorte });
            pagosArticulosPorTipo.AddRange(ServicioRestaurantes.ObtenerValorArticulosOrdenesPorPeriodoYLineas(idsLineas, new List<int>() { idCorte }));

            var meseros = ServicioEmpleados.ObtenerMeseros();
            var propinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin, Propina.TiposPropina.Comanda, Propina.TiposPropina.Restaurante);

            var gruposPropinas = meseros.ToDictionary(m => m.Id, m => m.NombreCompleto);

            foreach (var propina in propinas)
            {
                if (!gruposPropinas.ContainsKey(propina.IdEmpleado))
                    gruposPropinas.Add(propina.IdEmpleado, propina.Empleado.NombreCompleto);
            }

            foreach (var pago in pagosArticulosPorTipo)
            {
                if (!gruposPropinas.ContainsKey(pago.IdEmpleado))
                    gruposPropinas.Add(pago.IdEmpleado, pago.Nombre);
            }

            Dictionary<int, decimal[]> valoresResumen = new Dictionary<int, decimal[]>();

            #region comisiones de propinas

            Dictionary<string, Type> columnasPropinas = new Dictionary<string, Type>();

            columnasPropinas.Add("MESERO", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                columnasPropinas.Add(configuracionTipo.TipoTarjeta.Descripcion().ToUpper(), typeof(decimal));

            columnasPropinas.Add("COMISIÓN TARJETA", typeof(decimal));

            //var gruposPropinas = propinas.GroupBy(m => m.Empleado.NombreCompleto).ToList();

            object[][] datosPropinas = new object[gruposPropinas.Count][];

            int i = 0;
            foreach (var propinasMesero in gruposPropinas)
            {
                datosPropinas[i] = new object[columnasPropinas.Count];

                int j = 0;

                datosPropinas[i][j++] = propinasMesero.Value;//.First().Empleado.NombreCompleto;
#warning contemplar que en un futuro se puede quitar algun tipo de tarjeta y se deben contemplar las propinas de este tipo eliminado
                var totalesTipo = propinas.Where(m=> m.IdEmpleado == propinasMesero.Key).GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));

                decimal reembolso = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.TipoTarjeta))
                    {
                        datosPropinas[i][j++] = totalesTipo[configuracionTipo.TipoTarjeta];

                        reembolso += Math.Round(totalesTipo[configuracionTipo.TipoTarjeta] * configuracionTipo.Porcentaje);
                    }
                    else
                        datosPropinas[i][j++] = 0;
                }

                datosPropinas[i][j++] = reembolso;

                i++;

                if (!valoresResumen.ContainsKey(propinasMesero.Key))
                    valoresResumen.Add(propinasMesero.Key, new decimal[3]);

                valoresResumen[propinasMesero.Key][0] = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).Sum(m => m.Valor);
                valoresResumen[propinasMesero.Key][2] = reembolso;
            }

            resumenComisiones.DatosPropinasTarjetas = DataExtensions.GetDataTable(columnasPropinas, datosPropinas);

            #endregion
            #region comisiones por artículos

            

            Dictionary<string, Type> columnasArticulos = new Dictionary<string, Type>();

            columnasArticulos.Add("MESERO", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesPropinaTipo)
                columnasArticulos.Add(configuracionTipo.LineaTmp.Desc_Linea.ToUpper(), typeof(decimal));

            columnasArticulos.Add("PUNTOS A PAGAR", typeof(decimal));

            //var pagosEmpleado = pagosArticulosPorTipo.GroupBy(m => m.Nombre).ToList();

            object[][] datosPagos = new object[gruposPropinas.Count][];

            i = 0;
            foreach (var pagoMesero in gruposPropinas)
            {
                datosPagos[i] = new object[columnasArticulos.Count];

                int j = 0;

                datosPagos[i][j++] = pagoMesero.Value;

                var totalesTipo = pagosArticulosPorTipo.Where(m=>m.IdEmpleado == pagoMesero.Key).GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configuracionGlobal.Iva_CatPar));

                decimal puntosPagar = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesPropinaTipo)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.IdLineaArticulo))
                    {
                        datosPagos[i][j++] = totalesTipo[configuracionTipo.IdLineaArticulo];

                        puntosPagar += Math.Round(totalesTipo[configuracionTipo.IdLineaArticulo] * configuracionTipo.Porcentaje);
                    }
                    else
                        datosPagos[i][j++] = 0;
                }

                datosPagos[i][j++] = puntosPagar;

                i++;

                if (!valoresResumen.ContainsKey(pagoMesero.Key))
                    valoresResumen.Add(pagoMesero.Key, new decimal[3]);

                valoresResumen[pagoMesero.Key][1] = puntosPagar;
            }

            resumenComisiones.DatosPagosArticulos = DataExtensions.GetDataTable(columnasArticulos, datosPagos);

            #endregion
            #region resumen

            var columnasResumen = new Dictionary<string, Type>();
            columnasResumen.Add("MESERO", typeof(string));
            columnasResumen.Add("PROPINAS", typeof(decimal));
            columnasResumen.Add("PUNTOS A PAGAR", typeof(decimal));
            columnasResumen.Add("COMISIÓN TARJETA", typeof(decimal));
            columnasResumen.Add("TOTALES", typeof(decimal));

            object[][] valoresResumenArray = new object[valoresResumen.Count][];

            i = 0;
            foreach (var key in valoresResumen.Keys)
            {
                var empleado = meseros.FirstOrDefault(m => m.Id == key) ?? propinas.Where(m => m.IdEmpleado == key).Select(m => m.Empleado).FirstOrDefault();

                valoresResumenArray[i] = new object[5];

                valoresResumenArray[i][0] = empleado != null ? empleado.NombreCompleto : (pagosArticulosPorTipo.Any(m=> m.IdEmpleado == key) ? pagosArticulosPorTipo.FirstOrDefault(m=> m.IdEmpleado == key).Nombre : "");
                valoresResumenArray[i][1] = valoresResumen[key][0];
                valoresResumenArray[i][2] = valoresResumen[key][1];
                valoresResumenArray[i][3] = valoresResumen[key][2];
                valoresResumenArray[i][4] = Math.Round(valoresResumen[key][0] - valoresResumen[key][1] - valoresResumen[key][2]);

                i++;
            }

            resumenComisiones.DatosResumen = DataExtensions.GetDataTable(columnasResumen, valoresResumenArray);

            #endregion

            return resumenComisiones;
        }
        
        public DtoComisionesValets ObtenerComisionesValets(DtoUsuario usuario, int idCorte)
        {
            var corte = RepositorioCortesTurno.Obtener(m => m.Id == idCorte);

            if (corte == null)
                return null;

            var fechaInicio = corte.FechaInicio;
            var fechaFin = corte.FechaCorte;

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            var configuracion = ServicioConfiguracionesPropinas.ObtenerConfiguracionPropinas(usuario);

            var valets = ServicioEmpleados.ObtenerValets();
            var propinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin, Propina.TiposPropina.Valet);

            var diccionarioValets = valets.ToDictionary(m => m.Id, m => m.NombreCompleto);

            foreach (var propina in propinas) 
            {
                if (!diccionarioValets.ContainsKey(propina.IdEmpleado))
                    diccionarioValets.Add(propina.IdEmpleado, propina.Empleado.NombreCompleto);
            }

            var resumenComisiones = new DtoComisionesValets();

            Dictionary<int, decimal[]> valoresResumen = new Dictionary<int, decimal[]>();

            #region comisiones de propinas

            Dictionary<string, Type> columnasPropinas = new Dictionary<string, Type>();

            columnasPropinas.Add("VALET", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                columnasPropinas.Add(configuracionTipo.TipoTarjeta.Descripcion().ToUpper(), typeof(decimal));

            columnasPropinas.Add("COMISIÓN TARJETA", typeof(decimal));

            //var gruposPropinas = propinas.GroupBy(m => m.Empleado.NombreCompleto).ToList();

            object[][] datosPropinas = new object[diccionarioValets.Count][];

            int i = 0;
            foreach (var propinasMesero in diccionarioValets)
            {
                datosPropinas[i] = new object[columnasPropinas.Count];

                int j = 0;

                datosPropinas[i][j++] = propinasMesero.Value;//.First().Empleado.NombreCompleto;


                var totalesTipo = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));
                    //propinasMesero.GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));

                decimal reembolso = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.TipoTarjeta))
                    {
                        datosPropinas[i][j++] = totalesTipo[configuracionTipo.TipoTarjeta];

                        reembolso += Math.Round(totalesTipo[configuracionTipo.TipoTarjeta] * configuracionTipo.Porcentaje);
                    }
                    else
                        datosPropinas[i][j++] = 0;
                }

                datosPropinas[i][j++] = reembolso;

                i++;

                if (!valoresResumen.ContainsKey(propinasMesero.Key))
                    valoresResumen.Add(propinasMesero.Key, new decimal[3]);

                valoresResumen[propinasMesero.Key][0] = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).Sum(m => m.Valor);
                valoresResumen[propinasMesero.Key][2] = reembolso;
            }

            resumenComisiones.DatosPropinasTarjetas = DataExtensions.GetDataTable(columnasPropinas, datosPropinas);

            #endregion
            #region fondos a pagar

            Dictionary<string, Type> columnasArticulos = new Dictionary<string, Type>();

            columnasArticulos.Add("VALETS", typeof(string));

            columnasArticulos.Add("FONDOS A PAGAR", typeof(decimal));

            var fechaActual = fechaInicio;// ?? DateTime.Now;

            var diaActual = ((int)fechaActual.DayOfWeek) == 0 ? 7 : (int)fechaActual.DayOfWeek;

            var dia = configuracion.FondosDia.FirstOrDefault(m => m.Dia == diaActual);

            decimal pago = Math.Round(valets.Count == 0 ? 0 : (dia != null ? dia.Precio : 0) / valets.Count);

            object[][] datosPagos = new object[valets.Count][];

            i = 0;
            foreach (var valet in valets)
            {
                datosPagos[i] = new object[columnasArticulos.Count];

                int j = 0;

                datosPagos[i][j++] = valet.NombreCompleto;

                datosPagos[i][j++] = pago;

                i++;

                if (!valoresResumen.ContainsKey(valet.Id))
                    valoresResumen.Add(valet.Id, new decimal[3]);

                valoresResumen[valet.Id][1] = pago;
            }

            resumenComisiones.DatosFondosPagar = DataExtensions.GetDataTable(columnasArticulos, datosPagos);

            #endregion
            #region resumen

            var columnasResumen = new Dictionary<string, Type>();
            columnasResumen.Add("VALET", typeof(string));
            columnasResumen.Add("PROPINAS", typeof(decimal));
            columnasResumen.Add("FONDOS A PAGAR", typeof(decimal));
            columnasResumen.Add("COMISIÓN TARJETA", typeof(decimal));
            columnasResumen.Add("TOTALES", typeof(decimal));

            object[][] valoresResumenArray = new object[valoresResumen.Count][];

            i = 0;
            foreach (var key in valoresResumen.Keys)
            {
                var empleado = valets.FirstOrDefault(m=> m.Id == key) ?? propinas.Where(m=> m.IdEmpleado == key).Select(m=> m.Empleado).FirstOrDefault();

                valoresResumenArray[i] = new object[5];

                valoresResumenArray[i][0] = empleado != null ? empleado.NombreCompleto : "";
                valoresResumenArray[i][1] = valoresResumen[key][0];
                valoresResumenArray[i][2] = valoresResumen[key][1];
                valoresResumenArray[i][3] = valoresResumen[key][2];
                valoresResumenArray[i][4] = Math.Round(valoresResumen[key][0] - valoresResumen[key][1] - valoresResumen[key][2]);

                i++;
            }

            resumenComisiones.DatosResumen = DataExtensions.GetDataTable(columnasResumen, valoresResumenArray);

            #endregion

            return resumenComisiones;
        }
        public List<DtoPagoHabitacionCorte> ObtenerMovimientosHabitaciones(DtoUsuario usuario, DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago = null, bool soloConPropina = false)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            var pagos = ServicioRentas.ObtenerPagosRentasPorPeriodo(fechaInicio, fechaFin, formasPago);

            var retorno = new List<DtoPagoHabitacionCorte>();

            var idsVentasRenta = pagos.Select(m => m.IdVentaRenta).Distinct().ToList();

            var datosValet = ServicioRentas.ObtenerInformacionValetsPorIdVentaRenta(idsVentasRenta);

            var transacciones = pagos.Select(m => m.Transaccion).Distinct().ToList();

            var diccionarioTicketsTransaccion = ServicioPagos.ObtenerDiccionarioTransaccionesTicketsIgnoraEstado(transacciones);

            foreach (var par in (from pago in pagos
                                  join v in datosValet on pago.IdVentaRenta equals v.IdEntidadPadre into vl
                                  from v in vl.DefaultIfEmpty()
                                  orderby pago.FechaCreacion, pago.Transaccion
                                  select new 
                                  { 
                                    pago,
                                    v
                                  }))
            {
                //foreach (var grupoPagos in pago.Pagos.Where(m => m.Activo).GroupBy(m => m.Transaccion))
                //{
                var propinasTransaccion = ServicioPropinas.ObtenerResumenesPorTransaccion(par.pago.Transaccion);

                if (soloConPropina && propinasTransaccion.Count == 0)
                    continue;

                List<DetallePago> detalles = ServicioRentas.ObtenerDetallesPorTransaccion(par.pago.Transaccion, fechaFin);

                //var detalles = pago.DetallesPago.Where(m => m.Activo && m.Transaccion == grupoPagos.Key).ToList();

                decimal cortesias = par.pago.TipoPago == TiposPago.Cortesia || par.pago.TipoPago == TiposPago.Cupon ? par.pago.Valor : 0;
                decimal descuento = par.pago.TipoPago == TiposPago.VPoints ? par.pago.Valor : 0;
                var consumos = par.pago.TipoPago == TiposPago.Consumo ? par.pago.Valor : 0;

                var habitacion = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion /*|| m.ConceptoPago == DetallePago.ConceptosPago.Renovacion*/).Sum(m => m.ValorConIVA);
                var personasExtra = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA);
                var hospedajeExtra = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA);
                                    //detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA);
                var paquetes = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes).Sum(m => m.ValorConIVA);
                descuento += detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Descuentos).Sum(m => m.ValorConIVA);


                var cajero = ServicioEmpleados.ObtenerNombreCompletoEmpleado(par.pago.IdUsuarioCreo);

                var nuevoPago = new DtoPagoHabitacionCorte
                {
                    Id = par.pago.Id,
                    Fecha = par.pago.FechaCreacion,
                    Ticket = diccionarioTicketsTransaccion.ContainsKey(par.pago.Transaccion) ? diccionarioTicketsTransaccion[par.pago.Transaccion].ToString() : "",
                    //Total = grupoPagos.Sum(m => m.Valor),
                    Cajero = cajero,
                    Valet = par.v != null ? par.v.NombreEmpleado : "",
                    IdValet = par.v != null ? par.v.IdEmpleado : null,
                    FormaPago = par.pago.TipoPago,
                    //string.Join(", ", grupoPagos.Select(m => m.TipoPago.Descripcion()).Distinct().OrderBy(m => m)),
                    Propina = par.pago.TipoPago == TiposPago.TarjetaCredito ? propinasTransaccion.Where(m=> m.TipoTarjeta == par.pago.TipoTarjeta.Value && m.Referencia == par.pago.Referencia).Sum(m=> m.Valor) : 0,
                    Movimiento = string.Join(", ", detalles.Where(m => m.Activo).Select(m => m.ConceptoPago.Descripcion())),
                    Operacion = par.pago.TipoPago == TiposPago.TarjetaCredito ? par.pago.Referencia : "",
                    Habitacion = habitacion,
                    PersonasExtra = personasExtra,
                    HospedajeExtra = hospedajeExtra,
                    Paquetes = paquetes,
                    Cortesias = cortesias,
                    Descuento = descuento,
                    Consumos = consumos,
                    Referencia = par.pago.Referencia,
                    TipoTarjeta = par.pago.TipoTarjeta,
                    NumeroTarjeta = par.pago.NumeroTarjeta
                };

                retorno.Add(nuevoPago);
                //}
            }
            
            //for(int i = 0; i < 100; i++)
            //{
            //    retorno.Add(new DtoPagoHabitacionCorte { Fecha = DateTime.Now });
            //}

            return retorno;
        }

        public List<DtoPagoRestauranteCorte> ObtenerMovimientosRoomService(DtoUsuario usuario, DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago = null, bool soloConPropina = false)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            List<Comanda> comandas = ServicioRoomServices.ObtenerComandasCargadasPorPeriodo(fechaInicio, fechaFin, formasPago);

            var retorno = new List<DtoPagoRestauranteCorte>();

            var transacciones = comandas.SelectMany(m=>m.PagosComanda).Select(m => m.Transaccion).Distinct().ToList();

            var diccionarioTicketsTransaccion = ServicioPagos.ObtenerDiccionarioTransaccionesTicketsIgnoraEstado(transacciones);


            foreach (var comanda in comandas)
            {
                foreach (var pago in comanda.PagosComanda.Where(m => m.Activo))
                {
                    var propinasTransaccion = ServicioPropinas.ObtenerResumenesPorTransaccion(pago.Transaccion);

                    if (soloConPropina && propinasTransaccion.Count == 0)
                        continue;

                    //var detalles = renta.DetallesPago.Where(m => m.Activo && m.Transaccion == grupoPagos.Key).ToList();

                    var cortesias = pago.TipoPago == TiposPago.Cortesia || pago.TipoPago == TiposPago.Cupon ? pago.Valor : 0;
                    var consumos = pago.TipoPago == TiposPago.Consumo ? pago.Valor : 0;
                    decimal descuento = pago.TipoPago == TiposPago.VPoints ? pago.Valor : 0;

                    //var habitacion = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion).Select(m => m.Valor).FirstOrDefault();
                    //var personasExtra = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.Valor);
                    //var hospedajeExtra = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.Valor);
                    //var paquetes = detalles.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes).Sum(m => m.Valor);

#warning separar en la captura de detalles el valor de paquetes del de descuentos

                    var cajero = ServicioEmpleados.ObtenerNombreCompletoEmpleado(pago.IdUsuarioCreo);

                    var nuevoPago = new DtoPagoRestauranteCorte
                    {
                        Fecha = pago.FechaCreacion,
                        Ticket = diccionarioTicketsTransaccion.ContainsKey(pago.Transaccion) ? diccionarioTicketsTransaccion[pago.Transaccion].ToString() : "",
                        //Total = grupoPagos.Sum(m => m.Valor),
                        Cajero = cajero,
                        Mesero = comanda.EmpleadoCobro.NombreCompleto,
                        FormaPago = pago.TipoPago,
                        //string.Join(", ", grupoPagos.Select(m => m.TipoPago.Descripcion()).Distinct().OrderBy(m => m)),
                        Propina = pago.TipoPago == TiposPago.TarjetaCredito ? propinasTransaccion.Where(m => m.TipoTarjeta == pago.TipoTarjeta.Value && m.Referencia == pago.Referencia).Sum(m => m.Valor) : 0,
                        Operacion = pago.TipoPago == TiposPago.TarjetaCredito ? pago.Referencia : "",
                        Subtotal = pago.Valor,
                        Cortesias = cortesias,
                        Consumos = consumos,
                        Descuento = descuento,
                        Id = pago.Id,
                        IdMesero = comanda.IdEmpleadoCobro,
                        NumeroTarjeta = pago.NumeroTarjeta,
                        Referencia = pago.Referencia,
                        TipoTarjeta = pago.TipoTarjeta
                    };

                    retorno.Add(nuevoPago);
                }
            }

            //for(int i = 0; i < 100; i++)
            //{
            //    retorno.Add(new DtoPagoHabitacionCorte { Fecha = DateTime.Now });
            //}

            return retorno;
        }

        public List<DtoPagoRestauranteCorte> ObtenerMovimientosRestaurante(DtoUsuario usuario, DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago = null, bool soloConPropina = false)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            List<OcupacionMesa> ocupaciones = ServicioRestaurantes.ObtenerOcupacionesCargadasPorPeriodo(fechaInicio, fechaFin, formasPago);

            var retorno = new List<DtoPagoRestauranteCorte>();

            var transacciones = ocupaciones.SelectMany(m=>m.PagosOcupacionMesa).Select(m => m.Transaccion).Distinct().ToList();

            var diccionarioTicketsTransaccion = ServicioPagos.ObtenerDiccionarioTransaccionesTicketsIgnoraEstado(transacciones);


            foreach (var ocupacion in ocupaciones)
            {
                foreach (var pago in ocupacion.PagosOcupacionMesa.Where(m => m.Activo))
                {
                    var propinasTransaccion = ServicioPropinas.ObtenerResumenesPorTransaccion(pago.Transaccion);

                    if (soloConPropina && propinasTransaccion.Count == 0)
                        continue;

                    //var detalles = renta.DetallesPago.Where(m => m.Activo && m.Transaccion == grupoPagos.Key).ToList();

                    var cortesias = pago.TipoPago == TiposPago.Cortesia || pago.TipoPago == TiposPago.Cupon ? pago.Valor : 0;
                    var consumos = pago.TipoPago == TiposPago.Consumo ? pago.Valor : 0;
                    decimal descuento = pago.TipoPago == TiposPago.VPoints ? pago.Valor : 0;

#warning separar en la captura de detalles el valor de paquetes del de descuentos

                    var cajero = ServicioEmpleados.ObtenerNombreCompletoEmpleado(pago.IdUsuarioCreo);

                    var nuevoPago = new DtoPagoRestauranteCorte
                    {
                        Fecha = pago.FechaCreacion,
                        Ticket = diccionarioTicketsTransaccion.ContainsKey(pago.Transaccion) ? diccionarioTicketsTransaccion[pago.Transaccion].ToString() : "",
                        //Total = grupoPagos.Sum(m => m.Valor),
                        Cajero = cajero,
                        Mesero = ocupacion.Mesero.NombreCompleto,
                        FormaPago = pago.TipoPago,
                        //string.Join(", ", grupoPagos.Select(m => m.TipoPago.Descripcion()).Distinct().OrderBy(m => m)),
                        Propina = pago.TipoPago == TiposPago.TarjetaCredito ? propinasTransaccion.Where(m => m.TipoTarjeta == pago.TipoTarjeta.Value && m.Referencia == pago.Referencia).Sum(m => m.Valor) : 0,
                        Operacion = pago.TipoPago == TiposPago.TarjetaCredito ? pago.Referencia : "",
                        Subtotal = pago.Valor,
                        Cortesias = cortesias,
                        Consumos = consumos,
                        Descuento = descuento,
                        Id = pago.Id,
                        IdMesero = ocupacion.IdMesero,
                        NumeroTarjeta = pago.NumeroTarjeta,
                        Referencia = pago.Referencia,
                        TipoTarjeta = pago.TipoTarjeta
                    };

                    retorno.Add(nuevoPago);
                }
            }

            List<ConsumoInterno> consumosInternos = ServicioConsumosInternos.ObtenerConsumosInternosCargadosPorPeriodo(fechaInicio, fechaFin, formasPago);

            transacciones = consumosInternos.SelectMany(m => m.PagosConsumoInterno).Select(m => m.Transaccion).Distinct().ToList();

            diccionarioTicketsTransaccion = ServicioPagos.ObtenerDiccionarioTransaccionesTicketsIgnoraEstado(transacciones);

            foreach (var consumoInterno in consumosInternos)
            {
                foreach (var pago in consumoInterno.PagosConsumoInterno.Where(m => m.Activo))
                {
                    var propinasTransaccion = ServicioPropinas.ObtenerResumenesPorTransaccion(pago.Transaccion);

                    if (soloConPropina && propinasTransaccion.Count == 0)
                        continue;

                    //var detalles = renta.DetallesPago.Where(m => m.Activo && m.Transaccion == grupoPagos.Key).ToList();

                    var cortesias = pago.TipoPago == TiposPago.Cortesia || pago.TipoPago == TiposPago.Cupon ? pago.Valor : 0;
                    var consumos = pago.TipoPago == TiposPago.Consumo ? pago.Valor : 0;
                    decimal descuento = pago.TipoPago == TiposPago.VPoints ? pago.Valor : 0;

#warning separar en la captura de detalles el valor de paquetes del de descuentos

                    var cajero = ServicioEmpleados.ObtenerNombreCompletoEmpleado(pago.IdUsuarioCreo);

                    var nuevoPago = new DtoPagoRestauranteCorte
                    {
                        Fecha = pago.FechaCreacion,
                        Ticket = diccionarioTicketsTransaccion.ContainsKey(pago.Transaccion) ? diccionarioTicketsTransaccion[pago.Transaccion].ToString() : "",
                        //Total = grupoPagos.Sum(m => m.Valor),
                        Cajero = cajero,
                        Mesero = consumoInterno.Mesero.NombreCompleto,
                        FormaPago = pago.TipoPago,
                        //string.Join(", ", grupoPagos.Select(m => m.TipoPago.Descripcion()).Distinct().OrderBy(m => m)),
                        Propina = pago.TipoPago == TiposPago.TarjetaCredito ? propinasTransaccion.Where(m => m.TipoTarjeta == pago.TipoTarjeta.Value && m.Referencia == pago.Referencia).Sum(m => m.Valor) : 0,
                        Operacion = pago.TipoPago == TiposPago.TarjetaCredito ? pago.Referencia : "",
                        Subtotal = pago.Valor,
                        Cortesias = cortesias,
                        Consumos = consumos,
                        Descuento = descuento,
                        Id = pago.Id,
                        IdMesero = consumoInterno.IdMesero,
                        NumeroTarjeta = pago.NumeroTarjeta,
                        Referencia = pago.Referencia,
                        TipoTarjeta = pago.TipoTarjeta
                    };

                    retorno.Add(nuevoPago);
                }
            }

            //for(int i = 0; i < 100; i++)
            //{
            //    retorno.Add(new DtoPagoHabitacionCorte { Fecha = DateTime.Now });
            //}

            return retorno;
        }

        public DtoReporteContable ObtenerReporteContablePorRangoCortes(DtoUsuario usuario, int corteInicio, int corteFin)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDatosCorte = true });

            if (corteFin < corteInicio)
                throw new SOTException(Recursos.CortesTurno.corte_inicial_mayor_final_excepcion);

            var corteInicial = RepositorioCortesTurno.Obtener(m => m.NumeroCorte == corteInicio);

            var corteFinal = corteInicio == corteFin ? corteInicial : RepositorioCortesTurno.Obtener(m => m.NumeroCorte == corteFin);

            if (corteInicial == null)
                throw new SOTException(Recursos.CortesTurno.numero_corte_invalido_excepcion, corteInicio.ToString());

            if (corteFinal == null)
                throw new SOTException(Recursos.CortesTurno.numero_corte_invalido_excepcion, corteFin.ToString());

            DateTime fechaInicio = corteInicial.FechaInicio;
            DateTime fechaFin = corteFinal.FechaCorte ?? DateTime.Now;

            string folio = corteInicial == corteFinal ? corteInicial.NumeroCorte.ToString() : string.Join(" - ", corteInicio, corteFin);

            return ObtenerReporteContable(folio, corteInicio, corteFin, usuario.NombreCompleto, fechaInicio, fechaFin);
        }

        //public DtoReporteContable ObtenerReporteContableTurnoActual(DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDatosCorte = true });

        //    var idEstadoAbierto = (int)CorteTurno.Estados.Abierto;

        //    var corteInicial = RepositorioCortesTurno.Obtener(m => m.IdEstado == idEstadoAbierto);

        //    if (corteInicial == null)
        //        throw new SOTException(Recursos.CortesTurno.numero_corte_invalido_excepcion, corteInicial.ToString());

        //    DateTime fechaInicio = corteInicial.FechaInicio;
        //    DateTime fechaFin = DateTime.Now;

        //    string folio = corteInicial.NumeroCorte.ToString();

        //    return ObtenerReporteContable(folio, corteInicial.NumeroCorte, corteInicial.NumeroCorte, usuario.NombreCompleto, fechaInicio, fechaFin);
        //}

        private DtoReporteContable ObtenerReporteContable(string folio, int corteInicio, int corteFin, string datosUsuario, DateTime fechaInicio, DateTime fechaFin)
        {
            var configGlobal = ServicioParametros.ObtenerParametros();

            var lineasArticulos = ServicioLineas.ObtenerLineas(true);

            var reporte = new DtoReporteContable
            {
                Folio = folio,
                InicioReporte = fechaInicio,
                NombreUsuario = datosUsuario,
                NombreNegocio = configGlobal.Nombre,
                Direccion = configGlobal.Direccion,
                FinReporte = fechaFin
            };

            //reporte.InicioReporte = fechaInicio;

            #region propinas

            var valoresPropinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin);

            reporte.Propinas = valoresPropinas.Count > 0 ? valoresPropinas.Sum(m => m.Valor) : 0;

            #endregion
            #region habitaciones

            var detallesRenta = ServicioRentas.ObtenerDetallesPagosPorFecha(fechaInicio, fechaFin);
            var detallesHabitaciones = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion /*|| m.ConceptoPago == DetallePago.ConceptosPago.Renovacion*/).GroupBy(m => m.ValorConIVA);

            reporte.ResumenesHabitaciones = new List<DtoItemCobrable>();

            foreach (var dh in detallesHabitaciones)
            {
                reporte.ResumenesHabitaciones.Add(new DtoItemCobrable
                {
                    Cantidad = dh.Count(),
                    PrecioUnidad = dh.Key
                });

                reporte.VTH_Habitaciones += reporte.ResumenesHabitaciones.Last().Total;
            }

            var pagosRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(fechaInicio, fechaFin);

            reporte.VTH_NoReembolsables = ServicioRentas.ObtenerMontoNoReembolsablePorFecha(fechaInicio, fechaFin);
            reporte.VTH_PersonasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA);
            reporte.VTH_HospedajeExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA);
            //detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA);
            reporte.VTH_Paquetes = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes && m.ValorConIVA > 0).Sum(m => m.ValorConIVA);
            reporte.VTH_Descuentos = pagosRentaDic[TiposPago.VPoints] + detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Descuentos).Sum(m => m.ValorConIVA);
            //var valoresPagos = ServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(usuario, fechaInicio, fechaFin, TiposPago.Cortesia);

            reporte.VTH_Cortesias = pagosRentaDic[TiposPago.Cortesia] + pagosRentaDic[TiposPago.Cupon];
            reporte.VTH_ConsumosInternos = pagosRentaDic[TiposPago.Consumo];

#warning faltan los descuentos


            #endregion
            #region personas extra

            reporte.PersonasExtra = new List<DtoItemCobrable>();

            var personasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).ToList();
            //ServicioRentas.ObtenerPersonasExtraPorPeriodo(fechaInicio, fechaFin);

            foreach (var grupo in personasExtra.GroupBy(m => m.ValorConIVA / m.Cantidad).OrderBy(m => m.Key))
            {
                reporte.PersonasExtra.Add(new DtoItemCobrable
                {
                    Cantidad = grupo.Sum(m => m.Cantidad),//.Count(),
                    PrecioUnidad = grupo.Key// * (1 + configGlobal.Iva_CatPar)
                });
            }

            #endregion
            #region hospedaje extra

            reporte.HospedajeExtra = new List<DtoItemCobrable>();

            var horasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).ToList();
            //ServicioRentas.ObtenerHorasExtraPorPeriodo(fechaInicio, fechaFin);

            foreach (var grupo in horasExtra.GroupBy(m => m.ValorConIVA / m.Cantidad).OrderBy(m => m.Key))
            {
                reporte.HospedajeExtra.Add(new DtoItemCobrable
                {
                    Cantidad = grupo.Sum(m => m.Cantidad),//.Count(),
                    PrecioUnidad = grupo.Key// * (1 + configGlobal.Iva_CatPar)
                });
            }

            #endregion
            #region paquetes - descuentos

            reporte.Paquetes = new List<DtoItemCobrable>();
            reporte.Descuentos = new List<DtoItemCobrable>();

            var paquetes = ServicioRentas.ObtenerPaquetesRentaPorPeriodo(fechaInicio, fechaFin);

            foreach (var grupo in (from g in paquetes
                                   group g by new { g.NombrePaquete, Precio = g.Precio, Descuento = g.Descuento } into gg
                                   orderby gg.Key.NombrePaquete, gg.Key.Precio, gg.Key.Descuento
                                   select gg))
            {
                if (grupo.First().ValorFinal > 0)
                    reporte.Paquetes.Add(new DtoItemCobrable
                    {
                        Nombre = grupo.Key.NombrePaquete,
                        Cantidad = grupo.Sum(m => m.Cantidad),
                        PrecioUnidad = grupo.First().ValorFinal * (1 + configGlobal.Iva_CatPar)
                    });
                else
                    reporte.Descuentos.Add(new DtoItemCobrable
                    {
                        Nombre = grupo.Key.NombrePaquete,
                        Cantidad = grupo.Sum(m=> m.Cantidad),
                        PrecioUnidad = grupo.First().ValorFinal * (1 + configGlobal.Iva_CatPar) * -1
                    });
            }

            #endregion
            #region taxis

            var ordenesTaxi = ServicioTaxis.ObtenerOrdenesCobradasPorPeriodo(fechaInicio, fechaFin);

            reporte.ResumenesTaxis = new List<DtoItemCobrable>();

            foreach (var grupo in (from orden in ordenesTaxi
                                   group orden by orden.Precio into o
                                   select o).OrderBy(m => m.Key))

                reporte.ResumenesTaxis.Add(new DtoItemCobrable
                {
                    Cantidad = grupo.Count(),
                    PrecioUnidad = grupo.Key
                });

            //reporte.Taxis = ordenesTaxi.Count > 0 ? ordenesTaxi.Sum(m => m.Precio) : 0;

            #endregion
            #region room service

            reporte.RoomService = new List<DtoItemCobrableBase>();

            var idsTipos = lineasArticulos.Select(m => m.Cod_Linea).ToList();

            var idsCortes = RepositorioCortesTurno.ObtenerElementos(m => m.NumeroCorte >= corteInicio && m.NumeroCorte <= corteFin).Select(m => m.Id).ToList();

            var pagosArticulosComanda = ServicioRoomServices.ObtenerValorArticulosComandasPorPeriodoYLineas(idsTipos, idsCortes);

            var totalesLineas = pagosArticulosComanda.GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configGlobal.Iva_CatPar));

            foreach (var configuracionTipo in lineasArticulos)
            {
                if (totalesLineas.ContainsKey(configuracionTipo.Cod_Linea))
                {
                    reporte.RoomService.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = totalesLineas[configuracionTipo.Cod_Linea]
                    });
                }
                else
                    reporte.RoomService.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = 0
                    });
            }

            #endregion
            #region tarjetas de puntos

            var pagosTarjetasVDic = ServicioVPoints.ObtenerDiccionarioPagosPorFecha(fechaInicio, fechaFin);

            reporte.TarjetasV = pagosTarjetasVDic.Sum(m => m.Value);

            #endregion
            #region restaurante

            reporte.Restaurante = new List<DtoItemCobrableBase>();

            pagosArticulosComanda = ServicioRestaurantes.ObtenerValorArticulosOrdenesPorPeriodoYLineas(idsTipos, idsCortes);
            pagosArticulosComanda.AddRange(ServicioConsumosInternos.ObtenerValorArticulosConsumosPorPeriodoYLineas(idsTipos, idsCortes));

            totalesLineas = pagosArticulosComanda.GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configGlobal.Iva_CatPar));

            foreach (var configuracionTipo in lineasArticulos)
            {
                if (totalesLineas.ContainsKey(configuracionTipo.Cod_Linea))
                {
                    reporte.Restaurante.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = totalesLineas[configuracionTipo.Cod_Linea]
                    });
                }
                else
                    reporte.Restaurante.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = 0
                    });
            }

            reporte.Restaurante.Add(new DtoItemCobrableBase
            {
                Nombre = "Tarjetas V",
                Total = reporte.TarjetasV
            });

            #endregion

            #region room & rest

            var pagosComandas = ServicioRoomServices.ObtenerValorPagosComandasPorFecha(fechaInicio, fechaFin);
            var pagosRestaurante = ServicioRestaurantes.ObtenerPagosPorFecha(fechaInicio, fechaFin);
            var pagosConsumos = ServicioConsumosInternos.ObtenerPagosPorFecha(fechaInicio, fechaFin);

            reporte.VTRR_Restaurante = pagosComandas + pagosRestaurante + pagosConsumos + reporte.TarjetasV;// +(ordenesDeTaxi.Count > 0 ? ordenesDeTaxi.Sum(m => m.Precio) : 0);

            var valoresPagosComandas = ServicioRentas.ObtenerPagosComandaPorFecha(fechaInicio, fechaFin);
            var valoresPagosRestaurante = ServicioRestaurantes.ObtenerDiccionarioPagosMesaPorFecha(fechaInicio, fechaFin);
            var valoresConsumosInternos = ServicioConsumosInternos.ObtenerDiccionarioPagosConsumoInternoPorFecha(fechaInicio, fechaFin);

            reporte.VTRR_Cortesias = valoresPagosComandas[TiposPago.Cortesia] + valoresPagosRestaurante[TiposPago.Cortesia] + pagosTarjetasVDic[TiposPago.Cortesia] +
                                     valoresPagosComandas[TiposPago.Cupon] + valoresPagosRestaurante[TiposPago.Cupon] + pagosTarjetasVDic[TiposPago.Cupon];
            reporte.VTRR_Consumos = valoresPagosComandas[TiposPago.Consumo] + valoresPagosRestaurante[TiposPago.Consumo] + valoresConsumosInternos[TiposPago.Consumo] + pagosTarjetasVDic[TiposPago.Consumo];

            reporte.VTRR_Descuentos = valoresPagosComandas[TiposPago.VPoints] + valoresPagosRestaurante[TiposPago.VPoints] + pagosTarjetasVDic[TiposPago.VPoints];

            #endregion
            #region ingresos totales

            var pagosReservacionDic = ServicioReservaciones.ObtenerDiccionarioPagosDeConsumidasPorFecha(fechaInicio, fechaFin);


            reporte.FormasPago = new List<DtoItemCobrableBase>();

            //var pagosRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(usuario, fechaInicio, fechaFin, TiposPago.Efectivo);
            //var pagosRestauranteDic = ServicioRestaurantes.ObtenerDiccionarioPagosMesaPorFecha(usuario, fechaInicio, fechaFin, TiposPago.Efectivo);

            //var pagosTarjetasVDic = ServicioVPoints.ObtenerDiccionarioPagosPorFecha(fechaInicio, fechaFin);

            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = TiposPago.Efectivo.Descripcion() + " (+)",
                Total = pagosRentaDic[TiposPago.Efectivo] + valoresPagosComandas[TiposPago.Efectivo] + valoresPagosRestaurante[TiposPago.Efectivo] + pagosTarjetasVDic[TiposPago.Efectivo] + pagosReservacionDic[TiposPago.Efectivo] /*+
                        (ordenesTaxi.Count > 0 ? ordenesTaxi.Sum(m => m.Precio) : 0)*/
            });

            //reporte.FormasPago.Add(new DtoItemCobrableBase
            //{
            //    Nombre = TiposPago.VPoints.Descripcion() + " (+)",
            //    Total = pagosRentaDic[TiposPago.VPoints] + valoresPagosComandas[TiposPago.VPoints] + valoresPagosRestaurante[TiposPago.VPoints]
            //});


            var pagosTarjetaRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaReservacionDic = ServicioReservaciones.ObtenerDiccionarioPagosTarjetaDeConsumidasPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaComandaDic = ServicioRoomServices.ObtenerDiccionarioPagosComandasTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaRestauranteDic = ServicioRestaurantes.ObtenerDiccionarioPagosMesaTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaTV = ServicioVPoints.ObtenerDiccionarioPagosTarjetaPorFecha(fechaInicio, fechaFin);

            foreach (var tipoTarjeta in EnumExtensions.ComoLista<TiposTarjeta>())
            {
                //var propinasTipo = valoresPropinas.Where(m => m.TipoTarjeta == tipoTarjeta).Select(m => m.Valor).ToList();

                reporte.FormasPago.Add(new DtoItemCobrableBase
                {
                    Nombre = tipoTarjeta.Descripcion() + " (+)",
                    Total = pagosTarjetaRentaDic[tipoTarjeta] + pagosTarjetaReservacionDic[tipoTarjeta] + pagosTarjetaRestauranteDic[tipoTarjeta] + pagosTarjetaComandaDic[tipoTarjeta] + pagosTarjetaTV[tipoTarjeta] /*+
                            (propinasTipo.Count > 0 ? propinasTipo.Sum(m => m) : 0)*/
                });
            }


            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = "Transferencias (+)",
                Total = pagosRentaDic[TiposPago.Transferencia] + valoresPagosComandas[TiposPago.Transferencia] + valoresPagosRestaurante[TiposPago.Transferencia] +
                        valoresConsumosInternos[TiposPago.Transferencia] + pagosReservacionDic[TiposPago.Transferencia] + pagosTarjetasVDic[TiposPago.Transferencia]
            });

            //reporte.FormasPago.Add(new DtoItemCobrableBase
            //{
            //    Nombre = "Taxis (+)",
            //    Total = reporte.Taxis
            //});

            //reporte.FormasPago.Add(new DtoItemCobrableBase
            //{
            //    Nombre = "Propinas (+)",
            //    Total = reporte.Propinas
            //});

            #endregion

            return reporte;
        }

        public string  UsuarioCerroReporte(int idUsuariocerro)
        {
            return RepositorioCortesTurno.UsuarioCerroReporte(idUsuariocerro);
        }
        public string ObtieneSiglaTurno(int idconfigTurno)
        {
            return RepositorioCortesTurno.ObtieneSiglaTurno(idconfigTurno);
        }
        public DtoReporteCierre ObtenerReporteCierre(DtoUsuario usuario, int folioCorte)
        {
            var configGlobal = ServicioParametros.ObtenerParametros();

            var lineasArticulos = ServicioLineas.ObtenerLineas(true);

            //if (corteFin < corteInicio)
            //    throw new SOTException(Recursos.CortesTurno.corte_inicial_mayor_final_excepcion);

            var corte = RepositorioCortesTurno.Obtener(m => m.NumeroCorte == folioCorte);

            ProcesarDatosCorte(corte, corte.FechaCorte.Value, usuario);

            //var corteFinal = corteInicio == corteFin ? corteInicial : RepositorioCortesTurno.Obtener(m => m.NumeroCorte == corteFin);

            if (corte == null)
                throw new SOTException(Recursos.CortesTurno.numero_corte_invalido_excepcion, corte.ToString());

            //if (corteFinal == null)
            //    throw new SOTException(Recursos.CortesTurno.numero_corte_invalido_excepcion, corteFin.ToString());

            DateTime fechaInicio = corte.FechaInicio;
            DateTime fechaFin = corte.FechaCorte ?? DateTime.Now;

            string UsuarioCerroCorte = UsuarioCerroReporte(corte.IdUsuarioCerro.Value);
            string siglaturno = ObtieneSiglaTurno(corte.IdConfiguracionTurno);
            string folio = corte.NumeroCorte.ToString();//corteInicial == corteFinal ? corteInicial.NumeroCorte.ToString() : string.Join(" - ", corteInicio, corteFin);

            var reporte = new DtoReporteCierre
            {
                Folio = folio,
                SiglaTurno = siglaturno,										
                AliasUsuarioCerro = UsuarioCerroCorte,												  
                InicioReporte = fechaInicio,
                NombreUsuario = usuario.Alias,
                NombreNegocio = configGlobal.Nombre,
                Direccion = configGlobal.Direccion,
                FinReporte = fechaFin
            };

            //if (fechaInicio.HasValue)
            //    reporte.InicioReporte = fechaInicio.Value;

            #region propinas

            var valoresPropinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin);

            reporte.Propinas = valoresPropinas.Count > 0 ? valoresPropinas.Sum(m => m.Valor) : 0;

            #endregion
            #region habitaciones

            var detallesRenta = ServicioRentas.ObtenerDetallesPagosPorFecha(fechaInicio, fechaFin);
            var detallesHabitaciones = ServicioRentas.ObtenerDetallesPagosPorFechaYTipo(fechaInicio, fechaFin);
            //detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).GroupBy(m => m.ValorConIVA);

            reporte.ResumenesHabitaciones = new List<DtoItemCobrable>();

            foreach (var dh in detallesHabitaciones.Keys)
            {
                var detallesX = detallesHabitaciones[dh].Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion /*|| m.ConceptoPago == DetallePago.ConceptosPago.Renovacion*/).GroupBy(m => m.ValorConIVA / m.Cantidad);

                foreach (var detalle in detallesX)
                {
                    reporte.ResumenesHabitaciones.Add(new DtoItemCobrable
                    {
                        Nombre = dh,
                        Cantidad = detalle.Sum(m => m.Cantidad),
                        PrecioUnidad = detalle.Key
                    });

                    reporte.VTH_Habitaciones += reporte.ResumenesHabitaciones.Last().Total;
                }
            }

            var pagosRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(fechaInicio, fechaFin);

            reporte.VTH_PersonasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA);
            reporte.VTH_HospedajeExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA);
                                        //detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA);
            reporte.VTH_Paquetes = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes && m.ValorConIVA > 0).Sum(m => m.ValorConIVA);
            reporte.VTH_Descuentos = pagosRentaDic[TiposPago.VPoints] - detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes && m.ValorConIVA < 0).Sum(m => m.ValorConIVA);
            //var valoresPagos = ServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(usuario, fechaInicio, fechaFin, TiposPago.Cortesia);

            reporte.VTH_Cortesias = pagosRentaDic[TiposPago.Cortesia] + pagosRentaDic[TiposPago.Cupon];
            reporte.VTH_ConsumosInternos = pagosRentaDic[TiposPago.Consumo];

            reporte.VTH_NoReembolsables = corte.MontosNoReembolsables ?? 0;

            #endregion
            #region personas extra

            reporte.PersonasExtra = new List<DtoItemCobrable>();

            var personasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).ToList();
            //ServicioRentas.ObtenerPersonasExtraPorPeriodo(fechaInicio, fechaFin);

            /*      foreach (var grupo in personasExtra.GroupBy(m => m.ValorConIVA / m.Cantidad).OrderBy(m => m.Key))
                   {
                       reporte.PersonasExtra.Add(new DtoItemCobrable
                       {
                           Cantidad = grupo.Sum(m => m.Cantidad),//Count(),
                           PrecioUnidad = grupo.Key// * (1 + configGlobal.Iva_CatPar)
                       });
                   }*/

            var personaExtra1 = ServicioRentas.ObtienePersonasExtrasPorCorte(folioCorte);
            foreach (DtoDatosPersonaExtrasRepCorteTurno item1 in personaExtra1)
            {
                reporte.PersonasExtra.Add(new DtoItemCobrable
                {
                    Cantidad = item1.Cantidad, //grupo.Sum(m => m.Cantidad),//Count(),
                    PrecioUnidad = Math.Round(item1.Precio * (1 + configGlobal.Iva_CatPar))
                });
            }


       /*     foreach (var grupo in personasExtra.GroupBy(m => m.ValorConIVA).OrderBy(m => m.Key))
            {
                reporte.PersonasExtra.Add(new DtoItemCobrable
                {
                    Cantidad =   grupo.Count(), //grupo.Sum(m => m.Cantidad),//Count(),
                    PrecioUnidad = grupo.Key// * (1 + configGlobal.Iva_CatPar)
                });
            }*/

            #endregion
            #region hospedaje extra

            reporte.HospedajeExtra = new List<DtoItemCobrable>();

            var horasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).ToList();
            //ServicioRentas.ObtenerHorasExtraPorPeriodo(fechaInicio, fechaFin);

            foreach (var grupo in horasExtra.GroupBy(m => m.ValorConIVA / m.Cantidad).OrderBy(m => m.Key))
            {
                reporte.HospedajeExtra.Add(new DtoItemCobrable
                {
                    Cantidad = grupo.Sum(m => m.Cantidad),//Count(),
                    PrecioUnidad = grupo.Key// * (1 + configGlobal.Iva_CatPar)
                });
            }

            #endregion
            #region paquetes - descuentos

            reporte.Paquetes = new List<DtoItemCobrable>();
            reporte.Descuentos = new List<DtoItemCobrable>();

            var paquetes = ServicioRentas.ObtenerPaquetesRentaPorPeriodo(fechaInicio, fechaFin);

            foreach (var grupo in (from g in paquetes
                                   group g by new { g.NombrePaquete, Precio = g.Precio, Descuento = g.Descuento } into gg
                                   orderby gg.Key.NombrePaquete, gg.Key.Precio, gg.Key.Descuento
                                   select gg))
            {
                if (grupo.First().ValorFinal > 0)
                    reporte.Paquetes.Add(new DtoItemCobrable
                    {
                        Nombre = grupo.Key.NombrePaquete,
                        Cantidad = grupo.Sum(m => m.Cantidad),
                        PrecioUnidad = grupo.First().ValorFinal * (1 + configGlobal.Iva_CatPar)
                    });
                else
                    reporte.Descuentos.Add(new DtoItemCobrable
                    {
                        Nombre = grupo.Key.NombrePaquete,
                        Cantidad = grupo.Sum(m => m.Cantidad),
                        PrecioUnidad = grupo.First().ValorFinal * (1 + configGlobal.Iva_CatPar) * -1
                    });
            }

            #endregion
            #region taxis

            var ordenesTaxi = ServicioTaxis.ObtenerOrdenesCobradasPorPeriodo(fechaInicio, fechaFin);

            reporte.ResumenesTaxis = new List<DtoItemCobrable>();

            foreach (var grupo in (from orden in ordenesTaxi
                                   group orden by orden.Precio into o
                                   select o).OrderBy(m => m.Key))

                reporte.ResumenesTaxis.Add(new DtoItemCobrable
                {
                    Cantidad = grupo.Count(),
                    PrecioUnidad = grupo.Key
                });

            //reporte.Taxis = ordenesTaxi.Count > 0 ? ordenesTaxi.Sum(m => m.Precio) : 0;

            #endregion
            #region room service

            reporte.RoomService = new List<DtoItemCobrableBase>();

            var idsTipos = lineasArticulos.Select(m => m.Cod_Linea).ToList();

            var pagosArticulosComanda = ServicioRoomServices.ObtenerValorArticulosComandasPorPeriodoYLineas(idsTipos, new List<int>() { corte.Id });

            var totalesLineas = pagosArticulosComanda.GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configGlobal.Iva_CatPar));

            foreach (var configuracionTipo in lineasArticulos)
            {
                if (totalesLineas.ContainsKey(configuracionTipo.Cod_Linea))
                {
                    reporte.RoomService.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = totalesLineas[configuracionTipo.Cod_Linea]
                    });
                }
                else
                    reporte.RoomService.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = 0
                    });
            }

            #endregion
            #region restaurante

            reporte.Restaurante = new List<DtoItemCobrableBase>();

            var pagosArticulosOrdenRestaurante = ServicioRestaurantes.ObtenerValorArticulosOrdenesPorPeriodoYLineas(idsTipos, new List<int>() { corte.Id });
            pagosArticulosOrdenRestaurante.AddRange(ServicioConsumosInternos.ObtenerValorArticulosConsumosPorPeriodoYLineas(idsTipos, new List<int>() { corte.Id }));

            totalesLineas = pagosArticulosOrdenRestaurante.GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configGlobal.Iva_CatPar));

            foreach (var configuracionTipo in lineasArticulos)
            {
                if (totalesLineas.ContainsKey(configuracionTipo.Cod_Linea))
                {
                    reporte.Restaurante.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = totalesLineas[configuracionTipo.Cod_Linea]
                    });
                }
                else
                    reporte.Restaurante.Add(new DtoItemCobrableBase
                    {
                        Nombre = configuracionTipo.Desc_Linea,
                        Total = 0
                    });
            }

            #endregion
            #region room & rest

            var pagosComandas = ServicioRoomServices.ObtenerValorPagosComandasPorFecha(fechaInicio, fechaFin);
            var pagosRestaurante = ServicioRestaurantes.ObtenerPagosPorFecha(fechaInicio, fechaFin);
            var pagosConsumos = ServicioConsumosInternos.ObtenerPagosPorFecha(fechaInicio, fechaFin);

            reporte.VTRR_Restaurante = pagosComandas + pagosRestaurante + pagosConsumos;// +(ordenesDeTaxi.Count > 0 ? ordenesDeTaxi.Sum(m => m.Precio) : 0);

            var valoresPagosComandas = ServicioRentas.ObtenerPagosComandaPorFecha(fechaInicio, fechaFin);
            var valoresPagosRestaurante = ServicioRestaurantes.ObtenerDiccionarioPagosMesaPorFecha(fechaInicio, fechaFin);
            var valoresConsumosInternos = ServicioConsumosInternos.ObtenerDiccionarioPagosConsumoInternoPorFecha(fechaInicio, fechaFin);

            reporte.VTRR_Cortesias = valoresPagosComandas[TiposPago.Cortesia] + valoresPagosRestaurante[TiposPago.Cortesia] +
                                     valoresPagosComandas[TiposPago.Cupon] + valoresPagosRestaurante[TiposPago.Cupon];
            reporte.VTRR_Consumos = valoresPagosComandas[TiposPago.Consumo] + valoresPagosRestaurante[TiposPago.Consumo] + valoresConsumosInternos[TiposPago.Consumo];

            reporte.VTRR_Descuentos = valoresPagosComandas[TiposPago.VPoints] + valoresPagosRestaurante[TiposPago.VPoints];

            #endregion
            #region reservación

            var pagosReservacionDic = ServicioReservaciones.ObtenerDiccionarioPagosPorFecha(fechaInicio, fechaFin);

            reporte.VTRE_Reservaciones = corte.AnticiposReservas;

            reporte.VTRE_Cortesias = pagosReservacionDic[TiposPago.Cortesia] + pagosReservacionDic[TiposPago.Cupon];
            reporte.VTRE_ConsumosInternos = pagosReservacionDic[TiposPago.Consumo];
            reporte.VTRE_Descuentos = pagosReservacionDic[TiposPago.VPoints];


            //var totalReservacionesP = reporte.AnticipoReservas - cortesiasReservacion - descuentosReservacion - consumosReservacion;

            #endregion
            #region ingresos totales

            reporte.FormasPago = new List<DtoItemCobrableBase>();

            var pagosTarjetaRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaReservacionDic = ServicioReservaciones.ObtenerDiccionarioPagosTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaComandaDic = ServicioRoomServices.ObtenerDiccionarioPagosComandasTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaRestauranteDic = ServicioRestaurantes.ObtenerDiccionarioPagosMesaTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetaTV = ServicioVPoints.ObtenerDiccionarioPagosTarjetaPorFecha(fechaInicio, fechaFin);
            var pagosTarjetasPuntosDic = ServicioVPoints.ObtenerDiccionarioPagosPorFecha(fechaInicio, fechaFin);

            decimal totalVMC = 0;

            foreach (var tipoTarjeta in EnumExtensions.ComoLista<TiposTarjeta>())
            {
                var propinasTipo = valoresPropinas.Where(m => m.TipoTarjeta == tipoTarjeta).Select(m => m.Valor).ToList();

                if (tipoTarjeta == TiposTarjeta.Visa || tipoTarjeta == TiposTarjeta.MasterCard)
                    totalVMC += pagosTarjetaRentaDic[tipoTarjeta] + pagosTarjetaRestauranteDic[tipoTarjeta] + pagosTarjetaComandaDic[tipoTarjeta] + pagosTarjetaTV[tipoTarjeta] + pagosTarjetaReservacionDic[tipoTarjeta] +
                            (propinasTipo.Count > 0 ? propinasTipo.Sum(m => m) : 0);
                else
                    reporte.FormasPago.Add(new DtoItemCobrableBase
                    {
                        Nombre = tipoTarjeta.Descripcion().ToUpper(),
                        Total = pagosTarjetaRentaDic[tipoTarjeta] + pagosTarjetaRestauranteDic[tipoTarjeta] + pagosTarjetaComandaDic[tipoTarjeta] + pagosTarjetaTV[tipoTarjeta] + pagosTarjetaReservacionDic[tipoTarjeta] +
                                (propinasTipo.Count > 0 ? propinasTipo.Sum(m => m) : 0)
                    });
            }

            reporte.FormasPago.Insert(0, new DtoItemCobrableBase
            {
                Nombre = "TARJETAS DE CRÉDITO VISA Y MASTER CARD",
                Total = totalVMC
            });

            var trans = pagosRentaDic[TiposPago.Transferencia] + valoresPagosComandas[TiposPago.Transferencia] + valoresPagosRestaurante[TiposPago.Transferencia] +
                        valoresConsumosInternos[TiposPago.Transferencia] + pagosReservacionDic[TiposPago.Transferencia] + pagosTarjetasPuntosDic[TiposPago.Transferencia];

            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = "TRANSFERENCIAS",
                Total = trans
            });

            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = "CONSUMO INTERNO (HABITACIONES, ROOM SERVICE, RESTAURANTE)",
                Total = corte.ConsumoHabitacion + corte.ConsumoRestaurante + reporte.VTRE_ConsumosInternos
            });

            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = "CORTESÍAS (HABITACIONES, ROOM SERVICE, RESTAURANTE)",
                Total = corte.CortesiasHabitacion + corte.CortesiasRestautante + corte.CortesiasRoomService + reporte.VTRE_Cortesias
            });

            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = "ANTICIPOS VÁLIDOS POR CONSUMO",
                Total = corte.ReservasValidas
            });

            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = "DESCUENTOS",
                Total = corte.DescuentosHabitacion + corte.DescuentosRestaurante + corte.DescuentosRoomService + reporte.VTRE_Descuentos
            });

            reporte.FormasPago.Add(new DtoItemCobrableBase
            {
                Nombre = "REPORTE DE GASTOS EN TURNO",
                Total = corte.Gastos
            });

            //if (corte.PagoNetoPropinas.HasValue)
            reporte.PagoNetoPropinas = corte.PagoNetoPropinas.Value;
            //else
            //{
            //    var pagoNetoMeseros = ((IServicioCortesTurnoInterno)this).ObtenerPagoNetoPropinasMeseros(corte);
            //    var pagoNetoValets = ((IServicioCortesTurnoInterno)this).ObtenerPagoNetoPropinasValets(corte);

            //    reporte.PagoNetoPropinas = -(pagoNetoMeseros.TotalNegativo + pagoNetoValets.TotalNegativo);
            //}

            //if (corte.PropinasPagadas.HasValue)
            reporte.PropinasPagadas = corte.PropinasPagadas.Value;
            //else
            //{
            //    var propinasPagadas = ((IServicioCortesTurnoInterno)this).ObtenerPropinasPagadasMeseros(corte);
            //    propinasPagadas += ((IServicioCortesTurnoInterno)this).ObtenerPropinasPagadasValets(corte);

            //    reporte.PropinasPagadas = propinasPagadas;
            //}
            #endregion


            reporte.Desglose = ServicioFajillas.ObtenerDesgloseEfectivo(corte.Id);
            reporte.CantidadFajillas = ServicioFajillas.ObtenerCantidadFajillasAutorizadas(corte.Id);
            reporte.TarjetasV = corte.TarjetasV;

            reporte.VentaDocumental = corte.Habitaciones + corte.PersonasExtra + corte.HospedajeExtra + corte.PropinasHabitacion + corte.Paquetes +
                                      corte.ServicioRestaurante + corte.PropinasRestaurante + corte.RoomService + corte.PropinasRoomService + corte.Taxis + corte.TarjetasV + corte.AnticiposReservas + (corte.MontosNoReembolsables ?? 0);

            var diferencia = (corte.Total - reporte.PropinasPagadas - corte.PagosTarjeta /*- corte.ReservasValidas*/ - trans) - reporte.TotalEfectivoCierre;

            reporte.EstatusDiferencia = diferencia > 0 ? "FALTANTE" : "SOBRANTE";

            reporte.Diferencia = Math.Abs(diferencia);

            var cancelacionesTurno = ServicioVentas.SP_ObtenerCancelacionesTurno(corte.Id);

            reporte.CancelacionesHabitacion = cancelacionesTurno.Where(m => m.ClasificacionVenta == Venta.ClasificacionesVenta.Habitacion).Sum(m => m.Cantidad);
            reporte.CancelacionesRoomRest = cancelacionesTurno.Where(m => m.ClasificacionVenta == Venta.ClasificacionesVenta.Restaurante || m.ClasificacionVenta == Venta.ClasificacionesVenta.RoomService).Sum(m => m.Cantidad);
            reporte.TotalCancelaciones = cancelacionesTurno.Where(m => m.ClasificacionVenta == Venta.ClasificacionesVenta.Habitacion || m.ClasificacionVenta == Venta.ClasificacionesVenta.Restaurante || m.ClasificacionVenta == Venta.ClasificacionesVenta.RoomService).Sum(m => m.Total);

            return reporte;
        }

        //public DtoReporteCierre ObtenerDatosRevisionPorFolio(DtoUsuario usuario, int folioCorte) 
        //{
        //    var corte = RepositorioCortesTurno.Obtener(m => m.NumeroCorte == folioCorte);

        //    return ObtenerDatosRevision(usuario, corte);
        //}

        //public DtoReporteCierre ObtenerDatosRevisionPorId(DtoUsuario usuario, int idCorte)
        //{
        //    var corte = RepositorioCortesTurno.Obtener(m => m.Id == idCorte);

        //    return ObtenerDatosRevision(usuario, corte);
        //}

        //private DtoReporteCierre ObtenerDatosRevision(DtoUsuario usuario, CorteTurno corte)
        //{
        //    //ProcesarDatosCorte(corte, corte.FechaCorte.Value, usuario);

        //    if (corte == null)
        //        throw new SOTException(Recursos.CortesTurno.numero_corte_invalido_excepcion, corte.ToString());

        //    var configGlobal = ServicioParametros.ObtenerParametros();

        //    var lineasArticulos = ServicioLineas.ObtenerLineas(true);

        //    DateTime fechaInicio = corte.FechaInicio;
        //    DateTime fechaFin = corte.FechaCorte ?? DateTime.Now;

        //    string folio = corte.NumeroCorte.ToString();

        //    var reporte = new DtoReporteCierre
        //    {
        //        Folio = folio,
        //        InicioReporte = fechaInicio,
        //        NombreUsuario = usuario.Alias,
        //        NombreNegocio = configGlobal.Nombre,
        //        Direccion = configGlobal.Direccion,
        //        FinReporte = fechaFin
        //    };


        //    #region habitaciones

        //    var detallesRenta = ServicioRentas.ObtenerDetallesPagosPorFecha(fechaInicio, fechaFin);
        //    var detallesHabitaciones = ServicioRentas.ObtenerDetallesPagosPorFechaYTipo(fechaInicio, fechaFin);

        //    reporte.ResumenesHabitaciones = new List<DtoItemCobrable>();

        //    foreach (var dh in detallesHabitaciones.Keys)
        //    {
        //        var detallesX = detallesHabitaciones[dh].Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion /*|| m.ConceptoPago == DetallePago.ConceptosPago.Renovacion*/).GroupBy(m => m.ValorConIVA / m.Cantidad);

        //        foreach (var detalle in detallesX)
        //        {
        //            reporte.ResumenesHabitaciones.Add(new DtoItemCobrable
        //            {
        //                Nombre = dh,
        //                Cantidad = detalle.Sum(m => m.Cantidad),
        //                PrecioUnidad = detalle.Key
        //            });

        //            reporte.VTH_Habitaciones += reporte.ResumenesHabitaciones.Last().Total;
        //        }
        //    }

        //    var pagosRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(fechaInicio, fechaFin);

        //    reporte.VTH_PersonasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA);
        //    reporte.VTH_HospedajeExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA);
        //    reporte.VTH_Paquetes = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes && m.ValorConIVA > 0).Sum(m => m.ValorConIVA);
        //    reporte.VTH_Descuentos = pagosRentaDic[TiposPago.VPoints] - detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.Paquetes && m.ValorConIVA < 0).Sum(m => m.ValorConIVA);

        //    reporte.VTH_Cortesias = pagosRentaDic[TiposPago.Cortesia] + pagosRentaDic[TiposPago.Cupon];
        //    reporte.VTH_ConsumosInternos = pagosRentaDic[TiposPago.Consumo];

        //    #endregion
        //    #region personas extra

        //    reporte.PersonasExtra = new List<DtoItemCobrable>();

        //    var personasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).ToList();

        //    foreach (var grupo in personasExtra.GroupBy(m => m.ValorConIVA / m.Cantidad).OrderBy(m => m.Key))
        //    {
        //        reporte.PersonasExtra.Add(new DtoItemCobrable
        //        {
        //            Cantidad = grupo.Sum(m => m.Cantidad),
        //            PrecioUnidad = grupo.Key
        //        });
        //    }

        //    #endregion
        //    #region hospedaje extra

        //    reporte.HospedajeExtra = new List<DtoItemCobrable>();

        //    var horasExtra = detallesRenta.Where(m => m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra || m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).ToList();

        //    foreach (var grupo in horasExtra.GroupBy(m => m.ValorConIVA / m.Cantidad).OrderBy(m => m.Key))
        //    {
        //        reporte.HospedajeExtra.Add(new DtoItemCobrable
        //        {
        //            Cantidad = grupo.Sum(m => m.Cantidad),
        //            PrecioUnidad = grupo.Key
        //        });
        //    }

        //    #endregion
        //    #region paquetes - descuentos

        //    reporte.Paquetes = new List<DtoItemCobrable>();
        //    reporte.Descuentos = new List<DtoItemCobrable>();

        //    var paquetes = ServicioRentas.ObtenerPaquetesRentaPorPeriodo(fechaInicio, fechaFin);

        //    foreach (var grupo in (from g in paquetes
        //                           group g by new { g.NombrePaquete, Precio = g.Precio, Descuento = g.Descuento } into gg
        //                           orderby gg.Key.NombrePaquete, gg.Key.Precio, gg.Key.Descuento
        //                           select gg))
        //    {
        //        if (grupo.First().ValorFinal > 0)
        //            reporte.Paquetes.Add(new DtoItemCobrable
        //            {
        //                Nombre = grupo.Key.NombrePaquete,
        //                Cantidad = grupo.Sum(m => m.Cantidad),
        //                PrecioUnidad = grupo.First().ValorFinal * (1 + configGlobal.Iva_CatPar)
        //            });
        //        else
        //            reporte.Descuentos.Add(new DtoItemCobrable
        //            {
        //                Nombre = grupo.Key.NombrePaquete,
        //                Cantidad = grupo.Count(),
        //                PrecioUnidad = grupo.First().ValorFinal * (1 + configGlobal.Iva_CatPar)
        //            });
        //    }

        //    #endregion
        //    #region taxis

        //    var ordenesTaxi = ServicioTaxis.ObtenerOrdenesCobradasPorPeriodo(fechaInicio, fechaFin);

        //    reporte.ResumenesTaxis = new List<DtoItemCobrable>();

        //    foreach (var grupo in (from orden in ordenesTaxi
        //                           group orden by orden.Precio into o
        //                           select o).OrderBy(m => m.Key))

        //        reporte.ResumenesTaxis.Add(new DtoItemCobrable
        //        {
        //            Cantidad = grupo.Count(),
        //            PrecioUnidad = grupo.Key
        //        });

        //    #endregion
        //    #region room service

        //    reporte.RoomService = new List<DtoItemCobrableBase>();

        //    var idsTipos = lineasArticulos.Select(m => m.Cod_Linea).ToList();

        //    var pagosArticulosComanda = ServicioRoomServices.ObtenerValorArticulosComandasPorPeriodoYLineas(idsTipos, new List<int>() { corte.Id });

        //    var totalesLineas = pagosArticulosComanda.GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configGlobal.Iva_CatPar));

        //    foreach (var configuracionTipo in lineasArticulos)
        //    {
        //        if (totalesLineas.ContainsKey(configuracionTipo.Cod_Linea))
        //        {
        //            reporte.RoomService.Add(new DtoItemCobrableBase
        //            {
        //                Nombre = configuracionTipo.Desc_Linea,
        //                Total = totalesLineas[configuracionTipo.Cod_Linea]
        //            });
        //        }
        //        else
        //            reporte.RoomService.Add(new DtoItemCobrableBase
        //            {
        //                Nombre = configuracionTipo.Desc_Linea,
        //                Total = 0
        //            });
        //    }

        //    #endregion
        //    #region restaurante

        //    reporte.Restaurante = new List<DtoItemCobrableBase>();

        //    var pagosArticulosOrdenRestaurante = ServicioRestaurantes.ObtenerValorArticulosOrdenesPorPeriodoYLineas(idsTipos, new List<int>() { corte.Id });
        //    pagosArticulosOrdenRestaurante.AddRange(ServicioConsumosInternos.ObtenerValorArticulosConsumosPorPeriodoYLineas(idsTipos, new List<int>() { corte.Id }));

        //    totalesLineas = pagosArticulosOrdenRestaurante.GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configGlobal.Iva_CatPar));

        //    foreach (var configuracionTipo in lineasArticulos)
        //    {
        //        if (totalesLineas.ContainsKey(configuracionTipo.Cod_Linea))
        //        {
        //            reporte.Restaurante.Add(new DtoItemCobrableBase
        //            {
        //                Nombre = configuracionTipo.Desc_Linea,
        //                Total = totalesLineas[configuracionTipo.Cod_Linea]
        //            });
        //        }
        //        else
        //            reporte.Restaurante.Add(new DtoItemCobrableBase
        //            {
        //                Nombre = configuracionTipo.Desc_Linea,
        //                Total = 0
        //            });
        //    }

        //    #endregion
        //    //#region room & rest

        //    //var pagosComandas = ServicioRoomServices.ObtenerValorPagosComandasPorFecha(fechaInicio, fechaFin);
        //    //var pagosRestaurante = ServicioRestaurantes.ObtenerPagosPorFecha(fechaInicio, fechaFin);
        //    //var pagosConsumos = ServicioConsumosInternos.ObtenerPagosPorFecha(fechaInicio, fechaFin);

        //    //reporte.VTRR_Restaurante = pagosComandas + pagosRestaurante + pagosConsumos;// +(ordenesDeTaxi.Count > 0 ? ordenesDeTaxi.Sum(m => m.Precio) : 0);

        //    //var valoresPagosComandas = ServicioRentas.ObtenerPagosComandaPorFecha(fechaInicio, fechaFin);
        //    //var valoresPagosRestaurante = ServicioRestaurantes.ObtenerDiccionarioPagosMesaPorFecha(fechaInicio, fechaFin);
        //    //var valoresConsumosInternos = ServicioConsumosInternos.ObtenerDiccionarioPagosConsumoInternoPorFecha(fechaInicio, fechaFin);

        //    //reporte.VTRR_Cortesias = valoresPagosComandas[TiposPago.Cortesia] + valoresPagosRestaurante[TiposPago.Cortesia] +
        //    //                         valoresPagosComandas[TiposPago.Cupon] + valoresPagosRestaurante[TiposPago.Cupon];
        //    //reporte.VTRR_Consumos = valoresPagosComandas[TiposPago.Consumo] + valoresPagosRestaurante[TiposPago.Consumo] + valoresConsumosInternos[TiposPago.Consumo];

        //    //reporte.VTRR_Descuentos = valoresPagosComandas[TiposPago.VPoints] + valoresPagosRestaurante[TiposPago.VPoints];

        //    //#endregion
        //    //#region reservación

        //    //var pagosReservacionDic = ServicioReservaciones.ObtenerDiccionarioPagosPorFecha(fechaInicio, fechaFin);

        //    //reporte.VTRE_Reservaciones = corte.AnticiposReservas;

        //    //reporte.VTRE_Cortesias = pagosReservacionDic[TiposPago.Cortesia] + pagosReservacionDic[TiposPago.Cupon];
        //    //reporte.VTRE_ConsumosInternos = pagosReservacionDic[TiposPago.Consumo];
        //    //reporte.VTRE_Descuentos = pagosReservacionDic[TiposPago.VPoints];


        //    //#endregion
        //    //#region ingresos totales

        //    //reporte.FormasPago = new List<DtoItemCobrableBase>();

        //    //var pagosTarjetaRentaDic = ServicioRentas.ObtenerDiccionarioPagosHabitacionTarjetaPorFecha(fechaInicio, fechaFin);
        //    //var pagosTarjetaReservacionDic = ServicioReservaciones.ObtenerDiccionarioPagosTarjetaPorFecha(fechaInicio, fechaFin);
        //    //var pagosTarjetaComandaDic = ServicioRoomServices.ObtenerDiccionarioPagosComandasTarjetaPorFecha(fechaInicio, fechaFin);
        //    //var pagosTarjetaRestauranteDic = ServicioRestaurantes.ObtenerDiccionarioPagosMesaTarjetaPorFecha(fechaInicio, fechaFin);
        //    //var pagosTarjetaTV = ServicioVPoints.ObtenerDiccionarioPagosTarjetaPorFecha(fechaInicio, fechaFin);
        //    //var pagosTarjetasPuntosDic = ServicioVPoints.ObtenerDiccionarioPagosPorFecha(fechaInicio, fechaFin);

        //    //decimal totalVMC = 0;

        //    //foreach (var tipoTarjeta in EnumExtensions.ComoLista<TiposTarjeta>())
        //    //{
        //    //    var propinasTipo = valoresPropinas.Where(m => m.TipoTarjeta == tipoTarjeta).Select(m => m.Valor).ToList();

        //    //    if (tipoTarjeta == TiposTarjeta.Visa || tipoTarjeta == TiposTarjeta.MasterCard)
        //    //        totalVMC += pagosTarjetaRentaDic[tipoTarjeta] + pagosTarjetaRestauranteDic[tipoTarjeta] + pagosTarjetaComandaDic[tipoTarjeta] + pagosTarjetaTV[tipoTarjeta] + pagosTarjetaReservacionDic[tipoTarjeta] +
        //    //                (propinasTipo.Count > 0 ? propinasTipo.Sum(m => m) : 0);
        //    //    else
        //    //        reporte.FormasPago.Add(new DtoItemCobrableBase
        //    //        {
        //    //            Nombre = tipoTarjeta.Descripcion().ToUpper(),
        //    //            Total = pagosTarjetaRentaDic[tipoTarjeta] + pagosTarjetaRestauranteDic[tipoTarjeta] + pagosTarjetaComandaDic[tipoTarjeta] + pagosTarjetaTV[tipoTarjeta] + pagosTarjetaReservacionDic[tipoTarjeta] +
        //    //                    (propinasTipo.Count > 0 ? propinasTipo.Sum(m => m) : 0)
        //    //        });
        //    //}

        //    //reporte.FormasPago.Insert(0, new DtoItemCobrableBase
        //    //{
        //    //    Nombre = "TARJETAS DE CRÉDITO VISA Y MASTER CARD",
        //    //    Total = totalVMC
        //    //});

        //    //var trans = pagosRentaDic[TiposPago.Transferencia] + valoresPagosComandas[TiposPago.Transferencia] + valoresPagosRestaurante[TiposPago.Transferencia] +
        //    //            valoresConsumosInternos[TiposPago.Transferencia] + pagosReservacionDic[TiposPago.Transferencia] + pagosTarjetasPuntosDic[TiposPago.Transferencia];

        //    //reporte.FormasPago.Add(new DtoItemCobrableBase
        //    //{
        //    //    Nombre = "TRANSFERENCIAS",
        //    //    Total = trans
        //    //});

        //    //reporte.FormasPago.Add(new DtoItemCobrableBase
        //    //{
        //    //    Nombre = "CONSUMO INTERNO (HABITACIONES, ROOM SERVICE, RESTAURANTE)",
        //    //    Total = corte.ConsumoHabitacion + corte.ConsumoRestaurante + reporte.VTRE_ConsumosInternos
        //    //});

        //    //reporte.FormasPago.Add(new DtoItemCobrableBase
        //    //{
        //    //    Nombre = "CORTESÍAS (HABITACIONES, ROOM SERVICE, RESTAURANTE)",
        //    //    Total = corte.CortesiasHabitacion + corte.CortesiasRestautante + corte.CortesiasRoomService + reporte.VTRE_Cortesias
        //    //});

        //    //reporte.FormasPago.Add(new DtoItemCobrableBase
        //    //{
        //    //    Nombre = "ANTICIPOS VÁLIDOS POR CONSUMO",
        //    //    Total = corte.ReservasValidas
        //    //});

        //    //reporte.FormasPago.Add(new DtoItemCobrableBase
        //    //{
        //    //    Nombre = "DESCUENTOS",
        //    //    Total = corte.DescuentosHabitacion + corte.DescuentosRestaurante + corte.DescuentosRoomService + reporte.VTRE_Descuentos
        //    //});

        //    //reporte.FormasPago.Add(new DtoItemCobrableBase
        //    //{
        //    //    Nombre = "REPORTE DE GASTOS EN TURNO",
        //    //    Total = corte.Gastos
        //    //});


        //    //reporte.PagoNetoPropinas = corte.PagoNetoPropinas.Value;
        //    //reporte.PropinasPagadas = corte.PropinasPagadas.Value;

        //    //#endregion


        //    //reporte.Desglose = ServicioFajillas.ObtenerDesgloseEfectivo(corte.Id);
        //    //reporte.CantidadFajillas = ServicioFajillas.ObtenerCantidadFajillasAutorizadas(corte.Id);
        //    //reporte.TarjetasV = corte.TarjetasV;

        //    //reporte.VentaDocumental = corte.Habitaciones + corte.PersonasExtra + corte.HospedajeExtra + corte.PropinasHabitacion + corte.Paquetes +
        //    //                          corte.ServicioRestaurante + corte.PropinasRestaurante + corte.RoomService + corte.PropinasRoomService + corte.Taxis + corte.TarjetasV + corte.AnticiposReservas;

        //    //var diferencia = (corte.Total - reporte.PropinasPagadas - corte.PagosTarjeta - corte.ReservasValidas - trans) - reporte.TotalEfectivoCierre;

        //    //reporte.EstatusDiferencia = diferencia > 0 ? "FALTANTE" : "SOBRANTE";

        //    //reporte.Diferencia = Math.Abs(diferencia);

        //    return reporte;
        //}


        //public List<DtoCorteTurno> ObtenerResumenesCortesFinalizadosPorRango(DateTime? fechaInicial, DateTime? fechaFinal, DtoUsuario usuario)
        //{
        //    var resumenes = RepositorioCortesTurno.ObtenerResumenesCortesFinalizadosPorRango(fechaInicial, fechaFinal);

        //    foreach (var grupo in resumenes.GroupBy(m => m.IdUsuarioCerro)) 
        //    {
        //        var alias = grupo.Key.HasValue ? ServicioEmpleados.ObtenerNombreCompletoEmpleado(grupo.Key.Value) : "";

        //        foreach (var resumen in grupo) 
        //        {
        //            resumen.Recepcionista = alias;
        //        }
        //    }

        //    return resumenes;
        //}

        public List<DtoAreas> ObtieneAreas(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { RevisarCorteTurno = true });

            return  RepositorioCortesTurno.ObtieneAreas();
        }
		
        public List<DtoCorteTurno> ObtenerResumenesCortesPorRango(DateTime? fechaInicial, DateTime? fechaFinal, DtoUsuario usuario)
        {
            var resumenes = RepositorioCortesTurno.ObtenerResumenesCortesPorRango(fechaInicial, fechaFinal);

            foreach (var grupo in resumenes.GroupBy(m => m.IdUsuarioCerro))
            {
                var alias = grupo.Key.HasValue ? ServicioEmpleados.ObtenerNombreCompletoEmpleado(grupo.Key.Value) : "";

                foreach (var resumen in grupo)
                {
                    resumen.Recepcionista = alias;
                }
            }

            return resumenes;
        }
        public int ObtenerFolioCortePorId(int idCorte)
        {
            return RepositorioCortesTurno.ObtenerElementos(m => m.Id == idCorte).Select(m => m.NumeroCorte).FirstOrDefault();
        }

        public ConfiguracionTurno ObtenerSiguienteConfiguracionTurno(int ordenBase)
        {
            var siguiente = RepositorioConfiguracionesTurno.ObtenerElementos(m => m.Orden > ordenBase && m.Activa).OrderBy(m => m.Orden).FirstOrDefault();

            if(siguiente == null)
                siguiente = RepositorioConfiguracionesTurno.ObtenerTodo().OrderBy(m => m.Orden).FirstOrDefault();

            return siguiente;
        }

        public List<CorteTurno> ObtenerCortesNoSincronizados() 
        {
            var idEstadoCerrado = (int)CorteTurno.Estados.Cerrado;

            return RepositorioCortesTurno.ObtenerElementos(m => m.IdEstado == idEstadoCerrado && !m.Sincronizado, m => m.ConfiguracionTurno).ToList();
        }

        #region métodos internal

        string IServicioCortesTurnoInterno.ObtenerNombreTurno(int idCorteTurno) 
        {
            return RepositorioConfiguracionesTurno.ObtenerNombreTurnoPorCorte(idCorteTurno);
        }

        decimal IServicioCortesTurnoInterno.ObtenerPropinasPagadasMeseros(CorteTurno corte)
        {
            //var corte = RepositorioCortesTurno.Obtener(m => m.Id == idCorte);

            if (corte == null)
                return 0;

            var fechaInicio = corte.FechaInicio;
            var fechaFin = corte.FechaCorte;

            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            var configuracion = ServicioConfiguracionesPropinas.ObtenerConfiguracionPropinas();
            var configuracionGlobal = ServicioParametros.ObtenerParametros();

            var resumenComisiones = new DtoComisionesMesero();

            var idsLineas = configuracion.PorcentajesPropinaTipo.Select(m => m.IdLineaArticulo).Distinct().ToList();

#warning revisar si se agregan los consumos internos
            var pagosArticulosPorTipo = ServicioRoomServices.ObtenerValorArticulosComandasPorPeriodoYLineas(idsLineas, new List<int>() { corte.Id });
            pagosArticulosPorTipo.AddRange(ServicioRestaurantes.ObtenerValorArticulosOrdenesPorPeriodoYLineas(idsLineas, new List<int>() { corte.Id }));

            var meseros = ServicioEmpleados.ObtenerMeseros();
            var propinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin, Propina.TiposPropina.Comanda, Propina.TiposPropina.Restaurante);

            var gruposPropinas = meseros.ToDictionary(m => m.Id, m => m.NombreCompleto);

            foreach (var propina in propinas)
            {
                if (!gruposPropinas.ContainsKey(propina.IdEmpleado))
                    gruposPropinas.Add(propina.IdEmpleado, propina.Empleado.NombreCompleto);
            }

            foreach (var pago in pagosArticulosPorTipo)
            {
                if (!gruposPropinas.ContainsKey(pago.IdEmpleado))
                    gruposPropinas.Add(pago.IdEmpleado, pago.Nombre);
            }

            Dictionary<int, decimal[]> valoresResumen = new Dictionary<int, decimal[]>();

            #region comisiones de propinas

            Dictionary<string, Type> columnasPropinas = new Dictionary<string, Type>();

            columnasPropinas.Add("MESERO", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                columnasPropinas.Add(configuracionTipo.TipoTarjeta.Descripcion().ToUpper(), typeof(decimal));

            columnasPropinas.Add("COMISIÓN TARJETA", typeof(decimal));

            //var gruposPropinas = propinas.GroupBy(m => m.Empleado.NombreCompleto).ToList();

            object[][] datosPropinas = new object[gruposPropinas.Count][];

            int i = 0;
            foreach (var propinasMesero in gruposPropinas)
            {
                datosPropinas[i] = new object[columnasPropinas.Count];

                int j = 0;

                datosPropinas[i][j++] = propinasMesero.Key;//.First().Empleado.NombreCompleto;
#warning contemplar que en un futuro se puede quitar algun tipo de tarjeta y se deben contemplar las propinas de este tipo eliminado
                var totalesTipo = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));

                decimal reembolso = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.TipoTarjeta))
                    {
                        datosPropinas[i][j++] = totalesTipo[configuracionTipo.TipoTarjeta];

                        reembolso += Math.Round(totalesTipo[configuracionTipo.TipoTarjeta] * configuracionTipo.Porcentaje);
                    }
                    else
                        datosPropinas[i][j++] = 0;
                }

                datosPropinas[i][j++] = reembolso;

                i++;

                if (!valoresResumen.ContainsKey(propinasMesero.Key))
                    valoresResumen.Add(propinasMesero.Key, new decimal[3]);

                valoresResumen[propinasMesero.Key][0] = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).Sum(m => m.Valor);
                valoresResumen[propinasMesero.Key][2] = reembolso;
            }

            resumenComisiones.DatosPropinasTarjetas = DataExtensions.GetDataTable(columnasPropinas, datosPropinas);

            #endregion
            
            #region resumen

            decimal propinaNeta = 0;

            foreach (var key in valoresResumen.Keys)
            {
                propinaNeta += Math.Round(valoresResumen[key][0] /*- valoresResumen[key][1]*/ - valoresResumen[key][2]);

                i++;
            }

            //resumenComisiones.DatosResumen = DataExtensions.GetDataTable(columnasResumen, valoresResumenArray);

            #endregion

            return propinaNeta;
        }

        decimal IServicioCortesTurnoInterno.ObtenerPropinasPagadasValets(CorteTurno corte)
        {
            if (corte == null)
                return 0;

            var fechaInicio = corte.FechaInicio;
            var fechaFin = corte.FechaCorte;

            var configuracion = ServicioConfiguracionesPropinas.ObtenerConfiguracionPropinas();

            var valets = ServicioEmpleados.ObtenerValets();
            var propinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin, Propina.TiposPropina.Valet);

            var diccionarioValets = valets.ToDictionary(m => m.Id, m => m.NombreCompleto);

            foreach (var propina in propinas)
            {
                if (!diccionarioValets.ContainsKey(propina.IdEmpleado))
                    diccionarioValets.Add(propina.IdEmpleado, propina.Empleado.NombreCompleto);
            }

            var resumenComisiones = new DtoComisionesValets();

            Dictionary<int, decimal[]> valoresResumen = new Dictionary<int, decimal[]>();

            #region comisiones de propinas

            Dictionary<string, Type> columnasPropinas = new Dictionary<string, Type>();

            columnasPropinas.Add("VALET", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                columnasPropinas.Add(configuracionTipo.TipoTarjeta.Descripcion().ToUpper(), typeof(decimal));

            columnasPropinas.Add("COMISIÓN TARJETA", typeof(decimal));

            //var gruposPropinas = propinas.GroupBy(m => m.Empleado.NombreCompleto).ToList();

            object[][] datosPropinas = new object[diccionarioValets.Count][];

            int i = 0;
            foreach (var propinasMesero in diccionarioValets)
            {
                datosPropinas[i] = new object[columnasPropinas.Count];

                int j = 0;

                datosPropinas[i][j++] = propinasMesero.Key;//.First().Empleado.NombreCompleto;


                var totalesTipo = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));
                //propinasMesero.GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));

                decimal reembolso = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.TipoTarjeta))
                    {
                        datosPropinas[i][j++] = totalesTipo[configuracionTipo.TipoTarjeta];

                        reembolso += Math.Round(totalesTipo[configuracionTipo.TipoTarjeta] * configuracionTipo.Porcentaje);
                    }
                    else
                        datosPropinas[i][j++] = 0;
                }

                datosPropinas[i][j++] = reembolso;

                i++;

                if (!valoresResumen.ContainsKey(propinasMesero.Key))
                    valoresResumen.Add(propinasMesero.Key, new decimal[3]);

                valoresResumen[propinasMesero.Key][0] = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).Sum(m => m.Valor);
                valoresResumen[propinasMesero.Key][2] = reembolso;
            }

            resumenComisiones.DatosPropinasTarjetas = DataExtensions.GetDataTable(columnasPropinas, datosPropinas);

            #endregion
            
            #region resumen

            decimal propinaNeta = 0;

            foreach (var key in valoresResumen.Keys)
            {
                propinaNeta += Math.Round(valoresResumen[key][0] -/* valoresResumen[key][1] - */valoresResumen[key][2]);

                i++;
            }

            #endregion

            return propinaNeta;
        }

        DtoTotalesPropinas IServicioCortesTurnoInterno.ObtenerPagoNetoPropinasMeseros(CorteTurno corte)
        {
            //var corte = RepositorioCortesTurno.Obtener(m => m.Id == idCorte);

            if (corte == null)
                return null;

            var fechaInicio = corte.FechaInicio;
            var fechaFin = corte.FechaCorte;

            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            var configuracion = ServicioConfiguracionesPropinas.ObtenerConfiguracionPropinas();
            var configuracionGlobal = ServicioParametros.ObtenerParametros();

            var totalesP = new DtoTotalesPropinas();

            var idsLineas = configuracion.PorcentajesPropinaTipo.Select(m => m.IdLineaArticulo).Distinct().ToList();

#warning revisar si se deben incluir los consumos internos
            var pagosArticulosPorTipo = ServicioRoomServices.ObtenerValorArticulosComandasPorPeriodoYLineas(idsLineas, new List<int>() { corte.Id });
            pagosArticulosPorTipo.AddRange(ServicioRestaurantes.ObtenerValorArticulosOrdenesPorPeriodoYLineas(idsLineas, new List<int>() { corte.Id }));

            var meseros = ServicioEmpleados.ObtenerMeseros();
            var propinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin, Propina.TiposPropina.Comanda, Propina.TiposPropina.Restaurante);

            var gruposPropinas = meseros.ToDictionary(m => m.Id, m => m.NombreCompleto);

            foreach (var propina in propinas)
            {
                if (!gruposPropinas.ContainsKey(propina.IdEmpleado))
                    gruposPropinas.Add(propina.IdEmpleado, propina.Empleado.NombreCompleto);
            }

            foreach (var pago in pagosArticulosPorTipo)
            {
                if (!gruposPropinas.ContainsKey(pago.IdEmpleado))
                    gruposPropinas.Add(pago.IdEmpleado, pago.Nombre);
            }

            Dictionary<int, decimal[]> valoresResumen = new Dictionary<int, decimal[]>();

            #region comisiones de propinas

            Dictionary<string, Type> columnasPropinas = new Dictionary<string, Type>();

            columnasPropinas.Add("MESERO", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                columnasPropinas.Add(configuracionTipo.TipoTarjeta.Descripcion().ToUpper(), typeof(decimal));

            columnasPropinas.Add("COMISIÓN TARJETA", typeof(decimal));

            //var gruposPropinas = propinas.GroupBy(m => m.Empleado.NombreCompleto).ToList();

            object[][] datosPropinas = new object[gruposPropinas.Count][];

            int i = 0;
            foreach (var propinasMesero in gruposPropinas)
            {
                datosPropinas[i] = new object[columnasPropinas.Count];

                int j = 0;

                datosPropinas[i][j++] = propinasMesero.Value;//.First().Empleado.NombreCompleto;
#warning contemplar que en un futuro se puede quitar algun tipo de tarjeta y se deben contemplar las propinas de este tipo eliminado
                var totalesTipo = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));

                decimal reembolso = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.TipoTarjeta))
                    {
                        datosPropinas[i][j++] = totalesTipo[configuracionTipo.TipoTarjeta];

                        reembolso += Math.Round(totalesTipo[configuracionTipo.TipoTarjeta] * configuracionTipo.Porcentaje);
                    }
                    else
                        datosPropinas[i][j++] = 0;
                }

                datosPropinas[i][j++] = reembolso;

                i++;

                if (!valoresResumen.ContainsKey(propinasMesero.Key))
                    valoresResumen.Add(propinasMesero.Key, new decimal[3]);

                valoresResumen[propinasMesero.Key][0] = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).Sum(m => m.Valor);
                valoresResumen[propinasMesero.Key][2] = reembolso;
            }

            //resumenComisiones.DatosPropinasTarjetas = DataExtensions.GetDataTable(columnasPropinas, datosPropinas);

            #endregion
            #region comisiones por artículos



            Dictionary<string, Type> columnasArticulos = new Dictionary<string, Type>();

            columnasArticulos.Add("MESERO", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesPropinaTipo)
                columnasArticulos.Add(configuracionTipo.LineaTmp.Desc_Linea.ToUpper(), typeof(decimal));

            columnasArticulos.Add("PUNTOS A PAGAR", typeof(decimal));

            //var pagosEmpleado = pagosArticulosPorTipo.GroupBy(m => m.Nombre).ToList();

            object[][] datosPagos = new object[gruposPropinas.Count][];

            i = 0;
            foreach (var pagoMesero in gruposPropinas)
            {
                datosPagos[i] = new object[columnasArticulos.Count];

                int j = 0;

                datosPagos[i][j++] = pagoMesero.Value;

                var totalesTipo = pagosArticulosPorTipo.Where(m => m.IdEmpleado == pagoMesero.Key).GroupBy(m => m.IdLineaArticulo).ToDictionary(m => m.Key, m => m.Sum(e => e.ValorConIVA));// * (1 + configuracionGlobal.Iva_CatPar));

                decimal puntosPagar = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesPropinaTipo)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.IdLineaArticulo))
                    {
                        datosPagos[i][j++] = totalesTipo[configuracionTipo.IdLineaArticulo];

                        puntosPagar += Math.Round(totalesTipo[configuracionTipo.IdLineaArticulo] * configuracionTipo.Porcentaje);
                    }
                    else
                        datosPagos[i][j++] = 0;
                }

                datosPagos[i][j++] = puntosPagar;

                i++;

                if (!valoresResumen.ContainsKey(pagoMesero.Key))
                    valoresResumen.Add(pagoMesero.Key, new decimal[3]);

                valoresResumen[pagoMesero.Key][1] = puntosPagar;
            }

            //resumenComisiones.DatosPagosArticulos = DataExtensions.GetDataTable(columnasArticulos, datosPagos);

            #endregion
            #region resumen

            var columnasResumen = new Dictionary<string, Type>();
            columnasResumen.Add("MESERO", typeof(string));
            columnasResumen.Add("PROPINAS", typeof(decimal));
            columnasResumen.Add("PUNTOS A PAGAR", typeof(decimal));
            columnasResumen.Add("COMISIÓN TARJETA", typeof(decimal));
            columnasResumen.Add("TOTALES", typeof(decimal));

            object[][] valoresResumenArray = new object[valoresResumen.Count][];

            i = 0;
            foreach (var key in valoresResumen.Keys)
            {
                var empleado = meseros.FirstOrDefault(m => m.Id == key) ?? propinas.Where(m => m.IdEmpleado == key).Select(m => m.Empleado).FirstOrDefault();

                valoresResumenArray[i] = new object[5];

                valoresResumenArray[i][0] = empleado != null ? empleado.NombreCompleto : (pagosArticulosPorTipo.Any(m => m.IdEmpleado == key) ? pagosArticulosPorTipo.FirstOrDefault(m => m.IdEmpleado == key).Nombre : "");
                valoresResumenArray[i][1] = valoresResumen[key][0];
                valoresResumenArray[i][2] = valoresResumen[key][1];
                valoresResumenArray[i][3] = valoresResumen[key][2];
                var total = Math.Round(valoresResumen[key][0] - valoresResumen[key][1] - valoresResumen[key][2]);
                valoresResumenArray[i][4] = total;

                if (total > 0)
                    totalesP.TotalPositivo += total;
                else
                    totalesP.TotalNegativo -= total;

                i++;
            }

            //resumenComisiones.DatosResumen = DataExtensions.GetDataTable(columnasResumen, valoresResumenArray);

            #endregion

            return totalesP;
        }

        DtoTotalesPropinas IServicioCortesTurnoInterno.ObtenerPagoNetoPropinasValets(CorteTurno corte)
        {
            //var corte = RepositorioCortesTurno.Obtener(m => m.Id == idCorte);

            if (corte == null)
                return null;

#warning de momento se pone así por limitaciones operativas del cliente
            return new DtoTotalesPropinas();

            var fechaInicio = corte.FechaInicio;
            var fechaFin = corte.FechaCorte;

            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosCorte = true });

            var configuracion = ServicioConfiguracionesPropinas.ObtenerConfiguracionPropinas();

            var valets = ServicioEmpleados.ObtenerValets();
            var propinas = ServicioPropinas.ObtenerPropinasPorPeriodo(fechaInicio, fechaFin, Propina.TiposPropina.Valet);

            var diccionarioValets = valets.ToDictionary(m => m.Id, m => m.NombreCompleto);

            foreach (var propina in propinas)
            {
                if (!diccionarioValets.ContainsKey(propina.IdEmpleado))
                    diccionarioValets.Add(propina.IdEmpleado, propina.Empleado.NombreCompleto);
            }

            var resumenComisiones = new DtoComisionesValets();

            Dictionary<int, decimal[]> valoresResumen = new Dictionary<int, decimal[]>();

            #region comisiones de propinas

            Dictionary<string, Type> columnasPropinas = new Dictionary<string, Type>();

            columnasPropinas.Add("VALET", typeof(string));

            foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                columnasPropinas.Add(configuracionTipo.TipoTarjeta.Descripcion().ToUpper(), typeof(decimal));

            columnasPropinas.Add("COMISIÓN TARJETA", typeof(decimal));

            //var gruposPropinas = propinas.GroupBy(m => m.Empleado.NombreCompleto).ToList();

            object[][] datosPropinas = new object[diccionarioValets.Count][];

            int i = 0;
            foreach (var propinasMesero in diccionarioValets)
            {
                datosPropinas[i] = new object[columnasPropinas.Count];

                int j = 0;

                datosPropinas[i][j++] = propinasMesero.Value;//.First().Empleado.NombreCompleto;


                var totalesTipo = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));
                //propinasMesero.GroupBy(m => m.TipoTarjeta).ToDictionary(m => m.Key, m => m.Sum(e => e.Valor));

                decimal reembolso = 0;

                foreach (var configuracionTipo in configuracion.PorcentajesTarjeta)
                {
                    if (totalesTipo.ContainsKey(configuracionTipo.TipoTarjeta))
                    {
                        datosPropinas[i][j++] = totalesTipo[configuracionTipo.TipoTarjeta];

                        reembolso += totalesTipo[configuracionTipo.TipoTarjeta] * configuracionTipo.Porcentaje;
                    }
                    else
                        datosPropinas[i][j++] = 0;
                }

                datosPropinas[i][j++] = reembolso;

                i++;

                if (!valoresResumen.ContainsKey(propinasMesero.Key))
                    valoresResumen.Add(propinasMesero.Key, new decimal[3]);

                valoresResumen[propinasMesero.Key][0] = propinas.Where(m => m.IdEmpleado == propinasMesero.Key).Sum(m => m.Valor);
                valoresResumen[propinasMesero.Key][2] = reembolso;
            }

            resumenComisiones.DatosPropinasTarjetas = DataExtensions.GetDataTable(columnasPropinas, datosPropinas);

            #endregion
            #region fondos a pagar

            Dictionary<string, Type> columnasArticulos = new Dictionary<string, Type>();

            columnasArticulos.Add("VALETS", typeof(string));

            columnasArticulos.Add("FONDOS A PAGAR", typeof(decimal));

            var fechaActual = fechaInicio;// ?? DateTime.Now;

            var diaActual = ((int)fechaActual.DayOfWeek) == 0 ? 7 : (int)fechaActual.DayOfWeek;

            var dia = configuracion.FondosDia.FirstOrDefault(m => m.Dia == diaActual);

            decimal pago = valets.Count == 0 ? 0 : (dia != null ? dia.Precio : 0) / valets.Count;

            object[][] datosPagos = new object[valets.Count][];

            i = 0;
            foreach (var valet in valets)
            {
                datosPagos[i] = new object[columnasArticulos.Count];

                int j = 0;

                datosPagos[i][j++] = valet.NombreCompleto;

                datosPagos[i][j++] = pago;

                i++;

                if (!valoresResumen.ContainsKey(valet.Id))
                    valoresResumen.Add(valet.Id, new decimal[3]);

                valoresResumen[valet.Id][1] = pago;
            }

            resumenComisiones.DatosFondosPagar = DataExtensions.GetDataTable(columnasArticulos, datosPagos);

            #endregion
            #region resumen

            var columnasResumen = new Dictionary<string, Type>();
            columnasResumen.Add("VALET", typeof(string));
            columnasResumen.Add("PROPINAS", typeof(decimal));
            columnasResumen.Add("FONDOS A PAGAR", typeof(decimal));
            columnasResumen.Add("COMISIÓN TARJETA", typeof(decimal));
            columnasResumen.Add("TOTALES", typeof(decimal));

            object[][] valoresResumenArray = new object[valoresResumen.Count][];

            i = 0;
            foreach (var key in valoresResumen.Keys)
            {
                var empleado = valets.FirstOrDefault(m => m.Id == key) ?? propinas.Where(m => m.IdEmpleado == key).Select(m => m.Empleado).FirstOrDefault();

                valoresResumenArray[i] = new object[5];

                valoresResumenArray[i][0] = empleado != null ? empleado.NombreCompleto : "";
                valoresResumenArray[i][1] = valoresResumen[key][0];
                valoresResumenArray[i][2] = valoresResumen[key][1];
                valoresResumenArray[i][3] = valoresResumen[key][2];
                valoresResumenArray[i][4] = valoresResumen[key][0] - valoresResumen[key][1] - valoresResumen[key][2];

                i++;
            }

            resumenComisiones.DatosResumen = DataExtensions.GetDataTable(columnasResumen, valoresResumenArray);

            #endregion

            //return resumenComisiones;
        }

        CorteTurno IServicioCortesTurnoInterno.ObtenerCorteIncluyeFecha(DateTime fechaFiltroTurno)
        {
            return RepositorioCortesTurno.Obtener(m => m.FechaInicio <= fechaFiltroTurno && (!m.FechaCorte.HasValue || m.FechaCorte >= fechaFiltroTurno));
        }

        #endregion

        public List<DtoResumenCorteTurno> ObtenerResumenesCortesEnRevision(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { RevisarCorteTurno = true });

            return RepositorioCortesTurno.ObtenerResumenesCortesEnRevision();
        }

        bool IServicioCortesTurnoInterno.VerificarExistenTurnos()
        {
            return RepositorioCortesTurno.Alguno();
        }

        public void AbrirPrimerTurno(int idConfiguracionTurno, DtoUsuario usuario, DateTime? fechaInicial)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { RealizarCorteTurno = true });

            if (RepositorioCortesTurno.Alguno())
                throw new SOTException(Recursos.CortesTurno.cortes_existentes_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaInicial.HasValue && fechaInicial > fechaActual)
                throw new SOTException(Recursos.CortesTurno.fecha_inicio_mayor_fecha_actual_excepcion);

            var parametros = AplicacionServicioConfiguracionesSistemas.ObtenerParametros(IdentificadoresSistemas.CONFIGURADOR_SOT);

            var parametrosConfig = new ParametrosConfigurador();

            foreach (var propiedad in typeof(ParametrosConfigurador).GetProperties())
            {
                var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                if (parametro != null)
                {
                    propiedad.SetValue(parametrosConfig, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                }
            }

            if (!parametrosConfig.ConfiguracionFinalizada)
                throw new SOTException(Recursos.CortesTurno.sucursal_en_configuracion_excepcion);

            var corte = new CorteTurno
            {
                FechaInicio = fechaInicial ?? DateTime.Now,
                Estado = CorteTurno.Estados.Abierto,
                IdConfiguracionTurno = idConfiguracionTurno,
                NumeroCorte = 1
            };

            RepositorioCortesTurno.Agregar(corte);
            RepositorioCortesTurno.GuardarCambios();
        }

        public bool VerificarExistenTurnos(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { RealizarCorteTurno = true });

            return RepositorioCortesTurno.Alguno();
        }
    }
}
