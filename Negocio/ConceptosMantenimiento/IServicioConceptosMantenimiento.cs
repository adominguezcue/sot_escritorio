﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConceptosMantenimiento
{
    public interface IServicioConceptosMantenimiento
    {
        List<ConceptoMantenimiento> ObtenerConceptos();

        List<ConceptoMantenimiento.Clasificaciones> ObtenerClasificaciones();

        void CrearConcepto(ConceptoMantenimiento conceptoMantenimiento, DtoUsuario usuario);

        void ModificarConcepto(ConceptoMantenimiento conceptoMantenimiento, DtoUsuario usuario);

        void EliminarConcepto(int idConceptoMantenimiento, DtoUsuario usuario);
    }
}
