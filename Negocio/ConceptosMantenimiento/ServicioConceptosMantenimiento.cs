﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace Negocio.ConceptosMantenimiento
{
    public class ServicioConceptosMantenimiento : IServicioConceptosMantenimientoInterno
    {
        IRepositorioConceptosMantenimiento RepositorioConceptosMantenimiento
        {
            get { return _repositorioConceptosMantenimiento.Value; }
        }

        Lazy<IRepositorioConceptosMantenimiento> _repositorioConceptosMantenimiento = new Lazy<IRepositorioConceptosMantenimiento>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConceptosMantenimiento>(); });

        public List<ConceptoMantenimiento> ObtenerConceptos()
        {
            return RepositorioConceptosMantenimiento.ObtenerElementos(m => m.Activo).ToList();
        }

        public List<ConceptoMantenimiento.Clasificaciones> ObtenerClasificaciones()
        {
            return EnumExtensions.ComoLista<ConceptoMantenimiento.Clasificaciones>();
        }

        public void CrearConcepto(ConceptoMantenimiento conceptoMantenimiento, DtoUsuario usuario)
        {
            ValidarConcepto(conceptoMantenimiento);

            var fechaActual = DateTime.Now;

            conceptoMantenimiento.Activo = true;
            conceptoMantenimiento.FechaCreacion = fechaActual;
            conceptoMantenimiento.FechaModificacion = fechaActual;
            conceptoMantenimiento.IdUsuarioCreo = usuario.Id;
            conceptoMantenimiento.IdUsuarioModifico = usuario.Id;

            RepositorioConceptosMantenimiento.Agregar(conceptoMantenimiento);
            RepositorioConceptosMantenimiento.GuardarCambios();
        }

        public void ModificarConcepto(ConceptoMantenimiento conceptoMantenimiento, DtoUsuario usuario)
        {
            ValidarConcepto(conceptoMantenimiento);
            ValidarRelaciones(conceptoMantenimiento);

            var fechaActual = DateTime.Now;

            conceptoMantenimiento.FechaModificacion = fechaActual;
            conceptoMantenimiento.IdUsuarioModifico = usuario.Id;

            RepositorioConceptosMantenimiento.Modificar(conceptoMantenimiento);
            RepositorioConceptosMantenimiento.GuardarCambios();
        }

        public void EliminarConcepto(int idConceptoMantenimiento, DtoUsuario usuario)
        {
            var ConceptoMantenimiento = RepositorioConceptosMantenimiento.Obtener(m => m.Activo && m.Id == idConceptoMantenimiento);

            if (ConceptoMantenimiento == null)
                throw new SOTException(Recursos.ConceptosMantenimiento.concepto_mantenimiento_nulo_excepcion);

            ValidarRelaciones(ConceptoMantenimiento);

            var fechaActual = DateTime.Now;

            ConceptoMantenimiento.Activo = false;
            ConceptoMantenimiento.FechaModificacion = fechaActual;
            ConceptoMantenimiento.FechaEliminacion = fechaActual;
            ConceptoMantenimiento.IdUsuarioModifico = usuario.Id;
            ConceptoMantenimiento.IdUsuarioElimino = usuario.Id;

            RepositorioConceptosMantenimiento.Modificar(ConceptoMantenimiento);
            RepositorioConceptosMantenimiento.GuardarCambios();
        }

        #region métodos internal

        bool IServicioConceptosMantenimientoInterno.VerificarExiste(int idConcepto, bool incluirEliminados) 
        {
            if (!incluirEliminados)
                return RepositorioConceptosMantenimiento.Alguno(m => m.Id == idConcepto);

            return RepositorioConceptosMantenimiento.Alguno(m => m.Id == idConcepto && m.Activo);
        }

        #endregion

        private void ValidarRelaciones(ConceptoMantenimiento conceptoMantenimiento)
        {
#warning de momento no hace nada
        }

        private void ValidarConcepto(ConceptoMantenimiento conceptoMantenimiento)
        {
            if (conceptoMantenimiento == null)
                throw new SOTException(Recursos.ConceptosMantenimiento.concepto_mantenimiento_nulo_excepcion);

            if (string.IsNullOrWhiteSpace(conceptoMantenimiento.Concepto))
                throw new SOTException(Recursos.ConceptosMantenimiento.concepto_vacio_excepcion);

            if (string.IsNullOrWhiteSpace(conceptoMantenimiento.Codigo))
                throw new SOTException(Recursos.ConceptosMantenimiento.codigo_vacio_excepcion);

            if (RepositorioConceptosMantenimiento.Alguno(m => m.Activo &&
                                                        (m.Concepto.Trim().ToUpper().Equals(conceptoMantenimiento.Concepto.Trim().ToUpper()) ||
                                                        m.Codigo.Trim().ToUpper().Equals(conceptoMantenimiento.Codigo.Trim().ToUpper())) &&
                                                        m.Id != conceptoMantenimiento.Id))
                throw new SOTException(Recursos.ConceptosMantenimiento.concepto_repetido_excepcion, conceptoMantenimiento.Concepto);

            if (!ObtenerClasificaciones().Any(m => m == conceptoMantenimiento.Clasificacion))
                throw new SOTException(Recursos.ConceptosMantenimiento.tipo_invalido_excepcion);
        }
    }
}
