﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using Transversal.Excepciones;
using Negocio.CortesTurno;

namespace Negocio.ConfiguracionesGlobales
{
    public class ServicioConfiguracionesGlobales : IServicioConfiguracionesGlobales
    {
        IServicioPermisos AplicacionServicioPermisos
        {
            get { return _aplicacionServicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _aplicacionServicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioCortesTurnoInterno AplicacionServicioCortesTurno
        {
            get { return _aplicacionServicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurnoInterno> _aplicacionServicioCortesTurno = new Lazy<IServicioCortesTurnoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurnoInterno>(); });

        //IRepositorioConfiguracionesGlobales RepositorioConfiguracionesGlobales
        //{
        //    get { return _repostorioConfiguracionesGlobales.Value; }
        //}

        //Lazy<IRepositorioConfiguracionesGlobales> _repostorioConfiguracionesGlobales = new Lazy<IRepositorioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesGlobales>(); });

        IRepositorioConfiguracionesTurno RepositorioConfiguracionesTurno
        {
            get { return _repostorioConfiguracionesTurno.Value; }
        }

        Lazy<IRepositorioConfiguracionesTurno> _repostorioConfiguracionesTurno = new Lazy<IRepositorioConfiguracionesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesTurno>(); });


        IRepositorioDepartamentosMaestros RepositorioDepartamentosMaestros
        {
            get { return _repostorioDepartamentosMaestros.Value; }
        }

        Lazy<IRepositorioDepartamentosMaestros> _repostorioDepartamentosMaestros = new Lazy<IRepositorioDepartamentosMaestros>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioDepartamentosMaestros>(); });

        IRepositorioPuestosMaestros RepositorioPuestosMaestros
        {
            get { return _repostorioPuestosMaestros.Value; }
        }

        Lazy<IRepositorioPuestosMaestros> _repostorioPuestosMaestros = new Lazy<IRepositorioPuestosMaestros>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPuestosMaestros>(); });

        //public ConfiguracionGlobal ObtenerConfiguracionGlobal() 
        //{
        //    return RepositorioConfiguracionesGlobales.Obtener(m => m.Activa);
        //}


        public List<ConfiguracionTurno> ObtenerConfiguracionesTurno(bool soloActivas = true)
        {
            return RepositorioConfiguracionesTurno.ObtenerConfiguracionesTurno(soloActivas);
        }

        public DepartamentosMaestros ObtenerConfiguracionDepartamentosMaestros()
        {
            return RepositorioDepartamentosMaestros.ObtenerTodo().FirstOrDefault();
        }

        public PuestosMaestros ObtenerConfiguracionPuestosMaestros()
        {
            return RepositorioPuestosMaestros.ObtenerTodo().FirstOrDefault();
        }

        public bool VerificarEsDepartamentoMaestro(int idDepartamento)
        {
            return RepositorioDepartamentosMaestros.Alguno(m => m.Bar == idDepartamento || m.Cocina == idDepartamento || m.Lavanderia == idDepartamento);
        }

        public void ConfigurarTurnos(List<ConfiguracionTurno> configuracionesTurnos, DtoUsuario usuario)
        {
            AplicacionServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConfigurarTurnos = true });

            if (AplicacionServicioCortesTurno.VerificarExistenTurnos())
                throw new SOTException(Recursos.CortesTurno.cortes_existentes_excepcion);

            var configuracionesActuales = ObtenerConfiguracionesTurno(false);

            foreach (var configuracion in configuracionesActuales)
            {
                var config = configuracionesTurnos.FirstOrDefault(m => m.Id == configuracion.Id);

                if (config != null)
                {
                    configuracion.SincronizarPrimitivas(config);
                }
            }
            foreach (var config in configuracionesTurnos.Where(m => m.Activa && m.Id == 0))
            {
                configuracionesActuales.Add(config);
            }

            if (configuracionesActuales.Where(m => m.Activa).GroupBy(m => m.Orden).Count() != configuracionesActuales.Count(m => m.Activa))
                throw new SOTException(Recursos.CortesTurno.orden_configuraciones_repetido_excepcion);

            if (configuracionesActuales.Where(m => m.Activa).GroupBy(m => m.Nombre.Trim().ToUpper()).Count() != configuracionesActuales.Count(m => m.Activa))
                throw new SOTException(Recursos.CortesTurno.nombre_configuraciones_repetido_excepcion);

            if (configuracionesActuales.Where(m => m.Activa).GroupBy(m => m.MinutosInicioAsistencia).Count() != configuracionesActuales.Count(m => m.Activa))
                throw new SOTException(Recursos.CortesTurno.minutos_configuraciones_repetido_excepcion);

            int orden = 1;
            int minutos = -1;

            foreach (var configuracion in configuracionesActuales.OrderBy(m => m.Orden).ToList())
            {
                if (configuracion.Activa)
                {
                    configuracion.Orden = orden++;

                    if (minutos >= configuracion.MinutosInicioAsistencia)
                        throw new SOTException(Recursos.CortesTurno.entrada_mayor_igual_anterior_excepcion);

                    minutos = configuracion.MinutosInicioAsistencia;
                }
                if (configuracion.Id == 0)
                    RepositorioConfiguracionesTurno.Agregar(configuracion);
                else
                    RepositorioConfiguracionesTurno.Modificar(configuracion);
            }

            RepositorioConfiguracionesTurno.GuardarCambios();
        }
    }
}
