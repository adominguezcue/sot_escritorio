﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Negocio.Compartido.ConfiguracionesGlobales;
using Modelo.Seguridad.Dtos;

namespace Negocio.ConfiguracionesGlobales
{
    public interface IServicioConfiguracionesGlobales : IServicioConfiguracionesGlobalesCompartido
    {
        ///// <summary>
        ///// Obtiene un conjunto de valores globales que tienen diferentes utilidades en todo el sistema
        ///// </summary>
        ///// <returns></returns>
        //ConfiguracionGlobal ObtenerConfiguracionGlobal();
        /// <summary>
        /// Retorna las configuraciones de turno del sistema
        /// </summary>
        /// <returns></returns>
        List<ConfiguracionTurno> ObtenerConfiguracionesTurno(bool soloActivas = true);
        /// <summary>
        /// Retorna la configuración de los departamentos maestros en el sistema
        /// </summary>
        /// <returns></returns>
        DepartamentosMaestros ObtenerConfiguracionDepartamentosMaestros();
        /// <summary>
        /// Retorna la configuración de los puestos maestros en el sistema
        /// </summary>
        /// <returns></returns>
        PuestosMaestros ObtenerConfiguracionPuestosMaestros();
        void ConfigurarTurnos(List<ConfiguracionTurno> configuracionesTurnos, DtoUsuario usuario);
    }
}
