﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.ConfiguracionesTarifas
{
    public class ServicioConfiguracionesTarifas : IServicioConfiguracionesTarifas
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        IRepositorioConfiguracionesTarifa RepositorioConfiguracionesTarifa
        {
            get { return _repostorioConfiguracionesTarifa.Value; }
        }

        Lazy<IRepositorioConfiguracionesTarifa> _repostorioConfiguracionesTarifa = new Lazy<IRepositorioConfiguracionesTarifa>(() => FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesTarifa>());

        //IRepositorioConfiguracionesTarifa

        public List<ConfiguracionTarifa> ObtenerConfiguracionesActivas(DtoUsuario usuario)
        {
            return RepositorioConfiguracionesTarifa.ObtenerElementos(m => m.Activa).ToList();
        }
    }
}
