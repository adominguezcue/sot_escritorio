﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
namespace Negocio.ConfiguracionesTarifas
{
    public interface IServicioConfiguracionesTarifas
    {
        /// <summary>
        /// Retorna las configuraciones de tarifas activas en el sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<ConfiguracionTarifa> ObtenerConfiguracionesActivas(DtoUsuario usuario);
    }
}
