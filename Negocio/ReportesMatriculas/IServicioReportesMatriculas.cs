﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.ReportesMatriculas
{
    public interface IServicioReportesMatriculas
    {
        /// <summary>
        /// Permite agregar un nuevo reporte al sistema
        /// </summary>
        /// <param name="nuevoReporte"></param>
        /// <param name="usuario"></param>
        void CrearReporte(ReporteMatricula nuevoReporte, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar el reporte solicitado
        /// </summary>
        /// <param name="idReporte"></param>
        /// <param name="usuario"></param>
        void EliminarReporte(int idReporte, DtoUsuario usuario);
        /// <summary>
        /// Retorna los reportes relacionados con la matrícula especificada, deshabilitados o no
        /// </summary>
        /// <param name="matricula"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<ReporteMatricula> ObtenerReportesMatricula(string matricula, DtoUsuario usuario);
        /// <summary>
        /// Valida que la matrícula del automovil no esté relacionada con ninguna incidencia
        /// </summary>
        /// <param name="matricula"></param>
        void ValidarNoInicidencias(string matricula);
        /// <summary>
        /// Determina si la matrícula del automovil está vinculada a algun reporte
        /// </summary>
        /// <param name="matricula"></param>
        /// <returns>Si el resultado es True, significa que la matrícula tiene reportes de incidencias</returns>
        bool PoseeReportes(string matricula);
    }
}
