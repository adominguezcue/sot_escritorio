﻿using Modelo;
using Modelo.Repositorios;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Usuarios;
using Negocio.Empleados;

namespace Negocio.ReportesMatriculas
{
    public class ServicioReportesMatriculas : IServicioReportesMatriculas
    {
        IServicioEmpleados ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleados> _servicioEmpleados = new Lazy<IServicioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });


        IRepositorioReportesMatriculas RepositorioReportesMatriculas
        {
            get { return _repositorioReportesMatriculas.Value; }
        }

        Lazy<IRepositorioReportesMatriculas> _repositorioReportesMatriculas = new Lazy<IRepositorioReportesMatriculas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioReportesMatriculas>(); });


        public List<ReporteMatricula> ObtenerReportesMatricula(string matricula, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarReportesMatricula = true });

            var reportes = RepositorioReportesMatriculas.ObtenerReportesMatricula(matricula);

            foreach (var grupo in reportes.GroupBy(m=>m.IdUsuarioCreo)) 
            {
                var alias = ServicioEmpleados.ObtenerNombreCompletoEmpleado(grupo.Key);

                foreach (var reporte in grupo)
                    reporte.NombreUsuario = alias;
            }

            return reportes;
        }

        public void CrearReporte(ReporteMatricula nuevoReporte, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearReportesMatricula = true });

            if (nuevoReporte == null)
                throw new SOTException(Recursos.ReportesMatriculas.reporte_nulo_exception);

            ValidarCamposObligatorios(nuevoReporte);

            var fechaActual = DateTime.Now;

            nuevoReporte.Activo = true;
            nuevoReporte.FechaCreacion = fechaActual;
            nuevoReporte.IdUsuarioCreo = usuario.Id;

            RepositorioReportesMatriculas.Agregar(nuevoReporte);
            RepositorioReportesMatriculas.GuardarCambios();
        }

        public void EliminarReporte(int idReporte, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarReportesMatricula = true });

            var reporte = RepositorioReportesMatriculas.Obtener(m => m.Activo && m.Id == idReporte);

            if (reporte == null)
                throw new SOTException(Recursos.ReportesMatriculas.reporte_nulo_exception);

            var fechaActual = DateTime.Now;

            reporte.Activo = false;
            reporte.FechaEliminacion = fechaActual;
            reporte.IdUsuarioElimino = usuario.Id;

            RepositorioReportesMatriculas.Modificar(reporte);
            RepositorioReportesMatriculas.GuardarCambios();
        }

        public void ValidarNoInicidencias(string matricula) 
        {
            var upperMatricula = matricula.Trim().ToUpper();

            if (RepositorioReportesMatriculas.Alguno(m => m.Activo && m.Matricula.Trim().ToUpper().Equals(matricula)))
                throw new SOTException(Recursos.ReportesMatriculas.matricula_tiene_reportes_excepcion, matricula);
        }

        public bool PoseeReportes(string matricula)
        {
            return RepositorioReportesMatriculas.Alguno(m => m.Activo && m.Matricula.Trim().ToUpper().Equals(matricula));
        }

        private void ValidarCamposObligatorios(ReporteMatricula nuevoReporte)
        {
            if (string.IsNullOrWhiteSpace(nuevoReporte.Matricula))
                throw new SOTException(Recursos.ReportesMatriculas.matricula_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(nuevoReporte.Detalles))
                throw new SOTException(Recursos.ReportesMatriculas.detalles_invalidos_excepcion);
        }
    }
}
