﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;

namespace Negocio.ConfiguracionesNegocio
{
    public interface IServicioConfiguracionesNegocio
    {
        ConfiguracionTarifa ObtenerConfiguracion(int idConfiguracionNegocio);
    }
}
