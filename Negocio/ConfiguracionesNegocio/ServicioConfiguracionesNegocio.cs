﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Modelo.Entidades;

namespace Negocio.ConfiguracionesNegocio
{
    public class ServicioConfiguracionesNegocio : IServicioConfiguracionesNegocio
    {

        IRepositorioConfiguracionesNegocio RepositorioConfiguracionesNegocio 
        {
            get { return _repositorioConfiguracionesNegocio.Value; }
        }

        Lazy<IRepositorioConfiguracionesNegocio> _repositorioConfiguracionesNegocio = new Lazy<IRepositorioConfiguracionesNegocio>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesNegocio>(); });

        public ConfiguracionTarifa ObtenerConfiguracion(int idConfiguracionNegocio) 
        {
            var configuracion = RepositorioConfiguracionesNegocio.Obtener(m => m.Id == idConfiguracionNegocio && m.Activa);

            if (configuracion == null)
                throw new SOTException(Recursos.ConfiguracionesNegocio.id_configuracion_invalido_excepcion, idConfiguracionNegocio.ToString());

            return configuracion;
        }
    }
}
