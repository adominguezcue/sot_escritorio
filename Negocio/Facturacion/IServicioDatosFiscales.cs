﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;

namespace Negocio.Facturacion
{
    public interface IServicioDatosFiscales
    {
        /// <summary>
        /// Permite actualizar la información fiscal ligada a un rfc
        /// </summary>
        /// <param name="datosFiscales"></param>
        /// <param name="usuario"></param>
        void ActualizarDatosFiscales(DatosFiscales datosFiscales, DtoUsuario usuario);
        /// <summary>
        /// Permite registrar la información fiscal relacionada a un rfc
        /// </summary>
        /// <param name="datosFiscales"></param>
        /// <param name="usuario"></param>
        void AgregarDatosFiscales(DatosFiscales datosFiscales, DtoUsuario usuario);
        ///// <summary>
        ///// Permite validar los datos fiscales
        ///// </summary>
        ///// <param name="datosFiscales"></param>
        //void ValidarDatosFiscales(DatosFiscales datosFiscales);
        /// <summary>
        /// Retorna los datos fiscales ligados al rfc proporcionado o null en caso de no existir datos
        /// </summary>
        /// <param name="rfc"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        DatosFiscales ObtenerDatosPorRfc(string rfc, DtoUsuario usuario);
        /// <summary>
        /// Retorna los datos fiscales con el id proporcionado o null en caso de no existir datos
        /// </summary>
        /// <param name="idDatosFiscales"></param>
        /// <param name="UsuarioActual"></param>
        /// <returns></returns>
        DatosFiscales ObtenerDatosPorId(int idDatosFiscales, DtoUsuario UsuarioActual);
    }
}
