﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.Facturacion
{
    public class ServicioDatosFiscales : IServicioDatosFiscales
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IRepositorioDatosFiscales RepositorioDatosFiscales
        {
            get { return _repostorioDatosFiscales.Value; }
        }

        Lazy<IRepositorioDatosFiscales> _repostorioDatosFiscales = new Lazy<IRepositorioDatosFiscales>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioDatosFiscales>(); });

        public void AgregarDatosFiscales(DatosFiscales datosFiscales, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearDatosFiscalesClientes = true });

            if (RepositorioDatosFiscales.Alguno(m => m.Activo && m.RFC.Trim().ToUpper().Equals(datosFiscales.RFC.Trim().ToUpper())))
                throw new SOTException(Recursos.DatosFiscales.datos_fiscales_rfc_duplicado_excepcion, datosFiscales.RFC);

            ValidarDatosFiscales(datosFiscales);

            if (datosFiscales.RFC.Length == 13)
                datosFiscales.TipoPersona = TiposPersonas.Fisica;
            else
                datosFiscales.TipoPersona = TiposPersonas.Moral;

            var fechaActual = DateTime.Now;

            datosFiscales.FechaCreacion = fechaActual;
            datosFiscales.FechaModificacion = fechaActual;
            datosFiscales.IdUsuarioCreo = usuario.Id;
            datosFiscales.IdUsuarioModifico = usuario.Id;

            RepositorioDatosFiscales.Agregar(datosFiscales);
            RepositorioDatosFiscales.GuardarCambios();
        }

        public void ActualizarDatosFiscales(DatosFiscales datosFiscales, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarDatosFiscalesClientes = true });

            if (!datosFiscales.Activo)
                throw new SOTException(Recursos.DatosFiscales.eliminacion_no_permitida_excepcion);

            if (RepositorioDatosFiscales.Alguno(m => m.Activo && m.Id != datosFiscales.Id && m.RFC.Trim().ToUpper().Equals(datosFiscales.RFC.Trim().ToUpper())))
                throw new SOTException(Recursos.DatosFiscales.datos_fiscales_rfc_duplicado_excepcion, datosFiscales.RFC);

            ValidarDatosFiscales(datosFiscales);

            if (datosFiscales.RFC.Length == 13)
                datosFiscales.TipoPersona = TiposPersonas.Fisica;
            else
                datosFiscales.TipoPersona = TiposPersonas.Moral;

            var fechaActual = DateTime.Now;

            datosFiscales.FechaModificacion = fechaActual;
            datosFiscales.IdUsuarioModifico = usuario.Id;

            RepositorioDatosFiscales.Modificar(datosFiscales);
            RepositorioDatosFiscales.GuardarCambios();
        }

        private void ValidarDatosFiscales(DatosFiscales datosFiscales) 
        {
            if (!UtilidadesRegex.Rfc(datosFiscales.RFC))
                throw new SOTException(Recursos.Reservaciones.rfc_invalido_excepcion);

            if (string.IsNullOrWhiteSpace(datosFiscales.NombreRazonSocial))
                throw new SOTException(Recursos.Reservaciones.nombre_razon_social_nulo_vacio_excepcion);

            if (string.IsNullOrWhiteSpace(datosFiscales.Calle))
                throw new SOTException(Recursos.Reservaciones.calle_nula_vacia_excepcion);

            if (string.IsNullOrWhiteSpace(datosFiscales.NumeroExterior))
                throw new SOTException(Recursos.Reservaciones.numero_exterior_nulo_vacio_excepcion);

            if (string.IsNullOrWhiteSpace(datosFiscales.NumeroInterior))
                throw new SOTException(Recursos.Reservaciones.numero_interior_nulo_vacio_excepcion);

            if (string.IsNullOrWhiteSpace(datosFiscales.Colonia))
                throw new SOTException(Recursos.Reservaciones.colonia_nula_vacia_excepcion);

            if (string.IsNullOrWhiteSpace(datosFiscales.Ciudad))
                throw new SOTException(Recursos.Reservaciones.ciudad_nula_vacia_excepcion);

            if (string.IsNullOrWhiteSpace(datosFiscales.Estado))
                throw new SOTException(Recursos.Reservaciones.estado_nulo_vacio_excepcion);

            if (!UtilidadesRegex.SoloDigitos(datosFiscales.CP))
                throw new SOTException(Recursos.Reservaciones.cp_invalido_excepcion);

            if (datosFiscales.CP.Length != 5)
                throw new SOTException(Recursos.Reservaciones.longitud_cp_invalida_excepcion);

            if (!UtilidadesRegex.Email(datosFiscales.Email))
                throw new SOTException(Recursos.Reservaciones.facturacion_email_invalido_excepcion);
        }


        public DatosFiscales ObtenerDatosPorRfc(string rfc, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosFiscalesClientes = true });

            return RepositorioDatosFiscales.Obtener(m => m.Activo && m.RFC.Trim().ToUpper().Equals(rfc.Trim().ToUpper()));
        }

        public DatosFiscales ObtenerDatosPorId(int idDatosFiscales, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarDatosFiscalesClientes = true });

            return RepositorioDatosFiscales.Obtener(m => m.Activo && m.Id == idDatosFiscales);
        }
    }
}
