﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Dtos;

namespace Negocio.MedicionMantenimiento
{
    public interface IServicioMedicionMantenimiento
    {
       void AgregaRegistros(List<Modelo.Dtos.DtoMedicionesMantenimiento> dtoMedicionesMantenimiento, Modelo.Seguridad.Dtos.DtoUsuario usuario);
       List<Modelo.Entidades.MedicionMantenimiento> ObtieneMedicionesActivas();
       List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradas(int? idCorte);
       void ModificaRegistros(List<Modelo.Dtos.DtoMedicionesMantenimiento> dtoMedicionesMantenimiento, Modelo.Seguridad.Dtos.DtoUsuario usuario, int? idCorte);
        List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradasPorFecha(DateTime? fechaInicio);
        //void ELiminaRegistros(int? idCorte);

    }
}
