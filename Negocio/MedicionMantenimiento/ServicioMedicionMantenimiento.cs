﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Repositorios;
using Modelo.Entidades;
using Transversal.Dependencias;
using System.Transactions;
using Transversal.Excepciones;
using Modelo.Dtos;

namespace Negocio.MedicionMantenimiento
{
    public class ServicioMedicionMantenimiento : IServicioMedicionMantenimiento
    {
        IRepositorioMedicionesMantenimiento RepositorioMedicionesMantenimiento
        {
            get { return _repositoriomedicionesmantenimiento.Value; }
        }

        Lazy<IRepositorioMedicionesMantenimiento> _repositoriomedicionesmantenimiento
        = new Lazy<IRepositorioMedicionesMantenimiento>(() => FabricaDependencias.Instancia.Resolver<IRepositorioMedicionesMantenimiento>());

        public void AgregaRegistros(List<Modelo.Dtos.DtoMedicionesMantenimiento> dtoMedicionesMantenimiento, Modelo.Seguridad.Dtos.DtoUsuario usuario)
        {

            if (dtoMedicionesMantenimiento != null && dtoMedicionesMantenimiento.Count > 0)
            {
                foreach (Modelo.Dtos.DtoMedicionesMantenimiento datosInsertar in dtoMedicionesMantenimiento)
                {
                    if (!IsDecimalFormat(datosInsertar.ValorLectura))
                        throw new SOTException("Deben Ser Valores Decimales");
                    decimal valorInsertado = Convert.ToDecimal(datosInsertar.ValorLectura);
                    if (valorInsertado <= 0)
                        throw new SOTException("Introduce Valores mayores a 0");
                }

                    //ActualizaRegistrosAnteriores
                 ActualizaRegistrosAnteriores();
                //Aqui lo que se hace actualizar los registros Activos En 0
                ActualizaRegistrosActivos();
                foreach (Modelo.Dtos.DtoMedicionesMantenimiento datosInsertar in dtoMedicionesMantenimiento)
                {
                    decimal valorInsertado = Convert.ToDecimal(datosInsertar.ValorLectura);
                    var datoinsertado = new Modelo.Entidades.MedicionMantenimiento
                    {
                        IdCorteTurno = datosInsertar.CorteActual
                        , Activa = true
                        , IdEmpleadoRegistro = usuario.Id
                        , CatPresentacionMantenimientoId =  datosInsertar.Presentacion.Id
                        ,Valor = valorInsertado
                    };
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                        datoinsertado.FechaCreacion = DateTime.Now;
                        RepositorioMedicionesMantenimiento.Agregar(datoinsertado);
                        RepositorioMedicionesMantenimiento.GuardarCambios();
                        scope.Complete();
                    }
                }
            }

        }

        private void ActualizaRegistrosAnteriores()
        {
            var anteriores = RepositorioMedicionesMantenimiento.ObtieneLecturasAnteriores();
            if (anteriores != null && anteriores.Count > 0)
            {
                foreach (Modelo.Entidades.MedicionMantenimiento datoactual in anteriores)
                {
                    datoactual.EsAnterior = false;
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                        RepositorioMedicionesMantenimiento.Modificar(datoactual);
                        RepositorioMedicionesMantenimiento.GuardarCambios();
                        scope.Complete();
                    }
                }
            }
        }

        private void ActualizaRegistrosActivos()
        {
            var Activos = RepositorioMedicionesMantenimiento.ObtieneLecturasActivas();
            if (Activos != null && Activos.Count > 0)
            {
                foreach (Modelo.Entidades.MedicionMantenimiento datoactual in Activos)
                {
                    datoactual.Activa = false;
                    datoactual.EsAnterior = true;
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                        RepositorioMedicionesMantenimiento.Modificar(datoactual);
                        RepositorioMedicionesMantenimiento.GuardarCambios();
                        scope.Complete();
                    }
                }
            }
        }

        public void ModificaRegistros(List<Modelo.Dtos.DtoMedicionesMantenimiento> dtoMedicionesMantenimiento, Modelo.Seguridad.Dtos.DtoUsuario UsuarioActual, int? idCorte)
        {
            if (dtoMedicionesMantenimiento != null && dtoMedicionesMantenimiento.Count > 0)
            {
                var Activos = RepositorioMedicionesMantenimiento.ObtieneLecturasPorCorte(idCorte);
                if(Activos != null && Activos.Count > 0)
                {
                    foreach(Modelo.Dtos.DtoMedicionesMantenimiento datosmediciopagina in dtoMedicionesMantenimiento)
                    {
                        foreach(Modelo.Entidades.MedicionMantenimiento datomodificar in Activos)
                        {
                            if(datosmediciopagina.CorteActual== datomodificar.IdCorteTurno 
                                && datosmediciopagina.Presentacion.Id == datomodificar.CatPresentacionMantenimientoId)
                            {
                                if (!IsDecimalFormat(datosmediciopagina.ValorLectura))
                                    throw new SOTException("Deben Ser Valores Decimales");
                                decimal valorInsertado = Convert.ToDecimal(datosmediciopagina.ValorLectura);
                                if (valorInsertado <= 0)
                                    throw new SOTException("Introduce Valores mayores a 0");
                                datomodificar.Valor = valorInsertado;

                                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                                {
                                    datomodificar.FechaModificacion = DateTime.Now;
                                    RepositorioMedicionesMantenimiento.Modificar(datomodificar);
                                    RepositorioMedicionesMantenimiento.GuardarCambios();
                                    scope.Complete();
                                }
                            }
                        }
                    }
                }
                else
                {
                    AgregaRegistros(dtoMedicionesMantenimiento, UsuarioActual);
                }
            }
        }
        private bool IsDecimalFormat(string input)
        {
            bool respuesta = false;
            decimal dummy;
            return respuesta = Decimal.TryParse(input, out dummy);
        }

        public List<Modelo.Entidades.MedicionMantenimiento> ObtieneMedicionesActivas()
        {
            return RepositorioMedicionesMantenimiento.ObtieneLecturasActivas();
        }

        public List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradas(int? idCorte)
        {
            return RepositorioMedicionesMantenimiento.ObtieneLecturasFiltradas(idCorte);
        }

        public List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradasPorFecha(DateTime? fechaInicio)
        {
            return RepositorioMedicionesMantenimiento.ObtieneLecturasFiltradasPorFecha(fechaInicio);
        }
    }
}
