﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.RoomServices
{
    public interface IServicioRoomServices
    {
        /// <summary>
        /// Permite registrar una nueva comanda en el sistema
        /// </summary>
        /// <param name="comandaNueva"></param>
        /// <param name="usuario"></param>
        void CrearComanda(Comanda comandaNueva, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar los artículos que conforman la comanda, siempre y cuando no se haya cobrado
        /// </summary>
        /// <param name="idComanda"></param>
        /// <param name="articulos"></param>
        /// <param name="usuario"></param>
        void ModificarComanda(int idComanda, List<DtoArticuloPrepararGeneracion> articulos, DtoUsuario usuario);
        /// <summary>
        /// Indica que se finalizó la preparación de los artículos indicados
        /// </summary>
        /// <param name="idComanda"></param>
        /// <param name="idsArticulosComandas"></param>
        /// <param name="usuario"></param>
        void FinalizarElaboracionProductos(int idComanda, List<int> idsArticulosComandas, DtoCredencial credencial);
        /// <summary>
        /// Permite entregar los productos de una comanda que ya se encuentren listos
        /// </summary>
        /// <param name="idComanda"></param>
        /// <param name="idsArticulosComandas"></param>
        /// <param name="idEmpleadoCobro"></param>
        /// <param name="usuario"></param>
        void EntregarProductos(int idComanda, List<int> idsArticulosComandas, DtoCredencial credencial);
        /// <summary>
        /// Permite cancelar una comanda, siempre y cuando la huella digital corresponda a un usuario
        /// activo y con permisos adecuados
        /// </summary>
        /// <param name="idComanda"></param>
        /// <param name="motivo"></param>
        /// <param name="huellaDigital"></param>
        void CancelarComanda(int idComanda, string motivo, DtoCredencial credencial);
        /// <summary>
        /// Permite marcar una comanda como cobrada, a la espera de que el mesero retorne el efectivo
        /// o el ticket
        /// </summary>
        /// <param name="idComanda"></param>
        /// <param name="usuario"></param>
        void CobrarComanda(int idComanda, DtoUsuario usuario);
        /// <summary>
        /// Permite pagar una comanda que ya se encuentre lista en caja
        /// </summary>
        /// <param name="idComanda"></param>
        /// <param name="pagos"></param>
        /// <param name="propina"></param>
        /// <param name="usuario"></param>
        void PagarComanda(int idComanda, List<DtoInformacionPago> pagos, Propina propina, bool marcarComoCortesia, DtoUsuario usuario);
        /// <summary>
        /// Retorna las comandas que aun no han sido cobradas o canceladas relacionadas con la renta
        /// que posee el id proporcionado
        /// </summary>
        /// <param name="idRenta"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Comanda> ObtenerDetallesComandasPendientes(int idRenta, DtoUsuario usuario);
        /// <summary>
        /// Retorna el último número de comanda por turno o cero si no hay una
        /// </summary>
        /// <returns></returns>
        int ObtenerUltimoNumeroComanda(int idTurno);

        void SolicitaCambioComanda(DtoCredencial credencial);


        void SolicitaCambioRestaurante(DtoCredencial credencial);

        void SolicitaCambioConsumoInterno(DtoCredencial credencial);
        /// <summary>
        /// Retorna el nivel máximo de avance de las comandas pendientes o null en caso de no tener
        /// comandas en curso
        /// </summary>
        /// <param name="idRenta"></param>
        /// <returns></returns>
        Comanda.Estados? ObtenerMaximoEstadoComandasPendientes(int idRenta);
        /// <summary>
        /// Retorna las comandas con artículos preparados o en preparación que serán procesadas en el área especificada
        /// </summary>
        /// <returns></returns>
        List<DtoResumenComandaExtendido> ObtenerDetallesComandasArticulosPreparadosOEnPreparacion(string departamento);
        /// <summary>
        /// Permite actualizar la información del pago
        /// </summary>
        /// <param name="idPago"></param>
        /// <param name="formaPago"></param>
        /// <param name="valorPropina"></param>
        /// <param name="referencia"></param>
        /// <param name="idEmpleado"></param>
        /// <param name="usuario"></param>
        /// <param name="numeroTarjeta"></param>
        /// <param name="tipoTarjeta"></param>
        void ActualizarPagoComanda(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, int idEmpleado, DtoUsuario usuario, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null);
        /// <summary>
        /// Obtiene el estatus actual y los tiempos que se ha invertido en cada una de las etapas de
        /// las comandas creadas dentro del rango de fechas especificado
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<DtoLogComanda> ObtenerLogComandasPorFecha(DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario, int? ordenTurno = null, int? idLinea = null, Comanda.Tipos? tipo = null, Comanda.Estados? estado = null);
        void AgregaCambioComanda(DtoUsuario usuario, Comanda idcomanda, string motivo);

        void Imprimir(int idComanda);
    }
}
