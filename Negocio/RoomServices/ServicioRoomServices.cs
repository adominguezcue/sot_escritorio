﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Constantes;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Inventarios;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesImpresoras;
using Negocio.ConsumosInternos;
using Negocio.CortesTurno;
using Negocio.Empleados;
using Negocio.Habitaciones;
using Negocio.Pagos;
using Negocio.Preventas;
using Negocio.Propinas;
using Negocio.Reservaciones;
using Negocio.Restaurantes;
using Negocio.Seguridad.Permisos;
using Negocio.Tickets;
using Negocio.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace Negocio.RoomServices
{
    public class ServicioRoomServices : IServicioRoomServicesInterno
    {
        private static object bloqueadorProductos = new object();

        #region dependencias

        private IServicioReservacionesInterno ServicioReservaciones
        {
            get { return _servicioReservaciones.Value; }
        }

        private Lazy<IServicioReservacionesInterno> _servicioReservaciones = new Lazy<IServicioReservacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioReservacionesInterno>(); });


        IServicioInventarios ServicioInventarios
        {
            get { return _servicioInventarios.Value; }
        }

        Lazy<IServicioInventarios> _servicioInventarios = new Lazy<IServicioInventarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioInventarios>(); });

        IServicioRestaurantes ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantes> _servicioRestaurantes = new Lazy<IServicioRestaurantes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantes>(); });

        IServicioConsumosInternosInterno ServicioConsumosInternos
        {
            get { return _servicioConsumosInternos.Value; }
        }

        Lazy<IServicioConsumosInternosInterno> _servicioConsumosInternos = new Lazy<IServicioConsumosInternosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternosInterno>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IServicioPropinasInterno ServicioPropinas
        {
            get { return _servicioPropinas.Value; }
        }

        Lazy<IServicioPropinasInterno> _servicioPropinas = new Lazy<IServicioPropinasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPropinasInterno>(); });

        IServicioPagosInterno ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagosInterno> _servicioPagos = new Lazy<IServicioPagosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagosInterno>(); });

        IServicioTicketsInterno ServicioTickets
        {
            get { return _servicioTickets.Value; }
        }

        Lazy<IServicioTicketsInterno> _servicioTickets = new Lazy<IServicioTicketsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTicketsInterno>(); });

        IServicioConfiguracionesImpresorasInterno ServicioConfiguracionesImpresoras
        {
            get { return _servicioConfiguracionesImpresoras.Value; }
        }

        Lazy<IServicioConfiguracionesImpresorasInterno> _servicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresorasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresorasInterno>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });

        IServicioVPoints ServicioVPoints
        {
            get { return _servicioVPoints.Value; }
        }

        Lazy<IServicioVPoints> _servicioVPoints = new Lazy<IServicioVPoints>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVPoints>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });
        
        IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });

        IRepositorioRentas RepositorioRentas
        {
            get { return _repositorioRentas.Value; }
        }

        Lazy<IRepositorioRentas> _repositorioRentas = new Lazy<IRepositorioRentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioRentas>(); });

        IRepositorioComandas RepositorioComandas
        {
            get { return _repositorioComandas.Value; }
        }

        Lazy<IRepositorioComandas> _repositorioComandas = new Lazy<IRepositorioComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioComandas>(); });

        IRepositorioPagosComandas RepositorioPagosComandas
        {
            get { return _repositorioPagosComandas.Value; }
        }

        Lazy<IRepositorioPagosComandas> _repositorioPagosComandas = new Lazy<IRepositorioPagosComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosComandas>(); });

        IRepositorioCambiosComanda RepositorioCambiosComandas
        {
            get { return _repositorioCambiosComandas.Value; }
        }

        Lazy<IRepositorioCambiosComanda> _repositorioCambiosComandas = new Lazy<IRepositorioCambiosComanda>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCambiosComanda>(); });

        #endregion

        public void CrearComanda(Comanda comandaNueva, DtoUsuario usuario)
        {
            if (comandaNueva == null)
                throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

            if (!comandaNueva.ArticulosComanda.Any(m => m.Activo))
                throw new SOTException(Recursos.Comandas.comanda_sin_productos_excepcion);

            Renta rentaActual = RepositorioRentas.ObtenerConExtensiones(comandaNueva.IdRenta);

            if (rentaActual == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            var fechaActual = DateTime.Now;

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearComandas = true });

            ServicioHabitaciones.ValidarEstaOcupadaOPendienteCobro(rentaActual.IdHabitacion);

            var extension = rentaActual.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa /*&& m.FechaInicio <= fechaActual */&& m.FechaFin >= fechaActual).OrderBy(m => m.FechaInicio).FirstOrDefault();

            if (rentaActual.FechaFin < fechaActual && extension == null)
                throw new SOTException(Recursos.Rentas.renta_vencida_excepcion);

#warning considerar un validador de integridad entre la comanda y el premio

            if (string.IsNullOrEmpty(rentaActual.NumeroTarjeta) && !string.IsNullOrEmpty(comandaNueva.Cupon))
                throw new SOTException(Recursos.Comandas.cupon_no_soportado_excepcion);
            else if (!string.IsNullOrEmpty(comandaNueva.Cupon))
            {
                var datosCupon = ServicioVPoints.ObtenerCupon(comandaNueva.Cupon, usuario);

                if ((datosCupon.NumeroTarjeta.Length == rentaActual.NumeroTarjeta.Length && !datosCupon.NumeroTarjeta.Equals(rentaActual.NumeroTarjeta)) ||
                    (datosCupon.NumeroTarjeta.Length < rentaActual.NumeroTarjeta.Length) && !rentaActual.NumeroTarjeta.Equals(new string('0', rentaActual.NumeroTarjeta.Length - datosCupon.NumeroTarjeta.Length) + datosCupon.NumeroTarjeta))
                    throw new SOTException(Recursos.Comandas.cupon_diferente_tarjeta_v_exception);
            }

            decimal valorComanda = 0;

            var configGlobal = ServicioParametros.ObtenerParametros();

            foreach (var comandaArticulo in comandaNueva.ArticulosComanda.Where(m => m.Activo))
            {
                if (comandaArticulo.Cantidad <= 0)
                    throw new SOTException(Recursos.Comandas.cantidad_articulo_invalida_excepcion);

                if (comandaArticulo.PrecioUnidad == 0 && !comandaArticulo.EsCortesia)
                    throw new SOTException(Recursos.Comandas.articulos_precio_cero_no_cortesia_excepcion);

                ServicioArticulos.ValidarIntegridadPrecio(comandaArticulo.IdArticulo, comandaArticulo.PrecioUnidad);
                ServicioArticulos.ValidarDisponibilidad(comandaArticulo.IdArticulo, comandaArticulo.Cantidad);

                valorComanda += Math.Round(comandaArticulo.PrecioUnidad * (1 + configGlobal.Iva_CatPar), 2) * comandaArticulo.Cantidad;
            }

            if (comandaNueva.ValorConIVA.ToString("C") != valorComanda.ToString("C"))
                throw new SOTException(Recursos.Comandas.valor_incorrecto_excepcion, valorComanda.ToString("C"), comandaNueva.ValorConIVA.ToString("C"));

            #region validacion IVA

            //var vIVA = comandaNueva.ValorSinIVA * configGlobal.Iva_CatPar;
            //var vConIVA = comandaNueva.ValorSinIVA + vIVA;

            //if (comandaNueva.ValorConIVA.ToString("C") != vConIVA.ToString("C") || comandaNueva.ValorIVA.ToString("C") != vIVA.ToString("C"))
            //    throw new SOTException(Recursos.Comandas.valores_impuestos_comanda_invalidos_excepcion);

            #endregion

            var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();
            int siguienteNumero = ObtenerSiguienteNumeroComandaOrden(corteActual.Id);


            comandaNueva.ValorConIVA = valorComanda;
            comandaNueva.ValorSinIVA = comandaNueva.ValorConIVA / (1 + configGlobal.Iva_CatPar);
            comandaNueva.ValorIVA = comandaNueva.ValorConIVA - comandaNueva.ValorSinIVA;
            comandaNueva.Orden = siguienteNumero;
            comandaNueva.IdCorte = corteActual.Id;
            comandaNueva.FechaCreacion = fechaActual;
            comandaNueva.FechaModificacion = fechaActual;
            comandaNueva.FechaInicio = fechaActual;
            comandaNueva.IdUsuarioCreo = usuario.Id;
            comandaNueva.IdUsuarioModifico = usuario.Id;
            comandaNueva.Estado = Comanda.Estados.Preparacion;
            comandaNueva.Transaccion = "";//Guid.NewGuid().ToString();
            comandaNueva.MotivoCancelacion = string.Empty;
            if (comandaNueva.Observaciones == null)
                comandaNueva.Observaciones = "";

            comandaNueva.HistorialesComanda.Add(new HistorialComanda
            {
                Estado = comandaNueva.Estado,
                FechaInicio = fechaActual
            });

            foreach (var comandaArticulo in comandaNueva.ArticulosComanda.Where(m => m.Activo))
            {
                comandaArticulo.FechaCreacion = fechaActual;
                comandaArticulo.FechaModificacion = fechaActual;
                comandaArticulo.IdUsuarioCreo = usuario.Id;
                comandaArticulo.IdUsuarioModifico = usuario.Id;
                comandaArticulo.Estado = ArticuloComanda.Estados.EnProceso;

                if (comandaArticulo.Observaciones == null)
                    comandaArticulo.Observaciones = "";
            }

            var idPreventa = ServicioPreventas.GrenerarPreventa(Venta.ClasificacionesVenta.RoomService);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (!string.IsNullOrEmpty(comandaNueva.Cupon))
                {
                    try
                    {

                        ServicioVPoints.CanjearCupon(comandaNueva.Cupon, usuario);
                        ServicioVPoints.CancelarErroresCupon(comandaNueva.Cupon);
                    }
                    catch (Negocio.ServiciosExternos.Excepciones.VPointsException e)
                    {
                        throw e;
                    }
                    catch (Exception e)
                    {
                        ServicioVPoints.RegistrarErrorCupon(comandaNueva.Cupon, e.Message);
                        scope.Complete();

                        throw;
                    }
                }
                comandaNueva.IdPreventa = idPreventa;

                RepositorioComandas.Agregar(comandaNueva);
                RepositorioComandas.GuardarCambios();

                RegistrarMovimiento(comandaNueva);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicket(comandaNueva, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(comandaNueva, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            ServicioTickets.ImprimirTicket(comandaNueva, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(comandaNueva, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }

        public void Imprimir(int idComanda)
        {
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicketComanda(idComanda, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicketComanda(idComanda, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            ServicioTickets.ImprimirTicketComanda(idComanda, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicketComanda(idComanda, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }


        public void AgregaCambioComanda( DtoUsuario usuario, Comanda comanda, string motivo)
        {
            CambiosComandas cambiosComandas = new CambiosComandas();
            
            if(motivo.Equals(""))
            {
                throw new SOTException("Se debe poner un Motivo para el cambio de comanda");
            }
            var fechaActual = DateTime.Now;
            cambiosComandas.FechaCreacion = fechaActual;
            cambiosComandas.IdUsuarioCreo = usuario.Id;
            cambiosComandas.Observaciones = motivo;
            cambiosComandas.IdComanda = comanda.Id;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {

                RepositorioCambiosComandas.Agregar(cambiosComandas);
                RepositorioCambiosComandas.GuardarCambios();
                scope.Complete();
            }
        }

        public void ModificarComanda(int idComanda, List<DtoArticuloPrepararGeneracion> articulos, DtoUsuario usuario)
        {
            var configuracionGlobal = ServicioParametros.ObtenerParametros();

            var comanda = RepositorioComandas.Obtener(m => m.Activo && m.Id == idComanda);

            if (comanda == null)
                throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

            var fechaActual = DateTime.Now;

            Renta rentaActual = RepositorioRentas.ObtenerConExtensiones(comanda.IdRenta);

            if (rentaActual == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            var extension = rentaActual.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa /*&& m.FechaInicio <= fechaActual */&& m.FechaFin >= fechaActual).OrderBy(m => m.FechaInicio).FirstOrDefault();

            if (rentaActual.FechaFin < fechaActual && extension == null)
                throw new SOTException(Recursos.Rentas.renta_vencida_excepcion);

            comanda = RepositorioComandas.ObtenerParaPagar(idComanda);

            if (comanda == null)
                throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

            /*  if (/*comanda.Estado != Comanda.Estados.PorCobrar &&*/
            /*      comanda.Estado != Comanda.Estados.PorEntregar &&
                comanda.Estado != Comanda.Estados.Preparacion)
                throw new SOTException(Recursos.Comandas.comanda_no_modificable_excepcion);*/

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarComandas = true });

            foreach (var comandaArticulo in articulos.Where(m => m.Activo))
            {
                if (comandaArticulo.Cantidad <= 0)
                    throw new SOTException(Recursos.Comandas.cantidad_articulo_invalida_excepcion);

                if (comandaArticulo.PrecioConIVA == 0 && !comandaArticulo.EsCortesia)
                    throw new SOTException(Recursos.Comandas.articulos_precio_cero_no_cortesia_excepcion);
            }

            var articulosOriginales = new List<DtoArticuloPrepararGeneracion>();

            foreach (var articulo in comanda.ArticulosComanda.Where(m => m.Activo))
            {
                articulosOriginales.Add(new DtoArticuloPrepararGeneracion
                {
                    Activo = true,
                    Cantidad = articulo.Cantidad,
                    EsCortesia = articulo.EsCortesia,
                    Id = articulo.Id,
                    IdArticulo = articulo.IdArticulo,
                    Observaciones = articulo.Observaciones,
                    PrecioConIVA = articulo.PrecioUnidad
                });
            }

            foreach (var tupla in (from artGeneracion in articulos
                                   join articuloComanda in comanda.ArticulosComanda
                                    on new { artGeneracion.Id, Activo = true } equals new { articuloComanda.Id, articuloComanda.Activo } into x
                                   from articuloComanda in x.DefaultIfEmpty()
                                   select new
                                   {
                                       artGeneracion,
                                       articuloComanda
                                   }).ToList())
            {
                if (tupla.articuloComanda == null)
                {
                    if (tupla.artGeneracion.Id <= 0)
                    {
                        ServicioArticulos.ValidarIntegridadPrecio(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.PrecioConIVA);
                        ServicioArticulos.ValidarDisponibilidad(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad);

                        comanda.ArticulosComanda.Add(new ArticuloComanda
                        {
                            Activo = true,
                            IdArticulo = tupla.artGeneracion.IdArticulo,
                            PrecioUnidad = tupla.artGeneracion.PrecioConIVA,
                            Cantidad = tupla.artGeneracion.Cantidad,
                            FechaCreacion = fechaActual,
                            FechaModificacion = fechaActual,
                            IdUsuarioCreo = usuario.Id,
                            IdUsuarioModifico = usuario.Id,
                            Estado = ArticuloComanda.Estados.EnProceso,
                            Observaciones = tupla.artGeneracion.Observaciones ?? "",
                            EsCortesia = tupla.artGeneracion.EsCortesia
                        });
                    }
                }
                else
                {
                 /*   if (tupla.articuloComanda.Estado != ArticuloComanda.Estados.EnProceso
                        && tupla.articuloComanda.Estado != ArticuloComanda.Estados.PorEntregar)
                        throw new SOTException(Recursos.Comandas.articulo_comanda_no_modificable_excepcion);*/

                    if (!tupla.artGeneracion.Activo)
                    {
                        tupla.articuloComanda.Activo = false;
                        tupla.articuloComanda.Estado = ArticuloComanda.Estados.Cancelado;
                        tupla.articuloComanda.FechaEliminacion = fechaActual;
                        tupla.articuloComanda.FechaModificacion = fechaActual;
                        tupla.articuloComanda.IdUsuarioElimino = usuario.Id;
                        tupla.articuloComanda.IdUsuarioModifico = usuario.Id;
                    }
                    else
                    {
                        if (tupla.articuloComanda.Cantidad != tupla.artGeneracion.Cantidad
                            || tupla.articuloComanda.Observaciones != (tupla.artGeneracion.Observaciones ?? "")
                            || tupla.articuloComanda.EsCortesia != tupla.artGeneracion.EsCortesia)
                        {
                            if (tupla.articuloComanda.Cantidad < tupla.artGeneracion.Cantidad)
                                ServicioArticulos.ValidarDisponibilidad(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad);

                            if (tupla.articuloComanda.Cantidad != tupla.artGeneracion.Cantidad && tupla.articuloComanda.PrecioUnidad.ToString("C") != tupla.artGeneracion.PrecioConIVA.ToString("C"))
                            {
                                var articulo = ServicioArticulos.ObtenerArticuloPorId(tupla.articuloComanda.IdArticulo);
                                throw new SOTException(Recursos.Comandas.precio_articulo_modificado_diferente_excepcion,
                                                       articulo.Desc_Art, (tupla.articuloComanda.PrecioUnidad * (1 + configuracionGlobal.Iva_CatPar)).ToString("C"),
                                                       (tupla.artGeneracion.PrecioConIVA * (1 + configuracionGlobal.Iva_CatPar)).ToString("C"));
                            }

                            tupla.articuloComanda.Cantidad = tupla.artGeneracion.Cantidad;
                            tupla.articuloComanda.Estado = ArticuloComanda.Estados.EnProceso;
                            tupla.articuloComanda.FechaModificacion = fechaActual;
                            tupla.articuloComanda.IdUsuarioModifico = usuario.Id;
                            tupla.articuloComanda.Observaciones = tupla.artGeneracion.Observaciones ?? "";
                            tupla.articuloComanda.EsCortesia = tupla.artGeneracion.EsCortesia;
                        }
                    }
                }
            }

            if (!comanda.ArticulosComanda.Any(m => m.Activo))
                throw new SOTException(Recursos.Comandas.comanda_sin_productos_excepcion);

            decimal valorComandaConIVA = 0;

            foreach (var comandaArticulo in comanda.ArticulosComanda.Where(m => m.Activo))
                valorComandaConIVA += Math.Round(comandaArticulo.PrecioUnidad * (1 + configuracionGlobal.Iva_CatPar), 2) * comandaArticulo.Cantidad;

            if (comanda.ArticulosComanda.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.Entregado))
                comanda.Estado = Comanda.Estados.PorCobrar;
            else if (comanda.ArticulosComanda.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.PorEntregar || m.Estado == ArticuloComanda.Estados.Entregado))
                comanda.Estado = Comanda.Estados.PorEntregar;
            else
                comanda.Estado = Comanda.Estados.Preparacion;


            comanda.ValorConIVA = valorComandaConIVA;
            comanda.ValorSinIVA = comanda.ValorConIVA / (1 + configuracionGlobal.Iva_CatPar);
            comanda.ValorIVA = comanda.ValorConIVA - comanda.ValorSinIVA;
            comanda.FechaModificacion = fechaActual;
            comanda.FechaInicio = fechaActual;
            comanda.IdUsuarioCreo = usuario.Id;
            comanda.IdUsuarioModifico = usuario.Id;

            if (comanda.Observaciones == null)
                comanda.Observaciones = "";

            comanda.HistorialesComanda.Add(new HistorialComanda
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = comanda.Estado,
                FechaInicio = fechaActual
            });

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioComandas.Modificar(comanda);
                RepositorioComandas.GuardarCambios();

                ActualizarMovimientos(comanda.Id, articulosOriginales, articulos, fechaActual);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicket(comanda, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(comanda, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            ServicioTickets.ImprimirTicket(comanda, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(comanda, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }

        public void CancelarComanda(int idComanda, string motivo, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            var comanda = RepositorioComandas.ObtenerParaPagar(idComanda);

            if (comanda == null)
                throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

            //Antonio de jesus Domínguez Cuevas 11/11/2019 se quitan validaciones para que solo el gerente peda cancelar y/o cambiar comanda
           /* if (comanda.Estado == Comanda.Estados.PorPagar ||
                comanda.Estado == Comanda.Estados.Cobrada ||
                comanda.Estado == Comanda.Estados.Cancelada)
                throw new SOTException(Recursos.Comandas.comanda_no_cancelable_excepcion);*/

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarComandas = true });

            var fechaActual = DateTime.Now;

            foreach (var articulo in comanda.ArticulosComanda.Where(m => m.Activo))
            {
                //if (articulo.Estado != ArticuloComanda.Estados.EnProceso && articulo.Estado != ArticuloComanda.Estados.PorEntregar)
                //    throw new SOTException(Recursos.Comandas.articulo_comanda_no_cancelable_excepcion);


                articulo.Activo = false;
                articulo.Estado = ArticuloComanda.Estados.Cancelado;
                articulo.FechaEliminacion = fechaActual;
                articulo.FechaModificacion = fechaActual;
                articulo.IdUsuarioElimino = usuario.Id;
                articulo.IdUsuarioModifico = usuario.Id;
            }

            comanda.Activo = false;
            comanda.FechaModificacion = fechaActual;
            comanda.FechaEliminacion = fechaActual;
            comanda.IdUsuarioModifico = usuario.Id;
            comanda.IdUsuarioElimino = usuario.Id;
            comanda.IdEmpleadoCobro = null;
            comanda.Estado = Comanda.Estados.Cancelada;
            comanda.MotivoCancelacion = motivo;

            comanda.HistorialesComanda.Add(new HistorialComanda
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = comanda.Estado,
                FechaInicio = fechaActual
            });

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioComandas.Modificar(comanda);
                RepositorioComandas.GuardarCambios();

                CancelarMovimientos(comanda);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicketComandaCancelada(idComanda, configuracionImpresoras.ImpresoraTickets, 1);
        }

        public void CobrarComanda(int idComanda, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CobrarComanda = true });

            var comanda = RepositorioComandas.ObtenerParaPagar(idComanda);

            if (comanda == null)
                throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

            if (comanda.Estado != Comanda.Estados.PorCobrar)
                throw new SOTException(Recursos.Comandas.comanda_no_por_cobrar_excepcion);

            //ServicioEmpleados.ValidarMeseroActivoHabilitado(comanda.IdEmpleadoCobro.Value);


            var fechaActual = DateTime.Now;

            comanda.FechaModificacion = fechaActual;
            comanda.IdUsuarioModifico = usuario.Id;
            comanda.Estado = Comanda.Estados.PorPagar;

            comanda.HistorialesComanda.Add(new HistorialComanda
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = comanda.Estado,
                FechaInicio = fechaActual
            });

            RepositorioComandas.Modificar(comanda);
            RepositorioComandas.GuardarCambios();
        }

        public void PagarComanda(int idComanda, List<DtoInformacionPago> pagos, Propina propina, bool marcarComoCortesia, DtoUsuario usuario)
        {
            usuario = this.ServicioPagos.ProcesarPermisosEspeciales(pagos, usuario);

            var comanda = RepositorioComandas.ObtenerParaPagar(idComanda);

            if (comanda == null)
                throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

            if (comanda.Estado != Comanda.Estados.PorPagar)
                throw new SOTException(Recursos.Comandas.comanda_no_por_pagar_excepcion);

            if (!string.IsNullOrEmpty(comanda.Cupon) && pagos.Count > 0)
                throw new SOTException(Recursos.Comandas.comanta_pagada_cupon_excepcion);

            #region validación de cortesías que cubren toda la comanda

            if (marcarComoCortesia)
            {
                if (pagos.Any(m => m.TipoPago != TiposPago.Cortesia) || pagos.Count(m=> m.TipoPago == TiposPago.Cortesia) != 1)
                    throw new SOTException(Recursos.Comandas.pagos_invalidos_cortesia_excepcion);
            }

            #endregion

            #region validacion IVA

            var configGlobal = ServicioParametros.ObtenerParametros();
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            var vIVA = comanda.ValorSinIVA * configGlobal.Iva_CatPar;
            var vConIVA = comanda.ValorSinIVA + vIVA;

            if (comanda.ValorConIVA.ToString("C") != vConIVA.ToString("C") || comanda.ValorIVA.ToString("C") != vIVA.ToString("C"))
                throw new SOTException(Recursos.Comandas.valores_impuestos_comanda_invalidos_excepcion);

            #endregion

            var pagosLocales = string.IsNullOrEmpty(comanda.Cupon) ? pagos : new List<DtoInformacionPago>() 
            {
                new DtoInformacionPago
                {
                    Referencia = comanda.Cupon,
                    TipoPago = TiposPago.Cupon,
                    Valor = comanda.ValorConIVA
                }
            };

            if (!pagosLocales.Any())
                throw new SOTException(Recursos.Comandas.pago_incorrecto_excepcion, comanda.ValorConIVA.ToString("C"), "$0.00");

            var valorPagos = pagosLocales.Sum(m => m.Valor);

            if (comanda.ValorConIVA.ToString("C") != valorPagos.ToString("C"))
                throw new SOTException(Recursos.Comandas.pago_incorrecto_excepcion, comanda.ValorConIVA.ToString("C"), valorPagos.ToString("C"));

            //ServicioEmpleados.ValidarMeseroActivoHabilitado(comanda.IdEmpleadoCobro.Value);


            var fechaActual = DateTime.Now;
            string transaccion;

            comanda.Activo = false;
            comanda.FechaModificacion = fechaActual;
            comanda.FechaEliminacion = fechaActual;
            comanda.IdUsuarioModifico = usuario.Id;
            comanda.IdUsuarioElimino = usuario.Id;
            comanda.FechaCobro = fechaActual;
            comanda.Estado = Comanda.Estados.Cobrada;
            comanda.Transaccion = transaccion = Guid.NewGuid().ToString();
            comanda.HistorialesComanda.Add(new HistorialComanda
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = comanda.Estado,
                FechaInicio = fechaActual
            });

            decimal alimentos, bebidas, spa;
            alimentos = bebidas = spa = 0;

            foreach (var articulo in comanda.ArticulosComanda.Where(m => m.Activo))
            {
                articulo.Activo = false;
                articulo.FechaEliminacion = fechaActual;
                articulo.FechaModificacion = fechaActual;
                articulo.IdUsuarioModifico = usuario.Id;
                articulo.IdUsuarioElimino = usuario.Id;
                articulo.PrecioUnidadFinal = Math.Round(articulo.PrecioUnidad * (1 + configGlobal.Iva_CatPar), 2);

#warning aqui se intenta marcar como cortesías al momento del pago todos los artículos, siempre y cuando marcarComoCortesia sea true
                articulo.EsCortesia = articulo.EsCortesia || marcarComoCortesia;

                var lBase = ServicioArticulos.ObtenerLineaBaseIgnorarActivo(articulo.IdArticulo);

                if (lBase.HasValue)
                {
                    switch (lBase.Value)
                    {
                        case Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos:
                            alimentos += articulo.PrecioUnidadFinal * articulo.Cantidad;
                            break;
                        case Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas:
                            bebidas += articulo.PrecioUnidadFinal * articulo.Cantidad;
                            break;
                        case Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex:
                            spa += articulo.PrecioUnidadFinal * articulo.Cantidad;
                            break;
                    }
                }

                articulo.EntidadEstado = EntidadEstados.Modificado;
            }

            //var transaccion = comanda.Transaccion;//Guid.NewGuid().ToString();

            foreach (var pago in pagosLocales)
                comanda.PagosComanda.Add(new PagoComanda
                {
                    Activo = true,
                    Referencia = pago.Referencia,
                    TipoPago = pago.TipoPago,
                    Valor = pago.Valor,
                    TipoTarjeta = pago.TipoTarjeta,
                    NumeroTarjeta = pago.NumeroTarjeta ?? "",
                    Transaccion = transaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id
                });

            if (propina != null)
            {
                propina.Activo = true;
                propina.TipoPropina = Propina.TiposPropina.Comanda;
                propina.IdEmpleado = comanda.IdEmpleadoCobro.Value;
                propina.Transaccion = transaccion;
                propina.FechaCreacion = fechaActual;
                propina.FechaModificacion = fechaActual;
                propina.IdUsuarioCreo = usuario.Id;
                propina.IdUsuarioModifico = usuario.Id;
            }

            var renta = RepositorioRentas.Obtener(m => m.Id == comanda.IdRenta);

            var venta = ServicioPagos.GenerarVentaNoCompletada(comanda.PagosComanda.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.RoomService, comanda.IdPreventa);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ServicioPagos.Pagar(venta.Id, comanda.PagosComanda.Where(m => m.Activo), usuario);

                RepositorioComandas.Modificar(comanda);
                RepositorioComandas.GuardarCambios();

                if (propina != null)
                    ServicioPropinas.RegistrarPropina(propina, usuario.Id);

                var porcentaje = CalcularPorcentajeAbonar(comanda);

                if (!string.IsNullOrEmpty(renta.NumeroTarjeta))
                    ServicioVPoints.RegistrarAbonosPuntos(renta.Id, renta.NumeroTarjeta, venta.Ticket.ToString(), 0, 0,
                                                         alimentos * porcentaje, bebidas * porcentaje, spa * porcentaje, usuario);

                scope.Complete();
            }
#warning contemplar si se quita
            ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 0);
        }

        private decimal CalcularPorcentajeAbonar(Comanda comanda)
        {
            List<TiposPago> tiposValidor = new List<TiposPago>() { TiposPago.Efectivo, TiposPago.TarjetaCredito, TiposPago.Transferencia };

            List<IPago> pagosAbonar = comanda.PagosComanda.Where(m => m.Activo && tiposValidor.Contains(m.TipoPago)).Select(m => (IPago)m).ToList();

            if (comanda.PagosComanda.Any(m => m.Activo && m.TipoPago == TiposPago.Reservacion))
                pagosAbonar.AddRange(ServicioReservaciones.ObtenerPagosPorCodigosReserva(comanda.PagosComanda.Where(m => m.Activo && m.TipoPago == TiposPago.Reservacion).Select(m => m.Referencia).ToList())
                                     .Where(m => m.Activo && tiposValidor.Contains(m.TipoPago)));

            if (comanda.ValorConIVA == pagosAbonar.Sum(m => m.Valor))
                return 1;
            else
                return pagosAbonar.Sum(m => m.Valor) / comanda.ValorConIVA;
        }

        public void FinalizarElaboracionProductos(int idComanda, List<int> idsArticulosComandas, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { PrepararProductos = true });

            //ServicioEmpleados.ValidarCocineroActivoHabilitado(usuario.Id);

            lock (bloqueadorProductos)
            {
                var comanda = RepositorioComandas.ObtenerPorArticulos(idComanda, idsArticulosComandas);

                if (comanda == null)
                    throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

                if (comanda.Estado != Comanda.Estados.Preparacion)
                    throw new SOTException(Recursos.Comandas.comanda_no_preparacion_excepcion);

                var fechaActual = DateTime.Now;

                foreach (var idArticuloComanda in idsArticulosComandas)
                {
                    var comandaArticulo = comanda.ArticulosComanda.FirstOrDefault(m => m.Id == idArticuloComanda);

                    if (comandaArticulo.Estado != ArticuloComanda.Estados.EnProceso)
                        throw new SOTException(Recursos.Comandas.comanda_articulos_no_preparacion_excepcion);

                    comandaArticulo.FechaModificacion = fechaActual;
                    comandaArticulo.IdUsuarioModifico = usuario.Id;

                    comandaArticulo.Estado = ArticuloComanda.Estados.PorEntregar;
                }

                if (comanda.ArticulosComanda.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.PorEntregar || m.Estado == ArticuloComanda.Estados.Entregado))
                {
                    comanda.FechaModificacion = fechaActual;
                    comanda.IdUsuarioModifico = usuario.Id;
                    comanda.Estado = Comanda.Estados.PorEntregar;

                    comanda.HistorialesComanda.Add(new HistorialComanda
                    {
                        EntidadEstado = EntidadEstados.Creado,
                        Estado = comanda.Estado,
                        FechaInicio = fechaActual
                    });
                }


                RepositorioComandas.Modificar(comanda);
                RepositorioComandas.GuardarCambios();
            }
        }

        public void EntregarProductos(int idComanda, List<int> idsArticulosComandas, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { Meserear = true });

            Comanda comanda;

            lock (bloqueadorProductos)
            {
                comanda = RepositorioComandas.ObtenerPorArticulos(idComanda, idsArticulosComandas);

                if (comanda == null)
                    throw new SOTException(Recursos.Comandas.comanda_nula_excepcion);

                if (comanda.Estado != Comanda.Estados.Preparacion && comanda.Estado != Comanda.Estados.PorEntregar)
                    throw new SOTException(Recursos.Comandas.comanda_no_preparacion_o_por_entregar_excepcion);

                if (comanda.IdEmpleadoCobro.HasValue && comanda.IdEmpleadoCobro != usuario.Id)
                    throw new SOTException(Recursos.Comandas.mesero_comanda_diferente_excepcion);

                //ServicioEmpleados.ValidarMeseroActivoHabilitado(usuario.Id);

                var fechaActual = DateTime.Now;

                foreach (var idArticuloComanda in idsArticulosComandas)
                {
                    var comandaArticulo = comanda.ArticulosComanda.FirstOrDefault(m => m.Id == idArticuloComanda);

                    if (comandaArticulo.Estado != ArticuloComanda.Estados.PorEntregar)
                        throw new SOTException(Recursos.Comandas.comanda_articulos_no_por_entregar_excepcion);

                    comandaArticulo.FechaModificacion = fechaActual;
                    comandaArticulo.IdUsuarioModifico = usuario.Id;

                    comandaArticulo.Estado = ArticuloComanda.Estados.Entregado;
                }
                if (comanda.ArticulosComanda.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.Entregado))
                {
                    comanda.FechaModificacion = fechaActual;
                    comanda.IdUsuarioCreo = usuario.Id;
                    comanda.Estado = Comanda.Estados.PorCobrar;
                    comanda.IdEmpleadoCobro = usuario.Id;

                    comanda.HistorialesComanda.Add(new HistorialComanda
                    {
                        EntidadEstado = EntidadEstados.Creado,
                        Estado = comanda.Estado,
                        FechaInicio = fechaActual
                    });
                }


                RepositorioComandas.Modificar(comanda);
                RepositorioComandas.GuardarCambios();
            }

            var codigos = comanda.ArticulosComanda.Where(m => idsArticulosComandas.Contains(m.Id)).Select(m => m.IdArticulo).Distinct().ToList();

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            //ServicioTickets.ImprimirTicket(comanda, configuracionImpresoras.ImpresoraCocina, 1, codigos, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            //ServicioTickets.ImprimirTicket(comanda, configuracionImpresoras.ImpresoraBar, 1, codigos, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);
        }

        public List<DtoResumenComandaExtendido> ObtenerDetallesComandasArticulosPreparadosOEnPreparacion(string departamento)
        {
            //var idsArticulos = ServicioArticulos.ObtenerArticulosConLineaPorDepartamento(departamento);

            var comandas = RepositorioComandas.ObtenerDetallesComandasArticulosPreparadosOEnPreparacion();

            var idsArticulos = comandas.SelectMany(m => m.ArticulosComanda).Select(m => m.IdArticulo).ToList();

            var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos, departamento);

            var estados = new List<ArticuloComanda.Estados>() { ArticuloComanda.Estados.EnProceso, ArticuloComanda.Estados.PorEntregar };

            List<DtoResumenComandaExtendido> validos = new List<DtoResumenComandaExtendido>();
            string _contradepartamento = "";
            List<string> artsvalid = new List<string>();
            List<Modelo.Almacen.Entidades.Articulo> articulosacomparar = new List<Modelo.Almacen.Entidades.Articulo>();
            bool _TieneArticulosOtroDepto = false;
            string tipootrodepto = string.Empty;
            foreach (var comanda in comandas)
            {
                _TieneArticulosOtroDepto = false;
                artsvalid = new List<string>();
                articulosacomparar = new List<Modelo.Almacen.Entidades.Articulo>();
                foreach (var arti in comanda.ArticulosComanda)
                {
                    artsvalid.Add(arti.IdArticulo);
                }
                if (departamento.Equals("Cocina"))
                    _contradepartamento = "Bar";
                else
                    _contradepartamento = "Cocina";
                    var articontradepto = ServicioArticulos.ObtenerArticulosConLineaPorId(artsvalid, _contradepartamento);
                foreach (var oiu in articontradepto)
                {
                    articulosacomparar.Add(oiu);
                }
                    var artidepto = ServicioArticulos.ObtenerArticulosConLineaPorId(artsvalid, departamento);
                foreach (var oiu1 in artidepto)
                {
                    articulosacomparar.Add(oiu1);
                }
                    bool _esbebdida = false;
                    bool _essex = false;
                    bool _esalimentos = false;
                if (articontradepto.Count > 0)
                {
                    foreach (var guyg in articulosacomparar)
                    {
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Equals("ALIMENTOS"))
                            _esalimentos = true;
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Equals("BEBIDAS"))
                            _esbebdida = true;
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Contains("SEX"))
                            _essex = true;
                    }
                    if (_esalimentos && _esbebdida == false && _essex == false)
                        tipootrodepto = "A";
                    else if (_esalimentos && _esbebdida && _essex == false)
                        tipootrodepto = "AB";
                    else if (_esbebdida && _esbebdida && _essex)
                        tipootrodepto = "ABS";
                    _TieneArticulosOtroDepto = true;
                }
           
                if (!comanda.ArticulosComanda.Any(m => articulos.Any(a => a.Cod_Art == m.IdArticulo) && estados.Contains(m.Estado)))
                    continue;

                comanda.ArticulosComanda.RemoveAll(m => !articulos.Any(a => a.Cod_Art == m.IdArticulo));
                int contadordearticulosxcmd = 0;
                foreach (var artCom in comanda.ArticulosComanda)
                {
                    var art = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                    artCom.Nombre = art.Desc_Art;
                    artCom.Tipo = art.ZctCatLinea.Desc_Linea;
                    artCom.EsOtroDepto = false;
                    artCom.OtosDeptos = tipootrodepto;

                    if (_TieneArticulosOtroDepto==true && contadordearticulosxcmd== comanda.ArticulosComanda.Count -1)
                    {
                        artCom.EsOtroDepto = true;
                        artCom.OtosDeptos = tipootrodepto;
                        _TieneArticulosOtroDepto = false;
                    }
                    contadordearticulosxcmd = contadordearticulosxcmd + 1;
                }
                validos.Add(comanda);
            }

            return validos;
        }

        public Comanda.Estados? ObtenerMaximoEstadoComandasPendientes(int idRenta)
        {
            var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar, (int)Comanda.Estados.PorPagar };

            if (RepositorioComandas.Alguno(m => m.Activo && idsEstadoElaborada.Contains(m.IdEstado) && m.IdRenta == idRenta))
            {
                return (Comanda.Estados)RepositorioComandas.ObtenerElementos(m => m.Activo && idsEstadoElaborada.Contains(m.IdEstado) && m.IdRenta == idRenta).Select(m => m.IdEstado).Max();
            }

            return null;
        }

        public List<Comanda> ObtenerDetallesComandasPendientes(int idRenta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarComandas = true });

            var comandas = RepositorioComandas.ObtenerComandasPendientesCargadas(idRenta);

            if (comandas.Count == 0)
                return comandas;

            var idsArticulos = comandas.SelectMany(m => m.ArticulosComanda).Select(m => m.IdArticulo).ToList();

            var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

            foreach (var artCom in comandas.SelectMany(m => m.ArticulosComanda))
            {
                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
            }

            return comandas;
        }

        public void ActualizarPagoComanda(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, int idEmpleado, DtoUsuario usuario, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarPagos = true });

            var pago = RepositorioPagosComandas.Obtener(m => m.Activo && m.Id == idPago, m => m.Comanda);

            if (pago == null)
                throw new SOTException(Recursos.Pagos.pago_nulo_o_eliminado_excepcion);

            if (formaPago != TiposPago.TarjetaCredito && valorPropina != 0)
                throw new SOTException(Recursos.Pagos.propina_no_soportada_excepcion);

            ServicioPagos.ValidarEfectivoOTarjeta(pago);
            ServicioPagos.ValidarEfectivoOTarjeta(formaPago);

            if (pago.Comanda.IdEmpleadoCobro != idEmpleado/* && valorPropina != 0*/)
                ServicioEmpleados.ValidarPermisos(idEmpleado, new DtoPermisos { Meserear = true });//ServicioEmpleados.ValidarMeseroActivoHabilitado(idEmpleado);

            Propina propina = pago.TipoPago == TiposPago.TarjetaCredito ?
                ServicioPropinas.ObtenerPropina(pago.Transaccion, pago.NumeroTarjeta, pago.TipoTarjeta.Value) :
                null;

            var fechaActual = DateTime.Now;

            pago.FechaModificacion = fechaActual;
            pago.IdUsuarioModifico = usuario.Id;
            pago.TipoPago = formaPago;
            pago.Referencia = referencia;
            pago.TipoTarjeta = tipoTarjeta;
            pago.NumeroTarjeta = numeroTarjeta;

            ServicioPagos.ValidarPago(pago, usuario);


            if (propina != null)
            {
                if (valorPropina != 0)
                {
                    propina.IdEmpleado = idEmpleado;
                    propina.FechaModificacion = fechaActual;
                    propina.IdUsuarioModifico = usuario.Id;
                    propina.TipoTarjeta = tipoTarjeta.Value;
                    propina.NumeroTarjeta = numeroTarjeta;
                    propina.Referencia = referencia;
                    propina.Valor = valorPropina;
                }
                else
                {
                    propina.Activo = false;
                }
            }
            else if (valorPropina != 0)
            {
                propina = new Propina
                {
                    Activo = true,
                    TipoPropina = Propina.TiposPropina.Comanda,
                    TipoTarjeta = pago.TipoTarjeta.Value,
                    NumeroTarjeta = numeroTarjeta,
                    Referencia = referencia,
                    IdEmpleado = idEmpleado,
                    Transaccion = pago.Transaccion,
                    FechaCreacion = pago.FechaCreacion,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    Valor = valorPropina
                };
            }

            if (pago.Comanda.IdEmpleadoCobro != idEmpleado)
            {
                //pago.Comanda.FechaModificacion = fechaActual;
                //pago.Comanda.IdUsuarioModifico = usuario.Id;
                pago.Comanda.IdEmpleadoCobro = idEmpleado;
            }
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioPagosComandas.Modificar(pago);
                RepositorioPagosComandas.GuardarCambios();

                if (propina != null)
                {
                    if (propina.Id == 0)
                        ServicioPropinas.RegistrarPropina(propina, usuario.Id);
                    else if (propina.Activo)
                        ServicioPropinas.ModificarPropina(propina, usuario.Id);
                    else
                        ServicioPropinas.EliminarPropina(propina.Id, usuario.Id);
                }

                scope.Complete();
            }
        }

        public int ObtenerUltimoNumeroComanda(int idTurno)
        {
            return (from ordenR in RepositorioComandas.ObtenerElementos(m => m.IdCorte == idTurno)
                    orderby ordenR.Orden descending
                    select ordenR.Orden).FirstOrDefault();
        }

        public List<DtoLogComanda> ObtenerLogComandasPorFecha(DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario, int? ordenTurno = null, int? idLinea = null, Comanda.Tipos? tipo = null, Comanda.Estados? estado = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarComandas = true });

            var historiales = RepositorioComandas.SP_ObtenerHistorialesComandas(fechaInicio, fechaFin, ordenTurno, idLinea, tipo, estado);

            if (historiales.Count == 0)
                return new List<DtoLogComanda>();

            var retorno = new List<DtoLogComanda>();

            double tiempo;

            foreach (var grupoC in historiales.GroupBy(m => new { m.Transaccion, m.IdComanda }))
            {
                var muestra = grupoC.First();

                var item = new DtoLogComanda
                {
                    Estatus = muestra.Estado.Descripcion() + (muestra.Modificada ? " (Cambiada)" : ""),
                    FechaCambio = muestra.Fecha,
                    FechaCreacion = muestra.FechaCreacion,
                    Mesero = muestra.Mesero,
                    Monto = muestra.Monto,
                    Ticket = muestra.Ticket,
                    Turno = muestra.Turno
                };

                var historial = grupoC.Where(m => m.FechaHistorica >= m.Fecha && m.EstadoHistorico == Comanda.Estados.Preparacion).OrderBy(m => m.FechaHistorica).LastOrDefault();
                item.TiempoPreparacion = ObtenerTiempoFormateadoEntreHistoriales(historial, grupoC, out tiempo);
                item.SegundosPreparacion = tiempo;

                historial = grupoC.Where(m => m.FechaHistorica >= m.Fecha && m.EstadoHistorico == Comanda.Estados.PorEntregar).OrderBy(m => m.FechaHistorica).LastOrDefault();
                item.TiempoPorEntregar = ObtenerTiempoFormateadoEntreHistoriales(historial, grupoC, out tiempo);
                item.SegundosPorEntregar = tiempo;

                historial = grupoC.Where(m => m.FechaHistorica >= m.Fecha && m.EstadoHistorico == Comanda.Estados.PorCobrar).OrderBy(m => m.FechaHistorica).LastOrDefault();
                item.TiempoPorCobrar = ObtenerTiempoFormateadoEntreHistoriales(historial, grupoC, out tiempo);
                item.SegundosPorCobrar = tiempo;

                historial = grupoC.Where(m => m.FechaHistorica >= m.Fecha && m.EstadoHistorico == Comanda.Estados.PorPagar).OrderBy(m => m.FechaHistorica).LastOrDefault();
                item.TiempoPorPagar = ObtenerTiempoFormateadoEntreHistoriales(historial, grupoC, out tiempo);
                item.SegundosPorPagar = tiempo;

                var idsFormasStr = muestra.FormasPago.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                int idFormaPago;

                StringBuilder builder = new StringBuilder();

                foreach (var idStr in idsFormasStr)
                {
                    if (builder.Length > 0)
                        builder.Append(", ");

                    if (!int.TryParse(idStr, out idFormaPago))
                        throw new SOTException("Forma de pago desconocida: " + idStr);

                    builder.Append(((TiposPago)idFormaPago).Descripcion());
                }

                item.FormasPago = builder.ToString();

                TimeSpan total;

                if (muestra.Estado == Comanda.Estados.Cobrada || muestra.Estado == Comanda.Estados.Cancelada)
                    total = muestra.FechaModificacion - muestra.Fecha;
                else
                    total = DateTime.Now - muestra.Fecha;

                item.TiempoTotal = total.ToString("dd\\.hh\\:mm\\:ss");
                item.SegundosTotal = total.TotalSeconds;

                retorno.Add(item);
            }

            return retorno;
        }

        private int ObtenerSiguienteNumeroComandaOrden(int idTurno)
        {
            return Math.Max(Math.Max(ServicioRestaurantes.ObtenerUltimoNumeroOrden(idTurno), ObtenerUltimoNumeroComanda(idTurno)), ServicioConsumosInternos.ObtenerUltimoNumeroConsumoInterno(idTurno)) + 1;
        }
        private void RegistrarMovimiento(Comanda comanda)
        {
            foreach (var articulo in comanda.ArticulosComanda.Where(m => m.Activo))
                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.RS, 0, false, /*articulo.Cantidad * articulo.PrecioUnidad * -1,*/ comanda.Id.ToString(), articulo.IdArticulo, articulo.Cantidad * -1, articulo.FechaCreacion, true);
        }

        private void ActualizarMovimientos(int idComanda, List<DtoArticuloPrepararGeneracion> articulosOriginales, List<DtoArticuloPrepararGeneracion> articulos, DateTime fecha)
        {
            foreach (var tupla in (from artGeneracion in articulos
                                   join articuloOrden in articulosOriginales
                                    on new { artGeneracion.Id, Activo = true } equals new { articuloOrden.Id, articuloOrden.Activo } into x
                                   from articuloOrden in x.DefaultIfEmpty()
                                   select new
                                   {
                                       artGeneracion,
                                       articuloOrden
                                   }).ToList())
            {
                if (tupla.articuloOrden == null)
                {
                    if (tupla.artGeneracion.Id <= 0)
                    {
                        ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.RS, 0, false, /*tupla.artGeneracion.Cantidad * tupla.artGeneracion.PrecioConIVA * -1,*/ idComanda.ToString(), tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad * -1, fecha, true);
                    }
                }
                else
                {
                    if (!tupla.artGeneracion.Activo)
                    {
                        ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.RS, 0, true, /*tupla.artGeneracion.Cantidad * tupla.artGeneracion.PrecioConIVA,*/ idComanda.ToString(), tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad, fecha, true);
                    }
                    else
                    {
                        if (tupla.articuloOrden.Cantidad != tupla.artGeneracion.Cantidad)
                        {

                            var nuevaCantidad = tupla.articuloOrden.Cantidad - tupla.artGeneracion.Cantidad;

                            if (nuevaCantidad < 0)
                            {
                                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.RS, 0, false, /*nuevaCantidad * tupla.artGeneracion.PrecioConIVA,*/ idComanda.ToString(), tupla.artGeneracion.IdArticulo, nuevaCantidad, fecha, true);
                            }
                            else if (nuevaCantidad > 0)
                            {
                                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.RS, 0, true, /*nuevaCantidad * tupla.artGeneracion.PrecioConIVA,*/ idComanda.ToString(), tupla.artGeneracion.IdArticulo, nuevaCantidad, fecha, true);
                            }
                        }
                    }
                }
            }
        }

        private void CancelarMovimientos(Comanda comanda)
        {
#warning de momento no se van a reintegrar los artículos cuando es una cancelación, esto debido a que los artículos por
            foreach (var articulo in comanda.ArticulosComanda)
                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.RS, 0, true, /*articulo.Cantidad * articulo.PrecioUnidad,*/ comanda.Id.ToString(), articulo.IdArticulo, articulo.Cantidad, articulo.FechaEliminacion.Value, true);
        }

        private string ObtenerTiempoFormateadoEntreHistoriales(DtoHistorialesComanda historialInicio, IEnumerable<DtoHistorialesComanda> historiales, out double segundos)
        {
            if (historialInicio != null && historialInicio.FechaHistorica.HasValue)
            {
                var historialFin = historiales.Where(m => m.FechaHistorica >= historialInicio.FechaHistorica && m.EstadoHistorico != historialInicio.EstadoHistorico).OrderBy(m => m.FechaHistorica).FirstOrDefault();

                TimeSpan tiempo;

                if (historialFin != null && historialFin.FechaHistorica.HasValue)
                    tiempo = historialFin.FechaHistorica.Value - historialInicio.FechaHistorica.Value;
                else
                    tiempo = DateTime.Now - historialInicio.FechaHistorica.Value;

                segundos = tiempo.TotalSeconds;

                return tiempo.ToString("dd\\.hh\\:mm\\:ss");
            }
            else
            {
                segundos = 0;
                return "";
            }
        }

        #region métodos internal

        List<Comanda> IServicioRoomServicesInterno.ObtenerDetallesComandasCobradas(int idRenta)
        {
            //ServicioPermisos.ValidarPermisos();

            return RepositorioComandas.ObtenerComandasCobradasCargadas(idRenta);
        }

        List<Comanda> IServicioRoomServicesInterno.ObtenerComandasCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            return RepositorioComandas.ObtenerComandasCargadasPorPeriodo(fechaInicio, fechaFin, formasPago);
        }

        List<DtoEmpleadoLineaArticuloValor> IServicioRoomServicesInterno.ObtenerValorArticulosComandasPorPeriodoYLineas(List<int> idsTipos, List<int> idsCortes)
        {
            var articulos = ServicioArticulos.ObtenerIdsArticulosPorLineas(idsTipos);

            return RepositorioComandas.ObtenerArticulosPorEmpleado(articulos, idsCortes);
        }

        Dictionary<TiposTarjeta, decimal> IServicioRoomServicesInterno.ObtenerDiccionarioPagosComandasTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioComandas.ObtenerDiccionarioPagosComandasTarjetaPorFecha(fechaInicio, fechaFin);


            foreach (var item in Enum.GetValues(typeof(TiposTarjeta)).Cast<TiposTarjeta>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }

        decimal IServicioRoomServicesInterno.ObtenerValorPagosComandasPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            return RepositorioComandas.ObtenerPagosComandasPorFecha(fechaInicio, fechaFin);
        }

        bool IServicioRoomServicesInterno.VerificarNoExistenDependenciasArticulo(string codigoArticulo) 
        {
            return RepositorioComandas.ObtenerCantidadArticulosComandasPorCodigoArticulo(codigoArticulo) == 0;
        }

        public void SolicitaCambioComanda(DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarComandas = true });
        }

        public void SolicitaCambioRestaurante(DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarOrdenRestaurante = true });
        }

        public void SolicitaCambioConsumoInterno(DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarConsumosInternos = true });
        }

        #endregion
    }
}
