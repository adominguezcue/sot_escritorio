﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.RoomServices
{
    internal interface IServicioRoomServicesInterno : IServicioRoomServices
    {
        /// <summary>
        /// Retorna las comandas que ya han sido cobradas relacionadas con la renta
        /// que posee el id proporcionado
        /// </summary>
        /// <param name="idRenta"></param>
        /// <returns></returns>
        List<Comanda> ObtenerDetallesComandasCobradas(int idRenta);
        /// <summary>
        /// Retorna las comandas que se encuentran finalizadas dentro del período especificado
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="formasPago"></param>
        /// <returns></returns>
        List<Comanda> ObtenerComandasCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);
        List<DtoEmpleadoLineaArticuloValor> ObtenerValorArticulosComandasPorPeriodoYLineas(List<int> idsTipos, List<int> idsCortes);
        /// <summary>
        /// Permite obtener los pagos por tarjeta de las comandas en un período determinado.
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="tiposPagos"></param>
        /// <returns></returns>
        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosComandasTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Permite obtener el valor de todos los pagos de comandas realizados en el rango de
        /// fechas solicitados.
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        decimal ObtenerValorPagosComandasPorFecha(DateTime? fechaInicio, DateTime? fechaFin);


        bool VerificarNoExistenDependenciasArticulo(string codigoArticulo);
    }
}
