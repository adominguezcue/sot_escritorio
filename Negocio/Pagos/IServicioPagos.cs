﻿using Modelo;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Dominio.Nucleo.Entidades;

namespace Negocio.Pagos
{
    public interface IServicioPagos
    {
        /// <summary>
        /// Realiza los pagos pertinentes en base al monto, código y tipo de pago selecionado, siempre y
        /// cuando el usuario tenga los permisos necesarios
        /// </summary>
        /// <param name="pagos"></param>
        /// <param name="usuario"></param>
        void Pagar(int idVenta, IEnumerable<IPago> pagos, DtoUsuario usuario);
        Venta GenerarVentaNoCompletada(IEnumerable<IPago> pagos, DtoUsuario usuario, Venta.ClasificacionesVenta clasificacionVenta, int? idPreventa, bool ignorarConsumo = false);
       /* /// <summary>
        /// Realiza los pagos pertinentes en base al monto, código y tipo de pago selecionado, siempre y
        /// cuando el usuario tenga los permisos necesarios
        /// </summary>
        /// <param name="pagos"></param>
        /// <param name="usuario"></param>
        void Pagar(int idVenta, IEnumerable<PagoComanda> pagos, DtoUsuario usuario);

        /// <summary>
        /// Realiza los pagos pertinentes en base al monto, código y tipo de pago selecionado, siempre y
        /// cuando el usuario tenga los permisos necesarios
        /// </summary>
        /// <param name="pagos"></param>
        /// <param name="usuario"></param>
        void Pagar(int idVenta, IEnumerable<PagoOcupacionMesa> pagos, DtoUsuario usuario);
        /// <summary>
        /// Realiza los pagos pertinentes en base al monto, código y tipo de pago selecionado, siempre y
        /// cuando el usuario tenga los permisos necesarios
        /// </summary>
        /// <param name="pagos"></param>
        /// <param name="usuario"></param>
        void Pagar(int idVenta, IEnumerable<PagoTarjetaPuntos> pagos, DtoUsuario usuario);*/
        /// <summary>
        /// Retorna una lista de DtoGrupoPago, clase que representa a uno o más pagos al mismo tiempo
        /// </summary>
        /// <param name="incluirReservacion">Indica si se debe incluir el tipo Reservación</param>
        /// <returns></returns>
        List<DtoGrupoPago> ObtenerTiposValidosConMixtos(DtoUsuario usuario, bool incluirCortesia, bool incluirConsumoInterno);//bool incluirReservacion);
        /// <summary>
        /// Valida que la transacción corresponda con un pago existente
        /// </summary>
        /// <param name="transaccion"></param>
        void ValidarTransaccion(string transaccion);
        /// <summary>
        /// Valida que el pago sea en efectivo o tarjeta
        /// </summary>
        /// <param name="pago"></param>
        void ValidarEfectivoOTarjeta(IPago pago);
        /// <summary>
        /// Valida que el tipo de pago sea en efectivo o tarjeta
        /// </summary>
        /// <param name="pago"></param>
        void ValidarEfectivoOTarjeta(TiposPago formaPago);
        /// <summary>
        /// Valida que los datos del pago sean congruentes
        /// </summary>
        /// <param name="pago"></param>
        void ValidarPago(IPago pago, DtoUsuario usuario);
        /// <summary>
        /// Retorna un diccionario de tickets donde las transacciones son las llaves
        /// </summary>
        /// <param name="transacciones"></param>
        /// <returns></returns>
        Dictionary<string, string> ObtenerDiccionarioTransaccionesTicketsIgnoraEstado(List<string> transacciones);

        
    }
}
