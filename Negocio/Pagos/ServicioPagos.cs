﻿using Modelo;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Modelo.Entidades;
using Negocio.VPoints;
using Transversal.Dependencias;
using Modelo.Seguridad.Dtos;
using Negocio.ConsumosInternos;
using Modelo.Repositorios;
using System.Transactions;
using Negocio.CortesTurno;
using Negocio.ConfiguracionesGlobales;
using Dominio.Nucleo.Entidades;
using Negocio.Almacen.Parametros;
using Negocio.Preventas;
using Negocio.Seguridad.Permisos;

namespace Negocio.Pagos
{
    public class ServicioPagos : IServicioPagosInterno
    {
        Lazy<IRepositorioVentas> _repositorioVentas = new Lazy<IRepositorioVentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentas>(); });

        Lazy<IRepositorioPagosSeguros> _repositorioPagosSeguros = new Lazy<IRepositorioPagosSeguros>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosSeguros>(); });

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        Lazy<IServicioConsumosInternos> _servicioConsumosInternos = new Lazy<IServicioConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternos>(); });

        Lazy<IServicioConsumosInternosHabitacionInterno> _servicioConsumosInternosHabitacion = new Lazy<IServicioConsumosInternosHabitacionInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternosHabitacionInterno>(); });

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });
        
        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });

        IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        Lazy<IServicioVPointsInterno> _servicioVPoints = new Lazy<IServicioVPointsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVPointsInterno>(); });

        IServicioVPointsInterno ServicioVPoints
        {
            get { return _servicioVPoints.Value; }
        }
        IRepositorioVentas RepositorioVentas
        {
            get { return _repositorioVentas.Value; }
        }
        IRepositorioPagosSeguros RepositorioPagosSeguros
        {
            get { return _repositorioPagosSeguros.Value; }
        }

        IServicioConsumosInternos ServicioConsumosInternos
        {
            get { return _servicioConsumosInternos.Value; }
        }
        IServicioConsumosInternosHabitacionInterno ServicioConsumosInternosHabitacion
        {
            get { return _servicioConsumosInternosHabitacion.Value; }
        }

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }
        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }
        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }
        //public void Pagar(int idVenta, IEnumerable<PagoRenta> pagos, DtoUsuario usuario) 
        //{
        //    if(Transaction.Current == null)
        //        throw new SOTException("Se requiere una transacción abierta para realizar la operación");

        //    if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
        //        throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }

        //    var transaccion = pagos.First().Transaccion;
        //    var valor = pagos.Where(m => m.Activo).Sum(m => m.Valor);
        //    var fecha = pagos.Min(m => m.FechaCreacion);

        //    var venta = GenerarVenta(valor, transaccion, fecha, usuario);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        Pagar(venta, pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }

        //    venta.Correcta = true;

        //    RepositorioVentas.Modificar(venta);
        //    RepositorioVentas.GuardarCambios();
        //}

        //public void Pagar(int idVenta, IEnumerable<PagoComanda> pagos, DtoUsuario usuario)
        //{
        //    if (Transaction.Current == null)
        //        throw new SOTException("Se requiere una transacción abierta para realizar la operación");

        //    if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
        //        throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }

        //    var transaccion = pagos.First().Transaccion;
        //    var valor = pagos.Where(m => m.Activo).Sum(m => m.Valor);
        //    var fecha = pagos.Min(m => m.FechaCreacion);

        //    var venta = GenerarVenta(valor, transaccion, fecha, usuario);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        Pagar(venta, pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }

        //    venta.Correcta = true;

        //    RepositorioVentas.Agregar(venta);
        //    RepositorioVentas.GuardarCambios();
        //}

        //public void Pagar(int idVenta, IEnumerable<PagoOcupacionMesa> pagos, DtoUsuario usuario)
        //{
        //    if (Transaction.Current == null)
        //        throw new SOTException("Se requiere una transacción abierta para realizar la operación");

        //    if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
        //        throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }

        //    var transaccion = pagos.First().Transaccion;
        //    var valor = pagos.Where(m => m.Activo).Sum(m => m.Valor);
        //    var fecha = pagos.Min(m => m.FechaCreacion);

        //    var venta = GenerarVenta(valor, transaccion, fecha, usuario);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        Pagar(venta, pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }

        //    venta.Correcta = true;

        //    RepositorioVentas.Agregar(venta);
        //    RepositorioVentas.GuardarCambios();
        //}

        public List<DtoGrupoPago> ObtenerTiposValidosConMixtos(DtoUsuario usuario, bool incluirCortesia, bool incluirConsumoInterno/*bool incluirReservacion*/)
        {
            List<DtoGrupoPago> retorno = new List<DtoGrupoPago>();

            bool permisoT = false, permisoE = false;

            foreach (var tipoPago in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
            {
                if (!usuario.Pagos.Contains((int)tipoPago))
                    continue;

                if (tipoPago == TiposPago.Cupon 
                    || /*(*/tipoPago == TiposPago.Reservacion 
                    || (tipoPago == TiposPago.Cortesia && !incluirCortesia) 
                    || (tipoPago == TiposPago.Consumo && !incluirConsumoInterno)
                    || (tipoPago == TiposPago.VPoints))// && !incluirReservacion))
                    continue;

                if (tipoPago == TiposPago.Efectivo)
                    permisoE = true;

                if (tipoPago == TiposPago.TarjetaCredito)
                    permisoT = true;

                retorno.Add(new DtoGrupoPago(tipoPago.Descripcion(), tipoPago));
            }

            if (permisoE && permisoT)
            {
                if (retorno.Count > 2)
                    retorno.Insert(2, new DtoGrupoPago("Mixto", TiposPago.TarjetaCredito, TiposPago.Efectivo));
                else
                    retorno.Add(new DtoGrupoPago("Mixto", TiposPago.TarjetaCredito, TiposPago.Efectivo));
            }

            return retorno;
        }

        public void Pagar(int idVenta, IEnumerable<IPago> pagos, DtoUsuario usuario)
        {
            if (Transaction.Current == null)
                throw new SOTException("Se requiere una transacción abierta para realizar la operación");

            if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
                throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

            foreach (var pago in pagos.Where(m => m.Activo))
            {
                ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta, false);
            }

            var config = ServicioParametros.ObtenerParametros();

            var transaccion = pagos.First().Transaccion;
            var sumaPagos = pagos.Where(m => m.Activo).Sum(m => m.Valor);

            decimal valor = 0, montoIgnorable = 0;

            foreach (var pago in pagos)
            {
                switch (pago.TipoPago)
                {
                    case TiposPago.Consumo:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Cortesia:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Cupon:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Efectivo:
                        valor += pago.Valor;
                        break;
                    case TiposPago.Reservacion:
                        valor += pago.Valor;
                        break;
                    case TiposPago.TarjetaCredito:
                        valor += pago.Valor;
                        break;
                    case TiposPago.VPoints:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Transferencia:
                        valor += pago.Valor;
                        break;
                }
            }

            var venta = RepositorioVentas.Obtener(m => m.Id == idVenta && !m.Correcta/*
                                                       && m.Transaccion == transaccion
                                                       && m.SumaPagos == sumaPagos
                                                       && m.ValorConIVA == valor
                                                       && m.MontoIgnorable == montoIgnorable*/);//GenerarVenta(valor, transaccion, fecha, usuario);

            if (venta == null)
                throw new SOTException("Error al completar la venta");

            foreach (var pago in pagos.Where(m => m.Activo))
            {
                Pagar(venta, pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
            }

            venta.Correcta = true;

            RepositorioVentas.Modificar(venta);
            RepositorioVentas.GuardarCambios();
        }

        public Venta GenerarVentaNoCompletada(IEnumerable<IPago> pagos, DtoUsuario usuario, Venta.ClasificacionesVenta clasificacionVenta, int? idPreventa, bool ignorarConsumo = false)
        {
            //if (Transaction.Current == null)
            //    throw new SOTException("Se requiere una transacción abierta para realizar la operación");

            if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
                throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

            if (pagos.Any(m => m.Activo && m.TipoPago == TiposPago.Reservacion) && pagos.Count(m => m.Activo) > 1)
                throw new SOTException(Recursos.Pagos.pago_debe_ser_unico_excepcion, TiposPago.Reservacion.Descripcion().ToLower());

            foreach (var pago in pagos.Where(m => m.Activo))
            {
                if (!usuario.Pagos.Contains(pago.IdTipoPago))
                    throw new Transversal.Excepciones.Seguridad.SOTPermisosException(Recursos.Pagos.usuario_sin_permiso_pago_excepcion, pago.TipoPago.Descripcion().ToLower());

                ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta, ignorarConsumo);
            }

            var transaccion = pagos.First().Transaccion;
            var sumaPagos = pagos.Where(m => m.Activo).Sum(m => m.Valor);
            var fecha = pagos.Min(m => m.FechaCreacion);

            decimal valor = 0, montoIgnorable = 0;

            foreach (var pago in pagos) 
            {
                switch (pago.TipoPago)
                {
                    case TiposPago.Consumo:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Cortesia:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Cupon:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Efectivo:
                        valor += pago.Valor;
                        break;
                    case TiposPago.Reservacion:
                        valor += pago.Valor;
                        break;
                    case TiposPago.TarjetaCredito:
                        valor += pago.Valor;
                        break;
                    case TiposPago.VPoints:
                        montoIgnorable += pago.Valor;
                        break;
                    case TiposPago.Transferencia:
                        valor += pago.Valor;
                        break;
                }
            }

            if (sumaPagos != (montoIgnorable + valor))
                throw new SOTException("Los pagos no encajan");

            return GenerarVenta(sumaPagos, montoIgnorable, valor, transaccion, idPreventa, fecha, usuario, clasificacionVenta);

            //foreach (var pago in pagos.Where(m => m.Activo))
            //{
            //    Pagar(venta, pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
            //}

            //venta.Correcta = true;

            //RepositorioVentas.Modificar(venta);
            //RepositorioVentas.GuardarCambios();
        }

        //public void ValidarPagos(IEnumerable<PagoRenta> pagos, DtoUsuario usuario)
        //{
        //    if (Transaction.Current == null)
        //        throw new SOTException("Se requiere una transacción abierta para realizar la operación");

        //    if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
        //        throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }
        //}

        //public void ValidarPagos(IEnumerable<PagoComanda> pagos, DtoUsuario usuario)
        //{
        //    if (Transaction.Current == null)
        //        throw new SOTException("Se requiere una transacción abierta para realizar la operación");

        //    if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
        //        throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }
        //}

        //public void ValidarPagos(IEnumerable<PagoOcupacionMesa> pagos, DtoUsuario usuario)
        //{
        //    if (Transaction.Current == null)
        //        throw new SOTException("Se requiere una transacción abierta para realizar la operación");

        //    if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
        //        throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }
        //}
        /**/
        //public Venta GenerarVentaNoCompletada(IEnumerable<IPago> pagos, DtoUsuario usuario)
        //{
        //    if (Transaction.Current == null)
        //        throw new SOTException("Se requiere una transacción abierta para realizar la operación");

        //    if (pagos.GroupBy(m => m.Transaccion).Count() != 1)
        //        throw new SOTException(Recursos.Pagos.pagos_diferentes_operaciones_excepcion);

        //    foreach (var pago in pagos.Where(m => m.Activo))
        //    {
        //        ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    }

        //    var transaccion = pagos.First().Transaccion;
        //    var valor = pagos.Where(m => m.Activo).Sum(m => m.Valor);
        //    var fecha = pagos.Min(m => m.FechaCreacion);

        //    return GenerarVenta(valor, transaccion, fecha, usuario);

        //    //foreach (var pago in pagos.Where(m => m.Activo))
        //    //{
        //    //    Pagar(venta, pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta);
        //    //}

        //    //venta.Correcta = true;

        //    //RepositorioVentas.Modificar(venta);
        //    //RepositorioVentas.GuardarCambios();
        //}

        public void ValidarEfectivoOTarjeta(TiposPago formaPago)
        {
            if (formaPago != TiposPago.Efectivo && formaPago != TiposPago.TarjetaCredito)
                throw new SOTException(Recursos.Pagos.forma_pago_no_efectivo_o_tarjeta_excepcion);
        }

        public void ValidarEfectivoOTarjeta(IPago pago)
        {
            if (pago.TipoPago != TiposPago.Efectivo && pago.TipoPago != TiposPago.TarjetaCredito)
                throw new SOTException(Recursos.Pagos.pago_no_efectivo_o_tarjeta_excepcion);
        }

        public void ValidarPago(IPago pago, DtoUsuario usuario)
        {
            ValidarPago(pago.Valor, pago.Referencia, pago.TipoPago, usuario, pago.TipoTarjeta, pago.NumeroTarjeta, false);
        }

        public void ValidarTransaccion(string transaccion)
        {
#warning ahorita no hace nada, corregir para que busque dentro de los pagos
        }

        private Venta GenerarVenta(decimal sumatoriaPagos, decimal montoIgnorable, decimal valor, string transaccion, int? idPreventa, DateTime fecha, DtoUsuario usuario, Venta.ClasificacionesVenta clasificacionVenta)
        {
            //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            //{
                if (RepositorioVentas.Alguno(m => m.Transaccion == transaccion))
                    throw new SOTException(Recursos.Pagos.transaccion_duplicada_excepcion);



                //Preventa prev;

                if (!idPreventa.HasValue)
                {
                    idPreventa = ServicioPreventas.GrenerarPreventa(clasificacionVenta);
                }

                //if (idPreventa.HasValue)
                //{
                var prev = ServicioPreventas.ObtenerPreventaPorId(idPreventa.Value);

                if (prev == null || !prev.Activa)
                    throw new SOTException("No existe una preventa con Id " + idPreventa.Value.ToString());
                //}
                //else
                //    prev = null;

                //if (prev == null)
                //    prev = ServicioPreventas.GrenerarPreventaVolatil(clasificacionVenta);

                //var ticket = RepositorioVentas.ObtenerUltimoTicket() ?? 0;

                var config = ServicioParametros.ObtenerParametros();



                var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();

                var venta = new Venta
                {
                    Activa = true,
                    FechaCreacion = fecha,
                    FechaModificacion = fecha,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    Transaccion = transaccion,
                    FolioTicket = prev.FolioTicket,
                    SerieTicket = prev.SerieTicket,
                    SumaPagos = sumatoriaPagos,
                    MontoIgnorable = montoIgnorable,
                    ValorConIVA = valor,
                    ValorIVA = (valor / (1 + config.Iva_CatPar)) * config.Iva_CatPar,
                    ValorSinIVA = valor / (1 + config.Iva_CatPar),
                    IdCorteTurno = corteActual.Id,
                    ClasificacionVenta = clasificacionVenta,
                    IVAEnCurso = config.Iva_CatPar,
                };


                RepositorioVentas.Agregar(venta);
                RepositorioVentas.GuardarCambios();


                venta = RepositorioVentas.Obtener(m => m.Id == venta.Id);

                if (venta == null)
                    throw new SOTException("Error al completar la venta");

                //scope.Complete();

                return venta;
            //}
        }

        private void Pagar(Venta venta, decimal monto, string codigo, TiposPago tipoPago, DtoUsuario usuario, TiposTarjeta? tipoT, string numetoTarjeta)
        {
            if (string.IsNullOrWhiteSpace(codigo) && tipoPago != TiposPago.Efectivo && tipoPago != TiposPago.Cortesia)
                throw new SOTException("No se ha proporcionado la información necesaria para realizar el pago");

            if (tipoPago <= 0)
                throw new SOTException(Recursos.Pagos.forma_pago_invalida_exception);

            if (monto < 0)
                throw new SOTException("No puede haber pagos con valor negativo");

            if (monto == 0 && tipoPago != TiposPago.Consumo && tipoPago != TiposPago.Cortesia)
                throw new SOTException("Solamente los consumos y cortesías pueden tener valor cero");

            //if (tipoPago == TiposPago.TarjetaCredito)
            //    ValidarTarjeta(tipoT);

            //if (tipoPago == TiposPago.Consumo && !ServicioConsumosInternos.VerificarGuid(codigo))
            //    throw new SOTException(Recursos.Pagos.informacion_consumo_requerida_excepcion);



            switch (tipoPago) 
            { 
                case TiposPago.VPoints:
                    ServicioVPoints.DescontarPuntos(codigo, venta.Ticket.ToString(), monto);
                    break;
                default:
                    break;
            }
        }

        private void ValidarPago(decimal monto, string codigo, TiposPago tipoPago, DtoUsuario usuario, TiposTarjeta? tipoT, string numeroTarjeta, bool ignorarConsumo)
        {
            if (string.IsNullOrWhiteSpace(codigo) && tipoPago != TiposPago.Efectivo && tipoPago != TiposPago.Cortesia)
                throw new SOTException("No se ha proporcionado la información necesaria para realizar el pago");

            if (tipoPago <= 0)
                throw new SOTException(Recursos.Pagos.forma_pago_invalida_exception);

            if (monto < 0)
                throw new SOTException("No puede haber pagos con valor negativo");

            if(monto == 0 && tipoPago != TiposPago.Consumo && tipoPago != TiposPago.Cortesia)
                throw new SOTException("Solamente los consumos y cortesías pueden tener valor cero");

            if (tipoPago == TiposPago.TarjetaCredito)
            {
                ValidarTarjeta(tipoT);
                if (!Transversal.Utilidades.UtilidadesRegex.SoloDigitos(numeroTarjeta))
                    throw new SOTException(Recursos.Pagos.numero_tarjeta_no_solo_numeros_excepcion);
            }
            else
                if (!string.IsNullOrEmpty(numeroTarjeta))
                    throw new SOTException(Recursos.Pagos.numero_tarjeta_no_requerido_excepcion, tipoPago.Descripcion());

            //if (tipoPago == TiposPago.Consumo && !ServicioConsumosInternos.VerificarGuid(codigo))
            //    throw new SOTException(Recursos.Pagos.informacion_consumo_requerida_excepcion);

            switch (tipoPago)
            {
                case TiposPago.VPoints:
                    ServicioVPoints.ObtenerSaldoTarjetaV(codigo);
                    break;
                case TiposPago.Consumo:
                    if (!ignorarConsumo && !ServicioConsumosInternos.VerificarGuid(codigo) && !ServicioConsumosInternosHabitacion.VerificarGuid(codigo))
                        throw new SOTException(Recursos.Pagos.informacion_consumo_requerida_excepcion);
                    break;

                case TiposPago.Cortesia:
                    if (string.IsNullOrEmpty(codigo))
                        throw new SOTException(Recursos.Pagos.observaciones_cortesia_requeridas_excepcion);
                    break;
                default:
                    break;
            }
        }

        

        private void ValidarTarjeta(TiposTarjeta? tipoT)
        {
            if (!tipoT.HasValue ||
                (tipoT != TiposTarjeta.AmericanExpress &&
                 tipoT != TiposTarjeta.MasterCard &&
                 tipoT != TiposTarjeta.Visa))
                throw new SOTException(Recursos.Pagos.tipo_tarjeta_desconocido_excepcion);
        }


        public Dictionary<string, string> ObtenerDiccionarioTransaccionesTicketsIgnoraEstado(List<string> transacciones)
        {
            return RepositorioVentas.ObtenerElementos(m => transacciones.Contains(m.Transaccion)).Select(m => new { m.Transaccion, m.SerieTicket, m.FolioTicket }).ToList().
                                                           ToDictionary(m => m.Transaccion, m => m.SerieTicket + m.FolioTicket.ToString());
        }

        #region métodos internal

        DtoUsuario IServicioPagosInterno.ProcesarPermisosEspeciales(IEnumerable<IPago> pagos, DtoUsuario usuario)
        {
            foreach (var item in pagos.Where(m => m.Valor != 0))
            {
                if (RepositorioPagosSeguros.Alguno(m => m.IdTipoPago == item.IdTipoPago))
                    return ServicioPermisos.ObtenerUsuarioPorCredencial();
            }

            return usuario;
        }

        DtoUsuario IServicioPagosInterno.ProcesarPermisosEspeciales(IEnumerable<DtoInformacionPago> pagos, DtoUsuario usuario)
        {
            foreach (var item in pagos.Where(m => m.Valor != 0))
            {
                var idTipoPago = (int)item.TipoPago;

                if (RepositorioPagosSeguros.Alguno(m => m.IdTipoPago == idTipoPago))
                    return ServicioPermisos.ObtenerUsuarioPorCredencial();
            }

            return usuario;
        }

        void IServicioPagosInterno.CancelarPago(IPago pago, string transaccionCancelacion, int idUsuario)
        {
            if (string.IsNullOrWhiteSpace(pago.Referencia) && pago.TipoPago != TiposPago.Efectivo && pago.TipoPago != TiposPago.Cortesia)
                throw new SOTException("No se ha proporcionado la información necesaria para realizar el pago");

            if (pago.TipoPago <= 0)
                throw new SOTException(Recursos.Pagos.forma_pago_invalida_exception);

            if (pago.Valor < 0)
                throw new SOTException("No puede haber pagos con valor negativo");

            if (pago.Valor == 0 && pago.TipoPago != TiposPago.Consumo && pago.TipoPago != TiposPago.Cortesia)
                throw new SOTException("Solamente los consumos y cortesías pueden tener valor cero");

            if (pago.TipoPago == TiposPago.TarjetaCredito)
            {
                ValidarTarjeta(pago.TipoTarjeta);
                if (!Transversal.Utilidades.UtilidadesRegex.SoloDigitos(pago.Referencia))
                    throw new SOTException(Recursos.Pagos.numero_tarjeta_no_solo_numeros_excepcion);
            }
            else
                if (!string.IsNullOrEmpty(pago.NumeroTarjeta))
                    throw new SOTException(Recursos.Pagos.numero_tarjeta_no_requerido_excepcion, pago.TipoPago.Descripcion());

            //if (tipoPago == TiposPago.Consumo && !ServicioConsumosInternos.VerificarGuid(codigo))
            //    throw new SOTException(Recursos.Pagos.informacion_consumo_requerida_excepcion);

            switch (pago.TipoPago)
            {
                case TiposPago.VPoints:
                    ServicioVPoints.ReembolsarPuntos(pago.Referencia, transaccionCancelacion, pago.Valor, idUsuario);
                    break;
                case TiposPago.Consumo:
                    ServicioConsumosInternosHabitacion.CancelarConsumo(pago.Referencia, idUsuario);
                    break;

                //case TiposPago.Cortesia:
                //    if (string.IsNullOrEmpty(codigo))
                //        throw new SOTException(Recursos.Pagos.observaciones_cortesia_requeridas_excepcion);
                //    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
