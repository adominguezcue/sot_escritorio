﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Pagos
{
    internal interface IServicioPagosInterno : IServicioPagos
    {
        void CancelarPago(IPago pago, string transaccionCancelacion, int idUsuario);

        DtoUsuario ProcesarPermisosEspeciales(IEnumerable<IPago> pagos, DtoUsuario usuario);

        DtoUsuario ProcesarPermisosEspeciales(IEnumerable<DtoInformacionPago> pagos, DtoUsuario usuario);
    }
}
