﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConfiguracionesPropinas
{
    internal interface IServicioConfiguracionesPropinasInterno : IServicioConfiguracionesPropinas
    {
        ConfiguracionPropinas ObtenerConfiguracionPropinas();
    }
}
