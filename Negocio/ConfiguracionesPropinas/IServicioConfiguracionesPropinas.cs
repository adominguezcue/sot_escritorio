﻿using Modelo;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConfiguracionesPropinas
{
    public interface IServicioConfiguracionesPropinas
    {
        /// <summary>
        /// Retorna la configuración activa de las propinas
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        ConfiguracionPropinas ObtenerConfiguracionPropinas(DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar la configuración
        /// </summary>
        /// <param name="configuracionPropinas"></param>
        /// <param name="usuario"></param>
        void ActualizarConfiguracionPropinas(ConfiguracionPropinas configuracionPropinas, DtoUsuario usuario);
    }
}
