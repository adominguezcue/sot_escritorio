﻿using Dominio.Nucleo.Entidades;
using Modelo;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Lineas;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.ConfiguracionesPropinas
{
    public class ServicioConfiguracionesPropinas : IServicioConfiguracionesPropinasInterno
    {
        IServicioLineas ServicioLineas
        {
            get { return _servicioTiposArticulos.Value; }
        }

        Lazy<IServicioLineas> _servicioTiposArticulos = new Lazy<IServicioLineas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioLineas>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });


        IRepositorioConfiguracionesPropinas RepositorioConfiguracionesPropinas
        {
            get { return _repositorioConfiguracionesPropinas.Value; }
        }

        Lazy<IRepositorioConfiguracionesPropinas> _repositorioConfiguracionesPropinas = new Lazy<IRepositorioConfiguracionesPropinas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesPropinas>(); });


        public ConfiguracionPropinas ObtenerConfiguracionPropinas(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConfiguracionPropinas = true });

            return ObtenerConfiguracionPropinasPrivate();
        }

        public void ActualizarConfiguracionPropinas(ConfiguracionPropinas configuracionPropinas, DtoUsuario usuario)
        {
            if (configuracionPropinas == null)
                throw new SOTException(Recursos.Propinas.configuracion_nula_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarConfiguracionPropinas = true });

            var fachaActual = DateTime.Now;

            configuracionPropinas.FechaModificacion = fachaActual;
            configuracionPropinas.IdUsuarioModifico = usuario.Id;

            foreach (var p in configuracionPropinas.PorcentajesPropinaTipo)
            {
                p.EntidadEstado = EntidadEstados.Modificado;
            }

            foreach (var p in configuracionPropinas.PorcentajesTarjeta)
            {
                p.EntidadEstado = EntidadEstados.Modificado;
            }

            foreach (var p in configuracionPropinas.FondosDia)
            {
                p.EntidadEstado = EntidadEstados.Modificado;
            }

            RepositorioConfiguracionesPropinas.Modificar(configuracionPropinas);
            RepositorioConfiguracionesPropinas.GuardarCambios();
        }

        #region métodos internal

        ConfiguracionPropinas IServicioConfiguracionesPropinasInterno.ObtenerConfiguracionPropinas()
        {
            return ObtenerConfiguracionPropinasPrivate();
        }

        #endregion

        private ConfiguracionPropinas ObtenerConfiguracionPropinasPrivate()
        {
            var configuracion = RepositorioConfiguracionesPropinas.ObtenerConfiguracionActiva();

            var idsLineas = configuracion.PorcentajesPropinaTipo.Select(m => m.IdLineaArticulo).ToList();

            var tipos = ServicioLineas.ObtenerLineasPorId(idsLineas);

            foreach (var propina in configuracion.PorcentajesPropinaTipo)
            {
                propina.LineaTmp = tipos.FirstOrDefault(m => m.Cod_Linea == propina.IdLineaArticulo);
            }

            return configuracion;
        }
    }
}
