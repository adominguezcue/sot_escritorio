﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.ConfiguracionesAsistencia;
using Negocio.Empleados;
using Negocio.Seguridad.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Asistencias
{
    public interface IServicioAsistencias
    {
        /// <summary>
        /// Retorna un resumen de la última asistencia en curso del empleado con el número proporcionado
        /// </summary>
        /// <param name="numeroEmpleado"></param>
        /// <returns></returns>
        DtoResumenAsistencia ObtenerResumenUltimaAsistenciaAbierta(string numeroEmpleado);
        /// <summary>
        /// Retorna las asistencias filtradas
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="telefono"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<Asistencia> ObtenerAsistenciasFiltadas(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);
        /// <summary>
        /// Permite registrar una asistencia
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="datosCredencial"></param>
        /// <returns></returns>
        Asistencia RegistrarAsistencia(int idEmpleado, DtoCredencial datosCredencial);
        /// <summary>
        /// Permite registrar la salida a comer
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="datosCredencial"></param>
        /// <returns></returns>
        Asistencia RegistrarSalidaAComer(int idEmpleado, DtoCredencial datosCredencial);
        /// <summary>
        /// Permite registrar el regreso de la salida a comer, siempre y cuando exista una asistencia y una salida a comer registradas
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="datosCredencial"></param>
        /// <returns></returns>
        Asistencia RegistrarRegresoComer(int idEmpleado, DtoCredencial datosCredencial);
        /// <summary>
        /// Permite registrar la asistencia de un empleado diferente, siempre y cuando los permisos
        /// sean los adecuados
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="fechaEntrada"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Asistencia RegistrarAsistencia(DtoUsuario usuario, int idEmpleado, DateTime fechaEntrada, DateTime? fechaSalida = null);
        /// <summary>
        /// Permite marcar una asistencia como eliminada
        /// </summary>
        /// <param name="idAsistencia"></param>
        /// <param name="usuario"></param>
        void EliminarAsistencia(int idAsistencia, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar la información de una asistencia
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="idAsistencia"></param>
        /// <param name="fechaEntrada"></param>
        /// <param name="fechaSalida"></param>
        void ModificarAsistencia(DtoUsuario usuario, int idAsistencia, DateTime fechaEntrada, DateTime? fechaSalida);
        /// <summary>
        /// Permite justificar un retardo o una falta
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="idAsistencia"></param>
        /// <param name="motivo"></param>
        void JustificarRetardoFalta(DtoUsuario usuario, int idAsistencia, string motivo);
    }
}
