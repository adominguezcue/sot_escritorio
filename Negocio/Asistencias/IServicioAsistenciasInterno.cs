﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Asistencias
{
    internal interface IServicioAsistenciasInterno : IServicioAsistencias
    {
        /// <summary>
        /// Retorna las asistencias de un empleado dentro del período especificado
        /// </summary>
        /// <param name="idEmpleado"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<Asistencia> ObtenerAsistenciasEmpleado(int idEmpleado, DateTime fechaInicio, DateTime fechaFin);
    }
}
