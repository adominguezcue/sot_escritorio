﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.ConfiguracionesAsistencia;
using Negocio.CortesTurno;
using Negocio.Empleados;
using Negocio.Seguridad.Permisos;
using Negocio.Seguridad.Usuarios;
using Negocio.SolicitudesPermisosFaltas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Asistencias
{
    public class ServicioAsistencias : IServicioAsistenciasInterno
    {
        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IServicioConfiguracionesAsistencias ServicioConfiguracionesAsistencias
        {
            get { return _servicioConfiguracionesAsistencias.Value; }
        }

        Lazy<IServicioConfiguracionesAsistencias> _servicioConfiguracionesAsistencias = new Lazy<IServicioConfiguracionesAsistencias>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesAsistencias>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioSolicitudesPermisosFaltas ServicioSolicitudesPermisosFaltas
        {
            get { return _servicioSolicitudesPermisosFaltas.Value; }
        }

        Lazy<IServicioSolicitudesPermisosFaltas> _servicioSolicitudesPermisosFaltas = new Lazy<IServicioSolicitudesPermisosFaltas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioSolicitudesPermisosFaltas>(); });

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });


        IRepositorioAsistencias RepositorioAsistencias
        {
            get { return _repostorioAsistencias.Value; }
        }

        Lazy<IRepositorioAsistencias> _repostorioAsistencias = new Lazy<IRepositorioAsistencias>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioAsistencias>(); });

        public DtoResumenAsistencia ObtenerResumenUltimaAsistenciaAbierta(string numeroEmpleado) 
        {
            return RepositorioAsistencias.ObtenerResumenUltimaAsistenciaAbierta(numeroEmpleado);
        }

        private Asistencia RegistrarAsistencia(DateTime fechaActual, Empleado empleado, DtoUsuario usuario) 
        {
            var siguienteConfigT = ServicioCortesTurno.ObtenerSiguienteConfiguracionTurno(empleado.Turno.Orden);
            var config = ServicioConfiguracionesAsistencias.ObtenerConfiguracionAsistencias();

            var fechaEntradaIdeal = fechaActual.Date.AddMinutes(empleado.Turno.MinutosInicioAsistencia);
            var salidaTentativa = fechaActual.Date.AddMinutes(siguienteConfigT.MinutosInicioAsistencia);

            if (salidaTentativa <= fechaEntradaIdeal)
                salidaTentativa = salidaTentativa.AddDays(1);

            /*
             
             * entrada menor, salida entre e y s
             * entrada y salida entre e y s
             * entrada entre e y s y salida mayor
             * e y s entre entrada y salida

             */

            ValidarNoDuplicidad(0, empleado.Id, fechaActual, salidaTentativa);
            
            bool tieneFalta = false, tieneRetardo = false;
            
            if(fechaActual > fechaEntradaIdeal)
            {
                 if(!(tieneFalta = ((fechaActual - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Falta))))
                 {
                    tieneRetardo = ((fechaActual - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Retardo));
                 }
            }

            var asistencia = new Asistencia
            {
                Activa = true,
                FechaEntradaIdeal = fechaEntradaIdeal,
                FechaEntrada = fechaActual,
                FechaSalidaTentativa = salidaTentativa,
                IdEmpleado = empleado.Id,
                IdTurno = empleado.IdTurno,
                TieneFalta = tieneFalta,
                TieneRetardo = tieneRetardo,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                IdUsuarioModifico = usuario.Id, 
                MotivoRetardoFalta = ""
            };

            RepositorioAsistencias.Agregar(asistencia);
            RepositorioAsistencias.GuardarCambios();

            return asistencia;
        }

        public Asistencia RegistrarAsistencia(int idEmpleado, DtoCredencial datosCredencial)
        {
            var usuario = ServicioUsuarios.ObtenerUsuarioPorCredencial(idEmpleado, datosCredencial);

            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearAsistencias = true });

            var empleado = ServicioEmpleados.ObtenerEmpleadoActivoNoNullConTurno(idEmpleado);

            var asistencia = RepositorioAsistencias.ObtenerUltimaSinSalida(idEmpleado);

            var fechaActual = DateTime.Now;

            if(asistencia == null)
                return RegistrarAsistencia(fechaActual, empleado, usuario);

            asistencia.FechaSalida = fechaActual;
            asistencia.FechaModificacion = fechaActual;
            asistencia.IdUsuarioModifico = usuario.Id;

            if (!asistencia.FinComida.HasValue)
                asistencia.FinComida = fechaActual;

            if (string.IsNullOrEmpty(asistencia.MotivoRetardoFalta))
                asistencia.MotivoRetardoFalta = "";

            RepositorioAsistencias.Modificar(asistencia);
            RepositorioAsistencias.GuardarCambios();

            return asistencia;
        }

        public Asistencia RegistrarSalidaAComer(int idEmpleado, DtoCredencial datosCredencial)
        {
            var usuario = ServicioUsuarios.ObtenerUsuarioPorCredencial(idEmpleado, datosCredencial);

            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearAsistencias = true });

            var empleado = ServicioEmpleados.ObtenerEmpleadoActivoNoNullConTurno(idEmpleado);

            var asistencia = RepositorioAsistencias.ObtenerUltimaSinSalida(idEmpleado);

            if (asistencia == null)
                throw new SOTException(Recursos.Asistencias.asistencia_no_registrada_excepcion);
            if (asistencia.InicioComida.HasValue)
                throw new SOTException(Recursos.Asistencias.salida_comer_registrada_excepcion);

            var fechaActual = DateTime.Now;

            asistencia.InicioComida = fechaActual;
            asistencia.FechaModificacion = fechaActual;
            asistencia.IdUsuarioModifico = usuario.Id;

            RepositorioAsistencias.Modificar(asistencia);
            RepositorioAsistencias.GuardarCambios();

            return asistencia;
        }

        public Asistencia RegistrarRegresoComer(int idEmpleado, DtoCredencial datosCredencial)
        {
            var usuario = ServicioUsuarios.ObtenerUsuarioPorCredencial(idEmpleado, datosCredencial);

            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearAsistencias = true });

            var empleado = ServicioEmpleados.ObtenerEmpleadoActivoNoNullConTurno(idEmpleado);

            var asistencia = RepositorioAsistencias.ObtenerUltimaSinSalida(idEmpleado);

            if (asistencia == null)
                throw new SOTException(Recursos.Asistencias.asistencia_no_registrada_excepcion);
            if (!asistencia.InicioComida.HasValue)
                throw new SOTException(Recursos.Asistencias.no_salida_comer_excepcion);
            if (asistencia.FinComida.HasValue)
                throw new SOTException(Recursos.Asistencias.regreso_salida_comer_registrado_excepcion);

            var fechaActual = DateTime.Now;

            asistencia.FinComida = fechaActual;
            asistencia.FechaModificacion = fechaActual;
            asistencia.IdUsuarioModifico = usuario.Id;

            RepositorioAsistencias.Modificar(asistencia);
            RepositorioAsistencias.GuardarCambios();

            return asistencia;
        }

        public Asistencia SuplirAsistencia(int idEmpleado, int idEmpleadoSuplir, DtoCredencial datosCredencial)
        {
            var fechaActual = DateTime.Now;

            var usuario = ServicioUsuarios.ObtenerUsuarioPorCredencial(idEmpleado, datosCredencial);

            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            //ServicioPermisos.ValidarPermisos();

            var empleado = ServicioEmpleados.ObtenerEmpleadoActivoNoNullConTurno(idEmpleado);
            var empleadoSuplir = ServicioEmpleados.ObtenerEmpleadoActivoNoNullConTurno(idEmpleadoSuplir);

            var siguienteConfigT = ServicioCortesTurno.ObtenerSiguienteConfiguracionTurno(empleadoSuplir.Turno.Orden);
            var config = ServicioConfiguracionesAsistencias.ObtenerConfiguracionAsistencias();

            var fechaEntradaIdeal = fechaActual.Date.AddMinutes(empleadoSuplir.Turno.MinutosInicioAsistencia);
            var salidaTentativa = fechaActual.Date.AddMinutes(siguienteConfigT.MinutosInicioAsistencia);

            if (salidaTentativa <= fechaEntradaIdeal)
                salidaTentativa = salidaTentativa.AddDays(1);

            /*
             
             * entrada menor, salida entre e y s
             * entrada y salida entre e y s
             * entrada entre e y s y salida mayor
             * e y s entre entrada y salida

             */

            ValidarNoDuplicidad(0, empleado.Id, fechaActual, salidaTentativa);

            bool tieneFalta = false, tieneRetardo = false;

            if (fechaActual > fechaEntradaIdeal)
            {
                if (!(tieneFalta = ((fechaActual - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Falta))))
                {
                    tieneRetardo = ((fechaActual - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Retardo));
                }
            }

            var asistencia = new Asistencia
            {
                Activa = true, 
                EsDobleTurno = true,
                FechaEntradaIdeal = fechaEntradaIdeal,
                FechaEntrada = fechaActual,
                FechaSalidaTentativa = salidaTentativa,
                IdEmpleado = empleado.Id,
                IdTurno = empleadoSuplir.IdTurno,
                TieneFalta = tieneFalta,
                TieneRetardo = tieneRetardo,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                IdUsuarioModifico = usuario.Id
            };

            RepositorioAsistencias.Agregar(asistencia);
            RepositorioAsistencias.GuardarCambios();

            return asistencia;
        }

        public void EliminarAsistencia(int idAsistencia, DtoUsuario usuario)
        {
            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarAsistencias = true });

            var asistencia = RepositorioAsistencias.Obtener(m => m.Activa && m.Id == idAsistencia);

            if (asistencia == null)
                throw new SOTException(Recursos.Asistencias.asistencia_nula_eliminada_excepcion);

            var fechaActual = DateTime.Now;

            asistencia.Activa = false;
            asistencia.FechaEliminacion = fechaActual;
            asistencia.FechaModificacion = fechaActual;
            asistencia.IdUsuarioElimino = usuario.Id;
            asistencia.IdUsuarioModifico = usuario.Id;

            RepositorioAsistencias.Modificar(asistencia);
            RepositorioAsistencias.GuardarCambios();
        }

        private void ValidarNoDuplicidad(int idAsistencia, int idEmpleado, DateTime entrada, DateTime salida)
        {
            var duplicada = RepositorioAsistencias.Obtener(m => m.Id != idAsistencia && m.Activa && m.IdEmpleado == idEmpleado && ((m.FechaEntrada <= entrada && (m.FechaSalida ?? m.FechaSalidaTentativa) >= entrada && (m.FechaSalida ?? m.FechaSalidaTentativa) <= salida) ||
                                                 (m.FechaEntrada >= entrada && m.FechaEntrada <= salida) ||
                                                 (m.FechaEntrada <= entrada && (m.FechaSalida ?? m.FechaSalidaTentativa) >= salida) ||
                                                 (m.FechaEntrada >= entrada && (m.FechaSalida ?? m.FechaSalidaTentativa) <= salida)));

            if (duplicada != null)
                if (duplicada.FechaSalida.HasValue)
                    throw new SOTException(Recursos.Asistencias.asistencia_cruzada_excepcion, duplicada.FechaEntrada.ToString("dd/MM/yyyy hh:mm:ss tt"), duplicada.FechaSalida.Value.ToString("dd/MM/yyyy hh:mm:ss tt"));
                else
                    throw new SOTException(Recursos.Asistencias.posible_asistencia_cruzada_excepcion, duplicada.FechaEntrada.ToString("dd/MM/yyyy hh:mm:ss tt"), duplicada.FechaSalidaTentativa.ToString("dd/MM/yyyy hh:mm:ss tt"));

        }

        public Asistencia RegistrarAsistencia(DtoUsuario usuario, int idEmpleado, DateTime fechaEntrada, DateTime? fechaSalida = null)
        {
            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearAsistencias = true });

            if (fechaEntrada > DateTime.Now)
                throw new SOTException(Recursos.Asistencias.fecha_entrada_mayor_fecha_actual_excepcion);

            if (fechaSalida.HasValue && fechaSalida <= fechaEntrada)
                throw new SOTException(Recursos.Asistencias.fecha_entrada_mayor_igual_fecha_salida_excepcion);

            var empleado = ServicioEmpleados.ObtenerEmpleadoActivoNoNullConTurno(idEmpleado);

            var siguienteConfigT = ServicioCortesTurno.ObtenerSiguienteConfiguracionTurno(empleado.Turno.Orden);
            var config = ServicioConfiguracionesAsistencias.ObtenerConfiguracionAsistencias();

            var fechaEntradaIdeal = fechaEntrada.Date.AddMinutes(empleado.Turno.MinutosInicioAsistencia);
            var fechaSalidaTentativa = fechaEntrada.Date.AddMinutes(siguienteConfigT.MinutosInicioAsistencia);

            if (fechaSalidaTentativa <= fechaEntradaIdeal)
                fechaSalidaTentativa = fechaSalidaTentativa.AddDays(1);

            ValidarNoDuplicidad(0, idEmpleado, fechaEntrada, fechaSalida ?? fechaSalidaTentativa);

            bool tieneFalta = false, tieneRetardo = false;

            if (fechaEntrada > fechaEntradaIdeal)
            {
                if (!(tieneFalta = ((fechaEntrada - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Falta))))
                {
                    tieneRetardo = ((fechaEntrada - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Retardo));
                }
            }

            var fechaActual = DateTime.Now;

            var asistencia = new Asistencia
            {
                Activa = true,
                FechaEntradaIdeal = fechaEntradaIdeal,
                FechaEntrada = fechaEntrada,
                FechaSalida = fechaSalida,
                FechaSalidaTentativa = fechaSalidaTentativa,
                IdEmpleado = empleado.Id,
                IdTurno = empleado.IdTurno,
                TieneFalta = tieneFalta,
                TieneRetardo = tieneRetardo,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                IdUsuarioModifico = usuario.Id,
                MotivoRetardoFalta = ""
            };

            RepositorioAsistencias.Agregar(asistencia);
            RepositorioAsistencias.GuardarCambios();

            return asistencia;
        }

        public List<Asistencia> ObtenerAsistenciasFiltadas(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaInicio = null, DateTime? fechaFin = null) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarAsistencias = true });

            return RepositorioAsistencias.ObtenerAsistenciasFiltadas(nombre, apellidoPaterno, apellidoMaterno, telefono, fechaInicio, fechaFin);
        }


        public void ModificarAsistencia(DtoUsuario usuario, int idAsistencia, DateTime fechaEntrada, DateTime? fechaSalida)
        {
            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarAsistencias = true });

            if (fechaEntrada > DateTime.Now)
                throw new SOTException(Recursos.Asistencias.fecha_entrada_mayor_fecha_actual_excepcion);

            if (fechaSalida.HasValue && fechaSalida <= fechaEntrada)
                throw new SOTException(Recursos.Asistencias.fecha_entrada_mayor_igual_fecha_salida_excepcion);

            var asistencia = RepositorioAsistencias.Obtener(m => m.Activa && m.Id == idAsistencia, m=> m.ConfiguracionTurno);

            if (asistencia == null)
                throw new SOTException(Recursos.Asistencias.asistencia_nula_eliminada_excepcion);

            //var empleado = ServicioEmpleados.ObtenerEmpleadoActivoNoNullConTurno(asistencia.IdEmpleado);

            var siguienteConfigT = ServicioCortesTurno.ObtenerSiguienteConfiguracionTurno(asistencia.ConfiguracionTurno.Orden);
            var config = ServicioConfiguracionesAsistencias.ObtenerConfiguracionAsistencias();

            var fechaEntradaIdeal = fechaEntrada.Date.AddMinutes(asistencia.ConfiguracionTurno.MinutosInicioAsistencia);
            var fechaSalidaTentativa = fechaEntrada.Date.AddMinutes(siguienteConfigT.MinutosInicioAsistencia);

            if (fechaSalidaTentativa <= fechaEntradaIdeal)
                fechaSalidaTentativa = fechaSalidaTentativa.AddDays(1);

            ValidarNoDuplicidad(asistencia.Id, asistencia.IdEmpleado, fechaEntrada, fechaSalida ?? fechaSalidaTentativa);

            bool tieneFalta = false, tieneRetardo = false;

            if (fechaEntrada > fechaEntradaIdeal)
            {
                if (!(tieneFalta = ((fechaEntrada - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Falta))))
                {
                    tieneRetardo = ((fechaEntrada - fechaEntradaIdeal) >= TimeSpan.FromMinutes(config.Retardo));
                }
            }

            var fechaActual = DateTime.Now;

            asistencia.FechaEntrada = fechaEntrada;
            asistencia.FechaEntradaIdeal = fechaEntradaIdeal;
            asistencia.FechaSalidaTentativa = fechaSalidaTentativa;
            asistencia.FechaSalida = fechaSalida;
            asistencia.FechaModificacion = fechaActual;
            //asistencia.IdTurno = empleado.IdTurno;
            asistencia.TieneFalta = tieneFalta;
            asistencia.TieneRetardo = tieneRetardo;
            asistencia.IdUsuarioModifico = usuario.Id;


            RepositorioAsistencias.Modificar(asistencia);
            RepositorioAsistencias.GuardarCambios();
        }

        public void JustificarRetardoFalta(DtoUsuario usuario, int idAsistencia, string motivo) 
        {
            if (usuario == null)
                throw new SOTException(Recursos.Asistencias.empleado_no_reconocido_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { JustificarRetardoFalta = true });

            var asistencia = RepositorioAsistencias.Obtener(m => m.Activa && m.Id == idAsistencia);

            if (asistencia == null)
                throw new SOTException(Recursos.Asistencias.asistencia_nula_eliminada_excepcion);

            if (!asistencia.TieneFalta && !asistencia.TieneRetardo)
                throw new SOTException(Recursos.Asistencias.asistencia_no_requiere_justificacion_excepcion);

            if (string.IsNullOrWhiteSpace(motivo))
                throw new SOTException(Recursos.Asistencias.motivo_justificacion_invalido_excepcion);

            var fechaActual = DateTime.Now;

            asistencia.Justificado = true;
            asistencia.MotivoRetardoFalta = motivo.Trim();
            asistencia.FechaModificacion = fechaActual;
            asistencia.FechaJustificacion = fechaActual;
            asistencia.IdUsuarioModifico = usuario.Id;
            asistencia.IdUsuarioJustifico = usuario.Id;

            RepositorioAsistencias.Modificar(asistencia);
            RepositorioAsistencias.GuardarCambios();
        }

        List<Asistencia> IServicioAsistenciasInterno.ObtenerAsistenciasEmpleado(int idEmpleado, DateTime fechaInicio, DateTime fechaFin)
        {
            return RepositorioAsistencias.ObtenerElementos(m => m.Activa && m.IdEmpleado == idEmpleado && m.FechaEntradaIdeal >= fechaInicio && m.FechaEntradaIdeal <= fechaFin).ToList();
        }
    }
}
