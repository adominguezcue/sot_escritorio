﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Negocio.Recursos {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Paquetes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Paquetes() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Negocio.Recursos.Paquetes", typeof(Paquetes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ingrese una clave válida para el paquete.
        /// </summary>
        internal static string clave_invalida_excepcion {
            get {
                return ResourceManager.GetString("clave_invalida_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a La clave {0} ya se encuentra en uso por otro paquete.
        /// </summary>
        internal static string clave_paquete_duplicada_exception {
            get {
                return ResourceManager.GetString("clave_paquete_duplicada_exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Cuando solamente se proporciona el día, éste debe ir del 1 al 7 representando los días de lunes a domingo.
        /// </summary>
        internal static string dia_programacion_invalida_exception {
            get {
                return ResourceManager.GetString("dia_programacion_invalida_exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a El día y el mes seleccionados para la aplicación de los paquetes no son válidos.
        /// </summary>
        internal static string fecha_programacion_invalida_exception {
            get {
                return ResourceManager.GetString("fecha_programacion_invalida_exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ingrese un nombre válido para el paquete.
        /// </summary>
        internal static string nombre_invalido_excepcion {
            get {
                return ResourceManager.GetString("nombre_invalido_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a El paquete para este tipo de habitación ya existe, seleccione otro tipo o cambie el nombre del paquete.
        /// </summary>
        internal static string paquete_duplicado_tipo_exception {
            get {
                return ResourceManager.GetString("paquete_duplicado_tipo_exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a El paquete es nulo.
        /// </summary>
        internal static string paquete_nulo_exception {
            get {
                return ResourceManager.GetString("paquete_nulo_exception", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a El paquete no tiene valor.
        /// </summary>
        internal static string paquete_sin_valor_excepcion {
            get {
                return ResourceManager.GetString("paquete_sin_valor_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Un paquete solamente puede tener valor en precio o descuento, pero no en ambos campos.
        /// </summary>
        internal static string precio_y_descuento_con_valor_excepcion {
            get {
                return ResourceManager.GetString("precio_y_descuento_con_valor_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Este paquete no requiere  programación.
        /// </summary>
        internal static string programacion_configurada_excepcion {
            get {
                return ResourceManager.GetString("programacion_configurada_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Para programar un paquete se debe proporcionar el día de la semana en la que se va a aplicar como mínimo.
        /// </summary>
        internal static string programacion_no_configurada_excepcion {
            get {
                return ResourceManager.GetString("programacion_no_configurada_excepcion", resourceCulture);
            }
        }
    }
}
