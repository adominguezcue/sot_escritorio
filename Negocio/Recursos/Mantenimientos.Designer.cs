﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Negocio.Recursos {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Mantenimientos {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Mantenimientos() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Negocio.Recursos.Mantenimientos", typeof(Mantenimientos).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a El mantenimiento correctivo requiere que se especifiquen una o más órdenes de trabajo para poder ser finalizado.
        /// </summary>
        internal static string mantenimiento_correctivo_sin_ordenes_excepcion {
            get {
                return ResourceManager.GetString("mantenimiento_correctivo_sin_ordenes_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a La habitación ya cuenta con un mantenimiento en curso.
        /// </summary>
        internal static string mantenimiento_en_curso_excepcion {
            get {
                return ResourceManager.GetString("mantenimiento_en_curso_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a No se están realizando trabajos de mantenimiento en esta habitación.
        /// </summary>
        internal static string mantenimiento_nulo_excepcion {
            get {
                return ResourceManager.GetString("mantenimiento_nulo_excepcion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a No se ha seleccionado una taraea de mantenimiento válida.
        /// </summary>
        internal static string tarea_nula_excepcion {
            get {
                return ResourceManager.GetString("tarea_nula_excepcion", resourceCulture);
            }
        }
    }
}
