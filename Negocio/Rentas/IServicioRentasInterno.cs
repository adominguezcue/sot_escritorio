﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Rentas
{
    internal interface IServicioRentasInterno : IServicioRentas
    {
        /// <summary>
        /// Finaliza la renta actual de la habitación especificada
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void FinalizarRenta(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Permite cancelar la renta actual de la habitación seleccionada
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        List<int> CancelarRenta(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Retorna el número de habitación relacionada con una transacción
        /// </summary>
        /// <param name="numeroTransaccion"></param>
        /// <returns></returns>
        string ObtenerNumeroHabitacionPorTransaccion(string numeroTransaccion);
        string ObtenerNumeroHabitacionPorIdRenta(int idRenta);
        string ObtenerNumeroHabitacionPorIdComanda(int idComanda);
        /// <summary>
        /// Retorna el número de habitación relacionada con una transacción de room service
        /// </summary>
        /// <param name="numeroTransaccion"></param>
        /// <returns></returns>
        string ObtenerNumeroHabitacionPorTransaccionRoomService(string numeroTransaccion);

        /// <summary>
        /// Retorna la última renta que involucre a la habitación con el id proporcionado o null en
        /// caso de no existir rentas que la usen
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        Renta ObtenerUltimaRentaPorHabitacionCargada(int idHabitacion);

        string ObtieneCodigoReservacion(DateTime fecharegistro, int idHabitacion);
        
        /// <summary>
        /// Permite obtener el valor de todos los pagos de habitación realizados en el rango de
        /// fechas solicitados.
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<DetallePago> ObtenerDetallesPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Retorna los datos de los paquetes ligados a rentas que fueron vendidos dentro del período especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<PaqueteRenta> ObtenerPaquetesRentaPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);
        ///// <summary>
        ///// Retorna los pagos de horas exta que se encuentran realizados dentro del período especificado
        ///// </summary>
        ///// <param name="usuario"></param>
        ///// <param name="fechaInicio"></param>
        ///// <param name="fechaFin"></param>
        ///// <param name="formasPago"></param>
        ///// <returns></returns>
        //List<Extension> ObtenerHorasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);
        ///// <summary>
        ///// Retorna los pagos de personas exta que se encuentran realizados dentro del período especificado
        ///// </summary>
        ///// <param name="usuario"></param>
        ///// <param name="fechaInicio"></param>
        ///// <param name="fechaFin"></param>
        ///// <param name="formasPago"></param>
        ///// <returns></returns>
        //List<PersonaExtra> ObtenerPersonasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Permite obtener los pagos de las comandas en un período determinado. Si se especifican
        /// tipos de pago solamente se incluirán los pagos de esos tipos
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="tiposPagos"></param>
        /// <returns></returns>
        Dictionary<TiposPago, decimal> ObtenerPagosComandaPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos);
        /// <summary>
        /// Retorna los pagos de las rentas que se encuentran dentro del período especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="formasPago"></param>
        /// <returns></returns>
        List<PagoRenta> ObtenerPagosRentasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);
        /// <summary>
        /// Permite obtener los pagos por tarjeta de las habitaciones en un período determinado.
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="tiposPagos"></param>
        /// <returns></returns>
        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosHabitacionTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Permite obtener los pagos de las habitaciones en un período determinado. Si se especifican
        /// tipos de pago solamente se incluirán los pagos de esos tipos
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="tiposPagos"></param>
        /// <returns></returns>
        Dictionary<TiposPago, decimal> ObtenerDiccionarioPagosHabitacionPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos);

        Dictionary<string, List<DetallePago>> ObtenerDetallesPagosPorFechaYTipo(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Valida que no existan habitaciones por liberar o comandas no cobradas
        /// </summary>
        /// <param name="usuario"></param>
        void ValidarNoExistenPagosPendientes(int? idCorte);
        decimal ObtenerMontoNoReembolsablePorFecha(DateTime? fechaInicio, DateTime? fechaFin);
    }
}
