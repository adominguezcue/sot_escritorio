﻿using Modelo;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Dominio.Nucleo.Entidades;

namespace Negocio.Rentas
{
    public interface IServicioRentas
    {
        ///// <summary>
        ///// Permite registrar y pagar una nueva renta en el sistema
        ///// </summary>
        ///// <param name="rentaNueva"></param>
        ///// <param name="usuario"></param>
        //void CrearYPagarRenta(Renta rentaNueva, DtoUsuario usuario);
        ///// <summary>
        ///// Permite registrar y pagar una nueva renta en el sistema, es usado cuando se requieren permisos
        ///// especiales, como el permiso de agregar personas extra superiores al máximo permitido
        ///// </summary>
        ///// <param name="rentaNueva"></param>
        ///// <param name="usuario"></param>
        //void CrearYPagarRenta(Renta rentaNueva, DtoCredencial credencial);
        /// <summary>
        /// Permite registrar una nueva renta en el sistema
        /// </summary>
        /// <param name="rentaNueva"></param>
        /// <param name="usuario"></param>
        void CrearRenta(Renta rentaNueva, bool cobrar, Propina propina, DtoUsuario usuario, ConsumoInternoHabitacion consumo);
        /// <summary>
        /// Permite registrar una nueva renta en el sistema, es usado cuando se requieren permisos
        /// especiales, como el permiso de agregar personas extra superiores al máximo permitido
        /// </summary>
        /// <param name="rentaNueva"></param>
        /// <param name="usuario"></param>
        void CrearRenta(Renta rentaNueva, bool cobrar, Propina propina, DtoCredencial credencial, ConsumoInternoHabitacion consumo);
        ///// <summary>
        ///// Permite pagar una renta pendiente de cobro
        ///// </summary>
        ///// <param name="idRenta"></param>
        ///// <param name="informacionPagos"></param>
        ///// <param name="usuario"></param>
        //void PagarRenta(int idRenta, List<DtoInformacionPago> informacionPagos, Propina propina, DtoUsuario usuario, ConsumoInternoHabitacion consumo);
        ///// <summary>
        ///// Retorna las comandas con artículos en preparación que serán procesadas en el area especificada
        ///// </summary>
        ///// <returns></returns>
        //List<Comanda> ObtenerDetallesComandasArticulosEnPreparacionDepartamento(string departamento);
        ///// <summary>
        ///// Retorna las comandas con artículos preparados que serán procesadas en el área especificada
        ///// </summary>
        ///// <returns></returns>
        //List<Comanda> ObtenerDetallesComandasArticulosPreparadosDepartamento(string departamento);
        
        
        
        ///// <summary>
        ///// Permite agregar horas extra, personas extra y actualizar los datos de los automóviles de
        ///// una renta, siempre y cuando la huella digital corresponda a un usuario con los permisos necesarios
        ///// </summary>
        ///// <param name="idRenta"></param>
        ///// <param name="cantidadExtensiones"></param>
        ///// <param name="cantidadPersonasExtra"></param>
        ///// <param name="automoviles"></param>
        ///// <param name="informacionPagos"></param>
        ///// <param name="huellaDigital"></param>
        //void ActualizarDatosHabitacion(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
        //                                      List<DtoInformacionPago> informacionPagos, DtoCredencial credencial, ConsumoInternoHabitacion consumo);

        ///// <summary>
        ///// Permite agregar horas extra, personas extra y actualizar los datos de los automóviles de
        ///// una renta, siempre y cuando la huella digital corresponda a un usuario con los permisos necesarios
        ///// </summary>
        ///// <param name="idRenta"></param>
        ///// <param name="cantidadExtensiones"></param>
        ///// <param name="cantidadPersonasExtra"></param>
        ///// <param name="automoviles"></param>
        ///// <param name="informacionPagos"></param>
        ///// <param name="usuario"></param>
        //void ActualizarDatosHabitacion(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
        //                                      List<DtoInformacionPago> informacionPagos, DtoUsuario usuario, ConsumoInternoHabitacion consumo);
        
        /// <summary>
        /// Retorna la renta activa que involucre a la habitación con el id proporcionado o null en
        /// caso de no existir rentas que la usen
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        Renta ObtenerRentaActualPorHabitacion(int idHabitacion);
        /// <summary>
        /// Retorna una lista de resúmenes de cada una de las ventas realizadas durante la estancia actual
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        DtoDetallesRenta ObtenerResumenVentasUltimaRenta(int idHabitacion);
        /// <summary>
        /// Retorna la última renta que involucre a la habitación con el id proporcionado o null en
        /// caso de no existir rentas que la usen
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        Renta ObtenerUltimaRentaPorHabitacionConDatosFiscales(int idHabitacion);
        
        /// <summary>
        /// Permite obtener los autómóviles relacionados con la última renta
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        List<AutomovilRenta> ObtenerAutomovilesUltimaRenta(int idHabitacion);
        ///// <summary>
        ///// Determina si existe una renta activa para la habitación con el id proporcionado y que
        ///// tenga vinculada una tarjeta V
        ///// </summary>
        ///// <param name="idHabitacion"></param>
        ///// <param name="UsuarioActual"></param>
        ///// <returns></returns>
        //bool VerificarTieneTarjetaVPointsVinculada(int idHabitacion, DtoUsuario UsuarioActual);
        /// <summary>
        /// Retorna los detalles de pago que pertenecen a la transación proporcionada
        /// </summary>
        /// <param name="transaccion"></param>
        /// <returns></returns>
        List<DetallePago> ObtenerDetallesPorTransaccion(string transaccion, DateTime? fechaFin);
        /// <summary>
        /// Permite actualizar la información del pago
        /// </summary>
        /// <param name="idPago"></param>
        /// <param name="formaPago"></param>
        /// <param name="valorPropina"></param>
        /// <param name="referencia"></param>
        /// <param name="usuario"></param>
        /// <param name="numeroTarjeta"></param>
        /// <param name="tipoTarjeta"></param>
        /// <param name="idEmpleado"></param>
        void ActualizarPagoHabitacion(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, DtoUsuario usuario, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null, int? idEmpleado = null);
        
        /// <summary>
        /// Retorna información de los valets relacionados con las rentas con los ids proporcionados
        /// </summary>
        /// <param name="idsRentas"></param>
        /// <returns></returns>
        List<DtoEnvoltorioEmpleado> ObtenerInformacionValetsPorIdVentaRenta(List<int> idsRentas);
        /// <summary>
        /// Si el id de datos fiscales es diferente de null, la renta con el id proporcionado quedara
        /// vinculada a esa información, en caso contrario perderá cualquier vinculación con datos fiscales
        /// </summary>
        /// <param name="idRenta"></param>
        /// <param name="idDatosFiscales"></param>
        /// <param name="usuario"></param>
        void VincularDatosFiscales(int idRenta, int? idDatosFiscales, DtoUsuario usuario);
        /// <summary>
        /// Retorna los datos fiscales vinculados a la renta a la que pertenece la operación con la transacción proporcionada
        /// </summary>
        /// <param name="transaccion"></param>
        /// <returns></returns>
        DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion);
        
        /// <summary>
        /// Retorna la cantidad de personas extra pertenecientes a la última renovación, en caso de
        /// que no existan renovaciones se retornará la cantidad de personas extra de la renta actual
        /// </summary>
        /// <param name="idRenta"></param>
        /// <returns></returns>
        int ObtenerCantidadPersonasExtraUltimaRenovacion(int idRenta);
        void PagarRentaV2(int idRenta, List<DtoInformacionPago> informacionPagos, ConsumoInternoHabitacion consumo, Propina propina, int? idEmpleadoPropina, DtoUsuario usuario);
        void ActualizarDatosHabitacionV2(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
                                              DtoUsuario usuario);
        void ActualizarDatosHabitacionV2(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
                                              DtoCredencial credencial);

        void CancelarVentaPendiente(int idVentaRenta, DtoCredencial credencial);

        DtoAutomovil ObtenerDatosAutomovil(string matricula);

        List<DtoDatosPersonaExtrasRepCorteTurno> ObtienePersonasExtrasPorCorte(int folioCorte);
    }
}
