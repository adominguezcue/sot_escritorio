﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesGlobales;
using Negocio.ConfiguracionesImpresoras;
using Negocio.ConfiguracionesTipo;
using Negocio.ConsumosInternos;
using Negocio.Empleados;
using Negocio.Habitaciones;
using Negocio.Pagos;
using Negocio.Preventas;
using Negocio.Propinas;
using Negocio.ReportesMatriculas;
using Negocio.Reservaciones;
using Negocio.Seguridad.Permisos;
using Negocio.Tickets;
using Negocio.Ventas;
using Negocio.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Excepciones.Seguridad;

namespace Negocio.Rentas
{
    public partial class ServicioRentas : IServicioRentasInterno
    {
        #region dependencias

        private IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        private Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });

        private IServicioReservacionesInterno ServicioReservaciones
        {
            get { return _servicioReservaciones.Value; }
        }

        private Lazy<IServicioReservacionesInterno> _servicioReservaciones = new Lazy<IServicioReservacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioReservacionesInterno>(); });

        private IServicioVentasInterno ServicioVentas
        {
            get { return _servicioVentas.Value; }
        }

        private Lazy<IServicioVentasInterno> _servicioVentas = new Lazy<IServicioVentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVentasInterno>(); });

        private IServicioVPointsInterno ServicioVPoints
        {
            get { return _servicioVPoints.Value; }
        }

        private Lazy<IServicioVPointsInterno> _servicioVPoints = new Lazy<IServicioVPointsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVPointsInterno>(); });

        private IServicioTicketsInterno ServicioTickets
        {
            get { return _servicioTickets.Value; }
        }

        private Lazy<IServicioTicketsInterno> _servicioTickets = new Lazy<IServicioTicketsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTicketsInterno>(); });

        private IServicioConsumosInternosHabitacionInterno ServicioConsumosInternosHabitacion
        {
            get { return _servicioConsumosInternosHabitacion.Value; }
        }

        private Lazy<IServicioConsumosInternosHabitacionInterno> _servicioConsumosInternosHabitacion = new Lazy<IServicioConsumosInternosHabitacionInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternosHabitacionInterno>(); });

        private IServicioConfiguracionesTipo ServicioConfiguracionesTipo
        {
            get { return _servicioConfiguracionesTipo.Value; }
        }

        private Lazy<IServicioConfiguracionesTipo> _servicioConfiguracionesTipo = new Lazy<IServicioConfiguracionesTipo>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesTipo>(); });

        private IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        private Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        private IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        private Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        private IServicioConfiguracionesImpresorasInterno ServicioConfiguracionesImpresoras
        {
            get { return _servicioConfiguracionesImpresoras.Value; }
        }

        private Lazy<IServicioConfiguracionesImpresorasInterno> _servicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresorasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresorasInterno>(); });

        private IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        private Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });

        private IServicioPropinasInterno ServicioPropinas
        {
            get { return _servicioPropinas.Value; }
        }

        private Lazy<IServicioPropinasInterno> _servicioPropinas = new Lazy<IServicioPropinasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPropinasInterno>(); });

        private IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        private Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        private IServicioPagosInterno ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        private Lazy<IServicioPagosInterno> _servicioPagos = new Lazy<IServicioPagosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagosInterno>(); });

        private IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        private Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        private IRepositorioRentas RepositorioRentas
        {
            get { return _repositorioRentas.Value; }
        }

        private Lazy<IRepositorioRentas> _repositorioRentas = new Lazy<IRepositorioRentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioRentas>(); });

        private IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        private Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        private IServicioReportesMatriculas ServicioReportesMatriculas
        {
            get { return _servicioReportesMatriculas.Value; }
        }

        private Lazy<IServicioReportesMatriculas> _servicioReportesMatriculas = new Lazy<IServicioReportesMatriculas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioReportesMatriculas>(); });

        private IRepositorioComandas RepositorioComandas
        {
            get { return _repositorioComandas.Value; }
        }

        private Lazy<IRepositorioComandas> _repositorioComandas = new Lazy<IRepositorioComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioComandas>(); });

        private IRepositorioPagosRenta RepositorioPagosRenta
        {
            get { return _repositorioPagosRenta.Value; }
        }

        private Lazy<IRepositorioPagosRenta> _repositorioPagosRenta = new Lazy<IRepositorioPagosRenta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosRenta>(); });

        private IRepositorioVentasRentas RepositorioVentasRentas
        {
            get { return _repostorioVentasRentas.Value; }
        }

        private Lazy<IRepositorioVentasRentas> _repostorioVentasRentas = new Lazy<IRepositorioVentasRentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentasRentas>(); });

        #endregion dependencias

        //public void PagarRenta(int idRenta, List<DtoInformacionPago> informacionPagos, Propina propina, DtoUsuario usuario, ConsumoInternoHabitacion consumo)
        //{
        //    ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CobrarHabitaciones = true });

        //    var renta = RepositorioRentas.ObtenerCargada(idRenta);

        //    if (renta.Estado != Renta.Estados.PendienteCobro)
        //        throw new SOTException(Recursos.Rentas.renta_no_pendiente_cobro_excepcion);

        //    var configuracion = this.ServicioConfiguracionesTipo.ObtenerConfiguracionTipoNoNull(renta.IdConfiguracionTipo);
        //    var configGlobal = ServicioParametros.ObtenerParametros();

        //    if (renta == null)
        //        throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

        //    var fechaActual = DateTime.Now;
        //    var numeroTransaccion = Guid.NewGuid().ToString();

        //    int cantidadVentas = renta.VentasRenta.Count(m => m.Activo);

        //    if (cantidadVentas > 1)
        //        throw new SOTException(Recursos.Rentas.multiple_informacion_venta_excepcion);
        //    if (cantidadVentas == 0)
        //        throw new SOTException(Recursos.Rentas.informacion_venta_nula_excepcion);

        //    if (renta.NumeroTarjeta != null)
        //        renta.NumeroTarjeta = renta.NumeroTarjeta.Trim();

        //    var ventaRenta = renta.VentasRenta.First(m => m.Activo);

        //    if (informacionPagos.Any(m => m.TipoPago == TiposPago.Consumo) && consumo == null)
        //        throw new SOTException(Recursos.Rentas.consumo_interno_nulo_excepcion);
        //    if (!informacionPagos.Any(m => m.TipoPago == TiposPago.Consumo) && consumo != null)
        //        throw new SOTException(Recursos.Rentas.consumo_interno_no_requerido_excepcion);

        //    #region validacion IVA

        //    var vIVA = ventaRenta.ValorSinIVA * configGlobal.Iva_CatPar;
        //    var vConIVA = ventaRenta.ValorSinIVA + vIVA;

        //    if (ventaRenta.ValorConIVA.ToString("C") != vConIVA.ToString("C") || ventaRenta.ValorIVA.ToString("C") != vIVA.ToString("C"))
        //        throw new SOTException(Recursos.Rentas.valores_impuestos_renta_invalidos_excepcion);

        //    #endregion
        //    #region validación de habitaciones

        //    ServicioHabitaciones.ValidarEstaPendienteCobro(renta.IdHabitacion);

        //    #endregion

        //    //var estadosValidos = new List<int>(){(int)Renta.Estados.Finalizada, (int)Renta.Estados.Cancelada};
        //    int estadoEnCurso = (int)Renta.Estados.EnCurso;
        //    int estadoPendiente = (int)Renta.Estados.PendienteCobro;
        //    int estadoFinalizado = (int)Renta.Estados.Finalizada;

        //    if (!string.IsNullOrEmpty(renta.NumeroTarjeta))
        //    {
        //        if (RepositorioRentas.Alguno(m => m.NumeroTarjeta != null && m.NumeroTarjeta == renta.NumeroTarjeta && (m.IdEstado == estadoEnCurso || m.IdEstado == estadoPendiente)/*!estadosValidos.Contains(m.IdEstado)*/))
        //            throw new SOTException(Recursos.Rentas.tarjetaV_en_uso_exception);

        //        if (RepositorioRentas.Alguno(m => m.NumeroTarjeta != null && m.NumeroTarjeta == renta.NumeroTarjeta && m.IdEstado == estadoFinalizado
        //                                          && m.FechaSalida.Value.Year == fechaActual.Year && m.FechaSalida.Value.Month == fechaActual.Month && m.FechaSalida.Value.Day == fechaActual.Day))
        //            throw new SOTException(Recursos.Rentas.tarjetaV_usada_mismo_dia_exception);
        //    }

        //    if (!ventaRenta.IdValet.HasValue && propina != null)
        //        throw new SOTException(Recursos.Rentas.propina_no_necesaria_excepcion);

        //    foreach (var pago in informacionPagos)
        //    {
        //        ventaRenta.Pagos.Add(new PagoRenta
        //        {
        //            Activo = true,
        //            TipoPago = pago.TipoPago,
        //            TipoTarjeta = pago.TipoTarjeta,
        //            NumeroTarjeta = pago.NumeroTarjeta ?? "",
        //            Referencia = pago.Referencia,
        //            Transaccion = numeroTransaccion,
        //            Valor = pago.Valor,
        //            FechaCreacion = fechaActual,
        //            FechaModificacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            IdUsuarioModifico = usuario.Id,
        //        });
        //    }

        //    ProcesarDetallesYPagos(configuracion.Precio, ventaRenta, numeroTransaccion, fechaActual, usuario.Id);

        //    renta.IdUsuarioModifico = usuario.Id;
        //    renta.FechaModificacion = fechaActual;
        //    renta.NumeroServicio = Guid.NewGuid().ToString();
        //    renta.Estado = Renta.Estados.EnCurso;

        //    ventaRenta.Transaccion = numeroTransaccion;
        //    ventaRenta.FechaModificacion = fechaActual;
        //    ventaRenta.IdUsuarioModifico = usuario.Id;

        //    if (propina != null)
        //    {
        //        propina.Activo = true;
        //        propina.TipoPropina = Propina.TiposPropina.Valet;
        //        propina.IdEmpleado = ventaRenta.IdValet.Value;
        //        propina.Transaccion = ventaRenta.Transaccion;
        //        propina.FechaCreacion = fechaActual;
        //        propina.FechaModificacion = fechaActual;
        //        propina.IdUsuarioCreo = usuario.Id;
        //        propina.IdUsuarioModifico = usuario.Id;
        //    }

        //    var pagoReservacion = ventaRenta.Pagos.FirstOrDefault(m => m.Activo && m.TipoPago == TiposPago.Reservacion);

        //    if (consumo != null)
        //        ventaRenta.Pagos.First(m => m.TipoPago == TiposPago.Consumo).Referencia = consumo.Transaccion = Guid.NewGuid().ToString();

        //    var venta = ServicioPagos.GenerarVentaNoCompletada(ventaRenta.Pagos.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.Habitacion, ventaRenta.IdPreventa, true);

        //    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
        //    {
        //        if (consumo != null)
        //            ServicioConsumosInternosHabitacion.Crear(consumo, usuario.Id);

        //        ServicioPagos.Pagar(venta.Id, ventaRenta.Pagos.Where(m => m.Activo), usuario);

        //        RepositorioRentas.Modificar(renta);
        //        RepositorioRentas.GuardarCambios();

        //        ServicioHabitaciones.MarcarComoOcupada(renta.IdHabitacion, pagoReservacion == null ?
        //            null : pagoReservacion.Referencia, usuario.Id);

        //        if (!string.IsNullOrEmpty(renta.NumeroTarjeta))
        //        {
        //            decimal porcentaje;

        //            if (ventaRenta.ValorConIVA == ventaRenta.Pagos.Where(m => m.Activo && (m.TipoPago == TiposPago.Efectivo || m.TipoPago == TiposPago.TarjetaCredito)).Sum(m => m.Valor))
        //                porcentaje = 1;
        //            else
        //                porcentaje = ventaRenta.Pagos.Where(m => m.Activo && (m.TipoPago == TiposPago.Efectivo || m.TipoPago == TiposPago.TarjetaCredito)).Sum(m => m.Valor) / ventaRenta.ValorConIVA;

        //            ServicioVPoints.AbrirAbonoPuntos(renta.Id, renta.NumeroTarjeta, venta.Ticket.ToString(),
        //                                                 (ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Habitacion).Sum(m => m.ValorConIVA) +
        //                                                 ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA) +
        //                                                 ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA)) * porcentaje,
        //                                                 ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA) * porcentaje,
        //                                                 0, 0, 0, usuario);
        //        }

        //        if (propina != null)
        //            ServicioPropinas.RegistrarPropina(propina, usuario.Id);

        //        scope.Complete();
        //    }

        //    var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
        //    ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 1);
        //}

        //public void ActualizarDatosHabitacion(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
        //                                      List<DtoInformacionPago> informacionPagos, DtoCredencial credencial, ConsumoInternoHabitacion consumo)
        //{
        //    var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

        //    ActualizarDatosHabitacion(idRenta, cantidadExtensiones, cantidadRenovaciones, cantidadPersonasExtra, automoviles, informacionPagos, usuario, consumo);
        //}

        //public void ActualizarDatosHabitacion(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
        //                                      List<DtoInformacionPago> informacionPagos, DtoUsuario usuario, ConsumoInternoHabitacion consumo)
        //{
        //    var configuracionGlogal = ServicioParametros.ObtenerParametros();
        //    var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

        //    ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarRenta = true, AsignarPersonaExtraPosterior = cantidadPersonasExtra > 0 });

        //    var renta = RepositorioRentas.ObtenerCargada(idRenta);

        //    if (renta == null)
        //        throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

        //    var fechaActual = DateTime.Now;

        //    var ultimaExtension = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa /*&& m.FechaInicio <= fechaActual*/ && m.FechaFin >= fechaActual).OrderBy(m => m.FechaInicio).FirstOrDefault();

        //    if (renta.FechaFin < fechaActual && ultimaExtension == null && cantidadPersonasExtra > 0)
        //        throw new SOTException(Recursos.Rentas.renta_vencida_excepcion + ", no se pueden agregar personas extra.");

        //    if (automoviles.Count != automoviles.GroupBy(m => m.Matricula).Count())
        //        throw new SOTException(Recursos.Rentas.matriculas_duplicadas_excepcion);

        //    var configuracion = this.ServicioConfiguracionesTipo.ObtenerConfiguracionTipoNoNull(renta.IdConfiguracionTipo);

        //    if (automoviles.Count > configuracion.ConfiguracionTarifa.CantidadAutosMaxima)
        //        throw new SOTException(Recursos.Rentas.automoviles_maximos_excedidos_excepcion, configuracion.ConfiguracionTarifa.CantidadAutosMaxima.ToString());

        //    if (informacionPagos.Any(m => m.TipoPago == TiposPago.Consumo) && consumo == null)
        //        throw new SOTException(Recursos.Rentas.consumo_interno_nulo_excepcion);
        //    if (!informacionPagos.Any(m => m.TipoPago == TiposPago.Consumo) && consumo != null)
        //        throw new SOTException(Recursos.Rentas.consumo_interno_no_requerido_excepcion);

        //    #region validacion horas extra

        //    if (configuracion.ConfiguracionTarifa.RangoFijo && cantidadExtensiones > 0)
        //        throw new SOTException(Recursos.ConfiguracionesNegocio.horas_extra_no_soportadas_exception);

        //    var maximoExtensiones = configuracion.TiemposHospedaje.Count(m => m.Activo);

        //    if (maximoExtensiones == 0 && cantidadExtensiones > 0)
        //        throw new SOTException(Recursos.Rentas.horas_extras_no_soportadas_exception);

        //    int extensionesSobrantes = 0;

        //    if (maximoExtensiones > 0)
        //    {
        //        if (cantidadRenovaciones == 0)
        //        {
        //            int maximoOrdenRenovacion = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa && m.EsRenovacion).OrderByDescending(m => m.Orden).Select(m => m.Orden).LastOrDefault();
        //            extensionesSobrantes = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Count(m => m.Activa && !m.EsRenovacion && m.Orden > maximoOrdenRenovacion);

        //            if (extensionesSobrantes > maximoExtensiones)
        //                extensionesSobrantes = maximoExtensiones;
        //        }

        //        var extensionesSobranteTotales = extensionesSobrantes + cantidadExtensiones;

        //        if (maximoExtensiones < extensionesSobranteTotales)
        //            throw new SOTException(Recursos.Rentas.maximo_extensiones_superado_excepcion, maximoExtensiones.ToString());
        //    }
        //    #endregion
        //    #region validación de personas extra

        //    var ultimaRenovacion = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa && m.EsRenovacion).OrderByDescending(m => m.Orden).FirstOrDefault();

        //    int cantidadPE = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PersonasExtra).Where(m=> m.Activa && (ultimaRenovacion != null ? m.FolioRenovacion == ultimaRenovacion.Folio : string.IsNullOrWhiteSpace(m.FolioRenovacion))).Count(m => m.Activa);

        //    if (configuracion.TipoHabitacion.MaximoPersonasExtra < cantidadPE + cantidadPersonasExtra ||
        //        configuracion.TipoHabitacion.MaximoPersonasExtra < cantidadPersonasExtra)
        //        ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarPersonasExtraSobreMax = true });

        //    #endregion

        //    var numeroTransaccion = Guid.NewGuid().ToString();

        //    List<DetallePago> detalles = new List<DetallePago>();

        //    decimal valorDetalleConIVA, valorDetalleSinIVA, valorDetalleIVA;
        //    decimal precioHospedajeConIVA, precioHospedajeExtraConIVA, precioPersonasExtraConIVA;
        //    precioHospedajeConIVA = precioHospedajeExtraConIVA = precioPersonasExtraConIVA = 0;

        //    if (cantidadExtensiones > 0)
        //    {
        //        precioHospedajeExtraConIVA = Math.Round(configuracion.PrecioHospedajeExtra * (1 + configuracionGlogal.Iva_CatPar), 2);
        //        valorDetalleConIVA = precioHospedajeExtraConIVA * cantidadExtensiones;
        //        valorDetalleSinIVA = valorDetalleConIVA / (1 + configuracionGlogal.Iva_CatPar);
        //        valorDetalleIVA = valorDetalleConIVA - valorDetalleSinIVA;

        //        detalles.Add(new DetallePago
        //        {
        //            Activo = true,
        //            ValorConIVA = valorDetalleConIVA,
        //            ValorSinIVA = valorDetalleSinIVA,
        //            ValorIVA = valorDetalleIVA,
        //            ConceptoPago = DetallePago.ConceptosPago.HorasExtra,
        //            Transaccion = numeroTransaccion,
        //            FechaCreacion = fechaActual,
        //            FechaModificacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            IdUsuarioModifico = usuario.Id,
        //            Cantidad = cantidadExtensiones
        //        });
        //    }

        //    precioHospedajeConIVA = Math.Round(configuracion.Precio * (1 + configuracionGlogal.Iva_CatPar), 2);

        //    if (cantidadRenovaciones > 0)
        //    {
        //        valorDetalleConIVA = precioHospedajeConIVA * cantidadRenovaciones;
        //        valorDetalleSinIVA = valorDetalleConIVA / (1 + configuracionGlogal.Iva_CatPar);
        //        valorDetalleIVA = valorDetalleConIVA - valorDetalleSinIVA;

        //        detalles.Add(new DetallePago
        //        {
        //            Activo = true,
        //            ValorConIVA = valorDetalleConIVA,
        //            ValorSinIVA = valorDetalleSinIVA,
        //            ValorIVA = valorDetalleIVA,
        //            ConceptoPago = DetallePago.ConceptosPago.Renovacion,
        //            Transaccion = numeroTransaccion,
        //            FechaCreacion = fechaActual,
        //            FechaModificacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            IdUsuarioModifico = usuario.Id,
        //            Cantidad = cantidadRenovaciones
        //        });
        //    }

        //    if (cantidadPersonasExtra > 0)
        //    {
        //        precioPersonasExtraConIVA = Math.Round(configuracion.PrecioPersonaExtra * (1 + configuracionGlogal.Iva_CatPar), 2);
        //        valorDetalleConIVA = precioPersonasExtraConIVA * cantidadPersonasExtra * (1 + cantidadRenovaciones);
        //        valorDetalleSinIVA = valorDetalleConIVA / (1 + configuracionGlogal.Iva_CatPar);
        //        valorDetalleIVA = valorDetalleConIVA - valorDetalleSinIVA;

        //        detalles.Add(new DetallePago
        //        {
        //            Activo = true,
        //            ValorConIVA = valorDetalleConIVA,
        //            ValorSinIVA = valorDetalleSinIVA,
        //            ValorIVA = valorDetalleIVA,
        //            ConceptoPago = DetallePago.ConceptosPago.PersonasExtra,
        //            Transaccion = numeroTransaccion,
        //            FechaCreacion = fechaActual,
        //            FechaModificacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            IdUsuarioModifico = usuario.Id,
        //            Cantidad = cantidadPersonasExtra
        //        });
        //    }

        //    var precioTotalConIVA = detalles.Sum(m => m.ValorConIVA);
        //    var precioTotalSinIVA = detalles.Sum(m => m.ValorSinIVA);
        //    var totalIVA = detalles.Sum(m => m.ValorIVA);

        //    var valorPagos = informacionPagos.Any() ? informacionPagos.Sum(m => m.Valor) : 0;

        //    if (precioTotalConIVA.ToString("C") != valorPagos.ToString("C"))
        //        throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, precioTotalConIVA.ToString("C"), valorPagos.ToString("C"));

        //    var ventaRenta = new VentaRenta
        //    {
        //        Transaccion = numeroTransaccion,
        //        FechaCreacion = fechaActual,
        //        FechaModificacion = fechaActual,
        //        IdUsuarioCreo = usuario.Id,
        //        IdUsuarioModifico = usuario.Id,
        //        Activo = true,
        //        ValorConIVA = precioTotalConIVA,
        //        ValorSinIVA = precioTotalSinIVA,
        //        ValorIVA = totalIVA
        //    };

        //    foreach (var detalle in detalles)
        //        ventaRenta.DetallesPago.Add(detalle);

        //    renta.IdUsuarioModifico = usuario.Id;
        //    renta.FechaModificacion = fechaActual;

        //    var extensionesConfiguracion = configuracion.TiemposHospedaje.Where(m => m.Activo).OrderBy(m => m.Orden).ToList();

        //    decimal precioHospedajeExtraSinIVA = precioHospedajeExtraConIVA / (1 + configuracionGlogal.Iva_CatPar);
        //    decimal precioPersonasExtraSinIVA = precioPersonasExtraConIVA / (1 + configuracionGlogal.Iva_CatPar);
        //    decimal precioHospedajeSinIVA = precioHospedajeConIVA / (1 + configuracionGlogal.Iva_CatPar);

        //    for (int i = 0, cantidadExistente = cantidadPE; i < cantidadPersonasExtra; i++, cantidadExistente++)
        //    {
        //        ventaRenta.PersonasExtra.Add(new PersonaExtra
        //        {
        //            Activa = true,
        //            Precio = precioPersonasExtraSinIVA,
        //            FolioRenovacion = ultimaRenovacion != null ? ultimaRenovacion.Folio : "",
        //            CobroPorSeparado = true,
        //            FechaCreacion = fechaActual,
        //            FechaModificacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            IdUsuarioModifico = usuario.Id,
        //            EsFueraDeRango = cantidadExistente >= configuracion.TipoHabitacion.MaximoPersonasExtra
        //        });
        //    }

        //    ultimaExtension = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa && !m.EsRenovacion).OrderByDescending(m => m.Orden).FirstOrDefault();

        //    var maximoOrden = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa).OrderByDescending(m => m.Orden).Select(m => m.Orden).FirstOrDefault();

        //    for (int i = 0; i < cantidadRenovaciones; i++)
        //    {
        //        ventaRenta.Extensiones.Add(new Extension
        //        {
        //            Activa = true,
        //            EsRenovacion = true,
        //            Precio = precioHospedajeSinIVA,
        //            CobroPorSeparado = true,
        //            FechaCreacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            Folio = Guid.NewGuid().ToString(),
        //            Orden = ++maximoOrden
        //        });

        //        for (int j = 0; j < cantidadPersonasExtra; j++)
        //        {
        //            ventaRenta.PersonasExtra.Add(new Modelo.Entidades.PersonaExtra
        //            {
        //                Activa = true,
        //                Precio = precioPersonasExtraSinIVA,
        //                FolioRenovacion = ventaRenta.Extensiones.Last().Folio,
        //                CobroPorSeparado = true,
        //                FechaCreacion = fechaActual,
        //                FechaModificacion = fechaActual,
        //                IdUsuarioCreo = usuario.Id,
        //                IdUsuarioModifico = usuario.Id,
        //                EsFueraDeRango = j >= configuracion.TipoHabitacion.MaximoPersonasExtra
        //            });
        //        }
        //    }

        //    for (int i = 0; i < cantidadExtensiones; i++)
        //    {
        //        ventaRenta.Extensiones.Add(new Extension
        //        {
        //            Activa = true,
        //            Precio = precioHospedajeExtraSinIVA,
        //            CobroPorSeparado = true,
        //            FechaCreacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            Folio = "",
        //            Orden = ++maximoOrden
        //        });
        //    }

        //    int contador = extensionesSobrantes >= maximoExtensiones ? extensionesSobrantes : 0;

        //    if (configuracion.ConfiguracionTarifa.RangoFijo)
        //    {
        //        var fechaInicio = ultimaRenovacion != null ? ultimaRenovacion.FechaInicio : renta.FechaInicio;
        //        var fechaFin = ultimaRenovacion != null ? ultimaRenovacion.FechaFin : renta.FechaFin;

        //        var fechaInicioE = ultimaExtension != null ? ultimaExtension.FechaInicio : fechaInicio;
        //        var fechaFinE = ultimaExtension != null ? ultimaExtension.FechaFin : fechaFin;

        //        var tiemposConfig = configuracion.TiemposHospedaje.OrderBy(m => m.Orden).ToList();

        //        foreach (var extension in ventaRenta.Extensiones.Where(m => m.Activa).OrderBy(m => m.Orden))
        //        {
        //            if (extension.EsRenovacion)
        //            {
        //                fechaInicio = fechaInicio.AddDays(1);
        //                fechaFin = fechaFin.AddDays(1);

        //                fechaInicioE = fechaInicio;
        //                fechaFinE = fechaFin;

        //                extension.FechaInicio = fechaInicio;
        //                extension.FechaFin = fechaFin;
        //            }
        //            else
        //            {
        //                fechaInicioE = fechaFinE.AddSeconds(1);
        //                fechaFinE = fechaInicioE.AddMinutes(tiemposConfig[contador++].Minutos);

        //                extension.FechaInicio = fechaInicioE;
        //                extension.FechaFin = fechaFinE;
        //            }

        //            extension.CobroPorSeparado = true;
        //            extension.FechaCreacion = fechaActual;
        //            extension.IdUsuarioCreo = usuario.Id;

        //            if (contador >= maximoExtensiones)
        //                contador = 0;
        //        }
        //    }
        //    else
        //    {
        //        ultimaExtension = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa).OrderByDescending(m => m.Orden).FirstOrDefault();

        //        var tiemposConfig = configuracion.TiemposHospedaje.OrderBy(m => m.Orden).ToList();

        //        var fechaInicio = ultimaExtension != null ? ultimaExtension.FechaInicio : renta.FechaInicio;
        //        var fechaFin = ultimaExtension != null ? ultimaExtension.FechaFin : renta.FechaFin;

        //        foreach (var extension in ventaRenta.Extensiones.Where(m => m.Activa).OrderBy(m => m.Orden))
        //        {
        //            fechaInicio = fechaFin.AddSeconds(1);

        //            if (extension.EsRenovacion)
        //                fechaFin = fechaInicio.AddHours(configuracion.DuracionOEntrada);
        //            else
        //                fechaFin = fechaInicio.AddMinutes(tiemposConfig[contador++].Minutos);

        //            extension.FechaInicio = fechaInicio;
        //            extension.FechaFin = fechaFin;
        //            extension.CobroPorSeparado = true;
        //            extension.FechaCreacion = fechaActual;
        //            extension.IdUsuarioCreo = usuario.Id;

        //            if (contador >= maximoExtensiones)
        //                contador = 0;
        //        }
        //    }

        //    var autosNuevos = automoviles.Where(an => !renta.RentaAutomoviles.Any(m => m.Activa && m.Matricula.Trim().ToUpper()
        //                                               .Equals(an.Matricula))).ToList();

        //    var autosModificados = automoviles.Except(autosNuevos).ToList();

        //    foreach (var autoModificado in autosModificados)
        //    {
        //        var autoRenta = renta.RentaAutomoviles.FirstOrDefault(m => m.Activa && m.Matricula.Trim().ToUpper()
        //                                               .Equals(autoModificado.Matricula));

        //        if (autoRenta != null &&
        //            (autoRenta.Marca != autoModificado.Marca ||
        //            autoRenta.Modelo != autoModificado.Modelo ||
        //            autoRenta.Color != autoModificado.Color))
        //        {
        //            autoRenta.Marca = autoModificado.Marca;
        //            autoRenta.Modelo = autoModificado.Modelo;
        //            autoRenta.Color = autoModificado.Color;

        //            autoRenta.FechaModificacion = fechaActual;
        //            autoRenta.IdUsuarioModifico = usuario.Id;
        //        }
        //    }

        //    foreach (var autoNuevo in autosNuevos)
        //    {
        //        renta.RentaAutomoviles.Add(new AutomovilRenta
        //        {
        //            Activa = true,
        //            Matricula = autoNuevo.Matricula,
        //            Marca = autoNuevo.Marca,
        //            Modelo = autoNuevo.Modelo,
        //            Color = autoNuevo.Color,
        //            FechaCreacion = fechaActual,
        //            FechaModificacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            IdUsuarioModifico = usuario.Id
        //        });
        //    }

        //    foreach (var automovil in renta.RentaAutomoviles)
        //    {
        //        if (automovil.Activa && !automoviles.Any(m => m.Matricula.Equals(automovil.Matricula.Trim().ToUpper())))
        //        {
        //            automovil.Activa = false;
        //            automovil.FechaEliminacion = fechaActual;
        //            automovil.IdUsuarioElimino = usuario.Id;
        //        }
        //    }

        //    foreach (var pago in informacionPagos)
        //    {
        //        ventaRenta.Pagos.Add(new PagoRenta
        //        {
        //            Activo = true,
        //            TipoPago = pago.TipoPago,
        //            TipoTarjeta = pago.TipoTarjeta,
        //            NumeroTarjeta = pago.NumeroTarjeta ?? "",
        //            Referencia = pago.Referencia,
        //            Transaccion = numeroTransaccion,
        //            Valor = pago.Valor,
        //            FechaCreacion = fechaActual,
        //            FechaModificacion = fechaActual,
        //            IdUsuarioCreo = usuario.Id,
        //            IdUsuarioModifico = usuario.Id,
        //        });
        //    }

        //    if (ventaRenta.ValorConIVA != 0)
        //        renta.VentasRenta.Add(ventaRenta);

        //    if (consumo != null)
        //        ventaRenta.Pagos.First(m => m.TipoPago == TiposPago.Consumo).Referencia = consumo.Transaccion = Guid.NewGuid().ToString();

        //    Venta venta = null;

        //    if (ventaRenta.Pagos.Count > 0)
        //        venta = ServicioPagos.GenerarVentaNoCompletada(ventaRenta.Pagos.Where(m => m.Activo && m.Transaccion == numeroTransaccion), usuario, Venta.ClasificacionesVenta.Habitacion, null, true);

        //    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
        //    {
        //        if (consumo != null)
        //            ServicioConsumosInternosHabitacion.Crear(consumo, usuario.Id);

        //        if (ventaRenta.Pagos.Count > 0)
        //            ServicioPagos.Pagar(venta.Id, ventaRenta.Pagos.Where(m => m.Activo && m.Transaccion == numeroTransaccion), usuario);

        //        RepositorioRentas.Modificar(renta);
        //        RepositorioRentas.GuardarCambios();

        //        if (!string.IsNullOrEmpty(renta.NumeroTarjeta) && ventaRenta.Pagos.Count > 0)
        //        {
        //            decimal porcentaje;

        //            if (ventaRenta.ValorConIVA == ventaRenta.Pagos.Where(m => m.Activo && (m.TipoPago == TiposPago.Efectivo || m.TipoPago == TiposPago.TarjetaCredito)).Sum(m => m.Valor))
        //                porcentaje = 1;
        //            else
        //                porcentaje = ventaRenta.Pagos.Where(m => m.Activo && (m.TipoPago == TiposPago.Efectivo || m.TipoPago == TiposPago.TarjetaCredito)).Sum(m => m.Valor) / ventaRenta.ValorConIVA;

        //            ServicioVPoints.RegistrarAbonosPuntos(renta.Id, renta.NumeroTarjeta, venta.Ticket.ToString(),
        //                                                 (ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Habitacion).Sum(m => m.ValorConIVA) +
        //                                                 ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA) +
        //                                                 ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA)) * porcentaje,
        //                                                 ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA) * porcentaje,
        //                                                 0, 0, 0, usuario);
        //        }

        //        scope.Complete();
        //    }

        //    if (venta != null)
        //        ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 2);
        //}

        #region métodos public

        public void PagarRentaV2(int idRenta, List<DtoInformacionPago> informacionPagos, ConsumoInternoHabitacion consumo, Propina propina, int? idEmpleadoPropina, DtoUsuario usuario)
        {
            usuario = ServicioPagos.ProcesarPermisosEspeciales(informacionPagos, usuario);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CobrarHabitaciones = true });

            var renta = RepositorioRentas.ObtenerCargada(idRenta);

            if (renta.Estado != Renta.Estados.PendienteCobro)
                throw new SOTException(Recursos.Rentas.renta_no_pendiente_cobro_excepcion);

            var configuracion = this.ServicioConfiguracionesTipo.ObtenerConfiguracionTipoNoNull(renta.IdConfiguracionTipo);
            var configGlobal = ServicioParametros.ObtenerParametros();

            if (renta == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            var fechaActual = DateTime.Now;
            var numeroTransaccion = Guid.NewGuid().ToString();

            int cantidadVentas = renta.VentasRenta.Count(m => m.Activo && !m.Cobrada);

            if (cantidadVentas > 1)
                throw new SOTException(Recursos.Rentas.multiple_informacion_venta_excepcion);
            if (cantidadVentas == 0)
                throw new SOTException(Recursos.Rentas.informacion_venta_nula_excepcion);

            var ventaRenta = renta.VentasRenta.First(m => m.Activo && !m.Cobrada);

            if (informacionPagos.Any(m => m.TipoPago == TiposPago.Consumo) && consumo == null)
                throw new SOTException(Recursos.Rentas.consumo_interno_nulo_excepcion);
            if (!informacionPagos.Any(m => m.TipoPago == TiposPago.Consumo) && consumo != null)
                throw new SOTException(Recursos.Rentas.consumo_interno_no_requerido_excepcion);

            #region validacion IVA

            var vIVA = ventaRenta.ValorSinIVA * configGlobal.Iva_CatPar;
            var vConIVA = ventaRenta.ValorSinIVA + vIVA;

            if (ventaRenta.ValorConIVA.ToString("C") != vConIVA.ToString("C") || ventaRenta.ValorIVA.ToString("C") != vIVA.ToString("C"))
                throw new SOTException(Recursos.Rentas.valores_impuestos_renta_invalidos_excepcion);

            #endregion validacion IVA

            #region validación de habitaciones

            ServicioHabitaciones.ValidarEstaPendienteCobro(renta.IdHabitacion);

            #endregion validación de habitaciones

            int estadoEnCurso = (int)Renta.Estados.EnCurso;
            int estadoPendiente = (int)Renta.Estados.PendienteCobro;
            int estadoFinalizado = (int)Renta.Estados.Finalizada;

            if (!string.IsNullOrEmpty(renta.NumeroTarjeta))
            {
                if (RepositorioRentas.Alguno(m => m.Id != renta.Id && m.NumeroTarjeta != null && m.NumeroTarjeta == renta.NumeroTarjeta && (m.IdEstado == estadoEnCurso || m.IdEstado == estadoPendiente)/*!estadosValidos.Contains(m.IdEstado)*/))
                    throw new SOTException(Recursos.Rentas.tarjetaV_en_uso_exception);

                if (RepositorioRentas.Alguno(m => m.NumeroTarjeta != null && m.NumeroTarjeta == renta.NumeroTarjeta && m.IdEstado == estadoFinalizado
                                                  && m.FechaSalida.Value.Year == fechaActual.Year && m.FechaSalida.Value.Month == fechaActual.Month && m.FechaSalida.Value.Day == fechaActual.Day))
                    throw new SOTException(Recursos.Rentas.tarjetaV_usada_mismo_dia_exception);
            }

            if (!ventaRenta.EsInicial)
                ventaRenta.IdValet = idEmpleadoPropina;

            if (!ventaRenta.IdValet.HasValue && propina != null)
                throw new SOTException(Recursos.Rentas.propina_no_necesaria_excepcion);

            foreach (var pago in informacionPagos)
            {
                ventaRenta.Pagos.Add(new PagoRenta
                {
                    Activo = true,
                    TipoPago = pago.TipoPago,
                    TipoTarjeta = pago.TipoTarjeta,
                    NumeroTarjeta = pago.NumeroTarjeta ?? "",
                    Referencia = pago.Referencia,
                    Transaccion = numeroTransaccion,
                    Valor = pago.Valor,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                });
            }

            var pagoReservacion = ventaRenta.Pagos.FirstOrDefault(m => m.Activo && m.TipoPago == TiposPago.Reservacion);

            if (!ventaRenta.EsInicial && pagoReservacion != null)
                throw new SOTException(Recursos.Rentas.pago_reservacion_no_permitido_excepcion);

            Reservacion reserva = null;

            if (pagoReservacion != null)
            {
                reserva = ServicioReservaciones.ObtenerReservacionPorCodigoConMontosNoReembolsables(pagoReservacion.Referencia);

                if (reserva == null)
                    throw new SOTException("No existe una reservación con código " + pagoReservacion.Referencia);

                if (reserva.MontosNoReembolsables.Any(m => m.Activo))
                {
                    var valor = reserva.MontosNoReembolsables.Where(m => m.Activo).Sum(m => m.ValorConIVA);

                    if (valor != ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo).Sum(m => m.ValorConIVA))
                        throw new SOTException("El monto de los ajustes de la reservación no se está respetando");
                }
                else if (ventaRenta.MontosNoReembolsablesRenta.Any(m => m.Activo))
                    throw new SOTException("La reservación no tiene ajustes");
            }

            ProcesarDetallesYPagos(configuracion.Precio, ventaRenta, numeroTransaccion, fechaActual, usuario.Id);

            renta.IdUsuarioModifico = usuario.Id;
            renta.FechaModificacion = fechaActual;

#warning revisar esto
            renta.NumeroServicio = Guid.NewGuid().ToString();

            renta.Estado = Renta.Estados.EnCurso;

            ventaRenta.Transaccion = numeroTransaccion;
            ventaRenta.FechaModificacion = fechaActual;
            ventaRenta.IdUsuarioModifico = usuario.Id;
            ventaRenta.Cobrada = true;

            if (propina != null)
            {
                propina.Activo = true;
                propina.TipoPropina = Propina.TiposPropina.Valet;
                propina.IdEmpleado = ventaRenta.IdValet.Value;
                propina.Transaccion = ventaRenta.Transaccion;
                propina.FechaCreacion = fechaActual;
                propina.FechaModificacion = fechaActual;
                propina.IdUsuarioCreo = usuario.Id;
                propina.IdUsuarioModifico = usuario.Id;
            }



            if (consumo != null)
                ventaRenta.Pagos.First(m => m.TipoPago == TiposPago.Consumo).Referencia = consumo.Transaccion = Guid.NewGuid().ToString();

            var venta = ServicioPagos.GenerarVentaNoCompletada(ventaRenta.Pagos.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.Habitacion, ventaRenta.IdPreventa, true);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (consumo != null)
                    ServicioConsumosInternosHabitacion.Crear(consumo, usuario.Id);

                ServicioPagos.Pagar(venta.Id, ventaRenta.Pagos.Where(m => m.Activo), usuario);

                RepositorioRentas.Modificar(renta);
                RepositorioRentas.GuardarCambios();

                ServicioHabitaciones.MarcarComoOcupada(renta.IdHabitacion, pagoReservacion == null ?
                    null : pagoReservacion.Referencia, usuario.Id);

                if (!string.IsNullOrEmpty(renta.NumeroTarjeta))
                {
                    decimal porcentaje = CalcularPorcentajeAbonar(ventaRenta);

                    if (ventaRenta.EsInicial)
                        ServicioVPoints.AbrirAbonoPuntos(renta.Id, renta.NumeroTarjeta, venta.Ticket.ToString(),
                                                             (ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Habitacion).Sum(m => m.ValorConIVA) +
                                                             ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA) +
                                                             ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA)) * porcentaje,
                                                             ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA) * porcentaje,
                                                             0, 0, 0, usuario);
                    else
                        ServicioVPoints.RegistrarAbonosPuntos(renta.Id, renta.NumeroTarjeta, venta.Ticket.ToString(),
                                                         (ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Habitacion).Sum(m => m.ValorConIVA) +
                                                         ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA) +
                                                         ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA)) * porcentaje,
                                                         ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA) * porcentaje,
                                                         0, 0, 0, usuario);
                }

                if (propina != null)
                    ServicioPropinas.RegistrarPropina(propina, usuario.Id);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 0);
        }

        private decimal CalcularPorcentajeAbonar(VentaRenta ventaRenta)
        {
            List<TiposPago> tiposValidor = new List<TiposPago>() { TiposPago.Efectivo, TiposPago.TarjetaCredito, TiposPago.Transferencia };

            List<IPago> pagosAbonar = ventaRenta.Pagos.Where(m => m.Activo && tiposValidor.Contains(m.TipoPago)).Select(m => (IPago)m).ToList();

            if (ventaRenta.Pagos.Any(m => m.Activo && m.TipoPago == TiposPago.Reservacion))
                pagosAbonar.AddRange(ServicioReservaciones.ObtenerPagosPorCodigosReserva(ventaRenta.Pagos.Where(m => m.Activo && m.TipoPago == TiposPago.Reservacion).Select(m => m.Referencia).ToList())
                                     .Where(m => m.Activo && tiposValidor.Contains(m.TipoPago)));

            if (ventaRenta.ValorConIVA == pagosAbonar.Sum(m => m.Valor))
                return 1;
            else
                return pagosAbonar.Sum(m => m.Valor) / ventaRenta.ValorConIVA;
        }

        public void ActualizarDatosHabitacionV2(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
                                              DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ActualizarDatosHabitacionV2(idRenta, cantidadExtensiones, cantidadRenovaciones, cantidadPersonasExtra, automoviles, usuario);
        }

        public void ActualizarDatosHabitacionV2(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<DtoAutomovil> automoviles,
                                              DtoUsuario usuario)
        {
            var configuracionGlogal = ServicioParametros.ObtenerParametros();
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarRenta = true, AsignarPersonaExtraPosterior = cantidadPersonasExtra > 0 });

            var renta = RepositorioRentas.ObtenerCargada(idRenta);

            if (renta == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            var fechaActual = DateTime.Now;

            var ultimaExtension = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa /*&& m.FechaInicio <= fechaActual*/ && m.FechaFin >= fechaActual).OrderBy(m => m.FechaInicio).FirstOrDefault();

            if (cantidadRenovaciones == 0)
            {
                if (renta.FechaFin < fechaActual && ultimaExtension == null && cantidadPersonasExtra > 0)
                    throw new SOTException(Recursos.Rentas.renta_vencida_excepcion + ", no se pueden agregar personas extra.");
            }

            if (automoviles.Count != automoviles.GroupBy(m => m.Matricula).Count())
                throw new SOTException(Recursos.Rentas.matriculas_duplicadas_excepcion);

            var configuracion = this.ServicioConfiguracionesTipo.ObtenerConfiguracionTipoNoNull(renta.IdConfiguracionTipo);

            if (automoviles.Count > configuracion.ConfiguracionTarifa.CantidadAutosMaxima)
                throw new SOTException(Recursos.Rentas.automoviles_maximos_excedidos_excepcion, configuracion.ConfiguracionTarifa.CantidadAutosMaxima.ToString());

            #region validacion horas extra

            if (configuracion.ConfiguracionTarifa.RangoFijo && cantidadExtensiones > 0)
                throw new SOTException(Recursos.ConfiguracionesNegocio.horas_extra_no_soportadas_exception);

            var maximoExtensiones = configuracion.TiemposHospedaje.Count(m => m.Activo);

            if (maximoExtensiones == 0 && cantidadExtensiones > 0)
                throw new SOTException(Recursos.Rentas.horas_extras_no_soportadas_exception);

            int extensionesSobrantes = 0;

            if (maximoExtensiones > 0)
            {
                if (cantidadRenovaciones == 0)
                {
                    int maximoOrdenRenovacion = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa && m.EsRenovacion).OrderByDescending(m => m.Orden).Select(m => m.Orden).LastOrDefault();
                    extensionesSobrantes = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Count(m => m.Activa && !m.EsRenovacion && m.Orden > maximoOrdenRenovacion);

                    if (extensionesSobrantes > maximoExtensiones)
                        extensionesSobrantes = maximoExtensiones;
                }

                var extensionesSobranteTotales = extensionesSobrantes + cantidadExtensiones;

                if (maximoExtensiones < extensionesSobranteTotales)
                    throw new SOTException(Recursos.Rentas.maximo_extensiones_superado_excepcion, maximoExtensiones.ToString());
            }

            #endregion validacion horas extra

            #region validación de personas extra

            var ultimaRenovacion = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa && m.EsRenovacion).OrderByDescending(m => m.Orden).FirstOrDefault();

            int cantidadPE = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.PersonasExtra).Where(m => m.Activa && (ultimaRenovacion != null ? m.FolioRenovacion == ultimaRenovacion.Folio : string.IsNullOrWhiteSpace(m.FolioRenovacion))).Count(m => m.Activa);

            if (configuracion.TipoHabitacion.MaximoPersonasExtra < cantidadPE + cantidadPersonasExtra ||
                configuracion.TipoHabitacion.MaximoPersonasExtra < cantidadPersonasExtra)
                ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarPersonasExtraSobreMax = true });

            #endregion validación de personas extra

            var numeroTransaccion = Guid.NewGuid().ToString();

            List<DetallePago> detalles = new List<DetallePago>();

            decimal valorDetalleConIVA, valorDetalleSinIVA, valorDetalleIVA;
            decimal precioHospedajeConIVA, precioHospedajeExtraConIVA, precioPersonasExtraConIVA;
            precioHospedajeConIVA = precioHospedajeExtraConIVA = precioPersonasExtraConIVA = 0;

            if (cantidadExtensiones > 0)
            {
                precioHospedajeExtraConIVA = Math.Round(configuracion.PrecioHospedajeExtra * (1 + configuracionGlogal.Iva_CatPar), 2);
                valorDetalleConIVA = precioHospedajeExtraConIVA * cantidadExtensiones;
                valorDetalleSinIVA = valorDetalleConIVA / (1 + configuracionGlogal.Iva_CatPar);
                valorDetalleIVA = valorDetalleConIVA - valorDetalleSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorConIVA = valorDetalleConIVA,
                    ValorSinIVA = valorDetalleSinIVA,
                    ValorIVA = valorDetalleIVA,
                    ConceptoPago = DetallePago.ConceptosPago.HorasExtra,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    Cantidad = cantidadExtensiones
                });
            }

            precioHospedajeConIVA = Math.Round(configuracion.Precio * (1 + configuracionGlogal.Iva_CatPar), 2);

            if (cantidadRenovaciones > 0)
            {
                valorDetalleConIVA = precioHospedajeConIVA * cantidadRenovaciones;
                valorDetalleSinIVA = valorDetalleConIVA / (1 + configuracionGlogal.Iva_CatPar);
                valorDetalleIVA = valorDetalleConIVA - valorDetalleSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorConIVA = valorDetalleConIVA,
                    ValorSinIVA = valorDetalleSinIVA,
                    ValorIVA = valorDetalleIVA,
                    ConceptoPago = DetallePago.ConceptosPago.Renovacion,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    Cantidad = cantidadRenovaciones
                });
            }

            if (cantidadPersonasExtra > 0)
            {
                precioPersonasExtraConIVA = Math.Round(configuracion.PrecioPersonaExtra * (1 + configuracionGlogal.Iva_CatPar), 2);
                valorDetalleConIVA = precioPersonasExtraConIVA * cantidadPersonasExtra; //* (1 + cantidadRenovaciones);
                valorDetalleSinIVA = valorDetalleConIVA / (1 + configuracionGlogal.Iva_CatPar);
                valorDetalleIVA = valorDetalleConIVA - valorDetalleSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorConIVA = valorDetalleConIVA,
                    ValorSinIVA = valorDetalleSinIVA,
                    ValorIVA = valorDetalleIVA,
                    ConceptoPago = DetallePago.ConceptosPago.PersonasExtra,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    Cantidad = cantidadPersonasExtra
                });
            }

            var precioTotalConIVA = detalles.Sum(m => m.ValorConIVA);
            var precioTotalSinIVA = detalles.Sum(m => m.ValorSinIVA);
            var totalIVA = detalles.Sum(m => m.ValorIVA);

            var ventaRenta = new VentaRenta
            {
                Transaccion = numeroTransaccion,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                IdUsuarioModifico = usuario.Id,
                Activo = true,
                ValorConIVA = precioTotalConIVA,
                ValorSinIVA = precioTotalSinIVA,
                ValorIVA = totalIVA,
                EntidadEstado = EntidadEstados.Creado,
                MotivoCancelacion = string.Empty
            };

            renta.IdUsuarioModifico = usuario.Id;
            renta.FechaModificacion = fechaActual;

            var extensionesConfiguracion = configuracion.TiemposHospedaje.Where(m => m.Activo).OrderBy(m => m.Orden).ToList();

            decimal precioHospedajeExtraSinIVA = precioHospedajeExtraConIVA / (1 + configuracionGlogal.Iva_CatPar);
            decimal precioPersonasExtraSinIVA = precioPersonasExtraConIVA / (1 + configuracionGlogal.Iva_CatPar);
            decimal precioHospedajeSinIVA = precioHospedajeConIVA / (1 + configuracionGlogal.Iva_CatPar);

            for (int i = 0, cantidadExistente = cantidadPE; i < cantidadPersonasExtra; i++, cantidadExistente++)
            {
                ventaRenta.PersonasExtra.Add(new PersonaExtra
                {
                    Activa = true,
                    Precio = precioPersonasExtraSinIVA,
                    FolioRenovacion = ultimaRenovacion != null ? ultimaRenovacion.Folio : "",
                    CobroPorSeparado = true,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    EsFueraDeRango = cantidadExistente >= configuracion.TipoHabitacion.MaximoPersonasExtra
                });
            }

            ultimaExtension = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa && !m.EsRenovacion).OrderByDescending(m => m.Orden).FirstOrDefault();

            var maximoOrden = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa).OrderByDescending(m => m.Orden).Select(m => m.Orden).FirstOrDefault();

            for (int i = 0; i < cantidadRenovaciones; i++)
            {
                ventaRenta.Extensiones.Add(new Extension
                {
                    Activa = true,
                    EsRenovacion = true,
                    Precio = precioHospedajeSinIVA,
                    CobroPorSeparado = true,
                    FechaCreacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    Folio = Guid.NewGuid().ToString(),
                    Orden = ++maximoOrden
                });

                for (int j = 0; j < cantidadPersonasExtra; j++)
                {
                    ventaRenta.PersonasExtra.Add(new Modelo.Entidades.PersonaExtra
                    {
                        Activa = true,
                        Precio = 0,
                        FolioRenovacion = ventaRenta.Extensiones.Last().Folio,
                        CobroPorSeparado = true,
                        FechaCreacion = fechaActual,
                        FechaModificacion = fechaActual,
                        IdUsuarioCreo = usuario.Id,
                        IdUsuarioModifico = usuario.Id,
                        EsFueraDeRango = j >= configuracion.TipoHabitacion.MaximoPersonasExtra
                    });
                }
            }

            for (int i = 0; i < cantidadExtensiones; i++)
            {
                ventaRenta.Extensiones.Add(new Extension
                {
                    Activa = true,
                    Precio = precioHospedajeExtraSinIVA,
                    CobroPorSeparado = true,
                    FechaCreacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    Folio = "",
                    Orden = ++maximoOrden
                });
            }

            int contador = extensionesSobrantes >= maximoExtensiones ? extensionesSobrantes : 0;

            if (configuracion.ConfiguracionTarifa.RangoFijo)
            {
                var fechaInicio = ultimaRenovacion != null ? ultimaRenovacion.FechaInicio : renta.FechaInicio;
                var fechaFin = ultimaRenovacion != null ? ultimaRenovacion.FechaFin : renta.FechaFin;

                var fechaInicioE = ultimaExtension != null ? ultimaExtension.FechaInicio : fechaInicio;
                var fechaFinE = ultimaExtension != null ? ultimaExtension.FechaFin : fechaFin;

                var tiemposConfig = configuracion.TiemposHospedaje.OrderBy(m => m.Orden).ToList();

                foreach (var extension in ventaRenta.Extensiones.Where(m => m.Activa).OrderBy(m => m.Orden))
                {
                    if (extension.EsRenovacion)
                    {
                        fechaInicio = fechaInicio.AddDays(1);
                        fechaFin = fechaFin.AddDays(1);

                        fechaInicioE = fechaInicio;
                        fechaFinE = fechaFin;

                        extension.FechaInicio = fechaInicio;
                        extension.FechaFin = fechaFin;
                    }
                    else
                    {
                        fechaInicioE = fechaFinE.AddSeconds(1);
                        fechaFinE = fechaInicioE.AddMinutes(tiemposConfig[contador++].Minutos);

                        extension.FechaInicio = fechaInicioE;
                        extension.FechaFin = fechaFinE;
                    }

                    extension.CobroPorSeparado = true;
                    extension.FechaCreacion = fechaActual;
                    extension.IdUsuarioCreo = usuario.Id;

                    if (contador >= maximoExtensiones)
                        contador = 0;
                }
            }
            else
            {
                ultimaExtension = renta.VentasRenta.Where(m => m.Activo).SelectMany(m => m.Extensiones).Where(m => m.Activa).OrderByDescending(m => m.Orden).FirstOrDefault();

                var tiemposConfig = configuracion.TiemposHospedaje.OrderBy(m => m.Orden).ToList();

                var fechaInicio = ultimaExtension != null ? ultimaExtension.FechaInicio : renta.FechaInicio;
                var fechaFin = ultimaExtension != null ? ultimaExtension.FechaFin : renta.FechaFin;

                foreach (var extension in ventaRenta.Extensiones.Where(m => m.Activa).OrderBy(m => m.Orden))
                {
                    fechaInicio = fechaFin.AddSeconds(1);

                    if (extension.EsRenovacion)
                        fechaFin = fechaInicio.AddHours(configuracion.DuracionOEntrada);
                    else
                        fechaFin = fechaInicio.AddMinutes(tiemposConfig[contador++].Minutos);

                    extension.FechaInicio = fechaInicio;
                    extension.FechaFin = fechaFin;
                    extension.CobroPorSeparado = true;
                    extension.FechaCreacion = fechaActual;
                    extension.IdUsuarioCreo = usuario.Id;

                    if (contador >= maximoExtensiones)
                        contador = 0;
                }
            }

            var autosNuevos = automoviles.Where(an => !renta.RentaAutomoviles.Any(m => m.Activa && m.Matricula.Trim().ToUpper()
                                                       .Equals(an.Matricula))).ToList();

            var autosModificados = automoviles.Except(autosNuevos).ToList();

            foreach (var autoModificado in autosModificados)
            {
                var autoRenta = renta.RentaAutomoviles.FirstOrDefault(m => m.Activa && m.Matricula.Trim().ToUpper()
                                                       .Equals(autoModificado.Matricula));

                if (autoRenta != null &&
                    (autoRenta.Marca != autoModificado.Marca ||
                    autoRenta.Modelo != autoModificado.Modelo ||
                    autoRenta.Color != autoModificado.Color))
                {
                    autoRenta.Marca = autoModificado.Marca;
                    autoRenta.Modelo = autoModificado.Modelo;
                    autoRenta.Color = autoModificado.Color;

                    autoRenta.FechaModificacion = fechaActual;
                    autoRenta.IdUsuarioModifico = usuario.Id;
                }
            }

            foreach (var autoNuevo in autosNuevos)
            {
                renta.RentaAutomoviles.Add(new AutomovilRenta
                {
                    Activa = true,
                    Matricula = autoNuevo.Matricula,
                    Marca = autoNuevo.Marca,
                    Modelo = autoNuevo.Modelo,
                    Color = autoNuevo.Color,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id
                });
            }

            foreach (var automovil in renta.RentaAutomoviles)
            {
                if (automovil.Activa && !automoviles.Any(m => m.Matricula.Equals(automovil.Matricula.Trim().ToUpper())))
                {
                    automovil.Activa = false;
                    automovil.FechaEliminacion = fechaActual;
                    automovil.IdUsuarioElimino = usuario.Id;
                }
            }

            if (ventaRenta.ValorConIVA != 0)
            {
                renta.Estado = Renta.Estados.PendienteCobro;
                renta.VentasRenta.Add(ventaRenta);
            }
            else
                ventaRenta = null;

            if (ventaRenta != null)
            {
                var idPreventa = ServicioPreventas.GrenerarPreventa(Venta.ClasificacionesVenta.Habitacion);

                ventaRenta.IdPreventa = idPreventa;
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioRentas.Modificar(renta);
                RepositorioRentas.GuardarCambios();

                if (ventaRenta != null)
                    ServicioHabitaciones.MarcarComoPendienteCobro(renta.IdHabitacion, usuario.Id);

                scope.Complete();
            }

            if (ventaRenta != null)
            {
                ServicioTickets.ImprimirTicket(ventaRenta, configuracionImpresoras.ImpresoraTickets, 1);
                ServicioTickets.ImprimirTicket(ventaRenta, configuracionImpresoras.ImpresoraBar, 2);
            }
        }

        public void CancelarVentaPendiente(int idVentaRenta, DtoCredencial credencial)
        {
            string motivoCancelacion = "";

            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            var configuracionGlogal = ServicioParametros.ObtenerParametros();
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarRenta = true });

            var ventaR = RepositorioVentasRentas.ObtenerVentaRentaPendienteParaCancelar(idVentaRenta);

            if (ventaR == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            var renta = RepositorioRentas.Obtener(m => m.Id == ventaR.IdRenta);

            if (renta.Estado != Renta.Estados.PendienteCobro)
                throw new SOTException(Recursos.Rentas.renta_no_pendiente_cobro_excepcion);

            var fechaActual = DateTime.Now;

            var numeroTransaccion = Guid.NewGuid().ToString();

            ventaR.Activo = false;
            ventaR.FechaEliminacion = fechaActual;
            ventaR.FechaModificacion = fechaActual;
            ventaR.IdUsuarioElimino = usuario.Id;
            ventaR.IdUsuarioModifico = usuario.Id;
            ventaR.MotivoCancelacion = motivoCancelacion;

            if (ventaR.Preventa != null)
                ventaR.Preventa.Activa = false;

            //if (!string.IsNullOrEmpty(ventaR.Transaccion))
            //    transacciones.Add(ventaR.Transaccion);
            renta.FechaModificacion = fechaActual;
            renta.IdUsuarioModifico = usuario.Id;

            if (ventaR.EsInicial)
            {
                if (VerificarTieneComandasPendientes(renta.Id))
                    throw new SOTException(Recursos.Rentas.cuentas_pendientes_excepcion);

                ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { DesocuparHabitaciones = true });

                renta.Activa = false;
                renta.FechaSalida = fechaActual;
                renta.IdUsuarioElimino = usuario.Id;
                renta.Estado = Renta.Estados.Cancelada;
            }
            else
            {
                renta.Estado = Renta.Estados.EnCurso;
            }

            foreach (var detalle in ventaR.DetallesPago)
            {
                if (!detalle.Activo)
                    continue;

                detalle.Activo = false;
                detalle.FechaEliminacion = fechaActual;
                detalle.FechaModificacion = fechaActual;
                detalle.IdUsuarioElimino = usuario.Id;
                detalle.IdUsuarioModifico = usuario.Id;
            }

            foreach (var extension in ventaR.Extensiones)
            {
                if (!extension.Activa)
                    continue;

                extension.Activa = false;
                extension.FechaEliminacion = fechaActual;
                extension.IdUsuarioElimino = usuario.Id;
            }

            foreach (var personaExtra in ventaR.PersonasExtra)
            {
                if (!personaExtra.Activa)
                    continue;

                personaExtra.Activa = false;
                personaExtra.FechaEliminacion = fechaActual;
                personaExtra.FechaModificacion = fechaActual;
                personaExtra.IdUsuarioElimino = usuario.Id;
                personaExtra.IdUsuarioModifico = usuario.Id;
            }

            foreach (var paquete in ventaR.PaquetesRenta)
            {
                if (!paquete.Activo)
                    continue;

                paquete.Activo = false;
                paquete.FechaEliminacion = fechaActual;
                paquete.IdUsuarioElimino = usuario.Id;
            }

            foreach (var mnr in ventaR.MontosNoReembolsablesRenta)
            {
                if (!mnr.Activo)
                    continue;

                mnr.Activo = false;
                mnr.FechaEliminacion = fechaActual;
                mnr.IdUsuarioElimino = usuario.Id;
            }

            foreach (var pago in ventaR.Pagos)
            {
                if (!pago.Activo)
                    continue;

                pago.Activo = false;
                pago.FechaEliminacion = fechaActual;
                pago.FechaModificacion = fechaActual;
                pago.IdUsuarioElimino = usuario.Id;
                pago.IdUsuarioModifico = usuario.Id;
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioRentas.Modificar(renta);
                RepositorioRentas.GuardarCambios();

                RepositorioVentasRentas.Modificar(ventaR);
                RepositorioVentasRentas.GuardarCambios();

                if (ventaR.EsInicial)
                    ServicioHabitaciones.CancelarOcupacionInterno(renta.IdHabitacion, usuario.Id);
                else
                    ServicioHabitaciones.MarcarComoOcupada(renta.IdHabitacion, null, usuario.Id);

                scope.Complete();
            }

            //if (ventaRenta != null)
            //{
            //    ServicioTickets.ImprimirTicket(ventaRenta, configuracionImpresoras.ImpresoraTickets, 1);
            //    ServicioTickets.ImprimirTicket(ventaRenta, configuracionImpresoras.ImpresoraBar, 2);
            //}

            ServicioTickets.ImprimirTicketVentaRentaCancelada(idVentaRenta, configuracionImpresoras.ImpresoraTickets, 1);
        }

        public void CrearRenta(Renta rentaNueva, bool cobrar, Propina propina, DtoCredencial credencial, ConsumoInternoHabitacion consumo)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            CrearRenta(rentaNueva, cobrar, propina, usuario, consumo);
        }

        public void CrearRenta(Renta rentaNueva, bool cobrar, Propina propina, DtoUsuario usuario, ConsumoInternoHabitacion consumo)
        {
            var configuracion = ServicioConfiguracionesTipo.ObtenerConfiguracionTipoNoNull(rentaNueva.IdConfiguracionTipo);

            if (rentaNueva == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            var fechaActual = DateTime.Now;
            var numeroTransaccion = cobrar ? Guid.NewGuid().ToString() : "";

            int cantidadVentas = rentaNueva.VentasRenta.Count(m => m.Activo);

            if (cantidadVentas > 1)
                throw new SOTException(Recursos.Rentas.multiple_informacion_venta_excepcion);
            if (cantidadVentas == 0)
                throw new SOTException(Recursos.Rentas.informacion_venta_nula_excepcion);

            if (!cobrar && propina != null)
                throw new SOTException(Recursos.Rentas.propina_no_necesaria_excepcion);

#warning Se habilitó la vinculación de tarjetas v desde el momento de la asignación, pero no se permite asignar una forma de pago
            //if (!cobrar && !string.IsNullOrWhiteSpace(rentaNueva.NumeroTarjeta))
            //    throw new SOTException(Recursos.Rentas.numero_tarjeta_no_permitido_excepcion);

            if (rentaNueva.NumeroTarjeta != null)
                rentaNueva.NumeroTarjeta = rentaNueva.NumeroTarjeta.Trim();

            var ventaRenta = rentaNueva.VentasRenta.Single(m => m.Activo);

            usuario = ServicioPagos.ProcesarPermisosEspeciales(ventaRenta.Pagos.Where(m => m.Activo), usuario);

            ventaRenta.EsInicial = true;

            bool tienePermisosReales;

            try
            {
                ServicioPermisos.ValidarPermisos(usuario, ArmarPermiso(ventaRenta, cobrar));
                tienePermisosReales = true;
            }
            catch (SOTPermisosException)
            {
                tienePermisosReales = false;
            }

            if (!tienePermisosReales)
                ServicioPermisos.ValidarPermisos(usuario, ArmarPermiso(ventaRenta, cobrar && ventaRenta.Pagos.Where(m => m.Activo).All(m => m.TipoPago != TiposPago.Reservacion)));

            if (cobrar)
            {
                ValidarIVA(ventaRenta, configuracion);

                if (ventaRenta.Pagos.Any(m => m.Activo && m.TipoPago == TiposPago.Reservacion))
                    ServicioHabitaciones.ValidarEstaReservadaPreparada(rentaNueva.IdHabitacion);
                else
                    ServicioHabitaciones.ValidarEstaPreparada(rentaNueva.IdHabitacion);

                if (ventaRenta.Pagos.Any(m => m.Activo && m.TipoPago == TiposPago.Consumo) && consumo == null)
                    throw new SOTException(Recursos.Rentas.consumo_interno_nulo_excepcion);

                if (!ventaRenta.Pagos.Any(m => m.Activo && m.TipoPago == TiposPago.Consumo) && consumo != null)
                    throw new SOTException(Recursos.Rentas.consumo_interno_no_requerido_excepcion);
            }
            else
            {
                ServicioHabitaciones.ValidarEstaPreparada(rentaNueva.IdHabitacion);

                if (ventaRenta.Pagos.Any(m => m.Activo))
                    throw new SOTException(Recursos.Rentas.pagos_no_permitidos_excepcion);
            }

            #region validación de automóviles

            if (rentaNueva.RentaAutomoviles.Count > configuracion.ConfiguracionTarifa.CantidadAutosMaxima)
                throw new SOTException(Recursos.Rentas.automoviles_maximos_excedidos_excepcion, configuracion.ConfiguracionTarifa.CantidadAutosMaxima.ToString());

            foreach (var rentaAutomovil in rentaNueva.RentaAutomoviles)
            {
                if (string.IsNullOrWhiteSpace(rentaAutomovil.Matricula))
                    throw new SOTException(Recursos.Rentas.matricula_automovil_invalida_excepcion);

                ServicioReportesMatriculas.ValidarNoInicidencias(rentaAutomovil.Matricula);

                rentaAutomovil.Matricula = rentaAutomovil.Matricula.Trim().ToUpper();
            }

            #endregion validación de automóviles

            #region validación de personas extra

            if (configuracion.TipoHabitacion.MaximoPersonasExtra < ventaRenta.PersonasExtra.Count(m => m.Activa))
                //throw new SOTException(Recursos.Rentas.personas_extra_fuera_rango_excepcion, configuracion.TipoHabitacion.MaximoPersonasExtra.ToString());
                //Para validar que el usuario que realiza la acción tenga permisos para agregar personas fuera del máximo permitido
                ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarPersonasExtraSobreMax = true });

            #endregion validación de personas extra

            #region validacion horas extra

            if (ventaRenta.Extensiones.Where(m => m.Activa).GroupBy(m => m.Orden).Count() != ventaRenta.Extensiones.Count(m => m.Activa))
                throw new SOTException(Recursos.Rentas.extensiones_en_desorden_excepcion);

            if (configuracion.ConfiguracionTarifa.RangoFijo && ventaRenta.Extensiones.Any(m => m.Activa && !m.EsRenovacion))
                throw new SOTException(Recursos.ConfiguracionesNegocio.horas_extra_no_soportadas_exception);

            var cantidadExtensiones = ventaRenta.Extensiones.Count(m => m.Activa && !m.EsRenovacion);
            var cantidadRenovaciones = ventaRenta.Extensiones.Count(m => m.Activa && m.EsRenovacion);
            var maximoExtensiones = configuracion.TiemposHospedaje.Count(m => m.Activo);

            if (maximoExtensiones == 0 && cantidadExtensiones > 0)
                throw new SOTException(Recursos.Rentas.horas_extras_no_soportadas_exception);

            if (maximoExtensiones < cantidadExtensiones)
                throw new SOTException(Recursos.Rentas.maximo_extensiones_superado_excepcion, maximoExtensiones.ToString());

            /*if (maximoExtensiones > 0)
            {
                int proporcionExtensiones = cantidadExtensiones / maximoExtensiones;
                int extensionesSobrantes = cantidadExtensiones % maximoExtensiones;

                if ((extensionesSobrantes > 0 && proporcionExtensiones != cantidadRenovaciones) || (extensionesSobrantes == 0 && (proporcionExtensiones - 1) != cantidadRenovaciones && proporcionExtensiones != cantidadRenovaciones))
                    throw new SOTException(Recursos.Rentas.maximo_extensiones_superado_excepcion, maximoExtensiones.ToString());
            }

            int contador = 1;

            foreach (var extension in ventaRenta.Extensiones.Where(m => m.Activa).OrderBy(m => m.Orden))
            {
                if (contador > maximoExtensiones)
                {
                    if (!extension.EsRenovacion)
                        throw new SOTException(Recursos.Rentas.extensiones_en_desorden_excepcion);

                    contador = 1;
                }
                else
                {
                    if (extension.EsRenovacion)
                        throw new SOTException(Recursos.Rentas.extensiones_en_desorden_excepcion);

                    contador++;
                }
            }*/

            int ultimoOrdenRenovacion = ventaRenta.Extensiones.Where(m => m.Activa && m.EsRenovacion).OrderByDescending(m => m.Orden).Select(m => m.Orden).FirstOrDefault();

            if (ventaRenta.Extensiones.Any(m => m.Activa && !m.EsRenovacion && m.Orden < ultimoOrdenRenovacion))
                throw new SOTException(Recursos.Rentas.extensiones_en_desorden_excepcion);

            #endregion validacion horas extra

            #region validacion hospedaje extra

            //if (!configuracion.ConfiguracionTarifa.RangoFijo && ventaRenta.Extensiones.Any(m => m.Activa && m.EsRenovacion))
            //    throw new SOTException(Recursos.ConfiguracionesNegocio.renovaciones_de_inicio_no_soportadas_exception);

            foreach (var r in ventaRenta.PersonasExtra.Where(m => m.FolioRenovacion == null))
                r.FolioRenovacion = "";

            foreach (var renovacion in ventaRenta.PersonasExtra.Where(m => m.Activa && !string.IsNullOrEmpty(m.FolioRenovacion)))
            {
                if (!ventaRenta.Extensiones.Any(m => m.Activa && m.EsRenovacion && m.Folio == renovacion.FolioRenovacion))
                    throw new SOTException(Recursos.Rentas.folio_renovacion_desconocido_excepcion);
            }

            #endregion validacion hospedaje extra

            //var estadosValidos = new List<int>(){(int)Renta.Estados.Finalizada, (int)Renta.Estados.Cancelada};
            int estadoEnCurso = (int)Renta.Estados.EnCurso;
            int estadoPendiente = (int)Renta.Estados.PendienteCobro;
            int estadoFinalizado = (int)Renta.Estados.Finalizada;

            if (!string.IsNullOrEmpty(rentaNueva.NumeroTarjeta))
            {
                if (RepositorioRentas.Alguno(m => m.NumeroTarjeta != null && m.NumeroTarjeta == rentaNueva.NumeroTarjeta && (m.IdEstado == estadoEnCurso || m.IdEstado == estadoPendiente)/*!estadosValidos.Contains(m.IdEstado)*/))
                    throw new SOTException(Recursos.Rentas.tarjetaV_en_uso_exception);

                if (RepositorioRentas.Alguno(m => m.NumeroTarjeta != null && m.NumeroTarjeta == rentaNueva.NumeroTarjeta && m.IdEstado == estadoFinalizado
                                                  && m.FechaSalida.Value.Year == fechaActual.Year && m.FechaSalida.Value.Month == fechaActual.Month && m.FechaSalida.Value.Day == fechaActual.Day))
                    throw new SOTException(Recursos.Rentas.tarjetaV_usada_mismo_dia_exception);
            }

            var formaPagoReservaciones = ventaRenta.Pagos.FirstOrDefault(m => m.TipoPago == TiposPago.Reservacion);

            Reservacion reservacion = null;

            if (formaPagoReservaciones != null)
            {
                reservacion = ServicioReservaciones.ObtenerReservacionPorCodigoConMontosNoReembolsables(formaPagoReservaciones.Referencia);

                if (reservacion == null)
                    throw new SOTException("No existe una reservación con código " + formaPagoReservaciones.Referencia);
            }

            if (cobrar)
                ProcesarDetallesYPagos(rentaNueva.PrecioHabitacion, ventaRenta, numeroTransaccion, fechaActual, usuario.Id);

            #region Validación de la renta en base a la configuración

            rentaNueva.FechaRegistro = fechaActual;

            DateTime fechaInicio, fechaFin;

            if (formaPagoReservaciones != null)
            {
                //var reservacion = ServicioReservaciones.ObtenerReservacionPorCodigoConMontosNoReembolsables(formaPagoReservaciones.Referencia);

                //if (reservacion == null)
                //    throw new SOTException("No existe una reservación con código " + formaPagoReservaciones.Referencia);

                if (reservacion.MontosNoReembolsables.Any(m => m.Activo))
                {
                    var valor = reservacion.MontosNoReembolsables.Where(m => m.Activo).Sum(m => m.ValorConIVA);

                    if (valor != ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo).Sum(m => m.ValorConIVA))
                        throw new SOTException("El monto de los ajustes de la reservación no se está respetando");
                }
                else if (ventaRenta.MontosNoReembolsablesRenta.Any(m => m.Activo))
                    throw new SOTException("La reservación no tiene ajustes");

                fechaInicio = reservacion.FechaEntrada.Date.AddHours(configuracion.DuracionOEntrada);
                fechaFin = reservacion.FechaSalida.AddHours(configuracion.HoraSalida.Value);

                if (rentaNueva.FechaRegistro <= fechaInicio || rentaNueva.FechaRegistro >= fechaFin)
                    throw new SOTException("La reservación comienza en la fecha {0} y termina en la fecha {1}", fechaInicio.ToString("dd/MM/yyyy hh:mm tt"), fechaFin.ToString("dd/MM/yyyy hh:mm tt"));

                fechaFin = fechaInicio.Date.AddDays(1).AddHours(configuracion.HoraSalida.Value);

                rentaNueva.FechaInicio = fechaInicio;
                rentaNueva.FechaFin = fechaFin;
            }
            else if (configuracion.ConfiguracionTarifa.RangoFijo)
            {
                //fix de fechas
                fechaFin = rentaNueva.FechaRegistro.Date.AddHours(configuracion.HoraSalida.Value);

                if (fechaFin <= rentaNueva.FechaRegistro)
                {
                    fechaInicio = rentaNueva.FechaRegistro.Date.AddHours(configuracion.DuracionOEntrada);
                    fechaFin = fechaInicio.Date.AddDays(1).AddHours(configuracion.HoraSalida.Value);
                }
                else
                {
                    fechaInicio = fechaFin.Date.AddDays(-1).AddHours(configuracion.DuracionOEntrada);
                }
                if (rentaNueva.FechaRegistro < fechaInicio || rentaNueva.FechaRegistro > fechaFin)
                    throw new SOTException(Recursos.Rentas.renta_fuera_de_horario_excepcion, fechaInicio.ToString("hh:mm tt"), fechaFin.ToString("hh:mm tt"));

                rentaNueva.FechaInicio = fechaInicio;
                rentaNueva.FechaFin = fechaFin;
            }
            else
            {
                fechaInicio = rentaNueva.FechaInicio = rentaNueva.FechaRegistro;
                fechaFin = rentaNueva.FechaFin = rentaNueva.FechaRegistro.AddHours(configuracion.DuracionOEntrada);
            }

            if (configuracion.ConfiguracionTarifa.RangoFijo)
            {
                int contador = 0;

                var tiemposConfig = configuracion.TiemposHospedaje.OrderBy(m => m.Orden).ToList();

                foreach (var extension in ventaRenta.Extensiones.Where(m => m.Activa).OrderBy(m => m.Orden))
                {
                    if (extension.EsRenovacion)
                    {
                        fechaInicio = fechaInicio.AddDays(1);
                        fechaFin = fechaFin.AddDays(1);
                    }
                    else
                    {
                        fechaInicio = fechaFin.AddSeconds(1);
                        fechaFin = fechaInicio.AddMinutes(tiemposConfig[contador++].Minutos);
                    }

                    extension.FechaInicio = fechaInicio;
                    extension.FechaFin = fechaFin;
                    extension.CobroPorSeparado = false;
                    extension.FechaCreacion = fechaActual;
                    extension.IdUsuarioCreo = usuario.Id;
                    if (string.IsNullOrWhiteSpace(extension.Folio))
                        extension.Folio = "";

                    if (contador >= maximoExtensiones)
                        contador = 0;
                }
            }
            else
            {
                int contador = 0;

                var tiemposConfig = configuracion.TiemposHospedaje.OrderBy(m => m.Orden).ToList();

                foreach (var extension in ventaRenta.Extensiones.Where(m => m.Activa).OrderBy(m => m.Orden))
                {
                    fechaInicio = fechaFin.AddSeconds(1);

                    if (extension.EsRenovacion)
                        fechaFin = fechaInicio.AddHours(configuracion.DuracionOEntrada);
                    else
                        fechaFin = fechaInicio.AddMinutes(tiemposConfig[contador++].Minutos);

                    extension.FechaInicio = fechaInicio;
                    extension.FechaFin = fechaFin;
                    extension.CobroPorSeparado = false;
                    extension.FechaCreacion = fechaActual;
                    extension.IdUsuarioCreo = usuario.Id;
                    if (string.IsNullOrWhiteSpace(extension.Folio))
                        extension.Folio = "";

                    if (contador >= maximoExtensiones)
                        contador = 0;
                }
            }

            //foreach (var renovacion in ventaRenta.Renovaciones.Where(m => m.Activa))
            //{
            //    if (configuracion.ConfiguracionTarifa.RangoFijo)
            //    {
            //        fechaInicio = fechaInicio.AddDays(1);
            //        fechaFin = fechaFin.AddDays(1);
            //    }
            //    else
            //    {
            //        fechaInicio = fechaFin.AddSeconds(1);
            //        fechaFin = fechaInicio.AddHours(configuracion.DuracionOEntrada);
            //    }

            //    renovacion.CobroPorSeparado = false;
            //    renovacion.FechaCreacion = fechaActual;
            //    renovacion.FechaModificacion = fechaActual;
            //    renovacion.IdUsuarioCreo = usuario.Id;
            //    renovacion.IdUsuarioModifico = usuario.Id;
            //    renovacion.FechaInicio = fechaInicio;
            //    renovacion.FechaFin = fechaFin;
            //}

            #endregion Validación de la renta en base a la configuración

            rentaNueva.IdUsuarioCreo = usuario.Id;
            rentaNueva.IdUsuarioModifico = usuario.Id;
            rentaNueva.FechaModificacion = fechaActual;
            rentaNueva.NumeroServicio = Guid.NewGuid().ToString();
            rentaNueva.Estado = cobrar ? Renta.Estados.EnCurso : Renta.Estados.PendienteCobro;
            rentaNueva.MotivoCancelacion = string.Empty;

            //foreach (var extension in ventaRenta.TiemposExtra.Where(m => m.Activa))
            //{
            //    extension.FechaCreacion = fechaActual;
            //    extension.FechaModificacion = fechaActual;
            //    extension.IdUsuarioCreo = usuario.Id;
            //    extension.IdUsuarioModifico = usuario.Id;
            //    extension.CobroPorSeparado = false;
            //}

            int maximoPersonas = configuracion.TipoHabitacion.MaximoPersonasExtra;

            foreach (var personaExtra in ventaRenta.PersonasExtra.Where(m => m.Activa && string.IsNullOrEmpty(m.FolioRenovacion)))
            {
                personaExtra.FechaCreacion = fechaActual;
                personaExtra.FechaModificacion = fechaActual;
                personaExtra.IdUsuarioCreo = usuario.Id;
                personaExtra.IdUsuarioModifico = usuario.Id;
                personaExtra.EsFueraDeRango = maximoPersonas-- <= 0;
                personaExtra.CobroPorSeparado = false;
            }

            foreach (var personaExtra in ventaRenta.PersonasExtra.Where(m => m.Activa && !string.IsNullOrEmpty(m.FolioRenovacion)))
            {
                personaExtra.FechaCreacion = fechaActual;
                personaExtra.FechaModificacion = fechaActual;
                personaExtra.IdUsuarioCreo = usuario.Id;
                personaExtra.IdUsuarioModifico = usuario.Id;
                personaExtra.EsFueraDeRango = maximoPersonas-- <= 0;
                personaExtra.CobroPorSeparado = false;
            }

            foreach (var automovil in rentaNueva.RentaAutomoviles.Where(m => m.Activa))
            {
                automovil.FechaCreacion = fechaActual;
                automovil.FechaModificacion = fechaActual;
                automovil.IdUsuarioCreo = usuario.Id;
                automovil.IdUsuarioModifico = usuario.Id;
            }

            foreach (var paquete in ventaRenta.PaquetesRenta.Where(m => m.Activo))
            {
                paquete.FechaCreacion = fechaActual;
                paquete.IdUsuarioCreo = usuario.Id;
            }

            foreach (var paquete in ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo))
            {
                paquete.FechaCreacion = fechaActual;
                paquete.IdUsuarioCreo = usuario.Id;
            }

            ventaRenta.Transaccion = numeroTransaccion;
            ventaRenta.IdValet = cobrar ? default(int?) : usuario.Id;
            ventaRenta.FechaCreacion = fechaActual;
            ventaRenta.FechaModificacion = fechaActual;
            ventaRenta.IdUsuarioCreo = usuario.Id;
            ventaRenta.IdUsuarioModifico = usuario.Id;
            ventaRenta.EsInicial = true;
            ventaRenta.MotivoCancelacion = string.Empty;

            if (cobrar)
                ventaRenta.IdValet = usuario.Id;

            if (propina != null)
            {
                propina.Activo = true;
                propina.TipoPropina = Propina.TiposPropina.Valet;
                propina.IdEmpleado = ventaRenta.IdValet.Value;
                propina.Transaccion = ventaRenta.Transaccion;
                propina.FechaCreacion = fechaActual;
                propina.FechaModificacion = fechaActual;
                propina.IdUsuarioCreo = usuario.Id;
                propina.IdUsuarioModifico = usuario.Id;
            }

            if (consumo != null)
                ventaRenta.Pagos.First(m => m.TipoPago == TiposPago.Consumo).Referencia = consumo.Transaccion = Guid.NewGuid().ToString();

            FinalizarCreacion(rentaNueva, ventaRenta, cobrar, tienePermisosReales, propina, numeroTransaccion, fechaActual, usuario, consumo);
        }

        public Renta ObtenerRentaActualPorHabitacion(int idHabitacion)
        {
            return RepositorioRentas.ObtenerRentaActualPorHabitacion(idHabitacion);
        }

        public DtoDetallesRenta ObtenerResumenVentasUltimaRenta(int idHabitacion)
        {
            var ultimaRenta = RepositorioRentas.ObtenerUltimaRentaPorHabitacion(idHabitacion);

            if (ultimaRenta == null)
                return new DtoDetallesRenta { ResumenesVentasHabitacion = new List<DtoResumenVentaRenta>(), ResumenesComandas = new List<DtoResumenComanda>() };

            var configuracion = ServicioParametros.ObtenerParametros();

            var detalle = new DtoDetallesRenta
            {
                NumeroServicio = ultimaRenta.NumeroServicio,
                Reservacion = RepositorioRentas.ObtieneCodigoReservacion(ultimaRenta.FechaRegistro, ultimaRenta.IdHabitacion),
                ResumenesVentasHabitacion = RepositorioRentas.ObtenerResumenVentas(ultimaRenta.Id, configuracion.Iva_CatPar),
                ResumenesComandas = RepositorioComandas.ObtenerResumenesComandasPorRenta(ultimaRenta.Id)
            };

            foreach (var comanda in detalle.ResumenesComandas)
            {
                foreach (var articulo in comanda.Articulos)
                {
                    var art = ServicioArticulos.ObtenerArticuloPorId(articulo.Nombre);
                    if (art != null)
                        articulo.Nombre = art.Desc_Art;
                }
            }

            return detalle;
        }

        public Renta ObtenerUltimaRentaPorHabitacionConDatosFiscales(int idHabitacion)
        {
            return RepositorioRentas.ObtenerUltimaRentaPorHabitacionConDatosFiscales(idHabitacion);
        }

        public List<AutomovilRenta> ObtenerAutomovilesUltimaRenta(int idHabitacion)
        {
            return RepositorioRentas.ObtenerAutomovilesUltimaRenta(idHabitacion);
        }

        //public bool VerificarTieneTarjetaVPointsVinculada(int idHabitacion, DtoUsuario UsuarioActual)
        //{
        //    var estadoEnCurso = (int)Renta.Estados.EnCurso;

        //    return RepositorioRentas.Alguno(m => m.IdHabitacion == idHabitacion && m.IdEstado == estadoEnCurso && !string.IsNullOrEmpty(m.NumeroTarjeta));
        //}

        public List<DetallePago> ObtenerDetallesPorTransaccion(string transaccion, DateTime? fechaFin)
        {
            return RepositorioRentas.ObtenerDetallesPorTransaccion(transaccion, fechaFin);
        }

        public void ActualizarPagoHabitacion(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, DtoUsuario usuario, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null, int? idEmpleado = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarPagos = true });

            var pago = RepositorioPagosRenta.Obtener(m => m.Activo && m.Id == idPago, m => m.VentaRenta);

            if (pago == null)
                throw new SOTException(Recursos.Pagos.pago_nulo_o_eliminado_excepcion);

            if (formaPago != TiposPago.TarjetaCredito && valorPropina != 0)
                throw new SOTException(Recursos.Pagos.propina_no_soportada_excepcion);

            ServicioPagos.ValidarEfectivoOTarjeta(pago);
            ServicioPagos.ValidarEfectivoOTarjeta(formaPago);

            if (pago.VentaRenta.IdValet != idEmpleado && idEmpleado.HasValue)
                ServicioEmpleados.ValidarPermisos(idEmpleado.Value, new DtoPermisos { Aparcar = true });//ServicioEmpleados.ValidarValetActivoHabilitado(idEmpleado.Value);

            Propina propina = pago.TipoPago == TiposPago.TarjetaCredito ?
                ServicioPropinas.ObtenerPropina(pago.Transaccion, pago.NumeroTarjeta, pago.TipoTarjeta.Value) :
                null;

            var fechaActual = DateTime.Now;

            pago.FechaModificacion = fechaActual;
            pago.IdUsuarioModifico = usuario.Id;
            pago.TipoPago = formaPago;
            pago.Referencia = referencia;
            pago.TipoTarjeta = tipoTarjeta;
            pago.NumeroTarjeta = numeroTarjeta;

            ServicioPagos.ValidarPago(pago, usuario);

            if (propina != null)
            {
                if (valorPropina != 0)
                {
                    propina.IdEmpleado = idEmpleado.Value;
                    propina.FechaModificacion = fechaActual;
                    propina.IdUsuarioModifico = usuario.Id;
                    propina.TipoTarjeta = tipoTarjeta.Value;
                    propina.NumeroTarjeta = numeroTarjeta;
                    propina.Referencia = referencia;
                    propina.Valor = valorPropina;
                }
                else
                {
                    propina.Activo = false;
                }
            }
            else if (valorPropina != 0)
            {
                propina = new Propina
                {
                    Activo = true,
                    TipoPropina = Propina.TiposPropina.Valet,
                    TipoTarjeta = pago.TipoTarjeta.Value,
                    NumeroTarjeta = numeroTarjeta,
                    Referencia = referencia,
                    IdEmpleado = idEmpleado.Value,
                    Transaccion = pago.Transaccion,
                    FechaCreacion = pago.FechaCreacion,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    Valor = valorPropina
                };
            }

            if (pago.VentaRenta.IdValet != idEmpleado)
            {
                //pago.VentaRenta.FechaModificacion = fechaActual;
                //pago.VentaRenta.IdUsuarioModifico = usuario.Id;
                pago.VentaRenta.IdValet = idEmpleado;
            }
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioPagosRenta.Modificar(pago);
                RepositorioPagosRenta.GuardarCambios();

                if (propina != null)
                {
                    if (propina.Id == 0)
                        ServicioPropinas.RegistrarPropina(propina, usuario.Id);
                    else if (propina.Activo)
                        ServicioPropinas.ModificarPropina(propina, usuario.Id);
                    else
                        ServicioPropinas.EliminarPropina(propina.Id, usuario.Id);
                }

                scope.Complete();
            }
        }

        public List<DtoEnvoltorioEmpleado> ObtenerInformacionValetsPorIdVentaRenta(List<int> idsVentasRenta)
        {
            return RepositorioRentas.ObtenerInformacionValetsPorIdVentaRenta(idsVentasRenta);
        }

        public void VincularDatosFiscales(int idRenta, int? idDatosFiscales, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { VincularDatosFiscalesClientes = true });

            var renta = RepositorioRentas.Obtener(m => m.Id == idRenta);

            if (renta == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            renta.IdDatosFiscales = idDatosFiscales;

            RepositorioRentas.Modificar(renta);
            RepositorioRentas.GuardarCambios();
        }

        public DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion)
        {
            return RepositorioRentas.ObtenerDatosFiscalesPorTransaccion(transaccion);
        }

        public int ObtenerCantidadPersonasExtraUltimaRenovacion(int idRenta)
        {
            var ultimaRenovacion = RepositorioVentasRentas.ObtenerElementos(m => m.Activo && m.IdRenta == idRenta, m => m.Extensiones).SelectMany(m => m.Extensiones).Where(m => m.Activa && m.EsRenovacion).OrderByDescending(m => m.Orden).FirstOrDefault();

            return RepositorioVentasRentas.ObtenerElementos(m => m.Activo && m.IdRenta == idRenta, m => m.PersonasExtra).SelectMany(m => m.PersonasExtra).Where(m => m.Activa && (ultimaRenovacion != null ? m.FolioRenovacion == ultimaRenovacion.Folio : string.IsNullOrWhiteSpace(m.FolioRenovacion))).Count(m => m.Activa);
        }

        public List<DtoDatosPersonaExtrasRepCorteTurno> ObtienePersonasExtrasPorCorte(int folioCorte)
        {
            return RepositorioVentasRentas.ObtienePersonasExtrasPorCorte(folioCorte);
        }

        public DtoAutomovil ObtenerDatosAutomovil(string matricula)
        {
#warning validar permisos
            return RepositorioRentas.ObtenerDatosAutomovil(matricula);
        }

        #endregion
        #region métodos internal

        void IServicioRentasInterno.ValidarNoExistenPagosPendientes(int? idCorte)
        {
            var idEstadoPendiente = (int)Renta.Estados.PendienteCobro;

            if (RepositorioRentas.Alguno(m => m.IdEstado == idEstadoPendiente))
                throw new SOTException(Recursos.CortesTurno.habitaciones_pendientes_excepcion);

            if (idCorte.HasValue)
            {
                var idsEstadosComandas = new List<int>() { (int)Comanda.Estados.Cobrada, (int)Comanda.Estados.Cancelada };

                if (RepositorioComandas.Alguno(m => m.IdCorte == idCorte && !idsEstadosComandas.Contains(m.IdEstado)))
                    throw new SOTException(Recursos.CortesTurno.comandas_pendientes_excepcion);
            }
            else
            {
                //var idEstadosRenta = new List<int>() { (int)Renta.Estados.Finalizada, (int)Renta.Estados.Cancelada };

                var idsEstadosComandas = new List<int>()
                {
                    (int)Comanda.Estados.PorPagar
                    //(int)Comanda.Estados.Cobrada,
                    //(int)Comanda.Estados.Cancelada
                };

                if (RepositorioComandas.Alguno(m => /*!*/idsEstadosComandas.Contains(m.IdEstado)))
                    throw new SOTException(Recursos.CortesTurno.comandas_pendientes_excepcion);
            }
        }

        Dictionary<string, List<DetallePago>> IServicioRentasInterno.ObtenerDetallesPagosPorFechaYTipo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            return RepositorioRentas.ObtenerDetallesPagosPorFechaYTipo(fechaInicio, fechaFin);
        }

        Dictionary<TiposPago, decimal> IServicioRentasInterno.ObtenerDiccionarioPagosHabitacionPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos)
        {
            var diccionario = RepositorioRentas.ObtenerPagosHabitacionPorFecha(tiposPagos.Select(m => (int)m).ToList(), fechaInicio, fechaFin);

            if (tiposPagos.Length == 0)
                foreach (var item in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }
            else
                foreach (var item in tiposPagos)
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }

            return diccionario;
        }

        decimal IServicioRentasInterno.ObtenerMontoNoReembolsablePorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            return RepositorioRentas.ObtenerMontoNoReembolsablePorFecha(fechaInicio, fechaFin);
        }

        Dictionary<TiposTarjeta, decimal> IServicioRentasInterno.ObtenerDiccionarioPagosHabitacionTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioRentas.ObtenerPagosHabitacionTarjetaPorFecha(fechaInicio, fechaFin);

            foreach (var item in Enum.GetValues(typeof(TiposTarjeta)).Cast<TiposTarjeta>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }

        void IServicioRentasInterno.FinalizarRenta(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { DesocuparHabitaciones = true });

            var renta = RepositorioRentas.Obtener(m => m.Activa && m.IdHabitacion == idHabitacion);

            if (renta == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            if (VerificarTieneComandasPendientes(renta.Id))
                throw new SOTException(Recursos.Rentas.cuentas_pendientes_excepcion);

            var fechaActual = DateTime.Now;

            renta.Activa = false;
            renta.FechaSalida = fechaActual;
            renta.FechaModificacion = fechaActual;
            renta.IdUsuarioElimino = usuario.Id;
            renta.IdUsuarioModifico = usuario.Id;
            renta.Estado = Renta.Estados.Finalizada;

            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioRentas.Modificar(renta);
                RepositorioRentas.GuardarCambios();

                if (!string.IsNullOrEmpty(renta.NumeroTarjeta))
                {
                    ServicioVPoints.CerrarAbonosPuntos(renta.Id, renta.NumeroTarjeta, usuario);
                }

                scope.Complete();
            }
        }

        List<int> IServicioRentasInterno.CancelarRenta(int idHabitacion, DtoUsuario usuario)
        {
            string motivoCancelacion = "";

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CancelarOcupacionesHabitacion = true });

            var renta = RepositorioRentas.ObtenerRentaParaCancelar(idHabitacion);

            if (renta == null)
                throw new SOTException(Recursos.Rentas.renta_nula_excepcion);

            if (VerificarTieneComandasPendientes(renta.Id))
                throw new SOTException(Recursos.Rentas.cuentas_pendientes_excepcion);

            var fechaActual = DateTime.Now;

            renta.Activa = false;
            renta.FechaSalida = fechaActual;
            renta.FechaModificacion = fechaActual;
            renta.IdUsuarioElimino = usuario.Id;
            renta.IdUsuarioModifico = usuario.Id;
            renta.Estado = Renta.Estados.Cancelada;
            renta.MotivoCancelacion = motivoCancelacion;

            List<string> transacciones = new List<string>();
            List<int> idsVentasRentas = new List<int>();

            foreach (var auto in renta.RentaAutomoviles)
            {
                if (!auto.Activa)
                    continue;

                auto.Activa = false;
                auto.FechaEliminacion = fechaActual;
                auto.FechaModificacion = fechaActual;
                auto.IdUsuarioElimino = usuario.Id;
                auto.IdUsuarioModifico = usuario.Id;
            }

            foreach (var ventaR in renta.VentasRenta)
            {
                if (!ventaR.Activo)
                    continue;

                ventaR.Activo = false;
                ventaR.FechaEliminacion = fechaActual;
                ventaR.FechaModificacion = fechaActual;
                ventaR.IdUsuarioElimino = usuario.Id;
                ventaR.IdUsuarioModifico = usuario.Id;
                ventaR.MotivoCancelacion = motivoCancelacion;

                if (ventaR.Preventa != null)
                    ventaR.Preventa.Activa = false;

                if (!string.IsNullOrEmpty(ventaR.Transaccion))
                    transacciones.Add(ventaR.Transaccion);

                idsVentasRentas.Add(ventaR.Id);

                foreach (var detalle in ventaR.DetallesPago)
                {
                    if (!detalle.Activo)
                        continue;

                    detalle.Activo = false;
                    detalle.FechaEliminacion = fechaActual;
                    detalle.FechaModificacion = fechaActual;
                    detalle.IdUsuarioElimino = usuario.Id;
                    detalle.IdUsuarioModifico = usuario.Id;
                }

                foreach (var extension in ventaR.Extensiones)
                {
                    if (!extension.Activa)
                        continue;

                    extension.Activa = false;
                    extension.FechaEliminacion = fechaActual;
                    extension.IdUsuarioElimino = usuario.Id;
                }

                foreach (var personaExtra in ventaR.PersonasExtra)
                {
                    if (!personaExtra.Activa)
                        continue;

                    personaExtra.Activa = false;
                    personaExtra.FechaEliminacion = fechaActual;
                    personaExtra.FechaModificacion = fechaActual;
                    personaExtra.IdUsuarioElimino = usuario.Id;
                    personaExtra.IdUsuarioModifico = usuario.Id;
                }

                foreach (var paquete in ventaR.PaquetesRenta)
                {
                    if (!paquete.Activo)
                        continue;

                    paquete.Activo = false;
                    paquete.FechaEliminacion = fechaActual;
                    paquete.IdUsuarioElimino = usuario.Id;
                }

                foreach (var mnr in ventaR.MontosNoReembolsablesRenta)
                {
                    if (!mnr.Activo)
                        continue;

                    mnr.Activo = false;
                    mnr.FechaEliminacion = fechaActual;
                    mnr.IdUsuarioElimino = usuario.Id;
                }

                foreach (var pago in ventaR.Pagos)
                {
                    if (!pago.Activo)
                        continue;

                    pago.Activo = false;
                    pago.FechaEliminacion = fechaActual;
                    pago.FechaModificacion = fechaActual;
                    pago.IdUsuarioElimino = usuario.Id;
                    pago.IdUsuarioModifico = usuario.Id;
                }
            }

            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ServicioVentas.CancelarVentasPorTransacciones(transacciones, renta.NumeroServicio, usuario.Id, fechaActual, motivoCancelacion);

                RepositorioRentas.Modificar(renta);
                RepositorioRentas.GuardarCambios();

                //if (!string.IsNullOrEmpty(renta.NumeroTarjeta))
                //{
                //    ServicioVPoints.CancelarAbonosPuntos(renta.Id, renta.NumeroTarjeta, usuario.Id);
                //}

                scope.Complete();
            }

            return idsVentasRentas;
        }

        string IServicioRentasInterno.ObtenerNumeroHabitacionPorTransaccion(string numeroTransaccion)
        {
            return RepositorioVentasRentas.ObtenerNumeroHabitacionPorTransaccion(numeroTransaccion);
        }

        string IServicioRentasInterno.ObtenerNumeroHabitacionPorIdRenta(int idRenta)
        {
            return RepositorioVentasRentas.ObtenerNumeroHabitacionPorIdRenta(idRenta);
        }

        string IServicioRentasInterno.ObtenerNumeroHabitacionPorIdComanda(int idComanda)
        {
            return RepositorioVentasRentas.ObtenerNumeroHabitacionPorIdComanda(idComanda);
        }

        string IServicioRentasInterno.ObtenerNumeroHabitacionPorTransaccionRoomService(string numeroTransaccion)
        {
            return RepositorioComandas.ObtenerNumeroHabitacionPorTransaccion(numeroTransaccion);
        }

        Renta IServicioRentasInterno.ObtenerUltimaRentaPorHabitacionCargada(int idHabitacion)
        {
            return RepositorioRentas.ObtenerUltimaRentaPorHabitacionCargada(idHabitacion);
        }

        List<DetallePago> IServicioRentasInterno.ObtenerDetallesPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            return RepositorioRentas.ObtenerDetallesPagosPorFecha(fechaInicio, fechaFin);
        }

        List<PaqueteRenta> IServicioRentasInterno.ObtenerPaquetesRentaPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            return RepositorioRentas.ObtenerPaquetesRentaPorPeriodo(fechaInicio, fechaFin);
        }

        //List<Extension> IServicioRentasInterno.ObtenerHorasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        //{
        //    return RepositorioRentas.ObtenerHorasExtraPorPeriodo(fechaInicio, fechaFin);
        //}

        //List<PersonaExtra> IServicioRentasInterno.ObtenerPersonasExtraPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin)
        //{
        //    return RepositorioRentas.ObtenerPersonasExtraPorPeriodo(fechaInicio, fechaFin);
        //}

        Dictionary<TiposPago, decimal> IServicioRentasInterno.ObtenerPagosComandaPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos)
        {
            var diccionario = RepositorioComandas.ObtenerPagosComandasPorFecha(tiposPagos.Select(m => (int)m).ToList(), fechaInicio, fechaFin);

            if (tiposPagos.Length == 0)
                foreach (var item in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }
            else
                foreach (var item in tiposPagos)
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }

            return diccionario;
        }

        List<PagoRenta> IServicioRentasInterno.ObtenerPagosRentasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            return RepositorioRentas.ObtenerPagosRentasPorPeriodo(fechaInicio, fechaFin, formasPago);
        }

        #endregion métodos internal
        #region métodos private

        private bool VerificarTieneComandasPendientes(int idRenta)
        {
            var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar, (int)Comanda.Estados.PorPagar };

            return RepositorioComandas.Alguno(m => m.Activo && idsEstadoElaborada.Contains(m.IdEstado) && m.IdRenta == idRenta);
        }

        private void ValidarIVA(VentaRenta ventaRenta, ConfiguracionTipo configuracion)
        {
            var configGlobal = ServicioParametros.ObtenerParametros();

            #region validacion IVA

            var vIVA = ventaRenta.ValorSinIVA * configGlobal.Iva_CatPar;
            var vConIVA = ventaRenta.ValorSinIVA + vIVA;

            if (ventaRenta.ValorConIVA.ToString("C") != vConIVA.ToString("C") || ventaRenta.ValorIVA.ToString("C") != vIVA.ToString("C"))
                throw new SOTException(Recursos.Rentas.valores_impuestos_renta_invalidos_excepcion);

            if (ventaRenta.Extensiones.Any(m => m.Activa && !m.EsRenovacion && Math.Round(m.Precio * (1 + configGlobal.Iva_CatPar), 2) != Math.Round(configuracion.PrecioHospedajeExtra * (1 + configGlobal.Iva_CatPar), 2)))
                throw new SOTException(Recursos.Rentas.extension_tiempo_precion_invalido_excepcion, (configuracion.PrecioHospedajeExtra * (1 + configGlobal.Iva_CatPar)).ToString("C"));

            if (ventaRenta.Extensiones.Any(m => m.Activa && m.EsRenovacion && Math.Round(m.Precio * (1 + configGlobal.Iva_CatPar), 2) != Math.Round(configuracion.Precio * (1 + configGlobal.Iva_CatPar), 2)))
                throw new SOTException(Recursos.Rentas.extension_tiempo_precion_invalido_excepcion, (configuracion.Precio * (1 + configGlobal.Iva_CatPar)).ToString("C"));

            #endregion validacion IVA
        }

        private void FinalizarCreacion(Renta rentaNueva, VentaRenta ventaRenta, bool cobrar, bool tienePermisosReales, Propina propina, string numeroTransaccion, DateTime fechaActual, DtoUsuario usuario, ConsumoInternoHabitacion consumo)
        {
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            if (ventaRenta.Cobrada = cobrar)
            {
                foreach (var pago in ventaRenta.Pagos.Where(m => m.Activo))
                {
                    pago.Transaccion = numeroTransaccion;
                    pago.FechaCreacion = fechaActual;
                    pago.FechaModificacion = fechaActual;
                    pago.IdUsuarioCreo = usuario.Id;
                    pago.IdUsuarioModifico = usuario.Id;
                    if (pago.NumeroTarjeta == null)
                        pago.NumeroTarjeta = "";
                }

                //var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

                var pagoReservacion = ventaRenta.Pagos.FirstOrDefault(m => m.Activo && m.TipoPago == TiposPago.Reservacion);

                var venta = ServicioPagos.GenerarVentaNoCompletada(ventaRenta.Pagos.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.Habitacion, null, true);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    if (consumo != null)
                        ServicioConsumosInternosHabitacion.Crear(consumo, usuario.Id);

                    ServicioPagos.Pagar(venta.Id, ventaRenta.Pagos.Where(m => m.Activo), usuario);

                    RepositorioRentas.Agregar(rentaNueva);
                    RepositorioRentas.GuardarCambios();

                    ServicioHabitaciones.MarcarComoOcupada(rentaNueva.IdHabitacion, pagoReservacion == null ?
                        null : pagoReservacion.Referencia, usuario.Id);

                    if (!string.IsNullOrEmpty(rentaNueva.NumeroTarjeta))
                    {
                        decimal porcentaje = CalcularPorcentajeAbonar(ventaRenta);

                        ServicioVPoints.AbrirAbonoPuntos(rentaNueva.Id, rentaNueva.NumeroTarjeta, venta.Ticket.ToString(),
                                                             (ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Habitacion).Sum(m => m.ValorConIVA) +
                                                             ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.HorasExtra).Sum(m => m.ValorConIVA) +
                                                             ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.Renovacion).Sum(m => m.ValorConIVA)) * porcentaje,
                                                             ventaRenta.DetallesPago.Where(m => m.Activo && m.ConceptoPago == DetallePago.ConceptosPago.PersonasExtra).Sum(m => m.ValorConIVA) * porcentaje,
                                                             0, 0, 0, usuario);
                    }

                    if (propina != null)
                        ServicioPropinas.RegistrarPropina(propina, usuario.Id);

                    scope.Complete();
                }

                if (tienePermisosReales)
                    ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 2);
                else
                {
                    ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraVendedores, 2);
                    ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 1);
                }
            }
            else
            {

                var idPreventa = ServicioPreventas.GrenerarPreventa(Venta.ClasificacionesVenta.Habitacion);

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    ventaRenta.IdPreventa = idPreventa;

                    RepositorioRentas.Agregar(rentaNueva);
                    RepositorioRentas.GuardarCambios();

                    ServicioHabitaciones.MarcarComoPendienteCobro(rentaNueva.IdHabitacion, usuario.Id);

                    scope.Complete();
                }

                ServicioTickets.ImprimirTicket(ventaRenta, configuracionImpresoras.ImpresoraVendedores, 2);
                ServicioTickets.ImprimirTicket(ventaRenta, configuracionImpresoras.ImpresoraTickets, 1);
            }
        }

        private Modelo.Seguridad.Dtos.DtoPermisos ArmarPermiso(VentaRenta ventaRenta, bool cobrar)
        {
            return new Modelo.Seguridad.Dtos.DtoPermisos
            {
                AsignarHabitaciones = true,
                CobrarHabitaciones = cobrar,
                AsignarPaquetes = ventaRenta.PaquetesRenta.Any(m => m.Activo),
                CobrarPaquetes = cobrar && ventaRenta.PaquetesRenta.Any(m => m.Activo),
                AsignarPersonasExtra = ventaRenta.PersonasExtra.Any(m => m.Activa),
                CobrarPersonasExtra = cobrar && ventaRenta.PersonasExtra.Any(m => m.Activa),
                AsignarHorasExtra = ventaRenta.Extensiones.Any(m => m.Activa)//ventaRenta.TiemposExtra.Any(m => m.Activa) || ventaRenta.Renovaciones.Any(m => m.Activa)
            };
        }

        private void ProcesarDetallesYPagos(decimal precioHabitacionSinIVA, VentaRenta ventaRenta, string numeroTransaccion, DateTime fechaActual, int idUsuario)
        {
            var configGlobal = ServicioParametros.ObtenerParametros();

            List<DetallePago> detalles = new List<DetallePago>();

            decimal valorConIVA = Math.Round(precioHabitacionSinIVA * (1 + configGlobal.Iva_CatPar), 2);
            decimal valorSinIVA = valorConIVA / (1 + configGlobal.Iva_CatPar);
            decimal valorIVA = valorConIVA - valorSinIVA;

            if (precioHabitacionSinIVA > 0 && ventaRenta.EsInicial)
                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorSinIVA = valorSinIVA,//rentaNueva.PrecioHabitacion,
                    ValorIVA = valorIVA,//rentaNueva.PrecioHabitacion * configGlobal.Iva_CatPar,
                    ValorConIVA = valorConIVA,
                    ConceptoPago = DetallePago.ConceptosPago.Habitacion,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    Cantidad = 1,
                });

            if (ventaRenta.Extensiones.Any(m => m.Activa && m.EsRenovacion))
            {
                valorConIVA = ventaRenta.Extensiones.Where(m => m.Activa && m.EsRenovacion).Sum(m => Math.Round(m.Precio * (1 + configGlobal.Iva_CatPar), 2));
                valorSinIVA = valorConIVA / (1 + configGlobal.Iva_CatPar);
                valorIVA = valorConIVA - valorSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorSinIVA = valorSinIVA,//valorRenovaciones,
                    ValorIVA = valorIVA,//valorRenovaciones * configGlobal.Iva_CatPar,
                    ValorConIVA = valorConIVA,//valorRenovaciones * (1 + configGlobal.Iva_CatPar),
                    ConceptoPago = DetallePago.ConceptosPago.Renovacion,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    Cantidad = ventaRenta.Extensiones.Count(m => m.Activa && m.EsRenovacion)
                });
            }

            if (ventaRenta.Extensiones.Any(m => m.Activa && !m.EsRenovacion))
            {
                valorConIVA = ventaRenta.Extensiones.Where(m => m.Activa && !m.EsRenovacion).Sum(m => Math.Round(m.Precio * (1 + configGlobal.Iva_CatPar), 2));
                valorSinIVA = valorConIVA / (1 + configGlobal.Iva_CatPar);
                valorIVA = valorConIVA - valorSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorSinIVA = valorSinIVA,//valorTiemposExtra,
                    ValorIVA = valorIVA,//valorTiemposExtra * configGlobal.Iva_CatPar,
                    ValorConIVA = valorConIVA,//valorTiemposExtra * (1 + configGlobal.Iva_CatPar),
                    ConceptoPago = DetallePago.ConceptosPago.HorasExtra,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    Cantidad = ventaRenta.Extensiones.Count(m => m.Activa && !m.EsRenovacion)
                });
            }

            if (ventaRenta.PersonasExtra.Any(m => m.Activa))
            {
                valorConIVA = ventaRenta.PersonasExtra.Where(m => m.Activa).Sum(m => Math.Round(m.Precio * (1 + configGlobal.Iva_CatPar), 2));
                valorSinIVA = valorConIVA / (1 + configGlobal.Iva_CatPar);
                valorIVA = valorConIVA - valorSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorSinIVA = valorSinIVA,
                    ValorIVA = valorIVA,
                    ValorConIVA = valorConIVA,
                    //ValorSinIVA = valorPersonasExtra,
                    //ValorIVA = valorPersonasExtra * configGlobal.Iva_CatPar,
                    //ValorConIVA = valorPersonasExtra * (1 + configGlobal.Iva_CatPar),
                    ConceptoPago = DetallePago.ConceptosPago.PersonasExtra,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    Cantidad = ventaRenta.PersonasExtra.Count(m => m.Activa && m.Precio !=0)
                });
            }

            if (ventaRenta.PaquetesRenta.Any(m => m.Activo && m.Precio > 0))
            {
                valorConIVA = ventaRenta.PaquetesRenta.Where(m => m.Activo).Sum(m => Math.Round(m.Precio * (1 + configGlobal.Iva_CatPar), 2) * m.Cantidad);
                valorSinIVA = valorConIVA / (1 + configGlobal.Iva_CatPar);
                valorIVA = valorConIVA - valorSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorSinIVA = valorSinIVA,
                    ValorIVA = valorIVA,
                    ValorConIVA = valorConIVA,
                    //ValorSinIVA = valorPaquetes,
                    //ValorIVA = valorPaquetes * configGlobal.Iva_CatPar,
                    //ValorConIVA = valorPaquetes * (1 + configGlobal.Iva_CatPar),
                    ConceptoPago = DetallePago.ConceptosPago.Paquetes,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    Cantidad = ventaRenta.PaquetesRenta.Count(m => m.Activo && m.Precio > 0)
                });
            }

            if (ventaRenta.PaquetesRenta.Any(m => m.Activo && m.Descuento > 0))
            {
                valorConIVA = ventaRenta.PaquetesRenta.Where(m => m.Activo).Sum(m => Math.Round(m.Descuento * (1 + configGlobal.Iva_CatPar), 2) * m.Cantidad);
                valorSinIVA = valorConIVA / (1 + configGlobal.Iva_CatPar);
                valorIVA = valorConIVA - valorSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorSinIVA = valorSinIVA,
                    ValorIVA = valorIVA,
                    ValorConIVA = valorConIVA,
                    //ValorSinIVA = valorDescuentos,
                    //ValorIVA = valorDescuentos * configGlobal.Iva_CatPar,
                    //ValorConIVA = valorDescuentos * (1 + configGlobal.Iva_CatPar),
                    ConceptoPago = DetallePago.ConceptosPago.Descuentos,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    Cantidad = ventaRenta.PaquetesRenta.Count(m => m.Activo && m.Descuento > 0)
                });
            }

            if (ventaRenta.MontosNoReembolsablesRenta.Any(m => m.Activo))
            {
                valorConIVA = ventaRenta.MontosNoReembolsablesRenta.Where(m => m.Activo).Sum(m => m.ValorConIVA);
                valorSinIVA = valorConIVA / (1 + configGlobal.Iva_CatPar);
                valorIVA = valorConIVA - valorSinIVA;

                detalles.Add(new DetallePago
                {
                    Activo = true,
                    ValorSinIVA = valorSinIVA,
                    ValorIVA = valorIVA,
                    ValorConIVA = valorConIVA,
                    ConceptoPago = DetallePago.ConceptosPago.AjusteReservacion,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    Cantidad = 1
                });
            }

            var precioTotal = detalles.Sum(m => m.ConceptoPago != DetallePago.ConceptosPago.Descuentos ? m.ValorConIVA : -m.ValorConIVA);

            if (!ventaRenta.Pagos.Any(m => m.Activo))
                throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, precioTotal.ToString("C"), (0).ToString("C"));

            var valorPagos = ventaRenta.Pagos.Where(m => m.Activo).Sum(m => m.Valor);

            if (precioTotal.ToString("C") != valorPagos.ToString("C") || precioTotal.ToString("C") != ventaRenta.ValorConIVA.ToString("C"))
                throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, precioTotal.ToString("C"), valorPagos.ToString("C"));

            ventaRenta.DetallesPago.Clear();
            foreach (var detalle in detalles)
                ventaRenta.DetallesPago.Add(detalle);
        }

        public string ObtieneCodigoReservacion(DateTime fecharegistro, int idHabitacion)
        {
            return RepositorioRentas.ObtieneCodigoReservacion(fecharegistro,idHabitacion);
        }

        #endregion
    }
}