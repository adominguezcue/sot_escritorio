﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Paquetes
{
    public interface IServicioPaquetes
    {
        /// <summary>
        /// Permite crear un nuevo paquete, siempre que el usuario cuente con los permisos necesarios
        /// </summary>
        /// <param name="paquete"></param>
        /// <param name="usuario"></param>
        void CrearPaquete(Paquete paquete, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar un paquete, siempre que el usuario cuente con los permisos necesarios
        /// </summary>
        /// <param name="paquete"></param>
        /// <param name="usuario"></param>
        void ModificarPaquete(Paquete paquete, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar un paquete, siempre que el usuario cuente con los permisos necesarios
        /// </summary>
        /// <param name="idPaquete"></param>
        /// <param name="usuario"></param>
        void EliminarPaquete(int idPaquete, DtoUsuario usuario);
        /// <summary>
        /// Retrona todos los paquetes activos del sistema
        /// </summary>
        /// <returns></returns>
        List<Paquete> ObtenerPaquetesActivosConProgramas();
        /// <summary>
        /// Retorna todos los paquetes activos que pertenecen a un tipo de habitación
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <returns></returns>
        List<Paquete> ObtenerPaquetesActivosConProgramasPorTipoHabitacion(int idTipoHabitacion);
        /// <summary>
        /// Retorna true en caso de que existan paquetes activos que estén ligados al tipo de
        /// habitación con el id proporcionado
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <returns></returns>
        bool VerificarExistenPorTipo(int idTipoHabitacion);
    }
}
