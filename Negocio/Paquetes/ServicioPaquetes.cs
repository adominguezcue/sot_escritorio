﻿using Modelo;
using Modelo.Repositorios;
using Negocio.Seguridad.Permisos;
using Negocio.TiposHabitacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Paquetes
{
    public class ServicioPaquetes : IServicioPaquetes
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioTiposHabitaciones ServicioTiposHabitaciones
        {
            get { return _servicioTiposHabitaciones.Value; }
        }

        Lazy<IServicioTiposHabitaciones> _servicioTiposHabitaciones = new Lazy<IServicioTiposHabitaciones>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTiposHabitaciones>(); });

        IRepositorioPaquetes RepositorioPaquetes
        {
            get { return _repostorioPaquetes.Value; }
        }

        Lazy<IRepositorioPaquetes> _repostorioPaquetes = new Lazy<IRepositorioPaquetes>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPaquetes>(); });

        public void CrearPaquete(Paquete paquete, DtoUsuario usuario)
        {
            if (paquete == null)
                throw new SOTException(Recursos.Paquetes.paquete_nulo_exception);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearPaquetes = true });

            ServicioTiposHabitaciones.ValidarExisteActivo(paquete.IdTipoHabitacion);

            ValidarCamposObligatorios(paquete);

            if (RepositorioPaquetes.Alguno(m => m.Activo && m.Clave.Equals(paquete.Clave)))
                throw new SOTException(Recursos.Paquetes.clave_paquete_duplicada_exception, paquete.Clave);

            if (RepositorioPaquetes.Alguno(m => m.Activo && m.Nombre.Equals(paquete.Nombre) && m.IdTipoHabitacion == paquete.IdTipoHabitacion))
                throw new SOTException(Recursos.Paquetes.paquete_duplicado_tipo_exception, paquete.Nombre);

            if (paquete.Precio != 0 && paquete.Descuento != 0)
                throw new SOTException(Recursos.Paquetes.precio_y_descuento_con_valor_excepcion);

            if (paquete.Precio == 0 && paquete.Descuento == 0)
                throw new SOTException(Recursos.Paquetes.paquete_sin_valor_excepcion);

            ValidarProgramacion(paquete);

            var fechaActual = DateTime.Now;

            paquete.Activo = true;
            paquete.FechaCreacion = fechaActual;
            paquete.FechaModificacion = fechaActual;
            paquete.IdUsuarioCreo = usuario.Id;
            paquete.IdUsuarioModifico = usuario.Id;

            if (paquete.LeyendaDescuento == null)
                paquete.LeyendaDescuento = "";

            RepositorioPaquetes.Agregar(paquete);
            RepositorioPaquetes.GuardarCambios();
        }

        private void ValidarCamposObligatorios(Paquete paquete)
        {
            if (string.IsNullOrWhiteSpace(paquete.Clave))
                throw new SOTException(Recursos.Paquetes.clave_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(paquete.Nombre))
                throw new SOTException(Recursos.Paquetes.nombre_invalido_excepcion);
        }

        private void ValidarProgramacion(Paquete paquete)
        {
            if (paquete.Programado)
            {
                if (!paquete.ProgramasPaquete.Any(m => m.Activo))
                    throw new SOTException(Recursos.Paquetes.programacion_no_configurada_excepcion);

                foreach (var programacionP in paquete.ProgramasPaquete.Where(m => m.Activo))
                {
                    if (programacionP.Mes.HasValue)
                    {
                        if (programacionP.Mes != 2 && programacionP.Dia != 29)
                            try
                            {
                                new DateTime(DateTime.Now.Year, programacionP.Mes.Value, programacionP.Dia);
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                throw new SOTException(Recursos.Paquetes.fecha_programacion_invalida_exception);
                            }
                    }
                    else if (programacionP.Dia < 1 || programacionP.Dia > 7)
                        throw new SOTException(Recursos.Paquetes.dia_programacion_invalida_exception);
                }
            }
            else if (paquete.ProgramasPaquete.Any(m => m.Activo))
                throw new SOTException(Recursos.Paquetes.programacion_configurada_excepcion);
        }

        public void ModificarPaquete(Paquete paqueteM, DtoUsuario usuario)
        {
            if (paqueteM == null)
                throw new SOTException(Recursos.Paquetes.paquete_nulo_exception);

            var paquete = RepositorioPaquetes.ObtenerPaqueteActivoConProgramas(paqueteM.Id);

            if (paquete == null)
                throw new SOTException(Recursos.Paquetes.paquete_nulo_exception);

            paquete.SincronizarPrimitivas(paqueteM);

            foreach (var par in (from programaO in paquete.ProgramasPaquete
                                 join programaM in paqueteM.ProgramasPaquete.Where(m => m.Id != 0) on programaO.Id equals programaM.Id
                                 select new
                                 {
                                     programaO,
                                     programaM
                                 }))
            {
                par.programaO.SincronizarPrimitivas(par.programaM);
            }

            while (paqueteM.ProgramasPaquete.Any(m => m.Id == 0))
                paquete.ProgramasPaquete.Add(paqueteM.ProgramasPaquete.First(m => m.Id == 0));

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarPaquetes = true });

            ServicioTiposHabitaciones.ValidarExisteActivo(paquete.IdTipoHabitacion);

            ValidarCamposObligatorios(paquete);

            if (RepositorioPaquetes.Alguno(m => m.Activo && m.Id != paquete.Id && m.Clave.Equals(paquete.Clave)))
                throw new SOTException(Recursos.Paquetes.clave_paquete_duplicada_exception, paquete.Clave);

            if (RepositorioPaquetes.Alguno(m => m.Activo && m.Nombre.Equals(paquete.Nombre) && m.IdTipoHabitacion == paquete.IdTipoHabitacion && m.Id != paquete.Id))
                throw new SOTException(Recursos.Paquetes.paquete_duplicado_tipo_exception, paquete.Nombre);

            if (paquete.Precio != 0 && paquete.Descuento != 0)
                throw new SOTException(Recursos.Paquetes.precio_y_descuento_con_valor_excepcion);

            if (paquete.Precio == 0 && paquete.Descuento == 0)
                throw new SOTException(Recursos.Paquetes.paquete_sin_valor_excepcion);

            ValidarProgramacion(paquete);

            var fechaActual = DateTime.Now;
            paquete.Activo = true;
            paquete.FechaModificacion = fechaActual;
            paquete.IdUsuarioModifico = usuario.Id;

            foreach (var programa in paquete.ProgramasPaquete.Where(m => m.Activo || (!m.Activo && m.Id != 0))) 
            {
                if (programa.Id == 0)
                    programa.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Creado;
                else
                    programa.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;
            }

            if (paquete.LeyendaDescuento == null)
                paquete.LeyendaDescuento = "";

            RepositorioPaquetes.Modificar(paquete);
            RepositorioPaquetes.GuardarCambios();
        }

        public void EliminarPaquete(int idPaquete, DtoUsuario usuario)
        {
            var paquete = RepositorioPaquetes.Obtener(m => m.Id == idPaquete);

            if (paquete == null)
                throw new SOTException(Recursos.Paquetes.paquete_nulo_exception);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarPaquetes = true });

            var fechaActual = DateTime.Now;

            paquete.Activo = false;
            paquete.FechaEliminacion = fechaActual;
            paquete.FechaModificacion = fechaActual;
            paquete.IdUsuarioElimino = usuario.Id;
            paquete.IdUsuarioModifico = usuario.Id;

            RepositorioPaquetes.Modificar(paquete);
            RepositorioPaquetes.GuardarCambios();
        }

        public List<Paquete> ObtenerPaquetesActivosConProgramas() 
        {
            return RepositorioPaquetes.ObtenerPaquetesActivosConProgramas();
        }

        public List<Paquete> ObtenerPaquetesActivosConProgramasPorTipoHabitacion(int idTipoHabitacion) 
        {
            return RepositorioPaquetes.ObtenerPaquetesActivosConProgramasPorTipoHabitacion(idTipoHabitacion);
        }


        public bool VerificarExistenPorTipo(int idTipoHabitacion)
        {
            return RepositorioPaquetes.Alguno(m => m.Activo && m.IdTipoHabitacion == idTipoHabitacion);
        }
    }
}
