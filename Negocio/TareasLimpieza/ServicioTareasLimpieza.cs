﻿using Modelo;
using Modelo.Dtos;
using Modelo.Repositorios;
using Negocio.Empleados;
using Negocio.Habitaciones;
using Negocio.Seguridad.Permisos;
using Negocio.TiposHabitacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System.Linq.Expressions;
using Transversal.Extensiones;
using Negocio.ConceptosSistema;
using Negocio.CortesTurno;

namespace Negocio.TareasLimpieza
{
    public class ServicioTareasLimpieza : IServicioTareasLimpiezaInterno
    {
        IRepositorioTareasLimpieza RepositorioTareasLimpieza
        {
            get { return _repostorioTareasLimpiezaes.Value; }
        }

        Lazy<IRepositorioTareasLimpieza> _repostorioTareasLimpiezaes = new Lazy<IRepositorioTareasLimpieza>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTareasLimpieza>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });

        IServicioTiposHabitaciones ServicioTiposHabitaciones
        {
            get { return _servicioTiposHabitaciones.Value; }
        }

        Lazy<IServicioTiposHabitaciones> _servicioTiposHabitaciones = new Lazy<IServicioTiposHabitaciones>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTiposHabitaciones>(); });


        IServicioEmpleados ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleados> _servicioEmpleados = new Lazy<IServicioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>(); });

        IServicioConceptosSistema ServicioConceptosSistema
        {
            get { return _servicioConceptosSistema.Value; }
        }

        Lazy<IServicioConceptosSistema> _servicioConceptosSistema = new Lazy<IServicioConceptosSistema>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosSistema>(); });

       

        public void AsignarEmpleadosATarea(int idHabitacion, List<int> idsEmpleados, TareaLimpieza.TiposLimpieza tipoLimpieza, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AsignarRecamarerasLimpieza = true });

            var limpieza = RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);

            if (limpieza == null)
                throw new SOTException(Recursos.TareasLimpieza.tarea_limpieza_nula_exception);

            ServicioHabitaciones.ValidarEstaSucia(idHabitacion);

            if (idsEmpleados.Count == 0)
                throw new SOTException(Recursos.TareasLimpieza.recamareras_no_seleccionadas_exception);

            ServicioEmpleados.ValidarRecamarerasActivas(idsEmpleados);

            foreach (var idEmpleado in idsEmpleados)
                ServicioEmpleados.ValidarEmpleadoLibre(idEmpleado);

            var fechaActual = DateTime.Now;

            foreach (var idEmpleado in idsEmpleados) 
            {
                limpieza.LimpiezaEmpleados.Add(new LimpiezaEmpleado
                {
                    Activo = true,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdEmpleado = idEmpleado,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id
                });
            }

            var corte = ServicioCortesTurno.ObtenerUltimoCorte();

            limpieza.IdCorteTurno = corte.Id;
            limpieza.Estado = TareaLimpieza.Estados.EnProceso;
            limpieza.FechaModificacion = fechaActual;
            limpieza.FechaInicioLimpieza = fechaActual;
            limpieza.IdUsuarioModifico = usuario.Id;
            limpieza.Tipo = tipoLimpieza;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioTareasLimpieza.Modificar(limpieza);
                RepositorioTareasLimpieza.GuardarCambios();

                ServicioHabitaciones.MarcarEnLimpieza(limpieza.IdHabitacion, usuario.Id);

                scope.Complete();
            }
        }

        public void CambiarEmpleadosTarea(int idHabitacion, List<int> idsEmpleados, TareaLimpieza.TiposLimpieza tipoLimpieza, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AsignarRecamarerasLimpieza = true });

            var limpieza = RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);

            if (limpieza == null)
                throw new SOTException(Recursos.TareasLimpieza.tarea_limpieza_nula_exception);

            ServicioHabitaciones.ValidarEstaEnLimpieza(idHabitacion);

            if (idsEmpleados.Count == 0)
                throw new SOTException(Recursos.TareasLimpieza.recamareras_no_seleccionadas_exception);

            var empleadosActuales = limpieza.LimpiezaEmpleados.Where(m => m.Activo).Select(m => m.IdEmpleado)
                                    .Distinct().OrderBy(m => m).ToList();

            if (idsEmpleados.Distinct().OrderBy(m => m).SequenceEqual(empleadosActuales) && limpieza.Tipo == tipoLimpieza)
                return;

            var idsEmpleadosNuevos = idsEmpleados.Distinct().OrderBy(m => m).Except(empleadosActuales).ToList();

            ServicioEmpleados.ValidarRecamarerasActivas(idsEmpleadosNuevos);

            foreach (var idEmpleado in idsEmpleadosNuevos)
                ServicioEmpleados.ValidarEmpleadoLibre(idEmpleado);

            var fechaActual = DateTime.Now;

            foreach (var idEmpleado in idsEmpleadosNuevos)
            {
                limpieza.LimpiezaEmpleados.Add(new LimpiezaEmpleado
                {
                    Activo = true,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdEmpleado = idEmpleado,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id
                });
            }

            foreach (var empleadoLimpieza in limpieza.LimpiezaEmpleados)
            {
                if (!idsEmpleados.Contains(empleadoLimpieza.IdEmpleado))
                {
                    empleadoLimpieza.Activo = false;
                    empleadoLimpieza.FechaEliminacion = fechaActual;
                    empleadoLimpieza.FechaModificacion = fechaActual;
                    empleadoLimpieza.IdUsuarioElimino = usuario.Id;
                    empleadoLimpieza.IdUsuarioModifico = usuario.Id;
                }
            }

            //limpieza.Estado = TareaLimpieza.Estados.EnProceso;
            //limpieza.DetallesFinalizacion = string.Empty;
            limpieza.FechaModificacion = fechaActual;
            limpieza.IdUsuarioModifico = usuario.Id;
            limpieza.Tipo = tipoLimpieza;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioTareasLimpieza.Modificar(limpieza);
                RepositorioTareasLimpieza.GuardarCambios();

                //ServicioHabitaciones.MarcarEnLimpieza(limpieza.IdHabitacion, usuario);

                scope.Complete();
            }
        }

        public void AsignarSupervisorATarea(int idHabitacion, int idEmpleado, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AsignarSupervisorLimpieza = true });

            int idEstadoSupervision = (int)TareaLimpieza.Estados.EnSupervision;

            //if (RepositorioTareasLimpieza.Alguno(m => m.IdEstado == idEstadoSupervision && m.IdEmpleadoSupervisa == idEmpleado))
            //    throw new SOTException(Recursos.TareasLimpieza.supervisor_ocupado_excepcion);

            var limpieza = RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);

            if (limpieza == null)
                throw new SOTException(Recursos.TareasLimpieza.tarea_limpieza_nula_exception);

            ServicioHabitaciones.ValidarEstaEnLimpieza(idHabitacion);

            ServicioEmpleados.ValidarSupervisorActivoHabilitado(idEmpleado);

            var fechaActual = DateTime.Now;

            limpieza.IdEmpleadoSupervisa = idEmpleado;
            limpieza.IdEstado = idEstadoSupervision;
            limpieza.FechaModificacion = fechaActual;
            limpieza.FechaFinLimpieza = fechaActual;
            limpieza.FechaInicioSupervision = fechaActual;
            limpieza.IdUsuarioModifico = usuario.Id;

            foreach (var empleado in limpieza.LimpiezaEmpleados)
            {
                empleado.Activo = false;
                empleado.FechaModificacion = fechaActual;
                empleado.FechaEliminacion = fechaActual;
                empleado.IdUsuarioModifico = usuario.Id;
                empleado.IdUsuarioElimino = usuario.Id;
                empleado.TerminoLimpieza = true;
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioTareasLimpieza.Modificar(limpieza);
                RepositorioTareasLimpieza.GuardarCambios();

                ServicioHabitaciones.MarcarEnSupervision(limpieza.IdHabitacion, usuario.Id);

                scope.Complete();
            }
        }

        public void CambiarSupervisorTarea(int idHabitacion, int idEmpleado, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AsignarSupervisorLimpieza = true });

            var limpieza = RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);

            if (limpieza == null)
                throw new SOTException(Recursos.TareasLimpieza.tarea_limpieza_nula_exception);

            ServicioHabitaciones.ValidarEstaEnSupervision(idHabitacion);

            if (limpieza.IdEmpleadoSupervisa == idEmpleado)
                return;

            ServicioEmpleados.ValidarSupervisorActivoHabilitado(idEmpleado);

            var fechaActual = DateTime.Now;

            limpieza.IdEmpleadoSupervisa = idEmpleado;
            limpieza.FechaModificacion = fechaActual;
            limpieza.IdUsuarioModifico = usuario.Id;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioTareasLimpieza.Modificar(limpieza);
                RepositorioTareasLimpieza.GuardarCambios();

#warning ver si es necesario almacenar en alguna tabla los datos de los supervisores conforme sean asignados o desasignados
                //ServicioHabitaciones.MarcarEnSupervision(limpieza.IdHabitacion, usuario);

                scope.Complete();
            }
        }

        public void FinalizarLimpieza(int idHabitacion, int idMotivo, string observaciones, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { FinalizarLimpieza = true });

            var limpieza = RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);

            if (limpieza == null)
                throw new SOTException(Recursos.TareasLimpieza.tarea_limpieza_nula_exception);

            ServicioHabitaciones.ValidarEstaEnSupervision(idHabitacion);

            if (limpieza == null)
                throw new SOTException(Recursos.TareasLimpieza.tarea_limpieza_nula_exception);

            if (!ServicioConceptosSistema.VerificarConceptoPorTipo(idMotivo, ConceptoSistema.TiposConceptos.LiberacionHabitacion))
                throw new SOTException(Recursos.TareasLimpieza.motivo_liberacion_invalido_excepcion);

            var fechaActual = DateTime.Now;

            limpieza.Activa = false;
            limpieza.Estado = TareaLimpieza.Estados.Finalizada;
            limpieza.Observaciones = observaciones;
            limpieza.IdMotivo = idMotivo;
            limpieza.FechaModificacion = fechaActual;
            limpieza.FechaEliminacion = fechaActual;
            limpieza.FechaFinSupervision = fechaActual;
            limpieza.IdUsuarioModifico = usuario.Id;
            limpieza.IdUsuarioElimino = usuario.Id;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel= IsolationLevel.Snapshot }))
            {
                RepositorioTareasLimpieza.Modificar(limpieza);
                RepositorioTareasLimpieza.GuardarCambios();

                ServicioHabitaciones.HabilitarParaVentas(limpieza.IdHabitacion, usuario);

                scope.Complete();
            }
        }

        //public List<TareaLimpieza> ObtenerTareasHabitacion(int idHabitacion, DtoUsuario usuario, int pagina = 0, int elementos = 0) 
        //{
        //    ServicioPermisos.ValidarPermisos();

        //    if (pagina == 0 && elementos == 0)
        //        return RepositorioTareasLimpieza.ObtenerElementos(m => m.IdHabitacion == idHabitacion).ToList();

        //    return RepositorioTareasLimpieza.ObtenerElementos(m => m.IdHabitacion == idHabitacion, m=>m.FechaCreacion, pagina, elementos).ToList();
        //}

        public TareaLimpieza ObtenerTareaActualConEmpleados(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTareaLimpieza = true });

            ServicioHabitaciones.ValidarEstaSuciaLimpiezaSupervision(idHabitacion);

            return RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);
        }

        public DtoDetallesTarea ObtenerDetallesTarea(int idHabitacion, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTareaLimpieza = true });

            ServicioHabitaciones.ValidarEstaSuciaLimpiezaSupervision(idHabitacion);

            var limpieza = RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);

            if (limpieza == null)
                throw new SOTException(Recursos.TareasLimpieza.tarea_limpieza_nula_exception);

            TiempoLimpieza tiempoLimpieza;

            var tipoHabitacion = ServicioTiposHabitaciones.ObtenerTipoConTiemposLimpiezaPorHabitacion(idHabitacion);

            if (tipoHabitacion != null)
                tiempoLimpieza = tipoHabitacion.TiemposLimpieza.FirstOrDefault(m => m.Activa && m.TipoLimpieza == limpieza.Tipo);
            else
                tiempoLimpieza = null;

            var detalles = new DtoDetallesTarea
            {
                Estado = limpieza.Estado,
                Detalle = limpieza.Estado == TareaLimpieza.Estados.EnProceso ? 
                (limpieza.LimpiezaEmpleados.Count != 1 ? limpieza.LimpiezaEmpleados.Count + " Recamareras" : limpieza.LimpiezaEmpleados.First().Empleado.NombreCompleto):
                (limpieza.Estado == TareaLimpieza.Estados.EnSupervision ?
                limpieza.EmpleadoSupervisor.NombreCompleto : "")
            };

            switch (detalles.Estado)
            { 
                case TareaLimpieza.Estados.Creada:
                    detalles.Tiempo = DateTime.Now - limpieza.FechaCreacion;
                    detalles.EsVencida = tipoHabitacion.MinutosSucia < detalles.Tiempo.TotalMinutes;
                    break;
                case TareaLimpieza.Estados.EnProceso:
                    detalles.Tiempo = DateTime.Now - limpieza.FechaInicioLimpieza.Value;
                    detalles.EsVencida = tiempoLimpieza != null ? detalles.Tiempo > TimeSpan.FromMinutes(tiempoLimpieza.Minutos) : false;
                    break;
                case TareaLimpieza.Estados.EnSupervision:
                    detalles.Tiempo = DateTime.Now - limpieza.FechaInicioSupervision.Value;
                    break;
            }

            if (tiempoLimpieza != null &&
                (detalles.Estado == TareaLimpieza.Estados.Creada ||
                 detalles.Estado == TareaLimpieza.Estados.EnProceso))
            {
                
            }

            return detalles;
        }

        public List<DtoDetallesTareaImpuro> ObtenerDetallesTareasPorFecha(DtoUsuario usuario, int? ordenTurno = null, int? idEmpleado = null, int? idTipoHabitacion = null, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTareaLimpieza = true });

            var datos = RepositorioTareasLimpieza.ObtenerDetallesImpuros(ordenTurno, idEmpleado, idTipoHabitacion, fechaInicio, fechaFin);

            

            //var limpieza = RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);

            if (datos.Count == 0)
                return new List<DtoDetallesTareaImpuro>();

            //TiempoLimpieza tiempoLimpieza;
            //List<DtoDetallesTarea> detalles = new List<DtoDetallesTarea>();

            //foreach (var grupo in datos.GroupBy(m => m.IdHabitacion))
            //{

            //    var tipoHabitacion = ServicioTiposHabitaciones.ObtenerTipoConTiemposLimpiezaPorHabitacion(grupo.Key);

                

                //foreach (var item in grupo)
                //{

                    //var tipo = (TareaLimpieza.TiposLimpieza)item.IdTipo;
                    //var estado = (TareaLimpieza.Estados)item.IdEstado;

                    //if (tipoHabitacion != null)
                    //    tiempoLimpieza = tipoHabitacion.TiemposLimpieza.FirstOrDefault(m => m.Activa && m.TipoLimpieza == item.Tipo);
                    //else
                    //    tiempoLimpieza = null;

                    //var detalle = new DtoDetallesTarea
                    //{
                    //    Tipo = tipo,
                    //    Estado = estado,
                    //    Detalle = estado == TareaLimpieza.Estados.EnProceso ?
                    //    string.Join(", ", item.empleados) :
                    //    (estado == TareaLimpieza.Estados.EnSupervision ?
                    //    string.Join(" ", item.NombreSupervisor, item.ApellidoPSupervisor, item.ApellidoMSupervisor) : "")

                    //};

                    //switch (item.Estado)
                    //{
                    //    //case TareaLimpieza.Estados.Creada:
                    //    //    detalles.Tiempo = DateTime.Now - limpieza.FechaCreacion;
                    //    //    detalles.EsVencida = tipoHabitacion.MinutosSucia < detalles.Tiempo.TotalMinutes;
                    //    //    break;
                    //    case TareaLimpieza.Estados.EnProceso:
                    //        item.Tiempo = DateTime.Now - item.FechaInicioLimpieza.Value;
                    //        item.EsVencida = tiempoLimpieza != null ? item.Tiempo > TimeSpan.FromMinutes(tiempoLimpieza.Minutos) : false;
                    //        break;
                    //    case TareaLimpieza.Estados.EnSupervision:
                    //        item.Tiempo = DateTime.Now - item.FechaInicioSupervision.Value;
                    //        break;
                    //    case TareaLimpieza.Estados.Finalizada:
                    //        item.Tiempo = item.FechaFinSupervision.Value - item.FechaInicioSupervision.Value;
                    //        item.EsVencida = false;
                    //        break;
                    //}

                    //if (tiempoLimpieza != null &&
                    //    (detalle.Estado == TareaLimpieza.Estados.Creada ||
                    //     detalle.Estado == TareaLimpieza.Estados.EnProceso))
                    //{

                    //}
                    //detalle.NumeroHabitacion = item.Habitacion;
                    //item.EstadoStr = item.Estado.Descripcion();

                    //detalles.Add(detalle);
                //}
            //}
            return datos;
        }

        public List<LimpiezaEmpleado> ObtenerEmpleadosLimpiezaActivos(DtoUsuario usuario) 
        {
            return RepositorioTareasLimpieza.ObtenerEmpleadosLimpiezaActivos();
        }
        #region métodos internal

        TareaLimpieza IServicioTareasLimpiezaInterno.ObtenerTareaActualConEmpleados(int idHabitacion) 
        {
            ServicioHabitaciones.ValidarEstaSuciaLimpiezaSupervision(idHabitacion);

            return RepositorioTareasLimpieza.ObtenerConEmpleados(idHabitacion);
        }

        void IServicioTareasLimpiezaInterno.CrearTarea(int idHabitacion, TareaLimpieza.TiposLimpieza tipoLimpieza, int idUsuario)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConfiguracionPropinas = true });

            if (RepositorioTareasLimpieza.Alguno(m => m.Activa && m.IdHabitacion == idHabitacion))
                throw new SOTException(Recursos.TareasLimpieza.limpieza_en_curso_excepcion);

            var fechaActual = DateTime.Now;

            var tarea = new TareaLimpieza
            {
                Activa = true,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = idUsuario,
                IdUsuarioModifico = idUsuario,
                IdHabitacion = idHabitacion,
                Tipo = tipoLimpieza,
                Estado = TareaLimpieza.Estados.Creada,
                //DetallesFinalizacion = string.Empty
            };

            RepositorioTareasLimpieza.Agregar(tarea);
            RepositorioTareasLimpieza.GuardarCambios();
        }

        #endregion

    }
}
