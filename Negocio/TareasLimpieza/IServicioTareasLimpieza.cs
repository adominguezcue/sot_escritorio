﻿using Modelo;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.TareasLimpieza
{
    public interface IServicioTareasLimpieza
    {
        /// <summary>
        /// Asigna los empleados porporcionados a una tarea de limpieza, siempre y cuando sean
        /// recamareras activas, habilitadas y libres
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idsEmpleados"></param>
        /// <param name="usuario"></param>
        void AsignarEmpleadosATarea(int idHabitacion, List<int> idsEmpleados, TareaLimpieza.TiposLimpieza tipoLimpieza, DtoUsuario usuario);
        /// <summary>
        /// Modifica los empleados asignados a una tarea de limpieza, siempre y cuando sean
        /// recamareras activas, habilitadas y libres
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idsEmpleados"></param>
        /// <param name="usuario"></param>
        void CambiarEmpleadosTarea(int idHabitacion, List<int> idsEmpleados, TareaLimpieza.TiposLimpieza tipoLimpieza, DtoUsuario usuario);
        /// <summary>
        /// Asigna un supervisor a una tarea de limpieza, siempre y cuando se encuentre activo y habilitado
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idEmpleado"></param>
        /// <param name="usuario"></param>
        void AsignarSupervisorATarea(int idHabitacion, int idEmpleado, DtoUsuario usuario);
        /// <summary>
        /// Cambia un supervisor asignado a una tarea de limpieza, siempre y cuando se encuentre
        /// activo y habilitado
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idEmpleado"></param>
        /// <param name="usuario"></param>
        void CambiarSupervisorTarea(int idHabitacion, int idEmpleado, DtoUsuario usuario);
        /// <summary>
        /// Finaliza la limpieza de una habitación
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idMotivo"></param>
        /// <param name="usuario"></param>
        void FinalizarLimpieza(int idHabitacion, int idMotivo, string observaciones, DtoUsuario usuario);
        ///// <summary>
        ///// Retorna las tareas de Limpieza de una habiación desde la última hasta la primera
        ///// </summary>
        ///// <param name="idHabitacion"></param>
        ///// <param name="usuario"></param>
        ///// <param name="pagina"></param>
        ///// <param name="elementos"></param>
        ///// <returns></returns>
        //List<TareaLimpieza> ObtenerTareasHabitacion(int idHabitacion, DtoUsuario usuario, int pagina = 0, int elementos = 0);
        /// <summary>
        /// Retorna la tarea de Limpieza actual de la habitación con el id proporcionado,
        /// siempre y cuando se encuentre en estado "Limpieza"
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        TareaLimpieza ObtenerTareaActualConEmpleados(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Retorna detalles de la limpieza en curso, incluidos el tiempo que lleva, si ya está
        /// excedido, el tipo de limpieza, el estado y los empleados que participan.
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        DtoDetallesTarea ObtenerDetallesTarea(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Retorna información de las tareas de limpieza que cumplen con los filtros porporcionados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="idHabitacion"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<DtoDetallesTareaImpuro> ObtenerDetallesTareasPorFecha(DtoUsuario usuario, int? idTurno = null, int? idEmpleado = null, int? idHabitacion = null, DateTime? fechaInicio = null, DateTime? fechaFin = null);
        /// <summary>
        /// Retorna una lista con las relaciones activas entre los empleados y las tareas de limpieza
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<LimpiezaEmpleado> ObtenerEmpleadosLimpiezaActivos(DtoUsuario usuario);
    }
}
