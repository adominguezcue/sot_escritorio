﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.TareasLimpieza
{
    internal interface IServicioTareasLimpiezaInterno : IServicioTareasLimpieza
    {

        /// <summary>
        /// Permite registrar una tarea de Limpieza
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="descripcion"></param>
        /// <param name="usuario"></param>
        void CrearTarea(int idHabitacion, TareaLimpieza.TiposLimpieza tipoLimpieza, int idUsuario);
        //List<TareaLimpieza> ObtenerTareasHabitacion(int idHabitacion, DtoUsuario usuario, int pagina = 0, int elementos = 0);
        /// <summary>
        /// Retorna la tarea de Limpieza actual de la habitación con el id proporcionado,
        /// siempre y cuando se encuentre en estado "Limpieza"
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        TareaLimpieza ObtenerTareaActualConEmpleados(int idHabitacion);
    }
}
