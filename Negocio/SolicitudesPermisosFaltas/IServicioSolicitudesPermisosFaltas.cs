﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;

namespace Negocio.SolicitudesPermisosFaltas
{
    public interface IServicioSolicitudesPermisosFaltas
    {
        /// <summary>
        /// Registra una soliciud para entrar tarde o salir temprano en una fecha posterior a la actual
        /// </summary>
        /// <param name="solicitudPermiso"></param>
        /// <param name="usuario"></param>
        void SolicitarPermisoAnticipado(SolicitudPermiso solicitudPermiso, DtoUsuario usuario);
        /// <summary>
        /// Registra una soliciud para entrar tarde o salir temprano el dia actual
        /// </summary>
        /// <param name="solicitudPermiso"></param>
        /// <param name="usuario"></param>
        void SolicitarPermisoMismoDia(SolicitudPermiso solicitudPermiso, DtoUsuario usuario);
        /// <summary>
        /// Registra una solicitud para faltar
        /// </summary>
        /// <param name="solicitudFalta"></param>
        /// <param name="usuario"></param>
        void SolicitarFalta(SolicitudFalta solicitudFalta, DtoUsuario usuario);
        /// <summary>
        /// Registra una solicitud de descanso o cambio de turno
        /// </summary>
        /// <param name="solicitudCambioDescanso"></param>
        /// <param name="usuario"></param>
        void SolicitarCambioDescanso(SolicitudCambioDescanso solicitudCambioDescanso, DtoUsuario usuario);
        /// <summary>
        /// Registra una solicitud de vacaciones
        /// </summary>
        /// <param name="solicitudVacaciones"></param>
        /// <param name="usuario"></param>
        void SolicitarVacaciones(SolicitudVacaciones solicitudVacaciones, DtoUsuario usuario);
        /// <summary>
        /// Retorna las solicitudes de faltas activas que coinciden con los filtros proporcionados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<DtoSolicitudFalta> ObtenerSolicitudesFaltas(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null);
        /// <summary>
        /// Retorna las solicitudes de vacaciones activas que coinciden con los filtros proporcionados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<DtoSolicitudVacaciones> ObtenerSolicitudesVacaciones(DtoUsuario usuario, string nombre, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null);
        /// <summary>
        /// Retorna las solicitudes de permisos activas que coinciden con los filtros proporcionados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<DtoSolicitudPermiso> ObtenerSolicitudesPermiso(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null);
        /// <summary>
        /// Retorna las solicitudes de cambio de turno o de descanso activas que coinciden con los filtros proporcionados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<DtoSolicitudCambioDescanso> ObtenerSolicitudesCambioDescanso(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null);
        /// <summary>
        /// Retorna la solicitud de vacaciones activa que coincida con el id proporcionado
        /// </summary>
        /// <param name="idSolicitudVacaciones"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        SolicitudVacaciones ObtenerSolicitudVacaciones(int idSolicitudVacaciones, DtoUsuario usuario);
        /// <summary>
        /// Retorna la solicitud de permiso activa que coincida con el id proporcionado
        /// </summary>
        /// <param name="idSolicitudPermiso"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        SolicitudPermiso ObtenerSolicitudPermiso(int idSolicitudPermiso, DtoUsuario usuario);
        /// <summary>
        /// Retorna la solicitud de falta activa que coincida con el id proporcionado
        /// </summary>
        /// <param name="idSolicitudFalta"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        SolicitudFalta ObtenerSolicitudFalta(int idSolicitudFalta, DtoUsuario usuario);
        /// <summary>
        /// Retorna la solicitud de cambio de turno o descanso activa que coincida con el id proporcionado
        /// </summary>
        /// <param name="idSolicitudCambioDescanso"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        SolicitudCambioDescanso ObtenerSolicitudCambioDescanso(int idSolicitudCambioDescanso, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar una solicitud de permiso anticipado
        /// </summary>
        /// <param name="idSsolicitudPermiso"></param>
        /// <param name="fechaNueva"></param>
        /// <param name="usuario"></param>
        void ActualizarSolicitudPermisoAnticipado(int idSsolicitudPermiso, DateTime fechaNueva, string motivo, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar una solicitud de permiso de entrada tardía o salida temprano
        /// </summary>
        /// <param name="idSolicitudPermiso"></param>
        /// <param name="fechaNueva"></param>
        /// <param name="usuario"></param>
        void ActualizarSolicitudPermisoMismoDia(int idSolicitudPermiso, DateTime fechaNueva, string motivo, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar una solicitud de falta
        /// </summary>
        /// <param name="idSolicitudFalta"></param>
        /// <param name="idSuplente"></param>
        /// <param name="fechaNueva"></param>
        /// <param name="usuario"></param>
        void ActualizarSolicitudFalta(int idSolicitudFalta, int? idSuplente, DateTime fechaNueva, string motivo, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar una solicitud de descanso o cambio de turno
        /// </summary>
        /// <param name="idSolicitudCambioDescanso"></param>
        /// <param name="idSuplente"></param>
        /// <param name="fechaNueva"></param>
        /// <param name="usuario"></param>
        void ActualizarSolicitudCambioDescanso(int idSolicitudCambioDescanso, int? idSuplente, DateTime fechaNueva, bool esDescanso, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar una solicitud de vacaciones
        /// </summary>
        /// <param name="idSolicitudVacaciones"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <param name="usuario"></param>
        void ActualizarSolicitudVacaciones(int idSolicitudVacaciones, DateTime fechaInicial, DateTime fechaFinal, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar una solicitud de permiso anticipado
        /// </summary>
        /// <param name="idSsolicitudPermiso"></param>
        /// <param name="usuario"></param>
        void EliminarSolicitudPermiso(int idSsolicitudPermiso, DtoUsuario usuario);
        ///// <summary>
        ///// Permite eliminar una solicitud de permiso de entrada tarde o salida temprano el mismo dia
        ///// </summary>
        ///// <param name="idSolicitudPermiso"></param>
        ///// <param name="usuario"></param>
        //void EliminarSolicitudPermisoMismoDia(int idSolicitudPermiso, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar una solicitud de falta
        /// </summary>
        /// <param name="idSolicitudFalta"></param>
        /// <param name="usuario"></param>
        void EliminarSolicitudFalta(int idSolicitudFalta, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar una solicitud de cambio de turno o descanso
        /// </summary>
        /// <param name="idSolicitudCambioDescanso"></param>
        /// <param name="usuario"></param>
        void EliminarSolicitudCambioDescanso(int idSolicitudCambioDescanso, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar una solicitud de vacaciones
        /// </summary>
        /// <param name="idSolicitudVacaciones"></param>
        /// <param name="usuario"></param>
        void EliminarSolicitudVacaciones(int idSolicitudVacaciones, DtoUsuario usuario);
        ///// <summary>
        ///// Retorna las solicitudes de falta con vigencia dentro del período solicituado donde se
        ///// definió un suplente y éste posee el id proporcionado
        ///// </summary>
        ///// <param name="idEmpleado"></param>
        ///// <param name="fechaInicio"></param>
        ///// <param name="fechaFin"></param>
        //List<DtoSolicitudFalta> ObtenerSolicitudesFaltaSuplenciaPorFecha(int idEmpleado, DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario);
        ///// <summary>
        ///// Retorna las solicitudes de cambio de turno o descanso con vigencia dentro del período
        ///// solicituado donde se definió un suplente y éste posee el id proporcionado
        ///// </summary>
        ///// <param name="idEmpleado"></param>
        ///// <param name="fechaInicio"></param>
        ///// <param name="fechaFin"></param>
        //List<SolicitudCambioDescanso> ObtenerSolicitudesCambioDescansoSuplenciaPorFecha(int idEmpleado, DateTime fechaInicio, DateTime fechaFin);
    }
}
