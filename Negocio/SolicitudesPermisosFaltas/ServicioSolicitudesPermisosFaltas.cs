﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Empleados;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.SolicitudesPermisosFaltas
{
    public class ServicioSolicitudesPermisosFaltas : IServicioSolicitudesPermisosFaltas
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IRepositorioSolicitudesFalta RepositorioSolicitudesFalta 
        {
            get { return _repositorioSolicitudesFalta.Value; }
        }

        Lazy<IRepositorioSolicitudesFalta> _repositorioSolicitudesFalta = new Lazy<IRepositorioSolicitudesFalta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioSolicitudesFalta>(); });

        IRepositorioSolicitudesCambioDescanso RepositorioSolicitudesCambioDescanso
        {
            get { return _repositorioSolicitudesCambioDescanso.Value; }
        }

        Lazy<IRepositorioSolicitudesCambioDescanso> _repositorioSolicitudesCambioDescanso = new Lazy<IRepositorioSolicitudesCambioDescanso>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioSolicitudesCambioDescanso>(); });

        IRepositorioSolicitudesVacaciones RepositorioSolicitudesVacaciones
        {
            get { return _repositorioSolicitudesVacaciones.Value; }
        }

        Lazy<IRepositorioSolicitudesVacaciones> _repositorioSolicitudesVacaciones = new Lazy<IRepositorioSolicitudesVacaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioSolicitudesVacaciones>(); });

        IRepositorioSolicitudesPermisos RepositorioSolicitudesPermisos
        {
            get { return _repositorioSolicitudesPermisos.Value; }
        }

        Lazy<IRepositorioSolicitudesPermisos> _repositorioSolicitudesPermisos = new Lazy<IRepositorioSolicitudesPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioSolicitudesPermisos>(); });


        public void SolicitarPermisoAnticipado(SolicitudPermiso solicitudPermiso, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearSolicitudesPermisosFaltas = true });

            Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(solicitudPermiso.IdEmpleado);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaActual.Date == solicitudPermiso.FechaAplicacion.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_igual_fecha_actual);

            if (string.IsNullOrEmpty(solicitudPermiso.Motivo))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.motivo_nulo_excepcion);

            ValidarNoDuplicidad(solicitudPermiso);

            solicitudPermiso.Activa = true;
            solicitudPermiso.IdArea = empleado.Puesto.IdArea;
            solicitudPermiso.SolicitudAnticipada = true;
            solicitudPermiso.FechaCreacion = fechaActual;
            solicitudPermiso.FechaModificacion = fechaActual;
            solicitudPermiso.IdUsuarioCreo = usuario.Id;
            solicitudPermiso.IdUsuarioModifico = usuario.Id;
            if (solicitudPermiso.Motivo == null)
                solicitudPermiso.Motivo = "";

            RepositorioSolicitudesPermisos.Agregar(solicitudPermiso);
            RepositorioSolicitudesPermisos.GuardarCambios();

        }

        public void SolicitarPermisoMismoDia(SolicitudPermiso solicitudPermiso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearSolicitudesPermisosFaltas = true });

            Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(solicitudPermiso.IdEmpleado);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaActual.Date != solicitudPermiso.FechaAplicacion.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_diferente_fecha_actual);

            if (string.IsNullOrEmpty(solicitudPermiso.Motivo))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.motivo_nulo_excepcion);

            ValidarNoDuplicidad(solicitudPermiso);

            solicitudPermiso.Activa = true;
            solicitudPermiso.IdArea = empleado.Puesto.IdArea;
            solicitudPermiso.SolicitudAnticipada = false;
            solicitudPermiso.FechaCreacion = fechaActual;
            solicitudPermiso.FechaModificacion = fechaActual;
            solicitudPermiso.IdUsuarioCreo = usuario.Id;
            solicitudPermiso.IdUsuarioModifico = usuario.Id;
            if (solicitudPermiso.Motivo == null)
                solicitudPermiso.Motivo = "";

            RepositorioSolicitudesPermisos.Agregar(solicitudPermiso);
            RepositorioSolicitudesPermisos.GuardarCambios();

        }

        public void SolicitarFalta(SolicitudFalta solicitudFalta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearSolicitudesPermisosFaltas = true });

            Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(solicitudFalta.IdEmpleado);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaActual.Date >= solicitudFalta.FechaAplicacion.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_menor_igual_fecha_actual);

            if (string.IsNullOrEmpty(solicitudFalta.Motivo))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.motivo_nulo_excepcion);

            if (solicitudFalta.TipoPago == 0)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.forma_pago_falta_no_seleccionada_excepcion);

            if (solicitudFalta.IdEmpleado == solicitudFalta.IdEmpleadoSuplente)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_igual_empleado_excepcion);

            if (solicitudFalta.IdEmpleadoSuplente.HasValue) 
            {
                var suplente = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(solicitudFalta.IdEmpleadoSuplente.Value);

                if (suplente == null)
                    throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_nulo_excepcion);
            }

            ValidarNoDuplicidad(solicitudFalta);

            solicitudFalta.Activa = true;
            solicitudFalta.IdArea = empleado.Puesto.IdArea;
            solicitudFalta.FechaCreacion = fechaActual;
            solicitudFalta.FechaModificacion = fechaActual;
            solicitudFalta.IdUsuarioCreo = usuario.Id;
            solicitudFalta.IdUsuarioModifico = usuario.Id;
            if (solicitudFalta.Motivo == null)
                solicitudFalta.Motivo = "";

            RepositorioSolicitudesFalta.Agregar(solicitudFalta);
            RepositorioSolicitudesFalta.GuardarCambios();

        }

        public void SolicitarCambioDescanso(SolicitudCambioDescanso solicitudCambioDescanso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearSolicitudesPermisosFaltas = true });

            Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(solicitudCambioDescanso.IdEmpleado);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaActual.Date >= solicitudCambioDescanso.FechaAplicacion.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_menor_igual_fecha_actual);

            if (solicitudCambioDescanso.IdEmpleado == solicitudCambioDescanso.IdEmpleadoSuplente)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_igual_empleado_excepcion);

            if (solicitudCambioDescanso.IdEmpleadoSuplente.HasValue)
            {
                var suplente = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(solicitudCambioDescanso.IdEmpleadoSuplente.Value);

                if (suplente == null)
                    throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_nulo_excepcion);
            }

            ValidarNoDuplicidad(solicitudCambioDescanso);

            solicitudCambioDescanso.Activa = true;
            solicitudCambioDescanso.IdArea = empleado.Puesto.IdArea;
            solicitudCambioDescanso.FechaCreacion = fechaActual;
            solicitudCambioDescanso.FechaModificacion = fechaActual;
            solicitudCambioDescanso.IdUsuarioCreo = usuario.Id;
            solicitudCambioDescanso.IdUsuarioModifico = usuario.Id;

            RepositorioSolicitudesCambioDescanso.Agregar(solicitudCambioDescanso);
            RepositorioSolicitudesCambioDescanso.GuardarCambios();

        }

        public void SolicitarVacaciones(SolicitudVacaciones solicitudVacaciones, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearSolicitudesPermisosFaltas = true });

            Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(solicitudVacaciones.IdEmpleado);

            if (empleado == null)
                throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (solicitudVacaciones.FechaFin <= solicitudVacaciones.FechaInicio)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_inicio_vacaciones_mayor_o_igual_final_excepcion);

            if (fechaActual.Date >= solicitudVacaciones.FechaInicio || fechaActual.Date >= solicitudVacaciones.FechaFin)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_inicio_o_fin_menor_igual_fecha_actual);

            ValidarNoDuplicidad(solicitudVacaciones);

            solicitudVacaciones.Activa = true;
            solicitudVacaciones.IdArea = empleado.Puesto.IdArea;
            solicitudVacaciones.FechaCreacion = fechaActual;
            solicitudVacaciones.FechaModificacion = fechaActual;
            solicitudVacaciones.IdUsuarioCreo = usuario.Id;
            solicitudVacaciones.IdUsuarioModifico = usuario.Id;

            RepositorioSolicitudesVacaciones.Agregar(solicitudVacaciones);
            RepositorioSolicitudesVacaciones.GuardarCambios();

        }

        private void ValidarNoDuplicidad(SolicitudVacaciones solicitudVacaciones)
        {
            if (RepositorioSolicitudesVacaciones.Alguno(m => m.Id != solicitudVacaciones.Id && m.Activa && m.IdEmpleado == solicitudVacaciones.IdEmpleado
                                                         && ((m.FechaInicio <= solicitudVacaciones.FechaInicio && m.FechaFin >= solicitudVacaciones.FechaInicio && m.FechaFin <= solicitudVacaciones.FechaFin) ||
                                                             (m.FechaInicio >= solicitudVacaciones.FechaInicio && m.FechaInicio <= solicitudVacaciones.FechaFin) ||
                                                             (m.FechaInicio <= solicitudVacaciones.FechaInicio && m.FechaFin >= solicitudVacaciones.FechaFin) ||
                                                             (m.FechaInicio >= solicitudVacaciones.FechaInicio && m.FechaFin <= solicitudVacaciones.FechaFin))))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.vacaciones_rango_similar_excepcion);

            var sPermiso = RepositorioSolicitudesPermisos.Obtener(m => m.Activa && m.IdEmpleado == solicitudVacaciones.IdEmpleado
                                                         && solicitudVacaciones.FechaInicio <= m.FechaAplicacion
                                                         && solicitudVacaciones.FechaFin >= m.FechaAplicacion);

            if(sPermiso != null)
            throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_dentro_de_periodo_excepcion, sPermiso.FechaAplicacion.ToString("dd/MM/yyyy"));

            var sFalta = RepositorioSolicitudesFalta.Obtener(m => m.Activa && m.IdEmpleado == solicitudVacaciones.IdEmpleado
                                                         && solicitudVacaciones.FechaInicio <= m.FechaAplicacion
                                                         && solicitudVacaciones.FechaFin >= m.FechaAplicacion);

            if (sFalta != null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_dentro_de_periodo_excepcion, sFalta.FechaAplicacion.ToString("dd/MM/yyyy"));

            var sCambio= RepositorioSolicitudesCambioDescanso.Obtener(m => m.Activa && m.IdEmpleado == solicitudVacaciones.IdEmpleado
                                                         && solicitudVacaciones.FechaInicio <= m.FechaAplicacion
                                                         && solicitudVacaciones.FechaFin >= m.FechaAplicacion);

            if (sCambio != null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_dentro_de_periodo_excepcion, sCambio.FechaAplicacion.ToString("dd/MM/yyyy"));
        }

        private void ValidarNoDuplicidad(SolicitudCambioDescanso solicitudCambioDescanso)
        {
            if (RepositorioSolicitudesPermisos.Alguno(m => m.Activa && m.IdEmpleado == solicitudCambioDescanso.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudCambioDescanso.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudCambioDescanso.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudCambioDescanso.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            if (RepositorioSolicitudesFalta.Alguno(m => m.Activa && m.IdEmpleado == solicitudCambioDescanso.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudCambioDescanso.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudCambioDescanso.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudCambioDescanso.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            if (RepositorioSolicitudesCambioDescanso.Alguno(m => m.Id != solicitudCambioDescanso.Id && m.Activa && m.IdEmpleado == solicitudCambioDescanso.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudCambioDescanso.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudCambioDescanso.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudCambioDescanso.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            var sVacaciones = RepositorioSolicitudesVacaciones.Obtener(m => m.Activa && m.IdEmpleado == solicitudCambioDescanso.IdEmpleado
                                                         && m.FechaInicio <= solicitudCambioDescanso.FechaAplicacion
                                                         && m.FechaFin >= solicitudCambioDescanso.FechaAplicacion);

            if (sVacaciones != null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_vacaciones_incluye_fecha_excepcion, sVacaciones.FechaInicio.ToString("dd/MM/yyyy"), sVacaciones.FechaFin.ToString("dd/MM/yyyy"));
        }
        private void ValidarNoDuplicidad(SolicitudFalta solicitudFalta)
        {
            if (RepositorioSolicitudesPermisos.Alguno(m => m.Activa && m.IdEmpleado == solicitudFalta.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudFalta.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudFalta.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudFalta.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            if (RepositorioSolicitudesFalta.Alguno(m => m.Id != solicitudFalta.Id && m.Activa && m.IdEmpleado == solicitudFalta.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudFalta.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudFalta.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudFalta.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            if (RepositorioSolicitudesCambioDescanso.Alguno(m => m.Activa && m.IdEmpleado == solicitudFalta.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudFalta.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudFalta.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudFalta.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            var sVacaciones = RepositorioSolicitudesVacaciones.Obtener(m => m.Activa && m.IdEmpleado == solicitudFalta.IdEmpleado
                                                         && m.FechaInicio <= solicitudFalta.FechaAplicacion
                                                         && m.FechaFin >= solicitudFalta.FechaAplicacion);

            if (sVacaciones != null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_vacaciones_incluye_fecha_excepcion, sVacaciones.FechaInicio.ToString("dd/MM/yyyy"), sVacaciones.FechaFin.ToString("dd/MM/yyyy"));
        }
        
        private void ValidarNoDuplicidad(SolicitudPermiso solicitudPermiso)
        {
            if (RepositorioSolicitudesPermisos.Alguno(m => m.Id != solicitudPermiso.Id && m.Activa && m.IdEmpleado == solicitudPermiso.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudPermiso.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudPermiso.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudPermiso.FechaAplicacion.Day
                                                         && m.EsSalida == solicitudPermiso.EsSalida))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            if (RepositorioSolicitudesFalta.Alguno(m => m.Activa && m.IdEmpleado == solicitudPermiso.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudPermiso.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudPermiso.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudPermiso.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            if (RepositorioSolicitudesCambioDescanso.Alguno(m => m.Activa && m.IdEmpleado == solicitudPermiso.IdEmpleado
                                                         && m.FechaAplicacion.Year == solicitudPermiso.FechaAplicacion.Year
                                                         && m.FechaAplicacion.Month == solicitudPermiso.FechaAplicacion.Month
                                                         && m.FechaAplicacion.Day == solicitudPermiso.FechaAplicacion.Day))
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_permiso_duplicada_excepcion);

            var sVacaciones = RepositorioSolicitudesVacaciones.Obtener(m => m.Activa && m.IdEmpleado == solicitudPermiso.IdEmpleado
                                                         && m.FechaInicio <= solicitudPermiso.FechaAplicacion
                                                         && m.FechaFin >= solicitudPermiso.FechaAplicacion);

            if (sVacaciones != null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_vacaciones_incluye_fecha_excepcion, sVacaciones.FechaInicio.ToString("dd/MM/yyyy"), sVacaciones.FechaFin.ToString("dd/MM/yyyy"));
        }

        public List<DtoSolicitudFalta> ObtenerSolicitudesFaltas(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesFalta.ObtenerSolicitudesFaltas(nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public List<DtoSolicitudVacaciones> ObtenerSolicitudesVacaciones(DtoUsuario usuario, string nombre, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesVacaciones.ObtenerSolicitudesVacaciones(nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public List<DtoSolicitudPermiso> ObtenerSolicitudesPermiso(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesPermisos.ObtenerSolicitudesPermiso(nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public List<DtoSolicitudCambioDescanso> ObtenerSolicitudesCambioDescanso(DtoUsuario usuario, string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesCambioDescanso.ObtenerSolicitudesCambioDescanso(nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public SolicitudVacaciones ObtenerSolicitudVacaciones(int idSolicitudVacaciones, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesVacaciones.Obtener(m => m.Activa && m.Id == idSolicitudVacaciones);
        }

        public SolicitudPermiso ObtenerSolicitudPermiso(int idSolicitudPermiso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesPermisos.Obtener(m => m.Activa && m.Id == idSolicitudPermiso);
        }

        public SolicitudFalta ObtenerSolicitudFalta(int idSolicitudFalta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesFalta.Obtener(m => m.Activa && m.Id == idSolicitudFalta);
        }

        public SolicitudCambioDescanso ObtenerSolicitudCambioDescanso(int idSolicitudCambioDescanso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarSolicitudesPermisosFaltas = true });

            return RepositorioSolicitudesCambioDescanso.Obtener(m => m.Activa && m.Id == idSolicitudCambioDescanso);
        }

        public void ActualizarSolicitudPermisoAnticipado(int idSsolicitudPermiso, DateTime fechaNueva, string motivo, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarSolicitudesPermisosFaltas = true });

            var solicitudPermiso = RepositorioSolicitudesPermisos.Obtener(m => m.Activa && m.Id == idSsolicitudPermiso);

            if (solicitudPermiso == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudPermiso.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaActual.Date == fechaNueva.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_igual_fecha_actual);

            solicitudPermiso.FechaAplicacion = fechaNueva;
            solicitudPermiso.Motivo = motivo ?? "";

            ValidarNoDuplicidad(solicitudPermiso);

            solicitudPermiso.FechaModificacion = fechaActual;
            solicitudPermiso.IdUsuarioModifico = usuario.Id;

            RepositorioSolicitudesPermisos.Modificar(solicitudPermiso);
            RepositorioSolicitudesPermisos.GuardarCambios();

        }

        public void ActualizarSolicitudPermisoMismoDia(int idSolicitudPermiso, DateTime fechaNueva, string motivo, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarSolicitudesPermisosFaltas = true });

            var solicitudPermiso = RepositorioSolicitudesPermisos.Obtener(m => m.Activa && m.Id == idSolicitudPermiso);

            if (solicitudPermiso == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudPermiso.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (solicitudPermiso.FechaAplicacion.Date != fechaNueva.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_diferente_fecha_solicitud);

            solicitudPermiso.FechaAplicacion = fechaNueva;
            solicitudPermiso.Motivo = motivo ?? "";
            
            ValidarNoDuplicidad(solicitudPermiso);

            solicitudPermiso.FechaModificacion = fechaActual;
            solicitudPermiso.IdUsuarioModifico = usuario.Id;

            RepositorioSolicitudesPermisos.Modificar(solicitudPermiso);
            RepositorioSolicitudesPermisos.GuardarCambios();

        }

        public void ActualizarSolicitudFalta(int idSolicitudFalta, int? idSuplente, DateTime fechaNueva, string motivo, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarSolicitudesPermisosFaltas = true });

            var solicitudFalta = RepositorioSolicitudesFalta.Obtener(m=>m.Activa && m.Id == idSolicitudFalta);

            if(solicitudFalta == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudFalta.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaActual.Date >= fechaNueva.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_menor_igual_fecha_actual);

            if (solicitudFalta.IdEmpleado == idSuplente)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_igual_empleado_excepcion);

            if (idSuplente.HasValue)
            {
                var suplente = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(idSuplente.Value);

                if (suplente == null)
                    throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_nulo_excepcion);
            }
            
            solicitudFalta.IdEmpleadoSuplente = idSuplente;
            solicitudFalta.FechaAplicacion = fechaNueva;
            solicitudFalta.Motivo = motivo ?? "";

            ValidarNoDuplicidad(solicitudFalta);

            solicitudFalta.FechaModificacion = fechaActual;
            solicitudFalta.IdUsuarioModifico = usuario.Id;

            RepositorioSolicitudesFalta.Modificar(solicitudFalta);
            RepositorioSolicitudesFalta.GuardarCambios();

        }

        public void ActualizarSolicitudCambioDescanso(int idSolicitudCambioDescanso, int? idSuplente, DateTime fechaNueva, bool esDescanso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarSolicitudesPermisosFaltas = true });

            var solicitudCambioDescanso = RepositorioSolicitudesCambioDescanso.Obtener(m => m.Activa && m.Id == idSolicitudCambioDescanso);

            if (solicitudCambioDescanso == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudCambioDescanso.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaActual.Date >= fechaNueva.Date)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_menor_igual_fecha_actual);

            if (solicitudCambioDescanso.IdEmpleado == idSuplente)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_igual_empleado_excepcion);

            if (idSuplente.HasValue)
            {
                var suplente = ServicioEmpleados.ObtenerEmpleadoActivoConPuestoPorId(idSuplente.Value);

                if (suplente == null)
                    throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_nulo_excepcion);
            }

            solicitudCambioDescanso.FechaAplicacion = fechaNueva;
            solicitudCambioDescanso.IdEmpleadoSuplente = idSuplente;
            solicitudCambioDescanso.EsDescanso = esDescanso;

            ValidarNoDuplicidad(solicitudCambioDescanso);

            solicitudCambioDescanso.FechaModificacion = fechaActual;
            solicitudCambioDescanso.IdUsuarioModifico = usuario.Id;

            RepositorioSolicitudesCambioDescanso.Modificar(solicitudCambioDescanso);
            RepositorioSolicitudesCambioDescanso.GuardarCambios();

        }


        public void ActualizarSolicitudVacaciones(int idSolicitudVacaciones, DateTime fechaInicial, DateTime fechaFinal, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarSolicitudesPermisosFaltas = true });

            var solicitudVacaciones = RepositorioSolicitudesVacaciones.Obtener(m => m.Activa && m.Id == idSolicitudVacaciones);

            if (solicitudVacaciones == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //if (!solicitudVacaciones.Activa)
            //    throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_no_eliminable_modulo_actual_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudVacaciones.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            if (fechaFinal <= fechaInicial)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_inicio_vacaciones_mayor_o_igual_final_excepcion);

            if (fechaActual.Date >= fechaInicial || fechaActual.Date >= fechaFinal)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_inicio_o_fin_menor_igual_fecha_actual);

            solicitudVacaciones.FechaInicio = fechaInicial;
            solicitudVacaciones.FechaFin = fechaFinal;

            ValidarNoDuplicidad(solicitudVacaciones);

            solicitudVacaciones.Activa = true;
            //solicitudVacaciones.IdPuesto = empleado.IdPuesto;
            solicitudVacaciones.FechaModificacion = fechaActual;
            solicitudVacaciones.IdUsuarioModifico = usuario.Id;

            RepositorioSolicitudesVacaciones.Modificar(solicitudVacaciones);
            RepositorioSolicitudesVacaciones.GuardarCambios();

        }

        public void EliminarSolicitudPermiso(int idSsolicitudPermiso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarSolicitudesPermisosFaltas = true });

            var solicitudPermiso = RepositorioSolicitudesPermisos.Obtener(m => m.Activa && m.Id == idSsolicitudPermiso);

            if (solicitudPermiso == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudPermiso.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            solicitudPermiso.Activa = false;
            solicitudPermiso.FechaModificacion = fechaActual;
            solicitudPermiso.IdUsuarioModifico = usuario.Id;
            solicitudPermiso.FechaEliminacion = fechaActual;
            solicitudPermiso.IdUsuarioElimino = usuario.Id;

            RepositorioSolicitudesPermisos.Modificar(solicitudPermiso);
            RepositorioSolicitudesPermisos.GuardarCambios();

        }

        //public void EliminarSolicitudPermisoMismoDia(int idSolicitudPermiso, DtoUsuario usuario)
        //{
        //    if (usuario == null)
        //        throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

        //    ServicioPermisos.ValidarPermisos();

        //    var solicitudPermiso = RepositorioSolicitudesPermisos.Obtener(m => m.Activa && m.Id == idSolicitudPermiso);

        //    if (solicitudPermiso == null)
        //        throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

        //    //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudPermiso.IdEmpleado, usuario);

        //    //if (empleado == null)
        //    //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

        //    var fechaActual = DateTime.Now;

        //    solicitudPermiso.Activa = false;
        //    solicitudPermiso.FechaModificacion = fechaActual;
        //    solicitudPermiso.IdUsuarioModifico = usuario.Id;
        //    solicitudPermiso.FechaEliminacion = fechaActual;
        //    solicitudPermiso.IdUsuarioElimino = usuario.Id;

        //    RepositorioSolicitudesPermisos.Modificar(solicitudPermiso);
        //    RepositorioSolicitudesPermisos.GuardarCambios();

        //}

        public void EliminarSolicitudFalta(int idSolicitudFalta, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarSolicitudesPermisosFaltas = true });

            var solicitudFalta = RepositorioSolicitudesFalta.Obtener(m => m.Activa && m.Id == idSolicitudFalta);

            if (solicitudFalta == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudFalta.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            solicitudFalta.Activa = false;
            solicitudFalta.FechaModificacion = fechaActual;
            solicitudFalta.IdUsuarioModifico = usuario.Id;
            solicitudFalta.FechaEliminacion = fechaActual;
            solicitudFalta.IdUsuarioElimino = usuario.Id;

            RepositorioSolicitudesFalta.Modificar(solicitudFalta);
            RepositorioSolicitudesFalta.GuardarCambios();

        }

        public void EliminarSolicitudCambioDescanso(int idSolicitudCambioDescanso, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarSolicitudesPermisosFaltas = true });

            var solicitudCambioDescanso = RepositorioSolicitudesCambioDescanso.Obtener(m => m.Activa && m.Id == idSolicitudCambioDescanso);

            if (solicitudCambioDescanso == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            //Empleado empleado = ServicioEmpleados.ObtenerEmpleadoActivoPorId(solicitudCambioDescanso.IdEmpleado, usuario);

            //if (empleado == null)
            //    throw new SOTException(Recursos.Empleados.empleado_nulo_o_eliminado_excepcion);

            var fechaActual = DateTime.Now;

            //if (fechaActual.Date >= fechaNueva.Date)
            //    throw new SOTException(Recursos.SolicitudesPermisosFaltas.fecha_solicitada_menor_igual_fecha_actual);

            //if (idSuplente.HasValue)
            //{
            //    var suplente = ServicioEmpleados.ObtenerEmpleadoActivoPorId(idSuplente.Value, usuario);

            //    if (suplente == null)
            //        throw new SOTException(Recursos.SolicitudesPermisosFaltas.suplente_nulo_excepcion);
            //}

            //solicitudCambioDescanso.FechaAplicacion = fechaNueva;
            //solicitudCambioDescanso.IdEmpleadoSuplente = idSuplente;

            //ValidarNoDuplicidad(solicitudCambioDescanso);

            solicitudCambioDescanso.Activa = false;
            solicitudCambioDescanso.FechaModificacion = fechaActual;
            solicitudCambioDescanso.IdUsuarioModifico = usuario.Id;
            solicitudCambioDescanso.FechaEliminacion = fechaActual;
            solicitudCambioDescanso.IdUsuarioElimino = usuario.Id;

            RepositorioSolicitudesCambioDescanso.Modificar(solicitudCambioDescanso);
            RepositorioSolicitudesCambioDescanso.GuardarCambios();

        }

        public void EliminarSolicitudVacaciones(int idSolicitudVacaciones, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarSolicitudesPermisosFaltas = true });

            var solicitudVacaciones = RepositorioSolicitudesVacaciones.Obtener(m => m.Activa && m.Id == idSolicitudVacaciones);

            if (solicitudVacaciones == null)
                throw new SOTException(Recursos.SolicitudesPermisosFaltas.solicitud_nula_o_eliminada_excepcion);

            var fechaActual = DateTime.Now;

            solicitudVacaciones.Activa = false;
            solicitudVacaciones.FechaModificacion = fechaActual;
            solicitudVacaciones.FechaEliminacion = fechaActual;
            solicitudVacaciones.IdUsuarioModifico = usuario.Id;
            solicitudVacaciones.IdUsuarioElimino = usuario.Id;

            RepositorioSolicitudesVacaciones.Modificar(solicitudVacaciones);
            RepositorioSolicitudesVacaciones.GuardarCambios();

        }

        //public List<DtoSolicitudFalta> ObtenerSolicitudesFaltaSuplenciaPorFecha(int idEmpleado, DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos();

        //    return RepositorioSolicitudesFalta.ObtenerSolicitudesFaltaSuplenciaPorFecha(idEmpleado, fechaInicio, fechaFin);
        //}

        //public List<SolicitudCambioDescanso> ObtenerSolicitudesCambioDescansoSuplenciaPorFecha(int idEmpleado, DateTime fechaInicio, DateTime fechaFin)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
