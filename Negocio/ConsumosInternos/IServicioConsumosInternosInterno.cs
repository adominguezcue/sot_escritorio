﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConsumosInternos
{
    internal interface IServicioConsumosInternosInterno : IServicioConsumosInternos
    {
        decimal ObtenerPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        ///// <summary>
        ///// Permite registrar información de un nuevo consumo interno en la base de datos
        ///// </summary>
        ///// <param name="consumo"></param>
        //void Crear(Modelo.Entidades.ConsumoInterno consumo, int idUsuario);

        //void EliminarConsumo(int idConsumoInterno, int idUsuario);

        int ObtenerUltimoNumeroConsumoInterno(int idTurno);

        void ValidarNoExistenPagosPendientes(int? idCorte);
        Dictionary<TiposPago, decimal> ObtenerDiccionarioPagosConsumoInternoPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos);
        List<ConsumoInterno> ObtenerConsumosInternosCargadosPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);

        /// <summary>
        /// Retorna el valor de los artículos de comandas cobradas dentro del período especificado,
        /// divididos por tipo de artículo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<DtoEmpleadoLineaArticuloValor> ObtenerValorArticulosConsumosPorPeriodoYLineas(List<int> idsTipos, List<int> idsCortes);

        bool VerificarNoExistenDependenciasArticulo(string codigoArticulo);
    }
}
