﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConsumosInternos
{
    public interface IServicioConsumosInternosHabitacion
    {
        /// <summary>
        /// Verifica si el guid pertenece a un consumo interno registrado en la base de datos
        /// </summary>
        /// <param name="guidConsumo"></param>
        bool VerificarGuid(string guidConsumo);
    }
}
