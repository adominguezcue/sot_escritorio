﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Constantes;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Inventarios;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesGlobales;
using Negocio.ConfiguracionesImpresoras;
using Negocio.CortesTurno;
using Negocio.Empleados;
using Negocio.Pagos;
using Negocio.Preventas;
using Negocio.Rentas;
using Negocio.Restaurantes;
using Negocio.RoomServices;
using Negocio.Seguridad.Permisos;
using Negocio.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.ConsumosInternos
{
    public class ServicioConsumosInternos : IServicioConsumosInternosInterno
    {
        private static object bloqueadorProductos = new object();

        #region dependencias

        IServicioRoomServicesInterno ServicioRoomServices
        {
            get { return _servicioRoomServices.Value; }
        }

        Lazy<IServicioRoomServicesInterno> _servicioRoomServices = new Lazy<IServicioRoomServicesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRoomServicesInterno>(); });

        IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });


        IServicioInventarios ServicioInventarios
        {
            get { return _servicioInventarios.Value; }
        }

        Lazy<IServicioInventarios> _servicioInventarios = new Lazy<IServicioInventarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioInventarios>(); });


        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioPagos ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagos> _servicioPagos = new Lazy<IServicioPagos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagos>(); });

        IRepositorioConsumosInternos RepositorioConsumosInternos
        {
            get { return _RepositorioConsumosInternos.Value; }
        }

        Lazy<IRepositorioConsumosInternos> _RepositorioConsumosInternos = new Lazy<IRepositorioConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConsumosInternos>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioConfiguracionesImpresorasInterno ServicioConfiguracionesImpresoras
        {
            get { return _servicioConfiguracionesImpresoras.Value; }
        }

        Lazy<IServicioConfiguracionesImpresorasInterno> _servicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresorasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresorasInterno>(); });

        IServicioTicketsInterno ServicioTickets
        {
            get { return _servicioTickets.Value; }
        }

        Lazy<IServicioTicketsInterno> _servicioTickets = new Lazy<IServicioTicketsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTicketsInterno>(); });

        IServicioRestaurantes ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantes> _servicioRestaurantes = new Lazy<IServicioRestaurantes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantes>(); });

        #endregion
        #region métodos internal
        decimal IServicioConsumosInternosInterno.ObtenerPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            return RepositorioConsumosInternos.ObtenerPagosPorFecha(fechaInicio, fechaFin);
        }
        List<DtoEmpleadoLineaArticuloValor> IServicioConsumosInternosInterno.ObtenerValorArticulosConsumosPorPeriodoYLineas(List<int> idsTipos, List<int> idsCortes)
        {
            var articulos = ServicioArticulos.ObtenerIdsArticulosPorLineas(idsTipos);

            return RepositorioConsumosInternos.ObtenerArticulosPorEmpleado(articulos, idsCortes);
        }

        bool IServicioConsumosInternosInterno.VerificarNoExistenDependenciasArticulo(string codigoArticulo)
        {
            return RepositorioConsumosInternos.ObtenerCantidadArticulosConsumosInternosPorCodigoArticulo(codigoArticulo) == 0;
        }

        List<ConsumoInterno> IServicioConsumosInternosInterno.ObtenerConsumosInternosCargadosPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            return RepositorioConsumosInternos.ObtenerConsumosInternosCargadosPorPeriodo(fechaInicio, fechaFin, formasPago);
        }

        Dictionary<TiposPago, decimal> IServicioConsumosInternosInterno.ObtenerDiccionarioPagosConsumoInternoPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos)
        {
            var diccionario = RepositorioConsumosInternos.ObtenerPagosConsumoInternoPorFecha(tiposPagos.Select(m => (int)m).ToList(), fechaInicio, fechaFin);

            if (tiposPagos.Length == 0)
                foreach (var item in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }
            else
                foreach (var item in tiposPagos)
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }

            return diccionario;
        }

        void IServicioConsumosInternosInterno.ValidarNoExistenPagosPendientes(int? idCorte)
        {
            if (idCorte.HasValue)
            {
                var idsEstadosComandas = new List<int>() { (int)Comanda.Estados.Cobrada, (int)Comanda.Estados.Cancelada};


                if (RepositorioConsumosInternos.Alguno(m => m.IdCorteTurno == idCorte && !idsEstadosComandas.Contains(m.IdEstado)))
                    throw new SOTException(Recursos.CortesTurno.consumos_internos_pendientes_excepcion);

                List<int> idestadoscomandas1 = new List<int>() { (int)Comanda.Estados.PorCobrar};
                 bool sihayconsumosinternosporcobrar = RepositorioConsumosInternos.ExisteDeVerdadConsumosInternos(idestadoscomandas1);
                if (sihayconsumosinternosporcobrar)
                    throw new SOTException(Recursos.CortesTurno.consumos_internos_pendientes_excepcion);
            }
            else
            {
                var idsEstadosComandas = new List<int>()
                {
                    (int)Comanda.Estados.PorPagar
                    //(int)Comanda.Estados.Cobrada, 
                    //(int)Comanda.Estados.Cancelada 
                };

                if (RepositorioConsumosInternos.Alguno(m => /*!*/idsEstadosComandas.Contains(m.IdEstado)))
                    throw new SOTException(Recursos.CortesTurno.consumos_internos_pendientes_excepcion);
            }
        }

        //void IServicioConsumosInternosInterno.Crear(Modelo.Entidades.ConsumoInterno consumo, int idUsuario)
        //{
        //    //ServicioPermisos.ValidarPermisos();

        //    var fechaActual = DateTime.Now;

        //    consumo.Activo = true;
        //    consumo.FechaCreacion = fechaActual;
        //    consumo.IdUsuarioCreo = idUsuario;
        //    consumo.Transaccion = Guid.NewGuid().ToString();

        //    RepositorioConsumosInternos.Agregar(consumo);
        //    RepositorioConsumosInternos.GuardarCambios();
        //}

        //void IServicioConsumosInternosInterno.EliminarConsumo(int idConsumo, int idUsuario)
        //{
        //    var consumo = RepositorioConsumosInternos.Obtener(m => m.Activo && m.Id == idConsumo);

        //    if (consumo == null)
        //        return;

        //    var fechaActual = DateTime.Now;

        //    consumo.Activo = false;
        //    consumo.FechaEliminacion = fechaActual;
        //    consumo.IdUsuarioElimino = idUsuario;
        //    consumo.FechaModificacion = fechaActual;
        //    consumo.IdUsuarioModifico = idUsuario;

        //    consumo.HistorialesConsumoInterno.Add(new HistorialConsumoInterno
        //    {
        //        EntidadEstado = EntidadEstados.Creado,
        //        Estado = consumo.Estado,
        //        FechaInicio = fechaActual
        //    });

        //    RepositorioConsumosInternos.Modificar(consumo);
        //    RepositorioConsumosInternos.GuardarCambios();
        //}

        #endregion

        public void CrearConsumoInterno(ConsumoInterno consumo, DtoUsuario usuario)
        {
            if (consumo == null)
                throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

            if (!consumo.ArticulosConsumoInterno.Any(m => m.Activo))
                throw new SOTException(Recursos.ConsumosInternos.consumo_interno_sin_productos_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearConsumosInternos = true });

            decimal valorConsumoConIVA = 0;

            var configGlobal = ServicioParametros.ObtenerParametros();

            foreach (var ciArticulo in consumo.ArticulosConsumoInterno.Where(m => m.Activo))
            {
                if (ciArticulo.Cantidad <= 0)
                    throw new SOTException(Recursos.ConsumosInternos.cantidad_articulo_invalida_excepcion);

                ServicioArticulos.ValidarIntegridadPrecio(ciArticulo.IdArticulo, ciArticulo.PrecioUnidad);
                ServicioArticulos.ValidarDisponibilidad(ciArticulo.IdArticulo, ciArticulo.Cantidad);

                valorConsumoConIVA += Math.Round(ciArticulo.PrecioUnidad * (1 + configGlobal.Iva_CatPar), 2) * ciArticulo.Cantidad;
            }

            if (consumo.ValorConIVA.ToString("C") != valorConsumoConIVA.ToString("C"))
                throw new SOTException(Recursos.ConsumosInternos.valor_incorrecto_excepcion, valorConsumoConIVA.ToString("C"), consumo.ValorConIVA.ToString("C"));

            var fechaActual = DateTime.Now;
            var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();
            int siguienteNumero = ObtenerSiguienteNumeroConsumoInternoOrden(corteActual.Id);

            consumo.Orden = siguienteNumero;
            consumo.IdCorteTurno = corteActual.Id;

            ValidarDatos(consumo, corteActual, true);

            consumo.ValorConIVA = valorConsumoConIVA;
            consumo.ValorSinIVA = consumo.ValorConIVA / (1 + configGlobal.Iva_CatPar);
            consumo.ValorIVA = consumo.ValorConIVA - consumo.ValorSinIVA;
            consumo.FechaCreacion = fechaActual;
            consumo.FechaModificacion = fechaActual;
            consumo.FechaInicio = fechaActual;
            consumo.IdUsuarioCreo = usuario.Id;
            consumo.IdUsuarioModifico = usuario.Id;
            consumo.Estado = Comanda.Estados.Preparacion;
            consumo.MotivoCancelacion = string.Empty;
            consumo.Transaccion = Guid.NewGuid().ToString();

            consumo.HistorialesConsumoInterno.Add(new HistorialConsumoInterno
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = consumo.Estado,
                FechaInicio = fechaActual
            });

            foreach (var ciArticulo in consumo.ArticulosConsumoInterno.Where(m => m.Activo))
            {
                ciArticulo.FechaCreacion = fechaActual;
                ciArticulo.FechaModificacion = fechaActual;
                ciArticulo.IdUsuarioCreo = usuario.Id;
                ciArticulo.IdUsuarioModifico = usuario.Id;
                ciArticulo.Estado = ArticuloComanda.Estados.EnProceso;

                if (ciArticulo.Observaciones == null)
                    ciArticulo.Observaciones = "";
            }

            var idPreventa = ServicioPreventas.GrenerarPreventa(Venta.ClasificacionesVenta.Restaurante);
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                consumo.IdPreventa = idPreventa;


                RepositorioConsumosInternos.Agregar(consumo);
                RepositorioConsumosInternos.GuardarCambios();

                RegistrarMovimiento(consumo);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicket(consumo, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(consumo, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            ServicioTickets.ImprimirTicket(consumo, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(consumo, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }


        public void CancelarConsumoInterno(int idConsumoInterno, string motivo, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            var consumo = RepositorioConsumosInternos.ObtenerParaPagar(idConsumoInterno);

            if (consumo == null)
                throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

            //if (/*consumo.Estado != Comanda.Estados.PorCobrar &&*/
            //    consumo.Estado != Comanda.Estados.PorEntregar &&
            //    consumo.Estado != Comanda.Estados.Preparacion)
            if (consumo.Estado == Comanda.Estados.PorPagar ||
               consumo.Estado == Comanda.Estados.Cobrada ||
               consumo.Estado == Comanda.Estados.Cancelada)
                throw new SOTException(Recursos.ConsumosInternos.consumo_interno_no_cancelable_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarConsumosInternos = true });

            var fechaActual = DateTime.Now;

            foreach (var articulo in consumo.ArticulosConsumoInterno.Where(m => m.Activo))
            {
                //if (articulo.Estado != ArticuloComanda.Estados.EnProceso && articulo.Estado != ArticuloComanda.Estados.PorEntregar)
                //    throw new SOTException(Recursos.ConsumosInternos.articulo_consumo_interno_no_cancelable_excepcion);

                articulo.Activo = false;
                articulo.Estado = ArticuloComanda.Estados.Cancelado;
                articulo.FechaEliminacion = fechaActual;
                articulo.FechaModificacion = fechaActual;
                articulo.IdUsuarioElimino = usuario.Id;
                articulo.IdUsuarioModifico = usuario.Id;
            }

            consumo.Activo = false;
            consumo.FechaModificacion = fechaActual;
            consumo.FechaEliminacion = fechaActual;
            consumo.IdUsuarioModifico = usuario.Id;
            consumo.IdUsuarioElimino = usuario.Id;
            consumo.IdMesero = null;
            consumo.Estado = Comanda.Estados.Cancelada;
            consumo.MotivoCancelacion = motivo;

            consumo.HistorialesConsumoInterno.Add(new HistorialConsumoInterno
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = consumo.Estado,
                FechaInicio = fechaActual
            });

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioConsumosInternos.Modificar(consumo);
                RepositorioConsumosInternos.GuardarCambios();

                CancelarMovimientos(consumo);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicketConsumoInternoCancelado(idConsumoInterno, configuracionImpresoras.ImpresoraTickets, 1);
        }

        public void EntregarConsumoInterno(int idConsumoInterno, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { MarcarEntregadoConsumoInterno = true });

            var consumo = RepositorioConsumosInternos.ObtenerParaPagar(idConsumoInterno);

            if (consumo == null)
                throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

            if (consumo.Estado != Comanda.Estados.PorCobrar)
                throw new SOTException(Recursos.ConsumosInternos.consumo_interno_no_por_entregar_cliente_excepcion);

            #region validacion IVA

            var configGlobal = ServicioParametros.ObtenerParametros();
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

            var vIVA = consumo.ValorSinIVA * configGlobal.Iva_CatPar;
            var vConIVA = consumo.ValorSinIVA + vIVA;

            if (consumo.ValorConIVA.ToString("C") != vConIVA.ToString("C") || consumo.ValorIVA.ToString("C") != vIVA.ToString("C"))
                throw new SOTException(Recursos.ConsumosInternos.valores_impuestos_consumo_interno_invalidos_excepcion);

            #endregion


            var fechaActual = DateTime.Now;

            consumo.Activo = false;
            consumo.FechaModificacion = fechaActual;
            consumo.FechaEliminacion = fechaActual;
            consumo.IdUsuarioModifico = usuario.Id;
            consumo.IdUsuarioElimino = usuario.Id;
            consumo.FechaEntrega = fechaActual;
            consumo.Estado = Comanda.Estados.Cobrada;

            consumo.HistorialesConsumoInterno.Add(new HistorialConsumoInterno
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = consumo.Estado,
                FechaInicio = fechaActual
            });

            foreach (var articulo in consumo.ArticulosConsumoInterno.Where(m => m.Activo))
            {
                articulo.Activo = false;
                articulo.FechaEliminacion = fechaActual;
                articulo.FechaModificacion = fechaActual;
                articulo.IdUsuarioModifico = usuario.Id;
                articulo.IdUsuarioElimino = usuario.Id;
                articulo.PrecioUnidadFinal = Math.Round(articulo.PrecioUnidad * (1 + configGlobal.Iva_CatPar), 2);
                articulo.EntidadEstado = EntidadEstados.Modificado;
            }

            var transaccion = consumo.Transaccion;//Guid.NewGuid().ToString();

            //foreach (var pago in pagosLocales)
            consumo.PagosConsumoInterno.Add(new PagoConsumoInterno
            {
                Activo = true,
                Referencia = consumo.Transaccion,//pago.Referencia,
                TipoPago = TiposPago.Consumo,//pago.TipoPago,
                Valor = consumo.ValorConIVA,//pago.Valor,
                //TipoTarjeta = pago.TipoTarjeta,
                NumeroTarjeta = "",//pago.NumeroTarjeta ?? "",
                Transaccion = transaccion,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                IdUsuarioModifico = usuario.Id
            });

            var venta = ServicioPagos.GenerarVentaNoCompletada(consumo.PagosConsumoInterno.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.Restaurante, consumo.IdPreventa);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ServicioPagos.Pagar(venta.Id, consumo.PagosConsumoInterno.Where(m => m.Activo), usuario);

                RepositorioConsumosInternos.Modificar(consumo);
                RepositorioConsumosInternos.GuardarCambios();

                scope.Complete();
            }
#warning contemplar quitar
            ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 0);
        }

        public void EntregarProductos(int idConsumoInterno, List<int> idsArticulosConsumosInternos, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { Meserear = true });

            ConsumoInterno consumoInterno;

            lock (bloqueadorProductos)
            {
                consumoInterno = RepositorioConsumosInternos.ObtenerPorArticulos(idConsumoInterno, idsArticulosConsumosInternos);

                if (consumoInterno == null)
                    throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

                if (consumoInterno.Estado != Comanda.Estados.Preparacion && consumoInterno.Estado != Comanda.Estados.PorEntregar)
                    throw new SOTException(Recursos.ConsumosInternos.consumo_interno_no_preparacion_o_por_entregar_mesero_excepcion);

                if (consumoInterno.IdMesero.HasValue && consumoInterno.IdMesero != usuario.Id)
                    throw new SOTException(Recursos.ConsumosInternos.mesero_consumo_interno_diferente_excepcion);

                //ServicioEmpleados.ValidarMeseroActivoHabilitado(usuario.Id);

                var fechaActual = DateTime.Now;

                foreach (var idArticuloConsumoInterno in idsArticulosConsumosInternos)
                {
                    var consumoInternoArticulo = consumoInterno.ArticulosConsumoInterno.FirstOrDefault(m => m.Id == idArticuloConsumoInterno);

                    if (consumoInternoArticulo.Estado != ArticuloComanda.Estados.PorEntregar)
                        throw new SOTException(Recursos.ConsumosInternos.consumo_interno_articulos_no_por_entregar_excepcion);

                    consumoInternoArticulo.FechaModificacion = fechaActual;
                    consumoInternoArticulo.IdUsuarioModifico = usuario.Id;

                    consumoInternoArticulo.Estado = ArticuloComanda.Estados.Entregado;
                }
                if (consumoInterno.ArticulosConsumoInterno.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.Entregado))
                {
                    consumoInterno.FechaModificacion = fechaActual;
                    consumoInterno.IdUsuarioCreo = usuario.Id;
                    consumoInterno.Estado = Comanda.Estados.PorCobrar;
                    consumoInterno.IdMesero = usuario.Id;

                    consumoInterno.HistorialesConsumoInterno.Add(new HistorialConsumoInterno
                    {
                        EntidadEstado = EntidadEstados.Creado,
                        Estado = consumoInterno.Estado,
                        FechaInicio = fechaActual
                    });
                }


                RepositorioConsumosInternos.Modificar(consumoInterno);
                RepositorioConsumosInternos.GuardarCambios();
            }

            var codigos = consumoInterno.ArticulosConsumoInterno.Where(m => idsArticulosConsumosInternos.Contains(m.Id)).Select(m => m.IdArticulo).Distinct().ToList();

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            //ServicioTickets.ImprimirTicket(consumoInterno, configuracionImpresoras.ImpresoraCocina, 1, codigos, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            //ServicioTickets.ImprimirTicket(consumoInterno, configuracionImpresoras.ImpresoraBar, 1, codigos, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

        }

        public void FinalizarElaboracionProductos(int idConsumoInterno, List<int> idsArticulosConsumosInternos, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { PrepararProductos = true });

            //ServicioEmpleados.ValidarCocineroActivoHabilitado(usuario.Id);

            lock (bloqueadorProductos)
            {
                var consumoInterno = RepositorioConsumosInternos.ObtenerPorArticulos(idConsumoInterno, idsArticulosConsumosInternos);

                if (consumoInterno == null)
                    throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

                if (consumoInterno.Estado != Comanda.Estados.Preparacion)
                    throw new SOTException(Recursos.ConsumosInternos.consumo_interno_no_preparacion_excepcion);

                var fechaActual = DateTime.Now;

                foreach (var idArticuloConsumoInterno in idsArticulosConsumosInternos)
                {
                    var consumoInternoArticulo = consumoInterno.ArticulosConsumoInterno.FirstOrDefault(m => m.Id == idArticuloConsumoInterno);

                    if (consumoInternoArticulo.Estado != ArticuloComanda.Estados.EnProceso)
                        throw new SOTException(Recursos.ConsumosInternos.consumo_interno_articulos_no_preparacion_excepcion);

                    consumoInternoArticulo.FechaModificacion = fechaActual;
                    consumoInternoArticulo.IdUsuarioModifico = usuario.Id;

                    consumoInternoArticulo.Estado = ArticuloComanda.Estados.PorEntregar;
                }

                if (consumoInterno.ArticulosConsumoInterno.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.PorEntregar || m.Estado == ArticuloComanda.Estados.Entregado))
                {
                    consumoInterno.FechaModificacion = fechaActual;
                    consumoInterno.IdUsuarioModifico = usuario.Id;
                    consumoInterno.Estado = Comanda.Estados.PorEntregar;

                    consumoInterno.HistorialesConsumoInterno.Add(new HistorialConsumoInterno
                    {
                        EntidadEstado = EntidadEstados.Creado,
                        Estado = consumoInterno.Estado,
                        FechaInicio = fechaActual
                    });
                }


                RepositorioConsumosInternos.Modificar(consumoInterno);
                RepositorioConsumosInternos.GuardarCambios();
            }
        }


        public bool VerificarGuid(string guidConsumo)
        {
            return RepositorioConsumosInternos.Alguno(m => m.Activo && m.Transaccion == guidConsumo);
        }

        public ConsumoInterno ObtenerConsumoInternoPorId(int idConsumoInterno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConsumosInternos = true });

            return RepositorioConsumosInternos.Obtener(m => m.Id == idConsumoInterno);
        }

        public void ModificarConsumoInterno(ConsumoInterno consumoInternoM, List<DtoArticuloPrepararGeneracion> articulos, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            var configuracionGlobal = ServicioParametros.ObtenerParametros();

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarConsumosInternos = true });

            if (consumoInternoM == null)
                throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

            var consumoInterno = RepositorioConsumosInternos.ObtenerParaPagar(consumoInternoM.Id);

            if (consumoInterno == null)
                throw new SOTException(Recursos.ConsumosInternos.consumo_nulo_eliminado_excepcion);

            var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();

            bool empleadoCambio = consumoInternoM.IdEmpleadoConsume != consumoInterno.IdEmpleadoConsume;

            consumoInterno.IdEmpleadoConsume = consumoInternoM.IdEmpleadoConsume;
            consumoInternoM.Motivo = consumoInternoM.Motivo;

            ValidarDatos(consumoInterno, corteActual, empleadoCambio);

          //  if (/*consumoInterno.Estado != Comanda.Estados.PorEntregarCliente &&*/
           /*     consumoInterno.Estado != Comanda.Estados.PorEntregar &&
                consumoInterno.Estado != Comanda.Estados.Preparacion)
                throw new SOTException(Recursos.ConsumosInternos.consumo_interno_no_modificable_excepcion);*/

            var fechaActual = DateTime.Now;

            foreach (var consumoInternoArticulo in articulos.Where(m => m.Activo))
                if (consumoInternoArticulo.Cantidad <= 0)
                    throw new SOTException(Recursos.ConsumosInternos.cantidad_articulo_invalida_excepcion);

            var articulosOriginales = new List<DtoArticuloPrepararGeneracion>();

            foreach (var articulo in consumoInterno.ArticulosConsumoInterno.Where(m => m.Activo))
            {
                articulosOriginales.Add(new DtoArticuloPrepararGeneracion
                {
                    Activo = true,
                    Cantidad = articulo.Cantidad,
                    //EsCortesia = articulo.EsCortesia,
                    Id = articulo.Id,
                    IdArticulo = articulo.IdArticulo,
                    Observaciones = articulo.Observaciones,
                    PrecioConIVA = articulo.PrecioUnidad
                });
            }

            foreach (var tupla in (from artGeneracion in articulos
                                   join articuloConsumoInterno in consumoInterno.ArticulosConsumoInterno
                                    on new { artGeneracion.Id, Activo = true } equals new { articuloConsumoInterno.Id, articuloConsumoInterno.Activo } into x
                                   from articuloConsumoInterno in x.DefaultIfEmpty()
                                   select new
                                   {
                                       artGeneracion,
                                       articuloConsumoInterno
                                   }).ToList())
            {
                if (tupla.articuloConsumoInterno == null)
                {
                    if (tupla.artGeneracion.Id <= 0)
                    {
                        ServicioArticulos.ValidarIntegridadPrecio(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.PrecioConIVA);
                        ServicioArticulos.ValidarDisponibilidad(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad);

                        consumoInterno.ArticulosConsumoInterno.Add(new ArticuloConsumoInterno
                        {
                            Activo = true,
                            IdArticulo = tupla.artGeneracion.IdArticulo,
                            PrecioUnidad = tupla.artGeneracion.PrecioConIVA,
                            Cantidad = tupla.artGeneracion.Cantidad,
                            FechaCreacion = fechaActual,
                            FechaModificacion = fechaActual,
                            IdUsuarioCreo = usuario.Id,
                            IdUsuarioModifico = usuario.Id,
                            Estado = ArticuloComanda.Estados.EnProceso,
                            Observaciones = tupla.artGeneracion.Observaciones ?? ""
                        });
                    }
                }
                else
                {
                  /*  if (tupla.articuloConsumoInterno.Estado != ArticuloComanda.Estados.EnProceso
                        && tupla.articuloConsumoInterno.Estado != ArticuloComanda.Estados.PorEntregar)
                        throw new SOTException(Recursos.ConsumosInternos.articulo_consumo_interno_no_modificable_excepcion);*/

                    if (!tupla.artGeneracion.Activo)
                    {
                        tupla.articuloConsumoInterno.Activo = false;
                        tupla.articuloConsumoInterno.Estado = ArticuloComanda.Estados.Cancelado;
                        tupla.articuloConsumoInterno.FechaEliminacion = fechaActual;
                        tupla.articuloConsumoInterno.FechaModificacion = fechaActual;
                        tupla.articuloConsumoInterno.IdUsuarioElimino = usuario.Id;
                        tupla.articuloConsumoInterno.IdUsuarioModifico = usuario.Id;
                    }
                    else
                    {
                        if (tupla.articuloConsumoInterno.Cantidad != tupla.artGeneracion.Cantidad || tupla.articuloConsumoInterno.Observaciones != (tupla.artGeneracion.Observaciones ?? ""))
                        {
                            if (tupla.articuloConsumoInterno.Cantidad < tupla.artGeneracion.Cantidad)
                                ServicioArticulos.ValidarDisponibilidad(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad);

                            if (tupla.articuloConsumoInterno.Cantidad != tupla.artGeneracion.Cantidad && tupla.articuloConsumoInterno.PrecioUnidad != tupla.artGeneracion.PrecioConIVA)
                            {
                                var articulo = ServicioArticulos.ObtenerArticuloPorId(tupla.articuloConsumoInterno.IdArticulo);
                                throw new SOTException(Recursos.ConsumosInternos.precio_articulo_modificado_diferente_excepcion,
                                                       articulo.Desc_Art, (tupla.articuloConsumoInterno.PrecioUnidad * (1 + configuracionGlobal.Iva_CatPar)).ToString("C"),
                                                       tupla.artGeneracion.PrecioConIVA.ToString("C"));
                            }

                            tupla.articuloConsumoInterno.Cantidad = tupla.artGeneracion.Cantidad;
                            tupla.articuloConsumoInterno.Estado = ArticuloComanda.Estados.EnProceso;
                            tupla.articuloConsumoInterno.FechaModificacion = fechaActual;
                            tupla.articuloConsumoInterno.IdUsuarioModifico = usuario.Id;
                            tupla.articuloConsumoInterno.Observaciones = tupla.artGeneracion.Observaciones ?? "";
                        }
                    }
                }
            }

            if (!consumoInterno.ArticulosConsumoInterno.Any(m => m.Activo))
                throw new SOTException(Recursos.ConsumosInternos.consumo_interno_sin_productos_excepcion);

            decimal valorConsumoConIVA = 0;

            foreach (var consumoInternoArticulo in consumoInterno.ArticulosConsumoInterno.Where(m => m.Activo))
                valorConsumoConIVA += Math.Round(consumoInternoArticulo.PrecioUnidad * (1 + configuracionGlobal.Iva_CatPar), 2) * consumoInternoArticulo.Cantidad;

            if (consumoInterno.ArticulosConsumoInterno.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.Entregado))
                consumoInterno.Estado = Comanda.Estados.PorCobrar;
            else if (consumoInterno.ArticulosConsumoInterno.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.PorEntregar || m.Estado == ArticuloComanda.Estados.Entregado))
                consumoInterno.Estado = Comanda.Estados.PorEntregar;
            else
                consumoInterno.Estado = Comanda.Estados.Preparacion;

            consumoInterno.ValorConIVA = valorConsumoConIVA;
            consumoInterno.ValorSinIVA = consumoInterno.ValorConIVA / (1 + configuracionGlobal.Iva_CatPar);
            consumoInterno.ValorIVA = consumoInterno.ValorConIVA - consumoInterno.ValorSinIVA;
            consumoInterno.FechaModificacion = fechaActual;
            consumoInterno.FechaInicio = fechaActual;
            consumoInterno.IdUsuarioCreo = usuario.Id;
            consumoInterno.IdUsuarioModifico = usuario.Id;

            consumoInterno.HistorialesConsumoInterno.Add(new HistorialConsumoInterno
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = consumoInterno.Estado,
                FechaInicio = fechaActual
            });

            //if (consumoInterno.Observaciones == null)
            //    consumoInterno.Observaciones = "";
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioConsumosInternos.Modificar(consumoInterno);
                RepositorioConsumosInternos.GuardarCambios();

                ActualizarMovimientos(consumoInterno.Id, articulosOriginales, articulos, fechaActual);

                scope.Complete();
            }
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicket(consumoInterno, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(consumoInterno, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            ServicioTickets.ImprimirTicket(consumoInterno, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(consumoInterno, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }

        //public void CrearConsumoInterno(ConsumoInterno consumoInterno, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearConsumosInternos = true });

        //    if (consumoInterno == null)
        //        throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

        //    var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();

        //    ValidarDatos(consumoInterno, corteActual, true);

        //    var fechaActual = DateTime.Now;

        //    consumoInterno.FechaCreacion = fechaActual;
        //    consumoInterno.FechaModificacion = fechaActual;
        //    consumoInterno.IdUsuarioCreo = usuario.Id;
        //    consumoInterno.IdUsuarioModifico = usuario.Id;

        //    RepositorioConsumosInternos.Agregar(consumoInterno);
        //    RepositorioConsumosInternos.GuardarCambios();
        //}

        //private void ModificarConsumoInterno(ConsumoInterno consumoInternoM, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarConsumosInternos = true });

        //    if (consumoInternoM == null)
        //        throw new SOTException(Recursos.ConsumosInternos.nulo_excepcion);

        //    var consumoInterno = RepositorioConsumosInternos.Obtener(m => m.Id == consumoInternoM.Id && m.Activo);

        //    if (consumoInterno == null)
        //        throw new SOTException(Recursos.ConsumosInternos.consumo_nulo_eliminado_excepcion);

        //    var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();

        //    bool empleadoCambio = consumoInternoM.IdEmpleadoConsume != consumoInterno.IdEmpleadoConsume;

        //    consumoInterno.IdEmpleadoConsume = consumoInternoM.IdEmpleadoConsume;
        //    consumoInternoM.Motivo = consumoInternoM.Motivo;

        //    ValidarDatos(consumoInterno, corteActual, empleadoCambio);

        //    var fechaActual = DateTime.Now;

        //    consumoInterno.FechaModificacion = fechaActual;
        //    consumoInterno.IdUsuarioModifico = usuario.Id;

        //    RepositorioConsumosInternos.Modificar(consumoInterno);
        //    RepositorioConsumosInternos.GuardarCambios();
        //}


        public List<ConsumoInterno> ObtenerConsumosInternosFiltrados(string nombre, string apellidoPaterno, string apellidoMaterno, bool soloEnCurso, DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarConsumosInternos = true });

            return RepositorioConsumosInternos.ObtenerConsumosInternosFiltrados(nombre, apellidoPaterno, apellidoMaterno, soloEnCurso, fechaInicial, fechaFinal);
        }

        public ConsumoInterno ObtenerDetallesConsumoInternoPendiente(int idConsumoInterno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConsumosInternos = true });

            var consumo = RepositorioConsumosInternos.ObtenerConsumoPendienteCargado(idConsumoInterno);

            if (consumo == null)
                return consumo;

            ProcesarDetallesArticulos(consumo);

            return consumo;
        }

        public ConsumoInterno ObtenerDetallesConsumoInternoIgnorandoEstado(int idConsumoInterno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConsumosInternos = true });

            var consumo = RepositorioConsumosInternos.Obtener(m => m.Id == idConsumoInterno);

            if (consumo == null)
                return consumo;

            if (consumo.Estado != Comanda.Estados.Cobrada && consumo.Estado != Comanda.Estados.Cancelada)
                return ObtenerDetallesConsumoInternoPendiente(idConsumoInterno, usuario);

            if (consumo.Estado == Comanda.Estados.Cobrada)
                consumo = RepositorioConsumosInternos.ObtenerConsumoFinalizadoCargado(idConsumoInterno);
            else if (consumo.Estado == Comanda.Estados.Cancelada)
                consumo = RepositorioConsumosInternos.ObtenerConsumoCanceladoCargado(idConsumoInterno);

            if (consumo == null)
                return consumo;

            ProcesarDetallesArticulos(consumo);

            return consumo;
        }
        /*
        public ConsumoInterno ObtenerDetallesConsumoInternoCancelado(int idConsumoInterno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarConsumosInternos = true });

            var consumo = RepositorioConsumosInternos.ObtenerConsumoCanceladoCargado(idConsumoInterno);

            if (consumo == null)
                return consumo;

            ProcesarDetallesArticulos(consumo);

            return consumo;
        }*/


        public List<DtoResumenComandaExtendido> ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion(string departamento, DtoUsuario UsuarioActual)
        {
            //var idsArticulos = ServicioArticulos.ObtenerArticulosConLineaPorDepartamento(departamento);

            var consumos = RepositorioConsumosInternos.ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion();

            var idsArticulos = consumos.SelectMany(m => m.ArticulosComanda).Select(m => m.IdArticulo).ToList();

            var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos, departamento);

            var estados = new List<ArticuloComanda.Estados>() { ArticuloComanda.Estados.EnProceso, ArticuloComanda.Estados.PorEntregar };

            List<DtoResumenComandaExtendido> validos = new List<DtoResumenComandaExtendido>();
            string _contradepartamento = "";
            List<string> artsvalid = new List<string>();
            List<Modelo.Almacen.Entidades.Articulo> articulosacomparar = new List<Modelo.Almacen.Entidades.Articulo>();
            bool _TieneArticulosOtroDepto = false;
            string tipootrodepto = string.Empty;
            foreach (var consumo in consumos)
            {
                _TieneArticulosOtroDepto = false;
                artsvalid = new List<string>();
                articulosacomparar = new List<Modelo.Almacen.Entidades.Articulo>();
                foreach (var arti in consumo.ArticulosComanda)
                {
                    artsvalid.Add(arti.IdArticulo);
                }
                if (departamento.Equals("Cocina"))
                    _contradepartamento = "Bar";
                else
                    _contradepartamento = "Cocina";
                var articontradepto = ServicioArticulos.ObtenerArticulosConLineaPorId(artsvalid, _contradepartamento);
                foreach (var oiu in articontradepto)
                {
                    articulosacomparar.Add(oiu);
                }
                var artidepto = ServicioArticulos.ObtenerArticulosConLineaPorId(artsvalid, departamento);
                foreach (var oiu1 in artidepto)
                {
                    articulosacomparar.Add(oiu1);
                }
                bool _esbebdida = false;
                bool _essex = false;
                bool _esalimentos = false;
                if (articontradepto.Count > 0)
                {
                    foreach (var guyg in articulosacomparar)
                    {
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Equals("ALIMENTOS"))
                            _esalimentos = true;
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Equals("BEBIDAS"))
                            _esbebdida = true;
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Contains("SEX"))
                            _essex = true;
                    }
                    if (_esalimentos && _esbebdida == false && _essex == false)
                        tipootrodepto = "A";
                    else if (_esalimentos && _esbebdida && _essex == false)
                        tipootrodepto = "AB";
                    else if (_esbebdida && _esbebdida && _essex)
                        tipootrodepto = "ABS";
                    _TieneArticulosOtroDepto = true;
                }



                if (!consumo.ArticulosComanda.Any(m => articulos.Any(a => a.Cod_Art == m.IdArticulo) && estados.Contains(m.Estado)))
                    continue;

                consumo.ArticulosComanda.RemoveAll(m => !articulos.Any(a => a.Cod_Art == m.IdArticulo));

                int contadordearticulosxcmd = 0;
                foreach (var artCom in consumo.ArticulosComanda)
                {
                    var art = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                    artCom.Nombre = art.Desc_Art;
                    artCom.Tipo = art.ZctCatLinea.Desc_Linea;

                    if (_TieneArticulosOtroDepto == true && contadordearticulosxcmd == consumo.ArticulosComanda.Count - 1)
                    {
                        artCom.EsOtroDepto = true;
                        artCom.OtosDeptos = tipootrodepto;
                        _TieneArticulosOtroDepto = false;
                    }
                    contadordearticulosxcmd = contadordearticulosxcmd + 1;
                }

                validos.Add(consumo);
            }

            return validos;
        }
        public void Imprimir(int IdConsumoInterno)
        {
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicketConsumoInterno(IdConsumoInterno, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicketConsumoInterno(IdConsumoInterno, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            ServicioTickets.ImprimirTicketConsumoInterno(IdConsumoInterno, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicketConsumoInterno(IdConsumoInterno, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }
        public int ObtenerUltimoNumeroConsumoInterno(int idTurno)
        {
            return (from ordenR in RepositorioConsumosInternos.ObtenerElementos(m => m.IdCorteTurno == idTurno)
                    orderby ordenR.Orden descending
                    select ordenR.Orden).FirstOrDefault();
        }

        #region métodos private

        private void CancelarMovimientos(ConsumoInterno consumoInterno)
        {
            foreach (var articulo in consumoInterno.ArticulosConsumoInterno)
                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.CI, 0, true, /*articulo.Cantidad * articulo.PrecioUnidad,*/ consumoInterno.Id.ToString(), articulo.IdArticulo, articulo.Cantidad, articulo.FechaEliminacion.Value, true);
        }

        private void ActualizarMovimientos(int idConsumoInterno, List<DtoArticuloPrepararGeneracion> articulosOriginales, List<DtoArticuloPrepararGeneracion> articulos, DateTime fecha)
        {
            foreach (var tupla in (from artGeneracion in articulos
                                   join articuloOrden in articulosOriginales
                                    on new { artGeneracion.Id, Activo = true } equals new { articuloOrden.Id, articuloOrden.Activo } into x
                                   from articuloOrden in x.DefaultIfEmpty()
                                   select new
                                   {
                                       artGeneracion,
                                       articuloOrden
                                   }).ToList())
            {
                if (tupla.articuloOrden == null)
                {
                    if (tupla.artGeneracion.Id <= 0)
                    {
                        ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.CI, 0, false, /*tupla.artGeneracion.Cantidad * tupla.artGeneracion.PrecioConIVA * -1,*/ idConsumoInterno.ToString(), tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad * -1, fecha, true);
                    }
                }
                else
                {
                    if (!tupla.artGeneracion.Activo)
                    {
                        ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.CI, 0, true, /*tupla.artGeneracion.Cantidad * tupla.artGeneracion.PrecioConIVA,*/ idConsumoInterno.ToString(), tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad, fecha, true);
                    }
                    else
                    {
                        if (tupla.articuloOrden.Cantidad != tupla.artGeneracion.Cantidad)
                        {

                            var nuevaCantidad = tupla.articuloOrden.Cantidad - tupla.artGeneracion.Cantidad;

                            if (nuevaCantidad < 0)
                            {
                                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.CI, 0, false, /*nuevaCantidad * tupla.artGeneracion.PrecioConIVA,*/ idConsumoInterno.ToString(), tupla.artGeneracion.IdArticulo, nuevaCantidad, fecha, true);
                            }
                            else if (nuevaCantidad > 0)
                            {
                                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.CI, 0, true, /*nuevaCantidad * tupla.artGeneracion.PrecioConIVA,*/ idConsumoInterno.ToString(), tupla.artGeneracion.IdArticulo, nuevaCantidad, fecha, true);
                            }
                        }
                    }
                }
            }
        }

        private void RegistrarMovimiento(ConsumoInterno consumo)
        {
            foreach (var articulo in consumo.ArticulosConsumoInterno.Where(m => m.Activo))
                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.CI, 0, false, /*articulo.Cantidad * articulo.PrecioUnidad * -1,*/ consumo.Id.ToString(), articulo.IdArticulo, articulo.Cantidad * -1, articulo.FechaCreacion, true);
        }

        private void ProcesarDetallesArticulos(ConsumoInterno consumo)
        {
            var idsArticulos = consumo.ArticulosConsumoInterno.Select(m => m.IdArticulo).ToList();

            var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

            foreach (var artCom in consumo.ArticulosConsumoInterno)
            {
                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
            }
        }

        private void ValidarDatos(ConsumoInterno consumoInterno, CorteTurno corteActual, bool validarEmpleado)
        {
            if (validarEmpleado && !ServicioEmpleados.VerificarEmpleadoActivo(consumoInterno.IdEmpleadoConsume))
                throw new SOTException(Recursos.ConsumosInternos.empleado_destino_nulo_eliminado_excepcion);

            if (consumoInterno.IdCorteTurno != corteActual.Id)
                throw new SOTException(Recursos.ConsumosInternos.consumo_no_corte_actual_excepcion);

            if (string.IsNullOrEmpty(consumoInterno.Motivo))
                throw new SOTException(Recursos.ConsumosInternos.motivo_invalido_excepcion);
        }

        private int ObtenerSiguienteNumeroConsumoInternoOrden(int idTurno)
        {
            return Math.Max(Math.Max(ServicioRestaurantes.ObtenerUltimoNumeroOrden(idTurno), ServicioRoomServices.ObtenerUltimoNumeroComanda(idTurno)), ObtenerUltimoNumeroConsumoInterno(idTurno)) + 1;
        }

        #endregion

        public int ObtenerCantidadEnCurso(string nombre, string apellidoPaterno, string apellidoMaterno, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarConsumosInternos = true });

            return RepositorioConsumosInternos.ObtenerCantidadEnCurso(nombre, apellidoPaterno, apellidoMaterno);
        }
    }
}
