﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Negocio.CortesTurno;
using Negocio.Empleados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.ConsumosInternos
{
    public class ServicioConsumosInternosHabitacion : IServicioConsumosInternosHabitacionInterno
    {
        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });


        IRepositorioConsumosInternosHabitacion RepositorioConsumosInternosHabitacion
        {
            get { return _RepositorioConsumosInternosHabitacion.Value; }
        }

        Lazy<IRepositorioConsumosInternosHabitacion> _RepositorioConsumosInternosHabitacion = new Lazy<IRepositorioConsumosInternosHabitacion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConsumosInternosHabitacion>(); });

        public bool VerificarGuid(string guidConsumo)
        {
            return RepositorioConsumosInternosHabitacion.Alguno(m => m.Activo && m.Transaccion == guidConsumo);
        }

        void IServicioConsumosInternosHabitacionInterno.Crear(ConsumoInternoHabitacion consumo, int idUsuario)
        {
            if (!ServicioEmpleados.VerificarEmpleadoActivo(consumo.IdEmpleadoConsume))
                throw new SOTException(Recursos.ConsumosInternos.empleado_destino_nulo_eliminado_excepcion);

            if (string.IsNullOrEmpty(consumo.Motivo))
                throw new SOTException(Recursos.ConsumosInternos.motivo_invalido_excepcion);

            var fechaActual = DateTime.Now;

            var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();
            consumo.IdCorteTurno = corteActual.Id;
            consumo.Activo = true;
            consumo.FechaCreacion = fechaActual;
            consumo.FechaModificacion = fechaActual;
            consumo.IdUsuarioCreo = idUsuario;
            consumo.IdUsuarioModifico = idUsuario;

            RepositorioConsumosInternosHabitacion.Agregar(consumo);
            RepositorioConsumosInternosHabitacion.GuardarCambios();
        }

        void IServicioConsumosInternosHabitacionInterno.CancelarConsumo(string folioConsumo, int idUsuario) 
        {
            var consumo = RepositorioConsumosInternosHabitacion.Obtener(m => m.Activo && m.Transaccion == folioConsumo);

            if (consumo == null)
                return;

            var fechaActual = DateTime.Now;

            consumo.Activo = false;
            consumo.FechaEliminacion = fechaActual;
            consumo.FechaModificacion = fechaActual;
            consumo.IdUsuarioElimino = idUsuario;
            consumo.IdUsuarioModifico = idUsuario;

            RepositorioConsumosInternosHabitacion.Modificar(consumo);
            RepositorioConsumosInternosHabitacion.GuardarCambios();
        }
    }
}
