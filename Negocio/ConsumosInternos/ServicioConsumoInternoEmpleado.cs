﻿using System;
using System.Collections.Generic;
using Modelo.Repositorios;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Transversal.Dependencias;
using Modelo.Dtos;

namespace Negocio.ConsumosInternos
{
    public class ServicioConsumoInternoEmpleado : IServicioConsumoInternoEmpleado
    {

        IRepositorioConsumoInternoEmpleado RepositorioConsumoInternoEmpleado
        {
            get { return _RepositorioConsumoInternoEmpleado.Value; }
        }

        Lazy<IRepositorioConsumoInternoEmpleado> _RepositorioConsumoInternoEmpleado = new Lazy<IRepositorioConsumoInternoEmpleado>(() =>
        { return FabricaDependencias.Instancia.Resolver<IRepositorioConsumoInternoEmpleado>(); });

        public List<VW_ConsumoInternoEmpleado> ObtieneEmpleados(string nombreempleado)
        {
           return RepositorioConsumoInternoEmpleado.ObtieneEmpleados(nombreempleado);
        }

        public List<DtoConsumoInternosEmpleado> ObtieneConsumoInternoempleado(string numeroempleado, DateTime fechainicial, DateTime fechafinal, bool Esfechashablitadas)
        {
            return RepositorioConsumoInternoEmpleado.ObtieneConsumoInternoempleado(numeroempleado, fechainicial, fechafinal, Esfechashablitadas);
        }
    }
}
