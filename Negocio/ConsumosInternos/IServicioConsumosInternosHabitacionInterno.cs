﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConsumosInternos
{
    internal interface IServicioConsumosInternosHabitacionInterno : IServicioConsumosInternosHabitacion
    {
        /// <summary>
        /// Permite registrar información de un nuevo consumo interno en la base de datos
        /// </summary>
        /// <param name="consumo"></param>
        void Crear(Modelo.Entidades.ConsumoInternoHabitacion consumo, int idUsuario);
        /// <summary>
        /// Permite marcar como eliminados los datos de un consumo interno de habitación
        /// </summary>
        /// <param name="folioConsumo"></param>
        /// <param name="idUsuario"></param>
        void CancelarConsumo(string folioConsumo, int idUsuario);
    }
}
