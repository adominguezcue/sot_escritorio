﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConsumosInternos
{
    public interface IServicioConsumosInternos
    {
        /// <summary>
        /// Verifica si el guid pertenece a un consumo interno registrado en la base de datos
        /// </summary>
        /// <param name="guidConsumo"></param>
        bool VerificarGuid(string guidConsumo);
        /// <summary>
        /// Retorna el consumo interno con el  id proporcionado
        /// </summary>
        /// <param name="idConsumoInterno"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        ConsumoInterno ObtenerConsumoInternoPorId(int idConsumoInterno, DtoUsuario usuario);
        /// <summary>
        /// Permite registrar los datos de un consumo interno en el sistema
        /// </summary>
        /// <param name="consumoInterno"></param>
        /// <param name="usuario"></param>
        void CrearConsumoInterno(ConsumoInterno consumoInterno, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar los datos de un consumo interno en el sistema
        /// </summary>
        /// <param name="consumoInterno"></param>
        /// <param name="usuario"></param>
        void ModificarConsumoInterno(ConsumoInterno consumoInterno, List<Modelo.Dtos.DtoArticuloPrepararGeneracion> articulos, DtoCredencial credencial);
        /// <summary>
        /// Permite obtener los consumos internos en base a los datos del empleado al que va dirigido y un rango de fechas
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="apellidoPaterno"></param>
        /// <param name="apellidoMaterno"></param>
        /// <param name="usuario"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        List<ConsumoInterno> ObtenerConsumosInternosFiltrados(string nombre, string apellidoPaterno, string apellidoMaterno, bool soloEnCurso, DtoUsuario usuario, DateTime? fechaInicial = null, DateTime? fechaFinal = null);

        ConsumoInterno ObtenerDetallesConsumoInternoPendiente(int idConsumoInterno, DtoUsuario usuario);

        ConsumoInterno ObtenerDetallesConsumoInternoIgnorandoEstado(int idConsumoInterno, DtoUsuario usuario);

        //ConsumoInterno ObtenerDetallesConsumoInternoCancelado(int idConsumoInterno, DtoUsuario usuario);

        List<DtoResumenComandaExtendido> ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion(string departamento, DtoUsuario UsuarioActual);

        void FinalizarElaboracionProductos(int idConsumoInterno, List<int> idsArticulosComandas, DtoCredencial credencial);

        void EntregarProductos(int idConsumoInterno, List<int> idsArticulosComandas, DtoCredencial credencial);
        void CancelarConsumoInterno(int idConsumoInterno, string motivo, DtoCredencial credencial);
        void EntregarConsumoInterno(int idConsumoInterno, DtoCredencial credencial);

        void Imprimir(int IdConsumoInterno);

        int ObtenerCantidadEnCurso(string nombre, string apellidoPaterno, string apellidoMaterno, DtoUsuario usuario);
    }
}
