﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Dtos;

namespace Negocio.ConsumosInternos
{
    public interface IServicioConsumoInternoEmpleado
    {
        List<VW_ConsumoInternoEmpleado> ObtieneEmpleados(string nombreemplado);

        List<DtoConsumoInternosEmpleado> ObtieneConsumoInternoempleado(string numeroempleado, DateTime fechainicial, DateTime fechafinal, bool Esfechashablitadas);
    }
}
