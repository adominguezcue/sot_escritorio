﻿using Modelo;
using Modelo.Repositorios;
using Negocio.Seguridad.Permisos;
using Negocio.TiposHabitacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.ConfiguracionesTipo;
using Modelo.Dtos;
using Negocio.Habitaciones;
using Negocio.Facturacion;
using Negocio.ConfiguracionesGlobales;
using Negocio.Almacen.Parametros;
using Negocio.Ventas;
using Negocio.Pagos;
using Dominio.Nucleo.Entidades;

namespace Negocio.Reservaciones
{
    public class ServicioReservaciones : IServicioReservacionesInterno
    {
        IServicioConfiguracionesTipo ServicioConfiguracionesTipo
        {
            get { return _servicioConfiguracionesTipo.Value; }
        }

        Lazy<IServicioConfiguracionesTipo> _servicioConfiguracionesTipo = new Lazy<IServicioConfiguracionesTipo>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesTipo>(); });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioVentasInterno ServicioVentas
        {
            get { return _servicioVentas.Value; }
        }

        Lazy<IServicioVentasInterno> _servicioVentas = new Lazy<IServicioVentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVentasInterno>(); });

        IServicioPagosInterno ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagosInterno> _servicioPagos = new Lazy<IServicioPagosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagosInterno>(); });

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioDatosFiscales ServicioDatosFiscales
        {
            get { return _servicioDatosFiscales.Value; }
        }

        Lazy<IServicioDatosFiscales> _servicioDatosFiscales = new Lazy<IServicioDatosFiscales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioDatosFiscales>(); });

        IServicioTiposHabitaciones ServicioTiposHabitaciones
        {
            get { return _servicioTiposHabitaciones.Value; }
        }

        Lazy<IServicioTiposHabitaciones> _servicioTiposHabitaciones = new Lazy<IServicioTiposHabitaciones>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTiposHabitaciones>(); });

        IRepositorioReservaciones RepositorioReservaciones
        {
            get { return _repostorioReservaciones.Value; }
        }

        Lazy<IRepositorioReservaciones> _repostorioReservaciones = new Lazy<IRepositorioReservaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioReservaciones>(); });

        IRepositorioPagosReservaciones RepositorioPagosReservaciones
        {
            get { return _repostorioPagosReservaciones.Value; }
        }

        Lazy<IRepositorioPagosReservaciones> _repostorioPagosReservaciones = new Lazy<IRepositorioPagosReservaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosReservaciones>(); });


        private void ValidarNoDupicidad(ConfiguracionTipo configuracion, Reservacion reservacion, DateTime fechaActual, bool EsMismoDia) 
        {
            if (configuracion == null)
                throw new SOTException(Recursos.Reservaciones.tipo_habitacion_no_seleccionado_excepcion);

            var fechaI = reservacion.FechaEntrada.Date.AddHours(configuracion.DuracionOEntrada);
            var fechaS = reservacion.FechaSalida.Date.AddHours(configuracion.HoraSalida.Value);

            if (!EsMismoDia)
            {
                if (fechaI <= fechaActual)
                    throw new SOTException(Recursos.Reservaciones.fecha_entrada_menor_fecha_actual_excepcion);
            }

            var estados = new List<int> { (int)Reservacion.EstadosReservacion.Confirmada, (int)Reservacion.EstadosReservacion.Pendiente, (int)Reservacion.EstadosReservacion.Consumida };

            var cantidadReservaciones = RepositorioReservaciones.SP_ObtenerCantidadCruzadas(fechaI, fechaS, reservacion.Id, configuracion.Id);


            if (configuracion.TipoHabitacion.MaximoReservaciones < cantidadReservaciones + 1)
                throw new SOTException(Recursos.Reservaciones.maximo_reservaciones_excedido_exception, configuracion.TipoHabitacion.Descripcion);

            if (RepositorioReservaciones.Alguno(m => m.CodigoReserva == reservacion.CodigoReserva && m.Id != reservacion.Id))
                throw new SOTException(Recursos.Reservaciones.codigo_en_uso_excepcion, reservacion.CodigoReserva);

        }

        private void ValidarValores(ConfiguracionTipo configuracion, Modelo.Almacen.Entidades.ZctCatPar configuracionGlobal, Reservacion reservacion, DateTime fechaActual, DtoUsuario usuario) 
        {
            decimal valorEnviadoNocheConIVA = Math.Round(reservacion.TotalHabitacion * (1 + configuracionGlobal.Iva_CatPar), 2);
            decimal valorEnviadoNochesExtraConIVA = Math.Round(reservacion.TotalHospedajeExtra * (1 + configuracionGlobal.Iva_CatPar), 2);
            decimal valorEnviadoPersonasExtraConIVA = Math.Round(reservacion.TotalPersonasExtra * (1 + configuracionGlobal.Iva_CatPar), 2);
            decimal descuentoPaquetesEnviadoConIVA = Math.Round(reservacion.Descuento * (1 + configuracionGlobal.Iva_CatPar), 2);
            decimal precioPaquetesEnviadoConIVA = Math.Round(reservacion.Paquetes * (1 + configuracionGlobal.Iva_CatPar), 2);

            decimal descuentoPaquetesConIVA = 0, precioPaquetesConIVA = 0;

            foreach (var paqueteReservacion in reservacion.PaquetesReservaciones.Where(m => m.Activo || (!m.Activo && m.Id != 0)))
            {
                if (paqueteReservacion.Activo)
                {
                    paqueteReservacion.FechaCreacion = fechaActual;
                    paqueteReservacion.IdUsuarioCreo = usuario.Id;

                    descuentoPaquetesConIVA += Math.Round(paqueteReservacion.Descuento * (1 + configuracionGlobal.Iva_CatPar), 2) * paqueteReservacion.Cantidad;
                    precioPaquetesConIVA += Math.Round(paqueteReservacion.Precio * (1 + configuracionGlobal.Iva_CatPar), 2) * paqueteReservacion.Cantidad;
                }
                else
                {
                    paqueteReservacion.FechaEliminacion = fechaActual;
                    paqueteReservacion.IdUsuarioElimino = usuario.Id;
                }
            }

            if (descuentoPaquetesEnviadoConIVA != descuentoPaquetesConIVA)
                throw new SOTException(Recursos.Reservaciones.descuento_invalido_excepcion, descuentoPaquetesConIVA.ToString("C"), descuentoPaquetesEnviadoConIVA.ToString("C"));

            if (precioPaquetesEnviadoConIVA != precioPaquetesConIVA)
                throw new SOTException(Recursos.Reservaciones.precio_paquetes_invalido_excepcion, precioPaquetesConIVA.ToString("C"), precioPaquetesEnviadoConIVA.ToString("C"));

            decimal valorNocheConIVA = Math.Round(configuracion.Precio * (1 + configuracionGlobal.Iva_CatPar), 2);
            decimal valorNochesExtraConIVA = Math.Round(configuracion.Precio * (1 + configuracionGlobal.Iva_CatPar), 2) * (reservacion.Noches - 1);
            decimal valorPersonasExtraConIVA = Math.Round(configuracion.PrecioPersonaExtra * (1 + configuracionGlobal.Iva_CatPar), 2) * reservacion.PersonasExtra * reservacion.Noches;


            if (valorEnviadoNocheConIVA != valorNocheConIVA)
                throw new SOTException(Recursos.Reservaciones.valor_noche_diferente_configuracion_excepcion, valorNocheConIVA.ToString("C"), valorEnviadoNocheConIVA.ToString("C"));

            if (valorEnviadoNochesExtraConIVA != valorNochesExtraConIVA)
                throw new SOTException(Recursos.Reservaciones.valor_noches_extra_diferente_configuracion_excepcion, valorNochesExtraConIVA.ToString("C"), valorEnviadoNochesExtraConIVA.ToString("C"));

            if (valorEnviadoPersonasExtraConIVA != valorPersonasExtraConIVA)
                throw new SOTException(Recursos.Reservaciones.valor_personas_extra_diferente_configuracion_excepcion, valorPersonasExtraConIVA.ToString("C"), valorEnviadoPersonasExtraConIVA.ToString("C"));

            decimal vConIVA = valorNocheConIVA + valorPersonasExtraConIVA + valorNochesExtraConIVA + precioPaquetesConIVA - descuentoPaquetesConIVA;
            decimal valor = vConIVA / (1 + configuracionGlobal.Iva_CatPar);
            decimal vIVA = vConIVA - valor;

            if (reservacion.ValorConIVA.ToString("C") != vConIVA.ToString("C"))
                throw new SOTException(Recursos.Reservaciones.valor_reservacion_invalido_excepcion, vConIVA.ToString("C"), reservacion.ValorConIVA.ToString("C"));

            reservacion.ValorConIVA = vConIVA;
            reservacion.ValorIVA = vIVA;
            reservacion.ValorSinIVA = valor;
        }

        [Obsolete]
        private List<PagoReservacion> ProcesarPagos(Reservacion reservacion, List<DtoInformacionPago> informacionPagos, string numeroTransaccion, DateTime fechaActual, DtoUsuario usuario)
        {
            if (informacionPagos.Count == 0)
                throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, reservacion.ValorConIVA.ToString("C"), (0).ToString("C"));

            var valorPagos = informacionPagos.Sum(m => m.Valor);

            if (reservacion.ValorConIVA.ToString("C") != valorPagos.ToString("C"))
                throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, reservacion.ValorConIVA.ToString("C"), valorPagos.ToString("C"));

            List<PagoReservacion> pagosCancelar = null;

            
            if(reservacion.Id > 0) 
            {
                var pagosExistentes = reservacion.PagosReservacion.Where(m => m.Activo).ToList();//RepositorioPagosReservaciones.ObtenerElementos(m => m.Activo && m.IdReservacion == reservacion.Id).ToList();

                if (informacionPagos.Count != pagosExistentes.Count || pagosExistentes.Sum(m => m.Valor) != valorPagos)
                    pagosCancelar = pagosExistentes;
                else
                {
                    foreach (var infoPago in informacionPagos)
                    {
                        if (!pagosExistentes.Any(m =>
                            m.TipoPago == infoPago.TipoPago &&
                            m.Referencia == infoPago.Referencia &&
                            m.TipoTarjeta == infoPago.TipoTarjeta &&
                            m.Valor == infoPago.Valor &&
                            m.NumeroTarjeta == (infoPago.NumeroTarjeta ?? "")
                            ))
                            pagosCancelar = pagosExistentes;
                    }

                    foreach (var pago in pagosExistentes)
                    {
                        if (!informacionPagos.Any(m =>
                                m.TipoPago == pago.TipoPago &&
                                m.Referencia == pago.Referencia &&
                                m.TipoTarjeta == pago.TipoTarjeta &&
                                m.Valor == pago.Valor &&
                                m.NumeroTarjeta == (pago.NumeroTarjeta == "" ? null : pago.NumeroTarjeta)
                                ))
                            pagosCancelar = pagosExistentes;
                    }
                }
            }

            if (reservacion.Id == 0 || (pagosCancelar != null && pagosCancelar.Any(m=> m.Activo)))
            {
                if (reservacion.Id == 0)
                    reservacion.PagosReservacion.Clear();

                foreach (var pago in informacionPagos)
                {
                    reservacion.PagosReservacion.Add(new PagoReservacion
                    {
                        Activo = true,
                        Transaccion = numeroTransaccion,
                        FechaCreacion = fechaActual,
                        FechaModificacion = fechaActual,
                        IdUsuarioCreo = usuario.Id,
                        IdUsuarioModifico = usuario.Id,
                        TipoPago = pago.TipoPago,
                        Referencia = pago.Referencia,
                        TipoTarjeta = pago.TipoTarjeta,
                        Valor = pago.Valor,
                        NumeroTarjeta = pago.NumeroTarjeta ?? ""
                    });
                }
            }

            return pagosCancelar;
        }

        private void ProcesarPagos(Reservacion reservacion, List<DtoInformacionPago> informacionPagos, string numeroTransaccion, DateTime fechaActual, int idUsuario, decimal valorIVA)
        {
            //if (informacionPagos.Count == 0)
            //    throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, reservacion.ValorConIVA.ToString("C"), (0).ToString("C"));

            if (informacionPagos.Any(m => m.Valor < 0))
                throw new SOTException("No pueden haber pagos negativos");

            /*
            var valorPagos = informacionPagos.Sum(m => m.Valor);

            if (reservacion.ValorConIVA.ToString("C") != valorPagos.ToString("C"))
                throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, reservacion.ValorConIVA.ToString("C"), valorPagos.ToString("C"));
            
             */

            //List<PagoReservacion> pagosCancelar = null;

            decimal diferenciaConIVA = 0;

            if (reservacion.Id != 0)
            {
#warning Detallar mejor los montos no reembolsables en caso de que los totales no coincidan

                //var pagosExistentes = reservacion.PagosReservacion.Where(m => m.Activo).ToList();

                //Se agregan 
                var valorPagos = Math.Round(informacionPagos.Sum(m => m.Valor) + reservacion.PagosReservacion.Where(m => m.Activo).Sum(m => m.Valor), 2);

                var valorRealReservacionConIVA = Math.Round(reservacion.ValorConIVA, 2) +
                    (informacionPagos.Count == 0 ? reservacion.MontosNoReembolsables.Where(m => m.Activo).Sum(m => Math.Round(m.ValorConIVA, 2)) : 0);

                if (informacionPagos.Count > 0)
                {
                    if (valorRealReservacionConIVA != valorPagos)
                        throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, reservacion.ValorConIVA.ToString("C"), valorPagos.ToString("C"));
                }
                else if (valorRealReservacionConIVA > valorPagos && Math.Round(reservacion.ValorConIVA, 2) > valorPagos)
                    throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, reservacion.ValorConIVA.ToString("C"), valorPagos.ToString("C"));

                //Si el sistema requiere que se ingresen nuevos detalles de pago, significa que el valor de la reservación superó el de los pagos que tenía cuando fue creada
                diferenciaConIVA = informacionPagos.Count == 0 ? (valorPagos - valorRealReservacionConIVA) : 0;//Math.Round(reservacion.ValorConIVA, 2) < valorPagos;

                /*
                if (informacionPagos.Count != pagosExistentes.Count || pagosExistentes.Sum(m => m.Valor) != valorPagos)
                    pagosCancelar = pagosExistentes;
                else
                {
                    foreach (var infoPago in informacionPagos)
                    {
                        if (!pagosExistentes.Any(m =>
                            m.TipoPago == infoPago.TipoPago &&
                            m.Referencia == infoPago.Referencia &&
                            m.TipoTarjeta == infoPago.TipoTarjeta &&
                            m.Valor == infoPago.Valor &&
                            m.NumeroTarjeta == (infoPago.NumeroTarjeta ?? "")
                            ))
                            pagosCancelar = pagosExistentes;
                    }

                    foreach (var pago in pagosExistentes)
                    {
                        if (!informacionPagos.Any(m =>
                                m.TipoPago == pago.TipoPago &&
                                m.Referencia == pago.Referencia &&
                                m.TipoTarjeta == pago.TipoTarjeta &&
                                m.Valor == pago.Valor &&
                                m.NumeroTarjeta == (pago.NumeroTarjeta == "" ? null : pago.NumeroTarjeta)
                                ))
                            pagosCancelar = pagosExistentes;
                    }
                }*/
            }
            else
            {
                reservacion.MontosNoReembolsables.Clear();
                reservacion.PagosReservacion.Clear();

                var valorPagos = informacionPagos.Sum(m => m.Valor);

                if (reservacion.ValorConIVA.ToString("C") != valorPagos.ToString("C"))
                    throw new SOTException(Recursos.Rentas.pago_incorrecto_excepcion, reservacion.ValorConIVA.ToString("C"), valorPagos.ToString("C"));
            }

            //if (reservacion.Id == 0 || (pagosCancelar != null && pagosCancelar.Any(m => m.Activo)))
            //{
            //if (reservacion.Id == 0)
            //    reservacion.PagosReservacion.Clear();

            foreach (var pago in informacionPagos)
            {
                reservacion.PagosReservacion.Add(new PagoReservacion
                {
                    Activo = true,
                    Transaccion = numeroTransaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    TipoPago = pago.TipoPago,
                    Referencia = pago.Referencia,
                    TipoTarjeta = pago.TipoTarjeta,
                    Valor = pago.Valor,
                    NumeroTarjeta = pago.NumeroTarjeta ?? ""
                });
            }

            if (diferenciaConIVA != 0)
            {
                var diferenciaSinIVA = Math.Round(diferenciaConIVA / (1 + valorIVA), 2);

                reservacion.MontosNoReembolsables.Add(new MontoNoReembolsable
                {
                    Activo = true,
                    FechaCreacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    ValorConIVA = diferenciaConIVA,
                    ValorSinIVA = diferenciaSinIVA,
                    ValorIVA = diferenciaConIVA - diferenciaSinIVA
                });
            }
            else if (informacionPagos.Count > 0)
            {
                foreach (var montoNoReembolsable in reservacion.MontosNoReembolsables)
                {
                    if (!montoNoReembolsable.Activo)
                        continue;

                    montoNoReembolsable.Activo = false;
                    montoNoReembolsable.FechaCreacion = fechaActual;
                    montoNoReembolsable.IdUsuarioElimino = idUsuario;
                }
            }
            //}

            //return pagosCancelar;
        }

        public void CrearReservacion(Reservacion reservacion, List<DtoInformacionPago> informacionPagos, DatosFiscales datosFiscales, DtoUsuario usuario) 
        {
            if (reservacion == null)
                throw new SOTException(Recursos.Reservaciones.reservacion_nula_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearReservaciones = true });

            reservacion.FechaEntrada = reservacion.FechaEntrada.Date;//.AddHours(configuracion.ConfiguracionTarifa.DuracionOEntrada);
            reservacion.FechaSalida = reservacion.FechaSalida.Date;//.AddHours(configuracion.ConfiguracionTarifa.HoraSalida.Value);

            ValidarDatosReservacion(reservacion);

            var configuracion = ServicioConfiguracionesTipo.ObtenerConfiguracionTipo(reservacion.IdConfiguracionTipoHabitacion);
            var configuracionGlobal = ServicioParametros.ObtenerParametros();

            if (configuracion == null)
                throw new SOTException(Recursos.Reservaciones.tipo_habitacion_no_seleccionado_excepcion);

            var fechaActual = DateTime.Now;
            var numeroTransaccion = Guid.NewGuid().ToString();

            bool _EsMismoDia =  ServicioPermisos.VerificarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { PermiteReservacionMismoDia = true });

            ValidarNoDupicidad(configuracion, reservacion, fechaActual, _EsMismoDia);

            #region validación de personas extra

            if (configuracion.TipoHabitacion.MaximoPersonasExtra < reservacion.PersonasExtra)
                //throw new SOTException(Recursos.Rentas.personas_extra_fuera_rango_excepcion, configuracion.TipoHabitacion.MaximoPersonasExtra.ToString());
                //Para validar que el usuario que realiza la acción tenga permisos para agregar personas fuera del máximo permitido
                ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarPersonasExtraSobreMax = true });
            #endregion
            #region validación valores

            ValidarValores(configuracion, configuracionGlobal, reservacion, fechaActual, usuario);
            ProcesarPagos(reservacion, informacionPagos, numeroTransaccion, fechaActual, usuario);

            #endregion

            reservacion.FechaCreacion = fechaActual;
            reservacion.FechaModificacion = fechaActual;
            reservacion.IdUsuarioCreo = usuario.Id;
            reservacion.IdUsuarioModifico = usuario.Id;
            reservacion.Estado = Reservacion.EstadosReservacion.Pendiente;

            if (reservacion.Observaciones == null)
                reservacion.Observaciones = "";

            if (reservacion.IdCliente == null)
                reservacion.IdCliente = "";
            reservacion.DatosFiscales = null;

            foreach (var pago in reservacion.PagosReservacion.Where(m => m.Activo))
            {
                pago.Transaccion = numeroTransaccion;
                pago.FechaCreacion = fechaActual;
                pago.FechaModificacion = fechaActual;
                pago.IdUsuarioCreo = usuario.Id;
                pago.IdUsuarioModifico = usuario.Id;
                if (pago.NumeroTarjeta == null)
                    pago.NumeroTarjeta = "";
            }

            var venta = ServicioPagos.GenerarVentaNoCompletada(reservacion.PagosReservacion.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.Reservacion, null);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (datosFiscales != null && datosFiscales.Activo)
                {
                    if (datosFiscales.Id == 0)
                        ServicioDatosFiscales.AgregarDatosFiscales(datosFiscales, usuario);
                    else
                        ServicioDatosFiscales.ActualizarDatosFiscales(datosFiscales, usuario);

                    reservacion.IdDatosFiscales = datosFiscales.Id;
                }

                ServicioPagos.Pagar(venta.Id, reservacion.PagosReservacion.Where(m => m.Activo), usuario);

                RepositorioReservaciones.Agregar(reservacion);
                RepositorioReservaciones.GuardarCambios();

                scope.Complete();
            }
        }

        public void ModificarReservacion(Reservacion reservacion, List<DtoInformacionPago> informacionPagos, DatosFiscales datosFiscales, DtoUsuario usuario)
        {
            if (reservacion == null)
                throw new SOTException(Recursos.Reservaciones.reservacion_nula_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarReservaciones = true });

            reservacion.FechaEntrada = reservacion.FechaEntrada.Date;
            reservacion.FechaSalida = reservacion.FechaSalida.Date;

            ValidarEstaPendiente(reservacion);

            ValidarDatosReservacion(reservacion);

            var configuracion = ServicioConfiguracionesTipo.ObtenerConfiguracionTipo(reservacion.IdConfiguracionTipoHabitacion);
            var configuracionGlobal = ServicioParametros.ObtenerParametros();

            if (configuracion == null)
                throw new SOTException(Recursos.Reservaciones.tipo_habitacion_no_seleccionado_excepcion);

            var fechaActual = DateTime.Now;
            var numeroTransaccion = Guid.NewGuid().ToString();
            bool _EsMismoDia = ServicioPermisos.VerificarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { PermiteReservacionMismoDia = true });
            ValidarNoDupicidad(configuracion, reservacion, fechaActual, _EsMismoDia);

            #region validación de personas extra

            if (configuracion.TipoHabitacion.MaximoPersonasExtra < reservacion.PersonasExtra)
                //throw new SOTException(Recursos.Rentas.personas_extra_fuera_rango_excepcion, configuracion.TipoHabitacion.MaximoPersonasExtra.ToString());
                //Para validar que el usuario que realiza la acción tenga permisos para agregar personas fuera del máximo permitido
                ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarPersonasExtraSobreMax = true });
            #endregion
            #region validación valores

            var configSistema = ServicioParametros.ObtenerParametros();

            ValidarValores(configuracion, configuracionGlobal, reservacion, fechaActual, usuario);
            /*var pagosCancelar = */ProcesarPagos(reservacion, informacionPagos, numeroTransaccion, fechaActual, usuario.Id, configSistema.Iva_CatPar);

            #endregion

            reservacion.FechaModificacion = fechaActual;
            reservacion.IdUsuarioModifico = usuario.Id;
            if (reservacion.Observaciones == null)
                reservacion.Observaciones = "";
            if (reservacion.IdCliente == null)
                reservacion.IdCliente = "";

            reservacion.DatosFiscales = null;

            //bool cancelarPago = pagosCancelar != null && pagosCancelar.Any(m => m.Activo);
            Venta venta;
            List<PagoReservacion> pagosNuevos;

            if (reservacion.PagosReservacion.Any(m => m.Activo && m.Id == 0))//(cancelarPago)
            {
                //foreach (var pagoCancelar in pagosCancelar)
                //{
                //    if (!pagoCancelar.Activo)
                //        continue;

                //    pagoCancelar.Activo = false;
                //    pagoCancelar.FechaEliminacion = fechaActual;
                //    pagoCancelar.FechaModificacion = fechaActual;
                //    pagoCancelar.IdUsuarioModifico = usuario.Id;
                //    pagoCancelar.IdUsuarioElimino = usuario.Id;

                //    //RepositorioPagosReservaciones.Modificar(pagoCancelar);
                //}

                pagosNuevos = reservacion.PagosReservacion.Where(m => m.Activo && m.Id == 0).ToList();
                venta = ServicioPagos.GenerarVentaNoCompletada(pagosNuevos, usuario, Venta.ClasificacionesVenta.Reservacion, null);
            }
            else
            {
                venta = null;
                pagosNuevos = null;
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (datosFiscales != null && datosFiscales.Activo)
                {
                    if (datosFiscales.Id == 0)
                        ServicioDatosFiscales.AgregarDatosFiscales(datosFiscales, usuario);
                    else
                        ServicioDatosFiscales.ActualizarDatosFiscales(datosFiscales, usuario);

                    reservacion.IdDatosFiscales = datosFiscales.Id;
                }

                if (venta != null)
                {
                    //ServicioVentas.CancelarVentasPorTransacciones(pagosCancelar.Select(m => m.Transaccion).Distinct().ToList(), numeroTransaccion, usuario.Id, fechaActual, "Cambio de formas de pago o montos");
                    ServicioPagos.Pagar(venta.Id, pagosNuevos, usuario);
                }

                //if (cancelarPago)
                //    RepositorioPagosReservaciones.GuardarCambios();

                RepositorioReservaciones.Modificar(reservacion);
                RepositorioReservaciones.GuardarCambios();

                scope.Complete();
            }
        }

        public void CancelarReservacion(int idReservacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarReservaciones = true });

            var reservacion = ObtenerReservacionParaEditar(idReservacion, usuario);

            ValidarEstaPendiente(reservacion);

            var fechaActual = DateTime.Now;
            var numeroTransaccion = Guid.NewGuid().ToString();

            reservacion.Activo = false;
            reservacion.FechaModificacion = fechaActual;
            reservacion.FechaEliminacion = fechaActual;
            reservacion.IdUsuarioModifico = usuario.Id;
            reservacion.IdUsuarioElimino = usuario.Id;
            reservacion.Estado = Reservacion.EstadosReservacion.Cancelada;

            foreach (var paquete in reservacion.PaquetesReservaciones.Where(m => m.Activo))
            {
                paquete.Activo = false;
                paquete.FechaEliminacion = fechaActual;
                paquete.IdUsuarioElimino = usuario.Id;
            }

            var transaccionesCancelar = new List<string>();

            foreach (var pagoCancelar in reservacion.PagosReservacion)
            {
                if (!pagoCancelar.Activo)
                    continue;

                pagoCancelar.Activo = false;
                pagoCancelar.FechaEliminacion = fechaActual;
                pagoCancelar.FechaModificacion = fechaActual;
                pagoCancelar.IdUsuarioModifico = usuario.Id;
                pagoCancelar.IdUsuarioElimino = usuario.Id;

                RepositorioPagosReservaciones.Modificar(pagoCancelar);

                transaccionesCancelar.Add(pagoCancelar.Transaccion);
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ServicioVentas.CancelarVentasPorTransacciones(transaccionesCancelar, numeroTransaccion, usuario.Id, fechaActual, "Reservación cancelada");

                RepositorioReservaciones.Modificar(reservacion);
                RepositorioReservaciones.GuardarCambios();

                scope.Complete();
            }
        }

        private void ValidarEstaPendiente(Reservacion reservacion)
        {
            if (reservacion.Estado != Reservacion.EstadosReservacion.Pendiente)
                throw new SOTException(Recursos.Reservaciones.reservacion_no_pendiente_excepcion);
        }

        private void ValidarEstaConfirmada(Reservacion reservacion)
        {
            if (reservacion.Estado != Reservacion.EstadosReservacion.Confirmada)
                throw new SOTException(Recursos.Reservaciones.reservacion_no_confirmada_excepcion);
        }

        public Reservacion ObtenerReservacionParaEditar(int idReservacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarReservaciones = true });

            var reservacion = RepositorioReservaciones.ObtenerReservacionParaEditar(idReservacion);

            if (reservacion == null)
                throw new SOTException(Recursos.Reservaciones.reservacion_nula_excepcion);

            return reservacion;
        }

        private void ValidarDatosReservacion(Reservacion reservacion)
        {
            #region Reservacion

            if (string.IsNullOrWhiteSpace(reservacion.CodigoReserva))
                throw new SOTException(Recursos.Reservaciones.codigo_reserva_nulo_vacio_excepcion);

            if (string.IsNullOrWhiteSpace(reservacion.Nombre))
                throw new SOTException(Recursos.Reservaciones.nombre_nulo_vacio_excepcion);

            if (!UtilidadesRegex.SoloDigitos(reservacion.Telefono))
                throw new SOTException(Recursos.Reservaciones.telefono_invalido_excepcion);

            if (!UtilidadesRegex.Telefono(reservacion.Telefono))
                throw new SOTException(Recursos.Reservaciones.telefono_longitud_invalida_excepcion);

            //if (reservacion.Telefono.Length != 15)
            //    throw new SOTException(Recursos.Reservaciones.telefono_longitud_invalida_excepcion);

            if (!UtilidadesRegex.Email(reservacion.Email))
                throw new SOTException(Recursos.Reservaciones.email_invalido_excepcion);

            //if (string.IsNullOrWhiteSpace(reservacion.IdCliente))
            //    throw new SOTException(Recursos.Reservaciones.id_cliente_nulo_vacio_excepcion);

            if (reservacion.FechaEntrada == default(DateTime))
                throw new SOTException(Recursos.Reservaciones.fecha_entrada_invalida_excepcion, reservacion.FechaEntrada.ToString("dd/MM/yyyy HH:mm:ss"));

            if (reservacion.FechaSalida == default(DateTime))
                throw new SOTException(Recursos.Reservaciones.fecha_salida_invalida_excepcion, reservacion.FechaSalida.ToString("dd/MM/yyyy HH:mm:ss"));

            if (reservacion.FechaEntrada >= reservacion.FechaSalida)
                throw new SOTException(Recursos.Reservaciones.fecha_entrada_mayor_o_igual_fecha_salida_excepcion,
                                       reservacion.FechaEntrada.ToString("dd/MM/yyyy HH:mm:ss"),
                                       reservacion.FechaSalida.ToString("dd/MM/yyyy HH:mm:ss"));

            #endregion
            //#region Facturacion

            ////if (reservacion.DatosFiscales.Count(m => m.Activo) > 1)
            ////    throw new SOTException(Recursos.Reservaciones.multiples_datos_fiscales_excepcion);

            ////var datosFiscales = reservacion.DatosFiscales.FirstOrDefault(m => m.Activo);

            //if (reservacion.DatosFiscales != null && reservacion.DatosFiscales.Activo)
            //    ServicioDatosFiscales.ValidarDatosFiscales(reservacion.DatosFiscales);
            //#endregion
        }

        public List<DtoReservacion> ObtenerReservacionesFiltradas(DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario,string idEstados, int idTipoHabitacion = 0)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarReservaciones = true });

            if (idTipoHabitacion == 0)
                return RepositorioReservaciones.ObtenerReservacionesFiltradas(fechaInicio, fechaFin, idEstados);

            return RepositorioReservaciones.ObtenerReservacionesFiltradas(fechaInicio, fechaFin, idEstados, idTipoHabitacion);																												 
        }
		
        public List<DtoReservacion> ObtenerReservaciones(string reservacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarReservaciones = true });
                
            return RepositorioReservaciones.ObtenerReservaciones(reservacion);
        }

        public List<DtoReservacion> ObtenerReservacionesPorFechaCreacion(DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario, string idEstados, int idTipoHabitacion = 0)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarReservaciones = true });

            return RepositorioReservaciones.ObtenerReservacionesPorFechaCreacion(fechaInicio, fechaFin, idEstados, idTipoHabitacion);
        }		
        public List<Reservacion> ObtenerReservacionesPorCodigo(string codigoReservacion)
        {
            return RepositorioReservaciones.ObtenerElementos(m => m.CodigoReserva == codigoReservacion).ToList();
        }

        /*
        public DtoValor ObtenerValorReservacionesPendientesPorPeriodo(DtoUsuario UsuarioActual, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            return RepositorioReservaciones.ObtenerValorReservacionesPendientesPorPeriodo(fechaInicio, fechaFin);
        }*/


        public int ObtenerCantidadReservacionesPendientesDia(int idTipoHabitacion)
        {

            var idEstadoPendiente = (int)Reservacion.EstadosReservacion.Pendiente;
            //var idEstadoConfirmada = (int)Reservacion.EstadosReservacion.Confirmada;

            var fechaActual = DateTime.Now.Date;

            var cantidad = RepositorioReservaciones.Contador(m => m.ConfiguracionTipo.IdTipoHabitacion == idTipoHabitacion
                                                          && m.IdEstado == idEstadoPendiente// || m.IdEstado == idEstadoConfirmada)
                                                          && m.FechaEntrada == fechaActual);// && m.FechaSalida >= fechaActual);

            int cantidadHabitaciones = ServicioHabitaciones.ObtenerCantidadHabitacionesReservadasPorTipo(idTipoHabitacion);

            return Math.Max(0, cantidad - cantidadHabitaciones);
        }


        public bool VerificarPoseeReservacionNoConsumida(int idHabitacion)
        {
            var idEstadoPendiente = (int)Reservacion.EstadosReservacion.Pendiente;
            var idEstadoConsumido = (int)Reservacion.EstadosReservacion.Consumida;

            return RepositorioReservaciones.Alguno(m => m.IdHabitacion == idHabitacion && (m.IdEstado == idEstadoPendiente || m.IdEstado == idEstadoConsumido));
        }


        public void AsignarHabitacion(int idReservacion, int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarReservaciones = true });

            var reservacion = RepositorioReservaciones.Obtener(m => m.Id == idReservacion);

            if (reservacion == null)
                throw new SOTException(Recursos.Reservaciones.reservacion_nula_excepcion);

            ValidarEstaPendiente(reservacion);

            ServicioHabitaciones.ValidarEstaReservada(idHabitacion);

            var fechaActual = DateTime.Now;

            if (reservacion.FechaEntrada != fechaActual.Date)
                throw new SOTException(Recursos.Reservaciones.fecha_entrada_diferente_actual_excepcion);

            reservacion.FechaModificacion = fechaActual;
            reservacion.IdUsuarioModifico = usuario.Id;
            reservacion.IdHabitacion = idHabitacion;
            reservacion.Estado = Reservacion.EstadosReservacion.Confirmada;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioReservaciones.Modificar(reservacion);
                RepositorioReservaciones.GuardarCambios();

                ServicioHabitaciones.MarcarComoPreparadaReservada(idHabitacion, usuario.Id);

                scope.Complete();
            }
        }

        public void DesasignarHabitacion(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarAnclajeReservaciones = true });

            var estadoConfirmada = (int)Reservacion.EstadosReservacion.Confirmada;

            var reservacion = RepositorioReservaciones.Obtener(m => m.IdHabitacion == idHabitacion && m.IdEstado == estadoConfirmada && m.Activo);

            if (reservacion == null)
                throw new SOTException(Recursos.Reservaciones.reservacion_nula_excepcion);

            ValidarEstaConfirmada(reservacion);

            ServicioHabitaciones.ValidarEstaReservadaPreparada(idHabitacion);

            var fechaActual = DateTime.Now;

            //if (reservacion.FechaEntrada != fechaActual.Date)
            //    throw new SOTException(Recursos.Reservaciones.fecha_entrada_diferente_actual_excepcion);

            reservacion.FechaModificacion = fechaActual;
            reservacion.IdUsuarioModifico = usuario.Id;
            reservacion.IdHabitacion = idHabitacion;
            reservacion.Estado = Reservacion.EstadosReservacion.Pendiente;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioReservaciones.Modificar(reservacion);
                RepositorioReservaciones.GuardarCambios();

                ServicioHabitaciones.HabilitarParaVentas(idHabitacion, usuario.Id);

                scope.Complete();
            }
        }

        public void CambiarHabitacion(int idHabitacionOrigen, int idHabitacionDestino, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarReservaciones = true });

            var idEstadoConfirmada = (int)Reservacion.EstadosReservacion.Confirmada;

            var reservacion = RepositorioReservaciones.Obtener(m => m.IdHabitacion == idHabitacionOrigen && m.IdEstado == idEstadoConfirmada);

            if (reservacion == null)
                throw new SOTException(Recursos.Reservaciones.reservacion_no_confirmada_excepcion);

            //ServicioHabitaciones.ValidarEstaReservada(idHabitacion);

            ServicioHabitaciones.ValidarMismoTipo(idHabitacionOrigen, idHabitacionDestino);

            try
            {
                ServicioHabitaciones.ValidarEstaLibre(idHabitacionDestino);
            }
            catch(Exception e) 
            {
                throw new SOTException("Error en destino: " + e.Message);
            }

            var fechaActual = DateTime.Now;

            //if (reservacion.FechaEntrada != fechaActual.Date)
            //    throw new SOTException(Recursos.Reservaciones.fecha_entrada_diferente_actual_excepcion);

            reservacion.FechaModificacion = fechaActual;
            reservacion.IdUsuarioModifico = usuario.Id;
            reservacion.IdHabitacion = idHabitacionDestino;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioReservaciones.Modificar(reservacion);
                RepositorioReservaciones.GuardarCambios();

                ServicioHabitaciones.HabilitarParaVentas(idHabitacionOrigen, usuario.Id);

                ServicioHabitaciones.MarcarComoPreparadaReservada(idHabitacionDestino, usuario.Id);

                scope.Complete();
            }
        }

        public void ValidarPoseeReservacionConfirmada(int idHabitacion)
        {
            var idEstadoConfirmado = (int)Reservacion.EstadosReservacion.Confirmada;

            if (!RepositorioReservaciones.Alguno(m => m.IdHabitacion == idHabitacion && m.IdEstado == idEstadoConfirmado))
                throw new SOTException(Recursos.Reservaciones.no_reservaciones_confirmadas_habitacion_excepcion);
        }


        public string ObtenerCodigoReservacionActual(int idHabitacion)
        {
            var idEstadoConfirmado = (int)Reservacion.EstadosReservacion.Confirmada;

            return RepositorioReservaciones.ObtenerElementos(m => m.IdHabitacion == idHabitacion && m.IdEstado == idEstadoConfirmado).Select(m => m.CodigoReserva).FirstOrDefault();
        }


        public Reservacion ObtenerReservacionActual(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarReservaciones = true });

            return RepositorioReservaciones.ObtenerReservacionConfimada(idHabitacion);
        }
        #region métodos internal

        Reservacion IServicioReservacionesInterno.ObtenerReservacionPorCodigoConMontosNoReembolsables(string codigoReservacion) 
        {
            return RepositorioReservaciones.ObtenerPorCodigoConMontosNoReembolsables(codigoReservacion);
        }

        Dictionary<TiposPago, decimal> IServicioReservacionesInterno.ObtenerDiccionarioPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos)
        {
            var diccionario = RepositorioReservaciones.ObtenerPagosPorFecha(tiposPagos.Select(m => (int)m).ToList(), fechaInicio, fechaFin);

            if (tiposPagos.Length == 0)
                foreach (var item in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }
            else
                foreach (var item in tiposPagos)
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }

            return diccionario;
        }

        Dictionary<TiposPago, decimal> IServicioReservacionesInterno.ObtenerDiccionarioPagosDeConsumidasPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos)
        {
            var diccionario = RepositorioReservaciones.ObtenerPagosDeConsumidasPorFecha(tiposPagos.Select(m => (int)m).ToList(), fechaInicio, fechaFin);

            if (tiposPagos.Length == 0)
                foreach (var item in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }
            else
                foreach (var item in tiposPagos)
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }

            return diccionario;
        }

        Dictionary<TiposTarjeta, decimal> IServicioReservacionesInterno.ObtenerDiccionarioPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioReservaciones.ObtenerPagosTarjetaPorFecha(fechaInicio, fechaFin);


            foreach (var item in Enum.GetValues(typeof(TiposTarjeta)).Cast<TiposTarjeta>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }

        Dictionary<TiposTarjeta, decimal> IServicioReservacionesInterno.ObtenerDiccionarioPagosTarjetaDeConsumidasPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioReservaciones.ObtenerPagosTarjetaDeConsumidasPorFecha(fechaInicio, fechaFin);


            foreach (var item in Enum.GetValues(typeof(TiposTarjeta)).Cast<TiposTarjeta>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }

        List<PagoReservacion> IServicioReservacionesInterno.ObtenerPagosPorCodigosReserva(List<string> codigosReservaciones)
        {
            return RepositorioPagosReservaciones.ObtenerPagosPorCodigosReserva(codigosReservaciones);
        }

        #endregion
    }
}
