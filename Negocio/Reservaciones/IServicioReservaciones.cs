﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Modelo.Dtos;
using Dominio.Nucleo.Entidades;

namespace Negocio.Reservaciones
{
    public interface IServicioReservaciones
    {
        /// <summary>
        /// Retorna todas las reservaciones que estén dentro del rango de fechas proporcionado, si
        /// idTipoHabitacion = 0 no se aplica filtrado por tipo de habitación
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="idTipoHabitacion"></param>
        /// <returns></returns>
        List<DtoReservacion> ObtenerReservacionesFiltradas(DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario,string idEstado, int idTipoHabitacion = 0);
        /// <summary>
        /// Permite crear una nueva reservación
        /// </summary>
        /// <param name="reservacion"></param>
        /// <param name="usuario"></param>
        void CrearReservacion(Reservacion reservacion, List<DtoInformacionPago> informacionPagos, DatosFiscales datosFiscales, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar una reservación existente
        /// </summary>
        /// <param name="reservacion"></param>
        /// <param name="usuario"></param>
        void ModificarReservacion(Reservacion reservacion, List<DtoInformacionPago> informacionPagos, DatosFiscales datosFiscales, DtoUsuario usuario);
        List<DtoReservacion> ObtenerReservaciones(string reservacion, DtoUsuario usuario);


        List<DtoReservacion> ObtenerReservacionesPorFechaCreacion(DateTime fechaInicio, DateTime fechaFin, DtoUsuario usuario, string idEstado, int idTipoHabitacion = 0);																						  
		
        /// <summary>
        /// Retorna una reservación con la información necesaria para modificarla
        /// </summary>
        /// <param name="idReservacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Reservacion ObtenerReservacionParaEditar(int idReservacion, DtoUsuario usuario);
        /// <summary>
        /// Retorna las reservaciones que coincidan con el código proporcionado (debería ser una)
        /// </summary>
        /// <param name="codigoReservacion"></param>
        /// <returns></returns>
        List<Reservacion> ObtenerReservacionesPorCodigo(string codigoReservacion);
        /*/// <summary>
        /// Retorna el valor de todas las reservaciones q
        /// </summary>
        /// <param name="UsuarioActual"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        DtoValor ObtenerValorReservacionesPendientesPorPeriodo(DtoUsuario UsuarioActual, DateTime? fechaInicio = null, DateTime? fechaFin = null);*/
        /// <summary>
        /// Permite cancelar la reservación con el id proporcionado, siempre y cuando ésta esté en estado "Pendiente"
        /// </summary>
        /// <param name="idReservacion"></param>
        /// <param name="UsuarioActual"></param>
        void CancelarReservacion(int idReservacion, DtoUsuario UsuarioActual);
        /// <summary>
        /// Permite obtener la cantidad de reservaciones no asignadas del día para el tipo de habitación especificado
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        int ObtenerCantidadReservacionesPendientesDia(int idTipoHabitacion);
        /// <summary>
        /// Retorna true en caso de que exista una reservación no consumida o cancelada con el id de
        /// habitación proporcionado y false en caso contrario
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        bool VerificarPoseeReservacionNoConsumida(int idHabitacion);
        /// <summary>
        /// Permite establecer la relación entre una reservación y una habitación
        /// </summary>
        /// <param name="idReservacion"></param>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void AsignarHabitacion(int idReservacion, int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Permite quitar la relación entre una reservación y una habitación
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void DesasignarHabitacion(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Permite cambiar de habitación una reservación confirmada
        /// </summary>
        /// <param name="idHabitacionOrigen"></param>
        /// <param name="idHabitacionDestino"></param>
        /// <param name="usuario"></param>
        void CambiarHabitacion(int idHabitacionOrigen, int idHabitacionDestino, DtoUsuario usuario);
        /// <summary>
        /// Valida que exista una reservación confirmada para la habitación proporcionada
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarPoseeReservacionConfirmada(int idHabitacion);
        /// <summary>
        /// Retorna el código de la reservación con el id de habitación proporcionado que se encuentre confirmada
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        string ObtenerCodigoReservacionActual(int idHabitacion);
        /// <summary>
        /// Retorna la reservación confirmada de la habitación solicitada, o null en caso de no
        /// existir alguna
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Reservacion ObtenerReservacionActual(int idHabitacion, DtoUsuario usuario);
    }
}
