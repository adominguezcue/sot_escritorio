﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Reservaciones
{
    internal interface IServicioReservacionesInterno : IServicioReservaciones
    {
        Reservacion ObtenerReservacionPorCodigoConMontosNoReembolsables(string codigoReservacion);

        Dictionary<TiposPago, decimal> ObtenerDiccionarioPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos);
        Dictionary<TiposPago, decimal> ObtenerDiccionarioPagosDeConsumidasPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos);
        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosTarjetaDeConsumidasPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        List<PagoReservacion> ObtenerPagosPorCodigosReserva(List<string> codigosReservaciones);
    }
}
