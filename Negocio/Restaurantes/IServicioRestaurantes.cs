﻿using Modelo;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Dominio.Nucleo.Entidades;

namespace Negocio.Restaurantes
{
    public interface IServicioRestaurantes
    {
        /// <summary>
        /// Permite marcar una mesa como ocupada, con la finalidad de ir registrando un consumo
        /// </summary>
        /// <param name="idMesa"></param>
        /// <param name="idMesero"></param>
        /// <param name="usuario"></param>
        void MarcarMesaComoOcupada(int idMesa, int idMesero, DtoUsuario usuario);
        /// <summary>
        /// Permite crear una orden nueva, siempre que el usuario posea los permisos necesarios
        /// </summary>
        /// <param name="ordenRestauranteNueva"></param>
        /// <param name="idMesa"></param>
        /// <param name="usuario"></param>
        void CrearOrden(OrdenRestaurante ordenRestauranteNueva, int idMesa, DtoUsuario usuario);//, ConsumoInterno datosConsumo = null);
        /// <summary>
        /// Valida que el usuario posea los permisos necesarios para acceder al módulo de restaurante
        /// </summary>
        /// <param name="usuario"></param>
        void ValidarAcceso(DtoUsuario usuario);
        /// <summary>
        /// Retorna la cantidad de mesas que poseen cuentas abiertas
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        int ObtenerCantidadMesasCuentasAbiertas(DtoUsuario usuario);

        /// <summary>
        /// Permite modificar las órdenes de restaurante
        /// </summary>
        /// <param name="idOrden"></param>
        /// <param name="articulos"></param>
        /// <param name="usuario"></param>
        void ModificarOrden(int idOrden, List<DtoArticuloPrepararGeneracion> articulos, DtoUsuario usuario);

        /// <summary>
        /// Finaliza la elaboración del producto especificado, siempre que el usuario tenga los
        /// permisos adecuados
        /// </summary>
        /// <param name="idOrdenRestauranteArticulo"></param>
        /// <param name="usuario"></param>
        void FinalizarElaboracionProductos(int idOrden, List<int> idsArticulosOrden, DtoCredencial credencial);
        /// <summary>
        /// Permite eliminar una mesa que se encuentra en creación
        /// </summary>
        /// <param name="idMesa"></param>
        /// <param name="usuario"></param>
        void EliminarMesa(int idMesa, DtoUsuario usuario);

        /// <summary>
        /// Marca la orden como entregada al cliente
        /// </summary>
        /// <param name="idOrdenRestaurante"></param>
        /// <param name="idEmpleadoCobro"></param>
        /// <param name="usuario"></param>
        void EntregarOrdenACliente(int idOrdenRestaurante, DtoUsuario usuario);
        /// <summary>
        /// Entrega los productos seleccionados
        /// </summary>
        /// <param name="idOrden"></param>
        /// <param name="idsArticulosOrden"></param>
        /// <param name="idEmpleadoCobro"></param>
        /// <param name="usuario"></param>
        void EntregarProductos(int idOrden, List<int> idsArticulosOrden, DtoCredencial credencial);
        /// <summary>
        /// Permite cancelar una orden, siempre y cuando la huella digital corresponda con la de un
        /// usuario válido y con los permisos necesarios
        /// </summary>
        /// <param name="idOrdenRestaurante"></param>
        /// <param name="motivo"></param>
        /// <param name="huellaDigital"></param>
        void CancelarOrden(int idOrdenRestaurante, string motivo, DtoCredencial credencial);
        string RepararCancelacionesOcupacion(string motivoX, DtoCredencial credencial);
        /// <summary>
        /// Permite cancelar las órdenes de una mesa, siempre y cuando la huella digital corresponda con la de un
        /// usuario válido y con los permisos necesarios
        /// </summary>
        /// <param name="idOcupacion"></param>
        /// <param name="motivo"></param>
        /// <param name="huellaDigital"></param>
        void CancelarOcupacion(int idOcupacion, string motivo, DtoCredencial credencial);
        /// <summary>
        /// </summary>
        /// <param name="usuarioActual"></param>
        /// <returns></returns>
        List<Mesa> ObtenerMesas(DtoUsuario usuarioActual);
        /// <summary>
        /// Retorna las ordenes que aun no han sido cobradas o canceladas relacionadas con la mesa
        /// que posee el id proporcionado
        /// </summary>
        /// <param name="idMesa"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<OrdenRestaurante> ObtenerDetallesOrdenesPendientes(int idMesa, DtoUsuario usuario);
        /// <summary>
        /// Retorna los artículos de las órdenes que fueron entregadas en una ocupación de mesa
        /// </summary>
        /// <param name="idOcupacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<ArticuloOrdenRestaurante> ObtenerDetallesOrdenesEntregadasCliente(int idOcupacion, DtoUsuario usuario);
        ///// <summary>
        ///// Retorna la información de los artículos a preparar las ordenes en restaurante
        ///// </summary>
        ///// <param name="usuario"></param>
        ///// <returns></returns>
        //List<DtoArticuloPrepararConsulta> ObtenerArticulosPrepararOrdenes(DtoUsuario usuario);
        /// <summary>
        /// Retorna la mesa activa que coincida con el id proporcionado, si no existen
        /// coincidencias se arrojará una excepción
        /// </summary>
        /// <param name="idMesa"></param>
        /// <returns></returns>
        Mesa ObtenerMesa(int idMesa);
        /// <summary>
        /// Verifica si existen comandas pendientes por entregar en una mesa ocupada (idOcupacion)
        /// </summary>
        /// <param name="idOcupacion"></param>
        /// <returns></returns>
        bool VerificarTieneOrdenesPendientes(int idOcupacion);

        /// <summary>
        /// Retorna la ocupación activa y cargada que involucre a la mesa con el id proporcionado o null en
        /// caso de no existir alguna
        /// </summary>
        /// <param name="idMesa"></param>
        /// <returns></returns>
        OcupacionMesa ObtenerOcupacionActualPorMesa(int idMesa);
        /// <summary>
        /// Retorna la ocupación activa que involucre a la mesa con el id proporcionado o null en
        /// caso de no existir alguna
        /// </summary>
        /// <param name="idMesa"></param>
        /// <returns></returns>
        OcupacionMesa ObtenerOcupacionBaseActualPorMesa(int idMesa);
        ///// <summary>
        ///// Obtiene el valor a cobrar de una mesa ocupada en base a todas las órdenes entregadas a
        ///// los clientes actuales
        ///// </summary>
        ///// <param name="idMesa"></param>
        ///// <returns></returns>
        //DtoValor ObtenerValorCobrarMesa(int idMesa);
        /// <summary>
        /// Valida que no existan comandas pendientes por entregar en una mesa ocupada (idOcupacion)
        /// </summary>
        /// <param name="idOcupacion"></param>
        /// <returns></returns>
        void ValidarNoTieneOrdenesPendientes(int idOcupacion);
        /// <summary>
        /// Permite cobrar las órdenes del cliente actual de una mesa empleando las formas de pago seleccionadas
        /// </summary>
        /// <param name="idMesa"></param>
        /// <param name="formasPago"></param>
        /// <param name="usuario"></param>
        void CobrarMesa(int idMesa, List<DtoInformacionPago> formasPago, Propina propina, int idMeseroCobro, bool marcarComoCortesia, DtoUsuario usuario);
       
        /// <summary>
        /// Retorna la mesa relacionada con la ocupación
        /// </summary>
        /// <param name="idOcupacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Mesa ObtenerMesaPorOcupacion(int idOcupacion, DtoUsuario usuario);
        /// <summary>
        /// Marca una mesa como pendiente de cobro
        /// </summary>
        /// <param name="idMesa"></param>
        /// <param name="usuario"></param>
        void MarcarMesaComoPorCobrar(int idMesa, List<int> idsComandasCortesias, DtoUsuario usuario);
        /// <summary>
        /// Retorna los datos de la mesa actual
        /// </summary>
        /// <param name="idMesa"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        DtoDatosMesa ObtenerDatosMesaActiva(int idMesa, DtoUsuario usuario);
        void FinalizarConfiguracionMesas(DtoUsuario usuario);

        ///// <summary>
        ///// Retorna las comandas con artículos en preparación que serán procesadas en el area especificada
        ///// </summary>
        ///// <returns></returns>
        //List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosEnPreparacionDepartamento(string departamento);
        ///// <summary>
        ///// Retorna las comandas con artículos preparados que serán procesadas en el área especificada
        ///// </summary>
        ///// <returns></returns>
        //List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosPreparadosDepartamento(string departamento);
        /// <summary>
        /// Retorna las comandas con artículos preparados o en preparación que serán procesadas en el área especificada
        /// </summary>
        /// <returns></returns>
        List<DtoResumenComandaExtendido> ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(string departamento);
        
        /// <summary>
        /// Permite actualizar la información del pago
        /// </summary>
        /// <param name="idPago"></param>
        /// <param name="formaPago"></param>
        /// <param name="valorPropina"></param>
        /// <param name="referencia"></param>
        /// <param name="idEmpleado"></param>
        /// <param name="usuario"></param>
        /// <param name="numeroTarjeta"></param>
        /// <param name="tipoTarjeta"></param>
        void ActualizarPagoOcupacion(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, int idEmpleado, DtoUsuario usuario, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null);
        /// <summary>
        /// Retorna el último número de orden por turno o cero si no hay una
        /// </summary>
        /// <returns></returns>
        int ObtenerUltimoNumeroOrden(int idTurno);
        /// <summary>
        /// Retorna los datos fiscales vinculados a la renta a la que pertenece la operación con la transacción proporcionada
        /// </summary>
        /// <param name="transaccion"></param>
        DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion);
        /// <summary>
        /// Si el id de datos fiscales es diferente de null, la ocupación con el id proporcionado quedara
        /// vinculada a esa información, en caso contrario perderá cualquier vinculación con datos fiscales
        /// </summary>
        /// <param name="idOcupacionMesa"></param>
        /// <param name="idDatosFiscales"></param>
        /// <param name="usuario"></param>
        void VincularDatosFiscales(int idOcupacionMesa, int? idDatosFiscales, DtoUsuario usuario);
        /// <summary>
        /// Permite intercambiar la ocupación entre una mesa ocupada y una libre
        /// </summary>
        /// <param name="idMesaOrigen"></param>
        /// <param name="idMesaDestino"></param>
        /// <param name="UsuarioActual"></param>
        void IntercambiarMesas(int idMesaOrigen, int idMesaDestino, DtoUsuario usuario);

        void ReimprimirTicket(int idMesa);

        void Imprimir(int idOrden);
        void CrearMesa(Mesa mesa, DtoUsuario usuario);
        void ModificarMesa(Mesa mesa, DtoUsuario usuario);
    }
}
