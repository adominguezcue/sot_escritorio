﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Restaurantes
{
    internal interface IServicioRestaurantesInterno : IServicioRestaurantes
    {
        /// <summary>
        /// Retorna el número de mesa relacionada con una transacción
        /// </summary>
        /// <param name="numeroTransaccion"></param>
        /// <returns></returns>
        string ObtenerNumeroMesaPorTransaccion(string numeroTransaccion);
        /// <summary>
        /// Retorna el valor de los artículos de comandas cobradas dentro del período especificado,
        /// divididos por tipo de artículo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<DtoEmpleadoLineaArticuloValor> ObtenerValorArticulosOrdenesPorPeriodoYLineas(List<int> idsTipos, List<int> idsCortes);
        /// <summary>
        /// Permite obtener el valor de todos los pagos de restaurante realizados en el rango de
        /// fechas solicitados.
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        decimal ObtenerPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Permite obtener los pagos de las mesas en un período determinado. Si se especifican
        /// tipos de pago solamente se incluirán los pagos de esos tipos
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="tiposPagos"></param>
        /// <returns></returns>
        Dictionary<TiposPago, decimal> ObtenerDiccionarioPagosMesaPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos);
        /// <summary>
        /// Retorna las ocupaciones de mesa que se encuentran finalizadas dentro del período especificado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="formasPago"></param>
        /// <returns></returns>
        List<OcupacionMesa> ObtenerOcupacionesCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago);
        /// <summary>
        /// Permite obtener los pagos con tarjeta de las mesas en un período determinado.
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <param name="tiposPagos"></param>
        /// <returns></returns>
        Dictionary<TiposTarjeta, decimal> ObtenerDiccionarioPagosMesaTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Valida que no existan mesas por liberar u órdenes no cobradas
        /// </summary>
        /// <param name="usuario"></param>
        void ValidarNoExistenPagosPendientes(int? idCorte);

        bool VerificarNoExistenDependenciasArticulo(string codigoArticulo);
    }
}
