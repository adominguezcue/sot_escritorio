﻿using Modelo;
using Modelo.Dtos;
using Modelo.Repositorios;
using Negocio.Pagos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Entidades;
using Negocio.Empleados;
using Negocio.Propinas;
using Modelo.Seguridad.Dtos;
using Negocio.ConsumosInternos;
using Negocio.Almacen.Articulos;
using Negocio.CortesTurno;
using Negocio.Rentas;
using Negocio.ConfiguracionesGlobales;
using Negocio.ConfiguracionesImpresoras;
using Negocio.Tickets;
using Dominio.Nucleo.Entidades;
using Negocio.Almacen.Parametros;
using Negocio.Almacen.OrdenesTrabajo;
using Negocio.Almacen.Inventarios;
using Modelo.Almacen.Constantes;
using Negocio.Preventas;
using Negocio.RoomServices;

namespace Negocio.Restaurantes
{
    public class ServicioRestaurantes : IServicioRestaurantesInterno
    {
        private static object bloqueadorOrdenes = new object();

        IServicioRoomServicesInterno ServicioRoomServices
        {
            get { return _servicioRoomServices.Value; }
        }

        Lazy<IServicioRoomServicesInterno> _servicioRoomServices = new Lazy<IServicioRoomServicesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRoomServicesInterno>(); });

        IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });


        IServicioRentas ServicioRentas
        {
            get { return _servicioRentas.Value; }
        }

        Lazy<IServicioRentas> _servicioRentas = new Lazy<IServicioRentas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRentas>(); });

        IServicioInventarios ServicioInventarios
        {
            get { return _servicioInventarios.Value; }
        }

        Lazy<IServicioInventarios> _servicioInventarios = new Lazy<IServicioInventarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioInventarios>(); });

        IServicioTicketsInterno ServicioTickets
        {
            get { return _servicioTickets.Value; }
        }

        Lazy<IServicioTicketsInterno> _servicioTickets = new Lazy<IServicioTicketsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTicketsInterno>(); });

        IServicioCortesTurno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurno> _servicioCortesTurno = new Lazy<IServicioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IServicioConsumosInternosInterno ServicioConsumosInternos
        {
            get { return _servicioConsumosInternos.Value; }
        }

        Lazy<IServicioConsumosInternosInterno> _servicioConsumosInternos = new Lazy<IServicioConsumosInternosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternosInterno>(); });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioConfiguracionesImpresorasInterno ServicioConfiguracionesImpresoras
        {
            get { return _servicioConfiguracionesImpresoras.Value; }
        }

        Lazy<IServicioConfiguracionesImpresorasInterno> _servicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresorasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresorasInterno>(); });

        IServicioPagosInterno ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagosInterno> _servicioPagos = new Lazy<IServicioPagosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagosInterno>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioPropinasInterno ServicioPropinas
        {
            get { return _servicioPropinas.Value; }
        }

        Lazy<IServicioPropinasInterno> _servicioPropinas = new Lazy<IServicioPropinasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPropinasInterno>(); });

        IRepositorioOrdenesRestaurantes RepositorioOrdenesRestaurantes
        {
            get { return _repositorioOrdenesRestaurantes.Value; }
        }

        Lazy<IRepositorioOrdenesRestaurantes> _repositorioOrdenesRestaurantes = new Lazy<IRepositorioOrdenesRestaurantes>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOrdenesRestaurantes>(); });

        IRepositorioMesas RepositorioMesas
        {
            get { return _repositorioMesas.Value; }
        }

        Lazy<IRepositorioMesas> _repositorioMesas = new Lazy<IRepositorioMesas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioMesas>(); });

        IRepositorioPagosOcupacionesMesa RepositorioPagosOcupacionesMesa
        {
            get { return _repositorioPagosOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioPagosOcupacionesMesa> _repositorioPagosOcupacionesMesa = new Lazy<IRepositorioPagosOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosOcupacionesMesa>(); });


        IRepositorioOcupacionesMesa RepositorioOcupacionesMesa
        {
            get { return _repositorioOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioOcupacionesMesa> _repositorioOcupacionesMesa = new Lazy<IRepositorioOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOcupacionesMesa>(); });

        public void MarcarMesaComoOcupada(int idMesa, int idMesero, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AbrirMesa = true });

            ValidarMesaActivaHabilitada(idMesa);

            var mesa = RepositorioMesas.Obtener(m => m.Activa && m.Id == idMesa);

            ValidarMesaLibre(mesa);

            var fechaActual = DateTime.Now;
            var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();
            mesa.Estado = Mesa.EstadosMesa.Ocupada;
            mesa.FechaModificacion = fechaActual;
            mesa.IdUsuarioModifico = usuario.Id;

            var nuevaOcupacion = new OcupacionMesa
            {
                Activa = true,
                Transaccion = "",//Guid.NewGuid().ToString(),
                IdMesero = idMesero,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                IdUsuarioModifico = usuario.Id,
                Estado = OcupacionMesa.Estados.EnProceso,
                IdCorte = corteActual.Id,
                MotivoCancelacion = string.Empty
            };

            mesa.OcupacionesMesa.Add(nuevaOcupacion);

            nuevaOcupacion.IdPreventa = ServicioPreventas.GrenerarPreventa(Venta.ClasificacionesVenta.Restaurante);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioMesas.Modificar(mesa);
                RepositorioMesas.GuardarCambios();

                scope.Complete();
            }
        }

        public void CrearOrden(OrdenRestaurante ordenRestaurante, int idMesa, DtoUsuario usuario)//, ConsumoInterno datosConsumo = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearOrdenRestaurante = true });

            if (ordenRestaurante == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

            if (ordenRestaurante.ArticulosOrdenRestaurante.Count(m => m.Activo) == 0)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_sin_productos_excepcion);

            ValidarMesaActivaHabilitada(idMesa);

            var configGlobal = ServicioParametros.ObtenerParametros();
            var mesa = RepositorioMesas.Obtener(m => m.Activa && m.Id == idMesa);

            ValidarMesaOcupada(mesa);

            var ocupacion = ObtenerOcupacionActualPorMesa(idMesa);

            //if (ocupacion != null)
            //    ValidarOcupacionActiva(ocupacion.Id);
            //else
            //{
            //    ValidarMesaLibre(mesa);
            //    ServicioEmpleados.ValidarMeseroActivoHabilitado(ordenRestaurante.IdMesero);
            //}

            decimal valorConIVA = 0;

            foreach (var ordenRestauranteArticulo in ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo))
            {
                if (ordenRestauranteArticulo.Cantidad <= 0)
                    throw new SOTException(Recursos.OrdenesRestaurantes.cantidad_articulo_invalida_excepcion);

                if (ordenRestauranteArticulo.PrecioUnidad == 0 && !ordenRestauranteArticulo.EsCortesia)
                    throw new SOTException(Recursos.OrdenesRestaurantes.articulos_precio_cero_no_cortesia_excepcion);

                ServicioArticulos.ValidarIntegridadPrecio(ordenRestauranteArticulo.IdArticulo, ordenRestauranteArticulo.PrecioUnidad);
                ServicioArticulos.ValidarDisponibilidad(ordenRestauranteArticulo.IdArticulo, ordenRestauranteArticulo.Cantidad);

                valorConIVA += Math.Round(ordenRestauranteArticulo.PrecioUnidad * (1 + configGlobal.Iva_CatPar), 2) * ordenRestauranteArticulo.Cantidad;
            }
            /*
            if (!ordenRestaurante.PagosOrdenRestaurante.Any(m=>m.Activo))
                throw new SOTException(Recursos.OrdenesRestaurantes.pago_incorrecto_excepcion,  ordenRestaurante.Valor.ToString("C"), "$0.00");

            var valorPagos = ordenRestaurante.PagosOrdenRestaurante.Where(m => m.Activo).Sum(m => m.Valor);

            if (ordenRestaurante.Valor != valorPagos)
                throw new SOTException(Recursos.OrdenesRestaurantes.pago_incorrecto_excepcion,  ordenRestaurante.Valor.ToString("C"),  valorPagos.ToString("C"));
            */

            var fechaActual = DateTime.Now;
            var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();
            int siguienteNumero = ObtenerSiguienteNumeroComandaOrden(corteActual.Id);

            ordenRestaurante.ValorConIVA = valorConIVA;
            ordenRestaurante.ValorSinIVA = ordenRestaurante.ValorConIVA / (1 + configGlobal.Iva_CatPar);
            ordenRestaurante.ValorIVA = ordenRestaurante.ValorConIVA - ordenRestaurante.ValorSinIVA;

            ordenRestaurante.Orden = siguienteNumero;
            //ordenRestaurante.IdCorte = corteActual.Id;
            ordenRestaurante.FechaCreacion = fechaActual;
            ordenRestaurante.FechaModificacion = fechaActual;
            ordenRestaurante.FechaInicio = fechaActual;
            ordenRestaurante.IdUsuarioCreo = usuario.Id;
            ordenRestaurante.IdUsuarioModifico = usuario.Id;
            ordenRestaurante.Estado = Comanda.Estados.Preparacion;
            ordenRestaurante.MotivoCancelacion = string.Empty;
            if (ordenRestaurante.Observaciones == null)
                ordenRestaurante.Observaciones = "";

            ordenRestaurante.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = ordenRestaurante.Estado,
                FechaInicio = fechaActual
            });

            foreach (var ordenRestauranteArticulo in ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo))
            {
                ordenRestauranteArticulo.FechaCreacion = fechaActual;
                ordenRestauranteArticulo.FechaModificacion = fechaActual;
                ordenRestauranteArticulo.IdUsuarioCreo = usuario.Id;
                ordenRestauranteArticulo.IdUsuarioModifico = usuario.Id;
                ordenRestauranteArticulo.Estado = ArticuloComanda.Estados.EnProceso;
                if (ordenRestauranteArticulo.Observaciones == null)
                    ordenRestauranteArticulo.Observaciones = "";
            }

            //bool actualizarMesa = false;

            //if (ocupacion == null)
            //{
            //    actualizarMesa = true;

            //    mesa.Estado = Mesa.EstadosMesa.Ocupada;
            //    mesa.FechaModificacion = fechaActual;
            //    mesa.IdUsuarioModifico = usuario.Id;

            //    var nuevaOcupacion = new OcupacionMesa
            //    {
            //        Activa = true,
            //        IdMesero = ordenRestaurante.IdMesero,
            //        FechaCreacion = fechaActual,
            //        FechaModificacion = fechaActual,
            //        IdUsuarioCreo = usuario.Id,
            //        IdUsuarioModifico = usuario.Id,
            //        Estado = OcupacionMesa.Estados.EnProceso
            //    };

            //    nuevaOcupacion.OrdenesRestaurante.Add(ordenRestaurante);

            //    mesa.OcupacionesMesa.Add(nuevaOcupacion);
            //}
            //else 
            //{
            ordenRestaurante.IdOcupacionMesa = ocupacion.Id;
            //}

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                //ServicioPagos.Pagar(ordenRestaurante.PagosOrdenRestaurante.Where(m => m.Activo), usuario);

                //if (actualizarMesa)
                //{
                //    RepositorioMesas.Modificar(mesa);
                //    RepositorioMesas.GuardarCambios();
                //}
                //else
                //{

                //ServicioConsumosInternos.CrearConsumoInterno(datosConsumo, usuario);
                //ordenRestaurante.IdConsumoInterno = datosConsumo.Id;

                RepositorioOrdenesRestaurantes.Agregar(ordenRestaurante);
                RepositorioOrdenesRestaurantes.GuardarCambios();

                RegistrarMovimiento(ordenRestaurante);

                //}
                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicket(ordenRestaurante, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(ordenRestaurante, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            //ServicioTickets.ImprimirTicket(ordenRestaurante, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            //ServicioTickets.ImprimirTicket(ordenRestaurante, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }

        public void Imprimir(int idOrden)//, ConsumoInterno datosConsumo = null)
        {
            
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicketOrdenRestaurante(idOrden, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicketOrdenRestaurante(idOrden, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            //ServicioTickets.ImprimirTicket(ordenRestaurante, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            //ServicioTickets.ImprimirTicket(ordenRestaurante, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }

        private int ObtenerSiguienteNumeroComandaOrden(int idTurno)
        {
            return Math.Max(Math.Max(ObtenerUltimoNumeroOrden(idTurno), ServicioRoomServices.ObtenerUltimoNumeroComanda(idTurno)), ServicioConsumosInternos.ObtenerUltimoNumeroConsumoInterno(idTurno)) + 1;
        }

        private void ValidarMesaLibre(Mesa mesa)
        {
            if (mesa.Estado != Mesa.EstadosMesa.Libre)
                throw new SOTException(Recursos.Mesas.mesa_no_libre_excepcion, mesa.Clave);
        }

        private void ValidarOcupacionActiva(int idOcupacion)
        {
            if (!RepositorioOcupacionesMesa.Alguno(m => m.Activa && m.Id == idOcupacion))
                throw new SOTException(Recursos.OrdenesRestaurantes.ocupacion_mesa_invalida_excepcion);
        }

        public void ModificarOrden(int idOrden, List<DtoArticuloPrepararGeneracion> articulos, DtoUsuario usuario)
        {
            var configGlobal = ServicioParametros.ObtenerParametros();
            var ordenRestaurante = RepositorioOrdenesRestaurantes.ObtenerParaCobrar(idOrden);

            if (ordenRestaurante == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

		            //Antonio de jesus Domínguez Cuevas 11/11/2019 Actualiza para que solo gerente haga la modificacion																										
          /*  if (ordenRestaurante.Estado != Comanda.Estados.Preparacion &&
if (ordenRestaurante.Estado != Comanda.Estados.Preparacion &&
                ordenRestaurante.Estado != Comanda.Estados.PorEntregar)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_modificable_excepcion);*/

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarOrdenRestaurante = true });

            var fechaActual = DateTime.Now;

            foreach (var ordenRestauranteArticulo in articulos.Where(m => m.Activo))
            {
                if (ordenRestauranteArticulo.Cantidad <= 0)
                    throw new SOTException(Recursos.OrdenesRestaurantes.cantidad_articulo_invalida_excepcion);

                if (ordenRestauranteArticulo.PrecioConIVA == 0 && !ordenRestauranteArticulo.EsCortesia)
                    throw new SOTException(Recursos.OrdenesRestaurantes.articulos_precio_cero_no_cortesia_excepcion);
            }

            var articulosOriginales = new List<DtoArticuloPrepararGeneracion>();

            foreach (var articulo in ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo))
            {
                articulosOriginales.Add(new DtoArticuloPrepararGeneracion
                {
                    Activo = true,
                    Cantidad = articulo.Cantidad,
                    EsCortesia = articulo.EsCortesia,
                    Id = articulo.Id,
                    IdArticulo = articulo.IdArticulo,
                    Observaciones = articulo.Observaciones,
                    PrecioConIVA = articulo.PrecioUnidad
                });
            }


            foreach (var tupla in (from artGeneracion in articulos
                                   join articuloOrden in ordenRestaurante.ArticulosOrdenRestaurante
                                    on new { artGeneracion.Id, Activo = true } equals new { articuloOrden.Id, articuloOrden.Activo } into x
                                   from articuloOrden in x.DefaultIfEmpty()
                                   select new
                                   {
                                       artGeneracion,
                                       articuloOrden
                                   }).ToList())
            {
                if (tupla.articuloOrden == null)
                {
                    if (tupla.artGeneracion.Id <= 0)
                    {
                        ServicioArticulos.ValidarIntegridadPrecio(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.PrecioConIVA);
                        ServicioArticulos.ValidarDisponibilidad(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad);

                        ordenRestaurante.ArticulosOrdenRestaurante.Add(new ArticuloOrdenRestaurante
                        {
                            Activo = true,
                            IdArticulo = tupla.artGeneracion.IdArticulo,
                            PrecioUnidad = tupla.artGeneracion.PrecioConIVA,
                            Cantidad = tupla.artGeneracion.Cantidad,
                            FechaCreacion = fechaActual,
                            FechaModificacion = fechaActual,
                            IdUsuarioCreo = usuario.Id,
                            IdUsuarioModifico = usuario.Id,
                            Observaciones = tupla.artGeneracion.Observaciones ?? "",
                            Estado = ArticuloComanda.Estados.EnProceso,
                            EsCortesia = tupla.artGeneracion.EsCortesia
                        });
                    }
                }
                else
                {
                    /*if (tupla.articuloOrden.Estado != ArticuloComanda.Estados.EnProceso
                        && tupla.articuloOrden.Estado != ArticuloComanda.Estados.PorEntregar)
                        throw new SOTException(Recursos.OrdenesRestaurantes.articulo_orden_no_modificable_excepcion);*/

                    if (!tupla.artGeneracion.Activo)
                    {
                        tupla.articuloOrden.Activo = false;
                        tupla.articuloOrden.Estado = ArticuloComanda.Estados.Cancelado;
                        tupla.articuloOrden.FechaEliminacion = fechaActual;
                        tupla.articuloOrden.FechaModificacion = fechaActual;
                        tupla.articuloOrden.IdUsuarioElimino = usuario.Id;
                        tupla.articuloOrden.IdUsuarioModifico = usuario.Id;
                    }
                    else
                    {
                        if (tupla.articuloOrden.Cantidad != tupla.artGeneracion.Cantidad 
                            || tupla.articuloOrden.Observaciones != (tupla.artGeneracion.Observaciones ?? "")
                            || tupla.articuloOrden.EsCortesia != tupla.artGeneracion.EsCortesia)
                        {
                            if (tupla.articuloOrden.Cantidad < tupla.artGeneracion.Cantidad)
                                ServicioArticulos.ValidarDisponibilidad(tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad);

                            if (tupla.articuloOrden.Cantidad != tupla.artGeneracion.Cantidad && tupla.articuloOrden.PrecioUnidad.ToString("C") != tupla.artGeneracion.PrecioConIVA.ToString("C"))
                            {
                                var articulo = ServicioArticulos.ObtenerArticuloPorId(tupla.articuloOrden.IdArticulo);
                                throw new SOTException(Recursos.OrdenesRestaurantes.precio_articulo_modificado_diferente_excepcion,
                                                       articulo.Desc_Art, (tupla.articuloOrden.PrecioUnidad * (1 + configGlobal.Iva_CatPar)).ToString("C"),
                                                       (tupla.artGeneracion.PrecioConIVA * (1 + configGlobal.Iva_CatPar)).ToString("C"));
                            }

                            tupla.articuloOrden.Cantidad = tupla.artGeneracion.Cantidad;
                            tupla.articuloOrden.Estado = ArticuloComanda.Estados.EnProceso;
                            tupla.articuloOrden.FechaModificacion = fechaActual;
                            tupla.articuloOrden.IdUsuarioModifico = usuario.Id;
                            tupla.articuloOrden.Observaciones = tupla.artGeneracion.Observaciones ?? "";
                            tupla.articuloOrden.EsCortesia = tupla.artGeneracion.EsCortesia;
                        }
                    }
                }
            }

            if (!ordenRestaurante.ArticulosOrdenRestaurante.Any(m => m.Activo))
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_sin_productos_excepcion);

            decimal valorConIVA = 0;

            foreach (var comandaArticulo in ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo))
                valorConIVA += Math.Round(comandaArticulo.PrecioUnidad * (1 + configGlobal.Iva_CatPar), 2) * comandaArticulo.Cantidad;

            if (ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.Entregado))
                ordenRestaurante.Estado = Comanda.Estados.PorCobrar;
            else if (ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.PorEntregar || m.Estado == ArticuloComanda.Estados.Entregado))
                ordenRestaurante.Estado = Comanda.Estados.PorEntregar;
            else
                ordenRestaurante.Estado = Comanda.Estados.Preparacion;

            ordenRestaurante.ValorConIVA = valorConIVA;
            ordenRestaurante.ValorSinIVA = ordenRestaurante.ValorConIVA / (1 + configGlobal.Iva_CatPar);
            ordenRestaurante.ValorIVA = ordenRestaurante.ValorConIVA - ordenRestaurante.ValorSinIVA;
            ordenRestaurante.FechaModificacion = fechaActual;
            ordenRestaurante.FechaInicio = fechaActual;
            ordenRestaurante.IdUsuarioCreo = usuario.Id;
            ordenRestaurante.IdUsuarioModifico = usuario.Id;

            if (ordenRestaurante.Observaciones == null)
                ordenRestaurante.Observaciones = "";

            ordenRestaurante.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = ordenRestaurante.Estado,
                FechaInicio = fechaActual
            });

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioOrdenesRestaurantes.Modificar(ordenRestaurante);
                RepositorioOrdenesRestaurantes.GuardarCambios();

                ActualizarMovimientos(ordenRestaurante.Id, articulosOriginales, articulos, fechaActual);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicket(ordenRestaurante, true, configuracionImpresoras.ImpresoraCocina, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            ServicioTickets.ImprimirTicket(ordenRestaurante, true, configuracionImpresoras.ImpresoraBar, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);

            //ServicioTickets.ImprimirTicket(ordenRestaurante, false, configuracionImpresoras.ImpresoraBar, 2, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            //ServicioTickets.ImprimirTicket(ordenRestaurante, false, configuracionImpresoras.ImpresoraTickets, 1, null, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
        }

        public void FinalizarElaboracionProductos(int idOrden, List<int> idsArticulosOrden, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { PrepararProductos = true });

            //ServicioEmpleados.ValidarCocineroActivoHabilitado(usuario.Id);

            lock (bloqueadorOrdenes)
            {
                OrdenRestaurante ordenRestaurante = RepositorioOrdenesRestaurantes.ObtenerPorArticulos(idOrden, idsArticulosOrden);

                if (ordenRestaurante == null)
                    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

                if (ordenRestaurante.Estado != Comanda.Estados.Preparacion)
                    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_preparacion_excepcion);

                var fechaActual = DateTime.Now;

                foreach (var idArticuloOrden in idsArticulosOrden)
                {
                    var ordenRestauranteArticulo = ordenRestaurante.ArticulosOrdenRestaurante.FirstOrDefault(m => m.Id == idArticuloOrden);

                    if (ordenRestauranteArticulo.Estado != ArticuloComanda.Estados.EnProceso)
                        throw new SOTException(Recursos.OrdenesRestaurantes.orden_articulos_no_preparacion_excepcion);

                    ordenRestauranteArticulo.FechaModificacion = fechaActual;
                    ordenRestauranteArticulo.IdUsuarioModifico = usuario.Id;

                    ordenRestauranteArticulo.Estado = ArticuloComanda.Estados.PorEntregar;
                }

                if (ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.PorEntregar || m.Estado == ArticuloComanda.Estados.Entregado))
                {
                    ordenRestaurante.FechaModificacion = DateTime.Now;
                    ordenRestaurante.IdUsuarioCreo = usuario.Id;
                    ordenRestaurante.Estado = Comanda.Estados.PorEntregar;

                    ordenRestaurante.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
                    {
                        EntidadEstado = EntidadEstados.Creado,
                        Estado = ordenRestaurante.Estado,
                        FechaInicio = fechaActual
                    });
                }

                RepositorioOrdenesRestaurantes.Modificar(ordenRestaurante);
                RepositorioOrdenesRestaurantes.GuardarCambios();
            }
        }

        public void EntregarOrdenACliente(int idOrdenRestaurante, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { MarcarEntregadaOrden = true });

            var ordenRestaurante = RepositorioOrdenesRestaurantes.ObtenerParaEntregarCliente(idOrdenRestaurante);

            if (ordenRestaurante == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

            if (ordenRestaurante.Estado != Comanda.Estados.PorCobrar)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_entregada_mesero_excepcion);

            //ServicioEmpleados.ValidarMeseroActivoHabilitado(ordenRestaurante.OcupacionMesa.IdMesero);

            var fechaActual = DateTime.Now;

            ordenRestaurante.Activo = false;
            ordenRestaurante.FechaModificacion = fechaActual;
            ordenRestaurante.FechaEliminacion = fechaActual;
            ordenRestaurante.IdUsuarioCreo = usuario.Id;
            ordenRestaurante.IdUsuarioElimino = usuario.Id;
            ordenRestaurante.Estado = Comanda.Estados.PorPagar;

            ordenRestaurante.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = ordenRestaurante.Estado,
                FechaInicio = fechaActual
            });

            foreach (var articulo in ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo))
            {
                articulo.Activo = false;
                articulo.FechaEliminacion = fechaActual;
                articulo.FechaModificacion = fechaActual;
                articulo.IdUsuarioModifico = usuario.Id;
                articulo.IdUsuarioElimino = usuario.Id;
            }

            RepositorioOrdenesRestaurantes.Modificar(ordenRestaurante);
            RepositorioOrdenesRestaurantes.GuardarCambios();
        }

        public void EntregarProductos(int idOrden, List<int> idsArticulosOrden, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { Meserear = true });

            OrdenRestaurante ordenRestaurante;

            lock (bloqueadorOrdenes)
            {
                ordenRestaurante = RepositorioOrdenesRestaurantes.ObtenerPorArticulos(idOrden, idsArticulosOrden);

                if (ordenRestaurante == null)
                    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

                if (ordenRestaurante.Estado != Comanda.Estados.Preparacion && ordenRestaurante.Estado != Comanda.Estados.PorEntregar)
                    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_preparacion_o_por_entregar_excepcion);

                //if (ordenRestaurante.IdMesero.HasValue && ordenRestaurante.IdMesero != idEmpleadoCobro)
                //    throw new SOTException(Recursos.Comandas.mesero_comanda_diferente_excepcion);

                //ServicioEmpleados.ValidarMeseroActivoHabilitado(idEmpleadoCobro);

                //ValidarMismoMesero(ordenRestaurante.IdOcupacionMesa, usuario.Id);
                //ServicioEmpleados.ValidarMeseroActivoHabilitado(usuario.Id);

                var fechaActual = DateTime.Now;

                foreach (var idArticuloOrden in idsArticulosOrden)
                {
                    var ordenRestauranteArticulo = ordenRestaurante.ArticulosOrdenRestaurante.FirstOrDefault(m => m.Id == idArticuloOrden);

                    if (ordenRestauranteArticulo.Estado != ArticuloComanda.Estados.PorEntregar)
                        throw new SOTException(Recursos.OrdenesRestaurantes.orden_articulos_no_por_entregar_excepcion);

                    ordenRestauranteArticulo.FechaModificacion = fechaActual;
                    ordenRestauranteArticulo.IdUsuarioModifico = usuario.Id;

                    ordenRestauranteArticulo.Estado = ArticuloComanda.Estados.Entregado;
                }

                if (ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo).All(m => m.Estado == ArticuloComanda.Estados.Entregado))
                {
                    ordenRestaurante.FechaModificacion = DateTime.Now;
                    ordenRestaurante.IdUsuarioCreo = usuario.Id;
                    ordenRestaurante.Estado = Comanda.Estados.PorCobrar;

                    ordenRestaurante.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
                    {
                        EntidadEstado = EntidadEstados.Creado,
                        Estado = ordenRestaurante.Estado,
                        FechaInicio = fechaActual
                    });
                }

                RepositorioOrdenesRestaurantes.Modificar(ordenRestaurante);
                RepositorioOrdenesRestaurantes.GuardarCambios();
            }
            var codigos = ordenRestaurante.ArticulosOrdenRestaurante.Where(m => idsArticulosOrden.Contains(m.Id)).Select(m => m.IdArticulo).Distinct().ToList();

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            //ServicioTickets.ImprimirTicket(ordenRestaurante, configuracionImpresoras.ImpresoraCocina, 1, codigos, Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos);
            //ServicioTickets.ImprimirTicket(ordenRestaurante, configuracionImpresoras.ImpresoraBar, 1, codigos, Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas, Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex);
        }

        private void ValidarMismoMesero(int idOcupacionMesa, int idEmpleadoCobro)
        {
            if (!RepositorioOcupacionesMesa.Alguno(m => m.Id == idOcupacionMesa && m.IdMesero == idEmpleadoCobro))
                throw new SOTException(Recursos.OrdenesRestaurantes.mesero_diferente_exception);
        }

        public void CancelarOrden(int idOrdenRestaurante, string motivo, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarOrdenRestaurante = true });

            var orden = RepositorioOrdenesRestaurantes.ObtenerParaCobrar(idOrdenRestaurante);

            if (orden == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

            //if (orden.Estado != Comanda.Estados.Preparacion &&
            //    orden.Estado != Comanda.Estados.PorEntregar &&
            //    orden.Estado != Comanda.Estados.PorCobrar)
            //    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_cancelable_excepcion);
            if (orden.Estado == Comanda.Estados.PorPagar ||
                orden.Estado == Comanda.Estados.Cobrada ||
                orden.Estado == Comanda.Estados.Cancelada)
                throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_cancelable_excepcion);

            var fechaActual = DateTime.Now;

            foreach (var articulo in orden.ArticulosOrdenRestaurante.Where(m => m.Activo))
            {
                //if (articulo.Estado != ArticuloComanda.Estados.EnProceso && articulo.Estado != ArticuloComanda.Estados.PorEntregar)
                //    throw new SOTException(Recursos.OrdenesRestaurantes.articulo_orden_no_cancelable_excepcion);

                articulo.Activo = false;
                articulo.Estado = ArticuloComanda.Estados.Cancelado;
                articulo.FechaEliminacion = fechaActual;
                articulo.FechaModificacion = fechaActual;
                articulo.IdUsuarioElimino = usuario.Id;
                articulo.IdUsuarioModifico = usuario.Id;
            }

            orden.Activo = false;
            orden.FechaModificacion = fechaActual;
            orden.FechaEliminacion = DateTime.Now;
            orden.IdUsuarioCreo = usuario.Id;
            orden.IdUsuarioElimino = usuario.Id;
            orden.Estado = Comanda.Estados.Cancelada;
            orden.MotivoCancelacion = motivo;

            orden.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
            {
                EntidadEstado = EntidadEstados.Creado,
                Estado = orden.Estado,
                FechaInicio = fechaActual
            });

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioOrdenesRestaurantes.Modificar(orden);
                RepositorioOrdenesRestaurantes.GuardarCambios();

                //if (orden.IdConsumoInterno.HasValue)
                //    ServicioConsumosInternos.EliminarConsumo(orden.IdConsumoInterno.Value, usuario.Id);

                CancelarMovimientos(orden);

                scope.Complete();
            }
        }

        public string RepararCancelacionesOcupacion(string motivoX, DtoCredencial credencial)
        {
            if (motivoX != "YORHA")
                throw new SOTException("Operación no permitida");

            var usuarioX = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuarioX, new Modelo.Seguridad.Dtos.DtoPermisos { CancelarOcupacionesMesas = true });

            var idEstadoPorPagar = (int)Comanda.Estados.PorPagar;
            var idEstadoCancelada = (int)Comanda.Estados.Cancelada;
            var idEstadoOMCancelada = (int)OcupacionMesa.Estados.Cancelada;

            var idsOcupaciones = RepositorioOrdenesRestaurantes.ObtenerElementos(m => m.IdEstado != idEstadoCancelada && m.OcupacionMesa.IdEstado == idEstadoOMCancelada).Select(m => m.IdOcupacionMesa).ToList();


            var ocupaciones = (from ocupacion in RepositorioOcupacionesMesa.ObtenerTodo(m => m.OrdenesRestaurante)
                               where idsOcupaciones.Contains(ocupacion.Id)
                               select new
                               {
                                   ocupacion,
                                   ordenes = ocupacion.OrdenesRestaurante.Where(m => m.Activo || m.IdEstado == idEstadoPorPagar)
                               }).ToList().Select(m => m.ocupacion).ToList();




            StringBuilder retorno = new StringBuilder("Ocupaciones a revisar: " + ocupaciones.Count.ToString());



            //var mesa = RepositorioMesas.Obtener(m => m.Activa && m.Id == ocupaciones.IdMesa);

            //ValidarMesaOcupada(mesa);

            List<OrdenRestaurante> ordenesCancelar = new List<OrdenRestaurante>();

            foreach (var ocupacion in ocupaciones)
            {
                var fechaActual = ocupacion.FechaEliminacion.Value;
                var idUsuario = ocupacion.IdUsuarioElimino.Value;

                retorno.AppendLine();
                retorno.AppendFormat("La ocupación con id {0} posee {1} órdenes", ocupacion.Id, ocupacion.OrdenesRestaurante.Count);

                try
                {
                    var ocLocal = new List<OrdenRestaurante>();

                    foreach (var o in ocupacion.OrdenesRestaurante.Where(m => m.Activo || m.IdEstado == idEstadoPorPagar))
                    {
                        var orden = RepositorioOrdenesRestaurantes.ObtenerParaCancelar(o.Id);

                        if (orden == null)
                            throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

                        //if (orden.Estado == Comanda.Estados.PorPagar ||
                        //orden.Estado == Comanda.Estados.Cobrada ||
                        //orden.Estado == Comanda.Estados.Cancelada)
                        //    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_cancelable_excepcion);


                        orden.Activo = false;
                        orden.FechaModificacion = fechaActual;
                        orden.FechaEliminacion = fechaActual;
                        orden.IdUsuarioCreo = idUsuario;
                        orden.IdUsuarioElimino = idUsuario;
                        orden.Estado = Comanda.Estados.Cancelada;

                        orden.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
                        {
                            EntidadEstado = EntidadEstados.Creado,
                            Estado = orden.Estado,
                            FechaInicio = fechaActual
                        });

                        foreach (var articulo in orden.ArticulosOrdenRestaurante.Where(m => m.Activo || m.Estado == ArticuloComanda.Estados.Entregado))
                        {
                            articulo.Activo = false;
                            articulo.Estado = ArticuloComanda.Estados.Cancelado;
                            articulo.FechaEliminacion = fechaActual;
                            articulo.FechaModificacion = fechaActual;
                            articulo.IdUsuarioElimino = idUsuario;
                            articulo.IdUsuarioModifico = idUsuario;
                        }

                        RepositorioOrdenesRestaurantes.Modificar(orden);
                        ocLocal.Add(orden);
                    }

                    ordenesCancelar.AddRange(ocLocal);
                }
                catch (Exception e)
                {
                    retorno.AppendLine();
                    retorno.AppendFormat("Error al cancelar la ocupación {0} : {1}", ocupacion.Id, e.Message);
                }
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {

                RepositorioOrdenesRestaurantes.GuardarCambios();

                foreach (var orden in ordenesCancelar)
                    CancelarMovimientos(orden);

                scope.Complete();
            }

            return retorno.ToString();
        }

        private void ValidarMesaActivaHabilitada(int idMesa)
        {
            if (!RepositorioMesas.Alguno(m => m.Activa && m.Id == idMesa))
                throw new SOTException(Recursos.OrdenesRestaurantes.mesa_invalida_excepcion);
        }



        public List<OrdenRestaurante> ObtenerDetallesOrdenesPendientes(int idMesa, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDetallesMesa = true });

            var ordenes = RepositorioOrdenesRestaurantes.ObtenerOrdenesPendientesCargadas(idMesa);

            if (ordenes.Count == 0)
                return ordenes;

            var idsArticulos = ordenes.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

            var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

            foreach (var artCom in ordenes.SelectMany(m => m.ArticulosOrdenRestaurante))
            {
                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
            }

            return ordenes;
        }

        public List<ArticuloOrdenRestaurante> ObtenerDetallesOrdenesEntregadasCliente(int idOcupacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDetallesMesa = true });

            var articulosO = RepositorioOrdenesRestaurantes.ObtenerDetallesOrdenesEntregadasCliente(idOcupacion);

            if (articulosO.Count == 0)
                return articulosO;

            var idsArticulos = articulosO.Select(m => m.IdArticulo).ToList();

            var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

            foreach (var artCom in articulosO)
            {
                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
            }

            return articulosO;
        }

        //public List<DtoArticuloPrepararConsulta> ObtenerArticulosPrepararOrdenes(DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos();

        //    var articulosO = RepositorioOrdenesRestaurantes.ObtenerArticulosPorPreparar();

        //    if (articulosO.Count == 0)
        //        return articulosO;

        //    var idsArticulos = articulosO.Select(m => m.IdArticulo).ToList();

        //    var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

        //    foreach (var artCom in articulosO)
        //    {
        //        var articulo = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);

        //        if (articulo == null)
        //        {
        //            artCom.Nombre = "";
        //            artCom.NombreTipo = "";
        //        }
        //        else
        //        {
        //            artCom.Nombre = articulo.Desc_Art;
        //            artCom.NombreTipo = articulo.ZctCatLinea.Desc_Linea;
        //        }
        //    }

        //    return articulosO;
        //}

        public List<Mesa> ObtenerMesas(DtoUsuario usuarioActual)
        {
            return RepositorioMesas.ObtenerElementos(m => m.Activa).ToList();
        }

        public Mesa ObtenerMesa(int idMesa)
        {
            var mesa = RepositorioMesas.Obtener(m => m.Id == idMesa && m.Activa);

            if (mesa == null)
                throw new SOTException(Recursos.Mesas.id_invalido_excepcion, idMesa.ToString());

            return mesa;
        }




        public OcupacionMesa ObtenerOcupacionActualPorMesa(int idMesa)
        {
            return RepositorioOcupacionesMesa.ObtenerOcupacionActualPorMesa(idMesa);
        }





        public OcupacionMesa ObtenerOcupacionBaseActualPorMesa(int idMesa)
        {
            return RepositorioOcupacionesMesa.Obtener(m => m.Activa && m.IdMesa == idMesa, m => m.DatosFiscales);
        }

        public DtoValor ObtenerValorCobrarMesa(int idMesa)
        {
            var mesa = ObtenerMesa(idMesa);

            ValidarMesaCobrar(mesa);

            var estadoEntregado = (int)Comanda.Estados.PorPagar;

            var ultimaOcupacion = ObtenerOcupacionActualPorMesa(idMesa);

            if (ultimaOcupacion == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.ocupacion_mesa_invalida_excepcion);

            var valores = RepositorioOrdenesRestaurantes.ObtenerElementos(m => m.IdEstado == estadoEntregado
                                                                                && m.IdOcupacionMesa == ultimaOcupacion.Id).Select(m => new { m.ValorIVA, m.ValorSinIVA, m.ValorConIVA }).ToList();

            if (valores.Count == 0)
                return new DtoValor();

            return new DtoValor
            {
                ValorConIVA = valores.Sum(m => m.ValorConIVA),
                ValorIVA = valores.Sum(m => m.ValorIVA),
                ValorSinIVA = valores.Sum(m => m.ValorSinIVA),
            };
        }

        private void ValidarMesaOcupada(int idMesa)
        {
            var mesa = RepositorioMesas.Obtener(m => m.Id == idMesa);
            ValidarMesaOcupada(mesa);
        }

        private void ValidarMesaOcupada(Mesa mesa)
        {
            if (mesa.Estado != Mesa.EstadosMesa.Ocupada)
                throw new SOTException(Recursos.Mesas.mesa_no_ocupada_excepcion, mesa.Clave);
        }


        public void ValidarNoTieneOrdenesPendientes(int idOcupacion)
        {
            var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar };

            if (RepositorioOrdenesRestaurantes.Alguno(m => m.Activo && idsEstadoElaborada.Contains(m.IdEstado) && m.IdOcupacionMesa == idOcupacion))
                throw new SOTException(Recursos.Mesas.mesa_ordenes_pendientes_exception);
        }

        public bool VerificarTieneOrdenesPendientes(int idOcupacion)
        {
            var idsEstadoElaborada = new List<int> { (int)Comanda.Estados.Preparacion, (int)Comanda.Estados.PorEntregar, (int)Comanda.Estados.PorCobrar };

            return RepositorioOrdenesRestaurantes.Alguno(m => m.Activo && idsEstadoElaborada.Contains(m.IdEstado) && m.IdOcupacionMesa == idOcupacion);
        }


        public void CobrarMesa(int idMesa, List<DtoInformacionPago> formasPagos, Propina propina, int idMeseroCobro, bool marcarComoCortesia, DtoUsuario usuario)
        {
            usuario = this.ServicioPagos.ProcesarPermisosEspeciales(formasPagos, usuario);

            var configGlobal = ServicioParametros.ObtenerParametros();
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            var ocupacion = RepositorioOcupacionesMesa.ObtenerParaCobrar(idMesa);

            if (ocupacion == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.ocupacion_mesa_invalida_excepcion);

            ValidarOcupacionActiva(ocupacion.Id);

            var valor = ObtenerValorCobrarMesa(idMesa);

            if (!formasPagos.Any())
                throw new SOTException(Recursos.OrdenesRestaurantes.pago_incorrecto_excepcion, valor.ValorConIVA.ToString("C"), 0.ToString("C"));

            var valorPagos = formasPagos.Sum(m => m.Valor);

            if (valor.ValorConIVA.ToString("C") != valorPagos.ToString("C"))
                throw new SOTException(Recursos.OrdenesRestaurantes.pago_incorrecto_excepcion, valor.ValorConIVA.ToString("C"), valorPagos.ToString("C"));

            #region validación de cortesías que cubren toda la comanda

            if (marcarComoCortesia)
            {
                if (formasPagos.Any(m => m.TipoPago != TiposPago.Cortesia) || formasPagos.Count(m => m.TipoPago == TiposPago.Cortesia) != 1)
                    throw new SOTException(Recursos.Comandas.pagos_invalidos_cortesia_excepcion);
            }

            #endregion

            //ServicioEmpleados.ValidarMeseroActivoHabilitado(ocupacion.IdMesero);

            var mesa = RepositorioMesas.Obtener(m => m.Activa && m.Id == idMesa);

            ValidarMesaCobrar(mesa);

            var fechaActual = DateTime.Now;

            //bool existenPagosConsumo;

            //if ((existenPagosConsumo = formasPagos.Any(m => m.TipoPago == TiposPago.Consumo)) && consumo == null)
            //    throw new SOTException(Recursos.OrdenesRestaurantes.pago_consumo_sin_datos_excepcion);

            foreach (var orden in ocupacion.OrdenesRestaurante.Where(m => m.Estado == Comanda.Estados.PorPagar))
            {
                foreach (var articulo in orden.ArticulosOrdenRestaurante)
                {
                    articulo.PrecioUnidadFinal = Math.Round(articulo.PrecioUnidad * (1 + configGlobal.Iva_CatPar), 2);
                    articulo.EntidadEstado = EntidadEstados.Modificado;

#warning aqui se intenta marcar como cortesías al momento del pago todos los artículos, siempre y cuando marcarComoCortesia sea true
                    articulo.EsCortesia = articulo.EsCortesia || marcarComoCortesia;
                }

                orden.FechaModificacion = fechaActual;
                orden.IdUsuarioModifico = usuario.Id;
                orden.Estado = Comanda.Estados.Cobrada;
                orden.EntidadEstado = EntidadEstados.Modificado;

                orden.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
                {
                    EntidadEstado = EntidadEstados.Creado,
                    Estado = orden.Estado,
                    FechaInicio = fechaActual
                });
            }

            string transaccion;

            ocupacion.Activa = false;
            ocupacion.IdMesero = idMeseroCobro;
            ocupacion.FechaModificacion = fechaActual;
            ocupacion.FechaCobro = fechaActual;
            ocupacion.FechaEliminacion = fechaActual;
            ocupacion.IdUsuarioModifico = usuario.Id;
            ocupacion.IdUsuarioElimino = usuario.Id;
            ocupacion.Estado = OcupacionMesa.Estados.Cobrada;
            ocupacion.Transaccion = transaccion = Guid.NewGuid().ToString();

            foreach (var pago in formasPagos)
                ocupacion.PagosOcupacionMesa.Add(new PagoOcupacionMesa
                {
                    Activo = true,
                    Referencia = pago.Referencia,
                    TipoPago = pago.TipoPago,
                    Valor = pago.Valor,
                    TipoTarjeta = pago.TipoTarjeta,
                    NumeroTarjeta = pago.NumeroTarjeta ?? "",
                    Transaccion = transaccion,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id
                });

            if (propina != null)
            {
                propina.TipoPropina = Propina.TiposPropina.Restaurante;
                propina.IdEmpleado = ocupacion.IdMesero;
                propina.Transaccion = transaccion;
                propina.FechaCreacion = fechaActual;
                propina.FechaModificacion = fechaActual;
                propina.IdUsuarioCreo = usuario.Id;
                propina.IdUsuarioModifico = usuario.Id;
            }

            mesa.Estado = Mesa.EstadosMesa.Libre;
            mesa.FechaModificacion = fechaActual;
            mesa.IdUsuarioModifico = usuario.Id;

            var venta = ServicioPagos.GenerarVentaNoCompletada(ocupacion.PagosOcupacionMesa.Where(m => m.Activo), usuario, Venta.ClasificacionesVenta.Restaurante, ocupacion.IdPreventa);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                //#warning consumo interno va a fallar
                //                if (existenPagosConsumo)
                //                {
                //                    ServicioConsumosInternos.Crear(consumo, usuario.Id);
                //                    ocupacion.PagosOcupacionMesa.First(m => m.Activo && m.TipoPago == TiposPago.Consumo).Referencia = consumo.Guid;
                //                }

                ServicioPagos.Pagar(venta.Id, ocupacion.PagosOcupacionMesa.Where(m => m.Activo), usuario);

                RepositorioOcupacionesMesa.Modificar(ocupacion);
                RepositorioOcupacionesMesa.GuardarCambios();

                RepositorioMesas.Modificar(mesa);
                RepositorioMesas.GuardarCambios();

                if (propina != null)
                    ServicioPropinas.RegistrarPropina(propina, usuario.Id);

                scope.Complete();
            }
#warning contemplar si se debe quitar
            ServicioTickets.ImprimirTicket(venta, configuracionImpresoras.ImpresoraTickets, 0);
        }

        private void ValidarMesaCobrar(Mesa mesa)
        {
            if (mesa.Estado != Mesa.EstadosMesa.Cobrar)
                throw new SOTException(Recursos.Mesas.mesa_no_cobrar_excepcion, mesa.Clave);
        }

        public Mesa ObtenerMesaPorOcupacion(int idOcupacion, DtoUsuario usuario)
        {
            return RepositorioOcupacionesMesa.ObtenerElementos(m => m.Id == idOcupacion, m => m.Mesa).Select(m => m.Mesa).FirstOrDefault();
        }


        public void MarcarMesaComoPorCobrar(int idMesa, List<int> idsComandasCortesias, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { MarcarMesaPorCobrar = true });

            var mesa = RepositorioMesas.Obtener(m => m.Activa && m.Id == idMesa);

            ValidarMesaOcupada(mesa);

            var ocupacion = RepositorioOcupacionesMesa.ObtenerParaCobrar(idMesa);//RepositorioOcupacionesMesa.Obtener(m => m.Activa && m.IdMesa == idMesa);

            ValidarNoTieneOrdenesPendientes(ocupacion.Id);

            var fechaActual = DateTime.Now;

            foreach (var orden in ocupacion.OrdenesRestaurante.Where(m => m.Estado == Comanda.Estados.PorPagar))
            {
                bool mod = false;
                foreach (var articulo in orden.ArticulosOrdenRestaurante)
                {
                    if (idsComandasCortesias == null || !idsComandasCortesias.Contains(articulo.Id))
                        continue;
#warning aqui se intenta marcar como cortesías al momento de mandar a cobrar
                    articulo.EsCortesia = true;
                    articulo.EntidadEstado = EntidadEstados.Modificado;

                    mod = true;
                }

                if (!mod)
                    continue;

                orden.FechaModificacion = fechaActual;
                orden.IdUsuarioModifico = usuario.Id;
                orden.EntidadEstado = EntidadEstados.Modificado;
                //orden.Estado = Comanda.Estados.Cobrada;
                //orden.EntidadEstado = EntidadEstados.Modificado;

                //orden.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
                //{
                //    EntidadEstado = EntidadEstados.Creado,
                //    Estado = orden.Estado,
                //    FechaInicio = fechaActual
                //});

                RepositorioOrdenesRestaurantes.Modificar(orden);
            }

            mesa.Estado = Mesa.EstadosMesa.Cobrar;
            mesa.FechaModificacion = fechaActual;
            mesa.IdUsuarioModifico = usuario.Id;

            RepositorioMesas.Modificar(mesa);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioOrdenesRestaurantes.GuardarCambios();
                RepositorioMesas.GuardarCambios();
                scope.Complete();
            }
#warning contemplar si se debe quitar
            ReimprimirTicket(mesa.Id);
        }

        public void ReimprimirTicket(int idMesa)
        {
            
            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            //2 en barra y 1 en caja
            ServicioTickets.ImprimirTicket(idMesa, configuracionImpresoras.ImpresoraBar, 2);
            ServicioTickets.ImprimirTicket(idMesa, configuracionImpresoras.ImpresoraTickets, 1);
        }
        public DtoDatosMesa ObtenerDatosMesaActiva(int idMesa, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDetallesMesa = true });

            var config = ServicioParametros.ObtenerParametros();

            var mesa = RepositorioMesas.Obtener(m => m.Id == idMesa && m.Activa);

            if (mesa == null)
                return null;

            var resumen = new DtoDatosMesa
            {
                NumeroMesa = mesa.Clave,
                Estado = mesa.Estado
            };

            var ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Activa && m.IdMesa == idMesa, m => m.Mesero);

            if (ocupacion == null)
                return resumen;

            var articulos = ObtenerDetallesOrdenesEntregadasCliente(ocupacion.Id, usuario);

            resumen.FechaEntrada = ocupacion.FechaCreacion;
            resumen.NumeroServicio = ocupacion.Id.ToString();
            resumen.Mesero = ocupacion.Mesero.NombreCompleto;

            if (articulos.Count > 0)
            {
                resumen.Subtotal = articulos.Sum(m => Math.Round(m.PrecioUnidad * (1 + config.Iva_CatPar), 2) * m.Cantidad);
                resumen.Cortesias = articulos.Where(m => m.EsCortesia).Sum(m => Math.Round(m.PrecioUnidad * (1 + config.Iva_CatPar), 2) * m.Cantidad);
                //resumen.TotalSinIVA = resumen.Subtotal - resumen.Cortesias;
                //resumen.IVA = resumen.TotalSinIVA * config.Iva_CatPar;
                resumen.TotalConIVA = resumen.Subtotal - resumen.Cortesias; //resumen.TotalSinIVA + resumen.IVA;
            }

            return resumen;
        }


        public void CancelarOcupacion(int idOcupacion, string motivo, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CancelarOcupacionesMesas = true });

            var ocupacion = RepositorioOcupacionesMesa.ObtenerParaCancelar(idOcupacion);

            var mesa = RepositorioMesas.Obtener(m => m.Activa && m.Id == ocupacion.IdMesa);

            ValidarMesaOcupada(mesa);

            var fechaActual = DateTime.Now;

            foreach (var o in ocupacion.OrdenesRestaurante)
            {
                var orden = RepositorioOrdenesRestaurantes.ObtenerParaCancelar(o.Id);

                if (orden == null)
                    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_nula_excepcion);

                //if (orden.Estado != Comanda.Estados.Preparacion &&
                //    orden.Estado != Comanda.Estados.PorEntregar &&
                //    orden.Estado != Comanda.Estados.PorCobrar)
                //    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_cancelable_excepcion);
                if (/*orden.Estado == Comanda.Estados.PorPagar ||*/
                orden.Estado == Comanda.Estados.Cobrada ||
                orden.Estado == Comanda.Estados.Cancelada)
                    throw new SOTException(Recursos.OrdenesRestaurantes.orden_restaurante_no_cancelable_excepcion);


                orden.Activo = false;
                orden.FechaModificacion = fechaActual;
                orden.FechaEliminacion = fechaActual;
                orden.IdUsuarioCreo = usuario.Id;
                orden.IdUsuarioElimino = usuario.Id;
                orden.Estado = Comanda.Estados.Cancelada;
                orden.MotivoCancelacion = motivo;

                orden.HistorialesOrdenRestaurante.Add(new HistorialOrdenRestaurante
                {
                    EntidadEstado = EntidadEstados.Creado,
                    Estado = orden.Estado,
                    FechaInicio = fechaActual
                });

                foreach (var articulo in orden.ArticulosOrdenRestaurante.Where(m => m.Activo))
                {
                    articulo.Activo = false;
                    articulo.Estado = ArticuloComanda.Estados.Cancelado;
                    articulo.FechaEliminacion = fechaActual;
                    articulo.FechaModificacion = fechaActual;
                    articulo.IdUsuarioElimino = usuario.Id;
                    articulo.IdUsuarioModifico = usuario.Id;
                }

                RepositorioOrdenesRestaurantes.Modificar(orden);
            }

            ocupacion.Activa = false;
            ocupacion.Estado = OcupacionMesa.Estados.Cancelada;
            ocupacion.FechaModificacion = fechaActual;
            ocupacion.FechaEliminacion = fechaActual;
            ocupacion.IdUsuarioModifico = usuario.Id;
            ocupacion.IdUsuarioElimino = usuario.Id;
            ocupacion.MotivoCancelacion = motivo;

            mesa.Estado = Mesa.EstadosMesa.Libre;
            mesa.FechaModificacion = fechaActual;
            mesa.IdUsuarioModifico = usuario.Id;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioMesas.Modificar(mesa);
                RepositorioMesas.GuardarCambios();

                RepositorioOcupacionesMesa.Modificar(ocupacion);
                RepositorioOcupacionesMesa.GuardarCambios();

                RepositorioOrdenesRestaurantes.GuardarCambios();

                foreach(var orden in ocupacion.OrdenesRestaurante)
                    CancelarMovimientos(orden);

                scope.Complete();
            }

            var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
            ServicioTickets.ImprimirTicketOcupacionMesaCancelada(idOcupacion, configuracionImpresoras.ImpresoraTickets, 1);
        }

        public void ValidarAcceso(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { AccesoModuloRestaurante = true });
        }

        public int ObtenerCantidadMesasCuentasAbiertas(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarMesas = true });

            return RepositorioOcupacionesMesa.Contador(m => m.Activa);
        }


        //public List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosEnPreparacionDepartamento(string departamento)
        //{
        //    var idsArticulos = ServicioArticulos.ObtenerArticulosConLineaPorDepartamento(departamento);

        //    var ordenes = RepositorioOrdenesRestaurantes.ObtenerDetallesOrdenesArticulosEnPreparacionArea(idsArticulos);

        //    idsArticulos = ordenes.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

        //    var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

        //    foreach (var artCom in ordenes.SelectMany(m => m.ArticulosOrdenRestaurante))
        //    {
        //        artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
        //    }

        //    return ordenes;
        //}

        //public List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosPreparadosDepartamento(string departamento)
        //{
        //    var idsArticulos = ServicioArticulos.ObtenerArticulosConLineaPorDepartamento(departamento);

        //    var ordenes = RepositorioOrdenesRestaurantes.ObtenerDetallesOrdenesArticulosPreparadosArea(idsArticulos);

        //    idsArticulos = ordenes.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

        //    var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

        //    foreach (var artCom in ordenes.SelectMany(m => m.ArticulosOrdenRestaurante))
        //    {
        //        artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
        //    }

        //    return ordenes;
        //}

        public List<DtoResumenComandaExtendido> ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(string departamento)
        {
            //var idsArticulos = ServicioArticulos.ObtenerArticulosConLineaPorDepartamento(departamento);

            var ordenes = RepositorioOrdenesRestaurantes.ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(/*idsArticulos*/);

            var idsArticulos = ordenes.SelectMany(m => m.ArticulosComanda).Select(m => m.IdArticulo).ToList();

            var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos, departamento);

            var estados = new List<ArticuloComanda.Estados>() { ArticuloComanda.Estados.EnProceso, ArticuloComanda.Estados.PorEntregar };

            List<DtoResumenComandaExtendido> validos = new List<DtoResumenComandaExtendido>();
            string _contradepartamento = "";
            List<string> artsvalid = new List<string>();
            List<Modelo.Almacen.Entidades.Articulo> articulosacomparar = new List<Modelo.Almacen.Entidades.Articulo>();
            bool _TieneArticulosOtroDepto = false;
            string tipootrodepto = string.Empty;

            foreach (var orden in ordenes)
            {

                _TieneArticulosOtroDepto = false;
                artsvalid = new List<string>();
                articulosacomparar = new List<Modelo.Almacen.Entidades.Articulo>();
                foreach (var arti in orden.ArticulosComanda)
                {
                    artsvalid.Add(arti.IdArticulo);
                }
                if (departamento.Equals("Cocina"))
                    _contradepartamento = "Bar";
                else
                    _contradepartamento = "Cocina";
                var articontradepto = ServicioArticulos.ObtenerArticulosConLineaPorId(artsvalid, _contradepartamento);
                foreach (var oiu in articontradepto)
                {
                    articulosacomparar.Add(oiu);
                }
                var artidepto = ServicioArticulos.ObtenerArticulosConLineaPorId(artsvalid, departamento);
                foreach (var oiu1 in artidepto)
                {
                    articulosacomparar.Add(oiu1);
                }
                bool _esbebdida = false;
                bool _essex = false;
                bool _esalimentos = false;
                if (articontradepto.Count > 0)
                {
                    foreach (var guyg in articulosacomparar)
                    {
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Equals("ALIMENTOS"))
                            _esalimentos = true;
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Equals("BEBIDAS"))
                            _esbebdida = true;
                        if (guyg.NombreLineaTmp.Trim().ToUpper().Contains("SEX"))
                            _essex = true;
                    }
                    if (_esalimentos && _esbebdida == false && _essex == false)
                        tipootrodepto = "A";
                    else if (_esalimentos && _esbebdida && _essex == false)
                        tipootrodepto = "AB";
                    else if (_esbebdida && _esbebdida && _essex)
                        tipootrodepto = "ABS";
                    _TieneArticulosOtroDepto = true;
                }


                if (!orden.ArticulosComanda.Any(m => articulos.Any(a => a.Cod_Art == m.IdArticulo) && estados.Contains(m.Estado)))
                    continue;

                orden.ArticulosComanda.RemoveAll(m => !articulos.Any(a => a.Cod_Art == m.IdArticulo));


                int contadordearticulosxcmd = 0;
                foreach (var artCom in orden.ArticulosComanda)
                {
                    var art = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                    artCom.Nombre = art.Desc_Art;
                    artCom.Tipo = art.ZctCatLinea.Desc_Linea;


                    if (_TieneArticulosOtroDepto == true && contadordearticulosxcmd == orden.ArticulosComanda.Count - 1)
                    {
                        artCom.EsOtroDepto = true;
                        artCom.OtosDeptos = tipootrodepto;
                        _TieneArticulosOtroDepto = false;
                    }
                    contadordearticulosxcmd = contadordearticulosxcmd + 1;
                }

                validos.Add(orden);
            }

            return validos;
        }

        public void ActualizarPagoOcupacion(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, int idEmpleado, DtoUsuario usuario, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarPagos = true });

            var pago = RepositorioPagosOcupacionesMesa.Obtener(m => m.Activo && m.Id == idPago, m => m.OcupacionMesa);

            if (pago == null)
                throw new SOTException(Recursos.Pagos.pago_nulo_o_eliminado_excepcion);

            if (formaPago != TiposPago.TarjetaCredito && valorPropina != 0)
                throw new SOTException(Recursos.Pagos.propina_no_soportada_excepcion);

            ServicioPagos.ValidarEfectivoOTarjeta(pago);
            ServicioPagos.ValidarEfectivoOTarjeta(formaPago);

            if (pago.OcupacionMesa.IdMesero != idEmpleado/* && valorPropina != 0*/)
                ServicioEmpleados.ValidarPermisos(idEmpleado, new DtoPermisos { Meserear = true });//ServicioEmpleados.ValidarMeseroActivoHabilitado(idEmpleado);

            Propina propina = pago.TipoPago == TiposPago.TarjetaCredito ?
                ServicioPropinas.ObtenerPropina(pago.Transaccion, pago.NumeroTarjeta, pago.TipoTarjeta.Value) :
                null;

            var fechaActual = DateTime.Now;

            pago.FechaModificacion = fechaActual;
            pago.IdUsuarioModifico = usuario.Id;
            pago.TipoPago = formaPago;
            pago.Referencia = referencia;
            pago.TipoTarjeta = tipoTarjeta;
            pago.NumeroTarjeta = numeroTarjeta;

            ServicioPagos.ValidarPago(pago, usuario);


            if (propina != null)
            {
                if (valorPropina != 0)
                {
                    propina.IdEmpleado = idEmpleado;
                    propina.FechaModificacion = fechaActual;
                    propina.IdUsuarioModifico = usuario.Id;
                    propina.TipoTarjeta = tipoTarjeta.Value;
                    propina.NumeroTarjeta = numeroTarjeta;
                    propina.Referencia = referencia;
                    propina.Valor = valorPropina;
                }
                else
                {
                    propina.Activo = false;
                }
            }
            else if (valorPropina != 0)
            {
                propina = new Propina
                {
                    Activo = true,
                    TipoPropina = Propina.TiposPropina.Restaurante,
                    TipoTarjeta = pago.TipoTarjeta.Value,
                    NumeroTarjeta = numeroTarjeta,
                    Referencia = referencia,
                    IdEmpleado = idEmpleado,
                    Transaccion = pago.Transaccion,
                    FechaCreacion = pago.FechaCreacion,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = usuario.Id,
                    IdUsuarioModifico = usuario.Id,
                    Valor = valorPropina
                };
            }

            if (pago.OcupacionMesa.IdMesero != idEmpleado)
            {
                //pago.OcupacionMesa.FechaModificacion = fechaActual;
                //pago.OcupacionMesa.IdUsuarioModifico = usuario.Id;
                pago.OcupacionMesa.IdMesero = idEmpleado;
            }
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioPagosOcupacionesMesa.Modificar(pago);
                RepositorioPagosOcupacionesMesa.GuardarCambios();

                if (propina != null)
                {
                    if (propina.Id == 0)
                        ServicioPropinas.RegistrarPropina(propina, usuario.Id);
                    else if (propina.Activo)
                        ServicioPropinas.ModificarPropina(propina, usuario.Id);
                    else
                        ServicioPropinas.EliminarPropina(propina.Id, usuario.Id);
                }

                scope.Complete();
            }
        }


        public int ObtenerUltimoNumeroOrden(int idTurno)
        {
            return (from ordenR in RepositorioOrdenesRestaurantes.ObtenerElementos(m => m.OcupacionMesa.IdCorte == idTurno)
                    orderby ordenR.Orden descending
                    select ordenR.Orden).FirstOrDefault();
        }


        public DatosFiscales ObtenerDatosFiscalesPorTransaccion(string transaccion)
        {
            return RepositorioOcupacionesMesa.ObtenerDatosFiscalesPorTransaccion(transaccion);
        }


        public void VincularDatosFiscales(int idOcupacionMesa, int? idDatosFiscales, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { VincularDatosFiscalesClientes = true });

            var ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Id == idOcupacionMesa);

            if (ocupacion == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.ocupacion_mesa_nula_excepcion);

            ocupacion.IdDatosFiscales = idDatosFiscales;

            RepositorioOcupacionesMesa.Modificar(ocupacion);
            RepositorioOcupacionesMesa.GuardarCambios();
        }

        #region métodos internal

        void IServicioRestaurantesInterno.ValidarNoExistenPagosPendientes(int? idCorte)
        {
            if (idCorte.HasValue)
            {
                var idsEstadosOcupaciones = new List<int>() { (int)OcupacionMesa.Estados.Cancelada, (int)OcupacionMesa.Estados.Cobrada };

                if (RepositorioOcupacionesMesa.Alguno(m => m.IdCorte == idCorte && !idsEstadosOcupaciones.Contains(m.IdEstado)))
                    throw new SOTException(Recursos.CortesTurno.mesas_pendientes_excepcion);

                var idsEstadosComandas = new List<int>() 
                { 
                    (int)Comanda.Estados.PorPagar
                    //(int)Comanda.Estados.Cobrada, 
                    //(int)Comanda.Estados.Cancelada 
                };

                if (RepositorioOrdenesRestaurantes.Alguno(m => /*!*/idsEstadosComandas.Contains(m.IdEstado) && m.OcupacionMesa.IdCorte == idCorte))
                    throw new SOTException(Recursos.CortesTurno.mesas_pagar_excepcion);

                //var idsEstadosComandas = new List<int>() 
                //{ 
                //    //(int)Comanda.Estados.PorPagar
                //    (int)Comanda.Estados.Cobrada, 
                //    (int)Comanda.Estados.Cancelada 
                //};

                //if (RepositorioOrdenesRestaurantes.Alguno(m => m.OcupacionMesa.IdCorte == idCorte && !idsEstadosComandas.Contains(m.IdEstado)))
                //    throw new SOTException(Recursos.CortesTurno.mesas_pendientes_excepcion);
            }

            else
            {
                ////var idEstadoPendiente = (int)Renta.Estados.PendienteCobro;
                //var idsEstadosComandas = new List<int>() 
                //{ 
                //    (int)Comanda.Estados.PorPagar
                //    //(int)Comanda.Estados.Cobrada, 
                //    //(int)Comanda.Estados.Cancelada 
                //};

                //if(RepositorioOrdenesRestaurantes.Alguno(m => /*!*/idsEstadosComandas.Contains(m.IdEstado)))
                //    throw new SOTException(Recursos.CortesTurno.mesas_pagar_excepcion);

                var idEstadoCobrar = (int)Mesa.EstadosMesa.Cobrar;

                if(RepositorioMesas.Alguno(m=> m.IdEstado == idEstadoCobrar))
                    throw new SOTException(Recursos.CortesTurno.mesas_pagar_excepcion);
            }
        }

        bool IServicioRestaurantesInterno.VerificarNoExistenDependenciasArticulo(string codigoArticulo)
        {
            return RepositorioOrdenesRestaurantes.ObtenerCantidadArticulosOrdenesRestaurantePorCodigoArticulo(codigoArticulo) == 0;
        }

        Dictionary<TiposTarjeta, decimal> IServicioRestaurantesInterno.ObtenerDiccionarioPagosMesaTarjetaPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioOrdenesRestaurantes.ObtenerDiccionarioPagosMesaTarjetaPorFecha(fechaInicio, fechaFin);

            foreach (var item in Enum.GetValues(typeof(TiposTarjeta)).Cast<TiposTarjeta>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }

        List<DtoEmpleadoLineaArticuloValor> IServicioRestaurantesInterno.ObtenerValorArticulosOrdenesPorPeriodoYLineas(List<int> idsTipos, List<int> idsCortes)
        {
            var articulos = ServicioArticulos.ObtenerIdsArticulosPorLineas(idsTipos);

            return RepositorioOrdenesRestaurantes.ObtenerArticulosPorEmpleado(articulos, idsCortes);
        }

        string IServicioRestaurantesInterno.ObtenerNumeroMesaPorTransaccion(string numeroTransaccion)
        {
            return RepositorioOcupacionesMesa.ObtenerNumeroMesaPorTransaccion(numeroTransaccion);
        }

        decimal IServicioRestaurantesInterno.ObtenerPagosPorFecha(DateTime? fechaInicio, DateTime? fechaFin)
        {
            return RepositorioOrdenesRestaurantes.ObtenerPagosPorFecha(fechaInicio, fechaFin);
        }

        Dictionary<TiposPago, decimal> IServicioRestaurantesInterno.ObtenerDiccionarioPagosMesaPorFecha(DateTime? fechaInicio, DateTime? fechaFin, params TiposPago[] tiposPagos)
        {
            var diccionario = RepositorioOrdenesRestaurantes.ObtenerPagosMesaPorFecha(tiposPagos.Select(m => (int)m).ToList(), fechaInicio, fechaFin);

            if (tiposPagos.Length == 0)
                foreach (var item in Enum.GetValues(typeof(TiposPago)).Cast<TiposPago>())
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }
            else
                foreach (var item in tiposPagos)
                {
                    if (!diccionario.ContainsKey(item))
                        diccionario.Add(item, 0);
                }

            return diccionario;
        }

        List<OcupacionMesa> IServicioRestaurantesInterno.ObtenerOcupacionesCargadasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, TiposPago? formasPago)
        {
            return RepositorioOrdenesRestaurantes.ObtenerOcupacionesCargadasPorPeriodo(fechaInicio, fechaFin, formasPago);
        }

        #endregion


        public void IntercambiarMesas(int idMesaOrigen, int idMesaDestino, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CambiarMesa = true });

            var mesaOrigen = RepositorioMesas.Obtener(m => m.Id == idMesaOrigen);
            var mesaDestino = RepositorioMesas.Obtener(m => m.Id == idMesaDestino);

            if (mesaOrigen == null)
                throw new SOTException("La mesa de origen no existe o ha sido eliminada");

            if (mesaDestino == null)
                throw new SOTException("La mesa de destino no existe o ha sido eliminada");

            try
            {
                ValidarMesaOcupada(mesaOrigen);
            }
            catch (Exception e)
            {
                throw new SOTException("Error de la mesa de origen: " + e.Message);
            }
            try
            {
                ValidarMesaLibre(mesaDestino);
            }
            catch (Exception e)
            {
                throw new SOTException("Error de la mesa de destino: " + e.Message);
            }

            var ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.IdMesa == idMesaOrigen && m.Activa);

            if (ocupacion == null)
                throw new SOTException(Recursos.OrdenesRestaurantes.ocupacion_mesa_invalida_excepcion);

            var fechaActual = DateTime.Now;

            ocupacion.IdMesa = idMesaDestino;

            mesaOrigen.Estado = Mesa.EstadosMesa.Libre;
            mesaOrigen.FechaModificacion = fechaActual;
            mesaOrigen.IdUsuarioModifico = usuario.Id;

            mesaDestino.Estado = Mesa.EstadosMesa.Ocupada;
            mesaDestino.FechaModificacion = fechaActual;
            mesaDestino.IdUsuarioModifico = usuario.Id;

            RepositorioMesas.Modificar(mesaOrigen);
            RepositorioMesas.Modificar(mesaDestino);

            RepositorioOcupacionesMesa.Modificar(ocupacion);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioMesas.GuardarCambios();
                RepositorioOcupacionesMesa.GuardarCambios();

                scope.Complete();
            }
        }

        private void RegistrarMovimiento(OrdenRestaurante ordenRestaurante)
        {
            foreach (var articulo in ordenRestaurante.ArticulosOrdenRestaurante.Where(m => m.Activo))
                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.R, 0, false, /*articulo.Cantidad * articulo.PrecioUnidad * -1,*/ ordenRestaurante.Id.ToString(), articulo.IdArticulo, articulo.Cantidad * -1, articulo.FechaCreacion, true);
        }

        private void ActualizarMovimientos(int idOrdenRestaurante, List<DtoArticuloPrepararGeneracion> articulosOriginales, List<DtoArticuloPrepararGeneracion> articulos, DateTime fecha)
        {
            foreach (var tupla in (from artGeneracion in articulos
                                   join articuloOrden in articulosOriginales
                                    on new { artGeneracion.Id, Activo = true } equals new { articuloOrden.Id, articuloOrden.Activo } into x
                                   from articuloOrden in x.DefaultIfEmpty()
                                   select new
                                   {
                                       artGeneracion,
                                       articuloOrden
                                   }).ToList())
            {
                if (tupla.articuloOrden == null)
                {
                    if (tupla.artGeneracion.Id <= 0)
                    {
                        ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.R, 0, false, /*tupla.artGeneracion.Cantidad * tupla.artGeneracion.PrecioConIVA * -1,*/ idOrdenRestaurante.ToString(), tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad * -1, fecha, true);
                    }
                }
                else
                {
                    if (!tupla.artGeneracion.Activo)
                    {
                        ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.R, 0, true, /*tupla.artGeneracion.Cantidad * tupla.artGeneracion.PrecioConIVA,*/ idOrdenRestaurante.ToString(), tupla.artGeneracion.IdArticulo, tupla.artGeneracion.Cantidad, fecha, true);
                    }
                    else
                    {
                        if (tupla.articuloOrden.Cantidad != tupla.artGeneracion.Cantidad)
                        {

                            var nuevaCantidad = tupla.articuloOrden.Cantidad - tupla.artGeneracion.Cantidad;

                            if (nuevaCantidad < 0)
                            {
                                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.R, 0, false, /*nuevaCantidad * tupla.artGeneracion.PrecioConIVA,*/ idOrdenRestaurante.ToString(), tupla.artGeneracion.IdArticulo, nuevaCantidad, fecha, true);
                            }
                            else if (nuevaCantidad > 0)
                            {
                                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.R, 0, true, /*nuevaCantidad * tupla.artGeneracion.PrecioConIVA,*/ idOrdenRestaurante.ToString(), tupla.artGeneracion.IdArticulo, nuevaCantidad, fecha, true);
                            }
                        }
                    }
                }
            }
        }

        private void CancelarMovimientos(OrdenRestaurante ordenRestaurante)
        {
            foreach (var articulo in ordenRestaurante.ArticulosOrdenRestaurante)
                ServicioInventarios.AgregarMovimientoComanda(ClavesFolios.R, 0, true, /*articulo.Cantidad * articulo.PrecioUnidad,*/ ordenRestaurante.Id.ToString(), articulo.IdArticulo, articulo.Cantidad, articulo.FechaEliminacion.Value, true);
        }

        public void CrearMesa(Mesa mesa, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarMesas = true });

            if (mesa == null)
                throw new SOTException(Recursos.Mesas.mesa_nula_excepcion);

            ValidarDatos(mesa);

            mesa.Estado = Mesa.EstadosMesa.EnCreacion;
            mesa.Activa = true;
            mesa.IdUsuarioCreo = usuario.Id;
            mesa.IdUsuarioModifico = usuario.Id;
            mesa.FechaCreacion = DateTime.Now;
            mesa.FechaModificacion = DateTime.Now;

            RepositorioMesas.Agregar(mesa);
            RepositorioMesas.GuardarCambios();
        }

        public void ModificarMesa(Mesa mesa, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarMesas = true });

            if (mesa == null)
                throw new SOTException(Recursos.Mesas.mesa_nula_excepcion);

            ValidarDatos(mesa);

            var mesaOriginal = RepositorioMesas.Obtener(m => m.Activa && m.Id == mesa.Id);

            if (mesaOriginal == null)
                throw new SOTException(Recursos.Mesas.id_invalido_excepcion, mesa.Id.ToString());

            if (mesaOriginal.Estado != Mesa.EstadosMesa.EnCreacion)
                throw new SOTException(Recursos.Mesas.mesa_en_funcion_excepcion, mesa.Id.ToString());

            mesaOriginal.Fila = mesa.Fila;
            mesaOriginal.Columna = mesa.Columna;
            mesaOriginal.Clave = mesa.Clave;
            mesaOriginal.FechaModificacion = DateTime.Now;
            mesaOriginal.IdUsuarioModifico = usuario.Id;

            RepositorioMesas.Modificar(mesaOriginal);
            RepositorioMesas.GuardarCambios();
        }

        public void EliminarMesa(int idMesa, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarMesas = true });

            var mesaOriginal = RepositorioMesas.Obtener(m => m.Activa && m.Id == idMesa);

            if (mesaOriginal == null)
                throw new SOTException(Recursos.Mesas.id_invalido_excepcion, idMesa.ToString());

            if (mesaOriginal.Estado != Mesa.EstadosMesa.EnCreacion)
                throw new SOTException(Recursos.Mesas.mesa_en_funcion_excepcion, idMesa.ToString());

            var fechaActual = DateTime.Now;

            mesaOriginal.Activa = false;

            mesaOriginal.FechaEliminacion = fechaActual;
            mesaOriginal.FechaModificacion = fechaActual;
            mesaOriginal.IdUsuarioElimino = usuario.Id;
            mesaOriginal.IdUsuarioModifico = usuario.Id;

            RepositorioMesas.Modificar(mesaOriginal);
            RepositorioMesas.GuardarCambios();
        }

        private void ValidarDatos(Mesa mesaValidar)
        {
            if (string.IsNullOrWhiteSpace(mesaValidar.Clave))
                throw new SOTException(Recursos.Mesas.clave_mesa_invalida_excepcion);

            if (mesaValidar.Fila <= 0)
                throw new SOTException(Recursos.Mesas.fila_invalida_excepcion);

            if (mesaValidar.Columna <= 0)
                throw new SOTException(Recursos.Mesas.columna_invalida_excepcion);

            var habitacionExistente = RepositorioMesas.Obtener(m => m.Activa && m.Id != mesaValidar.Id && (m.Clave.Trim().ToUpper() == mesaValidar.Clave.Trim().ToUpper() ||
             (m.Fila == mesaValidar.Fila && m.Columna == mesaValidar.Columna)));

            if (habitacionExistente != null && habitacionExistente.Clave.Trim().ToUpper() == mesaValidar.Clave.Trim().ToUpper())
                throw new SOTException(Recursos.Mesas.clave_mesa_en_uso_excepcion, mesaValidar.Clave);

            if (habitacionExistente != null && habitacionExistente.Fila == mesaValidar.Fila && habitacionExistente.Columna == mesaValidar.Columna)
                throw new SOTException(Recursos.Mesas.fila_columna_mesa_en_uso_excepcion, mesaValidar.Fila.ToString(), mesaValidar.Columna.ToString());
        }

        public void FinalizarConfiguracionMesas(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarMesas = true });

            var idEstadoCreada = (int)Mesa.EstadosMesa.EnCreacion;

            var habitaciones = RepositorioMesas.ObtenerElementos(m => m.IdEstado == idEstadoCreada && m.Activa).ToList();

            foreach (var habitacion in habitaciones)
            {
                habitacion.FechaModificacion = DateTime.Now;
                habitacion.IdUsuarioModifico = usuario.Id;
                habitacion.Estado = Mesa.EstadosMesa.Libre;

                RepositorioMesas.Modificar(habitacion);
            }

            RepositorioMesas.GuardarCambios();
        }
    }
}
