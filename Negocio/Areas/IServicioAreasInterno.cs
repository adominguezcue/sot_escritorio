﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Areas
{
    internal interface IServicioAreasInterno : IServicioAreas
    {
        bool VerificarExistencia(int idArea);
    }
}
