﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Areas
{
    public class ServicioAreas : IServicioAreasInterno
    {
        IRepositorioAreas RepositorioAreas
        {
            get { return _repositorioAreas.Value; }
        }

        Lazy<IRepositorioAreas> _repositorioAreas = new Lazy<IRepositorioAreas>(() => FabricaDependencias.Instancia.Resolver<IRepositorioAreas>());

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());


        public List<Area> ObtenerAreasActivasConPuestos()
        {
            //ServicioPermisos.ValidarPermisos();//usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarAreas = true });

            return RepositorioAreas.ObtenerAreasActivasConPuestos();
        }


        public List<Area> ObtenerAreasActivas()
        {
            return RepositorioAreas.ObtenerElementos(m => m.Activa).ToList();
        }

        #region métodos internal

        bool IServicioAreasInterno.VerificarExistencia(int idArea) 
        {
            return RepositorioAreas.Alguno(m => m.Activa && m.Id == idArea);
        }

        #endregion
    }
}