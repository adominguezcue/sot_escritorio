﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.Areas
{
    public interface IServicioAreas
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Area> ObtenerAreasActivasConPuestos();

        List<Area> ObtenerAreasActivas();
    }
}
