﻿using Modelo;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Habitaciones
{
    public interface IServicioHabitaciones
    {
        /// <summary>
        /// Retorna una lista con todas las habitaciones del hotel
        /// </summary>
        /// <returns>Lista de habitaciones del hotel</returns>
        List<Habitacion> ObtenerActivasConTipos(DtoUsuario usuario);
        /// <summary>
        /// Retorna un resumen con los datos de la habitación con el número especificado
        /// </summary>
        /// <param name="numeroHabitacion">Número de la habitación</param>
        /// <returns>DtoDatosHabitacion</returns>
        DtoDatosHabitacion ObtenerDatosHabitacion(string numeroHabitacion, DtoUsuario usuario);
        void ValidarEstaReservadaPreparada(int idHabitacion);
        void ValidarEstaPreparada(int idHabitacion);
        /// <summary>
        /// Valida si el usuario actual posee los permisos necesarios para acceder al módulo del
        /// hotel vinculado a habitaciones
        /// </summary>
        /// <param name="usuarioActual"></param>
        void ValidarAcceso(DtoUsuario usuarioActual);
        /// <summary>
        /// Cambia el estado de una habitación a Habilitada para ventas, siempre que ésta esté en
        /// estado Limpia y el usuario posea los permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void HabilitarParaVentas(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Retorna la habitación activa que coincida con el id proporcionado, si no existen
        /// coincidencias se arrojará una excepción
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        Habitacion ObtenerHabitacion(int idHabitacion);
        /// <summary>
        /// Marca una habitación como bloqueada siempre y cuando el usuario tenga los permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="motivo"></param>
        /// <param name="usuario"></param>
        void BloquearHabitacion(int idHabitacion, string motivo, DtoUsuario usuario);
        /// <summary>
        /// Marca una habitación como preparada siempre y cuando el usuario tenga los permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void PrepararHabitacion(int idHabitacion, DtoUsuario usuario);

        /// <summary>
        /// Cancela la preparación de una habitación, dejándola en estado libre
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void CancelarPreparacionHabitacion(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Otorga a una habitación el estado de mantenimiento siempre y cuando el usuario tenga los
        /// permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idTareaMantenimiento"></param>
        /// <param name="usuario"></param>
        void PonerEnMantenimiento(int idHabitacion, int idTareaMantenimiento, DtoUsuario usuario);
        /// <summary>
        /// Finaliza el mantenimiento de una habitación
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void FinalizarMantenimiento(int idHabitacion, DtoUsuario usuario, List<int> idsOrdenesTrabajo);
        /// <summary>
        /// Finaliza la renta de una habitación
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void FinalizarOcupacion(int idHabitacion, bool esCancelacion, DtoUsuario usuario);
        /// <summary>
        /// Valida que una habitación se encuentre en mantenimiento
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaMantenimiento(int idHabitacion);
        void FinalizarConfiguracionHabitaciones(DtoUsuario usuario);

        /// <summary>
        /// Valida que una habitación se encuentre sucia, en limpieza o supervisión
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaSuciaLimpiezaSupervision(int idHabitacion);
        /// <summary>
        /// Valida que una habitación se encuentre ocupada
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaOcupada(int idHabitacion);
        void ValidarEstaOcupadaOPendienteCobro(int idHabitacion);
        /// <summary>
        /// Valida que una habitación se encuentre sucia
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaSucia(int idHabitacion);
        /// <summary>
        /// Valida que una habitación se encuentre en limpieza
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaEnLimpieza(int idHabitacion);
        /// <summary>
        /// Valida que una habitación se encuentre en supervisión
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaEnSupervision(int idHabitacion);
        /// <summary>
        /// Retorna el bloque actual de una habitación siempre y cuándo la habitación se encuentre
        /// bloqueada y el usuario posea los permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        BloqueoHabitacion ObtenerBloqueoActual(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Permite marcar una habitación como reservada
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void MarcarComoReservada(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Permite desmarcar como reservada una habitación, siempre y cuando no existan
        /// reservaciones cofirmadas para ella
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void CancelarEstadoReservacionHabitacion(int idHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Valida que la habitación con el id proporcionado se encuentre libre
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaLibre(int idHabitacion);
        /// <summary>
        /// Valida que la habitación con el id proporcionado se encuentre reservada
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaReservada(int idHabitacion);
        /// <summary>
        /// Valida que una habitación esté pendiente de cobro
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEstaPendienteCobro(int idHabitacion);
        /// <summary>
        /// Retorna true en caso de que existan habitaciones activas que pertenezcan al tipo con el id proporcionado
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <returns></returns>
        bool VerificarExistenPorTipo(int idTipoHabitacion);
        /// <summary>
        /// Retorna un resumen del estado actual de la habitación con el id proporcionado
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        DtoResumenHabitacion SP_ObtenerResumenHabitacion(int idHabitacion, int? cambiosup);
        /// <summary>
        /// Retorna un resumen del estado actual de las habitaciones activas
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        List<DtoResumenHabitacion> SP_ObtenerResumenesHabitaciones();
        /// <summary>
        /// Retorna las estadísticas por cada uno de los estados que puede tener una habitación
        /// </summary>
        /// <returns></returns>
        List<DtoEstadisticasHabitacion> ObtenerEstadisticasHabitaciones();
        ///// <summary>
        ///// Permite editar habitaciones que aun estén en estatus de creación
        ///// </summary>
        ///// <param name="habitacionesNuevas">Lista de habitaciones a crear o editar</param>
        ///// <param name="usuario">Usuario con permisos para realizar la acción</param>
        //void EditarHabitaciones(List<Habitacion> habitacionesNuevas, DtoUsuario usuario);
        /// <summary>
        /// Agrega una nueva habitación al sistema
        /// </summary>
        /// <param name="habitacionNueva"></param>
        /// <param name="usuario"></param>
        void CrearHabitacion(Habitacion habitacionNueva, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar una habitación existente, siempre y cuando su estatus sea En creación
        /// </summary>
        /// <param name="habitacion"></param>
        /// <param name="usuario"></param>
        void ModificarHabitacion(Habitacion habitacion, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar una habitación del sistema, siempre y cuando esté en creación.
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void EliminarHabitacion(int idHabitacion, DtoUsuario usuario);
    }
}
