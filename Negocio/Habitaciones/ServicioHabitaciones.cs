﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;
using Modelo;
using Modelo.Dtos;
using Modelo.Repositorios;
using Transversal.Dependencias;
using Negocio.Seguridad.Permisos;
using Transversal.Excepciones;
using System.Transactions;
using Negocio.TareasMantenimiento;
using Negocio.Rentas;
using Negocio.TareasLimpieza;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Reservaciones;
using Transversal.Excepciones.Seguridad;
using Negocio.Mantenimientos;
using Negocio.ConfiguracionesGlobales;
using Negocio.Almacen.Parametros;
using Negocio.RoomServices;
using Negocio.Tickets;
using Negocio.ConfiguracionesImpresoras;
using Transversal.Transacciones;
using Negocio.TiposHabitacion;

namespace Negocio.Habitaciones
{
    public class ServicioHabitaciones : IServicioHabitacionesInterno
    {
        IServicioTiposHabitaciones ServicioTiposHabitaciones
        {
            get { return _servicioTiposHabitaciones.Value; }
        }

        Lazy<IServicioTiposHabitaciones> _servicioTiposHabitaciones = new Lazy<IServicioTiposHabitaciones>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTiposHabitaciones>(); });
        //static List<Habitacion> habitaciones = new List<Habitacion>();

        IRepositorioAutorizaMinutosVenta RepositorioAutorizaMinutosVentas
        {
            get { return _repositorioAutorizaminutosventa.Value; }
        }
        Lazy<IRepositorioAutorizaMinutosVenta> _repositorioAutorizaminutosventa = new Lazy<IRepositorioAutorizaMinutosVenta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioAutorizaMinutosVenta>(); });

        IServicioConfiguracionesImpresorasInterno ServicioConfiguracionesImpresoras
        {
            get { return _servicioConfiguracionesImpresoras.Value; }
        }

        private Lazy<IServicioConfiguracionesImpresorasInterno> _servicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresorasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresorasInterno>(); });

        IServicioTicketsInterno ServicioTickets
        {
            get { return _servicioTickets.Value; }
        }

        Lazy<IServicioTicketsInterno> _servicioTickets = new Lazy<IServicioTicketsInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTicketsInterno>(); });


        IServicioRoomServicesInterno ServicioRoomServices
        {
            get { return _servicioRoomServices.Value; }
        }

        Lazy<IServicioRoomServicesInterno> _servicioRoomServices = new Lazy<IServicioRoomServicesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRoomServicesInterno>(); });

        IRepositorioHabitaciones RepositorioHabitaciones
        {
            get { return _repostorioHabitaciones.Value; }
        }
        Lazy<IRepositorioHabitaciones> _repostorioHabitaciones = new Lazy<IRepositorioHabitaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioHabitaciones>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }
        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioRentasInterno ServicioRentas
        {
            get { return _servicioRentas.Value; }
        }

        Lazy<IServicioRentasInterno> _servicioRentas = new Lazy<IServicioRentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRentasInterno>(); });

        IServicioReservaciones ServicioReservaciones
        {
            get { return _servicioReservaciones.Value; }
        }

        Lazy<IServicioReservaciones> _servicioReservaciones = new Lazy<IServicioReservaciones>(() => { return FabricaDependencias.Instancia.Resolver<IServicioReservaciones>(); });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioTareasLimpiezaInterno ServicioTareasLimpieza
        {
            get { return _servicioTareasLimpieza.Value; }
        }

        Lazy<IServicioTareasLimpiezaInterno> _servicioTareasLimpieza = new Lazy<IServicioTareasLimpiezaInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTareasLimpiezaInterno>(); });

        IServicioMantenimientosInterno ServicioMantenimientos
        {
            get { return _servicioMantenimientos.Value; }
        }
        Lazy<IServicioMantenimientosInterno> _servicioMantenimientos = new Lazy<IServicioMantenimientosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioMantenimientosInterno>(); });

        //public void EditarHabitaciones(List<Habitacion> habitacionesNuevas, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarHabitaciones = true });

        //    if (habitacionesNuevas.Count == 0)
        //        return;

        //    if (habitacionesNuevas.Any(m=> m == null))
        //        throw new SOTException(Recursos.Habitaciones.habitaciones_nulas_eliminadas_excepcion);


        //    using (var scope = new SotTransactionScope())
        //    {
        //        foreach (var habitacion in habitacionesNuevas)
        //        {
        //            if (habitacion.Id != 0)
        //            {
        //                ModificarHabitacion(habitacion, usuario);
        //            }
        //            else if (habitacion.Activo)
        //                CrearHabitacion(habitacion, usuario);
        //        }

        //        scope.Complete();
        //    }

        //}

        public void CrearHabitacion(Habitacion habitacionNueva, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarHabitaciones = true });

            if (habitacionNueva == null)
                throw new SOTException(Recursos.Habitaciones.habitacion_nula_excepcion);

            ValidarDatos(habitacionNueva);

            habitacionNueva.EstadoHabitacion = Habitacion.EstadosHabitacion.EnCreacion;
            habitacionNueva.Activo = true;
            habitacionNueva.IdUsuarioCreo = usuario.Id;
            habitacionNueva.IdUsuarioModifico = usuario.Id;
            habitacionNueva.FechaCreacion = DateTime.Now;
            habitacionNueva.FechaModificacion = DateTime.Now;

            RepositorioHabitaciones.Agregar(habitacionNueva);
            RepositorioHabitaciones.GuardarCambios();
        }

        public void ModificarHabitacion(Habitacion habitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarHabitaciones = true });

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.habitacion_nula_excepcion);

            ValidarDatos(habitacion);

            var habitacionOriginal = RepositorioHabitaciones.Obtener(m => m.Activo && m.Id == habitacion.Id);

            if (habitacionOriginal == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, habitacion.Id.ToString());

            if (habitacionOriginal.EstadoHabitacion != Habitacion.EstadosHabitacion.EnCreacion)
                throw new SOTException(Recursos.Habitaciones.habitacion_en_funcion_excepcion, habitacion.Id.ToString());

            habitacionOriginal.IdTipoHabitacion = habitacion.IdTipoHabitacion;
            habitacionOriginal.Piso = habitacion.Piso;
            habitacionOriginal.Posicion = habitacion.Posicion;
            habitacionOriginal.NumeroHabitacion = habitacion.NumeroHabitacion;
            habitacionOriginal.FechaModificacion = DateTime.Now;
            habitacionOriginal.IdUsuarioModifico = usuario.Id;

            RepositorioHabitaciones.Modificar(habitacionOriginal);
            RepositorioHabitaciones.GuardarCambios();
        }

        public void EliminarHabitacion(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarHabitaciones = true });

            var habitacionOriginal = RepositorioHabitaciones.Obtener(m => m.Activo && m.Id == idHabitacion);

            if (habitacionOriginal == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            if (habitacionOriginal.EstadoHabitacion != Habitacion.EstadosHabitacion.EnCreacion)
                throw new SOTException(Recursos.Habitaciones.habitacion_en_funcion_excepcion, idHabitacion.ToString());

            var fechaActual = DateTime.Now;

            habitacionOriginal.Activo = false;

            habitacionOriginal.FechaEliminacion = fechaActual;
            habitacionOriginal.FechaModificacion = fechaActual;
            habitacionOriginal.IdUsuarioElimino = usuario.Id;
            habitacionOriginal.IdUsuarioModifico = usuario.Id;

            RepositorioHabitaciones.Modificar(habitacionOriginal);
            RepositorioHabitaciones.GuardarCambios();
        }

        private void ValidarDatos(Habitacion habitacionValidar)
        {
            if (habitacionValidar.IdTipoHabitacion == 0)
                throw new SOTException(Recursos.Habitaciones.tipo_habitacion_invalido_excepcion);

            if (string.IsNullOrWhiteSpace(habitacionValidar.NumeroHabitacion))
                throw new SOTException(Recursos.Habitaciones.numero_habitacion_invalido_excepcion);

            if (habitacionValidar.Piso <= 0)
                throw new SOTException(Recursos.Habitaciones.piso_invalido_excepcion);

            if (habitacionValidar.Posicion <= 0)
                throw new SOTException(Recursos.Habitaciones.posicion_invalida_excepcion);

            var habitacionExistente = RepositorioHabitaciones.Obtener(m => m.Activo && m.Id != habitacionValidar.Id && (m.NumeroHabitacion.Trim().ToUpper() == habitacionValidar.NumeroHabitacion.Trim().ToUpper() ||
             (m.Piso == habitacionValidar.Piso && m.Posicion == habitacionValidar.Posicion)));

            if (habitacionExistente != null && habitacionExistente.NumeroHabitacion.Trim().ToUpper() == habitacionValidar.NumeroHabitacion.Trim().ToUpper())
                throw new SOTException(Recursos.Habitaciones.numero_habitacion_en_uso_excepcion, habitacionValidar.NumeroHabitacion);

            if (habitacionExistente != null && habitacionExistente.Piso == habitacionValidar.Piso && habitacionExistente.Posicion == habitacionValidar.Posicion)
                throw new SOTException(Recursos.Habitaciones.piso_posicion_habitacion_en_uso_excepcion, habitacionValidar.Piso.ToString(), habitacionValidar.Posicion.ToString());

            ServicioTiposHabitaciones.ValidarExisteActivo(habitacionValidar.IdTipoHabitacion);
        }

        public void BloquearHabitacion(int idHabitacion, string motivo, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaLibre(habitacion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { BloquearHabitaciones = true });

            if (string.IsNullOrWhiteSpace(motivo))
                throw new SOTException(Recursos.Habitaciones.motivo_bloqueo_invalido_excepcion);

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Bloqueada;
            habitacion.IdUsuarioModifico = usuario.Id;
            habitacion.FechaModificacion = fechaActual;

            habitacion.BloqueosHabitaciones.Add(new BloqueoHabitacion
            {
                Activo = true,
                FechaCreacion = fechaActual,
                IdUsuarioCreo = usuario.Id,
                Motivo = motivo
            });

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        public void FinalizarMantenimiento(int idHabitacion, DtoUsuario usuario, List<int> idsOrdenesTrabajo)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaMantenimiento(habitacion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { FinalizarMantenimiento = true });

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Sucia;
            habitacion.IdUsuarioModifico = usuario.Id;
            habitacion.FechaModificacion = fechaActual;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioHabitaciones.Modificar(habitacion);
                RepositorioHabitaciones.GuardarCambios();

                ServicioMantenimientos.FinalizarMantenimiento(habitacion.Id, usuario.Id, idsOrdenesTrabajo);

                ServicioTareasLimpieza.CrearTarea(habitacion.Id, TareaLimpieza.TiposLimpieza.Detallado, usuario.Id);

                scope.Complete();
            }
        }

        public void FinalizarOcupacion(int idHabitacion, bool esCancelacion, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaOcupada(habitacion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { DesocuparHabitaciones = true });

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Sucia;
            habitacion.IdUsuarioModifico = usuario.Id;
            habitacion.FechaModificacion = fechaActual;

            List<int> idsVentasRentas = null;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioHabitaciones.Modificar(habitacion);
                RepositorioHabitaciones.GuardarCambios();

                if(esCancelacion)
                    idsVentasRentas = ServicioRentas.CancelarRenta(habitacion.Id, usuario);
                else
                    ServicioRentas.FinalizarRenta(habitacion.Id, usuario);

                ServicioTareasLimpieza.CrearTarea(habitacion.Id, TareaLimpieza.TiposLimpieza.Normal, usuario.Id);

                scope.Complete();
            }

            if (idsVentasRentas != null && idsVentasRentas.Count > 0)
            {
                var configuracionImpresoras = ServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();

                foreach (var idVentaRenta in idsVentasRentas)
                    ServicioTickets.ImprimirTicketVentaRentaCancelada(idVentaRenta, configuracionImpresoras.ImpresoraTickets, 1);
            }
        }

        public void HabilitarParaVentas(int idHabitacion, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaSupervision(habitacion);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { HabilitarVentasHabitacion = true });

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre;
            habitacion.IdUsuarioModifico = usuario.Id;
            habitacion.FechaModificacion = fechaActual;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        

        private void ValidarEstaPreparadaOPendienteCobro(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Preparada && habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.PendienteCobro)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_preparada_o_pendiente_cobro_excepcion, habitacion.NumeroHabitacion);
        }

        private void ValidarEstaOcupadaOPendienteCobro(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Ocupada && habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.PendienteCobro)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_ocupada_o_pendiente_cobro_excepcion, habitacion.NumeroHabitacion);
        }

        public List<Habitacion> ObtenerActivasConTipos(DtoUsuario usuario)
        {
            if(ServicioPermisos.VerificarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarHabitacionesTodas = true }))
                return RepositorioHabitaciones.ObtenerActivasConTipos();
            else if (ServicioPermisos.VerificarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarHabitacionesHabilitadas = true, ConsultarHabitacionesPreparadas = true }))
                return RepositorioHabitaciones.ObtenerPreparadasOHabilitadasConTipos();

            return new List<Habitacion>();
        }

        public BloqueoHabitacion ObtenerBloqueoActual(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDetallesHabitacion = true });

            var habitacion = RepositorioHabitaciones.ObtenerConMotivoBloqueo(idHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaBloqueada(habitacion);

            return habitacion.BloqueosHabitaciones.LastOrDefault(m => m.Activo);
        }

        public DtoDatosHabitacion ObtenerDatosHabitacion(string numeroHabitacion, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.NumeroHabitacion == numeroHabitacion && m.Activo, m => m.TipoHabitacion);

            if (habitacion == null)
                return null;

            var configuracionGlobal = ServicioParametros.ObtenerParametros();
            decimal multiplicadorIVA = configuracionGlobal.Iva_CatPar + 1.0M;

            var datos = new DtoDatosHabitacion
            {
                Id = habitacion.Id,
                Habitacion = habitacion.NumeroHabitacion,
                Tipo = habitacion.TipoHabitacion.Descripcion,
                Estado = habitacion.EstadoHabitacion
            };

            if (habitacion.EstadoHabitacion == Habitacion.EstadosHabitacion.Ocupada || habitacion.EstadoHabitacion == Habitacion.EstadosHabitacion.PendienteCobro)
            {
                var rentaActual = ServicioRentas.ObtenerRentaActualPorHabitacion(habitacion.Id);

                if (rentaActual != null)
                {
                    datos.FechaEntrada = rentaActual.FechaInicio;
                    datos.FechaSalida = rentaActual.FechaSalida;
                    datos.PrecioHabitacion = Math.Round(rentaActual.PrecioHabitacion * multiplicadorIVA, 2);
                    datos.TarjetaV = rentaActual.NumeroTarjeta;
                    datos.Reservacion = ServicioRentas.ObtieneCodigoReservacion(rentaActual.FechaRegistro, rentaActual.IdHabitacion);
                    datos.HospedajeExtra = rentaActual.VentasRenta.Where(m=>m.Activo && m.Cobrada).SelectMany(m=> m.Extensiones).Where(m=>m.Activa).Sum(m => Math.Round(m.Precio * multiplicadorIVA, 2));
                    datos.Paquetes = rentaActual.VentasRenta.Where(m => m.Activo && m.Cobrada).SelectMany(m => m.PaquetesRenta).Where(m => m.Activo).Sum(m => Math.Round(m.Precio * multiplicadorIVA, 2) * m.Cantidad);
                    datos.NumeroServicio = rentaActual.NumeroServicio;
                    datos.PersonasExtra = rentaActual.VentasRenta.Where(m => m.Activo && m.Cobrada).SelectMany(m => m.PersonasExtra).Where(m => m.Activa).Sum(m => Math.Round(m.Precio * multiplicadorIVA, 2));

                    var comandas = ServicioRoomServices.ObtenerDetallesComandasCobradas(rentaActual.Id);

                    datos.CortesiaHabitacion = rentaActual.VentasRenta.Where(m => m.Activo && m.Cobrada).SelectMany(m => m.Pagos).Where(m => m.Activo && m.TipoPago == Dominio.Nucleo.Entidades.TiposPago.Cortesia).Sum(m => m.Valor);

                    if (comandas.Any())
                    {
                        datos.PoseeComanda = true;
                        datos.ServicioRestaurante = comandas.SelectMany(m => m.ArticulosComanda).Sum(m => m.PrecioUnidadFinal * m.Cantidad);
                        datos.CortesiaHabitacion += comandas.SelectMany(m => m.ArticulosComanda).Where(m => m.EsCortesia).Sum(m => m.PrecioUnidadFinal * m.Cantidad);
                    }

                    if (rentaActual.RentaAutomoviles.Count(m => m.Activa) == 1)
                    {
                        var datosAuto = rentaActual.RentaAutomoviles.First();

                        datos.Matricula = datosAuto.Matricula;
                        datos.Marca = datosAuto.Marca;
                        datos.Modelo = datosAuto.Modelo;
                        datos.Color = datosAuto.Color;
                        //datos = datosAuto.
                    }
                    else if (!rentaActual.RentaAutomoviles.Any(m => m.Activa))
                    {
                        datos.Matricula = "A PIE";
                        datos.Marca = "A PIE";
                    }
                }
            }
            else if (habitacion.EstadoHabitacion == Habitacion.EstadosHabitacion.Sucia)
            {
                var tareaActual = ServicioTareasLimpieza.ObtenerTareaActualConEmpleados(habitacion.Id);

                if (tareaActual != null)
                    datos.TipoLimpieza = tareaActual.Tipo;

                var ultimaRenta = ServicioRentas.ObtenerUltimaRentaPorHabitacionCargada(habitacion.Id);

                if (ultimaRenta != null)
                {
                    datos.FechaEntrada = ultimaRenta.FechaInicio;
                    datos.FechaSalida = ultimaRenta.FechaSalida;
                    datos.PrecioHabitacion = Math.Round(ultimaRenta.PrecioHabitacion * multiplicadorIVA, 2);
                    datos.Reservacion = ServicioRentas.ObtieneCodigoReservacion(ultimaRenta.FechaRegistro, ultimaRenta.IdHabitacion);
                    datos.HospedajeExtra = ultimaRenta.VentasRenta.Where(m => m.Activo && m.Cobrada).SelectMany(m => m.Extensiones).Where(m => m.Activa).Sum(m => Math.Round(m.Precio * multiplicadorIVA, 2));
                    datos.Paquetes = ultimaRenta.VentasRenta.Where(m => m.Activo && m.Cobrada).SelectMany(m => m.PaquetesRenta).Where(m => m.Activo).Sum(m => Math.Round(m.Precio * multiplicadorIVA, 2) * m.Cantidad);
                    datos.NumeroServicio = ultimaRenta.NumeroServicio;
                    datos.PersonasExtra = ultimaRenta.VentasRenta.Where(m => m.Activo && m.Cobrada).SelectMany(m => m.PersonasExtra).Where(m => m.Activa).Sum(m => Math.Round(m.Precio * multiplicadorIVA, 2));

                    var comandas = ServicioRoomServices.ObtenerDetallesComandasCobradas(ultimaRenta.Id);

                    if (comandas.Any())
                    {
                        datos.PoseeComanda = true;
                        datos.ServicioRestaurante = comandas.SelectMany(m => m.ArticulosComanda).Sum(m => m.PrecioUnidadFinal * m.Cantidad);
                    }

                    if (ultimaRenta.RentaAutomoviles.Count(m => m.Activa) == 1)
                    {
                        var datosAuto = ultimaRenta.RentaAutomoviles.First();

                        datos.Matricula = datosAuto.Matricula;
                        //datos = datosAuto.
                    }
                    else if (!ultimaRenta.RentaAutomoviles.Any(m => m.Activa))
                        datos.Matricula = "A PIE";
                }
            }

            return datos;
        }

        public Habitacion ObtenerHabitacion(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo, m => m.TipoHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            return habitacion;
        }

        public void PonerEnMantenimiento(int idHabitacion, int idTareaMantenimiento, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaLibre(habitacion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { IniciarMantenimiento = true });

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Mantenimiento;
            habitacion.IdUsuarioModifico = usuario.Id;
            habitacion.FechaModificacion = fechaActual;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioHabitaciones.Modificar(habitacion);
                RepositorioHabitaciones.GuardarCambios();

                ServicioMantenimientos.Crear(habitacion.Id, idTareaMantenimiento, usuario.Id);

                scope.Complete();
            }
        }

        public void PrepararHabitacion(int idHabitacion, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Libre && habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Bloqueada)
                throw new SOTException(Recursos.Habitaciones.estado_invalido_preparacion_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos{ PrepararHabitaciones = true} );
           
            ValidaTiempoParaVenta(habitacion, usuario);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (habitacion.EstadoHabitacion == Habitacion.EstadosHabitacion.Bloqueada)
                {
                    DesbloquearHabitacion(habitacion.Id, usuario);

                    habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);
                }

                var fechaActual = DateTime.Now;

                AgregarHistorial(habitacion, fechaActual);

                habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Preparada;
                habitacion.IdUsuarioModifico = usuario.Id;
                habitacion.FechaModificacion = fechaActual;

                RepositorioHabitaciones.Modificar(habitacion);
                RepositorioHabitaciones.GuardarCambios();

                scope.Complete();
            }
        }

        private void ValidaTiempoParaVenta(Habitacion habitacion, DtoUsuario usuario)
        {

            TiempoLimpieza tiempoLimpieza;

            var tipoHabitacion = ServicioTiposHabitaciones.ObtenerTipoConTiemposLimpiezaPorHabitacion(habitacion.Id);
            if (tipoHabitacion != null)
                tiempoLimpieza = tipoHabitacion.TiemposLimpieza.FirstOrDefault(m => m.Activa && m.TipoLimpieza == TareaLimpieza.TiposLimpieza.MinutosVenta);
            else
                tiempoLimpieza = null;

            if(tiempoLimpieza!= null)
            {
               var TiempoActual = DateTime.Now;
                var diferenciaminuti = TiempoActual - habitacion.FechaModificacion;
                int minutostranscurridos = (int)Math.Round((decimal)diferenciaminuti.TotalMinutes);
                int minutosdelimpieza = tiempoLimpieza.Minutos;
                if(minutostranscurridos<= minutosdelimpieza)
                {
                    var usuariocreden = ServicioPermisos.ObtenerUsuarioPorCredencial();
                    ServicioPermisos.ValidarPermisos(usuariocreden, new Modelo.Seguridad.Dtos.DtoPermisos { AsignarHabitaciones = true });
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                        AutorizaMinutosVenta autorizaminutosventa = new AutorizaMinutosVenta();
                        autorizaminutosventa.FechaCreacion = TiempoActual;
                        autorizaminutosventa.NumeroHabitacion = habitacion.NumeroHabitacion;
                        autorizaminutosventa.MinutosConfiguracion = minutosdelimpieza;
                        autorizaminutosventa.MinutosAutoriza = (int)minutostranscurridos;
                        autorizaminutosventa.IdUsuarioAutorizo = usuariocreden.Id;
                        RepositorioAutorizaMinutosVentas.Agregar(autorizaminutosventa);
                        RepositorioAutorizaMinutosVentas.GuardarCambios();
                        scope.Complete();
                    }

                }
            }

        }

        public void CancelarPreparacionHabitacion(int idHabitacion, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaPreparada(habitacion);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { HabilitarVentasHabitacion = true });

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre;
            habitacion.IdUsuarioModifico = usuario.Id;
            habitacion.FechaModificacion = fechaActual;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        public void ValidarEstaMantenimiento(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Activo && m.Id == idHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaMantenimiento(habitacion);
        }
        public void ValidarEstaReservadaPreparada(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaReservadaPreparada(habitacion);
        }
        public void ValidarEstaPreparada(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaPreparada(habitacion);
        }
        public void ValidarEstaSuciaLimpiezaSupervision(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Activo && m.Id == idHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Sucia
                && habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Limpieza
                && habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Supervision)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_proceso_limpieza_excepcion, habitacion.NumeroHabitacion);
        }

        public void ValidarEstaOcupada(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaOcupada(habitacion);
        }

        public void ValidarEstaOcupadaOPendienteCobro(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaOcupadaOPendienteCobro(habitacion);
        }

        public void ValidarEstaSucia(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaSucia(habitacion);
        }

        public void ValidarEstaEnLimpieza(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaEnLimpieza(habitacion);
        }

        public void ValidarEstaEnSupervision(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaEnSupervision(habitacion);
        }

        private void DesbloquearHabitacion(int idHabitacion, DtoUsuario usuario)
        {
            var habitacion = RepositorioHabitaciones.ObtenerConMotivoBloqueo(idHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaBloqueada(habitacion);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { DesbloquearHabitaciones = true });

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre;
            habitacion.IdUsuarioModifico = usuario.Id;
            habitacion.FechaModificacion = fechaActual;

            foreach (var bloqueo in habitacion.BloqueosHabitaciones.Where(m => m.Activo))
            {
                bloqueo.Activo = false;
                bloqueo.FechaEliminacion = fechaActual;
                bloqueo.IdUsuarioElimino = usuario.Id;
            }

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }
        private void ValidarEstaBloqueada(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Bloqueada)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_bloqueada_excepcion, habitacion.NumeroHabitacion);
        }

        private void ValidarEstaOcupada(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Ocupada)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_ocupada_excepcion, habitacion.NumeroHabitacion);
        }

        private void ValidarEstaEnLimpieza(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Limpieza)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_en_limpieza_excepcion, habitacion.NumeroHabitacion);
        }

        private void ValidarEstaEnSupervision(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Supervision)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_en_supervision_excepcion, habitacion.NumeroHabitacion);
        }

        public void ValidarEstaLibre(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Activo && m.Id == idHabitacion);

            ValidarEstaLibre(habitacion);
        }

        private void ValidarEstaLibre(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Libre)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_habilitada_ventas_excepcion, habitacion.NumeroHabitacion);
        }

        public void ValidarEstaReservada(int idHabitacion) 
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion);

            ValidarEstaReservada(habitacion);
        }

        private void ValidarEstaReservada(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Reservada)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_reservada_excepcion, habitacion.NumeroHabitacion);
        }

        private void ValidarEstaMantenimiento(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Mantenimiento)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_mantenimiento_excepcion, habitacion.NumeroHabitacion);
        }
        private void ValidarEstaPreparada(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Preparada)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_preparada_excepcion, habitacion.NumeroHabitacion);
        }
        private void ValidarEstaPreparadaUOcupada(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Preparada && habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Ocupada)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_preparada_u_ocupada_excepcion, habitacion.NumeroHabitacion);
        }

        private void ValidarEstaPendienteCobro(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.PendienteCobro)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_pendiente_cobro_excepcion, habitacion.NumeroHabitacion);
        }
        private void ValidarEstaReservadaPreparada(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.PreparadaReservada)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_preparada_reservada_excepcion, habitacion.NumeroHabitacion);
        }

        private void ValidarEstaSucia(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Sucia)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_sucia_excepcion, habitacion.NumeroHabitacion);
        }
        private void ValidarEstaSupervision(Habitacion habitacion)
        {
            if (habitacion.EstadoHabitacion != Habitacion.EstadosHabitacion.Supervision)
                throw new SOTException(Recursos.Habitaciones.habitacion_no_limpia_excepcion, habitacion.NumeroHabitacion);
        }

        public void ValidarAcceso(DtoUsuario usuarioActual)
        {
            ServicioPermisos.ValidarPermisos(usuarioActual, new Modelo.Seguridad.Dtos.DtoPermisos { AccesoModuloHotel = true });
        }



        public void MarcarComoReservada(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { MarcarHabitacionComoReservada = true });

            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion, m => m.TipoHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaLibre(habitacion);

            var cantidadReservaciones = ServicioReservaciones.ObtenerCantidadReservacionesPendientesDia(habitacion.IdTipoHabitacion);

            if (cantidadReservaciones == 0)
                throw new SOTException(Recursos.Reservaciones.limite_reservaciones_superado_excepcion, habitacion.TipoHabitacion.Descripcion);

            ((IServicioHabitacionesInterno)this).MarcarComoReservada(idHabitacion, usuario.Id);
        }


        public void CancelarEstadoReservacionHabitacion(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarAnclajeReservaciones = true });

            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion, m => m.TipoHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaReservada(habitacion);

            if (ServicioReservaciones.VerificarPoseeReservacionNoConsumida(habitacion.IdTipoHabitacion))
                throw new SOTException(Recursos.Habitaciones.habitacion_posee_reservacion_excepcion);

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre;
            habitacion.FechaModificacion = fechaActual;
            habitacion.IdUsuarioModifico = usuario.Id;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }


        public void ValidarEstaPendienteCobro(int idHabitacion)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaPendienteCobro(habitacion);
        }


        public bool VerificarExistenPorTipo(int idTipoHabitacion)
        {
            return RepositorioHabitaciones.Alguno(m => m.Activo && m.IdTipoHabitacion == idTipoHabitacion);
        }

        public DtoResumenHabitacion SP_ObtenerResumenHabitacion(int idHabitacion, int? cambiosup) 
        {
            return RepositorioHabitaciones.SP_ObtenerResumenHabitacion(idHabitacion, cambiosup);
        }

        public List<DtoResumenHabitacion> SP_ObtenerResumenesHabitaciones()
        {
            return RepositorioHabitaciones.SP_ObtenerResumenesHabitaciones();
        }

        public List<DtoEstadisticasHabitacion> ObtenerEstadisticasHabitaciones() 
        {
            var estadisticas = RepositorioHabitaciones.ObtenerEstadisticasHabitaciones();

            foreach (var itemEnum in EnumExtensions.ComoLista<Habitacion.EstadosHabitacion>()) 
            {
                if (!estadisticas.Any(m => m.EstadoHabitacion == itemEnum))
                    estadisticas.Add(new DtoEstadisticasHabitacion
                    {
                        Cantidad = 0,
                        EstadoHabitacion = itemEnum
                    });
            }

            return estadisticas.OrderBy(m => m.EstadoHabitacion).ToList();
        }

        #region métodos internal

        void IServicioHabitacionesInterno.CancelarOcupacionInterno(int idHabitacion, int idUsuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            //ValidarEstaOcupada(habitacion);

            
            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Sucia;
            habitacion.IdUsuarioModifico = idUsuario;
            habitacion.FechaModificacion = fechaActual;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioHabitaciones.Modificar(habitacion);
                RepositorioHabitaciones.GuardarCambios();

                ServicioTareasLimpieza.CrearTarea(habitacion.Id, TareaLimpieza.TiposLimpieza.Normal, idUsuario);

                scope.Complete();
            }
        }

        void IServicioHabitacionesInterno.MarcarComoPendienteCobro(int idHabitacion, int idUsuario)
        {

            var habitacion = RepositorioHabitaciones.Obtener(m => m.Activo && m.Id == idHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            ValidarEstaPreparadaUOcupada(habitacion);

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.PendienteCobro;
            habitacion.IdUsuarioModifico = idUsuario;
            habitacion.FechaModificacion = fechaActual;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        void IServicioHabitacionesInterno.ValidarMismoTipo(params int[] idsHabitaciones) 
        {
            if (idsHabitaciones.Length == 0)
                throw new SOTException(Recursos.Habitaciones.conjunto_validar_vacion_excepcion);

            var idsMaterializadas = idsHabitaciones.ToList();

            var resultado = RepositorioHabitaciones.ObtenerElementos(m => m.Activo && idsMaterializadas.Contains(m.Id)).Select(m => m.IdTipoHabitacion).ToList();

            if (resultado.Count != idsMaterializadas.Count)
                throw new SOTException(Recursos.Habitaciones.habitaciones_nulas_eliminadas_excepcion);

            if (resultado.Distinct().Count() != 1)
                throw new SOTException(Recursos.Habitaciones.tipos_variados_excepcion);
        }

        void IServicioHabitacionesInterno.HabilitarParaVentas(int idHabitacion, int idUsuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            //ValidarEstaSupervision(habitacion);
            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre;
            habitacion.IdUsuarioModifico = idUsuario;
            habitacion.FechaModificacion = fechaActual;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        void IServicioHabitacionesInterno.MarcarComoReservada(int idHabitacion, int idUsuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion, m => m.TipoHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Reservada;
            habitacion.FechaModificacion = fechaActual;
            habitacion.IdUsuarioModifico = idUsuario;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        void IServicioHabitacionesInterno.MarcarComoPreparadaReservada(int idHabitacion, int idUsuario)
        {
            if (Transaction.Current == null)
                throw new SOTException("Se requiere una transacción abierta para realizar la operación");

            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //ServicioReservaciones.ValidarPoseeReservacionConfirmada(habitacion.Id);

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.PreparadaReservada;
            habitacion.FechaModificacion = fechaActual;
            habitacion.IdUsuarioModifico = idUsuario;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        void IServicioHabitacionesInterno.MarcarEnLimpieza(int idHabitacion, int idUsuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaSucia(habitacion);

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Limpieza;
            habitacion.IdUsuarioModifico = idUsuario;
            habitacion.FechaModificacion = fechaActual;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        void IServicioHabitacionesInterno.MarcarComoOcupada(int idHabitacion, string codigoReservacion, int idUsuario)
        {
            var habitacion = codigoReservacion == null ?
                RepositorioHabitaciones.Obtener(m => m.Activo && m.Id == idHabitacion) :
                RepositorioHabitaciones.ObtenerConReservacionConfirmada(idHabitacion);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            var reservacion = habitacion.Reservaciones.FirstOrDefault(m => m.Activo && m.Estado == Reservacion.EstadosReservacion.Confirmada);

            if ((reservacion != null && reservacion.CodigoReserva != codigoReservacion) || (reservacion == null && codigoReservacion != null))
                throw new SOTException(Recursos.Reservaciones.codigo_reserva_confirmada_invalido_excepcion, codigoReservacion);
            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación

            if (reservacion == null)
                ValidarEstaPreparadaOPendienteCobro(habitacion);
            else
                ValidarEstaReservadaPreparada(habitacion);

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Ocupada;
            habitacion.IdUsuarioModifico = idUsuario;
            habitacion.FechaModificacion = fechaActual;

            if (reservacion != null)
            {
                reservacion.Estado = Reservacion.EstadosReservacion.Consumida;
                reservacion.FechaModificacion = fechaActual;
                reservacion.IdUsuarioModifico = idUsuario;
            }

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        void IServicioHabitacionesInterno.MarcarEnSupervision(int idHabitacion, int idUsuario)
        {
            var habitacion = RepositorioHabitaciones.Obtener(m => m.Id == idHabitacion && m.Activo);

            if (habitacion == null)
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());

            //Aqui se invocan a los métodos necesarios para validar los estados válidos de la habitación
            ValidarEstaEnLimpieza(habitacion);

            var fechaActual = DateTime.Now;

            AgregarHistorial(habitacion, fechaActual);

            habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Supervision;
            habitacion.IdUsuarioModifico = idUsuario;
            habitacion.FechaModificacion = fechaActual;

            RepositorioHabitaciones.Modificar(habitacion);
            RepositorioHabitaciones.GuardarCambios();
        }

        void IServicioHabitacionesInterno.ValidarEsActiva(int idHabitacion)
        {
            if (!RepositorioHabitaciones.Alguno(m => m.Activo && m.Id == idHabitacion))
                throw new SOTException(Recursos.Habitaciones.id_invalido_excepcion, idHabitacion.ToString());
        }

        int IServicioHabitacionesInterno.ObtenerCantidadHabitacionesReservadasPorTipo(int idTipoHabitacion)
        {
            var estadoReservado = (int)Habitacion.EstadosHabitacion.Reservada;

            return RepositorioHabitaciones.Contador(m => m.IdEstadoHabitacion == estadoReservado && m.IdTipoHabitacion == idTipoHabitacion);
        }

        int IServicioHabitacionesInterno.ObtenerCantidadActivarPorTipo(int idTipoHabitacion) 
        {
            return RepositorioHabitaciones.Contador(m => m.Activo && m.IdTipoHabitacion == idTipoHabitacion);
        }

        private void AgregarHistorial(Habitacion habitacion, DateTime fechaHistorial)
        {
            habitacion.HistorialesHabitacion.Add(new HistorialHabitacion
            {
                Activo = habitacion.Activo,
                IdEstadoHabitacion = habitacion.IdEstadoHabitacion,
                FechaInicio = habitacion.FechaModificacion,
                FechaFin = fechaHistorial.AddSeconds(-1),
                IdTipoHabitacion = habitacion.IdTipoHabitacion,
                IdUsuarioModifico = habitacion.IdUsuarioCreo,
                NumeroHabitacion = habitacion.NumeroHabitacion,
                Piso = habitacion.Piso,
                Posicion = habitacion.Posicion,
                EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Creado
            });
        }

        public void FinalizarConfiguracionHabitaciones(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { AdministrarHabitaciones = true });


            var idEstadoCreada = (int)Habitacion.EstadosHabitacion.EnCreacion;

            var habitaciones = RepositorioHabitaciones.ObtenerElementos(m => m.IdEstadoHabitacion == idEstadoCreada && m.Activo).ToList();

            foreach (var habitacion in habitaciones)
            {
                habitacion.FechaModificacion = DateTime.Now;
                habitacion.IdUsuarioModifico = usuario.Id;
                habitacion.EstadoHabitacion = Habitacion.EstadosHabitacion.Libre;

                RepositorioHabitaciones.Modificar(habitacion);
            }

            RepositorioHabitaciones.GuardarCambios();
        }

        #endregion
    }
}