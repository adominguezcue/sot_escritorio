﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Habitaciones
{
    internal interface IServicioHabitacionesInterno : IServicioHabitaciones
    {
        /// <summary>
        /// Cambia el estado de una habitación a Limpieza siempre y cuando el usuario tenga los
        /// permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void MarcarEnLimpieza(int idHabitacion, int idUsuario);
        /// <summary>
        /// Marca una habitación como ocupada siempre y cuando el usuario tenga los permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void MarcarComoOcupada(int idHabitacion, string codigoReservacion, int idUsuario);
        /// <summary>
        /// Cambia el estado de una habitación a Supervisión siempre y cuando el usuario tenga los
        /// permisos necesarios
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void MarcarEnSupervision(int idHabitacion, int idUsuario);
        /// <summary>
        /// Valida que exista una habitación activa con el id proporcionado
        /// </summary>
        /// <param name="idHabitacion"></param>
        void ValidarEsActiva(int idHabitacion);
        /// <summary>
        /// Retorna la cantidad de habitaciones reservadas por tipo de habitación
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <returns></returns>
        int ObtenerCantidadHabitacionesReservadasPorTipo(int idTipoHabitacion);
        /// <summary>
        /// Retorna la cantidad de habitaciones activas que pertenecen al tipo con el id proporcionado
        /// </summary>
        /// <param name="idTipo"></param>
        /// <returns></returns>
        int ObtenerCantidadActivarPorTipo(int idTipo);
        /// <summary>
        /// Permite marcar como preparada y reservada una habitación
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void MarcarComoPreparadaReservada(int idHabitacion, int idUsuario);

        /// <summary>
        /// Permite marcar una habitación como reservada
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void MarcarComoReservada(int idHabitacion, int idUsuario);
        void HabilitarParaVentas(int idHabitacion, int idUsuario);
        /// <summary>
        /// Valida que todas las habitaciones estén activas y sean del mismo tipo
        /// </summary>
        /// <param name="idsHabitaciones"></param>
        void ValidarMismoTipo(params int[] idsHabitaciones);
        /// <summary>
        /// Permite otorgar a una habitación el estado pendiente de cobro
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        void MarcarComoPendienteCobro(int idHabitacion, int idUsuario);
        void CancelarOcupacionInterno(int idHabitacion, int idUsuario);
    }
}
