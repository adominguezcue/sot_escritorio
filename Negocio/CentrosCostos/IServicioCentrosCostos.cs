﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Compartido.CentrosCostos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.CentrosCostos
{
    public interface IServicioCentrosCostos : IServicioCentrosCostosCompartido
    {
        /// <summary>
        /// Permite crear un centro de costos
        /// </summary>
        /// <param name="centroCostos"></param>
        /// <param name="usuario"></param>
        void CrearCentroCostos(CentroCostos centroCostos, DtoUsuario usuario);
        /// <summary>
        /// Retorna los centros de costos activos en el sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<CentroCostos> ObtenerCentrosCostosFILTRO(bool cargarNombreCategoria);
        /// <summary>
        /// Retorna la lista de categorías activas de los centros de costos
        /// </summary>
        /// <returns></returns>
        List<CategoriaCentroCostos> ObtenerCategoriasCentrosCostos();
        /// <summary>
        /// Retorna la lista de centros de costos activos filtrados
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombreCentro"></param>
        /// <param name="idCategoriaCentro"></param>
        /// <returns></returns>
        List<CentroCostos> ObtenerCentrosCostosFiltrados(DtoUsuario usuario, string nombreCentro = null, int? idCategoriaCentro = null);
        /// <summary>
        /// Permite actualizar la información de un centro de costos
        /// </summary>
        /// <param name="centroActual"></param>
        /// <param name="usuario"></param>
        void ModificarCentroCostos(CentroCostos centroActual, DtoUsuario usuario);
        /// <summary>
        /// Elimina el centro de costos con el id proporcionado
        /// </summary>
        /// <param name="idCentroCostos"></param>
        /// <param name="usuario"></param>
        void EliminarCentroCostos(int idCentroCostos, DtoUsuario usuario);
    }
}
