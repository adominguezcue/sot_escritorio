﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.CentrosCostos
{
    internal interface IServicioCentrosCostosInterno : IServicioCentrosCostos
    {
        ///// <summary>
        ///// Retorna el presupuesto restante de un centro de costos, que es el resultado de restar a
        ///// su tope el valor de los topes de sus conceptos de gastos. Si se proporciona un id de
        ///// concepto, éste será ignorado en la operación
        ///// </summary>
        ///// <param name="idCentroCostos"></param>
        ///// <param name="idConceptoIgnorar"></param>
        ///// <returns></returns>
        //decimal ObtenerPresupuestoRestante(int idCentroCostos, int? idConceptoIgnorar);
        bool VerificarPoseeVinculaciones(int idCentroCostos);
    }
}
