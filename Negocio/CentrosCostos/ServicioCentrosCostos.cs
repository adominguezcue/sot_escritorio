﻿using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Almacenes;
using Negocio.ConceptosGastos;
using Negocio.Gastos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.CentrosCostos
{
    public class ServicioCentrosCostos : IServicioCentrosCostosInterno
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioAlmacenes ServicioAlmacenes
        {
            get { return _servicioAlmacenes.Value; }
        }

        Lazy<IServicioAlmacenes> _servicioAlmacenes = new Lazy<IServicioAlmacenes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioAlmacenes>(); });


        IServicioGastosInterno ServicioGastos
        {
            get { return _servicioGastos.Value; }
        }

        Lazy<IServicioGastosInterno> _servicioGastos = new Lazy<IServicioGastosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioGastosInterno>(); });

        IServicioConceptosGastosInterno ServicioConceptosGastos
        {
            get { return _servicioConceptosGastos.Value; }
        }

        Lazy<IServicioConceptosGastosInterno> _servicioConceptosGastos = new Lazy<IServicioConceptosGastosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosGastosInterno>(); });


        IRepositorioCentrosCostos RepositorioCentrosCostos
        {
            get { return _repositorioCentrosCostos.Value; }
        }

        Lazy<IRepositorioCentrosCostos> _repositorioCentrosCostos = new Lazy<IRepositorioCentrosCostos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCentrosCostos>(); });

        IRepositorioCategoriasCentroCostos RepositorioCategoriasCentroCostos
        {
            get { return _repositorioCategoriasCentroCostos.Value; }
        }

        Lazy<IRepositorioCategoriasCentroCostos> _repositorioCategoriasCentroCostos = new Lazy<IRepositorioCategoriasCentroCostos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCategoriasCentroCostos>(); });


        public void CrearCentroCostos(CentroCostos centroCostos, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearCentroCostos = true });

            ValidarCamposObligatorios(centroCostos);

            if (RepositorioCentrosCostos.Alguno(m => m.Activo && m.Nombre.Trim().ToUpper().Equals(centroCostos.Nombre.Trim().ToUpper())))
                throw new SOTException(Recursos.CentrosCostos.nombre_duplicado_excepcion, centroCostos.Nombre);

            var fechaActual = DateTime.Now;

            centroCostos.Activo = true;
            centroCostos.FechaCreacion = fechaActual;
            centroCostos.FechaModificacion = fechaActual;
            centroCostos.IdUsuarioCreo = usuario.Id;
            centroCostos.IdUsuarioModifico = usuario.Id;

            RepositorioCentrosCostos.Agregar(centroCostos);
            RepositorioCentrosCostos.GuardarCambios();
        }

        public void ModificarCentroCostos(CentroCostos centroCostos, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarCentroCostos = true });

            ValidarCamposObligatorios(centroCostos);

            if (!RepositorioCentrosCostos.Alguno(m => m.Id == centroCostos.Id && m.Activo))
                throw new SOTException(Recursos.CentrosCostos.centro_costos_nulo_eliminado_excepcion);

            if (RepositorioCentrosCostos.Alguno(m => m.Id != centroCostos.Id && m.Activo && m.Nombre.Trim().ToUpper().Equals(centroCostos.Nombre.Trim().ToUpper())))
                throw new SOTException(Recursos.CentrosCostos.nombre_duplicado_excepcion, centroCostos.Nombre);

            //var presupuestoConceptosRegistrados = RepositorioCentrosCostos.ObtenerPresupuestoGastos(centroCostos.Id);

            //if (centroCostos.TopePresupuestal < presupuestoConceptosRegistrados)
            //    throw new SOTException(Recursos.CentrosCostos.tope_presupuestal_menor_conceptos_registrados_excepcion, presupuestoConceptosRegistrados.ToString("C"));

            var fechaActual = DateTime.Now;

            centroCostos.Activo = true;
            centroCostos.FechaModificacion = fechaActual;
            centroCostos.IdUsuarioModifico = usuario.Id;

            RepositorioCentrosCostos.Modificar(centroCostos);
            RepositorioCentrosCostos.GuardarCambios();
        }

        private void ValidarCamposObligatorios(CentroCostos centroCostos)
        {
            if (string.IsNullOrWhiteSpace(centroCostos.Nombre))//!UtilidadesRegex.SoloLetras(centroCostos.Nombre))
                throw new SOTException("El nombre del centro de costos no puede ser texto vacío");//Recursos.CentrosCostos.nombre_invalido_excepcion);

            if (centroCostos.IdCategoria <= 0)
                throw new SOTException(Recursos.CentrosCostos.categoria_no_seleccionada_excepcion);

            //if (centroCostos.TopePresupuestal <= 0)
            //    throw new SOTException(Recursos.CentrosCostos.tope_presupuestal_invalido_excepcion);
        }

        public void EliminarCentroCostos(int idCentroCostos, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarCentroCostos = true });

            var centroCostos = RepositorioCentrosCostos.Obtener(m => m.Id == idCentroCostos && m.Activo);

            if (centroCostos == null)
                throw new SOTException(Recursos.CentrosCostos.centro_costos_nulo_eliminado_excepcion);

            if (!ServicioConceptosGastos.VerificarNoPoseeConceptos(idCentroCostos))
                throw new SOTException(Recursos.CentrosCostos.centro_costos_posee_conceptos_excepcion);

            var fechaActual = DateTime.Now;

            centroCostos.Activo = false;
            centroCostos.FechaModificacion = fechaActual;
            centroCostos.IdUsuarioModifico = usuario.Id;
            centroCostos.FechaEliminacion = fechaActual;
            centroCostos.IdUsuarioElimino = usuario.Id;

            RepositorioCentrosCostos.Modificar(centroCostos);
            RepositorioCentrosCostos.GuardarCambios();
        }

        public List<CentroCostos> ObtenerCentrosCostosFILTRO(bool cargarNombreCategoria)
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarCentroCostos = true });

            if (cargarNombreCategoria)
                return RepositorioCentrosCostos.ObtenerCentrosCostosConNombreCategoria();

            return RepositorioCentrosCostos.ObtenerElementos(m => m.Activo).ToList();
        }


        public List<CategoriaCentroCostos> ObtenerCategoriasCentrosCostos()
        {
            return RepositorioCategoriasCentroCostos.ObtenerElementos(m => m.Activa).ToList();
        }


        public List<CentroCostos> ObtenerCentrosCostosFiltrados(DtoUsuario usuario, string nombreCentro = null, int? idCategoriaCentro = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarCentroCostos = true });

            return RepositorioCentrosCostos.ObtenerCentrosCostosFiltrados(nombreCentro, idCategoriaCentro);
        }

        //decimal IServicioCentrosCostosInterno.ObtenerPresupuestoRestante(int idCentroCostos, int? idConceptoIgnorar)
        //{
        //    return RepositorioCentrosCostos.ObtenerPresupuestoRestante(idCentroCostos, idConceptoIgnorar);
        //}

        public void ValidarExistencia(int idCentroCostos)
        {
            if (!RepositorioCentrosCostos.Alguno(m => m.Activo && m.Id == idCentroCostos))
                throw new SOTException(Recursos.CentrosCostos.centro_costos_nulo_eliminado_excepcion);


            if (!ServicioConceptosGastos.VerificarPoseeEnmascarador(idCentroCostos))
                throw new SOTException(Recursos.CentrosCostos.centro_costos_no_enmascarador_excepcion);
        }


        public void RegistrarGasto(int idCentroCostos, decimal valor, int idUsuario)
        {
            ServicioGastos.CrearGastoAutomatico(idCentroCostos, valor, idUsuario);
        }

        bool IServicioCentrosCostosInterno.VerificarPoseeVinculaciones(int idCentroCostos) 
        {
            return ServicioAlmacenes.VerificarPoseeVinculaciones(idCentroCostos);
        }
    }
}
