﻿using Modelo.Dtos;
using Modelo.Dtos.Reportes;
using Transversal.Utilidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;
using Transversal.Excepciones;
using Modelo.Seguridad.Dtos;

namespace Negocio.Reportes
{
    public interface IServicioReportes
    {
        DtoReporte GetReport<IEntity>(List<IEntity> elements, DtoUsuario usuario) where IEntity : class, IElementoReporte;

        DtoReporte GetReport(DataTable data, string reportName, DtoUsuario usuario);

        DtoReporte GetReportFromSheets(List<ExcelData.DtoWorkSheet> sheets, string reportName, DtoUsuario usuario, string folioCorte = null); //where IEntity : class, IReportElement;
    }
}
