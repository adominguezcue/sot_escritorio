﻿using Modelo.Dtos;
using Modelo.Dtos.Reportes;
using Transversal.Utilidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;
using Transversal.Excepciones;
using Modelo.Seguridad.Dtos;

namespace Negocio.Reportes
{
    public class ServicioReportes : IServicioReportes
    {
        public DtoReporte GetReport<IEntity>(List<IEntity> elements, DtoUsuario usuario)
            where IEntity : class, IElementoReporte
        {
            var report = new DtoReporte();

            string reportName;

            var displayAttribute = typeof(IEntity).GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault() as DisplayNameAttribute;

            if (displayAttribute != null)
                reportName = displayAttribute.DisplayName;
            else
                reportName = typeof(IEntity).Name;

            report.ReportName = reportName + "(" + DateTime.Now.ToString("dd-MMMM-yyyy", System.Globalization.CultureInfo.GetCultureInfo("es-MX")) + ")" + ".xlsx";

            var tableData = new ExcelData.DtoDataTable();

            tableData.Headers = new Dictionary<string, string>();

            tableData.Headers.Add("Fecha: ", DateTime.Now.ToShortDateString());
            tableData.Headers.Add("Hora: ", DateTime.Now.ToString("hh:mm tt"));
            tableData.Headers.Add("Usuario: ", usuario.NombreCompleto);

            tableData.Data = elements.ToDataTable();;

            var sheet = new ExcelData.DtoWorkSheet
            {
                SheetName = reportName,
                DtoSheetSections = new List<ExcelData.DtoSheetSection>() 
                {
                    new ExcelData.DtoSheetSection()
                    { 
                        DtoSectionRows = new List<ExcelData.DtoSheetSectionRow>
                        { 
                            new ExcelData.DtoSheetSectionRow
                            {
                                DtoDataTables = new List<ExcelData.DtoDataTable>() 
                                { 
                                    tableData 
                                }
                            }
                        }
                        //ColumnCount = tableData.Data.Columns.Count
                    }
                }
            };

            report.Data = ExcelData.GenerateExcelBytesFromDataTable(new List<ExcelData.DtoWorkSheet>{ sheet } , reportName);
            report.FileSize = report.Data.Length;

            return report;
        }

        public DtoReporte GetReport(DataTable data, string reportName, DtoUsuario usuario)
        {
            var report = new DtoReporte();

            //string reportName;

            //var displayAttribute = typeof(IEntity).GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault() as DisplayNameAttribute;

            //if (displayAttribute != null)
            //    reportName = displayAttribute.DisplayName;
            //else
            //    reportName = typeof(IEntity).Name;

            report.ReportName = reportName + "(" + DateTime.Now.ToString("dd-MMMM-yyyy", System.Globalization.CultureInfo.GetCultureInfo("es-MX")) + ")" + ".xlsx";

            var tableData = new ExcelData.DtoDataTable();

            tableData.Headers = new Dictionary<string, string>();

            tableData.Headers.Add("Fecha: ", DateTime.Now.ToShortDateString());
            tableData.Headers.Add("Hora: ", DateTime.Now.ToString("hh:mm tt"));
            tableData.Headers.Add("Usuario: ", usuario.NombreCompleto);

            tableData.Data = data;

            var sheet = new ExcelData.DtoWorkSheet
            {
                SheetName = reportName,
                DtoSheetSections = new List<ExcelData.DtoSheetSection>() 
                {
                    new ExcelData.DtoSheetSection()
                    { 
                        DtoSectionRows = new List<ExcelData.DtoSheetSectionRow>
                        { 
                            new ExcelData.DtoSheetSectionRow
                            {
                                DtoDataTables = new List<ExcelData.DtoDataTable>() 
                                { 
                                    tableData 
                                }
                            }
                        }
                        //ColumnCount = tableData.Data.Columns.Count
                    }
                }
            };

            report.Data = ExcelData.GenerateExcelBytesFromDataTable(new List<ExcelData.DtoWorkSheet> { sheet }, reportName);
            report.FileSize = report.Data.Length;

            return report;
        }

        public DtoReporte GetReportFromSheets(List<ExcelData.DtoWorkSheet> sheets, string reportName, DtoUsuario usuario, string folioCorte = null)
            //where IEntity : class, IReportElement
        {
            var report = new DtoReporte();

            if (sheets == null || sheets.Count == 0)
            {
                report.ReportName = "vacío.txt";
                report.Data = Encoding.UTF8.GetBytes("No existen elementos para generar el reporte de " + reportName);
                report.FileSize = report.Data.Length;

                return report;
            }

            if (string.IsNullOrWhiteSpace(folioCorte))
                report.ReportName = reportName + "(" + DateTime.Now.ToString("dd-MMMM-yyyy", System.Globalization.CultureInfo.GetCultureInfo("es-MX")) + ")" + ".xlsx";
            else
                report.ReportName = reportName + "(" + DateTime.Now.ToString("dd-MMMM-yyyy", System.Globalization.CultureInfo.GetCultureInfo("es-MX")) + ", Corte " + folioCorte + ").xlsx";

            foreach (var sheet in sheets)
            {
                var tableData = new ExcelData.DtoDataTable();

                tableData.Headers = new Dictionary<string, string>();

                tableData.Headers.Add("Fecha: ", DateTime.Now.ToShortDateString());
                tableData.Headers.Add("Hora: ", DateTime.Now.ToString("hh:mm tt"));
                tableData.Headers.Add("Usuario: ", usuario.NombreCompleto);
                if(!string.IsNullOrWhiteSpace(folioCorte))
                    tableData.Headers.Add("Folio de corte: ", folioCorte);

                tableData.Data = new DataTable();

                //tableData.Data = table;

                var infoSection = new ExcelData.DtoSheetSection();

                infoSection.AddTableAsSectionRow(tableData);

                sheet.DtoSheetSections.Insert(0, infoSection);

                //infoSection.ColumnCount = tableData.Data.Columns.Count;

                //tableData.ColumnCount = sheet.DtoSheetSections.SelectMany(m => m.DtoDataTables).Max(m => m.Data.Columns.Count);

            }

            report.Data = ExcelData.GenerateExcelBytesFromDataTable(sheets, reportName);
            //System.IO.File.ReadAllBytes(tempDir + realName);
            report.FileSize = report.Data.Length;

            return report;
        }
    }
}
