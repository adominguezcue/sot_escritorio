﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using Modelo.Repositorios;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesGlobales;
using Negocio.CortesTurno;
using Negocio.Propinas;
using Negocio.Rentas;
using Negocio.Restaurantes;
using Negocio.ServiciosExternos.Agentes.Sincronizacion;
using Negocio.ServiciosExternos.DTOs.Sincronizacion;
using Negocio.ServiciosExternos.wsSincronizacion;
using Negocio.Taxis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using serviciosE = Negocio.ServiciosExternos;

namespace Negocio.Sincronizacion
{
    public class ServicioSincronizacion : IServicioSincronizacion
    {

        private static readonly string[] FOLIOS_MAGICOS = new string[]
        {
            "GENERAL",
            "INGRESO HABITACION",
            "INGRESO COMANDA",
            "INGRESO PAGOS",
            "CORTE"
        };

        static Dictionary<serviciosE.Enums.Rastrilleo.EnumFormasPago, List<int>> PAGOS_TARJETAS = new Dictionary<serviciosE.Enums.Rastrilleo.EnumFormasPago, List<int>> 
        { 
            //{serviciosE.Enums.Rastrilleo.EnumFormasPago.Credito, null/*new List<int>{1,2,3}*/}, 
            //{serviciosE.Enums.Rastrilleo.EnumFormasPago.Debito, null/*new List<int>{1,2,3}*/}, 
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.Efectivo, null}, 
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.Paypal, null}, 
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.Tarjeta, new List<int>{1,2,3}},
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.Consumo, null}, 
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.Cortesia, null}, 
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.Cupon, null}, 
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.VPoints, null}, 
            {serviciosE.Enums.Rastrilleo.EnumFormasPago.Reservacion, null}
        };

        /*
         case TiposPago.Efectivo:
                                    idTipoPago = 1;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Reservacion:
                                    idTipoPago = 2;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.TarjetaCredito:
                                    idTipoPago = 3;
                                    idTipoTarjeta = pago.IdTipoTarjeta.Value;
                                    break;
         */

        IRepositorioTiposHabitacionSincronizacion RepositorioTiposHabitacionSincronizaciones
        {
            get { return _repostorioTiposHabitacionSincronizaciones.Value; }
        }

        Lazy<IRepositorioTiposHabitacionSincronizacion> _repostorioTiposHabitacionSincronizaciones = new Lazy<IRepositorioTiposHabitacionSincronizacion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTiposHabitacionSincronizacion>(); });

        IServicioTaxisInterno ServicioTaxis
        {
            get { return _servicioTaxis.Value; }
        }

        Lazy<IServicioTaxisInterno> _servicioTaxis = new Lazy<IServicioTaxisInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTaxisInterno>(); });

        IServicioCortesTurnoInterno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurnoInterno> _servicioCortesTurno = new Lazy<IServicioCortesTurnoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurnoInterno>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioRentas ServicioRentas
        {
            get { return _servicioRentas.Value; }
        }

        Lazy<IServicioRentas> _servicioRentas = new Lazy<IServicioRentas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRentas>(); });

        IServicioRestaurantes ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantes> _servicioRestaurantes = new Lazy<IServicioRestaurantes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantes>(); });
        IServicioPropinasInterno ServicioPropinas
        {
            get { return _servicioPropinas.Value; }
        }

        Lazy<IServicioPropinasInterno> _servicioPropinas = new Lazy<IServicioPropinasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPropinasInterno>(); });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IRepositorioVentas RepositorioVentas
        {
            get { return _repostorioVentas.Value; }
        }

        Lazy<IRepositorioVentas> _repostorioVentas = new Lazy<IRepositorioVentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentas>(); });

        IRepositorioGastos RepositorioGastos
        {
            get { return _repostorioGastos.Value; }
        }

        Lazy<IRepositorioGastos> _repostorioGastos = new Lazy<IRepositorioGastos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioGastos>(); });

        IRepositorioTiposHabitacion RepositorioTiposHabitacion
        {
            get { return _repostorioTiposHabitacion.Value; }
        }

        Lazy<IRepositorioTiposHabitacion> _repostorioTiposHabitacion = new Lazy<IRepositorioTiposHabitacion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTiposHabitacion>(); });

        IRepositorioFajillas RepositorioFajillas
        {
            get { return _repostorioFajillas.Value; }
        }

        Lazy<IRepositorioFajillas> _repostorioFajillas = new Lazy<IRepositorioFajillas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioFajillas>(); });

        IRepositorioConfiguracionesFajillas RepositorioConfiguracionesFajillas
        {
            get { return _repostorioConfiguracionesFajillas.Value; }
        }

        Lazy<IRepositorioConfiguracionesFajillas> _repostorioConfiguracionesFajillas = new Lazy<IRepositorioConfiguracionesFajillas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesFajillas>(); });

        IRepositorioCortesTurno RepositorioCortesTurno
        {
            get { return _repostorioCortesTurno.Value; }
        }

        Lazy<IRepositorioCortesTurno> _repostorioCortesTurno = new Lazy<IRepositorioCortesTurno>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCortesTurno>(); });

        IRepositorioVentasRentas RepositorioVentasRentas
        {
            get { return _repostorioVentasRentas.Value; }
        }

        Lazy<IRepositorioVentasRentas> _repostorioVentasRentas = new Lazy<IRepositorioVentasRentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentasRentas>(); });

        IRepositorioPagosComandas RepositorioPagosComandas
        {
            get { return _repostorioPagosComandas.Value; }
        }

        Lazy<IRepositorioPagosComandas> _repostorioPagosComandas = new Lazy<IRepositorioPagosComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosComandas>(); });

        IRepositorioPagosReservaciones RepositorioPagosReservaciones
        {
            get { return _repostorioPagosReservaciones.Value; }
        }

        Lazy<IRepositorioPagosReservaciones> _repostorioPagosReservaciones = new Lazy<IRepositorioPagosReservaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosReservaciones>(); });

        IRepositorioPagosConsumosInternos RepositorioPagosConsumosInternos
        {
            get { return _repostorioPagosConsumosInternos.Value; }
        }

        Lazy<IRepositorioPagosConsumosInternos> _repostorioPagosConsumosInternos = new Lazy<IRepositorioPagosConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosConsumosInternos>(); });

        IRepositorioPagosOcupacionesMesa RepositorioPagosOcupacionesMesa
        {
            get { return _repostorioPagosOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioPagosOcupacionesMesa> _repostorioPagosOcupacionesMesa = new Lazy<IRepositorioPagosOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosOcupacionesMesa>(); });

        IRepositorioOcupacionesMesa RepositorioOcupacionesMesa
        {
            get { return _repostorioOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioOcupacionesMesa> _repostorioOcupacionesMesa = new Lazy<IRepositorioOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOcupacionesMesa>(); });

        IRepositorioComandas RepositorioComandas
        {
            get { return _repostorioComandas.Value; }
        }

        Lazy<IRepositorioComandas> _repostorioComandas = new Lazy<IRepositorioComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioComandas>(); });


        IRepositorioPagosRenta RepositorioPagosRenta
        {
            get { return _repostorioPagosRenta.Value; }
        }

        Lazy<IRepositorioPagosRenta> _repostorioPagosRenta = new Lazy<IRepositorioPagosRenta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosRenta>(); });

        IRepositorioPagosTarjetasPuntos RepositorioPagosTarjetasPuntos
        {
            get { return _repostorioPagosTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioPagosTarjetasPuntos> _repostorioPagosTarjetasPuntos = new Lazy<IRepositorioPagosTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosTarjetasPuntos>(); });

        IRepositorioTarjetasPuntos RepositorioTarjetasPuntos
        {
            get { return _repostorioTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioTarjetasPuntos> _repostorioTarjetasPuntos = new Lazy<IRepositorioTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTarjetasPuntos>(); });

        IRepositorioConsumosInternos RepositorioConsumosInternos
        {
            get { return _repostorioConsumosInternos.Value; }
        }

        Lazy<IRepositorioConsumosInternos> _repostorioConsumosInternos = new Lazy<IRepositorioConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConsumosInternos>(); });

        //List<DtoEstatusTransaccion> CargarCorteCaja(Corte[] cortes)

        public List<DtoEstatusTransaccion> CargarCorteCaja(Corte[] cortes)
        {
            var agenteS = new AgenteWsSincronizacion();

            return agenteS.CargarCorteCaja(cortes);
        }

        public List<DtoEstatusGastos> CargarGastos(CorteGastos[] cortes)
        {
            var agenteS = new AgenteWsSincronizacion();

            return agenteS.CargarGastos(cortes);
        }

        public List<DtoEstatusHabitacion> CatalogoHabitaciones(Negocio.ServiciosExternos.wsSincronizacion.Habitacion[] habitaciones, int idMovimiento)
        {
            var agenteS = new AgenteWsSincronizacion();

            return agenteS.CatalogoHabitaciones(habitaciones, idMovimiento);
        }

        public List<DtoEstatusMoneda> CargarEfectivo(Efectivo[] efectivo)
        {
            var agenteS = new AgenteWsSincronizacion();

            return agenteS.CargarEfectivo(efectivo);
        }

        public void Sincronizar()
        {
            //var ultimoCorteCerrado = ServicioCortesTurno.ObtenerUltimoCorteCerrado();

            var cortes = ServicioCortesTurno.ObtenerCortesNoSincronizados();
            var nombreSucursal = ServicioParametros.ObtenerParametros().NombreSucursal;

#if DEBUG

            var ids = cortes.Select(m => m.Id).ToList();

            cortes = cortes.Where(m => ids.Contains(m.Id)).ToList();

#endif


            foreach (var corte in cortes.OrderBy(m => m.NumeroCorte))
            {
                int indexPedido = 0;
                try
                {
                    decimal propinasPagadas, propinasNetas;

                    if (corte.PropinasPagadas.HasValue && corte.PagoNetoPropinas.HasValue)
                    {
                        propinasPagadas = corte.PropinasPagadas.Value;
                        propinasNetas = corte.PagoNetoPropinas.Value;
                    }

                    else
                    {
                        var propinasPagadasMeseros = ServicioCortesTurno.ObtenerPropinasPagadasMeseros(corte);
                        var propinasPagadasValets = ServicioCortesTurno.ObtenerPropinasPagadasValets(corte);


                        var pagoNetoMeseros = ServicioCortesTurno.ObtenerPagoNetoPropinasMeseros(corte);
                        var pagoNetoValets = ServicioCortesTurno.ObtenerPagoNetoPropinasValets(corte);

                        propinasNetas = propinasPagadasMeseros - (pagoNetoMeseros.TotalPositivo + pagoNetoValets.TotalPositivo);

                        propinasPagadas = propinasPagadasMeseros + propinasPagadasValets;
                    }
                    var datosCorte = new serviciosE.wsSincronizacion.Corte
                    {
                        AnticiposRecibidos = corte.AnticiposReservas,
#warning fix temporal, los no reembolsables deberían tener su propia columna
                        AnticiposValidos = corte.ReservasValidas - (corte.MontosNoReembolsables ?? 0),
                        ComisionTaxis = corte.Taxis,
                        ConsumoInterno = corte.ConsumoRestaurante + corte.ConsumoHabitacion,
                        Cortesias = corte.CortesiasHabitacion + corte.CortesiasRestautante + corte.CortesiasRoomService,
                        HospedajeExtra = corte.HospedajeExtra,
                        PagoNetoPropinas = propinasNetas,//corte.PropinasHabitacion + corte.PropinasRestaurante + corte.PropinasRoomService,
                        PropinasPagadas = propinasPagadas,
                        Paquetes = corte.Paquetes,
                        Descuentos = corte.DescuentosHabitacion + corte.DescuentosRestaurante + corte.DescuentosRoomService,
                        PersonasExtra = corte.PersonasExtra,
                        PropinasHabitacion = corte.PropinasHabitacion + corte.PropinasRestaurante + corte.PropinasRoomService,//corte.PropinasHabitacion,
                        NumeroCorte = new FolioCaja
                        {
                            FechaCierre = corte.FechaCorte.Value,
                            FechaInicio = corte.FechaOperacion ?? corte.FechaInicio,
                            Folio = corte.NumeroCorte.ToString(),
                            Sucursal = nombreSucursal,//configuracionG.NombreSucursal,
                            Turno = corte.ConfiguracionTurno.Orden.ToString()
                        }, 
                    };

                    var pagosE = new List<serviciosE.wsSincronizacion.Pago>();

                    var ventas = RepositorioVentas.ObtenerElementos(m => (m.Activa || m.FechaCancelacion > corte.FechaCorte) && m.Correcta && m.IdCorteTurno == corte.Id).ToList();
                    //var ventas = RepositorioVentas.ObtenerElementos(m => m.Activa && m.Correcta && m.IdCorteTurno == corte.Id).ToList();

                    var detallesIngresosH = new List<serviciosE.wsSincronizacion.IngresoHabitacion>();
                    var detallesPedido = new List<serviciosE.wsSincronizacion.Comanda>();

                    #region ventas

                    foreach (var venta in ventas)
                    {
                        decimal porcentajeIVA = venta.IVAEnCurso;


                        IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
                        Modelo.Entidades.DatosFiscales datosF;
                        bool esMesa = false;

                        switch (venta.ClasificacionVenta)
                        {
                            case Venta.ClasificacionesVenta.Habitacion:
                                pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion, !venta.Activa);//FechaCancelacion);
                                datosF = ServicioRentas.ObtenerDatosFiscalesPorTransaccion(venta.Transaccion);
                                break;
                            case Venta.ClasificacionesVenta.Restaurante:
                                pagos = RepositorioPagosOcupacionesMesa.ObtenerPagosPorTransaccion(venta.Transaccion);

                                if (!(esMesa = pagos.Any()))
                                    pagos = RepositorioPagosConsumosInternos.ObtenerPagosPorTransaccion(venta.Transaccion);

                                datosF = ServicioRestaurantes.ObtenerDatosFiscalesPorTransaccion(venta.Transaccion);
                                break;
                            case Venta.ClasificacionesVenta.RoomService:
                                pagos = RepositorioPagosComandas.ObtenerPagosPorTransaccion(venta.Transaccion);
                                datosF = ServicioRentas.ObtenerDatosFiscalesPorTransaccion(venta.Transaccion);
                                break;
                            case Venta.ClasificacionesVenta.TarjetaPuntos:
                                pagos = RepositorioPagosTarjetasPuntos.ObtenerPagosPorTransaccion(venta.Transaccion);
                                datosF = null;
                                break;
                            //case Venta.ClasificacionesVenta.Taxi:
      

                            //    pagos = Repositoriot.ObtenerPagosPorTransaccion(venta.Transaccion, venta.FechaCancelacion);
                            //    datosF = null;
                            //    break;
                            case Venta.ClasificacionesVenta.Reservacion:
                                pagos = RepositorioPagosReservaciones.ObtenerPagosPorTransaccion(venta.Transaccion, !venta.Activa);//venta.FechaCancelacion);
                                datosF = null;
                                break;
                            default:
                                pagos = new List<IPago>();
                                datosF = null;
                                break;
                        }

                        /*
                            1 Efectivo
                            2 reservaciones//vpoints
                            3 Credito
                            4 Debito
                         */

                        var pagosAdecuados = pagos.ToList();//.Where(m => m.TipoPago == TiposPago.Efectivo || m.TipoPago == TiposPago.TarjetaCredito || m.TipoPago == TiposPago.Reservacion).ToList();

                        if (pagosAdecuados.Count == 0)
                            continue;

                        foreach (var pago in pagosAdecuados)
                        {
                            int idTipoPago;
                            int idTipoTarjeta;

                            switch (pago.TipoPago)
                            {
                                case TiposPago.Efectivo:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Efectivo;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Reservacion:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Reservacion;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.TarjetaCredito:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Tarjeta;
                                    idTipoTarjeta = pago.IdTipoTarjeta.Value;
                                    break;
                                case TiposPago.Consumo:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Consumo;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Cortesia:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Cortesia;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Cupon:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Cupon;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.VPoints:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.VPoints;
                                    idTipoTarjeta = 0;
                                    break;
                                case TiposPago.Transferencia:
                                    idTipoPago = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Paypal;
                                    idTipoTarjeta = 0;
                                    break;
                                default:
                                    idTipoPago = 0;
                                    idTipoTarjeta = 0;
                                    break;
                            }

                            pagosE.Add(new serviciosE.wsSincronizacion.Pago
                            {
                                Total = pago.Valor,
                                Tipo = idTipoPago,
                                TipoTarjeta = idTipoTarjeta
                            });
                        }

                        //Tipo Comanda                           Room Service,  Restaurante

                        //Categoria Comanda                  Alimentos,  Bebidas,  Sex & Spa

                        switch (venta.ClasificacionVenta)
                        {
                            case Venta.ClasificacionesVenta.Habitacion:
                                {
                                    #region detalles de habitación

                                    var ventaRenta = RepositorioPagosRenta.ObtenerVentaConConceptos(venta.Transaccion, venta.Cancelada);

#warning oojo aqui, se le quita un segundo a la fecha porque el filtro valida que los detalles tengan fecha de cancelación mayor que la fecha proporcionada, lo que es bueno cuando la fecha corresponde con el cierre de un turno, pero es malo cuando es la fecha de cancelación de la venta
                                    var detalles = ServicioRentas.ObtenerDetallesPorTransaccion(venta.Transaccion, venta.FechaCancelacion.HasValue ? venta.FechaCancelacion.Value.AddSeconds(-1) : venta.FechaCancelacion);

                                    if (ventaRenta.EsInicial)
                                    {
                                        var detalleH = detalles.FirstOrDefault(m => m.ConceptoPago == DetallePago.ConceptosPago.Habitacion);

                                        var precio = Math.Round(detalleH != null ? (detalleH.ValorSinIVA / detalleH.Cantidad) : ventaRenta.PrecioHotelTmp, 2);
                                        var iva = Math.Round(detalleH != null ? (detalleH.ValorIVA / detalleH.Cantidad) : (ventaRenta.PrecioHotelTmp * porcentajeIVA), 2);
                                        var total = Math.Round(detalleH != null ? (detalleH.ValorConIVA / detalleH.Cantidad) : ventaRenta.PrecioHotelTmp * (1 + porcentajeIVA), 2);

                                        detallesIngresosH.Add(new serviciosE.wsSincronizacion.IngresoHabitacion
                                        {
                                            Cantidad = 1,
                                            Codigo = ventaRenta.ClaveTipoHabitacionTmp,
                                            Total = total//Math.Round(ventaRenta.PrecioHotelTmp * (1 + porcentajeIVA), 2)
                                        });
                                    }
                                    /**foreach (var grupoHE in ventaRenta.Extensiones.Where(m => m.Activa /*&& !m.EsRenovacion* /).GroupBy(m => m.Precio))
                                    {
                                        detallesIngresosH.Add(new serviciosE.wsSincronizacion.IngresoHabitacion
                                        {
                                            Cantidad = grupoHE.Count(),
                                            Codigo = "HE",
                                            Total = Math.Round(grupoHE.Key * (1 + porcentajeIVA), 2) * grupoHE.Count()
                                        });
                                    }

                                    foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => m.Activa).GroupBy(m => m.Precio))
                                    {
                                        detallesIngresosH.Add(new serviciosE.wsSincronizacion.IngresoHabitacion
                                        {
                                            Cantidad = grupoPE.Count(),
                                            Codigo = "PE",
                                            Total = Math.Round(grupoPE.Key * (1 + porcentajeIVA), 2) * grupoPE.Count()
                                        });
                                    }
                                    /*
                                    foreach (var grupoRen in ventaRenta.Extensiones.Where(m => m.Activa && m.EsRenovacion).GroupBy(m => m.Precio))
                                    {
                                        detallesIngresosH.Add(new serviciosE.wsSincronizacion.IngresoHabitacion
                                        {
                                            Cantidad = grupoRen.Count(),
                                            Codigo = ventaRenta.ClaveTipoHabitacionTmp,//"HE",
                                            Total = Math.Round(grupoRen.Key * (1 + porcentajeIVA), 2) * grupoRen.Count()
                                        });
                                    }* /

                                    foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).GroupBy(m => m.IdPaquete))
                                    {
                                        var muestra = grupoPAQ.First();

                                        detallesIngresosH.Add(new serviciosE.wsSincronizacion.IngresoHabitacion
                                        {
                                            Cantidad = grupoPAQ.Count(),
                                            Codigo = muestra.Paquete.Clave,
                                            Total = Math.Round(muestra.Paquete.Precio * (1 + porcentajeIVA), 2) * grupoPAQ.Count()
                                        });
                                    }**/


                                    #endregion
                                }
                                break;
                            case Venta.ClasificacionesVenta.Restaurante:
                                {
                                    #region detalles de restaurante

                                    if (esMesa)
                                    {
                                        var ocupacion = RepositorioOcupacionesMesa.ObtenerOcupacionFinalizadaConDetalles(((PagoOcupacionMesa)pagosAdecuados.First()).IdOcupacionMesa);

                                        var idsArticulos = ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

                                        var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

                                        foreach (var artCom in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante))
                                        {
                                            artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                                        }

                                        foreach (var grupoArticulo in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).GroupBy(m => new { m.IdArticulo, m.PrecioUnidadFinal }))
                                        {
                                            var muestra = grupoArticulo.First();
                                            var cantidad = grupoArticulo.Sum(m => m.Cantidad);

                                            var datosA = new serviciosE.wsSincronizacion.Comanda
                                            {
                                                Tipo = 2,
                                                Total = muestra.PrecioUnidadFinal * cantidad
                                            };

                                            if (muestra.ArticuloTmp.ZctCatLinea.IdLineaBase.HasValue)
                                            {
                                                switch (muestra.ArticuloTmp.ZctCatLinea.LineaBase.Value)
                                                {
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos:
                                                        datosA.Categoria = 1;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas:
                                                        datosA.Categoria = 2;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex:
                                                        datosA.Categoria = 3;
                                                        break;
                                                }
                                            }

                                            detallesPedido.Add(datosA);
                                        }
                                    }
                                    else 
                                    {
                                        var consumoInterno = RepositorioConsumosInternos.ObtenerConsumoInternoEntregadoConDetalles(((PagoConsumoInterno)pagos.First()).IdConsumoInterno);

                                        var idsArticulos = consumoInterno.ArticulosConsumoInterno.Select(m => m.IdArticulo).ToList();

                                        var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                                        foreach (var artCom in consumoInterno.ArticulosConsumoInterno)
                                        {
                                            artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                                        }

                                        foreach (var grupoArticulo in consumoInterno.ArticulosConsumoInterno.GroupBy(m => m.IdArticulo))
                                        {
                                            var muestra = grupoArticulo.First();
                                            var cantidad = grupoArticulo.Sum(m => m.Cantidad);

                                            var datosA = new serviciosE.wsSincronizacion.Comanda
                                            {
                                                Tipo = 2,
                                                Total = muestra.PrecioUnidadFinal * cantidad
                                            };

                                            if (muestra.ArticuloTmp.ZctCatLinea.IdLineaBase.HasValue)
                                            {
                                                switch (muestra.ArticuloTmp.ZctCatLinea.LineaBase.Value)
                                                {
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos:
                                                        datosA.Categoria = 1;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas:
                                                        datosA.Categoria = 2;
                                                        break;
                                                    case Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex:
                                                        datosA.Categoria = 3;
                                                        break;
                                                }
                                            }

                                            detallesPedido.Add(datosA);
                                        }
                                    }

                                    #endregion
                                }
                                break;
                            case Venta.ClasificacionesVenta.RoomService:
                                {
                                    #region detalles de comanda

                                    var comanda = RepositorioComandas.ObtenerComandaPagadaConDetalles(((PagoComanda)pagosAdecuados.First()).IdComanda);

                                    var idsArticulos = comanda.ArticulosComanda.Select(m => m.IdArticulo).ToList();

                                    var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

                                    foreach (var artCom in comanda.ArticulosComanda)
                                    {
                                        artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                                    }

                                    foreach (var grupoArticulo in comanda.ArticulosComanda.GroupBy(m => new { m.IdArticulo, m.PrecioUnidadFinal }))
                                    {
                                        var muestra = grupoArticulo.First();
                                        var cantidad = grupoArticulo.Sum(m => m.Cantidad);

                                        var datosA = new serviciosE.wsSincronizacion.Comanda
                                        {
                                            Tipo = 1,
                                            Total = muestra.PrecioUnidadFinal * cantidad
                                        };

                                        if (muestra.ArticuloTmp.ZctCatLinea.IdLineaBase.HasValue)
                                        {
                                            switch (muestra.ArticuloTmp.ZctCatLinea.LineaBase.Value)
                                            {
                                                case Modelo.Almacen.Entidades.Linea.LineasBase.Alimentos:
                                                    datosA.Categoria = 1;
                                                    break;
                                                case Modelo.Almacen.Entidades.Linea.LineasBase.Bebidas:
                                                    datosA.Categoria = 2;
                                                    break;
                                                case Modelo.Almacen.Entidades.Linea.LineasBase.SpaYSex:
                                                    datosA.Categoria = 3;
                                                    break;
                                            }
                                        }

                                        detallesPedido.Add(datosA);
                                    }

                                    #endregion
                                }
                                break;
                            case Venta.ClasificacionesVenta.TarjetaPuntos:
                                {
                                    #region detalles de tarjeta de puntos

                                    var idTarjeta = ((PagoTarjetaPuntos)pagosAdecuados.First()).IdTarjetaPuntos;

                                    var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Id == idTarjeta);

                                    detallesPedido.Add(new serviciosE.wsSincronizacion.Comanda
                                    { 
#warning a las tarjetas v se les está dando la categoría de sex temporalmene, solamente para que no de error de sincronización
                                        Tipo = 2,
                                        Categoria = 3,
                                        Total = pagosAdecuados.Sum(m=> m.Valor)
                                    });

                                    #endregion
                                }
                                break;
                            default:
                                {
                                }
                                break;
                        }

                        indexPedido++;
                    }

                    var ordenesTaxi = ServicioTaxis.ObtenerOrdenesCobradasPorPeriodo(corte.FechaInicio, corte.FechaCorte);

                    if(ordenesTaxi.Count > 0)
                        pagosE.Add(new serviciosE.wsSincronizacion.Pago
                        {
                            Total = ordenesTaxi.Sum(m=> m.Precio),
                            Tipo = (int)serviciosE.Enums.Rastrilleo.EnumFormasPago.Efectivo,
                            TipoTarjeta = 0
                        });

                    #endregion

                    var gruposC = detallesPedido.GroupBy(m => new { m.Tipo, m.Categoria });
                    datosCorte.Comandas = new serviciosE.wsSincronizacion.Comanda[gruposC.Count()];

                    int i = 0;
                    foreach (var grupo in gruposC)
                    {
                        datosCorte.Comandas[i++] = new serviciosE.wsSincronizacion.Comanda
                        {
                            Categoria = grupo.Key.Categoria,
                            Tipo = grupo.Key.Tipo,
                            Total = grupo.Sum(m => m.Total)// * (1 + configuracionG.Iva_CatPar)
                        };
                    }

                    var gruposH = detallesIngresosH.GroupBy(m => m.Codigo);
                    datosCorte.Habitaciones = new serviciosE.wsSincronizacion.IngresoHabitacion[gruposH.Count()];

                    i = 0;
                    foreach (var grupo in gruposH)
                    {
                        datosCorte.Habitaciones[i++] = new serviciosE.wsSincronizacion.IngresoHabitacion
                        {
                            Codigo = grupo.Key,
                            Cantidad = grupo.Sum(m => m.Cantidad),
                            Total = grupo.Sum(m => m.Total)// * (1 + configuracionG.Iva_CatPar)
                        };
                    }

                    var valoresPropinas = ServicioPropinas.ObtenerTotalPropinasPorTarjeta(corte.FechaInicio, corte.FechaCorte.Value);

                    var pagosFinales = new List<Pago>();

                    foreach (var clave in PAGOS_TARJETAS.Keys) 
                    {
                        if (PAGOS_TARJETAS[clave] == null)
                            pagosFinales.Add(new Pago
                            {
                                Tipo = (int)clave,
                                TipoTarjeta = 0,
                                Total = pagosE.Where(m => m.Tipo == (int)clave && m.TipoTarjeta == 0).Sum(m => m.Total)
                            });
                        else
                            foreach (var tarjeta in PAGOS_TARJETAS[clave])
                            {
                                pagosFinales.Add(new Pago
                                {
                                    Tipo = (int)clave,
                                    TipoTarjeta = tarjeta,
                                    Total = pagosE.Where(m => m.Tipo == (int)clave && m.TipoTarjeta == tarjeta).Sum(m => m.Total) + valoresPropinas[(TiposTarjeta)tarjeta]
                                });
                            }

                    }

                    datosCorte.Pagos = pagosFinales.ToArray();

                    var resultado = CargarCorteCaja(new Corte[] { datosCorte });

                    var itemGeneral = resultado.FirstOrDefault(m => FOLIOS_MAGICOS.Contains(m.Folio.ToUpper()));
                    var itemOK = resultado.FirstOrDefault(m => m.Folio.ToUpper().Contains("OK"));

                    var itemResultado = resultado.FirstOrDefault(m => m.Folio == corte.NumeroCorte.ToString());

                    if (resultado.Count > 0 && itemOK == null)
                    {
                        if (itemGeneral != null)
                        {
                            corte.Sincronizado = false;
                            corte.ErrorUltimoIntento = itemGeneral.Respuesta;
                            corte.EsErrorSubida = false;
                        }
                        else if (itemResultado != null)
                        {
                            corte.Sincronizado = false;
                            corte.ErrorUltimoIntento = itemResultado.Respuesta;
                            corte.EsErrorSubida = false;
                        }
                        else
                        {
                            corte.Sincronizado = false;
                            corte.ErrorUltimoIntento = "DESCONOCIDO";
                            corte.EsErrorSubida = false;
                        }
                    }
                    else
                    {
                        //if (itemResultado.Respuesta.ToUpper().Contains("EXITO") || itemResultado.Respuesta.ToUpper().Contains("ÉXITO"))
                        //{
                        corte.Sincronizado = true;
                        corte.ErrorUltimoIntento = null;
                        corte.EsErrorSubida = false;
                        //}
                        //else
                        //{
                        //    corte.Sincronizado = false;
                        //    corte.ErrorUltimoIntento = itemResultado.Respuesta;
                        //    corte.EsErrorSubida = false;
                        //}
                    }

                    RepositorioCortesTurno.Modificar(corte);
                    RepositorioCortesTurno.GuardarCambios();
                }
                catch (Exception ex)
                {

                    corte.Sincronizado = false;
                    corte.ErrorUltimoIntento = ex.InnerException?.Message ?? ex.Message;
                    corte.EsErrorSubida = true;

                    RepositorioCortesTurno.Modificar(corte);
                    RepositorioCortesTurno.GuardarCambios();
                }
            }
        }

        public void SincronizarGastos()
        {
            //var ultimoCorteCerrado = ServicioCortesTurno.ObtenerUltimoCorteCerrado();

            var ultimoCorte = ServicioCortesTurno.ObtenerUltimoCorte();

            var gastos = RepositorioGastos.ObtenerGastosNoSincronizados(ultimoCorte.Id);// && m.IdCorteTurno == ultimoCorteCerrado);

            var configuracionG = ServicioParametros.ObtenerParametros();


            foreach (var grupoGastos in gastos.GroupBy(m => m.IdCorteTurno).ToList())
            {
                var idEstadoCerrado = (int)CorteTurno.Estados.Cerrado;

                var corte = RepositorioCortesTurno.Obtener(m => m.Id == grupoGastos.Key && m.IdEstado == idEstadoCerrado, m => m.ConfiguracionTurno);

                if (corte == null)
                    continue;

                var gastosSubir = new List<serviciosE.wsSincronizacion.CorteGastos>();

                try
                {
                    foreach (var gasto in grupoGastos.GroupBy(m => new { m.IdConceptoGasto, m.IdClasificacion }))
                    {
                        gastosSubir.Add(new CorteGastos
                        {
                            CentroDeCosto = gasto.First().CentroCostosTmp,
                            Concepto = gasto.First().ConceptoTmp,
                            Total = gasto.Sum(m => m.Valor),
                            Clasificacion = (short)(gasto.Key.IdClasificacion ?? 0),
                            NumeroCorte = new FolioCaja
                            {
                                FechaInicio = corte.FechaOperacion ?? corte.FechaInicio,
                                FechaCierre = corte.FechaCorte.Value,
                                Folio = corte.NumeroCorte.ToString(),
                                Turno = corte.ConfiguracionTurno.Orden.ToString(),
                                Sucursal = configuracionG.NombreSucursal
                            }
                        });
                    }

                    var resultado = CargarGastos(gastosSubir.ToArray());

                    var itemGeneral = resultado.FirstOrDefault(m => FOLIOS_MAGICOS.Contains(m.Folio.ToUpper()));
                    var itemOK = resultado.FirstOrDefault(m => m.Folio.ToUpper().Contains("OK"));

                    //var itemResultado = resultado.FirstOrDefault(m => m.Folio == corte.NumeroCorte.ToString());
                    foreach (var gasto in grupoGastos)
                    {
                        if (resultado.Count > 0 && itemOK == null)
                        {
                            if (itemGeneral != null)
                            {
                                gasto.Sincronizado = false;
                                gasto.ErrorUltimoIntento = itemGeneral.Respuesta;
                                gasto.EsErrorSubida = false;
                            }
                            else
                            {
                                gasto.Sincronizado = false;
                                gasto.ErrorUltimoIntento = "DESCONOCIDO";
                                gasto.EsErrorSubida = false;
                            }
                        }
                        else
                        {
                            //if (itemResultado.Respuesta.ToUpper().Contains("EXITO") || itemResultado.Respuesta.ToUpper().Contains("ÉXITO"))
                            //{
                            gasto.Sincronizado = true;
                            gasto.ErrorUltimoIntento = null;
                            gasto.EsErrorSubida = false;
                            //}
                            //else
                            //{
                            //    corte.Sincronizado = false;
                            //    corte.ErrorUltimoIntento = itemResultado.Respuesta;
                            //    corte.EsErrorSubida = false;
                            //}
                        }

                        RepositorioGastos.Modificar(gasto);
                    }
                    RepositorioGastos.GuardarCambios();
                }
                catch (Exception ex)
                {
                    foreach (var gasto in grupoGastos)
                    {
                        gasto.Sincronizado = false;
                        gasto.ErrorUltimoIntento = ex.Message;
                        gasto.EsErrorSubida = true;

                        RepositorioGastos.Modificar(gasto);
                    }

                    RepositorioGastos.GuardarCambios();
                }
            }
        }

        public void SincronizarEfectivo()
        {
            //var ultimoCorteCerrado = ServicioCortesTurno.ObtenerUltimoCorteCerrado();

            var fajillas = RepositorioFajillas.ObtenerFajillasYSobranteCargadosNoSincronizados();

            var configuracionFajillas = RepositorioConfiguracionesFajillas.ObtenerConfiguracionFajillasCargada();

            //var ultimoCorte = ServicioCortesTurno.ObtenerUltimoCorte();

            var configuracionG = ServicioParametros.ObtenerParametros();


            foreach (var grupoFajillas in fajillas.GroupBy(m => m.IdCorteTurno).ToList())
            {
                var idEstadoCerrado = (int)CorteTurno.Estados.Cerrado;

                var corte = RepositorioCortesTurno.Obtener(m => m.Id == grupoFajillas.Key && m.IdEstado == idEstadoCerrado, m => m.ConfiguracionTurno);

                if (corte == null)
                    continue;

                var montos = grupoFajillas.SelectMany(m => m.MontosFajilla).Where(m => m.Activa).ToList();

                var efectivoSubir = new List<serviciosE.wsSincronizacion.Efectivo>();

                try
                {
                    var numeroCorte = new serviciosE.wsSincronizacion.FolioCaja
                    {
                        FechaCierre = corte.FechaCorte.Value,
                        FechaInicio = corte.FechaOperacion ?? corte.FechaInicio,
                        Folio = corte.NumeroCorte.ToString(),
                        Sucursal = configuracionG.NombreSucursal,
                        Turno = corte.ConfiguracionTurno.Orden.ToString()
                    };

                    foreach (var moneda in configuracionFajillas.MontosConfiguracionFajilla)//.GroupBy(m => new { m.IdMonedaExtranjera, m.Monto }))
                    {
                        efectivoSubir.Add(new serviciosE.wsSincronizacion.Efectivo
                        {
                            Cantidad = montos.Where(m=> m.IdMontoConfiguracionFajilla == moneda.Id).Sum(m=>m.Cantidad),
                            Denominacion = moneda.Monto,
                            Moneda = moneda.IdMonedaExtranjera.HasValue ? moneda.MonedaExtranjera.Abreviatura : "MXN",
                            TipoCambio = moneda.IdMonedaExtranjera.HasValue ? moneda.MonedaExtranjera.ValorCambio : 1,
                            NumeroCorte = numeroCorte
                        });
                    }

                    var resultado = CargarEfectivo(efectivoSubir.ToArray());

                    var itemGeneral = resultado.FirstOrDefault(m => FOLIOS_MAGICOS.Contains(m.Moneda.ToUpper()));
                    var itemOK = resultado.FirstOrDefault(m => m.Moneda.ToUpper().Contains("OK"));

                    //var itemResultado = resultado.FirstOrDefault(m => m.Folio == corte.NumeroCorte.ToString());
                    foreach (var gasto in grupoFajillas)
                    {
                        if (resultado.Count > 0 && itemOK == null)
                        {
                            if (itemGeneral != null)
                            {
                                gasto.Sincronizada = false;
                                gasto.ErrorUltimoIntento = itemGeneral.Respuesta;
                                gasto.EsErrorSubida = false;
                            }
                            else
                            {
                                gasto.Sincronizada = false;
                                gasto.ErrorUltimoIntento = "DESCONOCIDO";
                                gasto.EsErrorSubida = false;
                            }
                        }
                        else
                        {
                            //if (itemResultado.Respuesta.ToUpper().Contains("EXITO") || itemResultado.Respuesta.ToUpper().Contains("ÉXITO"))
                            //{
                            gasto.Sincronizada = true;
                            gasto.ErrorUltimoIntento = null;
                            gasto.EsErrorSubida = false;
                            //}
                            //else
                            //{
                            //    corte.Sincronizado = false;
                            //    corte.ErrorUltimoIntento = itemResultado.Respuesta;
                            //    corte.EsErrorSubida = false;
                            //}
                        }

                        RepositorioFajillas.Modificar(gasto);
                    }
                    RepositorioFajillas.GuardarCambios();
                }
                catch (Exception ex)
                {
                    foreach (var gasto in grupoFajillas)
                    {
                        gasto.Sincronizada = false;
                        gasto.ErrorUltimoIntento = ex.Message;
                        gasto.EsErrorSubida = true;

                        RepositorioFajillas.Modificar(gasto);
                    }

                    RepositorioFajillas.GuardarCambios();
                }
            }
        }

        public void SincronizarTiposHabitacion()
        {
            //var ultimoCorteCerrado = ServicioCortesTurno.ObtenerUltimoCorteCerrado();

            //var ultimoCorte = ServicioCortesTurno.ObtenerUltimoCorte();

            var tipos = RepositorioTiposHabitacionSincronizaciones.ObtenerElementos(m => !m.Sincronizado, m=> m.PreciosTipoHabitacionSincronizacion).OrderBy(m=> m.Fecha).ToList();// && m.IdCorteTurno == ultimoCorteCerrado);
            var configuracionG = ServicioParametros.ObtenerParametros();

            if (tipos.Count == 0)
                return;

            foreach (var tipo in tipos)
            {
                //var gastosSubir = new List<serviciosE.wsSincronizacion.CorteGastos>();

                try
                {
                    var configMotel = tipo.PreciosTipoHabitacionSincronizacion.FirstOrDefault(m => m.IdTarifa == (int)ConfiguracionTarifa.Tarifas.Motel);
                    var configHotel = tipo.PreciosTipoHabitacionSincronizacion.FirstOrDefault(m => m.IdTarifa == (int)ConfiguracionTarifa.Tarifas.Hotel);
                    
                    var habitacion = new serviciosE.wsSincronizacion.Habitacion
                    {
                        Codigo = tipo.Clave,
                        Nombre = tipo.Nombre,
                        Sucursal = configuracionG.NombreSucursal,
                        Precio = configMotel != null ? decimal.Round(configMotel.Precio * (1 + configuracionG.Iva_CatPar), 2) : 0,
                        PrecioHotel = configHotel != null ? decimal.Round(configHotel.Precio * (1 + configuracionG.Iva_CatPar), 2) : 0,
                    };

                    var resultado = CatalogoHabitaciones(new serviciosE.wsSincronizacion.Habitacion[] { habitacion }, tipo.IdMovimiento);

                    var itemGeneral = resultado.FirstOrDefault(m => FOLIOS_MAGICOS.Contains(m.Clave.ToUpper()) || m.Clave == habitacion.Codigo);
                    var itemOK = resultado.FirstOrDefault(m => m.Clave.ToUpper().Contains("CORRECTAMENTE") || m.Respuesta.ToUpper().Contains("CORRECTAMENTE"));

                    //var itemResultado = resultado.FirstOrDefault(m => m.Folio == corte.NumeroCorte.ToString());

                    if (resultado.Count > 0 && itemOK == null)
                    {
                        if (itemGeneral != null)
                        {
                            tipo.Sincronizado = false;
                            tipo.ErrorUltimoIntento = itemGeneral.Respuesta;
                            tipo.EsErrorSubida = false;
                        }
                        else
                        {
                            tipo.Sincronizado = false;
                            tipo.ErrorUltimoIntento = "DESCONOCIDO";
                            tipo.EsErrorSubida = false;
                        }
                    }
                    else
                    {
                        //if (itemResultado.Respuesta.ToUpper().Contains("EXITO") || itemResultado.Respuesta.ToUpper().Contains("ÉXITO"))
                        //{
                        tipo.Sincronizado = true;
                        tipo.ErrorUltimoIntento = null;
                        tipo.EsErrorSubida = false;
                        //}
                        //else
                        //{
                        //    corte.Sincronizado = false;
                        //    corte.ErrorUltimoIntento = itemResultado.Respuesta;
                        //    corte.EsErrorSubida = false;
                        //}
                    }

                    RepositorioTiposHabitacionSincronizaciones.Modificar(tipo);

                    RepositorioTiposHabitacionSincronizaciones.GuardarCambios();
                }
                catch (Exception ex)
                {
                    tipo.Sincronizado = false;
                    tipo.ErrorUltimoIntento = ex.Message;
                    tipo.EsErrorSubida = true;

                    RepositorioTiposHabitacionSincronizaciones.Modificar(tipo);


                    RepositorioTiposHabitacionSincronizaciones.GuardarCambios();
                }
            }
        }

        public bool VerificarExisteSucursal(string identificadorSucursal)
        {
            var agenteS = new AgenteWsSincronizacion();

            return agenteS.VerificarExisteSucursal(identificadorSucursal);
        }
    }
}