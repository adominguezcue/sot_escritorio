﻿using Negocio.Compartido.Sincronizacion;
using System;
namespace Negocio.Sincronizacion
{
    public interface IServicioSincronizacion : IServicioSincronizacionCompartido
    {
        void Sincronizar();
        void SincronizarGastos();
        void SincronizarEfectivo();
        void SincronizarTiposHabitacion();
    }
}
