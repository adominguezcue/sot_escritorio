﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.ConceptosGastos;
using Negocio.ConceptosSistema;
using Negocio.CortesTurno;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Gastos
{
    public class ServicioGastos : IServicioGastosInterno
    {
        IRepositorioGastos RepositorioGastos
        {
            get { return _repostorioGastos.Value; }
        }

        Lazy<IRepositorioGastos> _repostorioGastos = new Lazy<IRepositorioGastos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioGastos>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioConceptosSistema ServicioConceptosSistema
        {
            get { return _servicioConceptosSistema.Value; }
        }

        Lazy<IServicioConceptosSistema> _servicioConceptosSistema = new Lazy<IServicioConceptosSistema>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosSistema>(); });

        IServicioConceptosGastosInterno ServicioConceptosGastos
        {
            get { return _servicioConceptosGastos.Value; }
        }

        Lazy<IServicioConceptosGastosInterno> _servicioConceptosGastos = new Lazy<IServicioConceptosGastosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosGastosInterno>(); });

        IServicioCortesTurnoInterno ServicioCortesTurno
        {
            get { return _servicioCortesTurno.Value; }
        }

        Lazy<IServicioCortesTurnoInterno> _servicioCortesTurno = new Lazy<IServicioCortesTurnoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCortesTurnoInterno>(); });


        public void CrearGasto(Gasto gasto, bool corteEnRevision, DtoUsuario usuario)
        {
            if (corteEnRevision)
                usuario = ServicioPermisos.ObtenerUsuarioPorCredencial();

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearGastos = true });

            if (gasto == null)
                throw new SOTException(Recursos.Gastos.gasto_nulo_excepcion);

            if (gasto.Valor <= 0)
                throw new SOTException(Recursos.Gastos.valor_gasto_invalido_excepcion);

            var concepto = ServicioConceptosGastos.ObtenerConceptoConRestante(gasto.IdConceptoGasto, null);

            if (concepto == null)
                throw new SOTException(Recursos.Gastos.concepto_seleccionado_invalido_excepcion);

            if (concepto.EnmascaraCompras)
                throw new SOTException(Recursos.Gastos.concepto_enmascarable_excepcion, concepto.Concepto);

            //if(gasto.Valor > concepto.PresupuestoRestanteTmp)
            //    throw new SOTException(Recursos.)

            CorteTurno corte;

            if (corteEnRevision)
            {
                corte = ServicioCortesTurno.ObtenerCorteEnRevision(gasto.IdCorteTurno);

                if (corte == null)
                    throw new SOTException(Recursos.Gastos.gasto_no_corte_revision_excepcion);
            }
            else
            {
                corte = ServicioCortesTurno.ObtenerUltimoCorte();
            }

            var fechaActual = DateTime.Now;

            gasto.Activo = true;
            gasto.FechaCreacion = fechaActual;
            gasto.FechaModificacion = fechaActual;
            gasto.IdUsuarioCreo = usuario.Id;
            gasto.IdUsuarioModifico = usuario.Id;
            gasto.ConceptoInstantaneo = concepto.Concepto;
            gasto.IdCorteTurno = corte.Id;

            RepositorioGastos.Agregar(gasto);
            RepositorioGastos.GuardarCambios();
        }

        public void ModificarGasto(int idGasto, decimal nuevoValor, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarGastos = true });

            var gasto = RepositorioGastos.Obtener(m => m.Activo && m.Id == idGasto);

            if (gasto == null)
                throw new SOTException(Recursos.Gastos.gasto_nulo_excepcion);

            if (nuevoValor <= 0)
                throw new SOTException(Recursos.Gastos.valor_gasto_invalido_excepcion);

            var corte = ServicioCortesTurno.ObtenerCorteEnRevision(gasto.IdCorteTurno);

            if (corte == null)
                throw new SOTException(Recursos.Gastos.gasto_no_corte_revision_excepcion);

            var concepto = ServicioConceptosGastos.ObtenerConceptoConRestante(gasto.IdConceptoGasto, idGasto);

            if (concepto == null)
                throw new SOTException(Recursos.Gastos.concepto_seleccionado_invalido_excepcion);

            if (concepto.EnmascaraCompras)
                throw new SOTException(Recursos.Gastos.concepto_enmascarable_excepcion, concepto.Concepto);

            //if(gasto.Valor > concepto.PresupuestoRestanteTmp)
            //    throw new SOTException(Recursos.)

            var fechaActual = DateTime.Now;

            gasto.FechaModificacion = fechaActual;
            gasto.IdUsuarioModifico = usuario.Id;
            gasto.Valor = nuevoValor;

            RepositorioGastos.Modificar(gasto);
            RepositorioGastos.GuardarCambios();
        }


        //public List<Gasto> ObtenerGastosSinTurno(DtoUsuario usuario)
        //{
        //    if (usuario == null)
        //        throw new SOTException(Recursos.Usuarios.usuario_nulo_excepcion);

        //    ServicioPermisos.ValidarPermisos();

        //    return RepositorioGastos.ObtenerElementos(m => m.Activo && !m.IdCorteTurno.HasValue).ToList();
        //}


        //public void AsignarCorteAGasto(int idGasto, DtoUsuario usuario, int idCorteTurno)
        //{
        //    var gasto = RepositorioGastos.Obtener(m => m.Activo && m.Id == idGasto);

        //    if (gasto == null)
        //        throw new SOTException(Recursos.Gastos.gasto_nulo_excepcion);


        //    gasto.IdCorteTurno = idCorteTurno;
        //    RepositorioGastos.Modificar(gasto);

        //    RepositorioGastos.GuardarCambios();
        //}


        public List<Gasto> ObtenerGastosPorTurno(int idCorteTurno, bool soloSinClasificacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarGastos = true });

            if(soloSinClasificacion)
                return RepositorioGastos.ObtenerElementos(m => m.Activo && m.IdCorteTurno == idCorteTurno && !m.IdClasificacion.HasValue).ToList();

            return RepositorioGastos.ObtenerElementos(m => m.Activo && m.IdCorteTurno == idCorteTurno).ToList();
        }


        public void CancelarGasto(int idGasto, DtoCredencial credencial)
        {
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(credencial);

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarGastos = true });

            var gasto = RepositorioGastos.Obtener(m => m.Id == idGasto && m.Activo);

            if (gasto == null)
                throw new SOTException(Recursos.Gastos.gasto_nulo_o_eliminado_excepcion);

            var corteActual = ServicioCortesTurno.ObtenerUltimoCorte();

            if (corteActual.Id != gasto.IdCorteTurno)
            {
                var corteRevision = ServicioCortesTurno.ObtenerCorteEnRevision(gasto.IdCorteTurno);

                if (corteRevision == null)
                    throw new SOTException(Recursos.Gastos.gasto_no_corte_revision_excepcion);
            }

            var concepto = ServicioConceptosGastos.ObtenerConceptoConRestante(gasto.IdConceptoGasto, idGasto);

            if (concepto.EnmascaraCompras)
                throw new SOTException(Recursos.Gastos.concepto_enmascarable_excepcion, concepto.Concepto);

            var fechaActual = DateTime.Now;

            gasto.Activo = false;
            gasto.FechaEliminacion = fechaActual;
            gasto.FechaModificacion = fechaActual;
            gasto.IdUsuarioElimino = usuario.Id;
            gasto.IdUsuarioModifico = usuario.Id;

            RepositorioGastos.Modificar(gasto);
            RepositorioGastos.GuardarCambios();
        }

        #region metodos internal

        void IServicioGastosInterno.CrearGastoAutomatico(int idCentroCostos, decimal valor, int idUsuario)
        {
            if (valor <= 0)
                throw new SOTException(Recursos.Gastos.valor_gasto_invalido_excepcion);

            var concepto = ServicioConceptosGastos.ObtenerConceptoEnmascarador(idCentroCostos);

            if (concepto == null)
                return;

            //if(gasto.Valor > concepto.PresupuestoRestanteTmp)
            //    throw new SOTException(Recursos.)

            var corte = ServicioCortesTurno.ObtenerUltimoCorte();

            var fechaActual = DateTime.Now;

            var gasto = new Gasto
            {
                Activo = true,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = idUsuario,
                IdUsuarioModifico = idUsuario,
                ConceptoInstantaneo = concepto.Concepto,
                IdCorteTurno = corte.Id,
                IdConceptoGasto = concepto.Id,
                Valor = valor
            };

            RepositorioGastos.Agregar(gasto);
            RepositorioGastos.GuardarCambios();
        }

        void IServicioGastosInterno.CrearGastoAutomaticoV2(int idCuentaPago, int idConceptoGastos, decimal valor, int idUsuario, ClasificacionesGastos? clasificacion, DateTime fechaFiltroTurno/*int? idTurno*/)
        {
            if (valor == 0)
                throw new SOTException(Recursos.Gastos.valor_gasto_cero_excepcion);

            if (!clasificacion.HasValue && valor < 0)
                throw new SOTException(Recursos.Gastos.valor_gasto_caja_invalido_excepcion);


            var concepto = ServicioConceptosGastos.ObtenerConcepto(idConceptoGastos);

            if (concepto == null)
                return;

            //if(gasto.Valor > concepto.PresupuestoRestanteTmp)
            //    throw new SOTException(Recursos.)

            CorteTurno corte;

            //if (idTurno.HasValue)
            //{
                corte = ServicioCortesTurno.ObtenerCorteIncluyeFecha(fechaFiltroTurno);

                //if (corte == null)
                //    corte = ServicioCortesTurno.ObtenerUltimoCorte();

                if (corte == null || (corte.Estado != CorteTurno.Estados.EnRevision && corte.Estado != CorteTurno.Estados.Abierto))
                    throw new SOTException(Recursos.CortesTurno.corte_no_en_revision_o_actual_excepcion);
            //}
            //else
            //    corte = ServicioCortesTurno.ObtenerUltimoCorte();

            if (corte == null)
                throw new SOTException(Recursos.CortesTurno.corte_actual_nulo_excepcion);

            var fechaActual = DateTime.Now;

            var gasto = new Gasto
            {
                Activo = true,
                IdCuentaPago = idCuentaPago,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = idUsuario,
                IdUsuarioModifico = idUsuario,
                ConceptoInstantaneo = concepto.Concepto,
                IdCorteTurno = corte.Id,
                IdConceptoGasto = concepto.Id,
                Valor = valor,
                IdClasificacion = (int?)clasificacion
            };

            RepositorioGastos.Agregar(gasto);
            RepositorioGastos.GuardarCambios();
        }

        void IServicioGastosInterno.EliminarGastos(int idCuentaPago, int idUsuario)
        {
            var gastos = RepositorioGastos.ObtenerElementos(m => m.IdCuentaPago == idCuentaPago && m.Activo).ToList();

            if (gastos.Count > 0)
            {
                var idsCortes = gastos.Select(m => m.IdCorteTurno).Distinct().ToList();

                CorteTurno corte;
                foreach (var idCorte in idsCortes)
                {
                    corte = ServicioCortesTurno.ObtenerCorteEnRevision(idCorte);

                    if (corte == null)
                        corte = ServicioCortesTurno.ObtenerUltimoCorte();

                    if (corte == null || corte.Id != idCorte)
                        throw new SOTException(Recursos.CortesTurno.corte_no_en_revision_o_actual_excepcion);

                }

                var fechaActual = DateTime.Now;

                foreach (var gasto in gastos)
                {

                    gasto.Activo = false;
                    gasto.FechaEliminacion = fechaActual;
                    gasto.FechaModificacion = fechaActual;
                    gasto.IdUsuarioElimino = idUsuario;
                    gasto.IdUsuarioModifico = idUsuario;

                    RepositorioGastos.Modificar(gasto);
                }
                RepositorioGastos.GuardarCambios();
            }
        }

        #endregion
    }
}
