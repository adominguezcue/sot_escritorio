﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Gastos
{
    public interface IServicioGastos
    {
        /// <summary>
        /// Permite registrar un nuevo gasto en la base de datos
        /// </summary>
        /// <param name="gasto"></param>
        /// <param name="usuario"></param>
        void CrearGasto(Gasto gasto, bool corteEnRevision, DtoUsuario usuario);
        /// <summary>
        /// Permite actualizar el valor de un gasto
        /// </summary>
        /// <param name="idGasto"></param>
        /// <param name="nuevoValor"></param>
        /// <param name="usuario"></param>
        void ModificarGasto(int idGasto, decimal nuevoValor, DtoCredencial credencial);
        /// <summary>
        /// Retorna los gastos activos del corte solicitado, siempre y cuando el usuario tenga los
        /// permisos necesarios para verlos
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Gasto> ObtenerGastosPorTurno(int idCorteTurno, bool soloSinClasificacion, DtoUsuario usuario);
        /// <summary>
        /// Permite cancelar un gasto del sistema
        /// </summary>
        /// <param name="idGasto"></param>
        /// <param name="huellaDigital"></param>
        void CancelarGasto(int idGasto, DtoCredencial credencial);
    }
}