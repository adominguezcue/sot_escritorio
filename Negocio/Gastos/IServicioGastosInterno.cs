﻿using Dominio.Nucleo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Gastos
{
    internal interface IServicioGastosInterno : IServicioGastos
    {
        void CrearGastoAutomatico(int idCentroCostos, decimal valor, int idUsuario);

        void CrearGastoAutomaticoV2(int idCuentaPago, int idConceptoGastos, decimal valor, int idUsuario, ClasificacionesGastos? clasificacion, DateTime fechaFiltroTurno/*int? idTurno*/);
        /// <summary>
        /// Elimina los gastos vinculados a la cuenta de pago proporcionada.
        /// </summary>
        /// <param name="idCuentaPago"></param>
        /// <param name="idUsuario"></param>
        void EliminarGastos(int idCuentaPago, int idUsuario);
    }
}
