﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Repositorios;
using Modelo.Entidades.Parciales;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using Transversal.Dependencias;

namespace Negocio.ReporteInventario
{
    public class ServicioReporteInventario : IServicioReporteInventario
    {
        private IRepositorioReporteInventario RepositorioReporteInventario
        {
            get { return _repositorioreporteinventario.Value; }
        }


        private Lazy<IRepositorioReporteInventario> _repositorioreporteinventario = new Lazy<IRepositorioReporteInventario>(() => {
            return FabricaDependencias.Instancia.Resolver<IRepositorioReporteInventario>();
        });
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        List<CatalogoAlmacen> IServicioReporteInventario.ObtieneCatalogoAlmacen(DtoUsuario usuario, string depto, string linea)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarReporteInventario = true });
            return RepositorioReporteInventario.ObtieneCatalogoAlmacen( depto, linea);
        }

        List<CatalogoLinea> IServicioReporteInventario.ObtienecatalogoLinea(DtoUsuario usuario, string deptos)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarReporteInventario = true });
            return RepositorioReporteInventario.ObtienecatalogoLinea(deptos);
        }

        List<CatalogoDepartamento> IServicioReporteInventario.ObtieneCatalogoDepto(DtoUsuario usuario, string deptos)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarReporteInventario = true });
            return RepositorioReporteInventario.ObtieneCatalogoDepto(deptos);
        }

        List<ArticulosInventarioActual> IServicioReporteInventario.ObtieneArticulosInvActual(DtoUsuario usuario, string almacen, string articulos, string linea, string depto)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarReporteInventario = true });
            return RepositorioReporteInventario.ObtieneArticulosInvActual(almacen, articulos, linea, depto);
        }
    }
}
