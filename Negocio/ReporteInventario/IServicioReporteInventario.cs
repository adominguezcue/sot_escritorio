﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades.Parciales;
using Modelo.Seguridad.Dtos;

namespace Negocio.ReporteInventario
{
    public interface IServicioReporteInventario
    {

        List<CatalogoAlmacen> ObtieneCatalogoAlmacen (DtoUsuario usuario, string depto, string linea);

        List<CatalogoLinea> ObtienecatalogoLinea(DtoUsuario usuario, string deptos);

        List<CatalogoDepartamento> ObtieneCatalogoDepto(DtoUsuario usuario,string deptos);

        List<ArticulosInventarioActual> ObtieneArticulosInvActual(DtoUsuario usuario, string almacen, string articulos, string linea, string depto);
    }
}
