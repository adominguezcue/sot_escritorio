﻿using Negocio.Compartido.Articulos;
using Negocio.ConsumosInternos;
using Negocio.Restaurantes;
using Negocio.RoomServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Articulos
{
    public class ServicioDependenciasArticulos : IServicioDependenciasArticulosCompartido
    {
        IServicioRoomServicesInterno ServicioRoomServices
        {
            get { return _servicioRoomServices.Value; }
        }

        Lazy<IServicioRoomServicesInterno> _servicioRoomServices = new Lazy<IServicioRoomServicesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRoomServicesInterno>(); });

        IServicioRestaurantesInterno ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantesInterno> _servicioRestaurantes = new Lazy<IServicioRestaurantesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantesInterno>(); });

        IServicioConsumosInternosInterno ServicioConsumosInternos
        {
            get { return _servicioConsumosInternos.Value; }
        }

        Lazy<IServicioConsumosInternosInterno> _servicioConsumosInternos = new Lazy<IServicioConsumosInternosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternosInterno>(); });




        public bool VerificarNoExistenDependenciasArticulo(string codigoArticulo)
        {
            return ServicioRoomServices.VerificarNoExistenDependenciasArticulo(codigoArticulo) &&
                ServicioRestaurantes.VerificarNoExistenDependenciasArticulo(codigoArticulo) &&
                ServicioConsumosInternos.VerificarNoExistenDependenciasArticulo(codigoArticulo);
        }
    }
}
