﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ConceptosGastos
{
    internal interface IServicioConceptosGastosInterno : IServicioConceptosGastos
    {
        /// <summary>
        /// Retorna true en caso de que no existan conceptos de gastos activos vinculados al centro
        /// de costos con el id proporcionado
        /// </summary>
        /// <param name="idCentroCostos"></param>
        /// <returns></returns>
        bool VerificarNoPoseeConceptos(int idCentroCostos);
        /// <summary>
        /// Retorna el concepto de gasto activo que posea el Id proporcionado. Incluye información
        /// del presupuesto restante
        /// </summary>
        /// <param name="idConceptoGasto"></param>
        /// <returns></returns>
        ConceptoGasto ObtenerConceptoConRestante(int idConceptoGasto, int? idGasto);

        ConceptoGasto ObtenerConceptoEnmascarador(int idCentroCostos);
        ConceptoGasto ObtenerConcepto(int idConcepto);

        bool VerificarPoseeEnmascarador(int idCentroCostos);
    }
}
