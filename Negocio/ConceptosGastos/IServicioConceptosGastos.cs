﻿using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Compartido.ConceptosGastos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.ConceptosGastos
{
    public interface IServicioConceptosGastos : IServicioConceptosGastosCompartido
    {
        /// <summary>
        /// Retorna los conceptos de gastos del sistema
        /// </summary>
        /// <param name="UsuarioActual"></param>
        /// <returns></returns>
        List<ConceptoGasto> ObtenerConceptosActivos(DtoUsuario UsuarioActual);
        /// <summary>
        /// Retorna los conceptos de gastos del sistema que afectan a caja
        /// </summary>
        /// <param name="UsuarioActual"></param>
        /// <returns></returns>
        List<ConceptoGasto> ObtenerConceptosPagablesEnCaja(DtoUsuario UsuarioActual);
        /// <summary>
        /// Permite registrar un nuevo concepto de gastos
        /// </summary>
        /// <param name="concepto"></param>
        /// <param name="usuario"></param>
        void CrearConceptoGasto(ConceptoGasto concepto, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar un concepto de gastos
        /// </summary>
        /// <param name="concepto"></param>
        /// <param name="usuario"></param>
        void ModificarConcepto(ConceptoGasto concepto, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar un concepto de gastos
        /// </summary>
        /// <param name="concepto"></param>
        /// <param name="usuario"></param>
        void EliminarConcepto(int idConcepto, DtoUsuario usuario);
    }
}
