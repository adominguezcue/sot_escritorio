﻿using Dominio.Nucleo.Entidades;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.ArticulosConceptosGasto;
using Negocio.CentrosCostos;
using Negocio.Gastos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.ConceptosGastos
{
    public class ServicioConceptosGastos : IServicioConceptosGastosInterno
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioCentrosCostosInterno ServicioCentrosCostos
        {
            get { return _servicioCentrosCostos.Value; }
        }

        Lazy<IServicioCentrosCostosInterno> _servicioCentrosCostos = new Lazy<IServicioCentrosCostosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCentrosCostosInterno>(); });

        IServicioGastosInterno ServicioGastos
        {
            get { return _servicioGastos.Value; }
        }

        Lazy<IServicioGastosInterno> _servicioGastos = new Lazy<IServicioGastosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioGastosInterno>(); });

        IServicioArticulosConceptosGastoInterno ServicioArticulosConceptosGasto
        {
            get { return _servicioArticulosConceptosGasto.Value; }
        }

        Lazy<IServicioArticulosConceptosGastoInterno> _servicioArticulosConceptosGasto = new Lazy<IServicioArticulosConceptosGastoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulosConceptosGastoInterno>(); });


        IRepositorioConceptosGastos RepositorioConceptosGastos
        {
            get { return _repostorioConceptosGastos.Value; }
        }

        Lazy<IRepositorioConceptosGastos> _repostorioConceptosGastos = new Lazy<IRepositorioConceptosGastos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConceptosGastos>(); });

        public List<ConceptoGasto> ObtenerConceptosPagablesEnCaja(DtoUsuario UsuarioActual)
        {
            return RepositorioConceptosGastos.ObtenerConceptosPagablesEnCaja();
        }

        public List<ConceptoGasto> ObtenerConceptosActivos(DtoUsuario UsuarioActual)
        {
            return RepositorioConceptosGastos.ObtenerConceptosActivos();
        }

        public void CrearConceptoGasto(ConceptoGasto concepto, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearConceptosGastos = true });

            if (concepto == null)
                throw new SOTException(Recursos.ConceptosGastos.concepto_nulo_excepcion);

            if (RepositorioConceptosGastos.Alguno(m => m.IdCentroCostos == concepto.IdCentroCostos && m.Activo && m.Concepto.Trim().ToUpper().Equals(concepto.Concepto.Trim().ToUpper())))
                throw new SOTException(Recursos.ConceptosGastos.concepto_duplicado_excepcion, concepto.Concepto);

            if (concepto.EnmascaraCompras && RepositorioConceptosGastos.Alguno(m => m.Activo && m.EnmascaraCompras && m.IdCentroCostos == concepto.IdCentroCostos))
                throw new SOTException(Recursos.ConceptosGastos.concepto_enmascarador_excepcion);

            ValidarCamposObligatorios(concepto);

            //var presupuestoRestante = ServicioCentrosCostos.ObtenerPresupuestoRestante(concepto.IdCentroCostos, null);

            //if (concepto.TopePresupuestal > presupuestoRestante)
            //    throw new SOTException(Recursos.ConceptosGastos.tope_excede_presupuesto_centro_excepcion, presupuestoRestante.ToString("C"));

            var fechaActual = DateTime.Now;

            concepto.Activo = true;
            concepto.FechaCreacion = fechaActual;
            concepto.FechaModificacion = fechaActual;
            concepto.IdUsuarioCreo = usuario.Id;
            concepto.IdUsuarioModifico = usuario.Id;

            RepositorioConceptosGastos.Agregar(concepto);
            RepositorioConceptosGastos.GuardarCambios();
        }

        public void ModificarConcepto(ConceptoGasto concepto, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarConceptosGastos = true });

            if (concepto == null)
                throw new SOTException(Recursos.ConceptosGastos.concepto_nulo_excepcion);

            if (RepositorioConceptosGastos.Alguno(m => m.Id != concepto.Id && m.IdCentroCostos == concepto.IdCentroCostos && m.Activo && m.Concepto.Trim().ToUpper().Equals(concepto.Concepto.Trim().ToUpper())))
                throw new SOTException(Recursos.ConceptosGastos.concepto_duplicado_excepcion, concepto.Concepto);

            if (concepto.EnmascaraCompras && RepositorioConceptosGastos.Alguno(m => m.Activo && m.EnmascaraCompras && m.IdCentroCostos == concepto.IdCentroCostos && m.Id != concepto.Id))
                throw new SOTException(Recursos.ConceptosGastos.concepto_enmascarador_excepcion);

            if (ServicioCentrosCostos.VerificarPoseeVinculaciones(concepto.IdCentroCostos))
            {
                if (((IServicioConceptosGastosInterno)this).ObtenerConceptoEnmascarador(concepto.IdCentroCostos).Id == concepto.Id && !concepto.EnmascaraCompras)
                    throw new SOTException(Recursos.ConceptosGastos.enmascarador_requerido_excepcion);
            }

            ValidarCamposObligatorios(concepto);

            //var presupuestoRestante = ServicioCentrosCostos.ObtenerPresupuestoRestante(concepto.IdCentroCostos, concepto.Id);

            //if (concepto.TopePresupuestal > presupuestoRestante)
            //    throw new SOTException(Recursos.ConceptosGastos.tope_excede_presupuesto_centro_excepcion, presupuestoRestante.ToString("C"));

            var fechaActual = DateTime.Now;

            concepto.Activo = true;
            concepto.FechaModificacion = fechaActual;
            concepto.IdUsuarioModifico = usuario.Id;

            RepositorioConceptosGastos.Modificar(concepto);
            RepositorioConceptosGastos.GuardarCambios();
        }

        private void ValidarCamposObligatorios(ConceptoGasto concepto)
        {
            if (string.IsNullOrWhiteSpace(concepto.Concepto))//!UtilidadesRegex.SoloLetras(concepto.Concepto))
                throw new SOTException("El concepto no puede ser texto vacío o espacios en blanco");//Recursos.ConceptosGastos.concepto_invalido_excepcion);

            if (concepto.IdCentroCostos <= 0)
                throw new SOTException(Recursos.ConceptosGastos.centro_costos_invalido_excepcion);

            //if (concepto.TopePresupuestal <= 0)
            //    throw new SOTException(Recursos.ConceptosGastos.tope_presupuestal_invalido_excepcion);
        }

        public void EliminarConcepto(int idConcepto, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarConceptosGastos = true });

            var concepto = RepositorioConceptosGastos.Obtener(m => m.Id == idConcepto && m.Activo);

            if (concepto == null)
                throw new SOTException(Recursos.ConceptosGastos.concepto_nulo_excepcion);

            if (ServicioCentrosCostos.VerificarPoseeVinculaciones(concepto.IdCentroCostos))
            {
                if (((IServicioConceptosGastosInterno)this).ObtenerConceptoEnmascarador(concepto.IdCentroCostos).Id == concepto.Id)
                    throw new SOTException(Recursos.ConceptosGastos.enmascarador_no_eliminable_excepcion);
            }

            //if (RepositorioConceptosGastos.Alguno(m => m.Id != concepto.Id && m.Activo && m.Concepto.Trim().ToUpper().Equals(concepto.Concepto.Trim().ToUpper())))
            //    throw new SOTException(Recursos.ConceptosGastos.concepto_duplicado_excepcion, concepto.Concepto);

            var fechaActual = DateTime.Now;

            concepto.Activo = false;
            concepto.FechaModificacion = fechaActual;
            concepto.FechaEliminacion = fechaActual;
            concepto.IdUsuarioModifico = usuario.Id;
            concepto.IdUsuarioElimino = usuario.Id;

            RepositorioConceptosGastos.Modificar(concepto);
            RepositorioConceptosGastos.GuardarCambios();
        }


        bool IServicioConceptosGastosInterno.VerificarNoPoseeConceptos(int idCentroCostos)
        {
            return !RepositorioConceptosGastos.Alguno(m => m.Activo && m.IdCentroCostos == idCentroCostos);
        }

        ConceptoGasto IServicioConceptosGastosInterno.ObtenerConceptoConRestante(int idConceptoGasto, int? idGasto)
        {
            return RepositorioConceptosGastos.ObtenerConceptoConRestante(idConceptoGasto, idGasto);
        }

        ConceptoGasto IServicioConceptosGastosInterno.ObtenerConceptoEnmascarador(int idCentroCostos)
        {
            return RepositorioConceptosGastos.Obtener(m => m.Activo && m.IdCentroCostos == idCentroCostos && m.EnmascaraCompras);
        }

        ConceptoGasto IServicioConceptosGastosInterno.ObtenerConcepto(int idConcepto)
        {
            return RepositorioConceptosGastos.Obtener(m => m.Activo && m.Id == idConcepto);
        }

        bool IServicioConceptosGastosInterno.VerificarPoseeEnmascarador(int idCentroCostos)
        {
            return RepositorioConceptosGastos.Alguno(m => m.Activo && m.IdCentroCostos == idCentroCostos && m.EnmascaraCompras);
        }

        public void RegistrarGasto(int idCuentaPago, string codigoArticulo, decimal valor, int idUsuario, ClasificacionesGastos? clasificacion, DateTime fechaFiltroTurno/*int? idTurno*/)
        {
            foreach (var conceptosArticulos in ServicioArticulosConceptosGasto.ObtenerVinculaciones(codigoArticulo))
                ServicioGastos.CrearGastoAutomaticoV2(idCuentaPago, conceptosArticulos.IdConceptoGasto, valor, idUsuario, clasificacion, fechaFiltroTurno);
        }

        public void RegistrarGasto(int idCuentaPago, int idConceptoGasto, decimal valor, int idUsuario, ClasificacionesGastos? clasificacion, DateTime fechaFiltroTurno/*int? idTurno*/)
        {
            //foreach (var conceptosArticulos in ServicioArticulosConceptosGasto.ObtenerVinculaciones(codigoArticulo))
            ServicioGastos.CrearGastoAutomaticoV2(idCuentaPago, idConceptoGasto, valor, idUsuario, clasificacion, fechaFiltroTurno);
        }

        public void EliminarGastos(int idCuentaPago, int idUsuario)
        {
            ServicioGastos.EliminarGastos(idCuentaPago, idUsuario);
        }

        public void ValidarConceptoGasto(int idGasto, bool soloActivos)
        {
            if (soloActivos)
            {
                if (!RepositorioConceptosGastos.Alguno(m => m.Id == idGasto && m.Activo))
                    throw new SOTException(Recursos.ConceptosGastos.concepto_nulo_eliminado_excepcion);
            }
            else
            {
                if (!RepositorioConceptosGastos.Alguno(m => m.Id == idGasto))
                    throw new SOTException(Recursos.ConceptosGastos.concepto_nulo_excepcion);
            }
        }
    }
}
