﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Modelo.Entidades;

namespace Negocio.ConfiguracionesTipo
{
    public class ServicioConfiguracionesTipo : IServicioConfiguracionesTipo
    {

        IRepositorioConfiguracionesTipo RepositorioConfiguracionesTipo 
        {
            get { return _repositorioConfiguracionesTipo.Value; }
        }

        Lazy<IRepositorioConfiguracionesTipo> _repositorioConfiguracionesTipo = new Lazy<IRepositorioConfiguracionesTipo>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesTipo>(); });

        public ConfiguracionTipo ObtenerConfiguracionTipoNoNull(int idConfiguracionTipo) 
        {
            var configuracion = RepositorioConfiguracionesTipo.ObtenerConfiguracionCargada(idConfiguracionTipo);

            if (configuracion == null)
                throw new SOTException(Recursos.ConfiguracionesTipo.id_configuracion_invalido_excepcion, idConfiguracionTipo.ToString());

            return configuracion;
        }

        public ConfiguracionTipo ObtenerConfiguracionTipo(int idConfiguracionTipo)
        {
            return RepositorioConfiguracionesTipo.ObtenerConfiguracionCargada(idConfiguracionTipo);
        }

        public ConfiguracionTipo ObtenerConfiguracionTipo(int idTipoHabitacion, ConfiguracionTarifa.Tarifas tarifa)
        {
            return RepositorioConfiguracionesTipo.ObtenerConfiguracionTipo(idTipoHabitacion, tarifa);
        }
    }
}
