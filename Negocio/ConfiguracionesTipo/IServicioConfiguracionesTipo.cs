﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;

namespace Negocio.ConfiguracionesTipo
{
    public interface IServicioConfiguracionesTipo
    {
        /// <summary>
        /// Retorna la configuración de un tipo de habitación con el id proporcionado. Si la
        /// configuración es null se arroja una excepción
        /// </summary>
        /// <param name="idConfiguracionTipo"></param>
        /// <returns></returns>
        ConfiguracionTipo ObtenerConfiguracionTipoNoNull(int idConfiguracionTipo);
        /// <summary>
        /// Retorna la configuración de un tipo de habitación con el id proporcionado o null si no
        /// existen coincidencias.
        /// </summary>
        /// <param name="idConfiguracionTipo"></param>
        /// <returns></returns>
        ConfiguracionTipo ObtenerConfiguracionTipo(int idConfiguracionTipo);
        /// <summary>
        /// Retorna la configuración de un tipo de habitación para una tarifa especificada
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <param name="tarifa"></param>
        /// <returns></returns>
        ConfiguracionTipo ObtenerConfiguracionTipo(int idTipoHabitacion, ConfiguracionTarifa.Tarifas tarifa);
    }
}
