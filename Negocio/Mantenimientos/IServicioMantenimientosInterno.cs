﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Mantenimientos
{
    internal interface IServicioMantenimientosInterno : IServicioMantenimientos
    {
        /// <summary>
        /// Permite registrar un mantenimiento
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idTareaMantenimiento"></param>
        /// <param name="idUsuario"></param>
        void Crear(int idHabitacion, int idTareaMantenimiento, int idUsuario);
        /// <summary>
        /// Permite finalizar el manteinimiento en curso de una habitacion
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="idUsuario"></param>
        /// <param name="idsOrdenesTrabajo"></param>
        void FinalizarMantenimiento(int idHabitacion, int idUsuario, List<int> idsOrdenesTrabajo);

        DateTime? ObtenerFechaUltimoMantenimiento(int idTareaMantenimiento);
    }
}
