﻿using Modelo;
using Modelo.Repositorios;
using Negocio.Habitaciones;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Usuarios;
using Transversal.Extensiones;
using Modelo.Dtos;
using Negocio.ConceptosMantenimiento;
using Negocio.TareasMantenimiento;
using System.Transactions;
using Negocio.Almacen.OrdenesTrabajo;
using Negocio.Empleados;

namespace Negocio.Mantenimientos
{
    public class ServicioMantenimientos : IServicioMantenimientosInterno
    {
        IServicioEmpleados ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleados> _servicioEmpleados = new Lazy<IServicioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>(); });

        IServicioTareasMantenimientoInterno ServicioTareasMantenimiento
        {
            get { return _servicioTareasMantenimiento.Value; }
        }

        Lazy<IServicioTareasMantenimientoInterno> _servicioTareasMantenimiento = new Lazy<IServicioTareasMantenimientoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioTareasMantenimientoInterno>(); });
       
        IServicioOrdenesTrabajo ServicioOrdenesTrabajo
        {
            get { return _servicioOrdenesTrabajo.Value; }
        }

        Lazy<IServicioOrdenesTrabajo> _servicioOrdenesTrabajo = new Lazy<IServicioOrdenesTrabajo>(() => { return FabricaDependencias.Instancia.Resolver<IServicioOrdenesTrabajo>(); });
        
        IServicioConceptosMantenimientoInterno ServicioConceptosMantenimiento
        {
            get { return _servicioConceptosMantenimiento.Value; }
        }

        Lazy<IServicioConceptosMantenimientoInterno> _servicioConceptosMantenimiento = new Lazy<IServicioConceptosMantenimientoInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosMantenimientoInterno>(); });


        IRepositorioMantenimientos RepositorioMantenimientos
        {
            get { return _repostorioMantenimientos.Value; }
        }

        Lazy<IRepositorioMantenimientos> _repostorioMantenimientos = new Lazy<IRepositorioMantenimientos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioMantenimientos>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioUsuarios ServicioUsuarios
        {
            get { return _servicioUsuarios.Value; }
        }

        Lazy<IServicioUsuarios> _servicioUsuarios = new Lazy<IServicioUsuarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioUsuarios>(); });

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>(); });



        public List<DtoResumenMantenimiento> ObtenerMantenimientosHabitacion(int idHabitacion, DtoUsuario usuario, int? idconcepto = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarMantenimientos = true });


            List<DtoResumenMantenimiento> retorno = RepositorioMantenimientos.ObtenerMantenimientosHabitacion(idHabitacion, idconcepto, fechaInicio, fechaFin, pagina, elementos);

            //if (pagina == 0 && elementos == 0)
            //    retorno = RepositorioMantenimientos.ObtenerElementos(m => m.IdHabitacion == idHabitacion).ToList();
            //else
            //    retorno = RepositorioMantenimientos.ObtenerElementos(m => m.IdHabitacion == idHabitacion, m => m.FechaCreacion, pagina, elementos).ToList();

            foreach (var grupo in retorno.GroupBy(m=>m.IdUsuario)) 
            {
                var alias = ServicioEmpleados.ObtenerNombreCompletoEmpleado(grupo.Key);

                foreach (var item in grupo) 
                {
                    item.NombreUsuario = alias;
                }
            }

            return retorno;
        }

        public Mantenimiento ObtenerMantenimientoActual(int idHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarMantenimientos = true });

            ServicioHabitaciones.ValidarEstaMantenimiento(idHabitacion);

            var mantenimiento = RepositorioMantenimientos.Obtener(m => m.Activa && m.IdHabitacion == idHabitacion, m => m.ConceptoMantenimiento);

            if (mantenimiento != null)
                return mantenimiento;

            return mantenimiento;
        }

        #region métodos internal

        void IServicioMantenimientosInterno.Crear(int idHabitacion, int idTareaMantenimiento, int idUsuario)
        {
            if (Transaction.Current == null)
                throw new SOTException("Se requiere de una transacción abierta para realizar esta operación");

            if (RepositorioMantenimientos.Alguno(m => m.Activa && m.IdHabitacion == idHabitacion))
                throw new SOTException(Recursos.Mantenimientos.mantenimiento_en_curso_excepcion);

            var tareaSeleccionada = ServicioTareasMantenimiento.ObtenerTareaPorId(idTareaMantenimiento);

            if (tareaSeleccionada == null)
                throw new SOTException(Recursos.Mantenimientos.tarea_nula_excepcion);

            var fechaActual = DateTime.Now;

            var mantenimiento = new Mantenimiento
            {
                Activa = true,
                //Descripcion = descripcion,
                FechaCreacion = fechaActual,
                FechaModificacion = fechaActual,
                IdUsuarioCreo = idUsuario,
                IdUsuarioModifico = idUsuario,
                IdHabitacion = idHabitacion,
                IdConceptoMantenimiento = tareaSeleccionada.IdConceptoMantenimiento,
                TipoMantenimiento = tareaSeleccionada.TipoMantenimiento,
                IdTareaMantenimiento = idTareaMantenimiento,
                Estado = Mantenimiento.Estados.EnProceso,
            };

            RepositorioMantenimientos.Agregar(mantenimiento);
            RepositorioMantenimientos.GuardarCambios();

            ServicioTareasMantenimiento.CalcularSiguienteFecha(idTareaMantenimiento);
        }
        void IServicioMantenimientosInterno.FinalizarMantenimiento(int idHabitacion, int idUsuario, List<int> idsOrdenesTrabajo)
        {
            if (Transaction.Current == null)
                throw new SOTException("Se requiere de una transacción abierta para realizar esta operación");

            var mantenimiento = RepositorioMantenimientos.Obtener(m => m.Activa && m.IdHabitacion == idHabitacion);

            if (mantenimiento == null)
                throw new SOTException(Recursos.Mantenimientos.mantenimiento_nulo_excepcion);

            ServicioOrdenesTrabajo.ValidarCodigosAutorizados(idsOrdenesTrabajo);

            //if (mantenimiento.TipoMantenimiento == TareaMantenimiento.TiposMantenimiento.Correctivo && idsOrdenesTrabajo.Count == 0)
            //    throw new SOTException(Recursos.Mantenimientos.mantenimiento_correctivo_sin_ordenes_excepcion);

            var fechaActual = DateTime.Now;


            mantenimiento.Activa = false;
            mantenimiento.FechaEliminacion = fechaActual;
            mantenimiento.FechaModificacion = fechaActual;
            mantenimiento.IdUsuarioElimino = idUsuario;
            mantenimiento.IdUsuarioModifico = idUsuario;
            mantenimiento.Estado = Mantenimiento.Estados.Finalizado;

            foreach (var id in idsOrdenesTrabajo.Distinct()) 
            {
                mantenimiento.OrdenesTrabajoMantenimiento.Add(new OrdenTrabajoMantenimiento
                {
                    Activa = true,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    IdUsuarioCreo = idUsuario,
                    IdUsuarioModifico = idUsuario,
                    IdOrdenTrabajo = id
                });
            }

            RepositorioMantenimientos.Modificar(mantenimiento);
            RepositorioMantenimientos.GuardarCambios();

            ServicioTareasMantenimiento.FinalizarTareaMantenimiento(mantenimiento.IdTareaMantenimiento, idUsuario);
        }

        DateTime? IServicioMantenimientosInterno.ObtenerFechaUltimoMantenimiento(int idTareaMantenimiento) 
        { 
            var estadoCancelado = (int)Mantenimiento.Estados.Cancelado;

            return RepositorioMantenimientos.ObtenerElementos(m => m.IdTareaMantenimiento == idTareaMantenimiento && m.IdEstado != estadoCancelado)
                   .Select(m => (DateTime?)m.FechaCreacion).FirstOrDefault();
        }

        #endregion

        private void ValidarDatos(Mantenimiento mantenimiento)
        {
            if (!ServicioConceptosMantenimiento.VerificarExiste(mantenimiento.IdConceptoMantenimiento, mantenimiento.Id != 0))
                throw new SOTException(Recursos.TareasMantenimiento.concepto_no_seleccionado_excepcion);

            //if (string.IsNullOrWhiteSpace(mantenimiento.Descripcion))
            //    throw new SOTException(Recursos.TareasMantenimiento.descripcion_invalida_excepcion);

            if (!EnumExtensions.ComoLista<TareaMantenimiento.TiposMantenimiento>().Contains(mantenimiento.TipoMantenimiento))
                throw new SOTException(Recursos.TareasMantenimiento.tipo_mantenimiento_invalido_excepcion);

            //if (mantenimiento.Periodicidad.HasValue && !EnumExtensions.ComoLista<Periodicidades>().Contains(.Periodicidad.Value))
            //    throw new SOTException(Recursos.Mantenimientos.periodicidad_invalida_excepcion);

            //if (!.Periodicidad.HasValue && .FechaFin.HasValue)
            //    throw new SOTException(Recursos.Mantenimientos.fecha_fin_no_permitida_excepcion);

            //if (.FechaFin.HasValue && .FechaFin.Value < .FechaInicio)
            //    throw new SOTException(Recursos.Mantenimientos.fecha_fin_menor_fecha_inicio_excepcion);

        }

    }
}
