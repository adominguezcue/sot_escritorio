﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Modelo.Dtos;

namespace Negocio.Mantenimientos
{
    public interface IServicioMantenimientos
    {
        /// <summary>
        /// Retorna los mantenimientos de una habiación desde la última hasta la primera
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        /// <param name="pagina"></param>
        /// <param name="elementos"></param>
        /// <returns></returns>
        List<DtoResumenMantenimiento> ObtenerMantenimientosHabitacion(int idHabitacion, DtoUsuario usuario , int? idconcepto = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, int pagina = 0, int elementos = 0);
        /// <summary>
        /// Retorna el mantenimiento actual de la habitación con el id proporcionado,
        /// siempre y cuando se encuentre en estado "Mantenimiento"
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Mantenimiento ObtenerMantenimientoActual(int idHabitacion, DtoUsuario usuario);
    }
}
