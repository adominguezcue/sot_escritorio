﻿using Modelo;
using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transversal.Dtos;
using Modelo.Entidades;
using System.Data;
using Modelo.Seguridad.Dtos;

namespace Negocio.TiposHabitacion
{
    public interface IServicioTiposHabitaciones
    {
        /// <summary>
        /// Retorna los tipos de habitaciones que se encuentran activos en el sistema
        /// </summary>
        /// <returns></returns>
        List<TipoHabitacion> ObtenerTiposActivos();
        /// <summary>
        /// Valida que exista un tipo de habitación activo con el Id proporcionado
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        void ValidarExisteActivo(int idTipoHabitacion);
        /// <summary>
        /// Retorna las tarifas soportadas como DtoEnum en base al tipo de habitación
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <returns></returns>
        List<DtoEnum> ObtenerTarifasSoportadasComoDto(int idTipoHabitacion);
        /// <summary>
        /// Retorna las tarifas soportadas en base al tipo de habitación
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <returns></returns>
        List<ConfiguracionTarifa.Tarifas> ObtenerTarifasSoportadas(int idTipoHabitacion);
        /// <summary>
        /// Obtiene el tipo de habitación al que pertenece la habitación con el id proporcionado, con
        /// los tiempos de limpieza configurados
        /// </summary>
        /// <param name="idHabitacion"></param>
        /// <returns></returns>
        TipoHabitacion ObtenerTipoConTiemposLimpiezaPorHabitacion(int idHabitacion);
        /// <summary>
        /// Retorna los tipos de habitación que posean una configuración para la tarifa especificada,
        /// incluyento esa configuración
        /// </summary>
        /// <param name="tarifa"></param>
        /// <returns></returns>
        List<TipoHabitacion> ObtenerTiposActivosConConfiguracion(ConfiguracionTarifa.Tarifas tarifa);
        /// <summary>
        /// Retorna la información de los tipos de pagos
        /// </summary>
        /// <returns></returns>
        List<DtoResumenTipoHabitacion> ObtenerTiposActivosCargadosDataTable();
        /// <summary>
        /// Retorna el tipo de habitación activo con el id proporcionado, cargado con sus tiempos de limpieza y configuraciones por tarifa
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        TipoHabitacion ObtenerTipoActivoCargado(int idTipoHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Permite registrar un nuevo tipo de habitación
        /// </summary>
        /// <param name="tipoNuevo"></param>
        /// <param name="usuario"></param>
        void CrearTipoHabitacion(TipoHabitacion tipoNuevo, DtoUsuario usuario);
        /// <summary>
        /// Permite modificar un tipo de habitación
        /// </summary>
        /// <param name="tipo"></param>
        /// <param name="usuario"></param>
        void ModificarTipoHabitacion(TipoHabitacion tipo, DtoUsuario usuario);
        /// <summary>
        /// Permite eliminar un tipo de habitación
        /// </summary>
        /// <param name="idTipoHabitacion"></param>
        /// <param name="usuario"></param>
        void EliminarTipoHabitacion(int idTipoHabitacion, DtoUsuario usuario);
        /// <summary>
        /// Obtiene la cantidad de habitaciones que están o estuvieron en los distintos estados de
        /// las habitaciones en la fecha proporcionada agrupadas por tipo
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        List<DtoCantidadesHabitacionesTipo> ObtenerEstadosPorTipoFecha(DateTime fecha, DtoUsuario usuario);
    }
}
