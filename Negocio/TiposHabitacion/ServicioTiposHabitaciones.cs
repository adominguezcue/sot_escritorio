﻿using Modelo;
using Modelo.Dtos;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Dtos;
using Transversal.Excepciones;
using Transversal.Extensiones;
using Modelo.Entidades;
using System.Data;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using Negocio.Habitaciones;
using Negocio.Paquetes;
using Negocio.ConfiguracionesGlobales;
using Negocio.Almacen.Parametros;
using System.Transactions;

namespace Negocio.TiposHabitacion
{
    public class ServicioTiposHabitaciones : IServicioTiposHabitaciones
    {
        IRepositorioTiposHabitacion RepositorioTiposHabitaciones
        {
            get { return _repostorioTiposHabitaciones.Value; }
        }

        Lazy<IRepositorioTiposHabitacion> _repostorioTiposHabitaciones = new Lazy<IRepositorioTiposHabitacion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTiposHabitacion>(); });

        IRepositorioTiposHabitacionSincronizacion RepositorioTiposHabitacionSincronizaciones
        {
            get { return _repostorioTiposHabitacionSincronizaciones.Value; }
        }

        Lazy<IRepositorioTiposHabitacionSincronizacion> _repostorioTiposHabitacionSincronizaciones = new Lazy<IRepositorioTiposHabitacionSincronizacion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTiposHabitacionSincronizacion>(); });

        IRepositorioConfiguracionesTarifa RepositorioConfiguracionesTarifaes
        {
            get { return _repostorioConfiguracionesTarifaes.Value; }
        }

        Lazy<IRepositorioConfiguracionesTarifa> _repostorioConfiguracionesTarifaes = new Lazy<IRepositorioConfiguracionesTarifa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesTarifa>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => FabricaDependencias.Instancia.Resolver<IServicioParametros>());

        IServicioHabitacionesInterno ServicioHabitaciones
        {
            get { return _servicioHabitaciones.Value; }
        }

        Lazy<IServicioHabitacionesInterno> _servicioHabitaciones = new Lazy<IServicioHabitacionesInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioHabitacionesInterno>());

        IServicioPaquetes ServicioPaquetes
        {
            get { return _servicioPaquetes.Value; }
        }

        Lazy<IServicioPaquetes> _servicioPaquetes = new Lazy<IServicioPaquetes>(() => FabricaDependencias.Instancia.Resolver<IServicioPaquetes>());


        public List<TipoHabitacion> ObtenerTiposActivos() 
        {
            return RepositorioTiposHabitaciones.ObtenerElementos(m => m.Activo).ToList();
        }

        public void ValidarExisteActivo(int idTipoHabitacion)
        {
            if (!RepositorioTiposHabitaciones.Alguno(m => m.Activo && m.Id == idTipoHabitacion))
                throw new SOTException(Recursos.TiposHabitacion.tipo_habitacion_no_existe_excepcion);
        }

        public List<DtoEnum> ObtenerTarifasSoportadasComoDto(int idTipoHabitacion) 
        {
            return RepositorioTiposHabitaciones.ObtenerTarifasSoportadas(idTipoHabitacion)
                          .Select(m => ((ConfiguracionTarifa.Tarifas)m).ComoDto()).ToList();
        }

        public List<ConfiguracionTarifa.Tarifas> ObtenerTarifasSoportadas(int idTipoHabitacion)
        {
            return RepositorioTiposHabitaciones.ObtenerTarifasSoportadas(idTipoHabitacion)
                          .Select(m => (ConfiguracionTarifa.Tarifas)m).ToList();
        }

        public TipoHabitacion ObtenerTipoConTiemposLimpiezaPorHabitacion(int idHabitacion)
        {
            return RepositorioTiposHabitaciones.ObtenerTipoConTiemposLimpiezaPorHabitacion(idHabitacion);
        }


        public List<TipoHabitacion> ObtenerTiposActivosConConfiguracion(ConfiguracionTarifa.Tarifas tarifa)
        {
            return RepositorioTiposHabitaciones.ObtenerTiposActivosConConfiguracion(tarifa);
        }

        public List<DtoResumenTipoHabitacion> ObtenerTiposActivosCargadosDataTable() 
        {
            return RepositorioTiposHabitaciones.ObtenerTiposActivosCargadosDataTable();
        }


        public TipoHabitacion ObtenerTipoActivoCargado(int idTipoHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarTiposHabitacion = true });

            return RepositorioTiposHabitaciones.ObtenerTipoActivoCargado(idTipoHabitacion);
        }

        public void CrearTipoHabitacion(TipoHabitacion tipoNuevo, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearTiposHabitacion = true });

            if (tipoNuevo == null)
                throw new SOTException(Recursos.TiposHabitacion.tipo_nulo_excepcion);

            if (string.IsNullOrWhiteSpace(tipoNuevo.Clave))
                throw new SOTException(Recursos.TiposHabitacion.clave_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(tipoNuevo.Descripcion))
                throw new SOTException(Recursos.TiposHabitacion.descripcion_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(tipoNuevo.DescripcionExtendida))
                throw new SOTException(Recursos.TiposHabitacion.descripcion_extendida_invalida_excepcion);

            if (RepositorioTiposHabitaciones.Alguno(m => m.Activo && m.Clave.Trim().ToUpper().Equals(tipoNuevo.Clave.Trim().ToUpper())))
                throw new SOTException(Recursos.TiposHabitacion.clave_duplicada_excepcion);

            if (RepositorioTiposHabitaciones.Alguno(m => m.Activo && m.Descripcion.Trim().ToUpper().Equals(tipoNuevo.Descripcion.Trim().ToUpper())))
                throw new SOTException(Recursos.TiposHabitacion.descripcion_duplicada_excepcion);

            if (!tipoNuevo.ConfiguracionesTipos.Any(m => m.Activa))
                throw new SOTException(Recursos.TiposHabitacion.tarifa_no_seleccionada_excepcion);

            if (tipoNuevo.TiemposLimpieza.Any(m => m.Activa && m.Minutos <= 0))
                throw new SOTException(Recursos.TiposHabitacion.tiempo_limpieza_invalido_excepcion);

#if !DEBUG_TEST

            if (tipoNuevo.MaximoReservaciones > 0)
                throw new SOTException(Recursos.TiposHabitacion.no_habitaciones_suficientes_excepcion);
#endif

            var fechaActual = DateTime.Now;

            foreach (var config in tipoNuevo.ConfiguracionesTipos.Where(m => m.Activa)) 
            {
                if (config.Precio <= 0)
                    throw new SOTException(Recursos.TiposHabitacion.precio_tarifa_invalido_excepcion);

                if (config.PrecioHospedajeExtra <= 0)
                    throw new SOTException(Recursos.TiposHabitacion.precio_hospedaje_extra_invalido_excepcion);

                if (config.PrecioPersonaExtra <= 0)
                    throw new SOTException(Recursos.TiposHabitacion.precio_persona_extra_invalido_excepcion);

                foreach (var tiempoExtra in config.TiemposHospedaje.Where(m => m.Activo)) 
                {
                    if (tiempoExtra.Minutos <= 0)
                        throw new SOTException(Recursos.TiposHabitacion.tiempo_extra_invalido_excepcion);
                }
            }

            var parametros = ServicioParametros.ObtenerParametros();

            var tipoSincro = new TipoHabitacionSincronizacion
            {
                Clave = tipoNuevo.Clave,
                Nombre = tipoNuevo.Descripcion,
                Fecha = fechaActual,
                PorcentajeIVA = parametros.Iva_CatPar,
                TipoMovimiento = TipoHabitacionSincronizacion.TiposMovimientos.Creacion
            };

            foreach (var config in tipoNuevo.ConfiguracionesTipos.Where(m => m.Activa))
            {
                config.FechaCreacion = fechaActual;
                config.FechaModificacion = fechaActual;
                config.IdUsuarioCreo = usuario.Id;
                config.IdUsuarioModifico = usuario.Id;

                var ct = RepositorioConfiguracionesTarifaes.Obtener(m => m.Id == config.IdConfiguracionNegocio);

                if (config.HoraSalida == 0)
                    config.HoraSalida = null;

                if (!ct.RangoFijo && config.HoraSalida.HasValue)
                    throw new SOTException("Hora de salida no requerida");

                if (ct.RangoFijo && !config.HoraSalida.HasValue)
                    throw new SOTException("Se requiere una hora de salida");

                tipoSincro.PreciosTipoHabitacionSincronizacion.Add(new PrecioTipoHabitacionSincronizacion
                {
                    Precio = config.Precio,
                    IdTarifa = ct.IdTarifa
                });

                foreach (var tiempoExtra in config.TiemposHospedaje.Where(m => m.Activo))
                {
                    tiempoExtra.FechaCreacion = fechaActual;
                    tiempoExtra.FechaModificacion = fechaActual;
                    tiempoExtra.IdUsuarioCreo = usuario.Id;
                    tiempoExtra.IdUsuarioModifico = usuario.Id;
                }
            }

            foreach (var tiempo in tipoNuevo.TiemposLimpieza.Where(m=>m.Activa)) 
            {
                tiempo.FechaCreacion = fechaActual;
                tiempo.FechaModificacion = fechaActual;
                tiempo.IdUsuarioCreo = usuario.Id;
                tiempo.IdUsuarioModifico = usuario.Id;
            }

            tipoNuevo.FechaCreacion = fechaActual;
            tipoNuevo.FechaModificacion = fechaActual;
            tipoNuevo.IdUsuarioCreo = usuario.Id;
            tipoNuevo.IdUsuarioModifico = usuario.Id;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
            {
                RepositorioTiposHabitaciones.Agregar(tipoNuevo);
                RepositorioTiposHabitaciones.GuardarCambios();

                RepositorioTiposHabitacionSincronizaciones.Agregar(tipoSincro);
                RepositorioTiposHabitacionSincronizaciones.GuardarCambios();


                scope.Complete();
            }

        }

        public void ModificarTipoHabitacion(TipoHabitacion tipoM, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarTiposHabitacion = true });

            if (tipoM == null)
                throw new SOTException(Recursos.TiposHabitacion.tipo_nulo_excepcion);

            if (string.IsNullOrWhiteSpace(tipoM.Clave))
                throw new SOTException(Recursos.TiposHabitacion.clave_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(tipoM.Descripcion))
                throw new SOTException(Recursos.TiposHabitacion.descripcion_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(tipoM.DescripcionExtendida))
                throw new SOTException(Recursos.TiposHabitacion.descripcion_extendida_invalida_excepcion);

            if (RepositorioTiposHabitaciones.Alguno(m => m.Activo && m.Id != tipoM.Id && m.Clave.Trim().ToUpper().Equals(tipoM.Clave.Trim().ToUpper())))
                throw new SOTException(Recursos.TiposHabitacion.clave_duplicada_excepcion);

            if (RepositorioTiposHabitaciones.Alguno(m => m.Activo && m.Id != tipoM.Id && m.Descripcion.Trim().ToUpper().Equals(tipoM.Descripcion.Trim().ToUpper())))
                throw new SOTException(Recursos.TiposHabitacion.descripcion_duplicada_excepcion);

            if (!tipoM.ConfiguracionesTipos.Any(m => m.Activa))
                throw new SOTException(Recursos.TiposHabitacion.tarifa_no_seleccionada_excepcion);

            if (tipoM.TiemposLimpieza.Any(m => m.Activa && m.IdTipoLimpieza != (int)TareaLimpieza.TiposLimpieza.MinutosVenta && m.Minutos <= 0))
                throw new SOTException(Recursos.TiposHabitacion.tiempo_limpieza_invalido_excepcion);

            if (tipoM == null)
                throw new SOTException(Recursos.TiposHabitacion.tipo_nulo_excepcion);

            var tipo = RepositorioTiposHabitaciones.ObtenerTipoActivoCargado(tipoM.Id);

            #region configuraciones

            foreach (var par in (from ctOriginal in tipo.ConfiguracionesTipos
                                 join ctMod in tipoM.ConfiguracionesTipos on ctOriginal.Id equals ctMod.Id
                                 select new
                                 {
                                     ctOriginal,
                                     ctMod
                                 }).ToList())
            {
                par.ctOriginal.SincronizarPrimitivas(par.ctMod);

                foreach (var parH in (from hOriginal in par.ctOriginal.TiemposHospedaje
                                      join hMod in par.ctMod.TiemposHospedaje on hOriginal.Id equals hMod.Id
                                      select new
                                      {
                                          hOriginal,
                                          hMod
                                      }).ToList())
                    parH.hOriginal.SincronizarPrimitivas(parH.hMod);

                foreach (var hNuevo in par.ctMod.TiemposHospedaje.Where(m => m.Activo && m.Id == 0)) 
                {
                    var nuevoTiempo = new TiempoHospedaje();

                    nuevoTiempo.SincronizarPrimitivas(hNuevo);

                    par.ctOriginal.TiemposHospedaje.Add(nuevoTiempo);
                }
            }
            foreach (var cNuevo in tipoM.ConfiguracionesTipos.Where(m => m.Activa && m.Id == 0))
            {
                var nuevoC = new ConfiguracionTipo();

                nuevoC.SincronizarPrimitivas(cNuevo);

                tipo.ConfiguracionesTipos.Add(nuevoC);
            }

            #endregion
            #region tiempo

            foreach (var par in (from tOriginal in tipo.TiemposLimpieza
                                 join tMod in tipoM.TiemposLimpieza on tOriginal.Id equals tMod.Id
                                 select new
                                 {
                                     ctOriginal = tOriginal,
                                     ctMod = tMod
                                 }).ToList())
                par.ctOriginal.SincronizarPrimitivas(par.ctMod);

            foreach (var tNuevo in tipoM.TiemposLimpieza.Where(m => m.Activa && m.Id == 0))
            {
                var nuevoT = new TiempoLimpieza();

                nuevoT.SincronizarPrimitivas(tNuevo);

                tipoM.TiemposLimpieza.Add(nuevoT);
            }

            #endregion

            var fechaActual = DateTime.Now;

            var cambioClave = !tipo.Clave.Equals(tipoM.Clave);

            var parametros = ServicioParametros.ObtenerParametros();

            TipoHabitacionSincronizacion tipoEliminar = cambioClave ?
                new TipoHabitacionSincronizacion
                {
                    Clave = tipo.Clave,
                    Nombre = tipo.Descripcion,
                    Fecha = fechaActual,
                    PorcentajeIVA = parametros.Iva_CatPar,
                    TipoMovimiento = TipoHabitacionSincronizacion.TiposMovimientos.Eliminacion
                } : null;

            tipo.SincronizarPrimitivas(tipoM);

#if !DEBUG_TEST

            if (ServicioHabitaciones.ObtenerCantidadActivarPorTipo(tipo.Id) < tipo.MaximoReservaciones)
                throw new SOTException(Recursos.TiposHabitacion.no_habitaciones_suficientes_excepcion);
#endif

            foreach (var config in tipo.ConfiguracionesTipos.Where(m => m.Activa))
            {
                if (config.Precio <= 0)
                    throw new SOTException(Recursos.TiposHabitacion.precio_tarifa_invalido_excepcion);

                if (config.PrecioHospedajeExtra <= 0)
                    throw new SOTException(Recursos.TiposHabitacion.precio_hospedaje_extra_invalido_excepcion);

                if (config.PrecioPersonaExtra <= 0)
                    throw new SOTException(Recursos.TiposHabitacion.precio_persona_extra_invalido_excepcion);

                foreach (var tiempoExtra in config.TiemposHospedaje.Where(m => m.Activo))
                {
                    if (tiempoExtra.Minutos <= 0)
                        throw new SOTException(Recursos.TiposHabitacion.tiempo_extra_invalido_excepcion);
                }
            }

            var tipoSincro = new TipoHabitacionSincronizacion
            {
                Clave = tipo.Clave,
                Nombre = tipo.Descripcion,
                Fecha = fechaActual,
                PorcentajeIVA = parametros.Iva_CatPar,
                TipoMovimiento = cambioClave ? TipoHabitacionSincronizacion.TiposMovimientos.Creacion : 
                                               TipoHabitacionSincronizacion.TiposMovimientos.Modificacion
            };

            

            foreach (var config in tipo.ConfiguracionesTipos.Where(m => m.Activa || (!m.Activa && m.Id != 0)))
            {
                if (config.Id == 0)
                {
                    config.FechaCreacion = fechaActual;
                    config.IdUsuarioCreo = usuario.Id;
                    config.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Creado;
                }
                else
                    config.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;

                if (!config.Activa)
                {
                    config.FechaEliminacion = fechaActual;
                    config.IdUsuarioElimino = usuario.Id;

                    foreach (var tiempoExtra in config.TiemposHospedaje)
                        tiempoExtra.Activo = false;
                }
                config.FechaModificacion = fechaActual;
                config.IdUsuarioModifico = usuario.Id;

                if (config.Activa)
                {
                    var ct = RepositorioConfiguracionesTarifaes.Obtener(m => m.Id == config.IdConfiguracionNegocio);

                    if (config.HoraSalida == 0)
                        config.HoraSalida = null;

                    if (!ct.RangoFijo && config.HoraSalida.HasValue)
                        throw new SOTException("Hora de salida no requerida");

                    if (ct.RangoFijo && !config.HoraSalida.HasValue)
                        throw new SOTException("Se requiere una hora de salida");

                    tipoSincro.PreciosTipoHabitacionSincronizacion.Add(new PrecioTipoHabitacionSincronizacion
                    {
                        Precio = config.Precio,
                        IdTarifa = ct.IdTarifa
                    });
                }

                foreach (var tiempoExtra in config.TiemposHospedaje.Where(m => m.Activo || (!m.Activo && m.Id != 0)))
                {
                    if (tiempoExtra.Id == 0)
                    {
                        tiempoExtra.FechaCreacion = fechaActual;
                        tiempoExtra.IdUsuarioCreo = usuario.Id;
                        tiempoExtra.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Creado;
                    }
                    else
                        tiempoExtra.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;

                    if (!tiempoExtra.Activo)
                    {
                        tiempoExtra.FechaEliminacion = fechaActual;
                        tiempoExtra.IdUsuarioElimino = usuario.Id;
                    }
                    tiempoExtra.FechaModificacion = fechaActual;
                    tiempoExtra.IdUsuarioModifico = usuario.Id;
                }
            }

            foreach (var tiempo in tipo.TiemposLimpieza.Where(m => m.Activa || (!m.Activa && m.Id != 0)))
            {
                if (tiempo.Id == 0)
                {
                    tiempo.FechaCreacion = fechaActual;
                    tiempo.IdUsuarioCreo = usuario.Id;
                }
                else
                    tiempo.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;

                if (!tiempo.Activa)
                {
                    tiempo.FechaEliminacion = fechaActual;
                    tiempo.IdUsuarioElimino = usuario.Id;
                }
                tiempo.FechaModificacion = fechaActual;
                tiempo.IdUsuarioModifico = usuario.Id;
            }

            tipo.FechaModificacion = fechaActual;
            tipo.IdUsuarioModifico = usuario.Id;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
            {
                RepositorioTiposHabitaciones.Modificar(tipo);
                RepositorioTiposHabitaciones.GuardarCambios();

                if (tipoEliminar != null)
                    RepositorioTiposHabitacionSincronizaciones.Agregar(tipoEliminar);

                RepositorioTiposHabitacionSincronizaciones.Agregar(tipoSincro);
                RepositorioTiposHabitacionSincronizaciones.GuardarCambios();

                scope.Complete();
            }
        }

        public void EliminarTipoHabitacion(int idTipoHabitacion, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarTiposHabitacion = true });

            var tipo = RepositorioTiposHabitaciones.ObtenerTipoActivoCargado(idTipoHabitacion);

            if (tipo == null)
                throw new SOTException(Recursos.TiposHabitacion.tipo_nulo_excepcion);

            if (ServicioHabitaciones.VerificarExistenPorTipo(tipo.Id))
                throw new SOTException(Recursos.TiposHabitacion.tipo_posee_habitaciones_excepcion);

            if (ServicioPaquetes.VerificarExistenPorTipo(tipo.Id))
                throw new SOTException(Recursos.TiposHabitacion.tipo_posee_paquetes_excepcion);

            var fechaActual = DateTime.Now;

            foreach (var config in tipo.ConfiguracionesTipos.Where(m => m.Activa))
            {
                config.Activa = false;
                config.FechaEliminacion = fechaActual;
                config.FechaModificacion = fechaActual;
                config.IdUsuarioElimino = usuario.Id;
                config.IdUsuarioModifico = usuario.Id;

                foreach (var tiempoExtra in config.TiemposHospedaje.Where(m => m.Activo))
                {
                    tiempoExtra.Activo = false;
                    tiempoExtra.FechaEliminacion = fechaActual;
                    tiempoExtra.FechaModificacion = fechaActual;
                    tiempoExtra.IdUsuarioElimino = usuario.Id;
                    tiempoExtra.IdUsuarioModifico = usuario.Id;
                }
            }

            foreach (var tiempo in tipo.TiemposLimpieza.Where(m => m.Activa))
            {
                tiempo.Activa = false;
                tiempo.FechaEliminacion = fechaActual;
                tiempo.FechaModificacion = fechaActual;
                tiempo.IdUsuarioElimino = usuario.Id;
                tiempo.IdUsuarioModifico = usuario.Id;
            }

            tipo.Activo = false;
            tipo.FechaEliminacion = fechaActual;
            tipo.FechaModificacion = fechaActual;
            tipo.IdUsuarioElimino = usuario.Id;
            tipo.IdUsuarioModifico = usuario.Id;

            var parametros = ServicioParametros.ObtenerParametros();

            var tipoSincro = new TipoHabitacionSincronizacion
            {
                Clave = tipo.Clave,
                Nombre = tipo.Descripcion,
                Fecha = fechaActual,
                PorcentajeIVA = parametros.Iva_CatPar,
                TipoMovimiento = TipoHabitacionSincronizacion.TiposMovimientos.Eliminacion
            };

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
            {
                RepositorioTiposHabitaciones.Modificar(tipo);
                RepositorioTiposHabitaciones.GuardarCambios();

                RepositorioTiposHabitacionSincronizaciones.Agregar(tipoSincro);
                RepositorioTiposHabitacionSincronizaciones.GuardarCambios();

                scope.Complete();
            }
        }

        public List<DtoCantidadesHabitacionesTipo> ObtenerEstadosPorTipoFecha(DateTime fecha, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarHabitacionesTodas = true });

            var resultado = new List<DtoCantidadesHabitacionesTipo>();

            var resumenes = RepositorioTiposHabitaciones.SP_ObtenerCantidadHabitacionesPorEstado(fecha);

            foreach (var grupoTipos in resumenes.GroupBy(m => m.TipoHabitacion)) 
            {
                resultado.Add(new DtoCantidadesHabitacionesTipo
                {
                    TipoHabitacion = grupoTipos.Key,
                    CantidadBloqueada = grupoTipos.Where(m=> m.EstadoHabitacion == Habitacion.EstadosHabitacion.Bloqueada).Sum(m=>m.Cantidad),
                    CantidadLibre = grupoTipos.Where(m=> m.EstadoHabitacion == Habitacion.EstadosHabitacion.Libre).Sum(m=>m.Cantidad),
                    CantidadLimpieza = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.Limpieza).Sum(m => m.Cantidad),
                    CantidadMantenimiento = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.Mantenimiento).Sum(m => m.Cantidad),
                    CantidadOcupada = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.Ocupada).Sum(m => m.Cantidad),
                    CantidadPendienteCobro = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.PendienteCobro).Sum(m => m.Cantidad),
                    CantidadPreparada = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.Preparada).Sum(m => m.Cantidad),
                    CantidadPreparadaReservada = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.PreparadaReservada).Sum(m => m.Cantidad),
                    CantidadReservada = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.Reservada).Sum(m => m.Cantidad),
                    CantidadSucia = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.Sucia).Sum(m => m.Cantidad),
                    CantidadSupervision = grupoTipos.Where(m => m.EstadoHabitacion == Habitacion.EstadosHabitacion.Supervision).Sum(m => m.Cantidad)
                });
            }

            var tiposIncluidos = resumenes.Select(m=>m.IdTipoHabitacion).Distinct().ToList();

            foreach (var tipoExcluido in RepositorioTiposHabitaciones.ObtenerElementos(m => !tiposIncluidos.Contains(m.Id)))
                resultado.Add(new DtoCantidadesHabitacionesTipo { TipoHabitacion = tipoExcluido.Descripcion });

            return resultado.OrderBy(m => m.TipoHabitacion).ToList();
        }
    }
}
