﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;

namespace Negocio.CortesiasCancelaciones
{
    public interface IServicioCortesiasCancelaciones
    {
        List<VW_ConceptoCancelacionesYCortesias> ObtieneConceptos();

        List<DtoCortesiasCancelaciones> ObtieneCortesiasCancelaciones(string concepto, DateTime fechainicio, DateTime fechafin, DtoUsuario usuario);

        List<VW_VentaCorrecta> ObtieneVentaCorrecta(int ticketseleccionado, string tipoid);
    }
}
