﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;
using Transversal.Dependencias;
using Modelo.Dtos;
using Modelo.Repositorios;
using Negocio.Seguridad.Permisos;
using Modelo.Seguridad.Dtos;

namespace Negocio.CortesiasCancelaciones
{
    public class ServicioCortesiasCancelaciones : IServicioCortesiasCancelaciones
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });


        IRepositorioCortesiaCancelaciones RepositorioCortesiasCancelaciones
        {
            get { return _RepositorioCortesiasCancelaciones.Value; }
        }

        Lazy<IRepositorioCortesiaCancelaciones> _RepositorioCortesiasCancelaciones = new Lazy<IRepositorioCortesiaCancelaciones>(() =>
        { return FabricaDependencias.Instancia.Resolver<IRepositorioCortesiaCancelaciones>(); });


        IRepositorioVW_ConceptosCancelacionesYCortesias RepositorioVW_ConceptosCancelacionesYCortesias
        {
            get { return _RepositorioVW_ConceptosCancelacionesYCortesias.Value; }
        }

        Lazy<IRepositorioVW_ConceptosCancelacionesYCortesias> _RepositorioVW_ConceptosCancelacionesYCortesias = new Lazy<IRepositorioVW_ConceptosCancelacionesYCortesias>(() =>
        { return FabricaDependencias.Instancia.Resolver<IRepositorioVW_ConceptosCancelacionesYCortesias>(); });

        public List<VW_ConceptoCancelacionesYCortesias> ObtieneConceptos()
        {
            return RepositorioVW_ConceptosCancelacionesYCortesias.ObtenerTodo().ToList();
        }

        public List<DtoCortesiasCancelaciones> ObtieneCortesiasCancelaciones(string concepto, DateTime fechainicio, DateTime fechafin, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultaCortesiaCancelaciones = true });

            return RepositorioCortesiasCancelaciones.ObtieneCortesiasCancelaciones(concepto, fechainicio, fechafin);
        }

        public List<VW_VentaCorrecta> ObtieneVentaCorrecta(int ticketseleccionado, string tipoid)
        {
            return RepositorioCortesiasCancelaciones.ObtieneVentaCorrecta( ticketseleccionado,  tipoid);
        }
    }
}
