﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Entidades;

namespace Negocio.LecturaMantenimiento
{
    public interface IServicioLecturaHMantenimiento 
    {
        List<CatPresentacionMantenimiento> ObtienePresentacionMantenimiento();
        List<CatPresentacionMantenimiento> ObtieneTodas();
        void AgregaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento);
        void ModificaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento);
        void EliminaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento);
    }
}
