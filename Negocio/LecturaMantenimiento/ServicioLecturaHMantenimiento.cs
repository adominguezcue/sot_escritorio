﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Modelo.Entidades;
using Modelo.Repositorios;
using Transversal.Dependencias;

namespace Negocio.LecturaMantenimiento
{
    public class ServicioLecturaHMantenimiento : IServicioLecturaHMantenimiento
    {

        IRepositorioCatPresentacionMantenimiento RepositorioMantenimientoLectura
        {
            get { return _repositorioMantenimientoLectura.Value; }
        }
        Lazy<IRepositorioCatPresentacionMantenimiento> _repositorioMantenimientoLectura = new Lazy<IRepositorioCatPresentacionMantenimiento>(
            () => { return FabricaDependencias.Instancia.Resolver<IRepositorioCatPresentacionMantenimiento>(); });


        public List<CatPresentacionMantenimiento> ObtienePresentacionMantenimiento()
        {
          return RepositorioMantenimientoLectura.ObtienePresentacionMantenimiento();
        }

        public List<CatPresentacionMantenimiento> ObtieneTodas()
        {
            var enumerable = RepositorioMantenimientoLectura.ObtenerTodo();
            return enumerable.ToList();
        }

        public void AgregaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento)
        {
            if(catPresentacionMantenimiento != null)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    RepositorioMantenimientoLectura.Agregar(catPresentacionMantenimiento);
                    RepositorioMantenimientoLectura.GuardarCambios();
                    scope.Complete();
                }

            }
        }

        public void ModificaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento)
        {
            if (catPresentacionMantenimiento != null)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    RepositorioMantenimientoLectura.Modificar(catPresentacionMantenimiento);
                    RepositorioMantenimientoLectura.GuardarCambios();
                    scope.Complete();
                }
            }
        }

        public void EliminaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento)
        {
            if (catPresentacionMantenimiento != null)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    catPresentacionMantenimiento.Activa = false;
                    RepositorioMantenimientoLectura.Modificar(catPresentacionMantenimiento);
                    RepositorioMantenimientoLectura.GuardarCambios();
                    scope.Complete();
                }
            }
        }
    }
}
