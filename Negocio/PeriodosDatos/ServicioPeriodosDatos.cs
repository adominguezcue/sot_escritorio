﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Modelo.Entidades;
using Modelo.Repositorios;
using Transversal.Dependencias;

namespace Negocio.PeriodosDatos
{
    public class ServicioPeriodosDatos : IServicioPeriodosDatos
    {
        IRepositorioCatPeriodo RepositorioCatPeriodo
        {
            get { return _repositoriocatperiodo.Value; }
        }

        Lazy<IRepositorioCatPeriodo> _repositoriocatperiodo = new Lazy<IRepositorioCatPeriodo>(
            () => { return FabricaDependencias.Instancia.Resolver<IRepositorioCatPeriodo>(); });

        public List<CatPeriodoDatos> ObtienePeriodosFiltrados()
        {
            return RepositorioCatPeriodo.ObtienePeriodosFiltrados();
        }

        public void AgregaPerioricidad(CatPeriodoDatos catPeriodoDatos)
        {
            if (catPeriodoDatos != null)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    RepositorioCatPeriodo.Agregar(catPeriodoDatos);
                    RepositorioCatPeriodo.GuardarCambios();
                    scope.Complete();
                }

            }
        }

        public void ModificaPerioricdad(CatPeriodoDatos catPeriodoDatos)
        {
            if (catPeriodoDatos != null)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    RepositorioCatPeriodo.Modificar(catPeriodoDatos);
                    RepositorioCatPeriodo.GuardarCambios();
                    scope.Complete();
                }

            }
        }

        public void EliminarPerioricidad(CatPeriodoDatos catPeriodoDatos)
        {
            if (catPeriodoDatos != null)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    catPeriodoDatos.Activa = false;
                    RepositorioCatPeriodo.Modificar(catPeriodoDatos);
                    RepositorioCatPeriodo.GuardarCambios();
                    scope.Complete();
                }
            }
        }
    }
}
