﻿using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Propinas
{
    internal interface IServicioPropinasInterno : IServicioPropinas
    {
        /// <summary>
        /// Permite crear un nuevo registro de propina y cobrar el valor a las entidades externas correspondientes
        /// </summary>
        /// <param name="propina"></param>
        /// <param name="usuario"></param>
        void RegistrarPropina(Propina propina, int idUsuario);
        /// <summary>
        /// Permite modificar un registro de propina y cobrar el valor a las entidades externas correspondientes
        /// </summary>
        /// <param name="propina"></param>
        /// <param name="usuario"></param>
        void ModificarPropina(Propina propina, int idUsuario);
        /// <summary>
        /// Permite marcar una propina como eliminada
        /// </summary>
        /// <param name="idPropina"></param>
        /// <param name="usuario"></param>
        void EliminarPropina(int idPropina, int idUsuario);

        /// <summary>
        /// Retorna el total de las propinas dentro del rango especificado, agrupadas por tipo
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        Dictionary<Propina.TiposPropina, decimal> ObtenerTotalPropinasPorTipo(DateTime? fechaInicio, DateTime? fechaFin);
        Dictionary<TiposTarjeta, decimal> ObtenerTotalPropinasPorTarjeta(DateTime? fechaInicio, DateTime? fechaFin);
        /// <summary>
        /// Retorna las propinas dentro del período especificado
        /// </summary>
        /// <param name="fechaInicio"></param>
        /// <param name="fechaFin"></param>
        /// <returns></returns>
        List<Propina> ObtenerPropinasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, params Propina.TiposPropina[] tipos);
    }
}
