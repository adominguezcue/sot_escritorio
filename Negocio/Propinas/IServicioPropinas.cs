﻿using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Propinas
{
    public interface IServicioPropinas
    {

        ///// <summary>
        ///// Retorna el total de las propinas dentro del rango especificado, agrupadas por tipo
        ///// </summary>
        ///// <param name="usuario"></param>
        ///// <param name="fechaInicio"></param>
        ///// <param name="fechaFin"></param>
        ///// <returns></returns>
        //Dictionary<Propina.TiposPropina, decimal> ObtenerTotalPropinasPorTipoExterno(DtoUsuario usuario, DateTime? fechaInicio = null, DateTime? fechaFin = null);
        /// <summary>
        /// Retorna el valor de las propinas que poseen la transacción especificada
        /// </summary>
        /// <param name="transaccion"></param>
        /// <returns></returns>
        List<DtoResumenPropina> ObtenerResumenesPorTransaccion(string transaccion);
        /// <summary>
        /// Retorna la propina que coindica con los filtros proprocionados
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="nullable"></param>
        /// <returns></returns>
        Propina ObtenerPropina(string transaccion, string numeroTarjeta, TiposTarjeta tipoTarjeta);
    }
}
