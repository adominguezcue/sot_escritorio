﻿using Modelo;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.ConfiguracionesPropinas;
using Negocio.Empleados;
using Negocio.Pagos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace Negocio.Propinas
{
    public class ServicioPropinas : IServicioPropinasInterno
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioPagos ServicioPagos
        {
            get { return _servicioPagos.Value; }
        }

        Lazy<IServicioPagos> _servicioPagos = new Lazy<IServicioPagos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPagos>(); });

        IServicioEmpleadosInterno ServicioEmpleados
        {
            get { return _servicioEmpleados.Value; }
        }

        Lazy<IServicioEmpleadosInterno> _servicioEmpleados = new Lazy<IServicioEmpleadosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioEmpleadosInterno>(); });

        IRepositorioPropinas RepositorioPropinas
        {
            get { return _repostorioPropinas.Value; }
        }

        Lazy<IRepositorioPropinas> _repostorioPropinas = new Lazy<IRepositorioPropinas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPropinas>(); });

        #region métodos internal

        List<Propina> IServicioPropinasInterno.ObtenerPropinasPorPeriodo(DateTime? fechaInicio, DateTime? fechaFin, params Propina.TiposPropina[] tipos)
        {
            return RepositorioPropinas.ObtenerPorPeriodo(fechaInicio, fechaFin, tipos);
        }

        void IServicioPropinasInterno.RegistrarPropina(Propina propina, int idUsuario)
        {
            if (Transaction.Current == null)
                throw new SOTException(Recursos.Propinas.registro_requiere_transaccion_exception);

            if (propina == null)
                throw new SOTException(Recursos.Propinas.propina_nula_excepcion);

            ValidarTipoPropina(propina);

            if (!ServicioEmpleados.VerificarEmpleadoActivo(propina.IdEmpleado))
                throw new SOTException("El destinatario de la propina no es un empleado activo en el sistema");

            ServicioEmpleados.ValidarPermisos(propina.IdEmpleado, new DtoPermisos { RecibirPropinas = true });

            //if (propina.TipoPropina == Propina.TiposPropina.Comanda || propina.TipoPropina == Propina.TiposPropina.Restaurante)
            //    ServicioEmpleados.ValidarMeseroActivoHabilitado(propina.IdEmpleado);
            //else
            //    ServicioEmpleados.ValidarValetActivoHabilitado(propina.IdEmpleado);

            ValidarTarjeta(propina);

            ValidarMonto(propina);

            ServicioPagos.ValidarTransaccion(propina.Transaccion);

            propina.Activo = true;

            Pagar(propina.Valor, propina.Referencia, idUsuario);

            RepositorioPropinas.Agregar(propina);
            RepositorioPropinas.GuardarCambios();
        }

        void IServicioPropinasInterno.ModificarPropina(Propina propina, int idUsuario)
        {
            if (Transaction.Current == null)
                throw new SOTException(Recursos.Propinas.registro_requiere_transaccion_exception);

            if (propina == null)
                throw new SOTException(Recursos.Propinas.propina_nula_excepcion);

            ValidarTipoPropina(propina);

            ServicioEmpleados.ValidarPermisos(propina.IdEmpleado, new DtoPermisos { RecibirPropinas = true });

            //if (propina.TipoPropina == Propina.TiposPropina.Comanda || propina.TipoPropina == Propina.TiposPropina.Restaurante)
            //    ServicioEmpleados.ValidarMeseroActivoHabilitado(propina.IdEmpleado);
            //else
            //    ServicioEmpleados.ValidarValetActivoHabilitado(propina.IdEmpleado);

            ValidarTarjeta(propina);

            ValidarMonto(propina);

            ServicioPagos.ValidarTransaccion(propina.Transaccion);

            propina.Activo = true;

            Pagar(propina.Valor, propina.Referencia, idUsuario);

            RepositorioPropinas.Modificar(propina);
            RepositorioPropinas.GuardarCambios();
        }

        void IServicioPropinasInterno.EliminarPropina(int idPropina, int idUsuario)
        {
            if (Transaction.Current == null)
                throw new SOTException(Recursos.Propinas.registro_requiere_transaccion_exception);

            var propina = RepositorioPropinas.Obtener(m => m.Activo && m.Id == idPropina);

            if (propina == null)
                throw new SOTException(Recursos.Propinas.propina_nula_excepcion);

            var fechaActual = DateTime.Now;

            propina.Activo = false;
            propina.FechaEliminacion = fechaActual;
            propina.FechaModificacion = fechaActual;
            propina.IdUsuarioElimino = idUsuario;
            propina.IdUsuarioModifico = idUsuario;

            RepositorioPropinas.Modificar(propina);
            RepositorioPropinas.GuardarCambios();
        }


        Dictionary<Propina.TiposPropina, decimal> IServicioPropinasInterno.ObtenerTotalPropinasPorTipo(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioPropinas.ObtenerTotalPropinasPorTipo(fechaInicio, fechaFin);

            foreach (var item in Enum.GetValues(typeof(Propina.TiposPropina)).Cast<Propina.TiposPropina>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }
        Dictionary<TiposTarjeta, decimal> IServicioPropinasInterno.ObtenerTotalPropinasPorTarjeta(DateTime? fechaInicio, DateTime? fechaFin)
        {
            var diccionario = RepositorioPropinas.ObtenerTotalPropinasPorTarjeta(fechaInicio, fechaFin);

            foreach (var item in EnumExtensions.ComoLista<TiposTarjeta>())
            {
                if (!diccionario.ContainsKey(item))
                    diccionario.Add(item, 0);
            }

            return diccionario;
        }

        #endregion


        private void ValidarTarjeta(Propina propina)
        {
            if (propina.TipoTarjeta != TiposTarjeta.AmericanExpress &&
               propina.TipoTarjeta != TiposTarjeta.MasterCard &&
               propina.TipoTarjeta != TiposTarjeta.Visa)
                throw new SOTException(Recursos.Pagos.tipo_tarjeta_desconocido_excepcion);
        }

        private void ValidarMonto(Propina propina)
        {
            if (propina.Valor <= 0)
                throw new SOTException(Recursos.Propinas.valor_propina_invalido_excepcion);
        }

        private void ValidarTipoPropina(Propina propina)
        {
            if (propina.TipoPropina != Propina.TiposPropina.Comanda &&
               propina.TipoPropina != Propina.TiposPropina.Restaurante &&
               propina.TipoPropina != Propina.TiposPropina.Valet)
                throw new SOTException(Recursos.Propinas.tipo_propina_desconocido_excepcion);
        }

        private void Pagar(decimal monto, string codigo, int idUsuario)
        {
#warning implementar la comunicación con los servicios externos pertinentes para realizar los pagos
        }

        //public Dictionary<Propina.TiposPropina, decimal> ObtenerTotalPropinasPorTipoExterno(DtoUsuario usuario, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        //{
        //    ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarPropinas = true });

        //    var diccionario = RepositorioPropinas.ObtenerTotalPropinasPorTipo(fechaInicio, fechaFin);

        //    foreach (var item in Enum.GetValues(typeof(Propina.TiposPropina)).Cast<Propina.TiposPropina>())
        //    {
        //        if (!diccionario.ContainsKey(item))
        //            diccionario.Add(item, 0);
        //    }

        //    return diccionario;
        //}


        public List<DtoResumenPropina> ObtenerResumenesPorTransaccion(string transaccion)
        {
            return RepositorioPropinas.ObtenerResumenesPorTransaccion(transaccion);
        }


        public Propina ObtenerPropina(string transaccion, string numeroTarjeta, TiposTarjeta tipoTarjeta)
        {
            var idTipoTarjeta = (int)tipoTarjeta;

            return RepositorioPropinas.Obtener(m => m.Transaccion == transaccion && m.NumeroTarjeta == numeroTarjeta && m.IdTipoTarjeta == idTipoTarjeta && m.Activo);
        }
    }
}
