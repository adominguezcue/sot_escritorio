﻿using Modelo.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Tickets
{
    public interface IImpresoraTickets
    {
        void ImprimirTicket(DtoResumenVenta resumen, string nombreImpresora, int copias);
    }
}
