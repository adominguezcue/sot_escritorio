﻿using Modelo.Dtos;
using Modelo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Tickets
{
    internal interface IServicioTicketsInterno : IServicioTickets
    {
        void ImprimirTicket(Venta venta, string nombreImpresora, int numeroCopias);
        void ImprimirTickets(List<string> transacciones, string nombreImpresora, int numeroCopias);
        void ImprimirTicket(VentaRenta venta, string nombreImpresora, int numeroCopias);
        void ImprimirTicket(OrdenTaxi orden, string nombreImpresora, int numeroCopias);
        void ImprimirTicket(Comanda comanda, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase);
        void ImprimirTicket(ConsumoInterno consumo, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase);
        void ImprimirTicket(OrdenRestaurante orden, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase);
        void ImprimirTicket(int idMesa, string nombreImpresora, int numeroCopias);
    }
}
