﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Repositorios;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Clientes;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesGlobales;
using Negocio.Preventas;
using Negocio.Rentas;
using Negocio.Restaurantes;
using Negocio.Ventas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Tickets
{
    public class ServicioTickets : IServicioTicketsInterno
    {
        IImpresoraTickets ImpresoraTickets
        {
            get { return _ImpresoraTickets.Value; }
        }

        Lazy<IImpresoraTickets> _ImpresoraTickets = new Lazy<IImpresoraTickets>(() => { return FabricaDependencias.Instancia.Resolver<IImpresoraTickets>(); });

        IServicioPreventasInterno ServicioPreventas
        {
            get { return _servicioPreventas.Value; }
        }

        Lazy<IServicioPreventasInterno> _servicioPreventas = new Lazy<IServicioPreventasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPreventasInterno>(); });

        IServicioVentasInterno ServicioVentas
        {
            get { return _servicioVentas.Value; }
        }

        Lazy<IServicioVentasInterno> _servicioVentas = new Lazy<IServicioVentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioVentasInterno>(); });


        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioRentasInterno ServicioRentas
        {
            get { return _servicioRentas.Value; }
        }

        Lazy<IServicioRentasInterno> _servicioRentas = new Lazy<IServicioRentasInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRentasInterno>(); });

        IServicioRestaurantesInterno ServicioRestaurantes
        {
            get { return _servicioRestaurantes.Value; }
        }

        Lazy<IServicioRestaurantesInterno> _servicioRestaurantes = new Lazy<IServicioRestaurantesInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioRestaurantesInterno>(); });

        IServicioConfiguracionesGlobales ServicioConfiguracionesGlobales
        {
            get { return _servicioConfiguracionesGlobales.Value; }
        }

        Lazy<IServicioConfiguracionesGlobales> _servicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });

        IServicioClientes ServicioClientes
        {
            get { return _servicioClientes.Value; }
        }

        Lazy<IServicioClientes> _servicioClientes = new Lazy<IServicioClientes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioClientes>(); });

        IRepositorioPagosRenta RepositorioPagosRenta
        {
            get { return _repostorioPagosRenta.Value; }
        }

        Lazy<IRepositorioPagosRenta> _repostorioPagosRenta = new Lazy<IRepositorioPagosRenta>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosRenta>(); });

        IRepositorioVentasRentas RepositorioVentasRenta
        {
            get { return _repostorioVentasRenta.Value; }
        }

        Lazy<IRepositorioVentasRentas> _repostorioVentasRenta = new Lazy<IRepositorioVentasRentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentasRentas>(); });

        IRepositorioVentas RepositorioVentas
        {
            get { return _repostorioVentas.Value; }
        }

        Lazy<IRepositorioVentas> _repostorioVentas = new Lazy<IRepositorioVentas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVentas>(); });

        IRepositorioPagosTarjetasPuntos RepositorioPagosTarjetasPuntos
        {
            get { return _repostorioPagosTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioPagosTarjetasPuntos> _repostorioPagosTarjetasPuntos = new Lazy<IRepositorioPagosTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosTarjetasPuntos>(); });

        IRepositorioTarjetasPuntos RepositorioTarjetasPuntos
        {
            get { return _repostorioTarjetasPuntos.Value; }
        }

        Lazy<IRepositorioTarjetasPuntos> _repostorioTarjetasPuntos = new Lazy<IRepositorioTarjetasPuntos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTarjetasPuntos>(); });

        IRepositorioPagosOcupacionesMesa RepositorioPagosOcupacionesMesa
        {
            get { return _repostorioPagosOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioPagosOcupacionesMesa> _repostorioPagosOcupacionesMesa = new Lazy<IRepositorioPagosOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosOcupacionesMesa>(); });

        IRepositorioPagosComandas RepositorioPagosComandas
        {
            get { return _repostorioPagosComandas.Value; }
        }

        Lazy<IRepositorioPagosComandas> _repostorioPagosComandas = new Lazy<IRepositorioPagosComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosComandas>(); });

        IRepositorioPagosConsumosInternos RepositorioPagosConsumosInternos
        {
            get { return _repostorioPagosConsumosInternos.Value; }
        }

        Lazy<IRepositorioPagosConsumosInternos> _repostorioPagosConsumosInternos = new Lazy<IRepositorioPagosConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPagosConsumosInternos>(); });


        IRepositorioEmpleados RepositorioEmpleados
        {
            get { return _repostorioEmpleados.Value; }
        }

        Lazy<IRepositorioEmpleados> _repostorioEmpleados = new Lazy<IRepositorioEmpleados>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioEmpleados>(); });

        IRepositorioComandas RepositorioComandas
        {
            get { return _repostorioComandas.Value; }
        }

        Lazy<IRepositorioComandas> _repostorioComandas = new Lazy<IRepositorioComandas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioComandas>(); });

        IRepositorioConsumosInternos RepositorioConsumosInternos
        {
            get { return _repostorioConsumosInternos.Value; }
        }

        Lazy<IRepositorioConsumosInternos> _repostorioConsumosInternos = new Lazy<IRepositorioConsumosInternos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConsumosInternos>(); });

        IRepositorioOrdenesRestaurantes RepositorioOrdenesRestaurantes
        {
            get { return _repostorioOrdenesRestaurantes.Value; }
        }

        Lazy<IRepositorioOrdenesRestaurantes> _repostorioOrdenesRestaurantes = new Lazy<IRepositorioOrdenesRestaurantes>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOrdenesRestaurantes>(); });

        IRepositorioOcupacionesMesa RepositorioOcupacionesMesa
        {
            get { return _repostorioOcupacionesMesa.Value; }
        }

        Lazy<IRepositorioOcupacionesMesa> _repostorioOcupacionesMesa = new Lazy<IRepositorioOcupacionesMesa>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioOcupacionesMesa>(); });

        public void ImprimirTicketVentaRenta(int idVentaRenta, string nombreImpresora, int numeroCopias)
        {
            var resumen = ObtenerResumenVentaRenta(idVentaRenta);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketVenta(int idVenta, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenVenta(idVenta);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketComanda(int idComanda, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var resumen = ObtenerResumenComanda(idComanda, filtros, lineasBase);
            resumen.EsComanda = esComanda;
            //await Task.Factory.StartNew(() =>
            //{

            if (resumen.Items.Count > 0)
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketOrdenRestaurante(int idOrden, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var resumen = ObtenerResumenOrdenRestaurante(idOrden, filtros, lineasBase);
            resumen.EsComanda = esComanda;
            //await Task.Factory.StartNew(() =>
            //{

            if (resumen.Items.Count > 0)
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketConsumoInterno(int idConsumoInterno, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var resumen = ObtenerResumenConsumoInterno(idConsumoInterno, filtros, lineasBase);
            resumen.EsComanda = esComanda;
            //await Task.Factory.StartNew(() =>
            //{

            if (resumen.Items.Count > 0)
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        #region métodos internal

        void IServicioTicketsInterno.ImprimirTickets(List<string> transacciones, string nombreImpresora, int numeroCopias)
        {

            foreach (var transaccion in transacciones.Distinct())
            {
                var resumen = ServicioVentas.ObtenerResumenVenta(transaccion);
                //await Task.Factory.StartNew(() =>
                //{
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
                //});
            }
        }

        void IServicioTicketsInterno.ImprimirTicket(Venta venta, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenVenta(venta.Id);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        void IServicioTicketsInterno.ImprimirTicket(VentaRenta venta, string nombreImpresora, int numeroCopias)
        {
            var resumen = ObtenerResumenVentaRenta(venta.Id);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        void IServicioTicketsInterno.ImprimirTicket(OrdenTaxi orden, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenOrdenTaxi(orden.Id);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        void IServicioTicketsInterno.ImprimirTicket(Comanda comanda, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var resumen = ObtenerResumen(comanda, filtros, lineasBase);
            resumen.EsComanda = esComanda;
            //await Task.Factory.StartNew(() =>
            //{

            if (resumen.Items.Count > 0)
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        void IServicioTicketsInterno.ImprimirTicket(ConsumoInterno consumo, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var resumen = ObtenerResumen(consumo, filtros, lineasBase);
            resumen.EsComanda = esComanda;
            //await Task.Factory.StartNew(() =>
            //{

            if (resumen.Items.Count > 0)
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        void IServicioTicketsInterno.ImprimirTicket(OrdenRestaurante orden, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var resumen = ObtenerResumen(orden, filtros, lineasBase);
            resumen.EsComanda = esComanda;
            //await Task.Factory.StartNew(() =>
            //{

            if (resumen.Items.Count > 0)
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        void IServicioTicketsInterno.ImprimirTicket(int idMesa, string nombreImpresora, int numeroCopias)
        {
            var resumen = ObtenerResumen(idMesa);
            //await Task.Factory.StartNew(() =>
            //{

            if (resumen.Items.Count > 0)
                ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        #endregion
        /*
        private DtoResumenVenta ObtenerResumen(Venta venta)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();



            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = venta.FechaModificacion,
                FechaFin = venta.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = configuracionG.RazonSocial,
                RFC = configuracionG.RFC,
                Ticket = venta.Ticket.ToString()
            };


            IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
            //Modelo.Entidades.DatosFiscales datosF;

            switch (venta.ClasificacionVenta)
            {
                case Venta.ClasificacionesVenta.Habitacion:
                    {
                        pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion, venta.FechaCancelacion);
                        resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccion(venta.Transaccion);

                        #region detalles de habitación

                        var ventaRenta = RepositorioPagosRenta.ObtenerVentaConConceptos(venta.Transaccion);

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(ventaRenta.IdValet ?? 0, ventaRenta.IdUsuarioModifico);

                        if (ventaRenta.EsInicial)
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = 1,
                                Nombre = ventaRenta.TipoHabitacionTmp,
                                PrecioUnidad = ventaRenta.PrecioHotelTmp * (1 + configuracionG.Iva_CatPar),
                            });
                        }
                        foreach (var grupoHE in ventaRenta.Extensiones.Where(m => m.Activa && !m.EsRenovacion).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoHE.Count(),
                                Nombre = "Tiempo extra",
                                PrecioUnidad = grupoHE.Key * (1 + configuracionG.Iva_CatPar),
                            });
                        }

                        foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => m.Activa).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPE.Count(),
                                Nombre = "Persona extra",
                                PrecioUnidad = grupoPE.Key * (1 + configuracionG.Iva_CatPar),
                            });
                        }

                        foreach (var grupoRen in ventaRenta.Extensiones.Where(m => m.Activa && m.EsRenovacion).GroupBy(m => m.Precio))
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoRen.Count(),
                                Nombre = "Renovación",
                                PrecioUnidad = grupoRen.Key * (1 + configuracionG.Iva_CatPar),
                            });
                        }

                        foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).GroupBy(m => m.IdPaquete))
                        {
                            var muestra = grupoPAQ.First();

                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                                Nombre = muestra.Paquete.Nombre,
                                PrecioUnidad = muestra.Paquete.Precio * (1 + configuracionG.Iva_CatPar),
                            });
                        }


                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.Restaurante:
                    {
                        pagos = RepositorioPagosOcupacionesMesa.ObtenerPagosPorTransaccion(venta.Transaccion);
                        bool esMesa;

                        if (!(esMesa = pagos.Any()))
                        {
                            pagos = RepositorioPagosConsumosInternos.ObtenerPagosPorTransaccion(venta.Transaccion);
                            resumen.Procedencia = "";
                        }
                        else
                        {
                            resumen.Procedencia = "Mesa " + ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(venta.Transaccion);
                        }

                        #region detalles de restaurante

                        if (esMesa)
                        {
                            var ocupacion = RepositorioOcupacionesMesa.ObtenerOcupacionFinalizadaConDetalles(((PagoOcupacionMesa)pagos.First()).IdOcupacionMesa);

                            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(ocupacion.IdMesero, ocupacion.IdUsuarioModifico);

                            var idsArticulos = ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

                            var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                            foreach (var artCom in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante))
                            {
                                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                            }

                            foreach (var grupoArticulo in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).GroupBy(m => m.IdArticulo))
                            {
                                var muestra = grupoArticulo.First();

                                resumen.Items.Add(new DtoItemCobrable
                                {
                                    Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                    Nombre = muestra.ArticuloTmp.Desc_Art,
                                    PrecioUnidad = muestra.PrecioUnidadFinal// * (1 + configuracionG.Iva_CatPar),
                                });
                            }
                        }
                        else
                        {
                            var consumoInterno = RepositorioConsumosInternos.ObtenerConsumoInternoEntregadoConDetalles(((PagoConsumoInterno)pagos.First()).IdConsumoInterno);

                            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(consumoInterno.IdMesero ?? 0, consumoInterno.IdUsuarioModifico);

                            var idsArticulos = consumoInterno.ArticulosConsumoInterno.Select(m => m.IdArticulo).ToList();

                            var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

                            foreach (var artCom in consumoInterno.ArticulosConsumoInterno)
                            {
                                artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                            }

                            foreach (var grupoArticulo in consumoInterno.ArticulosConsumoInterno.GroupBy(m => m.IdArticulo))
                            {
                                var muestra = grupoArticulo.First();

                                resumen.Items.Add(new DtoItemCobrable
                                {
                                    Cantidad = grupoArticulo.Sum(m => m.Cantidad),
                                    Nombre = muestra.ArticuloTmp.Desc_Art,
                                    PrecioUnidad = muestra.PrecioUnidadFinal //* (1 + configuracionG.Iva_CatPar),
                                });
                            }
                        }

                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.RoomService:
                    {
                        pagos = RepositorioPagosComandas.ObtenerPagosPorTransaccion(venta.Transaccion);
                        resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(venta.Transaccion);

                        #region detalles de comanda

                        var comanda = RepositorioComandas.ObtenerComandaPagadaConDetalles(((PagoComanda)pagos.First()).IdComanda);

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

                        var idsArticulos = comanda.ArticulosComanda.Select(m => m.IdArticulo).ToList();

                        var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

                        foreach (var artCom in comanda.ArticulosComanda)
                        {
                            artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
                        }

                        foreach (var articuloComanda in comanda.ArticulosComanda)
                        {
                            resumen.Items.Add(new DtoItemCobrable
                            {
                                Cantidad = articuloComanda.Cantidad,
                                Nombre = articuloComanda.ArticuloTmp.Desc_Art,
                                PrecioUnidad = articuloComanda.PrecioUnidadFinal,
                            });
                        }

                        #endregion
                    }
                    break;
                case Venta.ClasificacionesVenta.TarjetaPuntos:
                    {
                        pagos = RepositorioPagosTarjetasPuntos.ObtenerPagosPorTransaccion(venta.Transaccion);
                        resumen.Procedencia = "";

                        #region detalles de tarjeta de puntos

                        var idTarjeta = ((PagoTarjetaPuntos)pagos.First()).IdTarjetaPuntos;

                        var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Id == idTarjeta);

                        resumen.Items.Add(new DtoItemCobrable
                        {
                            Cantidad = 1,
                            Nombre = "Tarjeta de puntos",
                            PrecioUnidad = pagos.Sum(m => m.Valor)//tarjeta.Precio * (1 + configuracionG.Iva_CatPar),
                        });

                        resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(tarjeta.IdUsuarioModifico);


                        #endregion
                    }
                    break;
                default:
                    pagos = new List<IPago>();
                    resumen.Procedencia = "";
                    //datosF = null;
                    break;
            }

            #region pagos

            var sumaPagos = pagos.Where(m => m.Activo).Sum(m => m.Valor);

            decimal valor = 0, descuento = 0, cortesia = 0;

            foreach (var pago in pagos)
            {
                switch (pago.TipoPago)
                {
                    case TiposPago.Consumo:
                        descuento += pago.Valor;
                        break;
                    case TiposPago.Cortesia:
                        cortesia += pago.Valor;
                        break;
                    case TiposPago.Cupon:
                        cortesia += pago.Valor;
                        break;
                    case TiposPago.Efectivo:
                        valor += pago.Valor;
                        break;
                    case TiposPago.Reservacion:
                        valor += pago.Valor;
                        break;
                    case TiposPago.TarjetaCredito:
                        valor += pago.Valor;
                        break;
                    case TiposPago.VPoints:
                        descuento += pago.Valor;
                        break;
                    case TiposPago.Transferencia:
                        valor += pago.Valor;
                        break;
                }
            }

            resumen.SubTotal = sumaPagos;
            resumen.Cortesia = cortesia;
            resumen.Descuentos = descuento;
            resumen.Total = valor;

            #endregion

            return resumen;
        }*/
        //[Obsolete("Movido al servicio de ventas")]
        //private DtoResumenVenta ObtenerResumenVenta(int idVenta)
        //{
        //    var configG = ServicioParametros.ObtenerParametros();

        //    var venta = RepositorioVentas.Obtener(m => m.Id == idVenta);

        //    var resumen = new DtoResumenVenta
        //    {
        //        Direccion = configG.Direccion,
        //        FechaInicio = venta.FechaModificacion,
        //        FechaFin = venta.FechaModificacion,
        //        Hotel = configG.Nombre,
        //        RazonSocial = configG.RazonSocial,
        //        RFC = configG.RFC,
        //        Ticket = venta.Ticket.ToString()
        //    };


        //    IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
        //    //Modelo.Entidades.DatosFiscales datosF;

        //    var porcentajeIVA = venta.IVAEnCurso;

        //    decimal descuentoP = 0;

        //    switch (venta.ClasificacionVenta)
        //    {
        //        case Venta.ClasificacionesVenta.Habitacion:
        //            {
        //                pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion, !venta.Activa);//.FechaCancelacion);
        //                resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccion(venta.Transaccion);

        //                #region detalles de habitación

        //                var ventaRenta = RepositorioPagosRenta.ObtenerVentaConConceptos(venta.Transaccion, venta.Cancelada);

        //                resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(ventaRenta.IdValet ?? 0, ventaRenta.IdUsuarioModifico);

        //                if (ventaRenta.EsInicial)
        //                {
        //                    resumen.Items.Add(new DtoItemCobrable
        //                    {
        //                        Cantidad = 1,
        //                        Nombre = ventaRenta.TipoHabitacionTmp,
        //                        PrecioUnidad = Math.Round(ventaRenta.PrecioHotelTmp * (1 + porcentajeIVA), 2)
        //                    });
        //                }
        //                foreach (var grupoHE in ventaRenta.Extensiones.Where(m => m.Activa && !m.EsRenovacion).GroupBy(m => m.Precio))
        //                {
        //                    resumen.Items.Add(new DtoItemCobrable
        //                    {
        //                        Cantidad = grupoHE.Count(),
        //                        Nombre = "Tiempo extra",
        //                        PrecioUnidad = Math.Round(grupoHE.Key * (1 + porcentajeIVA), 2)
        //                    });
        //                }

        //                foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => m.Activa).GroupBy(m => m.Precio))
        //                {
        //                    resumen.Items.Add(new DtoItemCobrable
        //                    {
        //                        Cantidad = grupoPE.Count(),
        //                        Nombre = "Persona extra",
        //                        PrecioUnidad = Math.Round(grupoPE.Key * (1 + porcentajeIVA), 2)
        //                    });
        //                }

        //                foreach (var grupoRen in ventaRenta.Extensiones.Where(m => m.Activa && m.EsRenovacion).GroupBy(m => m.Precio))
        //                {
        //                    resumen.Items.Add(new DtoItemCobrable
        //                    {
        //                        Cantidad = grupoRen.Count(),
        //                        Nombre = "Renovación",
        //                        PrecioUnidad = Math.Round(grupoRen.Key * (1 + porcentajeIVA), 2)
        //                    });
        //                }

        //                foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).GroupBy(m => m.IdPaquete))
        //                {
        //                    var muestra = grupoPAQ.First();

        //                    resumen.Items.Add(new DtoItemCobrable
        //                    {
        //                        Cantidad = grupoPAQ.Sum(m => m.Cantidad),
        //                        Nombre = muestra.Paquete.Nombre + " (+)",
        //                        PrecioUnidad = Math.Round(muestra.Paquete.Precio * (1 + porcentajeIVA), 2)
        //                    });
        //                }

        //                descuentoP = ventaRenta.PaquetesRenta.Where(m => m.Activo).Sum(m => Math.Round(m.Descuento * (1 + porcentajeIVA), 2) * m.Cantidad);

        //                foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => m.Activo && m.Descuento > 0).GroupBy(m => m.IdPaquete))
        //                {
        //                    var muestra = grupoPAQ.First();

        //                    resumen.Items.Add(new DtoItemCobrable
        //                    {
        //                        Cantidad = grupoPAQ.Sum(m => m.Cantidad),
        //                        Nombre = muestra.Paquete.Nombre + " (-)",
        //                        PrecioUnidad = Math.Round(muestra.Paquete.Descuento * (1 + porcentajeIVA), 2)
        //                    });
        //                }


        //                #endregion
        //            }
        //            break;
        //        case Venta.ClasificacionesVenta.Restaurante:
        //            {
        //                pagos = RepositorioPagosOcupacionesMesa.ObtenerPagosPorTransaccion(venta.Transaccion);
        //                bool esMesa;

        //                if (!(esMesa = pagos.Any()))
        //                {
        //                    pagos = RepositorioPagosConsumosInternos.ObtenerPagosPorTransaccion(venta.Transaccion);
        //                    resumen.Procedencia = "";
        //                }
        //                else
        //                {
        //                    resumen.Procedencia = "Mesa " + ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(venta.Transaccion);
        //                }

        //                #region detalles de restaurante

        //                if (esMesa)
        //                {
        //                    var ocupacion = RepositorioOcupacionesMesa.ObtenerOcupacionFinalizadaConDetalles(((PagoOcupacionMesa)pagos.First()).IdOcupacionMesa);

        //                    resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(ocupacion.IdMesero, ocupacion.IdUsuarioModifico);

        //                    var idsArticulos = ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).Select(m => m.IdArticulo).ToList();

        //                    var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

        //                    foreach (var artCom in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante))
        //                    {
        //                        artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
        //                    }

        //                    foreach (var grupoArticulo in ocupacion.OrdenesRestaurante.SelectMany(m => m.ArticulosOrdenRestaurante).GroupBy(m => m.IdArticulo))
        //                    {
        //                        var muestra = grupoArticulo.First();

        //                        resumen.Items.Add(new DtoItemCobrable
        //                        {
        //                            Cantidad = grupoArticulo.Sum(m => m.Cantidad),
        //                            Nombre = muestra.ArticuloTmp.Desc_Art,
        //                            PrecioUnidad = muestra.PrecioUnidadFinal// * (1 + configuracionG.Iva_CatPar),
        //                        });
        //                    }
        //                }
        //                else
        //                {
        //                    var consumoInterno = RepositorioConsumosInternos.ObtenerConsumoInternoEntregadoConDetalles(((PagoConsumoInterno)pagos.First()).IdConsumoInterno);

        //                    resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(consumoInterno.IdMesero ?? 0, consumoInterno.IdUsuarioModifico);

        //                    var idsArticulos = consumoInterno.ArticulosConsumoInterno.Select(m => m.IdArticulo).ToList();

        //                    var articulos = ServicioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);

        //                    foreach (var artCom in consumoInterno.ArticulosConsumoInterno)
        //                    {
        //                        artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
        //                    }

        //                    foreach (var grupoArticulo in consumoInterno.ArticulosConsumoInterno.GroupBy(m => m.IdArticulo))
        //                    {
        //                        var muestra = grupoArticulo.First();

        //                        resumen.Items.Add(new DtoItemCobrable
        //                        {
        //                            Cantidad = grupoArticulo.Sum(m => m.Cantidad),
        //                            Nombre = muestra.ArticuloTmp.Desc_Art,
        //                            PrecioUnidad = muestra.PrecioUnidadFinal //* (1 + configuracionG.Iva_CatPar),
        //                        });
        //                    }
        //                }

        //                #endregion
        //            }
        //            break;
        //        case Venta.ClasificacionesVenta.RoomService:
        //            {
        //                pagos = RepositorioPagosComandas.ObtenerPagosPorTransaccion(venta.Transaccion);
        //                resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(venta.Transaccion);

        //                #region detalles de comanda

        //                var comanda = RepositorioComandas.ObtenerComandaPagadaConDetalles(((PagoComanda)pagos.First()).IdComanda);

        //                resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

        //                var idsArticulos = comanda.ArticulosComanda.Select(m => m.IdArticulo).ToList();

        //                var articulos = ServicioArticulos.ObtenerArticulosConLineaPorId(idsArticulos);

        //                foreach (var artCom in comanda.ArticulosComanda)
        //                {
        //                    artCom.ArticuloTmp = articulos.FirstOrDefault(m => m.Cod_Art == artCom.IdArticulo);
        //                }

        //                foreach (var articuloComanda in comanda.ArticulosComanda)
        //                {
        //                    resumen.Items.Add(new DtoItemCobrable
        //                    {
        //                        Cantidad = articuloComanda.Cantidad,
        //                        Nombre = articuloComanda.ArticuloTmp.Desc_Art,
        //                        PrecioUnidad = articuloComanda.PrecioUnidadFinal,
        //                    });
        //                }

        //                #endregion
        //            }
        //            break;
        //        case Venta.ClasificacionesVenta.TarjetaPuntos:
        //            {
        //                pagos = RepositorioPagosTarjetasPuntos.ObtenerPagosPorTransaccion(venta.Transaccion);
        //                resumen.Procedencia = "";

        //                #region detalles de tarjeta de puntos

        //                var idTarjeta = ((PagoTarjetaPuntos)pagos.First()).IdTarjetaPuntos;

        //                var tarjeta = RepositorioTarjetasPuntos.Obtener(m => m.Id == idTarjeta);

        //                resumen.Items.Add(new DtoItemCobrable
        //                {
        //                    Cantidad = 1,
        //                    Nombre = "Tarjeta de puntos",
        //                    PrecioUnidad = pagos.Sum(m => m.Valor)//tarjeta.Precio * (1 + configuracionG.Iva_CatPar),
        //                });

        //                resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIds(tarjeta.IdUsuarioModifico);


        //                #endregion
        //            }
        //            break;
        //        default:
        //            pagos = new List<IPago>();
        //            resumen.Procedencia = "";
        //            //datosF = null;
        //            break;
        //    }

        //    #region pagos

        //    var sumaPagos = pagos.Where(m => m.Activo).Sum(m => m.Valor) + descuentoP;

        //    decimal valor = 0, descuento = descuentoP, cortesia = 0, valorV = 0;

        //    foreach (var pago in pagos)
        //    {
        //        switch (pago.TipoPago)
        //        {
        //            case TiposPago.Consumo:
        //                descuento += pago.Valor;
        //                break;
        //            case TiposPago.Cortesia:
        //                cortesia += pago.Valor;
        //                break;
        //            case TiposPago.Cupon:
        //                cortesia += pago.Valor;
        //                break;
        //            case TiposPago.Efectivo:
        //                valor += pago.Valor;
        //                break;
        //            case TiposPago.Reservacion:
        //                valor += pago.Valor;
        //                break;
        //            case TiposPago.TarjetaCredito:
        //                valor += pago.Valor;
        //                break;
        //            case TiposPago.VPoints:
        //                descuento += pago.Valor;
        //                valorV += pago.Valor;
        //                break;
        //            case TiposPago.Transferencia:
        //                valor += pago.Valor;
        //                break;
        //        }
        //    }

        //    if(valorV != 0)
        //        resumen.Items.Add(new DtoItemCobrable
        //        {
        //            Cantidad = 1,
        //            Nombre = "VPOINTS (-)",
        //            PrecioUnidad = valorV,
        //        });

        //    resumen.SubTotal = sumaPagos;
        //    resumen.Cortesia = cortesia;
        //    resumen.Descuentos = descuento;
        //    resumen.Total = valor;

        //    #endregion

        //    return resumen;
        //}
        private DtoResumenVenta ObtenerResumenVentaRenta(int idVentaRenta)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var ventaRenta = RepositorioPagosRenta.ObtenerVentaConConceptos(idVentaRenta);
            Venta venta = null;

            if (ventaRenta == null || (venta = RepositorioVentas.Obtener(m => m.Transaccion == ventaRenta.Transaccion)) != null)
            {
                ventaRenta = RepositorioVentasRenta.Obtener(m => m.Id == idVentaRenta);

                if (ventaRenta != null && !string.IsNullOrEmpty(ventaRenta.Transaccion))
                {
                    venta = RepositorioVentas.Obtener(m => m.Transaccion == ventaRenta.Transaccion);

                    if (venta != null)
                        return ServicioVentas.ObtenerResumenVenta(venta.Id);
                }
            }

            var preventa = ServicioPreventas.ObtenerPreventaPorId(ventaRenta.IdPreventa ?? 0);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = ventaRenta.FechaModificacion,
                FechaFin = ventaRenta.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa != null ? preventa.Ticket : ""//venta.Ticket.ToString()
            };


            //IEnumerable<IPago> pagos;// = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
            //Modelo.Entidades.DatosFiscales datosF;

            //pagos = RepositorioPagosRenta.ObtenerPagosPorTransaccion(venta.Transaccion);
            resumen.Procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorIdRenta(ventaRenta.IdRenta);

            #region detalles de habitación



            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ventaRenta.IdValet ?? 0, ventaRenta.IdUsuarioModifico);

            if (ventaRenta.EsInicial)
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = 1,
                    Nombre = ventaRenta.TipoHabitacionTmp,
                    PrecioUnidad = Math.Round(ventaRenta.PrecioHotelTmp * (1 + configuracionG.Iva_CatPar), 2),
                });
            }
            foreach (var grupoHE in ventaRenta.Extensiones.Where(m => m.Activa && !m.EsRenovacion).GroupBy(m => m.Precio))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoHE.Count(),
                    Nombre = "Tiempo extra",
                    PrecioUnidad = Math.Round(grupoHE.Key * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            foreach (var grupoPE in ventaRenta.PersonasExtra.Where(m => m.Activa).GroupBy(m => m.Precio))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoPE.Count(),
                    Nombre = "Persona extra",
                    PrecioUnidad = Math.Round(grupoPE.Key * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            foreach (var grupoRen in ventaRenta.Extensiones.Where(m => m.Activa && m.EsRenovacion).GroupBy(m => m.Precio))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoRen.Count(),
                    Nombre = "Renovación",
                    PrecioUnidad = Math.Round(grupoRen.Key * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => m.Activo && m.Precio > 0).GroupBy(m => new { m.IdPaquete, m.Precio } ))
            {
                var muestra = grupoPAQ.First();

                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                    Nombre = muestra.Paquete.Nombre + " (+)",
                    PrecioUnidad = Math.Round(grupoPAQ.Key.Precio * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            var descuentos = ventaRenta.PaquetesRenta.Where(m => m.Activo).Sum(m => Math.Round(m.Descuento * (1 + configuracionG.Iva_CatPar), 2) * m.Cantidad);

            foreach (var grupoPAQ in ventaRenta.PaquetesRenta.Where(m => m.Activo && m.Descuento > 0).GroupBy(m => new { m.IdPaquete, m.Descuento }))
            {
                var muestra = grupoPAQ.First();

                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = grupoPAQ.Sum(m => m.Cantidad),
                    Nombre = muestra.Paquete.Nombre + " (-)",
                    PrecioUnidad = Math.Round(grupoPAQ.Key.Descuento * (1 + configuracionG.Iva_CatPar), 2)
                });
            }

            if (ventaRenta.MontosNoReembolsablesRenta.Any(m => m.Activo))
            {
                resumen.Items.Add(new DtoItemCobrable
                {
                    Cantidad = 1,//grupoPAQ.Sum(m => m.Cantidad),
                    Nombre = "Ajuste reservación",//muestra.Paquete.Nombre + " (-)",
                    PrecioUnidad = ventaRenta.MontosNoReembolsablesRenta.Sum(m => m.ValorConIVA)//Math.Round(muestra.Paquete.Precio * (1 + configuracionG.Iva_CatPar), 2)
                });
            }


            #endregion


            #region pagos



            resumen.SubTotal = ventaRenta.ValorConIVA + descuentos;//sumaPagos;
            resumen.Cortesia = 0;// cortesia;
            resumen.Descuentos = descuentos;// descuento;
            resumen.Total = ventaRenta.ValorConIVA;// valor;

            #endregion

            return resumen;
        }
        private DtoResumenVenta ObtenerResumen(ConsumoInterno consumo, List<string> filtros, Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            string procedencia = "";//"Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(consumo.Transaccion);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(consumo.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = consumo.FechaCreacion,
                FechaFin = consumo.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia
            };

            var articulos = RepositorioConsumosInternos.SP_ObtenerResumenesArticulosTicketConsumoInterno(consumo.Id);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(consumo.IdMesero ?? 0, consumo.IdUsuarioModifico);

            if (filtros == null)
                articulos = articulos.Where(m => m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();
            else
                articulos = articulos.Where(m => filtros.Contains(m.Codigo) && m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = resumen.SubTotal;
            resumen.Total = 0;

            return resumen;
        }
        private DtoResumenVenta ObtenerResumenConsumoInterno(int idConsumoInterno, List<string> filtros, Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            string procedencia = "";//"Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorTransaccionRoomService(consumo.Transaccion);

            Venta venta = null;

            var consumo = RepositorioConsumosInternos.Obtener(m => m.Id == idConsumoInterno);

            if (consumo == null || (venta = RepositorioVentas.Obtener(m => m.Transaccion == consumo.Transaccion)) != null)
            {
                consumo = RepositorioConsumosInternos.Obtener(m => m.Id == idConsumoInterno);

                if (consumo != null && !string.IsNullOrEmpty(consumo.Transaccion))
                {
                    venta = RepositorioVentas.Obtener(m => m.Transaccion == consumo.Transaccion);

                    if (venta != null)
                        return ServicioVentas.ObtenerResumenVenta(venta.Id);
                }
            }


            var preventa = ServicioPreventas.ObtenerPreventaPorId(consumo.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = consumo.FechaCreacion,
                FechaFin = consumo.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia
            };

            var articulos = RepositorioConsumosInternos.SP_ObtenerResumenesArticulosTicketConsumoInterno(consumo.Id);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(consumo.IdMesero ?? 0, consumo.IdUsuarioModifico);

            if (filtros == null)
                articulos = articulos.Where(m => m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();
            else
                articulos = articulos.Where(m => filtros.Contains(m.Codigo) && m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = resumen.SubTotal;
            resumen.Total = 0;

            return resumen;
        }
        private DtoResumenVenta ObtenerResumen(Comanda comanda, List<string> filtros, Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            string procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorIdComanda(comanda.Id);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(comanda.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = comanda.FechaCreacion,
                FechaFin = comanda.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia
            };

            var articulos = RepositorioComandas.SP_ObtenerResumenesArticulosTicketComanda(comanda.Id);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

            if (filtros == null)
                articulos = articulos.Where(m => m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();
            else
                articulos = articulos.Where(m => filtros.Contains(m.Codigo) && m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = 0;
            resumen.Cortesia = articulos.Where(m => m.EsCortesia).Sum(m => m.Total);
            resumen.Total = resumen.SubTotal - resumen.Cortesia - resumen.Descuentos;

            return resumen;
        }
        private DtoResumenVenta ObtenerResumenComanda(int idComanda, List<string> filtros, Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var comanda = RepositorioComandas.Obtener(m => m.Id == idComanda);

            string procedencia = "Habitación " + ServicioRentas.ObtenerNumeroHabitacionPorIdComanda(comanda.Id);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(comanda.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = comanda.FechaCreacion,
                FechaFin = comanda.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia
            };

            var articulos = RepositorioComandas.SP_ObtenerResumenesArticulosTicketComanda(comanda.Id);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(comanda.IdEmpleadoCobro ?? 0, comanda.IdUsuarioModifico);

            if (filtros == null)
                articulos = articulos.Where(m => m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();
            else
                articulos = articulos.Where(m => filtros.Contains(m.Codigo) && m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = 0;
            resumen.Cortesia = articulos.Where(m => m.EsCortesia).Sum(m => m.Total);
            resumen.Total = resumen.SubTotal - resumen.Cortesia - resumen.Descuentos;

            return resumen;
        }
        private DtoResumenVenta ObtenerResumen(OrdenRestaurante orden, List<string> filtros, Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Id == orden.IdOcupacionMesa, m => m.Mesa);

            var procedencia = "Mesa " + ocupacion.Mesa.Clave;//ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(ocupacion.Transaccion);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(ocupacion.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = orden.FechaCreacion,
                FechaFin = orden.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia
            };

            var articulos = RepositorioOrdenesRestaurantes.SP_ObtenerResumenesArticulosTicketOrdenRestaurante(orden.Id);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ocupacion.IdMesero, orden.IdUsuarioModifico);

            if (filtros == null)
                articulos = articulos.Where(m => m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();
            else
                articulos = articulos.Where(m => filtros.Contains(m.Codigo) && m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = 0;
            resumen.Cortesia = articulos.Where(m => m.EsCortesia).Sum(m => m.Total);
            resumen.Total = resumen.SubTotal - resumen.Cortesia - resumen.Descuentos;

            return resumen;
        }
        private DtoResumenVenta ObtenerResumenOrdenRestaurante(int idOrden, List<string> filtros, Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase)
        {
            var orden = RepositorioOrdenesRestaurantes.Obtener(m => m.Id == idOrden);
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Id == orden.IdOcupacionMesa, m => m.Mesa);

            var procedencia = "Mesa " + ocupacion.Mesa.Clave;//ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(ocupacion.Transaccion);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(ocupacion.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = orden.FechaCreacion,
                FechaFin = orden.FechaModificacion,
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia
            };

            var articulos = RepositorioOrdenesRestaurantes.SP_ObtenerResumenesArticulosTicketOrdenRestaurante(orden.Id);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(ocupacion.IdMesero, orden.IdUsuarioModifico);

            if (filtros == null)
                articulos = articulos.Where(m => m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();
            else
                articulos = articulos.Where(m => filtros.Contains(m.Codigo) && m.IdLineaBase.HasValue && lineasBase.Contains((Modelo.Almacen.Entidades.Linea.LineasBase)m.IdLineaBase.Value)).ToList();

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = 0;
            resumen.Cortesia = articulos.Where(m => m.EsCortesia).Sum(m => m.Total);
            resumen.Total = resumen.SubTotal - resumen.Cortesia - resumen.Descuentos;

            return resumen;
        }
        private DtoResumenVenta ObtenerResumen(int idMesa)
        {
            var configuracionG = ServicioParametros.ObtenerParametros();
            var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            var ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Activa && m.IdMesa == idMesa, m => m.Mesa);

            var procedencia = "Mesa " + ocupacion.Mesa.Clave;//ServicioRestaurantes.ObtenerNumeroMesaPorTransaccion(ocupacion.Transaccion);

            ocupacion = RepositorioOcupacionesMesa.Obtener(m => m.Activa && m.IdMesa == idMesa, m => m.OrdenesRestaurante);

            var preventa = ServicioPreventas.ObtenerPreventaPorId(ocupacion.IdPreventa);

            var resumen = new DtoResumenVenta
            {
                Direccion = configuracionG.Direccion,
                FechaInicio = ocupacion.OrdenesRestaurante.Where(m => m.Estado == Comanda.Estados.PorPagar).Select(m => m.FechaCreacion).OrderBy(m => m).FirstOrDefault(),
                FechaFin = ocupacion.OrdenesRestaurante.Where(m => m.Estado == Comanda.Estados.PorPagar).Select(m => m.FechaCreacion).OrderByDescending(m => m).FirstOrDefault(),
                Hotel = configuracionG.Nombre,
                RazonSocial = datosFiscales.RazonSocial,
                RFC = datosFiscales.RFC_Cte,
                Ticket = preventa.Ticket,
                Procedencia = procedencia
            };

            List<DtoArticuloTicket> articulos = new List<DtoArticuloTicket>();

            foreach (var orden in ocupacion.OrdenesRestaurante.Where(m => m.Estado == Comanda.Estados.PorPagar))
                articulos.AddRange(RepositorioOrdenesRestaurantes.SP_ObtenerResumenesArticulosTicketOrdenRestaurante(orden.Id));

            articulos = (from art in articulos
                         group art by new { PrecioUnidad = art.PrecioUnidad, art.Codigo, art.EsCortesia } into ar
                         select ar).ToList().Select(m => new DtoArticuloTicket
                                           {
                                               Cantidad = m.Sum(x => x.Cantidad),
                                               Codigo = m.Key.Codigo,
                                               EsCortesia = m.Key.EsCortesia,
                                               IdLineaBase = m.First().IdLineaBase,
                                               Nombre = m.First().Nombre,
                                               PrecioUnidad = m.Key.PrecioUnidad
                                           }).ToList();

            List<int> idsEmpleados = ocupacion.OrdenesRestaurante.Where(m => m.Estado == Comanda.Estados.PorPagar).Select(m => m.IdUsuarioModifico).ToList();
            idsEmpleados.Add(ocupacion.IdMesero);

            resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(idsEmpleados.ToArray());

            resumen.Items = articulos.Select(m => new DtoItemCobrable
            {
                Cantidad = m.Cantidad,
                Nombre = m.Nombre,
                PrecioUnidad = m.PrecioUnidad
            }).ToList();

            resumen.SubTotal = resumen.Items.Sum(m => m.Total);
            resumen.Descuentos = 0;
            resumen.Cortesia = articulos.Where(m => m.EsCortesia).Sum(m => m.Total);
            resumen.Total = resumen.SubTotal - resumen.Cortesia - resumen.Descuentos;

            return resumen;
        }
        //private DtoResumenVenta ObtenerResumen(OrdenTaxi orden)
        //{
        //    var configuracionG = ServicioParametros.ObtenerParametros();

        //    var preventa = ServicioPreventas.ObtenerPreventaPorId(orden.IdPreventa);

        //    var resumen = new DtoResumenVenta
        //    {
        //        Direccion = configuracionG.Direccion,
        //        FechaInicio = orden.FechaCreacion,
        //        FechaFin = orden.FechaModificacion,
        //        Hotel = configuracionG.Nombre,
        //        RazonSocial = configuracionG.RazonSocial,
        //        RFC = configuracionG.RFC,
        //        Ticket = preventa.Ticket//venta.Ticket.ToString(),
        //    };

        //    resumen.SubTotal = orden.Precio;
        //    resumen.Descuentos = 0;
        //    resumen.Total = orden.Precio;

        //    resumen.Empleados = RepositorioEmpleados.ObtenerNombreEmpleadoPuestoPorIdsConInactivos(orden.IdEmpleadoCobro ?? 0, orden.IdUsuarioModifico);

        //    resumen.Items.Add(new DtoItemCobrable
        //    {
        //        Cantidad = 1,
        //        Nombre = "Taxi",
        //        PrecioUnidad = orden.Precio,
        //    });

        //    return resumen;
        //}

        private void ImprimirTicket(DtoResumenVenta resumen, string nombreImpresora, int copias)
        {
            try
            {
                ImpresoraTickets.ImprimirTicket(resumen, nombreImpresora, copias);
            }
            catch (Exception ex)
            {
                Transversal.Log.Logger.Error("Error de impresión: " + ex.Message);
            }
            //var documentoReporte = new FormatoTicketGeneral();
            //documentoReporte.Load();

            //documentoReporte.SetDataSource(new List<DtoResumenVenta> { resumen });

            //documentoReporte.Subreports["Conceptos"].SetDataSource(resumen.Items);
            //documentoReporte.Subreports["Empleados"].SetDataSource(resumen.Empleados);

            //documentoReporte.PrintOptions.PrinterName = nombreImpresora;

            ////Si le paso directo el número de copias al método solamente me imprime una :S
            //for (int i = 0; i < copias; i++)
            //    documentoReporte.PrintToPrinter(1, false, 0, 0);
        }


        public void ImprimirTicketComandaCancelada(int idComanda, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenComandaCancelada(idComanda);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketConsumoInternoCancelado(int idConsumoInterno, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenConsumoInternoCancelado(idConsumoInterno);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketOcupacionMesaCancelada(int idOCupacionMesa, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenOcupacionMesaCancelada(idOCupacionMesa);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketOrdenTaxi(int idOrdenTaxi, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenOrdenTaxi(idOrdenTaxi);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }

        public void ImprimirTicketVentaRentaCancelada(int idVentaRenta, string nombreImpresora, int numeroCopias)
        {
            var resumen = ServicioVentas.ObtenerResumenVentaRentaCancelada(idVentaRenta);
            //await Task.Factory.StartNew(() =>
            //{
            ImprimirTicket(resumen, nombreImpresora, numeroCopias);
            //});
        }
    }
}
