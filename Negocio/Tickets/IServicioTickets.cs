﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Tickets
{
    public interface IServicioTickets
    {
        void ImprimirTicketVentaRenta(int idVentaRenta, string nombreImpresora, int numeroCopias);
        void ImprimirTicketComanda(int idComanda, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase);
        void ImprimirTicketOrdenRestaurante(int idOrden, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase);
        void ImprimirTicketConsumoInterno(int idConsumoInterno, bool esComanda, string nombreImpresora, int numeroCopias, List<string> filtros, params Modelo.Almacen.Entidades.Linea.LineasBase[] lineasBase);

        void ImprimirTicketVenta(int idVentaRenta, string nombreImpresora, int numeroCopias);

        void ImprimirTicketComandaCancelada(int idComanda, string nombreImpresora, int numeroCopias);

        void ImprimirTicketConsumoInternoCancelado(int idConsumoInterno, string nombreImpresora, int numeroCopias);

        void ImprimirTicketOcupacionMesaCancelada(int idOCupacionMesa, string nombreImpresora, int numeroCopias);

        void ImprimirTicketOrdenTaxi(int idOrdenTaxi, string nombreImpresora, int numeroCopias);

        void ImprimirTicketVentaRentaCancelada(int idVentaRenta, string nombreImpresora, int numeroCopias);
    }
}
