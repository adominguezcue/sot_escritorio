﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Seguridad.Entidades
{
    public partial class Credencial
    {
        public TiposCredencial TipoCredencial
        {
            get { return (TiposCredencial)IdTipoCredencial; }
            set { IdTipoCredencial = (int)value; }
        }

        [DataContract]
        public enum TiposCredencial 
        { 
            [EnumMember]
            Contrasena = 1,
            [EnumMember]
            Huella = 2
        }
    }
}
