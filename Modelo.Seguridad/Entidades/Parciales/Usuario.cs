﻿using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Modelo.Seguridad.Entidades
{
    public partial class Usuario
    {
        public Rol RolTmp { get; set; }

        public static explicit operator DtoUsuario(Usuario item)
        {
            if (item == null)
                return null;

            var dtoUserData = new DtoUsuario()
            {
                IdUsuario = item.Id,
                Alias = item.Alias,
                //NombreCompleto = item.Nombre + " " + item.ApellidoPaterno + " " + item.ApellidoMaterno,
                //Nombre = item.Nombre,
                IdRol = item.RolTmp!= null ? item.RolTmp.Id : 0,
                //ApellidoPaterno = item.ApellidoPaterno,
                //ApellidoMaterno = item.ApellidoMaterno,
                Activo = item.Activo,
                Id = item.IdEmpleado
            };

            if (item.RolTmp != null)
            {
                dtoUserData.NombreRol = item.RolTmp.Nombre;
                dtoUserData.Permisos = Newtonsoft.Json.JsonConvert.DeserializeObject<DtoPermisos>(item.RolTmp.Permisos);
                dtoUserData.Pagos = item.RolTmp.PermisosPagos.Where(m => m.Activo).Select(m => m.IdFormaPago).ToList();
            }
            else
            {
                dtoUserData.NombreRol = "";
                dtoUserData.Permisos = new DtoPermisos();
                dtoUserData.Pagos = new List<int>();
            }
            if (item.Sesiones.Count > 0)
            {
                dtoUserData.InicioSesion = item.Sesiones.Last().FechaCreacion;
                dtoUserData.TokenSesion = item.Sesiones.Last().Token;
                //dtoUserData.CaducidadSesion = item.Sesiones.Last().FechaCaducidad;
            }


            return dtoUserData;

        }

    }
}
