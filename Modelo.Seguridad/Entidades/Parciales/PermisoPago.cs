﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Seguridad.Entidades
{
    public partial class PermisoPago
    {
        public TiposPago FormaPago 
        {
            get { return (TiposPago)IdFormaPago; }
            set { IdFormaPago = (int)value; }
        }
    }
}
