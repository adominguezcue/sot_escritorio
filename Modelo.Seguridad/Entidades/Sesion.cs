//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Dominio.Nucleo.Utilidades;
using System.Linq.Expressions;
using Dominio.Nucleo.Entidades;

#pragma warning disable 1591 // this is for supress no xml comments in public members warnings 


namespace Modelo.Seguridad.Entidades
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Usuario))]
    [System.CodeDom.Compiler.GeneratedCode("STE-EF",".NET 4.0")]
    #if !SILVERLIGHT
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage()]
    #endif
    public partial class Sesion: IEntidad, INotifyPropertyChanged
    {
        public static readonly string TABLE_NAME = "Sesiones";
    
    	public string TableName
        {
            get
            {
                return TABLE_NAME;
            }
        }
    
    
        //public Expression<Func<Sesion, bool>> Comparador()
        //{
            //return m => m.Id == Id;
        //}
    
    	public EntidadEstados EntidadEstado { get; set; }
    
        public List<IEntidad> ObtenerPropiedadesNavegacion() 
        {
            List<IEntidad> propiedades = new List<IEntidad>();
    
            if(Usuario != null)
                propiedades.Add(Usuario);
    
            if(Usuario != null)
                Usuario.ObtenerPropiedadesNavegacion(propiedades);
    
    		propiedades.Remove(this);
    
            return propiedades;
        }
    
        public void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT)
        {
    		List<IEntidad> propiedades = new List<IEntidad>();
            if(Usuario != null && !propiedadesT.Contains(Usuario))
                propiedades.Add(Usuario);
    		propiedadesT.AddRange(propiedades);
    
            foreach(var item in propiedades)
                if(item != null)
                    item.ObtenerPropiedadesNavegacion(propiedadesT);
    
        }
    
        #region Constructor
    	public Sesion()
    	{
    		this._propertyChanged += EsCambio;
    	}

        #endregion

        #region ISeguimientoCambios
    	private List<string> CambiosRealizados;	
    	public void EsCambio(object sender, PropertyChangedEventArgs e)
    	{
    		if(CambiosRealizados == null)
    			CambiosRealizados = new List<string>();
    
    		if(!CambiosRealizados.Contains(e.PropertyName))
    			CambiosRealizados.Add(e.PropertyName);
    	}
    	public IEnumerable<string> ObtenerPropiedadesCambiadas()
        {
            return this.CambiosRealizados;
        }

        #endregion

        #region Copia y sincronización de primitivas
    
        public Sesion ObtenerCopiaDePrimitivas()
        {
            return new Sesion
            {
                Token = this.Token,
                IdUsuario = this.IdUsuario,
                FechaCreacion = this.FechaCreacion,
                FechaEliminacion = this.FechaEliminacion,
                Activo = this.Activo,
            };
        }
    
        public void SincronizarPrimitivas(Sesion origen)
        {
            this.Token = origen.Token;
            this.IdUsuario = origen.IdUsuario;
            this.FechaCreacion = origen.FechaCreacion;
            this.FechaEliminacion = origen.FechaEliminacion;
            this.Activo = origen.Activo;
        }

        #endregion

        #region Primitive Properties
    	[Key]
        [DataMember]
        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnPropertyChanged("Id");
                }
            }
        }
        private int _id;
        [DataMember]
        public string Token
        {
            get { return _token; }
            set
            {
                if ((value == null && _token != value) || (value != null && _token != value.Trim()))
                {
                    _token = (value == null ? value : value.Trim());
                    OnPropertyChanged("Token");
                }
            }
        }
        private string _token;
        [DataMember]
        public int IdUsuario
        {
            get { return _idUsuario; }
            set
            {
                if (_idUsuario != value)
                {
                    if (!IsDeserializing)
                    {
                        if (Usuario != null && Usuario.Id != value)
                        {
                            Usuario = null;
                        }
                    }
                    _idUsuario = value;
                    OnPropertyChanged("IdUsuario");
                }
            }
        }
        private int _idUsuario;
        [DataMember]
        public System.DateTime FechaCreacion
        {
            get { return _fechaCreacion; }
            set
            {
                if (_fechaCreacion != value)
                {
                    _fechaCreacion = value;
                    OnPropertyChanged("FechaCreacion");
                }
            }
        }
        private System.DateTime _fechaCreacion;
        [DataMember]
        public Nullable<System.DateTime> FechaEliminacion
        {
            get { return _fechaEliminacion; }
            set
            {
                if (_fechaEliminacion != value)
                {
                    _fechaEliminacion = value;
                    OnPropertyChanged("FechaEliminacion");
                }
            }
        }
        private Nullable<System.DateTime> _fechaEliminacion;
        [DataMember]
        public bool Activo
        {
            get { return _activo; }
            set
            {
                if (_activo != value)
                {
                    _activo = value;
                    OnPropertyChanged("Activo");
                }
            }
        }
        private bool _activo;

        #endregion

        #region Navigation Properties
    
        [DataMember]
        public Usuario Usuario
        {
            get { return _usuario; }
            set
            {
                if (!ReferenceEquals(_usuario, value))
                {
                    var previousValue = _usuario;
                    _usuario = value;
                    FixupUsuario(previousValue);
                    OnNavigationPropertyChanged("Usuario");
                }
            }
        }
        private Usuario _usuario;

        #endregion

        #region ChangeTracking
    
        protected virtual void OnPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        protected virtual void OnNavigationPropertyChanged(String propertyName)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    
        public event PropertyChangedEventHandler /*INotifyPropertyChanged.*/PropertyChanged{ add { _propertyChanged += value; } remove { _propertyChanged -= value; } }
        private event PropertyChangedEventHandler _propertyChanged;
    
    
        protected bool IsDeserializing { get; private set; }
    
        [OnDeserializing]
        public void OnDeserializingMethod(StreamingContext context)
        {
            IsDeserializing = true;
        }
    
        [OnDeserialized]
        public void OnDeserializedMethod(StreamingContext context)
        {
            IsDeserializing = false;
        }
    
        protected virtual void ClearNavigationProperties()
        {
            Usuario = null;
        }

        #endregion

        #region Association Fixup
    
        private void FixupUsuario(Usuario previousValue)
        {
            if (IsDeserializing)
            {
                return;
            }
    
            if (previousValue != null && previousValue.Sesiones.Contains(this))
            {
                previousValue.Sesiones.Remove(this);
            }
    
            if (Usuario != null)
            {
                if (!Usuario.Sesiones.Contains(this))
                {
                    Usuario.Sesiones.Add(this);
                }
    
                IdUsuario = Usuario.Id;
            }
        }

        #endregion

    }
}
