﻿using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Seguridad.Dtos
{
    public class DtoUsuario
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }

        public bool Activo { get; set; }

        public string NombreCompleto { get; set; }

        public string Alias { get; set; }

        //public string ApellidoPaterno { get; set; }

        //public string ApellidoMaterno { get; set; }

        //public string Nombre { get; set; }

        public int IdRol { get; set; }

        public string NombreRol { get; set; }

        public DtoPermisos Permisos { get; set; }

        public DateTime InicioSesion { get; set; }

        public string TokenSesion { get; set; }

        //public int IdEmpleado { get; set; }

        public List<int> Pagos { get; set; }
    }
}
