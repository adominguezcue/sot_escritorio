﻿using Modelo.Seguridad.Entidades;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Seguridad.Dtos
{
    //[DataContract]
    public class DtoCredencial
    {
        [JsonConverter(typeof(StringEnumConverter))]
        //[DataMember]
        public Credencial.TiposCredencial TipoCredencial { get; set; }
        //[DataMember]
        public string IdentificadorUsuario { get; set; }
        //[DataMember]
        public string ValorVerificarStr { get; set; }
        //[DataMember]
        public byte[] ValorVerificarBin { get; set; }
        //[DataMember]
        public bool SinValor { get { return ValorVerificarStr == null && ValorVerificarBin == null; } }
    }
}
