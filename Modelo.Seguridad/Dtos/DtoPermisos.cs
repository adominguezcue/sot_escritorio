﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Seguridad.Dtos
{
    public partial class DtoPermisos
    {
        public int IdRol { get; set; }
        #region Módulos

        [Category("Módulos")]
        [Description("Módulo de hotel")]
        public bool AccesoModuloHotel { get; set; }
        [Category("Módulos")]
        [Description("Módulo de restaurante")]
        public bool AccesoModuloRestaurante { get; set; }
        [Category("Módulos")]
        [Description("Módulo de cocina")]
        public bool AccesoModuloCocina { get; set; }
        [Category("Módulos")]
        [Description("Módulo de bar")]
        public bool AccesoModuloBar { get; set; }

        #endregion
        #region Tipos de habitaciones

        [Category("Tipos de habitación")]
        [Description("Consultar tipos de habitación")]
        public bool ConsultarTiposHabitacion { get; set; }
        [Category("Tipos de habitación")]
        [Description("Crear tipos de habitación")]
        public bool CrearTiposHabitacion { get; set; }
        [Category("Tipos de habitación")]
        [Description("Modificar tipos de habitación")]
        public bool ModificarTiposHabitacion { get; set; }
        [Category("Tipos de habitación")]
        [Description("Eliminar tipos de habitación")]
        public bool EliminarTiposHabitacion { get; set; }

        #endregion
        #region Habitaciones

        [Category("Habitaciones")]
        [Description("Consultar todas las habitaciones")]
        public bool ConsultarHabitacionesTodas { get; set; }
        [Category("Habitaciones")]
        [Description("Consultar las habitaciones habilitadas")]
        public bool ConsultarHabitacionesHabilitadas { get; set; }
        [Category("Habitaciones")]
        [Description("Consultar las habitaciones preparadas")]
        public bool ConsultarHabitacionesPreparadas { get; set; }
        [Category("Habitaciones")]
        [Description("Administrar habitaciones")]
        public bool AdministrarHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Modificar habitaciones")]
        public bool ModificarHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Asignar habitaciones")]
        public bool AsignarHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Cobrar habitaciones")]
        public bool CobrarHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Mandar habitaciones a limpieza")]
        public bool PonerHabitacionesLimpieza { get; set; }
        [Category("Habitaciones")]
        [Description("Mandar habitaciones a supervisión")]
        public bool PonerHabitacionesSupervision { get; set; }
        [Category("Habitaciones")]
        [Description("Desocupar habitaciones")]
        public bool DesocuparHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Limpiar habitaciones")]
        public bool LimpiarHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Supervisar habitaciones")]
        public bool SupervisarHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Cancelar ocupación de habitaciones")]
        public bool CancelarOcupacionesHabitacion { get; set; }
        [Category("Habitaciones")]
        [Description("Bloquear habitaciones")]
        public bool BloquearHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Desbloquear habitaciones")]
        public bool DesbloquearHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Habilitar habitaciones para ventas")]
        public bool HabilitarVentasHabitacion { get; set; }
        [Category("Habitaciones")]
        [Description("Consultar detalles habitaciones")]
        public bool ConsultarDetallesHabitacion { get; set; }
        [Category("Habitaciones")]
        [Description("Preparar habitaciones")]
        public bool PrepararHabitaciones { get; set; }
        [Category("Habitaciones")]
        [Description("Marcar habitaciones como reservadas")]
        public bool MarcarHabitacionComoReservada { get; set; }

        #endregion
        #region Paquetes

        [Category("Paquetes")]
        [Description("Asignar paquetes")]
        public bool AsignarPaquetes { get; set; }
        [Category("Paquetes")]
        [Description("Cobrar paquetes")]
        public bool CobrarPaquetes { get; set; }
        [Category("Paquetes")]
        [Description("Crear paquetes")]
        public bool CrearPaquetes { get; set; }
        [Category("Paquetes")]
        [Description("Modificar paquetes")]
        public bool ModificarPaquetes { get; set; }
        [Category("Paquetes")]
        [Description("Eliminar paquetes")]
        public bool EliminarPaquetes { get; set; }
        [Category("Paquetes")]
        [Description("Consultar paquetes")]
        public bool ConsultarPaquetes { get; set; }

        #endregion
        #region Personas extra

        [Category("Personas extra")]
        [Description("Asignar personas extra")]
        public bool AsignarPersonasExtra { get; set; }
        [Category("Personas extra")]
        [Description("Asignar personas extra arriba del máximo")]
        public bool AsignarPersonasExtraSobreMax { get; set; }
        [Category("Personas extra")]
        [Description("Asignar personas extra después de la entrada")]
        public bool AsignarPersonaExtraPosterior { get; set; }
        [Category("Personas extra")]
        [Description("Cobrar personas extra")]
        public bool CobrarPersonasExtra { get; set; }

        #endregion
        #region Horas extra
        [Category("Horas extra")]
        [Description("Asignar horas extra")]
        public bool AsignarHorasExtra { get; set; }

        #endregion
        #region Reservaciones

        [Category("Reservaciones")]
        [Description("Crear reservaciones")]
        public bool CrearReservaciones { get; set; }
        [Category("Reservaciones")]
        [Description("Consultar reservaciones")]
        public bool ConsultarReservaciones { get; set; }
        [Category("Reservaciones")]
        [Description("Modificar reservaciones")]
        public bool ModificarReservaciones { get; set; }
        [Category("Reservaciones")]
        [Description("Eliminar reservaciones")]
        public bool EliminarReservaciones { get; set; }
        [Category("Reservaciones")]
        [Description("Cambiar la habitación de una reservación")]
        public bool CambiarHabitacionReservacion { get; set; }
        [Category("Reservaciones")]
        [Description("Asignar reservaciones")]
        public bool AsignarReservaciones { get; set; }
        [Category("Reservaciones")]
        [Description("Eliminar el anclaje de una reservación")]
        public bool EliminarAnclajeReservaciones { get; set; }
        [Category("Reservaciones")]
        [Description("Permite Hacer Reservacion Mismo Día")]
        public bool PermiteReservacionMismoDia { get; set; }

        #endregion
        #region Comandas

        [Category("Comandas")]
        [Description("Crear comandas")]
        public bool CrearComandas { get; set; }
        [Category("Comandas")]
        [Description("Consultar comandas")]
        public bool ConsultarComandas { get; set; }
        [Category("Comandas")]
        [Description("Modificar comandas")]
        public bool ModificarComandas { get; set; }
        [Category("Comandas")]
        [Description("Eliminar comandas")]
        public bool EliminarComandas { get; set; }
        [Category("Comandas")]
        [Description("Meserear")]
        public bool Meserear { get; set; }
        [Category("Comandas")]
        [Description("Preparar productos")]
        public bool PrepararProductos { get; set; }
        [Category("Comandas")]
        [Description("Cobrar comandas")]
        public bool CobrarComanda { get; set; }

        #endregion
        #region Limpieza

        [Category("Limpieza")]
        [Description("Asignar recamareras")]
        public bool AsignarRecamarerasLimpieza { get; set; }
        [Category("Limpieza")]
        [Description("Asignar supervisor")]
        public bool AsignarSupervisorLimpieza { get; set; }
        [Category("Limpieza")]
        [Description("Finalizar tarea de limpieza")]
        public bool FinalizarLimpieza { get; set; }
        [Category("Limpieza")]
        [Description("Consultar tareas de limpieza")]
        public bool ConsultarTareaLimpieza { get; set; }

        #endregion
        #region Usuarios

        [Category("Usuarios")]
        [Description("Crear usuarios")]
        public bool CrearUsuarios { get; set; }
        [Category("Usuarios")]
        [Description("Modificar usuarios")]
        public bool ModificarUsuarios { get; set; }
        [Category("Usuarios")]
        [Description("Consultar usuarios")]
        public bool ConsultarUsuarios { get; set; }
        [Category("Usuarios")]
        [Description("Eliminar usuarios")]
        public bool EliminarUsuarios { get; set; }

        #endregion
        #region Roles

        [Category("Roles")]
        [Description("Crear roles")]
        public bool CrearRoles { get; set; }
        [Category("Roles")]
        [Description("Modificar roles")]
        public bool ModificarRoles { get; set; }
        [Category("Roles")]
        [Description("Consultar roles")]
        public bool ConsultarRoles { get; set; }
        [Category("Roles")]
        [Description("Eliminar roles")]
        public bool EliminarRoles { get; set; }

        #endregion
        #region Permisos

        [Category("Permisos")]
        [Description("Crear permisos")]
        public bool CrearPermisos { get; set; }
        [Category("Permisos")]
        [Description("Modificar permisos")]
        public bool ModificarPermisos { get; set; }
        [Category("Permisos")]
        [Description("Consultar permisos")]
        public bool ConsultarPermisos { get; set; }
        [Category("Permisos")]
        [Description("Eliminar permisos")]
        public bool EliminarPermisos { get; set; }

        #endregion
        #region Empleados

        [Category("Empleados")]
        [Description("Crear empleados")]
        public bool CrearEmpleado { get; set; }
        [Category("Empleados")]
        [Description("Modificar empleados")]
        public bool ModificarEmpleado { get; set; }
        [Category("Empleados")]
        [Description("Consultar empleados")]
        public bool ConsultarEmpleado { get; set; }
        [Category("Empleados")]
        [Description("Eliminar empleados")]
        public bool EliminarEmpleado { get; set; }

        #endregion
        #region Incidencias de turno

        [Category("Incidencias de turno")]
        [Description("Crear incidencia de turno")]
        public bool CrearIncidenciaTurno { get; set; }
        [Category("Incidencias de turno")]
        [Description("Modificar incidencia de turno")]
        public bool ModificarIncidenciaTurno { get; set; }
        [Category("Incidencias de turno")]
        [Description("Cancelar incidencia de turno")]
        public bool CancelarIncidenciaTurno { get; set; }
        [Category("Incidencias de turno")]
        [Description("Eliminar incidencia de turno")]
        public bool EliminarIncidenciaTurno { get; set; }
        [Category("Incidencias de turno")]
        [Description("Consultar incidencia de turno")]
        public bool ConsultarIncidenciaTurno { get; set; }

        #endregion
        #region Órdenes de trabajo

        [Category("Órdenes de trabajo")]
        [Description("Crear orden de trabajo")]
        public bool CrearOrdenTrabajo { get; set; }
        [Category("Órdenes de trabajo")]
        [Description("Modificar orden de trabajo")]
        public bool ModificarOrdenTrabajo { get; set; }
        [Category("Órdenes de trabajo")]
        [Description("Eliminar orden de trabajo")]
        public bool EliminarOrdenTrabajo { get; set; }
        [Category("Órdenes de trabajo")]
        [Description("Consultar orden de trabajo")]
        public bool ConsultarOrdenTrabajo { get; set; }
        [Category("Órdenes de trabajo")]
        [Description("Autorizar orden de trabajo")]
        public bool AutorizarOrdenTrabajo { get; set; }

        #endregion
        #region Órdenes de compra

        [Category("Órdenes de compra")]
        [Description("Crear orden de compra")]
        public bool CrearOrdenCompra { get; set; }
        [Category("Órdenes de compra")]
        [Description("Modificar orden de compra")]
        public bool ModificarOrdenCompra { get; set; }
        [Category("Órdenes de compra")]
        [Description("Eliminar orden de compra")]
        public bool EliminarOrdenCompra { get; set; }
        [Category("Órdenes de compra")]
        [Description("Consultar orden de compra")]
        public bool ConsultarOrdenCompra { get; set; }
        [Category("Órdenes de compra")]
        [Description("Cerrar orden de compra")]
        public bool CerrarOrdenCompra { get; set; }

        #endregion
        #region Proveedores

        [Category("Proveedores")]
        [Description("Crear proveedores")]
        public bool CrearProveedor { get; set; }
        [Category("Proveedores")]
        [Description("Modificar proveedores")]
        public bool ModificarProveedor { get; set; }
        [Category("Proveedores")]
        [Description("Eliminar proveedores")]
        public bool EliminarProveedor { get; set; }
        [Category("Proveedores")]
        [Description("Consultar proveedores")]
        public bool ConsultarProveedor { get; set; }
        [Category("Proveedores")]
        [Description("Consultar pagos a proveedores")]
        public bool ConsultarPagosProveedores { get; set; }

        #endregion
        #region Inventarios

        [Category("Inventarios")]
        [Description("Crear inventarios")]
        public bool CrearInventario { get; set; }
        [Category("Inventarios")]
        [Description("Modificar inventarios")]
        public bool ModificarInventario { get; set; }
        [Category("Inventarios")]
        [Description("Eliminar inventarios")]
        public bool EliminarInventario { get; set; }
        [Category("Inventarios")]
        [Description("Consultar inventarios")]
        public bool ConsultarInventario { get; set; }
        [Category("Inventarios")]
        [Description("Dar salida sin inventario suficiente")]
        public bool SalidaSinInventario { get; set; }
        [Category("Inventarios")]
        [Description("Reabrir inventarios")]
        public bool ReabirInventario { get; set; }
        [Category("Inventarios")]
        [Description("Cerrar inventarios")]
        public bool CerrarInventario { get; set; }
        [Category("Inventarios")]
        [Description("Consulta Reporte De Inventario")]
        public bool ConsultarReporteInventario { get; set; }
        #endregion
        #region Fichas

        [Category("Fichas")]
        [Description("Crear fichas")]
        public bool CrearFichas { get; set; }
        [Category("Fichas")]
        [Description("Modificar fichas")]
        public bool ModificarFichas { get; set; }
        [Category("Fichas")]
        [Description("Eliminar fichas")]
        public bool EliminarFichas { get; set; }
        [Category("Fichas")]
        [Description("Consultar fichas")]
        public bool ConsultarFichas { get; set; }

        #endregion
        #region Intercambios entre almacenes

        [Category("Intercambios entre almacenes")]
        [Description("Crear intercambio entre almacenes")]
        public bool CrearIntercambiosAlmacenes { get; set; }
        [Category("Intercambios entre almacenes")]
        [Description("Modificar intercambio entre almacenes")]
        public bool ModificarIntercambiosAlmacenes { get; set; }
        [Category("Intercambios entre almacenes")]
        [Description("Eliminar intercambio entre almacenes")]
        public bool EliminarIntercambiosAlmacenes { get; set; }
        [Category("Intercambios entre almacenes")]
        [Description("Consultar intercambio entre almacenes")]
        public bool ConsultarIntercambiosAlmacenes { get; set; }

        #endregion
        #region Centros de costos

        [Category("Centros de costos")]
        [Description("Crear centros de costos")]
        public bool CrearCentroCostos { get; set; }
        [Category("Centros de costos")]
        [Description("Modificar centros de costos")]
        public bool ModificarCentroCostos { get; set; }
        [Category("Centros de costos")]
        [Description("Eliminar centros de costos")]
        public bool EliminarCentroCostos { get; set; }
        [Category("Centros de costos")]
        [Description("Consultar centros de costos")]
        public bool ConsultarCentroCostos { get; set; }

        #endregion
        #region Objetos olvidados

        [Category("Objetos olvidados")]
        [Description("Crear objetos olvidados")]
        public bool CrearObjetoOlvidado { get; set; }
        [Category("Objetos olvidados")]
        [Description("Modificar objetos olvidados")]
        public bool ModificarObjetoOlvidado { get; set; }
        [Category("Objetos olvidados")]
        [Description("Consultar objetos olvidados")]
        public bool ConsultarObjetoOlvidado { get; set; }
        [Category("Objetos olvidados")]
        [Description("Cancelar objetos olvidados")]
        public bool CancelarObjetoOlvidado { get; set; }
        [Category("Objetos olvidados")]
        [Description("Eliminar objetos olvidados")]
        public bool EliminarObjetoOlvidado { get; set; }

        #endregion
        #region Incidencias de habitación

        [Category("Incidencias de habitación")]
        [Description("Crear incidencias de habitación")]
        public bool CrearIncidenciaHabitacion { get; set; }
        [Category("Incidencias de habitación")]
        [Description("Consultar incidencias de habitación")]
        public bool ConsultarIncidenciaHabitacion { get; set; }
        [Category("Incidencias de habitación")]
        [Description("Cancelar incidencias de habitación")]
        public bool CancelarIncidenciaHabitacion { get; set; }
        [Category("Incidencias de habitación")]
        [Description("Eliminar incidencias de habitación")]
        public bool EliminarIncidenciaHabitacion { get; set; }
        [Category("Incidencias de habitación")]
        [Description("Modificar incidencias de habitación")]
        public bool ModificarIncidenciaHabitacion { get; set; }

        #endregion
        #region Gastos

        [Category("Gastos")]
        [Description("Crear gastos")]
        public bool CrearGastos { get; set; }
        [Category("Gastos")]
        [Description("Modificar gastos")]
        public bool ModificarGastos { get; set; }
        [Category("Gastos")]
        [Description("Consultar gastos")]
        public bool ConsultarGastos { get; set; }
        [Category("Gastos")]
        [Description("Eliminar gastos")]
        public bool EliminarGastos { get; set; }
        [Category("Gastos")]
        [Description("Crear conceptos de gastos")]
        public bool CrearConceptosGastos { get; set; }
        [Category("Gastos")]
        [Description("Modificar conceptos de gastos")]
        public bool ModificarConceptosGastos { get; set; }
        [Category("Gastos")]
        [Description("Eliminar conceptos de gastos")]
        public bool EliminarConceptosGastos { get; set; }
        [Category("Gastos")]
        [Description("Consultar conceptos de gastos")]
        public bool ConsultarConceptosGastos { get; set; }

        #endregion
        #region Fajillas

        [Category("Fajillas")]
        [Description("Crear fajillas")]
        public bool CrearFajillas { get; set; }
        [Category("Fajillas")]
        [Description("Consultar fajillas")]
        public bool ConsultarFajillas { get; set; }
        [Category("Fajillas")]
        [Description("Autorizar fajillas")]
        public bool AutorizarFajillas { get; set; }
        [Category("Fajillas")]
        [Description("Consultar configuración de fajillas")]
        public bool ConsultarConfiguracionFajillas { get; set; }
        [Category("Fajillas")]
        [Description("Modificar configuración de fajillas")]
        public bool ModificarConfiguracionFajillas { get; set; }

        #endregion
        #region Artículos

        [Category("Artículos")]
        [Description("Consultar tipos de artículos")]
        public bool ConsultarTiposArticulos { get; set; }
        [Category("Artículos")]
        [Description("Crear artículos")]
        public bool CrearArticulos { get; set; }
        [Category("Artículos")]
        [Description("Modificar artículos")]
        public bool ModificarArticulos { get; set; }
        [Category("Artículos")]
        [Description("Eliminar artículos")]
        public bool EliminarArticulos { get; set; }
        [Category("Artículos")]
        [Description("Consultar artículos")]
        public bool ConsultarArticulos { get; set; }

        #endregion
        #region Taxis

        [Category("Taxis")]
        [Description("Crear taxis")]
        public bool CrearTaxis { get; set; }
        [Category("Taxis")]
        [Description("Modificar taxis")]
        public bool ModificarTaxis { get; set; }
        [Category("Taxis")]
        [Description("Eliminar taxis")]
        public bool EliminarTaxis { get; set; }
        [Category("Taxis")]
        [Description("Consultar taxis")]
        public bool ConsultarTaxis { get; set; }
        [Category("Taxis")]
        [Description("Cobrar taxis")]
        public bool CobrarTaxis { get; set; }

        #endregion
        #region Asistencias

        [Category("Asistencias")]
        [Description("Crear asistencias")]
        public bool CrearAsistencias { get; set; }
        [Category("Asistencias")]
        [Description("Modificar asistencias")]
        public bool ModificarAsistencias { get; set; }
        [Category("Asistencias")]
        [Description("Eliminar asistencias")]
        public bool EliminarAsistencias { get; set; }
        [Category("Asistencias")]
        [Description("Consultar asistencias")]
        public bool ConsultarAsistencias { get; set; }
        [Category("Asistencias")]
        [Description("Justificar retardos")]
        public bool JustificarRetardoFalta { get; set; }

        #endregion
        #region Impresoras

        [Category("Impresoras")]
        [Description("Consultar configuración impresoras")]
        public bool ConsultarConfiguracionImpresoras { get; set; }
        [Category("Impresoras")]
        [Description("Modificar configuración impresoras")]
        public bool ModificarConfiguracionImpresoras { get; set; }

        #endregion
        #region Propinas

        [Category("Propinas")]
        [Description("Consultar configuración de propinas")]
        public bool ConsultarConfiguracionPropinas { get; set; }
        [Category("Propinas")]
        [Description("Modificar configuración de propinas")]
        public bool ModificarConfiguracionPropinas { get; set; }
        [Category("Propinas")]
        [Description("Consultar propinas")]
        public bool ConsultarPropinas { get; set; }

        #endregion
        #region Cortes de turno

        [Category("Cortes de turno")]
        [Description("Realizar cortes de turno")]
        public bool RealizarCorteTurno { get; set; }
        [Category("Cortes de turno")]
        [Description("Consultar datos de corte")]
        public bool ConsultarDatosCorte { get; set; }
        [Category("Cortes de turno")]
        [Description("Revisar cortes de turno")]
        public bool RevisarCorteTurno { get; set; }
        [Category("Cortes de turno")]
        [Description("Finalizar cortes de turno")]
        public bool FinalizarCorteTurno { get; set; }
        [Category("Cortes de turno")]
        [Description("Consultar ventas")]
        public bool ConsultarVentas { get; set; }
        [Category("Cortes de turno")]
        [Description("Consultar Consumos Internos Por Empleado")]
        public bool ConsultaConsumosInternosEmpleado { get; set; }
        [Category("Cortes de turno")]
        [Description("Consulta Cortesias y Cancelaciones")]
        public bool ConsultaCortesiaCancelaciones { get; set; }									 													   
        #endregion
        #region VPoints

        [Category("VPoints")]
        [Description("Gestionar putos de lealtad")]
        public bool GestionarPuntosLealtad { get; set; }
        [Category("VPoints")]
        [Description("Vender tarjetas de putos de lealtad")]
        public bool VenderTarjetasLealtad { get; set; }
        [Category("VPoints")]
        [Description("Canjear cupones")]
        public bool CanjearCupones { get; set; }
        [Category("VPoints")]
        [Description("Consultar cupones")]
        public bool ConsultarCupones { get; set; }
        [Category("VPoints")]
        [Description("Consultar saldo de tarjetas de putos de lealtad")]
        public bool ConsultarSaldoTarjetasLealtad { get; set; }
        [Category("VPoints")]
        [Description("Consultar tarjetas de putos de lealtad")]
        public bool ConsultarTarjetasLealtad { get; set; }
        [Category("VPoints")]
        [Description("Modificar tarjetas de putos de lealtad")]
        public bool ModificarTarjetasLealtad { get; set; }
        [Category("VPoints")]
        [Description("Eliminar tarjetas de putos de lealtad")]
        public bool EliminarTarjetasLealtad { get; set; }
        [Category("VPoints")]
        [Description("Crear tarjetas de putos de lealtad")]
        public bool CrearTarjetasLealtad { get; set; }

        #endregion
        #region Pagos

        [Category("Pagos")]
        [Description("Modificar pagos")]
        public bool ModificarPagos { get; set; }

        #endregion
        #region Datos fiscales

        [Category("Datos fiscales")]
        [Description("Crear datos fiscales de clientes")]
        public bool CrearDatosFiscalesClientes { get; set; }
        [Category("Datos fiscales")]
        [Description("Modificar datos fiscales de clientes")]
        public bool ModificarDatosFiscalesClientes { get; set; }
        [Category("Datos fiscales")]
        [Description("Consultar datos fiscales de clientes")]
        public bool ConsultarDatosFiscalesClientes { get; set; }
        [Category("Datos fiscales")]
        [Description("Vincular datos fiscales de clientes")]
        public bool VincularDatosFiscalesClientes { get; set; }

        [Category("Datos fiscales")]
        [Description("Crear datos fiscales")]
        public bool CrearDatosFiscales { get; set; }
        [Category("Datos fiscales")]
        [Description("Modificar datos fiscales")]
        public bool ModificarDatosFiscales { get; set; }
        [Category("Datos fiscales")]
        [Description("Eliminar datos fiscales")]
        public bool EliminarDatosFiscales { get; set; }
        [Category("Datos fiscales")]
        [Description("Consultar datos fiscales")]
        public bool ConsultarDatosFiscales { get; set; }

        #endregion
        #region Tareas de mantenimiento

        [Category("Tareas de mantenimiento")]
        [Description("Crear tarea de mantenimiento")]
        public bool CrearTareaMantenimiento { get; set; }
        [Category("Tareas de mantenimiento")]
        [Description("Modificar tarea de mantenimiento")]
        public bool ModificarTareaMantenimiento { get; set; }
        [Category("Tareas de mantenimiento")]
        [Description("Eliminar tarea de mantenimiento")]
        public bool EliminarTareaMantenimiento { get; set; }
        [Category("Tareas de mantenimiento")]
        [Description("Consultar tarea de mantenimiento")]
        public bool ConsultarTareaMantenimiento { get; set; }

        #endregion
        #region Mantenimiento

        [Category("Mantenimiento")]
        [Description("Finalizar mantenimiento")]
        public bool FinalizarMantenimiento { get; set; }
        [Category("Mantenimiento")]
        [Description("Iniciar mantenimiento")]
        public bool IniciarMantenimiento { get; set; }
        [Category("Mantenimiento")]
        [Description("Consultar mantenimientos")]
        public bool ConsultarMantenimientos { get; set; }
        [Category("Mantenimiento")]
        [Description("Consultar conceptos mantenimiento")]
        public bool ConsultarConceptosMantenimiento { get; set; }

        #endregion
        #region Puestos

        [Category("Puestos")]
        [Description("Consultar puestos")]
        public bool ConsultarPuestos { get; set; }
        [Category("Puestos")]
        [Description("Eliminar puestos")]
        public bool EliminarPuestos { get; set; }
        [Category("Puestos")]
        [Description("Modificar puestos")]
        public bool ModificarPuestos { get; set; }
        [Category("Puestos")]
        [Description("Crear puestos")]
        public bool CrearPuestos { get; set; }

        #endregion
        #region Etiquetas

        [Category("Etiquetas")]
        [Description("Crear etiquetas")]
        public bool CrearEtiquetas { get; set; }
        [Category("Etiquetas")]
        [Description("Modificar etiquetas")]
        public bool ModificarEtiquetas { get; set; }
        [Category("Etiquetas")]
        [Description("Eliminar etiquetas")]
        public bool EliminarEtiquetas { get; set; }
        [Category("Etiquetas")]
        [Description("Consultar etiquetas")]
        public bool ConsultarEtiquetas { get; set; }

        #endregion
        #region Renta

        [Category("Renta")]
        [Description("Modificar renta")]
        public bool ModificarRenta { get; set; }

        #endregion
        #region Restaurante

        [Category("Restaurante")]
        [Description("Abrir mesa")]
        public bool AbrirMesa { get; set; }
        [Category("Restaurante")]
        [Description("Cancelar ocupación")]
        public bool CancelarOcupacionesMesas { get; set; }
        [Category("Restaurante")]
        [Description("Crear órdenes de restaurante")]
        public bool CrearOrdenRestaurante { get; set; }
        [Category("Restaurante")]
        [Description("Modificar órdenes de restaurante")]
        public bool ModificarOrdenRestaurante { get; set; }
        [Category("Restaurante")]
        [Description("Eliminar órdenes de restaurante")]
        public bool EliminarOrdenRestaurante { get; set; }
        [Category("Restaurante")]
        [Description("Consultar mesas")]
        public bool ConsultarMesas { get; set; }
        [Category("Restaurante")]
        [Description("Consultar detalles mesa")]
        public bool ConsultarDetallesMesa { get; set; }
        [Category("Restaurante")]
        [Description("Cambiar mesa")]
        public bool CambiarMesa { get; set; }
        [Category("Restaurante")]
        [Description("Marcar mesa como pendiente de cobro")]
        public bool MarcarMesaPorCobrar { get; set; }
        [Category("Restaurante")]
        [Description("Entregar orden de restaurante")]
        public bool MarcarEntregadaOrden { get; set; }
        [Category("Restaurante")]
        [Description("Administrar mesas")]
        public bool AdministrarMesas { get; set; }
        #endregion
        #region Presentaciones

        [Category("Presentaciones")]
        [Description("Consultar presentaciones")]
        public bool ConsultarPresentaciones { get; set; }
        [Category("Presentaciones")]
        [Description("Crear presentaciones")]
        public bool CrearPresentaciones { get; set; }
        [Category("Presentaciones")]
        [Description("Modificar presentaciones")]
        public bool ModificarPresentaciones { get; set; }
        [Category("Presentaciones")]
        [Description("Eliminar presentaciones")]
        public bool EliminarPresentaciones { get; set; }

        #endregion
        #region Consumos internos

        [Category("Consumos internos")]
        [Description("Consultar consumos internos")]
        public bool ConsultarConsumosInternos { get; set; }
        [Category("Consumos internos")]
        [Description("Crear consumos internos")]
        public bool CrearConsumosInternos { get; set; }
        [Category("Consumos internos")]
        [Description("Modificar consumos internos")]
        public bool ModificarConsumosInternos { get; set; }
        [Category("Consumos internos")]
        [Description("Eliminar consumos internos")]
        public bool EliminarConsumosInternos { get; set; }
        [Category("Consumos internos")]
        [Description("Marcar consumos internos como entregados")]
        public bool MarcarEntregadoConsumoInterno { get; set; }

        #endregion
        #region Almacenes

        [Category("Almacenes")]
        [Description("Consultar almacenes")]
        public bool ConsultarAlmacenes { get; set; }
        [Category("Almacenes")]
        [Description("Modificar almacenes")]
        public bool ModificarAlmacenes { get; set; }
        [Category("Almacenes")]
        [Description("Crear almacenes")]
        public bool CrearAlmacenes { get; set; }
        [Category("Almacenes")]
        [Description("Eliminar almacenes")]
        public bool EliminarAlmacenes { get; set; }

        #endregion
        #region Nóminas

        [Category("Nóminas")]
        [Description("Crear nóminas")]
        public bool CrearNomina { get; set; }
        [Category("Nóminas")]
        [Description("Consultar nóminas")]
        public bool ConsultarNomina { get; set; }
        [Category("Nóminas")]
        [Description("Modificar nóminas")]
        public bool ModificarNomina { get; set; }
        [Category("Nóminas")]
        [Description("Eliminar nóminas")]
        public bool EliminarNomina { get; set; }

        #endregion
        #region Reportes de matrícula

        [Category("Reportes de matrícula")]
        [Description("Consultar reportes de matrícula")]
        public bool ConsultarReportesMatricula { get; set; }
        [Category("Reportes de matrícula")]
        [Description("Crear reportes de matrícula")]
        public bool CrearReportesMatricula { get; set; }
        [Category("Reportes de matrícula")]
        [Description("Eliminar reportes de matrícula")]
        public bool EliminarReportesMatricula { get; set; }

        #endregion
        #region Requisiciones

        [Category("Requisiciones")]
        [Description("Consultar requisiciones")]
        public bool ConsultarRequisiciones { get; set; }

        #endregion
        #region Sistema

        [Category("Sistema")]
        [Description("Respaldar base de datos")]
        public bool CrearRespaldosBaseDatos { get; set; }
        [Category("Sistema")]
        [Description("Consultar conceptos del sistema")]
        public bool ConsultarConceptosSistema { get; set; }
        [Category("Sistema")]
        [Description("Crear conceptos del sistema")]
        public bool CrearConceptosSistema { get; set; }
        [Category("Sistema")]
        [Description("Modificar conceptos del sistema")]
        public bool ModificarConceptosSistema { get; set; }
        [Category("Sistema")]
        [Description("Eliminar conceptos del sistema")]
        public bool EliminarConceptosSistema { get; set; }

        #endregion
        #region Cliente
        [Category("Cliente")]
        [Description("Consulta Datos Cliente")]
        public bool ConsultaClientesMatricula { get; set; }

        #endregion
        #region Solicitudes de permisos, faltas, ausencias, retardos y vacaciones

        [Category("Solicitudes de permisos, faltas, ausencias, retardos y vacaciones")]
        [Description("Crear solicitud")]
        public bool CrearSolicitudesPermisosFaltas { get; set; }
        [Category("Solicitudes de permisos, faltas, ausencias, retardos y vacaciones")]
        [Description("Consultar solicitud")]
        public bool ConsultarSolicitudesPermisosFaltas { get; set; }
        [Category("Solicitudes de permisos, faltas, ausencias, retardos y vacaciones")]
        [Description("Modificar solicitud")]
        public bool ModificarSolicitudesPermisosFaltas { get; set; }
        [Category("Solicitudes de permisos, faltas, ausencias, retardos y vacaciones")]
        [Description("Eliminar solicitud")]
        public bool EliminarSolicitudesPermisosFaltas { get; set; }

        #endregion
        #region recetas

        [Category("Recetas")]
        [Description("Consultar recetas")]
        public bool ConsultarRecetas { get; set; }
        [Category("Recetas")]
        [Description("Editar recetas")]
        public bool EditarRecetas { get; set; }
        #endregion
        #region Categorias

        [Category("Categorias")]
        [Description("Consultar categorias")]
        public bool ConsultarDepartamentos { get; set; }
        [Category("Categorias")]
        [Description("Crear categoría")]
        public bool CrearDepartamento { get; set; }
        [Category("Categorias")]
        [Description("Modificar categoría")]
        public bool ModificarDepartamento { get; set; }
        [Category("Categorias")]
        [Description("Eliminar categoría")]
        public bool EliminarDepartamento { get; set; }

        #endregion
        #region Subcategorias

        [Category("Subategorias")]
        [Description("Consultar subcategorias")]
        public bool ConsultarLineas { get; set; }
        [Category("Subategorias")]
        [Description("Crear subcategorias")]
        public bool CrearLinea { get; set; }
        [Category("Subategorias")]
        [Description("Modificar subcategorias")]
        public bool ModificarLinea { get; set; }
        [Category("Subategorias")]
        [Description("Eliminar subcategorias")]
        public bool EliminarLinea { get; set; }

        #endregion
        #region Presupuestos

        [Category("Presupuestos")]
        [Description("Consultar presupuestos")]
        public bool ConsultarPresupuestos { get; set; }
        [Category("Presupuestos")]
        [Description("Crear presupuestos")]
        public bool CrearPresupuestos { get; set; }
        [Category("Presupuestos")]
        [Description("Modificar presupuestos")]
        public bool ModificarPresupuestos { get; set; }
        [Category("Presupuestos")]
        [Description("Eliminar presupuestos")]
        public bool EliminarPresupuestos { get; set; }

        #endregion
        #region Otros

        [Category("Otros")]
        [Description("Recibir propinas")]
        public bool RecibirPropinas { get; set; }
        [Category("Otros")]
        [Description("Aparcar")]
        public bool Aparcar { get; set; }
        [Category("Otros")]
        [Description("Reimprimir recibos")]
        public bool ReimprimirRecibos { get; set; }

        #endregion
        #region Configuraciones de sucursal

        [Category("Configuraciones de sucursal")]
        [Description("Configurar los parámetros de los sistemas")]
        public bool ConfigurarParametrosSistema { get; set; }
        [Category("Configuraciones de sucursal")]
        [Description("Editar la configuración de VPoints o similares")]
        public bool EditarConfiguracionPuntosLealtad { get; set; }
        [Category("Configuraciones de sucursal")]
        [Description("Eliminar la configuración de VPoints o similares")]
        public bool EliminarConfiguracionPuntosLealtad { get; set; }
        [Category("Configuraciones de sucursal")]
        [Description("Configurar los turnos")]
        public bool ConfigurarTurnos { get; set; }

        #endregion

        public bool ConsultarCaja { get; set; }
        public bool AplicarDescuentoCaja { get; set; }
        public bool CancelarCaja { get; set; }

        //public bool CrearRequisiciones { get; set; }
    }
}
