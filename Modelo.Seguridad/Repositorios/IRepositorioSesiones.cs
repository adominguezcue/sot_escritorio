﻿using Dominio.Nucleo.Repositorios;
using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Seguridad.Repositorios
{
    public interface IRepositorioSesiones : IRepositorio<Sesion>
    {
    }
}
