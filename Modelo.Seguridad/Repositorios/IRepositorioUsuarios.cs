﻿using Dominio.Nucleo.Repositorios;
using Modelo.Seguridad.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo.Seguridad.Repositorios
{
    public interface IRepositorioUsuarios: IRepositorio<Usuario>
    {
        Usuario ObtenerUsuarioConRolCredencial(string alias, Credencial.TiposCredencial tiposCredencial);
        Usuario ObtenerUsuarioConRol(int idUsuario);
        Usuario ObtenerUsuarioConRolCredencial(int idEmpleado, Credencial.TiposCredencial tiposCredencial);

        List<Credencial> ObtenerCredencialesActivas(Credencial.TiposCredencial tiposCredencial);
    }
}
