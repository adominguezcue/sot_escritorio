﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.DtosPruebas
{
    public class DtoPermisosPrueba : Modelo.Seguridad.Dtos.DtoPermisos
    {
        public string Rol { get; set; }
    }
}
