﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Transversal.Utilidades;
using System.IO;
using Transversal.Extensiones;
using System.Data;
using Modelo.Seguridad.Entidades;
using Modelo.Seguridad.Dtos;
using System.Collections.Generic;

namespace UnitTestProject1.Extras
{
    [TestClass]
    public class UnitTestImportarPermisos
    {
        [TestMethod]
        public void ImportarPermisos()
        {
            var roles = ImportarYGenerarPermisos();
        }

        internal static List<Rol> ImportarYGenerarPermisos()
        {
            ExcelData.ExcelDataRequest request = new ExcelData.ExcelDataRequest();

            using (var stream = File.OpenText("PermisosCliente.xlsx"))
            {

                //request.NamedValues.Add(LocalResources.DiscountImportNames.typeEnum, null);

                request.DataSheetName = "Permisos";
                request.TableName = "TablaPermisos";

                //if (stream.Position > 0)
                //    stream.Position = 0;

                ExcelData.ReadExcel(stream.BaseStream, request);
            }

            request.Data.Columns.Add("IdRol");

            foreach (DataColumn c in request.Data.Columns)
            {
                foreach (DataRow r in request.Data.Rows)
                {
                    if (c.ColumnName == "IdRol")
                        r[c] = 0;
                    else if (c.ColumnName != "Rol")
                    {
                        if (r[c] != null && r[c] != DBNull.Value)
                            r[c] = true;
                        else
                            r[c] = false;

                    }
                }
            }

            var lista = request.Data.ToList<DtosPruebas.DtoPermisosPrueba>();
            var listaRoles = new List<Rol>();


            var fechaActual = DateTime.Now;

            foreach (var item in lista)
            {
                listaRoles.Add(new Rol
                {
                    Activo = true,
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Nombre = item.Rol,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject((DtoPermisos)item)
                });
            }

            return listaRoles;
        }
    }
}
