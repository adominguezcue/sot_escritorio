﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestSOT
{
    [TestClass]
    public class CXC
    {
        [TestMethod]
        public void TestCreateInstance()
        {
            Modelo.CXC cxc = new Modelo.CXC();
            Assert.IsInstanceOfType(cxc, typeof(Modelo.CXC));
        }
        [TestMethod]
        public void TestDataInstance()
        {
            Modelo.CXC cxc = new Modelo.CXC();
            //cxc.id = 0;
            cxc.customer_id = 1;
            cxc.order_id = 1;
            cxc.amount = 100.00;
            cxc.paid = 0.00;
            cxc.type_of_payment_id = 1;
            cxc.create_date = DateTime.Now;
            cxc.due_date = DateTime.Now.AddDays(5);
        }
    }
}
