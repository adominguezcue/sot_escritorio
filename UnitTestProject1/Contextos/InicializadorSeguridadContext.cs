﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modelo.Seguridad.Repositorios;
using Transversal.Dependencias;
using Transversal.Extensiones;
using Modelo.Seguridad.Entidades;
using System.Transactions;
using Negocio.Seguridad.Usuarios;
using Modelo.Seguridad.Dtos;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Modelo.Entidades;
using Dominio.Nucleo.Entidades;
using Modelo.Repositorios;
using UnitTestProject1.Extras;

namespace UnitTestProject1.Contextos
{
    [TestClass]
    public class InicializadorSeguridadContext
    {
        //[TestMethod]
        //public void Inicializar()
        //{
        //    Init(0);
        //}

        internal void Init(List<Usuario> usuarios)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                IRepositorioRoles _RepositorioRoles = FabricaDependencias.Instancia.Resolver<IRepositorioRoles>();

                var enumPagos = Enum.GetValues(typeof(TiposPago)).Cast<int>().ToList();

                var fechaActual = DateTime.Now;

                DtoPermisos referencia;

                #region rol sistemas

                referencia = new DtoPermisos();
                referencia.AsignarValorABooleanos(true);

                var rolSistemas = new Rol
                {
                    Activo = true,
                    Nombre = "SISTEMAS",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia),
                };

                foreach (var tipoPago in enumPagos) 
                {
                    rolSistemas.PermisosPagos.Add(new Modelo.Seguridad.Entidades.PermisoPago
                    {
                        Activo = true,
                        IdFormaPago = tipoPago,
                        FechaCreacion = fechaActual,
                        FechaModificacion = fechaActual
                    });
                }

                _RepositorioRoles.Agregar(rolSistemas);
                _RepositorioRoles.GuardarCambios();
                #endregion
#if !DEBUG_TEST_CLIENTE

                #region rol recepcionista
                referencia = new DtoPermisos
                {
                    AccesoModuloHotel = true,
                    AccesoModuloRestaurante = true,
                    ConsultarHabitacionesTodas = true,
                    ConsultarHabitacionesHabilitadas = true,
                    ConsultarHabitacionesPreparadas = true,
                    AdministrarHabitaciones = true,
                    ModificarHabitaciones = true,
                    AsignarHabitaciones = true,
                    CobrarHabitaciones = true,
                    PonerHabitacionesLimpieza = true,
                    PonerHabitacionesSupervision = true,
                    DesocuparHabitaciones = true,
                    AsignarPaquetes = true,
                    CobrarPaquetes = true,
                    AsignarPersonasExtra = true,
                    CobrarPersonasExtra = true,
                    CambiarHabitacionReservacion = true,
                    ConsultarReservaciones = true,
                    AsignarReservaciones = true,
                    EliminarAnclajeReservaciones = true,
                    CrearComandas = true,
                    ModificarComandas = true,
                    ConsultarComandas = true,
                    EliminarComandas = true,
                    CancelarOcupacionesHabitacion = true,
                    RecibirPropinas = true,
                };

                var rolRecepcionista = new Rol
                {
                    Activo = true,
                    Nombre = "RECEPCIONISTA",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia)
                    
                };

                foreach (var tipoPago in enumPagos)
                {
                    rolRecepcionista.PermisosPagos.Add(new Modelo.Seguridad.Entidades.PermisoPago
                    {
                        Activo = true,
                        IdFormaPago = tipoPago,
                        FechaCreacion = fechaActual,
                        FechaModificacion = fechaActual
                    });
                }

                _RepositorioRoles.Agregar(rolRecepcionista);
                _RepositorioRoles.GuardarCambios();
                #endregion
                #region rol vendedor
                referencia = new DtoPermisos
                {
                    AccesoModuloHotel = true,
                    ConsultarHabitacionesHabilitadas = true,
                    ConsultarHabitacionesPreparadas = true,
                    AsignarHabitaciones = true,
                    AsignarPaquetes = true,
                    AsignarPersonasExtra = true,
                    PrepararHabitaciones = true,
                    RecibirPropinas = true,
                };


                var rolVendedor = new Rol
                {
                    Activo = true,
                    Nombre = "VENDEDOR",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia)
                    
                };

                _RepositorioRoles.Agregar(rolVendedor);
                _RepositorioRoles.GuardarCambios();
                #endregion
                #region rol gerente operativo

                referencia = new DtoPermisos
                {
                    AccesoModuloHotel = true,
                    AccesoModuloRestaurante = true,
                    ConsultarHabitacionesTodas = true,
                    ConsultarHabitacionesHabilitadas = true,
                    ConsultarHabitacionesPreparadas = true,
                    CrearReservaciones = true,
                    ConsultarReservaciones = true,
                    ModificarReservaciones = true,
                    EliminarReservaciones = true,
                    AsignarReservaciones = true,
                };
                var rolGerenteOperativo = new Rol
                {
                    Activo = true,
                    Nombre = "GERENTEOP",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia)
                };

                _RepositorioRoles.Agregar(rolGerenteOperativo);
                _RepositorioRoles.GuardarCambios();
                #endregion
                #region rol mesero

                referencia = new DtoPermisos
                {
                    Meserear = true,
                    RecibirPropinas = true,
                };
                var rolMesero = new Rol
                {
                    Activo = true,
                    Nombre = "MESERO",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia)
                };

                _RepositorioRoles.Agregar(rolMesero);
                _RepositorioRoles.GuardarCambios();
                #endregion
                #region rol cocinero

                referencia = new DtoPermisos
                {
                    PrepararProductos = true
                };
                var rolCocinero = new Rol
                {
                    Activo = true,
                    Nombre = "COCINERO",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia)
                };

                _RepositorioRoles.Agregar(rolCocinero);
                _RepositorioRoles.GuardarCambios();
                #endregion
                #region rol valet

                referencia = new DtoPermisos
                {
                    Aparcar = true,
                    RecibirPropinas = true,
                };
                var rolValet = new Rol
                {
                    Activo = true,
                    Nombre = "VALET",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia)
                };

                _RepositorioRoles.Agregar(rolValet);
                _RepositorioRoles.GuardarCambios();
                #endregion
                referencia = new DtoPermisos();

                var rol2 = new Rol
                {
                    Activo = true,
                    Nombre = "GENERAL",
                    FechaCreacion = fechaActual,
                    FechaModificacion = fechaActual,
                    Permisos = Newtonsoft.Json.JsonConvert.SerializeObject(referencia)
                };

                _RepositorioRoles.Agregar(rol2);
                _RepositorioRoles.GuardarCambios();
#endif
                IServicioUsuarios _ServicioUsuarios = FabricaDependencias.Instancia.Resolver<IServicioUsuarios>();

                #region huella sistemas
                var usuarioSistemas = usuarios.First(m => m.Alias == "SISTEMAS");

                byte[] arr;
                using (FileStream fs = File.OpenRead("huella.fpt"))
                {
                    DPFP.Template template = new DPFP.Template(fs);
                    arr = new byte[template.Size];
                    template.Serialize(ref arr);
                }

                usuarioSistemas.Credenciales.Add(new Credencial
                {
                    EsAcceso = true,
                    TipoCredencial = Credencial.TiposCredencial.Huella,
                    Valor = arr
                });
                #endregion
#if !DEBUG_TEST_CLIENTE
                #region huella recepcionista
                var usuarioRecepcionista = usuarios.First(m => m.Alias == "RECEPCIONISTA");

                using (FileStream fs = File.OpenRead("huellaRecepcionista.fpt"))
                {
                    DPFP.Template template = new DPFP.Template(fs);
                    arr = new byte[template.Size];
                    template.Serialize(ref arr);
                }

                usuarioRecepcionista.Credenciales.Add(new Credencial
                {
                    EsAcceso = true,
                    TipoCredencial = Credencial.TiposCredencial.Huella,
                    Valor = arr
                });
                #endregion
                #region huella vendedor
                var usuarioVendedor = usuarios.First(m => m.Alias == "VENDEDOR");


                using (FileStream fs = File.OpenRead("huellaVendedor.fpt"))
                {
                    DPFP.Template template = new DPFP.Template(fs);
                    arr = new byte[template.Size];
                    template.Serialize(ref arr);
                }

                usuarioVendedor.Credenciales.Add(new Credencial
                {
                    EsAcceso = true,
                    TipoCredencial = Credencial.TiposCredencial.Huella,
                    Valor = arr
                });
                #endregion
                #region huella gerenteOperativo
                var usuarioGerenteOperativo = usuarios.First(m => m.Alias == "GERENTEOP");


                using (FileStream fs = File.OpenRead("huellaGerenteOperativo.fpt"))
                {
                    DPFP.Template template = new DPFP.Template(fs);
                    arr = new byte[template.Size];
                    template.Serialize(ref arr);
                }

                usuarioGerenteOperativo.Credenciales.Add(new Credencial
                {
                    EsAcceso = true,
                    TipoCredencial = Credencial.TiposCredencial.Huella,
                    Valor = arr
                });
                #endregion
                #region huella mesero
                var mesero = usuarios.First(m => m.Alias == "Mesero01");

                using (FileStream fs = File.OpenRead("huellaMesero.fpt"))
                {
                    DPFP.Template template = new DPFP.Template(fs);
                    arr = new byte[template.Size];
                    template.Serialize(ref arr);
                }

                mesero.Credenciales.Add(new Credencial
                {
                    //EsAcceso = true,
                    TipoCredencial = Credencial.TiposCredencial.Huella,
                    Valor = arr
                });
                #endregion
                #region huella cocinero
                var cocinero = usuarios.First(m => m.Alias == "Cocinero01");

                using (FileStream fs = File.OpenRead("huellaCocinero.fpt"))
                {
                    DPFP.Template template = new DPFP.Template(fs);
                    arr = new byte[template.Size];
                    template.Serialize(ref arr);
                }

                cocinero.Credenciales.Add(new Credencial
                {
                    //EsAcceso = true,
                    TipoCredencial = Credencial.TiposCredencial.Huella,
                    Valor = arr
                });
                #endregion
#endif
                IRepositorioEmpleados _RepositorioEmpleados = FabricaDependencias.Instancia.Resolver<IRepositorioEmpleados>();

                var usuarioComodin = new DtoUsuario
                {
                    Permisos = new DtoPermisos { CrearUsuarios = true }
                };

                foreach (var usuario in usuarios)
                {
                    var empleado = _RepositorioEmpleados.Obtener(m => m.Activo && m.Id == usuario.IdEmpleado, m=> m.Puesto);

                    if (usuario.Alias == "SISTEMAS")
                        empleado.Puesto.IdRol = rolSistemas.Id;
#if !DEBUG_TEST_CLIENTE
                    else if (usuario.Alias == "RECEPCIONISTA")
                        empleado.Puesto.IdRol = rolRecepcionista.Id;
                    else if (usuario.Alias == "VENDEDOR")
                        empleado.Puesto.IdRol = rolVendedor.Id;
                    else if (usuario.Alias == "GERENTEOP")
                        empleado.Puesto.IdRol = rolGerenteOperativo.Id;
                    else if (empleado.Puesto.Nombre.ToUpper().Contains("MESERO"))
                        empleado.Puesto.IdRol = rolMesero.Id;
                    else if (empleado.Puesto.Nombre.ToUpper().Contains("VALET"))
                        empleado.Puesto.IdRol = rolValet.Id;
                    else if (empleado.Puesto.Nombre.ToUpper().Contains("COCINERO"))
                        empleado.Puesto.IdRol = rolCocinero.Id;
                    else
                        empleado.Puesto.IdRol = rol2.Id;
#endif
                    List<DtoCredencial> confirmaciones = new List<DtoCredencial>();

                    confirmaciones.Add(new DtoCredencial
                    {
                        TipoCredencial = Credencial.TiposCredencial.Contrasena,
                        ValorVerificarStr = "Pass123"
                    });

                    if(usuario.Credenciales.Any(m=>m.TipoCredencial == Credencial.TiposCredencial.Huella))
                    confirmaciones.Add(new DtoCredencial
                    {
                        TipoCredencial = Credencial.TiposCredencial.Huella,
                        ValorVerificarBin = arr
                    });

                    _ServicioUsuarios.CrearUsuario(usuario, confirmaciones, usuarioComodin);

                    _RepositorioEmpleados.Modificar(empleado);
                }

                _RepositorioEmpleados.GuardarCambios();

                scope.Complete();
            }
        }

        internal void InitV2(List<Usuario> usuarios)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                IRepositorioRoles _RepositorioRoles = FabricaDependencias.Instancia.Resolver<IRepositorioRoles>();

                var enumPagos = Enum.GetValues(typeof(TiposPago)).Cast<int>().ToList();

                var fechaActual = DateTime.Now;

                DtoPermisos referencia;

         

                referencia = new DtoPermisos();
                referencia.AsignarValorABooleanos(true);

                var roles = UnitTestImportarPermisos.ImportarYGenerarPermisos();

                var rolSistemas = roles.First(m => m.Nombre.Trim().ToUpper().Equals("ADMINISTRADOR"));

                foreach (var tipoPago in enumPagos)
                {
                    rolSistemas.PermisosPagos.Add(new Modelo.Seguridad.Entidades.PermisoPago
                    {
                        Activo = true,
                        IdFormaPago = tipoPago,
                        FechaCreacion = fechaActual,
                        FechaModificacion = fechaActual
                    });
                }

                var rolCaja = roles.First(m => m.Nombre.Trim().ToUpper().Equals("CAJA"));

                foreach (var tipoPago in enumPagos)
                {
                    rolCaja.PermisosPagos.Add(new Modelo.Seguridad.Entidades.PermisoPago
                    {
                        Activo = true,
                        IdFormaPago = tipoPago,
                        FechaCreacion = fechaActual,
                        FechaModificacion = fechaActual
                    });
                }

                var rolGerenciaOperativa = roles.First(m => m.Nombre.Trim().ToUpper().Equals("GERENCIA OPERATIVA"));

                foreach (var tipoPago in enumPagos)
                {
                    rolGerenciaOperativa.PermisosPagos.Add(new Modelo.Seguridad.Entidades.PermisoPago
                    {
                        Activo = true,
                        IdFormaPago = tipoPago,
                        FechaCreacion = fechaActual,
                        FechaModificacion = fechaActual
                    });
                }

                foreach(var rol in roles)
                    _RepositorioRoles.Agregar(rol);

                _RepositorioRoles.GuardarCambios();

                IServicioUsuarios _ServicioUsuarios = FabricaDependencias.Instancia.Resolver<IServicioUsuarios>();

                #region huella sistemas
                var usuarioSistemas = usuarios.First(m => m.Alias.Trim().ToUpper().Equals("SISTEMAS"));

                byte[] arr;
                using (FileStream fs = File.OpenRead("huella.fpt"))
                {
                    DPFP.Template template = new DPFP.Template(fs);
                    arr = new byte[template.Size];
                    template.Serialize(ref arr);
                }

                usuarioSistemas.Credenciales.Add(new Credencial
                {
                    EsAcceso = true,
                    TipoCredencial = Credencial.TiposCredencial.Huella,
                    Valor = arr
                });
                #endregion
                //#region huella recepcionista
                //var usuarioRecepcionista = usuarios.First(m => m.Alias == "RECEPCIONISTA");

                //using (FileStream fs = File.OpenRead("huellaRecepcionista.fpt"))
                //{
                //    DPFP.Template template = new DPFP.Template(fs);
                //    arr = new byte[template.Size];
                //    template.Serialize(ref arr);
                //}

                //usuarioRecepcionista.Credenciales.Add(new Credencial
                //{
                //    EsAcceso = true,
                //    TipoCredencial = Credencial.TiposCredencial.Huella,
                //    Valor = arr
                //});
                //#endregion
                //#region huella vendedor
                //var usuarioVendedor = usuarios.First(m => m.Alias == "VENDEDOR");


                //using (FileStream fs = File.OpenRead("huellaVendedor.fpt"))
                //{
                //    DPFP.Template template = new DPFP.Template(fs);
                //    arr = new byte[template.Size];
                //    template.Serialize(ref arr);
                //}

                //usuarioVendedor.Credenciales.Add(new Credencial
                //{
                //    EsAcceso = true,
                //    TipoCredencial = Credencial.TiposCredencial.Huella,
                //    Valor = arr
                //});
                //#endregion
                //#region huella gerenteOperativo
                //var usuarioGerenteOperativo = usuarios.First(m => m.Alias == "GERENTEOP");


                //using (FileStream fs = File.OpenRead("huellaGerenteOperativo.fpt"))
                //{
                //    DPFP.Template template = new DPFP.Template(fs);
                //    arr = new byte[template.Size];
                //    template.Serialize(ref arr);
                //}

                //usuarioGerenteOperativo.Credenciales.Add(new Credencial
                //{
                //    EsAcceso = true,
                //    TipoCredencial = Credencial.TiposCredencial.Huella,
                //    Valor = arr
                //});
                //#endregion
                //#region huella mesero
                //var mesero = usuarios.First(m => m.Alias == "Mesero01");

                //using (FileStream fs = File.OpenRead("huellaMesero.fpt"))
                //{
                //    DPFP.Template template = new DPFP.Template(fs);
                //    arr = new byte[template.Size];
                //    template.Serialize(ref arr);
                //}

                //mesero.Credenciales.Add(new Credencial
                //{
                //    //EsAcceso = true,
                //    TipoCredencial = Credencial.TiposCredencial.Huella,
                //    Valor = arr
                //});
                //#endregion
                //#region huella cocinero
                //var cocinero = usuarios.First(m => m.Alias == "Cocinero01");

                //using (FileStream fs = File.OpenRead("huellaCocinero.fpt"))
                //{
                //    DPFP.Template template = new DPFP.Template(fs);
                //    arr = new byte[template.Size];
                //    template.Serialize(ref arr);
                //}

                //cocinero.Credenciales.Add(new Credencial
                //{
                //    //EsAcceso = true,
                //    TipoCredencial = Credencial.TiposCredencial.Huella,
                //    Valor = arr
                //});
                //#endregion

                IRepositorioEmpleados _RepositorioEmpleados = FabricaDependencias.Instancia.Resolver<IRepositorioEmpleados>();

                var usuarioComodin = new DtoUsuario
                {
                    Permisos = new DtoPermisos { CrearUsuarios = true }
                };

                foreach (var usuario in usuarios)
                {
                    var empleado = _RepositorioEmpleados.Obtener(m => m.Activo && m.Id == usuario.IdEmpleado, m => m.Puesto);

                    empleado.Puesto.IdRol = roles.First(m => m.Nombre == empleado.Puesto.Nombre).Id;

                    //if (usuario.Alias == "SISTEMAS")
                    //    empleado.Puesto.IdRol = rolSistemas.Id;
                    //else if (usuario.Alias == "RECEPCIONISTA")
                    //    empleado.Puesto.IdRol = rolRecepcionista.Id;
                    //else if (usuario.Alias == "VENDEDOR")
                    //    empleado.Puesto.IdRol = rolVendedor.Id;
                    //else if (usuario.Alias == "GERENTEOP")
                    //    empleado.Puesto.IdRol = rolGerenteOperativo.Id;
                    //else if (empleado.Puesto.Nombre.ToUpper().Contains("MESERO"))
                    //    empleado.Puesto.IdRol = rolMesero.Id;
                    //else if (empleado.Puesto.Nombre.ToUpper().Contains("VALET"))
                    //    empleado.Puesto.IdRol = rolValet.Id;
                    //else if (empleado.Puesto.Nombre.ToUpper().Contains("COCINERO"))
                    //    empleado.Puesto.IdRol = rolCocinero.Id;
                    //else
                    //    empleado.Puesto.IdRol = rol2.Id;

                    List<DtoCredencial> confirmaciones = new List<DtoCredencial>();

                    confirmaciones.Add(new DtoCredencial
                    {
                        TipoCredencial = Credencial.TiposCredencial.Contrasena,
                        ValorVerificarStr = "Pass123"
                    });

                    if (usuario.Credenciales.Any(m => m.TipoCredencial == Credencial.TiposCredencial.Huella))
                        confirmaciones.Add(new DtoCredencial
                        {
                            TipoCredencial = Credencial.TiposCredencial.Huella,
                            ValorVerificarBin = arr
                        });

                    _ServicioUsuarios.CrearUsuario(usuario, confirmaciones, usuarioComodin);

                    _RepositorioEmpleados.Modificar(empleado);
                }

                _RepositorioEmpleados.GuardarCambios();

                scope.Complete();
            }
        }
    }
}
