﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;

namespace UnitTestProject1.Contextos
{
    [TestClass]
    public class InicializadorContextos
    {
        [TestMethod]
        public void Inicializar()
        {
            using (TransactionScope scope = new TransactionScope())
            {
                var idAdmin = new InicializadorSotContext().Init();
                new InicializadorSeguridadContext().InitV2(idAdmin);

                scope.Complete();
            }
        }
    }
}
