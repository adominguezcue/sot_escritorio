﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Modelo;
using System.Collections.Generic;
using Modelo.Repositorios;
using Transversal.Dependencias;
using System.Linq;
using System.Transactions;
using System.Data.Entity.Validation;
using Modelo.Entidades;
using UnitTestProject1.Herramientas;
using Negocio.Empleados;
using Negocio.CentrosCostos;
using Negocio.ConceptosGastos;
using Modelo.Almacen.Repositorios;
using Modelo.Almacen.Entidades;
using Transversal.Extensiones;
using UnitTestProject1.Extras;

namespace UnitTestProject1.Contextos
{
    [TestClass]
    public class InicializadorSotContext
    {
        [TestMethod]
        public void Inicializar()
        {
            Init();
        }

        internal List<Modelo.Seguridad.Entidades.Usuario> Init()
        {
            DateTime fechaActual = DateTime.Now;
            List<Modelo.Seguridad.Entidades.Usuario> usuarios = new List<Modelo.Seguridad.Entidades.Usuario>();

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    #region impresoras

                    IRepositorioConfiguracionesImpresoras _repositorioConfiguracionesImpresoras = FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesImpresoras>();

                    _repositorioConfiguracionesImpresoras.Agregar(new ConfiguracionImpresoras
                    {
                        Activa = true,
                        ImpresoraBar = "",
                        ImpresoraCocina = "",
                        ImpresoraCortes = "",
                        ImpresoraTickets = ""
                    });

                    _repositorioConfiguracionesImpresoras.GuardarCambios();

                    #endregion
                    #region fajillas

                    IRepositorioConfiguracionesFajillas _repositorioConfiguracionesFajillas = FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesFajillas>();

                    var configuracion = new ConfiguracionFajilla
                    {
                        Activa = true,
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        Valor = 5000,
                    };

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 1000
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 500
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 200
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 100
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 50
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 20
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 10
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 5
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 2
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 1
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 0.5M
                    });

                    var euro = new MonedaExtranjera
                    {
                        Activa = true,
                        Abreviatura = "EUR",
                        Nombre = "Euro",
                        ValorCambio = 20.67M
                    };

                    var dolar = new MonedaExtranjera
                    {
                        Activa = true,
                        Abreviatura = "USD",
                        Nombre = "Dólar",
                        ValorCambio = 18.35M
                    };

                    configuracion.MonedasExtranjeras.Add(euro);
                    configuracion.MonedasExtranjeras.Add(dolar);

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 10,
                        MonedaExtranjera = euro
                    });

                    configuracion.MontosConfiguracionFajilla.Add(new MontoConfiguracionFajilla
                    {
                        Activa = true,
                        Monto = 10,
                        MonedaExtranjera = dolar
                    });

                    _repositorioConfiguracionesFajillas.Agregar(configuracion);
                    _repositorioConfiguracionesFajillas.GuardarCambios();

                    #endregion
                    #region conceptos

                    //IRepositorioTiposConcepto _repositorioTiposConceptos = FabricaDependencias.Instancia.Resolver<IRepositorioTiposConcepto>();


                    IRepositorioConceptosSistema _repositorioConceptosSistema = FabricaDependencias.Instancia.Resolver<IRepositorioConceptosSistema>();


                    _repositorioConceptosSistema.Agregar(new ConceptoSistema
                    {
                        Activo = true,
                        Concepto = "CONSUMO PERSONAL",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        TipoConcepto = ConceptoSistema.TiposConceptos.Cortesia
                    });

                    //_repositorioConceptosSistema.Agregar(new ConceptoSistema
                    //{
                    //    Activo = true,
                    //    Concepto = "AGUA EMBOTELLADA",
                    //    FechaCreacion = DateTime.Now,
                    //    FechaModificacion = DateTime.Now,
                    //    TipoConcepto = ConceptoSistema.TiposConceptos.Gasto
                    //});

                    _repositorioConceptosSistema.Agregar(new ConceptoSistema
                    {
                        Activo = true,
                        Concepto = "FALTA DE PAGO",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        TipoConcepto = ConceptoSistema.TiposConceptos.CancelacionHabitacion
                    });

                    _repositorioConceptosSistema.Agregar(new ConceptoSistema
                    {
                        Activo = true,
                        Concepto = "LIMPIEZA TERMINADA",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        TipoConcepto = ConceptoSistema.TiposConceptos.LiberacionHabitacion
                    });

                    _repositorioConceptosSistema.Agregar(new ConceptoSistema
                    {
                        Activo = true,
                        Concepto = "SALIDA NORMAL",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        TipoConcepto = ConceptoSistema.TiposConceptos.LiberacionHabitacion
                    });

                    _repositorioConceptosSistema.Agregar(new ConceptoSistema
                    {
                        Activo = true,
                        Concepto = "ERROR DE SISTEMA",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        TipoConcepto = ConceptoSistema.TiposConceptos.CancelacionVenta
                    });

                    _repositorioConceptosSistema.GuardarCambios();

                    #endregion
                    #region configuración asistencia
                    IRepositorioConfiguracionesAsistencias _repositorioConfiguracionesAsistencias = FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesAsistencias>();

                    _repositorioConfiguracionesAsistencias.Agregar(new ConfiguracionAsistencias
                    {
                        DiasDescuento = 1.5M,
                        DiasDescuentoJustificado = 1,
                        DiasDobleTurno = 1.5M,
                        Falta = 16,
                        Retardo = 7
                    });

                    _repositorioConfiguracionesAsistencias.GuardarCambios();

                    #endregion
                    #region configuración global

                    IRepositorioConfiguracionesTurno _repositorioConfiguracionesTurno = FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesTurno>();

                    //var configuracionGlobal = new ConfiguracionGlobal
                    //{
                    //    Activa = true,
                    //    MaximoComandasSimultaneas = 3,
                    //    MaximoTaxisSimultaneos = 3,
                    //    PrecioPorDefectoTaxi = 40,
                    //    PrecioTarjetasPuntos = 86.21M,
                    //    Nombre = "V Motel Boutique",
                    //    SubNombre = "Suites & Villas",
                    //    Operadora = "BOUTIQUE VIADUCTO S DE RL DE CV",
                    //    Propietaria = "HOTELERA AMIUDALA SA DE CV",
                    //    NombreSucursal = "V VIADUCTO",
                    //    RazonSocial = "BOUTIQUE VIADUCTO S DE RL DE CV",
                    //    RFC = "EVI1102117P9",
                    //    Direccion = "VIADUCTO M IGUEL ALEM AN 77 COL TACUBAYA  DEL. M IGUEL HIDALGO, C.P. 04739",
                    //    Guid = Guid.NewGuid().ToString(),
                    //    IVA = 0.16M
                    //};

                    var primerTurno = new ConfiguracionTurno
                    {
                        Activa = true,
                        Orden = 1,
                        Nombre = "Matutino",
                        MinutosInicioAsistencia = 450
                    };


                    _repositorioConfiguracionesTurno.Agregar(primerTurno);

                    _repositorioConfiguracionesTurno.Agregar(new ConfiguracionTurno
                    {
                        Activa = true,
                        Orden = 2,
                        Nombre = "Vespertino",
                        MinutosInicioAsistencia = 930
                    });

                    _repositorioConfiguracionesTurno.Agregar(new ConfiguracionTurno
                    {
                        Activa = true,
                        Orden = 3,
                        Nombre = "Nocturno",
                        MinutosInicioAsistencia = 1410
                    });

                    //_repositorioConfiguracionesGlobales.Agregar(configuracionGlobal);

                    _repositorioConfiguracionesTurno.GuardarCambios();

                    #endregion
                    #region categorías de centros de costo

                    IRepositorioCategoriasCentroCostos _repositorioCategoriasCentroCostos = FabricaDependencias.Instancia.Resolver<IRepositorioCategoriasCentroCostos>();

                    var categoriaCC = new CategoriaCentroCostos
                    {
                        Activa = true,
                        Nombre = "Gastos",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now
                    };

                    _repositorioCategoriasCentroCostos.Agregar(categoriaCC);
                    _repositorioCategoriasCentroCostos.GuardarCambios();

                    #endregion
                    CrearConceptosYGastos(categoriaCC.Id, "GASTOS DE OPERACIÓN", "COMEDOR DE EMPLEADOS",
                                                                                    "PASAJES Y TAXIS",
                                                                                    "BASURA",
                                                                                    "HIELO",
                                                                                    "GARRAFON DE AGUA",
                                                                                    "AGUA EMBOTELLADA AMENITIES",
                                                                                    "FARMACIA",
                                                                                    "PAPELERÍA",
                                                                                    "FUMIGACION",
                                                                                    "PIPAS DE AGUA",
                                                                                    "TELMEX",
                                                                                    "NEXTEL",
                                                                                    "SKY",
                                                                                    "LUZ",
                                                                                    "GAS",
                                                                                    "DIESEL PLANTA",
                                                                                    "LAVANDERÍA",
                                                                                    "COMPRA O REPARACION ROPA",
                                                                                    "CORPORATIVO",
                                                                                    "AMENITIES DE HABITACION",
                                                                                    "PRESERVATIVOS AMENITIES",
                                                                                    "MATERIAL DE LIMPIEZA**",
                                                                                    "MATERIAL DE SUMINISTRO**",
                                                                                    "HONORARIOS**",
                                                                                    "COMISIONES BANCO**",
                                                                                    "IMPUESTOS**",
                                                                                    "PAGO DERECHOS**",
                                                                                    "IMPRENTA**",
                                                                                    "UNIFORMES**",
                                                                                    "CRISTALERIA Y VAJILLA**",
                                                                                    "INSUMOS PLANTA TRATAM**",
                                                                                    "OTROS**");
                    CrearConceptosYGastos(categoriaCC.Id, "GASTOS DE MANTENIMIENTO", "JARDINERIA",
                                                                                        "MATERIAL PLOMERÍA",
                                                                                        "MATERIAL ELECTRICIDAD",
                                                                                        "MATERIAL ALBAÑILERÍA",
                                                                                        "MATERIAL DE PINTURA",
                                                                                        "TRABAJO DE REPARACIONES**",
                                                                                        "TRABAJO DE MANTENIMIENTO**",
                                                                                        "OTROS**");
                    CrearConceptosYGastos(categoriaCC.Id, "GASTOS DE NOMINA", "PERSONAL EN NOMINA",
                                                                                "PRESTACIONES DE LEY",
                                                                                "SEGURO SOCIAL, INFONAVIT",
                                                                                "PAGOS EXTRA",
                                                                                "SUELDO GERENCIA",
                                                                                "SINDICATO",
                                                                                "FINIQUITOS",
                                                                                "SEGURO VIDA EMPLEADOS");
                    CrearConceptosYGastos(categoriaCC.Id, "GASTOS EXTRAS", "GRATIFICACIONES**",
                                                                            "EQUIPO Y HERRAMIENTA**",
                                                                            "PUBLICIDAD**",
                                                                            "OTROS**");
                    CrearConceptosYGastos(categoriaCC.Id, "GASTOS DE BAR", "REFRESCOS",
                                                                            "CERVEZAS",
                                                                            "AGUA EMBOTELLADA",
                                                                            "JUGOS",
                                                                            "HELADOS",
                                                                            "VINOS Y LICORES",
                                                                            "RIPEX",
                                                                            "PROVEEDORES**",
                                                                            "OTROS**");
                    CrearConceptosYGastos(categoriaCC.Id, "GASTOS DE COCINA", "CHAPATAS",
                                                                                "TORTILLAS",
                                                                                "COSTCO",
                                                                                "SUPERMERCADO",
                                                                                "SUPER ORIENTAL",
                                                                                "PROVEEDORES**",
                                                                                "OTROS**");
                    CrearConceptosYGastos(categoriaCC.Id, "GASTOS DE SEX & SPA", "EXPERIENCIAS",
                                                                                    "AROMATERIAPIA",
                                                                                    "AMENIDADES",
                                                                                    "JUGUETES",
                                                                                    "PRESERVATIVOS Y LUBRICANTES",
                                                                                    "DISFRACES Y LENCERIA",
                                                                                    "CIGARROS",
                                                                                    "OTROS**");
                    #region configuraciones de negocio

                    IRepositorioConfiguracionesNegocio _repositorioConfiguracionesNegocio = FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesNegocio>();

                    var configuracionHotel = new ConfiguracionTarifa
                    {
                        Activa = true,
                        //Id = 1,
                        CantidadAutosMaxima = 1,
                        AdmiteReservaciones = true,
                        //DuracionOEntrada = 15,
                        //HoraSalida = 12,
                        RangoFijo = true,
                        Tarifa = ConfiguracionTarifa.Tarifas.Hotel,
                    };

                    var configuracionMotel = new ConfiguracionTarifa
                    {
                        Activa = true,
                        //Id = 1,
                        CantidadAutosMaxima = 1,
                        //DuracionOEntrada = 8,
                        ExtensionesMaximas = 2,
                        //AdmiteHorasExtra = true,
                        Tarifa = ConfiguracionTarifa.Tarifas.Motel,
                    };

                    _repositorioConfiguracionesNegocio.Agregar(configuracionHotel);
                    _repositorioConfiguracionesNegocio.Agregar(configuracionMotel);

                    _repositorioConfiguracionesNegocio.GuardarCambios();

                    #endregion
                    #region tipos de habitación

                    IRepositorioTiposHabitacion _repostorioTiposHabitaciones = FabricaDependencias.Instancia.Resolver<IRepositorioTiposHabitacion>();

                    //string[] nombres = { "Twin Suite", "Junior Suite", "Steam Suite", "Sky Suite", "Pool Villa" };

                    //int tope = 3;

                    var tipoJuniorVilla = GenerarNuevoTipo(3, "JV", "Junior Villa", "Junior Villa",
                                                           950, 250, 950, configuracionHotel,
                                                           950, 250, 950, configuracionMotel);
                    _repostorioTiposHabitaciones.Agregar(tipoJuniorVilla);

                    var tipoTwinSuite = GenerarNuevoTipo(3, "TS", "Twin Suite", "Twin Suite",
                                                         1250, 250, 1250, configuracionHotel);
                    _repostorioTiposHabitaciones.Agregar(tipoTwinSuite);

                    var tipoPoolAndSpa = GenerarNuevoTipo(3, "PSS", "Pool & Spa", "Pool & Spa", 
                                                          1800, 250, 1800, configuracionHotel,
                                                          1800, 250, 1800, configuracionMotel);
                    _repostorioTiposHabitaciones.Agregar(tipoPoolAndSpa);

                    var tipoJuniorSuite = GenerarNuevoTipo(3, "JS", "Junior Suite", "Junior Suite",
                                                          900, 250, 900, configuracionHotel,
                                                          900, 250, 900, configuracionMotel);
                    _repostorioTiposHabitaciones.Agregar(tipoJuniorSuite);

                    var tipoPoolVilla = GenerarNuevoTipo(1, "PV", "Pool Villa", "Pool Villa",
                                                         4000, 500, 4000, configuracionHotel);
                    _repostorioTiposHabitaciones.Agregar(tipoPoolVilla);

                    var tipoSkySuite = GenerarNuevoTipo(2, "SS", "Sky Suite", "Sky Suite",
                                                         4000, 500, 4000, configuracionHotel);
                    _repostorioTiposHabitaciones.Agregar(tipoSkySuite);

                    _repostorioTiposHabitaciones.GuardarCambios();

                    #endregion
                    #region Habitaciones

                    IRepositorioHabitaciones _repostorioHabitaciones = FabricaDependencias.Instancia.Resolver<IRepositorioHabitaciones>();

                    var diccionarioHabitaciones = new Dictionary<int, List<string>>();

                    diccionarioHabitaciones.Add(tipoJuniorVilla.Id, new List<string>() { "1", "2", "3", "4", "5",
                                                                                         "6", "7", "8", "9", "10", "11",
                                                                                         "12", "13", "14", "15", "16", "17",
                                                                                         "18", "20", "21", "22", "23", "24", "25"});

                    diccionarioHabitaciones.Add(tipoPoolVilla.Id, new List<string>() { "19" });

                    diccionarioHabitaciones.Add(tipoTwinSuite.Id, new List<string>() { "101", "102", "103", "104", "105",
                                                                                         "106", "107", "108" });

                    diccionarioHabitaciones.Add(tipoPoolAndSpa.Id, new List<string>() { "201", "202", "301", "302", "401",
                                                                                         "402", "501", "502", "601", "602" });

                    diccionarioHabitaciones.Add(tipoJuniorSuite.Id, new List<string>() { "203", "204", "205", "206", "303", "304",
                                                                                         "305", "306", "403", "404", "405", "406",
                                                                                         "503", "504", "505", "506", "603", "604",
                                                                                         "605", "606" });

                    diccionarioHabitaciones.Add(tipoSkySuite.Id, new List<string>() { "701", "702" });

                    /*
                     -Habitación Junior Villa: 1 a 25 (excepto 19 Villa Pool)
                     -Habitación Twin suite: 101 a 108 
                     -Habitación Pool & Spa: 201,202, 301, 302, 401, 402, 501, 502, 601, 602 
                     -Habitación Junior Suite: 203, 204, 205, 206, 303, 304, 305, 306, 403, 404, 405, 406, 503, 504, 505, 506, 603, 604, 605, 606
                     -Habitación Sky 701,702

                     */

                    var diccionarioPisos = new Dictionary<int, List<Tuple<int, string>>>();

                    diccionarioPisos.Add(1, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "1"),
                                                                              new Tuple<int, string>(2, "2"),
                                                                              new Tuple<int, string>(3, "3"),
                                                                              new Tuple<int, string>(4, "4"),
                                                                              new Tuple<int, string>(5, "5"),
                                                                              new Tuple<int, string>(6, "6"),
                                                                              new Tuple<int, string>(7, "7"),
                                                                              new Tuple<int, string>(8, "8"),
                                                                              new Tuple<int, string>(9, "9"),
                                                                              new Tuple<int, string>(10, "10") });
                    diccionarioPisos.Add(2, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "11"),
                                                                              new Tuple<int, string>(2, "12"),
                                                                              new Tuple<int, string>(3, "13"),
                                                                              new Tuple<int, string>(4, "14"),
                                                                              new Tuple<int, string>(5, "15"),
                                                                              new Tuple<int, string>(6, "16"),
                                                                              new Tuple<int, string>(7, "17"),
                                                                              new Tuple<int, string>(8, "18"),
                                                                              new Tuple<int, string>(10, "20") });
                    diccionarioPisos.Add(3, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "21"),
                                                                              new Tuple<int, string>(2, "22"),
                                                                              new Tuple<int, string>(3, "23"),
                                                                              new Tuple<int, string>(4, "24"),
                                                                              new Tuple<int, string>(5, "25"),
                                                                              new Tuple<int, string>(9, "19") });
                    diccionarioPisos.Add(4, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "101"),
                                                                              new Tuple<int, string>(2, "102"),
                                                                              new Tuple<int, string>(3, "103"),
                                                                              new Tuple<int, string>(4, "104"),
                                                                              new Tuple<int, string>(5, "105"),
                                                                              new Tuple<int, string>(6, "106"),
                                                                              new Tuple<int, string>(7, "107"),
                                                                              new Tuple<int, string>(8, "108") });
                    diccionarioPisos.Add(5, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "201"),
                                                                              new Tuple<int, string>(2, "202"),
                                                                              new Tuple<int, string>(4, "203"),
                                                                              new Tuple<int, string>(5, "204"),
                                                                              new Tuple<int, string>(6, "205"),
                                                                              new Tuple<int, string>(7, "206") });
                    diccionarioPisos.Add(6, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "301"),
                                                                              new Tuple<int, string>(2, "302"),
                                                                              new Tuple<int, string>(4, "303"),
                                                                              new Tuple<int, string>(5, "304"),
                                                                              new Tuple<int, string>(6, "305"),
                                                                              new Tuple<int, string>(7, "306") });
                    diccionarioPisos.Add(7, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "401"),
                                                                              new Tuple<int, string>(2, "402"),
                                                                              new Tuple<int, string>(4, "403"),
                                                                              new Tuple<int, string>(5, "404"),
                                                                              new Tuple<int, string>(6, "405"),
                                                                              new Tuple<int, string>(7, "406") });
                    diccionarioPisos.Add(8, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "501"),
                                                                              new Tuple<int, string>(2, "502"),
                                                                              new Tuple<int, string>(4, "503"),
                                                                              new Tuple<int, string>(5, "504"),
                                                                              new Tuple<int, string>(6, "505"),
                                                                              new Tuple<int, string>(7, "506") });
                    diccionarioPisos.Add(9, new List<Tuple<int, string>>() { new Tuple<int, string>(1, "601"),
                                                                              new Tuple<int, string>(2, "602"),
                                                                              new Tuple<int, string>(4, "603"),
                                                                              new Tuple<int, string>(5, "604"),
                                                                              new Tuple<int, string>(6, "605"),
                                                                              new Tuple<int, string>(7, "606"),
                                                                              new Tuple<int, string>(9, "701"),
                                                                              new Tuple<int, string>(10, "702") });
                    


                    foreach (var piso in diccionarioPisos.Keys)
                    {

                        foreach (var posicion in diccionarioPisos[piso])
                        {
                            _repostorioHabitaciones.Agregar(new Habitacion
                            {
                                //TipoHabitacion = listaTipos[rnd.Next(listaTipos.Count)],
                                EstadoHabitacion = Habitacion.EstadosHabitacion.Libre,
                                IdTipoHabitacion = diccionarioHabitaciones.First(m=>m.Value.Contains(posicion.Item2)).Key,
                                NumeroHabitacion = posicion.Item2,
                                Activo = true,
                                Piso = piso,
                                Posicion = posicion.Item1,
                                FechaCreacion = DateTime.Now,
                                FechaModificacion = DateTime.Now
                            });
                        }
                    }
                    _repostorioHabitaciones.GuardarCambios();

                    #endregion
                    #region paquetes

                    IRepositorioPaquetes _repostorioPaquetes = FabricaDependencias.Instancia.Resolver<IRepositorioPaquetes>();
                    var tipos = _repostorioTiposHabitaciones.ObtenerElementos(m => m.Activo).ToList();
                    var rnd = new Random();

                    for (int i = 1; i <= 10; i++)
                    {
                        _repostorioPaquetes.Agregar(new Paquete
                        {
                            ClaveSAT = "55121609",
                            Clave = "P" + i,
                            Nombre = string.Format("Paquete {0}", i),
                            Activo = true,
                            IdTipoHabitacion = tipos[rnd.Next(tipos.Count)].Id,
                            Precio = 10 * i * rnd.Next(tipos.Count) + 1,
                            LeyendaDescuento = "",
                            FechaCreacion = DateTime.Now,
                            FechaModificacion = DateTime.Now
                        });
                    }

                    var paqueteTW = new Paquete
                    {
                        ClaveSAT = "55121609",
                        Clave = "TW",
                        Nombre = "Tarifa Wekkend",
                        Activo = true,
                        IdTipoHabitacion = tipos[rnd.Next(tipos.Count)].Id,
                        Precio = 120,
                        LeyendaDescuento = "",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now
                    };

                    paqueteTW.ProgramasPaquete.Add(new ProgramaPaquete
                    {
                        Activo = true,
                        Dia = 6
                    });

                    paqueteTW.ProgramasPaquete.Add(new ProgramaPaquete
                    {
                        Activo = true,
                        Dia = 7
                    });

                    _repostorioPaquetes.Agregar(paqueteTW);

                    var paquetePL = new Paquete
                    {
                        ClaveSAT = "55121609",
                        Clave = "PL",
                        Nombre = "Paquete Love",
                        Activo = true,
                        IdTipoHabitacion = tipos[rnd.Next(tipos.Count)].Id,
                        Precio = 120,
                        LeyendaDescuento = "",
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now
                    };

                    paquetePL.ProgramasPaquete.Add(new ProgramaPaquete
                    {
                        Activo = true,
                        Dia = 14,
                        Mes = 2
                    });

                    _repostorioPaquetes.Agregar(paquetePL);

                    _repostorioPaquetes.GuardarCambios();


                    #endregion
                    usuarios = InicializarEmpleadosV2(primerTurno.Id);
                    InicializarArticulosYPropinas();
                    InicializarMesas();
                    InicializarCorte(fechaActual, primerTurno.Id);

                    scope.Complete();
                }

                return usuarios;
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }
        }

        private void CrearConceptosYGastos(int idCategoria, string nombreCentro, params string[] conceptos)
        {
            #region centros de costos

            IServicioCentrosCostos _servicioCentrosCostos = FabricaDependencias.Instancia.Resolver<IServicioCentrosCostos>();

            var centroCostosGastos = new CentroCostos
            {
                Activo = true,
                IdCategoria = idCategoria,
                Nombre = nombreCentro,
                //TopePresupuestal = 100000
            };

            _servicioCentrosCostos.CrearCentroCostos(centroCostosGastos, new Modelo.Seguridad.Dtos.DtoUsuario());

            #endregion
            #region concepto de gastos

            IServicioConceptosGastos _servicioConceptosGastos = FabricaDependencias.Instancia.Resolver<IServicioConceptosGastos>();

            foreach (var concepto in conceptos)
            {
                var conceptoG1 = new ConceptoGasto
                {
                    Activo = true,
                    Concepto = concepto,
                    IdCentroCostos = centroCostosGastos.Id,
                    //TopePresupuestal = 5000
                    PagableEnCaja = true
                };

                _servicioConceptosGastos.CrearConceptoGasto(conceptoG1, new Modelo.Seguridad.Dtos.DtoUsuario());
            }
            #endregion
        }

        private TipoHabitacion GenerarNuevoTipo(int maximoReservaciones, string clave, string descripcion, string descripcionExt,
            decimal precioH, decimal precioPEH, decimal precioHEH, ConfiguracionTarifa configuracionHotel,
            decimal precioM = 0, decimal precioPEM = 0, decimal precioHEM = 0, ConfiguracionTarifa configuracionMotel = null)
        {
            IRepositorioParametros repoG = FabricaDependencias.Instancia.Resolver<IRepositorioParametros>();

            var config = repoG.Obtener(m => true/*m.Activa*/);

            var nuevoTipo = new TipoHabitacion
            {
                //Id = i,
                Activo = true,
                Clave = clave,
                ClaveSAT = "90111801",
                Descripcion = descripcion,
                DescripcionExtendida = descripcionExt,
                MinutosEntrada = 10,
                MinutosSucia = 30,
                MaximoReservaciones = maximoReservaciones,
                MaximoPersonasExtra = 2,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
            };

            nuevoTipo.TiemposLimpieza.Add(new TiempoLimpieza
            {
                Activa = true,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                TipoLimpieza = TareaLimpieza.TiposLimpieza.Normal,
                Minutos = 30
            });

            nuevoTipo.TiemposLimpieza.Add(new TiempoLimpieza
            {
                Activa = true,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                TipoLimpieza = TareaLimpieza.TiposLimpieza.Detallado,
                Minutos = 60
            });


            var configuracionTipoH = new ConfiguracionTipo
            {
                Activa = true,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdUsuarioCreo = 0,
                IdConfiguracionNegocio = configuracionHotel.Id,
                Precio = precioH / (1 + config.Iva_CatPar),
                PrecioPersonaExtra = precioPEH / (1 + config.Iva_CatPar),
                PrecioHospedajeExtra = precioHEH / (1 + config.Iva_CatPar),
                DuracionOEntrada = 15,
                HoraSalida = 12,
            };

            nuevoTipo.ConfiguracionesTipos.Add(configuracionTipoH);

            //if (nombres[i].Equals("Twin Suite"))
            //{
            if (configuracionMotel != null)
            {
                var configuracionTipoM = new ConfiguracionTipo
                {
                    Activa = true,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    IdUsuarioCreo = 0,
                    IdConfiguracionNegocio = configuracionMotel.Id,
                    Precio = precioM / (1 + config.Iva_CatPar),
                    PrecioPersonaExtra = precioPEM / (1 + config.Iva_CatPar),
                    PrecioHospedajeExtra = precioHEM / (1 + config.Iva_CatPar),
                    DuracionOEntrada = descripcion.ToUpper().Contains("VILLA") ? 5 : 8
                };

                configuracionTipoM.TiemposHospedaje.Add(new TiempoHospedaje
                {
                    Activo = true,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Minutos = 120,
                    Orden = 1,
                });

                configuracionTipoM.TiemposHospedaje.Add(new TiempoHospedaje
                {
                    Activo = true,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Minutos = 120,
                    Orden = 2
                });

                nuevoTipo.ConfiguracionesTipos.Add(configuracionTipoM);
            }
            //}

            return nuevoTipo;
        }

        List<Modelo.Seguridad.Entidades.Usuario> InicializarEmpleados(int idTurno)
        {
            var usuarioMaestroChido = new Modelo.Seguridad.Dtos.DtoUsuario
            {
                Permisos = new Modelo.Seguridad.Dtos.DtoPermisos()
            };

            usuarioMaestroChido.Permisos.AsignarValorABooleanos(true);

            List<Modelo.Seguridad.Entidades.Usuario> usuarios = new List<Modelo.Seguridad.Entidades.Usuario>();
            #region categorías

            IRepositorioCategoriasEmpleados _repositorioCategoriasEmpleados = FabricaDependencias.Instancia.Resolver<IRepositorioCategoriasEmpleados>();

            var categoriaRojo = new CategoriaEmpleado
            {
                Activa = true,
                Nombre = "Rojo",
                PorcentajeBonificacion = 0
            };

            var categoriaPlata = new CategoriaEmpleado
            {
                Activa = true,
                Nombre = "Plata",
                PorcentajeBonificacion = 0.05M
            };

            var categoriaOro = new CategoriaEmpleado
            {
                Activa = true,
                Nombre = "Oro",
                PorcentajeBonificacion = 0.15M
            };

            _repositorioCategoriasEmpleados.Agregar(categoriaRojo);
            _repositorioCategoriasEmpleados.Agregar(categoriaPlata);
            _repositorioCategoriasEmpleados.Agregar(categoriaOro);

            _repositorioCategoriasEmpleados.GuardarCambios();

            #endregion
            #region áreas

            IRepositorioAreas _repostorioAreas = FabricaDependencias.Instancia.Resolver<IRepositorioAreas>();

            var granArea = new Area
            {
                Nombre = "Gran área",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Activa = true
            };

            _repostorioAreas.Agregar(granArea);
            _repostorioAreas.GuardarCambios();

            #endregion
            #region puestos
            IRepositorioPuestos _repostorioPuestos = FabricaDependencias.Instancia.Resolver<IRepositorioPuestos>();

            var puestoAdmin = new Puesto
            {
                Nombre = "Administrador",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoRecepcionista = new Puesto
            {
                Nombre = "Recepcionista",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoVendedor = new Puesto
            {
                Nombre = "Vendedor",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoRecamareras = new Puesto
            {
                Nombre = "Recamarera",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoSupervisores = new Puesto
            {
                Nombre = "Supervisor",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoCocinero = new Puesto
            {
                Nombre = "Cocinero",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoMesero = new Puesto
            {
                Nombre = "Mesero",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoValet = new Puesto
            {
                Nombre = "Valet",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            var puestoGerenteOperativo = new Puesto
            {
                Nombre = "Gerente operativo",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Asignable = true,
                Activo = true,
                IdArea = granArea.Id
            };

            _repostorioPuestos.Agregar(puestoAdmin);
            _repostorioPuestos.Agregar(puestoRecepcionista);
            _repostorioPuestos.Agregar(puestoVendedor);
            _repostorioPuestos.Agregar(puestoRecamareras);
            _repostorioPuestos.Agregar(puestoSupervisores);
            _repostorioPuestos.Agregar(puestoCocinero);
            _repostorioPuestos.Agregar(puestoMesero);
            _repostorioPuestos.Agregar(puestoValet);
            _repostorioPuestos.Agregar(puestoGerenteOperativo);

            _repostorioPuestos.GuardarCambios();

            

            #endregion
            #region empleados

            IServicioEmpleados _servicioEmpleados = FabricaDependencias.Instancia.Resolver<IServicioEmpleados>();

            var empleadosTmp = LectorAgenda.ObtenerUsuarios();

            var mujeres = empleadosTmp.Where(m => m.Sexo == "M").Take(20).ToList();

            int numeroE = 1;

            #region administrador
            var admin = new Empleado
            {
                Activo = true,
                Nombre = "Eduardo",
                ApellidoPaterno = "Vázquez",
                ApellidoMaterno = "Rosado",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Habilitado = true,
                IdPuesto = puestoAdmin.Id,
                FechaIngreso = DateTime.Now,
                NSS = numeroE.ToString(),
                NumeroEmpleado = (numeroE++).ToString(),
                Salario = 10000,
                IdCategoria = categoriaOro.Id,
                IdTurno = idTurno
            };

            _servicioEmpleados.CrearEmpleado(admin, usuarioMaestroChido);
            puestoAdmin = _repostorioPuestos.Obtener(m => m.Id == puestoAdmin.Id);
            puestoAdmin.Asignable = false;
            _repostorioPuestos.Modificar(puestoAdmin);
            _repostorioPuestos.GuardarCambios();

            var nuevoUsuarioAdmin = new Modelo.Seguridad.Entidades.Usuario
            {
                Activo = true,
                Alias = "SISTEMAS",
                //Nombre = "Sistemas",
                //ApellidoPaterno = "Sistemas",
                //ApellidoMaterno = "Sistemas",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdEmpleado = admin.Id
            };

            nuevoUsuarioAdmin.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
            {
                EsAcceso = true,
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
            });

            usuarios.Add(nuevoUsuarioAdmin);
            #endregion
            #region recepcionista
            var recepcionista = new Empleado
            {
                Activo = true,
                Nombre = "Antonio",
                ApellidoPaterno = "Velásquez",
                ApellidoMaterno = "Ruiz",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Habilitado = true,
                IdPuesto = puestoRecepcionista.Id,
                FechaIngreso = DateTime.Now,
                NSS = numeroE.ToString(),
                NumeroEmpleado = (numeroE++).ToString(),
                Salario = 10000,
                IdCategoria = categoriaPlata.Id,
                IdTurno = idTurno
            };

            _servicioEmpleados.CrearEmpleado(recepcionista, usuarioMaestroChido);

            var nuevoUsuarioRecepcionista = new Modelo.Seguridad.Entidades.Usuario
            {
                Activo = true,
                Alias = "RECEPCIONISTA",
                //Nombre = "Sistemas",
                //ApellidoPaterno = "Sistemas",
                //ApellidoMaterno = "Sistemas",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdEmpleado = recepcionista.Id
            };

            nuevoUsuarioRecepcionista.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
            {
                EsAcceso = true,
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
            });

            usuarios.Add(nuevoUsuarioRecepcionista);
            #endregion
            #region vendedor
            var vendedor = new Empleado
            {
                Activo = true,
                Nombre = "Joaquin",
                ApellidoPaterno = "Fernández",
                ApellidoMaterno = "De la Rosa",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Habilitado = true,
                IdPuesto = puestoVendedor.Id,
                FechaIngreso = DateTime.Now,
                NSS = numeroE.ToString(),
                NumeroEmpleado = (numeroE++).ToString(),
                Salario = 10000,
                IdCategoria = categoriaPlata.Id,
                IdTurno = idTurno
            };

            _servicioEmpleados.CrearEmpleado(vendedor, usuarioMaestroChido);

            var nuevoUsuarioVendedor = new Modelo.Seguridad.Entidades.Usuario
            {
                Activo = true,
                Alias = "VENDEDOR",
                //Nombre = "Sistemas",
                //ApellidoPaterno = "Sistemas",
                //ApellidoMaterno = "Sistemas",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdEmpleado = vendedor.Id
            };

            nuevoUsuarioVendedor.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
            {
                EsAcceso = true,
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
            });

            usuarios.Add(nuevoUsuarioVendedor);
            #endregion
            #region gerenteOperativo
            var gerenteOperativo = new Empleado
            {
                Activo = true,
                Nombre = "Donaldo",
                ApellidoPaterno = "Rebolledo",
                ApellidoMaterno = "Jiménez",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Habilitado = true,
                IdPuesto = puestoGerenteOperativo.Id,
                FechaIngreso = DateTime.Now,
                NSS = numeroE.ToString(),
                NumeroEmpleado = (numeroE++).ToString(),
                Salario = 7000,
                IdCategoria = categoriaOro.Id,
                IdTurno = idTurno
            };

            _servicioEmpleados.CrearEmpleado(gerenteOperativo, usuarioMaestroChido);

            var nuevoUsuarioGerenteOperativo = new Modelo.Seguridad.Entidades.Usuario
            {
                Activo = true,
                Alias = "GERENTEOP",
                //Nombre = "Sistemas",
                //ApellidoPaterno = "Sistemas",
                //ApellidoMaterno = "Sistemas",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdEmpleado = gerenteOperativo.Id
            };

            nuevoUsuarioGerenteOperativo.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
            {
                EsAcceso = true,
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
            });

            usuarios.Add(nuevoUsuarioGerenteOperativo);
            #endregion
            int i = 1;
            foreach (var mujer in mujeres)
            {
                var empleado = new Empleado
                {
                    Activo = true,
                    Nombre = mujer.Nombre,
                    ApellidoPaterno = mujer.Apellido1,
                    ApellidoMaterno = mujer.Apellido2,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Habilitado = true,
                    IdPuesto = puestoRecamareras.Id,
                    FechaIngreso = DateTime.Now,
                    NSS = numeroE.ToString(),
                    NumeroEmpleado = (numeroE++).ToString(),
                    Salario = 1000,
                    IdCategoria = categoriaRojo.Id,
                    IdTurno = idTurno
                };

                _servicioEmpleados.CrearEmpleado(empleado, usuarioMaestroChido);


                var nuevoUsuario = new Modelo.Seguridad.Entidades.Usuario
                {
                    Activo = true,
                    Alias = puestoRecamareras.Nombre + "0" + (i++),
                    //Nombre = empleado.Nombre,
                    //ApellidoPaterno = empleado.ApellidoPaterno,
                    //ApellidoMaterno = empleado.ApellidoMaterno,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    IdEmpleado = empleado.Id
                };

                nuevoUsuario.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    //,
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
                });

                usuarios.Add(nuevoUsuario);

            }
            var restantes = empleadosTmp.Except(mujeres).ToList();

            int cantidadPuestos = restantes.Count / 4;
            int salto = 0;
            i = 1;
            foreach (var u in restantes.Skip(salto).Take(cantidadPuestos))
            {
                var empleado = new Empleado
                {
                    Activo = true,
                    Nombre = u.Nombre,
                    ApellidoPaterno = u.Apellido1,
                    ApellidoMaterno = u.Apellido2,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Habilitado = true,
                    IdPuesto = puestoSupervisores.Id,
                    FechaIngreso = DateTime.Now,
                    NSS = numeroE.ToString(),
                    NumeroEmpleado = (numeroE++).ToString(),
                    Salario = 1000,
                    IdCategoria = categoriaPlata.Id,
                    IdTurno = idTurno
                };


                _servicioEmpleados.CrearEmpleado(empleado, usuarioMaestroChido);


                var nuevoUsuario = new Modelo.Seguridad.Entidades.Usuario
                {
                    Activo = true,
                    Alias = puestoSupervisores.Nombre + "0" + (i++),
                    //Nombre = empleado.Nombre,
                    //ApellidoPaterno = empleado.ApellidoPaterno,
                    //ApellidoMaterno = empleado.ApellidoMaterno,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    IdEmpleado = empleado.Id
                };

                nuevoUsuario.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    //,
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
                });

                usuarios.Add(nuevoUsuario);
            }
            salto += cantidadPuestos;
            i = 1;
            foreach (var u in restantes.Skip(salto).Take(cantidadPuestos))
            {
                var empleado = new Empleado
                {
                    Activo = true,
                    Nombre = u.Nombre,
                    ApellidoPaterno = u.Apellido1,
                    ApellidoMaterno = u.Apellido2,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Habilitado = true,
                    IdPuesto = puestoCocinero.Id,
                    FechaIngreso = DateTime.Now,
                    NSS = numeroE.ToString(),
                    NumeroEmpleado = (numeroE++).ToString(),
                    Salario = 1000,
                    IdCategoria = categoriaRojo.Id,
                    IdTurno = idTurno
                };

                _servicioEmpleados.CrearEmpleado(empleado, usuarioMaestroChido);


                var nuevoUsuario = new Modelo.Seguridad.Entidades.Usuario
                {
                    Activo = true,
                    Alias = puestoCocinero.Nombre + "0" + (i++),
                    //Nombre = empleado.Nombre,
                    //ApellidoPaterno = empleado.ApellidoPaterno,
                    //ApellidoMaterno = empleado.ApellidoMaterno,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    IdEmpleado = empleado.Id
                };

                nuevoUsuario.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    //,
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
                });

                usuarios.Add(nuevoUsuario);
            }
            salto += cantidadPuestos;
            i = 1;
            foreach (var u in restantes.Skip(salto).Take(cantidadPuestos))
            {
                var empleado = new Empleado
                {
                    Activo = true,
                    Nombre = u.Nombre,
                    ApellidoPaterno = u.Apellido1,
                    ApellidoMaterno = u.Apellido2,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Habilitado = true,
                    IdPuesto = puestoMesero.Id,
                    FechaIngreso = DateTime.Now,
                    NSS = numeroE.ToString(),
                    NumeroEmpleado = (numeroE++).ToString(),
                    Salario = 1000,
                    IdCategoria = categoriaRojo.Id,
                    IdTurno = idTurno
                };

                _servicioEmpleados.CrearEmpleado(empleado, usuarioMaestroChido);


                var nuevoUsuario = new Modelo.Seguridad.Entidades.Usuario
                {
                    Activo = true,
                    Alias = puestoMesero.Nombre + "0" + (i++),
                    //Nombre = empleado.Nombre,
                    //ApellidoPaterno = empleado.ApellidoPaterno,
                    //ApellidoMaterno = empleado.ApellidoMaterno,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    IdEmpleado = empleado.Id
                };

                nuevoUsuario.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    //,
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
                });

                usuarios.Add(nuevoUsuario);
            }
            salto += cantidadPuestos;
            i = 1;
            foreach (var u in restantes.Skip(salto).Take(cantidadPuestos))
            {
                var empleado = new Empleado
                {
                    Activo = true,
                    Nombre = u.Nombre,
                    ApellidoPaterno = u.Apellido1,
                    ApellidoMaterno = u.Apellido2,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Habilitado = true,
                    IdPuesto = puestoValet.Id,
                    FechaIngreso = DateTime.Now,
                    NSS = numeroE.ToString(),
                    NumeroEmpleado = (numeroE++).ToString(),
                    Salario = 1000,
                    IdCategoria = categoriaRojo.Id,
                    IdTurno = idTurno
                };

                _servicioEmpleados.CrearEmpleado(empleado, usuarioMaestroChido);


                var nuevoUsuario = new Modelo.Seguridad.Entidades.Usuario
                {
                    Activo = true,
                    Alias = puestoValet.Nombre + "0" + (i++),
                    //Nombre = empleado.Nombre,
                    //ApellidoPaterno = empleado.ApellidoPaterno,
                    //ApellidoMaterno = empleado.ApellidoMaterno,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    IdEmpleado = empleado.Id
                };

                nuevoUsuario.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                {
                    //,
                    TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                    Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
                });

                usuarios.Add(nuevoUsuario);
            }

            salto += cantidadPuestos;

            //_repostorioEmpleados.GuardarCambios();

            #endregion

            return usuarios;
        }

        List<Modelo.Seguridad.Entidades.Usuario> InicializarEmpleadosV2(int idTurno)
        {
            var usuarioMaestroChido = new Modelo.Seguridad.Dtos.DtoUsuario
            {
                Permisos = new Modelo.Seguridad.Dtos.DtoPermisos()
            };

            usuarioMaestroChido.Permisos.AsignarValorABooleanos(true);

            List<Modelo.Seguridad.Entidades.Usuario> usuarios = new List<Modelo.Seguridad.Entidades.Usuario>();
            #region categorías

            IRepositorioCategoriasEmpleados _repositorioCategoriasEmpleados = FabricaDependencias.Instancia.Resolver<IRepositorioCategoriasEmpleados>();

            var categoriaRojo = new CategoriaEmpleado
            {
                Activa = true,
                Nombre = "Rojo",
                PorcentajeBonificacion = 0
            };

            var categoriaPlata = new CategoriaEmpleado
            {
                Activa = true,
                Nombre = "Plata",
                PorcentajeBonificacion = 0.05M
            };

            var categoriaOro = new CategoriaEmpleado
            {
                Activa = true,
                Nombre = "Oro",
                PorcentajeBonificacion = 0.15M
            };

            _repositorioCategoriasEmpleados.Agregar(categoriaRojo);
            _repositorioCategoriasEmpleados.Agregar(categoriaPlata);
            _repositorioCategoriasEmpleados.Agregar(categoriaOro);

            _repositorioCategoriasEmpleados.GuardarCambios();

            #endregion
            #region áreas

            IRepositorioAreas _repostorioAreas = FabricaDependencias.Instancia.Resolver<IRepositorioAreas>();

            var granArea = new Area
            {
                Nombre = "Gran área",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Activa = true
            };

            _repostorioAreas.Agregar(granArea);
            _repostorioAreas.GuardarCambios();

            #endregion
            #region puestos
            IRepositorioPuestos _repostorioPuestos = FabricaDependencias.Instancia.Resolver<IRepositorioPuestos>();

            var roles = UnitTestImportarPermisos.ImportarYGenerarPermisos();

            Puesto puestoAdmin = null;

            List<Puesto> puestos = new List<Puesto>();

            foreach (var rol in roles) 
            {
                var puesto = new Puesto
                {
                    Nombre = rol.Nombre,
                    FechaCreacion = DateTime.Now,
                    FechaModificacion = DateTime.Now,
                    Asignable = rol.Nombre.Trim().ToUpper() != "ADMINISTRADOR",
                    Activo = true,
                    IdArea = granArea.Id
                };

                if (!puesto.Asignable)
                    puestoAdmin = puesto;
                else
                    puestos.Add(puesto);

                _repostorioPuestos.Agregar(puesto);
            }

            _repostorioPuestos.GuardarCambios();

            IRepositorioPuestosMaestros _repostorioPuestosMaestros = FabricaDependencias.Instancia.Resolver<IRepositorioPuestosMaestros>();

            _repostorioPuestosMaestros.Agregar(new PuestosMaestros
            {
                Mesero = puestos.First(m => m.Nombre.Trim().ToUpper().Equals("MESEROS")).Id,
                Recamarera = puestos.First(m => m.Nombre.Trim().ToUpper().Equals("RECAMARISTAS")).Id,
                Supervisor = puestos.First(m => m.Nombre.Trim().ToUpper().Equals("SUPERVISORES")).Id,
                Valet = puestos.First(m => m.Nombre.Trim().ToUpper().Equals("VALETS")).Id,
                Vendedor = puestos.First(m => m.Nombre.Trim().ToUpper().Equals("VENDEDORES")).Id
            });

            _repostorioPuestosMaestros.GuardarCambios();

            #endregion
            #region empleados

            IServicioEmpleados _servicioEmpleados = FabricaDependencias.Instancia.Resolver<IServicioEmpleados>();

            var empleadosTmp = LectorAgenda.ObtenerUsuarios();

            int numeroE = 1;

            #region administrador
            var admin = new Empleado
            {
                Activo = true,
                Nombre = "Eduardo",
                ApellidoPaterno = "Vázquez",
                ApellidoMaterno = "Rosado",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Habilitado = true,
                IdPuesto = puestoAdmin.Id,
                FechaIngreso = DateTime.Now,
                NSS = numeroE.ToString(),
                NumeroEmpleado = (numeroE++).ToString(),
                Salario = 10000,
                IdCategoria = categoriaOro.Id,
                IdTurno = idTurno
            };

            _servicioEmpleados.CrearEmpleado(admin, usuarioMaestroChido);
            puestoAdmin = _repostorioPuestos.Obtener(m => m.Id == puestoAdmin.Id);
            puestoAdmin.Asignable = false;
            _repostorioPuestos.Modificar(puestoAdmin);
            _repostorioPuestos.GuardarCambios();

            var nuevoUsuarioAdmin = new Modelo.Seguridad.Entidades.Usuario
            {
                Activo = true,
                Alias = "SISTEMAS",
                //Nombre = "Sistemas",
                //ApellidoPaterno = "Sistemas",
                //ApellidoMaterno = "Sistemas",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                IdEmpleado = admin.Id
            };

            nuevoUsuarioAdmin.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
            {
                EsAcceso = true,
                TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
            });

            usuarios.Add(nuevoUsuarioAdmin);
            #endregion

#if !DEBUG_TEST_CLIENTE
            int i = 1;


            int cantidadPuestos = empleadosTmp.Count / puestos.Count;
            int salto = 0;
            i = 1;

            foreach (var puesto in puestos)
            {
                foreach (var u in empleadosTmp.Skip(salto).Take(cantidadPuestos))
                {
                    var empleado = new Empleado
                    {
                        Activo = true,
                        Nombre = u.Nombre,
                        ApellidoPaterno = u.Apellido1,
                        ApellidoMaterno = u.Apellido2,
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        Habilitado = true,
                        IdPuesto = puesto.Id,
                        FechaIngreso = DateTime.Now,
                        NSS = numeroE.ToString(),
                        NumeroEmpleado = (numeroE++).ToString(),
                        Salario = 1000,
                        IdCategoria = categoriaPlata.Id,
                        IdTurno = idTurno
                    };


                    _servicioEmpleados.CrearEmpleado(empleado, usuarioMaestroChido);


                    var nuevoUsuario = new Modelo.Seguridad.Entidades.Usuario
                    {
                        Activo = true,
                        Alias = puesto.Nombre + "0" + (i++),
                        //Nombre = empleado.Nombre,
                        //ApellidoPaterno = empleado.ApellidoPaterno,
                        //ApellidoMaterno = empleado.ApellidoMaterno,
                        FechaCreacion = DateTime.Now,
                        FechaModificacion = DateTime.Now,
                        IdEmpleado = empleado.Id
                    };

                    nuevoUsuario.Credenciales.Add(new Modelo.Seguridad.Entidades.Credencial
                    {
                        //,
                        TipoCredencial = Modelo.Seguridad.Entidades.Credencial.TiposCredencial.Contrasena,
                        Valor = global::Transversal.Seguridad.Encriptador.Encriptar("Pass123")
                    });

                    usuarios.Add(nuevoUsuario);
                }
                salto += cantidadPuestos;
                i = 1;

            }

            //_repostorioEmpleados.GuardarCambios();

#endif
            #endregion

            return usuarios;
        }

        void InicializarArticulosYPropinas()
        {
            #region departamentos

            IRepositorioDepartamentos _repostorioDepartamentos = FabricaDependencias.Instancia.Resolver<IRepositorioDepartamentos>();

            var departamentoBar = new Departamento
            {
                Desc_Dpto = "Bar"
            };

            var departamentoCocina = new Departamento
            {
                Desc_Dpto = "Cocina"
            };

            var departamentoLavanderia = new Departamento
            {
                Desc_Dpto = "Lavandería"
            };

            _repostorioDepartamentos.Agregar(departamentoBar);
            _repostorioDepartamentos.Agregar(departamentoCocina);
            _repostorioDepartamentos.Agregar(departamentoLavanderia);

            _repostorioDepartamentos.GuardarCambios();
            #endregion
            #region lineas
            IRepositorioLineas _repostorioLineas = FabricaDependencias.Instancia.Resolver<IRepositorioLineas>();

            var tipoAlimentos = new Linea
            {
                Activa = true,
                Desc_Linea = "Alimentos",
                //FechaCreacion = DateTime.Now,
                //FechaModificacion = DateTime.Now,
                Cod_Dpto = departamentoCocina.Cod_Dpto,
                LineaBase = Linea.LineasBase.Alimentos,
                Comandable = true
            };

            var tipoBebidas = new Linea
            {
                Activa = true,
                Desc_Linea = "Bebidas",
                //FechaCreacion = DateTime.Now,
                //FechaModificacion = DateTime.Now,
                Cod_Dpto = departamentoBar.Cod_Dpto,
                LineaBase = Linea.LineasBase.Bebidas,
                Comandable = true
            };

            var tipoSpa = new Linea
            {
                Activa = true,
                Desc_Linea = "Spa & Sex",
                //FechaCreacion = DateTime.Now,
                //FechaModificacion = DateTime.Now,
                Cod_Dpto = departamentoBar.Cod_Dpto,
                LineaBase = Linea.LineasBase.SpaYSex,
                Comandable = true
            };

            var lineaToallas = new Linea
            {
                Activa = true,
                Desc_Linea = "Toallas",
                //FechaCreacion = DateTime.Now,
                //FechaModificacion = DateTime.Now,
                Cod_Dpto = departamentoLavanderia.Cod_Dpto
            };

            var lineaJabones = new Linea
            {
                Activa = true,
                Desc_Linea = "Jabones",
                //FechaCreacion = DateTime.Now,
                //FechaModificacion = DateTime.Now,
                Cod_Dpto = departamentoLavanderia.Cod_Dpto
            };

            _repostorioLineas.Agregar(tipoAlimentos);
            _repostorioLineas.Agregar(tipoBebidas);
            _repostorioLineas.Agregar(tipoSpa);
            _repostorioLineas.Agregar(lineaToallas);
            _repostorioLineas.Agregar(lineaJabones);

            _repostorioLineas.GuardarCambios();

            #endregion
            #region presentaciones

            IRepositorioPresentaciones _repositorioPresentaciones = FabricaDependencias.Instancia.Resolver<IRepositorioPresentaciones>();

            var presentacionUnidad = new Presentacion
            {
                CodigoSAT = "",
                presentacion = "Unidad",
            };

            _repositorioPresentaciones.Agregar(presentacionUnidad);

            var presentacionKG = new Presentacion
            {
                CodigoSAT = "",
                presentacion = "Kg",
            };

            _repositorioPresentaciones.Agregar(presentacionKG);
            _repositorioPresentaciones.GuardarCambios();

            #endregion
            #region tipos de artículos

            IRepositorioTiposArticulos _repositorioTiposArticulos = FabricaDependencias.Instancia.Resolver<IRepositorioTiposArticulos>();

            var primerTipo = _repositorioTiposArticulos.Obtener(m => m.Desc_TpArt == "INSUMOS");
            #endregion
            #region articulos
            
#if !DEBUG_TEST_CLIENTE

            IRepositorioArticulos _repositorioArticulos = FabricaDependencias.Instancia.Resolver<IRepositorioArticulos>();

            for (int i = 1; i <= 100; i++)
            {
                _repositorioArticulos.Agregar(new Articulo
                {
                    CodigoSAT = "50121903",
                    //Activo = true,
                    Prec_Art = 200.5m + i,
                    Cos_Art = 50m,
                    //Descripcion = "Un rico caldo",
                    Cod_Art = "A" + i,
                    Desc_Art = "Caldo de camarón " + i,
                    caracteristicas = "Un rico caldo",
                    descuento = 0,
                    Cod_TpArt = primerTipo.Cod_TpArt,
                    Cod_Dpto = tipoAlimentos.Cod_Dpto,
                    //FechaCreacion = DateTime.Now,
                    //FechaModificacion = DateTime.Now,
                    Cod_Linea = tipoAlimentos.Cod_Linea,
                    id_presentacion = presentacionUnidad.id,
                    uuid = Guid.NewGuid()
                });

                _repositorioArticulos.Agregar(new Articulo
                {
                    CodigoSAT = "50202306",
                    //Activo = true,
                    Prec_Art = 200.5m + i,
                    Cos_Art = 50m,
                    //Descripcion = "Una coca bien helada",
                    Cod_Art = "B" + i,
                    Desc_Art = "Coca cola 2.5lts " + i,
                    caracteristicas = "Una coca bien helada",
                    descuento = 0,
                    Cod_TpArt = primerTipo.Cod_TpArt,
                    //FechaCreacion = DateTime.Now,
                    //FechaModificacion = DateTime.Now,
                    Cod_Linea = tipoBebidas.Cod_Linea,
                    Cod_Dpto = tipoBebidas.Cod_Dpto,
                    id_presentacion = presentacionUnidad.id
                });

                _repositorioArticulos.Agregar(new Articulo
                {
                    CodigoSAT = "10302471",
                    //Activo = true,
                    Prec_Art = 200.5m + i,
                    Cos_Art = 50m,
                    //Descripcion = "Un juguete locochón",
                    Cod_Art = "S" + i,
                    Desc_Art = "Juguete travieso " + i,
                    caracteristicas = "Un juguete locochón",
                    descuento = 0,
                    Cod_TpArt = primerTipo.Cod_TpArt,
                    //FechaCreacion = DateTime.Now,
                    //FechaModificacion = DateTime.Now,
                    Cod_Linea = tipoSpa.Cod_Linea,
                    Cod_Dpto = tipoSpa.Cod_Dpto,
                    id_presentacion = presentacionUnidad.id
                });

                _repositorioArticulos.Agregar(new Articulo
                {
                    //Activo = true,
                    Prec_Art = 20.5m + i,
                    Cos_Art = 50m,
                    caracteristicas = "Un juguete locochón",
                    descuento = 0,
                    Cod_Art = "TOA" + i,
                    Desc_Art = "Toalla " + i,
                    Cod_TpArt = primerTipo.Cod_TpArt,
                    //FechaCreacion = DateTime.Now,
                    //FechaModificacion = DateTime.Now,
                    Cod_Linea = lineaToallas.Cod_Linea,
                    Cod_Dpto = lineaToallas.Cod_Dpto,
                    id_presentacion = presentacionUnidad.id
                });

                _repositorioArticulos.Agregar(new Articulo
                {
                    //Activo = true,
                    Prec_Art = 20.5m + i,
                    Cos_Art = 50m,
                    caracteristicas = "Un juguete locochón",
                    descuento = 0,
                    Cod_Art = "JAB" + i,
                    Desc_Art = "Jabón " + i,
                    Cod_TpArt = primerTipo.Cod_TpArt,
                    //FechaCreacion = DateTime.Now,
                    //FechaModificacion = DateTime.Now,
                    Cod_Linea = lineaJabones.Cod_Linea,
                    Cod_Dpto = lineaJabones.Cod_Dpto,
                    id_presentacion = presentacionUnidad.id
                });
            }
            _repositorioArticulos.GuardarCambios();

#endif
            #endregion
            #region configuraciones propinas

            IRepositorioConfiguracionesPropinas _repositorioConfiguracionesPropinases = FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesPropinas>();

            var configuracionPropina = new ConfiguracionPropinas
            {
                Activa = true,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now
            };

            configuracionPropina.PorcentajesPropinaTipo.Add(new PorcentajePropinaTipo
            {
                Activo = true,
                IdLineaArticulo = tipoAlimentos.Cod_Linea,
                Porcentaje = 0.08M
            });

            configuracionPropina.PorcentajesPropinaTipo.Add(new PorcentajePropinaTipo
            {
                Activo = true,
                IdLineaArticulo = tipoBebidas.Cod_Linea,
                Porcentaje = 0.08M
            });

            configuracionPropina.PorcentajesPropinaTipo.Add(new PorcentajePropinaTipo
            {
                Activo = true,
                IdLineaArticulo = tipoSpa.Cod_Linea,
                Porcentaje = 0.05M
            });


            configuracionPropina.PorcentajesTarjeta.Add(new PorcentajeTarjeta
            {
                Activo = true,
                Porcentaje = 0.05M,
                TipoTarjeta = TiposTarjeta.Visa
            });

            configuracionPropina.PorcentajesTarjeta.Add(new PorcentajeTarjeta
            {
                Activo = true,
                Porcentaje = 0.05M,
                TipoTarjeta = TiposTarjeta.MasterCard
            });

            configuracionPropina.PorcentajesTarjeta.Add(new PorcentajeTarjeta
            {
                Activo = true,
                Porcentaje = 0.07M,
                TipoTarjeta = TiposTarjeta.AmericanExpress
            });


            for (int i = 1; i <= 7; i++)
                configuracionPropina.FondosDia.Add(new FondoDia
                {
                    Activo = true,
                    Dia = i,
                    Precio = i == 7 ? 20 : 100
                });

            _repositorioConfiguracionesPropinases.Agregar(configuracionPropina);
            _repositorioConfiguracionesPropinases.GuardarCambios();
            #endregion
            IRepositorioDepartamentosMaestros _repositorioDepartamentosMaestroses = FabricaDependencias.Instancia.Resolver<IRepositorioDepartamentosMaestros>();

            _repositorioDepartamentosMaestroses.Agregar(new DepartamentosMaestros
            {
                Bar = departamentoBar.Cod_Dpto,
                Cocina = departamentoCocina.Cod_Dpto,
                Lavanderia = departamentoLavanderia.Cod_Dpto
            });

            _repositorioDepartamentosMaestroses.GuardarCambios();

            #region premios
            
#if !DEBUG_TEST_CLIENTE

            IRepositorioPremios _repositorioPremios = FabricaDependencias.Instancia.Resolver<IRepositorioPremios>();

            var art = _repositorioArticulos.Obtener(m => m.Desc_Art == "Caldo de camarón 1");

            var premio = new Premio
            {
                Activa = true,
                Descripcion = "Un premio",
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Numero = "2"
            };

            premio.ArticulosPremio.Add(new ArticuloPremio
            {
                Activo = true,
                Cantidad = 2,
                CodigoArticulo = art.Cod_Art,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now
            });

            _repositorioPremios.Agregar(premio);
            _repositorioPremios.GuardarCambios();
            
#endif

            #endregion
        }

        void InicializarMesas()
        {
            IRepositorioMesas _repositorioMesas = FabricaDependencias.Instancia.Resolver<IRepositorioMesas>();

            #region Fila T

            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T1",
                Fila = 1,
                Columna = 1,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T2",
                Fila = 1,
                Columna = 2,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T3",
                Fila = 1,
                Columna = 3,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T4",
                Fila = 1,
                Columna = 4,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T5",
                Fila = 1,
                Columna = 5,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T6",
                Fila = 1,
                Columna = 6,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T7",
                Fila = 1,
                Columna = 7,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "T8",
                Fila = 1,
                Columna = 8,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });

            #endregion
            #region Fila M

            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "M1",
                Fila = 2,
                Columna = 1,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "M2",
                Fila = 2,
                Columna = 2,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "M3",
                Fila = 2,
                Columna = 3,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "M4",
                Fila = 2,
                Columna = 4,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "M5",
                Fila = 2,
                Columna = 5,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "M6",
                Fila = 2,
                Columna = 6,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "**1",
                Fila = 2,
                Columna = 7,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "M8",
                Fila = 2,
                Columna = 8,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });

            #endregion
            #region Fila B

            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "B1",
                Fila = 3,
                Columna = 1,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "B2",
                Fila = 3,
                Columna = 2,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "B3",
                Fila = 3,
                Columna = 3,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "B4",
                Fila = 3,
                Columna = 4,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "B5",
                Fila = 3,
                Columna = 5,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "B6",
                Fila = 3,
                Columna = 6,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "**2",
                Fila = 3,
                Columna = 7,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "**3",
                Fila = 3,
                Columna = 8,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });

            #endregion
            #region Fila R

            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "R1",
                Fila = 4,
                Columna = 1,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "R2",
                Fila = 4,
                Columna = 2,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "R3",
                Fila = 4,
                Columna = 3,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "R4",
                Fila = 4,
                Columna = 4,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "R5",
                Fila = 4,
                Columna = 5,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "R6",
                Fila = 4,
                Columna = 6,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "R7",
                Fila = 4,
                Columna = 7,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });
            _repositorioMesas.Agregar(new Mesa
            {
                Activa = true,
                Clave = "**4",
                Fila = 4,
                Columna = 8,
                FechaCreacion = DateTime.Now,
                FechaModificacion = DateTime.Now,
                Estado = Mesa.EstadosMesa.Libre
            });

            #endregion

            _repositorioMesas.GuardarCambios();
        }

        void InicializarCorte(DateTime fechaInicial, int idConfiguracionCorte) 
        {
            IRepositorioCortesTurno _repositorioCortesTurno = FabricaDependencias.Instancia.Resolver<IRepositorioCortesTurno>();

            var nuevoCorte = new CorteTurno
            {
                FechaInicio = fechaInicial,
                NumeroCorte = 1,
                IdConfiguracionTurno = idConfiguracionCorte,
                Estado = CorteTurno.Estados.Abierto
            };

            _repositorioCortesTurno.Agregar(nuevoCorte);
            _repositorioCortesTurno.GuardarCambios();
        }
    }
}
