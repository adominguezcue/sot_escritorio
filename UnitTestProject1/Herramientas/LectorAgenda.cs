﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestProject1.DtosPruebas;
using Newtonsoft.Json;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1.Herramientas
{
    public class LectorAgenda
    {
        public static List<DtoUsuarioPruebas> ObtenerUsuarios()
        {
            string valor;

            using (var fs = File.OpenText("agenda.json"))
                valor = fs.ReadToEnd();

            return JsonConvert.DeserializeObject<List<DtoUsuarioPruebas>>(valor);
        }
    }
}