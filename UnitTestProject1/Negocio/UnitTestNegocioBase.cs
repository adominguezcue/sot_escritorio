﻿using Negocio.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transversal.Dependencias;

namespace UnitTestProject1.Negocio
{
    public class UnitTestNegocioBase
    {
        protected IServicioVPoints NegocioServicioVpoints
        {
            get { return _negocioServicioVpoints.Value; }
        }

        private Lazy<IServicioVPoints> _negocioServicioVpoints = new Lazy<IServicioVPoints>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioVPoints>();
        });

        protected Modelo.Seguridad.Dtos.DtoUsuario ObtenerUsuarioValido()
        {
            return new Modelo.Seguridad.Dtos.DtoUsuario();
        }
    }
}
