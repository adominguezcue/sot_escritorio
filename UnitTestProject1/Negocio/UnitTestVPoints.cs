﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Negocio.VPoints;
using Transversal.Dependencias;
using Negocio.ServiciosExternos.Agentes.VPoints;

namespace UnitTestProject1.Negocio
{
    [TestClass]
    public class UnitTestVPoints : UnitTestNegocioBase
    {
        [TestMethod]
        public void ObtenerSaldoTarjetaV()
        {
            var saldo = NegocioServicioVpoints.ObtenerSaldoTarjetaV("0105501", ObtenerUsuarioValido());
            //saldo = NegocioServicioVpoints.ObtenerSaldoTarjetaV("1050105", ObtenerUsuarioValido());
        }

        [TestMethod]
        public void ObtenerCupon()
        {
            //var datos = NegocioServicioVpoints.ObtenerCupon("4234", ObtenerUsuarioValido());
            var datos = NegocioServicioVpoints.ObtenerCupon("6707", ObtenerUsuarioValido());
            datos = NegocioServicioVpoints.ObtenerCupon("4749", ObtenerUsuarioValido());
            datos = NegocioServicioVpoints.ObtenerCupon("4750", ObtenerUsuarioValido());
            datos = NegocioServicioVpoints.ObtenerCupon("4751", ObtenerUsuarioValido());
            datos = NegocioServicioVpoints.ObtenerCupon("4752", ObtenerUsuarioValido());
            datos = NegocioServicioVpoints.ObtenerCupon("4753", ObtenerUsuarioValido());
        }

        [TestMethod]
        public void RegistrarTarjeta() 
        {
            //NegocioServicioVpoints.VenderTarjeta("1050105", "5153", ObtenerUsuarioValido());
        }

        [TestMethod]
        public void AcumularPuntosDetalle()
        {
            var agenteV = new AgenteWsVPoints();

            agenteV.AcumularPuntosDetalle("webservice@vmotelboutique-rewards.com", "XqRwhnet5gnZBuQe", "0105501", "91909", 10000000, 2000000, 2000000, 2000000, 2000000, 2000000, "Sucursal X");
        }

        [TestMethod]
        public void DescontarPuntosDetalle()
        {
            var agenteV = new AgenteWsVPoints();

            agenteV.DescontarPuntos("webservice@vmotelboutique-rewards.com", "XqRwhnet5gnZBuQe", "0105501", Guid.NewGuid().ToString(), -10000000, "Sucursal X");
        }

        //[TestMethod]
        //public void ObtenerSaldo()
        //{
        //    var agenteV = new AgenteWsVPoints("http://www.vmotelboutique-rewards.com/webServiceTest/server.php");

        //    var saldo = agenteV.ObtenerSaldo("webservice@vmotelboutique-rewards.com", "XqRwhnet5gnZBuQe", "0105501");
        //}
    }
}
