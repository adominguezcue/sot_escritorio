﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Negocio.Rastrilleo;
using Transversal.Dependencias;
using serviciosE = Negocio.ServiciosExternos;
namespace UnitTestProject1.Negocio
{
    [TestClass]
    public class UnitTestRastrilleo
    {
        [TestMethod]
        public void EnviarOrden()
        {
            IServicioRastrilleo sr = FabricaDependencias.Instancia.Resolver<IServicioRastrilleo>();

            var pedidos = new serviciosE.wsRastrilleo.MastPedido[1];

            pedidos[0] = new serviciosE.wsRastrilleo.MastPedido
            {
                Folio = "0000",
                Sucursal = "BOUTIQUE VIADUCTO S DE RL DE CV", 
                NoFactura = ""
            };

            pedidos[0].Cliente = new serviciosE.wsRastrilleo.Cliente
            {
                Calle = "", 
                Ciudad = "", 
                CodigoPostal = "", 
                Colonia = "", 
                Estado = "", 
                NoExterior = "", 
                NoInterior = "", 
                Nombre = "", 
                Pais = "", 
                RFC = "", 
                TipoPersona = "F",
                Mail = "email@email.com", 
            };


            pedidos[0].Pago = new serviciosE.wsRastrilleo.Pago[1];
            pedidos[0].Pago[0] = new serviciosE.wsRastrilleo.Pago
            {
                Total = 232,
                NoDeTransaccion = Guid.NewGuid().ToString(),
                TipoPago = 1, //Efectivo 
                NoDeCuenta = ""
            };

            pedidos[0].DetPedido = new serviciosE.wsRastrilleo.DetPedido[1];
            pedidos[0].DetPedido[0] = new serviciosE.wsRastrilleo.DetPedido
            {
                Cantidad = 2,
                Codigo = "Art1",
                Descripcion = "Un artículo",
                Precio = 100,
                IVA = 16,
                Total = 232
            };

            var resultado = sr.EnviarOrden("0000", pedidos);
        }

        [TestMethod]
        public void MarcarOrden()
        {
            IServicioRastrilleo sr = FabricaDependencias.Instancia.Resolver<IServicioRastrilleo>();

            var pedidos = new serviciosE.wsRastrilleo.MastPedido[1];

            pedidos[0] = new serviciosE.wsRastrilleo.MastPedido
            {
                Folio = "0000",
                Sucursal = "BOUTIQUE VIADUCTO S DE RL DE CV",
                NoFactura = ""
            };

            pedidos[0].Cliente = new serviciosE.wsRastrilleo.Cliente
            {
                Calle = "",
                Ciudad = "",
                CodigoPostal = "",
                Colonia = "",
                Estado = "",
                NoExterior = "",
                NoInterior = "",
                Nombre = "",
                Pais = "",
                RFC = "",
                TipoPersona = "F"
            };


            pedidos[0].Pago = new serviciosE.wsRastrilleo.Pago[1];
            pedidos[0].Pago[0] = new serviciosE.wsRastrilleo.Pago
            {
                Total = 232,
                NoDeTransaccion = Guid.NewGuid().ToString(),
                TipoPago = 1, //Efectivo 
                NoDeCuenta = ""
            };

            pedidos[0].DetPedido = new serviciosE.wsRastrilleo.DetPedido[1];
            pedidos[0].DetPedido[0] = new serviciosE.wsRastrilleo.DetPedido
            {
                Cantidad = 2,
                Codigo = "Art1",
                Descripcion = "Un artículo",
                Precio = 100,
                IVA = 16,
                Total = 232
            };

            var resultado = sr.MarcarOrden("0000", pedidos);
        }

        [TestMethod]
        public void ObtenerRastrilleadas()
        {
            IServicioRastrilleo sr = FabricaDependencias.Instancia.Resolver<IServicioRastrilleo>();

            var resultado = sr.ObtenerRastrilleadas("0000");
        }

        [TestMethod]
        public void Rastrillar()
        {
            IServicioRastrilleo sr = FabricaDependencias.Instancia.Resolver<IServicioRastrilleo>();

            sr.Sincronizar();
        }
    }
}
