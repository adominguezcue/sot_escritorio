﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using serviciosE = Negocio.ServiciosExternos;
using Transversal.Dependencias;
using Negocio.Sincronizacion;
using Modelo.Repositorios;
using System.Linq;

namespace UnitTestProject1.Negocio
{
    [TestClass]
    public class UnitTestSincronizacion
    {
        [TestMethod]
        public void TestMethod1()
        {
            var x = new serviciosE.Agentes.Sincronizacion.AgenteWsSincronizacion();

            var cortes = new serviciosE.wsSincronizacion.Corte[1];
            cortes[0] = new serviciosE.wsSincronizacion.Corte
            {

            };

            var resultado = x.CargarCorteCaja(cortes);
        }

        [TestMethod]
        public void Sincronizar() 
        {
            var servicio = FabricaDependencias.Instancia.Resolver<IServicioSincronizacion>();

            servicio.Sincronizar();
        }

        [TestMethod]
        public void SincronizarGastos()
        {
            var servicio = FabricaDependencias.Instancia.Resolver<IServicioSincronizacion>();

            servicio.SincronizarGastos();
        }

        [TestMethod]
        public void SincronizarEfectivo()
        {
            var servicio = FabricaDependencias.Instancia.Resolver<IServicioSincronizacion>();

            servicio.SincronizarEfectivo();
        }

        [TestMethod]
        public void SincronizarHabitaciones() 
        {
            var repoH = FabricaDependencias.Instancia.Resolver<IRepositorioTiposHabitacion>();
            var hs = repoH.ObtenerElementos(m=> m.Activo).ToList();

            var arrhs = new global::Negocio.ServiciosExternos.wsSincronizacion.Habitacion[hs.Count];
            
            int i = 0;
            foreach (var h in hs)
                arrhs[i++] = new serviciosE.wsSincronizacion.Habitacion
                {
                    Codigo = h.Clave + "XD",
                    Nombre = h.Descripcion + "XD",
                    Sucursal = "V VIADUCTO",
                    Precio = 30 + i,
                    PrecioHotel = 50 + i
                };



            var servicio = new global::Negocio.ServiciosExternos.Agentes.Sincronizacion.AgenteWsSincronizacion();
            var rr= servicio.CatalogoHabitaciones(arrhs, 2);
        }
    }
}
