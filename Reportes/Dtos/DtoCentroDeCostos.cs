﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportesSOT.Dtos
{
  public class DtoCentroDeCostos
    {
        public string NombreCentroDeCostos { get; set; }

        public string Gastos { get; set; }
    }
}
