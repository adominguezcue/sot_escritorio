﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportesSOT.OrdenesCompra
{
    public class OrdenesServicio_Constantes
    {
        public static readonly string TABLA_ALMACENES = "Almacenes";
        public static readonly string TABLA_SERVICIOS_ORDEN_COMPRA = "Servicios";
        public static readonly string TABLA_ENCABEZADO_ORDEN_COMPRA = "EncabezadoOrdenCompra";
        public static readonly string TABLA_PROVEEDOR = "Proveedor";
        public static readonly string TABLA_PARAMETROS = "Parametros";
        public static readonly string TABLA_ENCABEZADO_REPORTE = "EncabezadoReporte";
        public static readonly string TABLA_CENTRODECOSTOS = "CentroDeCostos";
    }
}
