﻿using Modelo.Seguridad.Dtos;
using System;
namespace Negocio.Almacen.UbicacionValores
{
    public interface IServicioUbicacionValores
    {
        void CreaDetalleUbicacion(Modelo.Almacen.Entidades.ZctEncUbicacionValores Encabezado);
        string FolioEncabezado();
        decimal Total(DateTime fecha);
        void Grabar(Modelo.Almacen.Entidades.ZctEncUbicacionValores EnCabezado);
        Modelo.Almacen.Entidades.ZctEncUbicacionValores ValidaFecha(DateTime Fecha, DtoUsuario usuario);
    }
}
