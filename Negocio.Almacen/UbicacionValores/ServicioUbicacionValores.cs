﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.UbicacionValores
{
    public class ServicioUbicacionValores : Negocio.Almacen.UbicacionValores.IServicioUbicacionValores
    {
        IRepositorioZctEncUbicacionValores RepositorioZctEncUbicacionValores
        {
            get {return _repositorioZctEncUbicacionValores.Value; }
        }

        Lazy<IRepositorioZctEncUbicacionValores> _repositorioZctEncUbicacionValores = new Lazy<IRepositorioZctEncUbicacionValores>(()=> FabricaDependencias.Instancia.Resolver<IRepositorioZctEncUbicacionValores>());

        IRepositorioZctCatUbicacionValores RepositorioZctCatUbicacionValores
        {
            get { return _repositorioZctCatUbicacionValores.Value; }
        }

        Lazy<IRepositorioZctCatUbicacionValores> _repositorioZctCatUbicacionValores = new Lazy<IRepositorioZctCatUbicacionValores>(() => FabricaDependencias.Instancia.Resolver<IRepositorioZctCatUbicacionValores>());

        public ZctEncUbicacionValores ValidaFecha(DateTime Fecha, DtoUsuario usuario)
        {
            var Encabezado = RepositorioZctEncUbicacionValores.Obtener(E => E.fecha == Fecha);

            if (Encabezado != null)
            {
                //_Nuevo = false;
                return Encabezado;
            }

            Encabezado = new ZctEncUbicacionValores
            {
                fecha = Fecha.Date,
                folio = FolioEncabezado(),
                cod_usu = usuario.Id
            };

            //_Nuevo = true;
            return Encabezado;
        }
        public string  FolioEncabezado()
        {
            return (RepositorioZctEncUbicacionValores.ObtenerUltimoFolio() + 1).ToString();//(from u in _dao.ZctEncUbicacionValores select u.Cod_encUbicVal).Max(x => (int?)x) ?? 0
        }
        public decimal Total(DateTime fecha)
        {
            //decimal Total =(from v in _dao.ZctDetalleUbicacionValores where v.==Fecha select v.monto).Sum(x => (decimal?)x.monto) ?? 0.0M;

            return RepositorioZctEncUbicacionValores.ObtenerTotal(fecha);

           //decimal  Total = (from enc in _dao.ZctEncUbicacionValores
           //          join det in _dao.ZctDetalleUbicacionValores
           //              on enc.Cod_encUbicVal equals det.Cod_encUbicVal
           //          where enc.fecha == Fecha
           //          select det.monto).Sum(x => (decimal?)x) ?? 0.0M;
           // return Total;
        }
        public void CreaDetalleUbicacion(ZctEncUbicacionValores Encabezado)
        {
            //List<Modelo.ZctCatUbicacionValores> Ubicaciones = (from u in _dao.ZctCatUbicacionValores select u).ToList();
            foreach (var u in RepositorioZctCatUbicacionValores.ObtenerTodo().ToList())
            {
                var det = Encabezado.ZctDetalleUbicacionValores.FirstOrDefault(d => d.cod_ubicacion == u.cod_ubicacion);
                
                if (det == null)
                {
                    det = new ZctDetalleUbicacionValores();
                    Encabezado.ZctDetalleUbicacionValores.Add(det);
                }
                det.cod_ubicacion = u.cod_ubicacion;
                det.Cod_encUbicVal = Encabezado.Cod_encUbicVal;
                det.monto = 0;
                det.DescripcionUbicacion = u.descripcion;
                det.cod_ubicacion = u.cod_ubicacion; 
            }
        }
        public void Grabar(ZctEncUbicacionValores EnCabezado)
        {
            RepositorioZctEncUbicacionValores.Agregar(EnCabezado);
            RepositorioZctEncUbicacionValores.GuardarCambios();
        }
    }
}
