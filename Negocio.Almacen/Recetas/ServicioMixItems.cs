﻿using Modelo.Almacen.Constantes;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.CalendariosFiscales;
using Negocio.Almacen.Folios;
using Negocio.Almacen.Inventarios;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Extensiones;

namespace Negocio.Almacen.Recetas
{
    public class ServicioMixItems : IServicioMixItemsInterno
    {
        IServicioInventarios ServicioInventarios
        {
            get { return _servicioInventarios.Value; }
        }

        Lazy<IServicioInventarios> _servicioInventarios = new Lazy<IServicioInventarios>(() => FabricaDependencias.Instancia.Resolver<IServicioInventarios>());

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        IServicioFoliosInterno ServicioFolios
        {
            get { return _servicioFolios.Value; }
        }

        Lazy<IServicioFoliosInterno> _servicioFolios = new Lazy<IServicioFoliosInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioFoliosInterno>());

        IServicioArticulosInterno ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulosInterno> _servicioArticulos = new Lazy<IServicioArticulosInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioArticulosInterno>());


        IServicioCalendariosFiscales ServicioCalendariosFiscales
        {
            get { return _servicioCalendariosFiscales.Value; }
        }

        Lazy<IServicioCalendariosFiscales> _servicioCalendariosFiscales = new Lazy<IServicioCalendariosFiscales>(() => FabricaDependencias.Instancia.Resolver<IServicioCalendariosFiscales>());

        IRepositorioMixItems RepositorioMixItems
        {
            get { return _repositorioMixItems.Value; }
        }

        Lazy<IRepositorioMixItems> _repositorioMixItems = new Lazy<IRepositorioMixItems>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioMixItems>(); });

        IRepositorioEncabezadosOrdenesProduccion RepositorioEncabezadosOrdenesProduccion
        {
            get { return _repositorioEncabezadosOrdenesProduccion.Value; }
        }

        Lazy<IRepositorioEncabezadosOrdenesProduccion> _repositorioEncabezadosOrdenesProduccion = new Lazy<IRepositorioEncabezadosOrdenesProduccion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioEncabezadosOrdenesProduccion>(); });

        IRepositorioDetallesOrdenesProduccion RepositorioDetallesOrdenesProduccion
        {
            get { return _repositorioDetallesOrdenesProduccion.Value; }
        }

        Lazy<IRepositorioDetallesOrdenesProduccion> _repositorioDetallesOrdenesProduccion = new Lazy<IRepositorioDetallesOrdenesProduccion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioDetallesOrdenesProduccion>(); });

        public List<ZctMixItems> GetMixItems(string codigo, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarRecetas = true });

            return RepositorioMixItems.GetMixItems(codigo);
        }

        public List<ZctMixItems> GetMixItemsCost(string codArt, int codAlm, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarRecetas = true });

            return RepositorioMixItems.GetMixItemsCost(codArt, codAlm);
        }

        public void GuardarIngredientes(List<ZctMixItems> listMix, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EditarRecetas = true });

            if (listMix.Count == 0)
                throw new SOTException(Recursos.Recetas.receta_sin_ingredientes_exception);

            if (listMix.GroupBy(m => m.Cod_Art).Count() > 1)
                throw new SOTException(Recursos.Recetas.ingredientes_diferente_receta_excepcion);

            var idArticuloPadre = listMix.First().Cod_Art;

            if(listMix.Any(m=> m.Cod_Art_Mix == idArticuloPadre))
                throw new SOTException(Recursos.Recetas.articulo_elaborar_en_receta_exception);

            if(listMix.Count != listMix.GroupBy(m=> m.Cod_Art_Mix).Count())
                throw new SOTException(Recursos.Recetas.ingredientes_repetidos_excepcion);

            if (listMix.Any(m => m.cantidad.HasValue && m.cantidad.Value <= 0))
                throw new SOTException(Recursos.Recetas.cantidad_ingrediente_invalida_excepcion);

            if (listMix.Any(m => m.@base.HasValue && m.@base.Value <= 0))
                throw new SOTException(Recursos.Recetas.cantidad_preparar_invalida_excepcion);

            if (listMix.Select(m => string.IsNullOrWhiteSpace(m.proporcion)).Distinct().Count() > 1)
                throw new SOTException("No se pueden combinar recetas de la primera versión (por proporción) con recetas de la segunda (por cantidad).");

            foreach (var item in RepositorioMixItems.ObtenerElementos(m => m.Cod_Art.Equals(idArticuloPadre)).ToList())
                RepositorioMixItems.Eliminar(item);

            SetProportion(listMix);

            foreach (var item in listMix)
            {
                item.uuid = Guid.NewGuid();
                RepositorioMixItems.Agregar(item);
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
            {
                RepositorioMixItems.GuardarCambios();

                //lo puse aqui para que la validación contemple a las conversiones que se quieren guardar
                foreach (var conversion in listMix)
                {
                    ServicioArticulos.ValidarNoExisteReferenciaCircular(conversion.Cod_Art_Mix, conversion.Cod_Art);
                }

                scope.Complete();
            }
        }

        public void EliminarReceta(string codigoArticulo, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EditarRecetas = true });

            var listMix = RepositorioMixItems.ObtenerElementos(m => m.Cod_Art == codigoArticulo).ToList();

            if (listMix.Count == 0)
                throw new SOTException(Recursos.Recetas.receta_sin_ingredientes_exception);

            foreach (var item in listMix)
                RepositorioMixItems.Eliminar(item);

            RepositorioMixItems.GuardarCambios();
        }


        //public DataTable GetData(string sSPName, string sSpVariables, string sSpValues)
        //{
        //    return RepositorioMixItems.GetData(sSPName, sSpVariables, sSpValues);
        //}

        public void ProducirArticulo(ZctEncOP encabezado, List<ZctDetOP> detalles)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
            {
                if (!RepositorioEncabezadosOrdenesProduccion.Alguno(m => m.Cod_OP == encabezado.Cod_OP))
                    RepositorioEncabezadosOrdenesProduccion.Agregar(encabezado);
                else
                    RepositorioEncabezadosOrdenesProduccion.Modificar(encabezado);

                RepositorioEncabezadosOrdenesProduccion.GuardarCambios();

                var calendarioFiscal = ServicioCalendariosFiscales.ObtenerCalendarioPorFecha(encabezado.Fch_OP.Date);

                foreach (var det in detalles)
                {
                    det.Cod_OP = encabezado.Cod_OP;
                    det.anio_fiscal = calendarioFiscal != null ? calendarioFiscal.Anio : default(int?);
                    det.mes_fiscal = calendarioFiscal != null ? calendarioFiscal.Mes : default(int?);

                    if (!RepositorioDetallesOrdenesProduccion.Alguno(m => m.CodDet_OP == det.CodDet_OP))
                        RepositorioDetallesOrdenesProduccion.Agregar(det);
                    else
                        RepositorioDetallesOrdenesProduccion.Modificar(det);
                }

                RepositorioDetallesOrdenesProduccion.GuardarCambios();

                foreach (var movInv in detalles)
                {
                    decimal cantidadMovimiento = movInv.Cantidad ?? 0;

                    if (encabezado.Type_OP.Equals(ClavesFolios.OP))
                    {
                        List<ZctMixItems> items = RepositorioMixItems.ObtenerElementos(m => m.Cod_Art == movInv.Cod_Art).ToList();//.GetMixItemsCost(movInv.Cod_Art, (int)movInv.Cod_Alm);

                        if (items.Count == 0)
                            throw new SOTException("Artículo " + movInv.Cod_Art + ": " + Recursos.Recetas.receta_sin_ingredientes_exception);

                        foreach (var item in items)
                        {
                            var articulo = ServicioArticulos.ObtenerConCostoAcumulado(item.Cod_Art_Mix);

                            decimal multiplicadorConversion = (decimal)item.cantidad / (decimal)item.@base;

                            var surtido = -(cantidadMovimiento * multiplicadorConversion);
                            var costo = (articulo.ExistenciasTMP != 0 ? (articulo.CostoAcumuladoTMP / articulo.ExistenciasTMP) : (articulo.Cos_Art ?? 0)) * surtido;

                            //var surtido = ((cantidadMovimiento) * ((decimal)item.cantidad / (decimal)item.@base)) * -1;
                            //var costo = (item.Exist != 0 ? (item.Cost / item.Exist) : item.CostoArt) * surtido;
                            //mismo almacén
                            ServicioInventarios.AgregarMovimientoComanda(encabezado.Type_OP, 0, false, costo, movInv.Cod_OP.ToString(), item.Cod_Art_Mix, surtido, encabezado.Fch_OP, false, movInv.Cod_Alm);
                        }
                    }

                    ServicioInventarios.AgregarMovimientoComanda(encabezado.Type_OP,
                        0,
                        encabezado.Type_OP.Equals(ClavesFolios.OP),
                        (encabezado.Type_OP.Equals(ClavesFolios.OP) ? ((movInv.Costo ?? 0) * cantidadMovimiento) : -((movInv.Costo ?? 0) * cantidadMovimiento)),
                        movInv.Cod_OP.ToString(),
                        movInv.Cod_Art,
                        (encabezado.Type_OP.Equals(ClavesFolios.OP) ? cantidadMovimiento : -cantidadMovimiento),
                        encabezado.Fch_OP,
                        false,
                        movInv.Cod_Alm);
                }

                ServicioFolios.GenerarYObtenerSiguienteFolio(ClavesFolios.IP);

                scope.Complete();
      
            }
        }

        public ZctEncOP GetEncOP(int codOP)
        {
            return RepositorioMixItems.GetEncOP(codOP);
        }

        public List<ZctDetOP> GetDetOP(int codOP) 
        {
            return RepositorioMixItems.GetDetOP(codOP);
        }

        private void SetProportion(List<ZctMixItems> listMix)
        {
            foreach (var m in listMix)
            {
                if (!m.cantidad.HasValue || !m.@base.HasValue)
                {
                    m.cantidad = Decimal.Parse(m.proporcion.Split('/')[0]);
                    m.@base = Decimal.Parse(m.proporcion.Split('/')[1]);
                    //if (m.Proporcion.Split('/')[0].IndexOf(".") >= 0) {
                    //  m.Cantidad = m.Cantidad * Convert.ToDecimal(Math.Pow(10, m.Proporcion.Split('/')[0].Length - m.Proporcion.Split('/')[0].IndexOf(".")-1));
                    //  m.Base = m.Base * Convert.ToDecimal(Math.Pow(10, m.Proporcion.Split('/')[0].Length - m.Proporcion.Split('/')[0].IndexOf(".")-1));
                    //}
                }
                else
                {
                    m.proporcion = m.cantidad.Value.ToString() + "/" + m.@base.Value.ToString();
                }
            }
        }


        public bool VerificarTieneReceta(string Cod_Art)
        {
            return RepositorioMixItems.Alguno(m => m.Cod_Art == Cod_Art);
        }

        public void Preparar(string codigoArticulo, decimal cantidad, int idAlmacen, decimal costo, bool esEntrada, DateTime fecha)
        {
            var folio = ServicioFolios.ObtenerSiguienteFolio(ClavesFolios.IP);

            ZctEncOP enc = new ZctEncOP()
            {
                Cod_OP = folio,
                Fch_OP = fecha,
                Type_OP = esEntrada ? ClavesFolios.OP : ClavesFolios.OS
            };

            var movInv = new ZctDetOP
            {
                Cantidad = cantidad,
                Cod_Alm = idAlmacen,
                Cod_Art = codigoArticulo,
                Costo = costo
            };

            ProducirArticulo(enc, new List<ZctDetOP>() { movInv });
        }

        #region métodos internal

        List<ZctMixItems> IServicioMixItemsInterno.GetMixItems(string codigo)
        {
            return RepositorioMixItems.GetMixItems(codigo);
        }

        #endregion
    }
}
