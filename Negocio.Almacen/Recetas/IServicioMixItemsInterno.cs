﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.Almacen.Recetas
{
    internal interface IServicioMixItemsInterno : IServicioMixItems
    {
        List<ZctMixItems> GetMixItems(string codigo);
    }
}
