﻿using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
namespace Negocio.Almacen.Recetas
{
    public interface IServicioMixItems
    {
        ZctEncOP GetEncOP(int codOP);
        List<ZctDetOP> GetDetOP(int codOP);
        List<ZctMixItems> GetMixItems(string codigo, DtoUsuario usuario);
        List<ZctMixItems> GetMixItemsCost(string codArt, int codAlm, DtoUsuario usuario);
        void GuardarIngredientes(List<ZctMixItems> listMix, DtoUsuario usuario);

        void EliminarReceta(string codigoArticulo, DtoUsuario usuario);

        void ProducirArticulo(ZctEncOP encOP, List<ZctDetOP> detOP);

        bool VerificarTieneReceta(string Cod_Art);

        void Preparar(string codigoArticulo, decimal cantidad, int idAlmacen, decimal costo, bool esEntrada, DateTime fecha);
    }
}
