﻿using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Parametros
{
    public interface IServicioParametros
    {
        ZctCatPar ObtenerParametros();

        void CrearParametros(ZctCatPar parametros, DtoUsuario usuario);
        void ModificarParametros(ZctCatPar parametros, DtoUsuario usuario);
        void EliminarParametros(DtoUsuario usuario);
        void ValidarConfiguracionGlobalEsCorrecta();
    }
}
