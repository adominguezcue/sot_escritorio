﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Clientes;
using Negocio.Compartido.Sincronizacion;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.Almacen.Parametros
{
    public class ServicioParametros : IServicioParametrosInterno
    {
        IRepositorioParametros RepositorioParametros
        {
            get { return _repositorioParametros.Value; }
        }

        Lazy<IRepositorioParametros> _repositorioParametros = new Lazy<IRepositorioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioParametros>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioClientes ServicioClientes
        {
            get { return _servicioClientes.Value; }
        }

        Lazy<IServicioClientes> _servicioClientes = new Lazy<IServicioClientes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioClientes>(); });

        public IServicioSincronizacionCompartido AplicacionServiciosSincronizacion
        {
            get { return _aplicacionServiciosSincronizacion.Value; }
        }

        Lazy<IServicioSincronizacionCompartido> _aplicacionServiciosSincronizacion = new Lazy<IServicioSincronizacionCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioSincronizacionCompartido>(); });


        public Modelo.Almacen.Entidades.ZctCatPar ObtenerParametros()
        {
            return RepositorioParametros.Obtener(m => true);
        }

        public void CrearParametros(ZctCatPar parametros, DtoUsuario usuario)
        {
            if (parametros == null)
                throw new SOTException(Recursos.Parametros.parametros_nulos_exception);

            if (RepositorioParametros.Alguno())
                throw new SOTException(Recursos.Parametros.parametros_existentes_exception);

            //var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            //parametros.taller = parametros.NombreSucursal;
            //parametros.RazonSocial = datosFiscales?.Nom_Cte ?? "";
            //parametros.RFC = datosFiscales?.RFC_Cte ?? "";
            parametros.SubNombre = parametros.SubNombre ?? "";
            parametros.Reporteador_web = parametros.Reporteador_web ?? "";
            parametros.cod_alm_entrada = parametros.cod_alm_entrada ?? 0;
            parametros.cod_alm_salida = parametros.cod_alm_salida ?? 0;
            parametros.cod_cte_default = parametros.cod_cte_default ?? 0;

            ValidarDatos(parametros);

            RepositorioParametros.Agregar(parametros);
            RepositorioParametros.GuardarCambios();
        }

        public void ModificarParametros(ZctCatPar parametros, DtoUsuario usuario)
        {
            if (parametros == null)
                throw new SOTException(Recursos.Parametros.parametros_nulos_exception);

            var parametrosActuales = RepositorioParametros.Obtener(m => true);

            if (parametrosActuales == null)
                throw new SOTException(Recursos.Parametros.parametros_nulos_exception);

            //var datosFiscales = ServicioClientes.ObtenerUltimoCliente();

            //parametros.taller = parametros.NombreSucursal;
            //parametros.RazonSocial = datosFiscales?.Nom_Cte ?? "";
            //parametros.RFC = datosFiscales?.RFC_Cte ?? "";
            parametros.SubNombre = parametros.SubNombre ?? "";
            parametros.Reporteador_web = parametros.Reporteador_web ?? "";
            parametros.cod_alm_entrada = parametros.cod_alm_entrada ?? 0;
            parametros.cod_alm_salida = parametros.cod_alm_salida ?? 0;
            parametros.cod_cte_default = parametros.cod_cte_default ?? 0;

            parametrosActuales.SincronizarPrimitivas(parametros);

            ValidarDatos(parametrosActuales);

            RepositorioParametros.Modificar(parametrosActuales);
            RepositorioParametros.GuardarCambios();
        }

        public void EliminarParametros(DtoUsuario usuario)
        {
            var parametrosActuales = RepositorioParametros.Obtener(m => true);

            if (parametrosActuales == null)
                throw new SOTException(Recursos.Parametros.parametros_nulos_exception);

            RepositorioParametros.Eliminar(parametrosActuales);
            RepositorioParametros.GuardarCambios();
        }

        private void ValidarDatos(ZctCatPar parametrosActuales)
        {
            if (parametrosActuales.Iva_CatPar <= 0)
                throw new SOTException(Recursos.Parametros.iva_invalido_excepcion);

            if (parametrosActuales.MaximoComandasSimultaneas <= 0)

                throw new SOTException(Recursos.Parametros.maximo_comandas_simultaneas_invalido_excepcion);
            if (parametrosActuales.MaximoTaxisSimultaneos <= 0)
                throw new SOTException(Recursos.Parametros.maximo_taxis_simultaneos_invalido_excepcion);

            if (parametrosActuales.PrecioPorDefectoTaxi <= 0)
                throw new SOTException(Recursos.Parametros.precio_taxi_invalido_excepcion);

            //if (parametrosActuales.PrecioTarjetasPuntos <= 0)
            //    throw new SOTException(Recursos.Parametros.precio_tarjeta_puntos_invalido_excepcion);

#warning falta agregar la validación de no existencia en web
            if (string.IsNullOrWhiteSpace(parametrosActuales.taller))
                throw new SOTException(Recursos.Parametros.identificador_sucursal_invalido_excepcion);

            if (string.IsNullOrWhiteSpace(parametrosActuales.Nombre))
                throw new SOTException(Recursos.Parametros.nombre_sucursal_invalido_excepcion);

            if (string.IsNullOrWhiteSpace(parametrosActuales.Operadora))
                throw new SOTException(Recursos.Parametros.operadora_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(parametrosActuales.Propietaria))
                throw new SOTException(Recursos.Parametros.propietaria_invalida_excepcion);

            if (string.IsNullOrWhiteSpace(parametrosActuales.Direccion))
                throw new SOTException(Recursos.Parametros.direccion_reportes_invalido_excepcion);

            UtilidadesUrl.ValidarUrl(parametrosActuales.Reporteador_web, "GET");

            if (AplicacionServiciosSincronizacion.VerificarExisteSucursal(parametrosActuales.taller))
                throw new SOTException(Recursos.Parametros.nombre_sucursal_en_uso_sincronizacion_excepcion);
        }

        public void ValidarConfiguracionGlobalEsCorrecta()
        {
            var parametros = ObtenerParametros();

            if (parametros == null)
                throw new SOTException(Recursos.Parametros.parametros_nulos_exception);

            ValidarDatos(parametros);
        }

        #region Métodos internal

        //void IServicioParametrosInterno.ActualizarParametros(Cliente datosFiscales)
        //{
        //    var parametros = ObtenerParametros();

        //    if (parametros != null)
        //    {
        //        parametros.taller = parametros.NombreSucursal;
        //        parametros.RazonSocial = datosFiscales?.Nom_Cte ?? "";
        //        parametros.RFC = datosFiscales?.RFC_Cte ?? "";

        //        RepositorioParametros.Modificar(parametros);
        //        RepositorioParametros.GuardarCambios();
        //    }
        //} 

        #endregion
    }
}
