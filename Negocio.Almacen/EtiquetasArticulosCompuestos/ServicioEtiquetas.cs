﻿using Modelo.Almacen.Repositorios;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.EtiquetasArticulosCompuestos
{
    public class ServicioEtiquetas : IServicioEtiquetas
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IRepositorioVW_EtiquetasArticulosCompuestos RepositorioVW_EtiquetasArticulosCompuestos
        {
            get { return _repositorioVW_EtiquetasArticulosCompuestos.Value; }
        }

        Lazy<IRepositorioVW_EtiquetasArticulosCompuestos> _repositorioVW_EtiquetasArticulosCompuestos = new Lazy<IRepositorioVW_EtiquetasArticulosCompuestos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioVW_EtiquetasArticulosCompuestos>(); });


        public List<Modelo.Almacen.Entidades.VW_EtiquetasArticulosCompuestos> ObtenerEtiquetasFiltradas(Modelo.Seguridad.Dtos.DtoUsuario usuario, string nombreArticulo = "", DateTime? diaIngreso = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarEtiquetas = true });

            return RepositorioVW_EtiquetasArticulosCompuestos.ObtenerEtiquetasFiltradas(nombreArticulo, diaIngreso);
        }
    }
}
