﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.EtiquetasArticulosCompuestos
{
    public interface IServicioEtiquetas
    {
        /// <summary>
        /// Retorna los datos para generar etiquetas en base a los filtros
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="nombreArticulo"></param>
        /// <param name="diaIngreso"></param>
        /// <returns></returns>
        List<Modelo.Almacen.Entidades.VW_EtiquetasArticulosCompuestos> ObtenerEtiquetasFiltradas(Modelo.Seguridad.Dtos.DtoUsuario usuario, string nombreArticulo = "", DateTime? diaIngreso = null);
    }
}
