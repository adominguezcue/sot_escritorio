﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.CalendariosFiscales
{
    public class ServicioCalendariosFiscales : IServicioCalendariosFiscales
    {
        IRepositorioCalendariosFiscales RepositorioCalendariosFiscales
        {
            get { return _repositorioTiposArticulos.Value; }
        }

        Lazy<IRepositorioCalendariosFiscales> _repositorioTiposArticulos = new Lazy<IRepositorioCalendariosFiscales>(() => FabricaDependencias.Instancia.Resolver<IRepositorioCalendariosFiscales>());

        public CalendarioFiscal ObtenerCalendarioPorFecha(DateTime fecha) 
        {
            return RepositorioCalendariosFiscales.Obtener(m => m.fecha_ini <= fecha && m.fecha_fin >= fecha);
        }
    }
}
