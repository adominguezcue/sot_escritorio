﻿using Modelo.Almacen.Entidades;
using System;
namespace Negocio.Almacen.CalendariosFiscales
{
    interface IServicioCalendariosFiscales
    {
        CalendarioFiscal ObtenerCalendarioPorFecha(DateTime fecha);
    }
}
