﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.TiposPagos
{
    public class ServicioTiposPagos : IServicioTiposPagosInterno
    {
        IRepositorioTiposPagos RepositorioTiposPagos
        {
            get { return _repositorioTiposPagos.Value; }
        }

        Lazy<IRepositorioTiposPagos> _repositorioTiposPagos = new Lazy<IRepositorioTiposPagos>(() => FabricaDependencias.Instancia.Resolver<IRepositorioTiposPagos>());



        List<ClasificacionesGastos> IServicioTiposPagosInterno.ObtenerClasificacionesGastosPorTipos(List<int> idsTiposPagos)
        {
            return RepositorioTiposPagos.ObtenerElementos(m => m.IdClasificacionGasto.HasValue && idsTiposPagos.Contains(m.codtipo_pago)).Select(m => m.IdClasificacionGasto.Value).ToList()
                .Select(m => (ClasificacionesGastos)m).ToList();
        }
    }
}
