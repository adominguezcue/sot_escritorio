﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.TiposPagos
{
    internal interface IServicioTiposPagosInterno : IServicioTiposPagos
    {
        List<ClasificacionesGastos> ObtenerClasificacionesGastosPorTipos(List<int> idsTiposPagos);
    }
}
