﻿using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
namespace Negocio.Almacen.Requisiciones
{
    public interface IServicioRequisiciones
    {
        ZctRequisition ObtenerElemento(int folio, DtoUsuario usuario);
        List<ZctRequisition> ObtenerElementos(DtoUsuario usuario, string status = null, int? anio = null, int? mes = null);

        //void CrearRequisicion(ZctRequisition requisicion, DtoUsuario usuario);
    }
}
