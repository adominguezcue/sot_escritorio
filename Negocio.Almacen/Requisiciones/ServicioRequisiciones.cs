﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Inventarios;
using Negocio.Almacen.OrdenesCompra;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.Requisiciones
{
    public class ServicioRequisiciones : IServicioRequisiciones
    {
        IRepositorioZctRequisition RepositorioZctRequisition
        {
            get { return _repositorioZctRequisition.Value; }
        }

        Lazy<IRepositorioZctRequisition> _repositorioZctRequisition = new Lazy<IRepositorioZctRequisition>(() => FabricaDependencias.Instancia.Resolver<IRepositorioZctRequisition>());
        
        
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        IServicioOrdenesCompra ServicioOrdenesCompra
        {
            get { return _servicioOrdenesCompra.Value; }
        }

        Lazy<IServicioOrdenesCompra> _servicioOrdenesCompra = new Lazy<IServicioOrdenesCompra>(() => FabricaDependencias.Instancia.Resolver<IServicioOrdenesCompra>());

        IServicioInventarios ServicioInventarios
        {
            get { return _servicioInventarios.Value; }
        }

        Lazy<IServicioInventarios> _servicioInventarios = new Lazy<IServicioInventarios>(() => FabricaDependencias.Instancia.Resolver<IServicioInventarios>());


        public List<ZctRequisition> ObtenerElementos(DtoUsuario usuario, string status = null, int? anio = null, int? mes = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarRequisiciones = true });

            return RepositorioZctRequisition.ObtenerRequisiciones(status, anio, mes);
        }
        public ZctRequisition ObtenerElemento(int folio, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarRequisiciones = true });

            return RepositorioZctRequisition.Obtener(m => m.folio == folio);
        }

        //public int genera_orden_trabajo(ZctRequisition requistion)
        //{
        //    int? orden_compra = 0;
        //    datos.SP_REQUISITION_OT(requistion.folio, ref orden_compra);
        //    requistion.Cod_EncOT = orden_compra;
        //    requistion.estatus = "ORDEN DE TRABAJO GENERADA";
        //    requistion.update_web = false;

        //    return orden_compra.GetValueOrDefault();
        //}

        //public void CrearRequisicion(ZctRequisition requisicion, DtoUsuario usuario)
        //{
        //    ServicioPermisos.ValidarPermisos();//(usuario, new DtoPermisos { CrearRequisiciones = true });

        //    var fechaActual = DateTime.Now;

        //    if (string.IsNullOrEmpty(requisicion.observaciones))
        //        requisicion.observaciones = "";

        //    if (string.IsNullOrEmpty(requisicion.estatus))
        //        requisicion.estatus = "";

        //    requisicion.uid = Guid.NewGuid();
        //    requisicion.anio_fiscal = fechaActual.Year;
        //    requisicion.mes_fiscal = fechaActual.Month;
        //    requisicion.folio = RepositorioZctRequisition.ObtenerUltimoFolio() + 1;

        //    foreach (var detalle in requisicion.ZctRequisitionDetail) 
        //    {
        //        if (detalle.amount <= 0)
        //            throw new SOTException(Recursos.Requisiciones.cantidad_requisicion_invalida_excepcion);

        //        detalle.uid = Guid.NewGuid();
        //    }

        //    RepositorioZctRequisition.Agregar(requisicion);
        //    RepositorioZctRequisition.GuardarCambios();
        //}
    }
}
