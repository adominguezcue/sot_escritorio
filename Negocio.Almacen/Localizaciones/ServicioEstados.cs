﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.Localizaciones
{
    public class ServicioEstados : IServicioEstados
    {
        IRepositorioEstados RepositorioEstados
        {
            get { return _repositorioEstados.Value; }
        }

        Lazy<IRepositorioEstados> _repositorioEstados = new Lazy<IRepositorioEstados>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioEstados>(); });


        public List<Estado> ObtenerEstados()
        {
            return RepositorioEstados.ObtenerTodo().ToList();
        }
    }
}
