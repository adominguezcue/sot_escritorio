﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.Localizaciones
{
    public class ServicioCiudades : IServicioCiudades
    {
        IRepositorioCiudades RepositorioCiudades
        {
            get { return _repositorioCiudades.Value; }
        }

        Lazy<IRepositorioCiudades> _repositorioCiudades = new Lazy<IRepositorioCiudades>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioCiudades>(); });


        public List<Ciudad> ObtenerCiudades(int idEstado)
        {
            if (idEstado == 0)
                return RepositorioCiudades.ObtenerTodo().ToList();

            return RepositorioCiudades.ObtenerElementos(m => m.Cod_Edo == idEstado).ToList();
        }
    }
}
