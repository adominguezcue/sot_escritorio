﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Presentaciones
{
    public interface IServicioPresentaciones
    {
        List<Presentacion> ObtenerPresentacionesFILTRO();
        List<Presentacion> ObtenerPresentaciones(DtoUsuario usuario);

        Presentacion ObtenerPresentacion(int idPresentacion, DtoUsuario UsuarioActual);

        void CrearPresentacion(Presentacion presentacion, DtoUsuario UsuarioActual);

        void ModificarPresentacion(Presentacion presentacion, List<DtoConversion> conversion, DtoUsuario UsuarioActual);

        void EliminarPresentacion(int idPresentacion, DtoUsuario UsuarioActual);
    }
}
