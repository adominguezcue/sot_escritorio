﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.Presentaciones
{
    public class ServicioPresentaciones : IServicioPresentaciones
    {
        IRepositorioPresentaciones RepositorioPresentaciones
        {
            get { return _repositorioPresentaciones.Value; }
        }

        Lazy<IRepositorioPresentaciones> _repositorioPresentaciones = new Lazy<IRepositorioPresentaciones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioPresentaciones>(); });

        IRepositorioConversiones RepositorioConversiones
        {
            get { return _repositorioConversiones.Value; }
        }

        Lazy<IRepositorioConversiones> _repositorioConversiones = new Lazy<IRepositorioConversiones>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConversiones>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        public List<Presentacion> ObtenerPresentacionesFILTRO()
        {
            return RepositorioPresentaciones.ObtenerTodo().ToList();
        }
        public List<Presentacion> ObtenerPresentaciones(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarPresentaciones = true });

            return RepositorioPresentaciones.ObtenerTodo().ToList();
        }


        public Presentacion ObtenerPresentacion(int idPresentacion, DtoUsuario UsuarioActual)
        {
            ServicioPermisos.ValidarPermisos(UsuarioActual, new DtoPermisos { ConsultarPresentaciones = true });

            return RepositorioPresentaciones.Obtener(m => m.id == idPresentacion);
        }


        public void CrearPresentacion(Presentacion presentacion, DtoUsuario UsuarioActual)
        {
            ServicioPermisos.ValidarPermisos(UsuarioActual, new DtoPermisos { CrearPresentaciones = true });

            if (presentacion == null)
                throw new SOTException(Recursos.Presentaciones.presentacion_nula_excepcion);

            ValidarDatos(presentacion);

            if (presentacion.ConversionesEntrada.Where(m => m.Activa).GroupBy(m => m.IdSegundaPresentacion).Count() != presentacion.ConversionesEntrada.Where(m => m.Activa).Count())
                throw new SOTException(Recursos.Presentaciones.conversiones_duplicadas_excepcion);

            foreach (var conversion in presentacion.ConversionesEntrada.Where(m => m.Activa))
            {
                if (conversion.IdSegundaPresentacion == presentacion.id || !RepositorioPresentaciones.Alguno(m => m.id == conversion.IdSegundaPresentacion))
                    throw new SOTException(Recursos.Presentaciones.conversiones_invalidas_excepcion);

                if (conversion.Equivalencia <= 0)
                    throw new SOTException(Recursos.Presentaciones.equivalencia_invalida_excepcion);

                if (RepositorioConversiones.Alguno(m => m.Id != conversion.Id && m.Activa && m.IdPrimeraPresentacion == presentacion.id && m.IdSegundaPresentacion == conversion.IdSegundaPresentacion)
                   || RepositorioConversiones.Alguno(m => m.Activa && m.IdSegundaPresentacion == presentacion.id && m.IdPrimeraPresentacion == conversion.IdSegundaPresentacion))
                    throw new SOTException(Recursos.Presentaciones.conversion_existente_excepcion);
            }

            RepositorioPresentaciones.Agregar(presentacion);
            RepositorioPresentaciones.GuardarCambios();
        }

        public void ModificarPresentacion(Presentacion presentacionM, List<DtoConversion> conversiones, DtoUsuario usuario)
        {
            //ServicioPermisos.ValidarPermisos(UsuarioActual, new DtoPermisos { ModificarPresentaciones = true });

            //if (presentacion == null)
            //    throw new SOTException(Recursos.Presentaciones.presentacion_nula_excepcion);

            //if (string.IsNullOrWhiteSpace(presentacion.presentacion))
            //    throw new SOTException(Recursos.Presentaciones.presentacion_invalida_excepcion);

            //if (RepositorioPresentaciones.Alguno(m => m.id != presentacion.id && m.presentacion.Trim().ToUpper().Equals(presentacion.presentacion.Trim().ToUpper())))
            //    throw new SOTException(Recursos.Presentaciones.presentacion_duplicada_excepcion);

            //RepositorioPresentaciones.Modificar(presentacion);
            //RepositorioPresentaciones.GuardarCambios();

            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarPresentaciones = true });

            if (presentacionM == null)
                throw new SOTException(Recursos.Presentaciones.presentacion_nula_excepcion);

            //if (!presentacionM.Activa)
            //    throw new SOTException(Recursos.Presentaciones.presentacion_no_eliminable_excepcion);

            var presentacion = RepositorioPresentaciones.ObtenerConPresentacionesEntrada(presentacionM.id);

            if (presentacion == null)
                throw new SOTException(Recursos.Presentaciones.id_invalido_excepcion);

            ValidarDatos(presentacionM);

            if (conversiones.GroupBy(m => m.IdPresentacionSalida).Count() != conversiones.Count)
                throw new SOTException(Recursos.Presentaciones.conversiones_duplicadas_excepcion);

            foreach (var conversion in conversiones)
            {
                if (conversion.IdPresentacionSalida == presentacion.id || !RepositorioPresentaciones.Alguno(m=> m.id == conversion.IdPresentacionSalida))
                    throw new SOTException(Recursos.Presentaciones.conversiones_invalidas_excepcion);

                if (conversion.Equivalencia <= 0)
                    throw new SOTException(Recursos.Presentaciones.equivalencia_invalida_excepcion);

                if (RepositorioConversiones.Alguno(m => m.Id != conversion.Id && m.Activa && m.IdPrimeraPresentacion == presentacion.id && m.IdSegundaPresentacion == conversion.IdPresentacionSalida)
                   || RepositorioConversiones.Alguno(m => m.Activa && m.IdSegundaPresentacion == presentacion.id && m.IdPrimeraPresentacion == conversion.IdPresentacionSalida))
                    throw new SOTException(Recursos.Presentaciones.conversion_existente_excepcion);
            }

            presentacion.SincronizarPrimitivas(presentacionM);

            foreach (var conversion in conversiones.Where(m => m.Activa || m.Id != 0))
            {
                if (conversion.Id == 0)
                {
                    presentacion.ConversionesEntrada.Add(new Conversiones
                    {
                        Activa = true,
                        IdSegundaPresentacion = conversion.IdPresentacionSalida,
                        Equivalencia = conversion.Equivalencia,
                        EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Creado
                    });
                }
                else 
                {
                    var conversionExistente = presentacion.ConversionesEntrada.FirstOrDefault(m => m.Activa && m.Id == conversion.Id);

                    if (conversionExistente == null)
                        throw new SOTException(Recursos.Presentaciones.id_conversion_invalida_excepcion, conversion.Id.ToString());

                    conversionExistente.Activa = conversion.Activa;
                    conversionExistente.IdSegundaPresentacion = conversion.IdPresentacionSalida;
                    conversionExistente.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;
                }
            }

            foreach (var conversion in presentacion.ConversionesEntrada.Where(m => m.Activa))
            {
                if (conversion.IdSegundaPresentacion == presentacion.id || !RepositorioPresentaciones.Alguno(m => m.id == conversion.IdSegundaPresentacion))
                    throw new SOTException(Recursos.Presentaciones.conversiones_invalidas_excepcion);

                if (conversion.Equivalencia <= 0)
                    throw new SOTException(Recursos.Presentaciones.equivalencia_invalida_excepcion);

                if (RepositorioConversiones.Alguno(m => m.Id != conversion.Id && m.Activa && m.IdPrimeraPresentacion == presentacion.id && m.IdSegundaPresentacion == conversion.IdSegundaPresentacion)
                   || RepositorioConversiones.Alguno(m => m.Activa && m.IdSegundaPresentacion == presentacion.id && m.IdPrimeraPresentacion == conversion.IdSegundaPresentacion))
                    throw new SOTException(Recursos.Presentaciones.conversion_existente_excepcion);
            }

            //var fechaActual = DateTime.Now;

            ////ficha.Activa = true;
            //presentacion.FechaModificacion = fechaActual;
            //presentacion.IdUsuarioModifico = usuario.Id;

            //var archivosOriginales = RepositorioArchivosFicha.ObtenerElementos(m => m.IdFicha == presentacion.Id).ToList();

            //foreach (var grupo in (from archivoN in archivos
            //                       join archivoO in archivosOriginales on archivoN.Nombre.ToUpper() equals archivoO.Nombre.ToUpper() into arch
            //                       from archivoO in arch.DefaultIfEmpty()
            //                       select new
            //                       {
            //                           archivoN,
            //                           archivoO
            //                       }))
            //{
            //    if (grupo.archivoO == null)
            //        presentacion.ArchivosFicha.Add(new ArchivoFicha
            //        {
            //            Activa = true,
            //            Valor = grupo.archivoN.Valor,
            //            Nombre = grupo.archivoN.Nombre
            //        });
            //    else
            //    {
            //        grupo.archivoO.Valor = grupo.archivoN.Valor;
            //        RepositorioArchivosFicha.Modificar(grupo.archivoO);
            //    }
            //}

            //foreach (var grupo in (from archivoO in archivosOriginales
            //                       join archivoN in archivos on archivoO.Nombre.ToUpper() equals archivoN.Nombre.ToUpper() into arch
            //                       from archivoN in arch.DefaultIfEmpty()
            //                       select new
            //                       {
            //                           archivoN,
            //                           archivoO
            //                       }))
            //{
            //    if (grupo.archivoN == null)
            //        RepositorioArchivosFicha.Eliminar(grupo.archivoO);
            //}

            //ArchivoFicha archivoInactivo;

            //while ((archivoInactivo = ficha.ArchivosFicha.FirstOrDefault(m => !m.Activa && m.Id == 0)) != null)
            //    ficha.ArchivosFicha.Remove(archivoInactivo);

            foreach (var archivo in presentacion.ConversionesEntrada)
            {
                //if (archivo.Id == 0)
                //{
                //    archivo.FechaCreacion = fechaActual;
                //    archivo.IdUsuarioCreo = usuario.Id;
                //}
                //if (!archivo.Activa)
                //{
                //    archivo.FechaEliminacion = fechaActual;
                //    archivo.IdUsuarioElimino = usuario.Id;
                //}

                //archivo.FechaModificacion = fechaActual;
                //archivo.IdUsuarioModifico = usuario.Id;


                archivo.EntidadEstado = archivo.Id == 0 ?
                Dominio.Nucleo.Entidades.EntidadEstados.Creado :
                Dominio.Nucleo.Entidades.EntidadEstados.Modificado;
            }

            //using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            //{
            //    RepositorioArchivosFicha.GuardarCambios();

                RepositorioPresentaciones.Modificar(presentacion);
                RepositorioPresentaciones.GuardarCambios();

            //    scope.Complete();
            //}
        }

        private void ValidarDatos(Presentacion presentacion)
        {
            if (string.IsNullOrWhiteSpace(presentacion.presentacion))
                throw new SOTException(Recursos.Presentaciones.presentacion_invalida_excepcion);

            if (RepositorioPresentaciones.Alguno(m => m.id != presentacion.id && m.presentacion.Trim().ToUpper().Equals(presentacion.presentacion.Trim().ToUpper())))
                throw new SOTException(Recursos.Presentaciones.presentacion_duplicada_excepcion);
        }


        public void EliminarPresentacion(int idPresentacion, DtoUsuario UsuarioActual)
        {
            var presentacion = RepositorioPresentaciones.Obtener(m => m.id == idPresentacion);

            if (presentacion == null)
                throw new SOTException(Recursos.Presentaciones.presentacion_nula_eliminada_excepcion);

            if (ServicioArticulos.VerificarRelacionesConPresentacion(idPresentacion))
                throw new SOTException(Recursos.Presentaciones.articulos_dependientes_excepcion);

            RepositorioPresentaciones.Eliminar(presentacion);
            RepositorioPresentaciones.GuardarCambios();
        }
    }
}
