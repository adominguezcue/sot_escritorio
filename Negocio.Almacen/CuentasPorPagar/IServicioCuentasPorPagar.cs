﻿using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;

namespace Negocio.Almacen.CuentasPorPagar
{
    public interface IServicioCuentasPorPagar
    {
        void CancelarCxP(int OC);
        ZctEncCuentasPagar CreaCuentaPagar(int Cod_OC);
        ZctDetCuentasPagar CreaPago(ZctEncCuentasPagar cxp, string Referencia, int Metodo, decimal monto, decimal cambio, DateTime fechapago, DateTime fechavencido);
        void ActualizarPagos(ZctEncCuentasPagar CxP, DtoUsuario usuario);//, ZctDetCuentasPagar Detalle = null);
        List<ZctEncCuentasPagar> TraeCuentasPorEstado(string filtroProveedor, ZctEncCuentasPagar.Estados? Estado = null, DateTime? fechacreacionini=null, DateTime? fechacreacionfin=null, DateTime? fechavencimientoini=null, DateTime? fechavencimientofin=null, bool? esfechacreacion=null, bool? esfechafin=null);
        List<ZctEncCuentasPagar> TraeCuentasProveedores(int cod_prov, ZctEncCuentasPagar.Estados? Estado = null);
        List<Transversal.Dtos.DtoEnum> TraeEstadosPago();
        int TraeFolioCxP();
        List<ZctDetCuentasPagar> TraePagosVencidosProveedor(DateTime fecha, int cod_prov);
        ZctEncCuentasPagar ValidarYObtenerCargada(int cod_cuenta_pago);
        ZctEncCuentasPagar ObtenerCuentaPorPagarPorOrdenCompra(int codigoOrdenCompra);
        List<WV_ZctProveedoresCxP> TraeCuentasVencidas(DateTime fecha);
        List<WV_ZctProveedoresCxP> TraeProveedores(int? idEstado = null);
        List<WV_ZctProveedoresCxP> TraeProveedoresPorEstatusCuenta(string filtroProveedor, int? idEstado = null);
        DateTime? ObtenerFechaUltimaCuenta(int codigoOrdenCompra, DtoUsuario usuario);
    }
}
