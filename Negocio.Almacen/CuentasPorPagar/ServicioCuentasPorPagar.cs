﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.OrdenesCompra;
using Negocio.Almacen.Parametros;
using Negocio.Almacen.Proveedores;
using Negocio.Almacen.TiposPagos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.CuentasPorPagar
{
    public class ServicioCuentasPorPagar : IServicioCuentasPorPagar
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        IServicioTiposPagosInterno ServicioTiposPagos
        {
            get { return _servicioTiposPagos.Value; }
        }

        Lazy<IServicioTiposPagosInterno> _servicioTiposPagos = new Lazy<IServicioTiposPagosInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioTiposPagosInterno>());


        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => FabricaDependencias.Instancia.Resolver<IServicioParametros>());

        IRepositorioEncabezadosCuentasPorPagar RepositorioEncabezadosCuentasPorPagar
        {
            get { return _repositorioEncabezadosCuentasPorPagar.Value; }
        }

        Lazy<IRepositorioEncabezadosCuentasPorPagar> _repositorioEncabezadosCuentasPorPagar = new Lazy<IRepositorioEncabezadosCuentasPorPagar>(() => FabricaDependencias.Instancia.Resolver<IRepositorioEncabezadosCuentasPorPagar>());

        IRepositorioDetallesCuentasPorPagar RepositorioDetallesCuentasPorPagar
        {
            get { return _repositorioDetallesCuentasPorPagar.Value; }
        }

        Lazy<IRepositorioDetallesCuentasPorPagar> _repositorioDetallesCuentasPorPagar = new Lazy<IRepositorioDetallesCuentasPorPagar>(() => FabricaDependencias.Instancia.Resolver<IRepositorioDetallesCuentasPorPagar>());

        IRepositorioWV_ZctProveedoresCxP RepositorioWV_ZctProveedoresCxP
        {
            get { return _repositorioWV_ZctProveedoresCxP.Value; }
        }

        Lazy<IRepositorioWV_ZctProveedoresCxP> _repositorioWV_ZctProveedoresCxP = new Lazy<IRepositorioWV_ZctProveedoresCxP>(() => FabricaDependencias.Instancia.Resolver<IRepositorioWV_ZctProveedoresCxP>());

        IServicioOrdenesCompraInterno ServicioOrdenesCompra
        {
            get { return _servicioOrdenesCompra.Value; }
        }

        Lazy<IServicioOrdenesCompraInterno> _servicioOrdenesCompra = new Lazy<IServicioOrdenesCompraInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioOrdenesCompraInterno>());

        IServicioProveedores ServicioProveedores
        {
            get { return _servicioProveedores.Value; }
        }

        Lazy<IServicioProveedores> _servicioProveedores = new Lazy<IServicioProveedores>(() => FabricaDependencias.Instancia.Resolver<IServicioProveedores>());

        private decimal DosDecimales(object Cantidad)
        {
            return Decimal.Round(Convert.ToDecimal(Cantidad), 2);
        }

        public int TraeFolioCxP()
        {
            return RepositorioEncabezadosCuentasPorPagar.ObtenerFolioMaximo() + 1;
        }
        
        public ZctEncCuentasPagar ValidarYObtenerCargada(int cod_cuenta_pago)
        {
            var encabezadoCuenta = RepositorioEncabezadosCuentasPorPagar.ObtenerCargada(cod_cuenta_pago);

            if (encabezadoCuenta == null)
                throw new SOTException(Recursos.CuentasPorPagar.codigo_cuenta_pagar_invalido_excepcion, cod_cuenta_pago.ToString());

            return encabezadoCuenta;
        }
        public ZctEncCuentasPagar ObtenerCuentaPorPagarPorOrdenCompra(int codigoOrdenCompra)
        {
            var encabezadoCuenta = RepositorioEncabezadosCuentasPorPagar.Obtener(m => m.Cod_OC == codigoOrdenCompra);

            if (encabezadoCuenta == null)
                throw new SOTException(Recursos.CuentasPorPagar.no_cuenta_pagar_orden_compra_excepcion, codigoOrdenCompra.ToString());

            return encabezadoCuenta;
        }
        public ZctEncCuentasPagar CreaCuentaPagar(int Cod_OC)
        {
            decimal SubtoTotalOC = DosDecimales(ServicioOrdenesCompra.ObtenerSubTotal(Cod_OC));

            var encabezadoOC = ServicioOrdenesCompra.ValidarYObtenerOrdenCompra(Cod_OC);

            //int Cod_Prov = encabezadoOC.Cod_Prov ?? 0;
            var Proveedor = ServicioProveedores.ValidarYObtenerProveedor(encabezadoOC.Cod_Prov ?? 0);


            var ivaOmitir = DosDecimales(ServicioOrdenesCompra.ObtenerIVAOmitir(Cod_OC));

            ZctEncCuentasPagar cuenta = RepositorioEncabezadosCuentasPorPagar.Obtener(c => c.Cod_OC == Cod_OC);
            bool esNueva = cuenta == null;

            if (esNueva)
            {
                cuenta = new ZctEncCuentasPagar
                {
                    Cod_OC = Cod_OC,
                    monto = SubtoTotalOC,
                    uuid = Guid.NewGuid()
                };
            }

            var parametros = ServicioParametros.ObtenerParametros();
            decimal IvaPar = DosDecimales(parametros.Iva_CatPar);
            decimal IvaOC = DosDecimales(SubtoTotalOC * IvaPar) - ivaOmitir;


            //List<int?> lista = (from ZctEncOC oc in _dao.ZctEncOC join Proveedor p in _dao.Proveedor on oc.Cod_Prov equals p.Cod_Prov where oc.Cod_OC == Cod_OC select p.dias_credito).ToList();
            //if (lista.Count == 0)
            //{
            //    throw new Exception("Proveedor no encontrado, verifique la OC numero " + Cod_OC.ToString());
            //}
            //int DiasCredito = Convert.ToInt32(lista[0]);
            //DateTime FechaVencido = DateTime.Now.AddDays(DiasCredito);

#warning ojo aqui
            if (Proveedor.numero_pagos > 1)
                throw new SOTException("En estos momentos solamente se puede manejar un detalle de pago por cuenta");

            int dias_credito;
            int frecuencia_pago;
            int numero_pagos;
            dias_credito = Convert.ToInt32(Proveedor.dias_credito);
            frecuencia_pago = Convert.ToInt32(Proveedor.frecuencia_pagos);
            numero_pagos = Convert.ToInt32(Proveedor.numero_pagos);
            DateTime FechaVencimientoPago = DateTime.Now.AddDays(dias_credito);

            cuenta.fecha_creacion = DateTime.Now;
            cuenta.fecha_vencido = FechaVencimientoPago;
            cuenta.iva = IvaOC;
            cuenta.subtotal = SubtoTotalOC;
            cuenta.total = SubtoTotalOC + IvaOC;
            cuenta.Estado = ZctEncCuentasPagar.Estados.PENDIENTE;

            decimal MontoPagos;

            if (numero_pagos > 0)
            {
                MontoPagos = (cuenta.total / numero_pagos);
            }
            else
            {
                MontoPagos = (cuenta.total);
                numero_pagos = 1;
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (esNueva)
                {
                    RepositorioEncabezadosCuentasPorPagar.Agregar(cuenta);
                    RepositorioEncabezadosCuentasPorPagar.GuardarCambios();
                }

                List<ZctDetCuentasPagar> pagosRegistrados = RepositorioDetallesCuentasPorPagar.ObtenerElementos(m => m.cod_cuenta_pago == cuenta.cod_cuenta_pago).ToList();

                foreach (var pagoEliminar in pagosRegistrados.Where(m => m.consecutivo > numero_pagos))
                    RepositorioDetallesCuentasPorPagar.Eliminar(pagoEliminar);

                //actualizan o agregan pagos a la cxp 
                //int pagos = (from p in _dao.ZctDetCuentasPagar where p.cod_cuenta_pago == cuenta.cod_cuenta_pago select p.cod_cuenta_pago).Count();
                for (int p = 1; p <= numero_pagos; p++)
                {
                    var pagoExistente = pagosRegistrados.FirstOrDefault(m => m.consecutivo == p);

                    if (pagoExistente == null)
                    {

                        pagoExistente = new ZctDetCuentasPagar
                        {
                            cod_cuenta_pago = cuenta.cod_cuenta_pago,
                            consecutivo = p,
                            monto = MontoPagos,
                            fecha_factura = cuenta.fecha_vencido,
                            Estado = ZctEncCuentasPagar.Estados.PENDIENTE,
                            referencia = "",
                            fecha_vencido = FechaVencimientoPago.AddDays((p - 1 * frecuencia_pago))
                        };

                        RepositorioDetallesCuentasPorPagar.Agregar(pagoExistente);
                        //cuenta.ZctDetCuentasPagar.Add(pago);
                        //_dao.ZctDetCuentasPagar.InsertOnSubmit(pago);

                    }
                    else //if (pagosRegistrados.Count > 0)
                    {
                        //ZctDetCuentasPagar pago;
                        //if (pagosRegistrados.Count >= p)
                        //{
                        //    pago = pagosRegistrados[p - 1];
                        //}
                        //else
                        //{
                        //    pago = new ZctDetCuentasPagar();
                        //    pago.consecutivo = p;
                        //}
                        pagoExistente.monto = MontoPagos;
                        pagoExistente.Estado = ZctEncCuentasPagar.Estados.PENDIENTE;
                        pagoExistente.referencia = "";
                        pagoExistente.fecha_vencido = FechaVencimientoPago.AddDays((p - 1 * frecuencia_pago));

                        RepositorioDetallesCuentasPorPagar.Modificar(pagoExistente);
                        //if (pagosRegistrados.Count >= p - 1)
                        //{
                        //    cuenta.ZctDetCuentasPagar.Add(pago);
                        //}
                    }

                    if (!string.IsNullOrWhiteSpace(encabezadoOC.Fac_OC))
                        pagoExistente.factura = encabezadoOC.Fac_OC;
                }

                //List<ZctDetCuentasPagar> pagoelim = RepositorioDetallesCuentasPorPagar.ObtenerElementos(m => m.cod_cuenta_pago == cuenta.cod_cuenta_pago && m.consecutivo > numero_pagos).ToList();

                //foreach (var pagoEliminar in pagoelim)
                //    RepositorioDetallesCuentasPorPagar.Eliminar(pagoEliminar);
                //

                ServicioOrdenesCompra.ValidarYObtenerOrdenCompra(Cod_OC);


                RepositorioDetallesCuentasPorPagar.GuardarCambios();

                scope.Complete();
            }
            return cuenta;
        }

        public List<ZctEncCuentasPagar> TraeCuentasPorEstado(string filtroProveedor, ZctEncCuentasPagar.Estados? Estado = null, DateTime? fechacreacionini =null, DateTime? fechacreacionfin=null, DateTime? fechavencimientoini =null, DateTime? fechavencimientofin=null, bool? esfechacreacion=null, bool? esfechafin=null)
        {
            return RepositorioEncabezadosCuentasPorPagar.TraeCuentasPorEstado(filtroProveedor, Estado, fechacreacionini, fechacreacionfin, fechavencimientoini, fechavencimientofin, esfechacreacion, esfechafin);
        }

        public List<ZctEncCuentasPagar> TraeCuentasProveedores(int cod_prov, ZctEncCuentasPagar.Estados? Estado = null)
        {
            return RepositorioEncabezadosCuentasPorPagar.TraeCuentasProveedores(cod_prov, Estado);

            /*if (!Estado.HasValue)
                return RepositorioEncabezadosCuentasPorPagar.ObtenerElementos(m => m.ZctEncOC.Cod_Prov == cod_prov).ToList();

            int idEstado = (int)Estado.Value;

            return RepositorioEncabezadosCuentasPorPagar.ObtenerElementos(m => m.ZctEncOC.Cod_Prov == cod_prov && m.idEstado == idEstado).ToList();
            */
            //List<ZctEncCuentasPagar> Ctas = null;

            //if (!Estado.HasValue)
            //{
            //    Ctas = RepositorioEncabezadosCuentasPorPagar.ObtenerElementos(m => m.ZctEncOC.Cod_Prov == cod_prov).ToList();
            //}
            //else if (Estado.Value == ZctEncCuentasPagar.Estados.PENDIENTE)
            //{
            //    var idEstadoPendiente = (int)ZctEncCuentasPagar.Estados.PENDIENTE;

            //    Ctas = RepositorioEncabezadosCuentasPorPagar.ObtenerElementos(m => m.ZctEncOC.Cod_Prov == cod_prov && m.idEstado == idEstadoPendiente).ToList();
            //}
            //else if (Estado.Value == ZctEncCuentasPagar.Estados.PAGADO)
            //{
            //    var idEstadoPagado = (int)ZctEncCuentasPagar.Estados.PAGADO;

            //    Ctas = RepositorioEncabezadosCuentasPorPagar.ObtenerElementos(m => m.ZctEncOC.Cod_Prov == cod_prov && m.idEstado == idEstadoPagado).ToList();
            //}
            //return Ctas;
        }
        public List<WV_ZctProveedoresCxP> TraeCuentasVencidas(DateTime fecha)
        {
            return RepositorioWV_ZctProveedoresCxP.ObtenerTodo().ToList();
        }
        public List<WV_ZctProveedoresCxP> TraeProveedores(int? idEstado = null)
        {
            if (idEstado.HasValue)
                return RepositorioWV_ZctProveedoresCxP.SP_ZctProveedoresCxP(idEstado.Value);

            return RepositorioWV_ZctProveedoresCxP.ObtenerTodo().ToList();
        }
        public List<WV_ZctProveedoresCxP> TraeProveedoresPorEstatusCuenta(string filtroProveedor, int? idEstado = null)
        {
            if (idEstado.HasValue)
            {
                if (string.IsNullOrWhiteSpace(filtroProveedor))
                    return RepositorioWV_ZctProveedoresCxP.SP_TotalesCXCProveedor(idEstado.Value);

                return RepositorioWV_ZctProveedoresCxP.SP_TotalesCXCProveedor(idEstado.Value).Where(m=> m.Nom_Prov.Trim().ToUpper().Contains(filtroProveedor.Trim().ToUpper())).ToList();
            }

            if (string.IsNullOrWhiteSpace(filtroProveedor))
                return RepositorioWV_ZctProveedoresCxP.ObtenerTodo().ToList();

            return RepositorioWV_ZctProveedoresCxP.ObtenerElementos(m => m.Nom_Prov.Trim().ToUpper().Contains(filtroProveedor.Trim().ToUpper())).ToList();
        }

        public List<ZctDetCuentasPagar> TraePagosVencidosProveedor(DateTime fecha, int cod_prov)
        {
            return RepositorioDetallesCuentasPorPagar.ObtenerElementos(p => p.fecha_vencido <= fecha &&
                                                                     p.ZctEncCuentasPagar.ZctEncOC.ZctCatProv.Cod_Prov == cod_prov).ToList();
        }
        public ZctDetCuentasPagar CreaPago(ZctEncCuentasPagar cxp, string Referencia, int Metodo, decimal monto, decimal cambio, DateTime fechapago, DateTime fechavencido)
        {
            ZctDetCuentasPagar detpago = new ZctDetCuentasPagar();
            detpago.cod_cuenta_pago = cxp.cod_cuenta_pago;
            //detpago.cambio = 0;
            //detpago.fecha_pago = DateTime.Now;
            int Consecutivo = 1;
            int ConsecutivosPagos = RepositorioDetallesCuentasPorPagar.Contador(c => c.cod_cuenta_pago == cxp.cod_cuenta_pago);
            if (ConsecutivosPagos > 0)
            {
                Consecutivo = Convert.ToInt32(RepositorioDetallesCuentasPorPagar.ObtenerElementos(c => c.cod_cuenta_pago == cxp.cod_cuenta_pago).Select(m=> m.consecutivo).OrderByDescending(m=> m).FirstOrDefault() + 1);
            }
            detpago.referencia = Referencia;
            detpago.consecutivo = Consecutivo;
            detpago.fecha_vencido = cxp.fecha_vencido;
            detpago.codtipo_pago = Metodo;
            detpago.Estado = ZctEncCuentasPagar.Estados.PENDIENTE;
            detpago.fecha_factura = cxp.fecha_vencido;
            return detpago;
        }
        public void ActualizarPagos(ZctEncCuentasPagar CxP, DtoUsuario usuario)//, ZctDetCuentasPagar Detalle = null)
        {
            //if (Detalle != null)
            //{
            //    //CxP.ZctDetCuentasPagar.Add(Detalle);
            //    RepositorioDetallesCuentasPorPagar.Agregar(Detalle);
            //}

            if (CxP.Estado == ZctEncCuentasPagar.Estados.CANCELADO)
                throw new SOTException(Recursos.CuentasPorPagar.cancelacion_manual_no_permitida_excepcion);

            CxP.update_web = false;

            var detallesParaCentrosCostos = new List<ZctDetCuentasPagar>();

#warning ojo aqui
            if (CxP.ZctDetCuentasPagar.Count > 1)
                throw new SOTException("En estos momentos solamente se puede manejar un detalle de pago por cuenta");

            DateTime? fechaPago = null;

            foreach (var detalle in CxP.ZctDetCuentasPagar)
            {
                var original = RepositorioDetallesCuentasPorPagar.Obtener(m => m.cod_cuenta_pago == detalle.cod_cuenta_pago && m.consecutivo == detalle.consecutivo);

                if (detalle.codtipo_pago.HasValue && detalle.codtipo_pago == 0)
                    throw new SOTException("El tipo de pago seleccionado es inválido");

                if (detalle.cod_banco.HasValue && detalle.cod_banco == 0)
                    throw new SOTException("El banco seleccionado es inválido");

                if (detalle.Estado == ZctEncCuentasPagar.Estados.CANCELADO)
                    throw new SOTException(Recursos.CuentasPorPagar.cancelacion_manual_no_permitida_excepcion);

                if (detalle.Estado != ZctEncCuentasPagar.Estados.PAGADO && original.Estado == ZctEncCuentasPagar.Estados.PAGADO)
                    throw new SOTException(Recursos.CuentasPorPagar.detalle_ya_pagado_excepcion, detalle.consecutivo.ToString(), detalle.cod_cuenta_pago.ToString());
                else if (detalle.Estado == ZctEncCuentasPagar.Estados.PAGADO && original.Estado != ZctEncCuentasPagar.Estados.PAGADO)
                    detallesParaCentrosCostos.Add(detalle);

                if (original.Estado == ZctEncCuentasPagar.Estados.PAGADO && detalle.fecha_pago != original.fecha_pago)
                    throw new SOTException(Recursos.CuentasPorPagar.fecha_pago_cambiada_excepcion);

                    detalle.EntidadEstado = Dominio.Nucleo.Entidades.EntidadEstados.Modificado;

                if (detalle.fecha_pago.HasValue)
                    fechaPago = detalle.fecha_pago;
            }

            RepositorioEncabezadosCuentasPorPagar.Modificar(CxP);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioEncabezadosCuentasPorPagar.GuardarCambios();
                RepositorioDetallesCuentasPorPagar.GuardarCambios();

                if (CxP.Estado == ZctEncCuentasPagar.Estados.PAGADO)
                {
                    var idsTiposPagos = CxP.ZctDetCuentasPagar.Select(m => m.codtipo_pago.Value).ToList();

                    var clasificacionesGastos = ServicioTiposPagos.ObtenerClasificacionesGastosPorTipos(idsTiposPagos);

                    if (clasificacionesGastos.Count > 1)
                        throw new SOTException(Recursos.CuentasPorPagar.pagos_con_diferentes_clasificaciones_gastos_exception);

                    ServicioOrdenesCompra.ReflejaEnCentroCostos(CxP.cod_cuenta_pago, fechaPago.Value, CxP.Cod_OC, usuario.Id, clasificacionesGastos.Count > 0 ? (ClasificacionesGastos?)clasificacionesGastos.First() : null);
                }

                scope.Complete();
            }
        }

        //public void Refresca()
        //{
        //    _dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
        //}
        public List<Transversal.Dtos.DtoEnum> TraeEstadosPago()
        {
            return Transversal.Extensiones.EnumExtensions.ComoListaDto<ZctEncCuentasPagar.Estados>();
        }
        public void CancelarCxP(int OC)
        {
            RepositorioEncabezadosCuentasPorPagar.SP_EstadoCxP(OC, ZctEncCuentasPagar.Estados.CANCELADO);
        }

        public DateTime? ObtenerFechaUltimaCuenta(int codigoOrdenCompra, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarOrdenCompra = true });

            var estadoCancelado = (int)ZctEncCuentasPagar.Estados.CANCELADO;

            return RepositorioEncabezadosCuentasPorPagar.ObtenerOrdenado(m => m.Cod_OC == codigoOrdenCompra && m.idEstado != estadoCancelado, m => m.fecha_creacion)?.fecha_creacion;
        }
        //public List<ZctBancos> TraeBancos()
        //{
        //    return (from b in _dao.ZctBancos select b).ToList();
        //}
        //public Proveedor CambiaProveedor(int cod_prov)
        //{
        //    var prov = (from p in _dao.Proveedor where p.Cod_Prov == cod_prov select p).ToList()[0];
        //    _dao.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, prov);
        //    return prov;
        //}
    }
}
