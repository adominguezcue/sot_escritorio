﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Marcas
{
    public interface IServicioMarcas
    {
        List<Marca> ObtenerMarcas();
    }
}
