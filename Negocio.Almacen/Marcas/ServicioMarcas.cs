﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.Marcas
{
    public class ServicioMarcas : IServicioMarcas
    {
        IRepositorioMarcas RepositorioMarcas
        {
            get { return _repositorioTiposArticulos.Value; }
        }

        Lazy<IRepositorioMarcas> _repositorioTiposArticulos = new Lazy<IRepositorioMarcas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioMarcas>(); });

        public List<Marca> ObtenerMarcas() 
        {
            return RepositorioMarcas.ObtenerTodo().ToList();
        }
    }
}
