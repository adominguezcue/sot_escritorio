﻿using System;
using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;

namespace Negocio.Almacen.Clientes
{
    public interface IServicioClientes
    {
        bool VerificarNoExisteCliente();

        int ObtenerUltimoIdCliente();
        Cliente ObtenerUltimoCliente();
        void CrearCliente(Cliente cliente, DtoUsuario usuario);
        void ModificarCliente(Cliente cliente, DtoUsuario usuario);
        void EliminarCliente(int codigoCliente, DtoUsuario usuario);
    }
}
