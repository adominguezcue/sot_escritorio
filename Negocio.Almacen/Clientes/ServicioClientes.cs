﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Parametros;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Transacciones;

namespace Negocio.Almacen.Clientes
{
    public class ServicioClientes : IServicioClientes
    {
        IRepositorioClientes RepositorioClientes
        {
            get { return _repositorioClientes.Value; }
        }

        Lazy<IRepositorioClientes> _repositorioClientes = new Lazy<IRepositorioClientes>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioClientes>(); });

        IServicioPermisos AplicacionServicioPermisos
        {
            get { return _aplicacionServicioPermisos.Value; }
        }

        private Lazy<IServicioPermisos> _aplicacionServicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        IServicioParametrosInterno AplicacionServicioParametros
        {
            get { return _aplicacionServicioParametros.Value; }
        }

        private Lazy<IServicioParametrosInterno> _aplicacionServicioParametros = new Lazy<IServicioParametrosInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioParametrosInterno>());

        public bool VerificarNoExisteCliente()
        {
            return !RepositorioClientes.Alguno(m => true);
        }


        public int ObtenerUltimoIdCliente()
        {
            return RepositorioClientes.ObtenerTodo().OrderBy(m => m.Cod_Cte).Select(m => m.Cod_Cte).FirstOrDefault();
        }


        public Cliente ObtenerUltimoCliente()
        {
            return RepositorioClientes.ObtenerTodo().OrderBy(m => m.Cod_Cte).FirstOrDefault();
        }

        public void CrearCliente(Cliente cliente, DtoUsuario usuario)
        {
            AplicacionServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearDatosFiscalesClientes = true });

            if (cliente == null)
                throw new SOTException(Recursos.Clientes.cliente_nulo_excepcion);

            ValidarDatos(cliente);

            cliente.uuid = Guid.NewGuid();
            cliente.update_web = false;

            using (var scope = new SotTransactionScope())
            {
                RepositorioClientes.Agregar(cliente);
                RepositorioClientes.GuardarCambios();

                //AplicacionServicioParametros.ActualizarParametros(cliente);

                scope.Complete();
            }

        }

        public void ModificarCliente(Cliente cliente, DtoUsuario usuario)
        {
            AplicacionServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearDatosFiscalesClientes = true });

            if (cliente == null)
                throw new SOTException(Recursos.Clientes.cliente_nulo_excepcion);

            var clienteOriginal = RepositorioClientes.Obtener(m => m.Cod_Cte == cliente.Cod_Cte);

            if (clienteOriginal == null)
                throw new SOTException(Recursos.Clientes.cliente_nulo_excepcion);

            cliente.uuid = clienteOriginal.uuid;
            cliente.update_web = clienteOriginal.update_web;

            clienteOriginal.SincronizarPrimitivas(cliente);

            ValidarDatos(clienteOriginal);

            using (var scope = new SotTransactionScope())
            {
                RepositorioClientes.Modificar(clienteOriginal);
                RepositorioClientes.GuardarCambios();

                //AplicacionServicioParametros.ActualizarParametros(clienteOriginal);

                scope.Complete();
            }
        }

        public void EliminarCliente(int codigoCliente, DtoUsuario usuario)
        {
            throw new NotImplementedException("Eliminación de clientes no implementada");
        }

        private void ValidarDatos(Cliente cliente)
        {
            if (string.IsNullOrWhiteSpace(cliente.RFC_Cte))
            {
                cliente.RFC_Cte = "XAXX010101000";
            }
            var ErrMsj = "";

            if (!cliente.TipoPersona.HasValue)
            {
                ErrMsj = ErrMsj + "Seleccione un tipo de persona" + System.Environment.NewLine;
            }
            else if (cliente.TipoPersona.Value == TiposPersonas.Moral)
            {
                if (cliente.RFC_Cte.Length != 12)
                {
                    ErrMsj = ErrMsj + "El RFC para las personas morales debe ser de 12 caracteres" + System.Environment.NewLine;

                }
            }
            else if (cliente.RFC_Cte.Length != 13)
            {
                ErrMsj = ErrMsj + "El RFC para las personas fisicas debe ser de 13 caracteres" + System.Environment.NewLine;


            }

            if (string.IsNullOrWhiteSpace(cliente.Nom_Cte))
                ErrMsj = ErrMsj + "Debe de proprocionar el nombre del hotel." + System.Environment.NewLine;
            if (string.IsNullOrWhiteSpace(cliente.calle))
                ErrMsj = ErrMsj + "Debe proporcionar la calle." + System.Environment.NewLine;
            if (string.IsNullOrWhiteSpace(cliente.colonia))
                ErrMsj = ErrMsj + "Debe debe proporcionar la colonia." + System.Environment.NewLine;
            if (string.IsNullOrWhiteSpace(cliente.cp))
                ErrMsj = ErrMsj + "Debe proporcionar el codigo postal." + System.Environment.NewLine;
            if (!cliente.Cod_Edo.HasValue || cliente.Cod_Edo == 0)
                ErrMsj = ErrMsj + "Debe seleccionar un estado." + System.Environment.NewLine;
            if (!cliente.Cod_Ciu.HasValue || cliente.Cod_Ciu == 0)
                ErrMsj = ErrMsj + "Debe seleccionar una ciudad." + System.Environment.NewLine;
            if (string.IsNullOrWhiteSpace(cliente.pais))
                ErrMsj = ErrMsj + "Debe proporcionar el País." + System.Environment.NewLine;
            if (ErrMsj.Length > 0)
                throw new SOTException("Debe corregir los siguientes errores para poder guardar los datos fiscales del hotel: " + System.Environment.NewLine + ErrMsj);

            //VALIDACIÓN PARA LOS DATOS

            #region Validaciones de cliente único (hotel)

            var idClienteExistente = ObtenerUltimoIdCliente();

            if (!VerificarNoExisteCliente() && idClienteExistente != cliente.Cod_Cte)
            {
                throw new SOTException("Ya existen datos fiscales para este hotel");
            }

            #endregion
        }
    }
}
