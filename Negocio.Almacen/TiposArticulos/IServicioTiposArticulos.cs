﻿using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.TiposArticulos
{
    public interface IServicioTiposArticulos
    {
        /// <summary>
        /// Retorna los tipos de artículos en el sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<TipoArticulo> ObtenerTipos(DtoUsuario usuario);
    }
}
