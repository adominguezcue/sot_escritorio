﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.TiposArticulos
{
    public class ServicioTiposArticulos : IServicioTiposArticulos
    {
        IRepositorioTiposArticulos RepositorioTiposArticulos
        {
            get { return _repositorioTiposArticulos.Value; }
        }

        Lazy<IRepositorioTiposArticulos> _repositorioTiposArticulos = new Lazy<IRepositorioTiposArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioTiposArticulos>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });


        public List<TipoArticulo> ObtenerTipos(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarTiposArticulos = true });

            return RepositorioTiposArticulos.ObtenerTodo().ToList();
        }
    }
}
