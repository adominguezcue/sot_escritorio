﻿using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.Folios
{
    public class ServicioFolios : IServicioFoliosInterno
    {
        IRepositorioFolios RepositorioFolios
        {
            get { return _repositorioFolios.Value; }
        }

        Lazy<IRepositorioFolios> _repositorioFolios = new Lazy<IRepositorioFolios>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioFolios>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        #region métodos internal

        DtoFolio IServicioFoliosInterno.GenerarYObtenerSiguienteFolio(string tipoFolio)
        {
            var folio = RepositorioFolios.ObtenerElementos(m => m.Cod_Folio == tipoFolio).FirstOrDefault();

            if (folio == null)
                return null;

            folio.Consc_Folio = (folio.Consc_Folio ?? 0) + 1;

            RepositorioFolios.Modificar(folio);
            RepositorioFolios.GuardarCambios();

            return new DtoFolio
            {
                Cons = folio.Consc_Folio
            };

            //return RepositorioFolios.SP_ZctCatFolios(tipoFolio);
        }

        int IServicioFoliosInterno.ObtenerSiguienteFolio(string tipoFolio)
        {
            return RepositorioFolios.ObtenerElementos(m => m.Cod_Folio == tipoFolio).Select(m => m.Consc_Folio ?? 0).FirstOrDefault() + 1;
        }

        #endregion
    }
}
