﻿using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Folios
{
    internal interface IServicioFoliosInterno : IServicioFolios
    {
        DtoFolio GenerarYObtenerSiguienteFolio(string tipoFolio);

        int ObtenerSiguienteFolio(string tipoFolio);
    }
}
