﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Fichas
{
    public interface IServicioFichas
    {
        /// <summary>
        /// Retorna las fichas de activo fijo en base al día
        /// </summary>
        /// <param name="nombreArticulo"></param>
        /// <param name="dia"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Ficha> ObtenerFichasDia(DateTime dia, DtoUsuario usuario, string nombreArticulo = "");
        /// <summary>
        /// Retorna la ficha activa con el id proporcionado
        /// </summary>
        /// <param name="idFicha"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Ficha ObtenerPorIdConArchivos(int idFicha, DtoUsuario usuario);
        /// <summary>
        /// Marca una ficha como eliminada
        /// </summary>
        /// <param name="idFicha"></param>
        /// <param name="usuario"></param>
        void EliminarFicha(int idFicha, DtoUsuario usuario);

        void AgregarFicha(Ficha ficha, DtoUsuario usuario);

        void ModificarFicha(Ficha ficha, List<DtoArchivoFicha> archivos, DtoUsuario usuario);
    }
}
