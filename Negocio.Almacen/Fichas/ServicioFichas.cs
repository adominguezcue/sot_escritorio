﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Proveedores;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Transversal.Utilidades;

namespace Negocio.Almacen.Fichas
{
    public class ServicioFichas : IServicioFichas
    {
        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioProveedores ServicioProveedores
        {
            get { return _servicioProveedores.Value; }
        }

        Lazy<IServicioProveedores> _servicioProveedores = new Lazy<IServicioProveedores>(() => { return FabricaDependencias.Instancia.Resolver<IServicioProveedores>(); });

        IRepositorioFichas RepositorioFichas
        {
            get { return _repositorioFichas.Value; }
        }

        Lazy<IRepositorioFichas> _repositorioFichas = new Lazy<IRepositorioFichas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioFichas>(); });

        IRepositorioArchivosFicha RepositorioArchivosFicha
        {
            get { return _repositorioArchivosFicha.Value; }
        }

        Lazy<IRepositorioArchivosFicha> _repositorioArchivosFicha = new Lazy<IRepositorioArchivosFicha>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioArchivosFicha>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        public List<Ficha> ObtenerFichasDia(DateTime dia, DtoUsuario usuario, string nombreArticulo = "")
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarFichas = true });

            return RepositorioFichas.ObtenerFichasDia(dia, nombreArticulo);
        }


        public Ficha ObtenerPorIdConArchivos(int idFicha, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarFichas = true });

            return RepositorioFichas.ObtenerPorIdConArchivos(idFicha);
        }

        public void AgregarFicha(Ficha ficha, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarFichas = true });

            if (ficha == null)
                throw new SOTException(Recursos.Fichas.nula_excepcion);

            ValidarDatos(ficha);

            var fechaActual = DateTime.Now;

            ficha.Activa = true;
            ficha.FechaCreacion = fechaActual;
            ficha.FechaModificacion = fechaActual;
            ficha.IdUsuarioCreo = usuario.Id;
            ficha.IdUsuarioModifico = usuario.Id;

            ArchivoFicha archivoInactivo;

            while ((archivoInactivo = ficha.ArchivosFicha.FirstOrDefault(m => !m.Activa)) != null)
                ficha.ArchivosFicha.Remove(archivoInactivo);

            foreach (var archivo in ficha.ArchivosFicha)
            {
                archivo.FechaCreacion = fechaActual;
                archivo.FechaModificacion = fechaActual;
                archivo.IdUsuarioCreo = usuario.Id;
                archivo.IdUsuarioModifico = usuario.Id;
            }

            RepositorioFichas.Agregar(ficha);
            RepositorioFichas.GuardarCambios();
        }

        public void ModificarFicha(Ficha fichaM, List<DtoArchivoFicha> archivos, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarFichas = true });

            if (fichaM == null)
                throw new SOTException(Recursos.Fichas.nula_excepcion);

            if (!fichaM.Activa)
                throw new SOTException(Recursos.Fichas.ficha_no_eliminable_excepcion);

            var ficha = RepositorioFichas.Obtener(m => m.Activa && m.Id == fichaM.Id);

            if (ficha == null)
                throw new SOTException(Recursos.Fichas.id_invalido_excepcion);

            ValidarDatos(fichaM);

            if (archivos.GroupBy(m => m.Nombre.ToUpper()).Count() != archivos.Count)
                throw new SOTException(Recursos.Fichas.archivos_duplicados_excepcion);

            foreach (var archivo in archivos)
            {
                if (archivo.Valor == null || archivo.Valor.Length == 0)
                    throw new SOTException(Recursos.Fichas.archivo_invalido_excepcion, archivo.Nombre);
            }

            ficha.SincronizarPrimitivas(fichaM);

            var fechaActual = DateTime.Now;

            //ficha.Activa = true;
            ficha.FechaModificacion = fechaActual;
            ficha.IdUsuarioModifico = usuario.Id;

            var archivosOriginales = RepositorioArchivosFicha.ObtenerElementos(m => m.IdFicha == ficha.Id).ToList();

            foreach (var grupo in (from archivoN in archivos
                                   join archivoO in archivosOriginales on archivoN.Nombre.ToUpper() equals archivoO.Nombre.ToUpper() into arch
                                   from archivoO in arch.DefaultIfEmpty()
                                   select new
                                   {
                                       archivoN,
                                       archivoO
                                   })) 
            {
                if (grupo.archivoO == null)
                    ficha.ArchivosFicha.Add(new ArchivoFicha
                    {
                        Activa = true,
                        Valor = grupo.archivoN.Valor,
                        Nombre = grupo.archivoN.Nombre
                    });
                else
                {
                    grupo.archivoO.Valor = grupo.archivoN.Valor;
                    RepositorioArchivosFicha.Modificar(grupo.archivoO);
                }
            }

            foreach (var grupo in (from archivoO in archivosOriginales
                                   join archivoN in archivos on archivoO.Nombre.ToUpper() equals archivoN.Nombre.ToUpper() into arch
                                   from archivoN in arch.DefaultIfEmpty()
                                   select new
                                   {
                                       archivoN,
                                       archivoO
                                   })) 
            {
                if(grupo.archivoN == null)
                    RepositorioArchivosFicha.Eliminar(grupo.archivoO);
            }

                //ArchivoFicha archivoInactivo;

                //while ((archivoInactivo = ficha.ArchivosFicha.FirstOrDefault(m => !m.Activa && m.Id == 0)) != null)
                //    ficha.ArchivosFicha.Remove(archivoInactivo);

                foreach (var archivo in ficha.ArchivosFicha)
                {
                    if (archivo.Id == 0)
                    {
                        archivo.FechaCreacion = fechaActual;
                        archivo.IdUsuarioCreo = usuario.Id;
                    }
                    if (!archivo.Activa)
                    {
                        archivo.FechaEliminacion = fechaActual;
                        archivo.IdUsuarioElimino = usuario.Id;
                    }

                    archivo.FechaModificacion = fechaActual;
                    archivo.IdUsuarioModifico = usuario.Id;


                    archivo.EntidadEstado = archivo.Id == 0 ?
                    Dominio.Nucleo.Entidades.EntidadEstados.Creado :
                    Dominio.Nucleo.Entidades.EntidadEstados.Modificado;
                }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioArchivosFicha.GuardarCambios();

                RepositorioFichas.Modificar(ficha);
                RepositorioFichas.GuardarCambios();

                scope.Complete();
            }
        }

        private void ValidarDatos(Ficha ficha)
        {
            if (!ServicioArticulos.VerificarEsActivoFijo(ficha.Cod_Art))
                throw new SOTException(Recursos.Fichas.articulo_no_activo_fijo_excepcion);

            if (!ServicioProveedores.VerificarProveedor(ficha.Cod_Prov))
                throw new SOTException(Recursos.Fichas.proveedor_invalido_excepcion);

            if (!UtilidadesRegex.LetrasONumeros(ficha.Poliza))
                throw new SOTException(Recursos.Fichas.poliza_invalida_excepcion);

            if (!UtilidadesRegex.TextoONumerosConPuntuacion(ficha.Observaciones))
                throw new SOTException(Recursos.Fichas.observaciones_invalidas_excepcion);

            foreach (var archivo in ficha.ArchivosFicha)
            {
                if (!archivo.Activa)
                    continue;

                if (archivo.Valor == null || archivo.Valor.Length == 0)
                    throw new SOTException(Recursos.Fichas.archivo_invalido_excepcion, archivo.Nombre);
            }
        }


        public void EliminarFicha(int idFicha, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { EliminarFichas = true });

            var ficha = RepositorioFichas.Obtener(m => m.Activa && m.Id == idFicha, m => m.ArchivosFicha);

            if (ficha == null)
                throw new SOTException(Recursos.Fichas.id_invalido_excepcion);

            var fechaActual = DateTime.Now;

            ficha.Activa = false;
            ficha.FechaEliminacion = fechaActual;
            ficha.FechaModificacion = fechaActual;
            ficha.IdUsuarioElimino = usuario.Id;
            ficha.IdUsuarioModifico = usuario.Id;

            foreach (var archivo in ficha.ArchivosFicha) 
            {
                if (!archivo.Activa)
                    continue;

                archivo.Activa = false;
                archivo.FechaEliminacion = fechaActual;
                archivo.FechaModificacion = fechaActual;
                archivo.IdUsuarioElimino = usuario.Id;
                archivo.IdUsuarioModifico = usuario.Id;
            }

            RepositorioFichas.Modificar(ficha);
            RepositorioFichas.GuardarCambios();
        }
    }
}
