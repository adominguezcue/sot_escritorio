﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.Almacen.Proveedores
{
    public interface IServicioProveedores
    {
        bool VerificarProveedor(int idProveedor);
        /// <summary>
        /// Retorna los proveedores existentes en el sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        /// 
        List<Proveedor> ObtieneProveedoresPorFiltroNombre(string text);
        List<Proveedor> ObtenerProveedores(DtoUsuario usuario);
        /// <summary>
        /// Retorna la información básica de los proveedores existentes en el sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<DtoResumenProveedor> ObtenerResumenesProveedores(DtoUsuario usuario, string filtro = null);
        /// <summary>
        /// Retorna el proveedor con el código proporcionado
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="codigo"></param>
        /// <returns></returns>
        Proveedor ObtenerProveedor(DtoUsuario usuario, int codigo);
        /// <summary>
        /// Valida que exista un proveedor con el id proporcionado y lo retorna
        /// </summary>
        /// <param name="Cod_prov"></param>
        /// <returns></returns>
        Proveedor ValidarYObtenerProveedor(int Cod_prov);

        void CrearProveedor(Proveedor proveedor, DtoUsuario usuario);

        void ModificarProveedor(Proveedor proveedor, DtoUsuario usuario);

        void Elimina(int idProveedor, DtoUsuario usuario);

        string ObtenerSiguienteCodigoProveedor();

        int ObtenerPosicionProveedor(int Cod_proveedor);
    }
}
