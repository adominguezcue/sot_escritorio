﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.OrdenesCompra;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.Proveedores
{
    public class ServicioProveedores : IServicioProveedores
    {
        IRepositorioProveedores RepositorioProveedores
        {
            get { return _repositorioProveedores.Value; }
        }

        Lazy<IRepositorioProveedores> _repositorioProveedores = new Lazy<IRepositorioProveedores>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioProveedores>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioOrdenesCompraInterno ServicioOrdenesCompra
        {
            get { return _servicioOrdenesCompra.Value; }
        }

        Lazy<IServicioOrdenesCompraInterno> _servicioOrdenesCompra = new Lazy<IServicioOrdenesCompraInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioOrdenesCompraInterno>(); });


        public bool VerificarProveedor(int idProveedor)
        {
            return RepositorioProveedores.Alguno(m => m.Cod_Prov == idProveedor);
        }

        public List<Proveedor> ObtenerProveedores(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarProveedor = true });

            return RepositorioProveedores.ObtenerTodo().ToList();
        }

        public List<DtoResumenProveedor> ObtenerResumenesProveedores(DtoUsuario usuario, string filtro = null)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarProveedor = true });

            return RepositorioProveedores.ObtenerResumenesProveedores(filtro);
        }

        public Proveedor ObtenerProveedor(DtoUsuario usuario, int codigo) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarProveedor = true });

            return RepositorioProveedores.Obtener(m => m.Cod_Prov == codigo);
        }

        public Proveedor ValidarYObtenerProveedor(int Cod_prov)
        {
            var proveedor = RepositorioProveedores.Obtener(p=> p.Cod_Prov == Cod_prov);

            if(proveedor == null)
                throw new SOTException(Recursos.Proveedores.codigo_proveedor_invalido_excepcion, Cod_prov.ToString());

            return proveedor;
        }

        public void CrearProveedor(Proveedor proveedor, DtoUsuario usuario)
        {

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearProveedor = true });

            if (proveedor == null)
                throw new SOTException(Recursos.Proveedores.nulo_excepcion);

            ValidarDatos(proveedor);

            RepositorioProveedores.Agregar(proveedor);
            RepositorioProveedores.GuardarCambios();
        }

        public void ModificarProveedor(Proveedor proveedor, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarProveedor = true });

            if (proveedor == null)
                throw new SOTException(Recursos.Proveedores.nulo_excepcion);

            if (!RepositorioProveedores.Alguno(m => m.Cod_Prov == proveedor.Cod_Prov))
                throw new SOTException(Recursos.Proveedores.codigo_proveedor_invalido_excepcion, proveedor.Cod_Prov.ToString());

            ValidarDatos(proveedor);

            RepositorioProveedores.Modificar(proveedor);
            RepositorioProveedores.GuardarCambios();
        }

        private void ValidarDatos(Proveedor proveedor)
        {
            StringBuilder ErrorM = new StringBuilder();
            if (string.IsNullOrWhiteSpace(proveedor.Nom_Prov))
                ErrorM.AppendLine("Debe proporcionar la razón social del proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.NombreComercial))
                ErrorM.AppendLine("Debe proporcionar el nombre comercial del proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.RFC_Prov))
                ErrorM.AppendLine("Debe proporcionar el RFC del proveedor");
            else if (proveedor.RFC_Prov.Length < 12 || proveedor.RFC_Prov.Length > 13)
                ErrorM.AppendLine("El RFC del proveedor debe ser de mínimo 12 caracteres y un máximo de 13 caracteres, el actual tiene " + proveedor.RFC_Prov.Length.ToString() + " caracteres");
            
            if (string.IsNullOrWhiteSpace(proveedor.email))
                ErrorM.AppendLine("Debe proporcionar un correo electrónico para el proveedor");
            else if(!Transversal.Utilidades.UtilidadesRegex.Email(proveedor.email))
                ErrorM.AppendLine("El correo electrónico proporcionado para el proveedor no es válido");

            if (string.IsNullOrWhiteSpace(proveedor.calle))
                ErrorM.AppendLine("Debe proporcionar la calle del proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.colonia))
                ErrorM.AppendLine("Debe proporcionar una colonia para el proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.cp))
                ErrorM.AppendLine("Debe proporcionar un código postal para el proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.telefono))
                ErrorM.AppendLine("Debe proporcionar un teléfono para el proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.numext))
                ErrorM.AppendLine("Debe proporcionar un número exterior para el proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.estado))
                ErrorM.AppendLine("Debe proporcionar un estado para el proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.ciudad))
                ErrorM.AppendLine("Debe proporcionar una ciudad para el proveedor");

            if (string.IsNullOrWhiteSpace(proveedor.Contacto))
                ErrorM.AppendLine("Debe proporcionar un nombre de contacto para el proveedor");

            if (ErrorM.Length > 0)
                throw new SOTException("Debe corregir los siguentes errores antes de guardar el proveedor: " + System.Environment.NewLine + ErrorM.ToString());
        }

        public void Elimina(int idProveedor, DtoUsuario usuario)
        {
            var proveedor = RepositorioProveedores.Obtener(m => m.Cod_Prov == idProveedor);

            if (proveedor == null)
                throw new SOTException(Recursos.Proveedores.nulo_excepcion);

            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarProveedor = true });

            if (ServicioOrdenesCompra.VerificarExistenOrdenesParaProveedor(idProveedor))
                throw new SOTException(Recursos.Proveedores.proveedor_tiene_ordenes_compra_excepcion);

            RepositorioProveedores.Eliminar(proveedor);
            RepositorioProveedores.GuardarCambios();
        }

        public string ObtenerSiguienteCodigoProveedor()
        {
            return (RepositorioProveedores.ObtenerUltimoCodigo() + 1).ToString();
        }

        public int ObtenerPosicionProveedor(int Cod_proveedor)
        {
            if (RepositorioProveedores.Alguno(m => m.Cod_Prov == Cod_proveedor))
                return RepositorioProveedores.Contador(m => m.Cod_Prov < Cod_proveedor);

            return -1;

            //List<Modelo.ZctCatProv> provedores = (from p in _dao.ZctCatProv where p.Cod_Prov == Cod_proveedor select p).ToList();
            //if (provedores.Count == 0)
            //{
            //    provedores = null;
            //    return -1;
            //}
            //int res = TraeProveedores().IndexOf(provedores[0]);
            //return res;
        }

        public List<Proveedor> ObtieneProveedoresPorFiltroNombre(string text)
        {
            return RepositorioProveedores.ObtieneProveedoresPorFiltroNombre(text);
        }
    }
}
