﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
namespace Negocio.Almacen.Inventarios
{
    public interface IServicioInventarios
    {
        /// <summary>
        /// Este método valida las restricciones que tienen los almacenes para soportar entradas y salidas
        /// </summary>
        /// <param name="claveFolio"></param>
        /// <param name="CodMov_Inv"></param>
        /// <param name="esEntrada"></param>
        /// <param name="FolOS_Inv"></param>
        /// <param name="Cod_Art"></param>
        /// <param name="CtdMov_Inv"></param>
        /// <param name="FchMov_Inv"></param>
        /// <param name="preparar"></param>
        /// <param name="codigoAlmacen"></param>
        /// <returns></returns>
        decimal AgregarMovimientoComanda(string claveFolio, int CodMov_Inv, bool esEntrada, string FolOS_Inv, string Cod_Art, decimal CtdMov_Inv, DateTime FchMov_Inv, bool preparar, int? codigoAlmacen = null);
        void AgregarMovimientoComanda(string claveFolio, int CodMov_Inv, bool esEntrada, decimal CosMov_Inv, string FolOS_Inv, string Cod_Art, decimal CtdMov_Inv, DateTime FchMov_Inv, bool preparar, int? codigoAlmacen = null, bool validarEntradasSalidas = false);
        decimal ObtenerUltimoCostoMovimiento(string codigoArticulo);
        List<DtoItemKardex> SP_ZctKdxInv(string cod_Art, int cod_Alm, DateTime fchIni, DateTime fchFin, DtoUsuario usuario);
        bool VerificarExisteMovimiento(string codigoArticulo);

        List<VW_ZctInventarioFinal> DetallesInventario(string folioInventario, DtoUsuario usuario);
    }
}
