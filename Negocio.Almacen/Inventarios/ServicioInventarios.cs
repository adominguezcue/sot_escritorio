﻿using Modelo.Almacen.Constantes;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Almacenes;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Recetas;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.Inventarios
{
    public class ServicioInventarios : IServicioInventarios
    {
        IRepositorioZctEncMovInv RepositorioZctEncMovInv
        {
            get { return _repositorioZctEncMovInv.Value; }
        }

        Lazy<IRepositorioZctEncMovInv> _repositorioZctEncMovInv = new Lazy<IRepositorioZctEncMovInv>(() => FabricaDependencias.Instancia.Resolver<IRepositorioZctEncMovInv>());

        IRepositorioVW_ZctInventarioFinal RepositorioVW_ZctInventarioFinal
        {
            get { return _repositorioVW_ZctInventarioFinal.Value; }
        }

        Lazy<IRepositorioVW_ZctInventarioFinal> _repositorioVW_ZctInventarioFinal = new Lazy<IRepositorioVW_ZctInventarioFinal>(() => FabricaDependencias.Instancia.Resolver<IRepositorioVW_ZctInventarioFinal>());

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        IServicioArticulosInterno ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulosInterno> _servicioArticulos = new Lazy<IServicioArticulosInterno>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulosInterno>(); });

        IServicioMixItems ServicioMixItems
        {
            get { return _servicioMixItems.Value; }
        }

        Lazy<IServicioMixItems> _servicioMixItems = new Lazy<IServicioMixItems>(() => { return FabricaDependencias.Instancia.Resolver<IServicioMixItems>(); });

        IServicioAlmacenes ServicioAlmacenes
        {
            get { return _servicioAlmacenes.Value; }
        }

        Lazy<IServicioAlmacenes> _servicioAlmacenes = new Lazy<IServicioAlmacenes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioAlmacenes>(); });

        

        public decimal ObtenerUltimoCostoMovimiento(string codigoArticulo) 
        {
            return RepositorioZctEncMovInv.ObtenerUltimoCostoMovimiento(codigoArticulo);
        }

        public bool VerificarExisteMovimiento(string codigoArticulo)
        {
            return RepositorioZctEncMovInv.Alguno(m=> m.Cod_Art == codigoArticulo);
        }

        public List<DtoItemKardex> SP_ZctKdxInv(string cod_Art, int cod_Alm, DateTime fchIni, DateTime fchFin, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarAlmacenes = true, ConsultarInventario = true });

            return RepositorioZctEncMovInv.SP_ZctKdxInv(cod_Art, cod_Alm, fchIni, fchFin);
        }

        public List<VW_ZctInventarioFinal> DetallesInventario(string folioInventario, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarAlmacenes = true, ConsultarInventario = true });

            return RepositorioVW_ZctInventarioFinal.ObtenerElementos(m => m.Folio == folioInventario).OrderBy(m => m.FchyHrReg).ToList();
        }

        //public void AgregarMovimientoComanda(string claveFolio, int codigoMovimiento, bool esEntrada, decimal costoMovimiento, string folioMovimiento, string codigoArticulo, decimal candidadMover, DateTime fechaMovimiento, bool preparar, int? codigoAlmacen = null)
        //{
        //    string tipoMovimiento = esEntrada ? "EN" : "SA";

        //    var articuloXalmacen = ServicioArticulos.ObtenerArticulosAlmacenesAceptanSalida(codigoArticulo);

        //    var articulo = ServicioArticulos.ObtenerArticuloPorId(codigoArticulo);

        //    if (articulo == null)
        //        throw new SOTException(Recursos.Articulos.codigo_articulo_invalido_excepcion, codigoArticulo);


        //    var almacenes = esEntrada ? ServicioAlmacenes.ObtenerAlmacenesAceptanEntradas() :
        //                                ServicioAlmacenes.ObtenerAlmacenesAceptanSalidas();

        //    decimal movimientosAbsolutos = Math.Abs(candidadMover);

        //    var almacenesExistencias = esEntrada ? almacenes.Select(m => new { m.Cod_Alm, ext = movimientosAbsolutos }).ToList() :

        //        (from almacen in almacenes
        //         join articuloAlmacen in articuloXalmacen on almacen.Cod_Alm equals articuloAlmacen.Cod_Alm into aa
        //         from articuloAlmacen in aa.DefaultIfEmpty()
        //         where articuloAlmacen != null
        //         || (articulo.cod_alm.HasValue && almacen.Cod_Alm == articulo.cod_alm)
        //         select new
        //         {
        //             almacen.Cod_Alm,
        //             ext = articuloAlmacen != null ? (articuloAlmacen.Exist_Art ?? 0) : 0
        //         }).OrderByDescending(m => m.ext).ToList();

        //    //Si envían un código de almacén, se filtran las listas
        //    if (codigoAlmacen.HasValue)
        //    {
        //        almacenesExistencias = almacenesExistencias.Where(m => m.Cod_Alm == codigoAlmacen.Value).ToList();
        //        almacenes = almacenes.Where(m => m.Cod_Alm == codigoAlmacen.Value).ToList();
        //    }
        //    else if (articulo.cod_alm.HasValue)
        //    {
        //        almacenesExistencias = almacenesExistencias.Where(m => m.Cod_Alm == articulo.cod_alm.Value).ToList();
        //    }

        //    if (!almacenesExistencias.Any(m => m.ext >= movimientosAbsolutos))
        //    {
        //        if (almacenesExistencias.Count == 0)
        //        {

        //            if (almacenes.Count == 0)
        //                throw new SOTException(Recursos.Inventarios.no_almacenes_salida_excepcion);

        //            codigoAlmacen = almacenes.First().Cod_Alm;

        //            //throw new SOTException("No existe un almacén con {0} {1}", Cod_Art.ToString(), articulo.Desc_Art);
        //        }
        //        else
        //            codigoAlmacen = almacenesExistencias.OrderBy(m => m.ext).First().Cod_Alm;

        //    }
        //    else
        //        codigoAlmacen = almacenesExistencias.Where(m => m.ext >= movimientosAbsolutos).OrderBy(m => m.ext).First().Cod_Alm;



        //    if (!codigoAlmacen.HasValue || codigoAlmacen == 0)
        //        throw new SOTException("El almacén en la orden de compra no puede estar vacío");

        //    if (!almacenesExistencias.Any(m => m.ext >= movimientosAbsolutos))
        //    {
        //        bool convertido = false;

        //        if (ServicioArticulos.VerificarPoseeConversionesEntrara(codigoArticulo))
        //        {
        //            try
        //            {
        //                ServicioArticulos.Convertir(codigoArticulo, movimientosAbsolutos, codigoAlmacen.Value, articulo.Cos_Art ?? 0, !esEntrada, fechaMovimiento);

        //                convertido = true;
        //            }
        //            catch { }
        //        }

        //        if (!convertido && ServicioMixItems.VerificarTieneReceta(codigoArticulo))
        //        {
        //            if (preparar)
        //                ServicioMixItems.Preparar(codigoArticulo, movimientosAbsolutos, codigoAlmacen.Value, articulo.Cos_Art ?? 0, !esEntrada, fechaMovimiento);
        //            else
        //                throw new SOTException("No se ha preparado {0}", articulo.Desc_Art);
        //        }
        //    }

        //    //Originalmente no te dejaba generar el movimiento si no hay forma de prepara este elemento y si no hay existencias, se pidió omitir esa regla
        //    RepositorioZctEncMovInv.SP_ZctEncMovInv_EDICION(codigoMovimiento, tipoMovimiento, costoMovimiento, folioMovimiento, codigoArticulo, candidadMover, claveFolio, fechaMovimiento, codigoAlmacen.Value);
        //}

        public void AgregarMovimientoComanda(string claveFolio, int codigoMovimiento, bool esEntrada, decimal costoMovimiento, string folioMovimiento, string codigoArticulo, decimal candidadMover, DateTime fechaMovimiento, bool preparar, int? codigoAlmacen = null, bool validarEntradasSalidas = false)
        {
            string tipoMovimiento = esEntrada ? "EN" : "SA";

            var articulo = ServicioArticulos.ObtenerArticuloPorId(codigoArticulo);

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.codigo_articulo_invalido_excepcion, codigoArticulo);

            if (!codigoAlmacen.HasValue)
                codigoAlmacen = articulo.cod_alm;

            if (!codigoAlmacen.HasValue || codigoAlmacen == 0)
                throw new SOTException("Debe proporcionarse un almacén para realizar un movimiento de {0} del artículo {1}", esEntrada ? "entrada" : "salida", articulo.Desc_Art);

            if (validarEntradasSalidas)
            {
                if (esEntrada)
                {
                    //if(!ServicioAlmacenes.ObtenerAlmacenesAceptanEntradas().Any(m=> m.Cod_Alm == codigoAlmacen.Value))
                    //    throw new SOTException("No existe un ")
                }
                else
                {
                    if (!ServicioAlmacenes.ObtenerAlmacenesAceptanSalidas().Any(m => m.Cod_Alm == codigoAlmacen.Value))
                        throw new SOTException("El almacén con código {0} no acepta salidas, no se pueden realizar salidas de {1}", codigoAlmacen.Value.ToString(), articulo.Desc_Art);
                }
            }

            var articuloXalmacen = ServicioArticulos.ObtenerArticuloAlmacen(codigoArticulo, codigoAlmacen.Value);




            decimal movimientosAbsolutos = Math.Abs(candidadMover);

            var almacenesExistencias = esEntrada ? new { Cod_Alm = codigoAlmacen.Value, ext = movimientosAbsolutos } :

                new
                {
                    Cod_Alm = codigoAlmacen.Value,
                    ext = articuloXalmacen?.Exist_Art ?? 0
                };

            ////Si envían un código de almacén, se filtran las listas
            //if (codigoAlmacen.HasValue)
            //{
            //    almacenesExistencias = almacenesExistencias.Where(m => m.Cod_Alm == codigoAlmacen.Value).ToList();
            //    almacenes = almacenes.Where(m => m.Cod_Alm == codigoAlmacen.Value).ToList();
            //}
            //else if (articulo.cod_alm.HasValue)
            //{
            //    almacenesExistencias = almacenesExistencias.Where(m => m.Cod_Alm == articulo.cod_alm.Value).ToList();
            //}

            //if (!almacenesExistencias.Any(m => m.ext >= movimientosAbsolutos))
            //{
            //    if (almacenesExistencias.Count == 0)
            //    {

            //        if (almacenes.Count == 0)
            //            throw new SOTException(Recursos.Inventarios.no_almacenes_salida_excepcion);

            //        codigoAlmacen = almacenes.First().Cod_Alm;

            //        //throw new SOTException("No existe un almacén con {0} {1}", Cod_Art.ToString(), articulo.Desc_Art);
            //    }
            //    else
            //        codigoAlmacen = almacenesExistencias.OrderBy(m => m.ext).First().Cod_Alm;

            //}
            //else
            //    codigoAlmacen = almacenesExistencias.Where(m => m.ext >= movimientosAbsolutos).OrderBy(m => m.ext).First().Cod_Alm;





            if (almacenesExistencias.ext < movimientosAbsolutos)
            {
                bool convertido = false;

                if (ServicioArticulos.VerificarPoseeConversionesEntrara(codigoArticulo))
                {
                    try
                    {
                        ServicioArticulos.Convertir(codigoArticulo, movimientosAbsolutos, codigoAlmacen.Value, articulo.Cos_Art ?? 0, !esEntrada, fechaMovimiento);

                        convertido = true;
                    }
                    catch//(Exception e)
                    {
                        //Transversal.Log.Logger.Fatal(e.Message ?? "");
                        //if (e.InnerException != null)
                        //    Transversal.Log.Logger.Fatal(e.InnerException.Message ?? "");
                    }
                }

                if (!convertido && ServicioMixItems.VerificarTieneReceta(codigoArticulo))
                {
                    if (preparar)
                        ServicioMixItems.Preparar(codigoArticulo, movimientosAbsolutos, codigoAlmacen.Value, articulo.Cos_Art ?? 0, !esEntrada, fechaMovimiento);
                    else
                        throw new SOTException("No se ha preparado {0}", articulo.Desc_Art);
                }
            }

            //Originalmente no te dejaba generar el movimiento si no hay forma de prepara este elemento y si no hay existencias, se pidió omitir esa regla
            RepositorioZctEncMovInv.SP_ZctEncMovInv_EDICION(codigoMovimiento, tipoMovimiento, costoMovimiento, folioMovimiento, codigoArticulo, candidadMover, claveFolio, fechaMovimiento, codigoAlmacen.Value);
        }

        public decimal AgregarMovimientoComanda(string claveFolio, int codigoMovimiento, bool esEntrada, string folioMovimiento, string codigoArticulo, decimal candidadMover, DateTime fechaMovimiento, bool preparar, int? codigoAlmacen = null)
        {
#warning mejorar
            var articulo = ServicioArticulos.ObtenerArticuloPorId(codigoArticulo);

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.codigo_articulo_invalido_excepcion, codigoArticulo);

            //Se modificó el sistema para que todos los movimientos se ejecuten en el almacén del artículo a despachar en caso de que no se proporcione un almacén
            AgregarMovimientoComanda(claveFolio, codigoMovimiento, esEntrada, articulo.Cos_Art ?? 0, folioMovimiento, codigoArticulo, candidadMover, fechaMovimiento, preparar, codigoAlmacen ?? articulo.cod_alm, true);

            return articulo.Cos_Art ?? 0;
        }
    }
}
