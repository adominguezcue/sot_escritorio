﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio.Almacen.Articulos
{
    internal interface IServicioArticulosInterno : IServicioArticulos
    {
        bool VerificarPoseeConversionesEntrara(string codigoArticulo);
        void Convertir(string codigoArticulo, decimal cantidad, int idAlmacen, decimal costo, bool esEntrada, DateTime fecha);
        void ValidarNoExisteReferenciaCircular(string codigoArticuloSalida, string codigoArticuloFiltro);

        Articulo ObtenerConCostoAcumulado(string codigoArticulo);

        ArticuloAlmacen ObtenerArticuloAlmacen(string codigoArticulo, int codigoAlmacen);
    }
}
