﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Dtos;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;

namespace Negocio.Almacen.Articulos
{
    public interface IServicioArticulos
    {
        
        /// <summary>
        /// Retorna los artículos cuyo tipo tiene el código especificado, ignorando espacios en
        /// blanco al principio y al final del código
        /// </summary>
        /// <param name="nombreTipo"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Articulo> ObtenerArticulosPorTipo(string idTipo, DtoUsuario usuario);
        /// <summary>
        /// Retorna los artículos cuyo departament tiene el id especificado
        /// </summary>
        /// <param name="idDepartamento"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Articulo> ObtenerArticulosPorDepartamento(int idDepartamento, DtoUsuario usuario);
        /// <summary>
        /// Valida que el precio porporcionado coincida con el almacenado para el artículo solicitado
        /// </summary>
        /// <param name="idArticulo"></param>
        /// <param name="precio"></param>
        void ValidarIntegridadPrecio(string idArticulo, decimal precio);
        /// <summary>
        /// Valida que exista la cantidad del artículo solicitado
        /// </summary>
        /// <param name="idArticulo"></param>
        /// <param name="cantidad"></param>
        void ValidarDisponibilidad(string idArticulo, int cantidad);
        /// <summary>
        /// Retorna el artículo que coincide con el id proporcionado, sin tomar en cuenta el estado
        /// del elemento
        /// </summary>
        /// <param name="idArticulo"></param>
        /// <returns></returns>
        Articulo ObtenerArticuloPorId(string idArticulo);
        /// <summary>
        /// Retorna los artículos que coinciden con los ids proporcionados
        /// </summary>
        /// <param name="idsArticulos"></param>
        /// <returns></returns>
        List<Articulo> ObtenerArticulosConLineaPorId(List<string> idsArticulos, string departamentoFiltro = null, bool soloActivas = false);
        /// <summary>
        /// Retorna los artículos que coinciden con los ids proporcionados cargados con su línea y presentación
        /// </summary>
        /// <param name="idsArticulos"></param>
        /// <returns></returns>
        List<Articulo> ObtenerArticulosConLineaYPresentacionPorId(List<string> idsArticulos);

        List<DtoIdArticuloIdLinea> ObtenerIdsArticulosPorLineas(List<int> idsLineas);

        List<string> ObtenerArticulosConLineaPorDepartamento(string departamento, bool soloActivas = false);
        /// <summary>
        /// Retorna la línea base de la línea a la que pertenece el artículo, o null si el artículo no existe o la línea no tiene linea base
        /// </summary>
        /// <param name="codigoArticulo"></param>
        /// <returns></returns>
        Linea.LineasBase? ObtenerLineaBaseIgnorarActivo(string codigoArticulo);
        List<Linea.LineasBase> ObtenerLineaBaseIgnorarActivo(List<string> codigoArticulo);

        List<ZctImagenesArticulos> TraeImagenesArticulo(string cod_art);
        Articulo TraerOGenerarArticuloPorCodigoOUpc(string Cod_art, bool cargarTemporales = false);
        void AgregaArticulo(Articulo articulo, List<DtoArticuloPresentacion> presentaciones, DtoUsuario usuario);
        void ModificarArticulo(Articulo articulo, List<DtoArticuloPresentacion> presentaciones, DtoUsuario usuario);
        void EliminarFoto(string idArticulo, int Cod_imagen);
        int MaxImg(string idArticulo);
        void ModificaFoto(string Cod_art, int cod_imagen, string RutaImagen);

        void EliminaArticulo(string Cod_art, DtoUsuario usuario);

        List<DtoExistencias> SP_ConsultarExistencias(string codigoArticulo);

        bool VerificarEsActivoFijo(string codigoArticulo);

        List<Articulo> ObtenerActivosFijos(DtoUsuario usuario);
        /// <summary>
        /// Verifica si existen artículos relacionados a esta presentación
        /// </summary>
        /// <param name="idPresentacion"></param>
        /// <returns></returns>
        bool VerificarRelacionesConPresentacion(int idPresentacion);
        DtoResultadoBusquedaArticulo SP_ZctCatArt_Busqueda(string codigo, bool soloInventariable);

        Articulo ObtenerArticuloNoNull(string Cod_ArtOrUpc);

        List<ArticuloAlmacen> ObtenerArticulosAlmacenesAceptanSalida(string codigoArticulo);

        List<DtoResultadoBusquedaArticuloSimple> ObtenerResumenesArticulosPorFiltro(string filtro, DtoUsuario usuario, bool incluirArticulosPresentacion = true, bool soloInventariables = false, bool? EsInsumo=false, bool? _EsgestionArticulos=false);

        bool VerificarExistenciasPorLinea(int idLinea);

        decimal ObtenerCosto(string codigoArticulo, DtoUsuario usuario);

        bool VerificarEsPresentacion(string codigoArticulo);
        void ValidarExistenArticulosInventariables(int? codigoDepartamento, int? codigoLinea, DtoUsuario usuario);
        List<string> ObtenerClavesPresentaciones(string codigoArticulo);
    }
}
