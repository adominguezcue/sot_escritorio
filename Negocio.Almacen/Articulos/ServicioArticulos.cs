﻿using Modelo;
using Modelo.Almacen.Repositorios;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using Modelo.Almacen.Entidades.Dtos;
using System.Transactions;
using Negocio.Almacen.Inventarios;
using Negocio.Almacen.Parametros;
using Transversal.Excepciones.Identidad;
using Transversal.Extensiones;
using Negocio.Almacen.Folios;
using Modelo.Almacen.Constantes;
using Negocio.Almacen.CalendariosFiscales;
using Negocio.Almacen.Recetas;
using Transversal.Excepciones.Seguridad;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core;
using Negocio.Compartido.Articulos;
using System.Linq.Expressions;

namespace Negocio.Almacen.Articulos
{
    public class ServicioArticulos : IServicioArticulosInterno
    {
        IServicioFoliosInterno ServicioFolios
        {
            get { return _servicioFolios.Value; }
        }

        Lazy<IServicioFoliosInterno> _servicioFolios = new Lazy<IServicioFoliosInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioFoliosInterno>());

        IServicioMixItemsInterno ServicioMixItems
        {
            get { return _servicioMixItems.Value; }
        }

        Lazy<IServicioMixItemsInterno> _servicioMixItems = new Lazy<IServicioMixItemsInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioMixItemsInterno>());

        IServicioDependenciasArticulosCompartido ServicioDependenciasArticulosCompartido
        {
            get { return _servicioDependenciasArticulosCompartido.Value; }
        }

        Lazy<IServicioDependenciasArticulosCompartido> _servicioDependenciasArticulosCompartido = new Lazy<IServicioDependenciasArticulosCompartido>(() => FabricaDependencias.Instancia.Resolver<IServicioDependenciasArticulosCompartido>());


        IServicioCalendariosFiscales ServicioCalendariosFiscales
        {
            get { return _servicioCalendariosFiscales.Value; }
        }

        Lazy<IServicioCalendariosFiscales> _servicioCalendariosFiscales = new Lazy<IServicioCalendariosFiscales>(() => FabricaDependencias.Instancia.Resolver<IServicioCalendariosFiscales>());

        IRepositorioArticulos RepositorioArticulos
        {
            get { return _repositorioArticulos.Value; }
        }

        Lazy<IRepositorioArticulos> _repositorioArticulos = new Lazy<IRepositorioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioArticulos>(); });

        IRepositorioConversionesArticulos RepositorioConversionesArticulos
        {
            get { return _repositorioConversionesArticulos.Value; }
        }

        Lazy<IRepositorioConversionesArticulos> _repositorioConversionesArticulos = new Lazy<IRepositorioConversionesArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioConversionesArticulos>(); });

        IRepositorioArticulosAlmacen RepositorioArticulosAlmacen
        {
            get { return _repositorioArticulosAlmacen.Value; }
        }

        Lazy<IRepositorioArticulosAlmacen> _repositorioArticulosAlmacen = new Lazy<IRepositorioArticulosAlmacen>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioArticulosAlmacen>(); });

        IRepositorioEncabezadosOrdenesConversion RepositorioEncabezadosOrdenesConversion
        {
            get { return _repositorioEncabezadosOrdenesConversion.Value; }
        }

        Lazy<IRepositorioEncabezadosOrdenesConversion> _repositorioEncabezadosOrdenesConversion = new Lazy<IRepositorioEncabezadosOrdenesConversion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioEncabezadosOrdenesConversion>(); });

        IRepositorioDetallesOrdenesConversion RepositorioDetallesOrdenesConversion
        {
            get { return _repositorioDetallesOrdenesConversion.Value; }
        }

        Lazy<IRepositorioDetallesOrdenesConversion> _repositorioDetallesOrdenesConversion = new Lazy<IRepositorioDetallesOrdenesConversion>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioDetallesOrdenesConversion>(); });

        IRepositorioZctImagenesArticulos RepositorioZctImagenesArticulos
        {
            get { return _repositorioZctImagenesArticulos.Value; }
        }

        Lazy<IRepositorioZctImagenesArticulos> _repositorioZctImagenesArticulos = new Lazy<IRepositorioZctImagenesArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioZctImagenesArticulos>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => { return FabricaDependencias.Instancia.Resolver<IServicioParametros>(); });


        IServicioInventarios ServicioInventarios
        {
            get { return _servicioInventarios.Value; }
        }

        Lazy<IServicioInventarios> _servicioInventarios = new Lazy<IServicioInventarios>(() => { return FabricaDependencias.Instancia.Resolver<IServicioInventarios>(); });

        void algo(bool esNuevo, int @Cod_EncOT, int @Cod_DetOT, string @Cod_Art, int @Ctd_Art, decimal @CosArt_DetOT, decimal @PreArt_DetOT,
                  int @CtdStd_DetOT, DateTime @Fch_DetOT, DateTime @FchAplMov, int @Cat_Alm, int @cod_usu, int @Cod_Mot = 0, int @tiempo_vida = 0)
        {

            int @CtdStdOld = 0;
            int @CatAlmOLD = 0;
            decimal @CosArtOLD = 0;

            int @anio;
            int @mes;
            bool @Surtir_TpArt;

            var articulo = RepositorioArticulos.Obtener(m => m.Cod_Art == @Cod_Art, m => m.ZctCatTpArt);
            @Surtir_TpArt = articulo.ZctCatTpArt.Surtir_TpArt ?? false;

            @CosArt_DetOT = RepositorioArticulosAlmacen.ObtenerElementos(m => m.Cod_Art == @Cod_Art && m.Cod_Alm == @Cat_Alm && m.Exist_Art > 0).Select(m => (m.CostoProm_Art ?? 0) / (m.Exist_Art ?? 1)).FirstOrDefault();


            /*exec dbo.sp_get_anio_mes @FchAplMov, @anio output, @mes output*/



            if (esNuevo)//not exists(SELECT Cod_EncOT , Cod_DetOT, Cod_Art, Ctd_Art, CosArt_DetOT, PreArt_DetOT, CtdStd_DetOT, Fch_DetOT FROM [ZctDetOT] WHERE Cod_DetOT = @Cod_DetOT  ) 
            {
                //INSERT INTO [ZctDetOT] (Cod_EncOT, Cod_Art, Ctd_Art, CosArt_DetOT, PreArt_DetOT, CtdStd_DetOT, Fch_DetOT , Cat_Alm, Cod_Mot, uuid, anio_fiscal, mes_fiscal,tiempo_vida ,  cod_usu)
                //VALUES(@Cod_EncOT, @Cod_Art, @Ctd_Art, @CosArt_DetOT, @PreArt_DetOT, @CtdStd_DetOT, GetDate() , @Cat_Alm, @Cod_Mot, NEWID(), @anio, @mes,@tiempo_vida,  @cod_usu )
                if (@Surtir_TpArt)//(@Cod_TpArt  = 'ART')
                {
                    @CosArt_DetOT = (@CosArt_DetOT * (-1)) * @CtdStd_DetOT;
                    @CtdStd_DetOT = (@CtdStd_DetOT * (-1));
                    /*EXECUTE SP_ZctEncMovInv 2,0, 'SA', @CosArt_DetOT  , @Cod_EncOT,@Cod_Art  , @CtdStd_DetOT, 'OT', @FchAplMov , @Cat_Alm*/
                }

            }
            else
            {
                //SELECT @CosArtOLD = CosArt_DetOT,  @CtdStdOld =  CtdStd_DetOT , @CatAlmOLD = Cat_Alm FROM [ZctDetOT] WHERE Cod_DetOT = @Cod_DetOT  
                //UPDATE [ZctDetOT] SET update_web = 0, Cod_Art = @Cod_Art, Ctd_Art = @Ctd_Art, CosArt_DetOT = @CosArt_DetOT , PreArt_DetOT = @PreArt_DetOT, CtdStd_DetOT = @CtdStd_DetOT , Cat_Alm = @Cat_Alm, Cod_Mot = @Cod_Mot, anio_fiscal = @anio, mes_fiscal = @mes,tiempo_vida=@tiempo_vida  WHERE Cod_DetOT = @Cod_DetOT 

                if (@CatAlmOLD != @Cat_Alm && @Surtir_TpArt)//(@Cod_TpArt  = 'ART')
                {

                    @CosArtOLD = @CosArtOLD * @CtdStdOld;
                    /*EXECUTE SP_ZctEncMovInv 2,0, 'EC', @CosArtOLD   , @Cod_EncOT, @Cod_Art, @CtdStdOld, 'OT', @FchAplMov, @CatAlmOLD*/

                    @CosArt_DetOT = (@CosArt_DetOT * (-1)) * @CtdStd_DetOT;
                    @CtdStd_DetOT = (@CtdStd_DetOT * (-1));
                    /*EXECUTE SP_ZctEncMovInv 2,0, 'SA', @CosArt_DetOT  , @Cod_EncOT, @Cod_Art, @CtdStd_DetOT, 'OT', @FchAplMov, @Cat_Alm*/


                }
                else
                    if (@CtdStdOld > @CtdStd_DetOT && @Surtir_TpArt)//(@Cod_TpArt  = 'ART')
                    {
                        //DEVOLUCION
                        @CtdStd_DetOT = @CtdStdOld - @CtdStd_DetOT;
                        @CosArt_DetOT = @CtdStd_DetOT * @CosArt_DetOT;
                        /*EXECUTE SP_ZctEncMovInv 2,0, 'EN', @CosArt_DetOT  , @Cod_EncOT, @Cod_Art, @CtdStd_DetOT, 'OT', @FchAplMov, @Cat_Alm*/
                    }
                    else
                        if (@CtdStdOld < @CtdStd_DetOT && @Surtir_TpArt)//(@Cod_TpArt  = 'ART')
                        {
                            @CtdStd_DetOT = @CtdStdOld - @CtdStd_DetOT;
                            @CosArt_DetOT = @CosArt_DetOT * @CtdStd_DetOT;

                            /*EXECUTE SP_ZctEncMovInv 2,0, 'SA', @CosArt_DetOT  , @Cod_EncOT,@Cod_Art  , @CtdStd_DetOT , 'OT',@FchAplMov, @Cat_Alm*/
                        }


            }
            /*EXECUTE SP_ACTUALIZA_SURTIDO_REQUISICION @Cod_EncOT*/



        }

        public List<Articulo> ObtenerArticulosPorTipo(string idTipo, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarArticulos = true });

            return RepositorioArticulos.ObtenerArticulosPorTipo(idTipo);
        }

        public List<Articulo> ObtenerArticulosPorDepartamento(int idDepartamento, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarArticulos = true });

            return RepositorioArticulos.ObtenerArticulosPorDepartamento(idDepartamento);
        }

        public void ValidarIntegridadPrecio(string idArticulo, decimal precio)
        {
            var articulo = RepositorioArticulos.Obtener(m => !m.desactivar && m.Cod_Art == idArticulo);

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.id_invalido_excepcion, idArticulo.ToString());

            if (articulo.Prec_Art != precio)
                throw new SOTException(Recursos.Articulos.precio_diferente_excepcion, articulo.Desc_Art, articulo.Prec_Art.ToString("C"), precio.ToString("C"));
        }

        public void ValidarDisponibilidad(string idArticulo, int cantidad)
        {
            var articulo = RepositorioArticulos.Obtener(m => !m.desactivar && m.Cod_Art == idArticulo);

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.id_invalido_excepcion, idArticulo.ToString());

#warning implementar el manejo de inventarios para poder validar la disponibilidad de los productos
        }

        public Articulo ObtenerArticuloPorId(string idArticulo)
        {
            return RepositorioArticulos.Obtener(m => m.Cod_Art == idArticulo);
        }


        public List<Articulo> ObtenerArticulosConLineaPorId(List<string> idsArticulos, string departamentoFiltro = null, bool soloActivas = false)
        {
            Expression<Func<Articulo, bool>> filtro = m => idsArticulos.Contains(m.Cod_Art);

            if (!string.IsNullOrWhiteSpace(departamentoFiltro))
                filtro = filtro.Compose(m => m.ZctCatLinea.ZctCatDpto.Desc_Dpto == departamentoFiltro, Expression.AndAlso);

            if (soloActivas)
                filtro = filtro.Compose(m => !m.desactivar, Expression.AndAlso);

            return RepositorioArticulos.ObtenerElementos(filtro, m => m.ZctCatLinea).ToList();
        }

        public List<Articulo> ObtenerArticulosConLineaYPresentacionPorId(List<string> idsArticulos)
        {
            return RepositorioArticulos.ObtenerArticulosConLineaYPresentacionPorId(idsArticulos);
        }

        public List<DtoIdArticuloIdLinea> ObtenerIdsArticulosPorLineas(List<int> idsLineas)
        {
            return RepositorioArticulos.ObtenerElementos(m => m.Cod_Linea.HasValue && idsLineas.Contains(m.Cod_Linea.Value))
                   .Select(m => new { m.Cod_Art, m.Cod_Linea.Value }).ToList().Select(m => new DtoIdArticuloIdLinea { IdArticulo = m.Cod_Art, IdLinea = m.Value }).ToList();
        }


        public List<string> ObtenerArticulosConLineaPorDepartamento(string departamento, bool soloActivas = false)
        {
            if (soloActivas)
                return RepositorioArticulos.ObtenerElementos(m => !m.desactivar && m.ZctCatLinea.ZctCatDpto.Desc_Dpto == departamento)
                       .Select(m => m.Cod_Art).ToList();

            return RepositorioArticulos.ObtenerElementos(m => m.ZctCatLinea.ZctCatDpto.Desc_Dpto == departamento)
                   .Select(m => m.Cod_Art).ToList();
        }


        public Linea.LineasBase? ObtenerLineaBaseIgnorarActivo(string codigoArticulo)
        {
            var art = RepositorioArticulos.Obtener(m => m.Cod_Art == codigoArticulo, m => m.ZctCatLinea);

            if (art == null || !art.ZctCatLinea.IdLineaBase.HasValue)
                return null;

            return art.ZctCatLinea.LineaBase;
        }

        public List<Linea.LineasBase> ObtenerLineaBaseIgnorarActivo(List<string> codigoArticulo)
        {
            return RepositorioArticulos.ObtenerElementos(m => codigoArticulo.Contains(m.Cod_Art), m => m.ZctCatLinea).Where(m => m.ZctCatLinea.IdLineaBase.HasValue)
                .Select(m => m.ZctCatLinea.IdLineaBase.Value).Distinct().ToList().Select(m => (Linea.LineasBase)m).ToList();
        }

        public List<ZctImagenesArticulos> TraeImagenesArticulo(string cod_art)
        {
            return RepositorioZctImagenesArticulos.ObtenerElementos(I => I.cod_art == cod_art).ToList();

            //if (i.Count == 0)
            //{
            //    _nuevo = true;
            //    return new List<ZctImagenesArticulos>();
            //}
            //_nuevo = false;
            //return i;
        }
        public Articulo TraerOGenerarArticuloPorCodigoOUpc(string Cod_art, bool cargarTemporales = false)
        {
            var articulo = RepositorioArticulos.TraerOGenerarArticuloPorCodigoOUpc(Cod_art, cargarTemporales);
            var iva = ServicioParametros.ObtenerParametros().Iva_CatPar;

            if (articulo == null)
            {
                articulo = new Articulo();
                articulo.uuid = Guid.NewGuid();
                articulo.PresentacionesArticulos = new List<DtoArticuloPresentacion>();
                //_nuevo = true;
            }
            else if (articulo.CodigoPadre == null)
            {
                articulo.PresentacionesArticulos = new List<DtoArticuloPresentacion>();

                var articulosPresentacion = RepositorioArticulos.ObtenerElementos(m => m.CodigoPadre == articulo.Cod_Art /*&& !m.desactivar*/, m => m.ZctCatPresentacion).ToList();
                var conversionesExistentes = RepositorioConversionesArticulos.ObtenerElementos(m => m.CodigoArticuloEntrada == articulo.Cod_Art || m.CodigoArticuloSalida == articulo.Cod_Art).ToList();

                Articulo articuloPre = null;

                foreach (var conversionE in conversionesExistentes)
                {
                    var presentacionN = new DtoArticuloPresentacion { Activo = true, Id = conversionE.Id, IVA = iva };
                    articulo.PresentacionesArticulos.Add(presentacionN);

                    presentacionN.EsEntrada = conversionE.CodigoArticuloSalida == articulo.Cod_Art;

                    if (presentacionN.EsEntrada)
                    {
                        articuloPre = articulosPresentacion.FirstOrDefault(m => m.Cod_Art == conversionE.CodigoArticuloEntrada);

                        presentacionN.CantidadSalida = conversionE.CantidadEntrada;
                        presentacionN.CantidadEntrada = conversionE.CantidadSalida;
                    }
                    else
                    {
                        articuloPre = articulosPresentacion.FirstOrDefault(m => m.Cod_Art == conversionE.CodigoArticuloSalida);

                        presentacionN.CantidadSalida = conversionE.CantidadSalida;
                        presentacionN.CantidadEntrada = conversionE.CantidadEntrada;
                    }

                    presentacionN.PrecioConIVA = articuloPre.Prec_Art * (1 + iva);
                    presentacionN.CostoFinal = (articuloPre.Cos_Art ?? 0) * (1 + iva);
                    presentacionN.OmitirIVA = articuloPre.OmitirIVA;
                    presentacionN.NegarEntrada = articuloPre.NegarEntrada ?? false;
                    presentacionN.NegarSalida = articuloPre.NegarSalida ?? false;
                    presentacionN.DescripcionSalida = articuloPre.Desc_Art;
                    presentacionN.IdUnidad = articuloPre.id_presentacion;
                    presentacionN.CodigoEntrada = articulo.Cod_Art;
                    presentacionN.CodigoSalida = articuloPre.Cod_Art;
                    presentacionN.UnidadSalida = articuloPre.ZctCatPresentacion.presentacion;
                }
            }
            //else {
            //    _nuevo = false;
            //}
            //if (articulo.Count == 0)
            // {
            //     Modelo.ZctCatArt articuloN = new Modelo.ZctCatArt();
            //     _nuevo = true;
            //     return articuloN;
            // }


            return articulo;
        }
        public void AgregaArticulo(Articulo articulo, List<DtoArticuloPresentacion> presentaciones, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { CrearArticulos = true });

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.articulo_nulo_excepcion);

            ValidarDatos(articulo, false);

            if (articulo.desactivar)
                throw new SOTException(Recursos.Articulos.articulo_nuevo_desactivado_excepcion);

            if (presentaciones.Count(m => m.Activo) != presentaciones.Where(m => m.Activo).GroupBy(m => new { m.CantidadEntrada, m.CantidadSalida, m.IdUnidad, m.EsEntrada }).Count())
                throw new SOTException(Recursos.ConversionesArticulos.conversiones_repetidas_excepcion);

            if (presentaciones.Any(m => m.Activo && (m.CantidadEntrada <= 0 || m.CantidadSalida <= 0)))
                throw new SOTException(Recursos.ConversionesArticulos.conversiones_con_cantidades_invalidas_excepcion);

            int e = 1, s = 1;

            List<ConversionArticulos> conversiones = new List<ConversionArticulos>();
            List<Articulo> nuevos = new List<Articulo>();

            nuevos.Add(articulo);

            foreach (var presentacion in presentaciones.Where(m => m.Activo))
            {
                var nuevo = articulo.ObtenerCopiaDePrimitivas();

                nuevo.Prec_Art = presentacion.Precio;
                nuevo.Cos_Art = presentacion.Costo;
                nuevo.OmitirIVA = presentacion.OmitirIVA;
                nuevo.NegarEntrada = presentacion.NegarEntrada;
                nuevo.NegarSalida = presentacion.NegarSalida;
                nuevo.Desc_Art = presentacion.DescripcionSalida;
                nuevo.id_presentacion = presentacion.IdUnidad;
                nuevo.CodigoPadre = articulo.Cod_Art;
                nuevo.uuid = Guid.NewGuid();

                var conversion = new ConversionArticulos();

                if (presentacion.EsEntrada)
                {
                    while (true)
                    {
                        try
                        {
                            nuevo.Cod_Art = articulo.Cod_Art + "_E" + e++;
                            ValidarDatos(nuevo, true);
                            break;
                        }
                        catch (SOTKeyException)
                        {
                        }
                    }

                    conversion.CodigoArticuloEntrada = nuevo.Cod_Art;
                    conversion.CantidadEntrada = presentacion.CantidadSalida;
                    conversion.CodigoArticuloSalida = articulo.Cod_Art;
                    conversion.CantidadSalida = presentacion.CantidadEntrada;
                }
                else
                {
                    while (true)
                    {
                        try
                        {
                            nuevo.Cod_Art = articulo.Cod_Art + "_S" + s++;
                            ValidarDatos(nuevo, true);
                            break;
                        }
                        catch (SOTKeyException)
                        {
                        }
                    }

                    conversion.CodigoArticuloEntrada = articulo.Cod_Art;
                    conversion.CantidadEntrada = presentacion.CantidadEntrada;
                    conversion.CodigoArticuloSalida = nuevo.Cod_Art;
                    conversion.CantidadSalida = presentacion.CantidadSalida;
                }

                nuevos.Add(nuevo);
                conversiones.Add(conversion);
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                foreach (var art in nuevos)
                    RepositorioArticulos.Agregar(art);

                RepositorioArticulos.GuardarCambios();

                foreach (var conversion in conversiones)
                    RepositorioConversionesArticulos.Agregar(conversion);

                RepositorioConversionesArticulos.GuardarCambios();

                //lo puse aqui para que la validación contemple a las conversiones que se quieren guardar
                foreach (var conversion in conversiones)
                {
                    ValidarNoExisteReferenciaCircularPrivate(conversion.CodigoArticuloEntrada, conversion.CodigoArticuloSalida);
                }

                scope.Complete();
            }
        }

        public void ModificarArticulo(Articulo articulo, List<DtoArticuloPresentacion> presentaciones, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ModificarArticulos = true });

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.articulo_nulo_excepcion);

            var articuloOriginal = RepositorioArticulos.Obtener(m => m.Cod_Art == articulo.Cod_Art);

            if (articuloOriginal == null)
                throw new SOTException(Recursos.Articulos.articulo_nulo_excepcion);

            if (!string.IsNullOrEmpty(articuloOriginal.CodigoPadre))
                throw new SOTException(Recursos.Articulos.articulo_forma_parte_conversiones_excepcion);

            if ((articulo.Cod_Dpto != articuloOriginal.Cod_Dpto || articulo.Cod_Linea != articuloOriginal.Cod_Linea) && ServicioInventarios.VerificarExisteMovimiento(articuloOriginal.Cod_Art))
                throw new SOTException("El artículo ya posee movimientos de inventario, no se pueden modificar la categoría ni la subcategoría");

            var uuidOriginal = articuloOriginal.uuid;

            articuloOriginal.SincronizarPrimitivas(articulo);
            articuloOriginal.uuid = uuidOriginal;
            articuloOriginal.update_web = false;

            ValidarDatos(articuloOriginal, false);

            var articulosPresentacion = RepositorioArticulos.ObtenerElementos(m => m.CodigoPadre == articuloOriginal.Cod_Art/* && !m.desactivar*/).ToList();
            var conversionesExistentes = RepositorioConversionesArticulos.ObtenerElementos(m => m.CodigoArticuloEntrada == articuloOriginal.Cod_Art || m.CodigoArticuloSalida == articuloOriginal.Cod_Art).ToList();
            var codigos = articulosPresentacion.Select(m => m.Cod_Art).OrderBy(m => m).ToList();

            var presentacionesFinales = presentaciones.Where(m => m.Activo || m.Id != 0).ToList(); //|| !string.IsNullOrEmpty(m.CodigoSalida)).ToList();

            var codigosPre = presentacionesFinales.Where(m => !string.IsNullOrEmpty(m.CodigoSalida)).Select(m => m.CodigoSalida).OrderBy(m => m).ToList();

            if (!codigos.SequenceEqual(codigosPre))
                throw new SOTException(Recursos.ConversionesArticulos.informacion_conversiones_incompleta_exception);

            //Validamos que no existan conversiones con la misma información

            if (presentacionesFinales.Count(m => m.Activo) != presentacionesFinales.Where(m => m.Activo).GroupBy(m => new { m.CantidadEntrada, m.CantidadSalida, m.IdUnidad, m.EsEntrada }).Count())
                throw new SOTException(Recursos.ConversionesArticulos.conversiones_repetidas_excepcion);

            if (presentaciones.Any(m => m.Activo && (m.CantidadEntrada <= 0 || m.CantidadSalida <= 0)))
                throw new SOTException(Recursos.ConversionesArticulos.conversiones_con_cantidades_invalidas_excepcion);

            int e = 1, s = 1;

            List<ConversionArticulos> conversionesNuevas = new List<ConversionArticulos>();
            List<ConversionArticulos> conversionesEliminar = new List<ConversionArticulos>();
            List<Articulo> nuevos = new List<Articulo>();

            #region presentaciones nuevas

            foreach (var presentacion in presentacionesFinales.Where(m => m.Activo && m.Id == 0))
            {
                var nuevo = articuloOriginal.ObtenerCopiaDePrimitivas();

                nuevo.Prec_Art = presentacion.Precio;
                nuevo.Cos_Art = presentacion.Costo;
                nuevo.OmitirIVA = presentacion.OmitirIVA;
                nuevo.NegarEntrada = presentacion.NegarEntrada;
                nuevo.NegarSalida = presentacion.NegarSalida;
                nuevo.Desc_Art = presentacion.DescripcionSalida;
                nuevo.id_presentacion = presentacion.IdUnidad;
                nuevo.CodigoPadre = articuloOriginal.Cod_Art;
                nuevo.uuid = Guid.NewGuid();

                var conversion = new ConversionArticulos();

                if (presentacion.EsEntrada)
                {
                    while (true)
                    {
                        try
                        {
                            nuevo.Cod_Art = articuloOriginal.Cod_Art + "_E" + e++;
                            ValidarDatos(nuevo, true);
                            break;
                        }
                        catch (SOTKeyException)
                        {
                        }
                    }

                    conversion.CodigoArticuloEntrada = nuevo.Cod_Art;
                    conversion.CantidadEntrada = presentacion.CantidadSalida;
                    conversion.CodigoArticuloSalida = articuloOriginal.Cod_Art;
                    conversion.CantidadSalida = presentacion.CantidadEntrada;
                }
                else
                {
                    while (true)
                    {
                        try
                        {
                            nuevo.Cod_Art = articuloOriginal.Cod_Art + "_S" + s++;
                            ValidarDatos(nuevo, true);
                            break;
                        }
                        catch (SOTKeyException)
                        {
                        }
                    }

                    conversion.CodigoArticuloEntrada = articuloOriginal.Cod_Art;
                    conversion.CantidadEntrada = presentacion.CantidadEntrada;
                    conversion.CodigoArticuloSalida = nuevo.Cod_Art;
                    conversion.CantidadSalida = presentacion.CantidadSalida;
                }

                nuevos.Add(nuevo);
                conversionesNuevas.Add(conversion);
            }

            #endregion
            #region presentaciones previas

            foreach (var presentacion in presentacionesFinales.Where(m => m.Id != 0))
            {
                var conversion = conversionesExistentes.FirstOrDefault(m => m.Id == presentacion.Id);

                if (conversion == null)
                    throw new SOTException(Recursos.ConversionesArticulos.conversion_nula_excepcion, presentacion.Id.ToString());

                var articuloConversion = articulosPresentacion.FirstOrDefault(m => m.Cod_Art == presentacion.CodigoSalida);

                if (articuloConversion == null)
                    throw new SOTException(Recursos.ConversionesArticulos.extremos_conversion_cambiados_excepcion);

                if (presentacion.Activo)
                {
                    var departamento = articuloConversion.Cod_Dpto;
                    var linea = articuloConversion.Cod_Linea;

                    var uuidConversion = articuloConversion.uuid;
                    articuloConversion.SincronizarPrimitivas(articuloOriginal);
                    articuloConversion.uuid = uuidConversion;

                    articuloConversion.Prec_Art = presentacion.Precio;
                    articuloConversion.Cos_Art = presentacion.Costo;
                    articuloConversion.OmitirIVA = presentacion.OmitirIVA;
                    articuloConversion.NegarEntrada = presentacion.NegarEntrada;
                    articuloConversion.NegarSalida = presentacion.NegarSalida;
                    articuloConversion.Desc_Art = presentacion.DescripcionSalida;
                    articuloConversion.id_presentacion = presentacion.IdUnidad;
                    articuloConversion.CodigoPadre = articuloOriginal.Cod_Art;
                    articuloConversion.update_web = false;

                    if ((articuloConversion.Cod_Dpto != departamento || articuloConversion.Cod_Linea != linea) && ServicioInventarios.VerificarExisteMovimiento(articuloConversion.Cod_Art))
                        throw new SOTException("El artículo ya posee movimientos de inventario, no se pueden modificar la categoría ni la subcategoría");

                    ValidarDatos(articuloConversion, true);

                    if (presentacion.EsEntrada)
                    {
                        conversion.CodigoArticuloEntrada = articuloConversion.Cod_Art;
                        conversion.CantidadEntrada = presentacion.CantidadSalida;
                        conversion.CodigoArticuloSalida = articuloOriginal.Cod_Art;
                        conversion.CantidadSalida = presentacion.CantidadEntrada;
                    }
                    else
                    {
                        conversion.CodigoArticuloEntrada = articuloOriginal.Cod_Art;
                        conversion.CantidadEntrada = presentacion.CantidadEntrada;
                        conversion.CodigoArticuloSalida = articuloConversion.Cod_Art;
                        conversion.CantidadSalida = presentacion.CantidadSalida;
                    }
                }
                else
                {
                    articuloConversion.CodigoPadre = null;
                    articuloConversion.desactivar = true;
                    articuloConversion.update_web = false;

                    conversionesEliminar.Add(conversion);
                }
            }

            #endregion

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                RepositorioArticulos.Modificar(articuloOriginal);

                foreach (var articuloPresentacion in articulosPresentacion)
                {
                    if (!conversionesEliminar.Any(m=> m.CodigoArticuloEntrada == articuloPresentacion.Cod_Art || m.CodigoArticuloSalida == articuloPresentacion.Cod_Art))
                        //Vuelvo a asignar el padre aqui porque al marcar como modificado el principal, el campo CodigoPadre se vuelve null en éstos.
                        articuloPresentacion.CodigoPadre = articuloOriginal.Cod_Art;
                    RepositorioArticulos.Modificar(articuloPresentacion);
                }

                foreach (var articuloPresentacion in nuevos)
                    RepositorioArticulos.Agregar(articuloPresentacion);

                RepositorioArticulos.GuardarCambios();

                foreach (var conversion in conversionesNuevas)
                    RepositorioConversionesArticulos.Agregar(conversion);

                foreach (var conversion in conversionesExistentes)
                    RepositorioConversionesArticulos.Modificar(conversion);

                foreach (var conversion in conversionesEliminar)
                    RepositorioConversionesArticulos.Eliminar(conversion);

                RepositorioConversionesArticulos.GuardarCambios();

                var conversiones = RepositorioConversionesArticulos.ObtenerElementos(m => m.CodigoArticuloEntrada == articuloOriginal.Cod_Art || m.CodigoArticuloSalida == articuloOriginal.Cod_Art).ToList();

                //lo puse aqui para que la validación contemple a las conversiones que se quieren guardar
                foreach (var conversion in conversiones)
                {
                    ValidarNoExisteReferenciaCircularPrivate(conversion.CodigoArticuloEntrada, conversion.CodigoArticuloSalida);
                }

                scope.Complete();
            }
        }

        public void EliminarFoto(string idArticulo, int Cod_imagen)
        {
            List<ZctImagenesArticulos> imagenes = RepositorioZctImagenesArticulos.ObtenerElementos(i => i.numimg == Cod_imagen && i.cod_art == idArticulo).ToList();

            foreach (var item in imagenes)
            {
                RepositorioZctImagenesArticulos.Eliminar(item);
            }

            RepositorioZctImagenesArticulos.GuardarCambios();
        }

        public int MaxImg(string idArticulo)
        {
            return RepositorioZctImagenesArticulos.ObtenerUltimoNumero(idArticulo) + 1;

            //List <int > i= (from m in _dao.ZctImagenesArticulos where m.cod_art == idArticulo select m.numimg).ToList();

            //if (i.Count == 0)
            //{
            //    return 1;
            //}

            //return (from m in _dao.ZctImagenesArticulos where m.cod_art == idArticulo select m.numimg).Max() + 1;
        }
        public void ModificaFoto(string Cod_art, int cod_imagen, string RutaImagen)
        {
            List<ZctImagenesArticulos> imagenes = RepositorioZctImagenesArticulos.ObtenerElementos(i => i.cod_art == Cod_art && i.cod_imagen == cod_imagen).ToList();

            if (imagenes.Count == 0)
                throw new SOTException(Recursos.Articulos.codigo_imagen_articulo_invalido_excepcion, Cod_art, cod_imagen.ToString());

            foreach (var item in imagenes)
            {
                item.imagen = RutaImagen;
                RepositorioZctImagenesArticulos.Modificar(item);
            }

            RepositorioZctImagenesArticulos.GuardarCambios();
        }

#warning revisar como eliminarlo del sitio de requisiciones
        public void EliminaArticulo(string Cod_art, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarArticulos = true });

            var articulo = RepositorioArticulos.Obtener(a => a.Cod_Art == Cod_art);
            var imagenes = RepositorioZctImagenesArticulos.ObtenerElementos(i => i.cod_art == Cod_art).ToList();

            if (!string.IsNullOrEmpty(articulo.CodigoPadre))
                throw new SOTException(Recursos.Articulos.articulo_forma_parte_conversiones_excepcion);

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.codigo_articulo_invalido_excepcion, Cod_art);

            if (!ServicioDependenciasArticulosCompartido.VerificarNoExistenDependenciasArticulo(articulo.Cod_Art))
                throw new SOTException("Este artículo no puede ser eliminado porque posee vinculaciones, intente desactivarlo en lugar de eliminarlo.");

            foreach (var imagen in imagenes)
                RepositorioZctImagenesArticulos.Eliminar(imagen);

            RepositorioArticulos.Eliminar(articulo);

            try
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    RepositorioZctImagenesArticulos.GuardarCambios();
                    RepositorioArticulos.GuardarCambios();

                    scope.Complete();
                }
            }
            catch (DbUpdateException)
            {
                //var excepcion = (DbUpdateException)ex;
                //UpdateException inner = null;
                //Exception excepcion = null;

                //if (ex.InnerException != null && ex.InnerException.GetType() == typeof(UpdateException))
                //{
                //    excepcion = ex.InnerException;

                //    if (excepcion.InnerException != null)
                //        excepcion = excepcion.InnerException;
                //}
                //else
                //    excepcion = ex;

                throw new SOTException("Este artículo no puede ser eliminado porque posee vinculaciones, intente desactivarlo en lugar de eliminarlo.");
            }
            //List<Modelo.ZctImagenesArticulos > imagenes =(from Modelo.ZctImagenesArticulos i in _dao.ZctImagenesArticulos  where i.cod_art == Cod_art select i).ToList ();
            //_dao.ZctImagenesArticulos.DeleteAllOnSubmit(imagenes);
            //_dao.ZctCatArt.DeleteAllOnSubmit(articulo);
            //_dao.SubmitChanges();
        }
        //public string TraeRutaGuardadoImagenes()
        //{
        //    return (from p in _dao.ZctCatPar select p.ruta_imagenes_articulos).ToList()[0];
        //}

        public List<DtoExistencias> SP_ConsultarExistencias(string codigoArticulo)
        {
            return RepositorioArticulos.SP_ConsultarExistencias(codigoArticulo);
        }


        public bool VerificarEsActivoFijo(string codigoArticulo)
        {
            return RepositorioArticulos.Alguno(m => m.Cod_Art == codigoArticulo && m.ZctArtXAlm.Any(z => z.ZctCatAlm.salida != true && z.ZctCatAlm.entrada != true));
        }

        public List<Articulo> ObtenerActivosFijos(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarArticulos = true });

            return RepositorioArticulos.ObtenerElementos(m => m.ZctArtXAlm.Any(z => z.ZctCatAlm.salida != true && z.ZctCatAlm.entrada != true)).ToList();
        }


        public bool VerificarRelacionesConPresentacion(int idPresentacion)
        {
            return RepositorioArticulos.Alguno(m => m.id_presentacion == idPresentacion);
        }

        public DtoResultadoBusquedaArticulo SP_ZctCatArt_Busqueda(string codigo, bool soloInventariable)
        {
            return RepositorioArticulos.SP_ZctCatArt_Busqueda(codigo, soloInventariable);
        }

        public Articulo ObtenerArticuloNoNull(string Cod_ArtOrUpc)
        {
            var articulo = RepositorioArticulos.Obtener(art => art.Cod_Art == Cod_ArtOrUpc || art.upc == Cod_ArtOrUpc);

            if (articulo == null)
                throw new SOTException(Recursos.Articulos.codigo_articulo_invalido_excepcion, Cod_ArtOrUpc);

            return articulo;
        }

        public List<ArticuloAlmacen> ObtenerArticulosAlmacenesAceptanSalida(string codigoArticulo)
        {
            return RepositorioArticulosAlmacen.ObtenerElementos(m => m.Cod_Art == codigoArticulo && m.ZctCatAlm.salida == true).ToList();
        }

        public List<DtoResultadoBusquedaArticuloSimple> ObtenerResumenesArticulosPorFiltro(string filtro, DtoUsuario usuario, bool incluirArticulosPresentacion = true, bool soloInventariables = false, bool? EsInsumo=false, bool? _EsgestionArticulos =false)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarArticulos = true });

            var porcentajeIVA = ServicioParametros.ObtenerParametros().Iva_CatPar;

            var articulos = RepositorioArticulos.ObtenerResumenesArticulosPorFiltro(filtro, incluirArticulosPresentacion, soloInventariables, EsInsumo, _EsgestionArticulos);

            articulos.ForEach(m => m.PorcentajeIVA = porcentajeIVA);

            return articulos;
        }


        public bool VerificarExistenciasPorLinea(int idLinea)
        {
            return RepositorioArticulos.Alguno(m => m.Cod_Linea == idLinea);
        }

        #region métodos internal

        Articulo IServicioArticulosInterno.ObtenerConCostoAcumulado(string codigoArticulo) 
        {
            return RepositorioArticulos.ObtenerConCostoAcumulado(codigoArticulo);
        }

        bool IServicioArticulosInterno.VerificarPoseeConversionesEntrara(string codigoArticulo)
        {
            return RepositorioConversionesArticulos.Alguno(m => m.CodigoArticuloSalida == codigoArticulo);
        }

        ArticuloAlmacen IServicioArticulosInterno.ObtenerArticuloAlmacen(string codigoArticulo, int codigoAlmacen)
        {
            return RepositorioArticulosAlmacen.Obtener(m => m.Cod_Art == codigoArticulo && m.Cod_Alm == codigoAlmacen);
        }

        void IServicioArticulosInterno.Convertir(string codigoArticulo, decimal cantidad, int idAlmacen, decimal costo, bool esEntrada, DateTime fecha)
        {
            //var folio = ServicioFolios.ObtenerSiguienteFolio(ClavesFolios.IP);

            EncabezadoOrdenConversion enc = new EncabezadoOrdenConversion()
            {
                //Cod_OP = folio,
                FechaOperacion = fecha,
                EsEntrada = esEntrada, //? "OP" : "OS"
                FolioTipo = esEntrada ? ClavesFolios.CONV : ClavesFolios.DEV
            };

            var movInv = new DetalleOrdenConversion
            {
                Cantidad = cantidad,
                IdAlmacen = idAlmacen,
                CodigoArticulo = codigoArticulo,
                Costo = costo
            };

            ProducirArticulo(enc, movInv);
        }

        void IServicioArticulosInterno.ValidarNoExisteReferenciaCircular(string codigoArticuloSalida, string codigoArticuloFiltro) 
        {
            ValidarNoExisteReferenciaCircularPrivate(codigoArticuloSalida, codigoArticuloFiltro);
        }

        #endregion

        private void ValidarNoExisteReferenciaCircularPrivate(string codigoArticuloSalida, string codigoArticuloFiltro)
        {
            var conversiones = RepositorioConversionesArticulos.ObtenerElementos(m => m.CodigoArticuloSalida == codigoArticuloSalida).ToList();

            foreach (var conversion in conversiones)
            {
                if (conversion.CodigoArticuloEntrada == codigoArticuloFiltro)
                {
                    var ex = new SOTAcumulativoException(Recursos.ConversionesArticulos.referencia_circular_excepcion);

                    ex.AgregarDetalle(string.Format("Conversión: {0} -> {1}", conversion.CodigoArticuloEntrada, conversion.CodigoArticuloSalida));
                    throw ex;
                }

                try
                {
                    ValidarNoExisteReferenciaCircularPrivate(conversion.CodigoArticuloEntrada, codigoArticuloFiltro);
                }
                catch (SOTAcumulativoException ex)
                {
                    ex.AgregarDetalle(string.Format("Conversión: {0} -> {1}", conversion.CodigoArticuloEntrada, conversion.CodigoArticuloSalida));
                    throw ex;
                }
            }

            var ingredientes = ServicioMixItems.GetMixItems(codigoArticuloSalida);

            foreach (var ingrediente in ingredientes)
            {
                if (ingrediente.Cod_Art_Mix == codigoArticuloFiltro)
                {
                    var ex = new SOTAcumulativoException(Recursos.Recetas.referencia_circular_excepcion);

                    ex.AgregarDetalle(string.Format("Receta: {0} -> {1}", ingrediente.Cod_Art_Mix, ingrediente.Cod_Art));
                    throw ex;
                }

                try
                {
                    ValidarNoExisteReferenciaCircularPrivate(ingrediente.Cod_Art_Mix, codigoArticuloFiltro);
                }
                catch (SOTAcumulativoException ex)
                {
                    ex.AgregarDetalle(string.Format("Receta: {0} -> {1}", ingrediente.Cod_Art_Mix, ingrediente.Cod_Art));
                    throw ex;
                }
            }
        }

        private void ValidarDatos(Articulo articulo, bool esPresentacion)
        {
            if (RepositorioArticulos.Alguno(m => m.Cod_Art.Trim().ToUpper() == articulo.Cod_Art.Trim().ToUpper() && m.uuid != articulo.uuid))
                throw new SOTKeyException(articulo.NombrePropiedad(m => m.Cod_Art), string.Format("El código {0} ya está en uso", articulo.Cod_Art));

            string ErrorMsj = "";
            if (string.IsNullOrWhiteSpace(articulo.Cod_TpArt))
                ErrorMsj = (ErrorMsj + ("Debe de asignar un tipo de artículo." + "\r\n"));

            if (string.IsNullOrWhiteSpace(articulo.Cod_Art))
                ErrorMsj = (ErrorMsj + ("Debe de asignar un código de artículo." + "\r\n"));

            if (string.IsNullOrEmpty(articulo.Desc_Art))
                ErrorMsj = (ErrorMsj + ("Debe de asignar una descripción de artículo." + "\r\n"));

            //if (!ArticuloActual.Cos_Art.HasValue)
            //    ArticuloActual.Cos_Art = 0;
            if (!articulo.Cos_Art.HasValue || articulo.Cos_Art == 0)
                ErrorMsj = (ErrorMsj + ("Debe de asignar un costo." + "\r\n"));

            // If (TxtUpc.Text = "") Then
            //     ErrorMsj += "Debe especificar el campo UPC." & vbCrLf
            //     'Return False
            // End If

            /*if (ArticuloActual.Prec_Art == 0)
            {
                ErrorMsj = (ErrorMsj + ("Debe de asignar un precio con IVA" + "\r\n"));
                // Return False
            }*/

            if (!articulo.cod_alm.HasValue || articulo.cod_alm == 0)
                ErrorMsj = (ErrorMsj + ("Debe de asignar un almacén por defecto." + "\r\n"));

            if (articulo.id_presentacion == 0)
                ErrorMsj = (ErrorMsj + ("Debe de asignar una unidad." + "\r\n"));

            // If (CboMarca.Text = "") Then
            //     ErrorMsj += "Debe de asignar una marca." & vbCrLf
            //     'Return False
            // End If
            if (!articulo.Cod_Dpto.HasValue)
                ErrorMsj = (ErrorMsj + ("Debe de asignar una categoría." + "\r\n"));

            if (!articulo.Cod_Linea.HasValue)
                ErrorMsj = (ErrorMsj + ("Debe de asignar una sub categoría." + "\r\n"));

            if ((ErrorMsj.Length > 0))
            {
                ErrorMsj = (esPresentacion ?
                    "Debe corregir los siguientes errores antes de guardar la presentación: " :
                    "Debe corregir los siguientes errores antes de guardar el artículo: ")
                    + Environment.NewLine + ErrorMsj;
                throw new SOTException(ErrorMsj);
            }

            //return true;
        }

        

        private void ProducirArticulo(EncabezadoOrdenConversion encabezado, DetalleOrdenConversion detalle)
        {
            var conversionesExistentes = RepositorioConversionesArticulos.ObtenerElementos(m => m.CodigoArticuloSalida == detalle.CodigoArticulo).ToList();

            if (encabezado.FolioTipo.Equals(ClavesFolios.CONV) && conversionesExistentes.Count == 0)
                throw new SOTException(Recursos.ConversionesArticulos.articulo_sin_conversiones_entrada_excepcion, detalle.CodigoArticulo);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
            {
                if (!RepositorioEncabezadosOrdenesConversion.Alguno(m => m.Id != 0 && m.Id == encabezado.Id))
                    RepositorioEncabezadosOrdenesConversion.Agregar(encabezado);
                else
                    RepositorioEncabezadosOrdenesConversion.Modificar(encabezado);

                RepositorioEncabezadosOrdenesConversion.GuardarCambios();

                var calendarioFiscal = ServicioCalendariosFiscales.ObtenerCalendarioPorFecha(encabezado.FechaOperacion.Date);

                //foreach (var det in detalles)
                //{
                detalle.IdEncabezadoOrdenConversion = encabezado.Id;
                detalle.AnioFiscal = calendarioFiscal != null ? calendarioFiscal.Anio : default(int?);
                detalle.MesFiscal = calendarioFiscal != null ? calendarioFiscal.Mes : default(int?);

                if (!RepositorioDetallesOrdenesConversion.Alguno(m => m.Id == detalle.Id))
                    RepositorioDetallesOrdenesConversion.Agregar(detalle);
                else
                    RepositorioDetallesOrdenesConversion.Modificar(detalle);
                //}

                RepositorioDetallesOrdenesConversion.GuardarCambios();

                //foreach (var movInv in detalles)
                //{
                decimal cantidadMovimiento = detalle.Cantidad;

                if (encabezado.FolioTipo.Equals(ClavesFolios.CONV))
                {
                    //List<ZctMixItems> items = RepositorioMixItems.GetMixItemsCost(detalle.Cod_Art, (int)detalle.Cod_Alm);

                    foreach (var conversion in conversionesExistentes)
                    {
                        var articulo = RepositorioArticulos.ObtenerConCostoAcumulado(conversion.CodigoArticuloEntrada/*, detalle.IdAlmacen*/);

                        try
                        {
                            decimal multiplicadorConversion = conversion.CantidadEntrada / conversion.CantidadSalida;

                            var surtido = -(cantidadMovimiento * multiplicadorConversion);
                            var costo = (articulo.ExistenciasTMP != 0 ? (articulo.CostoAcumuladoTMP / articulo.ExistenciasTMP) : (articulo.Cos_Art ?? 0)) * surtido;

                            //todo en el mismo almacén
                            ServicioInventarios.AgregarMovimientoComanda(encabezado.FolioTipo, 0, false, costo, detalle.IdEncabezadoOrdenConversion.ToString(), conversion.CodigoArticuloEntrada, surtido, encabezado.FechaOperacion, false, detalle.IdAlmacen);

                            break;
                        }
                        catch
                        {
                            //Si la última conversión generó error significa que no hay modo de generar el elemento requerido, así que se lanzará una excepción
                            if (conversionesExistentes.IndexOf(conversion) == conversionesExistentes.Count - 1)
                                throw;
                        }
                    }
                }

                ServicioInventarios.AgregarMovimientoComanda(encabezado.FolioTipo, 0, encabezado.FolioTipo.Equals(ClavesFolios.CONV), detalle.Costo * cantidadMovimiento,
                                                             detalle.IdEncabezadoOrdenConversion.ToString(), detalle.CodigoArticulo, (encabezado.FolioTipo.Equals(ClavesFolios.CONV) ? cantidadMovimiento : -cantidadMovimiento), encabezado.FechaOperacion, false, detalle.IdAlmacen);

                //}

                ServicioFolios.GenerarYObtenerSiguienteFolio(ClavesFolios.IC);

                scope.Complete();

            }
        }

        public decimal ObtenerCosto(string codigoArticulo, DtoUsuario usuario)
        {
            return RepositorioArticulos.ObtenerElementos(m => m.Cod_Art == codigoArticulo).Select(m => m.Cos_Art).FirstOrDefault() ?? 0;
        }

        public bool VerificarEsPresentacion(string codigoArticulo) 
        { 
            var codigoU = codigoArticulo.Trim().ToUpper();

            return RepositorioArticulos.Alguno(m => m.Cod_Art.Trim().ToUpper().Equals(codigoU) && m.CodigoPadre != null);
        }

        public List<string> ObtenerClavesPresentaciones(string codigoArticulo) 
        {
            var codigoU = codigoArticulo.Trim().ToUpper();

            return RepositorioArticulos.ObtenerElementos(m => m.CodigoPadre != null && m.CodigoPadre.Trim().ToUpper().Equals(codigoU)).Select(m => m.Cod_Art).ToList();
        }

        public void ValidarExistenArticulosInventariables(int? codigoDepartamento, int? codigoLinea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarArticulos = true });

            Expression<Func<Articulo, bool>> filtro = m => m.ZctCatTpArt.Inventariable == true;

            if (codigoDepartamento.HasValue)
                filtro = filtro.Compose(m => m.Cod_Dpto == codigoDepartamento, Expression.AndAlso);

            if (codigoLinea.HasValue)
                filtro = filtro.Compose(m => m.Cod_Linea == codigoLinea, Expression.AndAlso);

            if (!RepositorioArticulos.Alguno(filtro))
                throw new SOTException(Recursos.Inventarios.no_articulos_para_inventariar_excepcion);
        }
    }
}
