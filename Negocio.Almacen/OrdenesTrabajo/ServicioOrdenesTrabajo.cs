﻿using Modelo.Almacen.Constantes;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Clientes;
using Negocio.Almacen.Folios;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.OrdenesTrabajo
{
    public class ServicioOrdenesTrabajo : IServicioOrdenesTrabajo
    {
        IServicioFoliosInterno ServicioFolios
        {
            get { return _servicioFolios.Value; }
        }

        Lazy<IServicioFoliosInterno> _servicioFolios = new Lazy<IServicioFoliosInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioFoliosInterno>());

        IRepositorioEncabezadosOrdenesTrabajo RepositorioEncabezadosOrdenesTrabajo
        {
            get { return _repositosioEncabezadosOrdenesTrabajo.Value; }
        }

        Lazy<IRepositorioEncabezadosOrdenesTrabajo> _repositosioEncabezadosOrdenesTrabajo = new Lazy<IRepositorioEncabezadosOrdenesTrabajo>(() => FabricaDependencias.Instancia.Resolver<IRepositorioEncabezadosOrdenesTrabajo>());

        IRepositorioDetallesOrdenesTrabajo RepositorioDetallesOrdenesTrabajo
        {
            get { return _repositosioDetallesOrdenesTrabajo.Value; }
        }

        Lazy<IRepositorioDetallesOrdenesTrabajo> _repositosioDetallesOrdenesTrabajo = new Lazy<IRepositorioDetallesOrdenesTrabajo>(() => FabricaDependencias.Instancia.Resolver<IRepositorioDetallesOrdenesTrabajo>());

        IRepositorioVW_ZctStatusOT RepositorioVW_ZctStatusOT
        {
            get { return _repositorioVW_ZctStatusOT.Value; }
        }

        Lazy<IRepositorioVW_ZctStatusOT> _repositorioVW_ZctStatusOT = new Lazy<IRepositorioVW_ZctStatusOT>(() => FabricaDependencias.Instancia.Resolver<IRepositorioVW_ZctStatusOT>());

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });

        IServicioClientes ServicioClientes
        {
            get { return _servicioClientes.Value; }
        }

        Lazy<IServicioClientes> _servicioClientes = new Lazy<IServicioClientes>(() => { return FabricaDependencias.Instancia.Resolver<IServicioClientes>(); });


        public void ValidarCodigosAutorizados(List<int> idsOrdenesTrabajo)
        {
            foreach (var id in idsOrdenesTrabajo)
                if (!RepositorioEncabezadosOrdenesTrabajo.Alguno(m => m.CodEnc_OT == id && m.Cod_Estatus == ZctEncOT.Estados.Aprobada))
                    throw new SOTException(Recursos.OrdenesTrabajo.id_invalido_excepcion, id.ToString());
        }


        public List<ZctEncOT> ObtenerEncabezadosOrdenesAprobadasFILTRO()
        {
            //ServicioPermisos.ValidarPermisos(usuario, new Modelo.Seguridad.Dtos.DtoPermisos { ConsultarOrdenTrabajo = true });

            return RepositorioEncabezadosOrdenesTrabajo.ObtenerElementos(m => m.Cod_Estatus == ZctEncOT.Estados.Aprobada).ToList();
        }

        public Boolean PermiteGrabar(int folio)
        {
            var ot = ObtenerStatusOrdenTrabajo(folio);
            if (ot == null)
                return true;
            else
                if (ot.factura == 1 && ot.caja == 1)
                    return false;
                else
                    if (ot.factura == 1 && ot.caja == 0)
                        return false;
                    else
                        if (ot.factura == 0 && ot.caja == 0)
                            return true;
                        else
                            if (ot.factura == 0 && ot.caja == 1)
                                return true;
                            else
                                return false;

        }
        public Boolean PermiteEditarDetalles(int folio)
        {
            var ot = ObtenerStatusOrdenTrabajo(folio);
            if (ot == null)
                return true;
            else
                if (ot.factura == 1 && ot.caja == 1)
                    return false;
                else
                    if (ot.factura == 1 && ot.caja == 0)
                        return false;
                    else
                        if (ot.factura == 0 && ot.caja == 0)
                            return true;
                        else
                            if (ot.factura == 0 && ot.caja == 1)
                                return false;
                            else
                                return false;

        }


        public VW_ZctStatusOT ObtenerStatusOrdenTrabajo(int folio)
        {
            return RepositorioVW_ZctStatusOT.Obtener(o => o.CodEnc_OT == folio);
        }


        //public int CrearOrdenTrabajoComanda(string concepto, List<Modelo.Almacen.Entidades.ZctDetOT> detalles, int idUsuario, int? folio = null)
        //{
        //    var hotelActual = ServicioClientes.ObtenerUltimoIdCliente();

        //    if (hotelActual == 0)
        //        throw new SOTException("No se han registrado los datos fiscales del hotel");

        //    var fechaActual = DateTime.Now;

        //    if (!folio.HasValue || folio == 0)
        //    {
        //        folio = ServicioFolios.GenerarYObtenerSiguienteFolio(ClavesFolios.CO).Cons;

        //        RepositorioEncabezadosOrdenesTrabajo.SP_ZctEncOT_Edicion(folio.Value, ClavesFolios.CO, fechaActual, hotelActual, 0, 0, 0, fechaActual, fechaActual, fechaActual, fechaActual,
        //                                                                 concepto, fechaActual, 0, 0, "A", "");
        //    }
        //    else
        //    {
        //        RepositorioEncabezadosOrdenesTrabajo.SP_ZctEncOT_Edicion(folio.Value, ClavesFolios.CO, fechaActual, hotelActual, 0, 0, 0, fechaActual, fechaActual, fechaActual, fechaActual,
        //                                                                 concepto, fechaActual, 0, 0, "N", "");
        //    }

        //    foreach (var detalle in detalles)
        //    {
        //        if (!string.IsNullOrWhiteSpace(detalle.Cod_Art))
        //        {
        //            var artXalm = ServicioArticulos.ObtenerArticulosAlmacenesAceptanSalida(detalle.Cod_Art);

        //            var articulo = ServicioArticulos.ObtenerArticuloPorId(detalle.Cod_Art);

        //            if (articulo == null)
        //                throw new SOTException(Recursos.Articulos.codigo_articulo_invalido_excepcion, detalle.Cod_Art);

        //            if (!artXalm.Any(m => m.Exist_Art.HasValue && m.Exist_Art.Value >= detalle.Ctd_Art))
        //            {
        //                throw new SOTException("No existe un almacén con {0} {1}", detalle.Ctd_Art.Value.ToString(), articulo.Desc_Art);
        //            }

        //            detalle.Cat_Alm = artXalm.OrderBy(m => m.Exist_Art ?? 0).First().Cod_Alm;

        //            if (!detalle.Cat_Alm.HasValue || detalle.Cat_Alm.Value == 0)
        //                throw new SOTException("El almacén en la orden de compra no puede estar vacío");


        //            RepositorioDetallesOrdenesTrabajo.SP_ZctDetOT_EDICION(folio.Value, detalle.Cod_DetOT, detalle.Cod_Art, detalle.Ctd_Art ?? 0, articulo.Cos_Art ?? 0, detalle.PreArt_DetOT ?? 0,
        //                detalle.CtdStd_DetOT ?? 0, fechaActual, fechaActual, detalle.Cat_Alm.Value, detalle.Cod_Mot ?? 0, cod_usu: idUsuario);
        //        }
        //    }

        //    return folio.Value;
        //}
    }
}
