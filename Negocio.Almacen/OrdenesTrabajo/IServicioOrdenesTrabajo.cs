﻿using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.OrdenesTrabajo
{
    public interface IServicioOrdenesTrabajo
    {
        /// <summary>
        /// Valida que los ids correspondan a órdenes de trabajo autorizadas
        /// </summary>
        /// <param name="idsOrdenesTrabajo"></param>
        void ValidarCodigosAutorizados(List<int> idsOrdenesTrabajo);
        /// <summary>
        /// Retorna los encabezados de las órdenes de trabajo autorizadas
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<ZctEncOT> ObtenerEncabezadosOrdenesAprobadasFILTRO();

        Boolean PermiteGrabar(int folio);
        Boolean PermiteEditarDetalles(int folio);
        VW_ZctStatusOT ObtenerStatusOrdenTrabajo(int folio);

        //int CrearOrdenTrabajoComanda(string concepto, List<Modelo.Almacen.Entidades.ZctDetOT> detalles, int idUsuario, int? folio = null);
    }
}
