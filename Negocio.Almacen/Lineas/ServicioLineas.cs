﻿using Modelo;
using Modelo.Almacen.Repositorios;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;
using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Departamentos;
using Negocio.Almacen.Articulos;

namespace Negocio.Almacen.Lineas
{
    public class ServicioLineas : IServicioLineas
    {
        IRepositorioLineas RepositorioLineas
        {
            get { return _repositorioTiposArticulos.Value; }
        }

        Lazy<IRepositorioLineas> _repositorioTiposArticulos = new Lazy<IRepositorioLineas>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioLineas>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioDepartamentos ServicioDepartamentos
        {
            get { return _servicioDepartamentos.Value; }
        }

        Lazy<IServicioDepartamentos> _servicioDepartamentos = new Lazy<IServicioDepartamentos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioDepartamentos>(); });
        IServicioArticulos ServicioArticulos
        {
            get { return _servicioArticulos.Value; }
        }

        Lazy<IServicioArticulos> _servicioArticulos = new Lazy<IServicioArticulos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioArticulos>(); });


        public List<Linea> ObtenerLineas(/*DtoUsuario usuario, */bool soloComandables)
        {
            //ServicioPermisos.ValidarPermisos();

            if(soloComandables)
                return RepositorioLineas.ObtenerElementos(m => m.Activa && m.Comandable).ToList();

            return RepositorioLineas.ObtenerElementos(m => m.Activa).ToList();
        }

        public List<Linea> ObtenerLineasParaFiltro(int cod_dpto = 0)
        {
            if (cod_dpto == 0)
                return RepositorioLineas.ObtenerElementos(m => m.Activa).ToList();

            return RepositorioLineas.ObtenerElementos(m => m.Activa && m.Cod_Dpto == cod_dpto).ToList();
        }

        public List<Linea> ObtenerLineas(DtoUsuario usuario, bool incluirInactivas = false, int cod_dpto = 0) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarLineas = true });

            //if (cod_dpto == 0)
            return RepositorioLineas.ObtenerConNombreDepartamento(incluirInactivas, cod_dpto);

            //return RepositorioLineas.ObtenerElementos(m => m.Activa && m.Cod_Dpto == cod_dpto).ToList();
        }

        public void CrearLinea(Linea linea, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearLinea = true });

            if (linea == null)
                throw new SOTException(Recursos.Lineas.nula_excepcion);

            ValidarDatos(linea);

            linea.Editable = true;
            linea.Activa = true;

            RepositorioLineas.Agregar(linea);
            RepositorioLineas.GuardarCambios();
        }

        public void ModificarLinea(Linea linea, DtoUsuario usuario) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarLinea = true });

            if (linea == null)
                throw new SOTException(Recursos.Lineas.nula_excepcion);

            var lineaOriginal = RepositorioLineas.Obtener(m => m.Activa && m.Cod_Linea == linea.Cod_Linea);

            if (lineaOriginal == null)
                throw new SOTException(Recursos.Lineas.nula_eliminada_excepcion);

            if (lineaOriginal.Editable != true)
                throw new SOTException(Recursos.Lineas.linea_no_editable_excepcion);

            if (linea.Cod_Dpto != lineaOriginal.Cod_Dpto)
                ValidarNoDependencias(lineaOriginal.Cod_Linea);

            lineaOriginal.SincronizarPrimitivas(linea);

            ValidarDatos(lineaOriginal);

            lineaOriginal.Activa = true;

            RepositorioLineas.Modificar(lineaOriginal);
            RepositorioLineas.GuardarCambios();

        }

        public void DesactivarLinea(int idLinea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarLinea = true });

            var lineaOriginal = RepositorioLineas.Obtener(m => m.Cod_Linea == idLinea);

            if (lineaOriginal == null)
                throw new SOTException(Recursos.Lineas.nula_eliminada_excepcion);

            if (lineaOriginal.Editable != true)
                throw new SOTException(Recursos.Lineas.linea_no_editable_excepcion);

            ValidarNoDependencias(idLinea);

            lineaOriginal.Activa = false;
            RepositorioLineas.Modificar(lineaOriginal);
            RepositorioLineas.GuardarCambios();
        }

        public void EliminarLinea(int idLinea, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarLinea = true });

            var lineaOriginal = RepositorioLineas.Obtener(m => m.Cod_Linea == idLinea);

            if (lineaOriginal == null)
                throw new SOTException(Recursos.Lineas.nula_eliminada_excepcion);

            if (lineaOriginal.Editable != true)
                throw new SOTException(Recursos.Lineas.linea_no_editable_excepcion);

            ValidarNoDependencias(idLinea);

            
            RepositorioLineas.Eliminar(lineaOriginal);
            RepositorioLineas.GuardarCambios();
        }

        public List<Linea> ObtenerLineasConArticulos(/*DtoUsuario usuario, */bool soloComandables = true, bool incluirSalidaNegada = false, string filtro = null)
        {
            //ServicioPermisos.ValidarPermisos();

            return RepositorioLineas.ObtenerLineasConArticulos(soloComandables, incluirSalidaNegada, filtro);
        }
        public List<Linea> ObtenerLineasPorId(List<int> idsLineas)
        {
            return RepositorioLineas.ObtenerElementos(m => idsLineas.Contains(m.Cod_Linea)).ToList();
        }


        public bool VerificarExistenLineas(int idDepartamento, bool incluirInactivas = false)
        {
            if (incluirInactivas)
                return RepositorioLineas.Alguno(m => m.Cod_Dpto == idDepartamento);

            return RepositorioLineas.Alguno(m => m.Activa && m.Cod_Dpto == idDepartamento);
        }

        private void ValidarDatos(Linea linea)
        {
            if (string.IsNullOrWhiteSpace(linea.Desc_Linea))
                throw new SOTException(Recursos.Lineas.nombre_invalido_excepcion);

            if (RepositorioLineas.Alguno(m=> m.Cod_Linea != linea.Cod_Linea && m.Activa && m.Desc_Linea.ToUpper().Equals(linea.Desc_Linea.ToUpper())))
                throw new SOTException(Recursos.Lineas.nombre_duplicado_excepcion);

            if (!ServicioDepartamentos.VerificarExistencia(linea.Cod_Dpto))
                throw new SOTException(Recursos.Lineas.departamento_invalido_excepcion);
        }

        private void ValidarNoDependencias(int idLinea)
        {
            if (ServicioArticulos.VerificarExistenciasPorLinea(idLinea))
                throw new SOTException(Recursos.Lineas.articulos_dependientes_excepcion);
        }
    }
}
