﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Dtos;
using Modelo.Almacen.Entidades;

namespace Negocio.Almacen.Lineas
{
    public interface IServicioLineas
    {
        /// <summary>
        /// Retorna los tipos de artículos activos del sistema
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Linea> ObtenerLineas(/*DtoUsuario usuario, */bool soloComandables);
        List<Linea> ObtenerLineasParaFiltro(int cod_dpto = 0);

        List<Linea> ObtenerLineas(DtoUsuario usuario, bool incluirInactivas = false, int cod_dpto = 0);

        void CrearLinea(Linea linea, DtoUsuario usuario);

        void ModificarLinea(Linea linea, DtoUsuario usuario);

        void DesactivarLinea(int idLinea, DtoUsuario usuario);

        void EliminarLinea(int idLinea, DtoUsuario usuario);
        /// <summary>
        /// Retorna los tipos de artículos activos del sistema, con sus respectivos artículos
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        List<Linea> ObtenerLineasConArticulos(/*DtoUsuario usuario, */bool soloComandables = true, bool incluirSalidaNegada = false, string filtro = null);
        List<Linea> ObtenerLineasPorId(List<int> idsLineas);

        bool VerificarExistenLineas(int idDepartamento, bool incluirInactivas = false);
    }
}
