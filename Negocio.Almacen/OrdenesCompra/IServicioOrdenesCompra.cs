﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.OrdenesCompra
{
    public interface IServicioOrdenesCompra
    {
        DtoEncabezadoOC ObtenerResumenOrdenCompra(int codigoOrdenCompra, DtoUsuario usuario);

        //List<DtoEncabezadoOC> SP_ZctEncOC_CONSULTA(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra,
        //                                                  DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion);

        //void SP_ZctEncOC_EDICION(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra,
        //                                                  DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion, string estado);

        //List<DtoDetalleOC> SP_ZctDetOC_CONSULTA(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, int cantidadArticulo,
        //    decimal costoArticulo, int cantidadEnStock, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA);

        //void SP_ZctDetOC_EDICION(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, int cantidadArticulo,
        //    decimal costoArticulo, int cantidadEnStock, DateTime fechaAplicacion, int idAlmacen);

        void EditarOrdenCompra(DtoEncabezadoOC encabezado, string sStatus, DtoUsuario usuario);
        void EditarOrdenServicio(DtoEncabezadoOC encabezado, string sStatus, DtoUsuario usuario);

        ZctEncOC ValidarYObtenerOrdenCompra(int Cod_OC);

        decimal ObtenerSubTotal(int Cod_OC);

        decimal ObtenerIVAOmitir(int Cod_OC);

        /// <summary>
        /// Retorna el mayor de los folios que es menor al proporcionado dentro de las órdenes de compra o de servicio (segun el parámetro esServicio). Retorna null si ninguno cumpe con la condición
        /// </summary>
        /// <param name="folioActual"></param>
        /// <param name="esServicio"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        int? ObtenerUltimoFolioMenorQue(int folioActual, bool esServicio, DtoUsuario usuario);

        /// <summary>
        /// Retorna el menor de los folios que es mayor al proporcionado dentro de las órdenes de compra o de servicio (segun el parámetro esServicio). Retorna null si ninguno cumpe con la condición
        /// </summary>
        /// <param name="folioActual"></param>
        /// <param name="esServicio"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        int? ObtenerPrimerFolioMayorQue(int folioActual, bool esServicio, DtoUsuario usuario);
    }
}
