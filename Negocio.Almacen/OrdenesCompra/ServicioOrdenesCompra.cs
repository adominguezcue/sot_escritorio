﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Constantes;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.CuentasPorPagar;
using Negocio.Almacen.Folios;
using Negocio.Almacen.Inventarios;
using Negocio.Almacen.Parametros;
using Negocio.Almacen.Requisiciones;
using Negocio.Compartido.CentrosCostos;
using Negocio.Compartido.ConceptosGastos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.OrdenesCompra
{
    public class ServicioOrdenesCompra : IServicioOrdenesCompraInterno
    {
        #region dependencias

        IServicioCuentasPorPagar AplicacionServicioCuentasPorPagar
        {
            get { return _aplicacionServicioCuentasPorPagar.Value; }
        }

        Lazy<IServicioCuentasPorPagar> _aplicacionServicioCuentasPorPagar = new Lazy<IServicioCuentasPorPagar>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioCuentasPorPagar>();
        });

        IServicioFoliosInterno ServicioFolios
        {
            get { return _servicioFolios.Value; }
        }

        Lazy<IServicioFoliosInterno> _servicioFolios = new Lazy<IServicioFoliosInterno>(() => FabricaDependencias.Instancia.Resolver<IServicioFoliosInterno>());

        IRepositorioEncabezadosOrdenesCompra RepositorioEncabezadosOrdenesCompra
        {
            get { return _repositorioEncabezadosOrdenesCompra.Value; }
        }

        Lazy<IRepositorioEncabezadosOrdenesCompra> _repositorioEncabezadosOrdenesCompra = new Lazy<IRepositorioEncabezadosOrdenesCompra>(() => FabricaDependencias.Instancia.Resolver<IRepositorioEncabezadosOrdenesCompra>());

        IRepositorioServiciosOrdenesCompra RepositorioServiciosOrdenesCompra
        {
            get { return _repositorioServiciosOrdenesCompra.Value; }
        }

        Lazy<IRepositorioServiciosOrdenesCompra> _repositorioServiciosOrdenesCompra = new Lazy<IRepositorioServiciosOrdenesCompra>(() => FabricaDependencias.Instancia.Resolver<IRepositorioServiciosOrdenesCompra>());

        IRepositorioDetallesOrdenesCompra RepositorioDetallesOrdenesCompra
        {
            get { return _repositorioDetallesOrdenesCompra.Value; }
        }

        Lazy<IRepositorioDetallesOrdenesCompra> _repositorioDetallesOrdenesCompra = new Lazy<IRepositorioDetallesOrdenesCompra>(() => FabricaDependencias.Instancia.Resolver<IRepositorioDetallesOrdenesCompra>());

        //IServicioRequisiciones ServicioRequisiciones
        //{
        //    get { return _servicioRequisiciones.Value; }
        //}

        //Lazy<IServicioRequisiciones> _servicioRequisiciones = new Lazy<IServicioRequisiciones>(() => FabricaDependencias.Instancia.Resolver<IServicioRequisiciones>());

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        //IServicioInventarios ServicioInventarios
        //{
        //    get { return _servicioInventarios.Value; }
        //}

        //Lazy<IServicioInventarios> _servicioInventarios = new Lazy<IServicioInventarios>(() => FabricaDependencias.Instancia.Resolver<IServicioInventarios>());

        IServicioParametros ServicioParametros
        {
            get { return _servicioParametros.Value; }
        }

        Lazy<IServicioParametros> _servicioParametros = new Lazy<IServicioParametros>(() => FabricaDependencias.Instancia.Resolver<IServicioParametros>());

        //IServicioCentrosCostosCompartido ServicioCentrosCostos
        //{
        //    get { return _servicioCentrosCostos.Value; }
        //}

        //Lazy<IServicioCentrosCostosCompartido> _servicioCentrosCostos = new Lazy<IServicioCentrosCostosCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCentrosCostosCompartido>(); });

        IServicioConceptosGastosCompartido ServicioConceptosGastos
        {
            get { return _servicioConceptosGastos.Value; }
        }

        Lazy<IServicioConceptosGastosCompartido> _servicioConceptosGastos = new Lazy<IServicioConceptosGastosCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosGastosCompartido>(); });

        #endregion

        //public List<DtoEncabezadoOC> SP_ZctEncOC_CONSULTA(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra,
        //                                                  DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion)
        //{
        //    return RepositorioEncabezadosOrdenesCompra.SP_ZctEncOC_CONSULTA(codigoOrdenCompra, codigoProveedor, fechaOrdenCompra, fechaAplicacionMovimiento, facturaOrdenCompra, fechaFacturacion);
        //}

        public DtoEncabezadoOC ObtenerResumenOrdenCompra(int codigoOrdenCompra, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarOrdenCompra = true });

            var encabezado = RepositorioEncabezadosOrdenesCompra.ObtenerResumenEncabezadoOC(codigoOrdenCompra);

            if (encabezado != null)
            {
                if (encabezado.Servicios != true)
                    encabezado.Detalles = RepositorioDetallesOrdenesCompra.ObtenerDetallesOrdenCompra(codigoOrdenCompra);
                else
                    encabezado.ServiciosOC = RepositorioServiciosOrdenesCompra.ObtenerServiciosOrdenCompra(codigoOrdenCompra);
            }

            return encabezado;
        }

        //public List<DtoDetalleOC> SP_ZctDetOC_CONSULTA(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, int cantidadArticulo,
        //                                               decimal costoArticulo, int cantidadEnStock, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA)
        //{
        //    return RepositorioDetallesOrdenesCompra.SP_ZctDetOC_CONSULTA(codigoOrdenCompra, codigoDetalle, codigoArticulo, cantidadArticulo, costoArticulo, cantidadEnStock, fechaAplicacion, idAlmacen, omitirIVA);
        //}

        public void EditarOrdenServicio(DtoEncabezadoOC encabezado, string sStatus, DtoUsuario usuario)
        {
            if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                usuario = ServicioPermisos.ObtenerUsuarioPorCredencial();

            if (encabezado.Servicios != true)
                throw new SOTException(Recursos.OrdenesCompra.orden_compra_excepcion);

            if (!encabezado.IdConceptoGasto.HasValue)
                throw new SOTException(Recursos.OrdenesCompra.orden_servicio_concepto_gasto_faltante_excepcion);

            var encabezadoOriginal = RepositorioEncabezadosOrdenesCompra.Obtener(m => m.Cod_OC == encabezado.Cod_OC, m=> m.ServiciosOrdenCompra);

            if (encabezadoOriginal != null)
            {
                //if (encabezadoOriginal.status == ZctEncOC.EstatusOrdenesCompra.CERRADA)
                  //  throw new SOTException("La orden de compra se encuentra cerrada");

                //if (encabezadoOriginal.status == ZctEncOC.EstatusOrdenesCompra.CERRADA && encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                  //  throw new SOTException(Recursos.OrdenesCompra.orden_cerrada_no_cancelable_excepcion);

                if (encabezadoOriginal.Servicios != true)
                    throw new SOTException(Recursos.OrdenesCompra.orden_compra_excepcion);

                ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarOrdenCompra = true });
            }
            else
            {
                if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                    throw new SOTException(Recursos.OrdenesCompra.creacion_orden_cancelada_excepcion);

                ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearOrdenCompra = true });
            }

            if (encabezado.Detalles.Count > 0)
                throw new SOTException(Recursos.OrdenesCompra.articulos_no_permitidos_servicio_excepcion);

            if (encabezado.ServiciosOC.All(m => m.Eliminar))
            {
                var idsDetalles = encabezado.ServiciosOC.Select(m => m.Id).ToList();

                if (encabezadoOriginal == null || !encabezadoOriginal.ServiciosOrdenCompra.Any(m =>!idsDetalles.Contains(m.Id)))
                    throw new SOTException(Recursos.OrdenesCompra.orden_servicio_vacia_excepcion);
            }

            bool nuevo;

            if (nuevo = encabezadoOriginal == null)
                encabezadoOriginal = new ZctEncOC { Cod_OC = encabezado.Cod_OC };

            bool soloActivos = nuevo || encabezadoOriginal.IdConceptoGasto != encabezado.IdConceptoGasto;

            encabezadoOriginal.Cod_Prov = encabezado.Cod_Prov;
            encabezadoOriginal.Fch_OC = encabezado.Fch_OC;
            encabezadoOriginal.FchAplMov = encabezado.FchAplMov;
            encabezadoOriginal.Fac_OC = encabezado.Fac_OC;
            encabezadoOriginal.FchFac_OC = encabezado.FchFac_OC;
            encabezadoOriginal.status = encabezado.status;
            encabezadoOriginal.Servicios = true;
            encabezadoOriginal.IdConceptoGasto = encabezado.IdConceptoGasto;

            if (nuevo)
            {
                foreach (var servicio in encabezado.ServiciosOC.Where(m=> !m.Eliminar))
                {
                    encabezadoOriginal.ServiciosOrdenCompra.Add(new ServicioOrdenCompra
                    {
                        Costo = servicio.Costo,
                        Descripcion = servicio.Descripcion,
                        Uuid = Guid.NewGuid()
                    });
                }

                encabezadoOriginal.Fch_OC = DateTime.Now;
                encabezadoOriginal.status = ZctEncOC.EstatusOrdenesCompra.APLICADO;

                RepositorioEncabezadosOrdenesCompra.Agregar(encabezadoOriginal);
            }
            else
            {
                foreach (var servicio in encabezado.ServiciosOC.Where(m => !m.Eliminar))
                {
                    var servicioOriginal = encabezadoOriginal.ServiciosOrdenCompra.FirstOrDefault(m => m.Id == servicio.Id);

                    if (servicioOriginal != null)
                    {
                        servicioOriginal.Costo = servicio.Costo;
                        servicioOriginal.Descripcion = servicio.Descripcion;
                        servicioOriginal.UpdateWeb = null;
                        servicioOriginal.EntidadEstado = EntidadEstados.Modificado; 
                    }
                    else
                    {
                        encabezadoOriginal.ServiciosOrdenCompra.Add(new ServicioOrdenCompra
                        {
                            Costo = servicio.Costo,
                            Descripcion = servicio.Descripcion,
                            Uuid = Guid.NewGuid()
                        });
                    }
                }

                RepositorioEncabezadosOrdenesCompra.Modificar(encabezadoOriginal);
            }

            


            ServicioConceptosGastos.ValidarConceptoGasto(encabezadoOriginal.IdConceptoGasto.Value, soloActivos);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (sStatus == "A")
                    ServicioFolios.GenerarYObtenerSiguienteFolio(ClavesFolios.OC);

                RepositorioEncabezadosOrdenesCompra.GuardarCambios();

                if (!nuevo)
                {
                    var idsEliminar = encabezado.ServiciosOC.Where(m => m.Eliminar).Select(m => m.Id).ToList();
                    var serviciosEliminar = RepositorioServiciosOrdenesCompra.ObtenerElementos(m => m.CodigoOrdenCompra == encabezado.Cod_OC && idsEliminar.Contains(m.Id)).ToList();

                    foreach (var sEliminar in serviciosEliminar)
                    {
                        RepositorioServiciosOrdenesCompra.Eliminar(sEliminar);
                    }

                    RepositorioServiciosOrdenesCompra.GuardarCambios();
                }

                if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                    AplicacionServicioCuentasPorPagar.CancelarCxP(encabezado.Cod_OC);

                else if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CERRADA)
                    AplicacionServicioCuentasPorPagar.CreaCuentaPagar(encabezado.Cod_OC);

                scope.Complete();
            }
        }

        public void EditarOrdenCompra(DtoEncabezadoOC encabezado, string sStatus, DtoUsuario usuario)
        {
            if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                usuario = ServicioPermisos.ObtenerUsuarioPorCredencial();

            if (encabezado.Servicios == true)
                throw new SOTException(Recursos.OrdenesCompra.orden_servicio_excepcion);

            if (encabezado.IdConceptoGasto.HasValue)
                throw new SOTException(Recursos.OrdenesCompra.orden_compra_concepto_gasto_vinculado_excepcion);

            var encabezadoOriginal = RepositorioEncabezadosOrdenesCompra.Obtener(m => m.Cod_OC == encabezado.Cod_OC);

            bool EsCancelado = false;
            bool esNuevo;

            if (esNuevo = encabezadoOriginal == null)
            {
                if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                    throw new SOTException(Recursos.OrdenesCompra.creacion_orden_cancelada_excepcion);

                ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearOrdenCompra = true });

                encabezado.Detalles.RemoveAll(m => m.Eliminar);
            }
            else
            {
                /*2019-09-12 Antonio De Jesús Domínguez Cuevas 
                Se corrige cuando una Orden de compra esta cerrada Se debe poder Cancelar la orden
                */
                /* if (encabezadoOriginal.status == ZctEncOC.EstatusOrdenesCompra.CERRADA)
                     throw new SOTException("La orden de compra se encuentra cerrada");

                 if (encabezadoOriginal.status == ZctEncOC.EstatusOrdenesCompra.CERRADA && encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                     throw new SOTException(Recursos.OrdenesCompra.orden_cerrada_no_cancelable_excepcion);*/


               
                if(encabezadoOriginal.status == ZctEncOC.EstatusOrdenesCompra.CERRADA)
                {
                    foreach (var detalle in encabezado.Detalles)
                    {
                        detalle.Ctd_Art = (-1) * detalle.Ctd_Art;
                        EsCancelado = true;
                    }
                }

                if (encabezadoOriginal.Servicios == true)
                    throw new SOTException(Recursos.OrdenesCompra.orden_servicio_excepcion);

                ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarOrdenCompra = true });

                var idsDetalles = encabezado.Detalles.Select(m => m.CodDet_Oc).ToList();
                idsDetalles = RepositorioDetallesOrdenesCompra.ObtenerElementos(m => m.Cod_OC == encabezado.Cod_OC && idsDetalles.Contains(m.CodDet_OC)).Select(m => m.CodDet_OC).ToList();

                encabezado.Detalles.RemoveAll(m => m.Eliminar && !idsDetalles.Contains(m.CodDet_Oc));

                encabezado.Fch_OC = encabezadoOriginal.Fch_OC;
            }

            if (encabezado.ServiciosOC.Count > 0)
                throw new SOTException(Recursos.OrdenesCompra.servicios_no_permitidos_compra_excepcion);

            if (encabezado.Detalles.All(m => m.Eliminar))
            {
                var idsDetalles = encabezado.Detalles.Select(m => m.CodDet_Oc).ToList();

                if (!RepositorioDetallesOrdenesCompra.Alguno(m => m.Cod_OC == encabezado.Cod_OC && !idsDetalles.Contains(m.CodDet_OC)))
                    throw new SOTException(Recursos.OrdenesCompra.orden_compra_vacia_excepcion);
            }
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                if (sStatus == "A")
                    ServicioFolios.GenerarYObtenerSiguienteFolio(ClavesFolios.OC);

                RepositorioEncabezadosOrdenesCompra.SP_ZctEncOC_EDICION(encabezado.Cod_OC, encabezado.Cod_Prov.Value, encabezado.Fch_OC.Value, encabezado.FchAplMov.Value, encabezado.Fac_OC, encabezado.FchFac_OC.Value, encabezado.status, encabezado.Servicios);

                foreach (var detalle in encabezado.Detalles)
                    RepositorioDetallesOrdenesCompra.SP_ZctDetOC_EDICION(detalle.Cod_OC, detalle.CodDet_Oc, detalle.Cod_Art, detalle.Ctd_Art.Value, detalle.Cos_Art.Value, detalle.Eliminar ? 0 : detalle.CtdStdDet_OC.Value, detalle.FchAplMov.Value, detalle.Cat_Alm.Value, detalle.OmitirIVA, EsCancelado);

                foreach (var detalle in encabezado.Detalles.Where(m => m.Eliminar))
                    RepositorioDetallesOrdenesCompra.SP_ZctDetOC_ELIMINACION(detalle.Cod_OC, detalle.CodDet_Oc, detalle.Cod_Art, detalle.Ctd_Art.Value, detalle.Cos_Art.Value, /*detalle.CtdStdDet_OC.Value*/0, detalle.FchAplMov.Value, detalle.Cat_Alm.Value, detalle.OmitirIVA);

                if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CANCELADO)
                    AplicacionServicioCuentasPorPagar.CancelarCxP(encabezado.Cod_OC);

                else if (encabezado.status == ZctEncOC.EstatusOrdenesCompra.CERRADA)
                    AplicacionServicioCuentasPorPagar.CreaCuentaPagar(encabezado.Cod_OC);

                scope.Complete();
            }
        }

        public ZctEncOC ValidarYObtenerOrdenCompra(int Cod_OC)
        {
            var ordenCompra = RepositorioEncabezadosOrdenesCompra.Obtener(m => m.Cod_OC == Cod_OC);

            if (ordenCompra == null)
                throw new SOTException(Recursos.OrdenesCompra.codigo_orden_compra_invalido_excepcion, Cod_OC.ToString());

            return ordenCompra;
        }

        public decimal ObtenerSubTotal(int Cod_OC)
        {
            var encabezado = RepositorioEncabezadosOrdenesCompra.Obtener(m => m.Cod_OC == Cod_OC);

            if (encabezado == null)
                return 0;

            var IVA = ServicioParametros.ObtenerParametros().Iva_CatPar;

            var valoresDetalles = encabezado.Servicios == true ? 
                RepositorioServiciosOrdenesCompra.ObtenerElementos(m => m.CodigoOrdenCompra == Cod_OC).Select(m => m.Costo / (1 + IVA)).ToList():
            RepositorioDetallesOrdenesCompra.ObtenerElementos(m => m.Cod_OC == Cod_OC).Select(m => (m.Cos_Art ?? 0) * (m.Ctd_Art ?? 0)).ToList();

            return valoresDetalles.Sum();

            //(from st in _dao.ZctDetOC where st.Cod_OC == Cod_OC select (st.Cos_Art * st.Ctd_Art)).Sum()
        }

        public decimal ObtenerIVAOmitir(int Cod_OC)
        {
            var parametros = ServicioParametros.ObtenerParametros();

            return RepositorioDetallesOrdenesCompra.ObtenerElementos(m => m.Cod_OC == Cod_OC && m.OmitirIVA).Select(m => (m.Cos_Art ?? 0) * (m.Ctd_Art ?? 0) * (parametros.Iva_CatPar)).ToList().Sum();

            //(from st in _dao.ZctDetOC where st.Cod_OC == Cod_OC select (st.Cos_Art * st.Ctd_Art)).Sum()
        }

        #region métodos internal 

        void IServicioOrdenesCompraInterno.ReflejaEnCentroCostos(int idCuentaPago, DateTime fechaFiltroTurno, int idEncabezadoOrdenCompra, int idUsuario, ClasificacionesGastos? clasificacion)
        {
            var ordenCompra = RepositorioEncabezadosOrdenesCompra.Obtener(m => m.Cod_OC == idEncabezadoOrdenCompra, m => m.ServiciosOrdenCompra);

            var parametros = ServicioParametros.ObtenerParametros();

#warning ¿solicitado o surtido?
            decimal valor;

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                ServicioConceptosGastos.EliminarGastos(idCuentaPago, idUsuario);

                if (ordenCompra.Servicios == true)
                {
                    valor = ordenCompra.ServiciosOrdenCompra.Sum(m => m.Costo);

                    if (valor != 0)
                        ServicioConceptosGastos.RegistrarGasto(idCuentaPago, ordenCompra.IdConceptoGasto.Value, valor, idUsuario, clasificacion, fechaFiltroTurno);
                }
                else
                {
                    var detallesOrden = RepositorioDetallesOrdenesCompra.ObtenerElementos(m => m.Cod_OC == idEncabezadoOrdenCompra).ToList();

                    foreach (var grupo in detallesOrden.GroupBy(m => m.Cod_Art))
                    {
                        valor = grupo.Sum(m => (m.Cos_Art ?? 0) * (m.Ctd_Art ?? 0) * (m.OmitirIVA ? 1 : (1 + parametros.Iva_CatPar)));

                        if (valor != 0)
                            ServicioConceptosGastos.RegistrarGasto(idCuentaPago, grupo.Key, valor, idUsuario, clasificacion, fechaFiltroTurno);
                    }
                }
                scope.Complete();
            }
        }

        bool IServicioOrdenesCompraInterno.VerificarExistenOrdenesParaProveedor(int codigoProveedor)
        {
            return RepositorioEncabezadosOrdenesCompra.Alguno(m => m.Cod_Prov == codigoProveedor);
        }

        #endregion

        public int? ObtenerUltimoFolioMenorQue(int folioActual, bool esServicio, DtoUsuario usuario)
        {
            return RepositorioEncabezadosOrdenesCompra.ObtenerElementos(m => (m.Servicios == esServicio || (!esServicio && !m.Servicios.HasValue)) && m.Cod_OC < folioActual).OrderByDescending(m=> m.Cod_OC).FirstOrDefault()?.Cod_OC;
        }

        public int? ObtenerPrimerFolioMayorQue(int folioActual, bool esServicio, DtoUsuario usuario)
        {
            return RepositorioEncabezadosOrdenesCompra.ObtenerElementos(m => (m.Servicios == esServicio || (!esServicio && !m.Servicios.HasValue)) && m.Cod_OC > folioActual).OrderBy(m => m.Cod_OC).FirstOrDefault()?.Cod_OC;
        }
    }
}
