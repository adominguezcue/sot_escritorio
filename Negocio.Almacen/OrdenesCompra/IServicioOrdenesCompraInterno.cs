﻿using Dominio.Nucleo.Entidades;
using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.OrdenesCompra
{
    internal interface IServicioOrdenesCompraInterno : IServicioOrdenesCompra
    {
        void ReflejaEnCentroCostos(int idCuentaPago, DateTime fechaFiltroTurno, int idEncabezadoOrdenCompra, int idUsuario, ClasificacionesGastos? clasificacion);

        bool VerificarExistenOrdenesParaProveedor(int codigoProveedor);
    }
}
