﻿using Modelo.Seguridad.Dtos;
using System;
namespace Negocio.Almacen.RetirosCaja
{
    public interface IServicioRetirosCaja
    {
        int RetiroCaja(string Cod_folioCaja, decimal Monto, DtoUsuario usuario);
        decimal TraeTotalRetirosCaja(string cod_folio, DtoUsuario usuario);
    }
}
