﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.RetirosCaja
{
    public class ServicioRetirosCaja : Negocio.Almacen.RetirosCaja.IServicioRetirosCaja
    {
        IRepositorioZctRetirosCajas RepositorioZctRetirosCajas
        {
            get { return _repositorioTiposArticulos.Value; }
        }

        Lazy<IRepositorioZctRetirosCajas> _repositorioTiposArticulos = new Lazy<IRepositorioZctRetirosCajas>(() => FabricaDependencias.Instancia.Resolver<IRepositorioZctRetirosCajas>());

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => FabricaDependencias.Instancia.Resolver<IServicioPermisos>());

        public int RetiroCaja(string Cod_folioCaja, decimal Monto, DtoUsuario usuario)
        {
            var Retiro = new ZctRetirosCajas();
            Retiro.cod_folio = Cod_folioCaja;
            Retiro.cod_usuario = usuario.Id;
            Retiro.monto = Monto;
            Retiro.fecha_retiro = DateTime.Now;
            Retiro.corte = false;
            RepositorioZctRetirosCajas.Agregar(Retiro);

            RepositorioZctRetirosCajas.GuardarCambios();

            return RepositorioZctRetirosCajas.ObtenerUltimoCodigoRetiro(Cod_folioCaja);

            //return (from Modelo.ZctRetirosCajas r in _dao.ZctRetirosCajas
            //        where r.cod_folio == Cod_folioCaja
            //        select r.cod_retiro).Max();
        }
        public decimal TraeTotalRetirosCaja(string cod_folio, DtoUsuario usuario)
        {
            return RepositorioZctRetirosCajas.ObtenerElementos(r => r.cod_usuario == usuario.Id && r.cod_folio == cod_folio && !r.corte).Select(m => m.monto).ToList().Sum(m => m);
            //if (Retiros.Count == 0)
            //{
            //    return 0;
            //}
            //return (from r in _dao.ZctRetirosCajas where r.cod_usuario == cod_user && r.cod_folio == cod_folio && r.corte == false select r.monto).Sum();
        }
    }
}
