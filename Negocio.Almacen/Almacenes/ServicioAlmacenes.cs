﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Compartido.CentrosCostos;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.Almacenes
{
    public class ServicioAlmacenes : IServicioAlmacenes
    {
        IRepositorioAlmacenes RepositorioAlmacenes
        {
            get { return _repositorioAlmacenes.Value; }
        }

        Lazy<IRepositorioAlmacenes> _repositorioAlmacenes = new Lazy<IRepositorioAlmacenes>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioAlmacenes>(); });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioCentrosCostosCompartido ServicioCentrosCostos
        {
            get { return _servicioCentrosCostos.Value; }
        }

        Lazy<IServicioCentrosCostosCompartido> _servicioCentrosCostos = new Lazy<IServicioCentrosCostosCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCentrosCostosCompartido>(); });

        public List<Modelo.Almacen.Entidades.Almacen> ObtenerAlmacenes(DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarAlmacenes = true });

            return RepositorioAlmacenes.ObtenerTodo().ToList();
        }

        public void VincularCentroCostos(int idAlmacen, int idCentroCostos, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarAlmacenes = true });

            var almacen = RepositorioAlmacenes.Obtener(m => m.Cod_Alm == idAlmacen);

            if (almacen == null)
                throw new SOTException(Recursos.Almacenes.nulo_excepcion);

            if (almacen.IdCentroCostos == idCentroCostos)
                return;

            ServicioCentrosCostos.ValidarExistencia(idCentroCostos);

            almacen.IdCentroCostos = idCentroCostos;
            RepositorioAlmacenes.Modificar(almacen);
            RepositorioAlmacenes.GuardarCambios();
        }


        public void DesvincularCentroCostos(int idAlmacen, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarAlmacenes = true });

            var almacen = RepositorioAlmacenes.Obtener(m => m.Cod_Alm == idAlmacen);

            if (almacen == null)
                throw new SOTException(Recursos.Almacenes.nulo_excepcion);

            if (!almacen.IdCentroCostos.HasValue)
                return;

            almacen.IdCentroCostos = null;
            RepositorioAlmacenes.Modificar(almacen);
            RepositorioAlmacenes.GuardarCambios();
        }


        public bool VerificarPoseeVinculaciones(int idCentroCostos)
        {
            return RepositorioAlmacenes.Alguno(m => m.IdCentroCostos == idCentroCostos);
        }


        public List<Modelo.Almacen.Entidades.Almacen> ObtenerAlmacenesAceptanSalidas()
        {
            return RepositorioAlmacenes.ObtenerElementos(m => m.salida == true).ToList();
        }
        public List<Modelo.Almacen.Entidades.Almacen> ObtenerAlmacenesAceptanEntradas()
        {
            return RepositorioAlmacenes.ObtenerElementos(m => m.entrada == true).ToList();
        }

        public DtoEncabezadoIntercambio SP_ObtenerResumenIntercambio(int idFolio, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarIntercambiosAlmacenes = true });

            return RepositorioAlmacenes.SP_ObtenerResumenIntercambio(idFolio);
        }
    }
}
