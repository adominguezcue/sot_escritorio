﻿using Modelo.Almacen.Entidades.Dtos;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Almacenes
{
    public interface IServicioAlmacenes
    {
        List<Modelo.Almacen.Entidades.Almacen> ObtenerAlmacenes(DtoUsuario usuario);

        void VincularCentroCostos(int idAlmacen, int idCentroCostos, DtoUsuario usuario);

        void DesvincularCentroCostos(int idAlmacen, DtoUsuario usuario);

        bool VerificarPoseeVinculaciones(int idCentroCostos);

        List<Modelo.Almacen.Entidades.Almacen> ObtenerAlmacenesAceptanSalidas();

        List<Modelo.Almacen.Entidades.Almacen> ObtenerAlmacenesAceptanEntradas();

        DtoEncabezadoIntercambio SP_ObtenerResumenIntercambio(int idFolio, DtoUsuario usuario);
    }
}
