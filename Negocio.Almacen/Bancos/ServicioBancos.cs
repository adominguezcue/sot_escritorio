﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.Almacen.Bancos
{
    public class ServicioBancos : IServicioBancos
    {
        IRepositorioBancos RepositorioBancos
        {
            get { return _repositorioBancos.Value; }
        }

        Lazy<IRepositorioBancos> _repositorioBancos = new Lazy<IRepositorioBancos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioBancos>(); });

        public List<ZctBancos> ObtenerBancos()
        {
            return RepositorioBancos.ObtenerTodo().ToList();
        }
    }
}
