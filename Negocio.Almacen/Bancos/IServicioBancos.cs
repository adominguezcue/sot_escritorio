﻿using Modelo.Almacen.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Bancos
{
    public interface IServicioBancos
    {
        List<ZctBancos> ObtenerBancos();
    }
}
