﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Repositorios;
using Modelo.Seguridad.Dtos;
using Negocio.Almacen.Lineas;
using Negocio.Compartido.ConfiguracionesGlobales;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Almacen.Departamentos
{
    public class ServicioDepartamentos : IServicioDepartamentos
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IServicioLineas ServicioLineas
        {
            get { return _servicioLineas.Value; }
        }

        Lazy<IServicioLineas> _servicioLineas = new Lazy<IServicioLineas>(() => { return FabricaDependencias.Instancia.Resolver<IServicioLineas>(); });

        IServicioConfiguracionesGlobalesCompartido ServicioConfiguracionesGlobalesCompartido
        {
            get { return _servicioConfiguracionesGlobalesCompartido.Value; }
        }

        Lazy<IServicioConfiguracionesGlobalesCompartido> _servicioConfiguracionesGlobalesCompartido = new Lazy<IServicioConfiguracionesGlobalesCompartido>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobalesCompartido>(); });


        IRepositorioDepartamentos RepositorioDepartamentos
        {
            get { return _repositorioTiposArticulos.Value; }
        }

        Lazy<IRepositorioDepartamentos> _repositorioTiposArticulos = new Lazy<IRepositorioDepartamentos>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioDepartamentos>(); });

        public List<Departamento> ObtenerDepartamentos(DtoUsuario usuario, bool soloConComandables = false) 
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConsultarDepartamentos = true });

            if (soloConComandables)
                return RepositorioDepartamentos.ObtenerElementos(m => m.ZctCatLinea.Any(l => l.Comandable)).ToList();

            return RepositorioDepartamentos.ObtenerTodo().ToList();
        }

        public List<Departamento> ObtenerDepartamentosParaFiltro(bool soloConComandables = false)
        {
            if (soloConComandables)
                return RepositorioDepartamentos.ObtenerElementos(m => m.ZctCatLinea.Any(l => l.Comandable)).ToList();

            return RepositorioDepartamentos.ObtenerTodo().ToList();
        }

        public void CrearDepartamento(Departamento departamento, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { CrearDepartamento = true });

            if (departamento == null)
                throw new SOTException(Recursos.Departamentos.nulo_excepcion);

            ValidarCampos(departamento);

            departamento.Editable = true;

            RepositorioDepartamentos.Agregar(departamento);
            RepositorioDepartamentos.GuardarCambios();
        }

        public void ModificarDepartamento(Departamento departamento, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ModificarDepartamento = true });

            if (departamento == null)
                throw new SOTException(Recursos.Departamentos.nulo_excepcion);

            var departamentoOriginal = RepositorioDepartamentos.Obtener(m => m.Cod_Dpto == departamento.Cod_Dpto);

            if (departamentoOriginal == null)
                throw new SOTException(Recursos.Departamentos.nulo_eliminado_excepcion);

            if (departamentoOriginal.Editable != true)
                throw new SOTException(Recursos.Departamentos.departamento_no_editable_excepcion);

            departamentoOriginal.SincronizarPrimitivas(departamento);

            ValidarCampos(departamentoOriginal);

            RepositorioDepartamentos.Modificar(departamentoOriginal);
            RepositorioDepartamentos.GuardarCambios();
        }

        public void EliminarDepartamento(int idDepartamento, DtoUsuario usuario)
        {
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { EliminarDepartamento = true });

            var departamentoOriginal = RepositorioDepartamentos.Obtener(m => m.Cod_Dpto == idDepartamento);

            if (departamentoOriginal == null)
                throw new SOTException(Recursos.Departamentos.nulo_eliminado_excepcion);

            if (departamentoOriginal.Editable != true)
                throw new SOTException(Recursos.Departamentos.departamento_no_editable_excepcion);

            ValidarNoDependencias(idDepartamento);

            RepositorioDepartamentos.Eliminar(departamentoOriginal);
            RepositorioDepartamentos.GuardarCambios();
        }

        private void ValidarNoDependencias(int idDepartamento)
        {
            if (ServicioLineas.VerificarExistenLineas(idDepartamento, true))
                throw new SOTException(Recursos.Departamentos.lineas_dependientes_excepcion);

            if (ServicioConfiguracionesGlobalesCompartido.VerificarEsDepartamentoMaestro(idDepartamento))
                throw new SOTException(Recursos.Departamentos.departamento_maestro_excepcion);
        }

        private void ValidarCampos(Departamento departamento)
        {
            if (string.IsNullOrWhiteSpace(departamento.Desc_Dpto))
                throw new SOTException(Recursos.Departamentos.nombre_invalido_excepcion);

            if (RepositorioDepartamentos.Alguno(m => m.Cod_Dpto != departamento.Cod_Dpto && m.Desc_Dpto.ToUpper().Equals(departamento.Desc_Dpto.ToUpper())))
                throw new SOTException(Recursos.Departamentos.nombre_duplicado_excepcion);
        }


        public bool VerificarExistencia(int idDepartamento)
        {
            return RepositorioDepartamentos.Alguno(m => m.Cod_Dpto == idDepartamento);
        }
    }
}
