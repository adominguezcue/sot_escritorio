﻿using Modelo.Almacen.Entidades;
using Modelo.Seguridad.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Almacen.Departamentos
{
    public interface IServicioDepartamentos
    {
        List<Departamento> ObtenerDepartamentos(DtoUsuario usuario, bool soloConComandables = false);

        List<Departamento> ObtenerDepartamentosParaFiltro(bool soloConComandables = false);

        void CrearDepartamento(Departamento departamento, DtoUsuario usuario);

        void ModificarDepartamento(Departamento departamento, DtoUsuario usuario);

        void EliminarDepartamento(int idDepartamento, DtoUsuario usuario);

        bool VerificarExistencia(int idDepartamento);
    }
}
