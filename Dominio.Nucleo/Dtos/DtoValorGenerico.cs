﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Nucleo.Dtos
{
    public class DtoValorGenerico<T>
    {
        public Guid Id { get; private set; }
        public T Valor { get; set; }

        public DtoValorGenerico() => Id = Guid.NewGuid();
    }
}
