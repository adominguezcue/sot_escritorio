﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Nucleo.Dtos
{
    [DataContract]
    public class DtoIdValor
    {
        [DataMember]
        public int? Id { get; set; }
        [DataMember]
        public string Valor { get; set; }
    }
}
