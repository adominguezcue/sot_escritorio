﻿using Dominio.Nucleo.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Nucleo.Repositorios
{
    /// <summary>
    /// Interfaz para implementar el patrón Repository
    /// </summary>
    /// <typeparam name="TClass"></typeparam>
    public interface IRepositorio<TClass> where TClass : class, IEntidad
    {
        /// <summary>
        /// Agrega un nuevo elemento a la colección correspondiente
        /// </summary>
        /// <param name="elemento"></param>
        void Agregar(TClass elemento);
        /// <summary>
        /// Actualiza los datos del elemento en la colección
        /// </summary>
        /// <param name="elemento"></param>
        void Modificar(TClass elemento);
        /// <summary>
        /// Elimina el elemento de la colección
        /// </summary>
        /// <param name="elemento"></param>
        void Eliminar(TClass elemento);
        /// <summary>
        /// Obtiene el primer elemento que cumpla con el filtro, si ningun elemento coincide, retorna null
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        TClass Obtener(Expression<Func<TClass, bool>> filtro);
        /// <summary>
        /// Obtiene el primer elemento que cumpla con el filtro cargando la propiedad seleccionada,
        /// si ningun elemento coincide, retorna null
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        TClass Obtener(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, object>> path);
        /// <summary>
        /// Obtiene el primer elemento que cumpla con el filtro ordenando el conjunto en base a la propiedad proporcionada,
        /// si ningun elemento coincide, retorna null
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="filtro"></param>
        /// <param name="orden"></param>
        /// <returns></returns>
        TClass ObtenerOrdenado<TKey>(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, TKey>> orden);
        /// <summary>
        /// Obtiene todos los elementos que cumplan con el filtro
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        IEnumerable<TClass> ObtenerElementos(Expression<Func<TClass, bool>> filtro);
        /// <summary>
        /// Obtiene todos los elementos que cumplen con el filtro, cargando la propiedad seleccionada
        /// </summary>
        /// <param name="filtro"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        IEnumerable<TClass> ObtenerElementos(Expression<Func<TClass, bool>> filtro, Expression<Func<TClass, object>> path);
        /// <summary>
        /// Obtiene todos los elementos que cumplan con el filtro, paginados
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        IEnumerable<TClass> ObtenerElementos<TKey>(Expression<Func<TClass, bool>> filtro, Expression<Func<TClass, TKey>> orden, int pagina, int elementos);
        /// <summary>
        /// Obtiene todos los elementos que cumplan con el filtro, paginados
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        IEnumerable<TClass> ObtenerElementos<TKey>(Expression<Func<TClass, bool>> filtro, Expression<Func<TClass, object>> path, Expression<Func<TClass, TKey>> orden, int pagina, int elementos);
        /// <summary>
        /// Retorna todos los elementos de la colección
        /// </summary>
        /// <returns></returns>
        IEnumerable<TClass> ObtenerTodo();
        /// <summary>
        /// Retorna todos los elementos de la colección
        /// </summary>
        /// <returns></returns>
        IEnumerable<TClass> ObtenerTodo(Expression<Func<TClass, object>> path);
        /// <summary>
        /// Determina si existe algun elemento
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        bool Alguno();
        /// <summary>
        /// Determina si existe algun elemento que cumpla con el filtro
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        bool Alguno(Expression<Func<TClass, bool>> filtro);
        /// <summary>
        /// Retorna la cantidad de elementos que cumplen con el filtro
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        int Contador(Expression<Func<TClass, bool>> filtro);
        /// <summary>
        /// Persiste los cambios aplicados
        /// </summary>
        int GuardarCambios();
        /// <summary>
        /// Descarta los cambios aplicados
        /// </summary>
        void DescartarCambios();
    }
}
