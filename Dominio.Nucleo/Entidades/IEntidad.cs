﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Nucleo.Entidades
{
    public interface IEntidad
    {
        List<IEntidad> ObtenerPropiedadesNavegacion();
        void ObtenerPropiedadesNavegacion(List<IEntidad> propiedadesT);
        //int LlavePrimaria { get; set; }

        //Expression<Func<TClass, bool>> Comparador<TClass>() 
        //    where TClass : IEntidad;

        EntidadEstados EntidadEstado { get; set; }
    }

    public enum EntidadEstados
    {
        Creado = 1,
        Modificado = 2,
        Eliminado = 3
    }

    //public interface IEntidadComparable<TClass> where TClass : IEntidad 
    //{
    //    Expression<Func<TClass, bool>> Comparador();
    //}
}
