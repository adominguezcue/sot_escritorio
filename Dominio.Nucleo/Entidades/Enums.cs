﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Nucleo.Entidades
{
    [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
    public enum TiposPago
    {
        Efectivo = 1,
        [Description("Tarjeta de crédito")]
        TarjetaCredito = 2,
        [Description("V Points")]
        VPoints = 3,
        [Description("Reservación")]
        Reservacion = 4,
        [Description("Cupón")]
        Cupon = 5,
        [Description("Cortesía")]
        Cortesia = 6,
        [Description("Consumo interno")]
        Consumo = 7,
        [Description("Transferencia")]
        Transferencia = 8
    }

    [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
    public enum TiposPersonas
    {
        [Description("Física")]
        Fisica = 1,
        [Description("Moral")]
        Moral = 2
    }

    [TypeConverter(typeof(Transversal.Conversores.EnumDescriptionTypeConverter))]
    public enum ClasificacionesGastos
    {
        [Description("Gasto de operadora")]
        Operadora = 1,
        [Description("Gasto de propietaria")]
        Propietaria = 2,
        [Description("Gasto de oficina")]
        Oficina = 3
    }
}
