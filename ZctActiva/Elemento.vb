Friend Class Elemento
    Implements ZctSQL.IZctDatos

    Private _IdDesc As Guid
    Public Property IdDesc() As Guid
        Get
            Return _IdDesc
        End Get
        Set(ByVal value As Guid)
            _IdDesc = value
        End Set
    End Property

    Private _Path As String
    Public Property Path() As String
        Get
            Return _Path
        End Get
        Set(ByVal value As String)
            _Path = value
        End Set
    End Property

    Private _TipoElemento As Tipo
    Public Property TipoElemento() As Tipo
        Get
            Return _TipoElemento
        End Get
        Set(ByVal value As Tipo)
            _TipoElemento = value
        End Set
    End Property

    Private _Version As String
    Public Property Version() As String
        Get
            Return _Version
        End Get
        Set(ByVal value As String)
            _Version = value
        End Set
    End Property

    Public Sub New(ByVal IdDesc As Guid, ByVal Path As String, ByVal TipoElemento As Tipo)
        _IdDesc = IdDesc
        _Path = Path
        _TipoElemento = TipoElemento
    End Sub


    Public Sub GetMe(ByVal DataReader As System.Data.SqlClient.SqlDataReader) Implements ZctSQL.IZctDatos.GetMe

    End Sub

    Public ReadOnly Property SQLElimina() As String Implements ZctSQL.IZctDatos.SQLElimina
        Get
            Return ""
        End Get
    End Property

    Public ReadOnly Property SQLGraba() As String Implements ZctSQL.IZctDatos.SQLGraba
        Get
            Return "INSERT INTO ZctActiva (IdActiva) VALUES ('" & _IdDesc.ToString & "')"
        End Get
    End Property

    Public ReadOnly Property SQLObtiene() As String Implements ZctSQL.IZctDatos.SQLObtiene
        Get
            Return ""
        End Get
    End Property
End Class

Public Enum Tipo
    Reporte = 0
    Estructura_Datos = 1
End Enum

