Imports System.IO
Imports System.Net
Imports System.Text
Public Class Activa
    Private _RutaInicial As String
    Private _RutaReportes As String
    Private _CargaInicial As String = "IF OBJECT_ID('ZctActiva', 'U') IS NULL " & vbCrLf & _
                                      " CREATE TABLE ZctActiva ( IdActiva uniqueidentifier  NOT NULL " & vbCrLf & _
                                      " CONSTRAINT [Pk_ActivaId]  PRIMARY KEY CLUSTERED " & vbCrLf & _
                                      " (IdActiva ASC)ON [PRIMARY] )"

    Private _rutaDownload As String = "http://engranedigital.com/restotalsot/ListActualiza.zct"
    Private _RutaSQL As String = "http://engranedigital.com/restotalsot/SQL/"
    Private _RutaRPT As String = "http://engranedigital.com/restotalsot/RPT/"
    Private _RutaLOCALRPT As String = "C:\Reportes\"



    Private _Lista As ListElementos
    Private _ListaErrores As List(Of Elemento)

    Private _Cnn As String
    Public Property Cnn() As String
        Get
            Return _Cnn
        End Get
        Set(ByVal value As String)
            _Cnn = value
        End Set
    End Property

    ''' <summary>
    ''' Busca actualizaciones
    ''' </summary>
    ''' <param name="cnn">Cadena de conexion</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal cnn As String)
        _Cnn = cnn

        ZctSQL.Implementacion.GenEjecuta(cnn, _CargaInicial)
    End Sub

    Public ReadOnly Property Errores() As String
        Get
            If _ListaErrores IsNot Nothing AndAlso _ListaErrores.Count > 0 Then
                Dim st As New StringBuilder
                For Each v As Elemento In _ListaErrores
                    st.AppendLine(String.Format("Error en la actualización {0} ", v.IdDesc.ToString))
                Next
                Return st.ToString
            Else
                Return ""
            End If
        End Get
    End Property

    Public Sub BuscaActualizaciones()
        If _Lista Is Nothing Then _Lista = New ListElementos(_Cnn)
        If _ListaErrores Is Nothing Then _ListaErrores = New List(Of Elemento)
        _Lista.Clear()
        GetActualizaciones()
        _Lista.ForEach(AddressOf Funciones)
    End Sub


    Private Sub GetActualizaciones()
        Dim myWebRequest As WebRequest = WebRequest.Create(_rutaDownload)

        '' Assign the response object of 'WebRequest' to a 'WebResponse' variable.
        Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
        ' Print the HTML contents of the page to the console. 
        Dim streamResponse As Stream = myWebResponse.GetResponseStream()
        Using sr As New StreamReader(streamResponse)
            While sr.Peek() >= 0
                Try
                    Dim str As String() = sr.ReadLine.Split("|")
                    _Lista.Agregar(New Elemento(New Guid(str(0)), str(1), CType(str(2), Tipo)))

                Catch ex As Exception
                    Debug.Print(ex.Message)

                End Try
            End While
        End Using
    End Sub
    Private Sub Funciones(ByVal Elm As Elemento)
        Try
            If Elm.TipoElemento = Tipo.Estructura_Datos Then
                Dim myWebRequest As WebRequest = WebRequest.Create(_RutaSQL & Elm.Path)
                Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
                Dim streamResponse As Stream = myWebResponse.GetResponseStream()

                Using sr As New StreamReader(streamResponse)
                    Dim str As String = sr.ReadToEnd
                    Dim BD As New ZctSQL.ZctDB
                    BD.GetCadenaConexion = _Cnn
                    BD.Conectar()
                    Dim sql_act As String = ""
                    Try

                        BD.ComenzarTransaccion()
                        For Each Cmd As String In str.Split("####")
                            'ZctSQL.GenEjecuta(_Cnn, sr.ReadToEnd)
                            sql_act = Cmd
                            
                            If Not String.IsNullOrEmpty(Cmd) Then ZctSQL.GenEjecuta(_Cnn, Cmd, BD)
                        Next
                        BD.ConfirmarTransaccion()

                    Catch ex As Exception
                        BD.CancelarTransaccion()
                        Console.WriteLine(Elm.IdDesc.ToString())
                        Console.WriteLine(sql_act)
                        Console.WriteLine(ex.Message)
                        Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al ejecutar el comando.", ex)
                    Finally
                        BD.Desconectar()
                        BD.Dispose()
                    End Try
                End Using
                ZctSQL.GenGraba(Of Elemento)(_Cnn, Elm)
            Else
                'Copia el archivo
                Dim myWebRequest As WebRequest = WebRequest.Create(_RutaRPT & Elm.Path)
                Dim myWebResponse As WebResponse = myWebRequest.GetResponse()
                Dim streamResponse As Stream = myWebResponse.GetResponseStream()



                Using localStream As Stream = File.Create(_RutaLOCALRPT & Elm.Path)



                    Dim buffer(1024) As Byte
                    Dim bytesRead As Integer

                    ' Simple do/while loop to read from stream until
                    'no bytes are returned
                    Do
                        '// Read data (up to 1k) from the stream
                        bytesRead = streamResponse.Read(buffer, 0, buffer.Length)
                        '// Write the data to the local file
                        localStream.Write(buffer, 0, bytesRead)

                    Loop While (bytesRead > 0)
                    ''// Increment total bytes processed
                    'bytesProcessed += bytesRead
                    'End While while (bytesRead > 0);

                End Using
                streamResponse.Close()
                streamResponse.Dispose()


            End If

        Catch ex As Exception
            Debug.Print(ex.Message)
            _ListaErrores.Add(Elm)
        End Try
    End Sub
    Private Sub GetArchivo(ByVal _rutaDownload)

    End Sub
End Class

Friend Class ListElementos
    Inherits List(Of Elemento)
    Private _Cnn As String
    Public Property Cnn() As String
        Get
            Return _Cnn
        End Get
        Set(ByVal value As String)
            _Cnn = value
        End Set
    End Property

    Public Sub New(ByVal cnn As String)
        _Cnn = cnn
    End Sub
    Public Sub Agregar(ByVal MiElemento As Elemento)
        Dim Elemento As New BuscaElemento(MiElemento.IdDesc.ToString)
        ZctSQL.GenDatos(Of BuscaElemento)(_Cnn, Elemento)

        If Not Elemento.Existe Then
            Me.Add(MiElemento)
        End If

    End Sub


    Private Class BuscaElemento
        Implements ZctSQL.IZctDatos

        Private _Activar As String

        Public Property Activar() As String
            Get
                Return _Activar
            End Get
            Set(ByVal value As String)
                _Activar = value
            End Set
        End Property

        Private _Resultado As Boolean
        Public ReadOnly Property Existe() As Boolean
            Get
                Return _Resultado
            End Get
        End Property


        Public Sub New(ByVal Activar As String)
            _Activar = Activar

        End Sub

        
        Public Sub GetMe(ByVal DataReader As System.Data.SqlClient.SqlDataReader) Implements ZctSQL.IZctDatos.GetMe
            Using DataReader
                If DataReader.HasRows Then
                    _Resultado = True
                Else
                    _Resultado = False
                End If
            End Using
        End Sub

        Public ReadOnly Property SQLElimina() As String Implements ZctSQL.IZctDatos.SQLElimina
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property SQLGraba() As String Implements ZctSQL.IZctDatos.SQLGraba
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property SQLObtiene() As String Implements ZctSQL.IZctDatos.SQLObtiene
            Get
                Return String.Format("SELECT IDActiva FROM ZctActiva WHERE  IDActiva = '{0}'", _Activar)
            End Get
        End Property
    End Class
End Class

