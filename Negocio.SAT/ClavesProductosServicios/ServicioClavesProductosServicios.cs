﻿using Modelo.SAT.Repositorio;
using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.SAT.ClavesProductosServicios
{
    public class ServicioClavesProductosServicios : IServicioClavesProductosServicios
    {
        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });

        IRepositorioClavesProductosServicios RepositorioClavesProductosServicios
        {
            get { return _repositorioClavesProductosServicios.Value; }
        }

        Lazy<IRepositorioClavesProductosServicios> _repositorioClavesProductosServicios = new Lazy<IRepositorioClavesProductosServicios>(() => { return FabricaDependencias.Instancia.Resolver<IRepositorioClavesProductosServicios>(); });

    }
}
