﻿using Modelo.Sistemas.Constantes;
using Modelo.Sistemas.Entidades.Dtos;
using Negocio.Rastrilleo;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace ServiciosTCP
{

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class ServicioSincronizacion : IServicioSincronizacion
    {
        private static readonly object bloqueador = new object();
        private static bool sincronizando = false;

        [Obsolete]
        public void SubirInformacion() 
        {
            SubirInformacionConBandera();
            /*
            lock (bloqueador)
            {
                if (sincronizando)
                    return;

                sincronizando = true;
            }

            try
            {
                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de tipos de habitación");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.SincronizarTiposHabitacion();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de tipos de habitación");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de tipos de habitación");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de tipos de habitación");
                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de tipos de habitación");
                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de ventas");

                    var servicioRastrilleo = FabricaDependencias.Instancia.Resolver<IServicioRastrilleo>();

                    servicioRastrilleo.Sincronizar();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de ventas");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de ventas");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de ventas");
                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de ventas");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de cortes");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.Sincronizar();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de cortes");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de cortes");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de cortes");

                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de cortes");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de gastos");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.SincronizarGastos();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de gastos");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de gastos");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de gastos");

                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de gastos");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de fajillas");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.SincronizarEfectivo();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de fajillas");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de fajillas");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de fajillas");

                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de fajillas");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }
            }
            finally
            {
                lock (bloqueador)
                {
                    sincronizando = false;
                }
            }
            */
        }

        /// <summary>
        /// Retorna true si el sistema detecta un proceso de sincronización ya ejecutándose, en caso contrario retorna false
        /// </summary>
        /// <returns></returns>
        public bool SubirInformacionConBandera()
        {
            lock (bloqueador)
            {
                if (sincronizando)
                    return true;

                sincronizando = true;
            }

            try
            {
                try
                {
                    var parametros = FabricaDependencias.Instancia.Resolver<Negocio.Sistemas.ConfiguracionesSistemas.IServicioConfiguracionesSistemas>().ObtenerParametros(IdentificadoresSistemas.CONFIGURADOR_SOT);

                    var parametrosConfig = new ParametrosConfigurador();

                    foreach (var propiedad in typeof(ParametrosConfigurador).GetProperties())
                    {
                        var parametro = parametros.FirstOrDefault(m => m.Nombre == propiedad.Name);

                        if (parametro != null)
                        {
                            propiedad.SetValue(parametrosConfig, Newtonsoft.Json.JsonConvert.DeserializeObject(parametro.Valor, propiedad.PropertyType));
                        }
                    }

                    if (!parametrosConfig.ConfiguracionFinalizada)
                        throw new SOTException("La configuración de la sucursal no ha finalizado");
                }
                catch(Exception e)
                {
                    Transversal.Log.Logger.Error($"[{ DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}] Error al leer la configuración: {e.Message}");
                    throw e;
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de tipos de habitación");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.SincronizarTiposHabitacion();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de tipos de habitación");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de tipos de habitación");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de tipos de habitación");
                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de tipos de habitación");
                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de ventas");

                    var servicioRastrilleo = FabricaDependencias.Instancia.Resolver<IServicioRastrilleo>();

                    servicioRastrilleo.Sincronizar();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de ventas");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de ventas");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de ventas");
                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de ventas");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de cortes");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.Sincronizar();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de cortes");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de cortes");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de cortes");

                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de cortes");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de gastos");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.SincronizarGastos();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de gastos");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de gastos");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de gastos");

                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de gastos");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }

                try
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Inicio de sincronización de fajillas");

                    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<Negocio.Sincronizacion.IServicioSincronizacion>();

                    servicioSincronizacion.SincronizarEfectivo();

                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Fin de sincronización de fajillas");
                }
                catch (DbEntityValidationException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de fajillas");

                    foreach (var er in ex.EntityValidationErrors)
                        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                catch (SOTException ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de fajillas");

                    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
                }
                catch (Exception ex)
                {
                    Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error de sincronización de fajillas");

                    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                    }
                }
            }
            finally
            {
                lock (bloqueador)
                {
                    sincronizando = false;
                }
            }

            return false;
        }
    }
}
