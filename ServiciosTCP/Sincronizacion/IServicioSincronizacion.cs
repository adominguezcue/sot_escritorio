﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiciosTCP
{
    [ServiceContract]
    public interface IServicioSincronizacion
    {
        [OperationContract]
        [Obsolete("Usar SubirInformacionConBandera")]
        void SubirInformacion();
        /// <summary>
        /// Retorna true si el sistema detecta un proceso de sincronización ya ejecutándose, en caso contrario retorna false
        /// </summary>
        /// <returns></returns>
        bool SubirInformacionConBandera();
    }
}
