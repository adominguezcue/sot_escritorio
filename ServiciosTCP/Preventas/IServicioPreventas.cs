﻿using Modelo.Entidades;
using ServiciosTCP.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Transversal.Excepciones;

namespace ServiciosTCP.Preventas
{
    [ServiceContract]
    public interface IServicioPreventas
    {
        //[FaultContract(typeof(SOTException))]
        [FaultContract(typeof(DatosError))]
        [OperationContract]
        int GrenerarPreventa(Venta.ClasificacionesVenta clasificacionVenta);
    }
}
