﻿using Modelo.Entidades;
using ServiciosTCP.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace ServiciosTCP.Preventas
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.Single)]
    public class ServicioPreventas : IServicioPreventas
    {
        public int GrenerarPreventa(Venta.ClasificacionesVenta clasificacionVenta)
        {
            try
            {
                var servicioPreventas = FabricaDependencias.Instancia.Resolver<Negocio.Preventas.IServicioPreventas>();

                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                {
                    int id = servicioPreventas.GrenerarPreventaExt(clasificacionVenta);

                    scope.Complete();

                    return id;
                }
            }
            catch (SOTException ex)
            {
                throw new FaultException<DatosError>(new DatosError
                {
                    Controlado = true,
                    Error = ex.Message
                });
            }
            catch (Exception ex)
            {
                throw new FaultException<DatosError>(new DatosError
                {
                    Error = ex.Message
                });
            }
        }
    }
}
