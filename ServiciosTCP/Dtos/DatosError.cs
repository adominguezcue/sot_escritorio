﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ServiciosTCP.Dtos
{
    [DataContract]
    public class DatosError
    {
        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public bool Controlado { get; set; }
    }
}
