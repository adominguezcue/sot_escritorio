﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using NUnit.Framework;
using ResTotal;


namespace Test_ResTotal
{
    [TestFixture]
    public class TestFacturacion
    {
        string Cnn;
        string folio_facturacion;
        
         ResTotal.Controlador.ControladorFacturacion controlador;
        [SetUp]
        public void setup()
        {
            
            Cnn = "Data Source=.\\sqlexpress; Initial Catalog=SOTRestotal;Connect Timeout=30;Persist Security Info=True ; User ID=Arturo;Password=L0k0m0t0r4";
            controlador = new ResTotal.Controlador.ControladorFacturacion(Cnn);
            controlador.timbra = new FacturacionElectronica.Procesa.TimbrarCFDPrueba(TimbrarCFDPrueba);
            folio_facturacion = "FC";
        }
        private string[] TimbrarCFDPrueba(string usuario, string password , string xml ) {
            Console.WriteLine(xml);
            string[] respuesta = {"","","si", xml};
            return respuesta;
        }

        [Test]
        public void test_01_objeto_factura() {
            ResTotal.Modelo.ZctFacturaEnc factura = new ResTotal.Modelo.ZctFacturaEnc();
            Assert.IsNotNull(factura);
                    
        }
        [Test]
        public void test_02_objeto_detalle_factura()
        {
            ResTotal.Modelo.ZctFacturaDet factura_detalle = new ResTotal.Modelo.ZctFacturaDet();
            Assert.IsNotNull(factura_detalle);

        }
        [Test]
        public void test_03_objeto_comprobante_factura()
        {
            ResTotal.Modelo.ZctFacturaComprobante factura_comprobante = new ResTotal.Modelo.ZctFacturaComprobante();
            Assert.IsNotNull(factura_comprobante);

        }

        [Test]
        public void test_04_obtiene_objeto_factura_ot()       
       {
           ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(100, folio_facturacion);
        Assert.IsNotNull(factura_encabezado);
        }

        [Test]
        public void test_05_obtiene_objeto_factura_encabezado() {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(100, folio_facturacion);
            Assert.AreEqual(factura_encabezado.cod_cte, 11099);
            Assert.AreEqual(factura_encabezado.importe_factura, 119.0044M);
            Assert.AreEqual(factura_encabezado.iva_factura, 16.4144M);
            Assert.Greater(factura_encabezado.folio_factura, 0);
            //R=> Realizado
            //=> Sin grabar
            //C=> Cancelado
            Assert.AreEqual(factura_encabezado.status_factura.ToString(), string.Empty);

        }

        [Test]
        public void test_06_obtiene_detalle_factura_ot() {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(100, folio_facturacion);
            Assert.Greater(factura_encabezado.ZctFacturaDet.Count(), 0);

        }

        [Test]
        public void test_07_obtiene_detalle_factura_ot()
        {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(100, folio_facturacion);
            ResTotal.Modelo.ZctFacturaDet factura_detalle = factura_encabezado.ZctFacturaDet.First();
            Assert.AreEqual(factura_detalle.cod_art, "01-0106");
            Assert.AreEqual(factura_detalle.ZctCatArt.Desc_Art, "BALATA (1 JUEGO-CAJA) TRAS HONDA CARGO CG125-|-980-703102");
            Assert.AreEqual(factura_detalle.articulo, "BALATA (1 JUEGO-CAJA) TRAS HONDA CARGO CG125-|-980-703102");
            Assert.AreEqual(factura_detalle.Cod_DetOT, 116068);
            Assert.AreEqual(factura_detalle.ctd_art, 2);
            Assert.AreEqual(factura_detalle.subtotal, 30.550 * 2);
            Assert.AreEqual(factura_detalle.IVA, (30.550 * 2) * 0.16);
            Assert.AreEqual(factura_detalle.Total, Math.Round( (30.550 * 2) * 1.16, 3));
        }

        [Test]
        public void test_07_graba_elimina_factura() {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(100, folio_facturacion);
            controlador.inserta_objeto_factura(factura_encabezado);
            //controlador.elimina_factura(factura_encabezado);
            Assert.IsNotNull(factura_encabezado.folio_factura);
        }

        [Test]
        public void test_08_obtiene_factura() {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            Assert.IsNotNull(factura_encabezado.folio_factura);
        }

        [Test]
        public void test_09_elimina_factura()
        {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(5, folio_facturacion);
            controlador.cancela_factura(factura_encabezado);
            Assert.IsNotNull(factura_encabezado.folio_factura);
            Assert.AreEqual('C',factura_encabezado.status_factura);
        }

        [Test]
        public void test_10_obtiene_cfdi_factura()
        {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            FacturacionElectronica.cFacturaCFDI  cfdi =  controlador.obtiene_objeto_factura_cfdi(factura_encabezado);
            Assert.IsNotNull(cfdi);
        }
        [Test]
        public void test_10_obtiene_encabezado_cfdi_factura()
        {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            FacturacionElectronica.cFacturaCFDI cfdi = controlador.obtiene_objeto_factura_cfdi(factura_encabezado);
            Assert.IsNotNull(cfdi.fecha);
            Assert.IsNotNull(cfdi.FechaFolioFiscalOrig);
            Assert.IsNotNull(cfdi.folio);
            Assert.IsNotNull(cfdi.formaDePago);
            Assert.IsNotNull(cfdi.MontoFolioFiscalOrig);
            Assert.IsNotNull(cfdi.noCertificado);
            //Assert.IsNotNull(cfdi.NumCtaPago);
            //Assert.IsNotNull(cfdi.sello);
            Assert.IsNotNull(cfdi.Serie);
            //Assert.IsNotNull(cfdi.SerieFolioFiscalOrig);
            Assert.IsNotNull(cfdi.subTotal);
            Assert.IsNotNull(cfdi.tipoDeComprobante);
            Assert.IsNotNull(cfdi.total);
            Assert.IsNotNull(cfdi.Version);
        }

        [Test]
        public void test_11_obtiene_emisor_cfdi_factura()
        {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            FacturacionElectronica.cFacturaCFDI cfdi = controlador.obtiene_objeto_factura_cfdi(factura_encabezado);
            Assert.IsNotNull(cfdi.Emisor.nombre);
            Assert.IsNotNull(cfdi.Emisor.RFC );
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.Calle);
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.CodigoPostal);
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.colonia);
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.estado);
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.localidad);
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.municipio);
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.noexterior);
            Assert.IsNotNull(cfdi.Emisor.DomicilioFiscal.pais);
            Assert.IsNotNull(cfdi.Emisor.RegimenFiscal.Regimen);
            Assert.IsNotNull(cfdi.Emisor.ExpedidoEn.Calle);
            Assert.IsNotNull(cfdi.Emisor.ExpedidoEn.CodigoPostal);
            Assert.IsNotNull(cfdi.Emisor.ExpedidoEn.colonia);
            Assert.IsNotNull(cfdi.Emisor.ExpedidoEn.estado);
            Assert.IsNotNull(cfdi.Emisor.ExpedidoEn.municipio);
            Assert.IsNotNull(cfdi.LugarExpedicion);
            Assert.IsNotNull(cfdi.Emisor.ExpedidoEn.noexterior);
         
        }

        [Test]
        public void test_12_obtiene_receptor_cfdi_factura() {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            FacturacionElectronica.cFacturaCFDI cfdi = controlador.obtiene_objeto_factura_cfdi(factura_encabezado);
            
            Assert.IsNotNull(cfdi.Receptor.RFC);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.Calle);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.CodigoPostal);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.colonia);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.estado);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.municipio);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.noexterior);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.nointerior);
            Assert.IsNotNull(cfdi.Receptor.Domicilio.pais);
            Assert.IsNotNull(cfdi.Receptor.nombre);
        }


        [Test]
        public void test_13_obtiene_detalle_cfdi_factura()
        {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            FacturacionElectronica.cFacturaCFDI cfdi = controlador.obtiene_objeto_factura_cfdi(factura_encabezado);
            Assert.Greater(cfdi.Conceptos.Count, 0);
            foreach (FacturacionElectronica.Concepto cp1 in cfdi.Conceptos) { 
                Assert.IsNotNull(cp1.cantidad);
                Assert.IsNotNull(cp1.descripcion);
                Assert.IsNotNull(cp1.unidad);
                Assert.IsNotNull(cp1.importe);
                Assert.IsNotNull(cp1.valorUnitario);
                
            }
        }

        [Test]
        public void test_14_impuestos() {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            FacturacionElectronica.cFacturaCFDI cfdi = controlador.obtiene_objeto_factura_cfdi(factura_encabezado);
            Assert.IsNotNull(cfdi.Impuestos.Traslados);
            Assert.Greater(cfdi.Impuestos.Traslados.Count, 0);
            foreach (FacturacionElectronica.Traslado tl in cfdi.Impuestos.Traslados) {
                Assert.IsNotNull(tl.Importe);
                Assert.IsNotNull(tl.Impuesto);
                Assert.IsNotNull(tl.Tasa);
            }

        }

        [Test]
        public void test_15_genera_xml() {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_factura(3, folio_facturacion);
            
            string xml = controlador.firma_factura(factura_encabezado);
        }

        [Test]
        //[ExpectedException(typeof(ArgumentException),ExpectedMessage = "El RFC del cliente no ha sido proporcionado")]
        public void test_16_graba_rfc_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.RFC_Cte = string.Empty;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El RFC del cliente no ha sido proporcionado"));
        }

        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "La calle del cliente no ha sido proporcionada")]
        public void test_17_graba_calle_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.calle = string.Empty;

                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("La calle del cliente no ha sido proporcionada"));
        }

        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "La colonia del cliente no ha sido proporcionada")]
        public void test_18_graba_colonia_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.colonia = string.Empty;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("La colonia del cliente no ha sido proporcionada"));
        }

        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El número exterior del cliente no ha sido proporcionada")]
        public void test_18_graba_no_exterior_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.numext = string.Empty;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El número exterior del cliente no ha sido proporcionada"));
        }


        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "La ciudad del cliente no ha sido proporcionada")]
        public void test_18_graba_ciudad_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.ZctCatCiudad.Desc_Ciu = string.Empty;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("La ciudad del cliente no ha sido proporcionada"));
        }

        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El estado del cliente no ha sido proporcionada")]
        public void test_18_graba_estado_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.ZctCatEstado.Desc_Edo = string.Empty;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El estado del cliente no ha sido proporcionada"));
        }


        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El pais del cliente no ha sido proporcionada")]
        public void test_18_graba_pais_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.pais = string.Empty;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El pais del cliente no ha sido proporcionada"));
        }


        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El CP del cliente no ha sido proporcionada")]
        public void test_18_graba_cp_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctCatCte.cp = "0";
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El CP del cliente no ha sido proporcionada"));
        }


        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El tipo de folio de la factura no ha sido proporcionada")]
        public void test_18_graba_tipo_folio_factura_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.cod_folio = string.Empty;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El tipo de folio de la factura no ha sido proporcionada"));
        }

        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El folio de la factura no ha sido proporcionada")]
        public void test_18_graba_folio_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.folio_factura = 0;
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El folio de la factura no ha sido proporcionada"));
        }


        [Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El detalle de la factura no ha sido proporcionada")]
        public void test_18_graba_detalle_marca_error_factura()
        {
            var ex = Assert.Throws<ArgumentException>(delegate
            {
                ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
                factura_encabezado.ZctFacturaDet.Clear();
                controlador.inserta_objeto_factura(factura_encabezado);
            });

            Assert.That<string>(ex.Message, Is.EqualTo("El detalle de la factura no ha sido proporcionada"));
        }

        [Test]
        
        public void test_19_obtiene_factura_nueva()
        {
            ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.nueva_factura(folio_facturacion);
            Assert.IsNotNull(factura_encabezado);

        }


        //[Test]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El detalle de la factura debe de poporcionar un artículo válido")]
        //public void test_18_graba_detalle_articulo_marca_error_factura()
        //{
        //    ResTotal.Modelo.ZctFacturaEnc factura_encabezado = controlador.obtiene_objeto_factura(101, folio_facturacion);
        //    foreach (ResTotal.Modelo.ZctFacturaDet det  in factura_encabezado.ZctFacturaDet ){
        //        det.cod_art ) 
        //    }
                
        //    controlador.graba_objeto_factura(factura_encabezado);

        //}




        //TODO: lanzar validación donde si no se proporciona el folio de facturación, marque error
        //TODO: probar cuando una orden ya tiene un folio de factura asignado


        
    }
}
