﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ResTotal;
using System.Windows.Forms; 
namespace Test_ResTotal
{
    [TestFixture]
    public class TestRestotal
    {
        string Cnn;
        ResTotal.Controlador.Controladorcaja controladorCaja;
       [SetUp]
        public void setup()
        {
            Cnn = "Data Source=.\\sqlexpress; Initial Catalog=SOTRestotal;Connect Timeout=30;Persist Security Info=True ; User ID=Arturo;Password=L0k0m0t0r4";
            controladorCaja = new ResTotal.Controlador.Controladorcaja(Cnn,"CJ",1);
          
        }
        [Test]
        public void test01_instancia()
        {
          
            Assert.IsNotNull(controladorCaja );
        }
        #region "Caja"
        [Test()]
        //[ExpectedException(typeof(ArgumentException), ExpectedMessage = "El folio tiene que ser un número.")]
        public void Test02UnOTfolioDebeSerEnteroMayorAcero()
        {
            int folio = -2;
            controladorCaja.ValidaFolioOT(folio.ToString());
        }
        [Test]
        public void Test03ElCodigoFolioCajaDebeExistir()
        {
            controladorCaja.ObtieneFolioCaja("S");
        }
        [Test]
        public void Test21UnaOTtieneSoloUnaCaja()
        {
            int Folio = 1;
            controladorCaja.ValidaCaja(Folio);
        }
        [Test]
        public void Test04DebeExistirUnaOTconFolio()
        {
            int folio = -2;
            controladorCaja.ValidaFolioOT(folio.ToString());
        }
        [Test]
        public void Test05DebeElistirDetalleOT()
        {
            int folio = -2;
            controladorCaja.BuscaDetalleOT(folio);
        }
        [Test]
        public void Test06AsignaFolioACaja()
        {
            ResTotal.Modelo.ZctEncCaja Caja = new ResTotal.Modelo.ZctEncCaja();
            controladorCaja.AsignaFolioCaja(Caja);
        }
        [Test ]
        public void Test07ElDetalleNoDebeEstarVacio()
        {
           //List< ResTotal.Modelo.ZctDetOT> det=new List<ResTotal.Modelo.ZctDetOT >();
            //controladorCaja.CreaDetalleCaja(0, det, "s");
        }
        //[Test ]
        //public void Test08ValidaUsuario()
        //{
        //    int CodUsuario=16;
        //  controladorCaja.ValidaUsuario(CodUsuario);
        //}
        [Test ]
        public void Test09TransformaObjetocaja()
        {
            string Folio = "1";
            controladorCaja.TranformaObjetoCaja(Folio, "CJ", 1, DateTime.Now);
        }
        [Test]
        public void Test10ElDetalleCajaTransformadaNoDebeSerNulo()
        {
            string Folio = "1";
           ResTotal.Modelo.ZctEncCaja CajaTransformada= controladorCaja.TranformaObjetoCaja(Folio, "CJ", 1, DateTime.Now);
           //Assert.IsNotNull(CajaTransformada.ZctDetCaja);
        }
        public void Test11ElDetalleCajaTransformadaNoDebeEstarVacio()
        {
             string Folio = "1";
           ResTotal.Modelo.ZctEncCaja CajaTransformada= controladorCaja.TranformaObjetoCaja(Folio, "CJ", 1, DateTime.Now);
           //Assert.Greater(CajaTransformada.ZctDetCaja.Count,0);
        }
        public void Test12ElStausCajaDebeSerAplicado()
        {
            string Folio = "1";
            ResTotal.Modelo.ZctEncCaja CajaTransformada = controladorCaja.TranformaObjetoCaja(Folio, "CJ", 1, DateTime.Now);
            Assert.True(CajaTransformada.staus == "APLICADO");
        }

        #endregion
#region Pagos
        public void Test13ElTipoDePagoDebeExistir()
        {
           int Folio = 1;
            ResTotal.Modelo.ZctEncCaja caja=new ResTotal.Modelo.ZctEncCaja ();

            controladorCaja.CreaPagoCaja(caja, Folio , 123);
            
        }
        [Test]
        public void Test14ElTipoDePagoDebeExistir()
        {
            int Folio = 1;
            ResTotal.Modelo.ZctEncCaja caja = new ResTotal.Modelo.ZctEncCaja();

            controladorCaja.CreaPagoCaja(caja, Folio, 123);

        }
        [Test]
        public void Test15ElPagoDebeSerMenorAlTotalDeCaja()
        {
            int Folio = 1;
            ResTotal.Modelo.ZctEncCaja caja = new ResTotal.Modelo.ZctEncCaja();

            controladorCaja.CreaPagoCaja(caja, Folio, 123000);

        }
        #region Recibos
        [Test]
        public void Test16ElFolioReciboNoDebeEstarVacio()
        {
            controladorCaja.ValidaFolioRecibo("",1);

        }
        [Test]
        public void Test17ElFolioReciboNoDebeEstarVacio()
        {
            controladorCaja.ValidaFolioRecibo("", 1);

        }
        [Test]
        public void Test17ElFolioReciboNoDebeExistir()
        {
            controladorCaja.ValidaFolioRecibo("a", 1);

        }
        [Test]
        public void Test18ElFolioReciboNoDebeExistir()
        {
            controladorCaja.BuscaRecibo("1",1);

        }
        [Test]
        public void Test19ElReciboDebeSerCancelado()
        {
          string status=  controladorCaja.CancelaRecibo("1", 1, 1);
          Assert.True("CANCELADO" == status);
        }
        #endregion
        [Test]
        public void TEST()
        {
            controladorCaja.TranformaObjetoCaja("2", "CJ", 1, DateTime.Now);

        }
#endregion
        [Test]
        public void Test20PruebaTranformacion()
        {
            controladorCaja.TranformaObjetoCaja("1", "CJ", 1, DateTime.Now);
        }
        [Test]
        public void Test21PruebaGuardado()
        {
         var Caja=controladorCaja.TranformaObjetoCaja("1", "CJ", 1, DateTime.Now);
         controladorCaja.GrabaCaja(Caja,12);          
        }

    }
}
