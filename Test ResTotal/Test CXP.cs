﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ResTotal;

namespace Test_ResTotal
{
    class Test_CXP
    {
        string Cnn;
        int OC; 
        ResTotal.Controlador.ControladorCxP controlador;
        ResTotal.Modelo.ZctEncCuentasPagar CxP;
        [SetUp]
        public void setup()
        {
            Cnn = "Data Source=ALSADILAP\\MSSQLSERVER2012; Initial Catalog=ZctSotRestotal;Connect Timeout=30;Persist Security Info=True ; User ID=Arturo;Password=L0k0m0t0r4";
            controlador = new ResTotal.Controlador.ControladorCxP(Cnn);
            OC = 1;
        }
        [Test]
        public void Test_00ObjetoCXP()
        {
            CxP=new ResTotal.Modelo.ZctEncCuentasPagar ();
            Assert.IsNotNull(CxP);
        }
        [Test]
        public void Test_01ValidarOC()
        {
            controlador.ValidaOC(OC);
        }
        [Test]
        public void Test_02LlenarObjetoCxP()
        {
            Console.WriteLine("//enc CXP");
            Console.WriteLine(CxP.fecha_vencido);
            controlador.CreaCuentaPagar(OC);
            Console.WriteLine(CxP.total );
            Console.WriteLine(CxP.iva);
            Console.WriteLine(CxP.fecha_creacion);
            Console.WriteLine(CxP.fecha_vencido);
            Console.WriteLine("//");
        }
        [Test]
        public void Test03ValidaCxP()
        {
            controlador.ValidaCxP(1);
        }
        [Test]
        public void Test_04CrearPago()
        {
            controlador.CreaPago(CxP, "pago prueba", 1, Convert.ToDecimal( 10.50), 0, DateTime.Now, DateTime.Now);
        }
        [Test]
        public void Test_05Grabar()
        {
            int _OC = 1;
            ResTotal.Modelo.ZctEncCuentasPagar Cuenta= controlador.CreaCuentaPagar(_OC);
            ResTotal.Modelo.ZctDetCuentasPagar Detalle = controlador.CreaPago(Cuenta, "Test" + _OC.ToString(), 1, 10, 0, DateTime.Now, DateTime.Now);
            controlador.Graba(Cuenta, Detalle);
        }
    }
}
