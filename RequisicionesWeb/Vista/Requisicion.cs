﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Transversal.Excepciones;

namespace RequisicionesWeb.Vista
{
    public delegate int CreaCompra(int Cod_EncOT);

    public delegate int CreaSalida(int Cod_OC);

    public partial class Requisicion : Form
    {
        
        
        public event  CreaSalida lanza_salida;
        public event CreaCompra lanza_compra;

        List<Modelo.ZctRequisition> list;
        Controlador.Controlador controlador;
        public Requisicion(string conn)
        {
            InitializeComponent();
            controlador = new Controlador.Controlador(conn);
            prepara_para_iniciar();
        }

        
        private void Requisicion_Load(object sender, EventArgs e)
        {
            CargaRequisiciones();
            this.CboStatus.Enabled = true; 
        }

        private void CargaRequisiciones(){
            list = controlador.ObtieneElementos();
            GridRequisition.AutoGenerateColumns = false;
            GridRequisition.DataSource = list;
            CboStatus.Items.Clear();
            CboStatus.Items.Add("Todos");
            CboStatus.Text = "Todos";
            foreach (Modelo.ZctRequisition R in list)
            {
                //MessageBox.Show(this, R.estatus);

                if (CboStatus.Items.IndexOf(R.estatus) == -1)
                {
                    CboStatus.Items.Add(R.estatus);
                }
            }
          
        }

        private void cmdGeneraCompra_Click(object sender, EventArgs e)
        {
             try
            {
                if (requisicion_actual != null)
                {
                    int orden_de_compra = 0;
                    if (requisicion_actual.Cod_OC > 0)
                        orden_de_compra = requisicion_actual.Cod_OC.GetValueOrDefault();
                    else
                        orden_de_compra = controlador.genera_orden_compra(requisicion_actual);
                    lanza_compra(orden_de_compra);
                }
             }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }

        }

        private void prepara_para_iniciar(){
        }


        Modelo.ZctRequisition requisicion_actual = null;
        
        private void GridRequisition_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
           //renglon_actual = e.RowIndex;
           
            requisicion_actual = list[e.RowIndex];
            
            GridRequisitionDetail.AutoGenerateColumns = false;
            GridRequisitionDetail.DataSource = requisicion_actual.ZctRequisitionDetail;

            if (requisicion_actual.estatus == "Rechazado")
            {
                button1.Enabled = false ; 
                cmdGeneraCompra.Enabled=false;
                cmdGeneraOrden.Enabled = false; 
            }
            else
            {
                button1.Enabled = true ;
                cmdGeneraCompra.Enabled = true ;
                cmdGeneraOrden.Enabled = true ; 
            }

            if (requisicion_actual.Cod_OC.GetValueOrDefault() > 0)
            {
                cmdGeneraCompra.Text ="Consulta Compra";
            }
            else {
                cmdGeneraCompra.Text = "Genera Compra";
            }

            if (requisicion_actual.Cod_EncOT.GetValueOrDefault() > 0)
            {
                cmdGeneraOrden.Text = "Consulta Salida";
            }
            else
            {
                cmdGeneraOrden.Text = "Genera Salida";
            }

        }

        private void cmdGeneraOrden_Click(object sender, EventArgs e)
        {
            try
            {
                if (requisicion_actual != null)
                {

                    int orden_de_trabajo = 0;
                    if (requisicion_actual.Cod_EncOT > 0)
                        orden_de_trabajo = requisicion_actual.Cod_EncOT.GetValueOrDefault();
                    else
                        orden_de_trabajo = controlador.genera_orden_trabajo(requisicion_actual);

                    lanza_salida(orden_de_trabajo);
                   
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void CboStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.CboStatus.Enabled ==false)
            {
                return;
            }
            list = controlador.ObtieneElementos(this.CboStatus.Text );
            GridRequisition.AutoGenerateColumns = false;
            GridRequisition.DataSource = list;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (requisicion_actual == null)
                    throw new SOTException("Seleccione una requisición");

                requisicion_actual.estatus = "Rechazado";
                controlador.grabar();
                CargaRequisiciones();
                button1.Enabled = false;
                cmdGeneraCompra.Enabled = false;
                cmdGeneraOrden.Enabled = false;
                MessageBox.Show(this, "Requisicion Rechazada", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GridRequisition_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }       
    }
}
