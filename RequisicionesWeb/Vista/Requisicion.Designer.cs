﻿namespace RequisicionesWeb.Vista
{
    partial class Requisicion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GridRequisitionDetail = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Articulo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdGeneraCompra = new System.Windows.Forms.Button();
            this.cmdGeneraOrden = new System.Windows.Forms.Button();
            this.GridRequisition = new System.Windows.Forms.DataGridView();
            this.CboStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Folio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tienda_base = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Almacen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalGeneral = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Compra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Observaciones = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridRequisitionDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridRequisition)).BeginInit();
            this.SuspendLayout();
            // 
            // GridRequisitionDetail
            // 
            this.GridRequisitionDetail.AllowUserToAddRows = false;
            this.GridRequisitionDetail.AllowUserToDeleteRows = false;
            this.GridRequisitionDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridRequisitionDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridRequisitionDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Articulo,
            this.Cantidad,
            this.Precio,
            this.Total});
            this.GridRequisitionDetail.Location = new System.Drawing.Point(12, 224);
            this.GridRequisitionDetail.Name = "GridRequisitionDetail";
            this.GridRequisitionDetail.ReadOnly = true;
            this.GridRequisitionDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridRequisitionDetail.Size = new System.Drawing.Size(1130, 188);
            this.GridRequisitionDetail.TabIndex = 1;
            // 
            // Codigo
            // 
            this.Codigo.DataPropertyName = "cod_art";
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            // 
            // Articulo
            // 
            this.Articulo.DataPropertyName = "articulo";
            this.Articulo.HeaderText = "Artículo";
            this.Articulo.Name = "Articulo";
            this.Articulo.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.DataPropertyName = "amount";
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // Precio
            // 
            this.Precio.DataPropertyName = "price";
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.Precio.DefaultCellStyle = dataGridViewCellStyle1;
            this.Precio.HeaderText = "Costo promedio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "total";
            dataGridViewCellStyle2.Format = "C2";
            dataGridViewCellStyle2.NullValue = null;
            this.Total.DefaultCellStyle = dataGridViewCellStyle2;
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 208);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "DETALLES";
            // 
            // cmdGeneraCompra
            // 
            this.cmdGeneraCompra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdGeneraCompra.Location = new System.Drawing.Point(921, 421);
            this.cmdGeneraCompra.Name = "cmdGeneraCompra";
            this.cmdGeneraCompra.Size = new System.Drawing.Size(103, 23);
            this.cmdGeneraCompra.TabIndex = 3;
            this.cmdGeneraCompra.Text = "Generar compra";
            this.cmdGeneraCompra.UseVisualStyleBackColor = true;
            this.cmdGeneraCompra.Click += new System.EventHandler(this.cmdGeneraCompra_Click);
            // 
            // cmdGeneraOrden
            // 
            this.cmdGeneraOrden.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdGeneraOrden.Location = new System.Drawing.Point(1039, 421);
            this.cmdGeneraOrden.Name = "cmdGeneraOrden";
            this.cmdGeneraOrden.Size = new System.Drawing.Size(103, 23);
            this.cmdGeneraOrden.TabIndex = 4;
            this.cmdGeneraOrden.Text = "Generar orden";
            this.cmdGeneraOrden.UseVisualStyleBackColor = true;
            this.cmdGeneraOrden.Click += new System.EventHandler(this.cmdGeneraOrden_Click);
            // 
            // GridRequisition
            // 
            this.GridRequisition.AllowUserToAddRows = false;
            this.GridRequisition.AllowUserToDeleteRows = false;
            this.GridRequisition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridRequisition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridRequisition.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Folio,
            this.tienda_base,
            this.Almacen,
            this.Estatus,
            this.TotalGeneral,
            this.Compra,
            this.Observaciones});
            this.GridRequisition.Location = new System.Drawing.Point(12, 35);
            this.GridRequisition.Name = "GridRequisition";
            this.GridRequisition.ReadOnly = true;
            this.GridRequisition.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridRequisition.Size = new System.Drawing.Size(1130, 161);
            this.GridRequisition.TabIndex = 5;
            this.GridRequisition.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridRequisition_CellContentClick);
            this.GridRequisition.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridRequisition_RowEnter);
            // 
            // CboStatus
            // 
            this.CboStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CboStatus.Enabled = false;
            this.CboStatus.FormattingEnabled = true;
            this.CboStatus.Location = new System.Drawing.Point(71, 8);
            this.CboStatus.Name = "CboStatus";
            this.CboStatus.Size = new System.Drawing.Size(542, 21);
            this.CboStatus.TabIndex = 6;
            this.CboStatus.SelectedIndexChanged += new System.EventHandler(this.CboStatus_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Estatus:";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(797, 421);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Rechazar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Folio
            // 
            this.Folio.DataPropertyName = "folio";
            this.Folio.HeaderText = "Folio";
            this.Folio.Name = "Folio";
            this.Folio.ReadOnly = true;
            // 
            // tienda_base
            // 
            this.tienda_base.DataPropertyName = "Cod_EncOT";
            this.tienda_base.HeaderText = "Entrega";
            this.tienda_base.Name = "tienda_base";
            this.tienda_base.ReadOnly = true;
            // 
            // Almacen
            // 
            this.Almacen.DataPropertyName = "almacen";
            this.Almacen.HeaderText = "Almacén";
            this.Almacen.Name = "Almacen";
            this.Almacen.ReadOnly = true;
            // 
            // Estatus
            // 
            this.Estatus.DataPropertyName = "estatus";
            this.Estatus.HeaderText = "Estatus";
            this.Estatus.Name = "Estatus";
            this.Estatus.ReadOnly = true;
            // 
            // TotalGeneral
            // 
            this.TotalGeneral.DataPropertyName = "total";
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = null;
            this.TotalGeneral.DefaultCellStyle = dataGridViewCellStyle3;
            this.TotalGeneral.HeaderText = "Total";
            this.TotalGeneral.Name = "TotalGeneral";
            this.TotalGeneral.ReadOnly = true;
            // 
            // Compra
            // 
            this.Compra.DataPropertyName = "Cod_OC";
            this.Compra.HeaderText = "Compra";
            this.Compra.Name = "Compra";
            this.Compra.ReadOnly = true;
            // 
            // Observaciones
            // 
            this.Observaciones.DataPropertyName = "observaciones";
            this.Observaciones.HeaderText = "Observaciones";
            this.Observaciones.Name = "Observaciones";
            this.Observaciones.ReadOnly = true;
            // 
            // Requisicion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 453);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.CboStatus);
            this.Controls.Add(this.GridRequisition);
            this.Controls.Add(this.cmdGeneraOrden);
            this.Controls.Add(this.cmdGeneraCompra);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GridRequisitionDetail);
            this.Name = "Requisicion";
            this.Text = "Requisición";
            this.Load += new System.EventHandler(this.Requisicion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridRequisitionDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridRequisition)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridRequisitionDetail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdGeneraCompra;
        private System.Windows.Forms.Button cmdGeneraOrden;
        private System.Windows.Forms.DataGridView GridRequisition;
        private System.Windows.Forms.ComboBox CboStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Articulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Folio;
        private System.Windows.Forms.DataGridViewTextBoxColumn tienda_base;
        private System.Windows.Forms.DataGridViewTextBoxColumn Almacen;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalGeneral;
        private System.Windows.Forms.DataGridViewTextBoxColumn Compra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Observaciones;
    }
}