using System.Linq;
namespace RequisicionesWeb.Modelo
{
    partial class DAODataContext
    {
        
    }
    partial class ZctRequisition {

        
        public  string cliente {
            get {
                return this.ZctCatCte.Nom_Cte;
            }
        }

        public string almacen {
            get {
                return this.ZctCatAlm != null ? this.ZctCatAlm.Desc_CatAlm : "";
            }
        }

        public decimal total {
            get {
                return this.ZctRequisitionDetail.Sum(x => x.total);
            }
        }
       
       
    }


    partial class ZctRequisitionDetail {
        public string articulo {
            get {
                return this.ZctCatArt.Desc_Art;
                }

        }
        public decimal total {
            get {
                return this.price.GetValueOrDefault() * (decimal) this.amount;
            }
        }
    }
}
