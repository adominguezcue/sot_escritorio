﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RequisicionesWeb.Controlador
{
    public class Controlador
    {
        List<Modelo.ZctRequisition> list; 
        Modelo.DAODataContext datos;
        public Controlador(string conn)
        {
            datos = new Modelo.DAODataContext(conn);
        }


        internal List<Modelo.ZctRequisition> ObtieneElementos(string status = "Todos")
        {
            if (status=="Todos")
            {
                list = (from m in datos.ZctRequisition select m).ToList();
            }
            else
            {
                list = (from m in datos.ZctRequisition where m.estatus==status select m).ToList();
            }
            return list;
        }
        internal Modelo.ZctRequisition ObtieneElemento(int folio) {
            Modelo.ZctRequisition elemento = (from m in datos.ZctRequisition where m.folio == folio select m).SingleOrDefault();
            return elemento;
        }

        public int genera_orden_trabajo(Modelo.ZctRequisition requistion) {
            int? orden_compra = 0;
            datos.SP_REQUISITION_OT(requistion.folio, ref orden_compra);
            requistion.Cod_EncOT = orden_compra;
            requistion.estatus = "ORDEN DE TRABAJO GENERADA";
            requistion.update_web = false;

            return orden_compra.GetValueOrDefault();
        }

        public int genera_orden_compra(Modelo.ZctRequisition requistion)
        {
            int? orden_compra = 0;
            datos.SP_REQUISITION_OC(requistion.folio, ref orden_compra);
            requistion.Cod_OC = orden_compra;
            requistion.estatus = "ORDEN DE COMPRA GENERADA";
            requistion.update_web = false;

            return orden_compra.GetValueOrDefault();
        }


        internal void grabar(List<Modelo.ZctRequisition> list)
        {
            datos.SubmitChanges();
        }
        internal void grabar()
        {
            datos.SubmitChanges();
        }
    }
}
