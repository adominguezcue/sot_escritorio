﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatosFake.Repositorios
{
    public class Repositorio<TClass> : IRepositorio<TClass> where TClass: class, IEntidad
    {
        protected List<TClass> datosFake = new List<TClass>();

        public virtual void Agregar(TClass elemento)
        {
            if (!datosFake.Contains(elemento))
                datosFake.Add(elemento);
        }

        public virtual void Modificar(TClass elemento)
        {
            
        }

        public virtual void Eliminar(TClass elemento)
        {
            if (datosFake.Contains(elemento))
                datosFake.Remove(elemento);
        }

        public virtual TClass Obtener(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return datosFake.AsQueryable().FirstOrDefault(filtro);
        }

        public virtual IEnumerable<TClass> ObtenerElementos(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return datosFake.AsQueryable().Where(filtro);
        }

        public virtual IEnumerable<TClass> ObtenerElementos<TKey>(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, TKey>> orden, int pagina, int elementos) 
        {
            return datosFake.AsQueryable().Where(filtro).OrderBy(orden).Skip(pagina * elementos).Take(elementos);
        }

        public virtual IEnumerable<TClass> ObtenerTodo()
        {
            return datosFake;
        }

        public bool Alguno(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return datosFake.AsQueryable().Any(filtro);
        }

        public int Contador(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro)
        {
            return datosFake.AsQueryable().Count(filtro);
        }

        public virtual int GuardarCambios()
        {
            return 0;
        }


        public IEnumerable<TClass> ObtenerElementos(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, object>> path)
        {
            throw new NotImplementedException();
        }


        public TClass Obtener(System.Linq.Expressions.Expression<Func<TClass, bool>> filtro, System.Linq.Expressions.Expression<Func<TClass, object>> path)
        {
            throw new NotImplementedException();
        }
    }
}
