﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatosFake.Repositorios
{
    public class RepositorioHabitaciones : Repositorio<Habitacion>, IRepositorioHabitaciones
    {

        internal static List<Habitacion> df;

        static RepositorioHabitaciones() 
        {
            var rnd = new Random();

            df = new List<Habitacion>();

            List<Habitacion.EstadosHabitacion> listaEstados = Enum.GetValues(typeof(Habitacion.EstadosHabitacion)).Cast<Habitacion.EstadosHabitacion>().ToList();

            Tuple<int, int>[] pisoHabitacion = new Tuple<int, int>[56];

            pisoHabitacion[0] = new Tuple<int, int>(1, 1);
            pisoHabitacion[1] = new Tuple<int, int>(1, 2);
            pisoHabitacion[2] = new Tuple<int, int>(1, 3);
            pisoHabitacion[3] = new Tuple<int, int>(1, 4);
            pisoHabitacion[4] = new Tuple<int, int>(1, 5);
            pisoHabitacion[5] = new Tuple<int, int>(1, 6);
            pisoHabitacion[6] = new Tuple<int, int>(1, 7);
            pisoHabitacion[7] = new Tuple<int, int>(1, 8);
            pisoHabitacion[8] = new Tuple<int, int>(1, 9);
            pisoHabitacion[9] = new Tuple<int, int>(1, 10);
            pisoHabitacion[10] = new Tuple<int, int>(2, 1);
            pisoHabitacion[11] = new Tuple<int, int>(2, 2);
            pisoHabitacion[12] = new Tuple<int, int>(2, 3);
            pisoHabitacion[13] = new Tuple<int, int>(2, 4);
            pisoHabitacion[14] = new Tuple<int, int>(2, 5);
            pisoHabitacion[15] = new Tuple<int, int>(2, 6);
            pisoHabitacion[16] = new Tuple<int, int>(2, 7);
            pisoHabitacion[17] = new Tuple<int, int>(2, 8);
            pisoHabitacion[18] = new Tuple<int, int>(2, 9);
            pisoHabitacion[19] = new Tuple<int, int>(3, 1);
            pisoHabitacion[20] = new Tuple<int, int>(3, 2);
            pisoHabitacion[21] = new Tuple<int, int>(3, 3);
            pisoHabitacion[22] = new Tuple<int, int>(3, 4);
            pisoHabitacion[23] = new Tuple<int, int>(3, 5);
            pisoHabitacion[24] = new Tuple<int, int>(3, 6);
            pisoHabitacion[25] = new Tuple<int, int>(3, 7);
            pisoHabitacion[26] = new Tuple<int, int>(3, 8);
            pisoHabitacion[27] = new Tuple<int, int>(3, 9);
            pisoHabitacion[28] = new Tuple<int, int>(4, 1);
            pisoHabitacion[29] = new Tuple<int, int>(4, 2);
            pisoHabitacion[30] = new Tuple<int, int>(4, 3);
            pisoHabitacion[31] = new Tuple<int, int>(4, 4);
            pisoHabitacion[32] = new Tuple<int, int>(4, 5);
            pisoHabitacion[33] = new Tuple<int, int>(4, 6);
            pisoHabitacion[34] = new Tuple<int, int>(4, 7);
            pisoHabitacion[35] = new Tuple<int, int>(4, 8);
            pisoHabitacion[36] = new Tuple<int, int>(4, 9);
            pisoHabitacion[37] = new Tuple<int, int>(5, 1);
            pisoHabitacion[38] = new Tuple<int, int>(5, 2);
            pisoHabitacion[39] = new Tuple<int, int>(5, 3);
            pisoHabitacion[40] = new Tuple<int, int>(5, 4);
            pisoHabitacion[41] = new Tuple<int, int>(5, 5);
            pisoHabitacion[42] = new Tuple<int, int>(5, 6);
            pisoHabitacion[43] = new Tuple<int, int>(5, 7);
            pisoHabitacion[44] = new Tuple<int, int>(5, 8);
            pisoHabitacion[45] = new Tuple<int, int>(5, 9);
            pisoHabitacion[46] = new Tuple<int, int>(6, 1);
            pisoHabitacion[47] = new Tuple<int, int>(6, 2);
            pisoHabitacion[48] = new Tuple<int, int>(6, 3);
            pisoHabitacion[49] = new Tuple<int, int>(6, 4);
            pisoHabitacion[50] = new Tuple<int, int>(6, 5);
            pisoHabitacion[51] = new Tuple<int, int>(6, 6);
            pisoHabitacion[52] = new Tuple<int, int>(6, 7);
            pisoHabitacion[53] = new Tuple<int, int>(6, 8);
            pisoHabitacion[54] = new Tuple<int, int>(6, 9);
            pisoHabitacion[55] = new Tuple<int, int>(6, 10);

            for (int i = 1; i <= 56; i++)
            {
                df.Add(new Habitacion
                {
                    Id = i,
                    //TipoHabitacion = listaTipos[rnd.Next(listaTipos.Count)],
                    EstadoHabitacion = listaEstados[rnd.Next(listaEstados.Count)],
                    TipoHabitacion = RepositorioTiposHabitacion.Tipos[rnd.Next(RepositorioTiposHabitacion.Tipos.Count)],
                    NumeroHabitacion = i.ToString("000"),
                    Activo = true,
                    Piso = pisoHabitacion[i - 1].Item1,
                    Posicion = pisoHabitacion[i - 1].Item2
                });
            }
        }

        public RepositorioHabitaciones() 
        {
            datosFake = df;
        }

        public List<Habitacion> ObtenerActivasConTipos()
        {
            return datosFake.Where(m => m.Activo).ToList();
        }


        public Habitacion ObtenerConMotivoBloqueo(int idHabitacion)
        {
            throw new NotImplementedException();
        }
    }
}
