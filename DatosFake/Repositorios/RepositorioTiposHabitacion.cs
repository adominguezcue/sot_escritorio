﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatosFake.Repositorios
{
    public class RepositorioTiposHabitacion : Repositorio<TipoHabitacion>, IRepositorioTiposHabitacion
    {
        internal static List<TipoHabitacion> Tipos;

        static RepositorioTiposHabitacion()
        {
            Tipos = new List<TipoHabitacion>();

            string[] nombres = { "Twin Suite", "Junior Suite", "Steam Suite", "Sky Suite", "Pool Villa", "Junior Villa" };

            for (int i = 0; i <= nombres.Length - 1; i++)
            {
                Tipos.Add(new TipoHabitacion
                {
                    Id = i,
                    Activo = true,
                    Descripcion = nombres[i],
                    //IdConfiguracionNegocio = RepositorioConfiguracionesNegocio.configuraciones.First().Id,
                    //PrecioHotel = 1800,
                    //PrecioPersonaExtraHotel = 250,
                    //PrecioHospedajeExtraHotel = 1800
                });

                //RepositorioConfiguracionesNegocio.configuraciones.First().TiposHabitacion.Add(Tipos.Last());
            }

        }

        public RepositorioTiposHabitacion() 
        {
            datosFake = Tipos;
        }

        public List<int> ObtenerTarifasSoportadas(int idTipoHabitacion)
        {
            throw new NotImplementedException();
        }


        public ConfiguracionTipo ObtenerConfiguracionTipo(int idTipoHabitacion, ConfiguracionNegocio.Tarifas tarifa)
        {
            throw new NotImplementedException();
        }


        public TipoHabitacion ObtenerTipoConTiemposLimpiezaPorHabitacion(int idHabitacion)
        {
            throw new NotImplementedException();
        }
    }
}
