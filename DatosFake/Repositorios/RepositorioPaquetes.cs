﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatosFake.Repositorios
{
    public class RepositorioPaquetes : Repositorio<Paquete>, IRepositorioPaquetes
    {
        internal static List<Paquete> df;

        static RepositorioPaquetes() 
        {
            var rnd = new Random();

            df = new List<Paquete>();

            List<Habitacion.EstadosHabitacion> listaEstados = Enum.GetValues(typeof(Habitacion.EstadosHabitacion)).Cast<Habitacion.EstadosHabitacion>().ToList();

            for (int i = 1; i <= 10; i++)
            {
                df.Add(new Paquete
                {
                    Id = i, 
                    Nombre = string.Format("Paquete {0}", i),
                    Activo = true,
                    TipoHabitacion = RepositorioTiposHabitacion.Tipos[rnd.Next(RepositorioTiposHabitacion.Tipos.Count)],
                    Precio = 10 * i * rnd.Next(RepositorioTiposHabitacion.Tipos.Count) + 1
                });

                df.Last().IdTipoHabitacion = df.Last().TipoHabitacion.Id;
            }
        }

        public RepositorioPaquetes() 
        {
            datosFake = df;
        }
    }
}
