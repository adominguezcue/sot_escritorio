﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatosFake.Repositorios
{
    public class RepositorioRentas : Repositorio<Renta>, IRepositorioRentas
    {
        internal static List<Renta> df;

        static RepositorioRentas() 
        {
            df = new List<Renta>();
        }

        public RepositorioRentas() 
        {
            datosFake = df;
        }

        public Renta ObtenerRentaActualPorHabitacion(int idHabitacion)
        {
            throw new NotImplementedException();
        }


        public Renta ObtenerCargada(int idRenta)
        {
            throw new NotImplementedException();
        }
    }
}
