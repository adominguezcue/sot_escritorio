﻿using Modelo;
using Modelo.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatosFake.Repositorios
{
    public class RepositorioConfiguracionesNegocio : Repositorio<ConfiguracionNegocio>, IRepositorioConfiguracionesNegocio
    {
        internal static List<ConfiguracionNegocio> configuraciones;

        static RepositorioConfiguracionesNegocio() 
        {
            configuraciones = new List<ConfiguracionNegocio>();

            configuraciones.Add(new ConfiguracionNegocio
            {
                Activa = true,
                Id = 1,
                CantidadAutosMaxima = 1,
                DuracionHoras = 8,
                RangoFijo = false
            });
        }

        public RepositorioConfiguracionesNegocio() 
        {
            datosFake = configuraciones;
        }
    }
}
