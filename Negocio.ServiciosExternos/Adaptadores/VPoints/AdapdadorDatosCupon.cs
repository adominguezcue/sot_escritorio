﻿using Negocio.ServiciosExternos.DTOs.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.Adaptadores.VPoints
{
    internal class AdapdadorDatosCupon
    {
        internal static bool StringToDtoDatosCupon(string parametros, out DtoDatosCupon resultado) 
        {

            if (string.IsNullOrWhiteSpace(parametros))
            {
                resultado = null;
                return false;
            }
            //fechaCupon|idPremio|numeroTarjeta|descuento|estatusCupon 
            string[] datos = parametros.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            if (datos.Length != 5)
            {
                resultado = null;
                return false;
            }

            DateTime fechaCupon;
            string idPremio;
            string numeroTarjeta;
            decimal descuento;
            string estatusCupon;

            if (!DateTime.TryParse(datos[0], out fechaCupon))
            {
                resultado = null;
                return false;
            }
            if (!decimal.TryParse(datos[3], out descuento))
            {
                resultado = null;
                return false;
            }

            idPremio = datos[1];
            numeroTarjeta = datos[2];
            estatusCupon = datos[4];

            resultado = new DtoDatosCupon
            {
                FechaCupon = fechaCupon,
                IdPremio = idPremio,
                NumeroTarjeta = numeroTarjeta,
                Descuento = descuento,
                EstatusCupon = estatusCupon,
                PorCanjear = estatusCupon.Trim().ToUpper().Equals("POR CANJEAR")
            };

            return true;
        }

        internal static bool DtoDatosCuponToString(DtoDatosCupon datos, out string resultado)
        {
            throw new NotImplementedException();
        }
    }
}
