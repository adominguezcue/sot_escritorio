﻿using Negocio.ServiciosExternos.DTOs.Sincronizacion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Negocio.ServiciosExternos.Adaptadores.Sincronizacion
{
    internal class AdaptadorEstatusHabitacion
    {
        internal static List<DtoEstatusHabitacion> DataTableToDtoEstatusHabitacion(DataTable data)
        {
            return data.ToList<DtoEstatusHabitacion>();
        }

        internal static DataTable DtoEstatusHabitacionsToDataTable(List<DtoEstatusHabitacion> list)
        {
            return list.ToDataTable();
        }
    }
}
