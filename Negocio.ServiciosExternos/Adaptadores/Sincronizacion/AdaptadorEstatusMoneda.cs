﻿using Negocio.ServiciosExternos.DTOs.Sincronizacion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Negocio.ServiciosExternos.Adaptadores.Sincronizacion
{
    internal class AdaptadorEstatusMoneda
    {
        internal static List<DtoEstatusMoneda> DataTableToDtoEstatusMoneda(DataTable data)
        {
            return data.ToList<DtoEstatusMoneda>();
        }

        internal static DataTable DtoEstatusMonedasToDataTable(List<DtoEstatusMoneda> list)
        {
            return list.ToDataTable();
        }
    }
}
