﻿using Negocio.ServiciosExternos.DTOs.Sincronizacion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Negocio.ServiciosExternos.Adaptadores.Sincronizacion
{
    internal class AdaptadorEstatusGasto
    {
        internal static List<DtoEstatusGastos> DataTableToDtoEstatusGasto(DataTable data)
        {
            return data.ToList<DtoEstatusGastos>();
        }

        internal static DataTable DtoEstatusGastosToDataTable(List<DtoEstatusGastos> list)
        {
            return list.ToDataTable();
        }
    }
}
