﻿using Negocio.ServiciosExternos.DTOs.Sincronizacion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Extensiones;

namespace Negocio.ServiciosExternos.Adaptadores.Sincronizacion
{
    internal class AdaptadorEstatusTransaccion
    {
        internal static List<DtoEstatusTransaccion> DataTableToDtoEstatusTransaccion(DataTable data) 
        {
            return data.ToList<DtoEstatusTransaccion>();
        }

        internal static DataTable DtoEstatusTransaccionToDataTable(List<DtoEstatusTransaccion> list)
        {
            return list.ToDataTable();
        }
    }
}
