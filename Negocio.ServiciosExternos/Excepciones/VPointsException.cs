﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.Excepciones
{
    public class VPointsException : Exception
    {
        private static string TEXTO_INICIAL = "Error de V points. Detalles: ";

        public VPointsException()
            : base(TEXTO_INICIAL)
        {
        }

        public VPointsException(string message, params string[] args)
            : base(TEXTO_INICIAL + string.Format(message, args))
        {
        }

        public VPointsException(string message, Exception inner)
            : base(TEXTO_INICIAL + message, inner)
        {
        }

        protected VPointsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
