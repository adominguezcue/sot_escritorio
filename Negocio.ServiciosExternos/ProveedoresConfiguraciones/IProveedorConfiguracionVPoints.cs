﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.ProveedoresConfiguraciones
{
    public interface IProveedorConfiguracionVPoints
    {
        string ObtenerUrl();
    }
}
