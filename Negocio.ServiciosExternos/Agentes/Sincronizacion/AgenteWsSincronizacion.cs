﻿using Negocio.ServiciosExternos.DTOs.Sincronizacion;
using Negocio.ServiciosExternos.Adaptadores.Sincronizacion;
using Negocio.ServiciosExternos.DTOs.Sincronizacion;
using Negocio.ServiciosExternos.wsSincronizacion;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ServiceModel;
using Transversal.Dependencias;
using Negocio.ServiciosExternos.ProveedoresConfiguraciones;

namespace Negocio.ServiciosExternos.Agentes.Sincronizacion
{
    public class AgenteWsSincronizacion
    {
        readonly string nombreServicio;
        private readonly BasicHttpBinding binding;
        private readonly EndpointAddress endPoint;
        private bool usarNombre;

        //public AgenteWsSincronizacion()
        //{
        //    usarNombre = true;

        //    try
        //    {
        //        ClientSection clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
        //        for (int i = 0; i < clientSection.Endpoints.Count; i++)
        //        {
        //            if (clientSection.Endpoints[i].Contract == "wsSincronizacion.Service1Soap")
        //            {
        //                nombreServicio = clientSection.Endpoints[i].Name;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("Ocurrió un error al intentar inicializar el agente de sincronización, consulte la excepción interna para obtener más detalles.", e);
        //    }
        //}

        public AgenteWsSincronizacion()
        {
            usarNombre = false;

            try
            {
                var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionSincronizacion>();

                var urlServicio = proveedor.ObtenerUrl();

                binding = new BasicHttpBinding()
                {
                    MaxBufferSize = 999999999,
                    MaxReceivedMessageSize = 999999999,
                    OpenTimeout = TimeSpan.FromMinutes(2),
                    ReceiveTimeout = TimeSpan.FromMinutes(210),
                    CloseTimeout = TimeSpan.FromMinutes(2),
                    SendTimeout = TimeSpan.FromMinutes(10),
                    UseDefaultWebProxy = true
                };

                endPoint = new EndpointAddress(urlServicio);
            }
            catch (Exception e)
            {
                throw new Exception("Ocurrió un error al intentar inicializar el agente de sincronización, consulte la excepción interna para obtener más detalles.", e);
            }
        }

        //        public object CatalogoHabitaciones(Habitacion[] habitaciones, int idMovimienot)
        //        {
        //            /*
        // 1 alta
        //2 update
        //3 baja
        // */

        //            using (Service1SoapClient servicio = ObtenerClient())
        //            {
        //                try
        //                {
        //                    return servicio.CatalogoHabitaciones(habitaciones, idMovimienot);
        //                }
        //                finally
        //                {
        //                    try
        //                    {
        //                        servicio.Close();
        //                    }
        //                    catch
        //                    {
        //                        servicio.Abort();
        //                    }
        //                }
        //            }
        //        }

        public List<DtoEstatusTransaccion> CargarCorteCaja(Corte[] cortes)
        {
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return AdaptadorEstatusTransaccion.DataTableToDtoEstatusTransaccion(servicio.CargarCorteCaja(cortes));
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        public List<DtoEstatusGastos> CargarGastos(CorteGastos[] cortes)
        {
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return AdaptadorEstatusGasto.DataTableToDtoEstatusGasto(servicio.CargarGastos(cortes));
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        public List<DtoEstatusMoneda> CargarEfectivo(Efectivo[] efectivo)
        {
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return AdaptadorEstatusMoneda.DataTableToDtoEstatusMoneda(servicio.Efectivo(efectivo));
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        public List<DtoEstatusHabitacion> CatalogoHabitaciones(Habitacion[] habitaciones, int movimiento)
        {
            /*
1 alta
2 update
3 baja
*/
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return AdaptadorEstatusHabitacion.DataTableToDtoEstatusHabitacion(servicio.CatalogoHabitaciones(habitaciones, movimiento));
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        public bool VerificarExisteSucursal(string identificadorSucursal)
        {
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return servicio.ExistePropietaria(identificadorSucursal);
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        private Service1SoapClient ObtenerClient()
        {
            if (usarNombre)
                return new Service1SoapClient(nombreServicio);

            return new Service1SoapClient(binding, endPoint);
        }
    }
}
