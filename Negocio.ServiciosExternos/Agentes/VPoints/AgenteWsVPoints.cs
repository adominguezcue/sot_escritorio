﻿using Negocio.ServiciosExternos.Adaptadores.VPoints;
using Negocio.ServiciosExternos.DTOs.VPoints;
using Negocio.ServiciosExternos.Excepciones;
using Negocio.ServiciosExternos.ProveedoresConfiguraciones;
using Negocio.ServiciosExternos.wsVPoints;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.ServiciosExternos.Agentes.VPoints
{
    public class AgenteWsVPoints
    {
        readonly string nombreServicio;
        private bool usarNombre;
        private readonly CustomBinding bindingIso;
        private readonly EndpointAddress endPoint;

        //public AgenteWsVPoints()
        //{

        //    usarNombre = true;

        //    try
        //    {
        //        ClientSection clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
        //        for (int i = 0; i < clientSection.Endpoints.Count; i++)
        //        {
        //            if (clientSection.Endpoints[i].Contract == "wsVPoints.serverPortType")
        //            {
        //                nombreServicio = clientSection.Endpoints[i].Name;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("Ocurrió un error al intentar inicializar el agente de VPoints, consulte la excepción interna para obtener más detalles.", e);
        //    }
        //}


        public AgenteWsVPoints()
        {
            usarNombre = false;

            

            try
            {
                var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionVPoints>();

                var urlServicio = proveedor.ObtenerUrl();

                var elemento = new Microsoft.Samples.CustomTextMessageEncoder.CustomTextMessageBindingElement { Encoding = "utf-8", MessageVersion = MessageVersion.Soap11 };

                bindingIso = new CustomBinding("ISO8859Binding", "", elemento, new HttpTransportBindingElement
                {
                    MaxBufferSize = 999999999,
                    MaxReceivedMessageSize = 999999999,
                    AuthenticationScheme = System.Net.AuthenticationSchemes.Anonymous,
                    ProxyAuthenticationScheme = System.Net.AuthenticationSchemes.Anonymous,
                    UseDefaultWebProxy = true
                });

                endPoint = new EndpointAddress(urlServicio);
            }
            catch (Exception e)
            {
                throw new Exception("Ocurrió un error al intentar inicializar el agente de VPoints, consulte la excepción interna para obtener más detalles.", e);
            }
        }

        /// <summary>
        /// Agrega una tarjeta a la base de datos en el momento en que es vendida. La tarjeta se
        /// agrega con estatus “pendiente de activación” y es necesario que el huésped la active
        /// desde el portal. Esta función impide que tanto un número de tarjeta como un ID de cliente
        /// se dupliquen o registren dos veces.
        /// </summary>
        /// <param name="usuario">Usuario del web service.</param>
        /// <param name="contrasena">Contraseña del web service. </param>
        /// <param name="numeroDeTarjeta">Número de la Tarjeta del huésped.</param>
        /// <param name="idCliente">ID de cliente del sistema de reservaciones. </param>
        /// <param name="sucursal">Sucursal.</param>
        public void RegistrarTarjeta(string usuario, string contrasena, string numeroDeTarjeta, string idCliente, string sucursal) 
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    string param = string.Join("|", usuario, contrasena, numeroDeTarjeta, idCliente, sucursal);

                    string respuesta = servicio.registraTarjeta(param);

                    if (respuesta != "EXITO")
                        throw new VPointsException(respuesta);
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        /// <summary>
        /// Devuelve el saldo en puntos de una tarjeta.
        /// </summary>
        /// <param name="usuario">Usuario del web service.</param>
        /// <param name="contrasena">Contraseña del web service.</param>
        /// <param name="numeroDeTarjeta">Número de tarjeta a consultar.</param>
        /// <returns>Si se trata de un número de tarjeta válido, regresa el saldo actual en puntos.</returns>
        public decimal ObtenerSaldo(string usuario, string contrasena, string numeroDeTarjeta)
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    string param = string.Join("|", usuario, contrasena, numeroDeTarjeta);


                    string respuesta = servicio.obtenSaldo(param);

                    decimal saldo;

                    if (!decimal.TryParse(respuesta, out saldo))
                        throw new VPointsException(respuesta);

                    return saldo;
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        /// <summary>
        /// Agrega puntos a una tarjeta en base al monto total del consumo
        /// </summary>
        /// <param name="usuario">Usuario del web service.</param>
        /// <param name="contrasena">Contraseña del web service.</param>
        /// <param name="numeroDeTarjeta">Número de la Tarjeta del huésped.</param>
        /// <param name="ticket">Folio del ticket o de la operación.</param>
        /// <param name="montoConsumoTotal">Monto total del consumo.</param>
        /// <param name="montoHabitacion">Subtotal por habitación.</param>
        /// <param name="montoPersonasExtra">Subtotal por personas extra.</param>
        /// <param name="montoAlimentos">Subtotal por consumo de alimentos.</param>
        /// <param name="montoBebidas">Subtotal por consumo de bebidas.</param>
        /// <param name="montoSexSpa">Subtotal por consumo en Sex Spa.</param>
        /// <param name="sucursal">Sucursal.</param>
        public void AcumularPuntosDetalle(string usuario, string contrasena, string numeroDeTarjeta,
                                          string ticket, decimal montoConsumoTotal, decimal montoHabitacion,
                                          decimal montoPersonasExtra, decimal montoAlimentos, decimal montoBebidas,
                                          decimal montoSexSpa, string sucursal) 
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    string param = string.Join("|", usuario, contrasena, numeroDeTarjeta, ticket, montoConsumoTotal, montoHabitacion, 
                                                    montoPersonasExtra, montoAlimentos, montoBebidas, montoSexSpa, sucursal);


                    string respuesta = servicio.acumulaPuntosDetalle(param);

                    if (respuesta != "EXITO")
                        throw new VPointsException(respuesta);
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        /// <summary>
        /// Regresa los datos de un cupón válido.
        /// </summary>
        /// <param name="usuario">Usuario del web service.</param>
        /// <param name="contrasena">Contraseña del web service.</param>
        /// <param name="idCupon">ID de cupón.</param>
        /// <returns>Información y estatus del cupón</returns>
        public DtoDatosCupon ObtenerCupon(string usuario, string contrasena, string idCupon) 
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    string param = string.Join("|", usuario, contrasena, idCupon);


                    string respuesta = servicio.obtenCupon(param);

                    DtoDatosCupon estado;
                         
                    if (!AdapdadorDatosCupon.StringToDtoDatosCupon(respuesta, out estado))
                        throw new VPointsException(respuesta);

                    estado.Cupon = idCupon;

                    return estado;
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario">Usuario del web service.</param>
        /// <param name="contrasena">Contraseña del web service.</param>
        /// <param name="idCupon">ID de cupón.</param>
        /// <param name="sucursal">Sucursal.</param>
        public void CanjearCupon(string usuario, string contrasena, string idCupon, string sucursal) 
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    string param = string.Join("|", usuario, contrasena, idCupon, sucursal);


                    string respuesta = servicio.canjeaCupon(param);

                    if (respuesta != "EXITO")
                        throw new VPointsException(respuesta);
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        /// <summary>
        /// Descuenta puntos a una tarjeta.
        /// </summary>
        /// <param name="usuario">Usuario del web service.</param>
        /// <param name="contrasena">Contraseña del usuario del web service.</param>
        /// <param name="tarjeta">Número de la Tarjeta del huésped.</param>
        /// <param name="ticketTransaccion">Número de la Tarjeta del huésped.</param>
        /// <param name="puntos">
        /// Número de ticket/transacción asociado. Este número es para identificar en que venta o
        /// transacción se aplicaron los puntos.
        /// </param>
        /// <param name="sucursal">Sucursal.</param>
        public void DescontarPuntos(string usuario, string contrasena, string tarjeta, string ticketTransaccion, decimal puntos, string sucursal)
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    string param = string.Join("|", usuario, contrasena, tarjeta, ticketTransaccion, puntos, sucursal);


                    string respuesta = servicio.descuentaPuntos(param);

                    if (respuesta != "EXITO")
                        throw new VPointsException(respuesta);
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        private serverPortTypeClient ObtenerClient()
        {
            if (usarNombre)
                return new serverPortTypeClient(nombreServicio);

            return new serverPortTypeClient(bindingIso, endPoint);
        }
    }
}
