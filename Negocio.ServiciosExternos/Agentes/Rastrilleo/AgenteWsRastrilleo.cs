﻿using Negocio.ServiciosExternos.Adaptadores.Rastrilleo;
using Negocio.ServiciosExternos.DTOs.Rastrilleo;
using Negocio.ServiciosExternos.ProveedoresConfiguraciones;
using Negocio.ServiciosExternos.wsRastrilleo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.ServiciosExternos.Agentes.Rastrilleo
{
    public class AgenteWsRastrilleo
    {
        readonly string nombreServicio;
        private readonly BasicHttpBinding binding;
        private readonly EndpointAddress endPoint;
        private bool usarNombre;

        //public AgenteWsRastrilleo()
        //{
        //    usarNombre = true;

        //    try
        //    {
        //        ClientSection clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
        //        for (int i = 0; i < clientSection.Endpoints.Count; i++)
        //        {
        //            if (clientSection.Endpoints[i].Contract == "wsRastrilleo.Service1Soap")
        //            {
        //                nombreServicio = clientSection.Endpoints[i].Name;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("Ocurrió un error al intentar inicializar el agente de rastrilleo, consulte la excepción interna para obtener más detalles.", e);
        //    }
        //}

        public AgenteWsRastrilleo()
        {
            usarNombre = false;

            try
            {
                var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionRastrilleo>();

                var urlServicio = proveedor.ObtenerUrl();

                binding = new BasicHttpBinding()
                {
                    MaxBufferSize = 999999999,
                    MaxReceivedMessageSize = 999999999,
                    OpenTimeout = TimeSpan.FromMinutes(2),
                    ReceiveTimeout = TimeSpan.FromMinutes(210),
                    CloseTimeout = TimeSpan.FromMinutes(2),
                    SendTimeout = TimeSpan.FromMinutes(10),
                    UseDefaultWebProxy = true
                };

                endPoint = new EndpointAddress(urlServicio);
            }
            catch (Exception e)
            {
                throw new Exception("Ocurrió un error al intentar inicializar el agente de rastrilleo, consulte la excepción interna para obtener más detalles.", e);
            }
        }

        public List<DtoEstatusTransaccion> EnviarOrden(string folioCaja, MastPedido[] pedidos)
        {
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return AdaptadorEstatusTransaccion.DataTableToDtoEstatusTransaccion(servicio.EnviarOrden(folioCaja, pedidos));
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }


        public List<DtoEstatusTransaccion> MarcarOrden(string folioCaja, MastPedido[] pedidos)
        {
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return AdaptadorEstatusTransaccion.DataTableToDtoEstatusTransaccion(servicio.MarcarOrden(folioCaja, pedidos));
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }

        }

        public System.Data.DataTable ObtenerRastrilleadas(string folioCaja, string sucursal) 
        {
            using (Service1SoapClient servicio = ObtenerClient())
            {
                try
                {
                    return servicio.ObtenerRastrilleadas(folioCaja, sucursal);
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        private Service1SoapClient ObtenerClient()
        {
            if (usarNombre)
                return new Service1SoapClient(nombreServicio);

            return new Service1SoapClient(binding, endPoint);
        }
    }
}
