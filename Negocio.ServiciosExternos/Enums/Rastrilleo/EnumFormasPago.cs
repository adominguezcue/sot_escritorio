﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.Enums.Rastrilleo
{
    public enum EnumFormasPago
    {
        Efectivo = 1,
        Tarjeta = 2,
        Credito = 3,
        Debito = 4,
        Paypal = 5,
        Cortesia = 6,
        Consumo = 7,
        VPoints = 8,
        Cupon = 9,
        Reservacion = 10
    }
}