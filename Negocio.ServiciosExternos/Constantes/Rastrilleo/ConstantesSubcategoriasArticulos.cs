﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.Constantes.Rastrilleo
{
    public class ConstantesSubcategoriasArticulos
    {
        public const string ALIMENTOS = "A";
        public const string BEBIDAS = "B";
        public const string SEX_AND_SPA = "S";
    }
}
