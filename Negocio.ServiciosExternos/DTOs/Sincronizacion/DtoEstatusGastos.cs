﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.DTOs.Sincronizacion
{
    public class DtoEstatusGastos
    {
        [DisplayName("vchCorte")]
        public string Folio { get; set; }
        [DisplayName("vchMensaje")]
        public string Respuesta { get; set; }
    }
}
