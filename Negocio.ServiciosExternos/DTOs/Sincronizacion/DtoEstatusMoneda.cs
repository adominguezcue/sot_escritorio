﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.DTOs.Sincronizacion
{
    public class DtoEstatusMoneda
    {
        [DisplayName("vchMoneda")]
        public string Moneda { get; set; }
        [DisplayName("vchMensaje")]
        public string Respuesta { get; set; }
    }
}
