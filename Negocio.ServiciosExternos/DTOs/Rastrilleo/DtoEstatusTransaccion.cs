﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.DTOs.Rastrilleo
{
    public class DtoEstatusTransaccion
    {
        [DisplayName("vchPedido")]
        public string Folio { get; set; }
        [DisplayName("vchMensaje")]
        public string Respuesta { get; set; }
        [DisplayName("idClasificacionVenta")]
        public int IdClasificacionVenta { get; set; }
    }
}
