﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosExternos.DTOs.VPoints
{
    public class DtoDatosCupon
    {
        //fechaCupon|idPremio|numeroTarjeta|descuento|estatusCupon  
        public DateTime FechaCupon { get; internal set; }
        public string IdPremio { get; internal set; }
        public string NumeroTarjeta { get; internal set; }
        public decimal Descuento { get; internal set; }
        public string EstatusCupon { get; internal set; }
        public bool PorCanjear { get; internal set; }

        public string Cupon { get; set; }
    }
}
