﻿using System;
using System.Collections.Generic;
using Modelo.Seguridad.Dtos;
using Modelo.Sistemas.Entidades;

namespace Negocio.Sistemas.ConfiguracionesSistemas
{
    public interface IServicioConfiguracionesSistemas
    {
        //ConfiguracionSistema ObtenerConfiguracion(Guid idSistema);
        List<ParametroSistema> ObtenerParametros(Guid idSistema);

        void GuardarParametrosSistema(List<ParametroSistema> parametros, DtoUsuario usuario);
        ParametroSistema ObtenerParametro(Guid idSistema, string nombre);
    }
}