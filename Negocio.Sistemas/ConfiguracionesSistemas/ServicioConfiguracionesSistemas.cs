﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Seguridad.Dtos;
using Modelo.Sistemas.Entidades;
using Modelo.Sistemas.Repositorios;
using Negocio.Seguridad.Permisos;
using Negocio.ServiciosExternos.ProveedoresConfiguraciones;
using Newtonsoft.Json;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.Sistemas.ConfiguracionesSistemas
{
    public class ServicioConfiguracionesSistemas : IServicioConfiguracionesSistemas
    {
        private IRepositorioConfiguracionesSistemas RepositorioConfiguracionesSistemas
        {
            get { return _repositorioConfiguracionesSistemas.Value; }
        }

        private Lazy<IRepositorioConfiguracionesSistemas> _repositorioConfiguracionesSistemas = new Lazy<IRepositorioConfiguracionesSistemas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioConfiguracionesSistemas>();
        });
        private IRepositorioParametrosSistemas RepositorioParametrosSistemas
        {
            get { return _repositorioParametrosSistemas.Value; }
        }

        private Lazy<IRepositorioParametrosSistemas> _repositorioParametrosSistemas = new Lazy<IRepositorioParametrosSistemas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IRepositorioParametrosSistemas>();
        });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });


//        public ConfiguracionSistema ObtenerConfiguracion(Guid idSistema)
//        {
//#warning revisar si hace falta alguna validación de permisos
//            return RepositorioConfiguracionesSistemas.Obtener(m => m.Id == idSistema);
//        }

        public List<ParametroSistema> ObtenerParametros(Guid idSistema)
        {
#warning revisar si hace falta alguna validación de permisos
            return RepositorioParametrosSistemas.ObtenerElementos(m => m.GuidSistema == idSistema).ToList();
        }

        public ParametroSistema ObtenerParametro(Guid idSistema, string nombre)
        {
#warning revisar si hace falta alguna validación de permisos
            return RepositorioParametrosSistemas.Obtener(m => m.GuidSistema == idSistema && m.Nombre == nombre);
        }

        public void GuardarParametrosSistema(List<ParametroSistema> parametros, DtoUsuario usuario)
        {
#warning revisar si hace falta alguna validación de permisos
            ServicioPermisos.ValidarPermisos(usuario, new DtoPermisos { ConfigurarParametrosSistema = true });

            if (parametros == null || parametros.Count == 0)
                throw new SOTException(Recursos.ParametrosSistemas.parametros_no_proporcionados_excepcion);

            if (parametros.GroupBy(m => m.GuidSistema).Count() != 1)
                throw new SOTException(Recursos.ParametrosSistemas.sistemas_diferentes_excepcion);

            if(parametros.GroupBy(m=> m.Nombre.Trim()).Count() != parametros.Count)
                throw new SOTException(Recursos.ParametrosSistemas.parametros_duplicados_excepcion);

            var idSistema = parametros.First().GuidSistema;

            if (idSistema == Guid.Empty)
                throw new SOTException(Recursos.ParametrosSistemas.identificador_sistema_invalido_excepcion);

            var parametrosExistentes = RepositorioParametrosSistemas.ObtenerElementos(m => m.GuidSistema == idSistema).ToList();

            var ids = parametros.Select(m => m.Id).ToList();

            foreach (var parametroAntiguo in parametrosExistentes.Where(m=> !ids.Contains(m.Id)))
                RepositorioParametrosSistemas.Eliminar(parametroAntiguo);

            foreach (var parametroAntiguo in parametrosExistentes.Where(m => ids.Contains(m.Id)))
            {
                var parametro = parametros.First(m => m.Id == parametroAntiguo.Id);

                parametroAntiguo.SincronizarPrimitivas(parametro);

                RepositorioParametrosSistemas.Modificar(parametroAntiguo);
            }

            foreach (var parametro in parametros.Where(m => m.Id == 0))
                RepositorioParametrosSistemas.Agregar(parametro);

            RepositorioParametrosSistemas.GuardarCambios();
        }

        //#region Servicios Externos

        //string IProveedorConfiguracionSincronizacion.ObtenerUrl()
        //{
        //    var parametro = RepositorioParametrosSistemas.Obtener(m => m.Nombre == nameof(Modelo.Sistemas.Entidades.Dtos.ParametrosSincronizacion.UrlWebServiceCorte));

        //    if (parametro == null || parametro.Valor == null)
        //        return null;

        //    return JsonConvert.DeserializeObject<string>(parametro.Valor);
        //}

        //string IProveedorConfiguracionRastrilleo.ObtenerUrl()
        //{
        //    var parametro = RepositorioParametrosSistemas.Obtener(m => m.Nombre == nameof(Modelo.Sistemas.Entidades.Dtos.ParametrosSincronizacion.UrlWebServiceRastrilleo));

        //    if (parametro == null || parametro.Valor == null)
        //        return null;

        //    return JsonConvert.DeserializeObject<string>(parametro.Valor);
        //}

        //#endregion
    }
}
