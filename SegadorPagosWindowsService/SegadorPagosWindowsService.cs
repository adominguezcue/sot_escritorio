﻿using Negocio.Rastrilleo;
using Negocio.ServiciosLocales.ProveedoresConfiguraciones;
using Negocio.Sincronizacion;
using ServiciosTCP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace SegadorPagosWindowsService
{
    public partial class SegadorPagosWindowsService : ServiceBase
    {
        System.Timers.Timer timer;
        int milisegundos;

        internal static ServiceHost hostSincronizacion = null;

        private static readonly object bloqueador = new object();
        private static bool sincronizando = false;

        private static ServiciosTCP.IServicioSincronizacion servicioSincronizacion;

        public SegadorPagosWindowsService()
        {
            InitializeComponent();

            //var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<ServiciosTCP.IServicioSincronizacion>();

            //servicioSincronizacion.SubirInformacion();

            if (!int.TryParse(System.Configuration.ConfigurationManager.AppSettings["minutosCiclo"], out milisegundos))
                throw new SOTException("Defina la clave \"minutosCiclo\" en el archivo de configuración");

            milisegundos *= 60000;
        }

        protected override void OnStart(string[] args)
        {
            //sincronizando = false;
            try
            {
                servicioSincronizacion = FabricaDependencias.Instancia.Resolver<ServiciosTCP.IServicioSincronizacion>();

                for (int i = 0; i < 3; i++)
                {
                    if (servicioSincronizacion.SubirInformacionConBandera())
                        Thread.Sleep(3000);
                    else
                        break;
                }

                timer = new System.Timers.Timer();
                timer.Interval = milisegundos;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimer);
                timer.Start();

                if (hostSincronizacion != null)
                    try
                    {
                        hostSincronizacion.Close();
                    }
                    catch
                    {
                        hostSincronizacion.Abort();
                    }
                var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionSincronizacion>();
                var rutaBase = proveedor.ObtenerUrl();

                var binding = new NetTcpBinding()
                {
                    MaxBufferSize = 999999999,
                    MaxReceivedMessageSize = 999999999,
                    OpenTimeout = TimeSpan.FromMinutes(2),
                    ReceiveTimeout = TimeSpan.FromMinutes(210),
                    CloseTimeout = TimeSpan.FromMinutes(2),
                    SendTimeout = TimeSpan.FromMinutes(10)
                };

                binding.Security.Mode = SecurityMode.None;
                binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;

                hostSincronizacion = new ServiceHost(servicioSincronizacion, new Uri(rutaBase));// typeof(ServiciosTCP.ServicioSincronizacion));
                hostSincronizacion.AddServiceEndpoint(typeof(ServiciosTCP.IServicioSincronizacion), binding, rutaBase);
                hostSincronizacion.Open();
            }
            catch (Exception ex)
            {
                Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error en la inicializacón del servicio");
                for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                {
                    Transversal.Log.Logger.Error(ex.Message);
                }
            }

        }

        protected override void OnStop()
        {
            if (timer != null)
                timer.Stop();

            if (hostSincronizacion != null)
            {
                hostSincronizacion.Close();
                hostSincronizacion = null;
            }
        }

        private static void OnTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (bloqueador)
            {
                if (sincronizando)
                    return;

                sincronizando = true;
            }

            try
            {
                //var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<ServiciosTCP.IServicioSincronizacion>();

                for (int i = 0; i < 3; i++)
                {
                    if (servicioSincronizacion.SubirInformacionConBandera())
                        Thread.Sleep(3000);
                    else
                        break;
                }
            }
            catch (Exception ex)
            {
                Transversal.Log.Logger.Info("[" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "] Error en la ejecución temporal");
                for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
                {
                    Transversal.Log.Logger.Error(ex.Message);
                }
            }
            finally
            {
                lock (bloqueador)
                {
                    sincronizando = false;
                }
            }
        }
    }
}
