﻿using Modelo.Sistemas.Entidades.Dtos;
using Negocio.ServiciosExternos.ProveedoresConfiguraciones;
using Negocio.Sistemas.ConfiguracionesSistemas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Extensiones;

namespace SegadorPagosWindowsService
{
    public class ProveedorConfiguracion : IProveedorConfiguracionRastrilleo, IProveedorConfiguracionSincronizacion, Negocio.ServiciosLocales.ProveedoresConfiguraciones.IProveedorConfiguracionSincronizacion
    {
        string IProveedorConfiguracionRastrilleo.ObtenerUrl()
        {
            var servicio = FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesSistemas>();

            var idSistema = typeof(Program).ObtenerGuidEnsamblado();

            if (idSistema.HasValue && idSistema.Value != Guid.Empty)
            {
                var parametro = servicio.ObtenerParametro(idSistema.Value, nameof(ParametrosSincronizacion.UrlWebServiceRastrilleo));

                return Newtonsoft.Json.JsonConvert.DeserializeObject<string>(parametro?.Valor);
            }

            return null;
        }

        string IProveedorConfiguracionSincronizacion.ObtenerUrl()
        {
            var servicio = FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesSistemas>();

            var idSistema = typeof(Program).ObtenerGuidEnsamblado();

            if (idSistema.HasValue && idSistema.Value != Guid.Empty)
            {
                var parametro = servicio.ObtenerParametro(idSistema.Value, nameof(ParametrosSincronizacion.UrlWebServiceCorte));

                return Newtonsoft.Json.JsonConvert.DeserializeObject<string>(parametro?.Valor);
            }

            return null;
        }


        string Negocio.ServiciosLocales.ProveedoresConfiguraciones.IProveedorConfiguracionSincronizacion.ObtenerUrl()
        {
            var servicio = FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesSistemas>();

            var idSistema = typeof(Program).ObtenerGuidEnsamblado();

            if (idSistema.HasValue && idSistema.Value != Guid.Empty)
            {
                var parametroServidor = servicio.ObtenerParametro(idSistema.Value, nameof(ParametrosGeneradorFolios.Servidor));
                var parametroPuerto = servicio.ObtenerParametro(idSistema.Value, nameof(ParametrosGeneradorFolios.Puerto));

                var servidor = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(parametroServidor?.Valor);
                var puerto = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(parametroPuerto?.Valor);

                return string.Format("net.tcp://{0}:{1}/ServicioSincronizacion", servidor, puerto);
            }

            return null;
        }

    }
}
