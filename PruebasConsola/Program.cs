﻿using Negocio.ServiciosLocales.ProveedoresConfiguraciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace PruebasConsola
{
    class Program
    {
        static void Main(string[] args)
        {
            OnTimer(null, null);

            System.Timers.Timer timer;

            timer = new System.Timers.Timer();
            timer.Interval = 900000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnTimer);
            timer.Start();

            var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionSincronizacion>();
            var rutaBase = proveedor.ObtenerUrl();

            var binding = new NetTcpBinding()
            {
                MaxBufferSize = 999999999,
                MaxReceivedMessageSize = 999999999,
                OpenTimeout = TimeSpan.FromMinutes(2),
                ReceiveTimeout = TimeSpan.FromMinutes(210),
                CloseTimeout = TimeSpan.FromMinutes(2),
                SendTimeout = TimeSpan.FromMinutes(10)
            };

            binding.Security.Mode = SecurityMode.None;
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;

            var hostSincronizacion = new ServiceHost(typeof(ServiciosTCP.ServicioSincronizacion), new Uri(rutaBase));
            hostSincronizacion.AddServiceEndpoint(typeof(ServiciosTCP.IServicioSincronizacion), binding, rutaBase);

            hostSincronizacion.Open();
            ConsoleKeyInfo? respuesta = null;

            while (!respuesta.HasValue || respuesta.Value.Key != ConsoleKey.Y)
            {
                Console.Clear();
                Console.ReadKey();
                Console.WriteLine("¿Está seguro de que desea salir? Y/N");
                respuesta = Console.ReadKey();
            }
                
        }

        private static void OnTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            //try
            //{
            //    var servicioRastrilleo = FabricaDependencias.Instancia.Resolver<IServicioRastrilleo>();

            //    servicioRastrilleo.Rastrillar();

            //    var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<IServicioSincronizacion>();

            //    servicioSincronizacion.Sincronizar();
            //    servicioSincronizacion.SincronizarGastos();
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    foreach (var er in ex.EntityValidationErrors)
            //        Transversal.Log.Logger.Error("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
            //}
            //catch (SOTException ex)
            //{
            //    Transversal.Log.Logger.Error("[SOTException] " + ex.Message);
            //}
            //catch (Exception ex)
            //{
            //    for (int i = 0; i < 50 && ex != null; i++, ex = ex.InnerException)
            //    {
            //        Transversal.Log.Logger.Error(ex.Message);
            //    }
            //}

            var servicioSincronizacion = FabricaDependencias.Instancia.Resolver<ServiciosTCP.IServicioSincronizacion>();
            //var servicioSincronizacion = new ServiciosTCP.ServicioSincronizacion();

            servicioSincronizacion.SubirInformacion();
        }
    }
}
