const currentVersion = '0.1.7.6';

function loadChanges() {

    // var currentVersion = '0.1.3.4';

    $.getJSON("cambios/" + currentVersion + '.json', function(data) {
        // changes = data;
        console.log(data);

        let template = $('#desktopTemplate').html();
        // Mustache.parse(template);
        let rendered = Mustache.render(template, data);
        $('#desktopChanges').html(rendered);

        template = $('#databaseTemplate').html();
        // Mustache.parse(template);
        rendered = Mustache.render(template, data);
        $('#databaseChanges').html(rendered);

        template = $('#servicesTemplate').html();
        // Mustache.parse(template);
        rendered = Mustache.render(template, data);
        $('#servicesChanges').html(rendered);

        let ver = {version: currentVersion};

        template = $('#mainTitleTemplate').html();
        // Mustache.parse(template);
        rendered = Mustache.render(template, ver);
        $('#mainTitle').html(rendered);
    });

    
  }