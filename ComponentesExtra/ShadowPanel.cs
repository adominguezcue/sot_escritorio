﻿using System;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ExtraComponents
{
    public class ShadowPanel : Panel
    {


        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            DrawShadow(e);
        }

        public void DrawShadow(PaintEventArgs e)
        {
            Color[] shadow = new Color[4];
            shadow[0] = Color.FromArgb(181, 181, 181);
            shadow[1] = Color.FromArgb(195, 195, 195);
            shadow[2] = Color.FromArgb(211, 211, 211);

            Pen pen = new Pen(shadow[0]);

            using (pen)
            {


                foreach (Control ctrl in this.Controls)
                {
                    if (!ctrl.Visible)
                    {
                        continue;
                    }

                    Point pt = ctrl.Location;
                    pt.Y += ctrl.Height;

                    for (int sp = 0; sp <= 2; sp++)
                    {
                        pen.Color = shadow[sp];
                        //e.Graphics.DrawLine(pen, pt.X, pt.Y, pt.X + ctrl.Width - 1, pt.Y)

                        e.Graphics.DrawLine(pen, pt.X + sp, pt.Y + sp, pt.X + ctrl.Width - 1 + sp, pt.Y + sp);
                        e.Graphics.DrawLine(pen, ctrl.Right + sp, ctrl.Top + sp, ctrl.Right + sp, ctrl.Bottom + sp);
                    }
                }
            }
        }

        //Public Sub DrawBlurShadow()
        //    Dim bmp As Bitmap = New Bitmap(ctrl.Width, ctrl.Height)
        //    BitmapFilter.GaussianBlur(bmp, 4)

        //    Dim pb As New PictureBox()
        //    panel1.Controls.Add(pb)
        //    pb.Image = bmp
        //    pb.Dock = DockStyle.Fill
        //    pb.BringToFront()
        //End Sub

    }
}
