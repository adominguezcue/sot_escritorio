﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace ExtraComponents
{
    public class GradientPanel : Panel
    {
        private Color _backgroundColor1 = Color.White;
        private Color _backgroundColor2 = Color.White;
        private bool _customGradient = false;
        private int _alpha = 0;

        private Rectangle _customGradientRectangle = Rectangle.Empty;

        [Editor()]
        public int Alpha
        {
            get { return _alpha; }
            set
            {
                if (value > 255)
                    _alpha = 255;
                else if (value < 0)
                    _alpha = 0;
                else
                    _alpha = value;

                _backgroundColor1 = Color.FromArgb(_alpha, _backgroundColor1);

                this.Invalidate();
            }
        }

        [Editor()]
        public Color CustomColor
        {
            get { return _backgroundColor2; }
            set
            {
                _backgroundColor2 = value;
                _backgroundColor1 = Color.FromArgb(_alpha, value);
                this.Invalidate();
            }
        }

        [Editor()]
        public bool CustomGradient
        {
            get { return _customGradient; }
            set
            {
                if (value != _customGradient)
                {
                    _customGradient = value;
                    Invalidate();
                }
            }
        }

        [Editor()]
        public int GradientTop
        {
            get { return _customGradientRectangle.Top; }
            set
            {
                if (value < 0)
                    value = 0;
                else if (value > Height)
                    value = Height;

                if (Height - value <= 0)
                    _customGradientRectangle = Rectangle.Empty;
                else
                    _customGradientRectangle = new Rectangle(0, value, Width, Height - value);

                if (CustomGradient)
                    Invalidate();
            }
        }

        private void PaintGradient(PaintEventArgs e)
        {
            var g = e.Graphics;

            if (Width == 0 || Height == 0)
                return;

            if (CustomGradient)
            {
                if (_customGradientRectangle == Rectangle.Empty)
                    return;

                var tmpRect = _customGradientRectangle;
                tmpRect.Inflate(0, 1);

                using (LinearGradientBrush gradBrush = new LinearGradientBrush(tmpRect, _backgroundColor1, _backgroundColor2, LinearGradientMode.Vertical))
                {
                    g.FillRectangle(gradBrush, _customGradientRectangle);
                }
            }
            else
            {
                if (ClientRectangle == Rectangle.Empty)
                    return;

                using (LinearGradientBrush gradBrush = new LinearGradientBrush(ClientRectangle, _backgroundColor1, _backgroundColor2, LinearGradientMode.Vertical))
                {
                    g.FillRectangle(gradBrush, ClientRectangle);
                }
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
            PaintGradient(e);
        }

        protected override void OnResize(EventArgs eventargs)
        {
            GradientTop = GradientTop;
            base.OnResize(eventargs);
        }

    }
}
