﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Windows.Forms;

namespace ExtraComponents
{
    public class CurrencyUpDown : NumericUpDown
    {

        public CurrencyUpDown()
            : base()
        {
            base.DecimalPlaces = DecimalPlaces;
        }


        private bool isEnter = false;
        protected override void OnEnter(EventArgs e)
        {
            isEnter = true;
            base.OnEnter(e);
            base.UpdateEditText();
        }

        protected override void OnLeave(EventArgs e)
        {
            isEnter = false;
            base.OnLeave(e);
            UpdateEditText();
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new int DecimalPlaces
        {
            get { return 2; }
            set { }
        }

        protected override void UpdateEditText()
        {
            if (isEnter)
            {
                base.UpdateEditText();
            }
            else
            {
                ChangingText = true;
                dynamic decimalRegex = new Regex("(${0,1}\\d+([.,]\\d{1,2})?)");
                dynamic m = decimalRegex.Match(this.Text);

                if (m.Success)
                {
                    Text = m.Value;
                }

                ChangingText = false;

                base.UpdateEditText();
                ChangingText = true;

                Text = this.Value.ToString("C");
            }
        }
    }
}