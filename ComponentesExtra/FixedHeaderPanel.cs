﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace ExtraComponents
{
    public class FixedHeaderPanel : Panel
    {

        private Color m_gradientTop = System.Drawing.SystemColors.ControlLight;
        private Color m_gradientBottom = System.Drawing.SystemColors.ControlDark;
        private Color paintGradientTop;
        private Color paintGradientBottom;
        private float rectOutlineWidth;

        private int tolerance = 10;
        public FixedHeaderPanel()
        {
            InitializeComponent();
        }

        [Category("Appearance")]
        [Description("The color to use for the top portion of the gradient fill of the component.")]
        [DefaultValue(typeof(Color), "System.Drawing.SystemColors.ControlLight")]
        public Color GradientTop
        {
            get { return m_gradientTop; }
            set
            {
                m_gradientTop = value;
                SetPaintColors();
                Invalidate();
            }
        }

        [Category("Appearance")]
        [Description("The color to use for the bottom portion of the gradient fill of the component.")]
        [DefaultValue(typeof(Color), "System.Drawing.SystemColors.ControlDark")]
        public Color GradientBottom
        {
            get { return m_gradientBottom; }
            set
            {
                m_gradientBottom = value;
                SetPaintColors();
                Invalidate();
            }
        }


        [Editor()]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        public override string Text
        {
            get { return base.Text; }
            set
            {
                base.Text = value;
                Invalidate();
            }
        }

        private void SetPaintColors()
        {
            if (Enabled)
            {
                if (SystemInformation.HighContrast)
                {
                    paintGradientTop = Color.Black;
                    paintGradientBottom = Color.Black;
                }
                else
                {
                    paintGradientTop = m_gradientTop;
                    paintGradientBottom = m_gradientBottom;
                }
            }
            else
            {
                if (SystemInformation.HighContrast)
                {
                    paintGradientTop = Color.Gray;
                    paintGradientBottom = Color.White;
                }
                else
                {
                    int grayscaleColorTop = Convert.ToInt32(m_gradientTop.GetBrightness() * 255);
                    paintGradientTop = Color.FromArgb(grayscaleColorTop, grayscaleColorTop, grayscaleColorTop);
                    int grayscaleGradientBottom = Convert.ToInt32(m_gradientBottom.GetBrightness() * 255);
                    paintGradientBottom = Color.FromArgb(grayscaleGradientBottom, grayscaleGradientBottom, grayscaleGradientBottom);
                    int grayscaleForeColor = Convert.ToInt32(ForeColor.GetBrightness() * 255);
                    if (grayscaleForeColor > 255 / 2)
                    {
                        grayscaleForeColor -= 60;
                    }
                    else
                    {
                        grayscaleForeColor += 60;
                    }
                }
            }
        }

        private void SetControlSizes()
        {
            int scalingDividend = Math.Min(ClientRectangle.Width, ClientRectangle.Height);
            rectOutlineWidth = Math.Max(1, scalingDividend / 50);
        }

        protected override void OnCreateControl()
        {
            SuspendLayout();
            SetControlSizes();
            SetPaintColors();
            base.OnCreateControl();
            ResumeLayout();
        }

        protected override void OnResize(EventArgs e)
        {
            SetControlSizes();
            this.Invalidate();
            base.OnResize(e);
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            SetPaintColors();
            Invalidate();
            base.OnEnabledChanged(e);
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;

            ButtonRenderer.DrawParentBackground(g, ClientRectangle, this);

            SizeF textSize = TextRenderer.MeasureText(this.Text, this.Font);

            dynamic textRectangle = new Rectangle(0, 0, this.Width, (int)textSize.Height + tolerance);

            dynamic newClientRectangle = new Rectangle(base.ClientRectangle.Location.X, base.ClientRectangle.Location.Y + textRectangle.Height, base.ClientRectangle.Width, base.ClientRectangle.Height - textRectangle.Height);

            using (GraphicsPath outerPath = new GraphicsPath())
            {

                outerPath.AddRectangle(textRectangle);

                using (LinearGradientBrush outerBrush = new LinearGradientBrush(textRectangle, paintGradientTop, paintGradientBottom, LinearGradientMode.Vertical))
                {
                    g.FillPath(outerBrush, outerPath);
                }

            }

            TextRenderer.DrawText(g, Text, Font, textRectangle, ForeColor, Color.Transparent, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis);

            PaintEventArgs ea = new PaintEventArgs(g, newClientRectangle);

            base.OnPaint(ea);
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            //
            //FixedHeaderPanel
            //
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ResumeLayout(false);

        }
    }
}