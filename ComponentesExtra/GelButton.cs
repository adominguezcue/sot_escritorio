﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ExtraComponents
{
    public class GelButton : Button
    {

        #region Fields

        private Color m_gradientTop = Color.FromArgb(255, 44, 85, 177);
        private Color m_gradientBottom = Color.FromArgb(255, 153, 198, 241);
        private Color _gelColor = Color.White;
        private Color paintGradientTop;
        private Color paintGradientBottom;
        private Color paintForeColor;
        private Point _buttonRectangleOrigin;
        private Rectangle highlightRect;
        private Rectangle buttonRectangle;
        private int rectCornerRadius;
        private float rectOutlineWidth;
        private int highlightRectOffset;
        private int defaultHighlightOffset;
        private int highlightAlphaTop = 255;
        private int highlightAlphaBottom;
        private Timer animateButtonHighlightedTimer = new Timer();
        private Timer animateResumeNormalTimer = new Timer();
        private bool increasingAlpha;
        private bool mousePressed = false;
        private Size _imageSize;

        private Point _imageLocation;
        private bool _disableHighlight = false;
        private bool _checkable = false;
        #endregion
        private bool _checked = false;

        public event CheckEventHandler Check;
        public delegate void CheckEventHandler(object sender, CheckGelButtonEventArgs e);

        #region Properties

        [Category("Appearance")]
        [Description("The color to use for the top portion of the gradient fill of the component.")]
        [DefaultValue(typeof(Color), "0x2C55B1")]
        public Color GradientTop
        {
            get { return m_gradientTop; }
            set
            {
                m_gradientTop = value;
                SetPaintColors();
                Invalidate();
            }
        }

        [Category("Appearance")]
        [Description("The color to use for the bottom portion of the gradient fill of the component.")]
        [DefaultValue(typeof(Color), "0x99C6F1")]
        public Color GradientBottom
        {
            get { return m_gradientBottom; }
            set
            {
                m_gradientBottom = value;
                SetPaintColors();
                Invalidate();
            }
        }

        [Category("Appearance")]
        [Description("El tamaño de la imagen")]
        public Size ImageSize
        {
            get { return _imageSize; }
            set
            {
                _imageSize = value;
                Invalidate();
            }
        }

        [Category("Appearance")]
        [Description("La posición de la imagen")]
        public Point ImageLocation
        {
            get { return _imageLocation; }
            set
            {
                _imageLocation = value;
                Invalidate();
            }
        }

        [Category("Appearance")]
        [Description("El área que representa visualmente al botón")]
        public Point ButtonRectangleOrigin
        {
            get
            {
                if (_buttonRectangleOrigin.X >= ClientRectangle.Right | _buttonRectangleOrigin.Y >= ClientRectangle.Bottom)
                {
                    return Point.Empty;
                }
                return _buttonRectangleOrigin;
            }
            set
            {
                _buttonRectangleOrigin = value;
                SetControlSizes();
                Invalidate();
            }
        }

        [Category("Appearance")]
        [Description("Indica si el botón tendrá o no un resaltado")]
        public bool DisableHighlight
        {
            get { return _disableHighlight; }
            set
            {
                if (_disableHighlight != value)
                {
                    _disableHighlight = value;
                    Invalidate();
                }
            }
        }

        [Category("Appearance")]
        [Description("Indica el color del brillo del botón")]
        public Color GelColor
        {
            get { return _gelColor; }
            set
            {
                if (_gelColor != value)
                {
                    _gelColor = value;
                    Invalidate();
                }
            }
        }

        public override Color ForeColor
        {
            get { return base.ForeColor; }
            set
            {
                base.ForeColor = value;
                SetPaintColors();
                Invalidate();
            }
        }

        [Category("Appearance")]
        [Description("Indica si el botón es checable o no")]
        public bool Checkable
        {
            get { return _checkable; }
            set
            {
                _checkable = value;
                Invalidate();
            }
        }

        public bool Checked
        {
            get { return _checked; }
            set
            {
                if (Enabled)
                {
                    CheckGelButtonEventArgs arg = new CheckGelButtonEventArgs(_checked, value);
                    _checked = value;
                    if (Checkable)
                    {
                        Invalidate();
                    }
                    if (Check != null)
                    {
                        Check(this, arg);
                    }
                }
                else
                {
                    _checked = value;
                }
            }
        }

        //Public Property Checked As Boolean
        //    Get
        //        Return _checked
        //    End Get
        //    Set(value As Boolean)
        //        _checked = value
        //        If Checkable Then
        //            Invalidate()
        //        End If
        //    End Set
        //End Property
        #endregion

        #region Initialization and Modification

        protected override void OnCreateControl()
        {
            SuspendLayout();
            SetControlSizes();
            SetPaintColors();
            InitializeTimers();
            base.OnCreateControl();
            ResumeLayout();
        }

        protected override void OnResize(EventArgs e)
        {
            SetControlSizes();
            this.Invalidate();
            base.OnResize(e);
        }

        private void SetControlSizes()
        {
            int scalingDividend = Math.Min(ClientRectangle.Width, ClientRectangle.Height);
            buttonRectangle = new Rectangle(ButtonRectangleOrigin.X, ButtonRectangleOrigin.Y, ClientRectangle.Width - ButtonRectangleOrigin.X, ClientRectangle.Height - ButtonRectangleOrigin.Y);
            rectCornerRadius = Math.Max(1, scalingDividend / 10);
            rectOutlineWidth = Math.Max(1, scalingDividend / 50);
            highlightRect = new Rectangle(ButtonRectangleOrigin.X, ButtonRectangleOrigin.Y, ClientRectangle.Width - ButtonRectangleOrigin.X, (ClientRectangle.Height - ButtonRectangleOrigin.Y) / 2);
            highlightRectOffset = Math.Max(1, scalingDividend / 35);
            defaultHighlightOffset = Math.Max(1, scalingDividend / 35);
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            if (!Enabled)
            {
                animateButtonHighlightedTimer.Stop();
                animateResumeNormalTimer.Stop();
            }
            SetPaintColors();
            Invalidate();
            base.OnEnabledChanged(e);
        }

        private void SetPaintColors()
        {
            if (Enabled)
            {
                if (SystemInformation.HighContrast)
                {
                    paintGradientTop = Color.Black;
                    paintGradientBottom = Color.Black;
                    paintForeColor = Color.White;
                }
                else
                {
                    paintGradientTop = m_gradientTop;
                    paintGradientBottom = m_gradientBottom;
                    paintForeColor = ForeColor;
                }
            }
            else
            {
                if (SystemInformation.HighContrast)
                {
                    paintGradientTop = Color.Gray;
                    paintGradientBottom = Color.White;
                    paintForeColor = Color.Black;
                }
                else
                {
                    int grayscaleColorTop = Convert.ToInt32(m_gradientTop.GetBrightness() * 255);
                    paintGradientTop = Color.FromArgb(grayscaleColorTop, grayscaleColorTop, grayscaleColorTop);
                    int grayscaleGradientBottom = Convert.ToInt32(m_gradientBottom.GetBrightness() * 255);
                    paintGradientBottom = Color.FromArgb(grayscaleGradientBottom, grayscaleGradientBottom, grayscaleGradientBottom);
                    int grayscaleForeColor = Convert.ToInt32(ForeColor.GetBrightness() * 255);
                    if (grayscaleForeColor > 255 / 2)
                    {
                        grayscaleForeColor -= 60;
                    }
                    else
                    {
                        grayscaleForeColor += 60;
                    }
                    paintForeColor = Color.FromArgb(grayscaleForeColor, grayscaleForeColor, grayscaleForeColor);
                }
            }
        }

        private void InitializeTimers()
        {
            animateButtonHighlightedTimer.Interval = 20;
            animateButtonHighlightedTimer.Tick += animateButtonHighlightedTimer_Tick;
            animateResumeNormalTimer.Interval = 5;
            animateResumeNormalTimer.Tick += animateResumeNormalTimer_Tick;
        }

        #endregion

        #region Custom Painting

        protected override void OnPaint(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;

            ButtonRenderer.DrawParentBackground(g, ClientRectangle, this);

            //Color bc = Color.Transparent;

            //var prnt = this.Parent;

            //while (bc == Color.Transparent && prnt != null) 
            //{
            //    bc = prnt.BackColor;
            //    prnt = prnt.Parent;
            //}

            //using (var brush = new SolidBrush(bc)) 
            //{
            //    g.FillRectangle(brush, ClientRectangle);
            //}

            // Paint the outer rounded rectangle
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            using (GraphicsPath outerPath = RoundedRectangle(buttonRectangle, rectCornerRadius, 0))
            {
                using (LinearGradientBrush outerBrush = new LinearGradientBrush(buttonRectangle, paintGradientTop, paintGradientBottom, LinearGradientMode.Vertical))
                {
                    g.FillPath(outerBrush, outerPath);
                }
                using (Pen outlinePen = new Pen(paintGradientTop, rectOutlineWidth))
                {
                    outlinePen.Alignment = PenAlignment.Inset;
                    g.DrawPath(outlinePen, outerPath);
                }
            }
            // If this is the default button, paint an additional highlight
            if (IsDefault)
            {
                using (GraphicsPath defaultPath = new GraphicsPath())
                {
                    defaultPath.AddPath(RoundedRectangle(buttonRectangle, rectCornerRadius, 0), false);
                    defaultPath.AddPath(RoundedRectangle(buttonRectangle, rectCornerRadius, defaultHighlightOffset), false);
                    using (PathGradientBrush defaultBrush = new PathGradientBrush(defaultPath))
                    {
                        defaultBrush.CenterColor = Color.FromArgb(50, Color.White);
                        defaultBrush.SurroundColors = new Color[] { Color.FromArgb(100, Color.White) };
                        g.FillPath(defaultBrush, defaultPath);
                    }
                }
            }

            if (!DisableHighlight)
            {
                // Paint the gel highlight
                using (GraphicsPath innerPath = RoundedRectangle(highlightRect, rectCornerRadius, highlightRectOffset))
                {
                    using (LinearGradientBrush innerBrush = new LinearGradientBrush(highlightRect, Color.FromArgb(highlightAlphaTop, GelColor), Color.FromArgb(highlightAlphaBottom, GelColor), LinearGradientMode.Vertical))
                    {
                        g.FillPath(innerBrush, innerPath);
                    }
                }
            }

            SolidBrush stringBrush = new SolidBrush(this.ForeColor);

            SizeF textSize = g.MeasureString(this.Text, this.Font);

            //if( the Mouse is pressed draw the text and the image, if available,
            // shifted 2 pixels to the right to simulate the 3D effect

            Rectangle textRectangle = default(Rectangle);


            if (this.mousePressed | (Checkable & Checked))
            {
                if (this.Image == null)
                {
                    textRectangle = new Rectangle(new Point(buttonRectangle.X, buttonRectangle.Y + 1), buttonRectangle.Size);
                }
                else
                {
                    dynamic recImage = new Rectangle(new Point(ImageLocation.X, ImageLocation.Y + 1), ImageSize);
                    g.DrawImage(Image, recImage);

                    Rectangle intersection = Rectangle.Intersect(buttonRectangle, recImage);

                    textRectangle = new Rectangle(new Point(buttonRectangle.X, buttonRectangle.Y + intersection.Height + 1), Size.Subtract(buttonRectangle.Size, new Size(0, intersection.Height)));
                }

                //Dim strFormat As New StringFormat
                //strFormat.Alignment = StringAlignment.Center


                //' Paint the text
                //g.DrawString(Text, Font, New SolidBrush(ForeColor), textRectangle, strFormat)
            }
            else
            {
                if (Image == null)
                {
                    textRectangle = buttonRectangle;
                    //g.DrawString(Me.Text, Me.Font, stringBrush, (((Me.Width + 3) - textSize.Width) / 2), (((Me.Height + 2) - textSize.Height) / 2))
                }
                else
                {
                    dynamic recImage = new Rectangle(ImageLocation, ImageSize);
                    g.DrawImage(Image, recImage);

                    Rectangle intersection = Rectangle.Intersect(buttonRectangle, recImage);

                    textRectangle = new Rectangle(new Point(buttonRectangle.X, buttonRectangle.Y + intersection.Height), Size.Subtract(buttonRectangle.Size, new Size(0, intersection.Height)));

                    //Dim pt = New Point((Me.Width + 3) / 12, (Me.Height + 2 - 16) / 2)
                    //g.DrawString(Me.Text, Me.Font, stringBrush, recString.X + recString.Width + 3, (((Me.Height + 2) - textSize.Height) / 2))
                }

                //Dim strFormat As New StringFormat
                //strFormat.Alignment = StringAlignment.Center


                //' Paint the text
                //g.DrawString(Text, Font, New SolidBrush(ForeColor), buttonRectangle, strFormat)
            }

            TextRenderer.DrawText(g, Text, Font, textRectangle, paintForeColor, Color.Transparent, TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.EndEllipsis);

        }

        private static GraphicsPath RoundedRectangle(Rectangle boundingRect, int cornerRadius, int margin)
        {
            GraphicsPath roundedRect = new GraphicsPath();
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 180, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + margin, cornerRadius * 2, cornerRadius * 2, 270, 90);
            roundedRect.AddArc(boundingRect.X + boundingRect.Width - margin - cornerRadius * 2, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
            roundedRect.AddArc(boundingRect.X + margin, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 90, 90);
            roundedRect.AddLine(boundingRect.X + margin, boundingRect.Y + boundingRect.Height - margin - cornerRadius * 2, boundingRect.X + margin, boundingRect.Y + margin + cornerRadius);
            roundedRect.CloseFigure();
            return roundedRect;
        }

        #endregion

        #region Mouse and Keyboard Interaction

        protected override void OnMouseEnter(EventArgs e)
        {
            HighlightButton();
            base.OnMouseEnter(e);
        }

        protected override void OnGotFocus(EventArgs e)
        {
            HighlightButton();
            base.OnGotFocus(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            ResumeNormalButton();
            base.OnMouseLeave(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            ResumeNormalButton();
            base.OnLostFocus(e);
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            PressButton();
            base.OnMouseDown(mevent);
        }

        protected override void OnKeyDown(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space || kevent.KeyCode == Keys.Return)
            {
                PressButton();
            }
            base.OnKeyDown(kevent);
        }

        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            ReleaseButton();
            if (DisplayRectangle.Contains(mevent.Location))
            {
                HighlightButton();
            }
            base.OnMouseUp(mevent);
        }

        protected override void OnKeyUp(KeyEventArgs kevent)
        {
            if (kevent.KeyCode == Keys.Space || kevent.KeyCode == Keys.Return)
            {
                ReleaseButton();
                if (IsDefault)
                {
                    HighlightButton();
                }
            }
            base.OnKeyUp(kevent);
        }

        protected override void OnMouseMove(MouseEventArgs mevent)
        {
            if (Enabled && (mevent.Button & MouseButtons.Left) == MouseButtons.Left && !ClientRectangle.Contains(mevent.Location))
            {
                ReleaseButton();
            }
            base.OnMouseMove(mevent);
        }

        private void PressButton()
        {
            if (Enabled)
            {
                animateButtonHighlightedTimer.Stop();
                animateResumeNormalTimer.Stop();
                highlightRect.Location = new Point(buttonRectangle.X, buttonRectangle.Y + buttonRectangle.Height / 2);
                highlightAlphaTop = 0;
                highlightAlphaBottom = 200;
                Checked = !Checked;
                Invalidate();
            }
            mousePressed = true;
        }

        private void ReleaseButton()
        {
            if (Enabled)
            {
                highlightRect.Location = new Point(buttonRectangle.X, buttonRectangle.Y);
                highlightAlphaTop = 255;
                highlightAlphaBottom = 0;
            }
            mousePressed = false;
        }

        private void HighlightButton()
        {
            if (Enabled)
            {
                animateResumeNormalTimer.Stop();
                animateButtonHighlightedTimer.Start();
            }
        }

        private void ResumeNormalButton()
        {
            if (Enabled)
            {
                animateButtonHighlightedTimer.Stop();
                animateResumeNormalTimer.Start();
            }
        }

        private void animateButtonHighlightedTimer_Tick(object sender, EventArgs e)
        {
            if (increasingAlpha)
            {
                if (100 <= highlightAlphaBottom)
                {
                    increasingAlpha = false;
                }
                else
                {
                    highlightAlphaBottom += 5;
                }
            }
            else
            {
                if (0 >= highlightAlphaBottom)
                {
                    increasingAlpha = true;
                }
                else
                {
                    highlightAlphaBottom -= 5;
                }
            }
            Invalidate();
        }

        private void animateResumeNormalTimer_Tick(object sender, EventArgs e)
        {
            bool modified = false;
            if (highlightAlphaBottom > 0)
            {
                highlightAlphaBottom -= 5;
                modified = true;
            }
            if (highlightAlphaTop < 255)
            {
                highlightAlphaTop += 5;
                modified = true;
            }
            if (!modified)
            {
                animateResumeNormalTimer.Stop();
            }
            Invalidate();
        }

        #endregion

        public class CheckGelButtonEventArgs : EventArgs
        {

            public CheckGelButtonEventArgs(bool oldValue, bool newValue)
            {
                Value = newValue;
                this.OldValue = oldValue;
            }

            public readonly bool Value;

            public readonly bool OldValue;
        }
    }
}
