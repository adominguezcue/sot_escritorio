﻿using System;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExtraComponents
{
    public partial class CurveLabel : Label
    {
        private int minWidth;
        private Color borderTLColor;
        private Color borderBRColor;
        private int m_borderSize;

        private BorderStyle m_borderStyle;
        public int BordersWidth
        {
            get { return minWidth / 2; }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }

                minWidth = value * 2;

                if (MinimumSize.Width < value * 2)
                {
                    MinimumSize = new Size(Convert.ToInt32(value) * 2, MinimumSize.Height);
                }

                if (MinimumSize.Height < value * 2)
                {
                    MinimumSize = new Size(MinimumSize.Width, Convert.ToInt32(value) * 2);
                }

                Invalidate();
            }
        }

        public override Size MinimumSize
        {
            get { return base.MinimumSize; }
            set
            {
                if (value.Width < minWidth)
                {
                    value.Width = minWidth;
                }

                if (value.Height < minWidth)
                {
                    value.Height = minWidth;
                }

                base.MinimumSize = value;
            }
        }

        //[RefreshProperties(RefreshProperties.Repaint)]
        //public BorderStyle BorderStyle { get; set; }

        [RefreshProperties(RefreshProperties.All)]
        public int BorderSize
        {
            get { return m_borderSize; }
            set
            {
                if (value <= 0)
                {
                    m_borderSize = 1;
                }
                else
                {
                    m_borderSize = value;
                }
            }
        }

        public CurveLabel()
        {
            //InitializeComponent()
            minWidth = 40;
            borderTLColor = Color.Black;
            borderBRColor = Color.White;
            m_borderSize = 5;
            BorderStyle = System.Windows.Forms.BorderStyle.None;
        }

        [RefreshProperties(RefreshProperties.Repaint)]
        public override BorderStyle BorderStyle
        {
            get { return m_borderStyle; }

            set
            {
                if (m_borderStyle != value)
                {
                    if (!Enum.IsDefined(typeof(BorderStyle), value))
                    {
                        throw new InvalidEnumArgumentException("value", Convert.ToInt32(value), typeof(BorderStyle));
                    }

                    m_borderStyle = value;
                    UpdateStyles();
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            using (GraphicsPath graphicsPath = new System.Drawing.Drawing2D.GraphicsPath())
            {
                if (minWidth > 0)
                {
                    dynamic rectLT = new Rectangle(0, 0, minWidth, minWidth);
                    dynamic rectLB = new Rectangle(0, this.Height - minWidth, minWidth, minWidth);
                    dynamic rectRT = new Rectangle(this.Width - minWidth, 0, minWidth, minWidth);
                    dynamic rectRB = new Rectangle(this.Width - minWidth, this.Height - minWidth, minWidth, minWidth);


                    graphicsPath.AddPie(rectLT, 180, 90);
                    graphicsPath.AddPie(rectLB, 180, -90);

                    graphicsPath.AddPie(rectRT, 0, -90);
                    graphicsPath.AddPie(rectRB, 0, 90);








                    graphicsPath.AddPolygon(new Point[] {
                    new Point(minWidth / 2, 0),
                    new Point(this.Width - minWidth / 2, 0),
                    new Point(this.Width - minWidth / 2, minWidth / 2),
                    new Point(this.Width, minWidth / 2),
                    new Point(this.Width, this.Height - minWidth / 2),
                    new Point(this.Width - minWidth / 2, this.Height - minWidth / 2),
                    new Point(this.Width - minWidth / 2, this.Height),
                    new Point(minWidth / 2, this.Height),
                    new Point(minWidth / 2, this.Height - minWidth / 2),
                    new Point(0, this.Height - minWidth / 2),
                    new Point(0, minWidth / 2),
                    new Point(minWidth / 2, minWidth / 2)
                });


                    this.Region = new Region(graphicsPath);
                }
                else
                {
                    dynamic rectangle = new RectangleF(0, 0, this.Width, this.Height);


                    graphicsPath.AddRectangle(rectangle);

                    this.Region = new Region(graphicsPath);
                }
            }




            base.OnPaint(e);

            //Using stringBrush = New SolidBrush(Me.ForeColor)
            // Dim textSize As SizeF = e.Graphics.MeasureString(Me.Text, Me.Font)

            // e.Graphics.DrawString(Me.Text, Me.Font, stringBrush, minWidth / 2, minWidth / 2 - textSize.Height)
            //End Using
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle = cp.ExStyle & (~NativeMethods.WS_EX_CLIENTEDGE);
                cp.Style = cp.Style & (~NativeMethods.WS_BORDER);

                switch (m_borderStyle)
                {
                    case BorderStyle.Fixed3D:
                        cp.ExStyle = cp.ExStyle | NativeMethods.WS_EX_CLIENTEDGE;
                        break; 
                    case BorderStyle.FixedSingle:
                        cp.Style = cp.Style | NativeMethods.WS_BORDER;
                        break; 
                }
                return cp;
            }
        }
    }

    class NativeMethods
    {
        public const int WS_EX_CLIENTEDGE = 0x200;
        public const int WS_BORDER = 0x800000;
    }
}
