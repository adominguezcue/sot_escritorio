﻿using Negocio.ServiciosLocales.ProveedoresConfiguraciones;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace GeneracionFoliosWpf
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer;

        ServiceHost hostSincronizacion;
        LoggerWPF log;
        ObservableCollection<string> listaLogs;

        object bloqueo = new object();
        int contadorReintentos = 0;

        public MainWindow()
        {
            InitializeComponent();
            log = new LoggerWPF();
            listaLogs = new ObservableCollection<string>();
            logs.ItemsSource = listaLogs;
        }

        private void btnDetener_Click(object sender, RoutedEventArgs e)
        {
            Detener();
        }

        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            Abrir();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Detener();
            LoggerWPF.EscribirLog = null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                LoggerWPF.EscribirLog = new Action<string>((mensaje) =>
                {
                    // Simulate some work taking place 
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                (ThreadStart)delegate ()
                                {
                                    listaLogs.Add(mensaje);

                                    if (listaLogs.Count > 100)
                                        listaLogs.RemoveAt(0);
                                }
                                  );

                });

                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMinutes(1);
                timer.Tick += Timer_Tick;
                try
                {
                    Timer_Tick(null, null);
                }
                catch
                {
                }
                if (hostSincronizacion == null || hostSincronizacion.State != CommunicationState.Opened)
                    timer.Start();

            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            lock (bloqueo)
            {
                if (contadorReintentos++ < 5)
                {
                    try
                    {
                        Abrir();
                        timer.Stop();
                    }
                    catch (Exception ex)
                    {
                        Transversal.Log.Logger.Error(ex.Message);
                        Transversal.Log.Logger.Info($"Intento {contadorReintentos} de iniciar servicios fallida, reintentando...");
                    }
                }
                else
                {
                    timer.Stop();

                    Transversal.Log.Logger.Info("El generador de folios no pudo iniciar de forma automática, inicialice a mano o revise la configuración");
                }

            }
        }

        private void Abrir()
        {
            if (hostSincronizacion == null || hostSincronizacion.State != CommunicationState.Opened)
            {
                var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionPreventas>();
                var rutaBase = proveedor.ObtenerUrl();

                var binding = new NetTcpBinding()
                {
                    MaxBufferSize = 999999999,
                    MaxReceivedMessageSize = 999999999,
                    OpenTimeout = TimeSpan.FromMinutes(2),
                    ReceiveTimeout = TimeSpan.FromMinutes(210),
                    CloseTimeout = TimeSpan.FromMinutes(2),
                    SendTimeout = TimeSpan.FromMinutes(10),
                    //TransactionFlow = false
                };

                binding.Security.Mode = SecurityMode.None;
                binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;

                hostSincronizacion = new ServiceHost(typeof(ServiciosTCP.Preventas.ServicioPreventas), new Uri(rutaBase));
                //hostSincronizacion.AddServiceEndpoint(typeof(ServiciosTCP.Preventas.IServicioPreventas), binding, rutaBase);
                var smv = hostSincronizacion.Description.Behaviors.Find<ServiceMetadataBehavior>();

                if (smv == null)
                    hostSincronizacion.Description.Behaviors.Add(new ServiceMetadataBehavior { HttpGetEnabled = false });
                else
                    smv.HttpGetEnabled = false;

                var sdb = hostSincronizacion.Description.Behaviors.Find<ServiceDebugBehavior>();

                if (sdb == null)
                    hostSincronizacion.Description.Behaviors.Add(new ServiceDebugBehavior { IncludeExceptionDetailInFaults = false });
                else
                    sdb.IncludeExceptionDetailInFaults = false;

                hostSincronizacion.AddServiceEndpoint(typeof(ServiciosTCP.Preventas.IServicioPreventas), binding, rutaBase);
                hostSincronizacion.AddServiceEndpoint(typeof(IMetadataExchange), new CustomBinding(new TcpTransportBindingElement()), "MEX");

                hostSincronizacion.Open();


                this.Title = $"Log de generación de folios {rutaBase}";
                Transversal.Log.Logger.Info("Servicios iniciados");
            }

            btnDetener.IsEnabled = true;
            btnIniciar.IsEnabled = true;
        }

        private void Detener()
        {
            if (hostSincronizacion != null && hostSincronizacion.State == CommunicationState.Opened)
            {
                try
                {
                    hostSincronizacion.Close();
                }
                catch
                {
                    hostSincronizacion.Abort();
                }

                this.Title = "Log de generación de folios";
                Transversal.Log.Logger.Info("Servicios finalizados");
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (System.Windows.MessageBox.Show("¿Está seguro de que desea salir?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                e.Cancel = true;

            base.OnClosing(e);
        }
    }
}
