﻿using Modelo.Sistemas.Entidades.Dtos;
using Negocio.ServiciosLocales.ProveedoresConfiguraciones;
using Negocio.Sistemas.ConfiguracionesSistemas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Extensiones;

namespace GeneracionFoliosWpf
{
    public class ProveedorConfiguracion: IProveedorConfiguracionPreventas
    {
        string IProveedorConfiguracionPreventas.ObtenerUrl()
        {
            var servicio = FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesSistemas>();

            var idSistema = typeof(App).ObtenerGuidEnsamblado();

            if (idSistema.HasValue && idSistema.Value != Guid.Empty)
            {
                var parametroServidor = servicio.ObtenerParametro(idSistema.Value, nameof(ParametrosGeneradorFolios.Servidor));
                var parametroPuerto = servicio.ObtenerParametro(idSistema.Value, nameof(ParametrosGeneradorFolios.Puerto));

                var servidor = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(parametroServidor?.Valor);
                var puerto = Newtonsoft.Json.JsonConvert.DeserializeObject<int>(parametroPuerto?.Valor);

                return string.Format("net.tcp://{0}:{1}/ServicioPreventas", servidor, puerto);
            }

            return null;
        }
    }
}
