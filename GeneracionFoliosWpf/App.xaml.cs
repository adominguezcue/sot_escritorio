﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;
using Transversal.Excepciones;
using Transversal.Excepciones.Seguridad;

namespace GeneracionFoliosWpf
{
    public partial class App : Application
    {
        /// <summary>
        /// Application Entry Point.
        /// </summary>
        [System.STAThreadAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        public static void Main(string[] args)
        {
            GeneracionFoliosWpf.App app = new GeneracionFoliosWpf.App();
            app.InitializeComponent();
            app.Run();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }

        public App()
        {
            Startup += new StartupEventHandler(App_Startup); // Can be called from XAML


            DispatcherUnhandledException += App_DispatcherUnhandledException; //Example 2

            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException; //Example 4

           Timeline.DesiredFrameRateProperty.OverrideMetadata(
                typeof(Timeline),
                new FrameworkPropertyMetadata { DefaultValue = 30 }
);
        }


        void App_Startup(object sender, StartupEventArgs e)
        {
            //Here if called from XAML, otherwise, this code can be in App()
            //AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException; // Example 1
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; // Example 3
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }

        private System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null;
        }

        // Example 1
        void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            ProcesarError(e.Exception);
            //MessageBox.Show("1. CurrentDomain_FirstChanceException");
            //ProcessError(e.Exception);   - This could be used here to log ALL errors, even those caught by a Try/Catch block
        }

        // Example 2
        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            ProcesarError(e.Exception);
            e.Handled = true;
        }

        private void ProcesarError(Exception ex, bool procesoParalelo = false)
        {
            //#if DEBUG
            try
            {
                if (ex.GetType() == typeof(DbEntityValidationException))
                {
                    var excepcion = (DbEntityValidationException)ex;

                    foreach (var er in excepcion.EntityValidationErrors)
                        Transversal.Log.Logger.Fatal("[DbEntityValidationException] " + string.Join("|", er.ValidationErrors.Select(m => m.ErrorMessage).ToList()));
                }
                else if (ex.GetType() != typeof(SOTException))
                {
                    var exCurrent = ex;

                    for (int i = 0; i < 50 && exCurrent != null; i++, exCurrent = exCurrent.InnerException)
                    {
                        Transversal.Log.Logger.Fatal(exCurrent.Message);
                    }
                }
            }
            catch { }
            //#endif
            if (ex.GetType() == typeof(SOTPermisosException))
            {
                var excepcion = (SOTPermisosException)ex;

                StringBuilder builder = new StringBuilder();
                builder.AppendLine(excepcion.Message);
                builder.AppendLine();

                foreach (var error in excepcion.Permisos)
                    builder.AppendLine(error);

                MessageBox.Show(builder.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Example 3
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //MessageBox.Show("3. CurrentDomain_UnhandledException");
            var exception = e.ExceptionObject as Exception;
            ProcesarError(exception, true);
            //if (e.IsTerminating)
            //{
            //    //Now is a good time to write that critical error file!
            //    MessageBox.Show("Goodbye world!");
            //}
        }

        // Example 4
        void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            foreach (var ex in e.Exception.InnerExceptions)
                ProcesarError(ex, true);

            e.SetObserved();
        }

        // Example 5
        void WinFormApplication_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
