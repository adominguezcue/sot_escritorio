﻿using Negocio.ServiciosLocales.lsSincronizacion;
using Negocio.ServiciosLocales.ProveedoresConfiguraciones;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace Negocio.ServiciosLocales.Agentes.Sincronizacion
{
    public class AgenteLsSincronizacion
    {
        readonly string nombreServicio;
        private bool usarNombre;
        private NetTcpBinding binding;
        private EndpointAddress endPoint;

        //public AgenteLsSincronizacion()
        //{
        //    try
        //    {
        //        ClientSection clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
        //        for (int i = 0; i < clientSection.Endpoints.Count; i++)
        //        {
        //            if (clientSection.Endpoints[i].Contract == "lsSincronizacion.IServicioSincronizacion")
        //            {
        //                nombreServicio = clientSection.Endpoints[i].Name;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("Ocurrió un error al intentar inicializar el agente de sincronización, consulte la excepción interna para obtener más detalles.", e);
        //    }
        //}

        public AgenteLsSincronizacion()
        {
            usarNombre = false;

            try
            {
                var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionSincronizacion>();

                var urlServicio = proveedor.ObtenerUrl();

                binding = new NetTcpBinding()
                {
                    MaxBufferSize = 999999999,
                    MaxReceivedMessageSize = 999999999,
                    OpenTimeout = TimeSpan.FromMinutes(2),
                    ReceiveTimeout = TimeSpan.FromMinutes(210),
                    CloseTimeout = TimeSpan.FromMinutes(2),
                    SendTimeout = TimeSpan.FromMinutes(10)
                };

                binding.Security.Mode = SecurityMode.None;
                binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;

                endPoint = new EndpointAddress(urlServicio);
            }
            catch (Exception e)
            {
                throw new Exception("Ocurrió un error al intentar inicializar el agente de sincronización, consulte la excepción interna para obtener más detalles.", e);
            }
        }

        public async Task SubirInformacionAsync()
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    await servicio.SubirInformacionAsync();
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        private ServicioSincronizacionClient ObtenerClient()
        {
            if (usarNombre)
                return new ServicioSincronizacionClient(nombreServicio);

            return new ServicioSincronizacionClient(binding, endPoint);
        }
    }
}
