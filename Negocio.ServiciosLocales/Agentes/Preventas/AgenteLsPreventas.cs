﻿using Negocio.ServiciosLocales.lsPreventas;
using Negocio.ServiciosLocales.ProveedoresConfiguraciones;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Excepciones;

namespace Negocio.ServiciosLocales.Agentes.Preventas
{
    public class AgenteLsPreventas
    {
        readonly string nombreServicio;
        private bool usarNombre;
        private NetTcpBinding binding;
        private EndpointAddress endPoint;

        //public AgenteLsPreventas()
        //{
        //    try
        //    {
        //        ClientSection clientSection = (ClientSection)ConfigurationManager.GetSection("system.serviceModel/client");
        //        for (int i = 0; i < clientSection.Endpoints.Count; i++)
        //        {
        //            if (clientSection.Endpoints[i].Contract == "lsPreventas.IServicioPreventas")
        //            {
        //                nombreServicio = clientSection.Endpoints[i].Name;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("Ocurrió un error al intentar inicializar el agente de sincronización, consulte la excepción interna para obtener más detalles.", e);
        //    }
        //}

        public AgenteLsPreventas()
        {
            usarNombre = false;

            try
            {
                var proveedor = FabricaDependencias.Instancia.Resolver<IProveedorConfiguracionPreventas>();

                var urlServicio = proveedor.ObtenerUrl();

                binding = new NetTcpBinding()
                {
                    MaxBufferSize = 999999999,
                    MaxReceivedMessageSize = 999999999,
                    OpenTimeout = TimeSpan.FromMinutes(2),
                    ReceiveTimeout = TimeSpan.FromMinutes(210),
                    CloseTimeout = TimeSpan.FromMinutes(2),
                    SendTimeout = TimeSpan.FromMinutes(10)
                };

                binding.Security.Mode = SecurityMode.None;
                binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.None;

                endPoint = new EndpointAddress(urlServicio);
            }
            catch (Exception e)
            {
                throw new Exception("Ocurrió un error al intentar inicializar el agente de sincronización, consulte la excepción interna para obtener más detalles.", e);
            }
        }

        public int SubirInformacionAsync(VentaClasificacionesVenta clasificacion)
        {
            using (var servicio = ObtenerClient())
            {
                try
                {
                    return servicio.GrenerarPreventa(clasificacion);
                }
                catch (FaultException<lsPreventas.DatosError> ex) 
                {
                    if (ex.Detail.Controlado)
                        throw new SOTException(ex.Detail.Error);
                    else
                        throw new Exception(ex.Detail.Error);
                }
                finally
                {
                    try
                    {
                        servicio.Close();
                    }
                    catch
                    {
                        servicio.Abort();
                    }
                }
            }
        }

        private ServicioPreventasClient ObtenerClient()
        {
            if (usarNombre)
                return new ServicioPreventasClient(nombreServicio);

            return new ServicioPreventasClient(binding, endPoint);
        }
    }
}
