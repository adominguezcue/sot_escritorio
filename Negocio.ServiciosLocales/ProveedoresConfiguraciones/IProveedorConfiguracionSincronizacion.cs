﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.ServiciosLocales.ProveedoresConfiguraciones
{
    public interface IProveedorConfiguracionSincronizacion
    {
        string ObtenerUrl();
    }
}
