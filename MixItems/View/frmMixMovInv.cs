﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZctTools.Controller;
using ZctTools.Forms;
using MixItems.Controller;
using MixItems.Model;

namespace MixItems.View
{
    public partial class frmMixMovInv : Form
    {
        int iColArt = 0;
        int iDescArt = 1;
        int iArtExist = 2;
        int iColAlm = 3;
        int iColCant = 4;
        int iColCost = 5;
        int iColSubTot = 6;
        int desBusqueda = 1;
        int costBusqueda = 3;
        int almtBusqueda = 7;
        int existMixItem = 1;
        int codMixItem = 3;
        int cantMixItem = 6;
        int baseMixItem = 7;
        bool backRow = false;
        bool isExist = false;
        string _conn = "";
        private void frmMixInv_Load(object sender, EventArgs e)
        {
            ZctToolService service = new ZctToolService(_conn);
            DataGridViewComboBoxColumn ColAlm = (DataGridViewComboBoxColumn)DGridArticulos.Columns[iColAlm];
            ColAlm.DataSource = service.GetCatAlm(0);
            ColAlm.ValueMember = "CodAlm";
            ColAlm.DisplayMember = "DescCatAlm";
            cboTypeMov.DataSource = new List<TypeMovMix>() { new TypeMovMix() { IdType = "OP", TypeDesc = "Entrada" }, new TypeMovMix() { IdType = "OS", TypeDesc = "Salida" } };
            cboTypeMov.ValueMember = "IdType";
            cboTypeMov.DisplayMember = "TypeDesc";
            zctControlFecha1.Value = DateTime.Now;
            txtFolio.Text = GetFolio().ToString();

            btnSave.Enabled = false;
            btnClean.Enabled = false;
            DGridArticulos.Enabled = false;
            cboTypeMov.Enabled = false;
            zctControlFecha1.Enabled = false;
        }

        private void DGridArticulos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == iColArt)
                {
                    var exist = ((DataTable)DGridArticulos.DataSource).AsEnumerable().Where(row => row[iColArt] != DBNull.Value && ((string)row[iColArt]).Equals((string)DGridArticulos.CurrentCell.Value)).FirstOrDefault();
                    if ((string)DGridArticulos.CurrentCell.Value != "" && (exist == null || exist[iDescArt].ToString() == ""))
                    {
                        DataTable dataArt = new ZctToolService(_conn).GetData("SP_ZctCatArt_Busqueda_Mix", "@Codigo;VARCHAR", (string)DGridArticulos.CurrentCell.Value);
                        if (dataArt.Rows.Count <= 0)
                        {
                            MessageBox.Show("El código de artículo no existe o no tiene las existencias necesarias para formarlo, verifique por favor.");
                            DGridArticulos.CurrentCell.Value = "";
                        }
                        else
                        {
                            DGridArticulos.CurrentRow.Cells[iDescArt].Value = dataArt.Rows[0].ItemArray[desBusqueda];
                            DGridArticulos.CurrentRow.Cells[iArtExist].Value = dataArt.Rows[0].ItemArray[iArtExist];
                            DGridArticulos.CurrentRow.Cells[iColCost].Value = dataArt.Rows[0].ItemArray[costBusqueda];
                            backRow = true;
                        }
                        dataArt.Dispose();
                        GetTotal();
                    }
                    else
                    {
                        MessageBox.Show("El articulo ya esta en lista");
                        isExist = true;
                    }
                }
                if (e.ColumnIndex == iColCant)
                {
                    try
                    {
                        var miss = (from row in new MixService(_conn).GetData("GetMixItemsCostTable", "@Cod_Art;varchar|@Cod_Alm;int", DGridArticulos.CurrentRow.Cells[iColArt].Value + "|" + DGridArticulos.CurrentRow.Cells[iColAlm].Value).Rows.Cast<DataRow>()
                                    where ((decimal)row.ItemArray[cantMixItem] / (decimal)row.ItemArray[baseMixItem]) * (decimal)DGridArticulos.CurrentRow.Cells[iColCant].Value > (decimal)row.ItemArray[existMixItem]
                                    select row.ItemArray[codMixItem]).ToArray();

                        if (miss.Count() == 0)
                        {
                            DGridArticulos.CurrentRow.Cells[iColSubTot].Value = (decimal)DGridArticulos.CurrentRow.Cells[iColCant].Value * (decimal)DGridArticulos.CurrentRow.Cells[iColCost].Value;
                            GetTotal();
                        }
                        else
                        {
                            MessageBox.Show("No hay suficiente existencia del articulo " + miss[0]);
                            DGridArticulos.CurrentRow.Cells[iColSubTot].Value = (decimal)0;
                            DGridArticulos.CurrentRow.Cells[iColCant].Value = (decimal)0;
                            GetTotal();
                        }
                    }
                    catch (Exception ex)
                    {
                        DGridArticulos.CurrentRow.Cells[iColSubTot].Value = (decimal)0;
                        DGridArticulos.CurrentRow.Cells[iColCant].Value = (decimal)0;
                        GetTotal();
                    }
                }
                if (e.ColumnIndex == iColAlm)
                {
                    try
                    {
                        decimal exist = (decimal)(new ZctToolService(_conn).GetData("SP_ZctArtXAlm", "@TpConsulta;int|@Cod_Art;varchar|@Cod_Alm;int|@Exist_Art;int|@CostoProm_Art;decimal", "1|" + DGridArticulos.CurrentRow.Cells[iColArt].Value + "|" + DGridArticulos.CurrentRow.Cells[iColAlm].Value + "|0|0.0").Rows[0][2]);
                        DGridArticulos.CurrentRow.Cells[iArtExist].Value = exist;
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DGridArticulos_SelectionChanged(object sender, EventArgs e)
        {
            try
            {


                if (backRow)
                {
                    backRow = false;
                    DGridArticulos.CurrentCell = DGridArticulos.Rows[DGridArticulos.CurrentRow.Index - 1].Cells[iColCant];
                    DGridArticulos.CurrentCell.Selected = true;
                }
                if (isExist)
                {
                    isExist = false;
                    DGridArticulos.CurrentCell = DGridArticulos.Rows[DGridArticulos.CurrentRow.Index - 1].Cells[iColArt];
                    DGridArticulos.CurrentCell.Value = "";
                    DGridArticulos.CurrentCell.Selected = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void ZctSotGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F3 && DGridArticulos.CurrentCell.ColumnIndex == iColArt)
                {
                    ZctBusqueda fBusqueda = new ZctBusqueda(_conn);
                    fBusqueda.sSPName = "SP_ZctCatArt_Busqueda_Mix";
                    fBusqueda.sSpVariables = "@Codigo;VARCHAR";
                    fBusqueda.sSPParametros = "0";
                    fBusqueda.ShowDialog(this);
                    if (fBusqueda.iValor != null)
                    {
                        DGridArticulos.BeginEdit(true);
                        DGridArticulos.CurrentCell.Value = fBusqueda.iValor;
                        DGridArticulos.EndEdit();
                        if (!isExist && !backRow)
                        {
                            DGridArticulos.UpdateCellValue(iColArt, DGridArticulos.CurrentCell.RowIndex);
                            DGridArticulos.CurrentCell = DGridArticulos.CurrentRow.Cells[iColCant];
                            DGridArticulos.CurrentCell.Selected = true;
                        }
                        else
                        {
                            if (isExist && backRow)
                                DGridArticulos.CurrentCell.Value = "";
                            isExist = false;
                            backRow = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DGridArticulos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex == iColCant)
            {
                MessageBox.Show("Indique un valor valido para la cantidad del articulo");
            }
            else
            {
                MessageBox.Show("Ocurrio un error inesperado, comuniquese con el personal de desarrollo");
            }
        }

        private void GetTotal()
        {
            if (DGridArticulos.Rows.Count > 0)
            {

                lblTotal.Text = (from row in DGridArticulos.Rows.Cast<DataGridViewRow>()
                                 where row.Cells[iColCant].Value != DBNull.Value && row.Cells[iColCant].Value != null
                                 select (decimal)row.Cells[iColCant].Value * (decimal)row.Cells[iColCost].Value).Sum().ToString("C2");
            }

        }

        private void clearControls()
        {
            try
            {
                txtFolio.Text = "";
                txtFolio.Enabled = true;
                if (DGridArticulos.DataSource != null)
                    ((DataTable)DGridArticulos.DataSource).Rows.Clear();
                DGridArticulos.Enabled = false;
                btnSave.Enabled = false;
                btnClean.Enabled = false;
                cboTypeMov.SelectedIndex = 0;
                cboTypeMov.Enabled = false;
                zctControlFecha1.Value = DateTime.Now;
                zctControlFecha1.Enabled = false;
                lblTotal.Text = "0";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error clear: " + ex.Message);
            }
        }
        private void txtFolio_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtFolio_OnLostFocus(object sender, EventArgs e)
        {
            if (txtFolio.Text != "" && txtFolio.Text != "0")
            {
                MixService service = new MixService(_conn);
                btnClean.Enabled = true;
                ZctEncOP enc = service.GetEncOP(int.Parse(txtFolio.Text));
                if (enc != null)
                    GetOPData(enc.CodOP);
                else
                {
                    DGridArticulos.DataSource = service.GetData("GetDetOPTable", "@CodOP;INT", txtFolio.Text);
                    txtFolio.Enabled = false;
                    btnSave.Enabled = true;
                    DGridArticulos.Enabled = true;
                    cboTypeMov.Enabled = true;
                    zctControlFecha1.Enabled = true;
                    cboTypeMov.Focus();
                }
            }
            else
            {
                txtFolio.Text = "0";
                txtFolio.Focus();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var without = ((DataTable)DGridArticulos.DataSource).AsEnumerable().Where(row => row[iColCant].ToString() == "").FirstOrDefault();
                var withoutAlm = ((DataTable)DGridArticulos.DataSource).AsEnumerable().Where(row => row[almtBusqueda].ToString() == "" && row[iColCant].ToString() != "0").FirstOrDefault();
                if (without == null && withoutAlm == null)
                {
                    ZctEncOP enc = new ZctEncOP() { CodOP = int.Parse(txtFolio.Text), FchOP = zctControlFecha1.Value, TypeOP = (string)cboTypeMov.SelectedValue };
                    if (new MixService(_conn).SaveOP(enc, (DataTable)DGridArticulos.DataSource))
                    {
                        MessageBox.Show("Información actualizada con éxito");
                        btnSave.Enabled = false;
                        DGridArticulos.Enabled = false;
                    }
                    else
                        MessageBox.Show("Ocurrio un error al momento de registrar la " + cboTypeMov.Text);
                }
                else
                    if (without != null)
                        MessageBox.Show("Indique una cantidad para los articulos en cero");
                    else
                        MessageBox.Show("Indique el almacen a ser afectado");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetOPData(int codOP)
        {
            MixService service = new MixService(_conn);
            ZctEncOP enc = service.GetEncOP(codOP);
            if (enc != null)
            {
                clearControls();
                txtFolio.Text = enc.CodOP.ToString();
                zctControlFecha1.Value = enc.FchOP;
                cboTypeMov.SelectedValue = enc.TypeOP;
                DGridArticulos.DataSource = service.GetData("GetDetOPTable", "@CodOP;INT", txtFolio.Text);
                GetTotal();
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (txtFolio.Text.Length > 0 && int.Parse(txtFolio.Text) - 1 >= 1)
            {
                txtFolio.Text = (int.Parse(txtFolio.Text) - 1).ToString();
                GetOPData(int.Parse(txtFolio.Text));
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            int folio = GetFolio();
            if (txtFolio.Text.Length > 0 && int.Parse(txtFolio.Text) + 1 < folio)
            {
                txtFolio.Text = (int.Parse(txtFolio.Text) + 1).ToString();
                GetOPData(int.Parse(txtFolio.Text));
            }
            else
            {
                if (txtFolio.Text.Length > 0 && int.Parse(txtFolio.Text) + 1 == folio)
                {
                    folio = int.Parse(txtFolio.Text) + 1;
                    clearControls();
                    txtFolio.Focus();
                    txtFolio.Text = folio.ToString();
                }
            }
        }
        public frmMixMovInv(string conn)
        {
            _conn = conn;
            InitializeComponent();
            DGridArticulos.KeyDown += ZctSotGrid1_KeyDown;
            DGridArticulos.CellEndEdit += DGridArticulos_CellEndEdit;
            DGridArticulos.DataError += DGridArticulos_DataError;
            DGridArticulos.SelectionChanged += DGridArticulos_SelectionChanged;
            btnLeft.Click += btnLeft_Click;
            btnRight.Click += btnRight_Click;
            btnSave.Click += btnSave_Click;
            this.Load += frmMixInv_Load;
        }

        private int GetFolio()
        {
            int folio = 1;
            try
            {
                folio = (int)(new ZctToolService(_conn).GetData("SP_ZctCatFolios", "@TpConsulta;int|@Cod_Folio;varchar", "1|IP").Rows[0][0]);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al recuperar el folio. " + ex.Message);
            }
            return folio;
        }
        private void btnClean_Click(object sender, EventArgs e)
        {
            clearControls();
            txtFolio.Text = GetFolio().ToString();
        }
    }
}