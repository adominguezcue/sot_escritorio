﻿using System.Windows.Forms;
using ZctTools.Controles;
namespace MixItems.View
{
    partial class MixItemsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCancelar = new ZctTools.Controles.ZctSOTButton();
            this.btnActualizar = new ZctTools.Controles.ZctSOTButton();
            this.zctGroupControls1 = new ZctTools.Controles.ZctGroupControls();
            this.DGridArticulos = new System.Windows.Forms.DataGridView();
            this.txtArticulo = new ZctTools.Controles.ZctControlBusqueda();
            this.zctSOTToolTip1 = new ZctTools.Controles.ZctSOTToolTip();
            this.CodArt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CodArtMix = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DescArt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uuid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Proporcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctGroupControls1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGridArticulos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(648, 382);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 27);
            this.btnCancelar.TabIndex = 4;
            this.btnCancelar.Text = "Limpiar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Location = new System.Drawing.Point(754, 382);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(100, 27);
            this.btnActualizar.TabIndex = 3;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.UseVisualStyleBackColor = true;
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // zctGroupControls1
            // 
            this.zctGroupControls1.Controls.Add(this.DGridArticulos);
            this.zctGroupControls1.Location = new System.Drawing.Point(3, 60);
            this.zctGroupControls1.Name = "zctGroupControls1";
            this.zctGroupControls1.Size = new System.Drawing.Size(857, 316);
            this.zctGroupControls1.TabIndex = 2;
            this.zctGroupControls1.TabStop = false;
            this.zctGroupControls1.Text = "Articulos agregados:";
            // 
            // DGridArticulos
            // 
            this.DGridArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGridArticulos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodArt,
            this.CodArtMix,
            this.DescArt,
            this.Uuid,
            this.Proporcion});
            this.DGridArticulos.Location = new System.Drawing.Point(9, 16);
            this.DGridArticulos.Name = "DGridArticulos";
            this.DGridArticulos.Size = new System.Drawing.Size(842, 294);
            this.DGridArticulos.TabIndex = 1;
            this.DGridArticulos.Tag = "En esta tabla se dan de alta los artículos, para producir el producto.";
            this.zctSOTToolTip1.SetToolTip(this.DGridArticulos, "En esta cuadricula se dan de alta los artículos y sus cantidades");
            // 
            // txtArticulo
            // 
            this.txtArticulo.Descripcion = "zctSOTLabelDesc1";
            this.txtArticulo.Location = new System.Drawing.Point(12, 12);
            this.txtArticulo.Name = "txtArticulo";
            this.txtArticulo.Nombre = "Artículo:";
            this.txtArticulo.Size = new System.Drawing.Size(308, 27);
            this.txtArticulo.SPName = "SP_ZctCatArt";
            this.txtArticulo.SPParametros = null;
            this.txtArticulo.SpVariables = "@Cod_Art;VARCHAR|@Desc_Art;VARCHAR|@Prec_Art;DECIMAL|@Cos_Art;DECIMAL|@Cod_Mar;IN" +
    "T|@Cod_Linea;INT|@Cod_Dpto;INT";
            this.txtArticulo.SqlBusqueda = null;
            this.txtArticulo.TabIndex = 0;
            this.txtArticulo.Tabla = "ZctCatArt";
            this.txtArticulo.Tag = "Indique la clave de un artículo";
            this.txtArticulo.TextWith = 111;
            this.txtArticulo.Tipo_Dato = ZctTools.Clases.ClassGen.Tipo_Dato.ALFANUMERICO;
            this.txtArticulo.TipoDato = ZctTools.Clases.ClassGen.Tipo_Dato.ALFANUMERICO;
            this.txtArticulo.Validar = true;
            // 
            // CodArt
            // 
            this.CodArt.DataPropertyName = "Cod_Art";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue;
            this.CodArt.DefaultCellStyle = dataGridViewCellStyle1;
            this.CodArt.HeaderText = "Artículo";
            this.CodArt.Name = "CodArt";
            this.CodArt.ReadOnly = true;
            this.CodArt.Width = 120;
            // 
            // CodArtMix
            // 
            this.CodArtMix.DataPropertyName = "Cod_Art_Mix";
            this.CodArtMix.HeaderText = "Ingrediente";
            this.CodArtMix.Name = "CodArtMix";
            this.CodArtMix.Width = 120;
            // 
            // DescArt
            // 
            this.DescArt.DataPropertyName = "DescArt";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue;
            this.DescArt.DefaultCellStyle = dataGridViewCellStyle2;
            this.DescArt.HeaderText = "Descripción";
            this.DescArt.Name = "DescArt";
            this.DescArt.ReadOnly = true;
            this.DescArt.Width = 358;
            // 
            // Uuid
            // 
            this.Uuid.DataPropertyName = "uuid";
            this.Uuid.HeaderText = "UUID";
            this.Uuid.Name = "Uuid";
            this.Uuid.ReadOnly = true;
            this.Uuid.Visible = false;
            // 
            // Proporcion
            // 
            this.Proporcion.DataPropertyName = "proporcion";
            this.Proporcion.HeaderText = "Proporción";
            this.Proporcion.Name = "Proporcion";
            // 
            // MixItemsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(872, 421);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.zctGroupControls1);
            this.Controls.Add(this.txtArticulo);
            this.Name = "MixItemsForm";
            this.Text = "Recetas";
            this.zctGroupControls1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGridArticulos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ZctControlBusqueda txtArticulo;
        private DataGridView DGridArticulos;
        private ZctGroupControls zctGroupControls1;
        private ZctSOTToolTip zctSOTToolTip1;
        private ZctSOTButton btnActualizar;
        private ZctSOTButton btnCancelar;
        private DataGridViewTextBoxColumn CodArt;
        private DataGridViewTextBoxColumn CodArtMix;
        private DataGridViewTextBoxColumn DescArt;
        private DataGridViewTextBoxColumn Uuid;
        private DataGridViewTextBoxColumn Proporcion;
        //private System.Windows.Forms.DataGridViewTextBoxColumn Proporcion;
    }
}