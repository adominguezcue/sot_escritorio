﻿namespace MixItems.View
{
    partial class MixMovInvForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.zctGroupControls1 = new ZctTools.Controles.ZctGroupControls();
            this.btnRight = new ZctTools.Controles.ZctSOTButton();
            this.btnLeft = new ZctTools.Controles.ZctSOTButton();
            this.txtFolio = new ZctTools.Controles.ZctControlTexto();
            this.zctGroupControls2 = new ZctTools.Controles.ZctGroupControls();
            this.lblTotal = new ZctTools.Controles.ZctSOTLabelDesc();
            this.zctSOTLabel1 = new ZctTools.Controles.ZctSOTLabel();
            this.lblDevolucion = new ZctTools.Controles.ZctSOTLabelDesc();
            this.lblSurtido = new ZctTools.Controles.ZctSOTLabelDesc();
            this.lblPorSurtir = new ZctTools.Controles.ZctSOTLabelDesc();
            this.DGridArticulos = new System.Windows.Forms.DataGridView();
            this.codArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArtExist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cod_Alm = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cantidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subTotalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zctDetOPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zctGroupControls3 = new ZctTools.Controles.ZctGroupControls();
            this.zctControlFecha1 = new ZctTools.Controles.ZctControlFecha();
            this.zctGroupControls4 = new ZctTools.Controles.ZctGroupControls();
            this.btnSave = new ZctTools.Controles.ZctSOTButton();
            this.btnClean = new ZctTools.Controles.ZctSOTButton();
            this.zctGroupControls5 = new ZctTools.Controles.ZctGroupControls();
            this.zctSOTLabel2 = new ZctTools.Controles.ZctSOTLabel();
            this.cboTypeMov = new ZctTools.Controles.ZctSOTComboBox();
            this.zctGroupControls1.SuspendLayout();
            this.zctGroupControls2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGridArticulos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zctDetOPBindingSource)).BeginInit();
            this.zctGroupControls3.SuspendLayout();
            this.zctGroupControls4.SuspendLayout();
            this.zctGroupControls5.SuspendLayout();
            this.SuspendLayout();
            // 
            // zctGroupControls1
            // 
            this.zctGroupControls1.Controls.Add(this.btnRight);
            this.zctGroupControls1.Controls.Add(this.btnLeft);
            this.zctGroupControls1.Controls.Add(this.txtFolio);
            this.zctGroupControls1.Location = new System.Drawing.Point(3, 5);
            this.zctGroupControls1.Name = "zctGroupControls1";
            this.zctGroupControls1.Size = new System.Drawing.Size(235, 51);
            this.zctGroupControls1.TabIndex = 0;
            this.zctGroupControls1.TabStop = false;
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(204, 19);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(19, 23);
            this.btnRight.TabIndex = 2;
            this.btnRight.Text = ">";
            this.btnRight.UseVisualStyleBackColor = true;
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(184, 19);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(19, 23);
            this.btnLeft.TabIndex = 1;
            this.btnLeft.Text = "<";
            this.btnLeft.UseVisualStyleBackColor = true;
            // 
            // txtFolio
            // 
            this.txtFolio.Location = new System.Drawing.Point(9, 17);
            this.txtFolio.Multiline = false;
            this.txtFolio.Name = "txtFolio";
            this.txtFolio.Nombre = "Folio:";
            this.txtFolio.Size = new System.Drawing.Size(176, 28);
            this.txtFolio.TabIndex = 0;
            this.txtFolio.Tag = "Folio de entrada";
            this.txtFolio.TipoDato = ZctTools.Clases.ClassGen.Tipo_Dato.NUMERICO;
            this.txtFolio.ToolTip = "Folio de entrada";
            this.txtFolio.TextChange += new ZctTools.Controles.EvenHandler(this.txtFolio_TextChanged);
            // 
            // zctGroupControls2
            // 
            this.zctGroupControls2.Controls.Add(this.lblTotal);
            this.zctGroupControls2.Controls.Add(this.zctSOTLabel1);
            this.zctGroupControls2.Controls.Add(this.lblDevolucion);
            this.zctGroupControls2.Controls.Add(this.lblSurtido);
            this.zctGroupControls2.Controls.Add(this.lblPorSurtir);
            this.zctGroupControls2.Controls.Add(this.DGridArticulos);
            this.zctGroupControls2.Location = new System.Drawing.Point(3, 62);
            this.zctGroupControls2.Name = "zctGroupControls2";
            this.zctGroupControls2.Size = new System.Drawing.Size(949, 341);
            this.zctGroupControls2.TabIndex = 1;
            this.zctGroupControls2.TabStop = false;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.LightBlue;
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(776, 321);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(104, 15);
            this.lblTotal.TabIndex = 5;
            this.lblTotal.Text = "$0";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // zctSOTLabel1
            // 
            this.zctSOTLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zctSOTLabel1.Location = new System.Drawing.Point(733, 321);
            this.zctSOTLabel1.Name = "zctSOTLabel1";
            this.zctSOTLabel1.Size = new System.Drawing.Size(40, 13);
            this.zctSOTLabel1.TabIndex = 4;
            this.zctSOTLabel1.Text = "Total:";
            // 
            // lblDevolucion
            // 
            this.lblDevolucion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lblDevolucion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDevolucion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDevolucion.Location = new System.Drawing.Point(183, 321);
            this.lblDevolucion.Name = "lblDevolucion";
            this.lblDevolucion.Size = new System.Drawing.Size(81, 15);
            this.lblDevolucion.TabIndex = 3;
            this.lblDevolucion.Text = "Devolución";
            this.lblDevolucion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDevolucion.Visible = false;
            // 
            // lblSurtido
            // 
            this.lblSurtido.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblSurtido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSurtido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSurtido.Location = new System.Drawing.Point(96, 321);
            this.lblSurtido.Name = "lblSurtido";
            this.lblSurtido.Size = new System.Drawing.Size(81, 15);
            this.lblSurtido.TabIndex = 2;
            this.lblSurtido.Text = "Surtido";
            this.lblSurtido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSurtido.Visible = false;
            // 
            // lblPorSurtir
            // 
            this.lblPorSurtir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblPorSurtir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPorSurtir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorSurtir.Location = new System.Drawing.Point(12, 321);
            this.lblPorSurtir.Name = "lblPorSurtir";
            this.lblPorSurtir.Size = new System.Drawing.Size(81, 15);
            this.lblPorSurtir.TabIndex = 1;
            this.lblPorSurtir.Text = "Por Surtir";
            this.lblPorSurtir.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblPorSurtir.Visible = false;
            // 
            // DGridArticulos
            // 
            this.DGridArticulos.AutoGenerateColumns = false;
            this.DGridArticulos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGridArticulos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codArtDataGridViewTextBoxColumn,
            this.descArtDataGridViewTextBoxColumn,
            this.ArtExist,
            this.Cod_Alm,
            this.cantidadDataGridViewTextBoxColumn,
            this.costoDataGridViewTextBoxColumn,
            this.subTotalDataGridViewTextBoxColumn});
            this.DGridArticulos.DataSource = this.zctDetOPBindingSource;
            this.DGridArticulos.Location = new System.Drawing.Point(9, 16);
            this.DGridArticulos.Name = "DGridArticulos";
            this.DGridArticulos.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.DGridArticulos.Size = new System.Drawing.Size(930, 294);
            this.DGridArticulos.TabIndex = 0;
            this.DGridArticulos.Tag = "En esta cuadricula se dan de cargan los articulos de entrada al inventario";
            // 
            // codArtDataGridViewTextBoxColumn
            // 
            this.codArtDataGridViewTextBoxColumn.DataPropertyName = "Cod_Art";
            this.codArtDataGridViewTextBoxColumn.HeaderText = "Artículo";
            this.codArtDataGridViewTextBoxColumn.Name = "codArtDataGridViewTextBoxColumn";
            this.codArtDataGridViewTextBoxColumn.Width = 80;
            // 
            // descArtDataGridViewTextBoxColumn
            // 
            this.descArtDataGridViewTextBoxColumn.DataPropertyName = "DescArt";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightBlue;
            this.descArtDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.descArtDataGridViewTextBoxColumn.HeaderText = "Descripción";
            this.descArtDataGridViewTextBoxColumn.Name = "descArtDataGridViewTextBoxColumn";
            this.descArtDataGridViewTextBoxColumn.ReadOnly = true;
            this.descArtDataGridViewTextBoxColumn.Width = 358;
            // 
            // ArtExist
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = "0";
            this.ArtExist.DefaultCellStyle = dataGridViewCellStyle2;
            this.ArtExist.HeaderText = "Existencia";
            this.ArtExist.Name = "ArtExist";
            this.ArtExist.ReadOnly = true;
            this.ArtExist.Width = 80;
            // 
            // Cod_Alm
            // 
            this.Cod_Alm.DataPropertyName = "Cod_Alm";
            this.Cod_Alm.HeaderText = "Almacén";
            this.Cod_Alm.Name = "Cod_Alm";
            // 
            // cantidadDataGridViewTextBoxColumn
            // 
            this.cantidadDataGridViewTextBoxColumn.DataPropertyName = "Cantidad";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = "0";
            this.cantidadDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.cantidadDataGridViewTextBoxColumn.HeaderText = "Surtido";
            this.cantidadDataGridViewTextBoxColumn.Name = "cantidadDataGridViewTextBoxColumn";
            this.cantidadDataGridViewTextBoxColumn.Width = 80;
            // 
            // costoDataGridViewTextBoxColumn
            // 
            this.costoDataGridViewTextBoxColumn.DataPropertyName = "Costo";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = "0";
            this.costoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.costoDataGridViewTextBoxColumn.HeaderText = "Costo";
            this.costoDataGridViewTextBoxColumn.Name = "costoDataGridViewTextBoxColumn";
            this.costoDataGridViewTextBoxColumn.ReadOnly = true;
            this.costoDataGridViewTextBoxColumn.Width = 80;
            // 
            // subTotalDataGridViewTextBoxColumn
            // 
            this.subTotalDataGridViewTextBoxColumn.DataPropertyName = "SubTotal";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle5.Format = "C2";
            dataGridViewCellStyle5.NullValue = "0";
            this.subTotalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.subTotalDataGridViewTextBoxColumn.HeaderText = "SubTotal";
            this.subTotalDataGridViewTextBoxColumn.Name = "subTotalDataGridViewTextBoxColumn";
            this.subTotalDataGridViewTextBoxColumn.ReadOnly = true;
            this.subTotalDataGridViewTextBoxColumn.Width = 80;
            // 
            // zctDetOPBindingSource
            // 
            this.zctDetOPBindingSource.DataSource = typeof(Modelo.Almacen.Entidades.ZctDetOP);
            // 
            // zctGroupControls3
            // 
            this.zctGroupControls3.Controls.Add(this.zctControlFecha1);
            this.zctGroupControls3.Location = new System.Drawing.Point(3, 409);
            this.zctGroupControls3.Name = "zctGroupControls3";
            this.zctGroupControls3.Size = new System.Drawing.Size(309, 51);
            this.zctGroupControls3.TabIndex = 2;
            this.zctGroupControls3.TabStop = false;
            // 
            // zctControlFecha1
            // 
            this.zctControlFecha1.DateWidth = 91;
            this.zctControlFecha1.Location = new System.Drawing.Point(6, 14);
            this.zctControlFecha1.Name = "zctControlFecha1";
            this.zctControlFecha1.Nombre = "Fecha aplicación:";
            this.zctControlFecha1.Size = new System.Drawing.Size(299, 27);
            this.zctControlFecha1.TabIndex = 0;
            this.zctControlFecha1.Value = new System.DateTime(2017, 9, 28, 11, 34, 10, 0);
            // 
            // zctGroupControls4
            // 
            this.zctGroupControls4.Controls.Add(this.btnSave);
            this.zctGroupControls4.Controls.Add(this.btnClean);
            this.zctGroupControls4.Location = new System.Drawing.Point(412, 409);
            this.zctGroupControls4.Name = "zctGroupControls4";
            this.zctGroupControls4.Size = new System.Drawing.Size(540, 51);
            this.zctGroupControls4.TabIndex = 3;
            this.zctGroupControls4.TabStop = false;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(412, 13);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(112, 28);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Guardar";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnClean
            // 
            this.btnClean.Location = new System.Drawing.Point(294, 13);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(112, 28);
            this.btnClean.TabIndex = 0;
            this.btnClean.Text = "Limpiar";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // zctGroupControls5
            // 
            this.zctGroupControls5.Controls.Add(this.zctSOTLabel2);
            this.zctGroupControls5.Controls.Add(this.cboTypeMov);
            this.zctGroupControls5.Location = new System.Drawing.Point(250, 5);
            this.zctGroupControls5.Name = "zctGroupControls5";
            this.zctGroupControls5.Size = new System.Drawing.Size(268, 51);
            this.zctGroupControls5.TabIndex = 4;
            this.zctGroupControls5.TabStop = false;
            // 
            // zctSOTLabel2
            // 
            this.zctSOTLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.zctSOTLabel2.AutoSize = true;
            this.zctSOTLabel2.Location = new System.Drawing.Point(6, 24);
            this.zctSOTLabel2.Name = "zctSOTLabel2";
            this.zctSOTLabel2.Size = new System.Drawing.Size(64, 13);
            this.zctSOTLabel2.TabIndex = 1;
            this.zctSOTLabel2.Text = "Movimiento:";
            this.zctSOTLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cboTypeMov
            // 
            this.cboTypeMov.FormattingEnabled = true;
            this.cboTypeMov.Location = new System.Drawing.Point(76, 21);
            this.cboTypeMov.Name = "cboTypeMov";
            this.cboTypeMov.Size = new System.Drawing.Size(176, 21);
            this.cboTypeMov.TabIndex = 0;
            // 
            // MixMovInvForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(960, 464);
            this.Controls.Add(this.zctGroupControls5);
            this.Controls.Add(this.zctGroupControls4);
            this.Controls.Add(this.zctGroupControls3);
            this.Controls.Add(this.zctGroupControls2);
            this.Controls.Add(this.zctGroupControls1);
            this.Name = "MixMovInvForm";
            this.Text = "Entradas y salidas de artículos compuestos";
            this.zctGroupControls1.ResumeLayout(false);
            this.zctGroupControls2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGridArticulos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zctDetOPBindingSource)).EndInit();
            this.zctGroupControls3.ResumeLayout(false);
            this.zctGroupControls4.ResumeLayout(false);
            this.zctGroupControls5.ResumeLayout(false);
            this.zctGroupControls5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private ZctTools.Controles.ZctGroupControls zctGroupControls1;
        private ZctTools.Controles.ZctControlTexto txtFolio;
        private ZctTools.Controles.ZctSOTButton btnLeft;
        private ZctTools.Controles.ZctSOTButton btnRight;
        private ZctTools.Controles.ZctGroupControls zctGroupControls2;
        private System.Windows.Forms.DataGridView DGridArticulos;
        private ZctTools.Controles.ZctSOTLabelDesc lblDevolucion;
        private ZctTools.Controles.ZctSOTLabelDesc lblSurtido;
        private ZctTools.Controles.ZctSOTLabelDesc lblPorSurtir;
        private ZctTools.Controles.ZctSOTLabelDesc lblTotal;
        private ZctTools.Controles.ZctSOTLabel zctSOTLabel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodArt;
        private System.Windows.Forms.DataGridViewTextBoxColumn DescArt;
        private System.Windows.Forms.DataGridViewComboBoxColumn CodAlm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubTot;
        private ZctTools.Controles.ZctGroupControls zctGroupControls3;
        private ZctTools.Controles.ZctControlFecha zctControlFecha1;
        private ZctTools.Controles.ZctGroupControls zctGroupControls4;
        private ZctTools.Controles.ZctSOTButton btnSave;
        private ZctTools.Controles.ZctSOTButton btnClean;
        private ZctTools.Controles.ZctGroupControls zctGroupControls5;
        private ZctTools.Controles.ZctSOTLabel zctSOTLabel2;
        private ZctTools.Controles.ZctSOTComboBox cboTypeMov;
        private System.Windows.Forms.BindingSource zctDetOPBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn codArtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descArtDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArtExist;
        private System.Windows.Forms.DataGridViewComboBoxColumn Cod_Alm;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subTotalDataGridViewTextBoxColumn;
    }
}