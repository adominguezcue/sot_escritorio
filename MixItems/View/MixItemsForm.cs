﻿using Modelo.Almacen.Entidades;
using SOTControladores.Controladores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZctTools.Controller;
using ZctTools.Forms;

namespace MixItems.View
{
    public partial class MixItemsForm : Form
    {
        int iColArt = 0;
        int iColMix = 1;
        int iColDes = 2;
        int iColUuid = 3;
        int iColCant = 4;
        int desBusqueda = 1;
        bool backRow = false;
        bool isExist = false;
        string _conn = "";

        int filaActual = 0;

        private void ZctSotGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F3 && DGridArticulos.CurrentCell.ColumnIndex == iColMix)
                {
                    ZctBusqueda fBusqueda = new ZctBusqueda(_conn);
                    fBusqueda.sSPName = txtArticulo.SPName;
                    fBusqueda.sSpVariables = txtArticulo.SpVariables;
                    if (txtArticulo.SPParametros != null && txtArticulo.SPParametros != "")
                    {
                        fBusqueda.sSPParametros = txtArticulo.SPParametros;
                    }
                    fBusqueda.ShowDialog(this);
                    if (fBusqueda.iValor != null)
                    {
                        DGridArticulos.BeginEdit(true);
                        DGridArticulos.CurrentCell.Value = fBusqueda.iValor;
                        DGridArticulos.EndEdit();

                        if (!isExist && !backRow)
                        {
                            DGridArticulos.UpdateCellValue(iColMix, DGridArticulos.CurrentCell.RowIndex);
                            DGridArticulos.CurrentCell = DGridArticulos.CurrentRow.Cells[iColCant];
                            DGridArticulos.CurrentCell.Selected = true;
                        }
                        else
                        {
                            if (isExist && backRow)
                                DGridArticulos.CurrentCell.Value = "";

                            isExist = false;
                            backRow = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DGridArticulos_SelectionChanged(object sender, EventArgs e)
        {
            //if (backRow)
            //{
            //    backRow = false;
            //    DGridArticulos.CurrentCell = DGridArticulos.Rows[filaActual].Cells[iColCant];
            //    DGridArticulos.CurrentCell.Selected = true;
            //}
            //if (isExist)
            //{
            //    isExist = false;
            //    DGridArticulos.CurrentCell = DGridArticulos.Rows[filaActual].Cells[iColMix];
            //    DGridArticulos.CurrentCell.Value = "";
            //    DGridArticulos.CurrentCell.Selected = true;
            //}
        }
        private void DGridArticulos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == iColMix)
                {
                    var valTmp = (string)(DGridArticulos.CurrentCell.Value == DBNull.Value ? null : DGridArticulos.CurrentCell.Value);

                    var exist = ((BindingList<ZctMixItems>)DGridArticulos.DataSource).Where(row => row.Cod_Art_Mix != null && /*((string)*/row.Cod_Art_Mix/*)*/.Equals(valTmp)).FirstOrDefault();
                    if (valTmp != "" && valTmp != txtArticulo.Text && (exist == null || string.IsNullOrEmpty(exist.DescArt)))
                    {
                        ZctToolService PkBusCon = new ZctToolService(_conn);

                        DataTable dataArt = PkBusCon.GetData("SP_ZctCatArt_Busqueda", "@Codigo;VARCHAR", valTmp);
                        if (dataArt.Rows.Count <= 0)
                        {
                            MessageBox.Show("El código de artículo no existe, verifique por favor.");
                            DGridArticulos.CurrentCell.Value = "";
                        }
                        else
                        {
                            DGridArticulos.CurrentRow.Cells[iColDes].Value = dataArt.Rows[0].ItemArray[desBusqueda];
                            DGridArticulos.CurrentRow.Cells[iColArt].Value = txtArticulo.Text;
                            DGridArticulos.CurrentRow.Cells[iColUuid].Value = System.Guid.NewGuid();
                            backRow = true;
                        }
                        dataArt.Dispose();
                    }
                    else
                    {
                        MessageBox.Show("El artículo ya esta en lista");
                        isExist = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtArticulo_LostFocus(object sender, EventArgs e)
        {
            try
            {
                var origen = new BindingList<ZctMixItems>();

                foreach (var item in new ControladorMixItems().GetMixItems(txtArticulo.Text))
                    origen.Add(item);
                //origen.Add(new MixItemShow { Porcion = "1/1" });

                

                DGridArticulos.DataSource = origen;
                if (txtArticulo.Text != "")
                {
                    txtArticulo.Enabled = false;
                    DGridArticulos.Enabled = true;
                    btnActualizar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al recuperar los artículos");
            }
        }

        private void DGridArticulos_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex == iColCant)
            {
                MessageBox.Show("Indique un valor válido para la cantidad del artículo");
            }
            else
            {
                MessageBox.Show("Ocurrió un error inesperado, comuníquese con el personal de desarrollo");
            }
        }

        public MixItemsForm(string conn)
        {
            _conn = conn;
            InitializeComponent();
            DGridArticulos.AutoGenerateColumns = false;
            txtArticulo.Descripcion = "";
            DGridArticulos.KeyDown += ZctSotGrid1_KeyDown;
            DGridArticulos.CellBeginEdit += DGridArticulos_CellBeginEdit;
            DGridArticulos.CellEndEdit += DGridArticulos_CellEndEdit;
            txtArticulo.LostFocus += txtArticulo_LostFocus;
            DGridArticulos.DataError += DGridArticulos_DataError;
            DGridArticulos.Enabled = false;
            DGridArticulos.ShowCellToolTips = false;
            DGridArticulos.SelectionChanged += DGridArticulos_SelectionChanged;
            btnActualizar.Enabled = false;
        }

        private void DGridArticulos_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            filaActual = DGridArticulos.CurrentRow.Index;
        }

        private void clearControls()
        {
            try
            {
                txtArticulo.Text = "";
                txtArticulo.Descripcion = "";
                txtArticulo.Enabled = true;
                if (DGridArticulos.DataSource != null)
                    ((BindingList<ZctMixItems>)DGridArticulos.DataSource).Clear();
                DGridArticulos.Enabled = false;
                btnActualizar.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error clear: " + ex.Message);
            }
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            clearControls();
        }

        private bool ValidateProportion(List<string> arts)
        {
            bool ok = true;
            try
            {
                foreach (string art in arts)
                {
                    string[] datas = art.Split('/');
                    if (datas.Length < 2 || !ZctTools.Clases.ClassGen.IsNumeric(datas[0]) || !ZctTools.Clases.ClassGen.IsNumeric(datas[1]))
                    {
                        ok = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return ok;
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            var values = ((BindingList<ZctMixItems>)DGridArticulos.DataSource).Select(row => row.proporcion).ToList();
            var without = ((BindingList<ZctMixItems>)DGridArticulos.DataSource).AsEnumerable().Where(row => string.IsNullOrWhiteSpace(row.proporcion)).FirstOrDefault();
            if (without == null && values.Count > 0 && ValidateProportion(values))
            {
                new ControladorMixItems().GuardarIngredientes(((BindingList<ZctMixItems>)DGridArticulos.DataSource).Select(m => m).ToList());

                MessageBox.Show("Información actualizada con éxito");
            }
            else
                MessageBox.Show("Revise la proporción de los artículos");
        }

        private void txtCapturaArticulos_Leave(object sender, EventArgs e)
        {

        }

        private void btnAgregarFila_Click(object sender, EventArgs e)
        {
            var lista = DGridArticulos.DataSource as BindingList<ZctMixItems>;

            if (lista != null)
                lista.Add(new ZctMixItems { proporcion = "1/1" });

            DGridArticulos.Refresh();
        }
    }
}
