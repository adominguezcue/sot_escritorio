﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;
using System.Runtime.Serialization;
using MixItems.Model;
using ZctTools.Clases;
using ZctTools.Controller;

namespace MixItems.Controller
{
  internal class MixProvider
  {
        private string _conn;

        public MixProvider(string conn)
        {
            _conn = conn;
        }

        internal List<MixItemShow> GetMixItems(string codArt)
    {
      List<MixItemShow> mixItems;
      try
      {
        using (ZctMixDBDataContext db = new ZctMixDBDataContext(_conn))
        {
          mixItems = (from m in db.MixItems
                      where m.CodArt.Equals(codArt)
                      select new MixItemShow
                      {
                        CodArt = m.CodArt,
                        CodArtMix = m.CodArtMix,
                        Uuid = m.Uuid,
                        Proporcion = m.Proporcion,
                        Cantidad = m.Cantidad,
                        Base = m.Base
                      }).ToList();
          if (mixItems == null)
          {
            mixItems = new List<MixItemShow>();
          }
          else
          {
            foreach (var m in mixItems)
            {
              m.DescArt = (string)(new ZctToolService(_conn).SP_ZctCatArt_Busqueda(m.CodArtMix).Rows[0].ItemArray[1]);
            }
          }
        }
        return mixItems;
      }
      catch(Exception ex)
      {
        return new List<MixItemShow>();
      }
    }

    internal List<MixItemCost> GetMixItemsCost(string codArt, int codAlm)
    {
      List<MixItemCost> mixItems;
      try
      {
        using (ZctMixDBDataContext db = new ZctMixDBDataContext(_conn))
        {
          mixItems = (from m in db.MixItems
                      join aa in db.ZctArtXAlms on m.CodArtMix equals aa.CodArt into items
                      where m.CodArt.Equals(codArt)
                      from i in items
                      where i.CodAlm == codAlm
                      select new MixItemCost
                      {
                        CodArt = m.CodArt,
                        CodArtMix = i.CodArt,
                        Uuid = i.Uuid,
                        Cantidad = m.Cantidad,
                        Exist = (decimal)i.ExistArt,
                        Cost = (decimal)i.CostoPromArt,
                        Base= (decimal)m.Base
                      }).ToList();
          if (mixItems == null)
          {
            mixItems = new List<MixItemCost>();
          }
        }
        return mixItems;
      }
      catch(Exception ex)
      {
        return new List<MixItemCost>();
      }
    }

    internal List<ZctDetOpShow> GetDetOP(int codOP)
    {
      List<ZctDetOpShow> detOP;
      try
      {
        using (ZctMixDBDataContext db = new ZctMixDBDataContext(_conn))
        {
          detOP = (from d in db.ZctDetOPs
                   where d.CodOP.Equals(codOP)
                   select new ZctDetOpShow
                   {
                     CodOP = d.CodOP,
                     CodDetOP = d.CodDetOP,
                     CodArt = d.CodArt,
                     Cantidad = d.Cantidad,
                     Costo = d.Costo,
                     SubTotal = (decimal)d.Cantidad * (decimal)d.Costo,
                     MesFiscal = d.MesFiscal,
                     AnioFiscal = d.AnioFiscal,
                     CodAlm = d.CodAlm,
                   }).ToList();
          if (detOP == null)
          {
            detOP = new List<ZctDetOpShow>();
          }
          else
          {
            foreach (var d in detOP)
            {
              d.DescArt = (string)(new ZctToolService(_conn).SP_ZctCatArt_Busqueda(d.CodArt).Rows[0].ItemArray[1]);
            }
          }
        }
        return detOP;
      }
      catch
      {
        return new List<ZctDetOpShow>();
      }
    }

    internal DataTable GetDetOPTable(int codOP)
    {
      try
      {
        return ClassGen.ToDataTable(GetDetOP(codOP));
      }
      catch
      {
        return new DataTable();
      }
    }

    internal ZctEncOP GetEncOP(int codOP)
    {
      ZctEncOP enc = null;
      using (ZctMixDBDataContext db = new ZctMixDBDataContext(_conn))
      {
        enc = (from e in db.ZctEncOPs
               where e.CodOP == codOP
               select e).FirstOrDefault();
      }
      return enc;
    }

    internal bool SaveOP(ZctEncOP encOP, List<ZctDetOP> detOP)
    {
        bool ok = false;
        string paramsMovInv = "@TpConsulta;Int|@CodMov_Inv;int|@TpMov_Inv;varchar|@CosMov_Inv;decimal|@FolOS_Inv;varchar|@Cod_Art;varchar|@CtdMov_Inv;decimal|@TpOs_Inv;varchar|@FchMov_Inv;smalldatetime|@Cod_Alm;int";
        string values = "";
        ZctToolService service = new ZctToolService(_conn);
        try
        {
            using (ZctMixDBDataContext db = new ZctMixDBDataContext(_conn))
            {
                db.ZctEncOPs.InsertOnSubmit(encOP);
                detOP = detOP.Select(det => { det.CodOP = encOP.CodOP; return det; }).ToList();
                detOP = SetAnioMes(detOP, encOP);
                db.ZctDetOPs.InsertAllOnSubmit(detOP);

                List<ZctTools.Model.ZctEncMovInv> encMovInv = (from d in detOP
                                                               select new ZctTools.Model.ZctEncMovInv
                                                               {
                                                                   FchMovInv = encOP.FchOP,
                                                                   TpMovInv = encOP.TypeOP.Equals("OP") ? "EN" : "SA",
                                                                   CodArt = d.CodArt,
                                                                   CtdMovInv = decimal.Parse(d.Cantidad.ToString()),
                                                                   CosMovInv = d.Costo * d.Cantidad,
                                                                   FolOSInv = d.CodOP.ToString(),
                                                                   TpOsInv = encOP.TypeOP,
                                                                   CodAlm = d.CodAlm,
                                                               }).ToList();
                foreach (var movInv in encMovInv)
                {
                    values = "2|" + movInv.CodMovInv + "|" + movInv.TpMovInv + "|" + movInv.CosMovInv + "|" + movInv.FolOSInv + "|" + movInv.CodArt + "|" + (movInv.TpOsInv.Equals("OP") ? movInv.CtdMovInv : movInv.CtdMovInv * -1) + "|" + movInv.TpOsInv + "|" + movInv.FchMovInv + "|" + movInv.CodAlm;
                    service.GetData("SP_ZctEncMovInv", paramsMovInv, values);
                    if (movInv.TpOsInv.Equals("OP"))
                    {
                        List<MixItemCost> items = GetMixItemsCost(movInv.CodArt, (int)movInv.CodAlm);
                        foreach (var item in items)
                        {
                            var surtido = ((movInv.CtdMovInv) * ((decimal)item.Cantidad / (decimal)item.Base)) * -1;
                            //values = "2|" + movInv.CodMovInv + "|SA|" + item.Cost + "|" + movInv.FolOSInv + "|" + item.CodArtMix + "|" + (movInv.CtdMovInv * Decimal.ToInt32(Decimal.Round(GetMaxProportion(item)/(decimal)item.Base*(decimal)item.Cantidad,0)) * -1) + "|" + movInv.TpOsInv + "|" + movInv.FchMovInv + "|" + movInv.CodAlm;
                            values = "2|" + movInv.CodMovInv + "|SA|" + (item.Cost / item.Exist) * surtido + "|" + movInv.FolOSInv + "|" + item.CodArtMix + "|" + surtido + "|" + movInv.TpOsInv + "|" + movInv.FchMovInv + "|" + movInv.CodAlm;
                            service.GetData("SP_ZctEncMovInv", paramsMovInv, values);
                        }
                    }
                }
                db.SubmitChanges();
                service.GetData("SP_ZctCatFolios", "@TpConsulta;int|@Cod_Folio;varchar", "2|IP");
                ok = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;

        }
        return ok;
    }

    internal void SetProportion(ref List<MixItem> listMix) {
      foreach (MixItem m in listMix) {
        m.Cantidad = Decimal.Parse(m.Proporcion.Split('/')[0]);
        m.Base = Decimal.Parse(m.Proporcion.Split('/')[1]);
        //if (m.Proporcion.Split('/')[0].IndexOf(".") >= 0) {
        //  m.Cantidad = m.Cantidad * Convert.ToDecimal(Math.Pow(10, m.Proporcion.Split('/')[0].Length - m.Proporcion.Split('/')[0].IndexOf(".")-1));
        //  m.Base = m.Base * Convert.ToDecimal(Math.Pow(10, m.Proporcion.Split('/')[0].Length - m.Proporcion.Split('/')[0].IndexOf(".")-1));
        //}
      }
    }

    internal decimal GetMaxProportion(MixItem item) {
      decimal prop= (decimal)item.Base;
      try
      {
        using (ZctMixDBDataContext db= new ZctMixDBDataContext(_conn)) {
          prop = (from m in db.MixItems
                  where m.CodArtMix == item.CodArtMix
                  select m.Base).Max().Value;
        }
      }
      catch (Exception ex) {
      }
      return prop;
    }
    internal bool SaveMixList(List<MixItem> listMix)
    {
      bool isOk = false;
      using (ZctMixDBDataContext db = new ZctMixDBDataContext(_conn))
      {
        try
        {
          List<MixItem> prevListMix = (from p in db.MixItems
                                       where p.CodArt.Equals(listMix.First().CodArt)
                                       select p).ToList();

          if (prevListMix != null)
            db.MixItems.DeleteAllOnSubmit(prevListMix);
          SetProportion(ref listMix);
          db.MixItems.InsertAllOnSubmit(listMix);
          db.SubmitChanges();
          isOk = true;
        }
        catch
        {
        }
      }
      return isOk;
    }

    internal bool SaveMixTable(DataTable tableMix)
    {
      bool isOk = false;
      try
      {
        isOk = SaveMixList(ClassGen.ConvertToList<MixItem>(tableMix));
      }
      catch (Exception ex)
      {
      }
      return isOk;
    }

    internal bool SaveOP(ZctEncOP encOP, DataTable tableOP)
    {
      bool isOk = false;
      try
      {
        isOk = SaveOP(encOP, ClassGen.ConvertToList<ZctDetOP>(tableOP));
      }
      catch (Exception ex)
      {
      }
      return isOk;
    }

    internal DataTable GetData(string sSPName, string sSpVariables)
    {
      try
      {
        MethodData methodData = ClassGen.GetCallMethod(sSPName, sSpVariables);
        MethodInfo method = this.GetType().GetMethod(sSPName, BindingFlags.NonPublic | BindingFlags.Instance, null, methodData.Types, null);

        return (DataTable)method.Invoke(this, methodData.Params);
      }
      catch (Exception ex)
      {
        return new DataTable();
      }
    }

    internal DataTable GetData(string sSPName, string sSpVariables, string sSpValues)
    {
      try
      {
        MethodData methodData = ClassGen.GetCallMethod(sSPName, sSpVariables, sSpValues);
        MethodInfo method = this.GetType().GetMethod(sSPName, BindingFlags.NonPublic | BindingFlags.Instance, null, methodData.Types, null);

        return (DataTable)method.Invoke(this, methodData.Params);
      }
      catch (Exception ex)
      {
        return new DataTable();
      }
    }

    internal DataTable GetMixItemsTable(string codArt)
    {
      return ClassGen.ToDataTable(GetMixItems(codArt));
    }

    internal DataTable GetMixItemsCostTable(string codArt, int codAlm)
    {
      return ClassGen.ToDataTable(GetMixItemsCost(codArt, codAlm));
    }
    internal List<ZctDetOP> SetAnioMes(List<ZctDetOP> detOP, ZctEncOP enc)
    {
      using (ZctTools.Model.ZctSOTToolsDBDataContext db = new ZctTools.Model.ZctSOTToolsDBDataContext(_conn))
      {
        int? anio = 0; int? mes = 0;
        db.sp_get_anio_mes(enc.FchOP.Date, ref anio, ref mes);
        detOP = detOP.Select(det => { det.AnioFiscal = anio; det.MesFiscal = mes; return det; }).ToList();
      }
      return detOP;
    }
  }

  [DataContract]
  public partial class MixItemShow : MixItem
  {
    [DataMember]
    public string DescArt { get; set; }

    //[DataMember]
    //public string Porcion { get; set; }
  }

  [DataContract]
  public partial class MixItemCost : MixItem
  {
    [DataMember]
    public decimal Cost { get; set; }
    public decimal Exist { get; set; }
  }

  [DataContract]
  public partial class ZctDetOpShow : ZctDetOP
  {
    [DataMember]
    public string DescArt { get; set; }

    public decimal SubTotal { get; set; }
  }

  [DataContract]
  public partial class TypeMovMix
  {
    public string IdType { get; set; }
    public string TypeDesc { get; set; }
  }
}
