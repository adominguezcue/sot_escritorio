﻿using System.Collections.Generic;
using System.Data;
using MixItems.Model;

namespace MixItems.Controller
{
  public class MixService
  {
        string _conn;
        public MixService(string conn) {
            _conn = conn;
        }
    public DataTable GetMixItemsTable(string codigo)
    {
      return new MixProvider(_conn).GetMixItemsTable(codigo);
    }

    public List<MixItemShow> GetMixItems(string codigo)
    {
      return new MixProvider(_conn).GetMixItems(codigo);
    }

    public bool SaveMixTable(DataTable mixTable)
    {
      return new MixProvider(_conn).SaveMixTable(mixTable);
    }

    public bool SaveMixList(List<MixItem> mixList)
    {
      return new MixProvider(_conn).SaveMixList(mixList);
    }
    public DataTable GetData(string sSPName, string sSpVariables)
    {
      return new MixProvider(_conn).GetData(sSPName, sSpVariables);
    }

    public DataTable GetData(string sSPName, string sSpVariables, string sSpValues)
    {
      return new MixProvider(_conn).GetData(sSPName, sSpVariables, sSpValues);
    }

    public bool SaveOP(ZctEncOP enc, DataTable opTable)
    {
      return new MixProvider(_conn).SaveOP(enc, opTable);
    }

    public ZctEncOP GetEncOP(int codOP)
    {
      return new MixProvider(_conn).GetEncOP(codOP);
    }
  }
}
