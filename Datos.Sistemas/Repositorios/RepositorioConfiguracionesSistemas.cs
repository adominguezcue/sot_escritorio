﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Nucleo.Repositorios;
using Modelo.Sistemas.Entidades;
using Modelo.Sistemas.Repositorios;

namespace Datos.Sistemas.Repositorios
{
    public class RepositorioConfiguracionesSistemas: Repositorio<ConfiguracionSistema>, IRepositorioConfiguracionesSistemas
    {
        public RepositorioConfiguracionesSistemas() : base(new Contextos.SistemasContext()) { }
    }
}
