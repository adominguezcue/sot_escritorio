﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Nucleo.Repositorios;
using Modelo.Sistemas.Entidades;
using Modelo.Sistemas.Repositorios;

namespace Datos.Sistemas.Repositorios
{
    public class RepositorioParametrosSistemas: Repositorio<ParametroSistema>, IRepositorioParametrosSistemas
    {
        public RepositorioParametrosSistemas() : base(new Contextos.SistemasContext()) { }
    }
}
