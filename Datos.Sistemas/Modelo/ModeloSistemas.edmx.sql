
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/28/2019 11:20:32
-- Generated from EDMX file: C:\Users\developer\Source\Trabajo\Git\EngraneDigital\sot_global\Datos.Sistemas\Modelo\ModeloSistemas.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [SOT];
GO
IF SCHEMA_ID(N'Sistemas') IS NULL EXECUTE(N'CREATE SCHEMA [Sistemas]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[Sistemas].[ConfiguracionesSistemas]', 'U') IS NOT NULL
    DROP TABLE [Sistemas].[ConfiguracionesSistemas];
GO
IF OBJECT_ID(N'[Sistemas].[ParametrosSistemas]', 'U') IS NOT NULL
    DROP TABLE [Sistemas].[ParametrosSistemas];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ConfiguracionesSistemas'
CREATE TABLE [Sistemas].[ConfiguracionesSistemas] (
    [Id] uniqueidentifier  NOT NULL,
    [TextoLogotipo1] nvarchar(max)  NULL,
    [TextoLogotipo2] nvarchar(max)  NULL,
    [Isotipo] nvarchar(max)  NULL,
    [Imagotipo] nvarchar(max)  NULL,
    [Isologo] nvarchar(max)  NULL,
    [ImagenReportes] nvarchar(max)  NULL,
    [ManejaPuntosLealtad] bit  NOT NULL
);
GO

-- Creating table 'ParametrosSistemas'
CREATE TABLE [Sistemas].[ParametrosSistemas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [GuidSistema] uniqueidentifier  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Valor] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ConfiguracionesSistemas'
ALTER TABLE [Sistemas].[ConfiguracionesSistemas]
ADD CONSTRAINT [PK_ConfiguracionesSistemas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ParametrosSistemas'
ALTER TABLE [Sistemas].[ParametrosSistemas]
ADD CONSTRAINT [PK_ParametrosSistemas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------