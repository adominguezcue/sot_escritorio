﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace ProcesaXMLRails
{
    public class Procesa
    {
        // compiles with 
// $  gmcs railsClient.cs



//public class HelloWorld
//{
    //static public void Main()
    //{
    //    Console.WriteLine("We're going to send an XML file to our rails database!");

        
    //    Console.WriteLine("Please input the path to your rails app and it's controller (or leave blank if you coded a default):");
        
    //    string path = Console.ReadLine();
    //    string response = "";
        
    //    if (path == "")
    //      response = sendXmlData("http://192.168.0.11:3000/books");
    //    else
    //      response = sendXmlData(path);
        
        
    //    Console.WriteLine("\n\n Output: \n");
    //    Console.WriteLine(response);
    //    Console.WriteLine("\n");
        
        
    //    Console.WriteLine("We're done!");
    //    Console.Read();
    //}
    public static string RemoveAllNamespaces(string xmlDocument)
{
    XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

    return xmlDocumentWithoutNs.ToString();
}

        
//Core recursion function
 private static XElement RemoveAllNamespaces(XElement xmlDocument)
    {
        if (!xmlDocument.HasElements)
        {
            XElement xElement = new XElement(xmlDocument.Name.LocalName);
            xElement.Value = xmlDocument.Value;

            foreach (XAttribute attribute in xmlDocument.Attributes())
                xElement.Add(attribute);

            return xElement;
        }
        return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
    }


    // return true if data is successfully stored (not yet working!)
        static public string sendXmlData(string pathToAction, string xmlData)
    {
        string response = "";
        
        HttpWebRequest req = null;
        HttpWebResponse rsp = null;
        try
        {
            string uri = pathToAction; 
            //string uri = "http://192.168.0.11:3000/books";
            //xmlData = RemoveAllNamespaces(xmlData);
            
           // Console.WriteLine(xmlData);


            
            req = (HttpWebRequest)WebRequest.Create(uri);
            //req.Proxy = WebProxy.GetDefaultProxy(); // Enable if using proxy
            req.Method = "POST";        // Post method
            req.ContentType = "text/xml";     // content type
            req.Accept = "text/xml";

            // Wrap the request stream with a text-based writer
            using (StreamWriter writer = new StreamWriter(req.GetRequestStream()))
            {
                // Write the XML text into the stream
                writer.WriteLine(xmlData);
                writer.Close();
            }
            
            // Send the data to the webserver
            using (rsp = (HttpWebResponse)req.GetResponse())
            {
                Encoding enc = System.Text.Encoding.GetEncoding(1252);
                using (StreamReader loResponseStream = new StreamReader(rsp.GetResponseStream(), enc)) {
                    response = loResponseStream.ReadToEnd();
                    //response =  RemoveAllNamespaces(response).Replace("-","_");

                    Console.WriteLine(response);
                }    
            }

        }
        catch (WebException webEx)
        {
            throw webEx;
        }
        catch (Exception ex)
        {
            throw ex;
        }
     
        return response;
    }


    static public string GetXmlData(string pathToAction)
    {
        string response = "";

        HttpWebRequest req = null;
        HttpWebResponse rsp = null;
        try
        {
            string uri = pathToAction;
            //string uri = "http://192.168.0.11:3000/books";

            

            req = (HttpWebRequest)WebRequest.Create(uri);
            //req.Proxy = WebProxy.GetDefaultProxy(); // Enable if using proxy
            req.Method = "GET";        // Post method
            req.ContentType = "text/xml";     // content type
            req.Accept = "text/xml";


            // Send the data to the webserver
            rsp = (HttpWebResponse)req.GetResponse();

            Encoding enc = System.Text.Encoding.GetEncoding(1252);
            StreamReader loResponseStream = new
              StreamReader(rsp.GetResponseStream(), enc);

            response = loResponseStream.ReadToEnd();
            Console.WriteLine(response);
            //response = rsp.StatusCode.ToString();    // you can return this if you'd prefer
            //MessageBox.Show(Response);
        }
        catch (WebException webEx)
        {
            throw webEx; 
        }
        catch (Exception ex)
        {
            throw ex;
        }
        

        return response;
    }



    }
}
