﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Lineas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Transversal.Dependencias;

namespace SOTWebApi.Controllers.Almacen
{
    [RoutePrefix("api/Lineas")]
    public class LineasController : ApiController
    {
        private IServicioLineas AplicacionServicioLineas
        {
            get { return _aplicacionServicioLineas.Value; }
        }

        private Lazy<IServicioLineas> _aplicacionServicioLineas = new Lazy<IServicioLineas>(() => FabricaDependencias.Instancia.Resolver<IServicioLineas>());

        [Route("{cod_dpto?}")]
        public List<Linea> Get(int cod_dpto = 0) 
        {
            return AplicacionServicioLineas.ObtenerLineasParaFiltro(cod_dpto);
        }
    }
}
