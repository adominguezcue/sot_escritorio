﻿using Negocio.Almacen.Articulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Transversal.Dependencias;
using SOTWebApi.Extensiones;
using Modelo.Almacen.Entidades.Dtos;
using Modelo.Almacen.Entidades;

namespace SOTWebApi.Controllers.Almacen
{
    [RoutePrefix("api/Articulos")]
    public class ArticulosController : ApiController
    {
        private IServicioArticulos AplicacionServicioArticulos
        {
            get { return _aplicacionServicioArticulos.Value; }
        }

        private Lazy<IServicioArticulos> _aplicacionServicioArticulos = new Lazy<IServicioArticulos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioArticulos>();
        });


        // GET: api/Articulos
      
        [Route("{filtro?}")]
        public List<DtoResultadoBusquedaArticuloSimple> Get(string filtro = null)
        {
            return AplicacionServicioArticulos.ObtenerResumenesArticulosPorFiltro(filtro, this.GetDtoUser());
        }

        // GET: api/Articulos/5
        [HttpGet]
        [Route("{codigoArticulo}/detalles")]
        public Articulo ObtenerPorCodigo(string codigoArticulo)
        {
            return AplicacionServicioArticulos.TraerOGenerarArticuloPorCodigoOUpc(codigoArticulo);
        }

        // GET: api/Articulos/5
        [HttpGet]
        [Route("{codigoArticulo}/existencias")]
        public List<DtoExistencias> ConsultarExistencias(string codigoArticulo)
        {
            return AplicacionServicioArticulos.SP_ConsultarExistencias(codigoArticulo);
        }

        // POST: api/Articulos
        [HttpPost]
        [Route("")]
        public string Post(Articulo articulo)
        {
            AplicacionServicioArticulos.AgregaArticulo(articulo, this.GetDtoUser());

            return "Creación exitosa";
        }

        // PUT: api/Articulos/5
        [HttpPut]
        [Route("")]
        public string Put(Articulo articulo)
        {
            AplicacionServicioArticulos.ModificarArticulo(articulo, this.GetDtoUser());

            return "Actualización exitosa";
        }

        // DELETE: api/Articulos/5
        [HttpDelete]
        [Route("{codigoArticulo}")]
        public string Delete(string codigoArticulo)
        {
            AplicacionServicioArticulos.EliminaArticulo(codigoArticulo);

            return "Eliminación exitosa";
        }
    }
}
