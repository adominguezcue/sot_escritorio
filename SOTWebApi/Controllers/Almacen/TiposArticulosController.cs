﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.TiposArticulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Transversal.Dependencias;
using SOTWebApi.Extensiones;

namespace SOTWebApi.Controllers.Almacen
{
    [RoutePrefix("api/TiposArticulos")]
    public class TiposArticulosController : ApiController
    {
        private IServicioTiposArticulos AplicacionServicioTiposArticulos
        {
            get { return _aplicacionServicioTiposArticulos.Value; }
        }

        private Lazy<IServicioTiposArticulos> _aplicacionServicioTiposArticulos = new Lazy<IServicioTiposArticulos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioTiposArticulos>();
        });

        [Route("")]
        public List<TipoArticulo> Get()
        {
            return AplicacionServicioTiposArticulos.ObtenerTipos(this.GetDtoUser());
        }
    }
}
