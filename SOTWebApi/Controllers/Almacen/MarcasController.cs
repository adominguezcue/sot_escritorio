﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Marcas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Transversal.Dependencias;

namespace SOTWebApi.Controllers.Almacen
{
    [RoutePrefix("api/Marcas")]
    public class MarcasController : ApiController
    {
        private IServicioMarcas AplicacionServicioMarcas
        {
            get { return _aplicacionServicioMarcas.Value; }
        }

        private Lazy<IServicioMarcas> _aplicacionServicioMarcas = new Lazy<IServicioMarcas>(() => FabricaDependencias.Instancia.Resolver<IServicioMarcas>());

        [Route("")]
        public List<Marca> Get() 
        {
            return AplicacionServicioMarcas.ObtenerMarcas();
        }
    }
}
