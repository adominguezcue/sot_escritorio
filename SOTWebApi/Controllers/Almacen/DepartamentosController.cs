﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Departamentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Transversal.Dependencias;

namespace SOTWebApi.Controllers.Almacen
{
    [RoutePrefix("api/Departamentos")]
    public class DepartamentosController : ApiController
    {
        private IServicioDepartamentos AplicacionServicioDepartamentos
        {
            get { return _aplicacionServicioDepartamentos.Value; }
        }

        private Lazy<IServicioDepartamentos> _aplicacionServicioDepartamentos = new Lazy<IServicioDepartamentos>(() => FabricaDependencias.Instancia.Resolver<IServicioDepartamentos>());

        [Route("")]
        public List<Departamento> Get() 
        {
            bool soloConComandables = false;

            return AplicacionServicioDepartamentos.ObtenerDepartamentos(null, soloConComandables);
        }
    }
}
