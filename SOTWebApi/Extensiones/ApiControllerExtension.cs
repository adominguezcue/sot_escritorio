﻿using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Sesiones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Transversal.Excepciones;

namespace SOTWebApi.Extensiones
{
    public static class ApiControllerExtension
    {
        static ServicioSesiones AplicacionServicioSesiones = new ServicioSesiones();
        //static List<DtoUsuario> usuarios = new List<DtoUsuario>();

        const string SOT_TOKEN = "SOTToken";

        public static DtoUsuario GetDtoUser(this ApiController controlador)
        {
            IEnumerable<string> some;//=new List<object>();
            if (!controlador.Request.Headers.TryGetValues(SOT_TOKEN, out some))
            {
                throw new SOTException("Encabezado Sesion no enviado");
            }

            if (controlador.Request.Headers.GetValues(SOT_TOKEN).ToList().Count == 0)
            {
                throw new Exception("No ha iniciado session");
            }

            var token = controlador.Request.Headers.GetValues(SOT_TOKEN).ToList()[0];


            /*var dtoUserSession = usuarios.FirstOrDefault(m => Equals(m.TokenSesion, token));

            if (dtoUserSession == null)
            {
                */

                //var time = ServiceSessions.GetTimeFromToken(token);

                //if (time > DateTime.Now.AddDays(2))throw new SessionsException("Sesión expirada");

                var dtoUserSession = AplicacionServicioSesiones.ObtenerPorToken(token);


                /*
                usuarios.Add(dtoUserSession);


            }
            else
            {

                throw new SOTException("Sesión no encontrada");

            }*/



            //throw new SessionsException("Sesión no encontrada");



            //

            return dtoUserSession;
        }

        //public static DtoUser GetDtoUser(this ApiController controlador, string token)
        //{
        //    return GetDtoUser(token);
        //}

        //public static DtoUser GetDtoUser(string token)
        //{
        //    var dtoUserSession = usuarios.FirstOrDefault(m => Equals(m.Token, token));

        //    if (dtoUserSession == null)
        //        throw new SOTException("Sesión no encontrada");

        //    var user = dtoUserSession;

        //    return user;

        //}

        /*public static DtoUser Usuario(this ApiController controlador)
        {
            var token = controlador.Request.Headers.GetValues("ZAuth").ToList()[0];
            //var servicio = new ServicioSesiones();

            var usuario = servicio.ObtenerUsuario(token);

            return usuario;
        }
         
         */

        
        //public static bool Verify(string token)
        //{
        //    var dtoUserSession = usuarios.FirstOrDefault(m => Equals(m.TokenSesion, token));


        //    if (dtoUserSession == null)
        //    {
        //        var time = AplicacionServicioSesiones.GetTimeFromToken(token);

        //        //if (time > DateTime.Now.AddDays(2))throw new SessionsException("Sesión expirada");

        //        var session = AplicacionServicioSesiones.GetSession(token);



        //        if (session != null)
        //        {

        //            var dtouser = session.User.ToDtoUserData();
        //            dtouser.Token = token;

        //            var permision = session.User.Role.Permissions.ToObject<DtoPermission>();
        //            dtouser.Permission = permision;


        //            usuarios.Add(dtouser);

        //            dtoUserSession = dtouser;


        //        }
        //        else
        //        {

        //            throw new SessionsException("Sesión no encontrada");

        //        }

        //    }


        //    if (dtoUserSession.DateStart.Date.AddDays(2) < DateTime.Now)
        //        return false;



        //    return true;
        //}

        
        public static void QuitarSesion(string token)
        {
            //var dtoUserSession = usuarios.FirstOrDefault(m => Equals(m.TokenSesion, token));

            //if (dtoUserSession != null)
            //    usuarios.Remove(dtoUserSession);

        }



        public static void AgregarSesion(string token)
        {
            //var dtoUserSession = usuarios.FirstOrDefault(m => Equals(m.TokenSesion, token));

            //if (dtoUserSession == null)
            //    usuarios.Add(dtoUserSession);

        }
    }
}