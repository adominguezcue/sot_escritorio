﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTControladores.Seguridad
{
    public interface ICapturadorHuellaDigital
    {
        void MostrarDialogo();

        Modelo.Seguridad.Dtos.DtoCredencial Credencial { get; }
    }
}
