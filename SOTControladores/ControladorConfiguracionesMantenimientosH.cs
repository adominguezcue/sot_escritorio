﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.ConfiguracionesMantenimientoH;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConfiguracionesMantenimientosH : ControladorBase
    {
        IServicioConfiguracionesMantenimientoH AplicacionServicioConfiguracionesMantenimientoH
        {
            get { return _aplicacionservicioconfiguracionesmantenimientoh.Value; }
        }

        Lazy<IServicioConfiguracionesMantenimientoH> _aplicacionservicioconfiguracionesmantenimientoh = new Lazy<IServicioConfiguracionesMantenimientoH>(
            () => { return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesMantenimientoH>(); });


        public void EsInsertaLecturas(int Idcorte) 
        {
            AplicacionServicioConfiguracionesMantenimientoH.ValidaSiRegistraLectura(Idcorte);
        }

        public void ModificaRegistro(int configturno, int periodo)
        {
            AplicacionServicioConfiguracionesMantenimientoH.ModificaRegistro(configturno, periodo);
        }
    }
}
