﻿using SOTControladores.Buscadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTControladores.Fabricas
{
    public static class FabricaBuscadores
    {
        public static IBuscadorArticulos ObtenerBuscadorArticulos() 
        {
            return Transversal.Dependencias.FabricaDependencias.Instancia.Resolver<IBuscadorArticulos>();
        }
    }
}
