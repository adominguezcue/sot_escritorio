﻿using Modelo.Almacen.Entidades.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOTControladores.Buscadores
{
    public interface IBuscadorArticulos : IBuscador
    {
        DtoResultadoBusquedaArticuloSimple ArticuloSeleccionado { get; }
        bool SoloInventariables { get; set; }
    }
}
