﻿using Modelo.Entidades;
using Negocio.Areas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorAreas : ControladorBase
    {
        private IServicioAreas AplicacionServicioAreas
        {
            get { return _aplicacionServicioAreas.Value; }
        }

        private Lazy<IServicioAreas> _aplicacionServicioAreas = new Lazy<IServicioAreas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioAreas>();
        });

        public List<Area> ObtenerAreasActivasConPuestos()
        {
            return AplicacionServicioAreas.ObtenerAreasActivasConPuestos();
        }

        public List<Area> ObtenerAreasActivas()
        {
            return AplicacionServicioAreas.ObtenerAreasActivas();
        }
    }
}
