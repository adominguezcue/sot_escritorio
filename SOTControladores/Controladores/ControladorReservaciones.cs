﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Reservaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorReservaciones: ControladorBase
    {
        private IServicioReservaciones AplicacionServicioReservaciones
        {
            get { return _aplicacionServicioReservaciones.Value; }
        }

        private Lazy<IServicioReservaciones> _aplicacionServicioReservaciones = new Lazy<IServicioReservaciones>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioReservaciones>();
        });

        //public decimal ObtenerValorReservacionesPendientesPorPeriodo(DateTime? fechaInicio = null, DateTime? fechaFin = null)
        //{
        //    return AplicacionServicioReservaciones.ObtenerValorReservacionesPendientesPorPeriodo(UsuarioActual, fechaInicio, fechaFin);
        //}

        public List<Modelo.Dtos.DtoReservacion> ObtenerReservacionesFiltradas(DateTime fechaInicio, DateTime fechaFin, string idEstado, int idTipoHabitacion = 0)
        {
            return AplicacionServicioReservaciones.ObtenerReservacionesFiltradas(fechaInicio, fechaFin, UsuarioActual, idEstado, idTipoHabitacion);																								
        }
        public List<Modelo.Dtos.DtoReservacion> ObtenerReservaciones(string reservacion)
        {
            return AplicacionServicioReservaciones.ObtenerReservaciones(reservacion, UsuarioActual);
        }
		
		public List<Modelo.Dtos.DtoReservacion> ObtenerReservacionesPorFechaCreacion(DateTime fechaInicio, DateTime fechaFin, string idEstado, int idTipoHabitacion = 0)
        {
            return AplicacionServicioReservaciones.ObtenerReservacionesPorFechaCreacion( fechaInicio,  fechaFin, UsuarioActual, idEstado, idTipoHabitacion);
        }
		
        public Modelo.Entidades.Reservacion ObtenerReservacionParaEditar(int idReservacion)
        {
            return AplicacionServicioReservaciones.ObtenerReservacionParaEditar(idReservacion, UsuarioActual);
        }

        public void CrearReservacion(Reservacion reservacionActual, List<DtoInformacionPago> informacionPagos, DatosFiscales datosFiscales)
        {
            AplicacionServicioReservaciones.CrearReservacion(reservacionActual, informacionPagos, datosFiscales, UsuarioActual);
        }

        public void ModificarReservacion(Reservacion reservacionActual, List<DtoInformacionPago> informacionPagos, DatosFiscales datosFiscales)
        {
            AplicacionServicioReservaciones.ModificarReservacion(reservacionActual, informacionPagos, datosFiscales, UsuarioActual);
        }

        public void CancelarReservacion(int idReservacion)
        {
            AplicacionServicioReservaciones.CancelarReservacion(idReservacion, UsuarioActual);
        }

        public int ObtenerCantidadReservacionesDia(int idTipoHabitacion)
        {
            return AplicacionServicioReservaciones.ObtenerCantidadReservacionesPendientesDia(idTipoHabitacion);
        }

        public void AsignarHabitacion(int idReservacion, int idHabitacion)
        {
            AplicacionServicioReservaciones.AsignarHabitacion(idReservacion, idHabitacion, UsuarioActual);
        }

        public void DesasignarHabitacion(int idHabitacion)
        {
            AplicacionServicioReservaciones.DesasignarHabitacion(idHabitacion, UsuarioActual);
        }

        public string ObtenerCodigoReservacionActual(int idHabitacion)
        {
            return AplicacionServicioReservaciones.ObtenerCodigoReservacionActual(idHabitacion);
        }

        public Modelo.Entidades.Reservacion ObtenerReservacionActual(int idHabitacion)
        {
            return AplicacionServicioReservaciones.ObtenerReservacionActual(idHabitacion, UsuarioActual);
        }

        public void CambiarHabitacion(int idHabitacionOrigen, int idHabitacionDestino)
        {
            AplicacionServicioReservaciones.CambiarHabitacion(idHabitacionOrigen, idHabitacionDestino, UsuarioActual);
        }
    }
}
