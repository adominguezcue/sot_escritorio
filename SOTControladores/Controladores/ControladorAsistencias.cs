﻿using Modelo.Dtos;
using Modelo.Entidades;
using Modelo.Seguridad.Dtos;
using Negocio.Asistencias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorAsistencias : ControladorBase
    {
        private IServicioAsistencias AplicacionServicioAsistencias
        {
            get { return _aplicacionServicioAsistencias.Value; }
        }

        private Lazy<IServicioAsistencias> _aplicacionServicioAsistencias = new Lazy<IServicioAsistencias>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioAsistencias>();
        });

        public DtoResumenAsistencia ObtenerResumenUltimaAsistenciaAbierta(string numeroEmpleado) 
        {
            return AplicacionServicioAsistencias.ObtenerResumenUltimaAsistenciaAbierta(numeroEmpleado);
        }

        public List<Asistencia> ObtenerAsistenciasFiltadas(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaInicio = null, DateTime? fechaFin = null) 
        {
            return AplicacionServicioAsistencias.ObtenerAsistenciasFiltadas(UsuarioActual, nombre, apellidoPaterno, apellidoMaterno, telefono, fechaInicio, fechaFin);
        }

        public Asistencia RegistrarAsistencia(int idEmpleado, DtoCredencial datosCredencial) 
        {
            return AplicacionServicioAsistencias.RegistrarAsistencia(idEmpleado, datosCredencial);
        }
        public Asistencia RegistrarSalidaAComer(int idEmpleado, DtoCredencial datosCredencial)
        {
            return AplicacionServicioAsistencias.RegistrarSalidaAComer(idEmpleado, datosCredencial);
        }
        public Asistencia RegistrarRegresoComer(int idEmpleado, DtoCredencial datosCredencial)
        {
            return AplicacionServicioAsistencias.RegistrarRegresoComer(idEmpleado, datosCredencial);
        }

        public Asistencia RegistrarAsistencia(int idEmpleado, DateTime fechaEntrada, DateTime? fechaSalida = null) 
        {
            return AplicacionServicioAsistencias.RegistrarAsistencia(UsuarioActual, idEmpleado, fechaEntrada, fechaSalida);
        }

        public void EliminarAsistencia(int idAsistencia) 
        {
            AplicacionServicioAsistencias.EliminarAsistencia(idAsistencia, UsuarioActual);
        }

        public void ModificarAsistencia(int idAsistencia, DateTime fechaEntrada, DateTime? fechaSalida = null)
        {
            AplicacionServicioAsistencias.ModificarAsistencia(UsuarioActual, idAsistencia, fechaEntrada, fechaSalida);
        }
    }
}
