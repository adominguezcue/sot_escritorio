﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Localizaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorEstados : ControladorBase
    {
        private IServicioEstados AplicacionServicioEstados
        {
            get { return _aplicacionServicioEstados.Value; }
        }

        private Lazy<IServicioEstados> _aplicacionServicioEstados = new Lazy<IServicioEstados>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioEstados>();
        });

        public List<Estado> ObtenerEstados()
        {
            return AplicacionServicioEstados.ObtenerEstados();
        }
    }
}
