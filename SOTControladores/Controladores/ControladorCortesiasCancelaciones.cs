﻿using System;
using Negocio.CortesiasCancelaciones;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Modelo.Dtos;
using Modelo.Entidades;

namespace SOTControladores.Controladores
{
    public class ControladorCortesiasCancelaciones : ControladorBase
    {

        private IServicioCortesiasCancelaciones AplicacionServicioCortesiasCancelaciones
        {
            get { return _AplicacionServicioCortesiasCancelaciones.Value; }
        }

        Lazy<IServicioCortesiasCancelaciones> _AplicacionServicioCortesiasCancelaciones = new Lazy<IServicioCortesiasCancelaciones>(
            () => { return FabricaDependencias.Instancia.Resolver<IServicioCortesiasCancelaciones>(); });

        public List<VW_ConceptoCancelacionesYCortesias> ObtieneConceptos()
        {
            return AplicacionServicioCortesiasCancelaciones.ObtieneConceptos();
        }

        public List<DtoCortesiasCancelaciones> ObtieneCortesiasCancelaciones(string concepto, DateTime fechainicio, DateTime fechafinal)
        {
            return AplicacionServicioCortesiasCancelaciones.ObtieneCortesiasCancelaciones(concepto, fechainicio, fechafinal, UsuarioActual);
        }

        public List<VW_VentaCorrecta> ObtieneVentaCorrecta(int ticketseleccionado, string tipoid)
        {
            return AplicacionServicioCortesiasCancelaciones.ObtieneVentaCorrecta(ticketseleccionado, tipoid);
        }
    }
}
