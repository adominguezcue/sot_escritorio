﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Departamentos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorDepartamentos : ControladorBase
    {
        private IServicioDepartamentos AplicacionServicioDepartamentos
        {
            get { return _aplicacionServicioDepartamentos.Value; }
        }

        private Lazy<IServicioDepartamentos> _aplicacionServicioDepartamentos = new Lazy<IServicioDepartamentos>(() => FabricaDependencias.Instancia.Resolver<IServicioDepartamentos>());

        public List<Departamento> ObtenerDepartamentosParaFiltro(bool soloConComandables = false) 
        {
            return AplicacionServicioDepartamentos.ObtenerDepartamentosParaFiltro(soloConComandables);
        }

        public List<Departamento> ObtenerDepartamentos(bool soloConComandables = false)
        {
            return AplicacionServicioDepartamentos.ObtenerDepartamentos(UsuarioActual, soloConComandables);
        }

        public void CrearDepartamento(Departamento departamento)
        {
            AplicacionServicioDepartamentos.CrearDepartamento(departamento, UsuarioActual);
        }

        public void ModificarDepartamento(Departamento departamento)
        {
            AplicacionServicioDepartamentos.ModificarDepartamento(departamento, UsuarioActual);
        }

        public void EliminarDepartamento(int idDepartamento)
        {
            AplicacionServicioDepartamentos.EliminarDepartamento(idDepartamento, UsuarioActual);
        }
    }
}
