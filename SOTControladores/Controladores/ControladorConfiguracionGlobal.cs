﻿using Modelo.Almacen.Entidades;
using Modelo.Entidades;
using Negocio.Almacen.Parametros;
using Negocio.ConfiguracionesGlobales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConfiguracionGlobal : ControladorBase
    {
        private Lazy<ZctCatPar> cfg = new Lazy<ZctCatPar>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioParametros>().ObtenerParametros();
        });

        private IServicioParametros AplicacionServicioParametros
        {
            get { return _aplicacionServicioParametros.Value; }
        }

        private Lazy<IServicioParametros> _aplicacionServicioParametros = new Lazy<IServicioParametros>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioParametros>();
        });

        private IServicioConfiguracionesGlobales AplicacionServicioConfiguracionesGlobales
        {
            get { return _aplicacionServicioConfiguracionesGlobales.Value; }
        }

        private Lazy<IServicioConfiguracionesGlobales> _aplicacionServicioConfiguracionesGlobales = new Lazy<IServicioConfiguracionesGlobales>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesGlobales>();
        });

        public ZctCatPar ObtenerConfiguracionGlobal() 
        {
            return cfg.Value;
        }

        public List<ConfiguracionTurno> ObtenerConfiguracionesTurno(bool soloActivas = true) 
        {
            return AplicacionServicioConfiguracionesGlobales.ObtenerConfiguracionesTurno(soloActivas);
        }

        public void ValidarConfiguracionGlobalEsCorrecta()
        {
            AplicacionServicioParametros.ValidarConfiguracionGlobalEsCorrecta();
        }

        public DepartamentosMaestros ObtenerConfiguracionDepartamentosMaestros()
        {
            return AplicacionServicioConfiguracionesGlobales.ObtenerConfiguracionDepartamentosMaestros();
        }

        public PuestosMaestros ObtenerConfiguracionPuestosMaestros()
        {
            return AplicacionServicioConfiguracionesGlobales.ObtenerConfiguracionPuestosMaestros();
        }

        public void ConfigurarTurnos(List<ConfiguracionTurno> configuracionesTurnos)
        {
            AplicacionServicioConfiguracionesGlobales.ConfigurarTurnos(configuracionesTurnos, UsuarioActual);
        }

        public void CrearParametros(ZctCatPar parametrosActuales)
        {
            AplicacionServicioParametros.CrearParametros(parametrosActuales, UsuarioActual);
        }

        public void ModificarParametros(ZctCatPar parametrosActuales)
        {
            AplicacionServicioParametros.ModificarParametros(parametrosActuales, UsuarioActual);
        }

        public void EliminarParametros()
        {
            AplicacionServicioParametros.EliminarParametros(UsuarioActual);
        }
    }
}
