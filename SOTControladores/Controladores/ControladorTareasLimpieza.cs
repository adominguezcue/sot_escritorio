﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Modelo;
using Negocio.TareasLimpieza;
using Transversal.Dependencias;
using Modelo.Entidades;

namespace SOTControladores.Controladores
{
	public class ControladorTareasLimpieza : ControladorBase
	{
		private IServicioTareasLimpieza AplicacionServicioTareasLimpieza {
			get { return _aplicacionServicioTareasLimpieza.Value; }
		}

		private Lazy<IServicioTareasLimpieza> _aplicacionServicioTareasLimpieza = new Lazy<IServicioTareasLimpieza>(() =>
		{
            return FabricaDependencias.Instancia.Resolver<IServicioTareasLimpieza>();
		});
		public TareaLimpieza ObtenerTareaActualConEmpleados(int idHabitacion)
		{
			return AplicacionServicioTareasLimpieza.ObtenerTareaActualConEmpleados(idHabitacion, UsuarioActual);
		}

		public Modelo.Dtos.DtoDetallesTarea ObtenerDetallesTarea(int idHabitacion)
		{
			return AplicacionServicioTareasLimpieza.ObtenerDetallesTarea(idHabitacion, UsuarioActual);
		}

        public List<Modelo.Dtos.DtoDetallesTareaImpuro> ObtenerDetallesTareasPorFecha(int? ordenTurno = null, int? idEmpleado = null, int? idTipoHabitacion = null, DateTime? fechaInicio = null, DateTime? fechaFin = null) 
        {
            return AplicacionServicioTareasLimpieza.ObtenerDetallesTareasPorFecha(UsuarioActual, ordenTurno, idEmpleado, idTipoHabitacion, fechaInicio, fechaFin);
        }

        public void FinalizarLimpieza(int idHabitacion, int idMotivo, string observaciones)
		{
            AplicacionServicioTareasLimpieza.FinalizarLimpieza(idHabitacion, idMotivo, observaciones, UsuarioActual);
		}

		public void AsignarEmpleadosATarea(int idHabitacion, List<int> idsEmpleados, TareaLimpieza.TiposLimpieza tipoLimpieza)
		{
			AplicacionServicioTareasLimpieza.AsignarEmpleadosATarea(idHabitacion, idsEmpleados, tipoLimpieza, UsuarioActual);
		}

		public void CambiarEmpleadosTarea(int idHabitacion, List<int> idsEmpleados, TareaLimpieza.TiposLimpieza tipoLimpieza)
		{
			AplicacionServicioTareasLimpieza.CambiarEmpleadosTarea(idHabitacion, idsEmpleados, tipoLimpieza, UsuarioActual);
		}

		public void AsignarSupervisorATarea(int idHabitacion, int idsEmpleado)
		{
			AplicacionServicioTareasLimpieza.AsignarSupervisorATarea(idHabitacion, idsEmpleado, UsuarioActual);
		}

		public void CambiarSupervisorTarea(int idHabitacion, int idsEmpleado)
		{
			AplicacionServicioTareasLimpieza.CambiarSupervisorTarea(idHabitacion, idsEmpleado, UsuarioActual);
		}

		public List<LimpiezaEmpleado> ObtenerEmpleadosLimpiezaActivos()
		{
			return AplicacionServicioTareasLimpieza.ObtenerEmpleadosLimpiezaActivos(UsuarioActual);
		}

	}
}