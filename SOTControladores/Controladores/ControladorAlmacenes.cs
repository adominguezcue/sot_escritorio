﻿using Modelo.Almacen.Entidades.Dtos;
using Negocio.Almacen.Almacenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorAlmacenes : ControladorBase
    {
        private IServicioAlmacenes AplicacionServicioAlmacenes
        {
            get { return _aplicacionServicioAlmacenes.Value; }
        }

        private Lazy<IServicioAlmacenes> _aplicacionServicioAlmacenes = new Lazy<IServicioAlmacenes>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioAlmacenes>();
        });

        public List<Modelo.Almacen.Entidades.Almacen> ObtenerAlmacenes() 
        {
            return AplicacionServicioAlmacenes.ObtenerAlmacenes(UsuarioActual);
        }

        public void VincularCentroCostos(int idAlmacen, int idCentroCostos)
        {
            AplicacionServicioAlmacenes.VincularCentroCostos(idAlmacen, idCentroCostos, UsuarioActual);
        }

        public void DesvincularCentroCostos(int idAlmacen)
        {
            AplicacionServicioAlmacenes.DesvincularCentroCostos(idAlmacen, UsuarioActual);
        }

        public DtoEncabezadoIntercambio SP_ObtenerResumenIntercambio(int idFolio)
        {
            return AplicacionServicioAlmacenes.SP_ObtenerResumenIntercambio(idFolio, UsuarioActual);
        }
    }
}
