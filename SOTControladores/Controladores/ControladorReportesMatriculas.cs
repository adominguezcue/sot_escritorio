﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Negocio.ReportesMatriculas;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{

	public class ControladorReportesMatriculas : ControladorBase
	{
        private IServicioReportesMatriculas AplicacionServicioReportesMatriculas
        {
			get { return _aplicacionServicioReportesMatriculas.Value; }
		}

		private Lazy<IServicioReportesMatriculas> _aplicacionServicioReportesMatriculas = new Lazy<IServicioReportesMatriculas>(() =>
		{
            return FabricaDependencias.Instancia.Resolver<IServicioReportesMatriculas>();
		});
		public List<Modelo.Entidades.ReporteMatricula> ObtenerReportesMatricula(string matricula)
		{
			return AplicacionServicioReportesMatriculas.ObtenerReportesMatricula(matricula, UsuarioActual);
		}

		public void CrearReporte(Modelo.Entidades.ReporteMatricula reporte)
		{
			AplicacionServicioReportesMatriculas.CrearReporte(reporte, UsuarioActual);
		}

		public void EliminarReporte(int idReporte)
		{
			AplicacionServicioReportesMatriculas.EliminarReporte(idReporte, UsuarioActual);
		}

		public bool PoseeReportes(string matricula)
		{
			return AplicacionServicioReportesMatriculas.PoseeReportes(matricula);
		}

	}

}