﻿using Modelo.Entidades;
using Negocio.Facturacion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorFacturacion : ControladorBase
    {
        private IServicioDatosFiscales AplicacionServicioDatosFiscales
        {
            get { return _aplicacionServicioDatosFiscales.Value; }
        }

        private Lazy<IServicioDatosFiscales> _aplicacionServicioDatosFiscales = new Lazy<IServicioDatosFiscales>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioDatosFiscales>();
        });

        public void AgregarDatosFiscales(DatosFiscales datosF)
        {
            AplicacionServicioDatosFiscales.AgregarDatosFiscales(datosF, UsuarioActual);
        }

        public void ActualizarDatosFiscales(DatosFiscales datosF)
        {
            AplicacionServicioDatosFiscales.ActualizarDatosFiscales(datosF, UsuarioActual);
        }

        public DatosFiscales ObtenerDatosPorRfc(string rfc) 
        {
            return AplicacionServicioDatosFiscales.ObtenerDatosPorRfc(rfc, UsuarioActual);
        }

        public DatosFiscales ObtenerDatosPorId(int idDatosFiscales)
        {
            return AplicacionServicioDatosFiscales.ObtenerDatosPorId(idDatosFiscales, UsuarioActual);
        }
    }
}
