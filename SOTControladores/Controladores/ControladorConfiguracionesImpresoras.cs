﻿using Modelo.Entidades;
using Modelo.Entidades.Parciales;
using Negocio.ConfiguracionesImpresoras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConfiguracionesImpresoras : ControladorBase
    {
        private IServicioConfiguracionesImpresoras AplicacionServicioConfiguracionesImpresoras
        {
            get { return _aplicacionServicioConfiguracionesImpresoras.Value; }
        }

        private Lazy<IServicioConfiguracionesImpresoras> _aplicacionServicioConfiguracionesImpresoras = new Lazy<IServicioConfiguracionesImpresoras>(() => FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesImpresoras>());

        public ConfiguracionImpresoras ObtenerConfiguracionImpresoras()
        {
            return AplicacionServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras(UsuarioActual);
        }

        public DtoConfiguracionImpresoras ObtenerImpresoras()
        {
            return AplicacionServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresoras();
        }

        public ConfiguracionImpresoras ObtenerConfiguracionImpresorasLocal()
        {
            return AplicacionServicioConfiguracionesImpresoras.ObtenerConfiguracionImpresorasLocal(UsuarioActual);
        }

        public void ActualizarConfiguracion(ConfiguracionImpresoras configuracionImpresoras)
        {
            AplicacionServicioConfiguracionesImpresoras.ActualizarConfiguracion(configuracionImpresoras, UsuarioActual);
        }

        public void ActualizarConfiguracionLocal(ConfiguracionImpresoras configuracionImpresoras)
        {
            AplicacionServicioConfiguracionesImpresoras.ActualizarConfiguracionLocal(configuracionImpresoras, UsuarioActual);
        }
    }
}
