﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorClientes : ControladorBase
    {
        private IServicioClientes AplicacionServicioClientes
        {
            get { return _aplicacionServicioClientes.Value; }
        }

        private Lazy<IServicioClientes> _aplicacionServicioClientes = new Lazy<IServicioClientes>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioClientes>();
        });

        public bool VerificarNoExisteCliente() 
        {
            return AplicacionServicioClientes.VerificarNoExisteCliente();
        }

        public int ObtenerUltimoIdCliente() 
        {
            return AplicacionServicioClientes.ObtenerUltimoIdCliente();
        }

        public Cliente ObtenerUltimoCliente() 
        {
            return AplicacionServicioClientes.ObtenerUltimoCliente();
        }

        public void CrearCliente(Cliente cliente)
        {
            AplicacionServicioClientes.CrearCliente(cliente, UsuarioActual);
        }

        public void ModificarCliente(Cliente cliente)
        {
            AplicacionServicioClientes.ModificarCliente(cliente, UsuarioActual);
        }

        public void EliminarCliente(int codigoCliente)
        {
            AplicacionServicioClientes.EliminarCliente(codigoCliente, UsuarioActual);
        }
    }
}
