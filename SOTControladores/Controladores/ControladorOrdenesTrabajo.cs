﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.OrdenesTrabajo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorOrdenesTrabajo : ControladorBase
    {
        private IServicioOrdenesTrabajo AplicacionServicioOrdenesTrabajo
        {
            get { return _aplicacionServicioOrdenesTrabajo.Value; }
        }

        private Lazy<IServicioOrdenesTrabajo> _aplicacionServicioOrdenesTrabajo = new Lazy<IServicioOrdenesTrabajo>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioOrdenesTrabajo>();
        });

        public List<ZctEncOT> ObtenerEncabezadosOrdenesAprobadasFILTRO() 
        {
            return AplicacionServicioOrdenesTrabajo.ObtenerEncabezadosOrdenesAprobadasFILTRO();
        }

        public Boolean PermiteGrabar(int folio)
        {
            return AplicacionServicioOrdenesTrabajo.PermiteGrabar(folio);
        }

        public Boolean PermiteEditarDetalles(int folio)
        {
            return AplicacionServicioOrdenesTrabajo.PermiteEditarDetalles(folio);
        }

        public VW_ZctStatusOT ObtenerStatusOrdenTrabajo(int folio)
        {
            return AplicacionServicioOrdenesTrabajo.ObtenerStatusOrdenTrabajo(folio);
        }
    }
}
