﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.SolicitudesPermisosFaltas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorSolicitudesPermisosFaltas : ControladorBase
    {
        private IServicioSolicitudesPermisosFaltas AplicacionServicioSolicitudesPermisosFaltas
        {
            get { return _aplicacionServicioSolicitudesPermisosFaltas.Value; }
        }

        private Lazy<IServicioSolicitudesPermisosFaltas> _aplicacionServicioSolicitudesPermisosFaltas = new Lazy<IServicioSolicitudesPermisosFaltas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioSolicitudesPermisosFaltas>();
        });

        public void SolicitarPermisoAnticipado(SolicitudPermiso solicitudPermiso) 
        {
            AplicacionServicioSolicitudesPermisosFaltas.SolicitarPermisoAnticipado(solicitudPermiso, UsuarioActual);
        }

        public void SolicitarPermisoMismoDia(SolicitudPermiso solicitudPermiso) 
        {
            AplicacionServicioSolicitudesPermisosFaltas.SolicitarPermisoMismoDia(solicitudPermiso, UsuarioActual);
        }

        public void SolicitarFalta(SolicitudFalta solicitudFalta) 
        {
            AplicacionServicioSolicitudesPermisosFaltas.SolicitarFalta(solicitudFalta, UsuarioActual);
        }

        public void SolicitarCambioDescanso(SolicitudCambioDescanso solicitudCambioDescanso) 
        {
            AplicacionServicioSolicitudesPermisosFaltas.SolicitarCambioDescanso(solicitudCambioDescanso, UsuarioActual);
        }

        public void SolicitarVacaciones(SolicitudVacaciones solicitudVacaciones) 
        {
            AplicacionServicioSolicitudesPermisosFaltas.SolicitarVacaciones(solicitudVacaciones, UsuarioActual);
        }

        public List<DtoSolicitudFalta> ObtenerSolicitudesFaltas(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudesFaltas(UsuarioActual, nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public List<DtoSolicitudVacaciones> ObtenerSolicitudesVacaciones(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudesVacaciones(UsuarioActual, nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public List<DtoSolicitudPermiso> ObtenerSolicitudesPermiso(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudesPermiso(UsuarioActual, nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public List<DtoSolicitudCambioDescanso> ObtenerSolicitudesCambioDescanso(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudesCambioDescanso(UsuarioActual, nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public SolicitudVacaciones ObtenerSolicitudVacaciones(int idSolicitudVacaciones)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudVacaciones(idSolicitudVacaciones, UsuarioActual);
        }

        public SolicitudFalta ObtenerSolicitudFalta(int idSolicitudVacaciones)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudFalta(idSolicitudVacaciones, UsuarioActual);
        }

        public SolicitudPermiso ObtenerSolicitudPermiso(int idSolicitudVacaciones)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudPermiso(idSolicitudVacaciones, UsuarioActual);
        }
        public SolicitudCambioDescanso ObtenerSolicitudCambioDescanso(int idSolicitudVacaciones)
        {
            return AplicacionServicioSolicitudesPermisosFaltas.ObtenerSolicitudCambioDescanso(idSolicitudVacaciones, UsuarioActual);
        }


        public void ActualizarSolicitudCambioDescanso(int idSolicitudVacaciones, int? idSuplente, DateTime fechaNueva, bool esDescanso)
        {
            AplicacionServicioSolicitudesPermisosFaltas.ActualizarSolicitudCambioDescanso(idSolicitudVacaciones, idSuplente, fechaNueva, esDescanso, UsuarioActual);
        }
        public void ActualizarSolicitudFalta(int idSolicitudVacaciones, int? idSuplente, DateTime fechaNueva, string motivo)
        {
            AplicacionServicioSolicitudesPermisosFaltas.ActualizarSolicitudFalta(idSolicitudVacaciones, idSuplente, fechaNueva, motivo, UsuarioActual);
        }
        public void ActualizarSolicitudPermisoAnticipado(int idSolicitudVacaciones, DateTime fechaNueva, string motivo)
        {
            AplicacionServicioSolicitudesPermisosFaltas.ActualizarSolicitudPermisoAnticipado(idSolicitudVacaciones, fechaNueva, motivo, UsuarioActual);
        }
        public void ActualizarSolicitudPermisoMismoDia(int idSolicitudVacaciones, DateTime fechaNueva, string motivo)
        {
            AplicacionServicioSolicitudesPermisosFaltas.ActualizarSolicitudPermisoMismoDia(idSolicitudVacaciones, fechaNueva, motivo, UsuarioActual);
        }
        public void ActualizarSolicitudVacaciones(int idSolicitudVacaciones, DateTime fechaInicial, DateTime fechaFinal)
        {
            AplicacionServicioSolicitudesPermisosFaltas.ActualizarSolicitudVacaciones(idSolicitudVacaciones, fechaInicial, fechaFinal, UsuarioActual);
        }

        public void EliminarSolicitudVacaciones(int idSolicitudVacaciones)
        {
            AplicacionServicioSolicitudesPermisosFaltas.EliminarSolicitudVacaciones(idSolicitudVacaciones, UsuarioActual);
        }

        public void EliminarSolicitudFaltas(int idSolicitudFalta)
        {
            AplicacionServicioSolicitudesPermisosFaltas.EliminarSolicitudFalta(idSolicitudFalta, UsuarioActual);
        }

        public void EliminarSolicitudPermiso(int idSolicitudPermiso)
        {
            AplicacionServicioSolicitudesPermisosFaltas.EliminarSolicitudPermiso(idSolicitudPermiso, UsuarioActual);
        }

        public void EliminarSolicitudCambioDescanso(int idSolicitudCambioDescanso)
        {
            AplicacionServicioSolicitudesPermisosFaltas.EliminarSolicitudCambioDescanso(idSolicitudCambioDescanso, UsuarioActual);
        }
    }
}
