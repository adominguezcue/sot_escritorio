﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Negocio.Almacen.OrdenesCompra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorOrdenesCompra : ControladorBase
    {
        private IServicioOrdenesCompra AplicacionServicioOrdenesCompra
        {
            get { return _aplicacionServicioOrdenesCompra.Value; }
        }

        private Lazy<IServicioOrdenesCompra> _aplicacionServicioOrdenesCompra = new Lazy<IServicioOrdenesCompra>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioOrdenesCompra>();
        });

        public DtoEncabezadoOC ObtenerResumenOrdenCompra(int codigoOrdenCompra)
        {
            return AplicacionServicioOrdenesCompra.ObtenerResumenOrdenCompra(codigoOrdenCompra, UsuarioActual);
        }

        //public List<DtoEncabezadoOC> SP_ZctEncOC_CONSULTA(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra,
        //                                                  DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion)
        //{
        //    return AplicacionServicioOrdenesCompra.SP_ZctEncOC_CONSULTA(codigoOrdenCompra, codigoProveedor, fechaOrdenCompra, fechaAplicacionMovimiento, facturaOrdenCompra, fechaFacturacion);
        //}

        //public List<DtoDetalleOC> SP_ZctDetOC_CONSULTA(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, int cantidadArticulo,
        //                                               decimal costoArticulo, int cantidadEnStock, DateTime fechaAplicacion, int idAlmacen, bool omitirIVA)
        //{
        //    return AplicacionServicioOrdenesCompra.SP_ZctDetOC_CONSULTA(codigoOrdenCompra, codigoDetalle, codigoArticulo, cantidadArticulo, costoArticulo, cantidadEnStock, fechaAplicacion, idAlmacen, omitirIVA);
        //}

        //public void SP_ZctEncOC_EDICION(int codigoOrdenCompra, int codigoProveedor, DateTime fechaOrdenCompra,
        //                                                  DateTime fechaAplicacionMovimiento, string facturaOrdenCompra, DateTime fechaFacturacion, string estado)
        //{
        //    AplicacionServicioOrdenesCompra.SP_ZctEncOC_EDICION(codigoOrdenCompra, codigoProveedor, fechaOrdenCompra, fechaAplicacionMovimiento, facturaOrdenCompra, fechaFacturacion, estado);
        //}

        //public void SP_ZctDetOC_EDICION(int codigoOrdenCompra, int codigoDetalle, string codigoArticulo, int cantidadArticulo,
        //                                               decimal costoArticulo, int cantidadEnStock, DateTime fechaAplicacion, int idAlmacen)
        //{
        //    AplicacionServicioOrdenesCompra.SP_ZctDetOC_EDICION(codigoOrdenCompra, codigoDetalle, codigoArticulo, cantidadArticulo, costoArticulo, cantidadEnStock, fechaAplicacion, idAlmacen);
        //}

        public void EditarOrdenCompra(DtoEncabezadoOC encabezado, string sStatus) 
        {
            AplicacionServicioOrdenesCompra.EditarOrdenCompra(encabezado, sStatus, UsuarioActual);
        }

        public void EditarOrdenServicio(DtoEncabezadoOC encabezado, string sStatus)
        {
            AplicacionServicioOrdenesCompra.EditarOrdenServicio(encabezado, sStatus, UsuarioActual);
        }

        /// <summary>
        /// Retorna el mayor de los folios que es menor al proporcionado dentro de las órdenes de compra o de servicio (segun el parámetro esServicio). Retorna null si ninguno cumpe con la condición
        /// </summary>
        /// <param name="folioActual"></param>
        /// <param name="esServicio"></param>
        /// <returns></returns>
        public int? ObtenerUltimoFolioMenorQue(int folioActual, bool esServicio)
        {
            return AplicacionServicioOrdenesCompra.ObtenerUltimoFolioMenorQue(folioActual, esServicio, UsuarioActual);
        }

        /// <summary>
        /// Retorna el menor de los folios que es mayor al proporcionado dentro de las órdenes de compra o de servicio (segun el parámetro esServicio). Retorna null si ninguno cumpe con la condición
        /// </summary>
        /// <param name="folioActual"></param>
        /// <param name="esServicio"></param>
        /// <returns></returns>
        public int? ObtenerPrimerFolioMayorQue(int folioActual, bool esServicio)
        {
            return AplicacionServicioOrdenesCompra.ObtenerPrimerFolioMayorQue(folioActual, esServicio, UsuarioActual);
        }
    }
}
