﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Lineas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorLineas : ControladorBase
    {
        private IServicioLineas AplicacionServicioLineas
        {
            get { return _aplicacionServicioLineas.Value; }
        }

        private Lazy<IServicioLineas> _aplicacionServicioLineas = new Lazy<IServicioLineas>(() => FabricaDependencias.Instancia.Resolver<IServicioLineas>());

        public List<Linea> ObtenerLineasParaFiltro(int cod_dpto = 0) 
        {
            return AplicacionServicioLineas.ObtenerLineasParaFiltro(cod_dpto);
        }

        public List<Linea> ObtenerLineas(bool incluirInactivas = false, int cod_dpto = 0)
        {
            return AplicacionServicioLineas.ObtenerLineas(UsuarioActual, incluirInactivas, cod_dpto);
        }

        public void CrearLinea(Linea linea)
        {
            AplicacionServicioLineas.CrearLinea(linea, UsuarioActual);
        }

        public void ModificarLinea(Linea linea)
        {
            AplicacionServicioLineas.ModificarLinea(linea, UsuarioActual);
        }

        public void DesactivarLinea(int idLinea)
        {
            AplicacionServicioLineas.DesactivarLinea(idLinea, UsuarioActual);
        }

        public void EliminarLinea(int idLinea)
        {
            AplicacionServicioLineas.EliminarLinea(idLinea, UsuarioActual);
        }
    }
}
