﻿using Modelo.Entidades;
using Negocio.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorTarjetasPuntos : ControladorBase
    {
        private IServicioVPoints AplicacionServicioVPoints
        {
            get { return _aplicacionServicioVPoints.Value; }
        }

        private Lazy<IServicioVPoints> _aplicacionServicioVPoints = new Lazy<IServicioVPoints>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioVPoints>();
        });

        public List<TarjetaPuntos> ObtenerTarjetas(DateTime? fechaInicio = null, DateTime? fechaFin = null, TarjetaPuntos.EstadosTarjeta? estado = null) 
        {
            return AplicacionServicioVPoints.ObtenerTarjetas(UsuarioActual, fechaInicio, fechaFin, estado);
        }

        public void CrearTarjeta(TarjetaPuntos tarjeta) 
        {
            AplicacionServicioVPoints.CrearTarjeta(tarjeta, UsuarioActual);
        }

        public void ModificarTarjeta(TarjetaPuntos tarjeta)
        {
            AplicacionServicioVPoints.ModificarTarjeta(tarjeta, UsuarioActual);
        }

        public void EliminarTarjeta(int idTarjeta)
        {
            AplicacionServicioVPoints.EliminarTarjeta(idTarjeta, UsuarioActual);
        }

        public void VenderTarjeta(string idCliente, List<Modelo.Dtos.DtoInformacionPago> pagos) 
        {
            AplicacionServicioVPoints.VenderTarjeta(idCliente, pagos, UsuarioActual);
        }

        public void AltaWebTarjeta(string idCliente)
        {
            AplicacionServicioVPoints.AltaWebTarjeta(idCliente, UsuarioActual);
        }
    }
}
