﻿using Negocio.Seguridad.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorPermisos : ControladorBase
    {
        private IServicioPermisos AplicacionServicioPermisos
        {
            get { return _aplicacionServicioPermisos.Value; }
        }

        private Lazy<IServicioPermisos> _aplicacionServicioPermisos = new Lazy<IServicioPermisos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioPermisos>();
        });

        public Modelo.Seguridad.Dtos.DtoUsuario  ObtenerUsuarioPorCredencial(Modelo.Seguridad.Dtos.DtoCredencial credencial)
        {
            return AplicacionServicioPermisos.ObtenerUsuarioPorCredencial(credencial);
        }

        public bool VerificarPermisos(Modelo.Seguridad.Dtos.DtoPermisos permisos, bool requiereCredencial = false) 
        {
            if (requiereCredencial)
                return AplicacionServicioPermisos.VerificarPermisos(ObtenerUsuarioPorCredencial(ObtenerCredencial()), permisos);

            return AplicacionServicioPermisos.VerificarPermisos(UsuarioActual, permisos);
        }

        //public List<Modelo.Seguridad.Entidades.Permisos> ObtenerPermisos()
        //{
        //    return AplicacionServicioPermisos.ObtenerPermisos(UsuarioActual);
        //}

        public Modelo.Seguridad.Dtos.DtoPermisos ObtenerPermisosActuales()
        {
            return UsuarioActual.Permisos;//AplicacionServicioPermisos.ObtenerPermisos(UsuarioActual);
        }
    }
}
