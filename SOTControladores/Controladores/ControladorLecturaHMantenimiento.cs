﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.LecturaMantenimiento;
using Transversal.Dependencias;
using Modelo.Entidades;

namespace SOTControladores.Controladores
{
    public class ControladorLecturaHMantenimiento : ControladorBase
    {
        private IServicioLecturaHMantenimiento Aplicaionserviciolecturamantenimiento
        {
            get { return _aplicaionServiciolecturamantenimiento.Value; }
        }

        Lazy<IServicioLecturaHMantenimiento> _aplicaionServiciolecturamantenimiento = new Lazy<IServicioLecturaHMantenimiento>(
            () => FabricaDependencias.Instancia.Resolver<IServicioLecturaHMantenimiento>());

        public List<CatPresentacionMantenimiento> ObtienePressentacion()
        {
            return Aplicaionserviciolecturamantenimiento.ObtienePresentacionMantenimiento();
        }

        public List<CatPresentacionMantenimiento> ObtieneTodas()
        {
            return Aplicaionserviciolecturamantenimiento.ObtieneTodas();
        }

        public void AgregaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento)
        {
            Aplicaionserviciolecturamantenimiento.AgregaPresentacion(catPresentacionMantenimiento);
        }

        public void ModificaPresentacion (CatPresentacionMantenimiento catPresentacionMantenimiento)
        {
            Aplicaionserviciolecturamantenimiento.ModificaPresentacion(catPresentacionMantenimiento);
        }

        public void EliminaPresentacion(CatPresentacionMantenimiento catPresentacionMantenimiento)
        {
            Aplicaionserviciolecturamantenimiento.EliminaPresentacion(catPresentacionMantenimiento);
        }


    }
}
