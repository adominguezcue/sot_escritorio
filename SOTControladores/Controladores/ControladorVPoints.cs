﻿using Modelo.Entidades;
using Negocio.VPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorVPoints : ControladorBase
    {
        private IServicioVPoints AplicacionServicioVPoints
        {
            get { return _aplicacionServicioVPoints.Value; }
        }

        private Lazy<IServicioVPoints> _aplicacionServicioVPoints = new Lazy<IServicioVPoints>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioVPoints>();
        });
        private IServicioConfiguracionesPuntosLealtad AplicacionServicioConfiguracionesPuntosLealtad
        {
            get { return _aplicacionServicioConfiguracionesPuntosLealtad.Value; }
        }

        private Lazy<IServicioConfiguracionesPuntosLealtad> _aplicacionServicioConfiguracionesPuntosLealtad = new Lazy<IServicioConfiguracionesPuntosLealtad>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesPuntosLealtad>();
        });

        public decimal ObtenerSaldoTarjetaV(string numeroTarjeta)
        {
            return AplicacionServicioVPoints.ObtenerSaldoTarjetaV(numeroTarjeta, UsuarioActual);
        }

        public Negocio.ServiciosExternos.DTOs.VPoints.DtoDatosCupon ObtenerCupon(string idCupon)
        {
            return AplicacionServicioVPoints.ObtenerCupon(idCupon, UsuarioActual);
        }

        public List<Modelo.Entidades.AbonoPuntos> ObtenerAbonosFallidos(int? idHabitacion = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null) 
        {
            return AplicacionServicioVPoints.ObtenerAbonosFallidos(UsuarioActual, idHabitacion, fechaInicial, fechaFinal);
        }

        public void AbonarManualmente(int idAbono) 
        {
            AplicacionServicioVPoints.AbonarManualmente(idAbono, UsuarioActual);
        }

        public List<ReembolsoPuntos> ObtenerReembolsosFallidos(int? idHabitacion = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            return AplicacionServicioVPoints.ObtenerReembolsosFallidos(UsuarioActual, idHabitacion, fechaInicial, fechaFinal);
        }

        public void ReembolsarManualmente(int idReembolso)
        {
            AplicacionServicioVPoints.ReembolsarManualmente(idReembolso, UsuarioActual);
        }

        public void EliminarConfiguracion()
        {
            AplicacionServicioConfiguracionesPuntosLealtad.EliminarConfiguracion(UsuarioActual);
        }

        public void GuardarConfiguracion(ConfiguracionPuntosLealtad configuracion)
        {
            AplicacionServicioConfiguracionesPuntosLealtad.GuardarConfiguracion(configuracion, UsuarioActual);
        }

        public ConfiguracionPuntosLealtad ObtenerConfiguracion()
        {
            return AplicacionServicioConfiguracionesPuntosLealtad.ObtenerConfiguracionPuntosLealtad();
        }
    }
}
