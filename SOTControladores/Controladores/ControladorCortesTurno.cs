﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.CortesTurno;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorCortesTurno : ControladorBase
    {
        private IServicioCortesTurno AplicacionServicioCortesTurno
        {
            get { return _aplicacionServicioCortesTurno.Value; }
        }

        private Lazy<IServicioCortesTurno> _aplicacionServicioCortesTurno = new Lazy<IServicioCortesTurno>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioCortesTurno>();
        });

        //public int ObtenerSiguienteNumeroCorte() 
        //{
        //    return AplicacionServicioCortesTurno.ObtenerSiguienteNumeroCorte();
        //}

        //public DateTime? ObtenerFechaUltimoCorte()
        //{
        //    return AplicacionServicioCortesTurno.ObtenerFechaUltimoCorte();
        //}

        public CorteTurno ObtenerUltimoCorte()
        {
            return AplicacionServicioCortesTurno.ObtenerUltimoCorte();
        }

        public CorteTurno ObtenerCorteEnRevision(int idCorteTurno)
        {
            return AplicacionServicioCortesTurno.ObtenerCorteEnRevision(idCorteTurno);
        }

        public void RealizarCorteTurno()
        {
            AplicacionServicioCortesTurno.RealizarCorteTurno(UsuarioActual);
        }

        public CorteTurno RevisarCorteTurno(int idCorteTurno)
        {
            return AplicacionServicioCortesTurno.RevisarCorteTurno(idCorteTurno, UsuarioActual);
        }

        public CorteTurno ObtenerDatosUltimoCorteTurno()
        {
            return AplicacionServicioCortesTurno.ObtenerDatosUltimoCorteTurno(UsuarioActual);
        }

        public void CerrarTurno(int idCorteTurno)
        {
            AplicacionServicioCortesTurno.CerrarTurno(idCorteTurno, UsuarioActual);
        }

        public Modelo.Dtos.DtoComisionesMesero ObtenerComisionesMeseros(int idCorte)
        {
            return AplicacionServicioCortesTurno.ObtenerComisionesMeseros(UsuarioActual, idCorte);
        }
        public Modelo.Dtos.DtoComisionesValets ObtenerComisionesValets(int idCorte)
        {
            return AplicacionServicioCortesTurno.ObtenerComisionesValets(UsuarioActual, idCorte);
        }

        public List<Modelo.Dtos.DtoPagoHabitacionCorte> ObtenerMovimientosHabitaciones(DateTime? fechaInicio = null, DateTime? fechaFin = null, TiposPago? formasPago = null, bool soloConPropina = false) 
        {
            return AplicacionServicioCortesTurno.ObtenerMovimientosHabitaciones(UsuarioActual, fechaInicio, fechaFin, formasPago, soloConPropina);
        }

        public bool VerificarExistenTurnos()
        {
            return AplicacionServicioCortesTurno.VerificarExistenTurnos(UsuarioActual);
        }

        public List<Modelo.Dtos.DtoPagoRestauranteCorte> ObtenerMovimientosRestaurante(DateTime? fechaInicio = null, DateTime? fechaFin = null, TiposPago? formaPago = null, bool soloConPropinas = false)
        {
            return AplicacionServicioCortesTurno.ObtenerMovimientosRestaurante(UsuarioActual, fechaInicio, fechaFin, formaPago, soloConPropinas);
        }

        public List<Modelo.Dtos.DtoPagoRestauranteCorte> ObtenerMovimientosRoomService(DateTime? fechaInicio = null, DateTime? fechaFin = null, TiposPago? formaPago = null, bool soloConPropinas = false)
        {
            return AplicacionServicioCortesTurno.ObtenerMovimientosRoomService(UsuarioActual, fechaInicio, fechaFin, formaPago, soloConPropinas);
        }

        public DtoReporteContable ObtenerReporteContablePorRangoCortes(int corteInicio, int corteFin) 
        {
            return AplicacionServicioCortesTurno.ObtenerReporteContablePorRangoCortes(UsuarioActual, corteInicio, corteFin);
        }

        //public DtoReporteContable ObtenerReporteContableTurnoActual()
        //{
        //    return AplicacionServicioCortesTurno.ObtenerReporteContableTurnoActual(UsuarioActual);
        //}

        public DtoReporteCierre ObtenerReporteCierre(int folioCorte) 
        {
            return AplicacionServicioCortesTurno.ObtenerReporteCierre(UsuarioActual, folioCorte);
        }

        //public DtoReporteCierre ObtenerDatosRevisionPorFolio(int folioCorte) 
        //{
        //    return AplicacionServicioCortesTurno.ObtenerDatosRevisionPorFolio(UsuarioActual, folioCorte);
        //}

        //public DtoReporteCierre ObtenerDatosRevisionPorId(int idCorte)
        //{
        //    return AplicacionServicioCortesTurno.ObtenerDatosRevisionPorId(UsuarioActual, idCorte);
        //}

        //public List<DtoCorteTurno> ObtenerResumenesCortesFinalizadosPorRango(DateTime? fechaInicial, DateTime? fechaFinal)
        //{
        //    return AplicacionServicioCortesTurno.ObtenerResumenesCortesFinalizadosPorRango(fechaInicial, fechaFinal, UsuarioActual);
        //}

        public List<DtoResumenCorteTurno> ObtenerResumenesCortesEnRevision()
        {
            return AplicacionServicioCortesTurno.ObtenerResumenesCortesEnRevision(UsuarioActual);
        }

        public List<DtoAreas> ObtieneAreas()
        {
            return AplicacionServicioCortesTurno.ObtieneAreas(UsuarioActual);
        }
        public List<DtoCorteTurno> ObtenerResumenesCortesPorRango(DateTime? fechaInicial, DateTime? fechaFinal)
        {
            return AplicacionServicioCortesTurno.ObtenerResumenesCortesPorRango(fechaInicial, fechaFinal, UsuarioActual);
        }

        public void AbrirPrimerTurno(int idConfiguracionTurno, DateTime? fechaInicial = null)
        {
            AplicacionServicioCortesTurno.AbrirPrimerTurno(idConfiguracionTurno, UsuarioActual, fechaInicial);
        }
    }
}
