﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Negocio.Almacen.Fichas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorFichas : ControladorBase
    {
        private IServicioFichas AplicacionServicioFichas
        {
            get { return _aplicacionServicioFichas.Value; }
        }

        private Lazy<IServicioFichas> _aplicacionServicioFichas = new Lazy<IServicioFichas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioFichas>();
        });

        public List<Ficha> ObtenerFichasDia(DateTime dia, string nombreArticulo = "")
        {
            return AplicacionServicioFichas.ObtenerFichasDia(dia, UsuarioActual, nombreArticulo);
        }

        public Ficha ObtenerPorId(int idFicha)
        {
            return AplicacionServicioFichas.ObtenerPorIdConArchivos(idFicha, UsuarioActual);
        }

        public void EliminarFicha(int idFicha)
        {
            AplicacionServicioFichas.EliminarFicha(idFicha, UsuarioActual);
        }

        public void AgregarFicha(Ficha ficha)
        {
            AplicacionServicioFichas.AgregarFicha(ficha, UsuarioActual);
        }

        public void ModificarFicha(Ficha ficha, List<DtoArchivoFicha> archivos)
        {
            AplicacionServicioFichas.ModificarFicha(ficha, archivos, UsuarioActual);
        }
    }
}
