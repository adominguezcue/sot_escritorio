﻿using Modelo.Entidades;
using Negocio.ConfiguracionesTarifas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConfiguracionesTarifas : ControladorBase
    {
        private IServicioConfiguracionesTarifas AplicacionServicioConfiguracionesTarifas
        {
            get { return _aplicacionServicioConfiguracionesTarifas.Value; }
        }

        private Lazy<IServicioConfiguracionesTarifas> _aplicacionServicioConfiguracionesTarifas = new Lazy<IServicioConfiguracionesTarifas>(() => FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesTarifas>());

        public List<ConfiguracionTarifa> ObtenerConfiguracionesActivas()
        {
            return AplicacionServicioConfiguracionesTarifas.ObtenerConfiguracionesActivas(UsuarioActual);
        }
    }
}