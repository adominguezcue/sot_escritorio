﻿using Modelo.Entidades;
using Negocio.Gastos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorGastos : ControladorBase
    {
        private IServicioGastos AplicacionServicioGastos
        {
            get { return _aplicacionServicioGastos.Value; }
        }

        private Lazy<IServicioGastos> _aplicacionServicioGastos = new Lazy<IServicioGastos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioGastos>();
        });

        //public List<Gasto> ObtenerGastosSinTurno() 
        //{
        //    return AplicacionServicioGastos.ObtenerGastosSinTurno(UsuarioActual);
        //}

        public void Crear(Gasto gasto, bool corteEnRevision)
        {
            AplicacionServicioGastos.CrearGasto(gasto, corteEnRevision, UsuarioActual);
        }

        public List<Gasto> ObtenerGastosPorTurno(int idCorteTurno, bool soloSinClasificacion)
        {
            return AplicacionServicioGastos.ObtenerGastosPorTurno(idCorteTurno, soloSinClasificacion, UsuarioActual);
        }

        public void CancelarGasto(int idGasto)
        {
            AplicacionServicioGastos.CancelarGasto(idGasto, ObtenerCredencial());
        }

        public void ModificarGasto(int idGasto, decimal nuevoValor)
        {
            AplicacionServicioGastos.ModificarGasto(idGasto, nuevoValor, ObtenerCredencial());
        }
    }
}
