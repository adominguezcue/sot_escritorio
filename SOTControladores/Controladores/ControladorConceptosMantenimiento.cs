﻿using Modelo.Entidades;
using Negocio.ConceptosMantenimiento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConceptosMantenimiento : ControladorBase
    {
        private IServicioConceptosMantenimiento ServicioConceptosMantenimiento
        {
            get { return _servicioConceptosMantenimiento.Value; }
        }

        private Lazy<IServicioConceptosMantenimiento> _servicioConceptosMantenimiento = new Lazy<IServicioConceptosMantenimiento>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosMantenimiento>(); });


        public List<ConceptoMantenimiento> ObtenerConceptos() 
        {
            return ServicioConceptosMantenimiento.ObtenerConceptos();
        }

        public List<ConceptoMantenimiento.Clasificaciones> ObtenerClasificaciones()
        {
            return ServicioConceptosMantenimiento.ObtenerClasificaciones();
        }

        public void CrearConcepto(ConceptoMantenimiento conceptoMantenimiento)
        {
            ServicioConceptosMantenimiento.CrearConcepto(conceptoMantenimiento, UsuarioActual);
        }

        public void ModificarConcepto(ConceptoMantenimiento conceptoMantenimiento)
        {
            ServicioConceptosMantenimiento.ModificarConcepto(conceptoMantenimiento, UsuarioActual);
        }

        public void EliminarConcepto(int idConceptoMantenimiento)
        {
            ServicioConceptosMantenimiento.EliminarConcepto(idConceptoMantenimiento, UsuarioActual);
        }
    }
}
