﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.ConsumosInternos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConsumosInternos : ControladorBase
    {
        private IServicioConsumosInternos AplicacionServicioConsumosInternos
        {
            get { return _aplicacionServicioConsumosInternos.Value; }
        }

        private Lazy<IServicioConsumosInternos> _aplicacionServicioConsumosInternos = new Lazy<IServicioConsumosInternos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConsumosInternos>();
        });

        public ConsumoInterno ObtenerConsumoInternoPorId(int idConsumoInterno)
        {
            return AplicacionServicioConsumosInternos.ObtenerConsumoInternoPorId(idConsumoInterno, UsuarioActual);
        }

        public void CrearConsumoInterno(ConsumoInterno consumoInterno)
        {
            AplicacionServicioConsumosInternos.CrearConsumoInterno(consumoInterno, UsuarioActual);
        }

        public void ModificarConsumoInterno(ConsumoInterno consumoInterno, List<Modelo.Dtos.DtoArticuloPrepararGeneracion> articulos)
        {
            AplicacionServicioConsumosInternos.ModificarConsumoInterno(consumoInterno, articulos, ObtenerCredencial());
        }

        public List<ConsumoInterno> ObtenerConsumosInternosFiltrados(string nombre, string apellidoPaterno, string apellidoMaterno, bool soloEnCurso, DateTime? fechaInicial = null, DateTime? fechaFinal = null)
        {
            return AplicacionServicioConsumosInternos.ObtenerConsumosInternosFiltrados(nombre, apellidoPaterno, apellidoMaterno, soloEnCurso, UsuarioActual, fechaInicial, fechaFinal);
        }

        public ConsumoInterno ObtenerDetallesConsumoInternoPendiente(int idConsumoInterno) 
        {
            return AplicacionServicioConsumosInternos.ObtenerDetallesConsumoInternoPendiente(idConsumoInterno, UsuarioActual);
        }

        public ConsumoInterno ObtenerDetallesConsumoInternoIgnorandoEstado(int idConsumoInterno)
        {
            return AplicacionServicioConsumosInternos.ObtenerDetallesConsumoInternoIgnorandoEstado(idConsumoInterno, UsuarioActual);
        }

        //public ConsumoInterno ObtenerDetallesConsumoInternoCancelado(int idConsumoInterno)
        //{
        //    return AplicacionServicioConsumosInternos.ObtenerDetallesConsumoInternoCancelado(idConsumoInterno, UsuarioActual);
        //}

        public List<DtoResumenComandaExtendido> ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion(string departamento)
        {
            return AplicacionServicioConsumosInternos.ObtenerDetallesConsumosArticulosPreparadosOEnPreparacion(departamento, UsuarioActual);
        }

        public void FinalizarElaboracionProductos(int idConsumoInterno, List<int> idsArticulosComandas)
        {
            AplicacionServicioConsumosInternos.FinalizarElaboracionProductos(idConsumoInterno, idsArticulosComandas, ObtenerCredencial());
        }

        public void EntregarProductos(int idConsumoInterno, List<int> idsArticulosComandas)
        {
            AplicacionServicioConsumosInternos.EntregarProductos(idConsumoInterno, idsArticulosComandas, ObtenerCredencial());
        }

        public void CancelarConsumoInterno(int idConsumoInterno, string motivo) 
        {
            AplicacionServicioConsumosInternos.CancelarConsumoInterno(idConsumoInterno, motivo, ObtenerCredencial());
        }
        public void EntregarConsumoInterno(int idConsumoInterno)
        {
            AplicacionServicioConsumosInternos.EntregarConsumoInterno(idConsumoInterno, ObtenerCredencial());
        }

        public void Imprimir(int IdConsumoInterno)
        {
            AplicacionServicioConsumosInternos.Imprimir(IdConsumoInterno);
        }

        public int ObtenerCantidadEnCurso(string nombre, string apellidoPaterno, string apellidoMaterno)
        {
            return AplicacionServicioConsumosInternos.ObtenerCantidadEnCurso(nombre, apellidoPaterno, apellidoMaterno, UsuarioActual);
        }
    }
}
