﻿using Modelo.Entidades;
using Negocio.Puestos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorPuestos : ControladorBase
    {
        private IServicioPuestos AplicacionServicioPuestos
        {
            get { return _aplicacionServicioPuestos.Value; }
        }

        private Lazy<IServicioPuestos> _aplicacionServicioPuestos = new Lazy<IServicioPuestos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioPuestos>();
        });

        public List<Puesto> ObtenerPuestos()
        {
            return AplicacionServicioPuestos.ObtenerPuestos(UsuarioActual);
        }

        public void EliminarPuesto(int idPuesto)
        {
            AplicacionServicioPuestos.EliminarPuesto(idPuesto, UsuarioActual);
        }

        public void CrearPuesto(Puesto puesto)
        {
            AplicacionServicioPuestos.CrearPuesto(puesto, UsuarioActual);
        }

        public void ModificarPuesto(int idPuesto, int idRol, int idArea)
        {
            AplicacionServicioPuestos.ModificarPuesto(idPuesto, idRol, idArea, UsuarioActual);
        }

        public Puesto ObtenerPuestoPorId(int idPuesto)
        {
            return AplicacionServicioPuestos.ObtenerPuestoPorId(idPuesto);
        }
    }
}
