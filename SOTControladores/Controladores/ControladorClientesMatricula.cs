﻿using Modelo.Entidades;
using Modelo.Dtos;
using System;
using Negocio.MatriculaClientes;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorClientesMatricula : ControladorBase
    {
        private IServicioMatriculaCliente AplicacionServicioMatriculaCliente
        {
            get { return _aplicacionServicioMatriculaCliente.Value; }
        }

        private Lazy<IServicioMatriculaCliente> _aplicacionServicioMatriculaCliente = new Lazy<IServicioMatriculaCliente>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioMatriculaCliente>();
        });

        public List<AutomovilRenta> ObtieneMatriculasAutos(string matricula = null)
        {
            return AplicacionServicioMatriculaCliente.ObtieneMatriculasAutos(UsuarioActual, matricula);
        }

        public List<DtoHabitacionesxMatricula> ObtieneHabitacionesPorMatricula(string matricula = null)
        {
            return AplicacionServicioMatriculaCliente.ObtieneHabitacionesPorMatricula(UsuarioActual, matricula);
        }

        public List<DtTotalComandasxRenta> ObtieneComandasPorRenta(int idrenta)
        {
            return AplicacionServicioMatriculaCliente.ObtieneComandasPorRenta(UsuarioActual,idrenta);
        }

    }
}
