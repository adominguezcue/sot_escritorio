﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Incidencias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorObjetosOlvidados: ControladorBase
    {
        private IServicioObjetosOlvidados AplicacionServicioObjetosOlvidados
        {
            get { return _aplicacionServicioObjetosOlvidados.Value; }
        }

        private Lazy<IServicioObjetosOlvidados> _aplicacionServicioObjetosOlvidados = new Lazy<IServicioObjetosOlvidados>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioObjetosOlvidados>();
        });

        public void RegistrarObjetosOlvidados(Modelo.Entidades.ObjetoOlvidado objetoOlvidado)
        {
            AplicacionServicioObjetosOlvidados.RegistrarObjetosOlvidados(objetoOlvidado, UsuarioActual);
        }

        public List<DtoObjetoOlvidado> ObtenerObjetosOlvidadosFiltradas(DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idHabitacion = null, int? idEmpleado = null)
        {
            return AplicacionServicioObjetosOlvidados.ObtenerObjetosOlvidadosFiltradas(UsuarioActual, fechaInicial, fechaFinal, idHabitacion, idEmpleado);
        }

        public void CancelarObjetoOlvidado(int idObjetoOlvidado) 
        {
            AplicacionServicioObjetosOlvidados.CancelarObjetoOlvidado(idObjetoOlvidado, ObtenerCredencial());
        }
        public void EliminarObjetoOlvidado(int idObjetoOlvidado)
        {
            AplicacionServicioObjetosOlvidados.EliminarObjetoOlvidado(idObjetoOlvidado, ObtenerCredencial());
        }

        public ObjetoOlvidado ObtenerObjetoOlvidado(int idObjetoOlvidado)
        {
            return AplicacionServicioObjetosOlvidados.ObtenerObjetoOlvidado(idObjetoOlvidado, UsuarioActual);
        }

        public void ModificarObjetoOlvidado(ObjetoOlvidado objetoOlvidado)
        {
            AplicacionServicioObjetosOlvidados.ModificarObjetoOlvidado(objetoOlvidado, ObtenerCredencial());
        }
    }
}
