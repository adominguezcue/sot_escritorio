﻿using Modelo.Entidades;
using Negocio.ConsumosInternos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Modelo.Dtos;

namespace SOTControladores.Controladores
{
    public class ControladorConsumoInternoEmpleado : ControladorBase
    {
        private IServicioConsumoInternoEmpleado AplicacionServicionConsumoInternoempleado
        {
            get { return _aplicacionservicionconsumointernoempleado.Value; }
        }

        private Lazy<IServicioConsumoInternoEmpleado> _aplicacionservicionconsumointernoempleado = new Lazy<IServicioConsumoInternoEmpleado>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConsumoInternoEmpleado>();
        });

        public List<VW_ConsumoInternoEmpleado> ObtieneEmpleados(string nombreempleado)
        {
          return  AplicacionServicionConsumoInternoempleado.ObtieneEmpleados(nombreempleado);
        }

        public List<DtoConsumoInternosEmpleado> ObtieneConsumoInternoempleado(string numeroempleado, DateTime fechainicial, DateTime fechafinal, bool Esfechashablitadas)
        {
            return AplicacionServicionConsumoInternoempleado.ObtieneConsumoInternoempleado(numeroempleado, fechainicial, fechafinal, Esfechashablitadas);
        }
    }
}
