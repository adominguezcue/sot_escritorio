﻿using Modelo.Dtos.Reportes;
using Negocio.Reportes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Transversal.Utilidades;

namespace SOTControladores.Controladores
{
    public class ControladorReportes : ControladorBase
    {
        private IServicioReportes AplicacionServicioReportes
        {
            get { return _aplicacionServicioReportes.Value; }
        }

        private Lazy<IServicioReportes> _aplicacionServicioReportes = new Lazy<IServicioReportes>(() => FabricaDependencias.Instancia.Resolver<IServicioReportes>());

        public DtoReporte GetReportFromSheets(List<ExcelData.DtoWorkSheet> sheets, string reportName, string folioCorte = null)
        {
            return AplicacionServicioReportes.GetReportFromSheets(sheets, reportName, UsuarioActual, folioCorte);
        }
    }
}
