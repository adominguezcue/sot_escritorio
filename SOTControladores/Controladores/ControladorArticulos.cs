﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Transversal.Dependencias;
using Negocio.Almacen.Articulos;
using Negocio.Almacen.Lineas;
using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;

namespace SOTControladores.Controladores
{

	public class ControladorArticulos : ControladorBase
	{

        private IServicioArticulos AplicacionServicioArticulos
        {
			get { return _aplicacionServicioArticulos.Value; }
		}

		private Lazy<IServicioArticulos> _aplicacionServicioArticulos = new Lazy<IServicioArticulos>(() =>
		{
            return FabricaDependencias.Instancia.Resolver<IServicioArticulos>();
		});

        private IServicioLineas AplicacionServicioLineas
        {
            get { return _aplicacionServicioLineas.Value; }
        }

        private Lazy<IServicioLineas> _aplicacionServicioLineas = new Lazy<IServicioLineas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioLineas>();
        });

        public List<Modelo.Almacen.Entidades.Linea> ObtenerLineasConArticulos(bool soloComandables, bool incluirSalidaNegada, string filtro)
		{
            return AplicacionServicioLineas.ObtenerLineasConArticulos(/*UsuarioActual, */soloComandables, incluirSalidaNegada, filtro);
		}

        //public List<Modelo.Almacen.Entidades.Articulo> ObtenerArticulosPorTipo(string idTipo)
        //{
        //    return AplicacionServicioArticulos.ObtenerArticulosPorTipo(idTipo, UsuarioActual);
        //}

        public List<Modelo.Almacen.Entidades.Articulo> ObtenerArticulosPorDepartamento(int idDepartamento)
        {
            return AplicacionServicioArticulos.ObtenerArticulosPorDepartamento(idDepartamento, UsuarioActual);
        }

        //public List<Modelo.Almacen.Entidades.Linea> ObtenerTipos()
        //{
        //    return AplicacionServicioLineas.ObtenerLineas();
        //}

        //public List<Modelo.Almacen.Entidades.Linea> ObtenerLineas(int cod_Dpto)
        //{
        //    return AplicacionServicioLineas.ObtenerLineas(cod_Dpto);
        //}

        //public List<ZctImagenesArticulos> TraeImagenesArticulo(string cod_art) 
        //{
        //    return AplicacionServicioArticulos.TraeImagenesArticulo(cod_art);
        //}
        public Articulo TraerOGenerarArticuloPorCodigoOUpc(string Cod_art)
        {
            return AplicacionServicioArticulos.TraerOGenerarArticuloPorCodigoOUpc(Cod_art);
        }
        public void AgregaArticulo(Articulo articulo, List<DtoArticuloPresentacion> presentaciones) 
        {
            AplicacionServicioArticulos.AgregaArticulo(articulo, presentaciones, UsuarioActual);
        }
        public void ModificarArticulo(Articulo articulo, List<DtoArticuloPresentacion> presentaciones)
        {
            AplicacionServicioArticulos.ModificarArticulo(articulo, presentaciones, UsuarioActual);
        }
        public void EliminarFoto(string idArticulo, int Cod_imagen) 
        {
            AplicacionServicioArticulos.EliminarFoto(idArticulo, Cod_imagen);
        }
        public int MaxImg(string idArticulo)
        {
            return AplicacionServicioArticulos.MaxImg(idArticulo);
        }
        //public void ModificaFoto(string Cod_art, int cod_imagen, string RutaImagen) 
        //{
        //    AplicacionServicioArticulos.ModificaFoto(Cod_art, cod_imagen, RutaImagen);
        //}

        public void EliminaArticulo(string Cod_art)
        {
            AplicacionServicioArticulos.EliminaArticulo(Cod_art, UsuarioActual);
        }

        public List<DtoExistencias> SP_ConsultarExistencias(string codigoArticulo) 
        {
            return AplicacionServicioArticulos.SP_ConsultarExistencias(codigoArticulo);
        }

        public List<Articulo> ObtenerActivosFijos() 
        {
            return AplicacionServicioArticulos.ObtenerActivosFijos(UsuarioActual);
        }

        public DtoResultadoBusquedaArticulo SP_ZctCatArt_Busqueda(string codigo, bool soloInventariable) 
        {
            return AplicacionServicioArticulos.SP_ZctCatArt_Busqueda(codigo, soloInventariable);
        }

        public Articulo ObtenerArticuloNoNull(string codigo) 
        {
            return AplicacionServicioArticulos.ObtenerArticuloNoNull(codigo);
        }

        public List<DtoResultadoBusquedaArticuloSimple> ObtenerResumenesArticulosPorFiltro(string filtro, bool incluirArticulosPresentacion = true, bool soloInventariables = false, bool? EsInsumo=false, bool? _EsgestionArticulos=false)
        {
            return AplicacionServicioArticulos.ObtenerResumenesArticulosPorFiltro(filtro, UsuarioActual, incluirArticulosPresentacion, soloInventariables, EsInsumo, _EsgestionArticulos);
        }

        public decimal ObtenerCosto(string codigoArticulo)
        {
            return AplicacionServicioArticulos.ObtenerCosto(codigoArticulo, UsuarioActual);
        }

        public void ValidarExistenArticulosInventariables(int? codigoDepartamento, int? codigoLinea)
        {
            AplicacionServicioArticulos.ValidarExistenArticulosInventariables(codigoDepartamento, codigoLinea, UsuarioActual);
        }
    }

}