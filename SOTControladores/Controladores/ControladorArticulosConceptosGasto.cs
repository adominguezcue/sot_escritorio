﻿using Modelo.Entidades;
using Negocio.ArticulosConceptosGasto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorArticulosConceptosGasto : ControladorBase
    {
        private IServicioArticulosConceptosGasto AplicacionServicioArticulosConceptosGasto
        {
            get { return _aplicacionServicioArticulosConceptosGasto.Value; }
        }

        private Lazy<IServicioArticulosConceptosGasto> _aplicacionServicioArticulosConceptosGasto = new Lazy<IServicioArticulosConceptosGasto>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioArticulosConceptosGasto>();
        });



        public List<ArticuloConceptoGasto> ObtenerVinculaciones(string codigoArticulo)
        {
            return AplicacionServicioArticulosConceptosGasto.ObtenerVinculaciones(codigoArticulo, UsuarioActual);
        }

        public void EliminarVinculacion(int idArticuloConcepto)
        {
            AplicacionServicioArticulosConceptosGasto.EliminarVinculacion(idArticuloConcepto, UsuarioActual);
        }

        public void ModificarVinculacion(ArticuloConceptoGasto articuloConcepto)
        {
            AplicacionServicioArticulosConceptosGasto.ModificarVinculacion(articuloConcepto, UsuarioActual);
        }

        public void CrearVinculacion(ArticuloConceptoGasto articuloConcepto)
        {
            AplicacionServicioArticulosConceptosGasto.CrearVinculacion(articuloConcepto, UsuarioActual);
        }
    }
}
