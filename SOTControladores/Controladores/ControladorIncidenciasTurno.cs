﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Incidencias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorIncidenciasTurno : ControladorBase
    {
        private IServicioIncidenciasTurno AplicacionServicioIncidencias
        {
            get { return _aplicacionServicioIncidencias.Value; }
        }

        private Lazy<IServicioIncidenciasTurno> _aplicacionServicioIncidencias = new Lazy<IServicioIncidenciasTurno>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioIncidenciasTurno>();
        });

        public void RegistrarIncidencia(IncidenciaTurno incidencia, int idCorteTurno)
        {
            AplicacionServicioIncidencias.RegistrarIncidencia(incidencia, idCorteTurno, UsuarioActual);
        }

        public List<DtoIncidenciaTurno> ObtenerIncidenciasFiltradas(DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idCorte = null, string area=null)
        {
            return AplicacionServicioIncidencias.ObtenerIncidenciasFiltradas(UsuarioActual, fechaInicial, fechaFinal, idCorte, area);
        }

        public void CancelarIncidencia(int idIncidencia) 
        {
            AplicacionServicioIncidencias.CancelarIncidencia(idIncidencia, ObtenerCredencial());
        }
        public void EliminarIncidencia(int idIncidencia)
        {
            AplicacionServicioIncidencias.EliminarIncidencia(idIncidencia, ObtenerCredencial());
        }

        public IncidenciaTurno ObtenerIncidencia(int idIncidencia)
        {
            return AplicacionServicioIncidencias.ObtenerIncidencia(idIncidencia, UsuarioActual);
        }

        public void ModificarIncidencia(IncidenciaTurno incidencia)
        {
            AplicacionServicioIncidencias.ModificarIncidencia(incidencia, ObtenerCredencial());
        }

        public void ValidarExistenIncidenciasArea(int idCorteTurno)
        {
            AplicacionServicioIncidencias.ValidarExistenIncidenciasArea(idCorteTurno, UsuarioActual);
        }
    }
}
