﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Mantenimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorMantenimientos : ControladorBase
    {
        private IServicioMantenimientos AplicacionServicioMantenimientos
        {
            get { return _aplicacionServicioMantenimientos.Value; }
        }

        private Lazy<IServicioMantenimientos> _aplicacionServicioMantenimientos = new Lazy<IServicioMantenimientos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioMantenimientos>();
        });

        public Mantenimiento ObtenerMantenimientoActual(int idHabitacion)
        {
            return AplicacionServicioMantenimientos.ObtenerMantenimientoActual(idHabitacion, UsuarioActual);
        }

        public List<DtoResumenMantenimiento> ObtenerMantenimientosHabitacion(int idHabitacion, int? idconcepto = 0, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            return AplicacionServicioMantenimientos.ObtenerMantenimientosHabitacion(idHabitacion, UsuarioActual, idconcepto, fechaInicio, fechaFin);
        }
    }
}
