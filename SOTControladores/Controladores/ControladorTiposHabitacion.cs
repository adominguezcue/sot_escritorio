﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.TiposHabitacion;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorTiposHabitacion : ControladorBase
    {
        private IServicioTiposHabitaciones AplicacionServicioTiposHabitaciones
        {
            get { return _aplicacionServicioTiposHabitaciones.Value; }
        }

        private Lazy<IServicioTiposHabitaciones> _aplicacionServicioTiposHabitaciones = new Lazy<IServicioTiposHabitaciones>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioTiposHabitaciones>();
        });

        public List<ConfiguracionTarifa.Tarifas> ObtenerTarifasSoportadas(int idTipoHabitacion)
        {
            return AplicacionServicioTiposHabitaciones.ObtenerTarifasSoportadas(idTipoHabitacion);
        }

        public List<TipoHabitacion> ObtenerTiposActivos()
        {
            return AplicacionServicioTiposHabitaciones.ObtenerTiposActivos();
        }

        public List<TipoHabitacion> ObtenerTiposActivosConConfiguracion(ConfiguracionTarifa.Tarifas tarifa)
        {
            return AplicacionServicioTiposHabitaciones.ObtenerTiposActivosConConfiguracion(tarifa);
        }

        public List<DtoResumenTipoHabitacion> ObtenerTiposActivosCargadosDataTable()
        {
            return AplicacionServicioTiposHabitaciones.ObtenerTiposActivosCargadosDataTable();
        }

        public TipoHabitacion ObtenerTipoActivoCargado(int idTipoHabitacion)
        {
            return AplicacionServicioTiposHabitaciones.ObtenerTipoActivoCargado(idTipoHabitacion, UsuarioActual);
        }

        public void CrearTipoHabitacion(TipoHabitacion tipoHabitacion) 
        {
            AplicacionServicioTiposHabitaciones.CrearTipoHabitacion(tipoHabitacion, UsuarioActual);
        }

        public void ModificarTipoHabitacion(TipoHabitacion tipoHabitacion)
        {
            AplicacionServicioTiposHabitaciones.ModificarTipoHabitacion(tipoHabitacion, UsuarioActual);
        }

        public void EliminarTipoHabitacion(int idTipoHabitacion)
        {
            AplicacionServicioTiposHabitaciones.EliminarTipoHabitacion(idTipoHabitacion, UsuarioActual);
        }

        public List<DtoCantidadesHabitacionesTipo> ObtenerEstadosPorTipoFecha(DateTime fecha) 
        {
            return AplicacionServicioTiposHabitaciones.ObtenerEstadosPorTipoFecha(fecha, UsuarioActual);
        }
    }
}
