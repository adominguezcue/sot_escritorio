﻿using Modelo.Seguridad.Dtos;
using Negocio.Seguridad.Sesiones;
using Negocio.Seguridad.Permisos;								 
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorSesiones : ControladorBase
    {
        private IServicioSesiones AplicacionServicioSesiones
        {
            get { return _aplicacionServicioSesiones.Value; }
        }

        private Lazy<IServicioSesiones> _aplicacionServicioSesiones = new Lazy<IServicioSesiones>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioSesiones>();
        });

        IServicioPermisos ServicioPermisos
        {
            get { return _servicioPermisos.Value; }
        }

        Lazy<IServicioPermisos> _servicioPermisos = new Lazy<IServicioPermisos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPermisos>(); });
		
        public void IniciarSesion(DtoCredencial credencial) 
        {
            UsuarioActual = AplicacionServicioSesiones.IniciarSesion(credencial);
        }

        //public bool IniciarSesion(int Cod_Aut, string servidor, string baseDatos, string usuario, string contrasena, ref string usuarioActual, ref bool esVendedor)
        //{

        //    ClassGen cClas = new ClassGen();

        //    bool Valida = false;

        //    //Dim sVariables As String

        //    //Dim sParametros As String
        //    DataTable DtUsu = default(DataTable);
        //    ZctSOT.Datos.DAO.Conexion.SetActualServer(servidor);
        //    ZctSOT.Datos.DAO.Conexion.SetActualBD(baseDatos);
        //    //verifica que exista el usuario
        //    //sVariables = ";INT|;VARCHAR|;VARCHAR|;INT"

        //    //sParametros = "|" &  & "|" & & "|" & 

        //    Dictionary<string, object> parametros = new Dictionary<string, object>();

        //    parametros.Add("@Cod_Usu", 0);
        //    parametros.Add("@Nom_Usu", usuario);
        //    parametros.Add("@Pass_Usu", contrasena);
        //    parametros.Add("@Cod_Aut", Cod_Aut);

        //    DtUsu = GetData(1, "SP_ZctSegUsu", ref parametros);
        //    if (DtUsu != null && DtUsu.Rows.Count > 0)
        //    {
        //        //si existe graba el movimiento
        //        //RibbonMain._NomUsuAct = usuario

        //        if (Cod_Aut == 0)
        //        {
        //            cClas.GrabaUsuario(Convert.ToInt32(DtUsu.Rows[0]["Cod_Usu"].ToString()), "ZctLogin", "E", "");
        //            usuarioActual = DtUsu.Rows[0]["Cod_Usu"].ToString();
        //            esVendedor = Convert.ToBoolean(DtUsu.Rows[0]["Vendedor"].ToString());
        //        }
        //        else
        //        {
        //            usuarioActual = usuario;
        //            cClas.GrabaUsuario(Convert.ToInt32(DtUsu.Rows[0]["Cod_Usu"].ToString()), "ZctLogin", "A", Cod_Aut.ToString());
        //        }
        //        //ZctSOTMain._NomUsuAct = RibbonMain._NomUsuAct
        //        //ZctSOTMain._UsuAct = RibbonMain._UsuAct
        //        //ZctSOTMain._EsVendedor = RibbonMain._EsVendedor
        //        //ZctSOTMain._UsuAut = RibbonMain._UsuAut

        //        Valida = true;

        //    }
        //    else
        //    {
        //        //MsgBox("El usuario es incorrecto, verifique por favor")
        //        Valida = false;
        //        //iCont += 1
        //        //usuario = ""
        //        //contrasena = ""
        //        //UsernameTextBox.Focus()
        //        //caso contrario envia un false
        //    }

        //    //If Valida = True Or iCont >= 3 Then
        //    //    Me.Dispose()
        //    //End If

        //    return Valida;
        //}

        public void CerrarSesion()
        {
            if (UsuarioActual != null)
            {
                AplicacionServicioSesiones.CerrarSesion(UsuarioActual.TokenSesion);

                UsuarioActual = null;
            }
        }

        public DtoCredencial HuellaInicioSesion()
        {
            var huellaDigital = ObtenerCredencial();
            var usuario = ServicioPermisos.ObtenerUsuarioPorCredencial(huellaDigital);
            return huellaDigital;
        }
		
        //private DataTable GetData(int TpConsulta, string sSpNAME, ref Dictionary<string, object> parametros)
        //{
        //    ZctDataBase PkBusCon = new ZctDataBase();
        //    //Objeto de la base de datos
        //    try
        //    {
        //        //Inicia el procedimieto almacenado
        //        PkBusCon.IniciaProcedimiento(sSpNAME);
        //        //Agrega el parametro que define que se va a realizar
        //        PkBusCon.AddParameterSP("@TpConsulta", TpConsulta, SqlDbType.Int);
        //        //Obtiene el total de los procedimientos
        //        //Recorre las variables del procedimiento almacenado
        //        foreach (string iSp in parametros.Keys)
        //        {
        //            var tipo = parametros[iSp].GetType();

        //            //Mid(sVector(iSp), InStr(sVector(iSp), ";") + 1)
        //            if (tipo == typeof(int))
        //                PkBusCon.AddParameterSP(iSp, parametros[iSp], SqlDbType.Int);
        //            else if (tipo == typeof(string))
        //                PkBusCon.AddParameterSP(iSp, parametros[iSp], SqlDbType.VarChar);
        //            else if (tipo == typeof(DateTime))
        //                PkBusCon.AddParameterSP(iSp, parametros[iSp], SqlDbType.SmallDateTime);
        //            else if (tipo == typeof(decimal))
        //                PkBusCon.AddParameterSP(iSp, parametros[iSp], SqlDbType.Decimal);
        //            //Case GetType(Integer)
        //            //    PkBusCon.AddParameterSP(Strings.Left(sVector(iSp), InStr(sVector(iSp), ";") - 1), CType(sValores(iSp), String), SqlDbType.Text)

        //        }

        //        switch (TpConsulta)
        //        {
        //            //SELECT
        //            case 1:
        //                PkBusCon.InciaDataAdapter();
        //                return PkBusCon.GetReaderSP().Tables["DATOS"];
        //            case 2:
        //                //Ejecuta el escalar
        //                PkBusCon.GetScalarSP();

        //                return null;
        //            case 3:
        //                //Delete
        //                PkBusCon.GetScalarSP();
        //                return null;
        //            default:
        //                return null;
        //        }

        //    }
        //    //catch (Exception ex)
        //    //{
        //    //    MsgBox(ex.Message.ToString);
        //    //    return null;
        //    //}
        //    finally
        //    {
        //        PkBusCon.CloseConTran();

        //    }
        //}

    }
}