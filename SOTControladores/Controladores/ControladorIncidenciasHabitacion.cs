﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Incidencias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorIncidenciasHabitacion : ControladorBase
    {
        private IServicioIncidenciasHabitacion AplicacionServicioIncidencias
        {
            get { return _aplicacionServicioIncidencias.Value; }
        }

        private Lazy<IServicioIncidenciasHabitacion> _aplicacionServicioIncidencias = new Lazy<IServicioIncidenciasHabitacion>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioIncidenciasHabitacion>();
        });

        public void RegistrarIncidencia(Modelo.Entidades.IncidenciaHabitacion incidencia)
        {
            AplicacionServicioIncidencias.RegistrarIncidencia(incidencia, UsuarioActual);
        }

        public List<DtoIncidenciaHabitacion> ObtenerIncidenciasFiltradas(DateTime? fechaInicial = null, DateTime? fechaFinal = null, int? idHabitacion = null, int? idEmpleado = null)
        {
            return AplicacionServicioIncidencias.ObtenerIncidenciasFiltradas(UsuarioActual, fechaInicial, fechaFinal, idHabitacion, idEmpleado);
        }

        public void CancelarIncidencia(int idIncidencia) 
        {
            AplicacionServicioIncidencias.CancelarIncidencia(idIncidencia, ObtenerCredencial());
        }
        public void EliminarIncidencia(int idIncidencia)
        {
            AplicacionServicioIncidencias.EliminarIncidencia(idIncidencia, ObtenerCredencial());
        }

        public IncidenciaHabitacion ObtenerIncidencia(int idIncidencia)
        {
            return AplicacionServicioIncidencias.ObtenerIncidencia(idIncidencia, UsuarioActual);
        }

        public void ModificarIncidencia(IncidenciaHabitacion incidencia)
        {
            AplicacionServicioIncidencias.ModificarIncidencia(incidencia, ObtenerCredencial());
        }
    }
}
