﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.PeriodosDatos;
using Transversal.Dependencias;
using Modelo.Entidades;


namespace SOTControladores.Controladores
{
    public class ControladorCatPeriodoDatos : ControladorBase
    {
        IServicioPeriodosDatos AplicacionServicioDatosPeriodo
        { 
            get { return _aplicaionserviciodatosperiodo.Value; }
        }

        Lazy<IServicioPeriodosDatos> _aplicaionserviciodatosperiodo =
            new Lazy<IServicioPeriodosDatos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioPeriodosDatos>();});

        public List<CatPeriodoDatos> ObtieneDatosPeriodos()
        {
            return AplicacionServicioDatosPeriodo.ObtienePeriodosFiltrados();
        }

        public void AgregaPerioricidad(CatPeriodoDatos periodosdatos)
        {
            AplicacionServicioDatosPeriodo.AgregaPerioricidad(periodosdatos);
        }

        public void ModificaPerioricdad(CatPeriodoDatos periodosdatos)
        {
            AplicacionServicioDatosPeriodo.ModificaPerioricdad(periodosdatos);
        }

        public void EliminaPeriricidad(CatPeriodoDatos periodosdatos)
        {
            AplicacionServicioDatosPeriodo.EliminarPerioricidad(periodosdatos);
        }

    }
}
