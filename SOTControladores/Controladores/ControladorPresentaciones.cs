﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Negocio.Almacen.Presentaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorPresentaciones : ControladorBase
    {
        private IServicioPresentaciones AplicacionServicioPresentaciones
        {
            get { return _aplicacionServicioPresentaciones.Value; }
        }

        private Lazy<IServicioPresentaciones> _aplicacionServicioPresentaciones = new Lazy<IServicioPresentaciones>(() => FabricaDependencias.Instancia.Resolver<IServicioPresentaciones>());

        public List<Presentacion> ObtenerPresentaciones()
        {
            return AplicacionServicioPresentaciones.ObtenerPresentaciones(UsuarioActual);
        }
        public List<Presentacion> ObtenerPresentacionesFILTRO()
        {
            return AplicacionServicioPresentaciones.ObtenerPresentacionesFILTRO();
        }

        public Presentacion ObtenerPresentacion(int idPresentacion)
        {
            return AplicacionServicioPresentaciones.ObtenerPresentacion(idPresentacion, UsuarioActual);
        }

        public void CrearPresentacion(Presentacion presentacion)
        {
            AplicacionServicioPresentaciones.CrearPresentacion(presentacion, UsuarioActual);
        }

        public void ModificarPresentacion(Presentacion presentacion, List<DtoConversion> conversion)
        {
            AplicacionServicioPresentaciones.ModificarPresentacion(presentacion, conversion, UsuarioActual);
        }

        public void EliminarPresentacion(int idPresentacion)
        {
            AplicacionServicioPresentaciones.EliminarPresentacion(idPresentacion, UsuarioActual);
        }
    }
}
