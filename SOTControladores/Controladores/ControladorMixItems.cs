﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Recetas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorMixItems : ControladorBase
    {
        private IServicioMixItems AplicacionServicioMixItems
        {
            get { return _aplicacionServicioMixItems.Value; }
        }

        private Lazy<IServicioMixItems> _aplicacionServicioMixItems = new Lazy<IServicioMixItems>(() => FabricaDependencias.Instancia.Resolver<IServicioMixItems>());

        public ZctEncOP GetEncOP(int codOP)
        {
            return AplicacionServicioMixItems.GetEncOP(codOP);
        }

        public List<ZctDetOP> GetDetOP(int codOP)
        {
            return AplicacionServicioMixItems.GetDetOP(codOP);
        }

        public List<ZctMixItems> GetMixItems(string codigo)
        {
            return AplicacionServicioMixItems.GetMixItems(codigo, UsuarioActual);
        }

        public List<ZctMixItems> GetMixItemsCost(string codArt, int codAlm) 
        {
            return AplicacionServicioMixItems.GetMixItemsCost(codArt, codAlm, UsuarioActual);
        }

        public void GuardarIngredientes(List<ZctMixItems> listMix)
        {
            AplicacionServicioMixItems.GuardarIngredientes(listMix, UsuarioActual);
        }

        public void EliminarReceta(string codigoArticulo)
        {
            AplicacionServicioMixItems.EliminarReceta(codigoArticulo, UsuarioActual);
        }

        public void SaveOP(ZctEncOP encOP, List<ZctDetOP> detOP)
        {
            AplicacionServicioMixItems.ProducirArticulo(encOP, detOP);
        }
    }
}
