﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Negocio.Rentas;
using Transversal.Dependencias;
using Modelo.Entidades;
using Modelo.Dtos;
using Dominio.Nucleo.Entidades;

namespace SOTControladores.Controladores
{
	public class ControladorRentas : ControladorBase
	{

		private IServicioRentas AplicacionServicioRentas {
			get { return _aplicacionServicioRentas.Value; }
		}

		private Lazy<IServicioRentas> _aplicacionServicioRentas = new Lazy<IServicioRentas>(() =>
		{
			return FabricaDependencias.Instancia.Resolver<IServicioRentas>();
		});
		public Modelo.Entidades.Renta ObtenerRentaActualPorHabitacion(int idHabitacion)
		{
			return AplicacionServicioRentas.ObtenerRentaActualPorHabitacion(idHabitacion);
		}

        public Renta ObtenerUltimaRentaPorHabitacionConDatosFiscales(int idHabitacion) 
        {
            return AplicacionServicioRentas.ObtenerUltimaRentaPorHabitacionConDatosFiscales(idHabitacion);
        }

        public void CrearYPagarRenta(Modelo.Entidades.Renta rentaNueva, Propina propina, bool requierePermisosEspeciales, ConsumoInternoHabitacion consumo)
        {
            if (requierePermisosEspeciales)
            {
                AplicacionServicioRentas.CrearRenta(rentaNueva, true, propina, ObtenerCredencial(), consumo);
            }
            else
            {
                AplicacionServicioRentas.CrearRenta(rentaNueva, true, propina, UsuarioActual, consumo);
            }
        }

        //public void PagarRenta(int idRentaNueva, List<DtoInformacionPago> pagos, Propina propina, ConsumoInternoHabitacion consumo)
        //{
        //    AplicacionServicioRentas.PagarRenta(idRentaNueva, pagos, propina, UsuarioActual, consumo);
        //}

        public void PagarRentaV2(int idRentaNueva, List<DtoInformacionPago> pagos, ConsumoInternoHabitacion consumo, Propina propina, int? idEmpleadoPropina)
        {
            AplicacionServicioRentas.PagarRentaV2(idRentaNueva, pagos, consumo, propina, idEmpleadoPropina, UsuarioActual);
        }

        public void CrearRenta(Modelo.Entidades.Renta rentaNueva, bool requierePermisosEspeciales)
        {
            if (requierePermisosEspeciales)
            {
                AplicacionServicioRentas.CrearRenta(rentaNueva, false, null, ObtenerCredencial(), null);
            }
            else
            {
                AplicacionServicioRentas.CrearRenta(rentaNueva, false, null, UsuarioActual, null);
            }
        }



        //public void AgregarHorasExtra(int idRenta, int cantidadExtensiones, List<Modelo.Dtos.DtoInformacionPago> informacionPagos)
        //{
        //    var huellaDigital = ObtenerHuella();

        //    AplicacionServicioRentas.AgregarHorasExtra(idRenta, cantidadExtensiones, informacionPagos, huellaDigital);
        //}

        //public void EntregarComanda(int idComanda, int idEmpleadoCobro)
        //{
        //    AplicacionServicioRentas.EntregarComanda(idComanda, idEmpleadoCobro, UsuarioActual);
        //}

        //public List<Comanda> ObtenerDetallesComandasArticulosEnPreparacionDepartamento(string departamento)
        //{
        //    return AplicacionServicioRentas.ObtenerDetallesComandasArticulosEnPreparacionDepartamento(departamento);
        //}

        //public List<Comanda> ObtenerDetallesComandasArticulosPreparadosDepartamento(string departamento)
        //{
        //    return AplicacionServicioRentas.ObtenerDetallesComandasArticulosPreparadosDepartamento(departamento);
        //}

        //public void AgregarPersonasExtra(int idRenta, int cantidadPersonasExtra, List<Modelo.Dtos.DtoInformacionPago> pagos)
        //{
        //    var huellaDigital = ObtenerHuella();

        //    AplicacionServicioRentas.AgregarPersonasExtra(idRenta, cantidadPersonasExtra, pagos, huellaDigital);
        //}

        //public List<Modelo.Dtos.DtoArticuloPrepararConsulta> ObtenerArticulosPrepararComandas()
        //{
        //    return AplicacionServicioRentas.ObtenerArticulosPrepararComandas(UsuarioActual);
        //}

		public List<AutomovilRenta> ObtenerAutomovilesUltimaRenta(int idHabitacion)
		{
			return AplicacionServicioRentas.ObtenerAutomovilesUltimaRenta(idHabitacion);
		}

        public void ActualizarDatosHabitacionV2(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<Modelo.Dtos.DtoAutomovil> automoviles, bool requierePermisosEspeciales)
        {
            if (requierePermisosEspeciales)//cantidadPersonasExtra > 0)
            {
                var huellaDigital = ObtenerCredencial();

                AplicacionServicioRentas.ActualizarDatosHabitacionV2(idRenta, cantidadExtensiones, cantidadRenovaciones, cantidadPersonasExtra, automoviles, huellaDigital);
            }
            else
                AplicacionServicioRentas.ActualizarDatosHabitacionV2(idRenta, cantidadExtensiones, cantidadRenovaciones, cantidadPersonasExtra, automoviles, UsuarioActual);
        }

        public void CancelarVentaPendiente(int idVentaRenta) 
        {
            AplicacionServicioRentas.CancelarVentaPendiente(idVentaRenta, ObtenerCredencial());
        }

        //public void ActualizarDatosHabitacion(int idRenta, int cantidadExtensiones, int cantidadRenovaciones, int cantidadPersonasExtra, List<Modelo.Dtos.DtoAutomovil> automoviles, List<DtoInformacionPago> pagos, ConsumoInternoHabitacion consumo, bool requierePermisosEspeciales)
        //{
        //    if (requierePermisosEspeciales)//cantidadPersonasExtra > 0)
        //    {
        //        var huellaDigital = ObtenerCredencial();

        //        AplicacionServicioRentas.ActualizarDatosHabitacion(idRenta, cantidadExtensiones, cantidadRenovaciones, cantidadPersonasExtra, automoviles, pagos, huellaDigital, consumo);
        //    }
        //    else
        //        AplicacionServicioRentas.ActualizarDatosHabitacion(idRenta, cantidadExtensiones, cantidadRenovaciones, cantidadPersonasExtra, automoviles, pagos, UsuarioActual, consumo);
        //}

        //public List<DetallePago> ObtenerDetallesPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null)
        //{
        //    return AplicacionServicioRentas.ObtenerDetallesPagosPorFecha(UsuarioActual, fechaInicio, fechaFin);
        //}

        //public Dictionary<TiposPago, decimal> ObtenerPagosHabitacionPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null, params TiposPago[] tiposPagos)
        //{
        //    return AplicacionServicioRentas.ObtenerDiccionarioPagosHabitacionPorFecha(UsuarioActual, fechaInicio, fechaFin, tiposPagos);
        //}

        //public bool VerificarTieneTarjetaVPointsVinculada(int idHabitacion)
        //{
        //    return AplicacionServicioRentas.VerificarTieneTarjetaVPointsVinculada(idHabitacion, UsuarioActual);
        //}

        public void ActualizarPagoHabitacion(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null, int? idEmpleado = null)
        {
            AplicacionServicioRentas.ActualizarPagoHabitacion(idPago, formaPago, valorPropina, referencia, UsuarioActual, numeroTarjeta, tipoTarjeta, idEmpleado);
        }

        public void VincularDatosFiscales(int idRenta, int? idDatosFiscales)
        {
            AplicacionServicioRentas.VincularDatosFiscales(idRenta, idDatosFiscales, UsuarioActual);
        }

        public DtoDetallesRenta ObtenerResumenVentasUltimaRenta(int idHabitacion) 
        {
            return AplicacionServicioRentas.ObtenerResumenVentasUltimaRenta(idHabitacion);
        }

        public int ObtenerCantidadPersonasExtraUltimaRenovacion(int idRenta)
        {
            return AplicacionServicioRentas.ObtenerCantidadPersonasExtraUltimaRenovacion(idRenta);
        }

        public DtoAutomovil ObtenerDatosAutomovil(string matricula)
        {
            return AplicacionServicioRentas.ObtenerDatosAutomovil(matricula);
        }
    }

}