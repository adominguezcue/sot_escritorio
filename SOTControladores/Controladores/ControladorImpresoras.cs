﻿using Negocio.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorTickets : ControladorBase
    {
        private IServicioTickets AplicacionServicioTickets
        {
            get { return _aplicacionServicioTickets.Value; }
        }

        private Lazy<IServicioTickets> _aplicacionServicioTickets = new Lazy<IServicioTickets>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioTickets>();
        });

        public void ImprimirTicketVentaRenta(int idVentaRenta, string nombreImpresora, int numeroCopias)
        {
            AplicacionServicioTickets.ImprimirTicketVentaRenta(idVentaRenta, nombreImpresora, numeroCopias);
        }

        public void ImprimirTicketVenta(int idVenta, string nombreImpresora, int numeroCopias)
        {
            AplicacionServicioTickets.ImprimirTicketVenta(idVenta, nombreImpresora, numeroCopias);
        }

        public void ImprimirTicketComandaCancelada(int idComanda, string nombreImpresora, int numeroCopias)
        {
            AplicacionServicioTickets.ImprimirTicketComandaCancelada(idComanda, nombreImpresora, numeroCopias);
        }

        public void ImprimirTicketConsumoInternoCancelado(int idConsumoInterno, string nombreImpresora, int numeroCopias)
        {
            AplicacionServicioTickets.ImprimirTicketConsumoInternoCancelado(idConsumoInterno, nombreImpresora, numeroCopias);
        }

        public void ImprimirTicketOcupacionMesaCancelada(int idOCupacionMesa, string nombreImpresora, int numeroCopias)
        {
            AplicacionServicioTickets.ImprimirTicketOcupacionMesaCancelada(idOCupacionMesa, nombreImpresora, numeroCopias);
        }

        public void ImprimirTicketOrdenTaxi(int idOrdenTaxi, string nombreImpresora, int numeroCopias)
        {
            AplicacionServicioTickets.ImprimirTicketOrdenTaxi(idOrdenTaxi, nombreImpresora, numeroCopias);
        }

        public void ImprimirTicketVentaRentaCancelada(int idVentaRenta, string nombreImpresora, int numeroCopias)
        {
            AplicacionServicioTickets.ImprimirTicketVentaRentaCancelada(idVentaRenta, nombreImpresora, numeroCopias);
        }
    }
}
