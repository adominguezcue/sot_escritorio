﻿using Negocio.Almacen.RetirosCaja;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorRetirosCaja : ControladorBase
    {
        private IServicioRetirosCaja AplicacionServicioRetirosCaja
        {
            get { return _aplicacionServicioRetirosCaja.Value; }
        }

        private Lazy<IServicioRetirosCaja> _aplicacionServicioRetirosCaja = new Lazy<IServicioRetirosCaja>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioRetirosCaja>();
        });


        public int RetiroCaja(string Cod_folioCaja, decimal Monto)
        {
            return AplicacionServicioRetirosCaja.RetiroCaja(Cod_folioCaja, Monto, UsuarioActual);
        }
        public decimal TraeTotalRetirosCaja(string cod_folio) 
        {
            return AplicacionServicioRetirosCaja.TraeTotalRetirosCaja(cod_folio, UsuarioActual);
        }

    }
}
