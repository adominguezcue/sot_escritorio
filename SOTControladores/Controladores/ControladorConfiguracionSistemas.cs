﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo.Sistemas.Entidades;
using Negocio.Sistemas.ConfiguracionesSistemas;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConfiguracionSistemas: ControladorBase
    {
        private IServicioConfiguracionesSistemas AplicacionServicioConfiguracionesSistemas
        {
            get { return _aplicacionServicioConfiguracionesSistemas.Value; }
        }

        private Lazy<IServicioConfiguracionesSistemas> _aplicacionServicioConfiguracionesSistemas = new Lazy<IServicioConfiguracionesSistemas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesSistemas>();
        });

        //public ConfiguracionSistema ObtenerConfiguracion(Guid idSistema)
        //{
        //    return AplicacionServicioConfiguracionesSistemas.ObtenerConfiguracion(idSistema);
        //}

        public List<ParametroSistema> ObtenerParametros(Guid idSistema)
        {
            return AplicacionServicioConfiguracionesSistemas.ObtenerParametros(idSistema);
        }

        public void GuardarParametrosSistema(List<ParametroSistema> parametros)
        {
            AplicacionServicioConfiguracionesSistemas.GuardarParametrosSistema(parametros, UsuarioActual);
        }

        public void ValidarConfiguracionCorrecta()
        {
            throw new NotImplementedException();
        }
    }
}
