﻿using System.Collections.Generic;
using Modelo.Entidades.Parciales;
using Negocio.ReporteInventario;
using System;
using Transversal.Dependencias;
namespace SOTControladores.Controladores
{
    public class ControladorReporteInventario : ControladorBase
    {
        private IServicioReporteInventario AplicacionServicioReporteInventario
        {
            get { return _aplicacionservicioreporteinventario.Value;  }
        }

        private Lazy<IServicioReporteInventario> _aplicacionservicioreporteinventario = new Lazy<IServicioReporteInventario>(() =>
        { return FabricaDependencias.Instancia.Resolver<IServicioReporteInventario>(); });

        public List<CatalogoAlmacen> ObtieneCatalogoAlmacen(string depto, string linea)
        {
            return AplicacionServicioReporteInventario.ObtieneCatalogoAlmacen(UsuarioActual,depto,linea);
        }

        public List<CatalogoLinea> ObtienecatalogoLinea(string deptos)
        {
            return AplicacionServicioReporteInventario.ObtienecatalogoLinea(UsuarioActual, deptos);
        }

        public List<CatalogoDepartamento> ObtieneCatalogoDepto(string deptos)
        {
            return AplicacionServicioReporteInventario.ObtieneCatalogoDepto(UsuarioActual, deptos);
        }

        public List<ArticulosInventarioActual> ObtieneArticulosInvActual(string almacen,string articulos, string linea,string depto)
        {
            return AplicacionServicioReporteInventario.ObtieneArticulosInvActual(UsuarioActual, almacen, articulos, linea, depto);
        }
    }
}
