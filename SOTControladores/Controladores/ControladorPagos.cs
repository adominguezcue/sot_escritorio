﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Negocio.Pagos;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
	public class ControladorPagos : ControladorBase
	{

        private IServicioPagos AplicacionServicioPagos
        {
			get { return _aplicacionServicioPagos.Value; }
		}

		private Lazy<IServicioPagos> _aplicacionServicioPagos = new Lazy<IServicioPagos>(() =>
		{
			return FabricaDependencias.Instancia.Resolver<IServicioPagos>();
		});
        public List<Modelo.Dtos.DtoGrupoPago> ObtenerTiposValidosConMixtos(bool incluirCortesia, bool incluirConsumoInterno)//bool incluirReservacion)
		{
            return AplicacionServicioPagos.ObtenerTiposValidosConMixtos(UsuarioActual, incluirCortesia, incluirConsumoInterno);//incluirReservacion);
		}
	}
}