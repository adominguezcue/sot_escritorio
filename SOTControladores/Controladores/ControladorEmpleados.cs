﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Empleados;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorEmpleados : ControladorBase
    {
        private IServicioEmpleados AplicacionServicioEmpleados
        {
            get { return _aplicacionServicioEmpleados.Value; }
        }

        private Lazy<IServicioEmpleados> _aplicacionServicioEmpleados = new Lazy<IServicioEmpleados>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioEmpleados>();
        });

        public void CrearEmpleado(Empleado empleado) 
        {
            AplicacionServicioEmpleados.CrearEmpleado(empleado, UsuarioActual);
        }

        public void ModificarEmpleado(Empleado empleado)
        {
            AplicacionServicioEmpleados.ModificarEmpleado(empleado, UsuarioActual);
        }
        public void EliminarEmpleado(int idEmpleado) 
        {
            AplicacionServicioEmpleados.EliminarEmpleado(idEmpleado, ObtenerCredencial());
        }
        public List<Empleado> ObtenerEmpleadosFiltrados(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, string telefono = null, DateTime? fechaRegistro = null) 
        {
            return AplicacionServicioEmpleados.ObtenerEmpleadosFiltrados(UsuarioActual, nombre, apellidoPaterno, apellidoMaterno, telefono, fechaRegistro);
        }

        public List<Empleado> ObtenerEmpleadosActivosPorPuesto(/*int idCorteTurno,*/ params int[] idsPuestos)//string nombrePuesto)
        {
            //return AplicacionServicioEmpleados.ObtenerEmpleadosActivosPorPuesto(nombrePuesto, UsuarioActual);
            return AplicacionServicioEmpleados.ObtenerEmpleadosActivosPorPuesto(UsuarioActual, /*idCorteTurno,*/ idsPuestos);
        }

        public List<Empleado> ObtenerEmpleadosFILTRO(bool soloHabilitadas = true)
        {
            return AplicacionServicioEmpleados.ObtenerEmpleadosFILTRO(soloHabilitadas);
        }

        public List<Empleado> ObtenerRecamarerasFILTRO(bool soloHabilitadas = true)
        {
            return AplicacionServicioEmpleados.ObtenerRecamarerasFILTRO(soloHabilitadas);
        }

        public List<Empleado> ObtenerRecamarerasTurnoFILTRO(bool soloHabilitadas = true)
        {
            return AplicacionServicioEmpleados.ObtenerRecamarerasFILTRO(soloHabilitadas);
        }

        public List<Empleado> ObtenerSupervisoresFILTRO()
        {
            return AplicacionServicioEmpleados.ObtenerSupervisoresFILTRO();
        }

        public void HabilitarDeshabilitarEmpleados(Dictionary<int, bool> idsEmpleadosHabilitacion)
        {
            AplicacionServicioEmpleados.HabilitarDeshabilitarEmpleados(idsEmpleadosHabilitacion, UsuarioActual);
        }

        public List<Empleado> ObtenerMeserosFILTRO(/*bool soloTurnoActual = false*/)
        {
            return AplicacionServicioEmpleados.ObtenerMeserosFILTRO(true/*soloTurnoActual: soloTurnoActual*/);
        }

        public List<Empleado> ObtenerMeserosFILTRO(/*int idCorteTurno,*/ int? idEmpleadoIncluir)
        {
            return AplicacionServicioEmpleados.ObtenerMeserosFILTRO(/*idCorteTurno,*/ idEmpleadoIncluir);
        }

        public List<Empleado> ObtenerVendedoresFILTRO(/*int idCorteTurno,*/ int? idEmpleadoIncluir)
        {
            return AplicacionServicioEmpleados.ObtenerVendedoresFILTRO(/*idCorteTurno,*/ idEmpleadoIncluir);
        }

        public List<Empleado> ObtenerValetsFILTRO(/*bool soloTurnoActual = false*/)
        {
            return AplicacionServicioEmpleados.ObtenerValetsFILTRO(/*soloTurnoActual: soloTurnoActual*/);
        }

        public Empleado ObtenerEmpleadoPorNumeroNoNull(string numeroEmpleado)
        {
            return AplicacionServicioEmpleados.ObtenerEmpleadoPorNumeroNoNull(numeroEmpleado);
        }

        public List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreas()
        {
            return AplicacionServicioEmpleados.ObtenerResumenEmpleadosPuestosAreas(UsuarioActual);
        }

        public List<DtoEmpleadoAreaPuesto> ObtenerResumenEmpleadosPuestosAreasFILTRO()
        {
            return AplicacionServicioEmpleados.ObtenerResumenEmpleadosPuestosAreasFILTRO();
        }

        public List<CategoriaEmpleado> ObtenerCategoriasEmpleados()
        {
            return AplicacionServicioEmpleados.ObtenerCategoriasEmpleados();
        }
    }
}
