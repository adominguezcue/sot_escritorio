﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Negocio.Almacen.Inventarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorInventarios : ControladorBase
    {
        private IServicioInventarios AplicacionServicioInventarios
        {
            get { return _aplicacionServicioInventarios.Value; }
        }

        private Lazy<IServicioInventarios> _aplicacionServicioInventarios = new Lazy<IServicioInventarios>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioInventarios>();
        });

        public decimal ObtenerUltimoCostoMovimiento(string codigoArticulo) 
        {
            return AplicacionServicioInventarios.ObtenerUltimoCostoMovimiento(codigoArticulo);
        }

        public List<DtoItemKardex> SP_ZctKdxInv(string cod_Art, int cod_Alm, DateTime fchIni, DateTime fchFin) 
        {
            return AplicacionServicioInventarios.SP_ZctKdxInv(cod_Art, cod_Alm, fchIni, fchFin, UsuarioActual);
        }

        public List<VW_ZctInventarioFinal> DetallesInventario(string folioInventario)
        {
            return AplicacionServicioInventarios.DetallesInventario(folioInventario, UsuarioActual);
        }
    }
}
