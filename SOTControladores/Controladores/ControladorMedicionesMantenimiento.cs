﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio.MedicionMantenimiento;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorMedicionesMantenimiento  : ControladorBase
    {
        IServicioMedicionMantenimiento Aplicaionserviciomedicionmantenimiento
        {
            get { return _acplicaionmedicionmantenimiento.Value; }
        }

        Lazy<IServicioMedicionMantenimiento> _acplicaionmedicionmantenimiento =
            new Lazy<IServicioMedicionMantenimiento>(() => FabricaDependencias.Instancia.Resolver<IServicioMedicionMantenimiento>());

        public void AgregaRegistros(List<Modelo.Dtos.DtoMedicionesMantenimiento> medicionMantenimientos)
        {
            Aplicaionserviciomedicionmantenimiento.AgregaRegistros(medicionMantenimientos, UsuarioActual);
        }

        public List<Modelo.Entidades.MedicionMantenimiento> ObtieneMedicionesActivas()
        {
            return Aplicaionserviciomedicionmantenimiento.ObtieneMedicionesActivas();
        }

        public List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradas(int?idCorte)
        {
            return Aplicaionserviciomedicionmantenimiento.ObtieneLecturasFiltradas(idCorte);
        }

        public List<Modelo.Dtos.DtoMedicionMantenimientoGestion> ObtieneLecturasFiltradasPorFecha(DateTime? fechaInicio)
        {
            return Aplicaionserviciomedicionmantenimiento.ObtieneLecturasFiltradasPorFecha(fechaInicio);
        }

        public void ModificaRegistros(List<Modelo.Dtos.DtoMedicionesMantenimiento> medicionMantenimientos,  int? idCorte)
        {
            Aplicaionserviciomedicionmantenimiento.ModificaRegistros(medicionMantenimientos, UsuarioActual, idCorte);
        }
    }
}
