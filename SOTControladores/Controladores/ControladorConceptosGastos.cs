﻿using Negocio.ConceptosGastos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConceptosGastos : ControladorBase
    {
        private IServicioConceptosGastos ServicioConceptosGastos
        {
            get { return _servicioConceptosGastos.Value; }
        }

        private Lazy<IServicioConceptosGastos> _servicioConceptosGastos = new Lazy<IServicioConceptosGastos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioConceptosGastos>(); });


        public List<Modelo.Entidades.ConceptoGasto> ObtenerConceptosPagablesEnCaja()
        {
            return ServicioConceptosGastos.ObtenerConceptosPagablesEnCaja(UsuarioActual);
        }

        public List<Modelo.Entidades.ConceptoGasto> ObtenerConceptosActivos()
        {
            return ServicioConceptosGastos.ObtenerConceptosActivos(UsuarioActual);
        }

        public void CrearConcepto(Modelo.Entidades.ConceptoGasto conceptoGasto)
        {
            ServicioConceptosGastos.CrearConceptoGasto(conceptoGasto, UsuarioActual);
        }

        public void ModificarConcepto(Modelo.Entidades.ConceptoGasto conceptoGasto)
        {
            ServicioConceptosGastos.ModificarConcepto(conceptoGasto, UsuarioActual);
        }

        public void EliminarConcepto(int idConcepto)
        {
            ServicioConceptosGastos.EliminarConcepto(idConcepto, UsuarioActual);
        }
    }
}
