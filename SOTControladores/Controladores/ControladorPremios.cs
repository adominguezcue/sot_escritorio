﻿using Negocio.Premios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorPremios : ControladorBase
    {
        private IServicioPremios AplicacionServicioPremios
        {
            get { return _aplicacionServicioPremios.Value; }
        }

        private Lazy<IServicioPremios> _aplicacionServicioPremios = new Lazy<IServicioPremios>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioPremios>();
        });

        public Modelo.Entidades.Premio ObtenerPremioCargadoNoNull(string numeroPremio)
        {
            return AplicacionServicioPremios.ObtenerPremioCargadoNoNull(numeroPremio, UsuarioActual);
        }
    }
}
