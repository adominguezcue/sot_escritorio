﻿using Modelo.Entidades;
using Negocio.Ventas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorVentas : ControladorBase
    {
        private IServicioArticulosVendidos AplicacionServicioArticulosVendidos
        {
            get { return _aplicacionServicioArticulosVendidos.Value; }
        }

        private Lazy<IServicioArticulosVendidos> _aplicacionServicioArticulosVendidos = new Lazy<IServicioArticulosVendidos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioArticulosVendidos>();
        });

        private IServicioVentas AplicacionServicioVentas
        {
            get { return _aplicacionServicioVentas.Value; }
        }

        private Lazy<IServicioVentas> _aplicacionServicioVentas = new Lazy<IServicioVentas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioVentas>();
        });

        public List<VW_ArticuloVendido> ObtenerArticulosVendidos(int? ordenTurno = null, string categoria = null, string subCategoria = null, DateTime? fechaInicio = null, DateTime? fechaFin = null, bool? agrupar=null) 
        {
            return AplicacionServicioArticulosVendidos.ObtenerArticulosVendidos(UsuarioActual, ordenTurno, categoria, subCategoria, fechaInicio, fechaFin, agrupar);
        }

        public List<Venta> ObtenerVentasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null) 
        {
            return AplicacionServicioVentas.ObtenerVentasFiltradas(ordenTurno, fechaInicio, fechaFin);
        }

        public List<VW_VentaCorrecta> ObtenerVentasCorrectasFiltradas(int? ordenTurno = null, DateTime? fechaInicio = null, DateTime? fechaFin = null) 
        {
            return AplicacionServicioVentas.ObtenerVentasCorrectasFiltradas(UsuarioActual, ordenTurno, fechaInicio, fechaFin);
        }

        public Modelo.Dtos.DtoResumenVenta ObtenerResumenVenta(int idVenta)
        {
            return AplicacionServicioVentas.ObtenerResumenVenta(idVenta);
        }

        public Modelo.Dtos.DtoResumenVenta ObtenerResumenComandaCancelada(int idComanda)
        {
            return AplicacionServicioVentas.ObtenerResumenComandaCancelada(idComanda);
        }

        public Modelo.Dtos.DtoResumenVenta ObtenerResumenConsumoInternoCancelado(int idConsumoInterno)
        {
            return AplicacionServicioVentas.ObtenerResumenConsumoInternoCancelado(idConsumoInterno);
        }

        public Modelo.Dtos.DtoResumenVenta ObtenerResumenOcupacionMesaCancelada(int idOcupacionMesa)
        {
            return AplicacionServicioVentas.ObtenerResumenOcupacionMesaCancelada(idOcupacionMesa);
        }

        public Modelo.Dtos.DtoResumenVenta ObtenerResumenOrdenTaxi(int idOrdenTaxi)
        {
            return AplicacionServicioVentas.ObtenerResumenOrdenTaxi(idOrdenTaxi);
        }

        public Modelo.Dtos.DtoResumenVenta ObtenerResumenVentaRentaCancelada(int idVentaRenta)
        {
            return AplicacionServicioVentas.ObtenerResumenVentaRentaCancelada(idVentaRenta);
        }
    }
}
