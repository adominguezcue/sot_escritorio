﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Modelo;
using Transversal.Dependencias;
using Negocio.Taxis;
using Modelo.Entidades;

namespace SOTControladores.Controladores
{
	public class ControladorTaxis : ControladorBase
	{

		private IServicioTaxis AplicacionServicioTaxis {
			get { return _aplicacionServicioTaxis.Value; }
		}

		private Lazy<IServicioTaxis> _aplicacionServicioTaxis = new Lazy<IServicioTaxis>(() =>
		{
            return FabricaDependencias.Instancia.Resolver<IServicioTaxis>();
		});
		public void CobrarOrden(int IdOrdenTaxi, int idEmpleado)
		{
			AplicacionServicioTaxis.CobrarOrden(IdOrdenTaxi, idEmpleado, UsuarioActual);
		}

		public int ObtenerCantidadOrdenesPendientes()
		{
			return AplicacionServicioTaxis.ObtenerCantidadOrdenesPendientes(UsuarioActual);
		}

		public void CrearOrden(OrdenTaxi orden)
		{
			AplicacionServicioTaxis.CrearOrden(orden, UsuarioActual);
		}


		public void CancelarOrden(int idOrden, string motivo)
		{
			AplicacionServicioTaxis.CancelarOrden(idOrden, motivo, ObtenerCredencial());
		}

		public List<OrdenTaxi> ObtenerOrdenesPendientes()
		{
			return AplicacionServicioTaxis.ObtenerOrdenesPendientes(UsuarioActual);
		}

		public void ModificarPrecio(int idOrdenTaxi, decimal nuevoPrecio)
		{
			AplicacionServicioTaxis.ModificarPrecioTaxi(idOrdenTaxi, nuevoPrecio, UsuarioActual);
		}

	}
}