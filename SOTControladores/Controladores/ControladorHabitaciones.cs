﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Negocio.Habitaciones;
using Transversal.Dependencias;
using Modelo.Dtos;
using Modelo.Entidades;

namespace SOTControladores.Controladores
{
	public class ControladorHabitaciones : ControladorBase
	{

		private IServicioHabitaciones AplicacionServicioHabitaciones {
			get { return _aplicacionServicioHabitaciones.Value; }
		}

		private Lazy<IServicioHabitaciones> _aplicacionServicioHabitaciones = new Lazy<IServicioHabitaciones>(() =>
		{
			return FabricaDependencias.Instancia.Resolver<IServicioHabitaciones>();
		});
		public Modelo.Entidades.BloqueoHabitacion ObtenerBloqueoActual(int idHabitacion)
		{
			return AplicacionServicioHabitaciones.ObtenerBloqueoActual(idHabitacion, UsuarioActual);
		}

		public Modelo.Entidades.Habitacion ObtenerHabitacion(int idHabitacion)
		{
			return AplicacionServicioHabitaciones.ObtenerHabitacion(idHabitacion);
		}

		public List<Modelo.Entidades.Habitacion> ObtenerActivasConTipos()
		{
			return AplicacionServicioHabitaciones.ObtenerActivasConTipos(UsuarioActual);
		}

        public void ValidarAcceso()
        {
            AplicacionServicioHabitaciones.ValidarAcceso(UsuarioActual);
        }

        public Modelo.Dtos.DtoDatosHabitacion ObtenerDatosHabitacion(string numeroHabitacion)
		{
			return AplicacionServicioHabitaciones.ObtenerDatosHabitacion(numeroHabitacion, UsuarioActual);
		}

		public void PrepararHabitacion(int idHabitacion)
		{
			AplicacionServicioHabitaciones.PrepararHabitacion(idHabitacion, UsuarioActual);
		}

		public void BloquearHabitacion(int idHabitacion, string motivo)
		{
			AplicacionServicioHabitaciones.BloquearHabitacion(idHabitacion, motivo, UsuarioActual);
		}

        public void PonerEnMantenimiento(int idHabitacion, int idTareaMantenimiento)
		{
            AplicacionServicioHabitaciones.PonerEnMantenimiento(idHabitacion, idTareaMantenimiento, UsuarioActual);
		}

        public void FinalizarMantenimiento(int idHabitacion, List<int> idsOrdenesTrabajo)
		{
			AplicacionServicioHabitaciones.FinalizarMantenimiento(idHabitacion, UsuarioActual, idsOrdenesTrabajo);
		}

		public void FinalizarOcupacion(int idHabitacion, bool esCancelacion)
		{
			AplicacionServicioHabitaciones.FinalizarOcupacion(idHabitacion, esCancelacion, UsuarioActual);
		}

		public void CancelarPreparacionHabitacion(int idHabitacion)
		{
			AplicacionServicioHabitaciones.CancelarPreparacionHabitacion(idHabitacion, UsuarioActual);
		}

        public void EliminarHabitacion(int idHabitacion)
        {
            AplicacionServicioHabitaciones.EliminarHabitacion(idHabitacion, UsuarioActual);
        }

        public void MarcarComoReservada(int idHabitacion)
        {
            AplicacionServicioHabitaciones.MarcarComoReservada(idHabitacion, UsuarioActual);
        }

        public void CancelarEstadoReservacionHabitacion(int idHabitacion)
        {
            AplicacionServicioHabitaciones.CancelarEstadoReservacionHabitacion(idHabitacion, UsuarioActual);
        }

        public DtoResumenHabitacion SP_ObtenerResumenHabitacion(int idHabitacion, int? cambiosup)
        {
            return AplicacionServicioHabitaciones.SP_ObtenerResumenHabitacion(idHabitacion, cambiosup);
        }

        public List<DtoResumenHabitacion> SP_ObtenerResumenesHabitaciones()
        {
            return AplicacionServicioHabitaciones.SP_ObtenerResumenesHabitaciones();
        }

        public List<DtoEstadisticasHabitacion> ObtenerEstadisticasHabitaciones() 
        {
            return AplicacionServicioHabitaciones.ObtenerEstadisticasHabitaciones();
        }

        public void CrearHabitacion(Habitacion habitacion)
        {
            AplicacionServicioHabitaciones.CrearHabitacion(habitacion, UsuarioActual);
        }

        public void ModificarHabitacion(Habitacion habitacion)
        {
            AplicacionServicioHabitaciones.ModificarHabitacion(habitacion, UsuarioActual);
        }

        public void FinalizarConfiguracionHabitaciones()
        {
            AplicacionServicioHabitaciones.FinalizarConfiguracionHabitaciones(UsuarioActual);
        }
    }
}