﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Negocio.Restaurantes;
using Transversal.Dependencias;
using Modelo.Entidades;
using Modelo.Dtos;
using Dominio.Nucleo.Entidades;

namespace SOTControladores.Controladores
{

    public class ControladorRestaurantes : ControladorBase
    {
        private IServicioRestaurantes AplicacionServicioRestaurantes
        {
            get { return _aplicacionServicioRestaurantes.Value; }
        }

        private Lazy<IServicioRestaurantes> _aplicacionServicioRestaurantes = new Lazy<IServicioRestaurantes>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioRestaurantes>();
        });

        public void MarcarMesaComoOcupada(int idMesa, int idMesero)
        {
            AplicacionServicioRestaurantes.MarcarMesaComoOcupada(idMesa, idMesero, UsuarioActual);
        }

        public void CrearOrden(Modelo.Entidades.OrdenRestaurante orden, int idMesa)//, ConsumoInterno datosConsumo = null)
        {
            AplicacionServicioRestaurantes.CrearOrden(orden, idMesa, UsuarioActual);//, datosConsumo);
        }

        public void ValidarAcceso()
        {
            AplicacionServicioRestaurantes.ValidarAcceso(UsuarioActual);
        }

        public void ModificarOrden(int idOrden, List<Modelo.Dtos.DtoArticuloPrepararGeneracion> articulos)
        {
            AplicacionServicioRestaurantes.ModificarOrden(idOrden, articulos, UsuarioActual);
        }

        public int ObtenerCantidadMesasAbiertas()
        {
            return AplicacionServicioRestaurantes.ObtenerCantidadMesasCuentasAbiertas(UsuarioActual);
        }

        public void CancelarOrden(int idOrden, string motivo)
        {
            AplicacionServicioRestaurantes.CancelarOrden(idOrden, motivo, ObtenerCredencial());
        }

        public string RepararCancelacionesOcupacion(string motivoX)
        {
            return AplicacionServicioRestaurantes.RepararCancelacionesOcupacion(motivoX, ObtenerCredencial());
        }

        public void CrearMesa(Mesa mesa)
        {
            AplicacionServicioRestaurantes.CrearMesa(mesa, UsuarioActual);
        }

        public void EliminarMesa(int idMesa)
        {
            AplicacionServicioRestaurantes.EliminarMesa(idMesa, UsuarioActual);
        }

        public void ModificarMesa(Mesa mesa)
        {
            AplicacionServicioRestaurantes.ModificarMesa(mesa, UsuarioActual);
        }

        public void CancelarOcupacion(int idOcupacion, string motivo)
        {
            AplicacionServicioRestaurantes.CancelarOcupacion(idOcupacion, motivo, ObtenerCredencial());
        }
        public void EntregarProductos(int idOrden, List<int> idsArticulosOrden)
        {
            AplicacionServicioRestaurantes.EntregarProductos(idOrden, idsArticulosOrden, ObtenerCredencial());
        }

        public void EntregarOrdenACliente(int idOrden)
        {
            AplicacionServicioRestaurantes.EntregarOrdenACliente(idOrden, UsuarioActual);
        }


        public void FinalizarElaboracionProductos(int idOrden, List<int> idsArticulosOrden)
        {
            AplicacionServicioRestaurantes.FinalizarElaboracionProductos(idOrden, idsArticulosOrden, ObtenerCredencial());
        }

        public List<Modelo.Entidades.OrdenRestaurante> ObtenerDetallesOrdenesPendientes(int idMesa)
        {
            return AplicacionServicioRestaurantes.ObtenerDetallesOrdenesPendientes(idMesa, UsuarioActual);
        }

        public List<ArticuloOrdenRestaurante> ObtenerDetallesOrdenesEntregadas(int idOcupacion)
        {
            return AplicacionServicioRestaurantes.ObtenerDetallesOrdenesEntregadasCliente(idOcupacion, UsuarioActual);
        }

        //public List<Modelo.Dtos.DtoArticuloPrepararConsulta> ObtenerArticulosPrepararOrdenes()
        //{
        //    return AplicacionServicioRestaurantes.ObtenerArticulosPrepararOrdenes(UsuarioActual);
        //}

        public List<Modelo.Entidades.Mesa> ObtenerMesas()
        {
            return AplicacionServicioRestaurantes.ObtenerMesas(UsuarioActual);
        }

        //public decimal ObtenerPagosPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null)
        //{
        //    return AplicacionServicioRestaurantes.ObtenerPagosPorFecha(UsuarioActual, fechaInicio, fechaFin);
        //}

        //public Dictionary<TiposPago, decimal> ObtenerPagosMesaPorFecha(DateTime? fechaInicio = null, DateTime? fechaFin = null, params TiposPago[] tiposPagos)
        //{
        //    return AplicacionServicioRestaurantes.ObtenerDiccionarioPagosMesaPorFecha(UsuarioActual, fechaInicio, fechaFin, tiposPagos);
        //}

        public OcupacionMesa ObtenerOcupacionActualPorMesa(int idMesa)
        {
            return AplicacionServicioRestaurantes.ObtenerOcupacionActualPorMesa(idMesa);
        }

        public OcupacionMesa ObtenerOcupacionBaseActualPorMesa(int idMesa)
        {
            return AplicacionServicioRestaurantes.ObtenerOcupacionBaseActualPorMesa(idMesa);
        }

        public void ValidarNoTieneOrdenesPendientes(int idOcupacion)
        {
            AplicacionServicioRestaurantes.ValidarNoTieneOrdenesPendientes(idOcupacion);
        }

        public Mesa ObtenerMesa(int idMesa)
        {
            return AplicacionServicioRestaurantes.ObtenerMesa(idMesa);
        }

        //public DtoValor ObtenerValorCobrarMesa(int idMesa)
        //{
        //    return AplicacionServicioRestaurantes.ObtenerValorCobrarMesa(idMesa);
        //}

        public void CobrarMesa(int idMesa, List<Modelo.Dtos.DtoInformacionPago> formasPago, Propina propina, int idMeseroCobro, bool marcarComoCortesia)//, ConsumoInterno consumo)
        {
            //#if DEBUG
            //#error No compilar en debug hasta mandar un valor adecuado para determinar si la comanda se va a pagar como cortesía
            //#endif

            AplicacionServicioRestaurantes.CobrarMesa(idMesa, formasPago, propina, idMeseroCobro, marcarComoCortesia, UsuarioActual);
        }

        public Mesa ObtenerMesaPorOcupacion(int idOcupacion)
        {
            return AplicacionServicioRestaurantes.ObtenerMesaPorOcupacion(idOcupacion, UsuarioActual);
        }

        public void MarcarMesaComoPorCobrar(int idMesa, List<int> idsComandasCortesias)
        {
            AplicacionServicioRestaurantes.MarcarMesaComoPorCobrar(idMesa, idsComandasCortesias, UsuarioActual);
        }

        public Modelo.Dtos.DtoDatosMesa ObtenerDatosMesaActiva(int idMesa)
        {
            return AplicacionServicioRestaurantes.ObtenerDatosMesaActiva(idMesa, UsuarioActual);
        }

        public bool VerificarTieneOrdenesPendientes(int idOcupacion)
        {
            return AplicacionServicioRestaurantes.VerificarTieneOrdenesPendientes(idOcupacion);
        }

        //public List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosEnPreparacionDepartamento(string departamento)
        //{
        //    return AplicacionServicioRestaurantes.ObtenerDetallesOrdenesArticulosEnPreparacionDepartamento(departamento);
        //}

        //public List<OrdenRestaurante> ObtenerDetallesOrdenesArticulosPreparadosDepartamento(string departamento)
        //{
        //    return AplicacionServicioRestaurantes.ObtenerDetallesOrdenesArticulosPreparadosDepartamento(departamento);
        //}

        public List<DtoResumenComandaExtendido> ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(string departamento)
        {
            return AplicacionServicioRestaurantes.ObtenerDetallesOrdenesArticulosPreparadosOEnPreparacion(departamento);
        }

        public void ActualizarPagoOcupacion(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, int idEmpleado, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null)
        {
            AplicacionServicioRestaurantes.ActualizarPagoOcupacion(idPago, formaPago, valorPropina, referencia, idEmpleado, UsuarioActual, numeroTarjeta, tipoTarjeta);
        }

        public void VincularDatosFiscales(int idOcupacionMesa, int? idDatosFiscales)
        {
            AplicacionServicioRestaurantes.VincularDatosFiscales(idOcupacionMesa, idDatosFiscales, UsuarioActual);
        }

        public void IntercambiarMesas(int idMesaOrigen, int idMesaDestino)
        {
            AplicacionServicioRestaurantes.IntercambiarMesas(idMesaOrigen, idMesaDestino, UsuarioActual);
        }

        public void ReimprimirTicket(int idMesa)
        {
            AplicacionServicioRestaurantes.ReimprimirTicket(idMesa);
        }

        public void Imprimir(int idOrden)
        {
            AplicacionServicioRestaurantes.Imprimir(idOrden);
        }

        public void FinalizarConfiguracionMesas()
        {
            AplicacionServicioRestaurantes.FinalizarConfiguracionMesas(UsuarioActual);
        }
    }
}