﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Negocio.ConfiguracionesTipo;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
	public class ControladorConfiguracionesTipo : ControladorBase
	{
        private IServicioConfiguracionesTipo AplicacionServicioConfiguracionesTipo
        {
			get { return _aplicacionServicioConfiguracionesTipo.Value; }
		}

		private Lazy<IServicioConfiguracionesTipo> _aplicacionServicioConfiguracionesTipo = new Lazy<IServicioConfiguracionesTipo>(() =>
		{
            return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesTipo>();
		});
		public Modelo.Entidades.ConfiguracionTipo ObtenerConfiguracionTipo(int idConfiguracionTipo, Modelo.Entidades.ConfiguracionTarifa.Tarifas tarifaElegida)
		{
			return AplicacionServicioConfiguracionesTipo.ObtenerConfiguracionTipo(idConfiguracionTipo, tarifaElegida);
		}
	}
}