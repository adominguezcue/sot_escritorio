﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Modelo;
using Negocio.TareasMantenimiento;
using Transversal.Dependencias;
using Modelo.Entidades;
using Modelo.Dtos;

namespace SOTControladores.Controladores
{
	public class ControladorTareasMantenimiento : ControladorBase
	{
		private IServicioTareasMantenimiento AplicacionServicioTareasMantenimiento {
			get { return _aplicacionServicioTareasMantenimiento.Value; }
		}

		private Lazy<IServicioTareasMantenimiento> _aplicacionServicioTareasMantenimiento = new Lazy<IServicioTareasMantenimiento>(() =>
		{
            return FabricaDependencias.Instancia.Resolver<IServicioTareasMantenimiento>();
		});

        public int ObtenerCantidadTareasPendientes(int idHabitacion = 0)
        {
            return AplicacionServicioTareasMantenimiento.ObtenerCantidadTareasPendientes(UsuarioActual, idHabitacion);
        }

        public List<DtoTareaMantenimientoBase> ObtenerTareasPorMes(int mes, int anio, int idHabitacion = 0)
        {
            return AplicacionServicioTareasMantenimiento.ObtenerTareasPorMes(mes, anio, UsuarioActual, idHabitacion);
        }

        public void CrearTarea(TareaMantenimiento tarea)
        {
            AplicacionServicioTareasMantenimiento.CrearTarea(tarea, UsuarioActual);
        }

        public void CrearTareaInstantanea(int idHabitacion, int idConceptoMantenimiento, string descripcion)
        {
            AplicacionServicioTareasMantenimiento.CrearTareaInstantanea(idHabitacion, idConceptoMantenimiento, descripcion, UsuarioActual);
        }

        public void ModificarTarea(TareaMantenimiento tarea)
        {
            AplicacionServicioTareasMantenimiento.ModificarTarea(tarea, UsuarioActual);
        }

        public TareaMantenimiento ObtenerTareaPorId(int idTarea)
        {
            return AplicacionServicioTareasMantenimiento.ObtenerTareaPorId(idTarea, UsuarioActual);
        }

        public void EliminarTarea(int idTarea)
        {
            AplicacionServicioTareasMantenimiento.EliminarTarea(idTarea, UsuarioActual);
        }

        public List<DtoResumenMantenimiento> ObtenerMantenimientosPendientes(int idHabitacion, DateTime? fechaInicio = null, DateTime? fechaFin = null)
        {
            return AplicacionServicioTareasMantenimiento.ObtenerMantenimientosPendientes(idHabitacion, UsuarioActual, fechaInicio, fechaFin);
        }
    }
}