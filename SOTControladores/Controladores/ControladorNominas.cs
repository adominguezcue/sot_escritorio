﻿using Modelo.Entidades;
using Negocio.Nominas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorNominas: ControladorBase
    {
        private IServicioNominas AplicacionServicioNominas
        {
            get { return _aplicacionServicioNominas.Value; }
        }

        private Lazy<IServicioNominas> _aplicacionServicioNominas = new Lazy<IServicioNominas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioNominas>();
        });

        public Nomina GenerarNominaNoPersistida(int idEmpleado, int anio, int mes, bool primeraMitad, bool enVacaciones) 
        {
            return AplicacionServicioNominas.GenerarNominaNoPersistida(idEmpleado, anio, mes, primeraMitad, enVacaciones, UsuarioActual);
        }

        public List<Nomina> ObtenerNominasFiltradas(string nombre = null, string apellidoPaterno = null, string apellidoMaterno = null, DateTime? fechaInicial = null, DateTime? fechaFinal = null) 
        {
            return AplicacionServicioNominas.ObtenerNominasFiltradas(UsuarioActual, nombre, apellidoPaterno, apellidoMaterno, fechaInicial, fechaFinal);
        }

        public void RegistrarNomina(Nomina nomina)
        {
            AplicacionServicioNominas.RegistrarNomina(nomina, UsuarioActual);
        }

        public void ModificarNomina(Nomina nomina)
        {
            AplicacionServicioNominas.ModificarNomina(nomina, UsuarioActual);
        }

        public void EliminarNomina(int idNomina)
        {
            AplicacionServicioNominas.EliminarNomina(idNomina, UsuarioActual);
        }
    }
}
