﻿using Negocio.CentrosCostos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorCentrosCostos : ControladorBase
    {
        private IServicioCentrosCostos ServicioCentrosCostos
        {
            get { return _servicioCentrosCostos.Value; }
        }

        private Lazy<IServicioCentrosCostos> _servicioCentrosCostos = new Lazy<IServicioCentrosCostos>(() => { return FabricaDependencias.Instancia.Resolver<IServicioCentrosCostos>(); });


        public List<Modelo.Entidades.CentroCostos> FILTRO(bool cargarNombreCategoria) 
        {
            return ServicioCentrosCostos.ObtenerCentrosCostosFILTRO(cargarNombreCategoria);
        }

        public List<Modelo.Entidades.CategoriaCentroCostos> ObtenerCategoriasCentrosCostos()
        {
            return ServicioCentrosCostos.ObtenerCategoriasCentrosCostos();
        }

        public List<Modelo.Entidades.CentroCostos> ObtenerCentrosCostosFiltrados(string nombreCentro = null, int? idCategoriaCentro = null)
        {
            return ServicioCentrosCostos.ObtenerCentrosCostosFiltrados(UsuarioActual, nombreCentro, idCategoriaCentro);
        }

        public void CrearCentroCostos(Modelo.Entidades.CentroCostos centroActual)
        {
            ServicioCentrosCostos.CrearCentroCostos(centroActual, UsuarioActual);
        }

        public void ModificarCentroCostos(Modelo.Entidades.CentroCostos centroActual)
        {
            ServicioCentrosCostos.ModificarCentroCostos(centroActual, UsuarioActual);
        }

        public void EliminarCentroCostos(int idCentroCostos)
        {
            ServicioCentrosCostos.EliminarCentroCostos(idCentroCostos, UsuarioActual);
        }
    }
}
