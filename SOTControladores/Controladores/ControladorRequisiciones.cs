﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Requisiciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorRequisiciones : ControladorBase
    {
        private IServicioRequisiciones AplicacionServicioRequisiciones
        {
            get { return _aplicacionServicioRequisiciones.Value; }
        }

        private Lazy<IServicioRequisiciones> _aplicacionServicioRequisiciones = new Lazy<IServicioRequisiciones>(() => FabricaDependencias.Instancia.Resolver<IServicioRequisiciones>());

        public ZctRequisition ObtenerElemento(int folio)
        {
            return AplicacionServicioRequisiciones.ObtenerElemento(folio, UsuarioActual);
        }
        public List<ZctRequisition> ObtenerElementos(string status = null, int? anio = null, int? mes = null)
        {
            return AplicacionServicioRequisiciones.ObtenerElementos(UsuarioActual, status, anio, mes);
        }

        //public void CrearRequisicion(ZctRequisition requisicion)
        //{
        //    AplicacionServicioRequisiciones.CrearRequisicion(requisicion, UsuarioActual);
        //}
    }
}
