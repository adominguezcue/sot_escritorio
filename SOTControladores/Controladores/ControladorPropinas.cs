﻿using Negocio.ConfiguracionesPropinas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;
using Modelo.Entidades;
using Negocio.Propinas;
using System.Data;

namespace SOTControladores.Controladores
{
    public class ControladorPropinas : ControladorBase
    {
        private IServicioConfiguracionesPropinas AplicacionServicioConfiguracionesPropinas
        {
            get { return _aplicacionServicioConfiguracionesPropinas.Value; }
        }

        private Lazy<IServicioConfiguracionesPropinas> _aplicacionServicioConfiguracionesPropinas = new Lazy<IServicioConfiguracionesPropinas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConfiguracionesPropinas>();
        });

        private IServicioPropinas AplicacionServicioPropinas
        {
            get { return _aplicacionServicioPropinas.Value; }
        }

        private Lazy<IServicioPropinas> _aplicacionServicioPropinas = new Lazy<IServicioPropinas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioPropinas>();
        });

        public Modelo.Entidades.ConfiguracionPropinas ObtenerConfiguracionPropinas()
        {
            return AplicacionServicioConfiguracionesPropinas.ObtenerConfiguracionPropinas(UsuarioActual);
        }

        public void ActualizarConfiguracionPropinas(ConfiguracionPropinas configuracionPropinas)
        {
            AplicacionServicioConfiguracionesPropinas.ActualizarConfiguracionPropinas(configuracionPropinas, UsuarioActual);
        }

        //public Dictionary<Propina.TiposPropina, decimal> ObtenerTotalPropinasPorTipo(DateTime? fechaInicio = null, DateTime? fechaFin = null) 
        //{
        //    return AplicacionServicioPropinas.ObtenerTotalPropinasPorTipoExterno(UsuarioActual, fechaInicio, fechaFin);
        //}
    }
}
