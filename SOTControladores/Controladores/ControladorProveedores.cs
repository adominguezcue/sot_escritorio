﻿using Modelo.Almacen.Entidades;
using Modelo.Almacen.Entidades.Dtos;
using Negocio.Almacen.Proveedores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorProveedores : ControladorBase
    {
        private IServicioProveedores AplicacionServicioProveedores
        {
            get { return _aplicacionServicioProveedores.Value; }
        }

        private Lazy<IServicioProveedores> _aplicacionServicioProveedores = new Lazy<IServicioProveedores>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioProveedores>();
        });

        public List<Proveedor> ObtenerProveedores()
        {
            return AplicacionServicioProveedores.ObtenerProveedores(UsuarioActual);
        }

        public List<Proveedor> ObtieneProveedoresPorFiltroNombre(string text)
        {
            return AplicacionServicioProveedores.ObtieneProveedoresPorFiltroNombre(text);
        }

        public List<DtoResumenProveedor> ObtenerResumenesProveedores(string filtro = null)
        {
            return AplicacionServicioProveedores.ObtenerResumenesProveedores(UsuarioActual, filtro);
        }

        public Proveedor ObtenerProveedor(int codigo = 0)
        {
            return AplicacionServicioProveedores.ObtenerProveedor(UsuarioActual, codigo);
        }
        public void CrearProveedor(Proveedor proveedor)
        {
            AplicacionServicioProveedores.CrearProveedor(proveedor, UsuarioActual);
        }

        public void ModificarProveedor(Proveedor proveedor)
        {
            AplicacionServicioProveedores.ModificarProveedor(proveedor, UsuarioActual);
        }

        public void Elimina(int idProveedor)
        {
            AplicacionServicioProveedores.Elimina(idProveedor, UsuarioActual);
        }


        public string ObtenerSiguienteCodigoProveedor()
        {
            return AplicacionServicioProveedores.ObtenerSiguienteCodigoProveedor();
        }

        public int ObtenerPosicionProveedor(int Cod_proveedor)
        {
            return AplicacionServicioProveedores.ObtenerPosicionProveedor(Cod_proveedor);
        }
    }
}
