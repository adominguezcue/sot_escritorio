﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.CuentasPorPagar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorCuentasPorPagar:ControladorBase
    {
         private IServicioCuentasPorPagar AplicacionServicioCuentasPorPagar
        {
            get { return _aplicacionServicioCuentasPorPagar.Value; }
        }

        private Lazy<IServicioCuentasPorPagar> _aplicacionServicioCuentasPorPagar = new Lazy<IServicioCuentasPorPagar>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioCuentasPorPagar>();
        });

        //public void CancelarCxP(int OC)
        //{
        //    AplicacionServicioCuentasPorPagar.CancelarCxP(OC);
        //}

        //public ZctEncCuentasPagar CreaCuentaPagar(int Cod_OC)
        //{
        //    return AplicacionServicioCuentasPorPagar.CreaCuentaPagar(Cod_OC);
        //}
        public ZctDetCuentasPagar CreaPago(ZctEncCuentasPagar cxp, string Referencia, int Metodo, decimal monto, decimal cambio, DateTime fechapago, DateTime fechavencido)
        {
            return AplicacionServicioCuentasPorPagar.CreaPago(cxp, Referencia, Metodo, monto, cambio, fechapago, fechavencido);
        }
        public void ActualizarPagos(ZctEncCuentasPagar CxP)//, ZctDetCuentasPagar Detalle = null)
        {
            AplicacionServicioCuentasPorPagar.ActualizarPagos(CxP, UsuarioActual);//, Detalle);
        }
        public List<ZctEncCuentasPagar> TraeCuentasPorEstado(string filtroProveedor, ZctEncCuentasPagar.Estados? Estado = null, DateTime? fechacreacionini=null, DateTime? fechacreacionfin=null, DateTime? fechavencimientoini=null, DateTime? fechavencimientofin=null, bool? esfechacreacion=null,bool? esfechafin=null)
        {
            return AplicacionServicioCuentasPorPagar.TraeCuentasPorEstado(filtroProveedor, Estado, fechacreacionini, fechacreacionfin, fechavencimientoini, fechavencimientofin, esfechacreacion, esfechafin);
        }
        public List<ZctEncCuentasPagar> TraeCuentasProveedores(int cod_prov, ZctEncCuentasPagar.Estados? Estado = null)
        {
            return AplicacionServicioCuentasPorPagar.TraeCuentasProveedores(cod_prov, Estado);
        }
        public List<Transversal.Dtos.DtoEnum> TraeEstadosPago()
        {
            return AplicacionServicioCuentasPorPagar.TraeEstadosPago();
        }
        public int TraeFolioCxP()
        {
            return AplicacionServicioCuentasPorPagar.TraeFolioCxP();
        }
        public List<ZctDetCuentasPagar> TraePagosVencidosProveedor(DateTime fecha, int cod_prov)
        {
            return AplicacionServicioCuentasPorPagar.TraePagosVencidosProveedor(fecha, cod_prov);
        }
        public ZctEncCuentasPagar ValidarYObtenerCargada(int cod_cuenta_pago)
        {
            return AplicacionServicioCuentasPorPagar.ValidarYObtenerCargada(cod_cuenta_pago);
        }
        public ZctEncCuentasPagar ValidaOcCxP(int codigoOrdenCompra)
        {
            return AplicacionServicioCuentasPorPagar.ObtenerCuentaPorPagarPorOrdenCompra(codigoOrdenCompra);
        }

        public List<WV_ZctProveedoresCxP> TraeCuentasVencidas(DateTime fecha)
        {
            return AplicacionServicioCuentasPorPagar.TraeCuentasVencidas(fecha);
        }
        public List<WV_ZctProveedoresCxP> TraeProveedores(int? idEstado = null)
        {
            return AplicacionServicioCuentasPorPagar.TraeProveedores(idEstado);
        }
        public List<WV_ZctProveedoresCxP> TraeProveedoresPorEstatusCuenta(string filtroProveedor, int? idEstado = null)
        {
            return AplicacionServicioCuentasPorPagar.TraeProveedoresPorEstatusCuenta(filtroProveedor, idEstado);
        }

        public DateTime? ObtenerFechaUltimaCuenta(int codigoOrdenCompra)
        {
            return AplicacionServicioCuentasPorPagar.ObtenerFechaUltimaCuenta(codigoOrdenCompra, UsuarioActual);
        }
    }
}
