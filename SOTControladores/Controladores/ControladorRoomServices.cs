﻿using Dominio.Nucleo.Entidades;
using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.RoomServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorRoomServices : ControladorBase
    {
        private IServicioRoomServices AplicacionServicioRoomServices
        {
            get { return _aplicacionServicioRoomServices.Value; }
        }

        private Lazy<IServicioRoomServices> _aplicacionServicioRoomServices = new Lazy<IServicioRoomServices>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioRoomServices>();
        });

        public Comanda.Estados? ObtenerMaximoEstadoComandasPendientes(int idRenta)
        {
            return AplicacionServicioRoomServices.ObtenerMaximoEstadoComandasPendientes(idRenta);
        }

        public void CrearComanda(Modelo.Entidades.Comanda nuevaComanda)
        {
            AplicacionServicioRoomServices.CrearComanda(nuevaComanda, UsuarioActual);
        }

        public List<Modelo.Entidades.Comanda> ObtenerDetallesComandasPendientes(int idRenta)
        {
            return AplicacionServicioRoomServices.ObtenerDetallesComandasPendientes(idRenta, UsuarioActual);
        }

        public void CancelarComanda(int idComanda, string motivo)
        {
            var huellaDigital = ObtenerCredencial();

            AplicacionServicioRoomServices.CancelarComanda(idComanda, motivo, huellaDigital);
        }

        public void EntregarProductos(int idComanda, List<int> idsArticulosComandas)
        {
            AplicacionServicioRoomServices.EntregarProductos(idComanda, idsArticulosComandas, ObtenerCredencial());
        }

        public void CrearMotivoDeCambio(Comanda comandaActual, string motivo)
        {
            AplicacionServicioRoomServices.AgregaCambioComanda(UsuarioActual, comandaActual, motivo);

        }

        public void PagarComanda(int idComanda, List<Modelo.Dtos.DtoInformacionPago> pagos, Modelo.Entidades.Propina propina, bool marcarComoCortesia)
        {
//#if DEBUG
//#error No compilar en debug hasta mandar un valod adecuado para determinar si la comanda se va a pagar como cortesía
//#endif

            AplicacionServicioRoomServices.PagarComanda(idComanda, pagos, propina, marcarComoCortesia, UsuarioActual);
        }

        public List<DtoResumenComandaExtendido> ObtenerDetallesComandasArticulosPreparadosOEnPreparacion(string departamento)
        {
            return AplicacionServicioRoomServices.ObtenerDetallesComandasArticulosPreparadosOEnPreparacion(departamento);
        }

        public void ModificarComanda(int idComanda, List<Modelo.Dtos.DtoArticuloPrepararGeneracion> articulos)
        {
            AplicacionServicioRoomServices.ModificarComanda(idComanda, articulos, UsuarioActual);
        }

        public void SolicitaCambioComanda()
        {
            var huellaDigital = ObtenerCredencial();
            AplicacionServicioRoomServices.SolicitaCambioComanda(huellaDigital);
        }

        public void  SolicitaCambioConsumoInterno()
        {
            var huellaDigital = ObtenerCredencial();
            AplicacionServicioRoomServices.SolicitaCambioConsumoInterno(huellaDigital);
        }

        public void SolicitaCambioRestaurante()
        {
            var huellaDigital = ObtenerCredencial();
            AplicacionServicioRoomServices.SolicitaCambioRestaurante(huellaDigital);
        }

        public void FinalizarElaboracionProductos(int idComanda, List<int> idsArticulosComandas)
        {
            AplicacionServicioRoomServices.FinalizarElaboracionProductos(idComanda, idsArticulosComandas, ObtenerCredencial());
        }

        public void CobrarComanda(int idComanda)
        {
            AplicacionServicioRoomServices.CobrarComanda(idComanda, UsuarioActual);
        }

        public void ActualizarPagoComanda(int idPago, TiposPago formaPago, decimal valorPropina, string referencia, int idEmpleado, string numeroTarjeta = null, TiposTarjeta? tipoTarjeta = null)
        {
            AplicacionServicioRoomServices.ActualizarPagoComanda(idPago, formaPago, valorPropina, referencia, idEmpleado, UsuarioActual, numeroTarjeta, tipoTarjeta);
        }

        public List<DtoLogComanda> ObtenerLogComandasPorFecha(DateTime fechaInicio, DateTime fechaFin, int? ordenTurno = null, int? idLinea = null, Comanda.Tipos? tipo = null, Comanda.Estados? estado = null)
        {
            return AplicacionServicioRoomServices.ObtenerLogComandasPorFecha(fechaInicio, fechaFin, UsuarioActual, ordenTurno, idLinea, tipo, estado);
        }


        public void Imprimir(int idComanda)
        {
            AplicacionServicioRoomServices.Imprimir(idComanda);
        }
    }
}
