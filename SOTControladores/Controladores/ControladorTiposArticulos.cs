﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.TiposArticulos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorTiposArticulos : ControladorBase
    {
        private IServicioTiposArticulos AplicacionServicioTiposArticulos
        {
            get { return _aplicacionServicioTiposArticulos.Value; }
        }

        private Lazy<IServicioTiposArticulos> _aplicacionServicioTiposArticulos = new Lazy<IServicioTiposArticulos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioTiposArticulos>();
        });

        public List<TipoArticulo> ObtenerTipos()
        {
            return AplicacionServicioTiposArticulos.ObtenerTipos(UsuarioActual);
        }
    }
}
