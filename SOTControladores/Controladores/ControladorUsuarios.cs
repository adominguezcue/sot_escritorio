﻿using Modelo.Seguridad.Entidades;
using Negocio.Seguridad.Usuarios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorUsuarios : ControladorBase
    {
        private IServicioUsuarios AplicacionServicioUsuarios
        {
            get { return _aplicacionServicioUsuarios.Value; }
        }

        private Lazy<IServicioUsuarios> _aplicacionServicioUsuarios = new Lazy<IServicioUsuarios>(() => FabricaDependencias.Instancia.Resolver<IServicioUsuarios>());

        public void ValidaUsuario(int idUsuario)
        {
            AplicacionServicioUsuarios.ValidaUsuario(idUsuario);
        }

        //public void CrearUsuario(Modelo.Seguridad.Entidades.Usuario usuario, List<Modelo.Seguridad.Dtos.DtoCredencial> confirmaciones)
        //{
        //    AplicacionServicioUsuarios.CrearUsuario(usuario, confirmaciones, UsuarioActual);
        //}

        public Modelo.Seguridad.Entidades.Usuario ObtenerUsuarioPorEmpleado(int idEmpleado)
        {
            return AplicacionServicioUsuarios.ObtenerUsuarioPorEmpleado(idEmpleado, UsuarioActual);
        }

        public void CrearUsuario(Modelo.Seguridad.Entidades.Usuario usuario, List<Modelo.Seguridad.Dtos.DtoCredencial> comprobaciones)
        {
            AplicacionServicioUsuarios.CrearUsuario(usuario, comprobaciones, UsuarioActual);
        }

        public void ModificarUsuario(int idUsuario, string nuevoAlias, List<Credencial> credencialesCrear, List<Credencial> credencialesEliminar, List<Modelo.Seguridad.Dtos.DtoCredencial> comprobaciones)
        {
            AplicacionServicioUsuarios.ModificarUsuario(idUsuario, nuevoAlias, credencialesCrear, credencialesEliminar, comprobaciones, UsuarioActual);
        }

        public void EliminarUsuario(int idUsuario) 
        {
            AplicacionServicioUsuarios.EliminarUsuario(idUsuario, UsuarioActual);
        }

        public List<Credencial.TiposCredencial> ObtenerTiposCredencialesExistentes(int idUsuario)
        {
            return AplicacionServicioUsuarios.ObtenerTiposCredencialesExistentes(idUsuario, UsuarioActual);
        }
    }
}
