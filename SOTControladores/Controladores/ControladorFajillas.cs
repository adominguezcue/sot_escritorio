﻿using Modelo.Dtos;
using Modelo.Entidades;
using Negocio.Fajillas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorFajillas : ControladorBase
    {
        private IServicioFajillas AplicacionServicioFajillas
        {
            get { return _aplicacionServicioFajillas.Value; }
        }

        private Lazy<IServicioFajillas> _aplicacionServicioFajillas = new Lazy<IServicioFajillas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioFajillas>();
        });

        public ConfiguracionFajilla ObtenerConfiguracionFajillas()
        {
            return AplicacionServicioFajillas.ObtenerConfiguracionFajillas(UsuarioActual);
        }

        public ConfiguracionFajilla ObtenerConfiguracionFajillasCargadaFILTRO()
        {
            return AplicacionServicioFajillas.ObtenerConfiguracionFajillasCargadaFILTRO();
        }

        public void CrearFajilla(Fajilla fajillaNueva)
        {
            AplicacionServicioFajillas.CrearFajilla(fajillaNueva, UsuarioActual);
        }

        public Fajilla ObtenerCombinacionFajillas(ConfiguracionFajilla configuracion, IEnumerable<Fajilla> fajillas) 
        {
            return AplicacionServicioFajillas.ObtenerCombinacionFajillas(configuracion, fajillas);
        }

        public void ActualizarMontoConfiguracion(decimal valor)
        {
            AplicacionServicioFajillas.ActualizarMontoConfiguracion(valor, UsuarioActual);
        }

        public int ObtenerCantidadFajillasPendientesPorTurno(int idCorte)
        {
            return AplicacionServicioFajillas.ObtenerCantidadFajillasPendientesPorTurno(idCorte, UsuarioActual);
        }

        public void GuardarConfiguracion(DtoConfiguracionFajilla configuracion)
        {
            AplicacionServicioFajillas.GuardarConfiguracion(configuracion, UsuarioActual);
        }

        public List<Fajilla> ObtenerFajillasPorTurno(int idCorte, bool soloAutorizadas) 
        {
            return AplicacionServicioFajillas.ObtenerFajillasPorTurno(idCorte, soloAutorizadas, UsuarioActual);
        }

        public List<Fajilla> ObtenerFajillasAutorizadasConMontosPorCorte(int idCorte)
        {
            return AplicacionServicioFajillas.ObtenerFajillasAutorizadasConMontosPorCorte(UsuarioActual, idCorte);
        }

        public void AutorizarFajilla(int idFajilla)
        {
            AplicacionServicioFajillas.AutorizarFajilla(idFajilla, ObtenerCredencial());
        }

        public void GuardarSobranteCaja(int idCorte, Fajilla sobranteCaja)
        {
            AplicacionServicioFajillas.GuardarSobranteCaja(idCorte, sobranteCaja, UsuarioActual);
        }

        public Fajilla ObtenerSobranteCajaCorteRevision(int idCorteTurno)
        {
            return AplicacionServicioFajillas.ObtenerSobranteCajaTurnoRevision(idCorteTurno, UsuarioActual);
        }

        public DtoCabeceraFajilla ObtenerDesgloseEfectivoPorId(int idFajilla) 
        {
            return AplicacionServicioFajillas.ObtenerDesgloseEfectivoPorId(idFajilla);
        }
    }
}
