﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Bancos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorBancos : ControladorBase
    {
        private IServicioBancos AplicacionServicioBancos
        {
            get { return _aplicacionServicioBancos.Value; }
        }

        private Lazy<IServicioBancos> _aplicacionServicioBancos = new Lazy<IServicioBancos>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioBancos>();
        });

        public List<ZctBancos> ObtenerBancos()
        {
            return AplicacionServicioBancos.ObtenerBancos();
        }
    }
}
