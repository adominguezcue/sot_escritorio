﻿using Negocio.Paquetes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorPaquetes : ControladorBase
    {
        private IServicioPaquetes AplicacionServicioPaquetes
        {
            get { return _aplicacionServicioPaquetes.Value; }
        }

        private Lazy<IServicioPaquetes> _aplicacionServicioPaquetes = new Lazy<IServicioPaquetes>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioPaquetes>();
        });

        public List<Modelo.Entidades.Paquete> ObtenerPaquetesActivosConProgramas()
        {
            return AplicacionServicioPaquetes.ObtenerPaquetesActivosConProgramas();
        }

        public void CrearPaquete(Modelo.Entidades.Paquete paquete)
        {
            AplicacionServicioPaquetes.CrearPaquete(paquete, UsuarioActual);
        }

        public void ModificarPaquete(Modelo.Entidades.Paquete paquete)
        {
            AplicacionServicioPaquetes.ModificarPaquete(paquete, UsuarioActual);
        }

        public void EliminarPaquete(int idPaquete)
        {
            AplicacionServicioPaquetes.EliminarPaquete(idPaquete, UsuarioActual);
        }

        public List<Modelo.Entidades.Paquete> ObtenerPaquetesActivosConProgramasPorTipoHabitacion(int idTipoHabitacion)
        {
            return AplicacionServicioPaquetes.ObtenerPaquetesActivosConProgramasPorTipoHabitacion(idTipoHabitacion);
        }
    }
}
