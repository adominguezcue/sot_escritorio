﻿using Negocio.Almacen.EtiquetasArticulosCompuestos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorEtiquetas : ControladorBase
    {
        private IServicioEtiquetas AplicacionServicioEtiquetas
        {
            get { return _aplicacionServicioEtiquetas.Value; }
        }

        private Lazy<IServicioEtiquetas> _aplicacionServicioEtiquetas = new Lazy<IServicioEtiquetas>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioEtiquetas>();
        });

        public List<Modelo.Almacen.Entidades.VW_EtiquetasArticulosCompuestos> ObtenerEtiquetasFiltradas(string nombreArticulo = "", DateTime? diaIngreso = null)
        {
            return AplicacionServicioEtiquetas.ObtenerEtiquetasFiltradas(UsuarioActual, nombreArticulo, diaIngreso);
        }
    }
}
