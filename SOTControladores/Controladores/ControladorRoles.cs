﻿using Modelo.Seguridad.Entidades;
using Negocio.Seguridad.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorRoles : ControladorBase
    {
        private IServicioRoles AplicacionServicioRoles
        {
            get { return _aplicacionServicioRoles.Value; }
        }

        private Lazy<IServicioRoles> _aplicacionServicioRoles = new Lazy<IServicioRoles>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioRoles>();
        });

        public List<Rol> ObtenerRoles(string nombre = null, int pagina = 0, int elementos = 0) 
        {
            return AplicacionServicioRoles.ObtenerRoles(UsuarioActual, nombre, pagina, elementos);
        }

        public Rol ObtenerRolConPermisos(int idRol)
        {
            return AplicacionServicioRoles.ObtenerRolConPermisos(idRol, UsuarioActual);
        }

        public void EliminarRol(int idRol)
        {
            AplicacionServicioRoles.EliminarRol(idRol, UsuarioActual);
        }

        public void CrearRol(Rol rol)
        {
            AplicacionServicioRoles.CrearRol(rol, UsuarioActual);
        }

        public void ModificarRol(Rol rol)
        {
            AplicacionServicioRoles.ModificarRol(rol, UsuarioActual);
        }
    }
}
