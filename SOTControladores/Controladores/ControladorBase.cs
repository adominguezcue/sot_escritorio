﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Modelo;
using Transversal.Excepciones;
using System.Windows;
using Modelo.Seguridad.Dtos;
using SOTControladores.Seguridad;
using Negocio.Seguridad.Permisos;

namespace SOTControladores.Controladores
{

    public abstract class ControladorBase : GestorCredenciales
    {
        public static DtoUsuario UsuarioActual { get; protected set; }

        public override Modelo.Seguridad.Dtos.DtoCredencial ObtenerCredencial()
        {
            //throw new NotImplementedException();
            var chF = Transversal.Dependencias.FabricaDependencias.Instancia.Resolver<ICapturadorHuellaDigital>();
            chF.MostrarDialogo();

            if (chF.Credencial == null)
                throw new SOTException("Operación cancelada");

            return chF.Credencial;
        }
    }
}