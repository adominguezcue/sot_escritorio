﻿using Negocio.Almacen.UbicacionValores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorUbicacionesValores : ControladorBase
    {
        private IServicioUbicacionValores AplicacionServicioUbicacionValores
        {
            get { return _aplicacionServicioUbicacionValores.Value; }
        }

        private Lazy<IServicioUbicacionValores> _aplicacionServicioUbicacionValores = new Lazy<IServicioUbicacionValores>(() => FabricaDependencias.Instancia.Resolver<IServicioUbicacionValores>());

        public void CreaDetalleUbicacion(Modelo.Almacen.Entidades.ZctEncUbicacionValores Encabezado) 
        {
            AplicacionServicioUbicacionValores.CreaDetalleUbicacion(Encabezado);
        }

        public string FolioEncabezado() 
        {
            return AplicacionServicioUbicacionValores.FolioEncabezado();
        }

        public void Grabar(Modelo.Almacen.Entidades.ZctEncUbicacionValores EnCabezado) 
        {
            AplicacionServicioUbicacionValores.Grabar(EnCabezado);
        }

        public Modelo.Almacen.Entidades.ZctEncUbicacionValores ValidaFecha(DateTime Fecha) 
        {
            return AplicacionServicioUbicacionValores.ValidaFecha(Fecha, UsuarioActual);
        }

        public decimal Total(DateTime fecha) 
        {
            return AplicacionServicioUbicacionValores.Total(fecha);
        }
    }
}
