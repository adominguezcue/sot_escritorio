﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Localizaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorCiudades : ControladorBase
    {
        private IServicioCiudades AplicacionServicioCiudades
        {
            get { return _aplicacionServicioCiudades.Value; }
        }

        private Lazy<IServicioCiudades> _aplicacionServicioCiudades = new Lazy<IServicioCiudades>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioCiudades>();
        });

        public List<Ciudad> ObtenerCiudades(int idEstado = 0)
        {
            return AplicacionServicioCiudades.ObtenerCiudades(idEstado);
        }
    }
}
