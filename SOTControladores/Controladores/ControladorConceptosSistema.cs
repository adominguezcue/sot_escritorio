﻿using Modelo.Entidades;
using Negocio.ConceptosSistema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorConceptosSistema : ControladorBase
    {

        IServicioConceptosSistema AplicacionServicioConceptosSistema
        {
            get { return _aplicacionServicioConceptosSistema.Value; }
        }

        private Lazy<IServicioConceptosSistema> _aplicacionServicioConceptosSistema = new Lazy<IServicioConceptosSistema>(() =>
        {
            return FabricaDependencias.Instancia.Resolver<IServicioConceptosSistema>();

        });
        public List<ConceptoSistema> ObtenerConceptos()
        {
            return AplicacionServicioConceptosSistema.ObtenerConceptos(UsuarioActual);
        }

        public List<ConceptoSistema.TiposConceptos> ObtenerTiposConcepto()
        {
            return AplicacionServicioConceptosSistema.ObtenerTiposConcepto();
        }

        public void CrearConcepto(ConceptoSistema conceptoSistema)
        {
            AplicacionServicioConceptosSistema.CrearConcepto(conceptoSistema, UsuarioActual);
        }

        public void ModificarConcepto(ConceptoSistema conceptoSistema)
        {
            AplicacionServicioConceptosSistema.ModificarConcepto(conceptoSistema, UsuarioActual);
        }

        public void EliminarConcepto(int idConceptoSistema)
        {
            AplicacionServicioConceptosSistema.EliminarConcepto(idConceptoSistema, UsuarioActual);
        }

        public List<ConceptoSistema> ObtenerConceptosPorTipoFILTRO(ConceptoSistema.TiposConceptos tipoConcepto)
        {
            return AplicacionServicioConceptosSistema.ObtenerConceptosPorTipoFILTRO(tipoConcepto);
        }
    }
}
