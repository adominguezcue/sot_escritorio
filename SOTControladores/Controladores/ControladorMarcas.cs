﻿using Modelo.Almacen.Entidades;
using Negocio.Almacen.Marcas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dependencias;

namespace SOTControladores.Controladores
{
    public class ControladorMarcas : ControladorBase
    {
        private IServicioMarcas AplicacionServicioMarcas
        {
            get { return _aplicacionServicioMarcas.Value; }
        }

        private Lazy<IServicioMarcas> _aplicacionServicioMarcas = new Lazy<IServicioMarcas>(() => FabricaDependencias.Instancia.Resolver<IServicioMarcas>());

        public List<Marca> ObtenerMarcas() 
        {
            return AplicacionServicioMarcas.ObtenerMarcas();
        }
    }
}
