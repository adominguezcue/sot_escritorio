﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace SOTControladores.Utilidades
{
    public interface IBuscadorBindingListView : IBindingListView
    {
        void ApplySort(string propertyName, ListSortDirection direction);
    }

    public class BuscadorBindingList<T> : BindingList<T>, IBuscadorBindingListView
    {

        public BuscadorBindingList()
        { }

        public BuscadorBindingList(IEnumerable<T> list)
        {
            foreach (var item in list)
                Add(item);
        }
        protected List<T> OriginalList { get; } = new List<T>();

        #region Searching

        protected override bool SupportsSearchingCore
        {
            get
            {
                return true;
            }
        }

        protected override int FindCore(PropertyDescriptor prop, object key)
        {
            // Get the property info for the specified property.
            PropertyInfo propInfo = typeof(T).GetProperty(prop.Name);
            T item;

            if (key != null)
            {
                // Loop through the items to see if the key
                // value matches the property value.
                for (int i = 0; i < Count; ++i)
                {
                    item = (T)Items[i];
                    if (propInfo.GetValue(item, null).Equals(key))
                        return i;
                }
            }
            return -1;
        }

        internal void AddRange(IEnumerable<T> list)
        {
            foreach (var item in list)
            {
#if DEBUG
                if (Contains(item))
                {

                }
#endif

                Add(item);
            }
            //throw new NotImplementedException();
        }

        

        public int Find(string property, object key)
        {
            // Check the properties for a property with the specified name.
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            PropertyDescriptor prop = properties.Find(property, true);

            // If there is not a match, return -1 otherwise pass search to
            // FindCore method.
            if (prop == null)
                return -1;
            else
                return FindCore(prop, key);
        }

        #endregion Searching

        #region Sorting
        //ArrayList sortedList;
        BuscadorBindingList<T> unsortedItems;
        bool isSortedValue;
        ListSortDirection sortDirectionValue;
        PropertyDescriptor sortPropertyValue;

        protected override bool SupportsSortingCore
        {
            get { return true; }
        }

        protected override bool IsSortedCore
        {
            get { return isSortedValue; }
        }

        protected override PropertyDescriptor SortPropertyCore
        {
            get { return sortPropertyValue; }
        }

        protected override ListSortDirection SortDirectionCore
        {
            get { return sortDirectionValue; }
        }


        public void ApplySort(string propertyName, ListSortDirection direction)
        {
            // Check the properties for a property with the specified name.
            PropertyDescriptor prop = TypeDescriptor.GetProperties(typeof(T))[propertyName];

            // If there is not a match, return -1 otherwise pass search to
            // FindCore method.
            if (prop == null)
                throw new ArgumentException(propertyName +
                    " is not a valid property for type:" + typeof(T).Name);
            else
                ApplySortCore(prop, direction);
        }

        protected override void ApplySortCore(PropertyDescriptor prop,
            ListSortDirection direction)
        {

            var sortedList = new BuscadorBindingList<T>();

            // Check to see if the property type we are sorting by implements
            // the IComparable interface.
            Type interfaceType = prop.PropertyType.GetInterface("IComparable");

            if (interfaceType != null)
            {
                RaiseListChangedEvents = false;

                try
                {
                    // If so, set the SortPropertyValue and SortDirectionValue.
                    sortPropertyValue = prop;
                    sortDirectionValue = direction;

                    unsortedItems = new BuscadorBindingList<T>();

                    if (sortPropertyValue != null)
                    {
                        sortedList.AddRange(direction == ListSortDirection.Descending ? 
                            this.OrderByDescending(m => prop.GetValue(m)) :
                            this.OrderBy(m => prop.GetValue(m)));
                        unsortedItems.AddRange(this);
//                        // Loop through each item, adding it the the sortedItems ArrayList.
//                        foreach (object item in Items)
//                        {
//                            unsortedItems.Add((T)item);

//#if DEBUG
//                            if (sortedList.Contains(prop.GetValue(item)))
//                            {

//                            }
//#endif

//                            sortedList.Add(prop.GetValue(item));
//                        }
                    }
                    //// Call Sort on the ArrayList.
                    //sortedList.Sort();
                    T temp;

                    //// Check the sort direction and then copy the sorted items
                    //// back into the list.
                    //if (direction == ListSortDirection.Descending)
                    //    sortedList.Reverse();

                    for (int i = 0; i < this.Count; i++)
                    {
                        int position = this.IndexOf(sortedList[i]);//Find(prop.Name, sortedList[i]);
                        if (position != i && position > 0)
                        {
                            temp = this[i];
                            this[i] = this[position];
                            this[position] = temp;
                        }
                    }

                    isSortedValue = true;
                }
                finally
                {
                    RaiseListChangedEvents = true;
                }
                // If the list does not have a filter applied, 
                // raise the ListChanged event so bound controls refresh their
                // values. Pass -1 for the index since this is a Reset.
                //if (string.IsNullOrEmpty(Filter))
                    OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
            }
            else
                // If the property type does not implement IComparable, let the user
                // know.
                throw new InvalidOperationException("Cannot sort by "
                    + prop.Name + ". This" + prop.PropertyType.ToString() +
                    " does not implement IComparable");
        }

        protected override void RemoveSortCore()
        {
            RaiseListChangedEvents = false;
            // Ensure the list has been sorted.
            if (unsortedItems != null && OriginalList.Count > 0)
            {
                try
                {
                    Clear();
                    if (Filter != null)
                    {
                        unsortedItems.Filter = this.Filter;
                        foreach (T item in unsortedItems)
                            Add(item);
                    }
                    else
                    {
                        foreach (T item in OriginalList)
                            Add(item);
                    }
                    isSortedValue = false;
                }
                finally
                {
                    RaiseListChangedEvents = true;
                }
                // Raise the list changed event, indicating a reset, and index
                // of -1.
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset,
                    -1));
            }
        }

        public void RemoveSort()
        {
            RemoveSortCore();
        }


        public override void EndNew(int itemIndex)
        {
            // Check to see if the item is added to the end of the list,
            // and if so, re-sort the list.
            if (IsSortedCore && itemIndex > 0
                && itemIndex == Count - 1)
            {
                ApplySortCore(sortPropertyValue,
                    sortDirectionValue);
                base.EndNew(itemIndex);
            }
        }

        #endregion Sorting

        #region AdvancedSorting
        public bool SupportsAdvancedSorting
        {
            get { return false; }
        }
        public ListSortDescriptionCollection SortDescriptions
        {
            get { return null; }
        }

        public void ApplySort(ListSortDescriptionCollection sorts)
        {
            throw new NotSupportedException();
        }

        #endregion AdvancedSorting

        #region Filtering

        public bool SupportsFiltering
        {
            get { return true; }
        }

        public void RemoveFilter()
        {
            if (Filter != null) Filter = null;
        }

        private string filterValue = null;

        public string Filter
        {
            get
            {
                return filterValue;
            }
            set
            {
                if (filterValue == value) return;

                //Turn off list-changed events.
                RaiseListChangedEvents = false;
                try
                {
                    // If the value is null or empty, reset list.
                    if (string.IsNullOrEmpty(value))
                        ResetList();
                    else
                    {
                        if (!string.IsNullOrEmpty(filterValue) && !value.Contains(filterValue))
                            ResetList();

                        ApplyFilter(value);

                        //int count = 0;
                        //string[] matches = value.Split(new string[] { " AND " },
                        //    StringSplitOptions.RemoveEmptyEntries);

                        //while (count < matches.Length)
                        //{
                        //    string filterPart = matches[count].ToString();

                        //    // Check to see if the filter was set previously.
                        //    // Also, check if current filter is a subset of 
                        //    // the previous filter.
                        //    if (!String.IsNullOrEmpty(filterValue)
                        //            && !value.Contains(filterValue))
                        //        ResetList();

                        //    // Parse and apply the filter.
                        //    SingleFilterInfo filterInfo = ParseFilter(filterPart);
                        //    ApplyFilter(filterInfo);
                        //    count++;
                        //}
                    }
                    // Set the filter value and turn on list changed events.
                    filterValue = value;
                }
                finally
                {
                    RaiseListChangedEvents = true;
                }
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
            }
        }




        private void ResetList()
        {
            ClearItems();
            foreach (T t in OriginalList)
                Items.Add(t);
            if (IsSortedCore)
                ApplySortCore(SortPropertyCore, SortDirectionCore);
        }


        protected override void OnListChanged(ListChangedEventArgs e)
        {
            // If the list is reset, check for a filter. If a filter 
            // is applied don't allow items to be added to the list.
            if (e.ListChangedType == ListChangedType.Reset)
            {
                if (Filter == null || Filter == "")
                    AllowNew = true;
                else
                    AllowNew = false;
            }
            // Add the new item to the original list.
            if (e.ListChangedType == ListChangedType.ItemAdded)
            {
                OriginalList.Add(this[e.NewIndex]);
                if (!string.IsNullOrEmpty(Filter))
                //if (Filter == null || Filter == "")
                {
                    string cachedFilter = this.Filter;
                    Filter = "";
                    Filter = cachedFilter;
                }
            }
            // Remove the new item from the original list.
            if (e.ListChangedType == ListChangedType.ItemDeleted)
                OriginalList.RemoveAt(e.NewIndex);

            base.OnListChanged(e);
        }


        internal void ApplyFilter(string filter)
        {
            List<T> results;

            //// Check to see if the property type we are filtering by implements
            //// the IComparable interface.
            //Type interfaceType =
            //    TypeDescriptor.GetProperties(typeof(T))[filterParts.PropName]
            //    .PropertyType.GetInterface("IComparable");

            //if (interfaceType == null)
            //    throw new InvalidOperationException("Filtered property" +
            //    " must implement IComparable.");

            results = this.Where(filter).ToList();

            //// Check each value and add to the results list.
            //foreach (T item in this)
            //{
            //    if (filterParts.PropDesc.GetValue(item) != null)
            //    {
            //        IComparable compareValue =
            //            filterParts.PropDesc.GetValue(item) as IComparable;
            //        int result =
            //            compareValue.CompareTo(filterParts.CompareValue);
            //        if (filterParts.OperatorValue ==
            //            FilterOperator.EqualTo && result == 0)
            //            results.Add(item);
            //        if (filterParts.OperatorValue ==
            //            FilterOperator.GreaterThan && result > 0)
            //            results.Add(item);
            //        if (filterParts.OperatorValue ==
            //            FilterOperator.LessThan && result < 0)
            //            results.Add(item);
            //    }
            //}
            this.ClearItems();

            this.AddRange(results);
            //foreach (T itemFound in results)
            //    this.Add(itemFound);
        }



        #endregion Filtering
    }
}
