﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SOTControladores.Utilidades
{
    public class ItemWrapper<T> : INotifyPropertyChanged //where T : class
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;

                NotifyPropertyChanged();
            }
        }
    }
}