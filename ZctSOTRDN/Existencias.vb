Public Class Existencias

    Public Sub New()

    End Sub

    Public Sub New(ByVal Cod_Art As String, ByVal Cod_Alm As String, ByVal Desc_Alm As String, ByVal Exist_Art As Double, ByVal CostoProm_Art As Decimal)
        _Cod_Art = Cod_Art
        _Cod_Alm = Cod_Alm
        _Desc_Alm = Desc_Alm
        _Exist_Art = Exist_Art
        _CostoProm_Art = CostoProm_Art
    End Sub
    Private _Cod_Art As String
    ''' <summary>
    ''' C�digo del art�culo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Codigo() As String
        Get
            Return _Cod_Art
        End Get
        Set(ByVal value As String)
            _Cod_Art = value
        End Set
    End Property

    Private _Cod_Alm As String
    ''' <summary>
    ''' Almacen
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>

    Public Property Codigo_Almacen() As String
        Get
            Return _Cod_Alm
        End Get
        Set(ByVal value As String)
            _Cod_Alm = value
        End Set
    End Property

    Private _Desc_Alm As String
    ''' <summary>
    ''' Descripci�n del almacen dado
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Almacen() As String
        Get
            Return _Desc_Alm
        End Get
        Set(ByVal value As String)
            _Desc_Alm = value
        End Set
    End Property
    Private _Exist_Art As Double
    ''' <summary>
    ''' Existencia en el almac�n
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Existencias() As Double
        Get
            Return _Exist_Art
        End Get
        Set(ByVal value As Double)
            _Exist_Art = value
        End Set
    End Property

    Private _CostoProm_Art As Decimal

    Public Property Costo_Promedio() As Decimal
        Get
            Return _CostoProm_Art
        End Get
        Set(ByVal value As Decimal)
            _CostoProm_Art = value
        End Set
    End Property

End Class
