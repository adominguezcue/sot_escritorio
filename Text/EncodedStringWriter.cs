﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Text
{
    public class EncodedStringWriter : StringWriter
    {
        private Encoding  _Encoding;
        public EncodedStringWriter(Encoding encoding){
           
            _Encoding = encoding;
        }

          public  override Encoding Encoding {
             get { return _Encoding;} 
         }
         
             
    }
}
