﻿Imports NUnit.Framework
Imports ZctSOT.ZctDataBase
Imports ZctSOT.ZctFunciones
<TestFixture()> _
Public Class GenericoLinea
    Public EnumGenerico As ZctSOT.Interfaces.IEnumGenericoEnum = New ZctSOT.Clases.Catalogos.listaLineas
    Private ProcesaCatalogos As New ZctSOT.DAO.ClProcesaCatalogos
    Public Sub ejecutar()
        Inicio()

    End Sub
    <NUnit.Framework.SetUp()> _
    Public Sub Inicio()
        Test01GetDatos()
    End Sub


    <Test()> _
    Public Sub Test01GetDatos()
        ProcesaCatalogos.CargaDatos(EnumGenerico)
        Assert.AreNotEqual(0, EnumGenerico.Count)
    End Sub

    <Test()> _
    Public Sub Test01SaveDatos()

        Dim elemento As New ZctSOT.Clases.Catalogos.ZctClCatLinea
        elemento.Codigo = 0
        elemento.Linea = "Nueva Linea"
        ProcesaCatalogos.ActualizarDatos(EnumGenerico)
        'ProcesaCatalogos.CargaDatos(EnumGenerico)
        'Realizar la busqueda con linq
        Assert.AreNotEqual(0, EnumGenerico.Count)
    End Sub


End Class
