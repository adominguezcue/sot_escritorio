﻿Imports NUnit.Framework
<TestFixture()> _
Public Class Ordenes
    Private _Rdn As New ZctSOT.RDN.Compras.ZctOCSRDN

    Public Sub ejecutar()
        Inicio()

    End Sub
    <NUnit.Framework.SetUp()> _
    Public Sub Inicio()

    End Sub
    <Test()> _
    Public Sub Test01GetConfig()
        Dim Cfg As New ZctSOT.Clases.Compras.ZctConfOCS
        Cfg.Cod_Alm = 2
        Cfg.DiasHabiles = 26
        Cfg.LT = 12
        Cfg.OC = 12
        Cfg.SS = 12
        _Rdn.SetConfig(Cfg)
        Dim Cfg2 As ZctSOT.Clases.Compras.ZctConfOCS = _Rdn.GetConfig(Cfg.Cod_Alm)
        Assert.AreEqual (12, Cfg.SS)
        Assert.AreEqual(12, Cfg.OC)
        Assert.AreEqual(12, Cfg.LT)
        Assert.AreEqual(26, Cfg.DiasHabiles)
    End Sub
End Class
