﻿Imports NUnit.Framework
<TestFixture()> _
Public Class ZctTestOCS
    Private _Rdn As New ZctSOT.RDN.Compras.ZctOCSRDN

    Public Sub ejecutar()
        Inicio()
        
    End Sub
    <NUnit.Framework.SetUp()> _
    Public Sub Inicio()
        Test01GetLista()
    End Sub
    <Test()> _
    Public Sub Test01GetLista()
        '02-29-076211
        _Rdn.FechaIni = DateValue("14/09/2009")
        _Rdn.FechaFin = DateValue("14/09/2009")
        _Rdn.Cod_alm = 2
        _Rdn.GetLista()
        Dim elemento As ZctSOT.Clases.Compras.ZctArtDet = _Rdn.GetElemento("02-29-076211")
        Assert.AreEqual(4, elemento.Mes3)
    End Sub



End Class
