﻿Imports NUnit.Framework
Imports ZctSOT.NotasCredito
Imports System.Data

<TestFixture()> _
Public Class TestNotasCredito

    <Test()> _
    Public Sub Test01_PuedeObtenerElNombre()

        Dim ctrl As New Controler()

        Assert.AreEqual(ctrl.ObtenerNomCliente(0), "AMERICAS")
    End Sub

    Public Sub Test02_articulos()

        Dim ctrl As New Controler()

        Dim tabla As DataTable = ctrl.obtenerArticulos(1230, 0)

        Dim desc1 As String = "CABLE CLUTCH HONDA  DIM."

        Dim desc2 As String = tabla.Rows(0).Item("Desc_Art")

        Assert.AreEqual(desc1, desc2)




    End Sub

End Class
