﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Dependencias
{
    public class FabricaDependencias
    {
        private readonly IUnityContainer Contenedor;

        private static FabricaDependencias _instancia;

        private FabricaDependencias() 
        {
            Contenedor = new UnityContainer().LoadConfiguration("Contenedor");
        }

        public T Resolver<T>() 
        {
            return Contenedor.Resolve<T>();
        }

        public static FabricaDependencias Instancia 
        {
            get { return _instancia ?? (_instancia = new FabricaDependencias()); }
        }
    }
}
