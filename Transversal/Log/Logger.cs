using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Log
{
    public class Logger
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public static void Debug(string mensaje) 
        {
            logger.Debug(mensaje);
        }
        public static void Info(string mensaje, params string [] args)
        {
            logger.Info(mensaje, args);
        }
        public static void Error(string mensaje)
        {
            logger.Error(mensaje);
        }
        public static void Fatal(string mensaje)
        {
            logger.Fatal(mensaje);
        }
    }
}