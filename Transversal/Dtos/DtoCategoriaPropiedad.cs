﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Dtos
{
    public class DtoCategoriaPropiedad
    {
        public string Categoria { get; set; }

        public ObservableCollection<DtoPropiedad> Propiedades { get; set; }
    }
}
