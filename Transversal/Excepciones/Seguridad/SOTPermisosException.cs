﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Excepciones.Seguridad
{
    public class SOTPermisosException : SOTException
    {
        public SOTPermisosException()
        {
        }

        public SOTPermisosException(string message, params string[] args)
            : base(message, args)
        {
        }

        public SOTPermisosException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected SOTPermisosException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        private List<string> _permisos = new List<string>();

        public List<string> Permisos 
        {
            get { return _permisos; }
            set { _permisos = value ?? new List<string>(); }
        }
    }
}
