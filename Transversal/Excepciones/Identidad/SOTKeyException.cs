﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Excepciones.Identidad
{
    public class SOTKeyException : SOTException
    {
        public SOTKeyException()
        {
        }

        public SOTKeyException(string keyName, string message, params string[] args)
            : base(message, args)
        {
            KeyName = keyName;
        }

        public SOTKeyException(string keyName, string message, Exception inner)
            : base(message, inner)
        {
            KeyName = keyName;
        }

        protected SOTKeyException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public string KeyName { get; private set; }
    }
}
