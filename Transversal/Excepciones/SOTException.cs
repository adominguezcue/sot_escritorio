﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Excepciones
{
    public class SOTException : Exception
    {
        public SOTException()
        {
        }

        public SOTException(string message, params string[] args)
            : base(string.Format(message, args))
        {
        }

        public SOTException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected SOTException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
