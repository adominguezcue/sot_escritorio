﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Excepciones.Seguridad
{
    public class SOTAcumulativoException : SOTException
    {
        public SOTAcumulativoException()
        {
        }

        public SOTAcumulativoException(string message, params string[] args)
            : base(message, args)
        {
        }

        public SOTAcumulativoException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected SOTAcumulativoException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        private List<string> acumulativo = new List<string>();

        public void AgregarDetalle(string detalle)
        {
            if (acumulativo == null)
                acumulativo = new List<string>();

            acumulativo.Add(detalle);
        }

        public IEnumerable<string> Iterar()
        {
            foreach (var item in acumulativo)
                yield return item;
        }
    }
}
