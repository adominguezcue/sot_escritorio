﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Transversal.Utilidades
{
    public class UtilidadesRegex
    {
        static readonly System.Text.RegularExpressions.Regex REGEX_SOLO_LETRAS
            = new System.Text.RegularExpressions.Regex("^[a-zA-ZÑñÁ-Úá-úä-üÄ-Ü]+( [a-zA-ZÑñÁ-Úá-úä-üÄ-Ü]+)*$");

        static readonly System.Text.RegularExpressions.Regex REGEX_SOLO_DIGITOS
            = new System.Text.RegularExpressions.Regex("^[0-9]+$");

#warning Expresión regular sacada de los esquemas del SAT el día 22 de agosto de 2017, si más adelante cambia, favor de actualizar
        static readonly System.Text.RegularExpressions.Regex REGEX_RFC
            = new System.Text.RegularExpressions.Regex("^[A-ZÑ&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z0-9]?[A-Z0-9]?[0-9A-Z]?$");

#warning confirmar el formato de teléfono
        static readonly System.Text.RegularExpressions.Regex REGEX_TELEFONO
            = new System.Text.RegularExpressions.Regex("^[0-9]{10,13}$");

        static readonly System.Text.RegularExpressions.Regex REGEX_LETRAS_Y_NUMEROS
            = new System.Text.RegularExpressions.Regex("^([a-zA-ZÑñÁ-Úá-úä-üÄ-Ü]*[0-9]+)+$");

        static readonly System.Text.RegularExpressions.Regex REGEX_LETRAS_O_NUMEROS
            = new System.Text.RegularExpressions.Regex("^([a-zA-ZÑñÁ-Úá-úä-üÄ-Ü0-9]+[ ]*)+$");

        static readonly System.Text.RegularExpressions.Regex REGEX_EMAIL
            = new System.Text.RegularExpressions.Regex(@"^[a-zA-Z0-9\.]+@[a-zA-Z]+([.]{1}[a-zA-Z]+){1,2}$");//"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

        static readonly System.Text.RegularExpressions.Regex REGEX_TEXTO_O_NUMEROS_CON_PUNTUACION
            = new System.Text.RegularExpressions.Regex(@"^[a-zA-ZÑñÁ-Úá-úä-üÄ-Ü0-9\s\p{P}]+$");

        static readonly System.Text.RegularExpressions.Regex REGEX_TEXTO_CON_PUNTUACION
            = new System.Text.RegularExpressions.Regex(@"^[a-zA-ZÑñÁ-Úá-úä-üÄ-Ü\s\p{P}]+$");

        public static bool SoloLetras(string cadena)
        {
            return cadena != null && REGEX_SOLO_LETRAS.IsMatch(cadena);
        }

        public static bool SoloDigitos(string cadena)
        {
            return cadena != null && REGEX_SOLO_DIGITOS.IsMatch(cadena);
        }

        public static bool Telefono(string cadena)
        {
            return cadena != null && REGEX_TELEFONO.IsMatch(cadena);
        }

        public static bool LetrasYNumeros(string cadena)
        {
            return cadena != null && REGEX_LETRAS_Y_NUMEROS.IsMatch(cadena);
        }

        public static bool LetrasONumeros(string cadena)
        {
            return cadena != null && REGEX_LETRAS_O_NUMEROS.IsMatch(cadena);
        }

        public static bool TextoONumerosConPuntuacion(string cadena)
        {
            return cadena != null && REGEX_TEXTO_O_NUMEROS_CON_PUNTUACION.IsMatch(cadena);
        }

        public static bool TextoConPuntuacion(string cadena)
        {
            return cadena != null && REGEX_TEXTO_CON_PUNTUACION.IsMatch(cadena);
        }

        public static bool Rfc(string cadena) 
        {
            return cadena != null && REGEX_RFC.IsMatch(cadena);
        }

        public static bool Email(string cadena)
        {
            //return cadena != null && REGEX_EMAIL.IsMatch(cadena);

#warning parche temporal
            if (cadena == null)
                return false;

            try
            {
                new System.Net.Mail.MailAddress(cadena);
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }

//        public static bool Contrasena(string password)
//        {
//#warning definil el regex para las contraseñas. La contraseña debe tener por lo menos una letra en Mayúscula, un número y un carácter.

//            bool hasUpper = false;
//            bool hasNumber = false;
//            bool hasCharacter = false;

//            Regex upperLettersRegex = new Regex("[A-ZÑÁ-ÚÄ-Ü]");
//            Regex numbers = new Regex("[0-9]");
//            Regex characters = new Regex("[^(a-zA-ZÑñÁ-Úá-úä-üÄ-Ü0-9 )]");

//            foreach (var letter in password)
//            {
//                if (upperLettersRegex.IsMatch(letter.ToString()))
//                    hasUpper = true;
//                if (numbers.IsMatch(letter.ToString()))
//                    hasNumber = true;
//                if (characters.IsMatch(letter.ToString()))
//                    hasCharacter = true;

//                if (hasUpper && hasNumber && hasCharacter)
//                    return true;
//            }

//            return false;
//        }

        public static bool Contrasena(string password)
        {
#warning definil el regex para las contraseñas. La contraseña debe tener por lo menos una letra en Mayúscula, un número y un carácter.

            if (password == null)
                return false;

            bool hasUpper = false;
            bool hasNumber = false;
            bool hasCharacter = false;

            Regex upperLettersRegex = new Regex("[A-ZÑÁ-ÚÄ-Ü]");
            Regex numbers = new Regex("[0-9]");
            Regex characters = new Regex("[^(a-zA-ZÑñÁ-Úá-úä-üÄ-Ü0-9 )]");

            foreach (var letter in password)
            {
                if (upperLettersRegex.IsMatch(letter.ToString()))
                    hasUpper = true;
                if (numbers.IsMatch(letter.ToString()))
                    hasNumber = true;
                if (characters.IsMatch(letter.ToString()))
                    hasCharacter = true;

                if (hasUpper && hasNumber && hasCharacter)
                    return true;
            }

            return false;
        }
    }
}
