﻿using Transversal.Excepciones;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Transversal.Utilidades
{
    public class ExcelData
    {
        private static readonly string[] SUFIX_MATRIX = new string[]
        {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", 
            "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
        };

        private Stream localStream;
        public ExcelData(Stream stream)
        {
            this.localStream = stream;
        }

        private static string DecimalToSufix(int number) 
        {
            if (number < 0)
                return string.Empty;

            if (number < SUFIX_MATRIX.Length)
                return SUFIX_MATRIX[number];

            int factor = SUFIX_MATRIX.Length;

            List<string> indexes = new List<string>();

            int trash;

            do
            {
                trash = number % factor;
                indexes.Add(SUFIX_MATRIX[trash]);

                number = number / factor;

            } while (number > 1);

            if (number > 0)
                indexes.Add(SUFIX_MATRIX[number]);

            return string.Concat(indexes.Reverse<string>());
        }

        public static byte[] GenerateExcelBytesFromDataTable(List<DtoWorkSheet> sheets, string reporType)
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                int sheetCounter = 1;

                foreach (var sheet in sheets)
                {
                    int headerRowInit = 1;
                    int headerColumnInit = 2;

                    string sheetName = string.IsNullOrEmpty(sheet.SheetName) ?
                                       (sheets.Count == 1 ? reporType : "Hoja " + sheetCounter) :
                                       sheet.SheetName;

                    Regex ptr = new Regex(@"[:\/\\\?\*\[\]]");

                    sheetName = ptr.Replace(sheetName, "_");

                    var tmpSheetName = sheetName;

                    if (tmpSheetName.Length >= 31)
                        throw new SOTException("El nombre {0} excede la longitud soportada", tmpSheetName);

                    int counter = 1;

                    while (package.Workbook.Worksheets.Any(m => m.Name.ToUpper() == tmpSheetName.ToUpper()))
                    {
                        tmpSheetName = sheetName + " (" + DecimalToSufix(counter++) + ") ";

                        if (tmpSheetName.Length >= 31)
                            throw new SOTException("El nombre {0} excede la longitud soportada", tmpSheetName);
                    }
                    sheetName = tmpSheetName;

                    ExcelWorksheet excelSheet = package.Workbook.Worksheets.Add(sheetName);

                    excelSheet.PrinterSettings.FitToPage = sheet.FitToPage;
                    excelSheet.PrinterSettings.PaperSize = sheet.PaperSize;
                    excelSheet.PrinterSettings.Orientation = sheet.Orientation;

                    int columnCount = Math.Max(sheet.DtoSheetSections.SelectMany(m=>m.DtoSectionRows).Max(m => m.ColumnCount + m.Spaces), 2);

                    var lastColumn = headerColumnInit + columnCount - 1;

                    var title = "REPORTE DE " + reporType.ToUpper();
                    var titleSize = 24;

                    var minimumWidth = title.Length * 1.5;

                    using (ExcelRange excelTitleRange = excelSheet.Cells[headerRowInit, headerColumnInit, headerRowInit, lastColumn])
                    {
                        excelTitleRange.Merge = true;
                        excelTitleRange.Style.Font.Size = titleSize;
                        excelTitleRange.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        excelTitleRange.Value = title;
                        //excelTitleRange.AutoFitColumns();
                    }

                    #region logo

                    if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("reportsLogoUrl"))
                    {
                        string urlLogo = System.Configuration.ConfigurationManager.AppSettings["reportsLogoUrl"];

                        if (File.Exists(urlLogo))
                        {
                            try
                            {
                                using (Image logo = Bitmap.FromFile(urlLogo))
                                {
                                    var picture = excelSheet.Drawings.AddPicture("logo", logo);

                                    picture.SetSize(21);

                                    picture.SetPosition(headerRowInit - 1, 0, lastColumn, 0);


                                }
                            }
                            catch
                            {

                            }
                        }
                    }

                    #endregion

                    int rowPos = headerRowInit + 1;

                    foreach (var section in sheet.DtoSheetSections)
                        rowPos = LoadSection(excelSheet, section, rowPos, headerColumnInit, lastColumn);

                    using (ExcelRange excelTitleRange = excelSheet.Cells[headerRowInit, headerColumnInit, rowPos, lastColumn])
                    {
                        excelTitleRange.AutoFitColumns(minimumWidth);
                    }

                    sheetCounter++;
                }

                return package.GetAsByteArray();
            }
        }

        private static int LoadSection(ExcelWorksheet excelSheet, DtoSheetSection section, int headerRow, int headerColumn, int lastColumn)
        {
            if (!string.IsNullOrEmpty(section.Title))
            {
                headerRow++;

                using (ExcelRange excelTitleRange = excelSheet.Cells[headerRow, headerColumn, headerRow, lastColumn])
                {
                    excelTitleRange.Merge = true;
                    excelTitleRange.Style.Font.Size = 18;
                    excelTitleRange.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    excelTitleRange.Value = section.Title;
                    excelTitleRange.AutoFitColumns();
                }
            }

            foreach (var row in section.DtoSectionRows)
                headerRow = LoadRow(excelSheet, row, ++headerRow, headerColumn);

            return headerRow;
        }

        public static void ReadExcel(Stream fileStream, ExcelDataRequest request)
        {
            try
            {
                using (ExcelPackage package = new ExcelPackage(fileStream))
                {
                    var keys = request.NamedValues.Keys.ToList();

                    foreach (var key in keys)
                    {
                        if (package.Workbook.Names.ContainsKey(key))
                            request.NamedValues[key] = package.Workbook.Names[key].Value;
                    }

                    if (string.IsNullOrEmpty(request.DataSheetName))
                        throw new SOTException("No se ha proporcionado el nombre de la hoja");

                    ExcelWorksheet programSheet = package.Workbook.Worksheets[request.DataSheetName];

                    if (programSheet == null)
                        throw new SOTException("Este documento no contiene una hoja llamada " + request.DataSheetName);

                    if (string.IsNullOrEmpty(request.TableName))
                        throw new SOTException("No se ha proporcionado el nombre de la tabla");

                    var table = programSheet.Tables[request.TableName];

                    if (table == null)
                        throw new SOTException("Este documento no contiene la tabla " + request.TableName);
                    //DataTable tableData = new DataTable();

                    foreach (var column in table.Columns)
                    {
                        request.Data.Columns.Add(column.Name);
                    }

                    using (var values = programSheet.Cells[table.Address.Address])
                    {
                        object[,] objectValues = (object[,])values.Value;

                        var length = objectValues.GetLength(1);

                        for (int i = 1; i < objectValues.GetLength(0); i++)
                        {
                            object[] data = new object[length];

                            for (int j = 0; j < data.Length; j++)
                                data[j] = objectValues[i, j];

                            request.Data.Rows.Add(data);
                        }
                    }
                }
            }
            catch (System.IO.InvalidDataException) 
            {
                throw new SOTException("El archivo seleccionado no es un documento de excel o está dañado");
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                throw new SOTException("El archivo seleccionado no es un documento de excel o está dañado");
            }
        }

        public static DataSet ReadExcel(Stream fileStream)
        {
            DataSet result = new DataSet();

            try
            {
                using (ExcelPackage package = new ExcelPackage(fileStream))
                {

                    foreach (ExcelWorksheet programSheet in package.Workbook.Worksheets)
                    {

                        foreach (var table in programSheet.Tables)
                        {
                            DataTable dataT = new DataTable(table.Name);

                            foreach (var column in table.Columns)
                            {
                                dataT.Columns.Add(column.Name);
                            }

                            using (var values = programSheet.Cells[table.Address.Address])
                            {
                                object[,] objectValues = (object[,])values.Value;

                                var length = objectValues.GetLength(1);

                                for (int i = 1; i < objectValues.GetLength(0); i++)
                                {
                                    object[] data = new object[length];

                                    for (int j = 0; j < data.Length; j++)
                                        data[j] = objectValues[i, j];

                                    dataT.Rows.Add(data);
                                }
                            }

                            result.Tables.Add(dataT);
                        }
                    }
                }
            }
            catch (System.IO.InvalidDataException)
            {
                throw new SOTException("El archivo seleccionado no es un documento de excel o está dañado");
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                throw new SOTException("El archivo seleccionado no es un documento de excel o está dañado");
            }

            return result;
        }
        //public DataSet GetData(bool isFirstRowAsColumnNames)
        //{
        //    return GetExcelDataReader(isFirstRowAsColumnNames).AsDataSet();
        //}

        private static void FormattingExcelCells(ExcelRange range, string HTMLcolorCode, System.Drawing.Color fontColor, bool isFontBold)
        {
            range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml(HTMLcolorCode));
            range.Style.Font.Color.SetColor(fontColor);

            if (isFontBold)
                range.Style.Font.Bold = isFontBold;
        }

        private static int LoadRow(ExcelWorksheet excelSheet, DtoSheetSectionRow sectionRow, int headerRowInit, int headerColumn)
        {
            int finalRow = headerRowInit;

            foreach (var table in sectionRow.DtoDataTables)
            {
                var location = LoadData(excelSheet, table, headerRowInit, headerColumn);
                finalRow = Math.Max(finalRow, location.Y + 1);
                headerColumn = Math.Max(headerColumn, location.X + 1) + 1;
            }

            return finalRow;
        }

        private static Point LoadData(ExcelWorksheet excelSheet, DtoDataTable table, int headerRowInit, int headerColumn)
        {
            int columnCount = headerColumn;

            #region header

            int tableRowNamesIndex = headerRowInit;

            if (table.Headers != null && table.Headers.Keys.Count > 0)
            {
                columnCount++;

                foreach (var key in table.Headers.Keys)
                {
                    excelSheet.Cells[tableRowNamesIndex, headerColumn].Value = key;
                    excelSheet.Cells[tableRowNamesIndex++, columnCount].Value = table.Headers[key];
                }

                using (var range = excelSheet.Cells[headerRowInit, headerColumn, tableRowNamesIndex - 1, columnCount])
                {
                    range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                }

                using (var range = excelSheet.Cells[headerRowInit, headerColumn, tableRowNamesIndex - 1, headerColumn])
                {
                    range.Style.Font.Bold = true;
                }

                //excelSheet.Cells[headerRowInit, headerColumn + 1, tableRowNamesIndex - 1, headerColumn + 1].Style.HorizontalAlignment
                //    = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }

            #endregion

            // loop through each row and add values to our sheet
            int rowCount = tableRowNamesIndex;

            if (table.Data != null && table.Data.Columns.Count > 0)
            {
                tableRowNamesIndex++;

                //excelSheet.Cells[headersRow, 1].LoadFromDataTable(dataTable, true);

                var lastColumn = headerColumn + table.Data.Columns.Count - 1;

                if (!string.IsNullOrWhiteSpace(table.TableTitle))
                {
                    using (ExcelRange excelTitleRange = excelSheet.Cells[tableRowNamesIndex, headerColumn, tableRowNamesIndex, lastColumn])
                    {
                        excelTitleRange.Merge = true;
                        excelTitleRange.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        excelTitleRange.Value = table.TableTitle;

                        FormattingExcelCells(excelTitleRange, "#000000", System.Drawing.Color.White, true);
                    }

                    tableRowNamesIndex++;

                    if (!table.CombineTile)
                    {
                        ExcelRow excelBlankRow = excelSheet.Row(tableRowNamesIndex);
                        excelBlankRow.Height = 3;

                        tableRowNamesIndex++;
                    }
                }

                rowCount = tableRowNamesIndex;

                if (table.Data.Rows.Count > 0)
                {
                    excelSheet.Cells[tableRowNamesIndex, headerColumn].LoadFromDataTable(table.Data, !table.HideColumns);

                    if (table.Hyperlinks.Count > 0) 
                    {
                        int extra = table.HideColumns ? 0 : 1;

                        foreach (var hyperlink in table.Hyperlinks) 
                        {
                            excelSheet.Cells[tableRowNamesIndex + hyperlink.Row + extra, headerColumn + hyperlink.Column].Hyperlink = hyperlink.Uri;
                        }
                    }
                }
                else if (!table.HideColumns)
                    for (int i = 0; i < table.Data.Columns.Count; i++)
                    {
                        excelSheet.Cells[tableRowNamesIndex, i + headerColumn].Value = table.Data.Columns[i].ColumnName;
                        //excelSheet.Cells.Style.Font.Color.SetColor(System.Drawing.Color.Black);
                    }

                if (table.HideColumns)
                    rowCount--;

                rowCount += table.Data.Rows.Count;

                if (rowCount >= tableRowNamesIndex)
                {
                    using (ExcelRange excelCellrange = excelSheet.Cells[tableRowNamesIndex, headerColumn, rowCount, lastColumn])
                    {
                        excelCellrange.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                    }

                    for (int i = headerColumn; i <= lastColumn; i++)
                    {
                        excelSheet.Column(i).Style.WrapText = true;
                    }

                    if (!table.HideColumns)
                        using (ExcelRange excelCellrange = excelSheet.Cells[tableRowNamesIndex, headerColumn, tableRowNamesIndex, lastColumn])
                            FormattingExcelCells(excelCellrange, "#000000", System.Drawing.Color.White, true);
                }
                //excelCellrange = excelSheet.Cells[1, 1, 1, dataTable.Columns.Count];
                //FormattingExcelCells(excelCellrange, "#FFFFFF", System.Drawing.Color.Black, true);

                //rowCount++;

                columnCount = Math.Max(columnCount, lastColumn);
            }

            return new Point(columnCount, rowCount);
        }

        //private IExcelDataReader GetExcelDataReader(bool isFirstRowAsColumnNames)
        //{
        //    using (var fileStream = localStream)
        //    {
        //        IExcelDataReader dataReader;

        //        dataReader = ExcelReaderFactory.CreateOpenXmlReader(fileStream);

        //        dataReader.IsFirstRowAsColumnNames = isFirstRowAsColumnNames;

        //        return dataReader;
        //    }

        //}

        public class DtoWorkSheet 
        {
            public DtoWorkSheet() 
            {
                SheetName = "";
                DtoSheetSections = new List<DtoSheetSection>();
                FitToPage = false;
                PaperSize = ePaperSize.Letter;
                Orientation = eOrientation.Portrait;
            }
            public List<DtoSheetSection> DtoSheetSections { get; set; }
            public string SheetName { get; set; }

            public bool FitToPage { get; set; }

            public ePaperSize PaperSize { get; set; }

            public eOrientation Orientation { get; set; }
        }

        public class DtoSheetSection 
        {
            public DtoSheetSection() 
            {
                Title = "";
                DtoSectionRows = new List<DtoSheetSectionRow>();
            }

            public List<DtoSheetSectionRow> DtoSectionRows { get; set; }
            public string Title { get; set; }

            //public int ColumnCount
            //{
            //    get { return DtoSectionRows == null || DtoSectionRows.Count == 0 ? 0 : DtoSectionRows.Max(m => m.ColumnCount); }
            //}

            public void AddTableAsSectionRow(DtoDataTable tableData)
            {
                DtoSectionRows.Add(new DtoSheetSectionRow
                {
                    DtoDataTables = new List<DtoDataTable> { tableData }
                });
            }

            public void AddTablesAsSectionRows(List<DtoDataTable> tables)
            {
                foreach (var table in tables)
                    AddTableAsSectionRow(table);
            }
        }

        public class DtoSheetSectionRow
        {
            public DtoSheetSectionRow()
            {
                DtoDataTables = new List<DtoDataTable>();
            }

            public List<DtoDataTable> DtoDataTables { get; set; }

            public int ColumnCount
            {
                get { return DtoDataTables == null || DtoDataTables.Count == 0 ? 0 : DtoDataTables.Sum(m => m.ColumnCount); }
            }

            public int Spaces
            {
                get { return DtoDataTables == null || DtoDataTables.Count == 0 ? 0 : DtoDataTables.Count - 1; }
            }
        }

        public class DtoDataTable
        {
            public DtoDataTable() 
            {
                Hyperlinks = new List<DtoHyperlink>();
                Data = new DataTable();
                Headers = new Dictionary<string, string>();
                TableTitle = string.Empty;
            }

            public List<DtoHyperlink> Hyperlinks { get; set; }
            public DataTable Data { get; set; }
            public Dictionary<string, string> Headers { get; set; }
            public string TableTitle { get; set; }

            public bool HideColumns { get; set; }

            public bool CombineTile { get; set; }

            public int ColumnCount 
            {
                get { return Data == null ? (Headers == null ? 0 : 2) : Math.Max((Headers == null ? 0 : 2), Data.Columns.Count); } 
            }
        }

        public class DtoHyperlink 
        {
            public int Column { get; set; }
            public int Row { get; set; }
            public Uri Uri { get; set; }
        }

        public class ExcelDataRequest 
        {
            public ExcelDataRequest() 
            {
                NamedValues = new Dictionary<string, object>();
                Data = new DataTable();
                DataSheetName = string.Empty;
            }

            public DataTable Data { get; set; }
            public string DataSheetName { get; set; }
            public Dictionary<string, object> NamedValues { get; set; }
            public string TableName { get; set; }
        }
    }
}
