﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Utilidades
{
    public static class UtilidadesUrl
    {
        public static void ValidarUrl(string url, string metodo = System.Net.WebRequestMethods.Http.Head)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.AllowAutoRedirect = false;

            request.Method = metodo;

            using (var response = request.GetResponse()) { }
        }
    }
}
