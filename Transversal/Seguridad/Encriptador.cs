﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Seguridad
{
    public class Encriptador
    {
        public static byte[] Encriptar(string text)
        {
            byte[] textBytes = Encoding.UTF8.GetBytes(text);
            HMACSHA256 sha256 = new HMACSHA256(textBytes);

            //return Convert.ToBase64String(sha256.ComputeHash(textBytes));

            return sha256.ComputeHash(textBytes);
        }
        public static bool Comparar(string text, byte[] encryptedText)
        {
            //StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            var textx = Encriptar(text);
            //return comparer.Compare(textx, encryptedText) == 0;

            return textx.SequenceEqual(encryptedText);
        }
    }
}
