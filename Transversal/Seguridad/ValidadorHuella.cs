﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Seguridad
{
    public class GestorHuella
    {
        public static void ValidarIntegridad(byte[] plantillaBin)
        {
            try
            {
                DPFP.Template plantilla = new DPFP.Template();
                plantilla.DeSerialize(plantillaBin);
            }
            catch(Exception e) 
            {
                throw new InvalidOperationException("Los datos proporcionados no corresponden a una huella digital", e);
            }
        }
        public static bool Comparar(byte[] originalBin, byte[] muestraBin)
        {
            DPFP.Sample muestra = new DPFP.Sample();
            muestra.DeSerialize(muestraBin);

            DPFP.Template plantilla = new DPFP.Template();
            plantilla.DeSerialize(originalBin);

            DPFP.FeatureSet features = ExtraerCaracteristicas(muestra, DPFP.Processing.DataPurpose.Verification);

            if (features != null)
            {
                var Verificator = new DPFP.Verification.Verification();

                DPFP.Verification.Verification.Result result = new DPFP.Verification.Verification.Result();
                Verificator.Verify(features, plantilla, ref result);

                return result.Verified;
            }

            return false;
        }

        public static DPFP.FeatureSet ExtraerCaracteristicas(DPFP.Sample muestra, DPFP.Processing.DataPurpose proposito)
        {
            DPFP.Processing.FeatureExtraction Extractor = new DPFP.Processing.FeatureExtraction();	// Create a feature extractor
            DPFP.Capture.CaptureFeedback retroalimentacion = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet caracteristicas = new DPFP.FeatureSet();
            Extractor.CreateFeatureSet(muestra, proposito, ref retroalimentacion, ref caracteristicas);			// TODO: return features as a result?
            if (retroalimentacion == DPFP.Capture.CaptureFeedback.Good)
                return caracteristicas;
            else
                return null;
        }
    }
}
