﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;

public class ImpresoraSimple
{
    public class Linea 
    {
        public Linea(string texto) 
        {
            Repeticiones = 1;
            Texto = texto ?? "";
        }

        public int Repeticiones { get; set; }
        public string Texto { get; set; }
    }

    class Lineas 
    {
        int indice = 0;
        Linea[] lineas;

        internal Lineas(Linea[] lineas) 
        {
            this.lineas = lineas;
        }

        internal Linea LeerSiguiente() 
        {
            if (lineas != null && lineas.Length > indice)
                return lineas[indice++];

            return null;
        }
    }

    private Font printFont;
    //private StreamReader streamToPrint;
    //static string filePath;
    int indice = 0;

    Lineas lineas;

    private void pd_PrintPage(object sender, PrintPageEventArgs ev)
    {
        float linesPerPage = 0;
        float yPos = 0;
        int count = 0;
        float leftMargin = ev.MarginBounds.Left;
        float topMargin = ev.MarginBounds.Top;
        Linea line = null;

        // Calculate the number of lines per page.
        linesPerPage = ev.MarginBounds.Height /
           printFont.GetHeight(ev.Graphics);

        // Iterate over the file, printing each line.
        while (count < linesPerPage &&
           ((line = lineas.LeerSiguiente()) != null))
        {
            yPos = topMargin + (count * printFont.GetHeight(ev.Graphics));
            ev.Graphics.DrawString(line.Texto, printFont, Brushes.Black,
               leftMargin, yPos, new StringFormat());
            count++;
        }

        // If more lines exist, print another page.
        if (line != null)
            ev.HasMorePages = true;
        else
            ev.HasMorePages = false;
    }

    // Print the file.
    public void Imprimir(string nombreImpresora, params Linea[] lineas)
    {
        this.lineas = new Lineas(lineas);
        //streamToPrint = new StreamReader(filePath);
        //try
        //{
        printFont = new Font("Arial", 10);
        PrintDocument pd = new PrintDocument();
        pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);


        pd.PrinterSettings.PrinterName = nombreImpresora;

        if (pd.PrinterSettings.IsValid)
            pd.Print();
        //}
        //finally
        //{
        //    streamToPrint.Close();
        //}
    }
}