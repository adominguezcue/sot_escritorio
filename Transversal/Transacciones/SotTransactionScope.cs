﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Transversal.Transacciones
{
    public class SotTransactionScope : IDisposable
    {
        private TransactionScope transaccion;

        public SotTransactionScope()
        {
            transaccion = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot });
        }

        public void Complete()
        {
            if (transaccion != null)
                transaccion.Complete();
        }

        public void Dispose()
        {
            if (transaccion != null)
                transaccion.Dispose();
        }
    }
}
