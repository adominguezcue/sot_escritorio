﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dtos;

namespace Transversal.Extensiones
{
    public static class EnumExtensions
    {
        public static DtoEnum ComoDto<TEnum>(this TEnum item) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("Type must be an enum");

            return new DtoEnum()
                {
                    Valor = item,
                    Nombre = item.Descripcion(),
                };
        }

        public static List<DtoEnum> ComoListaDto<TEnum>() where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("Type must be an enum");

            return Enum.GetValues(typeof(TEnum))
                .Cast<Enum>()
                .ToList().ConvertAll(m => new DtoEnum()
                {
                    Valor = m,
                    Nombre = m.Descripcion(),
                });
        }

        public static List<TEnum> ComoLista<TEnum>()where TEnum : struct
        {
            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>().ToList();
        }
    }
}