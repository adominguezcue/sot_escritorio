﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Extensiones
{
    public static class TypeExtensions
    {
        public static Guid? ObtenerGuidEnsamblado(this Type tipo)
        {
            object[] attributes = tipo.Assembly.GetCustomAttributes(typeof(GuidAttribute), false);

            if (attributes.Length == 1)
            {
                return Guid.Parse(((GuidAttribute)attributes[0]).Value);
            }

            return null;
        }
    }
}
