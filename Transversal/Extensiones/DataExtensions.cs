﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Transversal.Extensiones
{
    public static class DataExtensions
    {
        public static DataTable ToDataTable<TEntity>(this List<TEntity> data)
            where TEntity : class
        {
            var properties = typeof(TEntity).GetProperties();
            DataTable table = new DataTable();

            Dictionary<string, string> propertiesNames = new Dictionary<string, string>();

            foreach (var prop in properties)
            {
                DisplayNameAttribute displayNameAttribute = null;

                foreach (var attr in prop.GetCustomAttributes(true))
                {
                    if (displayNameAttribute == null)
                        displayNameAttribute = attr as DisplayNameAttribute;
                }

                if (displayNameAttribute != null)
                    propertiesNames.Add(prop.Name, displayNameAttribute.DisplayName);
                else
                    propertiesNames.Add(prop.Name, prop.Name);

                Type columnType;

                if (typeof(System.Collections.IEnumerable).IsAssignableFrom(prop.PropertyType) && prop.PropertyType.GenericTypeArguments.Any())
                {
                    if (prop.PropertyType.GenericTypeArguments.Count() > 1)
                        throw new InvalidOperationException(string.Format("La propiedad {0} que implementa {1} solamente soporta un argumento de tipo genérico", prop.Name, typeof(System.Collections.IEnumerable).FullName));

                    columnType = prop.PropertyType.GenericTypeArguments[0];
                }
                else
                    columnType = prop.PropertyType;

                table.Columns.Add(propertiesNames[prop.Name], Nullable.GetUnderlyingType(columnType) ?? columnType);
            }

            foreach (TEntity item in data)
            {
                DataRow row = table.NewRow();

                foreach (var prop in properties.Where(m => propertiesNames.Keys.Contains(m.Name)))
                {
                    var value = prop.GetValue(item) ?? DBNull.Value;

                    if (typeof(System.Collections.IEnumerable).IsAssignableFrom(prop.PropertyType) && prop.PropertyType.GenericTypeArguments.Any() && value != DBNull.Value)
                    {
                        List<object> values = new List<object>();

                        foreach (var currentValue in (System.Collections.IEnumerable)value)
                        {
                            values.Add(currentValue);
                        }

                        value = string.Join(", ", values);
                    }

                    row[propertiesNames[prop.Name]] = value;
                }

                table.Rows.Add(row);
            }

            return table;
        }

        public static List<TEntity> ToList<TEntity>(this DataTable table, bool caseSensitive = false)
            where TEntity : class
        {
            var properties = typeof(TEntity).GetProperties();//BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

            Dictionary<string, string> propertiesNames = new Dictionary<string, string>();

            foreach (var prop in properties)
            {
                DisplayNameAttribute displayNameAttribute = null;
                foreach (var attr in prop.GetCustomAttributes(true))
                {
                    displayNameAttribute = attr as DisplayNameAttribute;
                    if (displayNameAttribute != null)
                        break;
                }

                if (displayNameAttribute != null)
                    propertiesNames.Add(prop.Name, displayNameAttribute.DisplayName);
                else
                    propertiesNames.Add(prop.Name, prop.Name);

                Type columnType;

                columnType = prop.PropertyType;

                if (caseSensitive)
                {
                    if (!table.Columns.Contains(propertiesNames[prop.Name]))
                        throw new InvalidOperationException("La tabla no contiene una columna llamada " + propertiesNames[prop.Name]);
                }
                else
                {
                    bool any = false;

                    foreach (DataColumn currentColumn in table.Columns)
                    {
                        if (currentColumn.ColumnName.ToUpper() == propertiesNames[prop.Name].ToUpper())
                        {
                            propertiesNames[prop.Name] = currentColumn.ColumnName.ToUpper();
                            any = true;
                            break;
                        }
                    }

                    if (!any)
                        throw new InvalidOperationException("La tabla no contiene una columna llamada " + propertiesNames[prop.Name]);
                }
            }

            var result = new List<TEntity>();

            foreach (DataRow row in table.Rows)
            {
                var item = (TEntity)Activator.CreateInstance(typeof(TEntity));

                foreach (var prop in properties)
                {
                    var value = row[propertiesNames[prop.Name]];

                    if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    {
                        if (value == DBNull.Value)
                        {
                            if (prop.PropertyType.IsValueType)
                            {
                                value = Activator.CreateInstance(prop.PropertyType);
                            }
                            value = null;
                        }
                        else
                        {
                            var targetType = Nullable.GetUnderlyingType(prop.PropertyType);

                            value = Convert.ChangeType(value, targetType);
                        }
                    }
                    else if (value == DBNull.Value)
                    {
                        if (prop.PropertyType.IsValueType)
                        {
                            value = Activator.CreateInstance(prop.PropertyType);
                        }
                        value = null;
                    }
                    else
                    {
                        value = Convert.ChangeType(value, prop.PropertyType);
                    }

                    prop.SetValue(item, value, null);
                }

                result.Add(item);
            }

            return result;
        }

        public static DataTable GetDataTable(Dictionary<string, Type> columns, object[][] values)
        {
            DataTable table = new DataTable();

            foreach (var key in columns.Keys)
            {
                Type columnType;

                if (typeof(System.Collections.IEnumerable).IsAssignableFrom(columns[key]) && columns[key].GenericTypeArguments.Any())
                {
                    if (columns[key].GenericTypeArguments.Count() > 1)
                        throw new InvalidOperationException(string.Format("La propiedad {0} que implementa {1} solamente soporta un argumento de tipo genérico", key, typeof(System.Collections.IEnumerable).FullName));

                    columnType = columns[key].GenericTypeArguments[0];
                }
                else
                    columnType = columns[key];

                table.Columns.Add(key, Nullable.GetUnderlyingType(columnType) ?? columnType);
            }

            foreach (object[] item in values)
            {
                DataRow row = table.NewRow();

                int i = 0;
                foreach (var key in columns.Keys)
                {
                    var value = item[i] ?? DBNull.Value;

                    if (typeof(System.Collections.IEnumerable).IsAssignableFrom(columns[key]) && columns[key].GenericTypeArguments.Any() && value != DBNull.Value)
                    {
                        List<object> propertyValues = new List<object>();

                        foreach (var currentValue in (System.Collections.IEnumerable)value)
                        {
                            propertyValues.Add(currentValue);
                        }

                        value = string.Join(", ", propertyValues);
                    }

                    row[key] = value;

                    i++;
                }

                table.Rows.Add(row);
            }

            return table;
        }
    }
}
