﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Transversal.Dtos;

namespace Transversal.Extensiones
{
    public static class ObjectExtensions
    {
        public static string Descripcion<T>(this T value)
        {
            if (value == null)
                return "";

            FieldInfo field = value.GetType().GetField(value.ToString());

            if (field == null)
                return "";

            DescriptionAttribute attribute
                = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
                    as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;

        }

        public static string GetDescripcion<TEntity, TProperty>(this TEntity item, Expression<Func<TEntity, TProperty>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
                throw new InvalidOperationException("Expression must be a member expression");

            var attr = memberExpression.Member.GetCustomAttribute<DescriptionAttribute>();

            if (attr != null)
                return attr.Description ?? memberExpression.Member.Name;

            return memberExpression.Member.Name;
        }

        public static void AsignarValorABooleanos<T>(this T item, bool valor) 
        {
            var propiedades = item.GetType().GetProperties().Where(m => m.PropertyType == typeof(bool)).ToList();

            foreach (var propiedad in propiedades)
                propiedad.SetValue(item, valor);
        }

        public static string NombrePropiedad<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> expresionPropiedad)
            where TEntity : class
        {
            MemberExpression memberExpression = (MemberExpression)expresionPropiedad.Body;

            return memberExpression.Member.Name;
        }

        public static string NombrePropiedad<TEntity, TProperty>(this TEntity item, Expression<Func<TEntity, TProperty>> expresion)
            where TEntity : class
        {
            if (expresion == null)
                throw new ArgumentNullException("Expresión de propiedad inválida");

            MemberExpression body = expresion.Body as MemberExpression;

            if (body != null && body.Member.DeclaringType.IsAssignableFrom(item.GetType()) && body.Expression.NodeType == ExpressionType.Parameter)
                return body.Member.Name;

            throw new ArgumentException("Expresión de propiedad inválida");
        }

        public static string ToJsonString<T>(this T myObject)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(myObject);
        }

        public static T FromJsonString<T>(this string myJsonObject)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(myJsonObject);
        }

        public static List<DtoCategoriaPropiedad> CategorizarPropiedades<T>(this T item)
        {
            var propiedades = item.GetType().GetProperties().Where(m => m.PropertyType == typeof(bool)).ToList();

            List<DtoCategoriaPropiedad> retorno = new List<DtoCategoriaPropiedad>();

            foreach (var propiedad in propiedades)
            {
                var atributos = propiedad.GetCustomAttributes(false);
                if (propiedad.Name.Equals("ConsultaClientesMatricula"))
                {
                }
                var catAtt = atributos.FirstOrDefault(m => m.GetType() == typeof(CategoryAttribute)) as CategoryAttribute;
                var descAtt = atributos.FirstOrDefault(m => m.GetType() == typeof(DescriptionAttribute)) as DescriptionAttribute;

                if (catAtt == null)
                    continue;

                if (!retorno.Any(m => m.Categoria == catAtt.Category))
                    retorno.Add(new DtoCategoriaPropiedad
                    {
                        Categoria = catAtt.Category,
                        Propiedades = new ObservableCollection<DtoPropiedad>()
                    });

                retorno.First(m => m.Categoria == catAtt.Category).Propiedades.Add(new DtoPropiedad
                {
                    Propiedad = propiedad,
                    Descripcion = descAtt != null ? descAtt.Description : propiedad.Name,
                    Valor = propiedad.GetValue(item)
                });
            }

            return retorno;
        }
    }
}
