﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOCompra : Controlador.IDAOModelo<compra>
    {
  #region Miembros de IDAOModelo<compra>
        private DAO.ReportesDataContext _dao;
        public DAOCompra(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public compra BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.CompraDataTable dt = new PuntoDeVentaDataSet.CompraDataTable();
            //PuntoDeVentaDataSetTableAdapters.CompraTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.CompraTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZctCompras Compra = (from m in _dao.VW_ZctCompras 
                                            where m.uuid.ToString() == busqueda && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.compra _Compra = new Modelo.compra();
                if (Compra != null && Compra.Cod_Art != "")
                {
                    //Console..("Si lo encontre");
                    _Compra = TransformaCompra(Compra);
                }
                return _Compra;
        }

        private compra TransformaCompra(DAO.VW_ZctCompras Compra)
        {
            
            compra _Compra = new compra
            {
                
                actualizado = false,
                anio = Compra.anio.GetValueOrDefault(),
                Cat_Alm = Compra.Cat_Alm.GetValueOrDefault(),
                 Cod_Art = Compra.Cod_Art,
                 Cod_OC = Compra.Cod_OC,
                Cos_Art = Compra.Cos_Art.GetValueOrDefault(),
                CtdStdDet_OC = Compra.CtdStdDet_OC.GetValueOrDefault(),
                Fch_OC = Compra.Fch_OC.GetValueOrDefault(),
                mes = Compra.mes.GetValueOrDefault(),
                 Nom_Prov = Compra.Nom_Prov,
                 status = Compra.status,
                 uid_web  = Compra.uuid

            };
            return _Compra;
        }

        public List<compra> ObtieneModelos()
        {
            var Compras = (from m in _dao.VW_ZctCompras where m.update_web == null || m.update_web == false select m).Take(5);
            List<compra> lista_Compras = new List<compra>();
            foreach (DAO.VW_ZctCompras Compra in Compras)
            {
                Modelo.compra _Compra = new Modelo.compra();
                if (Compra != null && Compra.Cod_Art != "")
                {
                    //Console.WriteLine("Si lo encontre");
                    _Compra = TransformaCompra(Compra);
                }
                lista_Compras.Add( _Compra);
            }

            return lista_Compras;
            
        }

        //public List<Compra> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.CompraDataTable dt = new PuntoDeVentaDataSet.CompraDataTable();
        //    PuntoDeVentaDataSetTableAdapters.CompraTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.CompraTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<Compra> GetModelosByDT(PuntoDeVentaDataSet.CompraDataTable dt)
        //{
        //    List<Modelo.Compra> _Compras = new List<Modelo.Compra>();
        //    foreach (PuntoDeVentaDataSet.CompraRow row in dt.Rows)
        //    {
        //        _Compras.Add(TransformaCompra(row));
        //    }
        //    return _Compras;
        //}

        #endregion

        #region Miembros de IDAOModelo<Compra>
          

        public List<compra> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<Compra> Members


        public void GrabaModelo(compra modelo)
        {
            DAO.ZctDetOC Compra =  (from Compras in _dao.ZctDetOC where Compras.uuid == modelo.uid_web select Compras).SingleOrDefault();
            Compra.uuid = modelo.uid_web;
            Compra.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
