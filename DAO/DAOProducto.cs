﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOProducto : Controlador.IDAOModelo<Product>
    {
  #region Miembros de IDAOModelo<Producto>
        private DAO.ReportesDataContext _dao;
        public DAOProducto(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public Product BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.ProductoDataTable dt = new PuntoDeVentaDataSet.ProductoDataTable();
            //PuntoDeVentaDataSetTableAdapters.ProductoTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.ProductoTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZCTProductos Producto = (from m in _dao.VW_ZCTProductos 
                                            where m.Cod_Art == busqueda && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.Product _Producto = new Modelo.Product();
                if (Producto != null && Producto.Cod_Art != "")
                {
                    //Console.WriteLine("Si lo encontre");
                    _Producto = TransformaProducto(Producto);
                }
                return _Producto;
        }

        private Product TransformaProducto(DAO.VW_ZCTProductos producto)
        {
            Product _producto = new Product()
            {
                
                actualizado = false,
                codigo = producto.Cod_Art,
                precio = producto.Prec_Art.GetValueOrDefault(),
                costo = producto.Cos_Art.GetValueOrDefault(),
                nombre = producto.Desc_Art,
                taller= producto.taller,
                desactivar = producto.Sincronizar ? producto.desactivar.GetValueOrDefault(): true,
                uid_web = producto.uuid.GetValueOrDefault()

            };
            return _producto;
        }

        public List<Product> ObtieneModelos()
        {
            var productos = (from m in _dao.VW_ZCTProductos where m.update_web == null || m.update_web == false orderby m.Sincronizar descending select m).Take(5);
            List<Product> lista_Productos = new List<Product>();
            foreach (DAO.VW_ZCTProductos producto in productos)
            {
                Modelo.Product _Producto = new Modelo.Product();
                if (producto != null && producto.Cod_Art != "")
                {
                    //Console.WriteLine("Si lo encontre");
                    _Producto = TransformaProducto(producto);
                }
                lista_Productos.Add( _Producto);
            }

            return lista_Productos;
            
        }

        //public List<Producto> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.ProductoDataTable dt = new PuntoDeVentaDataSet.ProductoDataTable();
        //    PuntoDeVentaDataSetTableAdapters.ProductoTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.ProductoTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<Producto> GetModelosByDT(PuntoDeVentaDataSet.ProductoDataTable dt)
        //{
        //    List<Modelo.Producto> _Productos = new List<Modelo.Producto>();
        //    foreach (PuntoDeVentaDataSet.ProductoRow row in dt.Rows)
        //    {
        //        _Productos.Add(TransformaProducto(row));
        //    }
        //    return _Productos;
        //}

        #endregion

        #region Miembros de IDAOModelo<Producto>
          

        public List<Product> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<Producto> Members


        public void GrabaModelo(Product modelo)
        {
            DAO.ZctCatArt Producto =  (from Productos in _dao.ZctCatArt where Productos.uuid == modelo.uid_web select Productos).SingleOrDefault();
            Producto.uuid = modelo.uid_web;
            Producto.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
