﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOBaseMoto : Controlador.IDAOModelo<base_moto>
    {
        #region Miembros de IDAOModelo<BaseMoto>
        private DAO.ReportesDataContext _dao;
        public DAOBaseMoto(string conn) {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public base_moto BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.BaseMotoDataTable dt = new PuntoDeVentaDataSet.BaseMotoDataTable();
            //PuntoDeVentaDataSetTableAdapters.BaseMotoTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.BaseMotoTableAdapter();
            
                //ta.FillById(dt, Convert.ToInt32(busqueda));
                DAO.VW_ZctBaseMotos moto = (from m in _dao.VW_ZctBaseMotos 
                                            where m.CODIGO == Convert.ToInt32(busqueda) && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.base_moto _BaseMoto = new Modelo.base_moto();
                if (moto != null && moto.CODIGO != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _BaseMoto = TransformaBaseMoto(moto);
                }
                return _BaseMoto;
            
            
        }

        private base_moto TransformaBaseMoto(DAO.VW_ZctBaseMotos moto)
        {
            
            base_moto _BaseMoto = new base_moto()
            {
                actualizado = false,
                anno_motor = moto.Anno_Mot.GetValueOrDefault(),
                asegurada = moto.asegurada.ToString(),
                ceco = moto.CECO,
                codigo = moto.CODIGO,
                eco = moto.ECO.ToString(),
                estatus = moto.ESTATUS,
                fecha_alta = moto.fecha_alta.GetValueOrDefault(),
                fecha_baja = moto.fecha_baja.GetValueOrDefault(),
                uid_web = moto.uuid,
                marca = moto.MARCA,
                motor = moto.MOTOR,
                no_taller = "1",
                placas = moto.Placas_Mot,
                region = moto.REGION,
                responsoble = moto.RESPONSABLE,
                serie = moto.SERIE,
                tienda = moto.TIENDA,
                tipo = moto.TIPO,
                zona = moto.Desc_Zona ,
                taller = moto.taller
            };
            return _BaseMoto;
        }

        //public void GrabaModelo(BaseMoto modelo)
        //{
        //    PuntoDeVentaDataSetTableAdapters.BaseMotoTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.BaseMotoTableAdapter();
        //    PuntoDeVentaDataSet.BaseMotoDataTable dt = new PuntoDeVentaDataSet.BaseMotoDataTable();
        //    //DAO.DAOMethodPayment _daoMethodPayment = new DAO.DAOMethodPayment();
        //    try
        //    {
        //        ta.FillById(dt, modelo.id);


        //        PuntoDeVentaDataSet.BaseMotoRow row;
        //        if (dt.Rows.Count == 0)
        //        {
        //            row = dt.NewBaseMotoRow();

        //            SetRow(modelo, row);
        //            dt.Rows.Add(row);
        //            ta.Update(dt);
        //        }
        //        else
        //        {
        //            row = dt[0];
        //            SetRow(modelo, row);
        //            ta.Update(row);
        //        }
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}

        //private static void SetRow(BaseMoto modelo, PuntoDeVentaDataSet.BaseMotoRow row)
        //{
        //    row.id = modelo.id;
        //    row.description = modelo.description;

        //}



        public List<base_moto> ObtieneModelos()
        {
            var motos = (from m in _dao.VW_ZctBaseMotos where m.update_web == null || m.update_web== false select m).Take(5);
            List<base_moto> lista_motos = new List<base_moto>();
            foreach (DAO.VW_ZctBaseMotos moto in motos)
            {
                Modelo.base_moto _BaseMoto = new Modelo.base_moto();
                if (moto != null && moto.CODIGO != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _BaseMoto = TransformaBaseMoto(moto);
                }
                lista_motos.Add( _BaseMoto);
            }

            return lista_motos;
            
        }

        //public List<BaseMoto> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.BaseMotoDataTable dt = new PuntoDeVentaDataSet.BaseMotoDataTable();
        //    PuntoDeVentaDataSetTableAdapters.BaseMotoTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.BaseMotoTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<BaseMoto> GetModelosByDT(PuntoDeVentaDataSet.BaseMotoDataTable dt)
        //{
        //    List<Modelo.BaseMoto> _BaseMotos = new List<Modelo.BaseMoto>();
        //    foreach (PuntoDeVentaDataSet.BaseMotoRow row in dt.Rows)
        //    {
        //        _BaseMotos.Add(TransformaBaseMoto(row));
        //    }
        //    return _BaseMotos;
        //}

        #endregion

        #region Miembros de IDAOModelo<BaseMoto>


        public List<base_moto> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<BaseMoto> Members


        public void GrabaModelo(base_moto modelo)
        {
            DAO.ZctCatMot moto =  (from motos in _dao.ZctCatMot where motos.Cod_Mot == modelo.codigo select motos).SingleOrDefault();
            moto.uuid = modelo.uid_web;
            moto.update_web = true;
            _dao.SubmitChanges();


        }

        #endregion
    }
}
