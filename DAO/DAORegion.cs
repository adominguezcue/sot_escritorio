﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAORegion : Controlador.IDAOModelo<region>
    {
        #region Miembros de IDAOModelo<Region>
        private DAO.ReportesDataContext _dao;
        public DAORegion(string conn) {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public region BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.RegionDataTable dt = new PuntoDeVentaDataSet.RegionDataTable();
            //PuntoDeVentaDataSetTableAdapters.RegionTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.RegionTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.ZctCatRegion region = (from m in _dao.ZctCatRegion 
                                            where m.Cod_Region == busqueda && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.region _region = new Modelo.region();
                if (region != null && region.Cod_Region != "")
                {
                    //Console.WriteLine("Si lo encontre");
                    _region = TransformaRegion(region);
                }
                return _region;
        }

        private region TransformaRegion(DAO.ZctCatRegion region)
        {
            
            region _Region = new region()
            {
                actualizado = false,
                name = region.Desc_Region,
                uid_web = region.uuid

            };
            return _Region;
        }

        //public void GrabaModelo(Region modelo)
        //{
        //    PuntoDeVentaDataSetTableAdapters.RegionTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.RegionTableAdapter();
        //    PuntoDeVentaDataSet.RegionDataTable dt = new PuntoDeVentaDataSet.RegionDataTable();
        //    //DAO.DAOMethodPayment _daoMethodPayment = new DAO.DAOMethodPayment();
        //    try
        //    {
        //        ta.FillById(dt, modelo.id);


        //        PuntoDeVentaDataSet.RegionRow row;
        //        if (dt.Rows.Count == 0)
        //        {
        //            row = dt.NewRegionRow();

        //            SetRow(modelo, row);
        //            dt.Rows.Add(row);
        //            ta.Update(dt);
        //        }
        //        else
        //        {
        //            row = dt[0];
        //            SetRow(modelo, row);
        //            ta.Update(row);
        //        }
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}

        //private static void SetRow(Region modelo, PuntoDeVentaDataSet.RegionRow row)
        //{
        //    row.id = modelo.id;
        //    row.description = modelo.description;

        //}



        public List<region> ObtieneModelos()
        {
            var regions = (from m in _dao.ZctCatRegion where m.update_web == null || m.update_web== false select m).Take(5);
            List<region> lista_regions = new List<region>();
            foreach (DAO.ZctCatRegion region in regions)
            {
                Modelo.region _Region = new Modelo.region();
                if (region != null && region.Cod_Region != "")
                {
                    //Console.WriteLine("Si lo encontre");
                    _Region = TransformaRegion(region);
                }
                lista_regions.Add( _Region);
            }

            return lista_regions;
            
        }

        //public List<Region> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.RegionDataTable dt = new PuntoDeVentaDataSet.RegionDataTable();
        //    PuntoDeVentaDataSetTableAdapters.RegionTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.RegionTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<Region> GetModelosByDT(PuntoDeVentaDataSet.RegionDataTable dt)
        //{
        //    List<Modelo.Region> _Regions = new List<Modelo.Region>();
        //    foreach (PuntoDeVentaDataSet.RegionRow row in dt.Rows)
        //    {
        //        _Regions.Add(TransformaRegion(row));
        //    }
        //    return _Regions;
        //}

        #endregion

        #region Miembros de IDAOModelo<Region>


        public List<region> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<Region> Members
        

        public void GrabaModelo(region modelo)
        {
            DAO.ZctCatRegion region =  (from regions in _dao.ZctCatRegion where regions.uuid == modelo.uid_web select regions).SingleOrDefault();
            region.uuid = modelo.uid_web;
            region.update_web = true;
            _dao.SubmitChanges();
            

        }

        #endregion
    }
}
