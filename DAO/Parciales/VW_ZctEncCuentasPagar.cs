﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public partial class VW_ZctEncCuentasPagar
    {

        public string Estado 
        {
            get 
            {
                switch (idEstado)
                { 
                    case 1:
                        return "PENDIENTE";
                    case 2:
                        return "PAGADO";
                    case 3:
                        return "CANCELADO";
                    default:
                        return "";
                }
            }
        }
        /*
         PENDIENTE = 1,
            PAGADO = 2,
            CANCELADO = 3
         */
    }
}
