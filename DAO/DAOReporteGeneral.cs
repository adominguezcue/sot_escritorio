﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;


namespace DAO
{
    public class DAOReporteGeneral : Controlador.IDAOModelo<ReporteGeneral>
    {
        #region Miembros de IDAOModelo<ReporteGeneral>
        private DAO.ReportesDataContext _dao;
        public DAOReporteGeneral(string conn) {

           _dao = new DAO.ReportesDataContext(conn);
        }




        public ReporteGeneral BuscaModelo(string busqueda)
        {

            
            //PuntoDeVentaDataSet.ReporteGeneralDataTable dt = new PuntoDeVentaDataSet.ReporteGeneralDataTable();
            //PuntoDeVentaDataSetTableAdapters.ReporteGeneralTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.ReporteGeneralTableAdapter();
                
                Guid busqueda_guid =  Guid.Parse(busqueda);

                //ta.FillById(dt, Convert.ToInt32(busqueda));
                ReporteGeneral reporte_general = (from m in _dao.ZctReporteGeneral
                                       from p in _dao.ZctCatPar
                                       where m.uid == Guid.Parse(busqueda) && m.update_web == false
                                       select new ReporteGeneral(){
                                           //actualizado = false,
                                           almacen = m.almacen,
                                           month = m.month.GetValueOrDefault(),
                                           year = m.year.GetValueOrDefault(),
                                           tipo = m.tipo,
                                           total = (double)m.total.GetValueOrDefault(),
                                           uid = m.uid,
                                           taller = p.taller

                                       }).SingleOrDefault(); //m.almacen, m.month , m.year, m.tipo, m.total, m.uid, p.taller
                                            
                                            

                //Modelo.ReporteGeneral _ReporteGeneral = new Modelo.ReporteGeneral();
                //if (reporte_general != null && reporte_general.uid != Guid.Empty)
                //{
                    //Console.WriteLine("Si lo encontre");
                //    _ReporteGeneral = TransformaReporteGeneral(reporte_general);
                //}
                return reporte_general;
            
            
        }

        private ReporteGeneral TransformaReporteGeneral(DAO.ZctReporteGeneral reporte_general)
        {
            
            ReporteGeneral _ReporteGeneral = new ReporteGeneral()
            {
                //actualizado = false,
                almacen = reporte_general.almacen,
                month = reporte_general.month.GetValueOrDefault(),
                year = reporte_general.year.GetValueOrDefault(),
                tipo = reporte_general.tipo,
                total = (double)reporte_general.total.GetValueOrDefault(),
                uid = reporte_general.uid
                

            };
            return _ReporteGeneral;
        }

        //public void GrabaModelo(ReporteGeneral modelo)
        //{
        //    PuntoDeVentaDataSetTableAdapters.ReporteGeneralTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.ReporteGeneralTableAdapter();
        //    PuntoDeVentaDataSet.ReporteGeneralDataTable dt = new PuntoDeVentaDataSet.ReporteGeneralDataTable();
        //    //DAO.DAOMethodPayment _daoMethodPayment = new DAO.DAOMethodPayment();
        //    try
        //    {
        //        ta.FillById(dt, modelo.id);


        //        PuntoDeVentaDataSet.ReporteGeneralRow row;
        //        if (dt.Rows.Count == 0)
        //        {
        //            row = dt.NewReporteGeneralRow();

        //            SetRow(modelo, row);
        //            dt.Rows.Add(row);
        //            ta.Update(dt);
        //        }
        //        else
        //        {
        //            row = dt[0];
        //            SetRow(modelo, row);
        //            ta.Update(row);
        //        }
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}

        //private static void SetRow(ReporteGeneral modelo, PuntoDeVentaDataSet.ReporteGeneralRow row)
        //{
        //    row.id = modelo.id;
        //    row.description = modelo.description;

        //}



        public List<ReporteGeneral> ObtieneModelos()
        {
            List<ReporteGeneral> lista_reportes  = (from m in _dao.ZctReporteGeneral 
                            from p in _dao.ZctCatPar
                            where m.update_web == null || m.update_web== false 
                             select new ReporteGeneral(){
                                           //actualizado = false,
                                           almacen = m.almacen,
                                           month = m.month.GetValueOrDefault(),
                                           year = m.year.GetValueOrDefault(),
                                           tipo = m.tipo,
                                           total = (double)m.total.GetValueOrDefault(),
                                           uid = m.uid,
                                           taller = p.taller
                                       }).Take(5).ToList();
            //List<ReporteGeneral> lista_reportes = new List<ReporteGeneral>();
            //foreach (DAO.ZctReporteGeneral reporte in reportes)
            //{
            //    Modelo.ReporteGeneral _ReporteGeneral = new Modelo.ReporteGeneral();
            //    if (reporte != null && reporte.uid != Guid.Empty)
            //    {
            //        //Console.WriteLine("Si lo encontre");
            //        _ReporteGeneral = TransformaReporteGeneral(reporte);
            //    }
            //    lista_reportes.Add( _ReporteGeneral);
            //}

            return lista_reportes;
            
        }

        //public List<ReporteGeneral> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.ReporteGeneralDataTable dt = new PuntoDeVentaDataSet.ReporteGeneralDataTable();
        //    PuntoDeVentaDataSetTableAdapters.ReporteGeneralTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.ReporteGeneralTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<ReporteGeneral> GetModelosByDT(PuntoDeVentaDataSet.ReporteGeneralDataTable dt)
        //{
        //    List<Modelo.ReporteGeneral> _ReporteGenerals = new List<Modelo.ReporteGeneral>();
        //    foreach (PuntoDeVentaDataSet.ReporteGeneralRow row in dt.Rows)
        //    {
        //        _ReporteGenerals.Add(TransformaReporteGeneral(row));
        //    }
        //    return _ReporteGenerals;
        //}

        #endregion

        #region Miembros de IDAOModelo<ReporteGeneral>


        public List<ReporteGeneral> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<ReporteGeneral> Members


        public void GrabaModelo(ReporteGeneral modelo)
        {
            DAO.ZctReporteGeneral reporte =  (from reportes in _dao.ZctReporteGeneral where reportes.uid == modelo.uid select reportes).SingleOrDefault();
            reporte.update_web = true;
            _dao.SubmitChanges();


        }

        #endregion
    }
}
