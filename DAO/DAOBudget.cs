﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOBudget : Controlador.IDAOModelo<budget>
    {
  #region Miembros de IDAOModelo<Budget>
        private DAO.ReportesDataContext _dao;
        public DAOBudget(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public budget BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.BudgetDataTable dt = new PuntoDeVentaDataSet.BudgetDataTable();
            //PuntoDeVentaDataSetTableAdapters.BudgetTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.BudgetTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZctPresupuestosXmes Budget = (from m in _dao.VW_ZctPresupuestosXmes 
                                            where m.update_web.ToString() == busqueda && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.budget _Budget = new Modelo.budget();
                if (Budget != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _Budget = TransformaBudget(Budget);
                }
                return _Budget;
        }

        private budget TransformaBudget(DAO.VW_ZctPresupuestosXmes elemento)
        {
            
            budget _Budget = new budget()
            {
                
                actualizado = false,
                Cod_Cte = elemento.Cod_Cte,
                Anno_Pres = elemento.Anno_Pres,
                month_Pres = elemento.month_Pres,
                monto_Pres = elemento.monto_Pres.GetValueOrDefault(),
                 taller = elemento.taller,
                uid = elemento.uid.GetValueOrDefault(),
                cod_alm = elemento.cod_alm

            };
            return _Budget;
        }

        public List<budget> ObtieneModelos()
        {
            var Budgets = (from m in _dao.VW_ZctPresupuestosXmes where m.update_web == null || m.update_web == false select m).Take(5);
            List<budget> lista_Budgets = new List<budget>();
            foreach (DAO.VW_ZctPresupuestosXmes Budget in Budgets)
            {
                Modelo.budget _Budget = new Modelo.budget();
                if (Budget != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _Budget = TransformaBudget(Budget);
                }
                lista_Budgets.Add( _Budget);
            }

            return lista_Budgets;
            
        }

        //public List<Budget> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.BudgetDataTable dt = new PuntoDeVentaDataSet.BudgetDataTable();
        //    PuntoDeVentaDataSetTableAdapters.BudgetTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.BudgetTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<Budget> GetModelosByDT(PuntoDeVentaDataSet.BudgetDataTable dt)
        //{
        //    List<Modelo.Budget> _Budgets = new List<Modelo.Budget>();
        //    foreach (PuntoDeVentaDataSet.BudgetRow row in dt.Rows)
        //    {
        //        _Budgets.Add(TransformaBudget(row));
        //    }
        //    return _Budgets;
        //}

        #endregion

        #region Miembros de IDAOModelo<Budget>
          

        public List<budget> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<Budget> Members


        public void GrabaModelo(budget modelo)
        {
            DAO.ZctPresupuestosXmes Budget = (from Budgets in _dao.ZctPresupuestosXmes where Budgets.uid == modelo.uid select Budgets).SingleOrDefault();
           // Budget.uid = modelo.uid;
            Budget.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
