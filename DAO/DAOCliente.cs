﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOCliente : Controlador.IDAOModelo<customer>
    {
  #region Miembros de IDAOModelo<Cliente>
        private DAO.ReportesDataContext _dao;
        public DAOCliente(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public customer BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.ClienteDataTable dt = new PuntoDeVentaDataSet.ClienteDataTable();
            //PuntoDeVentaDataSetTableAdapters.ClienteTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.ClienteTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZctCatCteWeb Cliente = (from m in _dao.VW_ZctCatCteWeb 
                                            where m.Cod_Cte == Convert.ToInt32( busqueda) && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.customer _Cliente = new Modelo.customer();
                if (Cliente != null && Cliente.Cod_Cte != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _Cliente = TransformaCliente(Cliente);
                }
                return _Cliente;
        }

        private customer TransformaCliente(DAO.VW_ZctCatCteWeb Cliente)
        {
            
            customer _Cliente = new customer()
            {
                
                actualizado = false,
                Cod_Cte = Cliente.Cod_Cte,
                name = Cliente.Nom_Cte,
                taller= Cliente.taller,
                cod_cte_base = Cliente.cod_cte_base.GetValueOrDefault(),
                lugar_envio = Cliente.lugar_envio,
                ApMat_Cte = Cliente.ApMat_Cte,
                ApPat_Cte = Cliente.ApPat_Cte,
                email = Cliente.email,
                telefono = Cliente.Telefono,
                uid = Cliente.uuid.GetValueOrDefault()

            };
            return _Cliente;
        }

        public List<customer> ObtieneModelos()
        {
            var Clientes = (from m in _dao.VW_ZctCatCteWeb where m.update_web == null || m.update_web == false select m).Take(5);
            List<customer> lista_Clientes = new List<customer>();
            foreach (DAO.VW_ZctCatCteWeb Cliente in Clientes)
            {
                Modelo.customer _Cliente = new Modelo.customer();
                if (Cliente != null && Cliente.Cod_Cte != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _Cliente = TransformaCliente(Cliente);
                }
                lista_Clientes.Add( _Cliente);
            }

            return lista_Clientes;
            
        }

        //public List<Cliente> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.ClienteDataTable dt = new PuntoDeVentaDataSet.ClienteDataTable();
        //    PuntoDeVentaDataSetTableAdapters.ClienteTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.ClienteTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<Cliente> GetModelosByDT(PuntoDeVentaDataSet.ClienteDataTable dt)
        //{
        //    List<Modelo.Cliente> _Clientes = new List<Modelo.Cliente>();
        //    foreach (PuntoDeVentaDataSet.ClienteRow row in dt.Rows)
        //    {
        //        _Clientes.Add(TransformaCliente(row));
        //    }
        //    return _Clientes;
        //}

        #endregion

        #region Miembros de IDAOModelo<Cliente>
          

        public List<customer> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<Cliente> Members


        public void GrabaModelo(customer modelo)
        {
            DAO.ZctCatCte Cliente =  (from Clientes in _dao.ZctCatCte where Clientes.uuid == modelo.uid select Clientes).SingleOrDefault();
            //Cliente.uuid = modelo.uid;
            Cliente.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
