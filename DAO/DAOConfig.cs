﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOConfig
    {
        private DAO.ReportesDataContext _dao;
        public DAOConfig(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
            
        }
        public void BuscaModelo(string busqueda) { 
        
        }

        public int ObtieneRenglonesPorEnviar() { 
            
            int res = (from m in  _dao.VW_ZctPendientesWeb select m.total).Sum().GetValueOrDefault();
            return res;
        }

    }
}
