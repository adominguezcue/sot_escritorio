﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOCXP : Controlador.IDAOModelo<CXP>
    {
  #region Miembros de IDAOModelo<CXP>
        private DAO.ReportesDataContext _dao;
        public DAOCXP(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public CXP BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.CXPDataTable dt = new PuntoDeVentaDataSet.CXPDataTable();
            //PuntoDeVentaDataSetTableAdapters.CXPTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.CXPTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZctEncCuentasPagar CXP = (from m in _dao.VW_ZctEncCuentasPagar
                                            where m.uuid == new Guid( busqueda) && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.CXP _CXP = new Modelo.CXP();
                if (CXP != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _CXP = TransformaCXP(CXP);
                }
                return _CXP;
        }

        private CXP TransformaCXP(DAO.VW_ZctEncCuentasPagar CXP)
        {
            
            CXP _CXP = new CXP()
            {
                cod_cuenta_pago = CXP.cod_cuenta_pago,
                cod_oc = CXP.Cod_OC,
                estado = CXP.Estado,
                fecha_creacion = CXP.fecha_creacion,
                fecha_vencido = CXP.fecha_vencido,
                iva = CXP.iva,
                monto = CXP.monto,
                nom_prov = CXP.Nom_Prov,
                subtotal = CXP.subtotal,
                total = CXP.total,
                uid = CXP.uuid.GetValueOrDefault()

            };
            return _CXP;
        }

        public List<CXP> ObtieneModelos()
        {
            var CXPs = (from m in _dao.VW_ZctEncCuentasPagar where m.update_web == null || m.update_web == false select m).Take(5);
            List<CXP> lista_CXPs = new List<CXP>();
            foreach (DAO.VW_ZctEncCuentasPagar CXP in CXPs)
            {
                Modelo.CXP _CXP = new Modelo.CXP();
                if (CXP != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _CXP = TransformaCXP(CXP);
                }
                lista_CXPs.Add( _CXP);
            }

            return lista_CXPs;
            
        }

        //public List<CXP> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.CXPDataTable dt = new PuntoDeVentaDataSet.CXPDataTable();
        //    PuntoDeVentaDataSetTableAdapters.CXPTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.CXPTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<CXP> GetModelosByDT(PuntoDeVentaDataSet.CXPDataTable dt)
        //{
        //    List<Modelo.CXP> _CXPs = new List<Modelo.CXP>();
        //    foreach (PuntoDeVentaDataSet.CXPRow row in dt.Rows)
        //    {
        //        _CXPs.Add(TransformaCXP(row));
        //    }
        //    return _CXPs;
        //}

        #endregion

        #region Miembros de IDAOModelo<CXP>
          

        public List<CXP> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<CXP> Members


        public void GrabaModelo(CXP modelo)
        {
            DAO.ZctEncCuentasPagar CXP = (from CXPs in _dao.ZctEncCuentasPagar where CXPs.uuid == modelo.uid select CXPs).SingleOrDefault();
            //CXP.uuid = modelo.uid;
            CXP.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
