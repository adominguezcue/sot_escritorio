﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAORequisition : Controlador.IDAOModelo<Requisition>
    {
        #region Miembros de IDAOModelo<Requisition>
        private DAO.ReportesDataContext _dao;
        public DAORequisition(string conn) {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public Requisition BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.RequisitionDataTable dt = new PuntoDeVentaDataSet.RequisitionDataTable();
            //PuntoDeVentaDataSetTableAdapters.RequisitionTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.RequisitionTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZctRequisition Requisition = (from m in _dao.VW_ZctRequisition 
                                            where m.uid == new Guid(busqueda) && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.Requisition _Requisition = new Modelo.Requisition();
                if (Requisition != null && Requisition.folio != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _Requisition = TransformaRequisition(Requisition);
                }
                return _Requisition;
        }

        private Requisition TransformaRequisition(DAO.VW_ZctRequisition  requisition)
        {
            
            Requisition _requisition = new Requisition()
            {
                actualizado = false,
                cod_cte = requisition.Cod_Cte,
                estatus = requisition.estatus,
                uid_web = requisition.uid,
                taller = requisition.taller,
                cod_alm = requisition.cod_alm.GetValueOrDefault(),
                anio=requisition.anio_fiscal.GetValueOrDefault(),
                mes = requisition.mes_fiscal.GetValueOrDefault()

            };
            return _requisition;
        }


        //public void GrabaModelo(Requisition modelo)
        //{
        //    //PuntoDeVentaDataSetTableAdapters.RequisitionTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.RequisitionTableAdapter();
        //    //PuntoDeVentaDataSet.RequisitionDataTable dt = new PuntoDeVentaDataSet.RequisitionDataTable();
        //    //DAO.DAOMethodPayment _daoMethodPayment = new DAO.DAOMethodPayment();
        //    try
        //    {
        //        ta.FillById(dt, modelo.id);


        //        PuntoDeVentaDataSet.RequisitionRow row;
        //        if (dt.Rows.Count == 0)
        //        {
        //            row = dt.NewRequisitionRow();

        //            SetRow(modelo, row);
        //            dt.Rows.Add(row);
        //            ta.Update(dt);
        //        }
        //        else
        //        {
        //            row = dt[0];
        //            SetRow(modelo, row);
        //            ta.Update(row);
        //        }
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}

        //private static void SetRow(Requisition modelo, PuntoDeVentaDataSet.RequisitionRow row)
        //{
        //    row.id = modelo.id;
        //    row.description = modelo.description;

        //}



        public List<Requisition> ObtieneModelos()
        {
            var Requisitions = (from m in _dao.VW_ZctRequisition  where m.update_web == null || m.update_web== false select m).Take(5);
            List<Requisition> lista_Requisitions = new List<Requisition>();
            foreach (DAO.VW_ZctRequisition  Requisition in Requisitions)
            {
                Modelo.Requisition _Requisition = new Modelo.Requisition();
                if (Requisition != null && Requisition.Cod_Cte !=0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _Requisition = TransformaRequisition(Requisition);
                }
                lista_Requisitions.Add( _Requisition);
            }

            return lista_Requisitions;
            
        }

        //public List<Requisition> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.RequisitionDataTable dt = new PuntoDeVentaDataSet.RequisitionDataTable();
        //    PuntoDeVentaDataSetTableAdapters.RequisitionTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.RequisitionTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<Requisition> GetModelosByDT(PuntoDeVentaDataSet.RequisitionDataTable dt)
        //{
        //    List<Modelo.Requisition> _Requisitions = new List<Modelo.Requisition>();
        //    foreach (PuntoDeVentaDataSet.RequisitionRow row in dt.Rows)
        //    {
        //        _Requisitions.Add(TransformaRequisition(row));
        //    }
        //    return _Requisitions;
        //}

        #endregion

        #region Miembros de IDAOModelo<Requisition>


        public List<Requisition> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<Requisition> Members
        
        private int ObtieneFolio(Guid uid)
        {
            int? folio = (from r in _dao.ZctRequisition where r.uid == uid select r.folio).FirstOrDefault();
            if (folio == 0)
            {
                _dao.SP_ZctCatFolios(2, "RW");
                folio = (from f in _dao.ZctCatfolios where f.Cod_Folio == "RW" select f.Consc_Folio).FirstOrDefault();
               //'folio = (int)folios[0].Cons;

            }
            return folio.GetValueOrDefault();

        }


        public void GrabaModelo(Requisition modelo)
        {
            int folio = ObtieneFolio(modelo.uid_web.GetValueOrDefault());
            _dao.SP_ZctRequisition(folio, modelo.cod_cte, modelo.estatus, modelo.observaciones, modelo.uid_web.GetValueOrDefault(), modelo.cod_alm, modelo.actualizado);

            foreach (DetailRequisition detail in modelo.detail_requisitions_attributes.detail_requisitions){
                decimal costo = GetCosto(detail.cod_art, modelo.cod_alm);
                if (costo > 0)
                    detail.price = costo;
                _dao.SP_ZctRequisitionDetail(detail.uid_web, folio, detail.amount, detail.price, detail.cod_art, 0, modelo.actualizado, detail.omitir_iva);
            }
            

            //DAO.ZctRequisition  Requisition =  (from Requisitions in _dao.ZctRequisition  where Requisitions.uid == modelo.uid_web select Requisitions).SingleOrDefault();
            //Requisition.uid = modelo.uid_web.GetValueOrDefault();
            //Requisition.update_web = true;
            //_dao.SubmitChanges();
            

        }

        private decimal GetCosto(string cod_art, int cod_alm) {
            var existencia = (from c in _dao.ZctArtXAlm where c.Cod_Art == cod_art && c.Cod_Alm == cod_alm select c.Exist_Art).SingleOrDefault();
            decimal costo_promedio = 0;
            if (existencia.GetValueOrDefault() > 0) {
                decimal? promedio = (from c in _dao.ZctArtXAlm where c.Cod_Art == cod_art && c.Cod_Alm == cod_alm select c.CostoProm_Art).SingleOrDefault();
                costo_promedio = promedio.GetValueOrDefault() / (decimal)existencia.GetValueOrDefault();
            }
            return costo_promedio;
        }

        #endregion
    }
}
