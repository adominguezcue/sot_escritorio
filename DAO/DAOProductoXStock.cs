﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOproductxstock : Controlador.IDAOModelo<productxstock>
    {
  #region Miembros de IDAOModelo<productxstock>
        private DAO.ReportesDataContext _dao;
        public DAOproductxstock(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public productxstock BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.productxstockDataTable dt = new PuntoDeVentaDataSet.productxstockDataTable();
            //PuntoDeVentaDataSetTableAdapters.productxstockTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.productxstockTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZctArtXAlmWeb productxstock = (from m in _dao.VW_ZctArtXAlmWeb 
                                            where m.uuid == new Guid( busqueda) && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.productxstock _productxstock = new Modelo.productxstock();
                if (productxstock != null && productxstock.Cod_Alm != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _productxstock = Transformaproductxstock(productxstock);
                }
                return _productxstock;
        }

        private productxstock Transformaproductxstock(DAO.VW_ZctArtXAlmWeb productxstock)
        {
            
            productxstock _productxstock = new productxstock()
            {
                
                actualizado = false,
                cod_alm = productxstock.Cod_Alm,
                cod_art = productxstock.Cod_Art,
                costo_promedio = productxstock.CostoProm_Art.GetValueOrDefault(),
                existencia = productxstock.Exist_Art.GetValueOrDefault(),
                taller= productxstock.taller,
                uid = productxstock.uuid.GetValueOrDefault()
            };
            return _productxstock;
        }

        public List<productxstock> ObtieneModelos()
        {
            var productxstocks = (from m in _dao.VW_ZctArtXAlmWeb where m.update_web == null || m.update_web == false select m).Take(5);
            List<productxstock> lista_productxstocks = new List<productxstock>();
            foreach (DAO.VW_ZctArtXAlmWeb productxstock in productxstocks)
            {
                Modelo.productxstock _productxstock = new Modelo.productxstock();
                if (productxstock != null && productxstock.Cod_Alm != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _productxstock = Transformaproductxstock(productxstock);
                }
                lista_productxstocks.Add( _productxstock);
            }

            return lista_productxstocks;
            
        }

        //public List<productxstock> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.productxstockDataTable dt = new PuntoDeVentaDataSet.productxstockDataTable();
        //    PuntoDeVentaDataSetTableAdapters.productxstockTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.productxstockTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<productxstock> GetModelosByDT(PuntoDeVentaDataSet.productxstockDataTable dt)
        //{
        //    List<Modelo.productxstock> _productxstocks = new List<Modelo.productxstock>();
        //    foreach (PuntoDeVentaDataSet.productxstockRow row in dt.Rows)
        //    {
        //        _productxstocks.Add(Transformaproductxstock(row));
        //    }
        //    return _productxstocks;
        //}

        #endregion

        #region Miembros de IDAOModelo<productxstock>
          

        public List<productxstock> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<productxstock> Members


        public void GrabaModelo(productxstock modelo)
        {
            DAO.ZctArtXAlm productxstock =  (from productxstocks in _dao.ZctArtXAlm where productxstocks.uuid == modelo.uid select productxstocks).SingleOrDefault();
            productxstock.uuid = modelo.uid;
            productxstock.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
