﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOInventarioFinal : Controlador.IDAOModelo<inventario>
    {
        #region Miembros de IDAOModelo<InventarioFinal>
        private DAO.ReportesDataContext _dao;
        public DAOInventarioFinal(string conn) {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public inventario BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.InventarioFinalDataTable dt = new PuntoDeVentaDataSet.InventarioFinalDataTable();
            //PuntoDeVentaDataSetTableAdapters.InventarioFinalTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.InventarioFinalTableAdapter();
            
                //ta.FillById(dt, Convert.ToInt32(busqueda));
                DAO.VW_ZctInventarioFinal moto = (from m in _dao.VW_ZctInventarioFinal 
                                            where m.uuid.ToString() == busqueda                                            select m).SingleOrDefault();

                Modelo.inventario _InventarioFinal = new Modelo.inventario();
                if (moto != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _InventarioFinal = TransformaInventarioFinal(moto);
                }
                return _InventarioFinal;
            
            
        }

        private inventario TransformaInventarioFinal(DAO.VW_ZctInventarioFinal moto)
        {
            
            inventario _InventarioFinal = new inventario()
            {
                actualizado = false,
                uid_web = moto.uuid,
                //update_web = false,
                taller = moto.taller,
                
                codigo_articulo = moto.Cod_Art,
                conteo_3 = moto.Conteo3_Art.GetValueOrDefault(),
                costo_promedio = moto.CostoProm_Art.GetValueOrDefault(),
                articulo = moto.Desc_Art,
                almacen = moto.Desc_CatAlm,
                diferencia = moto.Diferencia_Art.GetValueOrDefault(),
                existencia = moto.Exist_Art.GetValueOrDefault(),
                anio =  moto.FchyHrReg.GetValueOrDefault().Year,
                mes =  moto.FchyHrReg.GetValueOrDefault().Month,
                folio = moto.Folio,
                termino = moto.Termino_EncFis.GetValueOrDefault()
                
            };
            return _InventarioFinal;
        }

        //public void GrabaModelo(InventarioFinal modelo)
        //{
        //    PuntoDeVentaDataSetTableAdapters.InventarioFinalTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.InventarioFinalTableAdapter();
        //    PuntoDeVentaDataSet.InventarioFinalDataTable dt = new PuntoDeVentaDataSet.InventarioFinalDataTable();
        //    //DAO.DAOMethodPayment _daoMethodPayment = new DAO.DAOMethodPayment();
        //    try
        //    {
        //        ta.FillById(dt, modelo.id);


        //        PuntoDeVentaDataSet.InventarioFinalRow row;
        //        if (dt.Rows.Count == 0)
        //        {
        //            row = dt.NewInventarioFinalRow();

        //            SetRow(modelo, row);
        //            dt.Rows.Add(row);
        //            ta.Update(dt);
        //        }
        //        else
        //        {
        //            row = dt[0];
        //            SetRow(modelo, row);
        //            ta.Update(row);
        //        }
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}

        //private static void SetRow(InventarioFinal modelo, PuntoDeVentaDataSet.InventarioFinalRow row)
        //{
        //    row.id = modelo.id;
        //    row.description = modelo.description;

        //}



        public List<inventario> ObtieneModelos()
        {
            var motos = (from m in _dao.VW_ZctInventarioFinal where m.update_web == null || m.update_web== false select m).Take(5);
            List<inventario> lista_motos = new List<inventario>();
            foreach (DAO.VW_ZctInventarioFinal moto in motos)
            {
                Modelo.inventario _InventarioFinal = new Modelo.inventario();
                if (moto != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _InventarioFinal = TransformaInventarioFinal(moto);
                }
                lista_motos.Add( _InventarioFinal);
            }

            return lista_motos;
            
        }

        //public List<InventarioFinal> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.InventarioFinalDataTable dt = new PuntoDeVentaDataSet.InventarioFinalDataTable();
        //    PuntoDeVentaDataSetTableAdapters.InventarioFinalTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.InventarioFinalTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<InventarioFinal> GetModelosByDT(PuntoDeVentaDataSet.InventarioFinalDataTable dt)
        //{
        //    List<Modelo.InventarioFinal> _InventarioFinales = new List<Modelo.InventarioFinal>();
        //    foreach (PuntoDeVentaDataSet.InventarioFinalRow row in dt.Rows)
        //    {
        //        _InventarioFinales.Add(TransformaInventarioFinal(row));
        //    }
        //    return _InventarioFinales;
        //}

        #endregion

        #region Miembros de IDAOModelo<InventarioFinal>


        public List<inventario> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<InventarioFinal> Members


        public void GrabaModelo(inventario modelo)
        {
            DAO.ZctInvCorteFis _elemento = (from elemento in _dao.ZctInvCorteFis where elemento.uuid == modelo.uid_web select elemento).SingleOrDefault();
            _elemento.uuid = modelo.uid_web;
            _elemento.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
