﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOCorteMensual : Controlador.IDAOModelo<corte_mensual>
    {
        #region Miembros de IDAOModelo<CorteMensual>
        private DAO.ReportesDataContext _dao;
        public DAOCorteMensual(string conn) {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public corte_mensual BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.CorteMensualDataTable dt = new PuntoDeVentaDataSet.CorteMensualDataTable();
            //PuntoDeVentaDataSetTableAdapters.CorteMensualTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.CorteMensualTableAdapter();
            
                //ta.FillById(dt, Convert.ToInt32(busqueda));
                DAO.VW_ZctCorteMensual moto = (from m in _dao.VW_ZctCorteMensual 
                                            where m.uuid.ToString() == busqueda                                            select m).SingleOrDefault();

                Modelo.corte_mensual _CorteMensual = new Modelo.corte_mensual();
                if (moto != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _CorteMensual = TransformaCorteMensual(moto);
                }
                return _CorteMensual;
            
            
        }

        private corte_mensual TransformaCorteMensual(DAO.VW_ZctCorteMensual moto)
        {
            
            corte_mensual _CorteMensual = new corte_mensual()
            {
                actualizado = false,
                uid_web = moto.uuid,
                motor = moto.Motor_Mot,
                region = moto.Region,
                tienda = moto.Tienda,
                almacen = moto.Desc_CatAlm,
                articulo = moto.Articulo,
                cantidad = moto.Cantidad.GetValueOrDefault(),
                CECO = moto.CECO,
                distrital = moto.Distrital,
                fecha_folio = moto.Fecha_folio.GetValueOrDefault(),
                folio = moto.Folio,
                nombre = moto.Nombre,
                Precio = moto.Precio.GetValueOrDefault(),
                presupuesto = moto.Presupuesto,
                sub_total = moto.SubTotal.GetValueOrDefault(),
                total = moto.Total.GetValueOrDefault(),
                vin = moto.Vin_Mot,
                taller = moto.taller,
                anio = moto.anio_fiscal.GetValueOrDefault(),
                mes = moto.mes_fiscal.GetValueOrDefault(),
                status = moto.Cod_Estatus.ToString()
                

                
            };
            return _CorteMensual;
        }

        //public void GrabaModelo(CorteMensual modelo)
        //{
        //    PuntoDeVentaDataSetTableAdapters.CorteMensualTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.CorteMensualTableAdapter();
        //    PuntoDeVentaDataSet.CorteMensualDataTable dt = new PuntoDeVentaDataSet.CorteMensualDataTable();
        //    //DAO.DAOMethodPayment _daoMethodPayment = new DAO.DAOMethodPayment();
        //    try
        //    {
        //        ta.FillById(dt, modelo.id);


        //        PuntoDeVentaDataSet.CorteMensualRow row;
        //        if (dt.Rows.Count == 0)
        //        {
        //            row = dt.NewCorteMensualRow();

        //            SetRow(modelo, row);
        //            dt.Rows.Add(row);
        //            ta.Update(dt);
        //        }
        //        else
        //        {
        //            row = dt[0];
        //            SetRow(modelo, row);
        //            ta.Update(row);
        //        }
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}

        //private static void SetRow(CorteMensual modelo, PuntoDeVentaDataSet.CorteMensualRow row)
        //{
        //    row.id = modelo.id;
        //    row.description = modelo.description;

        //}



        public List<corte_mensual> ObtieneModelos()
        {
            var motos = (from m in _dao.VW_ZctCorteMensual where m.update_web == null || m.update_web== false select m).Take(5);
            List<corte_mensual> lista_motos = new List<corte_mensual>();
            foreach (DAO.VW_ZctCorteMensual moto in motos)
            {
                Modelo.corte_mensual _CorteMensual = new Modelo.corte_mensual();
                if (moto != null )
                {
                    //Console.WriteLine("Si lo encontre");
                    _CorteMensual = TransformaCorteMensual(moto);
                }
                lista_motos.Add( _CorteMensual);
            }

            return lista_motos;
            
        }

        //public List<CorteMensual> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.CorteMensualDataTable dt = new PuntoDeVentaDataSet.CorteMensualDataTable();
        //    PuntoDeVentaDataSetTableAdapters.CorteMensualTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.CorteMensualTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<CorteMensual> GetModelosByDT(PuntoDeVentaDataSet.CorteMensualDataTable dt)
        //{
        //    List<Modelo.CorteMensual> _CorteMensuales = new List<Modelo.CorteMensual>();
        //    foreach (PuntoDeVentaDataSet.CorteMensualRow row in dt.Rows)
        //    {
        //        _CorteMensuales.Add(TransformaCorteMensual(row));
        //    }
        //    return _CorteMensuales;
        //}

        #endregion

        #region Miembros de IDAOModelo<CorteMensual>


        public List<corte_mensual> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<CorteMensual> Members


        public void GrabaModelo(corte_mensual modelo)
        {
            DAO.ZctDetOT _elemento = (from elemento in _dao.ZctDetOT where elemento.uuid == modelo.uid_web select elemento).SingleOrDefault();
            _elemento.uuid = modelo.uid_web;
            _elemento.update_web = true;
            _dao.SubmitChanges();


        }

        #endregion
    }
}
