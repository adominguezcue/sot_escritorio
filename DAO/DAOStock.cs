﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Modelo;

namespace DAO
{
    public class DAOStock : Controlador.IDAOModelo<stock>
    {
  #region Miembros de IDAOModelo<Stock>

        //centro_costos
        private DAO.ReportesDataContext _dao;
        public DAOStock(string conn)
        {
           _dao = new DAO.ReportesDataContext(conn);
        }


        public stock BuscaModelo(string busqueda)
        {
            
            //PuntoDeVentaDataSet.StockDataTable dt = new PuntoDeVentaDataSet.StockDataTable();
            //PuntoDeVentaDataSetTableAdapters.StockTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.StockTableAdapter();
            //ta.FillById(dt, Convert.ToInt32(busqueda));
            DAO.VW_ZctCatAlmWeb Stock = (from m in _dao.VW_ZctCatAlmWeb 
                                            where m.Cod_Alm == Convert.ToInt32( busqueda) && m.update_web == false
                                            select m).SingleOrDefault();

                Modelo.stock _Stock = new Modelo.stock();
                if (Stock != null && Stock.Cod_Alm != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _Stock = TransformaStock(Stock);
                }
                return _Stock;
        }

        private stock TransformaStock(DAO.VW_ZctCatAlmWeb Stock)
        {
            
            stock _Stock = new stock()
            {
                
                actualizado = false,
                cod_alm = Stock.Cod_Alm,
                name = Stock.Desc_CatAlm,
                taller= Stock.taller,
                uid = Stock.uuid.GetValueOrDefault(),
                centro_costos = Stock.centro_costos
            };
            return _Stock;
        }

        public List<stock> ObtieneModelos()
        {
            var Stocks = (from m in _dao.VW_ZctCatAlmWeb where m.update_web == null || m.update_web == false select m).Take(5);
            List<stock> lista_Stocks = new List<stock>();
            foreach (DAO.VW_ZctCatAlmWeb Stock in Stocks)
            {
                Modelo.stock _Stock = new Modelo.stock();
                if (Stock != null && Stock.Cod_Alm != 0)
                {
                    //Console.WriteLine("Si lo encontre");
                    _Stock = TransformaStock(Stock);
                }
                lista_Stocks.Add( _Stock);
            }

            return lista_Stocks;
            
        }

        //public List<Stock> ObtieneModelos(string folio)
        //{
        //    PuntoDeVentaDataSet.StockDataTable dt = new PuntoDeVentaDataSet.StockDataTable();
        //    PuntoDeVentaDataSetTableAdapters.StockTableAdapter ta = new PuntoDeVentaDataSetTableAdapters.StockTableAdapter();
        //    try
        //    {
        //        ta.FillById(dt, Convert.ToInt32(folio));
        //        return GetModelosByDT(dt);
        //    }
        //    finally
        //    {
        //        dt.Dispose();
        //        ta.Dispose();

        //    }
        //}
        //private List<Stock> GetModelosByDT(PuntoDeVentaDataSet.StockDataTable dt)
        //{
        //    List<Modelo.Stock> _Stocks = new List<Modelo.Stock>();
        //    foreach (PuntoDeVentaDataSet.StockRow row in dt.Rows)
        //    {
        //        _Stocks.Add(TransformaStock(row));
        //    }
        //    return _Stocks;
        //}

        #endregion

        #region Miembros de IDAOModelo<Stock>
          

        public List<stock> ObtieneModelosParaEnvioWeb()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDAOModelo<Stock> Members


        public void GrabaModelo(stock modelo)
        {
            DAO.ZctCatAlm Stock =  (from Stocks in _dao.ZctCatAlm where Stocks.uuid == modelo.uid select Stocks).SingleOrDefault();
            Stock.uuid = modelo.uid;
            Stock.update_web = true;
            _dao.SubmitChanges();

        }

        #endregion
    }
}
