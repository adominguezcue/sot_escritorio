Imports System.IO
Imports System.Reflection
Imports Microsoft.Office.Interop

Public Class ZctObjDatos(Of T As New)
    Private _Atributos As SortedList(Of ZctDBAtributoOrigen, MemberInfo)



    Public Function GetDatosExcel(ByVal Path As String) As List(Of T)
        Dim exa As Excel.Application
        Dim wb As Excel.Workbook
        Dim ws As Excel.Worksheet
        Try
            ' Instancia de la aplicacion
            exa = New Excel.Application
            ' Instancia de el libro
            wb = exa.Workbooks.Open(Path)
            ' Instancia de la hoja de trabajo
            ws = wb.Worksheets.Item(1)
            'Establece la cultura como "es-Mx"
            Dim OldCultureInfo As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-MX")
            'Cuenta el numero de Filas
            Dim ctdr As Integer = 1
            Dim type As Type = GetType(T)
            Dim _list As New List(Of T)

            While (ws.Range("A" & ctdr).Value IsNot Nothing AndAlso ws.Range("A" & ctdr).Value.ToString <> "")


                Dim _obj As New T
                If _Atributos Is Nothing Then GetMiembros()


                'Busca los campos en la clase
                For Each kvp As KeyValuePair(Of ZctDBAtributoOrigen, MemberInfo) In _Atributos
                    Dim Atributo As ZctDBAtributoOrigen = kvp.Key
                    Dim mi As MemberInfo = kvp.Value
                    'Busca el valor de la columna

                    Select Case mi.MemberType
                        Case MemberTypes.Property
                            If ws.Range(Atributo.OrigenExcel & ctdr).Value Is Nothing Then Throw New BaseDatosException("El archivo proporcionado esta mal configurado, verifique que se proporcionen todos los datos.")
                            Dim valor As Object = GetObjectGen(ws.Range(Atributo.OrigenExcel & ctdr).Value, DirectCast(mi, PropertyInfo).PropertyType)
                            Try
                                DirectCast(mi, PropertyInfo).SetValue(_obj, valor, Nothing)
                            Catch ex As Exception
                                Throw ex
                            End Try
                        Case MemberTypes.Field
                            'DirectCast(mi, FieldInfo).SetValue(obj, GetObjectGen(Reader.Item(Atributo.Campo), DirectCast(mi, FieldInfo).FieldType))
                    End Select
                Next
                _list.Add(_obj)
                ctdr = ctdr + 1
            End While
            Return _list
        Catch ex As Exception
            Throw ex
        Finally
            exa = Nothing
            If wb IsNot Nothing Then wb.Close()
            If ws IsNot Nothing Then ws = Nothing
        End Try
    End Function

    Private Sub GetMiembros()
        _Atributos = New SortedList(Of ZctDBAtributoOrigen, MemberInfo)
        For Each mi As MemberInfo In GetType(T).GetMembers
            Dim atributo As ZctDBAtributoOrigen = DirectCast(Attribute.GetCustomAttribute(mi, GetType(ZctDBAtributoOrigen)), ZctDBAtributoOrigen)
            If atributo IsNot Nothing Then
                _Atributos.Add(atributo, mi)
            End If
        Next
    End Sub

    Public Function GetObjectGen(ByVal valor As Object, ByVal tipo As Type) As Object
        If tipo Is GetType(String) Then
            Return valor.ToString
        ElseIf tipo Is GetType(Integer) Then
            Return CType(valor, Integer)
        ElseIf tipo Is GetType(Boolean) Then
            Return CType(valor, Boolean)
        ElseIf tipo Is GetType(Double) Then
            Return CType(valor, Double)
        ElseIf tipo Is GetType(Date) Then
            Return CType(valor, Date)
        Else
            Return valor
        End If
    End Function
End Class
