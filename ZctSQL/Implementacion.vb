

    Public Module Implementacion

#Region "Operaciones Genericas"

    Public Function GetGeneric(Of T As Structure)(ByVal Valor As System.DBNull) As T
        Dim Valor2 As Nullable(Of T)
        Return Valor2.GetValueOrDefault
    End Function

    Public Function GetGenericC(Of T)(ByVal Valor As System.DBNull) As T
        Dim Valor2 As T
        Valor2 = System.Activator.CreateInstance(Of T)()
        Return Valor2
    End Function


    Public Function GetGeneric(Of T As Structure)(ByVal Valor As Nullable(Of T)) As T
            Return Valor.GetValueOrDefault
        End Function

        Public Function GetGeneric(ByVal Valor As String) As String
            If Valor Is DBNull.Value Then
                Return ""
            Else
                Return Valor
            End If

        End Function

        Public Function GetGeneric(ByVal Valor As System.DBNull) As String
            Return ""
        End Function



        Public Sub GenGraba(Of T As IZctDatos)(ByVal BD As ZctDB, ByVal Elemento As T)

            Try

                BD.CrearComando(Elemento.SQLGraba)
                BD.EjecutarComando()


            Catch ex As BaseDatosException

                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
            Catch ex As Exception

                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)

            End Try
        End Sub
        Public Sub GenGraba(Of T As IZctDatos)(ByVal Conn As String, ByVal Elemento As T)
            Dim BD As New ZctDB
            Try
                BD.GetCadenaConexion = Conn
                BD.Conectar()
                BD.ComenzarTransaccion()
                BD.CrearComando(Elemento.SQLGraba)
                BD.EjecutarComando()
                BD.ConfirmarTransaccion()

            Catch ex As BaseDatosException
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
            Catch ex As Exception
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Sub

        Public Sub GenGraba(Of T As IZctDatos)(ByVal Conn As String, ByVal Array As List(Of T))
            If Array.Count <= 0 Then Exit Sub
            Dim BD As New ZctDB
            Try
                BD.GetCadenaConexion = Conn
                BD.Conectar()
                BD.ComenzarTransaccion()
                For Each Elemento As T In Array
                    BD.CrearComando(Elemento.SQLGraba)
                    BD.EjecutarComando()
                Next
                BD.ConfirmarTransaccion()

            Catch ex As BaseDatosException
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al grabar el dato.", ex)
            Catch ex As Exception
                BD.CancelarTransaccion()
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Sub



        Public Sub GenElimina(Of T As IZctDatos)(ByVal Conn As String, ByVal Elemento As T)
            Dim BD As New ZctDB
            Try
                BD.GetCadenaConexion = Conn
                BD.Conectar()
                BD.ComenzarTransaccion()
                BD.CrearComando(Elemento.SQLElimina)
                BD.EjecutarComando()
                BD.ConfirmarTransaccion()

            Catch ex As BaseDatosException
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Eliminar el dato.", ex)
            Catch ex As Exception
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Eliminar el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Sub

        Public Sub GenDatos(Of T As IZctDatos)(ByVal Conn As String, ByVal Elemento As T)
            Dim BD As New ZctDB
            Try
                BD.GetCadenaConexion = Conn
                BD.Conectar()
                BD.CrearComando(Elemento.SQLObtiene)
                Elemento.GetMe(BD.EjecutarConsulta)
            Catch ex As BaseDatosException
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Sub


        Public Sub GenDatos(Of T As IZctDatos)(ByVal BD As ZctDB, ByVal Elemento As T)

            Try
                BD.CrearComando(Elemento.SQLObtiene)
                Elemento.GetMe(BD.EjecutarConsulta)
            Catch ex As BaseDatosException
                Throw ex
            Catch ex As Exception
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al Obtener el dato.", ex)
            End Try
        End Sub

        Public Sub GenEjecuta(ByVal Conn As String, ByVal SQL As String)
            Dim BD As New ZctDB
            Try
                BD.GetCadenaConexion = Conn
                BD.Conectar()
                BD.ComenzarTransaccion()
                BD.CrearComando(SQL)
                BD.EjecutarComando()
                BD.ConfirmarTransaccion()
            Catch ex As BaseDatosException
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al ejecutar el comando.", ex)
            Catch ex As Exception
                BD.CancelarTransaccion()
                Throw New ZctSQL.BaseDatosException("Ha ocurrido un error al ejecutar el comando.", ex)
            Finally
                BD.Desconectar()
                BD.Dispose()
            End Try
        End Sub

        Public Sub GenEjecuta(ByVal Conn As String, ByVal SQL As String, ByVal BD As ZctDB)
            BD.CrearComando(SQL)
            BD.EjecutarComando()
        End Sub


#End Region
    End Module

    Public Interface IZctDatos
        ReadOnly Property SQLGraba() As String
        ReadOnly Property SQLObtiene() As String
        ReadOnly Property SQLElimina() As String
        Sub GetMe(ByVal DataReader As SqlClient.SqlDataReader)

    End Interface

