
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Configuration

''' <summary>
''' Representa la base de datos en el sistema.
''' Ofrece los m�todos de acceso a misma.
''' </summary>

Public Class ZctDB
    Implements IDisposable

    Private _sqlConn As New SqlClient.SqlConnection
    Private _sqlDataReader As SqlClient.SqlDataReader = Nothing
    Private _sqlCommand As New SqlClient.SqlCommand
    Private _CadenaConexion As String
    Private _Transaccion As SqlClient.SqlTransaction = Nothing
    Private _SP As Boolean = False
    Private _disposed As Boolean = False                'Campo para manejar multiples llamadas al Dispose

    ''' <summary>
    ''' Servidor al cual se va a conectar
    ''' </summary>
    Public Property GetCadenaConexion() As String
        Get
            Return _CadenaConexion
        End Get
        Set(ByVal value As String)
            _CadenaConexion = value
        End Set
    End Property

    ''' <summary>
    ''' Obtiene la transacci�n
    ''' </summary>

    Public ReadOnly Property TransStatus() As SqlClient.SqlTransaction
        Get
            Return _Transaccion
        End Get

    End Property

    ''' <summary>
    ''' Determina si se va a env�ar un procedimiento almacenado
    ''' </summary>
    Public Property ProcedimientoAlmacenado() As Boolean
        Get
            Return _SP
        End Get
        Set(ByVal value As Boolean)
            _SP = value

        End Set
    End Property
    ''' <summary>
    ''' Permite desconectarse de la base de datos.
    ''' </summary>
    Public Sub Desconectar()
        If Me._sqlConn.State.Equals(ConnectionState.Open) Then
            Me._sqlConn.Close()
        End If
    End Sub

    ''' <summary>
    ''' Se concecta con la base de datos.
    ''' </summary>
    ''' <exception cref="BaseDatosException">Si existe un error al conectarse.</exception> 
    Public Sub Conectar()

        Try
            'If Me._sqlConn Is Nothing Then
            Me._sqlConn.ConnectionString = _CadenaConexion
            'End If
            Me._sqlConn.Open()
        Catch ex As DataException
            Throw New BaseDatosException("Error al conectarse.")
        End Try
    End Sub


    ''' <summary>
    ''' Crea un comando en base a una sentencia SQL.Ejemplo:
    ''' <code>SELECT * FROM Tabla WHERE campo1=@campo1, campo2=@campo2</code>
    ''' Guarda el comando para el seteo de par�metros y la posterior ejecuci�n.
    ''' </summary>
    ''' <param name="sentenciaSQL">La sentencia SQL con el formato: SENTENCIA [param = @param,]</param>
    Public Sub CrearComando(ByVal sentenciaSQL As String)
        Me._sqlCommand.Connection = Me._sqlConn
        If _SP = True Then
            Me._sqlCommand.CommandType = CommandType.StoredProcedure
        Else
            Me._sqlCommand.CommandType = CommandType.Text
        End If
        Me._sqlCommand.CommandText = sentenciaSQL
        If Not Me._Transaccion Is Nothing Then
            Me._sqlCommand.Transaction = Me._Transaccion
        End If
    End Sub

    ''' <summary>
    ''' Comienza una transacci�n en base a la conexion abierta.
    '''  lo que se ejecute luego de esta ionvocaci�n estar� 
    ''' dentro de una tranasacci�n.
    ''' </summary>
    Public Sub ComenzarTransaccion()
        If Me._Transaccion Is Nothing Then
            Me._Transaccion = Me._sqlConn.BeginTransaction()
        End If
    End Sub

    ''' <summary>
    ''' Cancela la ejecuci�n de una transacci�n.
    '''  lo ejecutado entre �sta invocaci�n y su 
    ''' correspondiente <c>ComenzarTransaccion</c> ser� perdido.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub CancelarTransaccion()
        If Not Me._Transaccion Is Nothing Then
            Me._Transaccion.Rollback()
        End If
    End Sub

    ''' <summary>
    ''' Confirma todo los comandos ejecutados entre el <c>ComanzarTransaccion</c>
    ''' y �sta invocaci�n.
    ''' </summary>
    Public Sub ConfirmarTransaccion()
        If Not Me._Transaccion Is Nothing Then
            Me._Transaccion.Commit()
        End If
    End Sub


    ''' <summary>
    ''' Ejecuta el comando creado y retorna el resultado de la consulta.
    ''' </summary>
    ''' <returns>El resultado de la consulta.</returns>
    ''' <exception cref="BaseDatosException">Si ocurre un error al ejecutar el comando.</exception>
    Public Function EjecutarConsulta() As SqlClient.SqlDataReader
        Return Me._sqlCommand.ExecuteReader()
    End Function

    ''' <summary>
    ''' Ejecuta el comando creado y retorna un escalar.
    ''' </summary>
    ''' <returns>El escalar que es el resultado del comando.</returns>
    ''' <exception cref="BaseDatosException">Si ocurre un error al ejecutar el comando.</exception>
    Public Function EjecutarEscalar() As Integer
        Dim escalar As Integer = 0
        Try
            escalar = Integer.Parse(Me._sqlCommand.ExecuteScalar().ToString())
        Catch ex As InvalidCastException
            Throw New BaseDatosException("Error al ejecutar un escalar.", ex)
        End Try
        Return escalar
    End Function

    ''' <summary>
    ''' Ejecuta el comando creado.
    ''' </summary>
    Public Sub EjecutarComando()
        Me._sqlCommand.ExecuteNonQuery()
    End Sub


    ''' <summary>
    ''' Setea un par�metro como nulo del comando creado.
    ''' </summary>
    ''' <param name="nombre">El nombre del par�metro cuyo valor ser� nulo.</param>
    Public Sub AsignarParametroNulo(ByVal nombre As String)
        AsignarParametro(nombre, "", "NULL")
    End Sub

    ''' <summary>
    ''' Asigna un par�metro de tipo cadena al comando creado.
    ''' </summary>
    ''' <param name="nombre">El nombre del par�metro.</param>
    ''' <param name="valor">El valor del par�metro.</param>
    Public Sub AsignarParametroCadena(ByVal nombre As String, ByVal valor As String)
        AsignarParametro(nombre, "'", valor)
    End Sub

    ''' <summary>
    ''' Asigna un par�metro de tipo entero al comando creado.
    ''' </summary>
    ''' <param name="nombre">El nombre del par�metro.</param>
    ''' <param name="valor">El valor del par�metro.</param>
    Public Sub AsignarParametroEntero(ByVal nombre As String, ByVal valor As Integer)
        AsignarParametro(nombre, "", valor.ToString())
    End Sub

    ''' <summary>
    ''' Asigna un par�metro al comando creado.
    ''' </summary>
    ''' <param name="nombre">El nombre del par�metro.</param>
    ''' <param name="separador">El separador que ser� agregado al valor del par�metro.</param>
    ''' <param name="valor">El valor del par�metro.</param>
    Private Sub AsignarParametro(ByVal nombre As String, ByVal separador As String, ByVal valor As String)
        Dim indice As Integer = Me._sqlCommand.CommandText.IndexOf(nombre)
        Dim prefijo As String = Me._sqlCommand.CommandText.Substring(0, indice)
        Dim sufijo As String = Me._sqlCommand.CommandText.Substring(indice + nombre.Length)
        Me._sqlCommand.CommandText = prefijo + separador + valor + separador + sufijo
    End Sub

    ''' <summary>
    ''' Asigna un par�metro de tipo fecha al comando creado.
    ''' </summary>
    ''' <param name="nombre">El nombre del par�metro.</param>
    ''' <param name="valor">El valor del par�metro.</param>
    Public Sub AsignarParametroFecha(ByVal nombre As String, ByVal valor As DateTime)
        AsignarParametro(nombre, "'", valor.ToString())
    End Sub


    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
        If _disposed = False Then
            If disposing Then
                'Libera los datos administrados
                _disposed = True
            End If
        End If
        ' Free your own state (unmanaged objects).
        ' Set large fields to null.
    End Sub

    Protected Overrides Sub Finalize()
        'Libera los datos en caso de que el programador no haya llamado los datos
        Dispose(False)
    End Sub

End Class


Public Class BaseDatosException
    Inherits ApplicationException

    ''' <summary>
    ''' Construye una instancia en base a un mensaje de error y la una excepci�n original.
    ''' </summary>
    ''' <param name="mensaje">El mensaje de error.</param>
    ''' <param name="original">La excepci�n original.</param>
    Public Sub New(ByVal mensaje As String, ByVal original As Exception)
        MyBase.New(mensaje, original)
    End Sub

    ''' <summary>
    ''' Construye una instancia en base a un mensaje de error.
    ''' </summary>
    ''' <param name="mensaje">El mensaje de error.</param>
    Public Sub New(ByVal mensaje As String)
        MyBase.New(mensaje)
    End Sub

End Class
