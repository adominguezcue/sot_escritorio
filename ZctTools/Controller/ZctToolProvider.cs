﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZctTools.Model;
using System.Data;
using System.Reflection;
using System.Runtime.Serialization;
using ZctTools.Clases;

namespace ZctTools.Controller
{
  internal class ZctToolProvider
  {
        private string _conn;

        public ZctToolProvider(string conn)
        {
            _conn = conn;
        }

        internal DataTable BusquedaAlfa(string tabla, string busqueda) {
      try { 
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn)) {
          return Clases.ClassGen.ToDataTable(db.SP_ZctBusqueda_Alfa(tabla, busqueda));
        }
      }
      catch
      {
        return new DataTable();
      }
    }

    internal DataTable Busqueda(string tabla, int busqueda)
    {
      try { 
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn))
        {
          return Clases.ClassGen.ToDataTable(db.SP_ZctBusqueda(tabla, busqueda));
        }
      }
      catch
      {
        return new DataTable();
      }
    }
    internal DataTable SP_ZctCatArt_Busqueda_Mix(string codigo)
    {
      try
      {
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn))
        {
          return ClassGen.ToDataTable(db.SP_ZctCatArt_Busqueda_Mix(codigo));
        }
      }
      catch
      {
        return new DataTable();
      }
    }
    internal DataTable SP_ZctCatArt_Busqueda(string codigo)
    {
      try { 
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn))
        {
          return Clases.ClassGen.ToDataTable(db.SP_ZctCatArt_Busqueda(codigo));
        }
      }
      catch
      {
        return new DataTable();
      }
    }

    internal DataTable SP_ZctCatArt(int tpConsulta, string cod_art, string desc_Art, decimal prec_Art, decimal cos_Art, int cod_Mar, int cod_Linea, int cod_Dpto)
    {
      return SP_ZctCatArt(tpConsulta, cod_art, desc_Art, prec_Art, cos_Art, cod_Mar, cod_Linea, cod_Dpto, "ART", 0, false, 0, 0, null, false, null, null, null, null, 0, 0, 0, 0, 0);
    }
    internal DataTable SP_ZctCatArt(int tpConsulta, string cod_art,string desc_Art,decimal prec_Art, decimal cos_Art, int cod_Mar, int cod_Linea, int cod_Dpto,
      string cod_TpArt, int cod_TpRef, bool desactivar,int tiempo_vida, int cod_alm, string no_serie, bool completo, string activo_fijo, string comentarios,
      string ubicacion, string pais, int id_presentacion, int id_cat_moneda, decimal descuento, decimal cantidad_maxima, decimal cantidad_minima)
    {
      try { 
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn))
        {
          return Clases.ClassGen.ToDataTable(db.SP_ZctCatArt(tpConsulta, cod_art,desc_Art,prec_Art,cos_Art,cod_Mar, cod_Linea,cod_Dpto,cod_TpArt,cod_TpRef,
            desactivar,tiempo_vida,cod_alm,no_serie,completo,activo_fijo,comentarios,ubicacion,pais,id_presentacion,id_cat_moneda,descuento,cantidad_maxima,cantidad_minima));
        }
      }
      catch(Exception ex)
      {
        return new DataTable();
      }
    }

    internal DataTable SP_ZctCatFolios(int tpConsulta, string codFolio)
    {
      try { 
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn))
        {
          return Clases.ClassGen.ToDataTable(db.SP_ZctCatFolios(tpConsulta, codFolio));
        }
      }
      catch
      {
        return new DataTable();
      }
    }

    internal DataTable SP_ZctArtXAlm(int tpConsulta, string cod_Art, int cod_Alm, int exist_Art, decimal costoProm_Art)
    {
      try
      {
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn))
        {
          return Clases.ClassGen.ToDataTable(db.SP_ZctArtXAlm(tpConsulta,cod_Art,cod_Alm, exist_Art, costoProm_Art));
        }
      }
      catch (Exception ex)
      {
        return new DataTable();
      }
    }

    internal DataTable SP_ZctEncMovInv(int tpConsulta, int codMov_Inv, string tpMov_Inv, decimal cosMov_Inv, string folOS_Inv, string cod_Art, decimal ctdMov_Inv, string tpOs_Inv, DateTime fchMov_Inv, int cod_Alm) {
      try
      {
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn))
        {
          return Clases.ClassGen.ToDataTable(db.SP_ZctEncMovInv(tpConsulta, codMov_Inv, tpMov_Inv, cosMov_Inv, folOS_Inv, cod_Art, ctdMov_Inv, tpOs_Inv, fchMov_Inv, cod_Alm));
        }
      }
      catch(Exception ex) {
        return new DataTable();
      }
    }
    internal List<ZctCatAlm> GetCatAlm(int CodAlm) {
      List<ZctCatAlm> listAlm;
      try
      {
        using (ZctSOTToolsDBDataContext db = new ZctSOTToolsDBDataContext(_conn)) {
          listAlm = (from alm in db.ZctCatAlms
                     where alm.CodAlm == (CodAlm != 0 ? CodAlm : alm.CodAlm)
                     select alm).ToList();
        }
      }
      catch {
        listAlm = new List<ZctCatAlm>();
      }
      return listAlm;
    }
    internal DataTable GetData(string sSPName, string sSpVariables) {
      try
      {
        MethodData methodData = ClassGen.GetCallMethod(sSPName, sSpVariables);
        MethodInfo method = this.GetType().GetMethod(sSPName, BindingFlags.NonPublic | BindingFlags.Instance, null, methodData.Types, null);

        return (DataTable)method.Invoke(this, methodData.Params);
      }
      catch (Exception ex)
      {
        return new DataTable();
      }
    }

    internal DataTable GetData(string sSPName, string sSpVariables, string sSpValues)
    {
      try {
        MethodData methodData = ClassGen.GetCallMethod(sSPName, sSpVariables, sSpValues);
        MethodInfo method = this.GetType().GetMethod(sSPName, BindingFlags.NonPublic | BindingFlags.Instance, null, methodData.Types, null);

        return (DataTable)method.Invoke(this, methodData.Params);
      }
      catch(Exception ex) {
        return new DataTable();
      }
    }
  }
}
