﻿using System.Data;
using System.Collections.Generic;
using ZctTools.Model;

namespace ZctTools.Controller
{
    public class ZctToolService
    {
     string _conn;

        public ZctToolService(string conn)
        {
            _conn = conn;
        }

        public DataTable BusquedaAlfa(string tabla, string busqueda) {
      return new ZctToolProvider(_conn).BusquedaAlfa(tabla, busqueda);
    }

    public DataTable Busqueda(string tabla, int busqueda)
    {
      return new ZctToolProvider(_conn).Busqueda(tabla, busqueda);
    }

    public DataTable GetData(string sSPName, string sSpVariables)
    {
      return new ZctToolProvider(_conn).GetData(sSPName, sSpVariables);
    }

    public DataTable GetData(string sSPName, string sSpVariables, string sSpValues)
    {
      return new ZctToolProvider(_conn).GetData(sSPName, sSpVariables,sSpValues);
    }
    public DataTable SP_ZctCatArt_Busqueda(string codigo)
    {
      return new ZctToolProvider(_conn).SP_ZctCatArt_Busqueda(codigo);
    }

    public List<ZctCatAlm> GetCatAlm(int CodAlm) {
      return new ZctToolProvider(_conn).GetCatAlm(CodAlm);
    }
  }
}
