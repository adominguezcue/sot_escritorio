﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZctTools.Clases
{
  public class MethodData
  {
    public Type[] Types { get; set; }
    public object[] Params { get; set; }
  }
}
