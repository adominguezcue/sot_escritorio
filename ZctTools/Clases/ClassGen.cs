﻿using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Drawing;

namespace ZctTools.Clases
{
  public static class ClassGen
  {

    [DllImport("shell32.dll", CharSet = CharSet.Auto)]
    internal static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, out SHFILEINFO psfi, uint cbFileInfo, uint uFlags);

    [DllImport("shell32.dll", EntryPoint = "#727")]
    internal extern static int SHGetImageList(int iImageList, ref Guid riid, out IImageList ppv);

    [DllImport("user32.dll", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static extern bool DestroyIcon(IntPtr hIcon);

    internal const uint SHGFI_ICON = 0x000000100;
    internal const uint SHGFI_USEFILEATTRIBUTES = 0x000000010;
    internal const uint SHGFI_OPENICON = 0x000000002;
    internal const uint SHGFI_SMALLICON = 0x000000001;
    internal const uint SHGFI_LARGEICON = 0x000000000;
    internal const uint SHGFI_LINKOVERLAY = 0x000008000;
    internal const uint FILE_ATTRIBUTE_DIRECTORY = 0x00000010;
    internal const uint FILE_ATTRIBUTE_NORMAL = 0x00000080;
    internal const int SHIL_EXTRALARGE = 0x2;
    internal const int SHIL_JUMBO = 0x4;

    public enum Tipo_Dato { NUMERICO, ALFANUMERICO }

    public enum TypeEmployeeFile { IMAGE, OTHER }

    public static bool IsNumeric(string value) {
      bool itIs = false;
      int iValue;
      decimal dValue;

      itIs=Int32.TryParse(value, out iValue);
      if (!itIs) {
        itIs = Decimal.TryParse(value, out dValue);
      }

      return itIs;
    }

    public static DataTable ToDataTable<T>(this IEnumerable<T> items)
    {
      var tb = new DataTable(typeof(T).Name);
      PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
      foreach (var prop in props)
      {

        if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
          tb.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
        }
        else {
          tb.Columns.Add(prop.Name, prop.PropertyType);
        }
      }

      foreach (var item in items.ToList())
      {
        var values = new object[props.Length];
        for (var i = 0; i < props.Length; i++)
        {
          values[i] = props[i].GetValue(item, null);
        }

        tb.Rows.Add(values);
      }
      return tb;
    }

    public static List<T> ConvertToList<T>(DataTable dt)
    {
      var columnNames = dt.Columns.Cast<DataColumn>()
          .Select(c => c.ColumnName)
          .ToList();

      var properties = typeof(T).GetProperties();

      return dt.AsEnumerable().Select(row =>
      {
        var objT = Activator.CreateInstance<T>();

        foreach (var pro in properties)
        {
          if (columnNames.Contains(pro.Name) && row[pro.Name]!=DBNull.Value)
            pro.SetValue(objT, row[pro.Name]);
        }

        return objT;
      }).ToList();

    }

    public static MethodData GetCallMethod(string sSPName, string sSpVariables, string sSpValues) {
      MethodData methodData = new MethodData();
      try
      {
        string[] vars = sSpVariables.Split('|');
        string[] values = sSpValues.Split('|');
        object[] param = new object[vars.Count()];
        Type[] types = new Type[vars.Count()];
        for (var pos = 0; pos < vars.Count(); pos++)
        {
          switch (vars[pos].Substring(vars[pos].IndexOf(";") + 1).ToUpper())
          {
            case "INT":
              param[pos] = Int32.Parse(values[pos]);
              types[pos] = typeof(int);
              break;
            case "DECIMAL":
              param[pos] = Decimal.Parse(values[pos]); ;
              types[pos] = typeof(decimal);
              break;
            case "VARCHAR":
              param[pos] = values[pos];
              types[pos] = typeof(string);
              break;
            case "SMALLDATETIME":
              param[pos] = DateTime.Parse(values[pos]);
              types[pos] = typeof(DateTime);
              break;
          }
        }
        methodData.Types = types;
        methodData.Params = param;
        return methodData;
      }
      catch (Exception ex)
      {
        return methodData;
      }
    }

    public static MethodData GetCallMethod(string sSPName, string sSpVariables)
    {
      MethodData methodData = new MethodData();
      try
      {
        string[] vars = sSpVariables.Split('|');
        object[] param = new object[vars.Count() + 1];
        Type[] types = new Type[vars.Count() + 1];
        param[0] = 1;
        types[0] = typeof(int);
        for (var pos = 0; pos < vars.Count(); pos++)
        {
          switch (vars[pos].Substring(vars[pos].IndexOf(";") + 1))
          {
            case "INT":
              param[pos + 1] = 0;
              types[pos + 1] = typeof(int);
              break;
            case "DECIMAL":
              param[pos + 1] = 0m;
              types[pos + 1] = typeof(decimal);
              break;
            case "VARCHAR":
              param[pos + 1] = "0";
              types[pos + 1] = typeof(string);
              break;
            case "SMALLDATETIME":
              param[pos + 1] = DateTime.Now;
              types[pos + 1] = typeof(DateTime);
              break;
          }
        }
        methodData.Types = types;
        methodData.Params = param;
        return methodData;
      }
      catch (Exception ex)
      {
        return methodData;
      }
    }

    public static Icon GetSmallIcon(string name, System.Windows.Forms.View size, bool linkOverlay)
    {
      SHFILEINFO shfi = new SHFILEINFO();
      uint flags = SHGFI_ICON | SHGFI_USEFILEATTRIBUTES;

      if (true == linkOverlay) flags += SHGFI_LINKOVERLAY;

      if (System.Windows.Forms.View.SmallIcon == size)
      {
        flags += SHGFI_SMALLICON;
      }
      else
      {
        flags += SHGFI_LARGEICON;
      }

      SHGetFileInfo(name,
          FILE_ATTRIBUTE_NORMAL,
          out shfi,
          (uint)Marshal.SizeOf(shfi),
          flags);

      Icon icon = (Icon)Icon.FromHandle(shfi.hIcon).Clone();
      DestroyIcon(shfi.hIcon);
      return icon;
    }

    public static Icon GetLargeIcon(string name, System.Windows.Forms.View size, bool linkOverlay)
    {
      SHFILEINFO shfi = new SHFILEINFO();
      uint SHGFI_SYSICONINDEX = 0x4000;
      uint flags = SHGFI_SYSICONINDEX;

      SHGetFileInfo(name,
          FILE_ATTRIBUTE_NORMAL,
          out shfi,
          (uint)Marshal.SizeOf(shfi),
          flags);

      var iconIndex = shfi.iIcon;
      Guid iidImageList = new Guid("46EB5926-582E-4017-9FDF-E8998DAA0950");
      IImageList iml;
      int isize = SHIL_JUMBO;
      var hres = SHGetImageList(isize, ref iidImageList, out iml);
      IntPtr hIcon = IntPtr.Zero;
      int ILD_TRANSPARENT = 1;
      hres = iml.GetIcon(iconIndex, ILD_TRANSPARENT, ref hIcon);
      var icon = Icon.FromHandle(hIcon);

      DestroyIcon(shfi.hIcon);
      return icon;
    }

    public static Icon GetLargeIcon(string name)
    {
      SHFILEINFO shfi = new SHFILEINFO();
      uint flags = SHGFI_ICON | SHGFI_USEFILEATTRIBUTES;
      uint SHGFI_SYSICONINDEX = 0x4000;
      flags += SHGFI_SYSICONINDEX;

      SHGetFileInfo(name,
          FILE_ATTRIBUTE_NORMAL,
          out shfi,
          (uint)Marshal.SizeOf(shfi),
          flags);

      var iconIndex = shfi.iIcon;
      Guid iidImageList = new Guid("46EB5926-582E-4017-9FDF-E8998DAA0950");
      IImageList iml;
      int isize = SHIL_JUMBO;
      var hres = SHGetImageList(isize, ref iidImageList, out iml);
      IntPtr hIcon = IntPtr.Zero;
      int ILD_TRANSPARENT = 1;
      hres = iml.GetIcon(iconIndex, ILD_TRANSPARENT, ref hIcon);
      var icon = Icon.FromHandle(hIcon);

      DestroyIcon(shfi.hIcon);
      return icon;
    }
  }
}
