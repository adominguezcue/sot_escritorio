﻿namespace ZctTools.Controles
{
  partial class ZctControlFecha
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.zctSOTLabel1 = new ZctTools.Controles.ZctSOTLabel();
      this.zctSotDate1 = new ZctTools.Controles.ZctSotDate();
      this.zctSotTime1 = new ZctTools.Controles.ZctSotTime();
      this.SuspendLayout();
      // 
      // zctSOTLabel1
      // 
      this.zctSOTLabel1.AutoSize = true;
      this.zctSOTLabel1.Location = new System.Drawing.Point(3, 6);
      this.zctSOTLabel1.Name = "zctSOTLabel1";
      this.zctSOTLabel1.Size = new System.Drawing.Size(75, 13);
      this.zctSOTLabel1.TabIndex = 0;
      this.zctSOTLabel1.Text = "zctSOTLabel1";
      // 
      // zctSotDate1
      // 
      this.zctSotDate1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
      this.zctSotDate1.Location = new System.Drawing.Point(86, 2);
      this.zctSotDate1.Name = "zctSotDate1";
      this.zctSotDate1.Size = new System.Drawing.Size(91, 20);
      this.zctSotDate1.TabIndex = 1;
      // 
      // zctSotTime1
      // 
      this.zctSotTime1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
      this.zctSotTime1.Location = new System.Drawing.Point(183, 2);
      this.zctSotTime1.Name = "zctSotTime1";
      this.zctSotTime1.ShowUpDown = true;
      this.zctSotTime1.Size = new System.Drawing.Size(98, 20);
      this.zctSotTime1.TabIndex = 2;
      // 
      // ZctControlFecha
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.zctSotTime1);
      this.Controls.Add(this.zctSotDate1);
      this.Controls.Add(this.zctSOTLabel1);
      this.Name = "ZctControlFecha";
      this.Size = new System.Drawing.Size(289, 27);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ZctSOTLabel zctSOTLabel1;
    private ZctSotDate zctSotDate1;
    private ZctSotTime zctSotTime1;
  }
}
