﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace ZctTools.Controles
{
  public partial class ZctControlFecha : UserControl
  {
    public string Nombre {
      get {
        return zctSOTLabel1.Text;
      }
      set {
        zctSOTLabel1.Text = value;
      }
    }

    public DateTime Value
    {
      get
      {
        return DateTime.ParseExact(zctSotDate1.Value.ToString("d")+" "+zctSotTime1.Value.ToString("HH:mm:ss"), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
      }
      set
      {
        zctSotTime1.Value = value;
        zctSotDate1.Value = value;
      }
    }

    public int DateWidth {
      get {
        return zctSotDate1.Width;
      }
      set {
        zctSotDate1.Width = value;
      }
    }
    private void ZctSOTLabel1_Resize(object sender, EventArgs e) {
      zctSotDate1.Left = zctSOTLabel1.Width + zctSOTLabel1.Left + 5;
      zctSotTime1.Left= zctSotDate1.Width + zctSotDate1.Left + 5;
      this.Width = zctSOTLabel1.Width + zctSotDate1.Width + zctSotTime1.Width + 15;
    }

    private void ZctControlFecha_Resize(object sender, EventArgs e) {
      zctSotTime1.Left = Math.Abs(this.Width-5- zctSotTime1.Width);
      zctSotDate1.Left = Math.Abs(zctSotTime1.Left - 5 - zctSotDate1.Width);
      zctSOTLabel1.Left = Math.Abs(zctSotDate1.Left - 5 - zctSOTLabel1.Width);
    }

    private void ZctSotDate1_Resize(object sender, EventArgs e) {
      zctSotTime1.Left = zctSotDate1.Width + zctSotDate1.Left + 5;
      zctSotTime1.Width = Math.Abs(this.Width - (zctSotTime1.Left+5));
    }
    public void ClearValue() {
      zctSotDate1.Value = DateTime.Now;
      zctSotTime1.Value = DateTime.Now;
    }
    public ZctControlFecha()
    {
      InitializeComponent();
      zctSOTLabel1.Resize += ZctSOTLabel1_Resize;
      this.Resize += ZctControlFecha_Resize;
      zctSotDate1.Resize+= ZctSotDate1_Resize;
      ClearValue();
    }
  }
}
