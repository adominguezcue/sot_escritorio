﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using ZctTools.Clases;
using ZctTools.Forms;

namespace ZctTools.Controles
{
  public class ZctSOTToolTip : ToolTip { }

  public class ZctSOTLabel : Label { }

  public class ZctSOTTextBox : TextBox
  {
    ClassGen.Tipo_Dato _TipoDato = ClassGen.Tipo_Dato.ALFANUMERICO;

    public ClassGen.Tipo_Dato TipoDato
    {
      get
      {
        return _TipoDato;
      }
      set
      {
        _TipoDato = value;

        if (value == ClassGen.Tipo_Dato.ALFANUMERICO)
        {
          this.Text = "";
        }
        else {
          this.Text = "0";
        }

      }
    }

    protected override void OnLostFocus(EventArgs e)
    {
      if (_TipoDato == ClassGen.Tipo_Dato.NUMERICO) {
        if (!ClassGen.IsNumeric(base.Text)) {
          base.Text = "0";
        }
      }
      base.OnLostFocus(e);
    }

    protected override void OnKeyPress(KeyPressEventArgs e)
    {
      if (this.Multiline == false) {
        if ((int)e.KeyChar == (int)Keys.Enter) {
          e.Handled = true;
          SendKeys.Send("{TAB}");
        }
        else {
          base.OnKeyPress(e);
        }
      }
      else {
        base.OnKeyPress(e);
      }
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);
      if (e.KeyCode == Keys.F1)
      {
        if ((string)this.Tag != "")
        {
          ZctFrmAyuda fAyuda = new ZctFrmAyuda();
          fAyuda.Texto = (string)this.Tag;
          fAyuda.ShowDialog(this);
        }
      }
    }
  }

  public class ZctSOTLabelDesc : Label {
    public ZctSOTLabelDesc() {
      this.AutoSize = false;
      this.BorderStyle = BorderStyle.FixedSingle;
      this.BackColor = Color.LightBlue;
    }
  }

  public class ZctSOTButton : Button { }

  public class ZctSOTGroupBox : GroupBox { }

  public class ZctGroupControls : ZctSOTGroupBox {
    public void AddItem(Control cControl) {
      this.SuspendLayout();
      this.Controls.Add(cControl);
      cControl.Top = this.Controls.Count * 40;
      cControl.Left = 0;
      cControl.TabIndex = this.Controls.Count;
      cControl.Visible = true;
      this.ResumeLayout(false);
    }
  }
  public class ZctSOTComboBox : ComboBox {
    protected override void OnKeyPress(KeyPressEventArgs e)
    {
        if ((int)e.KeyChar == (int)Keys.Enter)
        {
          e.Handled = true;
          SendKeys.Send("{TAB}");
        }
        else
        {
          base.OnKeyPress(e);
        }
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);
      if (e.KeyCode == Keys.F1) {
        if ((string)this.Tag != "") {
          ZctFrmAyuda fAyuda = new ZctFrmAyuda();
          fAyuda.Texto = (string)this.Tag;
          fAyuda.ShowDialog(this);
        }
      }
    }
  }

  public class ZctSotGrid : DataGridView {
    public void SetColumn(string Nombre, string Propiedad, string Header, bool visible=true, string format="", int size=0, bool Editar=true ) {
      DataGridViewColumn col = new DataGridViewColumn();
      col.Name = Nombre;
      col.DataPropertyName = Propiedad;
      col.HeaderText = Header;
      col.Visible = visible;
      col.DefaultCellStyle.Format = format;
      col.CellTemplate = new DataGridViewTextBoxCell();
      col.ReadOnly = !Editar;
      col.Width=size > 0? size: col.Width;
      this.Columns.Add(col);
    }
  }

  public class ZctSotTime : DateTimePicker {
    public ZctSotTime() {
      this.Format = DateTimePickerFormat.Time;
      this.ShowUpDown = true;
    }
  }

  public class ZctSotDate : DateTimePicker {
    public ZctSotDate() {
      this.Format = DateTimePickerFormat.Short;
    }
  }
}
