﻿namespace ZctTools.Controles
{
  partial class ZctControlTexto
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.zctSOTLabel1 = new ZctTools.Controles.ZctSOTLabel();
      this.zctSOTTextBox1 = new ZctTools.Controles.ZctSOTTextBox();
      this.zctSOTToolTip1 = new ZctTools.Controles.ZctSOTToolTip();
      this.SuspendLayout();
      // 
      // zctSOTLabel1
      // 
      this.zctSOTLabel1.AutoSize = true;
      this.zctSOTLabel1.Location = new System.Drawing.Point(3, 6);
      this.zctSOTLabel1.Name = "zctSOTLabel1";
      this.zctSOTLabel1.Size = new System.Drawing.Size(77, 13);
      this.zctSOTLabel1.TabIndex = 0;
      this.zctSOTLabel1.Text = "zctSOTLabel1";
      this.zctSOTLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // zctSOTTextBox1
      // 
      this.zctSOTTextBox1.BackColor = System.Drawing.SystemColors.Window;
      this.zctSOTTextBox1.Location = new System.Drawing.Point(86, 3);
      this.zctSOTTextBox1.Name = "zctSOTTextBox1";
      this.zctSOTTextBox1.Size = new System.Drawing.Size(111, 20);
      this.zctSOTTextBox1.TabIndex = 1;
      this.zctSOTTextBox1.TipoDato = ZctTools.Clases.ClassGen.Tipo_Dato.ALFANUMERICO;
      //
      //ZctSOTToolTipInt
      //
      this.zctSOTToolTip1.AutoPopDelay = 5000;
      this.zctSOTToolTip1.InitialDelay = 50;
      this.zctSOTToolTip1.ReshowDelay = 100;
      this.zctSOTToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
      this.zctSOTToolTip1.ToolTipTitle = "Ayuda";
      // 
      // ZctControlTexto
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.zctSOTTextBox1);
      this.Controls.Add(this.zctSOTLabel1);
      this.Name = "ZctControlTexto";
      this.Size = new System.Drawing.Size(200, 28);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ZctSOTLabel zctSOTLabel1;
    private ZctSOTTextBox zctSOTTextBox1;
    private ZctSOTToolTip zctSOTToolTip1;
  }
}
