﻿namespace ZctTools.Controles
{
  partial class ZctControlBusqueda
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ZctSOTLabel1 = new ZctTools.Controles.ZctSOTLabel();
      this.zctSOTTextBox1 = new ZctTools.Controles.ZctSOTTextBox();
      this.zctSOTLabelDesc1 = new ZctTools.Controles.ZctSOTLabelDesc();
      this.SuspendLayout();
      // 
      // ZctSOTLabel1
      // 
      this.ZctSOTLabel1.AutoSize = true;
      this.ZctSOTLabel1.Location = new System.Drawing.Point(3, 6);
      this.ZctSOTLabel1.Name = "ZctSOTLabel1";
      this.ZctSOTLabel1.Size = new System.Drawing.Size(77, 13);
      this.ZctSOTLabel1.TabIndex = 0;
      this.ZctSOTLabel1.Text = "ZctSOTLabel1";
      // 
      // zctSOTTextBox1
      // 
      this.zctSOTTextBox1.Location = new System.Drawing.Point(86, 3);
      this.zctSOTTextBox1.Name = "zctSOTTextBox1";
      this.zctSOTTextBox1.Size = new System.Drawing.Size(111, 20);
      this.zctSOTTextBox1.TabIndex = 1;
      this.zctSOTTextBox1.TipoDato = ZctTools.Clases.ClassGen.Tipo_Dato.ALFANUMERICO;
      // 
      // zctSOTLabelDesc1
      // 
      this.zctSOTLabelDesc1.BackColor = System.Drawing.Color.LightBlue;
      this.zctSOTLabelDesc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.zctSOTLabelDesc1.Location = new System.Drawing.Point(200, 3);
      this.zctSOTLabelDesc1.Name = "zctSOTLabelDesc1";
      this.zctSOTLabelDesc1.Size = new System.Drawing.Size(104, 20);
      this.zctSOTLabelDesc1.TabIndex = 2;
      this.zctSOTLabelDesc1.Text = "zctSOTLabelDesc1";
      // 
      // ZctControlBusqueda
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.zctSOTLabelDesc1);
      this.Controls.Add(this.zctSOTTextBox1);
      this.Controls.Add(this.ZctSOTLabel1);
      this.Name = "ZctControlBusqueda";
      this.Size = new System.Drawing.Size(308, 27);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ZctSOTLabel ZctSOTLabel1;
    private ZctSOTTextBox zctSOTTextBox1;
    private ZctSOTLabelDesc zctSOTLabelDesc1;
  }
}
