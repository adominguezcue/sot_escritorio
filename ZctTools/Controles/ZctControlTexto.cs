﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZctTools.Clases;

namespace ZctTools.Controles
{
    public delegate void EvenHandler(object sender, EventArgs e);
    public partial class ZctControlTexto : UserControl
    {
        public event EvenHandler TextChange;
        public string Nombre
        {
            get
            {
                return zctSOTLabel1.Text;
            }
            set
            {
                zctSOTLabel1.Text = value;
            }
        }

        public ClassGen.Tipo_Dato TipoDato
        {
            get
            {
                return zctSOTTextBox1.TipoDato;
            }
            set
            {
                zctSOTTextBox1.TipoDato = value;
            }
        }

        public string Text
        {
            get
            {
                return zctSOTTextBox1.Text;
            }
            set
            {
                zctSOTTextBox1.Text = value;
            }
        }

        public bool Multiline
        {
            get
            {
                return zctSOTTextBox1.Multiline;
            }
            set
            {
                zctSOTTextBox1.Multiline = value;
            }
        }

        public object Tag
        {
            get
            {
                return zctSOTTextBox1.Tag;
            }
            set
            {
                zctSOTTextBox1.Tag = value;
            }
        }

        public string ToolTip
        {
            get
            {
                return zctSOTToolTip1.GetToolTip(zctSOTTextBox1);
            }
            set
            {
                zctSOTToolTip1.SetToolTip(zctSOTTextBox1, value);
            }
        }

        public ZctSOTTextBox TextBox
        {
            get
            {
                return zctSOTTextBox1;
            }
        }
        private void ZctSOTLabel1_Resize(object sender, System.EventArgs e)
        {
            zctSOTTextBox1.Left = zctSOTLabel1.Width + zctSOTLabel1.Left + 5;
            zctSOTTextBox1.Width = Math.Abs(this.Width - (zctSOTTextBox1.Left + 5));
        }
        private void ZctControlTexto_Resize(object sender, System.EventArgs e)
        {
            zctSOTTextBox1.Width = Math.Abs(this.Width - (zctSOTTextBox1.Left + 5));
            zctSOTTextBox1.Height = Math.Abs(this.Height - 10);
        }

        private void ZctSOTTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (TextChange != null)
                TextChange(sender, e);
        }

        private void ZctSOTTextBox1_LostFocus(object sender, EventArgs e)
        {
            base.OnLostFocus(e);
        }
        public ZctControlTexto()
        {
            InitializeComponent();
            this.Resize += ZctControlTexto_Resize;
            zctSOTLabel1.Resize += ZctSOTLabel1_Resize;
            zctSOTTextBox1.TextChanged += ZctSOTTextBox1_TextChanged;
            zctSOTTextBox1.LostFocus += ZctSOTTextBox1_LostFocus;
        }

    }
}