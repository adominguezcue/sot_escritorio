﻿using System;
using System.Windows.Forms;
using ZctTools.Clases;
using ZctTools.Controller;
using System.Data;
using ZctTools.Forms;
using System.ComponentModel;

namespace ZctTools.Controles
{
  public partial class ZctControlBusqueda : UserControl
  {
    private string _SPName;
    private string _Tabla;
    private ClassGen.Tipo_Dato _TipoDato = ClassGen.Tipo_Dato.NUMERICO;
    private bool _Validar = true;
    private string _SqlBusqueda;
    private string _SpVariables;
    private string _SPParametros;

    public string SPName {
      get {
        return _SPName;
      }
      set {
        this._SPName = value;
      }
    }

    public string Tabla
    {
      get
      {
        return _Tabla;
      }
      set
      {
        this._Tabla = value;
      }
    }

    public ClassGen.Tipo_Dato Tipo_Dato
    {
      get
      {
        return _TipoDato;
      }
      set
      {
        this._TipoDato = value;
      }
    }

    public ClassGen.Tipo_Dato TipoDato
    {
      get
      {
        return zctSOTTextBox1.TipoDato;
      }
      set
      {
        this.zctSOTTextBox1.TipoDato = value;
      }
    }
    public bool Validar
    {
      get
      {
        return _Validar;
      }
      set
      {
        this._Validar = value;
      }
    }

    public string SqlBusqueda
    {
      get
      {
        return _SqlBusqueda;
      }
      set
      {
        this._SqlBusqueda = value;
      }
    }

    public string SpVariables
    {
      get
      {
        return _SpVariables;
      }
      set
      {
        this._SpVariables = value;
      }
    }

    public string SPParametros
    {
      get
      {
        return _SPParametros;
      }
      set
      {
        this._SPParametros = value;
      }
    }

    public string Nombre
    {
      get
      {
        return ZctSOTLabel1.Text;
      }
      set
      {
        this.ZctSOTLabel1.Text = value;
      }
    }

    public override string Text
    {
      get
      {
        return zctSOTTextBox1.Text;
      }
      set
      {
        this.zctSOTTextBox1.Text = value;
      }
    }

    public string Descripcion
    {
      get
      {
        return zctSOTLabelDesc1.Text;
      }
      set
      {
        this.zctSOTLabelDesc1.Text = value;
      }
    }

    public int TextWith
    {
      get
      {
        return zctSOTTextBox1.Width;
      }
      set
      {
        this.zctSOTTextBox1.Width = value;
      }
    }

    private void ZctSOTTextBox1_Resize(object sender, System.EventArgs e){
      zctSOTLabelDesc1.Left = zctSOTTextBox1.Width + zctSOTTextBox1.Left + 5;
      zctSOTLabelDesc1.Width = Math.Abs(this.Width-(zctSOTLabelDesc1.Left+5));
    }

    private void ZctSOTLabel1_Resize(object sender, System.EventArgs e)
    {
      zctSOTTextBox1.Left = ZctSOTLabel1.Width + ZctSOTLabel1.Left + 5;
      zctSOTLabelDesc1.Left = zctSOTTextBox1.Width + zctSOTTextBox1.Left + 5;
      zctSOTLabelDesc1.Width = Math.Abs(this.Width - (zctSOTLabelDesc1.Left + 5));
    }

    private void ZctControlBusqueda_Resize(object sender, System.EventArgs e)
    {
      zctSOTLabelDesc1.Width = Math.Abs(this.Width - (zctSOTLabelDesc1.Left + 5));
    }

    public object Tag {
      get {
        return zctSOTTextBox1.Tag;
      }
      set {
        zctSOTTextBox1.Tag = value;
      }
    }

    public void pCargaDescripcion() {
      try
      {
        DataTable DtTemp=null;
        ZctToolService ZctBuscCon = new ZctToolService(_conn);
        zctSOTTextBox1.Text = zctSOTTextBox1.Text.Replace("'", "-").Trim();
        if (_Tabla != "" && zctSOTTextBox1.Text != "" && zctSOTTextBox1.Text != "0")
        {
          switch (_TipoDato)
          {
            case ClassGen.Tipo_Dato.ALFANUMERICO:
              DtTemp=ZctBuscCon.BusquedaAlfa(_Tabla, zctSOTTextBox1.Text);
              break;
            case ClassGen.Tipo_Dato.NUMERICO:
                int param = ClassGen.IsNumeric(zctSOTTextBox1.Text) ? Int32.Parse(zctSOTTextBox1.Text) : 0;
                DtTemp = ZctBuscCon.Busqueda(_Tabla, param);
              break;
          }
          if (DtTemp != null) {
            if (DtTemp.Rows.Count > 0)
            {
              zctSOTLabelDesc1.DataBindings.Clear();
              zctSOTLabelDesc1.DataBindings.Add("Text", DtTemp, "Descripcion");
              DtTemp.Dispose();
            }
            else {
              if (_Validar==true) {
                MessageBox.Show("El dato no es correcto, verifique por favor.");
                zctSOTLabelDesc1.DataBindings.Clear();
                zctSOTLabelDesc1.Text = "";
                zctSOTTextBox1.Text = "";

              }
            }
          }
        }
        else {
          zctSOTLabelDesc1.DataBindings.Clear();
          zctSOTLabelDesc1.Text = "";
        }
      }
      catch(Exception e) {
        MessageBox.Show(e.Message);
        zctSOTLabelDesc1.DataBindings.Clear();
        zctSOTLabelDesc1.Text = "";
        zctSOTTextBox1.Text = "";
      }
    }

    private void ZctSOTTextBox1_KeyDown(object sender, KeyEventArgs e) {
      try { 
        if (e.KeyCode == Keys.F3) {
          ZctBusqueda fBusqueda = new ZctBusqueda(_conn);
          fBusqueda.sSPName = _SPName;
          fBusqueda.sSpVariables = _SpVariables;
          if (_SPParametros != "") {
            fBusqueda.sSPParametros = _SPParametros;
          }
          fBusqueda.ShowDialog(this);
          this.zctSOTTextBox1.Text = fBusqueda.iValor;
          SendKeys.Send("{TAB}");
        }
      }
      catch (Exception ex) {
          MessageBox.Show(ex.Message);
        }
    }
    private void ZctSOTTextBox1_LostFocus(object sender, System.EventArgs e) {
      pCargaDescripcion();
      base.OnLostFocus(e);
    }

    private void ZctSOTTextBox1_TextChanged(object sender, EventArgs e) {
    }
        string _conn;
    public ZctControlBusqueda()
    {
        if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            _conn = ZctSOT.Datos.DAO.Conexion.CadenaConexion();
      InitializeComponent();
      zctSOTTextBox1.Resize += ZctSOTTextBox1_Resize;
      zctSOTTextBox1.LostFocus += ZctSOTTextBox1_LostFocus;
      ZctSOTLabel1.Resize += ZctSOTLabel1_Resize;
      zctSOTTextBox1.KeyDown+= ZctSOTTextBox1_KeyDown;
      zctSOTTextBox1.TextChanged += ZctSOTTextBox1_TextChanged;
      this.Resize += ZctControlBusqueda_Resize;
    }
  }
}
