﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZctTools.Forms
{
  public partial class ZctFrmAyuda : Form
  {
    public ZctFrmAyuda()
    {
      InitializeComponent();
    }

    public string Texto{
      get{
        return ZctRichBox.Text;
      }
      set {
        ZctRichBox.Text = value;

        int iRow = 1;
        int iSelLast;
        while (iRow < ZctRichBox.Text.Length) {
          if (ZctRichBox.Text.IndexOf("<B>") > 0)
          {
            if (ZctRichBox.Text.IndexOf("</B>") > 0)
            {
              iSelLast = ZctRichBox.Text.IndexOf("</B>");
            }
            else {
              iSelLast = ZctRichBox.Text.Length;
            }

            ZctRichBox.Select(ZctRichBox.Text.IndexOf("<B>") - 1, Math.Abs(ZctRichBox.Text.IndexOf("<B>") - iSelLast - 1));
            ZctRichBox.Font = new Font("Tahoma", 10, FontStyle.Regular);
            ZctRichBox.SelectionFont = new Font("Tahoma", 12, FontStyle.Bold);
            ZctRichBox.Text = ZctRichBox.Text.Replace("<B>", "");
            ZctRichBox.Text = ZctRichBox.Text.Replace("</B>", "");
            iRow = iSelLast + 1;
          }
          else {
            iRow = ZctRichBox.Text.Length;
          }
        }
        ZctRichBox.Text = ZctRichBox.Text.Replace("<BR>", "\r\n");
      }
    }

  }
}
