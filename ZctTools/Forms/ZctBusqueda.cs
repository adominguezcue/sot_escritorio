﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZctTools.Controller;

namespace ZctTools.Forms
{
  public partial class ZctBusqueda : Form
  {
    private DataView dDataView;

    public string sSQLP;

    //Parametros para los SP
    public string sSPName;
    public string sSpVariables;
    public string sSPParametros;

    //Parametro de la busqueda 
    public int iColBusq;
    public string sBusq;
    public string sColNuevo = "ColNuevo";

    public string iValor;
    public string sServer;
    public int iRow;
    public int iCol;

    private void ZctBusqueda_Load(object sender, EventArgs e)
    {
      try { 
            if (sSPParametros == null || sSPParametros == ""){
              GetData();
            } else{
              GetData(sSPParametros);
            }
            this.DGBusqueda.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.DGBusqueda.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);

            if (this.DGBusqueda.ColumnCount >= 2) {
              TxtColBus.Nombre = DGBusqueda.Columns[1].HeaderText;
              TxtColBus.Text = "";
              TxtColBus.Enabled = true;
              TxtColBus.Focus();
            }
            if (this.DGBusqueda.Columns.Contains("ColNuevo")) {
              DGBusqueda.Columns["ColNuevo"].Visible = false;
            }

      }
      catch (Exception ex) {
          MessageBox.Show(ex.Message);
        }
    }

    private void GetData() {
      ZctToolService PkBusCon = new ZctToolService(_conn);

      if (DGBusqueda.Columns.Contains(sColNuevo) == true) {
        DGBusqueda.Columns[sColNuevo].Visible = false;
        DGBusqueda.DataSource = PkBusCon.GetData(sSPName, sSpVariables);
      }
      else {
        dDataView = new DataView(PkBusCon.GetData(sSPName, sSpVariables));
        DGBusqueda.DataSource = dDataView;
      }
    }

    private void GetData(string sSPParametros)
    {
      ZctToolService PkBusCon = new ZctToolService(_conn);

      if (DGBusqueda.Columns.Contains(sColNuevo) == true)
      {
        DGBusqueda.Columns[sColNuevo].Visible = false;
        DGBusqueda.DataSource = PkBusCon.GetData(sSPName, sSpVariables, sSPParametros);
      }
      else
      {
        dDataView = new DataView(PkBusCon.GetData(sSPName, sSpVariables, sSPParametros));
        DGBusqueda.DataSource = dDataView;
      }
    }
    private void cmdAceptar_Click(object sender, System.EventArgs e)
    {
      iValor = (string)DGBusqueda.CurrentRow.Cells[0].Value;
      this.Dispose();
      this.Close();
    }

    private void DGBusqueda_CellDoubleClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
    {
      if (e.RowIndex >= 0) {
        iValor = (string)DGBusqueda.CurrentRow.Cells[0].Value;
        this.Dispose();
        this.Close();
        Application.DoEvents();
      }
    }

    private void DGBusqueda_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        iValor = (string)DGBusqueda.CurrentRow.Cells[0].Value;
        this.Dispose();
        this.Close();
        Application.DoEvents();
      }
    }

    private void DGBusqueda_ColumnHeaderMouseDoubleClick(object sender, System.Windows.Forms.DataGridViewCellMouseEventArgs e)
    {
      if (e.ColumnIndex >= 0 && DGBusqueda.Columns[DGBusqueda.CurrentCell.ColumnIndex].ValueType.Name == "String")
      {
        TxtColBus.Nombre = DGBusqueda.Columns[e.ColumnIndex].HeaderText;
        TxtColBus.Text = "";
        TxtColBus.Enabled = true;
        TxtColBus.Focus();
      }
    }

    private void TxtColBus_TextChanged(object sender, EventArgs e)
    {
      try
      {
        if (TxtColBus.Nombre != "Columna:")
        {
          this.dDataView.RowFilter = TxtColBus.Nombre + " LIKE '%" + TxtColBus.Text + "%' ";
        }
      }
      catch (Exception ex) {
        MessageBox.Show("Error: "+ex.Message);
      }
    }
        string _conn;
    public ZctBusqueda(string conn)
    {
            _conn = conn;
      InitializeComponent();
      cmdAceptar.Click += cmdAceptar_Click;
      DGBusqueda.CellDoubleClick += DGBusqueda_CellDoubleClick;
      DGBusqueda.KeyDown += DGBusqueda_KeyDown;
      DGBusqueda.ColumnHeaderMouseDoubleClick += DGBusqueda_ColumnHeaderMouseDoubleClick;
      TxtColBus.TextChange += TxtColBus_TextChanged;
      base.Load+= ZctBusqueda_Load;
    }
  }
}
