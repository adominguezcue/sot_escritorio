﻿namespace ZctTools.Forms
{
  partial class ZctBusqueda
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.zctGroupControls1 = new ZctTools.Controles.ZctGroupControls();
      this.zctSOTLabelDesc1 = new ZctTools.Controles.ZctSOTLabelDesc();
      this.TxtColBus = new ZctTools.Controles.ZctControlTexto();
      this.DGBusqueda = new System.Windows.Forms.DataGridView();
      this.cmdAceptar = new System.Windows.Forms.Button();
      this.zctGroupControls1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.DGBusqueda)).BeginInit();
      this.SuspendLayout();
      // 
      // zctGroupControls1
      // 
      this.zctGroupControls1.Controls.Add(this.zctSOTLabelDesc1);
      this.zctGroupControls1.Controls.Add(this.TxtColBus);
      this.zctGroupControls1.Location = new System.Drawing.Point(3, -1);
      this.zctGroupControls1.Name = "zctGroupControls1";
      this.zctGroupControls1.Size = new System.Drawing.Size(565, 52);
      this.zctGroupControls1.TabIndex = 0;
      this.zctGroupControls1.TabStop = false;
      this.zctGroupControls1.Text = "Busqueda por:";
      // 
      // zctSOTLabelDesc1
      // 
      this.zctSOTLabelDesc1.BackColor = System.Drawing.Color.LightBlue;
      this.zctSOTLabelDesc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.zctSOTLabelDesc1.Location = new System.Drawing.Point(320, 13);
      this.zctSOTLabelDesc1.Name = "zctSOTLabelDesc1";
      this.zctSOTLabelDesc1.Size = new System.Drawing.Size(239, 33);
      this.zctSOTLabelDesc1.TabIndex = 1;
      this.zctSOTLabelDesc1.Text = "Doble Click en el encabezado de columna para seleccionar la busqueda (No datos nu" +
    "mericos)";
      // 
      // TxtColBus
      // 
      this.TxtColBus.Enabled = false;
      this.TxtColBus.Location = new System.Drawing.Point(6, 13);
      this.TxtColBus.Multiline = false;
      this.TxtColBus.Name = "TxtColBus";
      this.TxtColBus.Nombre = "Columna:";
      this.TxtColBus.Size = new System.Drawing.Size(315, 28);
      this.TxtColBus.TabIndex = 0;
      this.TxtColBus.TipoDato = ZctTools.Clases.ClassGen.Tipo_Dato.ALFANUMERICO;
      this.TxtColBus.ToolTip = "";
      // 
      // DGBusqueda
      // 
      this.DGBusqueda.AllowUserToAddRows = false;
      this.DGBusqueda.AllowUserToDeleteRows = false;
      this.DGBusqueda.AllowUserToOrderColumns = true;
      this.DGBusqueda.AllowUserToResizeRows = false;
      this.DGBusqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.DGBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.DGBusqueda.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
      this.DGBusqueda.Location = new System.Drawing.Point(3, 57);
      this.DGBusqueda.MultiSelect = false;
      this.DGBusqueda.Name = "DGBusqueda";
      this.DGBusqueda.ReadOnly = true;
      this.DGBusqueda.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.DGBusqueda.Size = new System.Drawing.Size(753, 413);
      this.DGBusqueda.TabIndex = 2;
      // 
      // cmdAceptar
      // 
      this.cmdAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.cmdAceptar.Location = new System.Drawing.Point(671, 476);
      this.cmdAceptar.Name = "cmdAceptar";
      this.cmdAceptar.Size = new System.Drawing.Size(75, 23);
      this.cmdAceptar.TabIndex = 3;
      this.cmdAceptar.Text = "Aceptar";
      this.cmdAceptar.UseVisualStyleBackColor = true;
      // 
      // ZctBusqueda
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(758, 506);
      this.Controls.Add(this.cmdAceptar);
      this.Controls.Add(this.DGBusqueda);
      this.Controls.Add(this.zctGroupControls1);
      this.Name = "ZctBusqueda";
      this.Text = "Busqueda";
      this.zctGroupControls1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.DGBusqueda)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private Controles.ZctGroupControls zctGroupControls1;
    private Controles.ZctSOTLabelDesc zctSOTLabelDesc1;
    private Controles.ZctControlTexto TxtColBus;
    private System.Windows.Forms.DataGridView DGBusqueda;
    private System.Windows.Forms.Button cmdAceptar;
  }
}