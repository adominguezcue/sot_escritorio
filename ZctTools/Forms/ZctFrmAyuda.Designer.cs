﻿namespace ZctTools.Forms
{
  partial class ZctFrmAyuda
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ZctRichBox = new System.Windows.Forms.RichTextBox();
      this.SuspendLayout();
      // 
      // ZctRichBox
      // 
      this.ZctRichBox.BackColor = System.Drawing.SystemColors.Info;
      this.ZctRichBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.ZctRichBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ZctRichBox.Location = new System.Drawing.Point(0, 0);
      this.ZctRichBox.Name = "ZctRichBox";
      this.ZctRichBox.ReadOnly = true;
      this.ZctRichBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
      this.ZctRichBox.Size = new System.Drawing.Size(365, 194);
      this.ZctRichBox.TabIndex = 0;
      this.ZctRichBox.Text = "";
      // 
      // ZctFrmAyuda
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(365, 194);
      this.Controls.Add(this.ZctRichBox);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ZctFrmAyuda";
      this.Opacity = 0.8D;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Ayuda";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.RichTextBox ZctRichBox;
  }
}