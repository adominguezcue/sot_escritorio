using System;
namespace CalendarioFiscal.Modelo
{
    partial class ZctCalendarioFiscal
    {
        
        public  string month_name {
            get {
                if (this._Mes > 0 && this._Anio > 0)
                    return new DateTime(this._Anio, this._Mes, 1).ToString("MMMM").ToUpper();
                else
                    return "";
            }
        }
    }
}
