﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CalendarioFiscal.Vista
{
    public partial class FrmCalendarioFiscal : Form
    {
        Controlador.Controlador controlador;
        List<Modelo.ZctCalendarioFiscal> list = new List<Modelo.ZctCalendarioFiscal>();
        public FrmCalendarioFiscal(string conn) {
            InitializeComponent();
            controlador = new Controlador.Controlador(conn);
            prepara_para_iniciar();
        }
        

        private void cmdCargar_Click(object sender, EventArgs e)
        {
            try
            {

                int anio = 0;
                if (txtAnio.Text.Trim() == "" || !int.TryParse(txtAnio.Text.Trim(), out anio))
                {
                    MessageBox.Show("Debe de ingresar un año correcto.");
                    return;
                }
                 list = controlador.ObtieneElementos(anio);
                //grid.DataSource = list;
                GridDatos.AutoGenerateColumns = false;
                GridDatos.DataSource = list;
                estaus_formulario(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmdGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                controlador.grabar(list);
                prepara_para_iniciar();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void prepara_para_iniciar()
        {
            estaus_formulario(false);
            limpia_grid();
            txtAnio.Text = "";
        }

        private void cmdCancelar_Click(object sender, EventArgs e)
        {
            prepara_para_iniciar();
        }

        private void estaus_formulario(bool activa){
            GridDatos.Enabled = activa;
            cmdGuardar.Enabled = activa;
            txtAnio.Enabled = !activa;
            cmdCargar.Enabled = !activa;
            cmdCancelar.Enabled = activa;
        }

        private void limpia_grid() {
            GridDatos.DataSource = null;
            list.Clear();
        }

    }
}
