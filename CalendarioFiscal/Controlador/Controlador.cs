﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalendarioFiscal.Controlador
{
    internal class Controlador
    {

        Modelo.ModeloDataContext datos;
        public Controlador(string conn) {
            datos = new Modelo.ModeloDataContext(conn); 
        }
        
        
        internal List<Modelo.ZctCalendarioFiscal> ObtieneElementos(int anio)
        {

            List<Modelo.ZctCalendarioFiscal> list = (from m in datos.ZctCalendarioFiscal where m.Anio == anio select m).ToList();
            if (list.Count == 0){
                for (int i = 1; i<= 12; i++){
                    list.Add(new Modelo.ZctCalendarioFiscal(){ Anio = anio, Mes=i , fecha_ini= new DateTime(anio, i, 1),  fecha_fin= new DateTime(anio, i, 1).AddMonths(1).AddDays(-1)   });
                }
                datos.ZctCalendarioFiscal.InsertAllOnSubmit(list);
                datos.SubmitChanges();
            }
            return list;
        }




        internal void grabar(List<Modelo.ZctCalendarioFiscal> list)
        {
           datos.SubmitChanges();
        }
    }
}
